

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-04-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="VHisXEaWhaRglPZtiXNnz/7jCvqk6RqgTUDFUp2b3+k1/Flnu2WlK2jch3R21jUU8mPniGdYJ74NIZEoe8HUbKSvloxksub42huRK/W/ZT/Hw2Nu7lWF4bH8mPI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="95527DA8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 16 - April 29, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20410272">TRIM5{alpha} Disrupts the Structure of Assembled HIV-1 Capsid Complexes in vitro.</a> Black LR, Aiken C. J Virol. 2010 Apr 21. [Epub ahead of print]PMID: 20410272</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20405956">Three New Indole Alkaloids from Trigonostemon lii.</a>Tan CJ, Di YT, Wang YH, Zhang Y, Si YK, Zhang Q, Gao S, Hu XJ, Fang X, Li SF, Hao XJ. Org Lett. 2010 Apr 21. [Epub ahead of print]PMID: 20405956</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20405850">A New DNA Building Block, 4&#39;-Selenothymidine: Synthesis and Modification to 4&#39;-Seleno-AZT as a Potential Anti-HIV Agent.</a>Alexander V, Choi WJ, Chun J, Kim HO, Jeon JH, Tosh DK, Lee HW, Chandra G, Choi J, Jeong LS. Org Lett. 2010 Apr 20. [Epub ahead of print]PMID: 20405850</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20164190">The acyclic 2,4-diaminopyrimidine nucleoside phosphonate acts as a purine mimetic in HIV-1 reverse transcriptase DNA polymerization.</a>Herman BD, Votruba I, Holy A, Sluis-Cremer N, Balzarini J. J Biol Chem. 2010 Apr 16;285(16):12101-8. Epub 2010 Feb 17.PMID: 20164190</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20147291">Anti-HIV activity of defective cyanovirin-N mutants is restored by dimerization.</a> Matei E, Zheng A, Furey W, Rose J, Aiken C, Gronenborn AM. J Biol Chem. 2010 Apr 23;285(17):13057-65. Epub 2010 Feb 10.PMID: 20147291</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>6.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276220700001">GPG-NH2 acts via the metabolite alpha HGA to target HIV-1 Env to the ER-associated protein degradation pathway.</a> Jejcic, A; Hoglund, S; Vahlne, A.  RETROVIROLOGY, 2010; 7(20).</p>

    <p>7.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276291500019">Eastern extension of azoles as non-nucleoside inhibitors of HIV-1 reverse transcriptase; cyano group alternatives.</a>  Leung, CS; Zeevaart, JG; Domaoal, RA; Bollini, M; Thakur, VV; Spasov, KA; Anderson, KS; Jorgensen, WL. BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(8): 2485-2488.</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276061200009">Synthesis of Cyclodextrin and Sugar-Based Oligomers for the Efavirenz Drug Delivery.</a>  Shown, I; Banerjee, S; Ramchandran, AV; Geckeler, KE; Murthy, CN. MACROMOLECULAR SYMPOSIA, 2010; 287: 51-59.</p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276326600021">Anti-HIV-1 protease activities of crude extracts of some Garcinia species growing in Tanzania.</a>  Magadula, JJ; Tewtrakul, S.  AFRICAN JOURNAL OF BIOTECHNOLOGY, 2010; 9(12): 1848-1852.</p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276502600001">(Hetero)aroyl esters of 2-(N-phthalimido)ethanol and analogues: parallel synthesis, anti-HIV-1 activity and cytotoxicity.</a>  Cesarini, S; Spallarossa, A; Ranise, A; Schenone, S; La Colla, P; Collu, G; Sanna, G; Loddo, R.  MEDICINAL CHEMISTRY RESEARCH, 2010; 19(4): 311-336.</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276358000070">Cyclosporine Blocks Incorporation of HIV-1 Envelope Glycoprotein into Virions.</a>  Sokolskaja, E; Olivari, S; Zufferey, M; Strambio-De-Castillia, C; Pizzato, M; Luban, J. JOURNAL OF VIROLOGY, 2010; 84(9): 4851-4855.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276528700006">Synthesis and anti-HIV activity of alkylated quinoline 2,4-diols.</a>  Ahmed, N; Brahmbhatt, KG; Sabde, S; Mitra, D; Singh, IP; Bhutani, KK.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(8): 2872-2879.</p>
    <br clear="all">

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276528700014">Antiviral activity of benzimidazole derivatives. II. Antiviral activity of 2-phenylbenzimidazole derivatives.</a>  Tonelli, M; Simone, M; Tasso, B; Novelli, F; Boido, V; Sparatore, F; Paglietti, G; Pricl, S; Giliberti, G; Blois, S; Ibba, C; Sanna, G; Loddo, R; La Colla, P.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(8): 2937-2953.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
