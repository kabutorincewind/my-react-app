

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-03-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="R8TVqRtE3mNZjluX74t4ZTBZIDsjpW9EKXGPgTB9UWp5xktEiwB28a5kRahGEZoyxLHSMid2frXQeomXejgMzT6kajGk6zht2x5+1/k0BF2zXfDydwRo/XdWR2U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="85DDD3D8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">February 17 - March 1, 2012</h1>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22342889">Inhibition of Hepatitis B Virus Replication by Phospholipid scramblase 1 in Vitro and in Vivo.</a> Yang, J., X. Zhu, J. Liu, X. Ding, M. Han, W. Hu, X. Wang, Z. Zhou, and S. Wang, Antiviral Research, 2012. <b>[Epub ahead of print]</b>; PMID[22342889].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22355232">Optimization of Benzoisothiazole dioxide Inhibitory Activity of the NS5B polymerase of HCV Genotype 4 Using Ligand-steered Homological Modeling, Reaction-driven Scaffold-hopping and Enovo Workflow.</a> Mahmoud, A.H., K.A. Mohamed Abouzid, D.A. El Ella, and M.A. Hamid Ismail, Bioinformation, 2011. 7(7): p. 328-333; PMID[22355232].</p>

    <p class="plaintext">[PubMed]. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22347373">Cellular Growth Kinetics Distinguish a Cyclophilin Inhibitor from an HSP90 Inhibitor as a Selective Inhibitor of Hepatitis C Virus</a>. Beran, R.K., R. Sharma, A.C. Corsa, Y. Tian, J. Golde, G. Lundgaard, W.E.t. Delaney, W. Zhong, and A.E. Greenstein, PloS One, 2012. 7(2): p. e30286; PMID[22347373].</p>

    <p class="plaintext">[PubMed]. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22342631">Novel Hepatitis C Virus Replicon Inhibitors: Synthesis and Structure-Activity Relationships of Fused Pyrimidine Derivatives.</a> Chris Krueger, A., D.L. Madigan, D.W. Beno, D.A. Betebenner, R. Carrick, B.E. Green, W. He, D. Liu, C.J. Maring, K.F. McDaniel, H. Mo, A. Molla, C.E. Motter, T.J. Pilot-Matias, M.D. Tufano, and D.J. Kempf, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22342631].</p>

    <p class="plaintext">[PubMed]. OV_0217-030112.</p>

    <p> </p>

    <p class="memofmt2-2">HSV-1</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22373524">Synthesis and anti-HSV-1 Evaluation of New 3H-Benzo[b]pyrazolo[3,4-h]-1,6-naphthyridines and 3H-Pyrido[2,3-b]pyrazolo[3,4-h]-1,6-naphthyridines.</a> Bernardino, A.M., A.R. Azevedo, L.C. Pinheiro, J.C. Borges, I.C. Paixao, M. Mesquita, T.M. Souza, and M.S. Dos Santos, Organic and Medicinal Chemistry Letters, 2012. 2(1): p. 3; PMID[22373524].</p>

    <p class="plaintext">[PubMed]. OV_0217-030112.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298636700140">Novel HCV NS5B polymerase Inhibitors: Discovery of Indole C2 acyl sulfonamides.</a> Anilkumar, G.N., O. Selyutin, S.B. Rosenblum, Q.B. Zeng, Y.H. Jiang, T.Y. Chan, H.Y. Pu, L. Wang, F. Bennett, K.X. Chen, C.A. Lesburg, J. Duca, S. Gavalas, Y.H. Huang, P. Pinto, M. Sannigrahi, F. Velazquez, S. Venkatraman, B. Vibulbhan, S. Agrawal, E. Ferrari, C.K. Jiang, H.C. Huang, N.Y. Shih, F.G. Njoroge, and J.A. Kozlowski, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(1): p. 713-717; PMID[WOS:000298636700140].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299378305455">Discovery of BILB 1941, a Non-nucleoside polymerase Inhibitor for the Treatment of Hepatitis C Virus Infection.</a> Brochu, C., Abstracts of Papers of the American Chemical Society, 2011. 242; PMID[WOS:000299378305455].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299453300019">Structure-Activity Relationship (SAR) Development and Discovery of Potent Indole-based Inhibitors of the Hepatitis C Virus (HCV) NS5B Polymerase.</a> Chen, K.X., B. Vibulbhan, W.Y. Yang, M. Sannigrahi, F. Velazquez, T.Y. Chan, S. Venkatraman, G.N. Anilkumar, Q.B. Zeng, F. Bennet, Y.H. Jiang, C.A. Lesburg, J. Duca, P. Pinto, S. Gayalas, Y.H. Huang, W.L. Wu, O. Selyutin, S. Agrawal, B. Feld, H.C. Huang, C. Li, K.C. Cheng, N.Y. Shih, J.A. Kozlowski, S.B. Rosenblum, and F.G. Njoroge, Journal of Medicinal Chemistry, 2012. 55(2): p. 754-765; PMID[WOS:000299453300019].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299496100013">Towards a Stable Noeuromycin Analog with a D-Manno Configuration: Synthesis and Glycosidase Inhibition of D-Manno-like Tri- and Tetrahydroxylated azepanes.</a> Deschamp, J., M. Mondon, S. Nakagawa, A. Kato, D.S. Alonzi, T.D. Butters, Y.M. Zhang, M. Sollogoub, and Y. Bleriot, Bioorganic &amp; Medicinal Chemistry, 2012. 20(2): p. 641-649; PMID[WOS:000299496100013].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299734600014">Antiviral Activity of Conyza canadensis (L.) Cronquist Extracts Grown in Tunisia.</a> Edziri, H.L., G. Laurent, A. Mahjoub, and M. Mastouri, African Journal of Biotechnology, 2011. 10(45): p. 9097-9100; PMID[WOS:000299734600014].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299496100038">Pyridobenzothiazole Derivatives as New Chemotype Targeting the Hat NS5B polymerase.</a> Manfroni, G., F. Meschini, M.L. Barreca, P. Leyssen, A. Samuele, N. Iraci, S. Sabatini, S. Massari, G. Maga, J. Neyts, and V. Cecchetti, Bioorganic &amp; Medicinal Chemistry, 2012. 20(2): p. 866-876; PMID[WOS:000299496100038].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299378302136">Model for Inhibition of HCV NS5b polymerase by Nucleoside Analogs.</a> Perry, J.K., Abstracts of Papers of the American Chemical Society, 2011. 242; PMID[WOS:000299378302136].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299378304490">Discovery of the Novel HCV NS3 protease Inhibitor, GS-9451.</a> Pyun, H., C. Sheng, T. Appleby, O. Barauskas, T. Butler, R. Cai, X.W. Chen, A. Cho, M. Clarke, A. Corsa, E. Doerffler, M. Hung, M.Z. Ji, J. Link, X.H. Liu, R. Pakdaman, M. Robinson, B. Schultz, C. Tay, Q.Y. Wu, J. Xu, C. Yang, H.L. Yang, W. Delaney, and C. Kim, Abstracts of Papers of the American Chemical Society, 2011. 242; PMID[WOS:000299378304490].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299378304405">Structure-based Drug Design and the Evaluation of Irreversible HCV-protease Inhibitors.</a> Qiao, L.X., D.Q. Niu, Z.D. Zhu, M. Labenski, M. Hagel, T. St Martin, M. Sheet, H. Bernard, M. Nacht, W. Westlin, R. Petter, and J. Singh, Abstracts of Papers of the American Chemical Society, 2011. 242; PMID[WOS:000299378304405].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299510200016">Inhibition of Hepatitis C Virus Replication by Single and Dual Small Interfering RNA Using an HCV-infected Cell Model.</a>. Xing, X.K., S.J. Li, J.L. He, and Z. Chen, Biotechnology Letters, 2012. 34(2): p. 295-301; PMID[WOS:000299510200016].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299378305580">Diastereoselective Synthesis of a Purine phosphoramidate Prodrug Inhibitor of HCV NS5B polymerase.</a> Zhang, H.R., P.G. Reddy, B.S. Ross, S. Rachakonda, and M.J. Sofia, Abstracts of Papers of the American Chemical Society, 2011. 242; PMID[WOS:000299378305580].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0217-030112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
