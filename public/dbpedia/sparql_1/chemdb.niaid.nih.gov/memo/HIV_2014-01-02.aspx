

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-01-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Kk+SO6FBdKvP2Nih8yHHjJE5t2gRjkhnBrxt1ykBDvhLnY2AdHF1ebkGVXowjLos2Y1Z603vYNXueHqre2ahfK9UtAdvfszjogobCok7E/JBptCETbTAoQnj5dY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1470277F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: December 20, 2013 - January 2, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24155399">Effects of Alpha Interferon Treatment on Intrinsic anti-HIV-1 Immunity in Vivo.</a>Abdel-Mohsen, M., X. Deng, T. Liegler, J.C. Guatelli, M.S. Salama, D. Ghanem Hel, A. Rauch, B. Ledergerber, S.G. Deeks, H.F. Gunthard, J.K. Wong, and S.K. Pillai. Journal of Virology, 2014. 88(1): p. 763-767. PMID[24155399].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24145548">Activity of and Effect of Subcutaneous Treatment with the Broad-spectrum Antiviral Lectin Griffithsin in Two Laboratory Rodent Models.</a> Barton, C., J.C. Kouokam, A.B. Lasnik, O. Foreman, A. Cambon, G. Brock, D.C. Montefiori, F. Vojdani, A.A. McCormick, B.R. O&#39;Keefe, and K.E. Palmer. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 120-127. PMID[24145548].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24155391">Discovery of Novel Ribonucleoside Analogs with Activity against Human Immunodeficiency Virus Type 1.</a> Dapp, M.J., L. Bonnac, S.E. Patterson, and L.M. Mansky. Journal of Virology, 2014. 88(1): p. 354-363. PMID[24155391].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24155393">Kinase Control of Latent HIV-1 Infection: PIM-1 Kinase as a Major Contributor to HIV-1 Reactivation.</a> Duverger, A., F. Wolschendorf, J.C. Anderson, F. Wagner, A. Bosque, T. Shishido, J. Jones, V. Planelles, C. Willey, R.Q. Cron, and O. Kutsch. Journal of Virology, 2014. 88(1): p. 364-376. PMID[24155393].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23845925">Nano-NRTIs Demonstrate Low Neurotoxicity and High Antiviral Activity against HIV Infection in the Brain.</a> Gerson, T., E. Makarov, T.H. Senanayake, S. Gorantla, L.Y. Poluektova, and S.V. Vinogradov. Nanomedicine, 2014. 10(1): p. 177-185. PMID[23845925].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24269474">Development and in Vitro Evaluation of a Vaginal Microbicide Gel Formulation for UAMC01398, a Novel Diaryltriazine NNRTI against HIV-1.</a> Grammen, C., K.K. Arien, M. Venkatraj, J. Joossens, P. Van der Veken, J. Heeres, P.J. Lewi, S. Haenen, K. Augustyns, G. Vanham, P. Augustijns, and J. Brouwers. Antiviral Research, 2014. 101: p. 113-121. PMID[24269474].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24371077">GS-8374, a Prototype Phosphonate-containing Inhibitor of HIV-1 Protease, Effectively Inhibits Protease Mutants with Amino acid Insertions.</a>Grantz Saskova, K., M. Kozisek, K. Stray, D. de Jong, P. Rezacova, J. Brynda, N.M. van Maarseveen, M. Nijhuis, T. Cihlar, and J. Konvalinka. Journal of Virology, 2013. <b>[Epub ahead of print]</b>. PMID[24371077].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24227837">Novel Neuroprotective GSK-3beta Inhibitor Restricts Tat-mediated HIV-1 Replication.</a>Guendel, I., S. Iordanskiy, R. Van Duyne, K. Kehn-Hall, M. Saifuddin, R. Das, E. Jaworski, G.C. Sampey, S. Senina, L. Shultz, A. Narayanan, H. Chen, B. Lepene, C. Zeng, and F. Kashanchi. Journal of Virology, 2014. 88(2): p. 1189-1208. PMID[24227837].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24202207">Enantioselective Inhibition of Reverse Transcriptase (RT) of HIV-1 by Non-racemic Indole-based Trifluoropropanoates Developed by Asymmetric Catalysis Using Recyclable Organocatalysts.</a> Han, X., W. Ouyang, B. Liu, W. Wang, P. Tien, S. Wu, and H.B. Zhou. Organic &amp; Biomolecular Chemistry, 2013. 11(48): p. 8463-8475. PMID[24202207].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24379202">In Vitro Characterization of MK-1439: A Novel HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitor.</a> Lai, M.T., M. Feng, J.P. Falgueyret, P. Tawa, M. Witmer, D. Distefano, Y. Li, J. Burch, N. Sachs, M. Lu, E. Cauchon, L.C. Campeau, J. Grobler, Y. Yan, Y. Ducharme, B. Cote, E. Asante-Appiah, D.J. Hazuda, and M.D. Miller. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24379202].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23934770">Non-Nucleoside Reverse Transcriptase Inhibitor (NNRTI) Cross-resistance: Implications for Preclinical Evaluation of Novel NNRTIs and Clinical Genotypic Resistance Testing.</a> Melikian, G.L., S.Y. Rhee, V. Varghese, D. Porter, K. White, J. Taylor, W. Towner, P. Troia, J. Burack, E. Dejesus, G.K. Robbins, K. Razzeca, R. Kagan, T.F. Liu, W.J. Fessel, D. Israelski, and R.W. Shafer. The Journal of Antimicrobial Chemotherapy, 2014. 69(1): p. 12-20. PMID[23934770].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23591718">Protective Effect of Vaginal Application of Neutralizing and Nonneutralizing Inhibitory Antibodies against Vaginal SHIV Challenge in Macaques.</a>Moog, C., N. Dereuddre-Bosquet, J.L. Teillaud, M.E. Biedma, V. Holl, G. Van Ham, L. Heyndrickx, A. Van Dorsselaer, D. Katinger, B. Vcelar, S. Zolla-Pazner, I. Mangeot, C. Kelly, R.J. Shattock, and R. Le Grand. Mucosal Immunology, 2014. 7(1): p. 46-56. PMID[23591718].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24291042">Synthesis, anti-HIV Activity, Integrase Enzyme Inhibition and Molecular Modeling of Catechol, Hydroquinone and Quinol Labdane Analogs.</a> Pawar, R., T. Das, S. Mishra, Nutan, B. Pancholi, S.K. Gupta, and S.V. Bhat. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 302-307. PMID[24291042].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24361684.">The Tyrosine Kinase Inhibitor Dasatinib Blocks in-Vitro HIV-1 Production by Primary CD4+ T Cells from HIV-1 Infected Patients.</a>Pogliaghi, M., L. Papagno, S. Lambert, R. Calin, V. Calvez, C. Katlama, and B. Autran. AIDS, 2013. <b>[Epub ahead of print]</b>. PMID[24361684].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23999005">Unique Characteristics of Histone Deacetylase Inhibitors in Reactivation of Latent HIV-1 in Bcl-2-transduced Primary Resting CD4+ T Cells.</a>Shan, L., S. Xing, H.C. Yang, H. Zhang, J.B. Margolick, and R.F. Siliciano. The Journal of Antimicrobial Chemotherapy, 2014. 69(1): p. 28-33. PMID[23999005]. <b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23989454">HIV Cell Fusion Assay: Phenotypic Screening Tool for the Identification of HIV Entry Inhibitors via CXCR4.</a> Smith, E.B., R.A. Ogert, D. Pechter, A. Villafania, S.J. Abbondanzo, K. Lin, A. Rivera-Gines, C. Rebsch-Mastykarz, and F.J. Monsma, Jr. Journal of Biomolecular Screening, 2014. 19(1): p. 108-118. PMID[23989454].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24033292">Anti-HIV-1 Integrase Activity of Mimusops elengi Leaf Extracts.</a>Suedee, A., S. Tewtrakul, and P. Panichayupakaranant. Pharmaceutical Biology, 2014. 52(1): p. 58-61. PMID[24033292].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24173214">The Innate Immune Factor Apolipoprotein L1 Restricts HIV-1 Infection.</a>Taylor, H.E., A.K. Khatua, and W. Popik. Journal of Virology, 2014. 88(1): p. 592-603. PMID[24173214]. <b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24374442">Anti-HIV-1 Activities of Extracts and Phenolics from Smilax china L.</a>Wang, W.X., J.Y. Qian, X.J. Wang, A.P. Jiang, and A.Q. Jia. Pakistan Journal of Pharmaceutical Sciences, 2014. 27(1): p. 147-151. PMID[24374442].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24369352">10E8-like Neutralizing Antibodies against HIV-1 Induced Using a Precisely Designed Conformational Peptide as a Vaccine Prime.</a> Yu, Y., P. Tong, Y. Li, Z. Lu, and Y. Chen. Science China. Life Sciences, 2013. <b>[Epub ahead of print]</b>. PMID[24369352].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24254845">A Novel Modified Peptide Derived from Membrane-proximal External Region of Human Immunodeficiency Virus Type 1 Envelope Significantly Enhances Retrovirus Infection.</a>Zhang, L., C. Jiang, H. Zhang, X. Gong, L. Yang, L. Miao, Y. Shi, Y. Zhang, W. Kong, C. Zhang, and Y. Shan. Journal of Peptide Science, 2014. 20(1): p. 46-54. PMID[24254845].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24275349">Design, Synthesis and Preliminary SAR Studies of Novel N-Arylmethyl Substituted Piperidine-linked Aniline Derivatives as Potent HIV-1 NNRTIs.</a>Zhang, L., P. Zhan, X. Chen, Z. Li, Z. Xie, T. Zhao, H. Liu, E. De Clercq, C. Pannecouque, J. Balzarini, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 633-642. PMID[24275349].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24220107">A Novel Ribonuclease with Antiproliferative Activity toward Leukemia and Lymphoma Cells and HIV-1 Reverse Transcriptase Inhibitory Activity from the Mushroom, Hohenbuehelia serotina.</a> Zhang, R., L. Zhao, H. Wang, and T.B. Ng. International Journal of Molecular Medicine, 2014. 33(1): p. 209-214. PMID[24220107].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1220-010214.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327716800006">Pyrimidine-5-carbonitriles - Part III: Synthesis and Antimicrobial Activity of Novel 6-(2-Substituted propyl)-2,4-disubstituted pyrimidine-5-carbonitriles</a>. Al-Deeb, O.A., A.A. Al-Turkistani, E.S. Al-Abdullah, N.R. El-Brollosy, E.E. Habib, and A.A. El-Emam. Heterocyclic Communications, 2013. 19(6): p. 411-419. ISI[000327716800006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327814900021">Synthesis of 3,4-Disubstituted quinolin-2-(1H)-ones via Palladium-catalyzed Decarboxylative Arylation Reactions.</a> Carrer, A., J.D. Brion, S. Messaoudi, and M. Alami. Advanced Synthesis &amp; Catalysis, 2013. 355(10): p. 2044-2054. ISI[000327814900021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327787700013">Novel Piperidinylamino-diarylpyrimidine Derivatives with Dual Structural Conformations as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a>Chen, X.W., X. Liu, Q. Meng, D. Wanga, H.Q. Liu, E. De Clercq, C. Pannecouque, J. Balzarini, and X.Y. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6593-6597. ISI[000327787700013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326902300042">New Scaffolds of Natural Origin as Integrase LEDGF/P75 Interaction Inhibitors: Virtual Screening and Activity Assays.</a> De Luca, L., F. Morreale, F. Christ, Z. Debyser, S. Ferro, and R. Gitto. European Journal of Medicinal Chemistry, 2013. 68: p. 405-411. ISI[000326902300042].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327863400006">Cycloartanes from the Gum Resin of Gardenia gummifera L.F.</a>Gandhe, S., S. Lakavath, S. Palatheeya, W. Schuehly, K. Amancha, R.K.R. Nallamaddi, A. Palepu, Y. Thakur, V. Belvotagi, R.K. Bobbala, V. Achanta, and O. Kunert. Chemistry &amp; Biodiversity, 2013. 10(9): p. 1613-1622. ISI[000327863400006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327655100001">Parthenium hysterophorus: A Probable Source of Anticancer, Antioxidant and anti-HIV Agents.</a> Kumar, S., G. Chashoo, A.K. Saxena, and A.K. Pandey. Biomed Research International, 2013. 810734: p. 11. ISI[000327655100001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327945200003">Microwave-and Ultrasound-assisted Synthesis of Some Acyclonucleobases Based on a Uracil Moiety Using DMAP as Base.</a> Lahsasni, S.A. Nucleosides Nucleotides &amp; Nucleic Acids, 2013. 32(8): p. 439-452. ISI[000327945200003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327908500009">Structure-Activity Relationship Studies of the Aromatic Positions in Cyclopentapeptide CXCR4 Antagonists.</a> Mungalpara, J., Z.G. Zachariassen, S. Thiele, M.M. Rosenkilde, and J. Vabeno. Organic &amp; Biomolecular Chemistry, 2013. 11(47): p. 8202-8208. ISI[000327908500009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327875700002">Extracts from Acacia catechu Suppress HIV-1 Replication by Inhibiting the Activities of the Viral Protease and Tat.</a> Nutan, M. Modi, C.S. Dezzutti, S. Kulshreshtha, A.K.S. Rawat, S.K. Srivastava, S. Malhotra, A. Verma, U. Ranga, and S.K. Gupta. Virology Journal, 2013. 10. ISI[000327875700002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327657900025">Natural Stilbenoids Isolated from Grapevine Exhibiting Inhibitory Effects against HIV-1 Integrase and Eukaryote MOS1 Transposase in Vitro Activities.</a>Pflieger, A., P.W. Teguo, Y. Papastamoulis, S. Chaignepain, F. Subra, S. Munir, O. Delelis, P. Lesbats, C. Calmels, M.L. Andreola, J.M. Merillon, C. Auge-Gouillou, and V. Parissi. PloS One, 2013. 8(11): p. e81184. ISI[000327657900025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327285300021">Efficient Synthesis of 5&#39;-0(N)-Carbamyl and -Polycarbamyl Nucleosides.</a>Shelton, J.R. and M.A. Peterson. Tetrahedron Letters, 2013. 54(50): p. 6882-6885. ISI[000327285300021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326978600020">An Engineered HIV-1 gp41 Trimeric Coiled Coil with Increased Stability and anti-HIV-1 Activity: Implication for Developing anti-HIV Microbicides.</a>Tong, P., Z.F. Lu, X. Chen, Q. Wang, F. Yu, P. Zou, X.X. Yu, Y. Li, L. Lu, Y.H. Chen, and S.B. Jiang. Journal of Antimicrobial Chemotherapy, 2013. 68(11): p. 2533-2544. ISI[000326978600020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327940400016">Characterization of Structure, Dynamics, and Detergent Interactions of the anti-HIV Chemokine Variant 5P12-RANTES.</a>Wiktor, M., O. Hartley, and S. Grzesiek. Biophysical Journal, 2013. 105(11): p. 2586-2597. ISI[000327940400016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327882900013">Synthesis and anti-HIV Activity of a Series of 6-Modified 2&#39;,3&#39;-Dideoxyguanosine and 2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-dideoxyguanosine Analogs.</a> Xie, L.J., X.T. Yang, D.L. Pan, Y.L. Cao, M. Cao, G.C. Lin, Z. Guan, Y. Guo, L.H. Zhang, and Z.J. Yang. Chinese Journal of Chemistry, 2013. 31(9): p. 1207-1218. ISI[000327882900013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327491700010">Synthesis and Biological Evaluation of 2,6-Dichloropurine Bicyclonucleosides Containing a Triazolyl-carbohydrate Moiety.</a> Zhang, Q.R., P. He, G.Q. Zhou, Y.F. Gu, T. Fu, D.Q. Xue, and H.M. Liu. Carbohydrate Research, 2013. 382: p. 65-70. ISI[000327491700010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327543500102">DB-02, a C-6-Cyclohexylmethyl Substituted Pyrimidinone HIV-1 Reverse Transcriptase Inhibitor with Nanomolar Activity, Displays an Improved Sensitivity against K103N or Y181C Than S-DABOs.</a> Zhang, X.J., L.H. Lu, R.R. Wang, Y.P. Wang, R.H. Luo, C.C. Lai, L.M. Yang, Y.P. He, and Y.T. Zheng. PloS one, 2013. 8(11): p. e81489. ISI[000327543500102].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1220-010214.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131212&amp;CC=WO&amp;NR=2013182240A1&amp;KC=A1">Preparation of &#914;-Hairpin Peptidomimetics as CXCR4 Antagonists for Treatment of HIV Infection.</a> Gombert, F.O., D. Obrecht, A. Lederer, J. Zimmermann, and C. Oefner. Patent. 2013. 2012-EP60766 2013182240: 48pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1220-010214.</p>

    <br />

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131114&amp;CC=WO&amp;NR=2013169361A1&amp;KC=A1">Preparation of Thiosemicarbazone Iron Chelators as HIV-1 Inhibitors.</a> Nekhai, S. and D.B. Kovalskyy. Patent. 2013. 2013-US31231 2013169361: 58pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1220-010214.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
