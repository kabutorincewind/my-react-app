

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="l2gIz+X+77Bw/Q5sVn+ztMbP7A3rK8xw1/UxSJmSvYfZcKgEOYhZMS5V8Etwd21djJ8mQCOlXSEDbR4DqAxxC880RahtvvpN0yjaaWBr0HDURI/69yc+LwMFNCI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4AC991BE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: June, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402130300003">New Chalcones and Thiopyrimidine Analogues Derived from Mefenamic acid: Microwave-assisted Synthesis, anti-HIV Activity and Cytotoxicity as Antileukemic Agents.</a> Al-Hazam, H.A., Z.A. Al-Shamkani, N.A. Al-Masoudi, B.A. Saeed, and C. Pannecouque. Zeitschrift fur Naturforschung Section B-A Journal of Chemical Sciences, 2017. 72(4): p. 249-256. ISI[000402130300003].
    <br />
    <b>[WOS]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28408224">One Drug for Two Targets: Biological Evaluation of Antiretroviral Agents Endowed with Antiproliferative Activity.</a> Botta, L., G. Maccari, P. Calandro, M. Tiberi, A. Brai, C. Zamperini, F. Canducci, M. Chiariello, R. Marti-Centelles, E. Falomir, and M. Carda. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(11): p. 2502-2505. PMID[28408224].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28371664">Synthesis and Study of anti-HIV-1 RT Activity of 5-Benzoyl-4-methyl-1,3,4,5-tetrahydro-2H-1,5-benzodiazepin-2-one Derivatives.</a> Chander, S., C.R. Tang, H.M. Al-Maqtari, J. Jamalis, A. Penta, T.B. Hadda, H.M. Sirat, Y.T. Zheng, and M. Sankaranarayanan. Bioorganic Chemistry, 2017. 72: p. 74-79. PMID[28371664].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28624214">Further Characterization of the Bifunctional HIV Entry Inhibitor sCD4-FIT45.</a> Falkenhagen, A. and S. Joshi. Molecular Therapy - Nucleic Acids, 2017. 7: p. 387-395. PMID[28624214].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28507107">In Vitro Cross-resistance Profiles of Rilpivirine, Dapivirine, and MIV-150, Nonnucleoside Reverse Transcriptase Inhibitor Microbicides in Clinical Development for the Prevention of HIV-1 Infection.</a> Giacobbi, N.S. and N. Sluis-Cremer. Antimicrobial Agents &amp; Chemotherapy, 2017. 61(7): p. e00277-00217. PMID[28507107].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28448121">HIV-1 Frameshift RNA-Targeted Triazoles Inhibit Propagation of Replication-competent and Multi-drug-resistant HIV in Human Cells.</a> Hilimire, T.A., J.M. Chamberlain, V. Anokhina, R.P. Bennett, O. Swart, J.R. Myers, J.M. Ashton, R.A. Stewart, A.L. Featherston, K. Gates, E.D. Helms, H.C. Smith, S. Dewhurst, and B.L. Miller. ACS Chemical Biology, 2017. 12(6): p. 1674-1682. PMID[28448121]. PMCID[PMC5477779].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000398216100163">Towards Fluorinated Peptidic HIV-1 Fusion Inhibitors.</a> Huhmann, S., E. Nyakatura, and B. Koksch. Journal of Peptide Science, 2016. 22: p. S90-S90. ISI[000398216100163].
    <br />
    <b>[WOS]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28467486">The Effect of Ingenol-B on the Suppressive Capacity of Elite Suppressor HIV-specific CD8+ T Cells.</a> Kwaa, A.K., K. Goldsborough, V.E. Walker-Sperling, L.F. Pianowski, L. Gama, and J.N. Blankson. Plos One, 2017. 12(5): p. e0174516. PMID[28467486]. PMCID[PMC5414940].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28334941">RNA-protein Interactions Govern Antiviral Specificity and Encapsidation of Broad Spectrum anti-HIV Reverse Transcriptase Aptamers.</a> Lange, M.J., P.D.M. Nguyen, M.K. Callaway, M.C. Johnson, and D.H. Burke. Nucleic Acids Research, 2017. 45(10): p. 6087-6097. PMID[28334941].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402328000406">SERINC Inhibits HIV-1 Env Induced Membrane Fusion and Slows Fusion Pore Enlargement.</a> Markosyan, R.M., S.L. Liu, and F.S. Cohen. Biophysical Journal, 2017. 112(3): p. 79A-79A. ISI[000402328000406].
    <br />
    <b>[WOS]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400718500019">Diastereoselective Synthesis and Molecular Docking Studies of Novel Fused Tetrahydropyridine Derivatives as New Inhibitors of HIV Protease.</a> Mohammadi, A.A., S. Taheri, A. Amouzegar, R. Ahdenov, M.R. Halvagar, and A.S. Sadr. Journal of Molecular Structure, 2017. 1139: p. 166-174. ISI[000400718500019].
    <br />
    <b>[WOS]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28416550">Identification and Optimization of Thienopyridine Carboxamides as Inhibitors of HIV Regulatory Complexes.</a> Nakamura, R.L., M.A. Burlingame, S. Yang, D.C. Crosby, D.J. Talbot, K. Chui, A.D. Frankel, and A.R. Renslo. Antimicrobial Agents and Chemotherapy, 2017. 61(7): p. e02366-02316. PMID[28416550].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402049300021">Amiloride as a New RNA-binding Scaffold with Activity against HIV-1 TAR.</a> Patwardhan, N.N., L.R. Ganser, G.J. Kapral, C.S. Eubanks, J. Lee, B. Sathyamoorthy, H.M. Al-Hashimi, and A.E. Hargrove. MedChemComm, 2017. 8(5): p. 1022-1036. ISI[000402049300021].
    <br />
    <b>[WOS]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28634358">HIV-1 gp41-targeting Fusion Inhibitory Peptides Enhance the gp120-targeting Protein-mediated Inactivation of HIV-1 Virions.</a> Qi, Q., Q. Wang, W. Chen, L. Du, D.S. Dimitrov, L. Lu, and S. Jiang. Emerging Microbes &amp; Infections, 2017. 6(6): p. e59. PMID[28634358].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28286236">5-Hydroxytyrosol Inhibits HIV-1 Replication in Primary Cells of the Lower and Upper Female Reproductive Tract.</a> Saba, E., P. Panina-Bordignon, I. Pagani, M. Origoni, M. Candiani, C. Doglioni, G. Taccagni, S. Ghezzi, J. Alcami, E. Vicenzi, and G. Poli. Antiviral Research, 2017. 142: p. 16-20. PMID[28286236].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402328000405">SERINC5 Inhibits HIV Fusion through Inactivation of Env Glycoproteins and Interference with Productive Refolding of Env.</a> Sood, C., M. Marin, A. Chande, A.L. Mattheyses, K. Salaita, M. Pizzato, and G. Melikian. Biophysical Journal, 2017. 112(3): p. 79A-79A. ISI[000402328000405].
    <br />
    <b>[WOS].</b> HIV_06_2017.</p><br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000398216100027">Mid-size Drugs: Dimer &amp; Trimer Mimics of HIV-gp41 Peptides as Fusion Inhibitors.</a> Tamamura, H. Journal of Peptide Science, 2016. 22: p. S18-S20. ISI[000398216100027].
    <br />
    <b>[WOS]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28491940">Data on Eleven Sesquiterpenoids from the Cultured Mycelia of Ganoderma capense.</a> Tan, Z., J. Zhao, J. Liu, M. Zhang, R. Chen, K. Xie, and J. Dai. Data in Brief, 2017. 12: p. 361-363. PMID[28491940]. PMCID[PMC5413196].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28384548">Synthesis, Biological Evaluation and Molecular Modeling of 2-Hydroxyisoquinoline-1,3-dione Analogues as Inhibitors of HIV Reverse Transcriptase Associated Ribonuclease H and Polymerase.</a> Tang, J., S.K.V. Vernekar, Y.L. Chen, L. Miller, A.D. Huber, N. Myshakina, S.G. Sarafianos, M.A. Parniak, and Z. Wang. European Journal of Medicinal Chemistry, 2017. 133: p. 85-96. PMID[28384548].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402157900020">Evaluation of Biphenylalanine and Its Derivatives as Potential HIV-1 gp120 Attachment Inhibitors Based on Molecular Docking, CD4 Capture ELISA and Cytotoxicity Analysis.</a> Teoh, T.C., H.A. Rothan, and M.R. Idid. Chiang Mai Journal of Science, 2017. 44(2): p. 487-493. ISI[000402157900020].
    <br />
    <b>[WOS]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28605635">Expression of HERV-K108 Envelope Interferes with HIV-1 Production.</a> Terry, S.N., L. Manganaro, A. Cuesta-Dominguez, D. Brinzevich, V. Simon, and L.C.F. Mulder. Virology, 2017. 509: p. 52-59. PMID[28605635].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28525279">Double-winged 3-Hydroxypyrimidine-2,4-diones: Potent and Selective Inhibition against HIV-1 RNase H with Significant Antiviral Activity.</a> Vernekar, S.K.V., J. Tang, B. Wu, A.D. Huber, M.C. Casey, N. Myshakina, D.J. Wilson, J. Kankanala, K.A. Kirby, M.A. Parniak, S.G. Sarafianos, and Z. Wang. Journal of Medicinal Chemistry, 2017. 60(12): p. 5045-5056. PMID[28525279].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28431877">Synthesis and Evaluation of Pyridinium Polyoxometalates as anti-HIV-1 Agents.</a> Wang, S., W. Sun, Q. Hu, H. Yan, and Y. Zeng. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(11): p. 2357-2359. PMID[28431877].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28588284">A Novel Preventive Strategy against HIV-1 Infection: Combinatorial Use of Inhibitors Targeting the Nucleocapsid and Fusion Proteins.</a> Yang, Y., J. Zhu, M. Hassink, L.M.M. Jenkins, Y. Wan, D.H. Appella, J. Xu, E. Appella, and X. Zhang. Emerging Microbes &amp; Infections, 2017. 6(6): p. e40. PMID[28588284].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28613071">Potent Inhibitor of Drug-resistant HIV-1 Strains Identified from the Medicinal Plant Justicia gendarussa.</a> Zhang, H.J., E. Rumschlag-Booms, Y.F. Guan, D.Y. Wang, K.L. Liu, W.F. Li, V.H. Nguyen, N.M. Cuong, D.D. Soejarto, H.H.S. Fong, and L. Rong. Journal of Natural Products, 2017. 80(6): p. 1798-1807. PMID[28613071].
    <br />
    <b>[PubMed]</b>. HIV_06_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170622&amp;CC=WO&amp;NR=2017106069A1&amp;KC=A1">Antiviral Oxime phosphoramide Compounds.</a> Fu, W., Z. Guo, N. Qi, I.T. Raheem, P. Vachal, M. Wang, and J.D. Schreier. Patent. 2017. 2016-US66075 2017106069: 69pp.
    <br />
    <b>[Patent]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170526&amp;CC=WO&amp;NR=2017087257A1&amp;KC=A1">Amido-substituted Pyridotriazine Derivatives Useful as HIV Integrase Inhibitors and Their Preparation.</a> Graham, T.H., T. Yu, S.T. Waddell, J.A. McCauley, A. Stamford, W. Liu, J.A. Grobler, and L. Xu. Patent. 2017. 2016-US61456 2017087257: 77pp.
    <br />
    <b>[Patent]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170526&amp;CC=WO&amp;NR=2017087256A1&amp;KC=A1">Spirocyclic Pyridotriazine Derivatives Useful as HIV Integrase Inhibitors and Their Preparation.</a> Graham, T.H., T. Yu, S.T. Waddell, J.A. McCauley, A. Stamford, W. Liu, J.A. Grobler, and L. Xu. Patent. 2017. 2016-US61455 2017087256: 88pp.
    <br />
    <b>[Patent]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">29. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170608&amp;CC=WO&amp;NR=2017096161A1&amp;KC=A1">Compositions and Methods for Reactivating Latent Human Immunodeficiency Virus Using an Akt Activator.</a> Gramatica, A. and W.C. Greene. Patent. 2017. 2016-US64614 2017096161: 50pp.
    <br />
    <b>[Patent]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">30. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170608&amp;CC=WO&amp;NR=2017093938A1&amp;KC=A1">Preparation of Tetrahydroisoquinoline Derivatives as Antivirals Agents for the Treatment of HIV Infection.</a> Johns, B.A., E.J. Velthuisen, and J.G. Weatherhead. Patent. 2017. 2016-IB57270 2017093938: 225pp.
    <br />
    <b>[Patent]</b>. HIV_06_2017.</p><br />

    <p class="plaintext">31. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170608&amp;CC=WO&amp;NR=2017093937A1&amp;KC=A1">Preparation of Isoindoline Derivatives for the Treatment of Viral Infections.</a> Johns, B.A., E.J. Velthuisen, and J.G. Weatherhead. Patent. 2017. 2016-IB57269 2017093937: 34pp.
    <br />
    <b>[Patent]</b>. HIV_06_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
