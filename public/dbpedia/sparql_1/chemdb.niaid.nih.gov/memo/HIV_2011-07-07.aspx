

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-07-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nmOcCOIYHC9S+ipUTM2KytpdbkhxvoVrxrQndiBj5S5FVMIrsetupIPvwlvTypRAPvYE+n7Od4UM9P8ocI4uqYBcIxyMkYJCjq4UdXnudlQcRn7lwcQE69eJUvY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F450BBB6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  June 24, 2011 - July 7, 2011</h1>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21723379">Inhibition of HIV-1 Reverse Transcriptase, Toxicological and Chemical Profile of Calophyllum brasiliense Extracts from Chiapas, Mexico.</a> Cesar, G.Z., M.G. Alfonso, M.M. Marius, E.M. Elizabeth, C.B. Angel, H.R. Maira, C.L. Guadalupe, J.E. Manuel, and R.C. Ricardo. Fitoterapia. <b>[Epub ahead of print]</b>; PMID[21723379].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21728968">Diketo Acids Derivatives as Dual Inhibitors of Human Immunodeficiency Virus Type 1 Integrase and the Reverse Transcriptase RNase H Domain.</a> Di Santo, R. Current Medicinal Chemistry. <b>[Epub ahead of print]</b>; PMID[21728968].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21725586">An anti-HIV-1 Compound that Increases Steady-state Expression of Apoplipoprotein B mRNA-editing Enzyme-catalytic Polypeptide-like 3G.</a> Ejima, T., M. Hirota, T. Mizukami, M. Otsuka, and M. Fujita. International Journal of Molecular Medicine. <b>[Epub ahead of print]</b>; PMID[21725586].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21650224">Lindenane Disesquiterpenoids with anti-HIV-1 Activity from Chloranthus japonicus.</a> Fang, P.L., Y.L. Cao, H. Yan, L.L. Pan, S.C. Liu, N.B. Gong, Y. Lu, C.X. Chen, H.M. Zhong, Y. Guo, and H.Y. Liu. Journal of Natural Products. 74(6): p. 1408-1413; PMID[21650224].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21648396">A Concise Route to Dihydrobenzo[b]furans: Formal Total Synthesis of (+)-Lithospermic Acid.</a> Fischer, J., G.P. Savage, and M.J. Coster. Organic Letters. 13(13): p. 3376-3379; PMID[21648396].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21604740">Synthesis of Purine Nucleosides Built on a 3-Oxabicyclo[3.2.0]heptane Scaffold.</a> Flores, R., A. Rustullet, R. Alibes, A. Alvarez-Larena, P.D. March, M. Figueredo, and J. Font. Journal of Organic Chemistry. 76(13): p. 5369-5383; PMID[21604740].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21719464">Structural and Functional Analyses of the Second-generation Integrase Strand Transfer Inhibitor Dolutegravir (S/GSK1349572).</a> Hare, S., S.J. Smith, M. Metifiot, A. Jaxa-Chamiec, Y. Pommier, S.H. Hughes, and P. Cherepanov. Molecular Pharmacology. <b>[Epub ahead of print]</b>; PMID[21719464].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21709091">Novel Trichomonacidal Spermicides.</a> Jain, A., N. Lal, L. Kumar, V. Verma, R. Kumar, V. Singh, R.K. Mishra, A. Sarswat, S.K. Jain, J.P. Maikhuri, V.L. Sharma, and G. Gupta. Antimicrobial Agents and Chemotherapy. <b>[Epub ahead of print]</b>; PMID[21709091].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21730371">Epigallocatechin gallate Inhibits the HIV Reverse Transcription Step.</a> Li, S., T. Hattori, and E.N. Kodama. Antiviral Chemistry &amp; Chemotherapy. 21(6): p. 239-243; PMID[21730371].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21715497">Potent and Broad anti-HIV-1 Activity Exhibited by a GPI-anchored Peptide Derived from the CDR H3 of Broadly Neutralizing Antibody PG16.</a> Liu, L., M. Wen, W. Wang, S. Wang, L. Yang, Y. Liu, M. Qian, L. Zhang, Y. Shao, J.T. Kimata, and P. Zhou. Journal of Virology. <b>[Epub ahead of print]</b>; PMID[21715497].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21716073">Antiviral Activity, Safety, and Pharmacokinetics/Pharmacodynamics of Dolutegravir as 10-day Monotherapy in HIV-1-Infected Adults.</a> Min, S., L. Sloan, E. Dejesus, T. Hawkins, L. McCurdy, I. Song, R. Stroder, S. Chen, M. Underwood, T. Fujiwara, S. Piscitelli, and J. Lalezari. AIDS. <b>[Epub ahead of print]</b>; PMID[21716073].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21658957">Synthesis of Enantiomerically Pure D- and L-Bicyclo[3.1.0]hexenyl carbanucleosides and their Antiviral Evaluation.</a> Park, A.Y., W.H. Kim, J.A. Kang, H.J. Lee, C.K. Lee, and H.R. Moon. Bioorganic &amp; Medicinal Chemistry. 19(13): p. 3945-3955; PMID[21658957].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21717016">Microwave-assisted Synthesis of Dinucleoside Analogues Containing a Thiazolidin-4-one Linkage via One-pot Tandem Staudinger/aza-Wittig/cyclization.</a> Shen, F., X. Li, X. Zhang, Q. Yin, Z. Qin, H. Chen, J. Zhang, and Z. Ma. Organic &amp; Biomolecular Chemistry. <b>[Epub ahead of print]</b>; PMID[21717016].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21706033">Discovery of Selective Bioactive Small Molecules by Targeting an RNA Dynamic Ensemble.</a> Stelzer, A.C., A.T. Frank, J.D. Kratz, M.D. Swanson, M.J. Gonzalez-Hernandez, J. Lee, I. Andricioaei, D.M. Markovitz, and H.M. Al-Hashimi. Nature Chemical Biology. <b>[Epub ahead of print]</b>; PMID[21706033].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21567967">Structural Investigation of the Naphthyridone Scaffold: Identification of a 1,6-Naphthyridone Derivative with Potent and Selective Anti-HIV Activity.</a> Tabarrini, O., S. Massari, L. Sancineto, D. Daelemans, S. Sabatini, G. Manfroni, V. Cecchetti, and C. Pannecouque. ChemMedChem. 6(7): p. 1249-1257; PMID[21567967].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291647700021">Synthesis, Characterization, and Biological Activity of Some Aza-Uracil Derivatives.</a> El-Barbary, A.A., Y.A. Hafiz, and M.S. Abdel-Wahed. Journal of Heterocyclic Chemistry. 48(3): p. 639-644; ISI[000291647700021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291716000016">Antimicrobial properties and phenolic contents of medicinal plants used by the Venda people for conditions related to venereal diseases.</a> Mulaudzi, R.B., A.R. Ndhlala, M.G. Kulkarni, J.F. Finnie, and J. Van Staden. Journal of Ethnopharmacology. 135(2): p. 330-337; ISI[000291716000016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291716000038">Medical pluralism on Mfangano Island: Use of medicinal plants among persons living with HIV/AIDS in Suba District, Kenya.</a> Nagata, J.M., A.R. Jew, J.M. Kimeu, C.R. Salmen, E.A. Bukusi, and C.R. Cohen. Journal of Ethnopharmacology. 135(2): p. 501-509; ISI[000291716000038].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291775600016">Analysis of HIV-1 fusion peptide inhibition by synthetic peptides from E1 protein of GB virus C.</a> Sanchez-Martin, M.J., K. Hristova, M. Pujol, M.J. Gomara, I. Haro, M.A. Alsina, and M.A. Busquets. Journal of Colloid and Interface Science. 360(1): p. 124-131; ISI[000291775600016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291780700008">Synthesis and biological evaluation of andrographolide derivatives as potent anti-HIV agents.</a> Wang, B., J. Li, W.L. Huang, H.B. Zhang, H. Qian, and Y.T. Zheng. Chinese Chemical Letters. 22(7): p. 781-784; ISI[000291780700008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291454900003">Isolation of a Laccase with HIV-1 Reverse Transcriptase Inhibitory Activity from Fresh Fruiting Bodies of the Lentinus edodes (Shiitake Mushroom).</a> Sun, J., H.X. Wang, and T.B. Ng. Indian Journal of Biochemistry &amp; Biophysics. 48(2): p. 88-94; ISI[000291454900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291477600022">Preliminary Evaluation of a 3H Imidazoquinoline Library as Dual TLR7/TLR8 Antagonists.</a> Shukla, N.M., S.S. Malladi, V. Day, and S.A. David, Bioorganic &amp; Medicinal Chemistry. 19(12): p. 3801-3811; ISI[000291477600022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291439800010">HIV-1 Reverse Transcriptase Inhibitors: Beyond Classic Nucleosides and Non-nucleosides.</a> Scarth, B.J., M. Ehteshami, G.L. Beilhartz, and M. Gotte. Future Virology. 6(5): p. 581-598; ISI[000291439800010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291264600002">Synthesis and Anti-HIV Evaluation of 3&#39;-Triazolo Nucleosides.</a> Roy, V., A. Obikhod, H.W. Zhang, S.J. Coats, B.D. Herman, N. Sluis-Cremer, L.A. Agrofoglio, and R.F. Schinazi. Nucleosides Nucleotides &amp; Nucleic Acids. 30(4): p. 264-270; ISI[000291264600002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291237600229">Antigonococcal and Anti-HIV-type 1 Reverse Transcriptase (RT) of Medicinal Plants Used by the Venda People.</a> Mulaudzi, R.B., A.R. Ndhlala, M.G. Kulkarni, J.F. Finnie, and J. Van Staden. South African Journal of Botany. 77(2): p. 575-575; ISI[000291237600229].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291568400013">Potent CXCR4 Antagonists Containing Amidine Type Peptide Bond Isosteres.</a> Inokuchi, E., S. Oishi, T. Kubo, H. Ohno, K. Shimura, M. Matsuoka, and N. Fujii. Acs Medicinal Chemistry Letters. 2(6): p. 477-480; ISI[000291568400013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291414700008">Binding Modes of Diketo-acid Inhibitors of HIV-1 Integrase: A Comparative Molecular Dynamics Simulation Study.</a> Huang, M., G.H. Grant, and W.G. Richards. Journal of Molecular Graphics &amp; Modelling. 29(7): p. 956-964; ISI[000291414700008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291568400010">Disubstituted Bis-THF Moieties as New P2 Ligands in Non peptidal HIV-1 Protease Inhibitors.</a> Hohlfeld, K., C. Tomassi, J.K. Wegner, B. Kesteleyn, and B. Linclau. ACS Medicinal Chemistry Letters. 2(6): p. 461-465; ISI[000291568400010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291547800010">Discovery of Novel Compounds from Betula papyrifera that Inhibit HIV-1 Maturation.</a> Dorr, C., O. Kolomitsyna, S. Yemets, N. Somia, P. Krasutsky, and L.M. Mansky. Centennial Retrovirus Meeting [Prague]. 2010. p. 53. ISI[000291547800010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291474300018">Enantioselective Binding of Second Generation Pyrrolobenzoxazepinones to the Catalytic Ternary Complex of HIV-1 RT Wild-type and L100I and K103N Drug Resistant Mutants.</a> Butini, S., S. Gemmaa, M. Brindisi, G. Borrelli, I. Fiorini, A. Samuele, A. Karytinos, M. Facchini, A. Lossani, S. Zanoli, G. Campiani, E. Novellino, F. Focher, and G. Maga. Bioorganic &amp; Medicinal Chemistry Letters. 21(13): p. 3935-3938; ISI[000291474300018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0624-070711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
