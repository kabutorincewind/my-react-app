

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-335.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0sV9o9ypK+jvhl8LtNPzTDot8mM5RZ6Sv6vCyyfbt1g6LMrhnNHAstszGzypXjlSmcCwOlinpQf74OzU4g+4X2Un4PWENHvxPvnej0owNVLMpzepYD/rGh9zHFI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F0A3BB12" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-335-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48152   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Molecular diagnostics in tuberculosis</p>

    <p>          Cheng, VC, Yew, WW, and Yuen, KY</p>

    <p>          Eur J Clin Microbiol Infect Dis <b>2005</b>.: 1-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16283213&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16283213&amp;dopt=abstract</a> </p><br />

    <p>2.     48153   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Rational development of beta -peptide inhibitors of human cytomegalovirus entry</p>

    <p>          English, EP, Chumanov, RS, Gellman, SH, and Compton, T</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275647&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275647&amp;dopt=abstract</a> </p><br />

    <p>3.     48154   OI-LS-335; WOS-OI-11/13/2005</p>

    <p class="memofmt1-2">          Changes in energy metabolism of Mycobacterium tuberculosis in mouse lung and under in vitro conditions affecting aerobic respiration</p>

    <p>          Shi, LB, Sohaskey, CD, Kana, BD, Dawes, S, North, RJ, Mizrahi, V, and Gennaro, ML</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2005</b>.  102(43): 15629-15634, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232929400065">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232929400065</a> </p><br />

    <p>4.     48155   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Antioxidant and antifungal activities of extracts and condensed tannins from Stryphnodendron obovatum benth</p>

    <p>          Sanches, Andreia Cristina Conegero, Lopes, Gisely Cristiny, Nakamura, Celso Vataru, Dias Filho, Benedito Prado, and Palazzo de Mello, Joao Carlos</p>

    <p>          Revista Brasileira de Ciencias Farmaceuticas <b>2005</b>.  41(1): 101-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48156   OI-LS-335; WOS-OI-11/13/2005</p>

    <p class="memofmt1-2">          Sesquiterpenes from Warburgia ugandensis and their antimycobacterial activity</p>

    <p>          Wube, AA, Bucar, F, Gibbons, S, and Asres, K</p>

    <p>          PHYTOCHEMISTRY  <b>2005</b>.  66(19): 2309-2315, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232842000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232842000003</a> </p><br />

    <p>6.     48157   OI-LS-335; WOS-OI-11/13/2005</p>

    <p class="memofmt1-2">          Host sphingolipid biosynthesis as a target for hepatitis C virus therapy</p>

    <p>          Sakamoto, H, Okamoto, K, Aoki, M, Kato, H, Katsume, A, Ohta, A, Tsukuda, T, Shimma, N, Aoki, Y, Arisawa, M, Kohara, M, and Sudoh, M</p>

    <p>          NATURE CHEMICAL BIOLOGY <b>2005</b>.  1(6): 333-337, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232929900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232929900011</a> </p><br />
    <br clear="all">

    <p>7.     48158   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Flux balance analysis of mycolic Acid pathway: targets for anti-tubercular drugs</p>

    <p>          Raman, K, Rajagopalan, P, and Chandra, N</p>

    <p>          PLoS Comput Biol <b>2005</b>.  1(5): e46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16261191&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16261191&amp;dopt=abstract</a> </p><br />

    <p>8.     48159   OI-LS-335; WOS-OI-11/13/2005</p>

    <p class="memofmt1-2">          All four Mycobacterium tuberculosis glnA genes encode glutamine synthetase activities but only GlnA1 is abundantly expressed and essential for bacterial homeostasis</p>

    <p>          Harth, G, Maslesa-Galic, S, Tullius, MV, and Horwitz, MA</p>

    <p>          MOLECULAR MICROBIOLOGY <b>2005</b>.  58(4): 1157-1172, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232893800018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232893800018</a> </p><br />

    <p>9.     48160   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Differential antibiotic susceptibilities of starved Mycobacterium tuberculosis isolates</p>

    <p>          Xie, Z, Siddiqi, N, and Rubin, EJ</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4778-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251329&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251329&amp;dopt=abstract</a> </p><br />

    <p>10.   48161   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of oxygenated cholesterol derivatives</p>

    <p>          Brunel, Jean Michel, Loncle, Celine, Vidal, Nicolas, Dherbomez, Michel, and Letourneux, Yves</p>

    <p>          Steroids <b>2005</b>.  70(13): 907-912</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   48162   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          In vitro investigation of antifungal activities of phenotypic variation Candida albicans strains against fluconazole, itraconazole and voriconazole</p>

    <p>          Cetinkaya, Zafer and Kiraz, Nuri</p>

    <p>          Nippon Ishinkin Gakkai Zasshi <b>2005</b>.  46(3): 197-201</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   48163   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of 3-methyl-4-amino-5-mercapto-1,2,4-triazole derivatives</p>

    <p>          Yu, Shi-chong, Cao, Yong-bing, Sun, Qing-yan, Wu, Qiu-ye, Xu, Jian-ming, Zhang, Jun, and Jiang, Yuan-ying</p>

    <p>          Huaxue Shiji <b>2005</b>.  27(9): 522-524,547</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   48164   OI-LS-335; WOS-OI-11/13/2005</p>

    <p class="memofmt1-2">          Synthesis and structure-activity relationship (SAR) of novel perfluoroalkyl-containing quaternary ammonium salts</p>

    <p>          Sun, HY, Li, J, Qiu, XL, and Qing, FL</p>

    <p>          JOURNAL OF FLUORINE CHEMISTRY <b>2005</b>.  126(9-10): 1425-1431, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232818200025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232818200025</a> </p><br />

    <p>14.   48165   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal evaluation of allyl-containing derivatives of 5-(5-nitrofuran-2-yl)-1,3,4-thiadiazole</p>

    <p>          Emami, Saeed and Foroumadi, Alireza</p>

    <p>          Chemistry (Rajkot, India) <b>2005</b>.  2(1): 10-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   48166   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Synthesis and Anti-HCMV Activity of Novel 5&#39;-Norcarboacyclic Nucleosides</p>

    <p>          Kim, A and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16281312&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16281312&amp;dopt=abstract</a> </p><br />

    <p>16.   48167   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Design and synthesis of 3,4-dihydro-1H-[1]-benzothieno[2,3-c]pyran and 3,4-dihydro-1H-pyrano[3,4-b]benzofuran derivatives as non-nucleoside inhibitors of HCV NS5B RNA dependent RNA polymerase</p>

    <p>          Gopalsamy, A, Aplasca, A, Ciszewski, G, Park, K, Ellingboe, JW, Orlowski, M, Feld, B, and Howe, AY</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16274990&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16274990&amp;dopt=abstract</a> </p><br />

    <p>17.   48168   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Synthesis of substituted 1,3,4-oxadiazole, 1,3,4-thiadiazole and 1,2,4-triazole derivatives as potential antimicrobial agents</p>

    <p>          Vosooghi, M, Akbarzadeh, T, Fallah, A, Fazeli, MR, Jamalifar, H, and Shafiee, A</p>

    <p>          Journal of Sciences, Islamic Republic of Iran <b>2005</b>.  16(2): 145-151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   48169   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Synthesis of homo-N-nucleoside with 1,2,4-triazole-3-carboxamide</p>

    <p>          Chun, MW, Kim, JH, Kim, MJ, Kim, BR, and Jeong, LS</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 979-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248076&amp;dopt=abstract</a> </p><br />

    <p>19.   48170   OI-LS-335; PUBMED-OI-11/14/2005</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-hcv activity of beta-D- and 1-2&#39;-deoxy-2&#39;-fluororibonucleosides</p>

    <p>          Shi, J, Du, J, Ma, T, Pankiewicz, KW, Patterson, SE, Hassan, AE, Tharnish, PM, McBrayer, TR, Lostia, S, Stuyver, LJ, Watanabe, KA, Chu, CK, Schinazi, RF, and Otto, MJ</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 875-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248053&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>20.   48171   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          The renaissance of natural products as drug candidates</p>

    <p>          Paterson, I and Anderson, EA</p>

    <p>          SCIENCE <b>2005</b>.  310(5747): 451-453, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232786000032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232786000032</a> </p><br />

    <p>21.   48172   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Synthesis of some new imidazolones and 1,2,4-triazoles bearing benzo[b]thiophene nucleus as antimicrobial agents</p>

    <p>          Thaker, K, Zalavadiya, P, and Joshi, HS</p>

    <p>          Journal of Sciences, Islamic Republic of Iran <b>2005</b>.  16(2): 139-144</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   48173   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Expression, purification and properties of shikimate dehydrogenase from Mycobacterium tuberculosis</p>

    <p>          Zhang, Xuelian, Zhang, Shunbao, Hao, Fang, Lai, Xuhui, Yu, Haidong, Huang, Yishu, and Wang, Honghai</p>

    <p>          Journal of Biochemistry and Molecular Biology <b>2005</b>.  38(5): 624-631</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   48174   OI-LS-335; WOS-OI-11/13/2005</p>

    <p class="memofmt1-2">          Detection and evaluation of the mutations of embB gene in ethambutol-susceptible and resistant Mycobacterium tuberculosis isolates from China</p>

    <p>          Wu, XQ, Liang, JQ, Zhang, JX, Lu, Y, Li, HM, Zhang, GY, Yan, GR, and Ding, BC</p>

    <p>          CHINESE MEDICAL JOURNAL <b>2005</b>.  118(20): 1739-1741, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232871400011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232871400011</a> </p><br />

    <p>24.   48175   OI-LS-335; SCIFINDER-OI-11/7/2005</p>

    <p class="memofmt1-2">          Synthesis of certain 1,3,4-oxadiazoles as potential antitubercular and antimicrobial agents</p>

    <p>          Dhol, SR, Bhimani, AS, Khunt, RC, and Parikh, AR</p>

    <p>          Indian Journal of Heterocyclic Chemistry <b>2005</b>.  15(1): 63-64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   48176   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          Synthesis of some new 1,3- or 1,4-bis(glucopyranosyl-1,2,4-triazol-5-ylthio)propanes or butanes as potential antimicrobial agents</p>

    <p>          Abbas, AA and Khalil, NSAM</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(9): 1353-1372, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232692000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232692000009</a> </p><br />

    <p>26.   48177   OI-LS-335; WOS-OI-11/13/2005</p>

    <p class="memofmt1-2">          In vitro activity of ciprofloxacin, ofloxacin and levofloxacin against Mycobacterium tuberculosis</p>

    <p>          Akcali, S, Surucuoglu, S, Cicek, C, and Ozbakkaloglu, B</p>

    <p>          ANNALS OF SAUDI MEDICINE <b>2005</b>.  25(5): 409-412, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232812900009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232812900009</a> </p><br />
    <br clear="all">

    <p>27.   48178   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          Quantitative analysis of the hepatitis C virus replication complex</p>

    <p>          Quinkert, D, Bartenschlager, R, and Lohmann, V</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(21): 13594-13605, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232666300038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232666300038</a> </p><br />

    <p>28.   48179   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          The human macrophage mannose receptor directs Mycobacterium tuberculosis lipoarabinomannan-mediated phagosome biogenesis</p>

    <p>          Kang, PB, Azad, AK, Torrelles, JB, Kaufman, TM, Beharka, A, Tibesar, E, DesJardin, LE, and Schlesinger, LS</p>

    <p>          JOURNAL OF EXPERIMENTAL MEDICINE <b>2005</b>.  202(7): 987-999, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232619000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232619000013</a> </p><br />

    <p>29.   48180   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus nonstructural protein 5A (NS5A) is an RNA-binding protein</p>

    <p>          Huang, LY, Hwang, J, Sharma, SD, Hargittai, MRS, Chen, YF, Arnold, JJ, Raney, KD, and Cameron, CE</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(43): 36417-36428, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232726900076">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232726900076</a> </p><br />

    <p>30.   48181   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          1-Aryl-5-benzylsulfanyltetrazoles, a new group of antimycobacterial compounds against potentially pathogenic strains</p>

    <p>          Waisser, K, Adamec, J, Dolezal, R, and Kaustova, J</p>

    <p>          FOLIA MICROBIOLOGICA <b>2005</b>.  50(3): 195-197, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232764300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232764300003</a> </p><br />

    <p>31.   48182   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          Strategy of utilizing in vitro and in vivo ADME tools for lead optimization and drug candidate selection</p>

    <p>          Balani, SK, Miwa, GT, Gan, LS, Wu, JT, and Lee, FW</p>

    <p>          CURRENT TOPICS IN MEDICINAL CHEMISTRY <b>2005</b>.  5(11): 1033-1038, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232610900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232610900002</a> </p><br />

    <p>32.   48183   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          Antimycobacterial and antiplasmodial unsaturated carboxylic acid from the twigs of Scleropyrum wallichianum</p>

    <p>          Suksamrarn, A, Buaprom, M, Udtip, S, Nuntawong, N, Haritakun, R, and Kanokmedhakul, S</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2005</b>.  53(10): 1327-1329, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232679900022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232679900022</a> </p><br />

    <p>33.   48184   OI-LS-335; WOS-OI-11/6/2005</p>

    <p class="memofmt1-2">          Detection of mutations in RNA polymerase beta subunit gene encoding resistance to rifampin in mycobacterium tuberculosis by DNA microarray</p>

    <p>          Yang, MS, Tsoi, PY, Li, CW, Woo, HS, Zhao, JL, and Yam, WC</p>

    <p>          ANALYTICAL LETTERS <b>2005</b>.  38(13): 2117-2134, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232699400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232699400009</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
