

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-04-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/x+guIn6c/UZJd60zwaU+dxQBxAnDxCSBqMuIxQegDZ2yciblMP+M/aji1eGENytXUbNXyj/07INpWTxOhp2+YiUMD4sRREtEilb428rPnjG0f6yye6iLDZFE8U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1F3DFD94" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">April 1 - April 14, 2011</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21480511">Composition, and Antimicrobial and Remarkable Antiprotozoal Activities of the Essential Oil of Rhizomes of Aframomum sceptrum K. Schum. (Zingiberaceae).</a> Cheikh-Ali, Z., M. Adiko, S. Bouttier, C. Bories, T. Okpekon, E. Poupon, and P. Champy. Chemistry and Biodiversity, 2011. 8(4): p. 658-667; PMID[21480511].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21476019">Antifungal Activity of Alpha-methyl Trans Cinnamaldehyde, its Ligand and Metal Complexes: Promising Growth and Ergosterol Inhibitors.</a> Shreaz, S., R.A. Sheikh, R. Bhatia, K. Neelofar, S. Imran, A.A. Hashmi, N. Manzoor, S.F. Basir, and L.A. Khan. Biometals, 2011. <b>[Epub ahead of print]</b>; PMID[21476019].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21474610">In vitro Antifungal Activities of Amphotericin B in Combination with Acteoside, a Phenylethanoid Glycoside from Colebrookea oppositifolia.</a> Ali, I., P. Sharma, K.A. Suri, N.K. Satti, P. Dutt, F. Afrin, and I.A. Khan. Journal of Medical Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21474610].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21459913">In vitro Antifungal Susceptibilities of Candida Isolated from Patients with Invasive Candidiasis in Kuala Lumpur Hospital, Malaysia.</a> Amran, F., M.N. Aziz, M.I. H, A. Norhalim, P. Sabaratnam, M.R. N, and M.I. Ismail. Journal of Medical Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21459913].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21452374">In Vitro Antimicrobial and Antioxidant Activities of Anthrone and Chromone from the Latex of Aloe harlana Reynolds.</a> Asamenew, G., D. Bisrat, A. Mazumder, and K. Asres Phytotherapy Research, 2011. <b>[Epub ahead of print]</b>; PMID[21452374].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21441482">Effect of Traditional Chinese Medicinal Herbs on Candida spp. from Patients with HIV/AIDS.</a> Liu, X., Y. Han, K. Peng, Y. Liu, J. Li, and H. Liu. Advances in Dental Research, 2011. 23(1): p. 56-60; PMID[21441482].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21324567">Synthesis of Some Novel 3-(1-(1-substitutedpiperidin-4-yl)-1H-1,2,3-triazol-4-yl)-5-substituted phenyl-1,2,4-oxadiazoles as Antifungal Agents.</a> Sangshetti, J.N. and D.B. Shinde. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1040-1044; PMID[21324567].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21342734">Synthesis and Biological Evaluation of Some 4-functionalized-pyrazoles as Antimicrobial Agents.</a> Sharma, P.K., N. Chandak, P. Kumar, C. Sharma, and K.R. Aneja. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1425-1432; PMID[21342734].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21316819">Synthesis, Stereochemistry, Antimicrobial Evaluation and QSAR Studies of 2,6-Diaryltetrahydropyran-4-one thiosemicarbazones.</a> Umamatheswari, S., B. Balaji, M. Ramanathan, and S. Kabilan. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1415-1424; PMID[21316819].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21237261">Pharmacological Properties of Anagallis arvensis L. (&quot;scarlet pimpernel&quot;) and Anagallis foemina Mill. (&quot;blue pimpernel&quot;) Traditionally Used as Wound Healing Remedies in Navarra (Spain).</a> Lopez, V., A.K. Jager, S. Akerreta, R.Y. Cavero, and M.I. Calvo. Journal of Ethnopharmacology, 2011. 134(3): p. 1014-1017; PMID[21237261].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20936361">In vitro Antifungal Activity of Dihydroxyacetone Against Causative Agents of Dermatomycosis.</a> Stopiglia, C.D., F.J. Vieira, A.G. Mondadori, T.P. Oppe, and M.L. Scroferneker. Mycopathologia, 2011. 171(4): p. 267-271; PMID[20936361].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21411168">The Antimicrobial Activity of Microencapsulated Thymol and Carvacrol.</a> Guarda, A., J.F. Rubilar, J. Miltz, and M.J. Galotto. International Journal of Food Microbiology, 2011. 146(2): p. 144-150; PMID[21411168].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20687790">Activated Fly Ash Catalyzed Facile Synthesis of Novel Spiro Imidazolidine Derivatives as Potential Antibacterial and Antifungal Agents.</a> Kanagarajan, V., J. Thanusu, and M. Gopalakrishnan. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. 26(2): p. 280-287; PMID[20687790].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21393183">Novel High-throughput Screen Against Candida albicans Identifies Antifungal Potentiators and Agents Effective Against Biofilms.</a> LaFleur, M.D., E. Lucumi, A.D. Napper, S.L. Diamond, and K. Lewis. Journal of Antimicrobial Chemotherapy, 2011. 66(4): p. 820-826; PMID[21393183].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21419634">Synthesis and Biological Evaluation of New 2-Alkylaminoethyl-1,1-bisphosphonic acids Against Trypanosoma cruzi and Toxoplasma gondii Targeting Farnesyl Diphosphate Synthase.</a> Rosso, V.S., S.H. Szajnman, L. Malayil, M. Galizzi, S.N.J. Moreno, R. Docampo, and J.B. Rodriguez. Bioorganic &amp; Medicinal Chemistry, 2011. 19(7): p. 2211-2217; PMID[21419634].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21417236">Ethionamide Boosters: Synthesis, Biological Activity, and Structure-Activity Relationships of a Series of 1,2,4-Oxadiazole EthR Inhibitors.</a> Flipo, M., M. Desroses, N. Lecat-Guillet, B. Dirie, X. Carette, F. Leroux, C. Piveteau, F. Demirkaya, Z. Lens, P. Rucktooa, V. Villeret, T. Christophe, H.K. Jeon, C. Locht, P. Brodin, B. Deprez, A.R. Baulard, and N. Willand. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21417236].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21290071">Substrate Specificity of Rv3378c, an Enzyme from Mycobacterium tuberculosis, and the Inhibitory Activity of the Bicyclic Diterpenoids Against Macrophage Phagocytosis.</a> Hoshino, T., C. Nakano, T. Ootsuka, Y. Shinohara, and T. Hara. Organic and Biomolecular Chemistry. 2011. 9(7): p. 2156-2165; PMID[21290071].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">18.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21464254">Efficacy of Antimicrobial Peptoids Against Mycobacterium.</a> Kapoor, R., P.R. Eimerman, J.W. Hardy, J.D. Cirillo, C.H. Contag, and A.E. Barron. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21464254].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21459133">Ethyl p-Methoxycinnamate Isolated from a Traditional Anti-tuberculosis Medicinal Herb Inhibits Drug Resistant Strains of Mycobacterium tuberculosis In vitro.</a> Lakshmanan, D., J. Werngren, L. Jose, K.P. Suja, M.S. Nair, R.L. Varma, S. Mundayoor, S. Hoffner, and R.A. Kumar. Fitoterapia, 2011. <b>[Epub ahead of print]</b>; PMID[21459133].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21482481">Effective Inhibitors of the Essential Kinase PknB and Their Potential as Anti-mycobacterial Agents.</a> Lougheed, K.E., S.A. Osborne, B. Saxty, D. Whalley, T. Chapman, N. Bouloc, J. Chugh, T.J. Nott, D. Patel, V.L. Spivey, C.A. Kettleborough, J.S. Bryans, D.L. Taylor, S.J. Smerdon, and R.S. Buxton. Tuberculosis (Edinburgh, Scotland), 2011. <b>[Epub ahead of print]</b>; PMID[21482481].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21465667">Inhibition of Mycobacterium tuberculosis Methionine Aminopeptidases by Bengamide Derivatives.</a> Lu, J.P., X.H. Yuan, H. Yuan, W.L. Wang, B. Wan, S.G. Franzblau, and Q.Z. Ye. ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21465667].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">22.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21353489">Comparative In vitro and In vivo Antimicrobial Activities of Sitafloxacin, Gatifloxacin and Moxifloxacin Against Mycobacterium avium.</a> Sano, C., Y. Tatano, T. Shimizu, S. Yamabe, K. Sato, and H. Tomioka. International Journal of Antimicrobial Agents, 2011. <b>[Epub ahead of print]</b>; PMID[21353489].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0216-030211.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21469696.">Antituberculosis Cycloartane Triterpenoids from Radermachera boniana.</a> Truong, N.B., C.V. Pham, H.T. Doan, H.V. Nguyen, C.M. Nguyen, H.T. Nguyen, H.J. Zhang, H.H. Fong, S.G. Franzblau, D.D. Soejarto, and M.V. Chau. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21469696].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21334792">Synthesis and Antimycobacterial Activity of Prodrugs of Indeno[2,1-c]quinoline Derivatives.</a> Upadhayaya, R.S., P.D. Shinde, S.A. Kadam, A.N. Bawane, A.Y. Sayyed, R.A. Kardile, P.N. Gitay, S.V. Lahore, S.S. Dixit, A. Foldesi, and J. Chattopadhyaya. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1306-1324; PMID[21334792].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="memofmt2-3"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p class="memofmt2-3"> </p>

    <p class="NoSpacing">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21256952">Potent Antiplasmodial Extracts from Cameroonian Annonaceae.</a> Boyom, F.F., P.V. Fokou, L.R. Yamthe, A.N. Mfopa, E.M. Kemgne, W.F. Mbacham, E. Tsamo, P.H. Zollo, J. Gut, and P.J. Rosenthal. Journal of Ethnopharmacology, 2011. 134(3): p. 717-724; PMID[21256952].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21476508">Synthesis, Structure-Activity Relationship and Antimalarial Activity of Ureas and Thioureas of 15-membered Azalides.</a> Bukvic Krajacic, M., M. Peric, K.S. Smith, Z. Ivezic Schonfeld, D. Ziher, A. Fajdetic, N. Kujundzic, W. Schonfeld, G. Landek, J. Padovan, D. Jelic, A. Ager, W.K. Milhous, W. Ellis, R. Spaventi, and C. Ohrt. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21476508].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21476932">Chemical Study and Antimalarial, Antioxidant, and Anticancer Activities of Melaleuca armillaris (Sol Ex Gateau) Sm Essential Oil.</a> Chabir, N., M. Romdhane, A. Valentin, B. Moukarzel, H.N. Marzoug, N.B. Brahim, M. Mars, and J. Bouajila. Journal of Medicinal Food, 2011. <b>[Epub ahead of print]</b>; PMID[21476932].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21421317">Exploration of Larger Central Ring Linkers in Furamidine Analogues: Synthesis and Evaluation of Their DNA Binding, Antiparasitic and Fluorescence Properties.</a> Farahat, A.A., E. Paliakov, A. Kumar, A.E. Barghash, F.E. Goda, H.M. Eisa, T. Wenzler, R. Brun, Y. Liu, W.D. Wilson, and D.W. Boykin. Bioorganic &amp;  Medicinal Chemistry, 2011. 19(7): p. 2156-2167; PMID[21421317].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21457474">Synthesis of 4-Aminoquinoline-1,2,3-triazole and 4-Aminoquinoline-1,2,3-triazole-1,3,5-triazine Hybrids as Potential Antimalarial Agents.</a> Manohar, S., S.I. Khan, and D.S. Rawat. Chemical Biology &amp; Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21457474].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21477369">Parasitostatic Effect of Maslinic Acid. I. Growth Arrest of Plasmodium Falciparum Intraerythrocytic Stages.</a> Moneriz, C., P. Marin-Garcia, A. Garcia-Granados, J.M. Bautista, A. Diez, and A. Puyet. Malaria Journal, 2011. 10(1): p. 82; PMID[21477369].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21456549">Marinoquinolines A-F, Pyrroloquinolines from Ohtaekwangia kribbensis (Bacteroidetes).</a> Okanya, P.W., K.I. Mohr, K. Gerth, R. Jansen, and R. Muller. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21456549].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21464242">Investigations Into the In vivo and In vitro Antimalarial Properties of Azithromycin-Chloroquine Combinations Including the Reversal Agent Amlodipine.</a> Pereira, M.R., P.P. Henrich, A.B. Sidhu, D. Johnson, J. Hardink, J. Van Deusen, J. Lin, K. Gore, C. O&#39;Brien, M. Wele, A. Djimde, R. Chandra, and D.A. Fidock. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21464242].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20978786">In vitro Antiplasmodial Activity of Ethanolic Extracts of Mangrove Plants from South East Coast of India Against Chloroquine-sensitive Plasmodium falciparum.</a> Ravikumar, S., S. Jacob Inbaneson, P. Suganthi, and M. Gnanadesigan. Parasitology Research, 2011. 108(4): p. 873-878; PMID[20978786].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21481272">Icacina senegalensis (Icacinaceae), Traditionally Used for the Treatment of Malaria, Inhibits In vitro Plasmodium falciparum Growth Without Host Cell Toxicity.</a> Sarr, S.O., S. Perrotey, I. Fall, S. Ennahar, M. Zhao, Y.M. Diop, E. Candolfi, and E. Marchioni. Malaria Journal, 2011. 10(1): p. 85; PMID[21481272].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288568000006">Synthesis and Evaluation of Potent Ene-yne Inhibitors of Type II Dehydroquinases as Tuberculosis Drug Leads.</a> Anh, T.T., K.M. Cergol, N.P. West, E.J. Randall, W.J. Britton, S.A.I. Bokhari, M. Ibrahim, A.J. Lapthorn, and R.J. Payne. ChemMedChem, 2011. 6(2): p. 262-265; ISI[000288568000006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288844500007">Spectral and Antimicrobial Studies on Tetraaza Macrocyclic Complexes of Pd-II, Pt-II, Rh-III and Ir-III Metal Ions.</a> Chandra, S., M. Tyagi, and S. Agrawal. Journal of Saudi Chemical Society, 2011. 15(1): p. 49-54; ISI[000288844500007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288547800033">Surface-modified Sulfur Nanoparticles: An Effective Antifungal Agent Against Aspergillus niger and Fusarium oxysporum.</a> Choudhury, S.R., M. Ghosh, A. Mandal, D. Chakravorty, M. Pal, S. Pradhan, and A. Goswami. Applied Microbiology and Biotechnology, 2011. 90(2): p. 733-743; ISI[000288547800033].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288516900013">Physicochemical Properties and Antifungal Activity of Amphotericin B Incorporated in Cholesteryl Carbonate Esters.</a> Chuealee, R., T.S. Wiedmann, and T. Srichana. Journal of Pharmaceutical Sciences, 2011. 100(5): p. 1727-1735; ISI[000288516900013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288400700012">Natural Product-Based Phenols as Novel Probes for Mycobacterial and Fungal Carbonic Anhydrases.</a> Davis, R.A., A. Hofmann, A. Osman, R.A. Hall, F.A. Muhlschlegel, D. Vullo, A. Innocenti, C.T. Supuran, and S.A. Poulsen. Journal of Medicinal Chemistry, 2011. 54(6): p. 1682-1692; ISI[000288400700012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288245100032">Novel Thiolactone-isatin Hybrids as Potential Antimalarial and Antitubercular Agents.</a> Hans, R.H., I.J.F. Wiid, P.D. van Helden, B. Wan, S.G. Franzblau, J. Gut, P.J. Rosenthal, and K. Chibale. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(7): p. 2055-2058; ISI[000288245100032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288555900012">Evaluation of Chemically Characterised Essential Oils of Coleus aromaticus, Hyptis suaveolens and Ageratum conyzoides Against Storage Fungi and Aflatoxin Contamination of Food Commodities.</a> Jaya, B.P. and N.K. Dubey. International Journal of Food Science and Technology, 2011. 46(4): p. 754-760; ISI[000288555900012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288547800015">An Altered Camelid-like Single Domain Anti-idiotypic Antibody Fragment of HM-1 Killer Toxin: Acts as an Effective Antifungal Agent.</a> Kabir, M.E., S. Krishnaswamy, M. Miyamoto, Y. Furuichi, and T. Komiyama. Applied Microbiology and Biotechnology, 2011. 90(2): p. 553-564; ISI[000288547800015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288245100019">Design, Synthesis and Antimycobacterial Activity of Cinnamide Derivatives: A Molecular Hybridization Approach.</a> Kakwani, M.D., P. Suryavanshi, M. Ray, M.G.R. Rajan, S. Majee, A. Samad, P. Devarajan, and M.S. Degani. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(7): p. 1997-1999; ISI[000288245100019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288469200010">Anti-tubercular Agents. Part 6: Synthesis and Antimycobacterial Activity of Novel Arylsulfonamido conjugated oxazolidinones.</a>  Kamal, A., R. Shetti, S. Azeeza, P. Swapna, M.N.A. Khan, I.A. Khan, S. Sharma, and S.T. Abdullah. European Journal of Medicinal Chemistry, 2011. 46(3): p. 893-900; ISI[000288469200010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288340100030">Biocidal Polymers: Synthesis, Antimicrobial Activity, and Possible Toxicity of Poly(hydroxystyrene-co-methylmethacrylate) Derivatives.</a> Kenawy, E., A.E.R. El-Shanshoury, N.O. Shaker, B.M. El-Sadek, A.H.B. Khattab, and B.I. Badr. Journal of Applied Polymer Science, 2011. 120(5): p. 2734-2742; ISI[000288340100030].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">46. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288398900012">DNA Cleavage and Antimicrobial Investigation of Co(II), Ni(II), and Cu(II) Complexes with Triazole Schiff Bases: Synthesis and Spectral Characterization.</a> Kulkarni, A.D., S.A. Patil, V.H. Naik, and P.S. Badami. Medicinal Chemistry Research, 2011. 20(3): p. 346-354; ISI[000288398900012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">47. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288594600039">Voriconazole-Induced Inhibition of the Fungicidal Activity of Amphotericin B in Candida Strains with Reduced Susceptibility to Voriconazole: an Effect Not Predicted by the MIC Value Alone.</a> Lignell, A., E. Lowdin, O. Cars, D. Sanglard, and J. Sjolin. Antimicrobial Agents and Chemotherapy, 2011. 55(4): p. 1629-1637; ISI[000288594600039].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">48. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288224900008">Evaluation of Antifungal Activity of Pittosporum undulatum L. Essential Oil Against Aspergillus flavus and Aflatoxin Production.</a> Medeiros, R.T.D., E. Goncalez, R.C. Felicio, and J.D. Felicio. Ciencia E Agrotecnologia, 2011. 35(1): p. 71-76; ISI[000288224900008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">49. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288451100013">Antimycobacterial 3-Phenyl-4-thioxo-2H-1,3-benzoxazine-2(3H)-ones and 3-Phenyl-2H-1,3-benzoxazine-2,4(3H)-dithiones Substituted on Phenyl and Benzoxazine Moiety in Position 6.</a> Petrlikova, E., K. Waisser, R. Dolezal, P. Holy, J. Gregor, J. Kunes, and J. Kaustova. Chemical Papers, 2011. 65(3): p. 352-366; ISI[000288451100013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">50. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288568000015">Pyrido 1,2-a benzimidazole-Based Agents Active Against Tuberculosis (TB), Multidrug-Resistant (MDR) TB and Extensively Drug-Resistant (XDR) TB.</a> Pieroni, M., S.K. Tipparaju, S.C. Lun, Y. Song, A.W. Sturm, W.R. Bishai, and A.P. Kozikowski. ChemMedChem, 2011. 6(2): p. 334-342; ISI[000288568000015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">51. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288398900002">In vitro Antimycobacterial Activity of Novel N&#39;-(4-(substituted phenyl amino)-6-(pyridin-2-ylamino)-1,3,5-triazin-2-yl)isonicotinohydrazide.</a> Raval, J.P., N.H. Patel, H.V. Patel, and P.S. Patel. Medicinal Chemistry Research, 2011. 20(3): p. 274-279; ISI[000288398900002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">52. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288877600002">Synthesis, Characterization, and Antimicrobial Evaluation of Carbostyril Derivatives of 1H-Pyrazole.</a> Thumar, N.J. and M.P. Patel. Saudi Pharmaceutical Journal, 2011. 19(2): p. 75-83; ISI[000288877600002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">53. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288792400009">Dihydro-beta-agarofuran sesquiterpenes Isolated from Celastrus vulcanicola as Potential Anti-Mycobacterium tuberculosis Multidrug-resistant Agents.</a> Torres-Romero, D., I.A. Jimenez, R. Rojas, R.H. Gilman, M. Lopez, and I.L. Bazzocchi. Bioorganic &amp; Medicinal Chemistry, 2011. 19(7): p. 2182-2189; ISI[000288792400009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0401-041411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
