

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-72.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="L8d7yHeINmu4FCgw/bdZCTefZBIC/4E6Kw4IC4UNvxpTKGSYWpcHF0n6+aL+mXJMh0wZeNgjmP+3s/Sh+7CjnGqx1fYZmcpS6Dt0UfqAM8uqKgwX8cS7TMx7LHo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0351653A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-72-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    6249   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Inactivation of HSV-2 by ascorbate-Cu(II) and its protecting evaluation in CF-1 mice against encephalitis</p>

<p>           Betanzos-Cabrera, G, Ramirez, FJ, Munoz, JL, Barron, BL, and Maldonado, R</p>

<p>           J Virol Methods 2004. 120(2): 161-165</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15288958&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15288958&amp;dopt=abstract</a> </p><br />

<p>     2.    6250   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Design and development of oral drugs for the prophylaxis and treatment of smallpox infection</p>

<p>           Painter, GR and Hostetler, KY</p>

<p>           Trends Biotechnol 2004. 22(8): 423-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15283988&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15283988&amp;dopt=abstract</a> </p><br />

<p>     3.    6251   DMID-LS-72; WOS-DMID-8/4/2004</p>

<p>           <b>Effect of a Combination of Clevudine and Emtricitabine With Adenovirus-Mediated Delivery of Gamma Interferon in the Woodchuck Model of Hepatitis B Virus Infection</b></p>

<p>           Jacquard, AC, Nassal, M, Pichoud, C, Ren, S, Schultz, U, Guerret, S, Chevallier, M, Werle, B, Peyrol, S, Jamard, C, Rimsky, LT, Trepo, C, and Zoulim, F</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(7): 2683-2692</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>     4.    6252   DMID-LS-72; WOS-DMID-8/4/2004</p>

<p>           <b>Inhibitory Role of Cyclosporin a and Its Derivatives on Replication of Hepatitis C Virus.</b></p>

<p>           Shimotohno, K and Watashi, K</p>

<p>           American Journal of Transplantation 2004. 4: 334-335</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>     5.    6253   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Antiviral agents and corticosteroids in the treatment of severe acute respiratory syndrome (SARS)</p>

<p>           Yu, WC, Hui, DS, and Chan-Yeung, M</p>

<p>           Thorax 2004. 59(8): 643-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15282381&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15282381&amp;dopt=abstract</a> </p><br />

<p>     6.    6254   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Cytomegalovirus infection in the era of HAART: fewer reactivations and more immunity</p>

<p>           Springer, KL and Weinberg, A</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15282241&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15282241&amp;dopt=abstract</a> </p><br />

<p>     7.    6255   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral evaluation of some novel tricyclic pyrazolo[3,4-b]indole nucleosides</p>

<p>           Williams, JD, Drach, JC, and Townsend, LB</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(5): 805-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15281368&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15281368&amp;dopt=abstract</a> </p><br />

<p>     8.    6256   DMID-LS-72; WOS-DMID-8/4/2004</p>

<p>           <b>Acute and Chronic Hepatitis: Working Group Report of the Second World Congress of Pediatric Gastroenterology, Hepatology, and Nutrition</b></p>

<p>           Chang, MH, Hadzic, D, Rouassant, SH, Jonas, M, Kohn, IJ, Negro, F, Roberts, E, and Sibal, A</p>

<p>           Journal of Pediatric Gastroenterology and Nutrition 2004. 39: S584-S588</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>     9.    6257   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Synthesis of new 2&#39;-[beta]-C-methyl related triciribine analogues as anti-HCV agents</p>

<p>           Smith, Kenneth L, Lai, Vicky CH, Prigaro, Brett J, Ding, Yili, Gunic, Esmir, Girardet, Jean-Luc, Zhong, Weidong, Hong, Zhi, Lang, Stanley, and An, Haoyun</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(13): 3517-3520</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CC7S3F-5/2/040fd226b1b7d01317556717fb29408d">http://www.sciencedirect.com/science/article/B6TF9-4CC7S3F-5/2/040fd226b1b7d01317556717fb29408d</a> </p><br />

<p>   10.    6258   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Herpes simplex virus 1 has multiple mechanisms for blocking virus-induced interferon production</p>

<p>           Melroe, GT, DeLuca, NA, and Knipe, DM</p>

<p>           J Virol 2004. 78(16): 8411-20</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280450&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280450&amp;dopt=abstract</a> </p><br />

<p>   11.    6259   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Diterpenes from the brown algae Dictyota dichotoma and Dictyota linearis</p>

<p>           Siamopoulou, P, Bimplakis, A, Iliopoulou, D, Vagias, C, Cos, P, Vanden, Berghe D, and Roussis, V</p>

<p>           Phytochemistry 2004. 65(14): 2025-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15279967&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15279967&amp;dopt=abstract</a> </p><br />

<p>   12.    6260   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Rapid detection and quantitation of hepatitis B virus DNA by real-time PCR using a new fluorescent (FRET) detection system</p>

<p>           Aliyu, Sani Hussein, Aliyu, Muktar Hassan, Salihu, Hamisu M, Parmar, Surendra, Jalal, Hamid, and Curran, Martin David</p>

<p>           Journal of Clinical Virology 2004.  30(2): 191-195</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4BBKHBS-1/2/dd3aa54a13ec595fa8494257c4e987ef">http://www.sciencedirect.com/science/article/B6VJV-4BBKHBS-1/2/dd3aa54a13ec595fa8494257c4e987ef</a> </p><br />

<p>   13.    6261   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           A New Compound from Forsythia suspensa (Thunb.) Vahl with Antiviral Effect on RSV</p>

<p>           Zhang, GG, Song, SJ, Ren, J, and Xu, SX</p>

<p>           J Herb Pharmcother 2003. 2(3): 35-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15277088&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15277088&amp;dopt=abstract</a> </p><br />

<p>   14.    6262   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           A Randomized, Controlled Study of Kan Jang versus Amantadine in the Treatment of Influenza in Volgograd</p>

<p>           Kulichenko, LL, Kireyeva, LV, Malyshkina, EN, and Wikman, G</p>

<p>           J Herb Pharmcother 2003. 3(1): 77-92</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15277072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15277072&amp;dopt=abstract</a> </p><br />

<p>   15.    6263   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           A lipid-peptide microbicide inactivates herpes simplex virus</p>

<p>           Isaacs, CE, Jia, JH, and Xu, W</p>

<p>           Antimicrob Agents Chemother 2004.  48(8): 3182-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273146&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273146&amp;dopt=abstract</a> </p><br />

<p>   16.    6264   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Inhibition of hepatitis C virus replication by arsenic trioxide</p>

<p>           Hwang, DR, Tsai, YC, Lee, JC, Huang, KK, Lin, RK, Ho, CH, Chiou, JM, Lin, YT, Hsu, JT, and Yeh, CT</p>

<p>           Antimicrob Agents Chemother 2004.  48(8): 2876-82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273095&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273095&amp;dopt=abstract</a> </p><br />

<p>   17.    6265   DMID-LS-72; WOS-DMID-8/4/2004</p>

<p>           <b>Structural Characterization of the Fusion-Active Complex of Severe Acute Respiratory Syndrome (Sars) Coronavirus</b></p>

<p>           Ingallinella, P, Bianchi, E, Finotto, M, Cantoni, G, Eckert, DM, Supekar, VM, Bruckmann, C, Carfi, A, and Pessi, A</p>

<p>           Proceedings of the National Academy of Sciences of the United States of America 2004. 101(23): 8709-8714</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>   18.    6266   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           A synthetic peptide from a heptad repeat region of herpesvirus glycoprotein B inhibits virus replication</p>

<p>           Okazaki, K and Kida, H</p>

<p>           J Gen Virol 2004. 85(Pt 8): 2131-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15269351&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15269351&amp;dopt=abstract</a> </p><br />

<p>   19.    6267   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Attempt to reduce cytotoxicity by synthesizing the L-enantiomer of 4&#39;-C-ethynyl-2&#39;-deoxypurine nucleosides as antiviral agents against HIV and HBV</p>

<p>           Kitano, K, Kohgo, S, Yamada, K, Sakata, S, Ashida, N, Hayakawa, H, Nameki, D, Kodama, E, Matsuoka, M, Mitsuya, H, and Ohrui, H</p>

<p>           Antivir Chem Chemother 2004. 15(3): 161-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266898&amp;dopt=abstract</a> </p><br />

<p>   20.    6268   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Inhibitory effect of medicinal herbs against RNA and DNA viruses</p>

<p>           Ruffa, MJ, Wagner, ML, Suriano, M, Vicente, C, Nadinic, J, Pampuro, S, Salomon, H, Campos, RH, and Cavallaro, L</p>

<p>           Antivir Chem Chemother 2004. 15(3): 153-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266897&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266897&amp;dopt=abstract</a> </p><br />

<p>  21.     6269   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Action of celgosivir (6 O-butanoyl castanospermine) against the pestivirus BVDV: implications for the treatment of hepatitis C</p>

<p>           Whitby, K, Taylor, D, Patel, D, Ahmed, P, and Tyms, AS</p>

<p>           Antivir Chem Chemother 2004. 15(3): 141-51</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266896&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266896&amp;dopt=abstract</a> </p><br />

<p>   22.    6270   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Helicase primase: targeting the Achilles heel of herpes simplex viruses</p>

<p>           Kleymann, G</p>

<p>           Antivir Chem Chemother 2004. 15(3): 135-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266895&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266895&amp;dopt=abstract</a> </p><br />

<p>   23.    6271   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Inhibitors of measles virus</p>

<p>           Barnard, DL</p>

<p>           Antivir Chem Chemother 2004. 15(3): 111-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266893&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266893&amp;dopt=abstract</a> </p><br />

<p>   24.    6272   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Amantadine and rimantadine for preventing and treating influenza A in adults</p>

<p>           Jefferson, T, Deeks, J, Demicheli, V, Rivetti, D, and Rudin, M</p>

<p>           Cochrane Database Syst Rev 2004. 3: CD001169</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266442&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266442&amp;dopt=abstract</a> </p><br />

<p>   25.    6273   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           63 In-vitro and in-vivo evaluation of HCV polymerase inhibitors as potential drug candidates for treatment of chronic hepatitis C infection</p>

<p>           Dagan, S, Ilan, E, Aviel, S, Nussbaum, O, Arazi, E, Ben-Moshe, O, Slama, D, Tzionas, I, Shoshany, Y, and Lee, SW</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 23</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-28/2/3bae4eb66319cce4e2d3c2c0f8c62136">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-28/2/3bae4eb66319cce4e2d3c2c0f8c62136</a> </p><br />

<p>   26.    6274   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           475 Sustained virological response (SVR) to peginterferon alfa-2A (40KD) (PEGASYS) plus ribavirin (RBV) and amantadine (AMA) vs induction therapy with interferon alfa-2A (Roferon-A) plus RBV and AMA in IFN/RBV non responders (NR) with chronic hepatitis C (CHC)</p>

<p>           Fargion, S, Borzio, M, Maraschi, A, and Cargnel, A</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 140</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-K0/2/749036445acbe2827303ee8f8cd65d27">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-K0/2/749036445acbe2827303ee8f8cd65d27</a> </p><br />

<p>   27.    6275   DMID-LS-72; WOS-DMID-8/4/2004</p>

<p>           <b>Cell Culture and Animal Models of Viral Hepatitis. Part I: Hepatitis B</b></p>

<p>           Guha, C, Mohan, S, Roy-Chowdhury, N, and Roy-Chowdhury, J</p>

<p>           Lab Animal 2004. 33(7): 37-46</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>   28.    6276   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Severe acute respiratory syndrome (SARS): epidemiology and clinical features</p>

<p>           Hui, DS, Chan, MC, Wu, AK, and Ng, PC</p>

<p>           Postgrad Med J 2004. 80(945): 373-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254300&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254300&amp;dopt=abstract</a> </p><br />

<p>   29.    6277   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           From the Academy: Discovery of antivirals against smallpox</p>

<p>           Harrison, SC, Alberts, B, Ehrenfeld, E, Enquist, L, Fineberg, H, McKnight, SL, Moss, B, O&#39;Donnell, M, Ploegh, H, Schmid, SL, Walter, KP, and Theriot, J</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(31): 11178-11192</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15249657&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15249657&amp;dopt=abstract</a> </p><br />

<p>   30.    6278   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Replicative multivirus infection with cytomegalovirus, herpes simplex virus 1, and parvovirus B19, and latent Epstein-Barr virus infection in the synovial tissue of a psoriatic arthritis patient</p>

<p>           Mehraein, Yasmin, Lennerz, Carsten, Ehlhardt, Sandra, Zang, Klaus D, and Madry, Henning</p>

<p>           Journal of Clinical Virology 2004.  31(1): 25-31</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4C82FGG-1/2/c3ad6e683faca7b864dbca283a726ba5">http://www.sciencedirect.com/science/article/B6VJV-4C82FGG-1/2/c3ad6e683faca7b864dbca283a726ba5</a> </p><br />

<p>   31.    6279   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Varicella-zoster virus infection of human neural cells in vivo</p>

<p>           Baiker, A, Fabel, K, Cozzio, A, Zerboni, L, Fabel, K, Sommer, M, Uchida, N, He, D, Weissman, I, and Arvin, AM</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(29): 10792-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247414&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247414&amp;dopt=abstract</a> </p><br />

<p>   32.    6280   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           The synergistic effects of betulin with acyclovir against herpes simplex viruses</p>

<p>           Gong, Yunhao, Raj, Karim M, Luscombe, Carolyn A, Gadawski, Izabelle, Tam, Teresa, Chu, Jianhua, Gibson, David, Carlson, Robert, and Sacks, Stephen L</p>

<p>           Antiviral Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4CXKN94-1/2/e1432356a394157e3eb5509202a451da">http://www.sciencedirect.com/science/article/B6T2H-4CXKN94-1/2/e1432356a394157e3eb5509202a451da</a> </p><br />

<p>   33.    6281   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Immunity to West Nile virus</p>

<p>           Wang, T and Fikrig, E</p>

<p>           Curr Opin Immunol 2004. 16(4): 519-23</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245749&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245749&amp;dopt=abstract</a> </p><br />

<p>  34.     6282   DMID-LS-72; PUBMED-DMID-8/4/2004</p>

<p class="memofmt1-3">           Severe acute respiratory syndrome: an update</p>

<p>           Poutanen, SM and Low, DE</p>

<p>           Curr Opin Infect Dis 2004. 17(4): 287-94</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15241071&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15241071&amp;dopt=abstract</a>  </p><br />

<p>   35.    6283   DMID-LS-72; WOS-DMID-8/4/2004</p>

<p>           <b>Epidemiology of Bk Virus Replication in Renal Allograft Recipients and Identification of Corticosteroid Maintenance Therapy as an Independent Risk Factor.</b></p>

<p>           Dadhania, D, Muthukumar, T, Snopkowski, C, Ding, R, Li, B, Aull, M, Thomas, D, Shakibai, N, Sharma, V, Serur, D, Hartono, C, Kapur, S, and Suthanthiran, M</p>

<p>           American Journal of Transplantation 2004. 4: 198</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>   36.    6284   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Once, twice, or three times daily famciclovir compared with aciclovir for the oral treatment of herpes zoster in immunocompetent adults: a randomized, multicenter, double-blind clinical trial</p>

<p>           Shafran, Stephen D, Tyring, Stephen K, Ashton, Richard, Decroix, Jacque, Forszpaniak, Chistine, Wade, Alan, Paulet, Christian, and Candaele, Daniel</p>

<p>           Journal of Clinical Virology 2004.  29(4): 248-253</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4B2D0G7-1/2/d3a691f78721ddec6050e3020d13e6e3">http://www.sciencedirect.com/science/article/B6VJV-4B2D0G7-1/2/d3a691f78721ddec6050e3020d13e6e3</a> </p><br />

<p>   37.    6285   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Mutational analysis of the PA and PB2 subunits of the influenza RNA polymerase complex leads to new insights into function</p>

<p>           Fodor, Ervin, Fechter, Pierre, Crow, Mandy, Deng, Tao, Mingay, Louise, Sharps, Jane, and Brownlee, George G</p>

<p>           International Congress Series 2004.  1263: 25-28</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B7581-4CRPC4H-9/2/c393082f98c04f0d9828ef9e816b737e">http://www.sciencedirect.com/science/article/B7581-4CRPC4H-9/2/c393082f98c04f0d9828ef9e816b737e</a> </p><br />

<p>   38.    6286   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Neuraminidase inhibitors: assessment of limitations to greater therapeutic use</p>

<p>           Hayden, Frederick G</p>

<p>           International Congress Series 2004.  1263: 29-37</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B7581-4CRPC4H-B/2/089a90e53323220034a25f383cdc5fc7">http://www.sciencedirect.com/science/article/B7581-4CRPC4H-B/2/089a90e53323220034a25f383cdc5fc7</a> </p><br />

<p>   39.    6287   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Characterization of influenza A and B viruses recovered from immunocompromised patients treated with antivirals</p>

<p>           Gubareva, Larisa V</p>

<p>           International Congress Series 2004.  1263: 126-129</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B7581-4CRPC4H-13/2/12dbb99933cbeef9c65ed40d236f8058">http://www.sciencedirect.com/science/article/B7581-4CRPC4H-13/2/12dbb99933cbeef9c65ed40d236f8058</a> </p><br />

<p>   40.    6288   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Up close and personal: anti-herpes drug development</p>

<p>           Bradbury, Jane</p>

<p>           Drug Discovery Today 2004. 9(14): 589</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T64-4CSGPSR-3/2/0fe2a07b5a6797417bd9ecc8d5439200">http://www.sciencedirect.com/science/article/B6T64-4CSGPSR-3/2/0fe2a07b5a6797417bd9ecc8d5439200</a> </p><br />

<p>   41.    6289   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           &quot;Teaching old drugs to kill new bugs&quot;: structure-based discovery of anti-SARS drugs</p>

<p>           Rajnarayanan, Rajendram V, Dakshanamurthy, Sivanesan, and Pattabiraman, Nagarajan</p>

<p>           Biochemical and Biophysical Research Communications 2004. 321(2): 370-378</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4CX02PF-5/2/676344dcc4b66460e3e7dac68c434b10">http://www.sciencedirect.com/science/article/B6WBK-4CX02PF-5/2/676344dcc4b66460e3e7dac68c434b10</a> </p><br />

<p>   42.    6290   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           P4 and P1&#39; optimization of bicycloproline P2 bearing tetrapeptidyl [alpha]-ketoamides as HCV protease inhibitors</p>

<p>           Yip, Yvonne, Victor, Frantz, Lamar, Jason, Johnson, Robert, Wang, QMay, Glass, John I, Yumibe, Nathan, Wakulchik, Mark, Munroe, John, and Chen, Shu-Hui</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4D09F40-4/2/57a1b462d983145835bdce8e03953d59">http://www.sciencedirect.com/science/article/B6TF9-4D09F40-4/2/57a1b462d983145835bdce8e03953d59</a> </p><br />

<p>   43.    6291   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           The 3D structure analysis of SARS-CoV S1 protein reveals a link to influenza virus neuraminidase and implications for drug and antibody discovery</p>

<p>           Zhang, Xue Wu and Yap, Yee Leng</p>

<p>           Journal of Molecular Structure: THEOCHEM 2004. 681(1-3): 137-141</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TGT-4CTN56B-4/2/e87e0de8ef0be87b0bbf11c0cfa423c7">http://www.sciencedirect.com/science/article/B6TGT-4CTN56B-4/2/e87e0de8ef0be87b0bbf11c0cfa423c7</a> </p><br />

<p>   44.    6292   DMID-LS-72; EMBASE-DMID-8/4/2004</p>

<p class="memofmt1-3">           Characterization of the 3a Protein of SARS-associated Coronavirus in Infected Vero E6 Cells and SARS Patients</p>

<p>           Zeng, Rong, Yang, Rui-Fu, Shi, Mu-De, Jiang, Man-Rong, Xie, You-Hua, Ruan, Hong-Qiang, Jiang, Xiao-Sheng, Shi, Lv, Zhou, Hu, and Zhang, Lei</p>

<p>           Journal of Molecular Biology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WK7-4CP4M85-F/2/07baf50f6d649da67fb0df4ff0579a20">http://www.sciencedirect.com/science/article/B6WK7-4CP4M85-F/2/07baf50f6d649da67fb0df4ff0579a20</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
