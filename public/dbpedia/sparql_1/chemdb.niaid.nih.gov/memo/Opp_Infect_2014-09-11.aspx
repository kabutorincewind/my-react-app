

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-09-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="u3wFFcWtUEip0H0ByZKaV+pyMD4UIyyVaYM3dJaCrkMjqnaLoZVvuOHJ5lq5Dlt6j8mxddeq8ZDxdlIuPjm8TOcunV6PWHtx5j9QbWDRMe5But7v3DhDTl2IqFw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C081D538" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 29 - September 11, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24913563">Polyethyleneimine and Polyethyleneimine-based Nanoparticles: Novel Bacterial and Yeast Biofilm Inhibitors.</a>Azevedo, M.M., P. Ramalho, A.P. Silva, R. Teixeira-Santos, C. Pina-Vaz, and A.G. Rodrigues. Journal of Medical Microbiology, 2014. 63(Pt 9): p. 1167-1173. PMID[24913563].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24880247">Sulfone Derivatives Reduce Growth, Adhesion and Aspartic Protease SAP2 Gene Expression.</a> Bondaryk, M., Z. Ochal, and M. Staniszewska. World Journal of Microbiology and Biotechnology, 2014. 30(9): p. 2511-2521. PMID[24880247].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24788027">Essential Oil Composition and Antimicrobial Activity of Angelica archangelica L. (Apiaceae) Roots.</a> Fraternale, D., G. Flamini, and D. Ricci. Journal of Medicinal Food, 2014. 17(9): p. 1043-1047. PMID[24788027].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24957821">Micafungin at Physiological Serum Concentrations Shows Antifungal Activity against Candida albicans and Candida parapsilosis Biofilms.</a>Guembe, M., J. Guinea, L.J. Marcos-Zambrano, A. Fernandez-Cruz, T. Pelaez, P. Munoz, and E. Bouza. Antimicrobial Agents and Chemotherapy, 2014. 58(9): p. 5581-5584. PMID[24957821].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25115626">Scaffold Hopping of Sampangine: Discovery of Potent Antifungal Lead Compound against Aspergillus fumigatus and Cryptococcus neoformans.</a> Jiang, Z., N. Liu, G. Dong, Y. Jiang, Y. Liu, X. He, Y. Huang, S. He, W. Chen, Z. Li, J. Yao, Z. Miao, W. Zhang, and C. Sheng. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(17): p. 4090-4094. PMID[25115626].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24611777">Antimicrobial Activities of Some Thai Traditional Medical Longevity Formulations from Plants and Antibacterial Compounds from Ficus foveolata.</a> Meerungrueang, W. and P. Panichayupakaranant. Pharmaceutical Biology, 2014. 52(9): p. 1104-1109. PMID[24611777].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25164197">Which Tree Orders in Southern Africa Have the Highest Antimicrobial Activity and Selectivity against Bacterial and Fungal Pathogens of Animals?</a> Pauw, E. and J.N. Eloff. BMC Complementary and Alternative Medicine, 2014. 14: p. 317. PMID[25164197].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24988412">Biosynthesis of Silver Nanoparticles Using Lingonberry and Cranberry Juices and Their Antimicrobial Activity.</a>Puiso, J., D. Jonkuviene, I. Macioniene, J. Salomskiene, I. Jasutiene, and R. Kondrotas. Colloids and Surfaces B: Biointerfaces, 2014. 121: p. 214-221. PMID[24988412].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25185072">Synthesis and Anti-yeast Evaluation of Novel 2-Alkylthio-4-chloro-5-methyl-N-[imino-(1-oxo-(1H)-phthalazin-2-yl)methyl]benzene Sulfonamide Derivatives.</a> Slawinski, J., A. Pogorzelska, B. Zolnowska, A. Kedzia, M. Ziolkowska-Klinkosz, and E. Kwapisz. Molecules, 2014. 19(9): p. 13704-13723. PMID[25185072].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25110806">Synthesis and Fungistatic Activity of Bicyclic Lactones and Lactams against Botrytis cinerea, Penicillium citrinum, and Aspergillus glaucus.</a> Walczak, P., J. Pannek, F. Boratynski, A. Janik-Polanowicz, and T. Olejniczak. Journal of Agricultural and Food Chemistry, 2014. 62(34): p. 8571-8578. PMID[25110806].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24791826">In Vitro Synergy between Linezolid and Clarithromycin against Mycobacterium tuberculosis.</a> Bolhuis, M.S., T. van der Laan, J.G. Kosterink, T.S. van der Werf, D. van Soolingen, and J.W. Alffenaar. European Respiratory Journal, 2014. 44(3): p. 808-811. PMID[24791826].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24957839">1,4-Azaindole, a Potential Drug Candidate for Treatment of Tuberculosis.</a> Chatterji, M., R. Shandil, M.R. Manjunatha, S. Solapure, V. Ramachandran, N. Kumar, R. Saralaya, V. Panduga, J. Reddy, P. Kr, S. Sharma, C. Sadler, C.B. Cooper, K. Mdluli, P.S. Iyer, S. Narayanan, and P.S. Shirude. Antimicrobial Agents and Chemotherapy, 2014. 58(9): p. 5325-5331. PMID[24957839].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25127167">New Class of Methyl Tetrazole Based Hybrid of (Z)-5-Benzylidene-2-(piperazin-1-yl)thiazol-4(%H)-one as Potent Antitubercular Agents.</a> Chauhan, K., M. Sharma, P. Trivedi, V. Chaturvedi, and P.M. Chauhan. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(17): p. 4166-4170. PMID[25127167].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25161383">Synthesis and Evaluation of a Pyrazinoic acid Prodrug in Mycobacterium tuberculosis.</a>Fernandes, J.P., F.R. Pavan, C.Q. Leite, and V.M. Felli. Saudi Pharmaceutical Journal, 2014. 22(4): p. 376-380. PMID[25161383].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24978593">The 3-(4,5-Dimethylthiazol-2-yl)-2,5-diphenyl tetrazolium bromide (MTT) Assay Is a Rapid, Cheap, Screening Test for the in Vitro Anti-tuberculous Activity of Chalcones.</a> Moodley, S., N.A. Koorbanally, T. Moodley, D. Ramjugernath, and M. Pillay. Journal of Microbiological Methods, 2014. 104: p. 72-78. PMID[24978593].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25129171">Design, Synthesis, Biological Evaluation of Substituted Benzofurans as DNA GyraseB Inhibitors of Mycobacterium tuberculosis.</a>Renuka, J., K.I. Reddy, K. Srihari, V.U. Jeankumar, M. Shravan, J.P. Sridevi, P. Yogeeswari, K.S. Babu, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2014. 22(17): p. 4924-4934. PMID[25129171].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24803084">New Compounds Hybrids 1H-1,2,3-Triazole-quinoline against Plasmodium falciparum.</a> Boechat, N., L. Ferreira Mde, L.C. Pinheiro, A.M. Jesus, M.M. Leite, C.C. Junior, A.C. Aguiar, I.M. de Andrade, and A.U. Krettli. Chemical Biology &amp; Drug Design, 2014. 84(3): p. 325-332. PMID[24803084].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25103602">2-Octadecynoic acid as a Dual Life Stage Inhibitor of Plasmodium Infections and Plasmodial FAS-II Enzymes.</a> Carballeira, N.M., A.G. Bwalya, M.A. Itoe, A.D. Andricopulo, M.L. Cordero-Maldonado, M. Kaiser, M.M. Mota, A.D. Crawford, R.V. Guido, and D. Tasdemir. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(17): p. 4151-4157. PMID[25103602].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25115700">Endoperoxide Polyketides from a Chinese Plakortis Simplex: Further Evidence of the Impact of Stereochemistry on Antimalarial Activity of Simple 1,2-Dioxanes.</a> Chianese, G., M. Persico, F. Yang, H.W. Lin, Y.W. Guo, N. Basilico, S. Parapini, D. Taramelli, O. Taglialatela-Scafati, and C. Fattorusso. Bioorganic &amp; Medicinal Chemistry, 2014. 22(17): p. 4572-4580. PMID[25115700].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25039009">Indolosesquiterpene Alkaloids from the Cameroonian Medicinal Plant Polyalthia oliveri (Annonaceae).</a> Kouam, S.F., A.W. Ngouonpe, M. Lamshoft, F.M. Talontsi, J.O. Bauer, C. Strohmann, B.T. Ngadjui, H. Laatsch, and M. Spiteller. Phytochemistry, 2014. 105: p. 52-59. PMID[25039009].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24913172">KAF156 Is an Antimalarial Clinical Candidate with Potential for Use in Prophylaxis, Treatment, and Prevention of Disease Transmission.</a> Kuhen, K.L., A.K. Chatterjee, M. Rottmann, K. Gagaring, R. Borboa, J. Buenviaje, Z. Chen, C. Francek, T. Wu, A. Nagle, S.W. Barnes, D. Plouffe, M.C. Lee, D.A. Fidock, W. Graumans, M. van de Vegte-Bolmer, G.J. van Gemert, G. Wirjanata, B. Sebayang, J. Marfurt, B. Russell, R. Suwanarusk, R.N. Price, F. Nosten, A. Tungtaeng, M. Gettayacamin, J. Sattabongkot, J. Taylor, J.R. Walker, D. Tully, K.P. Patra, E.L. Flannery, J.M. Vinetz, L. Renia, R.W. Sauerwein, E.A. Winzeler, R.J. Glynne, and T.T. Diagana. Antimicrobial Agents and Chemotherapy, 2014. 58(9): p. 5060-5067. PMID[24913172].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0829-091114.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339715100006">Vicilin-like Peptides from Capsicum baccatum L. Seeds are alpha-Amylase Inhibitors and Exhibit Antifungal Activity against Important Yeasts in Medical Mycology.</a>Bard, G.C.V., V.V. Nascimento, A.E.A. Oliveira, R. Rodrigues, M. Da Cunha, G.B. Dias, I.M. Vasconcelos, A.O. Carvalho, and V.M. Gomes. Biopolymers, 2014. 102(4): p. 335-343. ISI[000339715100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340318400006">Antioxidant and Antimicrobial Activities of Psidium guajava L. Spray Dried Extracts.</a> Fernandes, M.R.V., A.L.T. Dias, R.R. Carvalho, C.R.F. Souza, and W.P. Oliveira. Industrial Crops and Products, 2014. 60: p. 39-44. ISI[000340318400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />   

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340341000012">The Effect of Zuccagnia punctata, an Argentine Medicinal Plant, on Virulence Factors from Candida Species.</a> Gabriela, N., A.M. Rosa, Z.I. Catiana, C. Soledad, O.R. Mabel, S.J. Esteban, B. Veronica, W. Daniel, and I.M. Ines. Natural Product Communications, 2014. 9(7): p. 933-936. ISI[000340341000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340168200031">Chemical Constituents, Antimicrobial and Antioxidative Effects of Trachyspermum ammi Essential Oil.</a> Gandomi, H., S. Abbaszadeh, A. Jebellijavan, and A. Sharifzadeh. Journal of Food Processing and Preservation, 2014. 38(4): p. 1690-1695. ISI[000340168200031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340106800009">Metal-based Carboxamide-derived Compounds Endowed with Antibacterial and Antifungal Activity.</a> Hanif, M., Z.H. Chohan, J.Y. Winum, and J. Akhtar. Journal of Enzyme Inhibition and Medicinal Chemistry, 2014. 29(4): p. 517-526. ISI[000340106800009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340149800006">Two Cu(II) Complexes from an N-Alkylated Benzimidazole: Synthesis, Structural Characterization, and Biological Properties.</a> Kose, M., M. Digrak, I. Gonul, and V. McKee. Journal of Coordination Chemistry, 2014. 67(10): p. 1746-1759. ISI[000340149800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340341000031">Chemical Composition, Antioxidant, Antimicrobial and Anti-inflammatory Activities of the Stem and Leaf Essential Oils from Piper flaviflorum from Xishuangbanna, Sw China.</a> Li, R., J.J. Yang, Y.F. Wang, Q. Sun, and H.B. Hu. Natural Product Communications, 2014. 9(7): p. 1011-1014. ISI[000340341000031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340273000001">Asterisulphoxide and Asterisulphone: Two New Antibacterial and Antifungal Metabolites from the Tunisian Asteriscus maritimus (L.) Less.</a> Medimagh-Saidana, S., M. Daami-Remadi, P. Abreu, F. Harzallah-Skhiri, H. Ben Jannet, and M.A. Hamza. Natural Product Research, 2014. 28(18): p. 1418-1426. ISI[000340273000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339931600004">Synthesis, Characterization, Electrochemical Behavior and Antibacterial/Antifungal Activities of [Cd(L)X<sub>2</sub>] Complexes with a New Schiff Base Ligand.</a> Montazerozohori, M., S. Yadegari, and A. Naghiha. Journal of the Serbian Chemical Society, 2014. 79(7): p. 793-804. ISI[000339931600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339835000001">Antimycobacterial, Antimicrobial, and Biocompatibility Properties of para-Aminosalicylic acid with Zinc Layered Hydroxide and Zn/Al Layered Double Hydroxide Nanocomposites.</a> Saifullah, B., M.E. El Zowalaty, P. Arulselvan, S. Fakurazi, T.J. Webster, B.M. Geilich, and M.Z. Hussein. Drug Design Development and Therapy, 2014. 8: p. 1029-1036. ISI[000339835000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340223400014">Synthesis, Docking and ADMET Prediction of Novel 5-((5-Substituted-1-H-1,2,4-triazol-3-yl) methyl)-4,5,6,7-tetrahydrothieno[3,2-c]pyridine as Antifungal Agents.</a> Sangshetti, J.N., F.A.K. Khan, R.S. Chouthe, M.G. Damale, and D.B. Shinde. Chinese Chemical Letters, 2014. 25(7): p. 1033-1038. ISI[000340223400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339857900002">Antifungal Effect of Gatifloxacin and Copper Ions Combination.</a> Shams, S., B. Ali, M. Afzal, I. Kazmi, F.A. A-Abbasi, and F. Anwar. Journal of Antibiotics, 2014. 67(7): p. 499-504. ISI[000339857900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340321900002">Design of Novel Camphane-based Derivatives with Antimycobacterial Activity.</a> Stavrakov, G., V. Valcheva, I. Philipova, and I. Doytchinova. Journal of Molecular Graphics &amp; Modelling, 2014. 51: p. 7-12. ISI[000340321900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339707000004">Phenolic Composition, Antioxidant and Antimicrobial Activity of the Extracts from Prunus spinosa L. Fruit.</a> Velickovic, J.M., D.A. Kostic, G.S. Stojanovic, S.S. Mitic, M.N. Mitic, S.S. Randelovic, and A.S. Dordevic. Hemijska Industrija, 2014. 68(3): p. 297-303. ISI[000339707000004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340140400008">Influence of Acetylation Degree and Molecular Weight of Homogeneous Chitosans on Antibacterial and Antifungal Activities.</a>Younes, I., S. Sellimi, M. Rinaudo, K. Jellouli, and M. Nasri. International Journal of Food Microbiology, 2014. 185: p. 57-63. ISI[000340140400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0829-091114.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
