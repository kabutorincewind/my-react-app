

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-377.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="2pKSgXjMtSFKJCtqMS2Goe9PIRtUdJIx/6XP7hkVhr62VIjjSPki4SCq4IO24YtI0NTtv7pCVg3ofqXmTcjVW33fGdhcBcWSH/UvOc0iNQ4atlLYumbW+sSrUH4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D5D293DA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-377-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61116   HIV-LS-377; WOS-HI-6/15/2007</p>

    <p class="memofmt1-2">          Cd4(+) T Cell Targeting of Human Immunodeficiency Virus Type 1 (Hiv-1) Peptide Sequences Present in Vivo During Chronic, Progressive Hiv-1 Disease</p>

    <p>          Boritz, E. <i>et al.</i></p>

    <p>          Virology <b>2007</b>.  361(1): 34-44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246042300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246042300005</a> </p><br />

    <p>2.     61117   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          A new strategy to inhibit the excision reaction catalysed by HIV-1 reverse transcriptase: compounds that compete with the template-primer</p>

    <p>          Cruchaga, Carlos, Anso, Elena, Font, Maria, Martino, Virginia S, Rouzaut, Ana, and Martinez-irujo, Juan J</p>

    <p>          Biochem. J. <b>2007</b>.  405(1): 165-171</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61118   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          Compounds and methods for inhibiting viral entry</p>

    <p>          Gervay-Hague, Jacquelyn, Meadows, Christopher D, North, Thomas W, and Duong, Yen T</p>

    <p>          PATENT:  WO <b>2007065032</b>  ISSUE DATE:  20070607</p>

    <p>          APPLICATION: 2006  PP: 61pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61119   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          Aminophenylsulfonamide derivatives as HIV protease inhibitors, their preparation, pharmaceutical compositions, and use in therapy</p>

    <p>          De Kock, Herman Augustinus, Jonckers, Tim Hugo Maria, Last, Stefaan Julien, Boonants, Paul Jozef Gabriel Maria, Surleraux, Dominique Louis Nestor Ghislain, and Wigerinck, Piet Tom Bert Paul</p>

    <p>          PATENT:  WO <b>2007060253</b>  ISSUE DATE:  20070531</p>

    <p>          APPLICATION: 2006  PP: 41pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61120   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          Single-Stranded DNA Aptamer RT1t49 Inhibits RT Polymerase and RNase H Functions of HIV Type 1, HIV Type 2, and SIVCPZ RTs</p>

    <p>          Kissel, Jay D, Held, Daniel M, Hardy, Richard W, and Burke, Donald H</p>

    <p>          AIDS Res. Hum. Retroviruses <b>2007</b>.  23(5): 699-708</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61121   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          Synthesis of novel 1,4-disubstituted nucleosides as potential antitumor agents</p>

    <p>          Kim, Aihong, Ko, Ok Hyun, and Hong, Joon Hee</p>

    <p>          Yakhak Hoechi <b>2007</b>.  51(2): 103-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     61122   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          MK-0518 HIV integrase inhibitor</p>

    <p>          Wang, Y, Serradell, N, Bolos, J, and Rosa, E</p>

    <p>          Drugs Future <b>2007</b>.  32(2): 118-122</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>8.     61123   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          Sensitive and specific LC-ESI-MS/MS method for the determination of a styrylquinoline, BA011FZ041, a potent HIV anti-integrase agent, in rat plasma</p>

    <p>          Pruvost, Alain, Levi, Mikael, Zouhiri, Fatima, Menier, Isabelle, and Benech, Henri</p>

    <p>          J. Chromatogr., B: Anal. Technol. Biomed. Life Sci. <b>2007</b>.  850(1-2): 259-266</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61124   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          Recombinant anti-viral polypeptides and conjugates of human interferon a for treating HCV, HBV and HIV infection</p>

    <p>          Paidhungat, Madan M, Sas, Ian, Bouquin, Thomas, Lin, David, Chen, Teddy, Guptill, Douglas, Brideau-Andersen, Amy, Bass, Steven H, and Patten, Phillip A</p>

    <p>          PATENT:  WO <b>2007044083</b>  ISSUE DATE:  20070419</p>

    <p>          APPLICATION: 2006  PP: 365pp.</p>

    <p>          ASSIGNEE:  (Maxygen, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61125   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          CD25+ Regulatory T Cells Isolated from HIV-Infected Individuals Suppress The Cytolytic And Nonlytic Antiviral Activity of HIV-specific CD8+ T Cells in Vitro</p>

    <p>          Kinter, Audrey L, Horak, Robin, Sion, Melanie, Riggin, Lindsey, McNally, Jonathan, Lin, Yin, Jackson, Robert, O&#39;Shea, Angeline, Roby, Gregg, Kovacs, Colin, Connors, Mark, Migueles, Stephen A, and Fauci, Anthony S</p>

    <p>          AIDS Res. Hum. Retroviruses <b>2007</b>.  23(3): 438-450</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61126   HIV-LS-377; SCIFINDER-HIV-6/18/2007</p>

    <p class="memofmt1-2">          Preparation of alkenyldiarylmethanes and fused analogs as inhibitors of HIV-I reverse transcriptase</p>

    <p>          Cushman, Mark S and Deng, Bo-Liang</p>

    <p>          PATENT:  WO <b>2007005531</b>  ISSUE DATE:  20070111</p>

    <p>          APPLICATION: 2006  PP: 72pp.</p>

    <p>          ASSIGNEE:  (Purdue Research Foundation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61127   HIV-LS-377; WOS-HI-6/22/2007</p>

    <p class="memofmt1-2">          Interaction of Human Immunodeficiency Virus Type 1 Integrase With Cellular Nuclear Import Receptor Importin 7 and Its Impact on Viral Replication</p>

    <p>          Ao, Z. <i>et al.</i></p>

    <p>          Journal of Biological Chemistry <b>2007</b>.  282(18): 13456-13467</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246060300035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246060300035</a> </p><br />

    <p>13.   61128   HIV-LS-377; WOS-HI-6/22/2007</p>

    <p class="memofmt1-2">          Intensification of a Triple-Nucleoside Regimen With Tenofovir or Efavirenz in Hiv-1-Infected Patients With Virological Suppression</p>

    <p>          Gulick, R. <i>et al.</i></p>

    <p>          Aids <b>2007</b>.  21 (7): 813-823</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246162900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246162900004</a> </p><br />
    <br clear="all">

    <p>14.   61129   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Thiotetrazole alkynylacetanilides as potent and bioavailable non-nucleoside inhibitors of the HIV-1 wild type and K103N/Y181C double mutant reverse transcriptases</p>

    <p>          Gagnon, A, Amad, MH, Bonneau, PR, Coulombe, R, Deroy, PL, Doyon, L, Duan, J, Garneau, M, Guse, I, Jakalian, A, Jolicoeur, E, Landry, S, Malenfant, E, Simoneau, B, and Yoakim, C</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17583503&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17583503&amp;dopt=abstract</a> </p><br />

    <p>15.   61130   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Synthesis and Anti-HIV Activity of New Metabolically Stable Alkenyldiarylmethane Non-Nucleoside Reverse Transcriptase Inhibitors Incorporating N-Methoxy Imidoyl Halide and 1,2,4-Oxadiazole Systems</p>

    <p>          Sakamoto, T, Cullen, MD, Hartman, TL, Watson, KM, Buckheit, RW, Pannecouque, C, Clercq, ED, and Cushman, M</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17579385&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17579385&amp;dopt=abstract</a> </p><br />

    <p>16.   61131   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Design, synthesis, and antiviral evaluation of some 3&#39;-carboxymethyl-3&#39;-deoxyadenosine derivatives</p>

    <p>          Peterson, MA, Ke, P, Shi, H, Jones, C, McDougall, BR, and Robinson, WE Jr</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(5): 499-519</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17578746&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17578746&amp;dopt=abstract</a> </p><br />

    <p>17.   61132   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Azt Analogues with A Spacer Arm Between Glucidic and Base Moieties. Part II</p>

    <p>          Roy, V, Zerrouki, R, Krausz, P, Laumond, G, and Aubertin, AM</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(5): 413-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17578740&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17578740&amp;dopt=abstract</a> </p><br />

    <p>18.   61133   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          5&#39;-Modified G-Quadruplex Forming Oligonucleotides Endowed with Anti-HIV Activity: Synthesis and Biophysical Properties</p>

    <p>          D&#39;Onofrio, J, Petraccone, L, Erra, E, Martino, L, Fabio, GD, Napoli, LD, Giancola, C, and Montesarchio, D</p>

    <p>          Bioconjug Chem  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17569499&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17569499&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   61134   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Phase 2 Study of the Safety and Efficacy of Vicriviroc, a CCR5 Inhibitor, in HIV-1-Infected, Treatment-Experienced Patients: AIDS Clinical Trials Group 5211</p>

    <p>          Gulick, RM, Su, Z, Flexner, C, Hughes, MD, Skolnik, PR, Wilkin, TJ, Gross, R, Krambrink, A, Coakley, E, Greaves, WL, Zolopa, A, Reichman, R, Godfrey, C, Hirsch, M, and Kuritzkes, DR</p>

    <p>          J Infect Dis <b>2007</b>.  196(2): 304-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17570119&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17570119&amp;dopt=abstract</a> </p><br />

    <p>20.   61135   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Anti HIV-1 Flavonoid Glycosides from Ochna integerrima</p>

    <p>          Reutrakul, V, Ningnuek, N, Pohmakotr, M, Yoosook, C, Napaswad, C, Kasisit, J, Santisuk, T, and Tuchinda, P</p>

    <p>          Planta Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562490&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562490&amp;dopt=abstract</a> </p><br />

    <p>21.   61136   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Synthetic access to spacer-linked 3,6-diamino-2,3,6-trideoxy-alpha-d-glucopyranosides-potential aminoglycoside mimics for the inhibition of the HIV-1 TAR-RNA/Tat-peptide complex</p>

    <p>          Joge, T, Jesberger, M, Broker, P, and Kirschning, A</p>

    <p>          Carbohydr Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562328&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562328&amp;dopt=abstract</a> </p><br />

    <p>22.   61137   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          CCR5 density levels on primary CD4 T cells impact the replication and Enfuvirtide susceptibility of R5 HIV-1</p>

    <p>          Heredia, A, Gilliam, B, Devico, A, Le, N, Bamba, D, Flinko, R, Lewis, G, Gallo, RC, and Redfield, RR</p>

    <p>          AIDS <b>2007</b>.  21 (10): 1317-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17545708&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17545708&amp;dopt=abstract</a> </p><br />

    <p>23.   61138   HIV-LS-377; PUBMED-HIV-6/25/2007</p>

    <p class="memofmt1-2">          Design of Mutation-resistant HIV Protease Inhibitors with the Substrate Envelope Hypothesis</p>

    <p>          Chellappan, S, Kiran, Kumar Reddy GS, Ali, A, Nalam, MN, Anjum, SG, Cao, H, Kairys, V, Fernandes, MX, Altman, MD, Tidor, B, Rana, TM, Schiffer, CA, and Gilson, MK</p>

    <p>          Chem Biol Drug Des <b>2007</b>.  69(5): 298-313</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17539822&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17539822&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
