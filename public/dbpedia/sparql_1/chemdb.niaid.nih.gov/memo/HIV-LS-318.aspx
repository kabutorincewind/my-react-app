

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-318.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lMwZ+v0sAaz74AgQ+i08bfWuWF4OTBtRBd38s7gsj1ybMnExdyY3WFuSBPokhlh6cFjWgh0rkFDQVy55ombLvGIGdAjcN7CUVAA3qdY/TlX4e80JXHSOc4ngsEE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="940EB249" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-318-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         45023   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Indole and carbazole alkaloids from Glycosmis montana with weak anti-HIV and cytotoxic activities</p>

    <p>          Wang, J, Zheng, Y, Efferth, T, Wang, R, Shen, Y, and Hao, X</p>

    <p>          Phytochemistry  <b>2005</b>.  66(6): 697-701</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771893&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771893&amp;dopt=abstract</a> </p><br />

    <p>2.         45024   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Development of Low Molecular Weight HIV-1 Protease Dimerization Inhibitors</p>

    <p>          Hwang, YS and Chmielewski, J</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 2239-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771466&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771466&amp;dopt=abstract</a> </p><br />

    <p>3.         45025   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Structure Based Activity Prediction of HIV-1 Reverse Transcriptase Inhibitors</p>

    <p>          de Jonge, MR,  Koymans, LM, Vinkers, HM , Daeyaert, FF, Heeres, J, Lewi, PJ, and Janssen, PA</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 2176-2183</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771460&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771460&amp;dopt=abstract</a> </p><br />

    <p>4.         45026   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Synthesis of Novel Diarylpyrimidine Analogues and Their Antiviral Activity against Human Immunodeficiency Virus Type 1</p>

    <p>          Guillemont, J, Pasquier, E, Palandjian, P, Vernier, D, Gaurrand, S, Lewi, PJ, Heeres, J, de, Jonge MR, Koymans, LM, Daeyaert, FF, Vinkers, MH, Arnold, E, Das, K, Pauwels, R, Andries, K, de, Bethune MP, Bettens, E, Hertogs, K, Wigerinck, P, Timmerman, P, and Janssen, PA</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 2072-2079</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771449&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771449&amp;dopt=abstract</a> </p><br />

    <p>5.         45027   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Design of HIV-1 Protease Inhibitors Active on Multidrug-Resistant Virus</p>

    <p>          Surleraux, DL, de, Kock HA, Verschueren, WG, Pille, GM, Maes, LJ, Peeters, A, Vendeville, S, De, Meyer S, Azijn, H, Pauwels, R, de, Bethune MP, King, NM, Prabu-Jeyabalan, M, Schiffer, CA, and Wigerinck, PB</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 1965-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771440&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771440&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         45028   HIV-LS-318; EMBASE-HIV-3/22/2005</p>

    <p class="memofmt1-2">          The fd phage and a peptide derived from its p8 coat protein interact with the HIV-1 Tat-NLS and inhibit its biological functions</p>

    <p>          Krichevsky, A, Rusnati, M, Bugatti, A, Waigmann, E, Shohat, S, and Loyter, A</p>

    <p>          Antiviral Research <b>2005</b>.  66(1): 67-78</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4FJXJKJ-1/2/24393053fd342e143ba92531fd1cbf3c">http://www.sciencedirect.com/science/article/B6T2H-4FJXJKJ-1/2/24393053fd342e143ba92531fd1cbf3c</a> </p><br />

    <p>7.         45029   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Design and Optimization of Tricyclic Phtalimide Analogues as Novel Inhibitors of HIV-1 Integrase</p>

    <p>          Verschueren, WG, Dierynck, I, Amssoms, KI, Hu, L, Boonants, PM, Pille, GM, Daeyaert, FF, Hertogs, K, Surleraux, DL, and Wigerinck, PB</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 1930-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771437&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771437&amp;dopt=abstract</a> </p><br />

    <p>8.         45030   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Design, Synthesis, and SAR of a Novel Pyrazinone Series with Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitory Activity</p>

    <p>          Heeres, J, de, Jonge MR, Koymans, LM, Daeyaert, FF, Vinkers, M, Van, Aken KJ, Arnold, E, Das, K, Kilonda, A, Hoornaert, GJ, Compernolle, F, Cegla, M, Azzam, RA, Andries, K, de, Bethune MP, Azijn, H, Pauwels, R, Lewi, PJ, and Janssen, PA</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 1910-1918</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771435&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771435&amp;dopt=abstract</a> </p><br />

    <p>9.         45031   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          In Search of a Novel Anti-HIV Drug: Multidisciplinary Coordination in the Discovery of 4-[[4-[[4-[(1E)-2-Cyanoethenyl]-2,6-dimethylphenyl]amino]-2- pyrimidinyl]amino]benzonitrile (R278474, Rilpivirine)</p>

    <p>          Janssen, PA, Lewi, PJ, Arnold, E, Daeyaert, F, de, Jonge M, Heeres, J, Koymans, L, Vinkers, M, Guillemont, J, Pasquier, E, Kukla, M, Ludovici, D, Andries, K, de, Bethune MP, Pauwels, R, Das, K, Clark, AD Jr, Frenkel, YV, Hughes, SH, Medaer, B, De, Knaep F, Bohets, H, De, Clerck F, Lampo, A, Williams, P, and Stoffels, P</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 1901-1909</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771434&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771434&amp;dopt=abstract</a> </p><br />

    <p>10.       45032   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Discovery and Selection of TMC114, a Next Generation HIV-1 Protease Inhibitor</p>

    <p>          Surleraux, DL, Tahri, A, Verschueren, WG, Pille, GM, de, Kock HA, Jonckers, TH, Peeters, A, De, Meyer S, Azijn, H, Pauwels, R, de, Bethune MP, King, NM, Prabu-Jeyabalan, M, Schiffer, CA, and Wigerinck, PB</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 1813-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771427&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771427&amp;dopt=abstract</a> </p><br />

    <p>11.       45033   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Atazanavir: the advent of a new generation of more convenient protease inhibitors</p>

    <p>          Barreiro, P, Rendon, A, Rodriguez-Novoa, S, and Soriano, V</p>

    <p>          HIV Clin Trials <b>2005</b>.  6(1): 50-61</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15765311&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15765311&amp;dopt=abstract</a> </p><br />

    <p>12.       45034   HIV-LS-318; EMBASE-HIV-3/22/2005</p>

    <p class="memofmt1-2">          HIV protease inhibitors: synthesis and activity of N-aryl-N&#39;-hydroxyalkyl hydrazide pseudopeptides</p>

    <p>          Marastoni, M, Baldisserotto, A, Trapella, C, McDonald, J, Bortolotti, F, and Tomatis, R</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4FJTNRY-3/2/74e0f5bd1626524fdf52ce04faa8c929">http://www.sciencedirect.com/science/article/B6VKY-4FJTNRY-3/2/74e0f5bd1626524fdf52ce04faa8c929</a> </p><br />

    <p>13.       45035   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Tipranavir: a novel second-generation nonpeptidic protease inhibitor</p>

    <p>          Kandula, VR, Khanlou, H, and Farthing, C</p>

    <p>          Expert Rev Anti Infect Ther <b>2005</b>.  3(1): 9-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757454&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757454&amp;dopt=abstract</a> </p><br />

    <p>14.       45036   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          The cellular HIV-1 Rev cofactor hRIP is required for viral replication</p>

    <p>          Yu, Z, Sanchez-Velar, N, Catrina, IE, Kittler, EL, Udofia, EB, and Zapp, ML</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.  102(11): 4027-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749819&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749819&amp;dopt=abstract</a> </p><br />

    <p>15.       45037   HIV-LS-318; EMBASE-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Study on the interactions between anti-HIV-1 active compounds with trans-activation response RNA by affinity capillary electrophoresis</p>

    <p>          Ding, Li, Zhang, Xin-Xiang, Chang, Wen-Bao, Lin, Wei, and Yang, Ming</p>

    <p>          Journal of Chromatography B <b>2005</b>.  814(1): 99-104</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6X0P-4DS7T1N-4/2/97a0752715dcbf41814c8ff5d06ef9f8">http://www.sciencedirect.com/science/article/B6X0P-4DS7T1N-4/2/97a0752715dcbf41814c8ff5d06ef9f8</a> </p><br />

    <p>16.       45038   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Dynamic receptor-based pharmacophore model development and its application in designing novel HIV-1 integrase inhibitors</p>

    <p>          Deng, J, Lee, KW, Sanchez, T, Cui, M, Neamati, N, and Briggs, JM</p>

    <p>          J Med Chem <b>2005</b>.  48(5): 1496-1505</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743192&amp;dopt=abstract</a> </p><br />

    <p>17.       45039   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Integrase inhibitors to treat HIV/AIDS</p>

    <p>          Pommier, Y, Johnson, AA, and Marchand, C</p>

    <p>          Nat Rev Drug Discov <b>2005</b>.  4(3): 236-48</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15729361&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15729361&amp;dopt=abstract</a> </p><br />

    <p>18.       45040   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          A novel and efficient approach to discriminate between pre- and post-transcription HIV inhibitors</p>

    <p>          Daelemans, D,  Pannecouque, C, Pavlakis, GN, Tabarrini, O, and De Clercq, E</p>

    <p>          Mol Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728798&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728798&amp;dopt=abstract</a> </p><br />

    <p>19.       45041   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          5-Alkyl-2-alkylamino-6-(2,6-difluorophenylalkyl)-3,4-dihydropyrimidin-4(3H )-ones, a new series of potent, broad-spectrum non-nucleoside reverse transcriptase inhibitors belonging to the DABO family</p>

    <p>          Mai, A, Artico, M, Ragno, R, Sbardella, G, Massa, S, Musiu, C, Mura, M, Marturana, F, Cadeddu, A, Maga, G, and Colla, PL</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(6): 2065-2077</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15727860&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15727860&amp;dopt=abstract</a> </p><br />

    <p>20.       45042   HIV-LS-318; PUBMED-HIV-3/22/2005</p>

    <p class="memofmt1-2">          Hexitol nucleic acid-containing aptamers are efficient ligands of HIV-1 TAR RNA</p>

    <p>          Kolb, G, Reigadas, S, Boiziau, C, van Aerschot, A, Arzumanov, A, Gait, MJ, Herdewijn, P, and Toulme, JJ</p>

    <p>          Biochemistry <b>2005</b>.  44(8): 2926-2933</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15723535&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15723535&amp;dopt=abstract</a> </p><br />

    <p>21.       45043   HIV-LS-318; WOS-HIV-3/13/2005</p>

    <p class="memofmt1-2">          Adaptive inhibitors of the HIV-1 protease</p>

    <p>          Ohtaka, H and Freire, E</p>

    <p>          PROGRESS IN BIOPHYSICS &amp; MOLECULAR BIOLOGY <b>2005</b>.  88(2): 193-208, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227204200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227204200002</a> </p><br />

    <p>22.       45044   HIV-LS-318; WOS-HIV-3/13/2005</p>

    <p class="memofmt1-2">          Cluster analysis and three-dimensional QSAR studies of HIV-1 integrase inhibitors</p>

    <p>          Yuan, HB and Parrill, A</p>

    <p>          JOURNAL OF MOLECULAR GRAPHICS &amp; MODELLING <b>2005</b>.  23(4): 317-328, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227150400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227150400003</a> </p><br />
    <br clear="all">

    <p>23.       45045   HIV-LS-318; WOS-HIV-3/13/2005</p>

    <p class="memofmt1-2">          A novel once-daily protease inhibitor</p>

    <p>          Piliero, PJ</p>

    <p>          DRUGS OF TODAY  <b>2004</b>.  40(11): 901-912, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227168600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227168600002</a> </p><br />

    <p>24.       45046   HIV-LS-318; WOS-HIV-3/13/2005</p>

    <p class="memofmt1-2">          The N-terminus of HIV-1 Tat protein is essential for Tat-TAR RNA interaction</p>

    <p>          Chaloin, O, Peter, JC, Briand, JP, Masquida, B, Desgranges, C, Muller, S, and Hoebeke, J</p>

    <p>          CELLULAR AND MOLECULAR LIFE SCIENCES <b>2005</b>.  62(3): 355-361, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227149600010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227149600010</a> </p><br />

    <p>25.       45047   HIV-LS-318; WOS-HIV-3/13/2005</p>

    <p class="memofmt1-2">          Human immunodeficiency virus-reverse transcriptase inhibition and hepatitis C virus RNA-dependent RNA polymerase inhibition activities of fullerene derivatives</p>

    <p>          Mashino, T, Shimotohno, K, Ikegami, N, Nishikawa, D, Okuda, K, Takahashi, K, Nakamura, S, and Mochizuki, M</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(4): 1107-1109, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227135400047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227135400047</a> </p><br />

    <p>26.       45048   HIV-LS-318; WOS-HIV-3/13/2005</p>

    <p class="memofmt1-2">          Analysis of interactions of DNA polymerase beta and reverse transcriptases of human immunodeficiency and mouse leukemia viruses with dNTP analogs containing a modified sugar residue</p>

    <p>          Lebedeva, NA, Seredina, TA, Silnikov, VN, Abramova, TV, Levina, AS, Khodyreva, SN, Rechkunova, NI, and Lavrik, OI</p>

    <p>          BIOCHEMISTRY-MOSCOW <b>2005</b>.  70(1): 1-7, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227139600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227139600001</a> </p><br />

    <p>27.       45049   HIV-LS-318; WOS-HIV-3/20/2005</p>

    <p class="memofmt1-2">          Substrate properties of dinucleoside 5 &#39;,5 &#39;&#39;-oligophosphates in the reactions catalyzed by HIV reverse transcriptase, E-coli DNA polymerase I, and E-coli RNA polymerase</p>

    <p>          Skoblov, AY, Sosunov, VV, Victorova, LS, Skoblov, YS, and Kukhanova, MK</p>

    <p>          RUSSIAN JOURNAL OF BIOORGANIC CHEMISTRY <b>2005</b>.  31(1): 48-57, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227321000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227321000006</a> </p><br />

    <p>28.       45050   HIV-LS-318; WOS-HIV-3/20/2005</p>

    <p class="memofmt1-2">          SIRT1 regulates HIV transcription via Tat deacetylation</p>

    <p>          Pagans, S, Pedal, A, North, BJ, Kaehlcke, K, Marshall, BL, Dorr, A, Hetzer-Egger, C, Henklein, P, Frye, R, McBurney, MW, Hruby, H, Jung, M, Verdin, E, and Ott, M</p>

    <p>          PLOS BIOLOGY <b>2005</b>.  3(2): 210-220, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227404100006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227404100006</a> </p><br />

    <p>29.       45051   HIV-LS-318; WOS-HIV-3/20/2005</p>

    <p class="memofmt1-2">          Sulfonated naphthyl porphyrins as agents against HIV-1</p>

    <p>          Dixon, DW, Gill, AF, Giribabu, L, Vzorov, AN, Alam, AB, and Compans, RW</p>

    <p>          JOURNAL OF INORGANIC BIOCHEMISTRY <b>2005</b>.  99(3): 813-821, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227374900017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227374900017</a> </p><br />

    <p>30.       45052   HIV-LS-318; WOS-HIV-3/20/2005</p>

    <p class="memofmt1-2">          HIV protease inhibitors-induced atherosclerosis: Prevention by alpha-tocopherol</p>

    <p>          Munteanu, A, Ricciarelli, R, and Zingg, JM</p>

    <p>          IUBMB LIFE <b>2004</b>.  56(10): 629-631, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227425000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227425000008</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
