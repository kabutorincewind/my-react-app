

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="STV9E4G95OQZKv/H31lic0+mUA9bvRwSMCgeDda75vC9thtNoDOUqXfH5bNEWMqcJmbTLw9e2QabQvpYoPz3d7+mFNCHXEeW3JDMwrspHPDZJiyiKhk4xDPhc/M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A842C599" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479676103"><i>Mycobacterium</i> Citations List: December, 2017</a></h1>

    <h2><a name="_Toc479676104">Literature Citations</a></h2>

    <p>1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29235357">Synthesis, Characterization and Biological Screening of Pyrazole-conjugated Benzothiazole Analogs.</a> Bhat, M. and S.L. Belagali. Future Medicinal Chemistry, 2018. 10(1): p. 71-87. PMID[29235357].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29035551">Improved Phenoxyalkylbenzimidazoles with Activity against Mycobacterium tuberculosis Appear to Target QcrB.</a> Chandrasekera, N.S., B.J. Berube, G. Shetye, S. Chettiar, T. O&#39;Malley, A. Manning, L. Flint, D. Awasthi, T.R. Ioerger, J. Sacchettini, T. Masquelin, P.A. Hipskind, J. Odingo, and T. Parish. ACS Infectious Diseases, 2017. 3(12): p. 898-916. PMID[29035551]. PMCID[PMC5727484].<b><br />
    [PubMed]</b>. TB_12_2017.</p>

    <p>3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29107541">Synthesis and Evaluation of Analogues of the Tuberculosis Drug Bedaquiline Containing Heterocyclic B-ring Units.</a> Choi, P.J., H.S. Sutherland, A.S.T. Tong, A. Blaser, S.G. Franzblau, C.B. Cooper, M.U. Lotlikar, A.M. Upton, J. Guillemont, M. Motte, L. Queguiner, K. Andries, W. Van den Broeck, W.A. Denny, and B.D. Palmer. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(23): p. 5190-5196. PMID[29107541]. PMCID[PMC5696560].<b><br />
    [PubMed]</b>. TB_12_2017.</p>

    <p>4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29214549">Synthesis, Structure and Toxicity Evaluation of Ethanolamine Nitro/chloronitrobenzoates: A Combined Experimental and Theoretical Study.</a> Crisan, M., L. Halip, P. Bourosh, S.A. Chicu, and Y. Chumakov. Chemistry Central Journal, 2017. 11(1): p. 129. PMID[29214549]. PMCID[PMC5718998].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000416988100053">Antituberculosis, Antibacterial and Antioxidant Activities of Aegiceras corniculatum, a Mangrove Plant and Effect of Various Extraction Processes on Its Phytoconstituents and Bioactivity.</a> Janmanchi, H., A. Raju, M.S. Degani, M.K. Ray, and M.G.R. Rajan. South African Journal of Botany, 2017. 113: p. 421-427. ISI[000416988100053].
    <br />
    <b>[WOS]</b>. TB_12_2017.</p>

    <p>6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28961440">Chemical Synthesis and in Silico Molecular Modeling of Novel Pyrrolyl benzohydrazide Derivatives: Their Biological Evaluation against Enoyl ACP Reductase (InhA) and Mycobacterium tuberculosis.</a> Joshi, S.D., U.A. More, S.R. Dixit, S.V. Balmi, B.G. Kulkarni, G. Ullagaddi, C. Lherbet, and T.M. Aminabhavi. Bioorganic Chemistry, 2017. 75: p. 181-200. PMID[28961440].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28973990">Versatility of 7-Substituted Coumarin Molecules as Antimycobacterial Agents, Neuronal Enzyme Inhibitors and Neuroprotective Agents.</a> Kapp, E., H. Visser, S.L. Sampson, S.F. Malan, E.M. Streicher, G.B. Foka, D.F. Warner, S.I. Omoruyi, A.B. Enogieru, O.E. Ekpo, F.T. Zindo, and J. Joubert. Molecules, 2017. 22(10): E1644. PMID[28973990].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000417615400007">Preparation and Activity of a Complex of Oligohexamethyleneguanine with P-Aminosalicylic acid Derivatives.</a> Kedik, S.A., D.O. Shatalov, P.M. Isaikina, A.D. Askretkov, I.P. Sedishev, A.V. Panov, and A.S. Evseeva. Pharmaceutical Chemistry Journal, 2017. 51(9): p. 773-776. ISI[000417615400007]. <b><br />
    [WOS]</b> . TB_12_2017.</p>

    <p>9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29097168">Synthesis and Biological Evolution of Hydrazones Derived from 4-(Trifluoromethyl)benzohydrazide.</a> Kratky, M., S. Bosze, Z. Baranyai, J. Stolarikova, and J. Vinsova. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(23): p. 5185-5189. PMID[29097168]. <b><br />
    [PubMed]</b>. TB_12_2017.</p>

    <p>10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000416999700008">Synthesis, Spectroscopic Characterization and Antimicrobial Activity Evaluation of New Tridentate Schiff Bases and Their Co(II) Complexes.</a> More, G., D. Raut, K. Aruna, and S. Bootwala. Journal of Saudi Chemical Society, 2017. 21(8): p. 954-964. ISI[000416999700008].
    <br />
    <b>[WOS]</b> . TB_12_2017.</p>

    <p>11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28808382">Allium sativum Constituents Exhibit Anti-tubercular Activity in Vitro and in RAW 264.7 Mouse Macrophage Cells Infected with Mycobacterium tuberculosis H37Rv.</a> Nair, S.S., S.S. Gaikwad, S.P. Kulkarni, and A.P. Mukne. Pharmacognosy Magazine, 2017. 13(50): p. S209-S215. PMID[28808382]. PMCID[PMC5538156].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29072097">Antimycobacterial Activity of Nitrogen Heterocycles Derivatives: 7-(Pyridine-4-yl)-indolizine Derivatives. Part VII(8-12).</a> Olaru, A.M., V. Vasilache, R. Danac, and Mangalagiu, II. Journal of Enzyme Inhibition and Medicinal Chemistry, 2017. 32(1): p. 1291-1298. PMID[29072097].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29220797">Primaquine Hybrids as Promising Antimycobacterial and Antimalarial Agents.</a> Pavic, K., I. Perkovic, S. Pospisilova, M. Machado, D. Fontinha, M. Prudencio, J. Jampilek, A. Coffey, L. Endersen, H. Rimac, and B. Zorc. European Journal of Medicinal Chemistry, 2018. 143: p. 769-779. PMID[29220797].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28609019">Synthesis, Biological Evaluation, and Molecular Docking Studies of Novel 3-Aryl-5-(alkyl-thio)-1H-1,2,4-triazoles Derivatives Targeting Mycobacterium tuberculosis.</a> Rode, N.D., A.D. Sonawane, L. Nawale, V.M. Khedkar, R.A. Joshi, A.P. Likhite, D. Sarkar, and R.R. Joshi. Chemical Biology &amp; Drug Design, 2017. 90(6): p. 1206-1214. PMID[28609019].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29113761">Design, Synthesis and in Vitro Anti-tuberculosis Activity of Benzo[6,7]cyclohepta[1,2-b]pyridine-1,2,3-triazole Derivatives.</a> Sajja, Y., S. Vanguru, H.R. Vulupala, R. Bantu, P. Yogeswari, D. Sriram, and L. Nagarapu. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(23): p. 5119-5121. PMID[29113761].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29222671">Biological Potential of Thiazolidinedione Derivatives of Synthetic Origin.</a> Sucheta, S. Tahlan, and P.K. Verma. Chemistry Central Journal, 2017. 11(130): 29pp. PMID[29222671]. PMCID[PMC5722786].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29259747">Antitubercular Nitroimidazoles Revisited: Synthesis and Activity of the Authentic 3-Nitro Isomer of Pretomanid.</a> Thompson, A.M., M. Bonnet, H.H. Lee, S.G. Franzblau, B. Wan, G.S. Wong, C.B. Cooper, and W.A. Denny. ACS Medicinal Chemistry Letters, 2017. 8(12): p. 1275-1280. PMID[29259747]. PMCID[PMC5733301].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29259738">Mtb PKNA/PKNB Dual Inhibition Provides Selectivity Advantages for Inhibitor Design to Minimize Host Kinase Interactions.</a> Wang, T., G. Bemis, B. Hanzelka, H. Zuccola, M. Wynn, C.S. Moody, J. Green, C. Locher, A. Liu, H. Gao, Y. Xu, S. Wang, J. Wang, Y.L. Bennani, J.A. Thomson, and U. Muh. ACS Medicinal Chemistry Letters, 2017. 8(12): p. 1224-1229. PMID[29259738]. PMCID[PMC5733270].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29148755">Novel Antitubercular 6-Dialkylaminopyrimidine carboxamides from Phenotypic Whole-cell High Throughput Screening of a SoftFocus Library: Structure-Activity Relationship and Target Identification Studies.</a> Wilson, C.R., R.K. Gessner, A. Moosa, R. Seldon, D.F. Warner, V. Mizrahi, C. Soares de Melo, S.B. Simelane, A. Nchinda, E. Abay, D. Taylor, M. Njoroge, C. Brunschwig, N. Lawrence, H.I.M. Boshoff, C.E. Barry, 3rd, F.A. Sirgel, P. van Helden, C.J. Harris, R. Gordon, S. Ghidelli-Disse, H. Pflaumer, M. Boesche, G. Drewes, O. Sanz, G. Santos, M.J. Rebollo-Lopez, B. Urones, C. Selenski, M.J. Lafuente-Monasterio, M. Axtman, J. Lelievre, L. Ballell, R. Mueller, L.J. Street, S.R. Ghorpade, and K. Chibale. Journal of Medicinal Chemistry, 2017. 60(24): p. 10118-10134. PMID[29148755]. PMCID[PMC5748279].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29227930">Synthesis and in Vitro Evaluation of Novel Substituted Isatin-propylene-1H-1,2,3-triazole-4-methylene-moxifloxacin Hybrids for Their Anti-mycobacterial Activities.</a> Yan, X., Z. Lv, J. Wen, S. Zhao, and Z. Xu. European Journal of Medicinal Chemistry, 2018. 143: p. 899-904. PMID[29227930].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <p>21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28870094">Design, Synthesis and Biological Activity of Pyrazinamide Derivatives for anti-Mycobacterium tuberculosis.</a> Zhou, S., S. Yang, and G. Huang. Journal of Enzyme Inhibition and Medicinal Chemistry, 2017. 32(1): p. 1183-1186. PMID[28870094].
    <br />
    <b>[PubMed]</b>. TB_12_2017.</p>

    <h2>Patent Citations</h2>

    <p>This month, no relevant <i>Mycobacterium</i> patents were identified.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
