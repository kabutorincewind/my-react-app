

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-04-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="atLpShHGn/ItcI/Kmbzguea5CpCJLbJn5C6hlO/uxfD/r9UeedAmdbRO4w25uChDnnSq5Ojj7l7AnAJQ/wEMq1/tPq0FSWJiYrET3QSIhVH4CiiTIAYqPB3Im0Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="51C43D6B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">April 1 - April 14, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21479405">Effect of Caffeine on the Multiplication of DNA and RNA Viruses</a>. Murayama, M., K. Tsujimoto, M. Uozaki, Y. Katsuyama, H. Yamasaki, H. Utsunomiya, and A.H. Koyama. Molecular Medicine Reports, 2008. 1(2): p. 251-255; PMID[21479405].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21471320">Generation of Neutralising Aptamers against HSV-2: Potential Components of Multivalent Microbicides</a>. Moore, M.D., D.H. Bunka, M. Forzan, P. Spear, P.G. Stockley, I. McGowan, and W.S. James. The Journal of General Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21471320].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21480515">Two New Lignans and Anti-HBV Constituents from Illicium Henryi</a>. Liu, J.F., Z.Y. Jiang, C.A. Geng, Q. Zhang, Y. Shi, Y.B. Ma, X.M. Zhang, and J.J. Chen. Chemistry &amp; Biodiversity, 2011. 8(4): p. 692-698; PMID[21480515].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21472112">Epigallocatechin Gallate Inhibits HBV DNA Synthesis in a Viral Replication - Inducible Cell Line</a>. He, W., L.X. Li, Q.J. Liao, C.L. Liu, and X.L. Chen. World Journal of Gastroenterology : WJG, 2011. 17(11): p. 1507-1514; PMID[21472112].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>  

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21488067">Antiviral Activity of the Hepatitis C Virus Polymerase Inhibitor Filibuvir in Genotype 1 Infected Patients</a>. Wagner, F., R. Thompson, C. Kantaridis, P. Simpson, P.J. Troke, S. Jagannatha, S. Neelakantan, V.S. Purohit, and J.L. Hammond. Hepatology (Baltimore, Md.), 2011. <b>[Epub ahead of print]</b>; PMID[21488067].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21486696">Syntheses and Initial Evaluation of a Series of Indolo-Fused Heterocyclic Inhibitors of the Polymerase Enzyme (Ns5b) of the Hepatitis C Virus</a>. Zheng, X., T.W. Hudyma, S.W. Martin, C. Bergstrom, M. Ding, F. He, J. Romine, M.A. Poss, J.F. Kadow, C.H. Chang, J. Wan, M.R. Witmer, P. Morin, D.M. Camac, S. Sheriff, B.R. Beno, K.L. Rigat, Y.K. Wang, R. Fridell, J. Lemm, D. Qiu, M. Liu, S. Voss, L. Pelosi, S.B. Roberts, M. Gao, J. Knipe, and R.G. Gentles. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21486696].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21473883">Colony-Forming Assays Reveal Enhanced Suppression of Hepatitis C Virus Replication Using Combinations of Direct-Acting Antivirals</a>. Graham, E.J., R. Hunt, S.M. Shaw, C. Pickford, J. Hammond, M. Westby, and P. Targett-Adams. Journal of Virological Methods, 2011. <b>[Epub ahead of print]</b>; PMID[21473883].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21458278">Fused Heterocyclic Amido Compounds as Anti-Hepatitis C Virus Agents</a>. Aoyama, H., K. Sugita, M. Nakamura, A. Aoyama, M.T. Salim, M. Okamoto, M. Baba, and Y. Hashimoto. Bioorganic &amp; Medicinal Chemistry, 2011. 19(8): p. 2675-2687; PMID[21458278].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21453551">Inhibition of HCV 3a Core Gene through Silymarin and Its Fractions</a>. Ashfaq, U.A., T. Javed, S. Rehman, Z. Nawaz, and S. Riazuddin. Virology Journal, 2011. 8(1): p. 153; PMID[21453551].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21450465">SAR Studies on a Series of N-Benzyl-4-Heteroaryl-1-(Phenylsulfonyl)Piperazine-2-Carboxamides: Potent Inhibitors of the Polymerase Enzyme (Ns5b) of the Hepatitis C Virus</a>. Gentles, R.G., M. Ding, X. Zheng, L. Chupak, M.A. Poss, B.R. Beno, L. Pelosi, M. Liu, J. Lemm, Y.K. Wang, S. Roberts, M. Gao, and J. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21450465].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0401-041411.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288245100031">Synthesis and SAR of Acyclic HCV Ns3 Protease Inhibitors with Novel P4-Benzoxaborole Moieties</a>. Li, X.F., S.M. Zhang, Y.K. Zhang, Y. Liu, C.Z. Ding, Y. Zhou, J.J. Plattner, S.J. Baker, W. Bu, L.A. Liu, W.M. Kazmierski, M.S. Duan, R.M. Grimes, L.L. Wright, G.K. Smith, R.L. Jarvest, J.J. Ji, J.P. Cooper, M.D. Tallant, R.M. Crosby, K. Creech, Z.J. Ni, W.X. Zou, and J. Wright. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(7): p. 2048-2054; ISI[000288245100031].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288401500007">Inhibitors of HCV Ns5a: From Iminothiazolidinones to Symmetrical Stilbenes</a>. Romine, J.L., D.R. St Laurent, J.E. Leet, S.W. Martin, M.H. Serrano-Wu, F.K. Yang, M. Gao, D.R. O&#39;Boyle, J.A. Lemm, J.H. Sun, P.T. Nower, X.H. Huang, M.S. Deshpande, N.A. Meanwell, and L.B. Snyder. ACS Medicinal Chemistry Letters, 2011. 2(3): p. 224-229; ISI[000288401500007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0401-041411.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288401500004">Discovery of Mk-1220: A Macrocyclic Inhibitor of Hepatitis C Virus Ns3/4a Protease with Improved Preclinical Plasma Exposure</a>. Rudd, M.T., J.A. McCauley, J.W. Butcher, J.J. Romano, C.J. McIntyre, K.T. Nguyen, K.F. Gilbert, K.J. Bush, M.K. Holloway, J. Swestock, B.L. Wan, S.S. Carroll, J.M. DiMuzio, D.J. Graham, S.W. Ludmerer, M.W. Stahlhut, C.M. Fandozzi, N. Trainor, D.B. Olsen, J.P. Vacca, and N.J. Liverton. ACS Medicinal Chemistry Letters, 2011. 2(3): p. 207-212; ISI[000288401500004]. <b>[WOS]</b>. <b>OV_0401-041411</b>.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288844500004">QSAR Based Modeling of Hepatitis C Virus Ns5b Inhibitors</a>. Srivastava, A.K., A. Pandey, A. Srivastava, and N. Shukla. Journal of Saudi Chemical Society, 2011. 15(1): p. 25-28; ISI[000288844500004]. <b>[WOS]</b>. <b>OV_0401-041411</b>.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
