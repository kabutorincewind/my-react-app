

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-12-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="a0vg85QOqz1085EkVpxBCil+3q0hoaBZ9JBhB7OWZehUDPvVaCKSL7FPjmfNrMcF8n1ghh/785yG0+1mAgMzTqrkN2J1bgapB5woPhc6KKvFdAuqtPDHZrrDqE8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="28B0687A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: November 21 - December 4, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25209888">Identification of a Novel Antimicrobial Peptide, Scolopendin 1, derived from Centipede Scolopendra subspinipes Mutilans and its Antifungal Mechanism.</a> Choi, H., J.S. Hwang, and D.G. Lee. Insect Molecular Biology, 2014. 23(6): p. 788-799. PMID[25209888].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25360825">Pyrene Schiff Base: Photophysics, Aggregation Induced Emission, and Antimicrobial Properties.</a> Kathiravan, A., K. Sundaravel, M. Jaccob, G. Dhinagaran, A. Rameshkumar, D. Arul Ananth, and T. Sivasudha. The Journal of Physical Chemistry B, 2014. 118(47): p. 13573-13581. PMID[25360825].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24863276">Possible Mechanisms of the Antifungal Activity of Fluconazole in Combination with Terbinafine against Candida albicans.</a> Khodavandi, A., F. Alizadeh, N.A. Vanda, G. Karimi, and P.P. Chong. Pharmaceutical Biology, 2014. 52(12): p. 1505-1509. PMID[24863276].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25288082">Quinacrine Inhibits Candida albicans Growth and Filamentation at Neutral pH.</a> Kulkarny, V.V., A. Chavez-Dozal, H.S. Rane, M. Jahng, S.M. Bernardo, K.J. Parra, and S.A. Lee. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7501-7509. PMID[25288082].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25282420">Antioxidant and Antibacterial Activities of Exopolysaccharides from Bifidobacterium bifidum WBIN03 and Lactobacillus plantarum R315.</a> Li, S., R. Huang, N.P. Shah, X. Tao, Y. Xiong, and H. Wei. Journal of Dairy Science, 2014. 97(12): p. 7334-7343. PMID[25282420].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24946720">Deciphering the Role of the Chitin Synthase Families 1 and 2 in the in Vivo and in Vitro Growth of Aspergillus fumigatus by Multiple Gene Targeting Deletion.</a> Muszkieta, L., V. Aimanianda, E. Mellado, S. Gribaldo, L. Alcazar-Fuoli, E. Szewczyk, M.C. Prevost, and J.P. Latge. Cellular Microbiology, 2014. 16(12): p. 1784-1805. PMID[24946720].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25375978">Gamma-butyrolactone, Cytochalasin, Cyclic Carbonate, Eutypinic acid, and Phenalenone Derivatives from the Soil Fungus Aspergillus Sp. PSU-RSPG185.</a> Rukachaisirikul, V., N. Rungsaiwattana, S. Klaiklay, S. Phongpaichit, K. Borwornwiriyapan, and J. Sakayaroj. Journal of Natural Products, 2014. 77(11): p. 2375-2382. PMID[25375978].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25409186">Alginate Oligosaccharides Inhibit Fungal Cell Growth and Potentiate the Activity of Antifungals Against Candida and Aspergillus spp.</a> Tondervik, A., H. Sletta, G. Klinkenberg, C. Emanuel, L.C. Powell, M.F. Pritchard, S. Khan, K.M. Craine, E. Onsoyen, P.D. Rye, C. Wright, D.W. Thomas, and K.E. Hill. PloS One, 2014. 9(11): p. e112518. PMID[25409186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24677385">Novel-porous-Ag(0) Nanocomposite Hydrogels via Green Process for Advanced Antibacterial Applications.</a> Vimala, K., K. Kanny, K. Varaprasad, N.M. Kumar, and G.S. Reddy. Journal of Biomedical Materials Research Part A, 2014. 102(12): p. 4616-4624. PMID[24677385].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25196996">From Antidiabetic to Antifungal: Discovery of Highly Potent Triazole-thiazolidinedione Hybrids as Novel Antifungal Agents.</a> Wu, S., Y. Zhang, X. He, X. Che, S. Wang, Y. Liu, Y. Jiang, N. Liu, G. Dong, J. Yao, Z. Miao, Y. Wang, W. Zhang, and C. Sheng. ChemMedChem, 2014. 9(12): p. 2639-2646. PMID[25196996].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25429565">Synthesis, Anticancer and Antibacterial Activity of Salinomycin N-Benzyl Amides.</a> Antoszczak, M., E. Maj, A. Napiorkowska, J. Stefanska, E. Augustynowicz-Kopec, J. Wietrzyk, J. Janczak, B. Brzezinski, and A. Huczynski. Molecules, 2014. 19(12): p. 19435-19459. PMID[25429565].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25297523">Synthesis and in Vitro Evaluation of the Antitubercular and Antibacterial Activity of Novel Oxazolidinones Bearing Octahydrocyclopenta[c]pyrrol-2-yl Moieties.</a> Bhattarai, D., J.H. Lee, S.H. Seo, G. Nam, H. Choo, S.B. Kang, J.H. Kwak, T. Oh, S.N. Cho, A.N. Pae, E.E. Kim, N. Jeong, and G. Keum. Chemical &amp; Pharmaceutical Bulletin, 2014. 62(12): p. 1214-1224. PMID[25297523].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25305333">Phosphorus-nitrogen Compounds. Part 29. Syntheses, Crystal Structures, Spectroscopic and Stereogenic Properties, Electrochemical Investigations, Antituberculosis, Antimicrobial and Cytotoxic Activities and DNA Interactions of Ansa-spiro-ansa Cyclotetraphosphazenes.</a> Elmas, G., A. Okumus, L.Y. Koc, H. Soltanzade, Z. Kilic, T. Hokelek, H. Dal, L. Acik, Z. Ustundag, D. Dundar, and M. Yavuz. European Journal of Medicinal Chemistry, 2014. 87: p. 662-676. PMID[25305333].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25350535">Thiol-modified Gold Nanoparticles for the Inhibition of Mycobacterium smegmatis.</a> Gifford, J.C., J. Bresee, C.J. Carter, G. Wang, R.J. Melander, C. Melander, and D.L. Feldheim. Chemical Communications, 2014. 50(100): p. 15860-15863. PMID[25350535].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25429559">Natural Cinnamic acids, Synthetic Derivatives and Hybrids with Antimicrobial Activity.</a> Guzman, J.D. Molecules, 2014. 19(12): p. 19292-19349. PMID[25429559].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25451998">2-Butyl-4-chloroimidazole Based Substituted Piperazine-thiosemicarbazone Hybrids as Potent Inhibitors of Mycobacterium tuberculosis.</a> Jallapally, A., D. Addla, P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(23): p. 5520-5524. PMID[25451998].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25335118">Membrane Active Phenylalanine Conjugated Lipophilic Norspermidine Derivatives with Selective Antibacterial Activity.</a> Konai, M.M., C. Ghosh, V. Yarlagadda, S. Samaddar, and J. Haldar. Journal of Medicinal Chemistry, 2014. 57(22): p. 9409-9423. PMID[25335118].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25261824">Bioactivity of Pyridine-2-thiolato-1-oxide Metal Complexes: Bi(III), Fe(III) and Ga(III) Complexes as Potent Anti-Mycobacterium tuberculosis Prospective Agents.</a> Machado, I., L.B. Marino, B. Demoro, G.A. Echeverria, O.E. Piro, C.Q. Leite, F.R. Pavan, and D. Gambino. European Journal of Medicinal Chemistry, 2014. 87: p. 267-273. PMID[25261824].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25305331">2-Aminothiazole Derivatives as Antimycobacterial Agents: Synthesis, Characterization, in Vitro and in Silico Studies.</a> Makam, P. and T. Kannan. European Journal of Medicinal Chemistry, 2014. 87: p. 643-656. PMID[25305331].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25240097">Design, Synthesis and Antimycobacterial Activity of Various 3-(4(Substitutedsulfonyl)piperazin-1-yl)benzo[d]isoxazole Derivatives.</a> Naidu, K.M., A. Suresh, J. Subbalakshmi, D. Sriram, P. Yogeeswari, P. Raghavaiah, and K.V. Chandra Sekhar. European Journal of Medicinal Chemistry, 2014. 87: p. 71-78. PMID[25240097].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25360691">Efficient Synthesis of Oligosaccharyl 1,2-O-Orthoesters from N-Pentenyl Glycosides and Application to the Pentaarabinofuranoside of the Mycobacterial Cell Surface.</a> Thadke, S.A. and S. Hotha. Organic &amp; Biomolecular Chemistry, 2014. 12(48): p. 9914-9920. PMID[25360691].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25421605">Anti-malarial Activity of a Polyherbal Product (Nefang) During Early and Established Plasmodium Infection in Rodent Models.</a> Arrey Tarkang, P., F.A. Okalebo, L.S. Ayong, G.A. Agbor, and A.N. Guantai. Malaria Journal, 2014. 13(1): p. 456. PMID[25421605].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25375026">Structures and Bioactivities of Dihydrochalcones from Metrodorea stipularis.</a> Burger, M.C., J.B. Fernandes, M.F. da Silva, A. Escalante, J. Prudhomme, K.G. Le Roch, M.A. Izidoro, and P.C. Vieira. Journal of Natural Products, 2014. 77(11): p. 2418-2422. PMID[25375026].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25199554">Antiplasmodial Activity-aided Isolation and Identification of Quercetin-4&#39;-methyl ether in Chromolaena odorata Leaf Fraction with High Activity against Chloroquine-resistant Plasmodium falciparum.</a> Ezenyi, I.C., O.A. Salawu, R. Kulkarni, and M. Emeje. Parasitology Research, 2014. 113(12): p. 4415-4422. PMID[25199554].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25288088">Assessment of the Induction of Dormant Ring Stages in Plasmodium falciparum Parasites by Artemisone and Artemisone Entrapped in Pheroid Vesicles in Vitro.</a> Grobler, L., M. Chavchich, R.K. Haynes, M.D. Edstein, and A.F. Grobler. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7579-7582. PMID[25288088].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br />  

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25282260">Diastereoselective Synthesis of Potent Antimalarial cis-beta-Lactam Agents through a [2 + 2] Cycloaddition of Chiral Imines with a Chiral Ketene.</a> Jarrahpour, A., E. Ebrahimi, V. Sinou, C. Latour, and J.M. Brunel. European Journal of Medicinal Chemistry, 2014. 87: p. 364-371. PMID[25282260].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">27.<a href="http://www.ncbi.nlm.nih.gov/pubmed/25451997">Pentacycloundecylamines and Conjugates Thereof as Chemosensitizers and Reversed Chloroquine Agents.</a> Joubert, J., E.E. Fortuin, D. Taylor, P.J. Smith, and S.F. Malan. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(23): p. 5516-5519. PMID[25451997].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25282267">New Series of Monoamidoxime Derivatives Displaying Versatile Antiparasitic Activity.</a> Tabele, C., A. Cohen, C. Curti, A. Bouhlel, S. Hutter, V. Remusat, N. Primas, T. Terme, N. Azas, and P. Vanelle. European Journal of Medicinal Chemistry, 2014. 87: p. 440-453. PMID[25282267].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25422853">Pyrazoleamide Compounds are Potent Antimalarials That Target Na(+) Homeostasis in Intraerythrocytic Plasmodium falciparum.</a> Vaidya, A.B., J.M. Morrisey, Z. Zhang, S. Das, T.M. Daly, T.D. Otto, N.J. Spillman, M. Wyvratt, P. Siegl, J. Marfurt, G. Wirjanata, B.F. Sebayang, R.N. Price, A. Chatterjee, A. Nagle, M. Stasiak, S.A. Charman, I. Angulo-Barturen, S. Ferrer, M. Belen Jimenez-Diaz, M.S. Martinez, F.J. Gamo, V.M. Avery, A. Ruecker, M. Delves, K. Kirk, M. Berriman, S. Kortagere, J. Burrows, E. Fan, and L.W. Bergman. Nature Communications, 2014. 5: p. 5521. PMID[25422853].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25199781">Role of Pfmdr1 in in Vitro Plasmodium falciparum Susceptibility to Chloroquine, Quinine, Monodesethylamodiaquine, Mefloquine, Lumefantrine, and Dihydroartemisinin.</a> Wurtz, N., B. Fall, A. Pascual, M. Fall, E. Baret, C. Camara, A. Nakoulima, B. Diatta, K.B. Fall, P.S. Mbaye, Y. Dieme, R. Bercion, B. Wade, and B. Pradines. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7032-7040. PMID[25199781].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24890222">Influence of Amodiaquine on the Antimalarial Activity of Ellagic acid: Crystallographic and Biological Studies.</a> Zeslawska, E., B. Oleksyn, A. Fabre, and F. Benoit-Vical. Chemical Biology &amp; Drug Design, 2014. 84(6): p. 669-675. PMID[24890222].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25288090">Triazole-based Compound as a Candidate to Develop Novel Medicines to Treat Toxoplasmosis.</a> Dzitko, K., A. Paneth, T. Plech, J. Pawelczyk, L. Weglinska, and P. Paneth. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7583-7585. PMID[25288090].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1121-120414.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343882400014">Antimicrobial Pyrimidinones II: Synthesis and Antimicrobial Evaluation of Certain Novel 5,6-Disubstituted 2-(substituted amino)alkylthiopyrimidin-4(3H)-ones.</a> Attia, M.I., A.L. Kansoh, and N.R. El-Brollosy. Monatshefte für Chemie, 2014. 145(11): p. 1825-1837. ISI[000343882400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343337700045">Synthesis, Spectroscopic, Anticancer, Antibacterial and Antifungal Studies of Ni(II) and Cu(II) Complexes with Hydrazine Carboxamide, 2-[3-methy1-2-thienyl methylene].</a> Chandra, S., Vandana, and S. Kumar. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2015. 135: p. 356-363. ISI[000343337700045].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343339600017">Synthesis, Characterization, Crystal Structure and Antimicrobial Activity of Copper(II) Complexes with a Thiosemicarbazone Derived from 3-Formyl-6-methylchromone.</a> Hies, D.C., E. Pahontu, S. Shova, R. Georgescu, N. Stanica, R. Olar, A. Gulea, and T. Rosu. Polyhedron, 2014. 81: p. 123-131. ISI[000343339600017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343880700014">Antifungal Potential of Fenugreek Coriander, Mint, Spinach Herbs Extracts against Aspergillus niger and Pseudomonas aeruginosa Phyto-pathogenic Fungi.</a> Jha, Y., R.B. Subramanian, and S. Sahoo. Allelopathy Journal, 2014. 34(2): p. 325-334. ISI[000343880700014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343901400008">Design, Synthesis and Molecular Docking of Substituted 3-Hydrazinyl-3-oxo-propanamides as Anti-tubercular Agents.</a> Naqvi, A., R. Malasoni, A. Srivastava, R.R. Pandey, and A.K. Dwivedi. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(22): p. 5181-5184. ISI[000343901400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343337700126">Synthesis Aspects, Structural, Spectroscopic, Antimicrobial and Room Temperature Ferromagnetism of Zinc Iodide Complex with Schiff Based Ligand.</a> Shakila, K. and S. Kalainathan. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2015. 135: p. 1059-1065. ISI[000343337700126].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343660800074">A Tyrosinase Inhibitor from Aspergillus niger.</a> Vasantha, K.Y., C.S. Murugesh, and A.P. Sattur. Journal of Food Science and Technology-Mysore, 2014. 51(10): p. 2877-2880. ISI[000343660800074].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343687300042">Novel Silicon(IV) Phthalocyanines Containing Piperidinyl Moieties: Synthesis and in Vitro Antifungal Photodynamic Activities.</a> Zheng, B.Y., X.J. Jiang, T. Lin, M.R. Ke, and J.D. Huang. Dyes and Pigments, 2015. 112: p. 311-316. ISI[000343687300042].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1121-120414.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
