

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-331.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fApXcGsOKE63tLEs96ieHym06wp67PqA6dXhfsPVbsP/9La5oj1uopF+trRhVgrIu5lH3hXC6kBB2JwqNJKFJqL+UHluhV5VrGB+YbKd3ilBULssLiAQAW1eLYI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EC486825" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-331-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47894   OI-LS-331; PUBMED-OI-9/19/2005</p>

    <p class="memofmt1-2">          Cell-dependent interference of a series of new 6-aminoquinolone derivatives with viral (HIV/CMV) transactivation</p>

    <p>          Stevens, M, Balzarini, J, Tabarrini, O, Andrei, G, Snoeck, R, Cecchetti, V, Fravolini, A, De, Clercq E, and Pannecouque, C</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16150861&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16150861&amp;dopt=abstract</a> </p><br />

    <p>2.     47895   OI-LS-331; PUBMED-OI-9/19/2005</p>

    <p class="memofmt1-2">          Novel type I interferon IL-28A suppresses hepatitis C viral RNA replication</p>

    <p>          Zhu, H, Butera, M, Nelson, D, and Liu, C</p>

    <p>          Virol J <b>2005</b>.  2(1): 80</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16146571&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16146571&amp;dopt=abstract</a> </p><br />

    <p>3.     47896   OI-LS-331; PUBMED-OI-9/19/2005</p>

    <p class="memofmt1-2">          In vitro and ex vivo antimycobacterial potential of azole drugs against Mycobacterium tuberculosis H(37)Rv</p>

    <p>          Ahmad, Z, Sharma, S, and Khuller, GK</p>

    <p>          FEMS Microbiol Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16143463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16143463&amp;dopt=abstract</a> </p><br />

    <p>4.     47897   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Synthesis, spectroscopic and biological properties of bis(3-arylimidazolidinyl-1)methanes. A novel family of antimicrobial agents</p>

    <p>          Perillo, Isabel, Repetto, Evangelina, Caterina, Maria Cristina, Massa, Rosana, Gutkind, Gabriel, and Salerno, Alejandra</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(8): 811-815</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     47898   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Manufacture of antifungal benzopyran derivative FA200B with Beverwykella and agrochemicals and antifungal agents contg. it</p>

    <p>          Ishihara, Masaru, Kaida, Kenichi, Fudo, Ryosuke, Sumitomo, Atsushi, and Kojika, Hajime</p>

    <p>          PATENT:  JP <b>2005220040</b>  ISSUE DATE:  20050818</p>

    <p>          APPLICATION: 2004-27250  PP: 9 pp.</p>

    <p>          ASSIGNEE:  (Ajinomoto Co., Inc. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     47899   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Manufacture of antifungal hydroquinone derivative FA200A with Beverwykella and agrochemicals and antifungal agents containing it</p>

    <p>          Ishihara, Masaru, Kaita, Kenichi, Fudo, Ryosuke, Komizu, Jun, and Oga, Hajime</p>

    <p>          PATENT:  JP <b>2005220039</b>  ISSUE DATE:  20050818</p>

    <p>          APPLICATION: 2004-27249  PP: 9 pp.</p>

    <p>          ASSIGNEE:  (Ajinomoto Co., Inc. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     47900   OI-LS-331; PUBMED-OI-9/19/2005</p>

    <p class="memofmt1-2">          Viral proteases and phosphorodiesterase inhibitors</p>

    <p>          Agathangelou, P</p>

    <p>          IDrugs <b>1999</b>.  2(6): 523-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127607&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127607&amp;dopt=abstract</a> </p><br />

    <p>8.     47901   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Haloxylines A and B, antifungal and cholinesterase inhibiting piperidine alkaloids from Haloxylon salicornicum</p>

    <p>          Ferheen, Sadia, Ahmed, Ejaz, Afza, Nighat, Malik, Abdul, Shah, Muhammad Raza, Nawaz, Sarfraz Ahmad, and Choudhary, Muhammad Iqbal</p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2005</b>.  53(5): 570-572</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     47902   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Preparation of the periodic protein analogs with antimicrobial activities</p>

    <p>          Strom, Robert M and Brondsema, Philip J</p>

    <p>          PATENT:  US <b>20050187151</b>  ISSUE DATE: 20050825</p>

    <p>          APPLICATION: 2004-64314  PP: 24 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   47903   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Oxazolidinone derivatives as antimicrobials</p>

    <p>          Salman, Mohammad, Biswajit, Das, Rudra, Sonali, Yadav, Ajay Singh, and Rattan, Ashok</p>

    <p>          PATENT:  WO <b>2005082899</b>  ISSUE DATE:  20050909</p>

    <p>          APPLICATION: 2004  PP: 63 pp.</p>

    <p>          ASSIGNEE:  (Ranbaxy Laboratories Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   47904   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Small-molecule inhibition of siderophore biosynthesis in Mycobacterium tuberculosis and Yersinia pestis</p>

    <p>          Ferreras, Julian A, Ryu, Jae-Sang, Di Lello, Federico, Tan, Derek S, and Quadri, Luis EN</p>

    <p>          Nature Chemical Biology <b>2005</b>.  1(1): 29-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   47905   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Facile synthesis of some new azetidinones and acetyl oxadiazoles bearing benzo[b]thiophene nucleus as a potent biological active agent</p>

    <p>          Vasoya, SL, Patel, MR, Dobaria, SV, and Joshi, HS</p>

    <p>          Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry <b>2005</b>.  44B(2): 405-409</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   47906   OI-LS-331; WOS-OI-9/11/2005</p>

    <p class="memofmt1-2">          Ibogaine reduces organ colonization in murine systemic and gastrointestinal Candida albicans infections</p>

    <p>          Yordanov, M, Dimitrova, P, Patkar, S, Falcocchio, S, Xoxi, E, Saso, L, and Ivanovska, N</p>

    <p>          JOURNAL OF MEDICAL MICROBIOLOGY <b>2005</b>.  54(7): 647-653, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231445900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231445900005</a> </p><br />

    <p>14.   47907   OI-LS-331; WOS-OI-9/11/2005</p>

    <p class="memofmt1-2">          CCC in the phytochemical analysis of anti-tuberculosis ethnobotanicals</p>

    <p>          Inui, T, Case, R, Chou, E, Soejarto, DD, Fong, HHS, Franzblau, SG, Smith, DC, and Pauli, GF</p>

    <p>          JOURNAL OF LIQUID CHROMATOGRAPHY &amp; RELATED TECHNOLOGIES <b>2005</b>.  28(12-13): 2017-2028, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231270000020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231270000020</a> </p><br />

    <p>15.   47908   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Synthesis and Antimycobacterial Activity of Triazene Derivatives of N-Arylcarbamates</p>

    <p>          Velikorodov, AV, Urlyapova, NG, and Daudova, AD</p>

    <p>          Pharmaceutical Chemistry Journal <b>2005</b>.  39(3): 126-128</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   47909   OI-LS-331; WOS-OI-9/11/2005</p>

    <p class="memofmt1-2">          Opportunistic infections (OI) following monoclonal antibody treatment</p>

    <p>          Cornel, OA, Heidecke, CN, and Karthaus, M</p>

    <p>          JOURNAL OF CLINICAL ONCOLOGY <b>2005</b>.  23(16): 181S-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230326601138">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230326601138</a> </p><br />

    <p>17.   47910   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Heterocyclic isosters of antimycobacterial 5-methylsalicylanilides with other heterocyclic moiety as pyridine</p>

    <p>          Waisser, Karel, Drazkova, Katerina, Matyk, Josef, Kunes, Jiri, and Kaustova, Jarmila</p>

    <p>          Folia Pharmaceutica Universitatis Carolinae <b>2005</b>.  31-32: 47-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   47911   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Heterocyclic isosters of antimycobacterial sanicylanilides: N-pyridyl-salicylamide with steric hindrance of amide group</p>

    <p>          Waisser, Karel, Drazkova, Katerina, Matyk, Josef, Kunes, Jiri, and Kaustova, Jarmila</p>

    <p>          Folia Pharmaceutica Universitatis Carolinae <b>2005</b>.  31-32: 41-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>19.   47912   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          New small-molecule synthetic antimycobacterials</p>

    <p>          Ballell, Lluis, Field, Robert A, Duncan, Ken, and Young, Robert J</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(6): 2153-2163</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   47913   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Murine cytomegalovirus resistant to antivirals has genetic correlates with human cytomegalovirus</p>

    <p>          Scott, GM, Ng, H-L, Morton, CJ, Parker, MW, and Rawlinson, WD</p>

    <p>          Journal of General Virology <b>2005</b>.  86(8): 2141-2151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   47914   OI-LS-331; WOS-OI-9/11/2005</p>

    <p class="memofmt1-2">          CpG oligonucleotides partially inhibit growth of Mycobacterium tuberculosis, but not Salmonella or Listeria, in human monocyte-derived macrophages</p>

    <p>          Wang, JP, Hayashi, T, Datta, SK, Kornbluth, RS, Raz, E, and Guiney, DG</p>

    <p>          FEMS IMMUNOLOGY AND MEDICAL MICROBIOLOGY <b>2005</b>.  45(2): 303-310, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231306000024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231306000024</a> </p><br />

    <p>22.   47915   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Antiviral drugs: Triphosphates of nucleoside analogues active as antiviral drugs</p>

    <p>          De Clercq, Erik</p>

    <p>          Nucleoside Triphosphates and Their Analogs <b>2005</b>.: 329-341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   47916   OI-LS-331; WOS-OI-9/11/2005</p>

    <p class="memofmt1-2">          The search for new DHFR inhibitors: a review of patents, January 2001 February 2005</p>

    <p>          da Cunha, EFF, Ramalho, TC, Mala, ER, and de Alencastro, RB</p>

    <p>          EXPERT OPINION ON THERAPEUTIC PATENTS <b>2005</b>.  15(8): 967-986, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231340800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231340800003</a> </p><br />

    <p>24.   47917   OI-LS-331; WOS-OI-9/11/2005</p>

    <p class="memofmt1-2">          Patents and development of HBV and HCV clinical treatment: from 2001 to April 2005</p>

    <p>          Chen, Z and Zheng, M</p>

    <p>          EXPERT OPINION ON THERAPEUTIC PATENTS <b>2005</b>.  15(8): 1027-1039, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231340800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231340800007</a> </p><br />

    <p>25.   47918   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Design of translactam HCMV protease inhibitors as potent antivirals</p>

    <p>          Borthwick, Alan D</p>

    <p>          Medicinal Research Reviews <b>2005</b>.  25(4): 427-452</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   47919   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Acyclic 3-[(2-Hydroxyethoxy)methyl] Analogues of Antiviral Furo- and Pyrrolo[2,3-d]pyrimidine Nucleosides</p>

    <p>          Janeba, Zlatko, Balzarini, Jan, Andrei, Graciela, Snoeck, Robert, De Clercq, Erik, and Robins, Morris J</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(14): 4690-4696</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>27.   47920   OI-LS-331; SCIFINDER-OI-9/15/2005</p>

    <p class="memofmt1-2">          Preparation of imidazo[4,5-c]pyridine derivatives as antiviral agents</p>

    <p>          Puerstinger, Gerhard, Bondy, Steven S, Dowdy, Eric Davis, Kim, Choung U, Oare, David A, Neyts, Johan, and Zia, Vahid</p>

    <p>          PATENT:  WO <b>2005063744</b>  ISSUE DATE:  20050714</p>

    <p>          APPLICATION: 2004  PP: 265 pp.</p>

    <p>          ASSIGNEE:  (K. U. Leuven Research &amp; Development, Belg. and Gilead Sciences, Inc.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   47921   OI-LS-331; WOS-OI-9/11/2005</p>

    <p class="memofmt1-2">          In vitro antifungal activity of ZJ-522, a new triazole restructured from fluconazole and butenatine, against clinically important fungi in comparison with fluconazole and butenatine</p>

    <p>          Gao, PH, Cao, YB, Xu, Z, Zhang, JD, Zhang, WN, Wang, Y, Gu, J, Cao, YY, Li, RY, Jia, XM, and Jiang, YY</p>

    <p>          BIOLOGICAL &amp; PHARMACEUTICAL BULLETIN <b>2005</b>.  28(8): 1414-1417, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231343500015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231343500015</a> </p><br />

    <p>29.   47922   OI-LS-331; WOS-OI-9/18/2005</p>

    <p class="memofmt1-2">          Antifungal activity of artemisinin derivatives</p>

    <p>          Galal, AM, Ross, SA, Jacob, M, and ElSohly, MA</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2005</b>.  68(8): 1274-1276, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231578000028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231578000028</a> </p><br />

    <p>30.   47923   OI-LS-331; WOS-OI-9/18/2005</p>

    <p class="memofmt1-2">          Aspirochlorine class compounds from Aspergillus flavus inhibit azole-resistant Candida albicans</p>

    <p>          Klausmeyer, P, McCloud, TG, Tucker, KD, Cardellina, JH, and Shoemaker, RH</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2005</b>.  68(8): 1300-1302, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231578000036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231578000036</a> </p><br />

    <p>31.   47924   OI-LS-331; WOS-OI-9/18/2005</p>

    <p class="memofmt1-2">          Significant increases in the levels of liver enzymes in mice treated with anti-tuberculosis drugs</p>

    <p>          Lenaerts, AJ, Johnson, CM, Marrieta, KS, Gruppo, V, and Orme, IM</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2005</b>.  26(2): 152-158, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231546100009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231546100009</a> </p><br />

    <p>32.   47925   OI-LS-331; WOS-OI-9/18/2005</p>

    <p class="memofmt1-2">          The therapeutic potential of NS3 protease inhibitors in HCV infection</p>

    <p>          Goudreau, N and Llinas-Brunet, M</p>

    <p>          EXPERT OPINION ON INVESTIGATIONAL DRUGS <b>2005</b>.  14(9): 1129-1144, 16</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231618100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231618100005</a> </p><br />

    <p>33.   47926   OI-LS-331; WOS-OI-9/18/2005</p>

    <p class="memofmt1-2">          A note on the antitubercular activities of 1-aryl-5-benzylsulfanyltetrazoles</p>

    <p>          Adamec, J, Waisser, K, Kunes, J, and Kaustova, J</p>

    <p>          ARCHIV DER PHARMAZIE <b>2005</b>.  338(8): 385-389, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231549700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231549700006</a> </p><br />
    <br clear="all">

    <p>34.   47927   OI-LS-331; WOS-OI-9/18/2005</p>

    <p class="memofmt1-2">          Genomic approach to identifying the putative target of and mechanisms of resistance to mefloquine in mycobacteria</p>

    <p>          Danelishvili, L, Wu, M, Young, LS, and Bermudez, LE</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(9): 3707-3714, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231542900017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231542900017</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
