

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-70.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6S0qJ779YKFIZDSR1t1hoxJcG3/4hG3HmsnqpX0bc1/SlStpvPuaw+gq9ftC3TCE06innMIzDN1ICpvchmFim1ZVfk+Q7QKZNICpxoAEQHTdFxoiz6nUPphV0ek=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EF543CEA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-70-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    6142   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Specific Inhibition of Type I Interferon Signal Transduction by Respiratory Syncytial Virus</b></p>

<p>           Ramaswamy, M, Shi, L, Monick, MM, Hunninghake, GW, and  Look, DC</p>

<p>           American Journal of Respiratory Cell and Molecular Biology 2004. 30(6): 893-900</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     2.    6143   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Inhibition of SARS-coronavirus infection in vitro by S-nitroso-N-acetylpenicillamine, a nitric oxide donor compound</p>

<p>           Keyaerts, E, Vijgen, L, Chen, L, Maes, P, Hedenstierna, G, and Van, Ranst M</p>

<p>           Int J Infect Dis 2004. 8(4): 223-226</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15234326&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15234326&amp;dopt=abstract</a> </p><br />

<p>     3.    6144   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Inhibition of human cytomegalovirus signaling and replication by the immunosuppressant FK778</p>

<p>           Evers, David L, Wang, Xin, Huong, Shu-Mei, Andreoni, Kenneth A, and Huang, Eng-Shang</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4CGNBJ8-1/2/d49270fba8e338682b914b690533edbf">http://www.sciencedirect.com/science/article/B6T2H-4CGNBJ8-1/2/d49270fba8e338682b914b690533edbf</a> </p><br />

<p>     4.    6145   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Differential Antiviral Response of Endothelial Cells After Infection With Pathogenic and Nonpathogenic Hantaviruses</b></p>

<p>           Kraus, AA, Raftery, MJ, Giese, T, Ulrich, R, Zawatzky, R, Hippenstiel, S, Suttorp, N, Kruger, DH, and Schonrich, G</p>

<p>           Journal of Virology 2004. 78(12): 6143-6150</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     5.    6146   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Mechanism of the interferon alpha response against hepatitis C virus replicons</p>

<p>           Guo, JT, Sohn, JA, Zhu, Q, and Seeger, C</p>

<p>           Virology 2004. 325(1): 71-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231387&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231387&amp;dopt=abstract</a> </p><br />

<p>     6.    6147   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Inhibition of papilloma progression by antisense oligonucleotides targeted to HPV11 E6/E7 RNA</p>

<p>           Clawson, GA, Miranda, GQ, Sivarajah, A, Xin, P, Pan, W, Thiboutot, D, and Christensen, ND</p>

<p>           Gene Ther 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15229628&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15229628&amp;dopt=abstract</a> </p><br />

<p>     7.    6148   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Antiviral Activity on Hantavirus and Apoptosis of Vero Cells of Natural and Semi-Synthetic Compounds From Heliotropium Filifolium Resin.</b></p>

<p>           Modak, B, Galeno, H, and Torres, R</p>

<p>           Journal of the Chilean Chemical Society 2004. 49(2): 143-145</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    8.     6149   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>5-Fluorouracil or Gemcitabine Combined With Adenoviral-Mediated Reintroduction of P16(Ink4a) Greatly Enhanced Cytotoxicity in Panc-1 Pancreatic Adenocarcinoma Cells</b></p>

<p>           Halloran, CM, Ghaneh, P, Shore, S , Greenhalf, W, Zumstein, L, Wilson, D, Neoptolemos, JP, and Costello, E</p>

<p>           Journal of Gene Medicine 2004. 6(5): 514-525</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     9.    6150   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Small molecules targeting severe acute respiratory syndrome human coronavirus</p>

<p>           Wu, CY, Jan, JT, Ma, SH, Kuo, CJ, Juan, HF, Cheng, YS, Hsu, HH, Huang, HC, Wu, D, Brik, A, Liang, FS, Liu, RS, Fang, JM, Chen, ST, Liang, PH, and Wong, CH</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15226499&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15226499&amp;dopt=abstract</a> </p><br />

<p>   10.    6151   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Interferon Alpha-2b Inhibits Negative-Strand Rna and Protein Expression From Full-Length Hcv1a Infectious Clone</b></p>

<p>           Prabhu, R, Joshi, V, Garry, RF, Bastian, F, Haque, S, Regenstein, F, Thung, S, and Dash, S</p>

<p>           Experimental and Molecular Pathology 2004. 76(3): 242-252</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   11.    6152   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Exploring the pathogenesis of severe acute respiratory syndrome (SARS): the tissue distribution of the coronavirus (SARS-CoV) and its putative receptor, angiotensin-converting enzyme 2 (ACE2)</p>

<p>           To, K and Lo, AW</p>

<p>           J Pathol 2004. 203(3): 740-743</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15221932&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15221932&amp;dopt=abstract</a> </p><br />

<p>   12.    6153   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Combination therapy with amantadine and interferon in naive patients with chronic hepatitis C: meta-analysis of individual patient data from six clinical trials</p>

<p>           Mangia, Alessandra, Leandro, Gioacchino, Helbling, Beat, Renner, Eberhard L, Tabone, Marco, Sidoli, Laura, Caronia, Simona, Foster, Graham R, Zeuzem, Stephan, and Berg, Thomas</p>

<p>           Journal of Hepatology 2004. 40(3): 478-483</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4BRRC3V-N/2/3d5f68ee278d7411979d8a55663e0741">http://www.sciencedirect.com/science/article/B6W7C-4BRRC3V-N/2/3d5f68ee278d7411979d8a55663e0741</a> </p><br />

<p>   13.    6154   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Human cytomegalovirus carries a cell-derived phospholipase A2 required for infectivity</p>

<p>           Allal, C, Buisson-Brenac, C, Marion, V, Claudel-Renard, C, Faraut, T, Dal, Monte P, Streblow, D, Record, M, and Davignon, JL</p>

<p>           J Virol 2004. 78(14): 7717-26</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220446&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220446&amp;dopt=abstract</a> </p><br />

<p>  14.     6155   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Inhibition of Severe Acute Respiratory Syndrome Virus Replication by Small Interfering RNAs in Mammalian Cells</p>

<p>           Wang, Z, Ren, L, Zhao, X, Hung, T, Meng, A, Wang, J, and Chen, YG</p>

<p>           J Virol 2004. 78(14): 7523-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220426&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220426&amp;dopt=abstract</a> </p><br />

<p>   15.    6156   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Tenofovir disoproxil fumarate : clinical pharmacology and pharmacokinetics</p>

<p>           Kearney, BP, Flaherty, JF, and Shah, J</p>

<p>           Clin Pharmacokinet 2004. 43(9): 595-612</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217303&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217303&amp;dopt=abstract</a> </p><br />

<p>   16.    6157   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>West Nile Encephalitis and Myelitis</b></p>

<p>           Roos, KL</p>

<p>           Current Opinion in Neurology 2004. 17(3): 343-346</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   17.    6158   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Rhesus cytomegalovirus is similar to human cytomegalovirus in susceptibility to benzimidazole nucleosides</p>

<p>           North, TW, Sequar, G, Townsend, LB, Drach, JC, and Barry, PA</p>

<p>           Antimicrob Agents Chemother 2004.  48(7): 2760-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215146&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215146&amp;dopt=abstract</a> </p><br />

<p>   18.    6159   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Inhibition of severe acute respiratory syndrome coronavirus replication by niclosamide</p>

<p>           Wu, CJ, Jan, JT, Chen, CM, Hsieh, HP, Hwang, DR, Liu, HW, Liu, CY, Huang, HW, Chen, SC, Hong, CF, Lin, RK, Chao, YS, and Hsu, JT</p>

<p>           Antimicrob Agents Chemother 2004.  48(7): 2693-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215127&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215127&amp;dopt=abstract</a> </p><br />

<p>   19.    6160   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Evaluation of the Virucidal Efficacy of Two Chemical Disinfectants Against Avian Influenza Virus a at Different Temperatures</b></p>

<p>           Yilmaz, A, Heffels-Redmann, U, and Redmann, T</p>

<p>           Archiv Fur Geflugelkunde 2004. 68(2): 50-56</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   20.    6161   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Oral efficacy of a respiratory syncytial virus inhibitor in rodent models of infection</p>

<p>           Cianci, C, Genovesi, EV, Lamb, L, Medina, I, Yang, Z, Zadjura, L, Yang, H, D&#39;Arienzo, C, Sin, N, Yu, KL, Combrink, K, Li, Z, Colonno, R, Meanwell, N, Clark, J, and Krystal, M</p>

<p>           Antimicrob Agents Chemother 2004.  48(7): 2448-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215093&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215093&amp;dopt=abstract</a> </p><br />

<p>   21.    6162   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           The role of target membrane sialic acid residues in the fusion activity of the influenza virus: the effect of two types of ganglioside on the kinetics of membrane merging</p>

<p>           Ramalho-Santos, J and Pedroso, De Lima MC</p>

<p>           Cell Mol Biol Lett 2004. 9(2): 337-351</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15213813&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15213813&amp;dopt=abstract</a> </p><br />

<p>   22.    6163   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Anti-Hepatitis B Virus Activity of Ori-9020, a Novel Phosphorothioate Dinucleotide, in a Transgenic Mouse Model</b></p>

<p>           Iyer, RP, Roland, A, Jin, Y, Mounir, S, Korba, B, Julander, JG, and Morrey, JD</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(6): 2318-2320</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   23.    6164   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Cytotoxicity and cellular uptake of pyrimidine nucleosides for imaging herpes simplex type-1 thymidine kinase (HSV-1 TK) expression in mammalian cells</p>

<p>           Morin, Kevin W, Duan, Weili, Xu, Lihua, Zhou, Aihua, Moharram, Sameh, Knaus, Edward E, McEwan, Alexander JB, and Wiebe, Leonard I</p>

<p>           Nuclear Medicine and Biology 2004.  31(5): 623-630</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T9Y-4CP78X7-D/2/f729122d8aee61e31021c3cd691a5199">http://www.sciencedirect.com/science/article/B6T9Y-4CP78X7-D/2/f729122d8aee61e31021c3cd691a5199</a> </p><br />

<p>   24.    6165   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Emergence of resistance to carbocyclic oxetanocin G in herpes simplex virus type 1 and genetic analysis of resistant mutants</p>

<p>           Hu, N and Shiota, H</p>

<p>           Acta Pharmacol Sin 2004. 25(7): 921-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15210066&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15210066&amp;dopt=abstract</a> </p><br />

<p>   25.    6166   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>In Vitro Delivery of Novel, Highly Potent Anti-Varicella Zoster Virus Nucleoside Analogues to Their Target Site in the Skin</b></p>

<p>           Jarvis, CA, Mcguigan, C, and Heard, CM</p>

<p>           Pharmaceutical Research 2004. 21(6): 914-919</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   26.    6167   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           4&#39;-[alpha]-C-BranchedN,O-nucleosides: synthesis and biological properties</p>

<p>           Chiacchio, Ugo, Genovese, Filippo, Iannazzo, Daniela, Piperno, Anna, Quadrelli, Paolo, Antonino, Corsaro, Romeo, Roberto, Valveri, Vincenza, and Mastino, Antonio</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(14): 3903-3909</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4CHJ5YY-4/2/2f5d9b428cacd356afc48c03215dc242">http://www.sciencedirect.com/science/article/B6TF8-4CHJ5YY-4/2/2f5d9b428cacd356afc48c03215dc242</a> </p><br />

<p>  27.     6168   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           A novel pattern (sW195a) in surface gene of HBV DNA due to YSDD (L180M plus M204S) mutation selected during lamivudine therapy and successful treatment with adefovir dipivoxil</p>

<p>           Bozdayi, AMithat, Eyigun, Can Polat, Turkyilmaz, Ahmet R, Avci, Ismail Yasar, Pahsa, Alaaddin, and Yurdaydin, Cihan</p>

<p>           Journal of Clinical Virology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4CMJGS5-2/2/bc98b5b0c01665e9f123d01bb71d1322">http://www.sciencedirect.com/science/article/B6VJV-4CMJGS5-2/2/bc98b5b0c01665e9f123d01bb71d1322</a> </p><br />

<p>   28.    6169   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           What is disrupting IFN-[alpha]&#39;s antiviral activity?</p>

<p>           Mbow, MLamine and Sarisky, Robert T</p>

<p>           Trends in Biotechnology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCW-4CPVMT4-3/2/c15f5d6f7b90c7ebf2677417f418ab80">http://www.sciencedirect.com/science/article/B6TCW-4CPVMT4-3/2/c15f5d6f7b90c7ebf2677417f418ab80</a> </p><br />

<p>   29.    6170   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Antivirals and Antibiotics for Influenza in the United States, 1995-2001.</b></p>

<p>           Linder, JA and Bates, DW</p>

<p>           Journal of General Internal Medicine 2004. 19: 119</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   30.    6171   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Hepatitis C and treatment with pegylated interferon and ribavirin</p>

<p>           Gotto, J and Dusheiko, GM</p>

<p>           Int J Biochem Cell Biol 2004. 36(10):  1874-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15203100&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15203100&amp;dopt=abstract</a> </p><br />

<p>   31.    6172   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Emerging Infectious Diseases: Dengue-Fever, West-Nile-Fever, Sars, Avian Influenza, Hiv</b></p>

<p>           Haas, W, Krause, G, Marcus, U, Stark, K, Ammon, A, and Burger, R</p>

<p>           Internist 2004. 45(6): 684-+</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   32.    6173   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           RNA interference as a new strategy against viral hepatitis</p>

<p>           Radhakrishnan, SK, Layden, TJ, and Gartel, AL</p>

<p>           Virology 2004. 323(2): 173-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193913&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193913&amp;dopt=abstract</a> </p><br />

<p>   33.    6174   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Vitro Antioxidant, Antimicrobial, and Antiviral Activities of the Essential Oil and Various Extracts From Herbal Parts and Callus Cultures of Origanum Acutidens</b></p>

<p>           Sokmen, M, Serkedjieva, J, Daferera, D, Gulluce, M, Polissiou, M, Tepe, B, Akpulat, HA, Sahin, F, and Sokmen, A</p>

<p>           Journal of Agricultural and Food Chemistry 2004. 52(11): 3309-3312</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  34.     6175   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Synthesis and Antiviral Activity of Some Novel Substituted Phenothiazines</b></p>

<p>           Pandey, VK, Saxena, SK, Joshi, MN, and Bajpai, SK</p>

<p>           Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry 2004. 43(5): 1015-1017</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   35.    6176   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           R-118958, a unique anti-influenza agent showing high efficacy for both prophylaxis and treatment after a single administration: from the in vitro stage to phase I study</p>

<p>           Yamashita, Makoto</p>

<p>           International Congress Series 2004.  1263: 38-42</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B7581-4CRPC4H-C/2/cb8728fd8880d987e5215db6d426bfef">http://www.sciencedirect.com/science/article/B7581-4CRPC4H-C/2/cb8728fd8880d987e5215db6d426bfef</a> </p><br />

<p>   36.    6177   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Synthesis and Antiviral Activity of Dimeric Capsid-Binding Inhibitors of Human Rhinovirus (Hrv)</b></p>

<p>           Krippner, GY, Chalmers, DK, Stanislawski, PC, Tucker, SP, and Watson, KG</p>

<p>           Australian Journal of Chemistry 2004. 57(6): 553-564</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   37.    6178   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>A Benzoic Acid Inhibitor Induces a Novel Conformational Change in the Active Site of Influenza B Virus Neurarninidase</b></p>

<p>           Lommer, BS, Ali, SM, Bajpai, SN, Brouillette, WJ, Air, GM, and Luo, M</p>

<p>           Acta Crystallographica Section D-Biological Crystallography 2004. 60: 1017-1023</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   38.    6179   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Change in receptor-binding specificity of recent human influenza A viruses (H3N2) affects recognition of the receptor on MDCK cells</p>

<p>           Nobusawa, E, Nakajima, K, Kozuka, S, and Ishihara, H</p>

<p>           International Congress Series 2004.  1263: 472-475</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B7581-4CRPC4H-3X/2/8aeed1b240ca5793ae675c658b74e648">http://www.sciencedirect.com/science/article/B7581-4CRPC4H-3X/2/8aeed1b240ca5793ae675c658b74e648</a> </p><br />

<p>   39.    6180   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Novel imidazo[1,2-c]pyrimidine base-modified nucleosides: synthesis and antiviral evaluation</p>

<p>           Kifli, Nurolaini, De Clercq, Erik, Balzarini, Jan, and Simons, Claire</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4CMHR6K-2/2/e3a4c67f486504635791ddcb26c6df53">http://www.sciencedirect.com/science/article/B6TF8-4CMHR6K-2/2/e3a4c67f486504635791ddcb26c6df53</a> </p><br />

<p>   40.    6181   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Regulation of Semliki Forest virus RNA replication: a model for the control of alphavirus pathogenesis in invertebrate hosts</p>

<p>           Hwang Kim, Kyongmin, Rumenapf, Tillmann, Strauss, Ellen G, and Strauss, James H</p>

<p>           Virology 2004. 323(1): 153-163</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WXR-4C7VXCD-7/2/f357d70b052c5ba94acad56155b47e77">http://www.sciencedirect.com/science/article/B6WXR-4C7VXCD-7/2/f357d70b052c5ba94acad56155b47e77</a> </p><br />

<p>  41.     6182   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Natural products and combinatorial chemistry: back to the future</p>

<p>           Ortholand, Jean-Yves and Ganesan, A</p>

<p>           Current Opinion in Chemical Biology 2004. 8(3): 271-280</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VRX-4CB05GS-6/2/4eb741e16f9d707a5fec5bda900dfc29">http://www.sciencedirect.com/science/article/B6VRX-4CB05GS-6/2/4eb741e16f9d707a5fec5bda900dfc29</a> </p><br />

<p>   42.    6183   DMID-LS-70; PUBMED-DMID-7/7/2004</p>

<p class="memofmt1-3">           Natural medicine: the genus Angelica</p>

<p>           Sarker, SD and Nahar, L</p>

<p>           Curr Med Chem  2004. 11(11): 1479-500</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180579&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180579&amp;dopt=abstract</a> </p><br />

<p>   43.    6184   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Mechanism of Action at the Molecular Level of the Antiviral Drug 3(2h)-Isoflavene Against Type 2 Poliovirus</b></p>

<p>           Salvati, AL,  De Dominicis, A, Tait, S , Canitano, A, Lahm, A, and Fiore, L</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(6): 2233-2243</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   44.    6185   DMID-LS-70; EMBASE-DMID-7/6/2004</p>

<p class="memofmt1-3">           Synthesis and antiproliferative activity of 3-aryl-2-[1H(2H)-benzotriazol-1(2)-yl]acrylonitriles variously substituted: Part 4</p>

<p>           Carta, Antonio, Palomba, Michele, Boatto, Gianpiero, Busonera, Bernardetta, Murreddu, Marta, and Loddo, Roberta</p>

<p>           Il Farmaco 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJ8-4C6KX34-5/2/4cae06278570de98f35ce42ae17ae8a6">http://www.sciencedirect.com/science/article/B6VJ8-4C6KX34-5/2/4cae06278570de98f35ce42ae17ae8a6</a> </p><br />

<p>   45.    6186   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Inhibition of Reovirus by Mycophenolic Acid Is Associated With the M1 Genome Segment</b></p>

<p>           Hermann, LL and Coombs, KM</p>

<p>           Journal of Virology 2004. 78(12): 6171-6179</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   46.    6187   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Microarray Profiling of Antiviral Antibodies for the Development of Diagnostics, Vaccines, and Therapeutics</b></p>

<p>           De Vegvar, HEN and Robinson, WH</p>

<p>           Clinical Immunology 2004. 111(2): 196-201</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   47.    6188   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Antisense Morpholino-Oligomers Directed Against the 5 &#39; End of the Genome Inhibit Coronavirus Proliferation and Growth</b></p>

<p>           Neuman, BW, Stein, DA, Kroeker, AD, Paulino, AD, Moulton, HM, Iversen, PL, and Buchmeier, MJ</p>

<p>           Journal of Virology 2004. 78(11): 5891-5899</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  48.     6189   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>Cleavage Inhibition of the Murine Coronavirus Spike Protein by a Furin-Like Enzyme Affects Cell-Cell but Not Virus-Cell Fusion</b></p>

<p>           De Haan, CAM, Stadler, K, Godeke, GJ, Bosch, BJ, and Rottier, PJM</p>

<p>           Journal of Virology 2004. 78(11):  6048-6054</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   49.    6190   DMID-LS-70; WOS-DMID-7/6/2004</p>

<p>           <b>A Study on the Antipicornavirus Activity of Flavonoid Compounds (Flavones) by Using Quantum Chemical and Chemometric Methods</b></p>

<p>           Souza, J, Molfetta, FA, Honorio, KM, Santos, RHA, and Da Silva, ABF</p>

<p>           Journal of Chemical Information and Computer Sciences 2004. 44(3): 1153-1161</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
