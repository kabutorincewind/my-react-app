

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-02-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="y2nY7ABg7D8vBBJ8rYmVHcGSuWlLONWmM+rmkncZy6h1imev1ARvKnGpb9UDJwReT+lXzzYJjIHgNBBd1V9awVYtpbGs3bfuvF7xVRiaqT7sr2JeDbwPiu9uN6Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3623563B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">January 20 - February 2, 2012</h1>

    <p> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22264474">Synthesis, Characterization and Biological Activity of a Niobium-substituted-heteropolytungstate on Hepatitis B Virus.</a> Zhang, H., Y. Qi, Y. Ding, J. Wang, Q. Li, J. Zhang, Y. Jiang, X. Chi, J. Li, and J. Niu, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22264474].</p>

    <p class="plaintext">[PubMed]. OV_0120-020212.</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22297745">Synthesis and Antiviral Evaluation of 7-O-Arylmethylquercetin Derivatives against SARS-associated Coronavirus (SCV) and Hepatitis C Virus (HCV).</a> Park, H.R., H. Yoon, M.K. Kim, S.D. Lee, and Y. Chong, Archives of Pharmacal Research, 2012. 35(1): p. 77-85; PMID[22297745].</p>

    <p class="plaintext">[PubMed]. OV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22293206">Fluoroquinolones Inhibit HCV by Targeting Its Helicase.</a> Khan, I.A., S. Siddiqui, S. Rehmani, S.U. Kazmi, and S.H. Ali, Antiviral Therapy, 2011. <b>[Epub ahead of print]</b>; PMID[22293206].</p>

    <p class="plaintext">[PubMed]. OV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22280819">Inhibition of Hepatitis C Virus NS5B Polymerase by S-Trityl-l-cysteine Derivatives.</a> Nichols, D.B., G. Fournet, K.R. Gurukumar, A. Basu, J.C. Lee, N. Sakamoto, F. Kozielski, I. Musmuca, B. Joseph, R. Ragno, and N. Kaushik-Basu, European Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22280819].</p>

    <p class="plaintext">[PubMed]. OV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22279172">Evaluation of ITX 5061, a Scavenger Receptor B1 Antagonist: Resistance Selection and Activity in Combination With Other Hepatitis C Virus Antivirals.</a> Zhu, H., F. Wong-Staal, H. Lee, A. Syder, J. McKelvy, R.T. Schooley, and D.L. Wyles, The Journal of Infectious Diseases, 2012. 205(4): p. 656-662; PMID[22279172].</p>

    <p class="plaintext">[PubMed]. OV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22263860">Insight into the Structural Requirements of Narlaprevir-type Inhibitors of NS3/NS4A Protease Based on HQSAR and Molecular Field Analyses.</a> Zhu, J., Y. Li, H. Yu, L. Zhang, X. Mao, and T. Hou, Combinatorial Chemistry &amp; High Throughput Screening, 2012. <b>[Epub ahead of print]</b>; PMID[22263860].</p>

    <p class="plaintext">[PubMed]. OV_0120-020212.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298484500012">In Vitro Selection and Characterization of HCV Replicons Resistant to Multiple Non-nucleoside Polymerase Inhibitors.</a> Delang, L., I. Vliegen, P. Leyssen, and J. Neyts, Journal of Hepatology, 2012. 56(1): p. 41-48; ISI[000298484500012].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298498000004">1,3,4-Oxadiazole: A Privileged Structure in Antiviral Agents.</a> Li, Z., P. Zhan, and X. Liu, Mini-Reviews in Medicinal Chemistry, 2011. 11(13): p. 1130-1142; ISI[000298498000004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298305400051">Inhibition on Hepatitis B Virus E-gene Expression of 10-23 DNAzyme Delivered by Novel Chitosan Oligosaccharide-stearic acid Micelles.</a> Miao, J., X.G. Zhang, Y. Hong, Y.F. Rao, Q. Li, X.J. Xie, J.E. Wo, and M.W. Li, Carbohydrate Polymers, 2012. 87(2): p. 1342-1347; ISI[000298305400051]. <b>[WOS]</b>. OV_0120-020212.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
