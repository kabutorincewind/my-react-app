

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-303.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gW+EFliYQD8zh1xgVlZPH4KV1C+ia0f/S6739GZYSj4z+pnhIjRuBTP0Pu9qABT4XJFa09DJFg1Tf9xKiuVA0HGYA7ngmF6XN+5wuhWdQSaQrjCZvHazoM9/sK8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9A4ACE6F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-303-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>     1.    43948   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Comparison of Ternary Complexes of Pneumocystis carinii and Wild-Type Human Dihydrofolate Reductase With a Novel Classical Antitumor Furo[2,3-d]pyrimidine Antifolate</p>

    <p>           Cody, V</p>

    <p>           Acta Crystallogr D Biol Crystallogr 97. 53(Pt 6): 638-649</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299851&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299851&amp;dopt=abstract</a> </p><br />

    <p>     2.    43949   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Effects of pesticides on sporulation of Cyclospora cayetanensis and viability of Cryptosporidium parvum</p>

    <p>           Sathyanarayanan, Lakshmi and Ortega, Ynes</p>

    <p>           Journal of Food Protection 2004. 67(5): 1044-1049</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>     3.    43950   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Inhibitors of 3C Cysteine Proteinases from Picornaviridae</p>

    <p>           Lall, MS, Jain, RP, and Vederas, JC</p>

    <p>           Curr Top Med Chem 2004. 4(12): 1239-53</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320724&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320724&amp;dopt=abstract</a> </p><br />

    <p>     4.    43951   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Antifungal pharmacokinetics and pharmacodynamics: understanding the implications for antifungal drug resistance</p>

    <p>           Andes, D</p>

    <p>           Drug Resist Updat 2004. 7(3): 185-194</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296860&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296860&amp;dopt=abstract</a> </p><br />

    <p>     5.    43952   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Effects of caspofungin (MK-0991) and anidulafungin (LY303366) on phagocytosis, oxidative burst and killing of Candida albicans by human phagocytes</p>

    <p>           Frank, U, Greiner, M, Engels, I, and Daschner, FD</p>

    <p>           Eur J Clin Microbiol Infect Dis 2004</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15300456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15300456&amp;dopt=abstract</a> </p><br />

    <p>     6.    43953   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Prp8 intein in fungal pathogens: target for potential antifungal drugs</p>

    <p>           Liu, XQ and Yang, J</p>

    <p>           FEBS Lett 2004. 572(1-3): 46-50</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15304322&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15304322&amp;dopt=abstract</a> </p><br />

    <p>        </p>
    <br clear="all">

    <p>    7.     43954   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Molecular characterisation of streptomycin-resistant Mycobacterium tuberculosis strains isolated in Poland</p>

    <p>           Brzostek, A, Sajduda, A, Sliwinski, T, Augustynowicz-Kopec, E, Jaworski, A, Zwolska, Z, and Dziadek, J</p>

    <p>           Int J Tuberc Lung Dis 2004. 8(8): 1032-5</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15305490&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15305490&amp;dopt=abstract</a> </p><br />

    <p>     8.    43955   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Antimycobacterial activity of synthetic pamamycins</p>

    <p>           Lefevre, P, Peirs, P, Braibant, M, Fauville-Dufaux, M, Vanhoof, R, Huygen, K, Wang, XM, Pogell, B, Wang, Y, Fischer, P, Metz, P, and Content, J</p>

    <p>           J Antimicrob Chemother 2004</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15317744&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15317744&amp;dopt=abstract</a> </p><br />

    <p>     9.    43956   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Interactions of 5-deazapteridine Derivatives with Mycobacterium tuberculosis and with Human Dihydrofolate Reductases</p>

    <p>           Da Cunha, EF, De Castro, Ramalho T, Bicca, De Alencastro R, and Maia, ER</p>

    <p>           J Biomol Struct Dyn 2004. 22(2): 119-130</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15317473&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15317473&amp;dopt=abstract</a> </p><br />

    <p>   10.    43957   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Antimicrobial activity of some thiadiazolyl- and triazolylbenzimidazoles</p>

    <p>           Kus, Canan, Ayhan-Kilcigil, Guelguen, and Altanlar, Nurten</p>

    <p>           Ankara Universitesi Eczacilik Fakultesi Dergisi 2004. 33(1): 1-6</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   11.    43958   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           The in vitro model of tissue cyst formation in Toxoplasma gondii</p>

    <p>           McHugh, TD, Holliman, RE, and Butcher, PD</p>

    <p>           Parasitol Today 94. 10(7): 281-5</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15275448&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15275448&amp;dopt=abstract</a> </p><br />

    <p>   12.    43959   OI-LS-303; PUBMED-OI-8/24/2004</p>

    <p class="memofmt1-2">           Substrate-induced asymmetry and channel closure revealed by the apoenzyme structure of Mycobacterium tuberculosis phosphopantetheine adenylyltransferase</p>

    <p>           Morris, VK and Izard, T</p>

    <p>           Protein Sci 2004. 13(9): 2547-52</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15322293&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15322293&amp;dopt=abstract</a> </p><br />

    <p>   13.    43960   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           The antimicrobial activity of extracts of the lichen Cladonia foliacea and its (-)-usnic acid, atranorin, and fumarprotocetraric acid constituents</p>

    <p>           Yilmaz, Meral, Tuerk, Aysen Oezdemir, Tay, Turgay, and Kivanc, Merih</p>

    <p>           Zeitschrift fuer Naturforschung, C: Journal of Biosciences 2004. 59(3/4): 249-254</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   14.    43961   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Antimicrobial activities of heparin-binding peptides</p>

    <p>           Andersson, Emma, Rydengard, Victoria, Sonesson, Andreas, Moergelin, Matthias, Bjoerck, Lars, and Schmidtchen, Artur</p>

    <p>           European Journal of Biochemistry 2004. 271(6): 1219-1226</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   15.    43962   OI-LS-303; WOS-OI-8/15/2004</p>

    <p class="memofmt1-2">           Hindered nucleoside analogs as antiflaviviridae agents</p>

    <p>           Manfredini, S, Angusti, A, Veronese, AC, Durini, E, Vertuani, S, Nalin, F, Solaroli, N, Pricl, S, Ferrone, M, Mura, M, Piano, MA, Poddesu, B, Cadeddu, A, La, Colla P, and Loddo, R</p>

    <p>           PURE APPL CHEM 2004. 76(5): 1007-1015</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222693700014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222693700014</a> </p><br />

    <p>   16.    43963   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Synthesis, anti-mycobacterial, anti-trichomonas and anti-candida in vitro activities of 2-substituted 6,7-difluoro-3-methylquinoxaline 1,4-dioxides</p>

    <p>           Carta, Antonio, Loriga, Mario, Paglietti, Giuseppe, Mattana, Antonella, Fiori, Pier Luigi, Mollicotti, Paola, Sechi, Leonardo, and Zanetti, Stefania</p>

    <p>           European Journal of Medicinal Chemistry 2004. 39(2): 195-203</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   17.    43964   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p><b>           Preparation of pyridyl triazinamines as microbiocides</b> ((Ciba Specialty Chemicals Holding Inc., Switz.)</p>

    <p>           Hoelzl, Werner, Eichacker, Gabriele, and Preuss, Andrea</p>

    <p>           PATENT: WO 2004013124 A1;  ISSUE DATE: 20040212</p>

    <p>           APPLICATION: 2003; PP: 34 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   18.    43965   OI-LS-303; WOS-OI-8/15/2004</p>

    <p class="memofmt1-2">           Synthesis and antiviral evaluation of some novel tricyclic pyrazolo[3,4-b]indole nucleosides</p>

    <p>           Williams, JD, Drach, JC, and Townsend, LB</p>

    <p>           NUCLEOS NUCLEOT NUCL 2004. 23(5): 805-812</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222641300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222641300007</a> </p><br />

    <p>   19.    43966   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Antimicrobial efficacy of L-carnitine</p>

    <p>           Olgun, A, Kisa, O, Yildiran, ST, Tezcan, S, Akman, S, and Erbil, MK</p>

    <p>           Annals of Microbiology (Milano, Italy) 2004. 54(1): 95-101</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   20.    43967   OI-LS-303; WOS-OI-8/15/2004</p>

    <p class="memofmt1-2">           Managing a portfolio of interdependent new product candidates in the pharmaceutical industry</p>

    <p>           Blau, GE, Pekny, JF, Varma, VA, and Bunch, PR</p>

    <p>           J PROD INNOVAT MANAG 2004. 21(4): 227-245</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222662900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222662900001</a> </p><br />

    <p>   21.    43968   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p><b>           Combinations for the treatment of fungal infections</b> ((Combinatorx Incorporated, USA)</p>

    <p>           Johansen, Lisa M, Serbedzija, George N, Auspitz, Benjamin A, Zimmermann, Grant R, Keith, Curtis, Nichols, James M, and Gaw, Debra A</p>

    <p>           PATENT: WO 2004002430 A2;  ISSUE DATE: 20040108</p>

    <p>           APPLICATION: 2003; PP: 69 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>        </p>
    <br clear="all">

    <p>  22.     43969   OI-LS-303; WOS-OI-8/15/2004</p>

    <p class="memofmt1-2">           Intracellular phenotype of Mycobacterium avium enters macrophages primarily by a macropinocytosis-like mechanism and survives in a compartment that differs from that with extracellular phenotype</p>

    <p>           Bermudez, LE, Petrofsky, M, and Sangari, F</p>

    <p>           CELL BIOL INT  2004. 28(5): 411-419</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222627500010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222627500010</a> </p><br />

    <p>   23.    43970   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Design, Synthesis, and Biological Evaluation of 2,4-Diamino-5-methyl-6-substituted-pyrrolo[2,3-d]pyrimidines as Dihydrofolate Reductase Inhibitors</p>

    <p>           Gangjee, Aleem, Lin, Xin, and Queener, Sherry F</p>

    <p>           Journal of Medicinal Chemistry 2004.  47(14): 3689-3692</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   24.    43971   OI-LS-303; WOS-OI-8/15/2004</p>

    <p class="memofmt1-2">           Reporter substrates for assessing the activity of the hepatitis C virus NS3-4A serine protease in living cells</p>

    <p>           Pacini, L, Bartholomew, L, Vitelli, A, and Migliaccio, G</p>

    <p>           ANAL BIOCHEM 2004. 331(1): 46-59</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222697200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222697200006</a> </p><br />

    <p>   25.    43972   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p><b>           Anti-microbial agents, diagnostic reagents, and vaccines based on unique Apicomplexan parasite components</b> ((USA))</p>

    <p>           Mcleod, Rima LW, Roberts, Craig W, Roberts, Fiona, Johnson, Jennifer J, and Mets, Laurens</p>

    <p>           PATENT: US 6699654 B1;  ISSUE DATE: 20040302</p>

    <p>           APPLICATION: 98-37795; PP: 81 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   26.    43973   OI-LS-303; WOS-OI-8/23/2004</p>

    <p class="memofmt1-2">           Purification and characterization of Mycobacterium tuberculosis KatG, KatG(S315T), and Mycobacterium bovis KatG(R463L)</p>

    <p>           Wengenack, NL, Lane, BD, Hill, PJ, Uhl, JR, Lukat-Rodgers, GS, Hall, L, Roberts, GD, Cockerill, FR, Brennan, PJ, Rodgers, KR, Belisle, JT, and Rusnak, F</p>

    <p>           PROTEIN EXPRES PURIF 2004. 36(2): 232-243</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863100010</a> </p><br />

    <p>   27.    43974   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Comparison of inhibitory effect of rifalazil and rifampicin against Mycobacterium ulcerans infection induced in mice</p>

    <p>           Nakanaga, Kazue, Saito, Hajime, Ishi, Norihisa, and Goto, Masamichi</p>

    <p>           Kekkaku 2004. 79(5): 333-339</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   28.    43975   OI-LS-303; WOS-OI-8/23/2004</p>

    <p class="memofmt1-2">           Proteome study of macrophage infected with Mycobacterium tuberculosis isoniazid resistant strain</p>

    <p>           Liu, LR, Yue, J, and Wang, HH</p>

    <p>           PROG BIOCHEM BIOPHYS 2004. 31(4): 361-367</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222848800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222848800014</a> </p><br />

    <p>        </p>
    <br clear="all">

    <p>  29.     43976   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Role for malonyl coenzyme A:acyl carrier protein transacylase (MCAT) in the growth-inhibitory effect of the calmodulin antagonist trifluoperazine in Mycobacterium bovis BCG</p>

    <p>           Sinha, Indrajit and Dick, Thomas</p>

    <p>           Journal of Antimicrobial Chemotherapy 2004. 53(6): 1072-1075</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   30.    43977   OI-LS-303; WOS-OI-8/23/2004</p>

    <p class="memofmt1-2">           Imidazole analogues of fluoxetine, a novel class of anti-candida agents</p>

    <p>           Silvestri, R, Artico, M, La Regina, G, Di, Pasquali A, De Martino, G, D&#39;Auria, FD, Nencioni, L, and Palamara, AT</p>

    <p>           J MED CHEM 2004. 47(16): 3924-3926</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222856800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222856800004</a> </p><br />

    <p>   31.    43978   OI-LS-303; WOS-OI-8/23/2004</p>

    <p class="memofmt1-2">           US firm licenses Cuban drug candidates</p>

    <p>           Anon</p>

    <p>           CHEM ENG NEWS 2004. 82(29): 10</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222813700023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222813700023</a> </p><br />

    <p>   32.    43979   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Induction of Mycobacterium avium growth restriction and inhibition of phagosome-endosome interactions during macrophage activation and apoptosis induction by picolinic acid plus IfNg</p>

    <p>           Pais, Teresa F and Appelberg, Rui</p>

    <p>           Microbiology (Reading, United Kingdom) 2004. 150(5): 1507-1518</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   33.    43980   OI-LS-303; WOS-OI-8/23/2004</p>

    <p class="memofmt1-2">           Synthesis and antimycobacterial activities of ring-substituted quinolinecarboxylic acid/ester analogues. Part 1</p>

    <p>           Vaitilingam, B, Nayyar, A, Palde, PB, Monga, V, Jain, R, Kaur, S, and Singh, PP</p>

    <p>           BIOORGAN MED CHEM 2004. 12(15): 4179-4188</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222886700023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222886700023</a> </p><br />

    <p>   34.    43981   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Effects of gamma interferon, interleukin-10, and transforming growth factor b on the survival of Mycobacterium avium subsp. paratuberculosis in monocyte-derived macrophages from naturally infected cattle</p>

    <p>           Khalifeh, MS and Stabel, JR</p>

    <p>           Infection and Immunity 2004. 72(4): 1974-1982</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   35.    43982   OI-LS-303; WOS-OI-8/23/2004</p>

    <p class="memofmt1-2">           Novel imidazo[1,2-c]pyrimidine base-modified nucleosides: synthesis and antiviral evaluation</p>

    <p>           Kifli, N, De Clercq, E, Balzarini, J, and Simons, C</p>

    <p>           BIOORGAN MED CHEM 2004. 12(15): 4245-4252</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222886700029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222886700029</a> </p><br />

    <p>   36.    43983   OI-LS-303; WOS-OI-8/23/2004</p>

    <p class="memofmt1-2">           Increased human cytomegalovirus replication in fibroblasts after treatment with therapeutical plasma concentrations of valproic acid</p>

    <p>           Michaelis, M, Kohler, N, Reinisch, A, Eikel, D, Gravemann, U, Doerr, HW, Nau, H, and Cinatl, J</p>

    <p>           BIOCHEM PHARMACOL 2004. 68(3): 531-538</p>

    <p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222881600014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222881600014</a> </p><br />

    <p>        </p>
    <br clear="all">

    <p>  37.     43984   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Establishment of Mycobacterium avium subsp. paratuberculosis infection in the intestine of ruminants</p>

    <p>           Sigurethardottir, Olof G, Valheim, Mette, and Press, Charles McL</p>

    <p>           Advanced Drug Delivery Reviews 2004.  56(6): 819-834</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   38.    43985   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Synthesis of Aldehydo-sugar Derivatives of Pyrazoloquinoline as Inhibitors of Herpes Simplex Virus Type 1 Replication</p>

    <p>           Bekhit, Adnan A, El-Sayed, Ola A, Aboul-Enein, Hassan Y, Siddiqui, Yunus M, and Al-Ahdal, Mohammed N</p>

    <p>           Journal of Enzyme Inhibition and Medicinal Chemistry 2004. 19(1): 33-38</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   39.    43986   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p><b>           Preparation of 3-(benzothiadiazin-3-yl)quinolines as HCV anti-infectives</b> ((Smithkline Beecham Corporation, USA)</p>

    <p>           Chai, Deping, Duffy, Kevin J, Fitch, Duke M, Shaw, Antony N, Tedesco, Rosanna, Wiggall, Kenneth J, and Zimmerman, Michael N</p>

    <p>           PATENT: WO 2004052313 A2;  ISSUE DATE: 20040624</p>

    <p>           APPLICATION: 2003; PP: 88 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   40.    43987   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           Action of celgosivir (6 O-butanoyl castanospermine) against the pestivirus BVDV: Implications for the treatment of hepatitis C</p>

    <p>           Whitby, Kevin, Taylor, Debra, Patel, Dipa, Ahmed, Parvin, and Tyms, AStanley</p>

    <p>           Antiviral Chemistry &amp; Chemotherapy 2004. 15(3): 141-151</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   41.    43988   OI-LS-303; SCIFINDER-OI-8/17/2004</p>

    <p class="memofmt1-2">           The effects of a herbal medicine (Mao-to) in patients with chronic hepatitis C after injection of IFN-b</p>

    <p>           Kainuma, M, Sakai, S, Sekiya, N, Mantani, N, Ogata, N, Shimada, Y, and Terasawa, K</p>

    <p>           Phytomedicine  2004. 11(1): 5-10</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
