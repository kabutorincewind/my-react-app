

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-322.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+ui23pDELeYWJpUKuUFKkGVh+TlVdtvzRuOq5Uu6t/w7BgW8Fh+hV6xgn9OWUdzrco/N/G1Wa+N9xNG5Zss7AARl2D/Sz7x1oi7L9pMNxRlPzDHWHllY2Q8DZK4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0E5EEAFA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-322-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45274   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          Structure-Based Design: Synthesis and Biological Evaluation of a Series of Novel Cycloamide-Derived HIV-1 Protease Inhibitors</p>

    <p>          Ghosh, AK, Swanson, LM, Cho, H, Leshchenko, S, Hussain, KA, Kay, S, Walters, DE, Koh, Y, and Mitsuya, H</p>

    <p>          J Med Chem <b>2005</b>.  48(10): 3576-3585</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15887965&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15887965&amp;dopt=abstract</a> </p><br />

    <p>2.     45275   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          Stronger anti-HIV-1 activity of C-peptide derived from HIV-1(89.6) gp41 C-terminal heptad repeated sequence</p>

    <p>          Seo, JK, Kim, HK, Lee, TY, Hahm, KS, Kim, KL, and Lee, MK</p>

    <p>          Peptides <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15876473&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15876473&amp;dopt=abstract</a> </p><br />

    <p>3.     45276   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          Newer aminopyrimidinimino isatin analogues as non-nucleoside HIV-1 reverse transcriptase inhibitors for HIV and other opportunistic infections of AIDS: design, synthesis and biological evaluation</p>

    <p>          Sriram, D, Bal, TR, and Yogeeswari, P</p>

    <p>          Farmaco <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15876436&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15876436&amp;dopt=abstract</a> </p><br />

    <p>4.     45277   HIV-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          Mesuol, a natural occurring 4-phenylcoumarin, inhibits HIV-1 replication by targeting the NF-[kappa]B pathway</p>

    <p>          Marquez, Nieves, Sancho, Rocio, Bedoya, Luis M, Alcami, Jose, Lopez-Perez, Jose Luis, Feliciano, Arturo San, Fiebich, Bernd L, and Munoz, Eduardo</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4G0DB2D-1/2/c144ccf29b61abd468d45260640f1b33">http://www.sciencedirect.com/science/article/B6T2H-4G0DB2D-1/2/c144ccf29b61abd468d45260640f1b33</a> </p><br />

    <p>5.     45278   HIV-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          2H-Pyrrolo[3,4-b] [1,5]benzothiazepine derivatives as potential inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Di Santo, Roberto and Costi, Roberta</p>

    <p>          Il Farmaco <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJ8-4G1GFHR-2/2/75e1fc8ab4b9f8cbb8a713ac67ccf571">http://www.sciencedirect.com/science/article/B6VJ8-4G1GFHR-2/2/75e1fc8ab4b9f8cbb8a713ac67ccf571</a> </p><br />
    <br clear="all">

    <p>6.     45279   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          Rare one and two amino acid inserts adjacent to codon 103 of the HIV-1 reverse transcriptase (RT) affect susceptibility to non-nucleoside RT inhibitors</p>

    <p>          Winters, MA, Kagan, RM, Kovari, L, Heseltine, PN, and Merigan, TC</p>

    <p>          Antivir Ther <b>2005</b>.  10(2): 363-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865232&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865232&amp;dopt=abstract</a> </p><br />

    <p>7.     45280   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          In vitro combination studies of tenofovir and other nucleoside analogues with ribavirin against HIV-1</p>

    <p>          Margot, NA and Miller, MD</p>

    <p>          Antivir Ther <b>2005</b>.  10(2): 343-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865229&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865229&amp;dopt=abstract</a> </p><br />

    <p>8.     45281   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          Phaseococcin, an antifungal protein with antiproliferative and anti-HIV-1 reverse transcriptase activities from small scarlet runner beans</p>

    <p>          Ngai, PH and Ng, TB</p>

    <p>          Biochem Cell Biol <b>2005</b>.  83(2): 212-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15864329&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15864329&amp;dopt=abstract</a> </p><br />

    <p>9.     45282   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          5-(3-Bromophenyl)-7-(6-morpholin-4-ylpyridin-3-yl)pyrido[2,3-d]pyrimidin-4 -ylamine: structure-activity relationships of 7-substituted heteroaryl analogs as non-nucleoside adenosine kinase inhibitors</p>

    <p>          Matulenko, MA, Lee, CH, Jiang, M, Frey, RR, Cowart, MD, Bayburt, EK, Didomenico, S, Gfesser, GA, Gomtsyan, A, Zheng, GZ, McKie, JA, Stewart, AO, Yu, H, Kohlhaas, KL, Alexander, KM, McGaraughty, S, Wismer, CT, Mikusa, J, Marsh, KC, Snyder, RD, Diehl, MS, Kowaluk, EA, Jarvis, MF, and Bhagwat, SS</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(11): 3705-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15863000&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15863000&amp;dopt=abstract</a> </p><br />

    <p>10.   45283   HIV-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          Bioanalysis of HIV protease inhibitors in samples from sanctuary sites</p>

    <p>          Crommentuyn, KML, Huitema, ADR, and Beijnen, JH</p>

    <p>          Journal of Pharmaceutical and Biomedical Analysis <b>2005</b> .  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGX-4F8TKK9-1/2/cc3333b6f4a745c4cebd4dec1ef53a03">http://www.sciencedirect.com/science/article/B6TGX-4F8TKK9-1/2/cc3333b6f4a745c4cebd4dec1ef53a03</a> </p><br />
    <br clear="all">

    <p>11.   45284   HIV-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          HIV reverse transcriptase structures: designing new inhibitors and understanding mechanisms of drug resistance</p>

    <p>          Ren, Jingshan and Stammers, David K</p>

    <p>          Trends in Pharmacological Sciences <b>2005</b>.  26(1): 4-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1K-4DXT7XY-2/2/aec303a5e708fed8dbf444f5a47ca8c6">http://www.sciencedirect.com/science/article/B6T1K-4DXT7XY-2/2/aec303a5e708fed8dbf444f5a47ca8c6</a> </p><br />

    <p>12.   45285   HIV-LS-322; PUBMED-HIV-5/16/2005</p>

    <p class="memofmt1-2">          Switching between Allosteric and Dimerization Inhibition of HIV-1 Protease</p>

    <p>          Bowman, MJ, Byrne, S, and Chmielewski, J</p>

    <p>          Chem Biol <b>2005</b>.  12(4): 439-44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850980&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850980&amp;dopt=abstract</a> </p><br />

    <p>13.   45286   HIV-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          Design, synthesis and biological evaluation of heteroaryl diketohexenoic and diketobutanoic acids as HIV-1 integrase inhibitors endowed with antiretroviral activity</p>

    <p>          Di Santo, R, Costi, R, Artico, M, Ragno, R, Greco, G, Novellino, E, Marchand, C, and Pommier, Y</p>

    <p>          Il Farmaco <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJ8-4G24XKV-1/2/1473910fcb33c43b2f7f66ec30ec5e79">http://www.sciencedirect.com/science/article/B6VJ8-4G24XKV-1/2/1473910fcb33c43b2f7f66ec30ec5e79</a> </p><br />

    <p>14.   45287   HIV-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          Syntheses of new difluoromethylene benzoxazole and 1,2,4-oxadiazole derivatives, as potent non-nucleoside HIV-1 reverse transcriptase inhibitors</p>

    <p>          Medebielle, Maurice, Ait-Mohand, Samia, Burkhloder, Conrad, Dolbier, Jr William R, Laumond, Geraldine, and Aubertin, Anne-Marie</p>

    <p>          Journal of Fluorine Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGD-4FHJYKC-2/2/b60afb445c1bc571be5df15c5b25a40b">http://www.sciencedirect.com/science/article/B6TGD-4FHJYKC-2/2/b60afb445c1bc571be5df15c5b25a40b</a> </p><br />

    <p>15.   45288   HIV-LS-322; WOS-HIV-5/8/2005</p>

    <p class="memofmt1-2">          Synthesis of 3,4-disubstituted quinolin-2(1H)-ones via palladium-catalyzed regioselective cross-coupling reactions of 3-bromo-4-trifloxyquinolin-2(1H)-one with arylboronic acids</p>

    <p>          Wu, J, Zhang, L, and Sun, XY</p>

    <p>          CHEMISTRY LETTERS <b>2005</b>.  34(4): 550-551, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228541600047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228541600047</a> </p><br />

    <p>16.   45289   HIV-LS-322; WOS-HIV-5/8/2005</p>

    <p class="memofmt1-2">          Potent and selective inhibition of HIV-1 transcription by a novel naphthalene derivative</p>

    <p>          Wang, X, Yamataka, K, Okamoto, M, Ikeda, A, and Baba, M</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A48-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900043</a> </p><br />
    <br clear="all">

    <p>17.   45290   HIV-LS-322; WOS-HIV-5/8/2005</p>

    <p class="memofmt1-2">          Exploring a new approach in AIDS therapy. Design, synthesis and biological evaluation of potential dimerization inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Garcia-Aparicio, C, Rodriguez-Barrios, F, Gago, F, De, Clercq E, Balzarini, J, Camarasa, MJ, and Velazquez, S</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A48-A49, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900044</a> </p><br />

    <p>18.   45291   HIV-LS-322; WOS-HIV-5/8/2005</p>

    <p class="memofmt1-2">          Inhibitors of HIV integrase: New diketo structures with heterocyclic scaffolds</p>

    <p>          Nair, V, Chi, GC, and Uchil, V</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A50-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900047</a> </p><br />

    <p>19.   45292   HIV-LS-322; WOS-HIV-5/8/2005</p>

    <p class="memofmt1-2">          Synthesis and study of 1-(2 &#39;-deoxy-beta-D-ribofuranosy )-1,2,4-triazole-3-carboxamide as an anti-HIV-1 mutagenic agent</p>

    <p>          Vivet-Boudou, V, Paillart, JC, Burger, A, and Marquet, R</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A51-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900050</a> </p><br />

    <p>20.   45293   HIV-LS-322; WOS-HIV-5/15/2005</p>

    <p class="memofmt1-2">          Tosylation/mesylation of 4-hydroxy-3-nitro-2-pyridinones as an activation step in the construction of dihydropyrido[3,4-b] benzo[f][1,4]thiazepin-1-one based anti-HIV agents</p>

    <p>          Storck, P, Aubertin, AM, and Grierson, DS</p>

    <p>          TETRAHEDRON LETTERS <b>2005</b>.  46(16): 2919-2922, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228195200043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228195200043</a> </p><br />

    <p>21.   45294   HIV-LS-322; WOS-HIV-5/15/2005</p>

    <p class="memofmt1-2">          Glycyrrhizin inhibits R5 HIV replication in peripheral blood monocytes treated with 1-methyladenosine</p>

    <p>          Takei, M, Kobayashi, M, Li, XD, Pollard, RB, and Suzuki, F</p>

    <p>          PATHOBIOLOGY <b>2005</b>.  72(3): 117-123, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228703900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228703900002</a> </p><br />

    <p>22.   45295   HIV-LS-322; WOS-HIV-5/15/2005</p>

    <p class="memofmt1-2">          Antiviral activity in vitro of Urtica dioica L., Parietaria diffusa M.et K. and Sambucus nigra L</p>

    <p>          Manganelli, REU, Zaccaro, L, and Tomei, PE</p>

    <p>          JOURNAL OF ETHNOPHARMACOLOGY <b>2005</b>.  98(3): 323-327, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228688300015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228688300015</a> </p><br />

    <p>23.   45296   HIV-LS-322; WOS-HIV-5/15/2005</p>

    <p class="memofmt1-2">          A novel aspochalasin with HIV-1 integrase inhibitory activity from Aspergillus flavipes</p>

    <p>          Rochfort, S, Ford, J, Ovenden, S, Wan, SS, George, S, Wildman, H, Tait, RM, Meurer-Grimes, B, Cox, S, Coates, J, and Rhodes, D</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2005</b>.  58(4): 279-283, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228778500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228778500008</a> </p><br />

    <p>24.   45297   HIV-LS-322; WOS-HIV-5/15/2005</p>

    <p class="memofmt1-2">          Large-scale conformational dynamics of the HIV-1 integrase core domain and its catalytic loop mutants</p>

    <p>          Lee, MC, Deng, JX, Briggs, JM, and Duan, Y</p>

    <p>          BIOPHYSICAL JOURNAL <b>2005</b>.  88(5): 3133-3146, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228688800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228688800011</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
