

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-374.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="o6yQH2LqbIe8ZLiWv4fe4tWSL8HvzRS6UVGcIrvuDrzZgukmobrBWIkM+JZpAPZPh6PFi55ZVlHrN3ZBzgXw17K7PmrG8+5G5TIbOkIKt5WsCxiz2Hycirl/Gy4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="31F4F042" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-374-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60814   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Preliminary safety and efficacy data of brecanavir, a novel HIV-1 protease inhibitor: 24 week data from study HPR10006</p>

    <p>          Lalezari, JP, Ward, DJ, Tomkins, SA, and Garges, HP</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17491001&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17491001&amp;dopt=abstract</a> </p><br />

    <p>2.     60815   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Inhibition of the Activities of Reverse Transcriptase and Integrase of Human Immunodeficiency Virus Type-1 by Peptides Derived from the Homologous Viral Protein R (Vpr)</p>

    <p>          Gleenberg, IO, Herschhorn, A, and Hizi, A</p>

    <p>          J Mol Biol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17490682&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17490682&amp;dopt=abstract</a> </p><br />

    <p>3.     60816   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Identification of a Novel Small Molecule Inhibitor that Targets HIV-1 Envelope Maturation</p>

    <p>          Jimenez, Judith, Cao, Joan, Jackson, Lynn, Peng, Qinghai, Wu, Hua, Isaacson, Jason, Butler, Scott, Patick, Amy K, and Blair, Wade S</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A27-293</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2/2/b559edaf85914d129930db284343f060">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2/2/b559edaf85914d129930db284343f060</a> </p><br />

    <p>4.     60817   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Rapamycin reduces CCR5 density levels on CD4 T cells and this effect results in potentiation of Enfuvirtide (T-20) against R5 HIV-1 in vitro</p>

    <p>          Heredia, A, Gilliam, B, Latinovic, O, Le, N, Bamba, D, Devico, A, Melikyan, GB, Gallo, RC, and Redfield, RR</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17485501&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17485501&amp;dopt=abstract</a> </p><br />

    <p>5.     60818   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          HIV-1 escape from the entry-inhibiting effects of a cholesterol-binding compound via cleavage of gp41 by the viral protease</p>

    <p>          Waheed, AA, Ablan, SD, Roser, JD, Sowder, RC, Schaffner, CP, Chertova, E, and Freed, EO</p>

    <p>          Proc Natl Acad Sci U S A <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17483482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17483482&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60819   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          The alpha(1,2)-mannosidase I inhibitor 1-deoxymannojirimycin potentiates the antiviral activity of carbohydrate-binding agents against wild-type and mutant HIV-1 strains containing glycan deletions in gp120</p>

    <p>          Balzarini, J</p>

    <p>          FEBS Lett <b>2007</b>.  581(10): 2060-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17475258&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17475258&amp;dopt=abstract</a> </p><br />

    <p>7.     60820   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Peptidomimetic Dimerization Inhibitors of HIV-1 Protease: Further Insights into Structural Variations and Mechanism of Action</p>

    <p>          Bannwarth, Ludovic, Rose, Thierry, Frutos, Silvia, Giralt, Ernest, Vanderesse, Regis, Jamart-Gregoire, Brigitte, Vidu, Anamaria, Ongeri, Sandrine, Sicsic, Sames, Pannecouque, Christophe, De Clercq, Erik, and Reboud-Ravaux, Michele</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A64-A58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-37/2/38e592efb46a80210c9b7a59a497b24b">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-37/2/38e592efb46a80210c9b7a59a497b24b</a> </p><br />

    <p>8.     60821   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          HIV-1 Integrase Inhibitors : An Emerging Clinical Reality</p>

    <p>          Dayam, R, Al-Mawsawi, LQ, and Neamati, N</p>

    <p>          Drugs R D <b>2007</b>.  8(3): 155-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17472411&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17472411&amp;dopt=abstract</a> </p><br />

    <p>9.     60822   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Synthesis and biological activity of vicinal diaryl-substituted 1H-imidazoles</p>

    <p>          Bellina, Fabio, Cauteruccio, Silvia, and Rossi, Renzo</p>

    <p>          Tetrahedron <b>2007</b>.  63(22): 4571-4624</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4N3WYGD-1/2/45c6237a9f6ae2280971a160effdebbe">http://www.sciencedirect.com/science/article/B6THR-4N3WYGD-1/2/45c6237a9f6ae2280971a160effdebbe</a> </p><br />

    <p>10.   60823   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Tricyclo-DNA containing oligonucleotides as steric block inhibitors of human immunodeficiency virus type 1 tat-dependent trans-activation and HIV-1 infectivity</p>

    <p>          Ivanova, G, Reigadas, S, Ittig, D, Arzumanov, A, Andreola, ML, Leumann, C, Toulme, JJ, and Gait, MJ</p>

    <p>          Oligonucleotides <b>2007</b>.  17(1): 54-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17461763&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17461763&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60824   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Synthesis of some novel thiourea derivatives obtained from5-[(4-aminophenoxy)methyl]-4-alkyl/aryl-2,4-dihydro-3H-1,2,4-triazole-3-thiones and evaluation as antiviral / anti-HIV and anti-tuberculosis agents</p>

    <p>          Kucukguzel, Ilkay, Tatar, Esra, Guniz Kucukguzel, S, Rollas, Sevim, and De Clercq, Erik</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript:  2064</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4NR1BRD-1/2/7f3459ec6d07df777e536f9503e62d0c">http://www.sciencedirect.com/science/article/B6VKY-4NR1BRD-1/2/7f3459ec6d07df777e536f9503e62d0c</a> </p><br />

    <p>12.   60825   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Efficient inhibition of HIV-1 expression by LNA modified antisense oligonucleotides and DNAzymes targeted to functionally selected binding sites</p>

    <p>          Jakobsen, MR, Haasnoot, J, Wengel, J, Berkhout, B, and Kjems, J</p>

    <p>          Retrovirology <b>2007</b>.  4: 29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17459171&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17459171&amp;dopt=abstract</a> </p><br />

    <p>13.   60826   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          New HIV drug classes on the horizon</p>

    <p>          Opar, A</p>

    <p>          Nat Rev Drug Discov <b>2007</b>.  6(4): 258-259</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17457997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17457997&amp;dopt=abstract</a> </p><br />

    <p>14.   60827   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Discovery of structurally diverse HIV-1 integrase inhibitors based on a chalcone pharmacophore</p>

    <p>          Deng, Jinxia, Sanchez, Tino, Al-Mawsawi, Laith Q, Dayam, Raveendra, Yunes, Rosendo A, Garofalo, Antonio, Bolger, Michael B, and Neamati, Nouri</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 234</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NK4G35-1/2/7249b0844623149cdbe9551fc2733544">http://www.sciencedirect.com/science/article/B6TF8-4NK4G35-1/2/7249b0844623149cdbe9551fc2733544</a> </p><br />

    <p>15.   60828   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Scaffold hopping in the rational design of novel HIV-1 non-nucleoside reverse transcriptase inhibitors</p>

    <p>          O&#39;meara, JA, Jakalian, A, Laplante, S, Bonneau, PR, Coulombe, R, Faucher, AM, Guse, I, Landry, S, Racine, J, Simoneau, B, Thavonekham, B, and Yoakim, C</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17451954&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17451954&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60829   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Design, synthesis, and anti-HIV activity of 2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxyuridine (d4U), 2&#39;,3&#39;-dideoxyuridine (ddU) phosphoramidate `ProTide&#39; derivatives</p>

    <p>          Mehellou, Youcef, McGuigan, Christopher, Brancale, Andrea, and Balzarini, Jan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  228</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NJG3Y4-2/2/bafe39295b1a09d9449811bc6e15614d">http://www.sciencedirect.com/science/article/B6TF9-4NJG3Y4-2/2/bafe39295b1a09d9449811bc6e15614d</a> </p><br />

    <p>17.   60830   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Anti-HIV agents. Integrase inhibitor raltegravir makes its mark</p>

    <p>          Anon</p>

    <p>          TreatmentUpdate <b>2007</b>.  19(2): 8-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17447316&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17447316&amp;dopt=abstract</a> </p><br />

    <p>18.   60831   HIV-LS-374; PUBMED-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Raltegravir: a new antiretroviral class for salvage therapy</p>

    <p>          Cahn, P and Sued, O</p>

    <p>          Lancet <b>2007</b>.  369(9569): 1235-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434380&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434380&amp;dopt=abstract</a> </p><br />

    <p>19.   60832   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          In vitro HIV-1 reverse transcriptase inhibitory activity of naphthoquinones and derivatives from Euclea natalensis</p>

    <p>          Tshikalange, TE, Lall, N, Meyer, JJM, and Mahapatra, A</p>

    <p>          South African Journal of Botany <b>2007</b>.  73(2): 339-482</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7XN9-4NBRG1G-6P/2/3c24f999f7faf2f99fdbb4d7ab8e6b10">http://www.sciencedirect.com/science/article/B7XN9-4NBRG1G-6P/2/3c24f999f7faf2f99fdbb4d7ab8e6b10</a> </p><br />

    <p>20.   60833   HIV-LS-374; EMBASE-HIV-5/14/2007</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of (-)-[beta]-d-(2R,4R)-1,3-dioxolane-2,6-diamino purine (DAPD) (amdoxovir) and (-)-[beta]-d-(2R,4R)-1,3-dioxolane guanosine (DXG) prodrugs</p>

    <p>          Narayanasamy, Janarthanan, Pullagurla, Manik R, Sharon, Ashoke, Wang, Jianing, Schinazi, Raymond F, and Chu, Chung K</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 482</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4NCMCSH-2/2/d852917b06d11ba3082e521afaa6b026">http://www.sciencedirect.com/science/article/B6T2H-4NCMCSH-2/2/d852917b06d11ba3082e521afaa6b026</a> </p><br />

    <p>21.   60834   HIV-LS-374; WOS-HIV-5/11/2007</p>

    <p class="memofmt1-2">          RNA binding protein HuR contributes to HIV protease inhibitor-induced TNF-a and IL-6 expression in macrophages</p>

    <p>          Zhou, HP, Jarujaron, S, Chen, L, Gurley, EB, Studer, E, Hu, WH, Pandak, WM, and Hylemon, PB</p>

    <p>          FASEB JOURNAL <b>2007</b>.  21(6): A852</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708701008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708701008</a> </p><br />

    <p>22.   60835   HIV-LS-374; WOS-HIV-5/11/2007</p>

    <p class="memofmt1-2">          The inhibition of HIV-1 transcription by hypoxia</p>

    <p>          Charles, SM, Ammosova, T, Nui, XM, Jerebtsova, M, Gordeuk, VR, and Nekhai, S</p>

    <p>          FASEB JOURNAL <b>2007</b>.  21(6): A1033-A1034</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708702314">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708702314</a> </p><br />

    <p>23.   60836   HIV-LS-374; WOS-HIV-5/11/2007</p>

    <p class="memofmt1-2">          Protein design of a bacterially expressed HIV-1 gp41 fusion inhibitor</p>

    <p>          Deng, YQ, Zheng, Q, Ketas, TJ, Moore, JP, and Lu, M</p>

    <p>          BIOCHEMISTRY <b>2007</b>.  46(14): 4360-4369</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245370400014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245370400014</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
