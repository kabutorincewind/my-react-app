

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-09-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="HYFzDsEHOSOSGDhz4QrT3kTRbGxpinbcZu2oQvFW0Duc3uJW+4hNasNVh/5kxqN1xKLLMHRZLpUVqWU8eqiwdGbiYX7d26ZY5qJkQbj0ld8o3C9fJpagvfeLqbQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1CD42574" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 30 - September 12, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24002136">Phytochemical, Antimicrobial and Antiprotozoal Evaluation of Garcinia mangostana Pericarp and Alpha-mangostin, Its Major Xanthone Derivative.</a> Al-Massarani, S.M., A.A. El Gamal, N.M. Al-Musayeib, R.A. Mothana, O.A. Basudan, A.J. Al-Rehaily, M. Farag, M.H. Assaf, K.H. El Tahir, and L. Maes. Molecules, 2013. 18(9): p. 10599-10608. PMID[24002136].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23792913">Synthesis, Structure and Antifungal Activity of Thiophene-2,3-dicarboxaldehyde bis(thiosemicarbazone) and Nickel(II), Copper(II) and Cadmium(II) Complexes: Unsymmetrical Coordination Mode of Nickel Complex.</a> Alomar, K., A. Landreau, M. Allain, G. Bouet, and G. Larcher. Journal of Inorganic Biochemistry, 2013. 126: p. 76-83. PMID[23792913].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23831506">Synthesis and Antifungal Activity of Terpenyl-1,4-naphthoquinone and 1,4-Anthracenedione Derivatives.</a> Castro, M.A., A.M. Gamito, V. Tangarife-Castano, B. Zapata, J.M. Miguel Del Corral, A.C. Mesa-Arango, L. Betancur-Galvis, and A. San Feliciano. European Journal of Medicinal Chemistry, 2013. 67: p. 19-27. PMID[23831506].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23804388">4. Isavuconazole Activity against Aspergillus lentulus, Neosartorya udagawae, and Cryptococcus gattii, Emerging Fungal Pathogens with Reduced Azole Susceptibility</a>. Datta, K., P. Rhee, E. Byrnes, 3rd, G. Garcia-Effron, D.S. Perlin, J.F. Staab, and K.A. Marr. Journal of Clinical Microbiology, 2013. 51(9): p. 3090-3093. PMID[23804388].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23996153">Phylogenetic Diversity and Antimicrobial Activity of Marine Bacteria Associated with the Soft Coral Sarcophyton glaucum.</a> Elahwany, A.M., H.A. Ghozlan, H.A. Elsharif, and S.A. Sabry. Journal of Basic Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23996153].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24003018">Synthesis and Characterization of Novel Organocobaloximes as Potential Catecholase and Antimicrobial Activity Agents.</a> Erdem-Tuncmen, M., F. Karipcin, and B. Sariboga. Archiv der Pharmazie (Weinheim), 2013. <b>[Epub ahead of print]</b>. PMID[24003018].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23871413">Activity of Antifungal Combinations against Aspergillus Species Evaluated by Isothermal Microcalorimetry.</a> Furustrand Tafin, U., C. Orasch, and A. Trampuz. Diagnostic Microbiology and Infectious Disease, 2013. 77(1): p. 31-36. PMID[23871413].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.></p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23770440">Two Peptides, TsAP-1 and TsAP-2, from the Venom of the Brazilian Yellow Scorpion, Tityus serrulatus: Evaluation of Their Antimicrobial and Anticancer Activities</a>. Guo, X., C. Ma, Q. Du, R. Wei, L. Wang, M. Zhou, T. Chen, and C. Shaw. Biochimie, 2013. 95(9): p. 1784-1794. PMID[23770440].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24010721">Phenyl Aldehyde and Propanoids Exert Multiple Sites of Action Towards Cell Membrane and Cell Wall Targeting Ergosterol in Candida albicans.</a> Khan, M.S., I. Ahmad, and S.S. Cameotra. AMB Express, 2013. 3(1): p. 54. PMID[24010721].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24013408">Chaetochromones A and B, Two New Polyketides from the Fungus Chaetomium indicum (CBS.860.68).</a> Lu, K., Y. Zhang, L. Li, X. Wang, and G. Ding. Molecules, 2013. 18(9): p. 10944-10952. PMID[24013408].</p>

    <p class="plaintext"><b>PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23620465">Nystatin Nanosizing Enhances in Vitro and in Vivo Antifungal Activity against Candida albicans.</a> Melkoumov, A., M. Goupil, F. Louhichi, M. Raymond, L. de Repentigny, and G. Leclair. The Journal of Antimicrobial Chemotherapy, 2013. 68(9): p. 2099-2105. PMID[23620465].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23870164">Comparison of the Vitek 2 Yeast Susceptibility System with CLSI Microdilution for Antifungal Susceptibility Testing of Fluconazole and Voriconazole against Candida spp., Using New Clinical Breakpoints and Epidemiological Cutoff Values.</a> Pfaller, M.A., D.J. Diekema, G.W. Procop, and M.G. Rinaldi. Diagnostic Microbiology and Infectious Disease, 2013. 77(1): p. 37-40. PMID[23870164].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24010893">Anti-candida Properties of Asaronaldehyde of Acorus gramineus Rhizome and Three Structural Isomers.</a> Rajput, S.B., R.B. Shinde, M.M. Routh, and S.M. Karuppayil. Chinese Medicine, 2013. 8(1): p. 18. PMID[24010893].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23891181">Synthesis and Antifungal Activities of Miltefosine Analogs.</a> Ravu, R.R., Y.L. Chen, M.R. Jacob, X. Pan, A.K. Agarwal, S.I. Khan, J. Heitman, A.M. Clark, and X.C. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4828-4831. PMID[23891181].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24013184">Anti-biofilm Activity of Low-molecular Weight Chitosan Hydrogel against Candida Species.</a> Silva-Dias, A., A. Palmeira-de-Oliveira, I.M. Miranda, J. Branco, L. Cobrado, M. Monteiro-Soares, J.A. Queiroz, C. Pina-Vaz, and A.G. Rodrigues. Medical Microbiology and Immunology, 2013. <b>[Epub ahead of print]</b>. PMID[24013184].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23811777">Bioassay-guided Fractionation of Extracts from Hypericum perforatum in Vitro Roots Treated with Carboxymethylchitosans and Determination of Antifungal Activity against Human Fungal Pathogens.</a> Tocci, N., F.D. D&#39;Auria, G. Simonetti, S. Panella, A.T. Palamara, A. Debrassi, C.A. Rodrigues, V.C. Filho, F. Sciubba, and G. Pasqua. Plant Physiology and Biochemistry, 2013. 70: p. 342-347. PMID[23811777].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br />   

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23891164">3-Aryl-4-acyloxyethoxyfuran-2(5H)-ones as Inhibitors of Tyrosyl-tRNA Synthetase: Synthesis, Molecular Docking and Antibacterial Evaluation.</a> Wang, X.D., R.C. Deng, J.J. Dong, Z.Y. Peng, X.M. Gao, S.T. Li, W.Q. Lin, C.L. Lu, Z.P. Xiao, and H.L. Zhu. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 4914-4922. PMID[23891164].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23988774">Potential Antimicrobial Agents for the Treatment of MDR-TB.</a> Alsaad, N., B. Wilffert, R. van Altena, W.C. de Lange, T.S. van der Werf, J.G. Kosterink, and J.W. Alffenaar. The European Respiratory Journal, 2013. <b>[Epub ahead of print]</b>. PMID[23988774].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23850570">Synthesis and Antimycobacterial Activity of Analogues of the Bioactive Natural Products Sampangine and Cleistopholine.</a> Claes, P., D. Cappoen, B.M. Mbala, J. Jacobs, B. Mertens, V. Mathys, L. Verschaeve, K. Huygen, and N. De Kimpe. European Journal of Medicinal Chemistry, 2013. 67: p. 98-110. PMID[23850570].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23997233">20. Low-Dose Aspirin and Ibuprofen&#39;s Sterilizing Effects on Mycobacterium tuberculosis Suggest Safe New Adjuvant Therapies for Tuberculosis.</a> Eisen, D.P., E.S. McBryde, and A. Walduck. The Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[23997233].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23836169">Impact of Fluoroquinolone Resistance on Bactericidal and Sterilizing Activity of a Moxifloxacin-Containing Regimen in Murine Tuberculosis.</a> Fillion, A., A. Aubry, F. Brossier, A. Chauffour, V. Jarlier, and N. Veziris. Antimicrobial Agents and Chemotherapy, 2013. 57(9): p. 4496-4500. PMID[23836169].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23886691">Synthesis and Structure-Activity Relationships Evaluation of Benzothiazinone Derivatives as Potential Anti-tubercular Agents.</a> Gao, C., T.H. Ye, N.Y. Wang, X.X. Zeng, L.D. Zhang, Y. Xiong, X.Y. You, Y. Xia, Y. Xu, C.T. Peng, W.Q. Zuo, Y. Wei, and L.T. Yu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4919-4922. PMID[23886691].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23735598">In Vitro Antimicrobial Activity of Squalamine Derivatives against Mycobacteria.</a> Ghodbane, R., S.M. Ameen, M. Drancourt, and J.M. Brunel. Tuberculosis, 2013. 93(5): p. 565-566. PMID[23735598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23822735">Assessment of Clofazimine Activity in a Second-line Regimen for Tuberculosis in Mice.</a> Grosset, J.H., S. Tyagi, D.V. Almeida, P.J. Converse, S.Y. Li, N.C. Ammerman, W.R. Bishai, D. Enarson, and A. Trebucq. American Journal of Respiratory and Critical Care Medicine, 2013. 188(5): p. 608-612. PMID[23822735].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23850103">Synthesis of Novel Isothiazolopyridines and Their in Vitro Evaluation against Mycobacterium and Propionibacterium acnes.</a> Malinka, W., P. Swiatek, M. Sliwinska, B. Szponar, A. Gamian, Z. Karczmarzyk, and A. Fruzinski. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 5282-5291. PMID[23850103].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23823011">Synthesis and Bioevaluation of Some New Isoniazid Derivatives.</a> Matei, L., C. Bleotu, I. Baciu, C. Draghici, P. Ionita, A. Paun, M.C. Chifiriuc, A. Sbarcea, and I. Zarafu. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 5355-5361. PMID[23823011].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24002140">Antimycobacterial and Photosynthetic Electron Transport Inhibiting Activity of Ring-substituted 4-Arylamino-7-chloroquinolinium chlorides.</a> Otevrel, J., P. Bobal, I. Zadrazilova, R. Govender, M. Pesko, S. Keltosova, P. Koleckarova, P. Marsalek, A. Imramovsky, A. Coffey, J. O&#39;Mahony, P. Kollar, A. Cizek, K. Kralova, and J. Jampilek. Molecules, 2013. 18(9): p. 10648-10670. PMID[24002140].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23867166">Synthesis and Structure Activity Relationship of Imidazo[1,2-a]pyridine-8-carboxamides as a Novel Antimycobacterial Lead Series.</a> Ramachandran, S., M. Panda, K. Mukherjee, N.R. Choudhury, S.J. Tantry, C.K. Kedari, V. Ramachandran, S. Sharma, V.K. Ramya, S. Guptha, and V.K. Sambandamurthy. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4996-5001. PMID[23867166].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23891229">Inhibition of Mycobacterium tuberculosis Strains H37Rv and MDR MS-115 by a New Set of C5 Modified Pyrimidine Nucleosides.</a> Shmalenyuk, E.R., L.N. Chernousova, I.L. Karpenko, S.N. Kochetkov, T.G. Smirnova, S.N. Andreevskaya, A.O. Chizhov, O.V. Efremenkova, and L.A. Alexandrova. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 4874-4884. PMID[23891229].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23851117">Synthesis and Antiprotozoal Activity of Original Porphyrin Precursors and Derivatives.</a> Abada, Z., S. Cojean, S. Pomel, L. Ferrie, B. Akagah, A.T. Lormier, P.M. Loiseau, and B. Figadere. European Journal of Medicinal Chemistry, 2013. 67: p. 158-165. PMID[23851117].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23850568">The Effect of Novel [3-Fluoro-(2-phosphonoethoxy)propyl]purines on the Inhibition of Plasmodium falciparum, Plasmodium vivax and Human hypoxanthine-guanine-(xanthine) Phosphoribosyltransferases.</a> Baszczynski, O., D. Hockova, Z. Janeba, A. Holy, P. Jansa, M. Dracinsky, D.T. Keough, and L.W. Guddat. European Journal of Medicinal Chemistry, 2013. 67: p. 81-89. PMID[23850568].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23645588">A Plasmodium falciparum Screening Assay for Anti-gametocyte Drugs Based on Parasite Lactate Dehydrogenase Detection.</a> D&#39;Alessandro, S., F. Silvestrini, K. Dechering, Y. Corbett, S. Parapini, M. Timmerman, L. Galastri, N. Basilico, R. Sauerwein, P. Alano, and D. Taramelli. The Journal of Antimicrobial Chemotherapy, 2013. 68(9): p. 2048-2058. PMID[23645588].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23988087">Improvement of the Efficacy of Dihydroartemisinin with Atorvastatin in an Experimental Cerebral Malaria Murine Model.</a> Dormoi, J., S. Briolant, A. Pascual, C. Desgrouas, C. Travaille, and B. Pradines. Malaria Journal, 2013. 12(1): p. 302. PMID[23988087].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br />   

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23880082">Antiprotozoal Activity of Bicyclic Diamines with a N-Methylpiperazinyl Group at the Bridgehead Atom.</a> Faist, J., W. Seebacher, M. Kaiser, R. Brun, R. Saf, and R. Weis. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 4988-4996. PMID[23880082].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23871905">Synthesis of Novel Analogs of 2-Pyrazoline Obtained from [(7-Chloroquinolin-4-yl)amino]chalcones and Hydrazine as Potential Antitumor and Antimalarial Agents.</a> Insuasty, B., A. Montoya, D. Becerra, J. Quiroga, R. Abonia, S. Robledo, I.D. Velez, Y. Upegui, M. Nogueras, and J. Cobo. European Journal of Medicinal Chemistry, 2013. 67: p. 252-262. PMID[23871905].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23871911">Synthesis and Antiprotozoal Activities of Benzyl phenyl ether diamidine Derivatives.</a> Patrick, D.A., S.A. Bakunov, S.M. Bakunova, S.K. Jones, T. Wenzler, T. Barszcz, A. Kumar, D.W. Boykin, K.A. Werbovetz, R. Brun, and R.R. Tidwell. European Journal of Medicinal Chemistry, 2013. 67: p. 310-324. PMID[23871911].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23902828">Synthesis of Polysubstituted Benzofuran Derivatives as Novel Inhibitors of Parasitic Growth.</a> Thevenin, M., S. Thoret, P. Grellier, and J. Dubois. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 4885-4892. PMID[23902828].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23896611">Tetrazole-based Deoxyamodiaquines: Synthesis, ADME/PK Profiling and Pharmacological Evaluation as Potential Antimalarial Agents.</a> Tukulula, M., M. Njoroge, G.C. Mugumbate, J. Gut, P.J. Rosenthal, S. Barteau, J. Streckfuss, O. Heudi, J. Kameni-Tcheudji, and K. Chibale. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 4904-4913. PMID[23896611].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23927599">Cell-based Medicinal Chemistry Optimization of High Throughput Screening Hits for Orally Active Antimalarials. Part 2: Hits from SoftFocus Kinase and Other Libraries</a>. Younis, Y., L.J. Street, D. Waterson, M.J. Witty, and K. Chibale. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23927599].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23987585">Farnesides A and B, Sesquiterpenoid Nucleoside Ethers from a Marine-derived Streptomyces sp., Strain CNT-372 from Fiji.</a> Zafrir Ilan, E., M.R. Torres, J. Prudhomme, K. Le Roch, P.R. Jensen, and W. Fenical. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23987585].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0830-091213.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322850100051">A Novel Amalgamation of 1,2,3-Triazoles, Piperidines and Thieno Pyridine Rings and Evaluation of Their Antifungal Activity.</a> Darandale, S.N., N.A. Mulla, D.N. Pansare, J.N. Sangshetti, and D.B. Shinde. European Journal of Medicinal Chemistry, 2013. 65: p. 527-532. ISI[000322850100051].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322358100003">Microwave Assisted Synthesis of New Coumarin Based 3-Cyanopyridine Scaffolds Bearing Sulfonamide Group Having Antimicrobial Activity.</a> Desai, N.C., H.M. Satodiya, K.M. Rajpara, V.V. Joshi, and H.V. Vaghani. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2013. 52(7): p. 904-914. ISI[000322358100003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323069200082">Chemical Screening Identifies Filastatin, a Small Molecule Inhibitor of Candida albicans Adhesion, Morphogenesis, and Pathogenesis.</a> Fazly, A., C. Jain, A.C. Dehner, L. Issi, E.A. Lilly, A. Ali, H. Cao, P.L. Fidel, R.P. Rao, and P.D. Kaufman. Proceedings of the National Academy of Sciences of the United States of America, 2013. 110(33): p. 13594-13599. ISI[000323069200082].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322944200011">Membrane Active Chelators as Novel Anti-african Trypanosome and Anti-malarial Drugs.</a> Grab, D.J., E. Nenortas, R.P. Bakshi, O.V. Nikolskaia, J.E. Friedman, and T.A. Shapiro. Parasitology International, 2013. 62(5): p. 461-463. ISI[000322944200011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323186600002">Identification of Dihydroorotate Dehydrogenase as a Relevant Drug Target for 1-Hydroxyquinolones in Toxoplasma gondii.</a> Hegewald, J., U. Gross, and W. Bohne. Molecular and Biochemical Parasitology, 2013. 190(1): p. 6-15. ISI[000323186600002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321738400006">Discovery of a Novel Dual Fungal CYP51/Human 5-Lipoxygenase Inhibitor: Implications for Anti-fungal Therapy.</a> Hoobler, E.K., G. Rai, A.G.S. Warrilow, S.C. Perry, C.J. Smyrniotis, A. Jadhav, A. Simeonov, J.E. Parker, D.E. Kelly, D.J. Maloney, S.L. Kelly, and T.R. Holman. Plos One, 2013. 8(6): p. e65928 . ISI[000321738400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322299100018">Synthesis and Biological Evaluation of 3-(4-Chlorophenyl)-4-substituted pyrazole Derivatives.</a> Horrocks, P., M.R. Pickard, H.H. Parekh, S.P. Patel, and R.B. Pathak. Organic &amp; Biomolecular Chemistry, 2013. 11(29): p. 4891-4898. ISI[000322299100018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322601400001">In Vitro Antiviral Activity of Plant Extracts from Asteraceae Medicinal Plants.</a> Jaime, M.F.V., F. Redko, L.V. Muschietti, R.H. Campos, V.S. Martino, and L.V. Cavallaro. Virology Journal, 2013. 10: p. 245. ISI[000322601400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322850100034">Design and Synthesis of Biquinolone-Isoniazid Hybrids as a New Class of Antitubercular and Antimicrobial Agents.</a> Jardosh, H.H. and M.P. Patel. European Journal of Medicinal Chemistry, 2013. 65: p. 348-359. ISI[000322850100034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323227300005">Identification of Antifungal Niphimycin from Streptomyces sp. KP6107 by Screening Based on Adenylate Kinase Assay.</a> Kim, H.Y., J. Do Kim, J.S. Hong, J.H. Ham, and B.S. Kim. Journal of Basic Microbiology, 2013. 53(7): p. 581-589. ISI[000323227300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322108300028">Syntheses, Protonation Constants and Antimicrobial Activity of 2-Substituted N-alkylimidazole Derivatives.</a> Kleyi, P., R.S. Walmsley, I.Z. Gundhla, T.A. Walmsley, T.I. Jauka, J. Dames, R.B. Walker, N. Torto, and Z.R. Tshentu. South African Journal of Chemistry, 2012. 65: p. 231-238. ISI[000322108300028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322316700010">Targeting Iron Acquisition Blocks Infection with the Fungal Pathogens Aspergillus fumigatus and Fusarium oxysporum.</a> Leal, S.M., S. Roy, C. Vareechon, S.D. Carrion, H. Clark, M.S. Lopez-Berges, A. diPietro, M. Schrettl, N. Beckmann, B. Redl, H. Haas, and E. Pearlman. Plos Pathogens, 2013. 9(7): p. e1003436 . ISI[000322316700010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322869700003">Isoniazid-Gelatin Conjugate Microparticles Containing Rifampicin for the Treatment of Tuberculosis.</a> Manca, M.L., R. Cassano, D. Valenti, S. Trombino, T. Ferrarelli, N. Picci, A.M. Fadda, and M. Manconi. Journal of Pharmacy and Pharmacology, 2013. 65(9): p. 1302-1311. ISI[000322869700003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322408100008">A Novel Combinatorial Biocatalytic Approach for Producing Antibacterial Compounds Effective against Mycobacterium tuberculosis (TB).</a> McClay, K., B.J. Wan, Y.H. Wang, S. Cho, J. Yu, B. Santarsiero, S. Mehboob, M. Johnson, S. Franzblau, and R. Steffan. Applied Microbiology and Biotechnology, 2013. 97(16): p. 7151-7163. ISI[000322408100008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">55. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323197400001">Combined Efficacy of Biologically Synthesized Silver Nanoparticles and Different Antibiotics against Multidrug-resistant Bacteria.</a> Naqvi, S.Z.H., U. Kiran, M.I. Ali, A. Jamal, A. Hameed, S. Ahmed, and N. Ali. International Journal of Nanomedicine, 2013. 8: p. 3187-3195. ISI[000323197400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">56. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321500200059">Small Molecule Inhibitors of Trans-translation Have Broad-spectrum Antibiotic Activity.</a> Ramadoss, N.S., J.N. Alumasa, L. Cheng, Y. Wang, S. Li, B.S. Chambers, H. Chang, A.K. Chatterjee, A. Brinker, I.H. Engels, and K.C. Keiler. Proceedings of the National Academy of Sciences of the United States of America, 2013. 110(25): p. 10282-10287. ISI[000321500200059].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">57. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321978000012">Identification of a Small Molecule with Activity against Drug-resistant and Persistent Tuberculosis.</a> Wang, F., D. Sambandan, R. Halder, J.N. Wang, S.M. Batt, B. Weinrick, I. Ahmad, P.Y. Yang, Y. Zhang, J. Kim, M. Hassani, S. Huszar, C. Trefzer, Z.K. Ma, T. Kaneko, K.E. Mdluli, S. Franzblau, A.K. Chatterjee, K. Johnson, K. Mikusova, G.S. Besra, K. Futterer, W.R. Jacobs, and P.G. Schultz. Proceedings of the National Academy of Sciences of the United States of America, 2013. 110(27): p. E2510-E2517. ISI[000321978000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br /> 

    <p class="plaintext">58. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321977300011">Identification and Characterization of Antifungal Active Substances of Streptomyces hygroscopicus BS-112.</a> Zhang, N., Z. Song, Y.H. Xie, P. Cui, H.X. Jiang, T. Yang, R.C. Ju, Y.H. Zhao, J.Y. Li, and X.L. Liu. World Journal of Microbiology and Biotechnology, 2013. 29(8): p. 1443-1452. ISI[000321977300011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0830-091213.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
