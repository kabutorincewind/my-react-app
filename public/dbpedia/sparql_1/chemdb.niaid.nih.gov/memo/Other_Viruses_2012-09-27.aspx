

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-09-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9aUBOSc7dY34DaYNTRDsK6CoDUJ1RVFbXaTEjsGlj6yHfp5sXdDY+rgIQTEfD7iyBj7BbQPjOdJTK/92ABQn4cVeMsIz3CkGQOCJiR0hGrgnNZDf2WhUtbm/bbY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ECD2FAF7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: September 14 - September 27, 2012</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22989264">Combination of Fluvastatin with Pegylated Interferon/Ribavirin Therapy Reduces Viral Relapse in Chronic Hepatitis C Infected with HCV Genotype 1B.</a> Atsukawa, M., A. Tsubota, C. Kondo, N. Itokawa, Y. Narahara, K. Nakatsuka, S. Hashimoto, T. Fukuda, Y. Matsushita, H. Kidokoro, T. Kobayashi, H. Kanazawa, and C. Sakamoto. Journal of Gastroenterology and Hepatology, 2012.  <b>[Epub ahead of print]</b>; PMID[22989264].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23009750">Small Molecule Inhibitors of the Hepatitis C Virus-encoded NS5A Protein.</a> Belda, O. and P. Targett-</p>

    <p class="plaintext">Adams. Virus Research, 2012.  <b>[Epub ahead of print]</b>; PMID[23009750].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22892115">Beta-D-2&#39;-alpha-F-2&#39;-beta-C-Methyl-6-O-substituted 3&#39;,5&#39;-cyclic phosphate nucleotide Prodrugs as Inhibitors of Hepatitis C Virus Replication: A Structure-Activity Relationship Study.</a> Du, J., D. Bao, B.K. Chun, Y. Jiang, P. Ganapati Reddy, H.R. Zhang, B.S. Ross, S. Bansal, H. Bao, C. Espiritu, A.M. Lam, E. Murakami, C. Niu, H.M. Micolochick Steuer, P.A. Furman, M.J. Otto, and M.J. Sofia. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(18): p. 5924-5929; PMID[22892115].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23010622">Exploration of the in Vitro Antiviral Activity of a Series of New Pyrimidine Analogues on the Replication of HIV and HCV<i>.</i></a> Jafar, N.N., N.A. Al-Masoudi, S.J. Baqir, P. Leyssen, and C. Pannecouque. Antiviral Chemistry &amp; Chemotherapy, 2012.  <b>[Epub ahead of print]</b>; PMID[23010622].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23011959">Silymarin for HCV Infection.</a> Polyak, S.J., N.H. Oberlies, E.I. Pecheur, P. Ferenci, and J.M. Pawlotsky. Antiviral Therapy, 2012.  <b>[Epub ahead of print]</b>; PMID[23011959].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22884577">Conjugation of Cyclodextrin with Fullerene as a New Class of HCV Entry Inhibitors.</a> Xiao, S.L., Q. Wang, F. Yu, Y.Y. Peng, M. Yang, M. Sollogoub, P. Sinay, Y.M. Zhang, L.H. Zhang, and D.M. Zhou. Bioorganic &amp; Medicinal Chemistry, 2012. 20(18): p. 5616-5622; PMID[22884577].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23006904">A Natural Antiviral and Immunomodulatory Compound with Antiangiogenic Properties.</a> Bueno, C.A., M.G. Lombardi, M.E. Sales, and L.E. Alche. Microvascular Research, 2012.  <b>[Epub ahead of print]</b>; PMID[23006904].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22982541">Sterol Analogues with Diamide Side Chains Interfere with the Intracellular Localization of Viral Glycoproteins.</a> Davola, M.E., F. Alonso, G.M. Cabrera, J.A. Ramirez, and A.A. Barquero. Biochemical and Biophysical Research Communications, 2012.  <b>[Epub ahead of print]</b>; PMID[22982541].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22753591">Copper Chelation Enhances Antitumor Efficacy and Systemic Delivery of Oncolytic HSV.</a> Yoo, J.Y., J. Pradarelli, A. Haseley, J. Wojton, A. Kaka, A. Bratasz, C.A. Alvarez-Breckenridge, J.G. Yu, K. Powell, A.P. Mazar, T.N. Teknos, E.A. Chiocca, J.C. Glorioso, M. Old, and B. Kaur. Clinical Cancer Research, 2012. 18(18): p. 4931-4941; PMID[22753591].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <h2>Epstein Barr Virus</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22985630">Resveratrol Inhibits Epstein Barr Virus Lytic Cycle in Burkitt&#39;s Lymphoma Cells by Affecting Multiple Molecular Targets.</a> Alessandra, D.L., A. Giuseppe, L. Egidio, O. Giorgio, C. Francesca, and M. Elena. Antiviral Research, 2012.  <b>[Epub ahead of print];</b> PMID[22985630].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0914-092712.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">11. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=7&amp;SID=1ApB12l1MJF4bFPk@D8&amp;page=1&amp;doc=1">Limited Effect on NS3-NS4A Protein Cleavage after Alanine Substitutions within the Immunodominant HLA-A2-Restricted Epitope of the Hepatitis C Virus Genotype 3a Non-Structural 3/4A Protease.</a> Ahlen, G., A. Chen, B. Roe, T. Falkeborn, L. Frelin, W.W. Hall, M. Sallberg, and J. Soderholm. Journal of General Virology, 2012. 93: p. 1680-1686; ISI[000307746100007].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=6&amp;SID=1ApB12l1MJF4bFPk@D8&amp;page=1&amp;doc=1">Inhibitory Effects of Fluvastatin on Hepatitis C Virus Involves the Doublecortin and Cam Kinase-like-1 (DCAMKL-1) Stem Cell Marker.</a> Ali, N., H. Allam, T. Bader, R. May, S. Umar, and C.W. Houchen. Gastroenterology, 2012. 142(5): p. S696-S696; ISI[000306994303675].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=5&amp;SID=1ApB12l1MJF4bFPk@D8&amp;page=1&amp;doc=1">Synthesis of New Pentaheterocyclic Ring Systems as Anti-androgene, anti-HCV and anti-H1N1 Agents.</a> Farghaly, T.A., I.M. Abbas, M.M. Abdalla, and R.O.A. Mahgoub. Arkivoc, 2012: p. 57-70; ISI[000307877800006].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=4&amp;SID=1ApB12l1MJF4bFPk@D8&amp;page=1&amp;doc=1">CVVS-732: A Conformational Modulator of NS5B Polymerase Acting a Potent HCV Inhibitor.</a> Guedat, P., I. Valarche, M. Lahmar, A. Berecibar, L. Batard, A. Bertrand, A. Martin, A. Piessens, C. Fernagut, C. Freslon-Evain, L. Besnard, M. Ventura, T. Durand, C. Feray, and M. Mehtali. Gastroenterology, 2012. 142(5): p. S963-S963; ISI[000306994305342].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=2&amp;SID=1ApB12l1MJF4bFPk@D8&amp;page=1&amp;doc=1">Boron Containing Compounds as Protease Inhibitors.</a> Smoum, R., A. Rubinstein, V.M. Dembitsky, and M. Srebnik. Chemical Reviews, 2012. 112(7): p. 4156-4220; ISI[000306298800014].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0914-092712.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">16. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=1&amp;SID=1ApB12l1MJF4bFPk@D8&amp;page=1&amp;doc=1">Efficient Inhibition of HIV-1 Replication by an Artificial Polycistronic miRNA Construct.</a> Zhang, T., T. Cheng, L.H. Wei, Y.J. Cai, A.E. Yeo, J.H. Han, Y.A. Yuan, J. Zhang, and N.S. Xia. Virology Journal, 2012. 9: p. 118; ISI[000307410000001].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0914-092712.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
