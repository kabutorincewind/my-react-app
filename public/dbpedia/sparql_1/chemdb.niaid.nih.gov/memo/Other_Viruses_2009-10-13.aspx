

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-10-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6lgrFVE0zxZlQkX1Hqkts3LtP9vrQc4OQY79dqPapikjvdcpv/oN4VLX2Q3YWDCw/2pRkv1v8uMuwhOuIwfF/nOZl0Cks+DEQ/CBtI8fqlqKb16/vLgo/MdtPdA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C1D07B9B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List:</p>

    <p class="memofmt2-h1">September 30, 2009 -October 13, 2009</p>

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19812153?ordinalpos=31&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of Hepatitis C Virus NS5A Inhibitors.</a></p>

    <p class="NoSpacing">Lemm JA, O&#39;Boyle D 2nd, Liu M, Nower PT, Colonno R, Deshpande MS, Snyder LB, Martin SW, St Laurent DR, Serrano-Wu MH, Romine JL, Meanwell NA, Gao M.</p>

    <p class="NoSpacing">J Virol. 2009 Oct 7. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19812153 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19800789?ordinalpos=50&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">2-(3-Thienyl)-5,6-dihydroxypyrimidine-4-carboxylic acids as inhibitors of HCV NS5B RdRp.</a></p>

    <p class="NoSpacing">Pacini B, Avolio S, Ercolani C, Koch U, Migliaccio G, Narjes F, Pacini L, Tomei L, Harper S.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Nov 1;19(21):6245-9. Epub 2009 Jul 17.</p>

    <p class="NoSpacing">PMID: 19800789 [PubMed - in process]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19796940?ordinalpos=61&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design and efficient synthesis of novel arylthiourea derivatives as potent hepatitis C virus inhibitors.</a></p>

    <p class="NoSpacing">Kang IJ, Wang LW, Hsu SJ, Lee CC, Lee YC, Wu YS, Yueh A, Wang JC, Hsu TA, Chao YS, Chern JH.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Nov 1;19(21):6063-8. Epub 2009 Sep 17.</p>

    <p class="NoSpacing">PMID: 19796940 [PubMed - in process]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19780760?ordinalpos=84&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Dimethylthiazolidine carboxylic acid as a rigid p3 unit in inhibitors of serine proteases: application to two targets.</a></p>

    <p class="NoSpacing">Kawai SH, Aubry N, Duceppe JS, Llinàs-Brunet M, LaPlante SR.</p>

    <p class="NoSpacing">Chem Biol Drug Des. 2009 Nov;74(5):517-22. Epub 2009 Sep 22.</p>

    <p class="NoSpacing">PMID: 19780760 [PubMed - in process]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19747948?ordinalpos=113&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A novel HCV NS3 protease mutation selected by combination treatment of the protease inhibitor boceprevir and NS5B polymerase inhibitors.</a></p>

    <p class="NoSpacing">Chase R, Skelton A, Xia E, Curry S, Liu S, McMonagle P, Huang HC, Tong X.</p>

    <p class="NoSpacing">Antiviral Res. 2009 Nov;84(2):178-84. Epub 2009 Sep 10.</p>

    <p class="NoSpacing">PMID: 19747948 [PubMed - in process]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19710151?ordinalpos=131&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Blocking hepatitis C virus infection with recombinant form of envelope protein 2 ectodomain.</a></p>

    <p class="NoSpacing">Whidby J, Mateu G, Scarborough H, Demeler B, Grakoui A, Marcotrigiano J.</p>

    <p class="NoSpacing">J Virol. 2009 Nov;83(21):11078-89. Epub 2009 Aug 26.</p>

    <p class="NoSpacing">PMID: 19710151 [PubMed - in process]</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19493591?ordinalpos=206&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and evaluation of A-seco type triterpenoids for anti-HIV-1protease activity.</a></p>

    <p class="NoSpacing">Wei Y, Ma CM, Hattori M.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Oct;44(10):4112-20. Epub 2009 May 15.</p>

    <p class="NoSpacing">PMID: 19493591 [PubMed - indexed for MEDLINE]</p>

    <p class="memofmt2-2">Hepatitis B Virus</p>

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19733548?ordinalpos=93&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of hepatitis B virus replication with linear DNA sequences expressing antiviral micro-RNA shuttles.</a></p>

    <p class="NoSpacing">Chattopadhyay S, Ely A, Bloom K, Weinberg MS, Arbuthnot P.</p>

    <p class="NoSpacing">Biochem Biophys Res Commun. 2009 Nov 20;389(3):484-9. Epub 2009 Sep 4.</p>

    <p class="NoSpacing">PMID: 19733548 [PubMed - in process]</p>

    <p class="memofmt2-2">Herpes Simplex Virus</p> 

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19808846?ordinalpos=26&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Excoecarianin, Isolated from Phyllanthus urinaria Linnea, Inhibits Herpes Simplex Virus Type 2 Infection through Inactivation of Viral Particles.</a></p>

    <p class="NoSpacing">Cheng HY, Yang CM, Lin TC, Lin LT, Chiang LC, Lin CC.</p>

    <p class="NoSpacing">Evid Based Complement Alternat Med. 2009 Oct 6. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19808846 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19800369?ordinalpos=36&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antiviral evaluation of octadecyloxyethyl esters of (S)-3-hydroxy-2-(phosphonomethoxy)propyl nucleosides against herpesviruses and orthopoxviruses.</a></p>

    <p class="NoSpacing">Valiaeva N, Prichard MN, Buller RM, Beadle JR, Hartline CB, Keith KA, Schriewer J, Trahan J, Hostetler KY.</p>

    <p class="NoSpacing">Antiviral Res. 2009 Oct 1. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19800369 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19647908?ordinalpos=83&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and antiviral activity of beta-carboline derivatives bearing a substituted carbohydrazide at C-3 against poliovirus and herpes simplex virus (HSV-1).</a></p>

    <p class="NoSpacing">Nazari Formagio AS, Santos PR, Zanoli K, Ueda-Nakamura T, Düsman Tonin LT, Nakamura CV, Sarragiotto MH.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Nov;44(11):4695-701. Epub 2009 Jul 16.</p>

    <p class="NoSpacing">PMID: 19647908 [PubMed - in process]</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">12. Kim, SJ., et al</p>

    <p class="NoSpacing">Suppression of hepatitis C virus replication by protein kinase C-related kinase 2 inhibitors that block phosphorylation of viral RNA polymerase</p>

    <p class="NoSpacing">JOURNAL OF VIRAL HEPATITIS  (2009) October  16: 697 - 704</p> 

    <p class="ListParagraph">13. Venkatraman, S., and Njoroge, FG</p>

    <p class="NoSpacing">Macrocyclic inhibitors of HCV NS3 protease</p>

    <p class="NoSpacing">EXPERT OPINION ON THERAPEUTIC PATENTS  2009 (September) 19: 1277 - 1303</p> 

    <p class="ListParagraph">14. de Vicente, Javier, et al</p>

    <p class="NoSpacing">Non-nucleoside inhibitors of HCV polymerase NS5B. Part 3: Synthesis and optimization studies of benzothiazine-substituted tetramic acids</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS   2009 (October) 19: 5648 - 5651</p> 

    <p class="ListParagraph">15. de Vicente, Javier, et al</p>

    <p class="NoSpacing">Non-nucleoside inhibitors of HCV polymerase NS5B. Part 4: Structure-based design, synthesis, and biological evaluation of benzo[d]isothiazole-1,1-dioxides</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS   2009 (October) 19: 5652 - 5656</p> 

    <p class="ListParagraph">16. Boloor, A., et al.</p>

    <p class="NoSpacing">Synthesis and antiviral activity of HCV NS3/4A peptidomimetic boronic acid inhibitors</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS   2009 (October) 19: 5708- 5711</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
