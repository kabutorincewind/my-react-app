

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-352.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bVYpPRWL2xzL89G2E42y0QKfNFNWV87rpeliANZ10D50o7QHuN1hWnjYdaKxzrZucDRPUSCRqcTdEprRkIzgjf6L9nXrA9M2C6Wa2wBEPuYaXVQh6ZpYdTX5Bok=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="943643B3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-352-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49135   OI-LS-352; EMBASE-OI-7/10/2006</p>

    <p class="memofmt1-2">          A duplicated nitrotienyl derivative with antimycobacterial activity: synthesis, X-ray crystallography, biological and mutagenic activity tests</p>

    <p>          Rando, DG, Doriguetto, AC, Tomich de Paula da Silva, CH, Ellena, J, Sato, DN, Leite, CQF, Varanda, EA, and Ferreira, EI</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4KBVTY8-3/2/cded80f1dc5f53841da97a1f46cf1e55">http://www.sciencedirect.com/science/article/B6VKY-4KBVTY8-3/2/cded80f1dc5f53841da97a1f46cf1e55</a> </p><br />

    <p>2.     49136   OI-LS-352; EMBASE-OI-7/10/2006</p>

    <p class="memofmt1-2">          Purification and characterization of a functionally active Mycobacterium tuberculosis prephenate dehydrogenase</p>

    <p>          Xu, Shengfeng, Yang, Yanping, Jin, Ruiliang, Zhang, Min, and Wang, Honghai</p>

    <p>          Protein Expression and Purification <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WPJ-4K6CHSG-1/2/81d6b77d7993606a415a3f0f7af18c7a">http://www.sciencedirect.com/science/article/B6WPJ-4K6CHSG-1/2/81d6b77d7993606a415a3f0f7af18c7a</a> </p><br />

    <p>3.     49137   OI-LS-352; EMBASE-OI-7/10/2006</p>

    <p class="memofmt1-2">          Aggravated infection in mice co-administered with Mycobacterium tuberculosis and the 27-kDa lipoprotein</p>

    <p>          Hovav, Avi-Hai, Mullerad, Jacob, Maly, Alexander, Davidovitch, Liuba, Fishman, Yolanta, and Bercovier, Herve</p>

    <p>          Microbes and Infection <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4JT3NKC-1/2/1ec179c3d9c3ef5bede55a0ec6de7860">http://www.sciencedirect.com/science/article/B6VPN-4JT3NKC-1/2/1ec179c3d9c3ef5bede55a0ec6de7860</a> </p><br />

    <p>4.     49138   OI-LS-352; EMBASE-OI-7/10/2006</p>

    <p class="memofmt1-2">          Selective inhibitors of hepatitis C virus replication</p>

    <p>          Neyts, Johan</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K7X85V-2/2/a374f7b3e0fffabd6e58d743759153d7">http://www.sciencedirect.com/science/article/B6T2H-4K7X85V-2/2/a374f7b3e0fffabd6e58d743759153d7</a> </p><br />

    <p>5.     49139   OI-LS-352; EMBASE-OI-7/10/2006</p>

    <p class="memofmt1-2">          Pivotal role of animal models in the development of new therapies for cytomegalovirus infections</p>

    <p>          Kern, Earl R</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K7158K-1/2/9e5607b0c5ecb59e13372e402182e561">http://www.sciencedirect.com/science/article/B6T2H-4K7158K-1/2/9e5607b0c5ecb59e13372e402182e561</a> </p><br />
    <br clear="all">

    <p>6.     49140   OI-LS-352; EMBASE-OI-7/10/2006</p>

    <p class="memofmt1-2">          Experimental study on the action of allitridin against human cytomegalovirus in vitro: Inhibitory effects on immediate-early genes</p>

    <p>          Zhen, Hong, Fang, Feng, Ye, Du-yun, Shu, Sai-nan, Zhou, Yu-feng, Dong, Yong-sui, Nie, Xing-cao, and Li, Ge</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JTRM36-2/2/733b5e0322103a072aee55a5f6c24151">http://www.sciencedirect.com/science/article/B6T2H-4JTRM36-2/2/733b5e0322103a072aee55a5f6c24151</a> </p><br />

    <p>7.     49141   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Purification and characterization of Mycobacterium tuberculosis 1D-myo-inosityl-2-acetamido-2-deoxy-alpha-D-glucopyranoside deacetylase, MshB, a mycothiol biosynthetic enzyme</p>

    <p>          Newton, GL, Ko, M, Ta, PL, Av-Gay, Y, and Fahey, RC</p>

    <p>          PROTEIN EXPRESSION AND PURIFICATION: Protein Expr. Purif <b>2006</b>.  47(2): 542-550, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238277000027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238277000027</a> </p><br />

    <p>8.     49142   OI-LS-352; WOS-OI-7/2/2006</p>

    <p class="memofmt1-2">          Newer triazole antifungal agents: pharmacology, spectrum, clinical efficacy and limitations</p>

    <p>          Aperis, G and Mylonakis, E</p>

    <p>          EXPERT OPINION ON INVESTIGATIONAL DRUGS: Expert Opin. Investig. Drugs <b>2006</b>.  15(6): 579-602, 24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238376600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238376600002</a> </p><br />

    <p>9.     49143   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Cytokine and chemokine networks: Pathways to antiviral defense</p>

    <p>          Salazar-Mather, TP and Hokeness, KL</p>

    <p>          CHEMOKINES AND VIRAL INFECTION: Curr.Top.Microbiol.Immunol <b>2006</b>.  303: 29-46, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237591600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237591600002</a> </p><br />

    <p>10.   49144   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 5-(alkyn-1-yl)-1-(p-toluenesulfonyl)uracil derivatives</p>

    <p>          Janeba, Z, Balzarini, J, Andrei, G, Snoeck, R, De Clercq, E, and Robins, MJ</p>

    <p>          CANADIAN JOURNAL OF CHEMISTRY-REVUE CANADIENNE DE CHIMIE: Can. J. Chem.-Rev. Can. Chim <b>2006</b>.  84 (4): 580-586, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238285200016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238285200016</a> </p><br />

    <p>11.   49145   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          m-tyrosine- and tic-based macrocyclic inhibitors of Hepatitis C Virus (HCV) NS3 protease: Synthesis and biological activity</p>

    <p>          Chen, KX, Njoroge, FG, Pichardo, J, Prongay, A, Butkiewicz, N, Yao, NH, Madison, V, and Girijavallabhan, V</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY: Abstr. Pap. Am. Chem. Soc <b>2005</b>.  230: U2602-U2603, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305162">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305162</a> </p><br />
    <br clear="all">

    <p>12.   49146   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Hepatitis C virus NS3 serine protease inhibitors: Discovery of potent P2 &#39; moiety</p>

    <p>          Arasappan, A, Njoroge, FG, Chan, TY, Bennett, F, Bogen, SL, Chen, K, Gu, H, Hong, L, Jao, E, Liu, YT, Lovey, RG, Parekh, T, Pike, RE, Pinto, P, Santhanam, B, Venkatraman, S, Vaccaro, H, Wang, H, Yang, X, Zhu, Z, Patel, N, McCormick, J, McKittrick, B, Saksena, AK, Girijavallabhan, V, Pichardo, J, Butkiewicz, N, Ingram, R, Malcolm, B, Prongay, A, Yao, N, Marten, B, Madison, V, Kemp, S, Levy, O, Lim-Wilby, M, Tamura, S, and Ganguly, AK</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY: Abstr. Pap. Am. Chem. Soc <b>2005</b>.  230: U2603-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305163">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305163</a> </p><br />

    <p>13.   49147   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Novel 2-oxoimidazolidine-4-carboxylic acid derivatives as hepatitis C virus NS3 serine protease inhibitors</p>

    <p>          Arasappan, A, Njoroge, FG, Parekh, TN, Pichardo, J, Butkiewicz, N, Prongay, A, Yao, NH, and Girijavallabhan, V</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY: Abstr. Pap. Am. Chem. Soc <b>2005</b>.  230: U2604-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305164">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305164</a> </p><br />

    <p>14.   49148   OI-LS-352; WOS-OI-7/2/2006</p>

    <p class="memofmt1-2">          P2 diamino acid derivatives as hepatitis C virus NS3 serine protease inhibitors</p>

    <p>          Arasappan, A, Njoroge, FG, Gu, HN, Parekh, TN, Pichardo, J, Butkiewicz, N, Prongay, A, Yao, NH, and Girijavallabhan, V</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY: Abstr. Pap. Am. Chem. Soc <b>2005</b>.  230: U2604-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305165">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305165</a> </p><br />

    <p>15.   49149   OI-LS-352; WOS-OI-7/2/2006</p>

    <p class="memofmt1-2">          Highly stereoselective synthesis of bicyclic-acetal-based macrocyclic inhibitors of hepatitis C virus</p>

    <p>          Chen, K and Njoroge, FG</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY: Abstr. Pap. Am. Chem. Soc <b>2005</b>.  230: U3341-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797306520">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797306520</a> </p><br />

    <p>16.   49150   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Synthesis and antifungal properties of some benzimidazole derivatives</p>

    <p>          Kilcigil, GA and Altanlar, N</p>

    <p>          TURKISH JOURNAL OF CHEMISTRY: Turk. J. Chem <b>2006</b>.  30(2): 223-228, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238191600010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238191600010</a> </p><br />

    <p>17.   49151   OI-LS-352; WOS-OI-7/2/2006</p>

    <p class="memofmt1-2">          Evaluation of antiparasitic, antituberculosis and antiangiogenic activities of 3-aminoquinolin-2-one derivatives</p>

    <p>          Muscia, GC, Bollini, M, Bruno, AM, and Asis, SE</p>

    <p>          JOURNAL OF THE CHILEAN CHEMICAL SOCIETY: J. Chil. Chem. Soc <b>2006</b>.  51(2): 859-863, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238131700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238131700005</a> </p><br />

    <p>18.   49152   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Discovery of new antitubercular oxazolyl thiosemicarbazones</p>

    <p>          Sriram, D, Yogeeswari, P, Thirumurugan, R, and Pavana, RK</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY: J. Med. Chem <b>2006</b>.  49(12): 3448-3450, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238114700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238114700005</a> </p><br />

    <p>19.   49153   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Effect of various pyrimidines possessing the 1-[(2-hydroxy-1-(hydroxymethyl)ethoxy)methyl] moiety, able to mimic natural 2 &#39;-deoxyribose, on wild-type and mutant hepatitis B virus replication</p>

    <p>          Kumar, R, Semaine, W, Johar, M, Tyrrell, DLJ, and Agrawal, B</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY: J. Med. Chem <b>2006</b>.  49(12): 3693-3700, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238114700031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238114700031</a> </p><br />

    <p>20.   49154   OI-LS-352; WOS-OI-7/2/2006</p>

    <p class="memofmt1-2">          Hepatotoxicity and antiretroviral therapy with protease inhibitors: A review</p>

    <p>          Bruno, R, Sacchi, P, Maiocchi, L, Patruno, S, and Filice, G</p>

    <p>          DIGESTIVE AND LIVER DISEASE: Dig. Liver Dis <b>2006</b>.  38(6): 363-373, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238141900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238141900001</a> </p><br />

    <p>21.   49155   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Nitroimidazoles - Part 2 - Synthesis, antiviral and antitumor activity of new 4-nitroimidazoles</p>

    <p>          Al-Masoudi, NA, Al-Soud, YA, Kalogerakis, A, Pannecouque, C, and De Clercq, E</p>

    <p>          CHEMISTRY &amp; BIODIVERSITY: Chem. Biodivers <b>2006</b>.  3(5): 515-526, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238084000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238084000004</a> </p><br />

    <p>22.   49156   OI-LS-352; WOS-OI-7/2/2006</p>

    <p class="memofmt1-2">          Inhibitors of HCVNS5B polymerase: Synthesis and structure-activity relationships of N-alkyl-4-hydroxyquinolon-3-yl-benzothiadiazine sulfamides</p>

    <p>          Krueger, AC, Madigan, DL, Jiang, WW, Kati, WM, Liu, DC, Liu, Y, Maring, CJ, Masse, S, McDaniel, KF, Middleton, T, Mo, HM, Molla, A, Montgomery, D, Pratt, JK, Rockway, TW, Zhang, R, and Kempf, DJ</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS: Bioorg. Med. Chem. Lett <b>2006</b>.  16(13): 3367-3370, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238182700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238182700003</a> </p><br />

    <p>23.   49157   OI-LS-352; WOS-OI-7/2/2006</p>

    <p class="memofmt1-2">          The 2.15 angstrom crystal structure of Mycobacterium tuberculosis chorismate mutase reveals an unexpected gene duplication and suggests a role in host-pathogen interactions</p>

    <p>          Qamra, R, Prakash, P, Aruna, B, Hasnain, SE, and Mande, SC</p>

    <p>          BIOCHEMISTRY: Biochemistry <b>2006</b>.  45(23): 6997-7005, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238047600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238047600001</a> </p><br />
    <br clear="all">

    <p>24.   49158   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          In vitro activities of posaconazole, fluconazole, itraconazole, voriconazole, and amphotericin B against a large collection of clinically important molds and yeasts</p>

    <p>          Sabatelli, F, Patel, R, Mann, PA, Mendrick, CA, Norris, CC, Hare, R, Loebenberg, D, Black, TA, and McNicholas, PM</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY: Antimicrob. Agents Chemother <b>2006</b>.  50( 6): 2009-2015, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237932200015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237932200015</a> </p><br />

    <p>25.   49159   OI-LS-352; WOS-OI-7/9/2006</p>

    <p class="memofmt1-2">          Expression, purification and preliminary crystallographic analysis of the Toxoplasma gondii enoyl reductase</p>

    <p>          Muench, SP, Prigge, ST, Zhu, LQ, Kirisits, MJ, Roberts, CW, Wernimont, S, McLeod, R, and Rice, DW</p>

    <p>          ACTA CRYSTALLOGRAPHICA SECTION F-STRUCTURAL BIOLOGY AND CRYSTALLIZATION COMMUNICATIONS: Acta Crystallogr. F-Struct. Biol. Cryst. Commun <b>2006</b>.  62: 604-606, 3</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238067600032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238067600032</a> </p><br />

    <p>26.   49160   OI-LS-352; PUBMED-OI-7/10/2006</p>

    <p class="memofmt1-2">          New antiviral agents</p>

    <p>          Abdel-Haq, N, Chearskul, P, Al-Tatari, H, and Asmar, B</p>

    <p>          Indian J Pediatr <b>2006</b>.  73(4): 313-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16816493&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16816493&amp;dopt=abstract</a> </p><br />

    <p>27.   49161   OI-LS-352; PUBMED-OI-7/10/2006</p>

    <p class="memofmt1-2">          Hepatitis C virus entry depends on clathrin-mediated endocytosis</p>

    <p>          Blanchard, E, Belouzard, S, Goueslain, L, Wakita, T, Dubuisson, J, Wychowski, C, and Rouille, Y</p>

    <p>          J Virol <b>2006</b>.  80(14): 6964-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809302&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809302&amp;dopt=abstract</a> </p><br />

    <p>28.   49162   OI-LS-352; PUBMED-OI-7/10/2006</p>

    <p class="memofmt1-2">          Antisense Oligonucleotide Inhibition of Hepatitis C Virus Genotype 4 Replication in HepG2 Cells</p>

    <p>          El Awady, MK,  Badr, El Din NG, El, Garf WT, Youssef, SS, Omran, MH, Elabd, J, and Goueli, SA</p>

    <p>          Cancer Cell Int <b>2006</b>.  6(1): 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16803625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16803625&amp;dopt=abstract</a> </p><br />

    <p>29.   49163   OI-LS-352; PUBMED-OI-7/10/2006</p>

    <p class="memofmt1-2">          Effect of cell culture conditions on the anticytomegalovirus activity of maribavir</p>

    <p>          Chou, S, Van Wechel, LC, and Marousek, GI</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(7): 2557-2559</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801445&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801445&amp;dopt=abstract</a> </p><br />

    <p>30.   49164   OI-LS-352; PUBMED-OI-7/10/2006</p>

    <p class="memofmt1-2">          Different anti-HCV profiles of statins and their potential for combination therapy with interferon</p>

    <p>          Ikeda, M, Abe, K, Yamada, M, Dansako, H, Naka, K, and Kato, N</p>

    <p>          Hepatology <b>2006</b>.  44(1): 117-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16799963&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16799963&amp;dopt=abstract</a> </p><br />

    <p>31.   49165   OI-LS-352; PUBMED-OI-7/10/2006</p>

    <p class="memofmt1-2">          Cost-Effectiveness of Treating Multidrug-Resistant Tuberculosis</p>

    <p>          Resch, SC, Salomon, JA, Murray, M, and Weinstein, MC</p>

    <p>          PLoS Med <b>2006</b>.  3(7): e241</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16796403&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16796403&amp;dopt=abstract</a> </p><br />

    <p>32.   49166   OI-LS-352; PUBMED-OI-7/10/2006</p>

    <p class="memofmt1-2">          Usefulness of acr Expression for Monitoring Latent Mycobacterium tuberculosis Bacilli in &#39;In Vitro&#39; and &#39;In Vivo&#39; Experimental Models</p>

    <p>          Gordillo, S, Guirado, E, Gil, O, Diaz, J, Amat, I, Molinos, S, Vilaplana, C, Ausina, V, and Cardona, PJ</p>

    <p>          Scand J Immunol <b>2006</b>.  64(1): 30-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16784488&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16784488&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
