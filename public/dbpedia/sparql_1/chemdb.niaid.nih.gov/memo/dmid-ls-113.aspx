

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-113.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="aAhpm0sUjnKDXeYbws1WJuGDt0q3BdIH/VFuSGqnW1xtZ8J8m9+6Vzz58a6QsqlPlGcW6dbIYJxMaarQhmW7Gg4Xog64XW3ZyLYCFkM4geKFweIMLUVJHZTJa3g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B83D12D7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-113-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58219   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Tetrahydro-b-carboline derivatives useful for the treatment of human papillomavirus infection</p>

    <p>          Gudmundsson, Kristjan, Miller, John Franklin, Sherrill, Ronald George, and Turner, Elizabeth Madalena</p>

    <p>          PATENT:  WO <b>2006015035</b>  ISSUE DATE:  20060209</p>

    <p>          APPLICATION: 2005  PP: 51 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     58220   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cidofovir peptide conjugates as prodrugs, their preparation, and therapeutic use thereof</p>

    <p>          McKenna, Charles E</p>

    <p>          PATENT:  WO <b>2006014429</b>  ISSUE DATE:  20060209</p>

    <p>          APPLICATION: 2005  PP: 30 pp.</p>

    <p>          ASSIGNEE:  (University of Southern California (LA), USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     58221   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Microwave-Assisted Synthesis and Anti-Yfv Activity of 2,3-Diaryl-1,3-Thiazolidin-4-Ones</p>

    <p>          Sriram, D., Yogeeswari, P., and Kumar, T.</p>

    <p>          Journal of Pharmacy and Pharmaceutical Sciences <b>2005</b>.  8(3 ): 426-429</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235183400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235183400007</a> </p><br />

    <p>4.     58222   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro characterization of the anti-hepatitis B virus activity and cross-resistance profile of 2&#39;,3&#39;-dideoxy-3&#39;-fluoroguanosine</p>

    <p>          Jacquard, AC, Brunelle, MN, Pichoud, C, Durantel, D, Carrouee-Durantel, S, Trepo, C, and Zoulim, F</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(3): 955-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495257&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495257&amp;dopt=abstract</a> </p><br />

    <p>5.     58223   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preclinical Profile of VX-950, a Potent, Selective, and Orally Bioavailable Inhibitor of Hepatitis C Virus NS3-4A Serine Protease</p>

    <p>          Perni, RB, Almquist, SJ, Byrn, RA, Chandorkar, G, Chaturvedi, PR, Courtney, LF, Decker, CJ, Dinehart, K, Gates, CA, Harbeson, SL, Heiser, A, Kalkeri, G, Kolaczkowski, E, Lin, K, Luong, YP, Rao, BG, Taylor, WP, Thomson, JA, Tung, RD, Wei, Y, Kwong, AD, and Lin, C</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(3): 899-909</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495249&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495249&amp;dopt=abstract</a> </p><br />

    <p>6.     58224   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Agents with leukotriene B4-like antiviral (enveloped RNA) activities</p>

    <p>          Gosselin, Jean and Borgeat, Pierre</p>

    <p>          PATENT:  US <b>2006025479</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2004-11016  PP: 28 pp., Cont.-in-part of U.S. Ser. No. 683,882, abandoned.</p>

    <p>          ASSIGNEE:  (Can.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     58225   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Selective anti-cytomegalovirus compounds discovered by screening for inhibitors of subunit interactions of the viral polymerase</p>

    <p>          Loregian, A and Coen, DM</p>

    <p>          Chem Biol <b>2006</b>.  13(2): 191-200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16492567&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16492567&amp;dopt=abstract</a> </p><br />

    <p>8.     58226   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pyridinylamines and their preparations, pharmaceutical compositions and use for prophylaxis or treatment of various diseases</p>

    <p>          Eickhoff, Jan Eike, Hafenbradl, Doris, Schwab, Wilfried, Cotton, Matthew, Klebl, Bert Matthias, Zech, Birgit, Mueller, Stefan, Harris, John, Savic, Vladimir, Macritchie, Jackie, Sherborne, Brad, and Le, Joelle</p>

    <p>          PATENT:  WO <b>2006010637</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2005  PP: 177 pp.</p>

    <p>          ASSIGNEE:  (Gpc Biotech AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     58227   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of compounds that inhibit the interaction between core and surface protein of hepatitis B virus</p>

    <p>          Asif-Ullah, M, Choi, KJ, Choi, KI, Jeong, YJ, and Yu, YG</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16487605&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16487605&amp;dopt=abstract</a> </p><br />

    <p>10.   58228   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of cell proliferation by SARS-CoV infection in Vero E6 cells</p>

    <p>          Mizutani, T, Fukushi, S, Iizuka, D, Inanami, O, Kuwabara, M, Takashima, H, Yanagawa, H, Saijo, M, Kurane, I, and Morikawa, S</p>

    <p>          FEMS Immunol Med Microbiol <b>2006</b>.  46(2): 236-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16487305&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16487305&amp;dopt=abstract</a> </p><br />

    <p>11.   58229   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of 5-aza-7-deazadeazapurine and C-branched nucleosides as antiviral agents for treating Flaviviridae</p>

    <p>          Gosselin, Gilles, La Colla, Paolo, Seela, Frank, Storer, Richard, Dukhan, David, and Leroy, Frederic</p>

    <p>          PATENT:  WO <b>2006000922</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 115 pp.</p>

    <p>          ASSIGNEE:  (Idenix (Cayman) Limited, Cayman I.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   58230   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of phosphonated dihydroisoxazole nucleosides</p>

    <p>          Romeo, G, Iannazzo, D, Piperno, A, Romeo, R, Saglimbeni, M, Chiacchio, MA, Balestrieri, E, Macchi, B, and Mastino, A</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480883&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480883&amp;dopt=abstract</a> </p><br />

    <p>13.   58231   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and synthesis of 2,3,4,9-tetrahydro-1H-carbazole and 1,2,3,4-tetrahydro-cyclopenta[b]indole derivatives as non-nucleoside inhibitors of hepatitis C virus NS5B RNA-dependent RNA polymerase</p>

    <p>          Gopalsamy, A, Shi, M, Ciszewski, G, Park, K, Ellingboe, JW, Orlowski, M, Feld, B, and Howe, AY </p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480869&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480869&amp;dopt=abstract</a> </p><br />

    <p>14.   58232   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          P1 and P1 &#39; Optimization of [3,4]-Bicycloproline P2 Incorporated Tetrapeptidyl Alpha-Ketoamide Based Hcv Protease Inhibitors</p>

    <p>          Chen, S. <i>et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(2): 118-123</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400006</a> </p><br />

    <p>15.   58233   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-Viral Therapies for Hepatitis C Virus Infection: Current Options and Evolving Candidate Drugs</p>

    <p>          Fanning, L.</p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(2): 150-161</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400012</a> </p><br />

    <p>16.   58234   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phenyldihydroxypyrimidines as Hcvns5b Rna Dependent Rna Polymerase Inhibitors. Part 1: Amides and Ureas</p>

    <p>          Crescenzi, B. <i> et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(6): 451-455</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900004</a> </p><br />

    <p>17.   58235   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparative Activities of Oseltamivir and A-322278 in Immunocompetent and Immunocompromised Murine Models of Influenza Virus Infection</p>

    <p>          Ison, MG, Mishin, VP, Braciale, TJ, Hayden, FG, and Gubareva, LV</p>

    <p>          J Infect Dis <b>2006</b>.  193(6): 765-772</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16479509&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16479509&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   58236   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phenyldihydroxypyrimidines as Hcvns5b Rna Dependent Rna Polymerase Inhibitors. Part Ii: Sulfonamides</p>

    <p>          Ponzi, S. <i>et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(6): 456-461</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900005</a> </p><br />

    <p>19.   58237   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel Inhibitors of Hepatitis C Virus RNA-dependent RNA Polymerases</p>

    <p>          Lee, G, Piper, DE, Wang, Z, Anzola, J, Powers, J, Walker, N, and Li, Y</p>

    <p>          J Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16476448&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16476448&amp;dopt=abstract</a> </p><br />

    <p>20.   58238   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of Hepatitis C Virus Ns3 Center Dot 4a Protease: P-2 Proline Variants</p>

    <p>          Farmer, L. <i>et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(7): 497-502</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168000001</a> </p><br />

    <p>21.   58239   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity of Transiently Expressed Ifn-Kappa Is Cell-Associated</p>

    <p>          Buontempo, P. <i> et al.</i></p>

    <p>          Journal of Interferon and Cytokine Research <b>2006</b>.  26(1): 40-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235063500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235063500006</a> </p><br />

    <p>22.   58240   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Characterization of the antiviral effects of interferon-alpha against a SARS-like coronoavirus infection in vitro</p>

    <p>          Zorzitto, J, Galligan, CL, Ueng, JJ, and Fish, EN</p>

    <p>          Cell Res <b>2006</b>.  16(2): 220-229</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16474437&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16474437&amp;dopt=abstract</a> </p><br />

    <p>23.   58241   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of Antiviral Drugs Targeting the Flaviviruses Fusion Mechanism</p>

    <p>          Kampmann, T. <i>et al.</i></p>

    <p>          Febs Journal <b>2005</b>.  272: 553</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234826102702">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234826102702</a> </p><br />
    <br clear="all">

    <p>24.   58242   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hammerhead ribozymes with cleavage site specificity for NUH and NCH display significant anti-hepatitis C viral effect in vitro and in recombinant HepG2 and CCL13 cells</p>

    <p>          Gonzalez-Carmona, MA, Schussler, S, Serwe, M, Alt, M, Ludwig, J, Sproat, BS, Steigerwald, R, Hoffmann, P, Quasdorff, M, Schildgen, O, and Caselmann, WH</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16469406&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16469406&amp;dopt=abstract</a> </p><br />

    <p>25.   58243   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peroxisome Proliferator-Activated Receptor Alpha Antagonism Inhibits Hepatitis C Virus Replication</p>

    <p>          Rakic, B. <i>et al.</i></p>

    <p>          Chemistry &amp; Biology <b>2006</b>.  13(1): 23-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235090900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235090900006</a> </p><br />

    <p>26.   58244   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          ent-epiafzelechin-(4a-&gt;8)-epiafzelechin extracted from Cassia javanica inhibits herpes simplex virus type 2 replication</p>

    <p>          Cheng, Hua-Yew, Yang, Chien-Min, Lin, Ta-Chen, Shieh, Den-En, and Lin, Chun-Ching</p>

    <p>          Journal of Medical Microbiology <b>2006</b>.  55(2): 201-206</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   58245   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          2-Substituted Furans from the Roots of Polyalthia evecta</p>

    <p>          Kanokmedhakul, Somdej, Kanokmedhakul, Kwanjai, Kantikeaw, Itsara, and Phonkerd, Nutchanat</p>

    <p>          Journal of Natural Products <b>2006</b>.  69(1): 68-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   58246   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The protein phosphatase 2A represents a novel cellular target for hepatitis C virus NS5A protein</p>

    <p>          Georgopoulou, U, Tsitoura, P, Kalamvoki, M, and Mavromara, P</p>

    <p>          Biochimie <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16460864&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16460864&amp;dopt=abstract</a> </p><br />

    <p>29.   58247   DMID-LS-113; PUBMED-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Smallpox</p>

    <p>          Moore, ZS, Seward, JF, and Lane, JM</p>

    <p>          Lancet <b>2006</b>.  367(9508): 425-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458769&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458769&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>30.   58248   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Owl monkey polynucleotide encoding a TRIM5-cyclophilin A fusion protein, and its use in treating HIV-1 and other viral infections</p>

    <p>          Luban, Jeremy and Sayah, David</p>

    <p>          PATENT:  WO <b>2006014422</b>  ISSUE DATE:  20060209</p>

    <p>          APPLICATION: 2005  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (The Trustees of Columbia University In the City of New York, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   58249   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of the Hepatitis B Virus Replication in Vitro by an Oligodeoxynucleotide Containing Cytidine-Guano Sine Motifs</p>

    <p>          Ning, L. <i>et al.</i></p>

    <p>          Immunology Letters <b>2006</b>.  102(1): 60-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234768400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234768400008</a> </p><br />

    <p>32.   58250   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activities of 1,2,3-triazole functionalized thymidines: 1,3-dipolar cycloaddition for efficient regioselective diversity generation</p>

    <p>          Zhou, Longhu, Amer, Adel, Korn, Michael, Burda, Robert, Balzarini, Jan, De Clercq, Erik, Kern, Earl R, and Torrence, Paul F</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2005</b>.  16(6): 375-383</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   58251   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Aromatic Polycationic Molecules With Restricted Conformations: an Alternative Approach to Antiherpes Agents</p>

    <p>          Tsotinis, A. <i>et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(5): 424-427</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167800009</a> </p><br />

    <p>34.   58252   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          RNA-dependent RNA polymerase from SARS CoV and its homologs from other coronaviruses and their use in molecular biology and drug screening</p>

    <p>          Imbert, Isabelle, Guillemot, Jean-Claude, Canard, Bruno, and Ferron, Francois-Patrice</p>

    <p>          PATENT:  EP <b>1619246</b>  ISSUE DATE: 20060125</p>

    <p>          APPLICATION: 2004-29735  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (Universite de la Mediterranee, Aix-Marseille II Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   58253   DMID-LS-113; LR-14709-LWC; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pyridine N-oxide derivatives are inhibitory to the human SARS and feline infectious peritonitis coronavirus in cell culture</p>

    <p>          Balzarini, Jan, Keyaerts, Els, Vijgen, Leen, Vandermeer, Frank, Stevens, Miguel, De Clercq, Erik, Egberink, Herman, and Van Ranst, Marc</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  57(3): 472-481</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>36.   58254   DMID-LS-113; SCIFINDER-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Therapeutic agents for human papilloma virus disease and anti human immunodeficiency virus medicine</p>

    <p>          Sakagami, Hiroshi, Kawazoe, Yutaka, Otsuki, Mamitarou, Inaba, Youichi, Sai, Bun, Horiuchi, Akira, Nakajima, Hideki, and Aratsu, Chiaki</p>

    <p>          PATENT:  WO <b>2006001364</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 28 pp.</p>

    <p>          ASSIGNEE:  (Nihonsyouyaku Laboratory Corporation, Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   58255   DMID-LS-113; WOS-DMID-2/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian Influenza Virus Infections in Humans</p>

    <p>          Wong, S. and Yuen, K.</p>

    <p>          Chest <b>2006</b>.  129(1): 156-168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234944900026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234944900026</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
