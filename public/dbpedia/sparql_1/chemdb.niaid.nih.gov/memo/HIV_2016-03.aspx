

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RH+fg4CvmWzfPdU/0ERXT7VD+Axhrmc6UlwCJ6VILmMjn0PMk2fJvWoIkOK/mEiOfohvGnRBwz/F3RchwLTCnpboNzSga21reXvJbwpqpJqpe/MpFm0ySQ5TSwo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5717AB2C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: March, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26876929">Novel Indole sulfides as Potent HIV-1 NNRTIs.</a> Brigg, S., N. Pribut, A.E. Basson, M. Avgenikos, R. Venter, M.A. Blackie, W.A. van Otterlo, and S.C. Pelly. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(6): p. 1580-1584. PMID[26876929].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26878150">A Parallel Synthesis Approach to the Identification of Novel Diheteroarylamide-based Compounds Blocking HIV Replication: Potential Inhibitors of HIV-1 Pre-mRNA Alternative Splicing.</a> Cheung, P.K., D. Horhant, L.E. Bandy, M. Zamiri, S.M. Rabea, S.K. Karagiosov, M. Matloobi, S. McArthur, P.R. Harrigan, B. Chabot, and D.S. Grierson. Journal of Medicinal Chemistry, 2016. 59(5): p. 1869-1879. PMID[26878150].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26937954">Steroidal Saponins from the Rhizomes of Aspidistra typica.</a> Cui, J.M., L.P. Kang, Y. Zhao, J.Y. Zhao, J. Zhang, X. Pang, H.S. Yu, D.X. Jia, C. Liu, L.Y. Yu, and B.P. Ma. Plos One, 2016. 11(3): e0150595. PMID[26937954]. PMCID[PMC4777403].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368020102263">Selective Inhibitors of Nuclear Export (Sine) Compounds Suppress Both HIV Replication and AIDS Related Lymphoma.</a> Daelemans, D., E. Boons, E. Vanstreels, M. Jacquemyn, T.C. Nogueira, J.E. Neggers, T. Vercruysse, J. van den Oord, S. Tamir, M. Kauffman, S. Shacham, Y. Landesman, R. Snoeck, C. Pannecouque, and G. Andrei. Blood, 2016. 126(23). Poster 2751. ISI[000368020102263].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27009333">Dual Anti-HIV Mechanism of Clofarabine.</a> Daly, M.B., M.E. Roth, L. Bonnac, J.O. Maldonado, J. Xie, C.L. Clouser, S.E. Patterson, B. Kim, and L.M. Mansky. Retrovirology, 2016. 13(1): p. 20. PMID[27009333]. PMCID[PMC4806454].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26985308">Aloperine and Its Derivatives as a New Class of HIV-1 Entry Inhibitors.</a> Dang, Z., L. Zhu, W. Lai, H. Bogerd, K.H. Lee, L. Huang, and C.H. Chen. ACS Medicinal Chemistry Letters, 2016. 7(3): p. 240-244. PMID[26985308]. PMCID[PMC4789664].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26804933">Discovery of the Aryl-phospho-indole IDX899, a Highly Potent anti-HIV Non-Nucleoside Reverse Transcriptase Inhibitor.</a> Dousson, C., F.R. Alexandre, A. Amador, S. Bonaric, S. Bot, C. Caillet, T. Convard, D. da Costa, M.P. Lioure, A. Roland, E. Rosinovsky, S. Maldonado, C. Parsy, C. Trochet, R. Storer, A. Stewart, J. Wang, B.A. Mayes, C. Musiu, B. Poddesu, L. Vargiu, M. Liuzzi, A. Moussa, J. Jakubik, L. Hubbard, M. Seifer, and D. Standring. Journal of Medicinal Chemistry, 2016. 59(5): p. 1891-1898. PMID[26804933].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26852364">Synthesis and Evaluation of Orally Active Small Molecule HIV-1 Nef Antagonists.</a> Emert-Sedlak, L.A., H.M. Loughran, H. Shi, J.L. Kulp, 3rd, S.T. Shu, J. Zhao, B.W. Day, J.E. Wrobel, A.B. Reitz, and T.E. Smithgall. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(5): p. 1480-1484. PMID[26852364]. PMCID[PMC4756635].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26803470">A High Throughput Cre-lox Activated Viral Membrane Fusion Assay Identifies Pharmacological Inhibitors of HIV Entry.</a> Esposito, A.M., P. Cheung, T.H. Swartz, H.R. Li, T. Tsibane, N.D. Durham, C.F. Basler, D.P. Felsenfeld, and B.K. Chen. Virology, 2016. 490: p. 6-16. PMID[26803470].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26964017">Anti-HIV Cytotoxicity Enzyme Inhibition and Molecular Docking Studies of Quinoline Based Chalcones as Potential Non-Nucleoside Reverse Transcriptase Inhibitors (NNRT).</a> Hameed, A., M.I. Abdullah, E. Ahmed, A. Sharif, A. Irfan, and S. Masood. Bioorganic Chemistry, 2016. 65: p. 175-182. PMID[26964017].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26661793">High-mannose Specific Lectin and Its Recombinants from a Carrageenophyta Kappaphycus alvarezii Represent a Potent Anti-HIV Activity through High-affinity Binding to the Viral Envelope Glycoprotein gp120.</a> Hirayama, M., H. Shibata, K. Imamura, T. Sakaguchi, and K. Hori. Marine Biotechnology, 2016. 18(2): p. 215-231. PMID[26661793].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27046952">In Vitro Inhibition of Cytopathic Effect of Influenza Virus and Human Immunodeficiency Virus by Bamboo Leaf Extract Solution and Sodium Copper Chlorophyllin.</a> Ito, A., A. Tsuneki, Y. Yoshida, K. Ryoke, T. Kaidoh, and S. Kageyama. Yonago Acta Medica, 2016. 59(1): p. 61-65. PMID[27046952]. PMCID[PMC4816750].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26756779">Carolignans from the Aerial Parts of Euphorbia sikkimensis and Their anti-HIV Activity.</a> Jiang, C., P. Luo, Y. Zhao, J. Hong, S.L. Morris-Natschke, J. Xu, C.H. Chen, K.H. Lee, and Q. Gu. Journal of Natural Products, 2016. 79(3): p. 578-583. PMID[26756779]. PMCID[PMC4808429].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26855237">Synthesis and Antiviral Activity Evaluation of 2&#39;,5&#39;,5&#39;-Trifluoro-apiosyl Nucleoside Phosphonic acid Analogs.</a> Kim, E. and J.H. Hong. Nucleosides, Nucleotides &amp; Nucleic Acids, 2016. 35(3): p. 130-146. PMID[26855237].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26728103">Short Communication: Preferential Killing of HIV Latently Infected Cd4(+) T Cells by Malt1 Inhibitor.</a> Li, H.M., H. He, L.Y. Gong, M.G. Fu, and T.T. Wang. AIDS Research and Human Retroviruses, 2016. 32(2): p. 174-177. PMID[26728103]. PMCID[PMC4761853].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26730512">Screening HIV-1 Fusion Inhibitors Based on Capillary Electrophoresis Head-end Microreactor Targeting to the Core Structure of gp41.</a> Liu, L.H., X.Y. Xu, Y.H. Liu, X.X. Zhang, L. Li, and Z.M. Jia. Journal of Pharmaceutical and Biomedical Analysis, 2016. 120: p. 153-157. PMID[26730512].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26968652">C-3 Benzoic acid Derivatives of C-3 Deoxybetulinic acid and Deoxybetulin as HIV-1 Maturation Inhibitors.</a> Liu, Z., J.J. Swidorski, B. Nowicka-Sans, B. Terry, T. Protack, Z. Lin, H. Samanta, S. Zhang, Z. Li, D.D. Parker, S. Rahematpura, S. Jenkins, B.R. Beno, M. Krystal, N.A. Meanwell, I.B. Dicker, and A. Regueiro-Ren. Bioorganic &amp; Medicinal Chemistry, 2016. 24(8): p. 1757-1770. PMID[26968652].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26810800">Screening Platform Toward New anti-HIV Aptamers Set on Molecular Docking and Fluorescence Quenching Techniques.</a> Oliviero, G., M. Stornaiuolo, V. D&#39;Atri, F. Nici, A.M. Yousif, S. D&#39;Errico, G. Piccialli, L. Mayol, E. Novellino, L. Marinelli, P. Grieco, A. Carotenuto, S. Noppen, S. Liekens, J. Balzarini, and N. Borbone. Analytical Chemistry, 2016. 88(4): p. 2327-2334. PMID[26810800].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000371716300091">Graphene oxide Decorated with Cu(I)Br Nanoparticles: A Reusable Catalyst for the Synthesis of Potent bis(Indolyl)methane Based Anti HIV Drugs.</a> Srivastava, A., A. Agarwal, S.K. Gupta, and N. Jain. RSC Advances, 2016. 6(27): p. 23008-23011. ISI[000371716300091].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26927866">3-Hydroxypyrimidine-2,4-diones as Selective Active Site Inhibitors of HIV Reverse Transcriptase-associated RNase H: Design, Synthesis, and Biochemical Evaluations.</a> Tang, J., F. Liu, E. Nagy, L. Miller, K.A. Kirby, D.J. Wilson, B. Wu, S.G. Sarafianos, M.A. Parniak, and Z. Wang. Journal of Medicinal Chemistry, 2016. 59(6): p. 2648-2659. PMID[26927866].</p>
     
    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>
    
    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000369991800004">Integrase Inhibitors against HIV: Efficacy and Resistance.</a> Tau, P. and S. Rusconi. Future Virology, 2016. 11(2): p. 109-112. ISI[000369991800004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26838028">Isolation and Antiviral Activity of Water-soluble Cynomorium songaricum Rupr. Polysaccharides.</a> Tuvaanjav, S., H. Shuqin, M. Komata, C.J. Ma, T. Kanamoto, H. Nakashima, and T. Yoshida. Journal of Asian Natural Products Research, 2016. 18(2): p. 159-171. PMID[26838028].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26944616">Synthesis of Tetracyclic Iminosugars Fused Benzo[e][1,3]thiazin-4-one and Their HIV-RT Inhibitory Activity.</a> Yin, Z.Q., M. Zhu, S.A. Wei, J. Shao, Y.H. Hou, H. Chen, and X.L. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(7): p. 1738-1741. PMID[26944616].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26935098">2-Thio-6-azauridine Inhibits Vpu Mediated BST-2 Degradation.</a> Zhang, Q., Z.Y. Mi, Y.M. Huang, L. Ma, J.W. Ding, J. Wang, Y.X. Zhang, Y. Chen, J.M. Zhou, F. Guo, X.Y. Li, and S. Cen. Retrovirology, 2016. 13(13): 14pp. PMID[26935098]. PMCID[PMC4776379].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27023513">Purification and Characterization of a White Laccase with Pronounced Dye Decolorizing Ability and HIV-1 Reverse Transcriptase Inhibitory Activity from Lepista nuda.</a> Zhu, M., G. Zhang, L. Meng, H. Wang, K. Gao, and T. Ng. Molecules, 2016. 21(415): 16pp. PMID[27023513].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160317&amp;CC=WO&amp;NR=2016039403A1&amp;KC=A1">Preparation of N-[[(Hexahydrofuro[2,3-b]furan-3-yl)oxy]carbonyl]-2-benzyl-1,3-diaminopropan-2-ol Derivatives as Sustained HIV Protease Inhibitors.</a> Kawasuji, T., H. Mikamiyama, N. Suzuki, K. Masuda, H. Sugimoto, A. Okano, M. Yoshida, S. Sugiyama, K. Asahi, I. Kozono, K. Miyazaki, H. Ozasa, and M. Miyagawa. Patent. 2016. 2015-JP75669 2016039403: 500pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_03_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160303&amp;CC=WO&amp;NR=2016033009A1&amp;KC=A1">Preparation of Imidazo[1,2-a]pyridine Derivatives as Human Immunodeficiency Virus Replication Inhibitors.</a> Naidu, B.N., T.P. Connolly, and K.J. Eastman. Patent. 2016. 2015-US46648 2016033009: 60pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_03_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160317&amp;CC=WO&amp;NR=2016040084A1&amp;KC=A1">Preparation of N-(Alkylcarbamoyl)sulfonamide Compds. Useful for the Treatment of HIV Infection.</a> Pendri, A., G. Li, S. Zhu, R.G. Gentles, J.A. Bender, Z. Yang, M. Belema, N.A. Meanwell, and B.R. Beno. Patent. 2016. 2015-US482712016040084: 229pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_03_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
