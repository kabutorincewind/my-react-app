

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-319.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="GIiLd7CnNY5pvO+onsKqiCnthjUhosTG5d7Aisj1Urr9E0O9Mv/mSmxsCb8oi0RLH63YLYLPHlYz9z1eslP8XeVfhT28IMSFkdEpbc1hFTvKyhAOJBbVU/RRfxU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E4C15DBF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-319-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         45106   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Azithromycin prophylaxis and treatment of murine toxoplasmosis</p>

    <p>          Tabbara, KF, Hammouda, E, Tawfik, A, Al-Omar, OM, and Abu, El-Asrar AM</p>

    <p>          Saudi Med J <b>2005</b>.  26(3): 393-397</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15806206&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15806206&amp;dopt=abstract</a> </p><br />

    <p>2.         45107   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Structure of pyrR (Rv1379) from Mycobacterium tuberculosis: a persistence gene and protein drug target</p>

    <p>          Kantardjieff, KA, Vasquez, C, Castro, P, Warfel, NM, Rho, BS, Lekin, T, Kim, CY, Segelke, BW, Terwilliger, TC, and Rupp, B</p>

    <p>          Acta Crystallogr D Biol Crystallogr <b>2005</b>.  61(Pt 4): 355-364</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15805589&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15805589&amp;dopt=abstract</a> </p><br />

    <p>3.         45108   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Capreomycin is active against non-replicating Mycobacterium tuberculosis</p>

    <p>          Heifets, L, Simon, J, and Pham, V</p>

    <p>          Ann Clin Microbiol Antimicrob <b>2005</b>.  4(1): 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15804353&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15804353&amp;dopt=abstract</a> </p><br />

    <p>4.         45109   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          FTR1335 Is a Novel Synthetic Inhibitor of Candida albicans N-Myristoyltransferase with Fungicidal Activity</p>

    <p>          Ebara, S, Naito, H, Nakazawa, K, Ishii, F, and Nakamura, M</p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(4): 591-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15802792&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15802792&amp;dopt=abstract</a> </p><br />

    <p>5.         45110   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Synthesis, Biological Activity, and SAR of Antimycobacterial 9-Aryl-, 9-Arylsulfonyl-, and 9-Benzyl-6-(2-furyl)purines</p>

    <p>          Bakkestuen, AK, Gundersen, LL, and Utenova, BT</p>

    <p>          J Med Chem <b>2005</b>.  48(7): 2710-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801862&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801862&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         45111   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Direct comparison of selected methods for genetic categorisation of Cryptosporidium parvum and Cryptosporidium hominis species</p>

    <p>          Chalmers, Rachel M, Ferguson, Christobel, Caccio, Simone, Gasser, Robin B, Abs El-Osta, Youssef G, Heijnen, Leo, Xiao, Lihua, Elwin, Kristin, Hadfield, Stephen, Sinclair, Martha, and Stevens, Melita</p>

    <p>          International Journal for Parasitology <b>2005</b>.  35(4): 397-410</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.         45112   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          In Vitro and In Vivo Activities of Macrolide Derivatives against Mycobacterium tuberculosis</p>

    <p>          Falzari, K, Zhu, Z, Pan, D, Liu, H, Hongmanee, P, and Franzblau, SG</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(4): 1447-1454</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793125&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793125&amp;dopt=abstract</a> </p><br />

    <p>8.         45113   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p><b>          Rational design of inhibitors of the Cryptosporidium parvum ATP-Binding Cassette 3 protein</b> </p>

    <p>          Terreux, R, Lawton, P, Radix, S, Deruaz, D, Lussignol, M, Reynaud, J, Marminon, C, Nebois, P, Bouaziz, Z, and Walchshofer, N</p>

    <p>          229th ACS National Meeting <b>2005</b>.  229: MEDI-393</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.         45114   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          A Homologue of the Mycobacterium tuberculosis PapA5 Protein, Rif-Orf20, Is an Acetyltransferase Involved in the Biosynthesis of Antitubercular Drug Rifamycin B by Amycolatopsis mediterranei S699</p>

    <p>          Xiong, Y, Wu, X, and Mahmud, T</p>

    <p>          Chembiochem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15791687&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15791687&amp;dopt=abstract</a> </p><br />

    <p>10.       45115   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Design of translactam HCMV protease inhibitors as potent antivirals</p>

    <p>          Borthwick, AD</p>

    <p>          Med Res Rev <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15789440&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15789440&amp;dopt=abstract</a> </p><br />

    <p>11.       45116   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          In vitro antifungal properties structure-activity relationships and studies on the mode of action of N-phenyl, N-aryl, N-phenylalkyl maleimides and related compounds</p>

    <p>          Lopez, SN, Castelli, MV, de Campos, F, Correa, R, Cechinel, Filho V, Yunes, RA, Zamora, MA, Enriz, RD, Ribas, JC, Furlan, RL, and Zacchino, SA</p>

    <p>          Arzneimittelforschung <b>2005</b>.  55(2): 123-132</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15787280&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15787280&amp;dopt=abstract</a> </p><br />

    <p>12.       45117   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Design and Synthesis of Melanocortin Peptides with Candidacidal and Anti-TNF-a Properties</p>

    <p>          Grieco, Paolo, Rossi, Claudia, Gatti, Stefano, Colombo, Gualtiero, Carlin, Andrea, Novellino, Ettore, Lama, Teresa, Lipton, James M, and Catania, Anna</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(5): 1384-1388</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.       45118   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Spectroscopic studies, antimicrobial activities and crystal structures of N-(2-hydroxy-3-methoxybenzalidene)1-aminonaphthalene</p>

    <p>          Uenver, Hueseyin, Yildiz, Mustafa, Duelger, Basaran, Oezgen, Oezen, Kendi, Engin, and Durlu, Tahsin Nuri</p>

    <p>          Journal of Molecular Structure <b>2005</b>.  737(2-3): 159-164</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.       45119   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Development of a pyrosequencing approach for rapid screening of rifampin, isoniazid and ethambutol-resistant Mycobacterium tuberculosis</p>

    <p>          Zhao, JR, Bai, YJ, Wang, Y, Zhang, QH, Luo, M, and Yan, XJ</p>

    <p>          Int J Tuberc Lung Dis <b>2005</b>.  9(3): 328-332</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15786899&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15786899&amp;dopt=abstract</a> </p><br />

    <p>15.       45120   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          FR207944, an Antifungal Antibiotic from Chaetomium sp. No. 217 I. Taxonomy, Fermentation, and Biological Properties</p>

    <p>          Kobayashi, M,  Kanasaki, R, Sato, I, Abe, F, Nitta, K, Ezaki, M, Sakamoto, K, Hashimoto, M, Fujie, A, Hino, M, and Hori, Y</p>

    <p>          Biosci Biotechnol Biochem <b>2005</b>.  69(3): 515-521</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784979&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784979&amp;dopt=abstract</a> </p><br />

    <p>16.       45121   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Identification of a novel antifungal nonapeptide generated by combinatorial approach</p>

    <p>          Kumar, M, Chaturvedi, AK, Kavishwar, A , Shukla, PK, Kesarwani, AP, and Kundu, B</p>

    <p>          Int J Antimicrob Agents <b>2005</b>.  25(4): 313-320</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784311&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784311&amp;dopt=abstract</a> </p><br />

    <p>17.       45122   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p><b>          Spectroscopic study, antimicrobial activity and crystal structures of N-(2-hydroxy-5-nitrobenzalidene)4-aminomorpholine and N-(2-hydroxy-1-naphthylidene)4-aminomorpholine</b> </p>

    <p>          Yildiz, Mustafa, Uenver, Hueseyin, Duelger, Basaran, Erdener, Digdem, Ocak, Nazan, Erdoenmez, Ahmet, and Durlu, Tahsin Nuri</p>

    <p>          Journal of Molecular Structure <b>2005</b>.  738(1-3): 253-260</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.       45123   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Isolation and preparation of diterpenoid compounds, compositions thereof and their use as anticancer or antifungal agents</p>

    <p>          Lavallee, Jean-Francois, Doyle, Terrence W., Rioux, Elise, Belec, Laurent, Rabouin, Daniel, and Billot, Xavier</p>

    <p>          PATENT:  US <b>2005032802</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004-13294  PP: 109 pp., Cont.-in-part of U.S. Ser. No. 725,629.</p>

    <p>          ASSIGNEE:  (Gemin X Biotechnologies Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.       45124   OI-LS-319; PUBMED-OI-4/5/2005</p>

    <p class="memofmt1-2">          Mechanism of phagolysosome biogenesis block by viable Mycobacterium tuberculosis</p>

    <p>          Vergne, I, Chua, J, Lee, HH, Lucas, M, Belisle, J, and Deretic, V</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.  102(11): 4033-4038</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15753315&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15753315&amp;dopt=abstract</a> </p><br />

    <p>20.       45125   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of agelasine E and analogs</p>

    <p>          Bakkestuen, Anne Kristin, Gundersen, Lise-Lotte, Petersen, Dirk, Utenova, Bibigul T, and Vik, Anders</p>

    <p>          Organic &amp; Biomolecular Chemistry <b>2005</b>.  3(6): 1025-1033</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.       45126   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          The Novel L- and D-Amino Acid Derivatives of Hydroxyurea and Hydantoins: Synthesis, X-ray Crystal Structure Study, and Cytostatic and Antiviral Activity Evaluations</p>

    <p>          Opacic, Ninoslav, Barbaric, Monika, Zorc, Branka, Cetina, Mario, Nagl, Ante, Frkovic, Danijel, Kralj, Marijeta, Pavelic, Kresimir, Balzarini, Jan, Andrei, Graciela, Snoeck, Robert, De Clercq, Erik, Raic-Malic, Silvana, and Mintas, Mladen</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(2): 475-482</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.       45127   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Antifungal diterpenoids and flavonoids from Ballota inaequidens</p>

    <p>          Citoglu, GS, Sever, B, Antus, S, Baitz-Gacs, E, and Altanlar, N</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2004</b>.  42(8): 659-663, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227438300017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227438300017</a> </p><br />

    <p>23.       45128   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          The evaluation of plants from Turkey for in vitro antimycobacterial activity</p>

    <p>          Tosun, F, Kizilay, CA, Sener, B, and Vural, M</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2005</b>.  43(1): 58-63, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227473400011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227473400011</a> </p><br />

    <p>24.       45129   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          A preparation of acridone derivatives, useful as anti-herpes virus agents</p>

    <p>          Bastow, Kenneth F. and Lowden, Christopher T</p>

    <p>          PATENT:  US <b>20050049273</b>  ISSUE DATE: 20050303</p>

    <p>          APPLICATION: 2003-62052  PP: 19 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.       45130   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Synthesis, in vitro cytotoxic and antiviral activity of cis-[Pt(R(-) and S(+)-2-a-hydroxybenzylbenzimidazole)2Cl2] complexes</p>

    <p>          Goekce, M, Utku, S, Guer, S, Oezkul, A, and Guemues, F</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(2): 135-141</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.       45131   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Mycobacteria use their surface-exposed glycolipids to infect human macrophages through a receptor-dependent process</p>

    <p>          Villeneuve, C, Gilleron, M, Maridonneau-Parini, I, Daffe, M, Astarie-Dequeker, C, and Etienne, G</p>

    <p>          JOURNAL OF LIPID RESEARCH <b>2005</b>.  46(3): 475-483, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227450900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227450900011</a> </p><br />

    <p>27.       45132   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Genetic diversity of multidrug-resistant Mycobacterium tuberculosis isolates and identification of 11 novel rpoB alleles in Taiwan</p>

    <p>          Jou, R, Chen, HY, Chiang, CY, Yu, MC, and Su, IJ</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2005</b>.  43(3): 1390-1394, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227538900062">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227538900062</a> </p><br />

    <p>28.       45133   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          The acyl-AMP ligase FadD32 and AccD4-containing acyl-CoA carboxylase are required for the synthesis of mycolic acids and essential for mycobacterial growth - Identification of the carboxylation product and determination of the acyl-CoA carboxylase components</p>

    <p>          Portevin, D, D&#39;Auria, LD, Montrozier, H, Houssin, C, Stella, A, Laneelle, MA, Bardou, F, Guilhot, C, and Daffe, M</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(10): 8862-8874, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100031</a> </p><br />

    <p>29.       45134   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Preparation of quinoxalinecarboxamides as antivirals</p>

    <p>          An, Haoyun, Rong, Frank, Wu, Jim, Harris, Clayton, and Chow, Suetying</p>

    <p>          PATENT:  US <b>2005026923</b>  ISSUE DATE:  20050203</p>

    <p>          APPLICATION: 2004-40007  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.       45135   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Iron inactivates the RNA polymerase NS5B and suppresses subgenomic replication of hepatitis C virus</p>

    <p>          Fillebeen, C, Rivas-Estilla, AM, Bisaillon, M, Ponka, P, Muckenthaler, M, Hentze, MW, Koromilas, AE, and Pantopoulos, K</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(10): 9049-9057, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100053">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100053</a> </p><br />
    <br clear="all">

    <p>31.       45136   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Structural diversities of active site in clinical azole-bound forms between sterol 14 alpha-demethylases ( CYP51s) from human and Mycobacterium tuberculosis</p>

    <p>          Matsuura, K, Yoshioka, S, Tosha, T, Hori, H, Ishimori, K, Kitagawa, T, Morishima, I, Kagawa, N, and Waterman, MR</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(10): 9088-9096, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100058">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100058</a> </p><br />

    <p>32.       45137   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Anti-viral therapies for hepatitis C virus infection: current options and evolving candidate drugs</p>

    <p>          Fanning, Liam J</p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(2): 150-161</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.       45138   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Thienopyrroles as antiviral agents</p>

    <p>          Attenni, Barbara, Hernando, Jose Ignacio Martin, Malancona, Savina, Narjes, Frank, Ontoria Ontoria, Jesus Maria, and Rowley, Michael</p>

    <p>          PATENT:  WO <b>2005023819</b>  ISSUE DATE:  20050317</p>

    <p>          APPLICATION: 2004  PP: 94 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.       45139   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Atovaquone maintenance therapy prevents reactivation of toxoplasmic encephalitis in a murine model</p>

    <p>          Dunay, IR, Heimesaat, MM, Bushrab, FN, Muller, RH, Stocker, H, Arasteh, K, Kurowski, M, Fitzner, R, Borner, K, and Liesenfeld, O</p>

    <p>          INTERNATIONAL JOURNAL OF MEDICAL MICROBIOLOGY <b>2004</b>.  294: 148-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224456000148">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224456000148</a> </p><br />

    <p>35.       45140   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Systems for screening viral protease inhibitors and anti-hepatitis drugs, using fusion polypeptide having a protease recognition site and detectable reporter proteins</p>

    <p>          Hsu, Tsu-An and Lee, Jin-Ching</p>

    <p>          PATENT:  US <b>20050053921</b>  ISSUE DATE: 20050310</p>

    <p>          APPLICATION: 2004-51442  PP: 17 pp.</p>

    <p>          ASSIGNEE:  (Taiwan)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.       45141   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Prevention and treatment of viral hepatitis c</p>

    <p>          Deriabin, Petr Grigorievich, Lvov, Dmitry Konstantinovich, Balakshin, Vladimir Vladimirovich, and Chistiakov, Aleksey Nikolaevich</p>

    <p>          PATENT:  WO <b>2005021007</b>  ISSUE DATE:  20050310</p>

    <p>          APPLICATION: 2004  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Obschestvo S Ogranichennoi Otvetstvennostju &#39;berezovy Mir&#39;, Russia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>37.       45142   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Sesquiterpene derivatives from Stachybotrys as antiviral medicines against HIV, BVDV, HCV, and CoV viruses</p>

    <p>          Sato, Akihiko and Kobayashi, Masanori</p>

    <p>          PATENT:  JP <b>2005060362</b>  ISSUE DATE:  20050310</p>

    <p>          APPLICATION: 2004-14136  PP: 17 pp.</p>

    <p>          ASSIGNEE:  (Shionogi and Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.       45143   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Substituted 4H-pyrazolo[1,5-a]pyrimidin-7-ones as hepatitis C virus polymerase inhibitors</p>

    <p>          Deng, Yongqi,  Popovici-Muller, Janeta V, Shipps, Gerald W, Rosner, Kristin E, Wang, Tong, Curran, Patrick, Cooper, Alan B, Girijavallabhan, Viyyoor, Butkiewicz, Nancy, and Cable, Mickey</p>

    <p>          229th ACS National Meeting <b>2005</b>.  229: MEDI-340</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.       45144   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Pyrazolopyrimidines as inhibitors of HCV RNA polymerase</p>

    <p>          Popovici-Muller, Janeta V, Shipps, Gerald W, Rosner, Kristin E, Deng, Yongqi, Wang, Tong, Curran, Patrick, Brown, Meredith A, Cooper, Alan B, Cable, Mickey, Butkiewicz, Nancy, and Girijavallabhan, Viyyoor</p>

    <p>          229th ACS National Meeting <b>2005</b>.  229: MEDI-337</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.       45145   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Design and synthesis of pyranobenzothiophenes as inhibitors of HCV RNA dependent RNA polymerase</p>

    <p>          Gopalsamy, Ariamala, Ciszewski, Gregory M, Aplasca, Alexis, Ellingboe, John W, Feld, Boris, Orlowski, Mark, and Howe, Anita Y. M</p>

    <p>          229th ACS National Meeting <b>2005</b>.  229: MEDI-322</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.       45146   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          Antimetabolite antiviral dosing regimen for hepatitis C virus or flaviviridae therapy</p>

    <p>          Stuyver, Lieven J</p>

    <p>          PATENT:  US <b>2005049220</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2004-3548  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Belg.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.       45147   OI-LS-319; SCIFINDER-OI-3/29/2005</p>

    <p class="memofmt1-2">          The artificial CpG single strand deoxidation oligonucleotide and its antiviral uses</p>

    <p>          Yu, Yongli and Wang, Liying</p>

    <p>          PATENT:  WO <b>2005014611</b>  ISSUE DATE:  20050217</p>

    <p>          APPLICATION: 2004  PP: 22 pp.</p>

    <p>          ASSIGNEE:  (Changchun Huapu Biotechnology Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.       45148   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Assessment of chemical libraries for their druggability</p>

    <p>          Sirois, S, Hatzakis, G, Wei, DQ, Du, QS, and Chou, KC</p>

    <p>          COMPUTATIONAL BIOLOGY AND CHEMISTRY <b>2005</b>.  29(1): 55-67, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227611200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227611200006</a> </p><br />
    <br clear="all">

    <p>44.       45149   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Studies of early bactericidal activity: New insights into isnoniazid pharmacokinetics</p>

    <p>          Gillespie, SH</p>

    <p>          CLINICAL INFECTIOUS DISEASES <b>2004</b>.  39(10): 1431-1432, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227492000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227492000004</a> </p><br />

    <p>45.       45150   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Synthesis and evaluation of the antibacterial and antitubercular activity of some N-1-aral-N-4-(3-chloro-4-fluoro henyl)thiosemicarbazones and their copper(I) complexes</p>

    <p>          Khazi, IM, Koti, RS, Chadha, MV, Mahajanshetti, CS, and Gadad, AK</p>

    <p>          ARZNEIMITTEL-FORSCHUNG-DRUG RESEARCH <b>2005</b>.  55(2): 107-113, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227506900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227506900005</a> </p><br />

    <p>46.       45151   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          New steroidal saponins from the sponge Erylus lendenfeldi</p>

    <p>          Fouad, M, Al-Trabeen, K, Badran, M, Wray, V, Edrada, R, Proksch, P, and Ebel, R</p>

    <p>          ARKIVOC <b>2004</b>.: 17-27, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227496000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227496000003</a> </p><br />

    <p>47.       45152   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          The magic bullets and tuberculosis drug targets</p>

    <p>          Ying, Z</p>

    <p>          ANNUAL REVIEW OF PHARMACOLOGY AND TOXICOLOGY <b>2005</b>.  45: 529-564, 36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227504000022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227504000022</a> </p><br />

    <p>48.       45153   OI-LS-319; WOS-OI-3/27/2005</p>

    <p class="memofmt1-2">          Variation in antiviral 2 &#39;,5 &#39;-oligoadenylate synthetase (2 &#39; 5 &#39; AS) enzyme activity is controlled by a single-nucleotide polymorphism at a splice-acceptor site in the OAS1 gene</p>

    <p>          Bonnevie-Nielsen, V, Field, LL, Lu, S, Zheng, DJ, Li, M, Martensen, PM, Nielsen, TB, Beck-Nielsen, H, Lau, YL, and Pociot, F</p>

    <p>          AMERICAN JOURNAL OF HUMAN GENETICS <b>2005</b>.  76(4): 623-633, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227516000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227516000008</a> </p><br />

    <p>49.       45154   OI-LS-319; WOS-OI-4/3/2005 </p>

    <p class="memofmt1-2">          Modulation of cooperativity in Mycobacterium tuberculosis NADPH-ferredoxin reductase: Cation- and pH-induced alterations in native conformation and destabilization of the NADP(+)-binding domain</p>

    <p>          Bhatt, AN, Shukla, N, Aliverti, A, Zanetti, G, and Bhakuni, V</p>

    <p>          PROTEIN SCIENCE <b>2005</b>.  14(4): 980-992, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227738900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227738900016</a> </p><br />

    <p>50.       45155   OI-LS-319; WOS-OI-4/3/2005 </p>

    <p class="memofmt1-2">          Synthesis of new quinoxaline-2-carboxylate 1,4-dioxide derivatives as anti-Mycobacterium tuberculosis agents</p>

    <p>          Jaso, A, Zarranz, B, Aldana, I, and Monge, A</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(6): 2019-2025, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227749300037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227749300037</a> </p><br />

    <p>51.       45156   OI-LS-319; WOS-OI-4/3/2005 </p>

    <p class="memofmt1-2">          Illuminating drug discovery with biological pathways</p>

    <p>          Apic, G, Ignjatovic, T, Boyer, S, and Russell, RB</p>

    <p>          FEBS LETTERS <b>2005</b>.  579(8): 1872-1877, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227657800018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227657800018</a> </p><br />

    <p>52.       45157   OI-LS-319; WOS-OI-4/3/2005 </p>

    <p class="memofmt1-2">          The target discovery process</p>

    <p>          Egner, U, Kratzschmar, J, Kreft, B, Pohlenz, HD, and Schneider, M</p>

    <p>          CHEMBIOCHEM <b>2005</b>.  6(3): 468-479, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227636700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227636700003</a> </p><br />

    <p>53.       45158   OI-LS-319; WOS-OI-4/3/2005 </p>

    <p class="memofmt1-2">          Synthesis of isonicotinic acid N&#39;-arylidene-N-[2-oxo-2-(4-ary -piperazin-1-yl)-ethyl]-hydrazides as antituberculosis agents</p>

    <p>          Sinha, N, Jain, S, Tilekar, A, Upadhayaya, RS, Kishore, N, Jana, GH, and Arora, SK</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(6): 1573-1576, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227739500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227739500006</a> </p><br />

    <p>54.       45159   OI-LS-319; WOS-OI-4/3/2005 </p>

    <p class="memofmt1-2">          Berberine synergy with amphotericin B against disseminated candidiasis in mice</p>

    <p>          Han, Y and Lee, JH</p>

    <p>          BIOLOGICAL &amp; PHARMACEUTICAL BULLETIN <b>2005</b>.  28(3): 541-544, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227705300029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227705300029</a> </p><br />

    <p>55.       45160   OI-LS-319; WOS-OI-4/3/2005</p>

    <p class="memofmt1-2">          Quantitative-PCR assessment of Cryptosporidium parvum cell culture infection</p>

    <p>          Di Giovanni, GD and LeChevallier, MW</p>

    <p>          APPLIED AND ENVIRONMENTAL MICROBIOLOGY <b>2005</b>.  71(3): 1495-1500, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227702900045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227702900045</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
