

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-04-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jcBOphlCg6Jg2O6POP1q/nsQiYLteyvHYIzL9rTf5H2pNNtxcmGqr9gxXr8N470k/Zjk1EN8mdQeh66Gs8ajv/fA3wYJVGBS8MN6cLFSvyryIrfbuX9MLfmuPNI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="32281092" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 27 - April 9, 2015</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25665940">Chemical Constituents of Swertia mussotii and Their anti-Hepatitis B Virus Activity.</a> Cao, T.W., C.A. Geng, Y.B. Ma, X.M. Zhang, J. Zhou, Y.D. Tao, and J.J. Chen. Fitoterapia, 2015. 102: p. 15-22. PMID[25665940].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25612255">Influence of the Basal Core Promoter and Precore Mutation on Replication of Hepatitis B Virus and Antiviral Susceptibility of Different Genotypes.</a> Cui, X.J., Y.K. Cho, and B.C. Song. Journal of Medical Virology, 2015. 87(4): p. 601-608. PMID[25612255].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25737009">Synthesis of Erythrocentaurin Derivatives as a New Class of Hepatitis B Virus Inhibitors.</a> Geng, C.A., X.Y. Huang, Y.B. Ma, X.M. Zhang, and J.J. Chen. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1568-1571. PMID[25737009].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25721422">Five New Secoiridoid Glycosides and One Unusual Lactonic Enol Ketone with anti-HBV Activity from Swertia cincta.</a> Jie, X.X., C.A. Geng, X.Y. Huang, Y.B. Ma, X.M. Zhang, R.P. Zhang, and J.J. Chen. Fitoterapia, 2015. 102: p. 96-101. PMID[25721422].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25676532">Transbody against Hepatitis B Virus Core Protein Inhibits Hepatitis B Virus Replication in Vitro.</a> Wang, Y., Y. Li, N. Li, Q. Zhu, L. Hui, X. Liu, Q. Han, Y. Lv, Q. Wang, G. Yang, Z. Zhou, and Z. Liu. International Immunopharmacology, 2015. 25(2): p. 363-369. PMID[25676532].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25737008">Isolation, Synthesis and anti-Hepatitis B Virus Evaluation of P-Hydroxyacetophenone Derivatives from Artemisia capillaris.</a> Zhao, Y., C.A. Geng, H. Chen, Y.B. Ma, X.Y. Huang, T.W. Cao, K. He, H. Wang, X.M. Zhang, and J.J. Chen. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1509-1514. PMID[25737008].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25736995">A Coumarin Lignanoid from the Stems of Kadsura heteroclita.</a> Su, W., J. Zhao, M. Yang, H.W. Yan, T. Pang, S.H. Chen, H.Y. Huang, S.H. Zhang, X.C. Ma, D.A. Guo, I.A. Khan, and W. Wang. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1506-1508. PMID[25736995].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25644675">Synthesis and Biological Evaluation of alpha-Aryl-alpha-tetralone Derivatives as Hepatitis C Virus Inhibitors.</a> Manvar, D., A. Fernandes Tde, J.L. Domingos, E. Baljinnyam, A. Basu, E.F. Junior, P.R. Costa, and N. Kaushik-Basu. European Journal of Medicinal Chemistry, 2015. 93: p. 51-54. PMID[25644675].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25759009">P2-Quinazolinones and bis-Macrocycles as New Templates for Next-generation Hepatitis C Virus NS3/4A Protease Inhibitors: Discovery of MK-2748 and MK-6325.</a> Rudd, M.T., J.W. Butcher, K.T. Nguyen, C.J. McIntyre, J.J. Romano, K.F. Gilbert, K.J. Bush, N.J. Liverton, M.K. Holloway, S. Harper, M. Ferrara, M. DiFilippo, V. Summa, J. Swestock, J. Fritzen, S.S. Carroll, C. Burlein, J.M. DiMuzio, A. Gates, D.J. Graham, Q. Huang, S. McClain, C. McHale, M.W. Stahlhut, S. Black, R. Chase, A. Soriano, C.M. Fandozzi, A. Taylor, N. Trainor, D.B. Olsen, P.J. Coleman, S.W. Ludmerer, and J.A. McCauley. ChemMedChem, 2015. 10(4): p. 727-735. PMID[25759009].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25449227">Interactions of the Hepatitis C Virus Protease Inhibitor Faldaprevir with Cytochrome P450 Enzymes: In Vitro and in Vivo Correlation.</a> Sabo, J.P., J. Kort, C. Ballow, A.D. Kashuba, M. Haschke, M. Battegay, B. Girlich, N. Ting, B. Lang, W. Zhang, C. Cooper, D. O&#39;Brien, E. Seibert, T.S. Chan, D. Tweedie, and Y. Li. Journal of Clinical Pharmacology, 2015. 55(4): p. 467-477. PMID[25449227].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25710739">Discovery of Imidazo[2,1-b]thiazole HCV NS4B Inhibitors Exhibiting Synergistic Effect with Other Direct-acting Antiviral Agents.</a> Wang, N.Y., Y. Xu, W.Q. Zuo, K.J. Xiao, L. Liu, X.X. Zeng, X.Y. You, L.D. Zhang, C. Gao, Z.H. Liu, T.H. Ye, Y. Xia, Y. Xiong, X.J. Song, Q. Lei, C.T. Peng, H. Tang, S.Y. Yang, Y.Q. Wei, and L.T. Yu. Journal of Medicinal Chemistry, 2015. 58(6): p. 2764-2778. PMID[25710739].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25628391">Discovery of anti-Claudin-1 Antibodies as Candidate Therapeutics against Hepatitis C Virus.</a> Yamashita, M., M. Iida, M. Tada, Y. Shirasago, M. Fukasawa, S. Nagase, A. Watari, A. Ishii-Watabe, K. Yagi, and M. Kondoh. The Journal of Pharmacology and Experimental Therapeutics, 2015. 353(1): p. 112-118. PMID[25628391].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p>

    <h2>Simian Immunodeficiency Virus</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25583721">Simian-tropic HIV as a Model to Study Drug Resistance against Integrase Inhibitors.</a> Wares, M., S. Hassounah, T. Mesplede, P.A. Sandstrom, and M.A. Wainberg. Antimicrobial Agents and Chemotherapy, 2015. 59(4): p. 1942-1949. PMID[25583721].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25647077">Neo-clerodane Diterpenoids from Scutellaria barbata with Activity against Epstein-Barr Virus Lytic Replication.</a> Wu, T., Q. Wang, C. Jiang, S.L. Morris-Natschke, H. Cui, Y. Wang, Y. Yan, J. Xu, K.H. Lee, and Q. Gu. Journal of Natural Products, 2015. 78(3): p. 500-509. PMID[25647077].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25624324">A Novel CDK7 Inhibitor of the Pyrazolotriazine Class Exerts Broad-spectrum Antiviral Activity at Nanomolar Concentrations.</a> Hutterer, C., J. Eickhoff, J. Milbradt, K. Korn, I. Zeittrager, H. Bahsi, S. Wagner, G. Zischinsky, A. Wolf, C. Degenhart, A. Unger, M. Baumann, B. Klebl, and M. Marschall. Antimicrobial Agents and Chemotherapy, 2015. 59(4): p. 2062-2071. PMID[25624324].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0327-040915.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350670600008">HPLC Analysis and Antimicrobial, Antimycobacterial and Antiviral Activities of Tabernaemontana catharinensis A. DC.</a> Boligon, A.A., M. Piana, T.F. Kubica, D.N. Mario, T.V. Dalmolin, P.C. Bonez, R. Weiblen, L. Lovato, S.H. Alves, M.M.A. Campos, and M.L. Athayde. Journal of Applied Biomedicine, 2015. 13(1): p. 7-18. ISI[000350670600008].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000351218300003">Rescuing Compound Bioactivity in a Secondary Cell-based Screening by Using Gamma-cyclodextrin as a Molecular Carrier.</a> Claveria-Gimeno, R., S. Vega, V. Grazu, J.M. de la Fuente, A. Lanas, A. Velazquez-Campoy, and O. Abian. International Journal of Nanomedicine, 2015. 10: p. 2249-2259. ISI[000351218300003].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000351019500003">Investigation of the Antiviral Bioactivity of Lactobacillus bulgaricus 761N Extracellular Extract against Hepatitis C Virus (HCV).</a> El-Adawi, H., I. Nour, F. Fattouh, and N. El-Deeb. International Journal of Pharmacology, 2015. 11(1): p. 19-26. ISI[000351019500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350919100060">Design, Synthesis, and Antiviral Activity of Novel Rutin Derivatives Containing 1, 4-Pentadien-3-one Moiety.</a> Han, Y., Y. Ding, D.D. Xie, D.Y. Hu, P. Li, X.Y. Li, W. Xue, L.H. Jin, and B. Song. European Journal of Medicinal Chemistry, 2015. 92: p. 732-737. ISI[000350919100060].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0327-040915.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350670800017">Aptamers in Diagnostics and Treatment of Viral Infections.</a> Wandtke, T., J. Wozniak, and P. Kopinski. Viruses, 2015. 7(2): p. 751-780. ISI[000350670800017].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0327-040915.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
