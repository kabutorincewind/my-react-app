

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2008-11-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="z0aG0+PyVNCMdVcOtTTEGxIkDmdROUHRcY1zVVTvwZk97VyNeHBSv0Jk8782bx7/jry6qfILFN9MH/d3cHkLuoOU4BgjEyFtwNlZMcE6W68I20vIEwgy5H00JQ0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="155731C6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 30 - November 12, 2008</h1>

    <h2>Anti-HIV</h2>

    <p class="ListParagraph">1.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18996020?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Hunter R, Younis Y, Muhanji CI, Curtin TL, Naidoo KJ, Petersen M, Bailey CM, Basavapathruni A, Anderson KS.</a></p>

    <p class="plaintext">C-2-Aryl O-substituted HI-236 derivatives as non-nucleoside HIV-1 reverse-transcriptase inhibitors.</p>

    <p class="plaintext">Bioorg Med Chem. 2008 Nov 1. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18996020 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">2.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18823111?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Ghosh SK, Patra R, Rath SP.</a></p>

    <p class="plaintext">Axial ligand coordination in sterically strained vanadyl porphyrins: synthesis, structure, and properties.</p>

    <p class="plaintext">Inorg Chem. 2008 Nov 3;47(21):9848-56. Epub 2008 Sep 30.      </p>

    <p class="plaintext">PMID: 18823111 [PubMed - in process]</p> 

    <p class="ListParagraph">3.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18662985?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Qi Z, Shi W, Xue N, Pan C, Jing W, Liu K, Jiang S.</a></p>

    <p class="plaintext">Rationally Designed Anti-HIV Peptides Containing Multifunctional Domains as Molecule Probes for Studying the Mechanisms of Action of the First and Second Generation HIV Fusion Inhibitors.</p>

    <p class="plaintext">J Biol Chem. 2008 Oct 31;283(44):30376-84. Epub 2008 Jul 28.   </p>

    <p class="plaintext">PMID: 18662985 [PubMed - in process]</p>

    <h2>Anti-Viral and HIV</h2>

    <p class="ListParagraph">4.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18799178?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Cohen C, Forzan M, Sproat B, Pantophlet R, McGowan I, Burton D, James W.</a></p>

    <p class="plaintext">An aptamer that neutralizes R5 strains of HIV-1 binds to core residues of gp120 in the CCR5 binding site.</p>

    <p class="plaintext">Virology. 2008 Nov 10;381(1):46-54. Epub 2008 Sep 17. </p>

    <p class="plaintext">PMID: 18799178 [PubMed - in process]</p>

    <h2>(HIV or AIDS) and (Therapeutic or Compound)</h2>

    <p class="ListParagraph">5.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18984007?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Samuele A, Kataropoulou A, Viola M, Zanoli S, La Regina G, Piscitelli F, Silvestri R, Maga G.</a></p>

    <p class="plaintext">Non-nucleoside HIV-1 reverse transcriptase inhibitors di-halo-indolyl aryl sulfones achieve tight binding to drug-resistant mutants by targeting the enzyme-substrate complex.</p>

    <p class="plaintext">Antiviral Res. 2008 Nov 1. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18984007 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">6.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18835718?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Van Neck T, Pannecouque C, Vanstreels E, Stevens M, Dehaen W, Daelemans D.</a></p>

    <p class="plaintext">Inhibition of the CRM1-mediated nucleocytoplasmic transport by N-azolylacrylates: structure-activity relationship and mechanism of action.</p>

    <p class="plaintext">Bioorg Med Chem. 2008 Nov 1;16(21):9487-97. Epub 2008 Sep 20.        </p>

    <p class="plaintext">PMID: 18835718 [PubMed - in process]</p> 

    <p class="ListParagraph">7.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18642033?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Sapre NS, Gupta S, Pancholi N, Sapre N.</a></p>

    <p class="plaintext">Data mining using template-based molecular docking on tetrahydroimidazo-[4,5,1-jk][1,4]-benzodiazepinone (TIBO) derivatives as HIV-1RT inhibitors.</p>

    <p class="plaintext">J Mol Model. 2008 Nov;14(11):1009-21. Epub 2008 Jul 19.         </p>

    <p class="plaintext">PMID: 18642033 [PubMed - in process]</p>

    <h2> (HIV or AIDS) and Inhibition</h2>

    <p class="ListParagraph">8.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18996899?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Michalowski D, Chitima-Matsiga R, Held DM, Burke DH.</a></p>

    <p class="plaintext">Novel bimodular DNA aptamers with guanosine quadruplexes inhibit phylogenetically diverse HIV-1 reverse transcriptases.</p>

    <p class="plaintext">Nucleic Acids Res. 2008 Nov 7. [Epub ahead of print]   </p>

    <p class="plaintext">PMID: 18996899 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">9.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18983138?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Bacchi A, Biemmi M, Carcelli M, Carta F, Compari C, Fisicaro E, Rogolino D, Sechi M, Sippel M, Sotriffer CA, Sanchez TW, Neamati N.</a></p>

    <p class="plaintext">From Ligand to Complexes. Part 2. Remarks on Human Immunodeficiency Virus type 1 Integrase Inhibition by beta-Diketo Acid Metal Complexes.</p>

    <p class="plaintext">J Med Chem. 2008 Nov 5. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18983138 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">10.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18835718?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Van Neck T, Pannecouque C, Vanstreels E, Stevens M, Dehaen W, Daelemans D.</a></p>

    <p class="plaintext">Inhibition of the CRM1-mediated nucleocytoplasmic transport by N-azolylacrylates: structure-activity relationship and mechanism of action.</p>

    <p class="plaintext">Bioorg Med Chem. 2008 Nov 1;16(21):9487-97. Epub 2008 Sep 20.        </p>

    <p class="plaintext">PMID: 18835718 [PubMed - in process]</p> 

    <h2>(HIV or AIDS) and Inhibitor</h2>

    <p class="ListParagraph">11.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18993071?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Elworthy TR, Dunn JP, Hogg JH, Lam G, Saito YD, Silva TM, Stefanidis D, Woroniecki W, Zhornisky E, Zhou AS, Klumpp K.</a></p>

    <p class="plaintext">Orally bioavailable prodrugs of a BCS class 2 molecule, an inhibitor of HIV-1 reverse transcriptase.</p>

    <p class="plaintext">Bioorg Med Chem Lett. 2008 Nov 1. [Epub ahead of print]         </p>

    <p class="plaintext">PMID: 18993071 [PubMed - as supplied by publisher]</p>

    <h2>(HIV or AIDS) and (Therapeutic or Compound or Pharmaceutical)</h2>

    <p class="ListParagraph">12.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18713798?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Asamitsu K, Yamaguchi T, Nakata K, Hibi Y, Victoriano AF, Imai K, Onozaki K, Kitade Y, Okamoto T.</a></p>

    <p class="plaintext">Inhibition of Human Immunodeficiency Virus Type 1 Replication by Blocking I{kappa}B Kinase with Noraristeromycin.</p>

    <p class="plaintext">J Biochem. 2008 Nov;144(5):581-589. Epub 2008 Aug 19.          </p>

    <p class="plaintext">PMID: 18713798 [PubMed - as supplied by publisher]</p>

    <h2>Journal Check</h2>

    <p class="ListParagraph">13.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18842407?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Radi M, Angeli L, Franchi L, Contemori L, Maga G, Samuele A, Zanoli S, Armand-Ugon M, Gonzalez E, Llano A, Esté JA, Botta M.</a></p>

    <p class="plaintext">Towards novel S-DABOC inhibitors: Synthesis, biological investigation, and molecular modeling studies.</p>

    <p class="plaintext">Bioorg Med Chem Lett. 2008 Sep 23. [Epub ahead of print]        </p>

    <p class="plaintext">PMID: 18842407 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">14.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18834110?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Maga G, Falchi F, Garbelli A, Belfiore A, Witvrouw M, Manetti F, Botta M.</a></p>

    <p class="plaintext">Pharmacophore modeling and molecular docking led to the discovery of inhibitors of human immunodeficiency virus-1 replication targeting the human cellular aspartic acid-glutamic acid-alanine-aspartic acid box polypeptide 3.</p>

    <p class="plaintext">J Med Chem. 2008 Nov 13;51(21):6635-8. Epub 2008 Oct 4.      </p>

    <p class="plaintext">PMID: 18834110 [PubMed - in process]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
