

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-10-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="FN3NY2L2tpKgsNXGASTgApeldwJvcsuw1NMbiTmL4ikb54u3hBsHIfZ0ZEZUcwZ4ETR4cN6OciCoc+8U0OeGlqOjS5JHftDUkpWjEKBbGLvCB7mnOiXm3efMV34=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA34ADC4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">September 30, 2009 -October 13, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19805570?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Polyamine Transport as a Target for Pneumocystis Pneumonia Therapy.</a></p>

    <p class="NoSpacing">Liao CP, Phanstiel O 4th, Lasbury ME, Zhang C, Shao S, Durant PJ, Cheng BH, Lee CH.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Oct 5. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19805570 [PubMed - as supplied by publisher]</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19794038?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Combination Testing of MGCD290, a Hos2 Histone Deacetylase Inhibitor, with Azole Antifungals against Opportunistic Fungal Pathogens.</a></p>

    <p class="NoSpacing">Pfaller MA, Messer SA, Georgopapadakou N, Martell LA, Besterman JM, Diekema DJ.</p>

    <p class="NoSpacing">J Clin Microbiol. 2009 Sep 30. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19794038 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19783432?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Puupehanol, a sesquiterpene-dihydroquinone derivative from the marine sponge Hyrtios sp.</a></p>

    <p class="NoSpacing">Xu WH, Ding Y, Jacob MR, Agarwal AK, Clark AM, Ferreira D, Liang ZS, Li XC.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Nov 1;19(21):6140-3. Epub 2009 Sep 11.</p>

    <p class="NoSpacing">PMID: 19783432 [PubMed - in process]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19820144?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">2-Amino-3-(oxirane-2,3-dicarboxamido)-propanoyl-valine an effective peptide antibiotic from the epiphyte Pantoea agglomerans 48b/90.</a></p>

    <p class="NoSpacing">Sammer UF, Völksch B, Möllmann U, Schmidtke M, Spiteller P, Spiteller M, Spiteller D.</p>

    <p class="NoSpacing">Appl Environ Microbiol. 2009 Oct 9. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19820144 [PubMed - as supplied by publisher]</p> 

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">5. de Carvalho, L.P., G. Lin, X. Jiang, and C. Nathan, Nitazoxanide kills replicating and nonreplicating Mycobacterium tuberculosis and evades resistance. J Med Chem, 2009. 52(19): p. 5789-92; PMID[19736929] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19736929?ordinalpos=217&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19736929?ordinalpos=217&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>

    <p class="ListParagraph">6. Del Olmo, E., G.M. Molina-Salinas, R. Escarcena, M. Alves, J.L. Lopez-Perez, R. Hernandez-Pando, S. Said-Fernandez, and A.S. Feliciano, Simple dihydrosphyngosine analogues with potent activity against MDR-Mycobacterium tuberculosis. Bioorg Med Chem Lett, 2009. 19(19): p. 5764-8; PMID[19703769] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19703769?ordinalpos=243&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19703769?ordinalpos=243&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>

    <p class="ListParagraph">7. Feese, E. and R.A. Ghiladi, Highly efficient in vitro photodynamic inactivation of Mycobacterium smegmatis. J Antimicrob Chemother, 2009. 64(4): p. 782-5; PMID[19661130] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19661130?ordinalpos=272&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19661130?ordinalpos=272&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>

    <p class="ListParagraph">8. Ferreira Mde, L., T.R. Vasconcelos, E.M. de Carvalho, M.C. Lourenco, S.M. Wardell, J.L. Wardell, V.F. Ferreira, and M.V. de Souza, Synthesis and antitubercular activity of novel Schiff bases derived from d-mannitol. Carbohydr Res, 2009. 344(15): p. 2042-7; PMID[19709650] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19709650?ordinalpos=240&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19709650?ordinalpos=240&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>

    <p class="ListParagraph">9. Guo, S., S.K. Tipparaju, S.D. Pegan, B. Wan, S. Mo, J. Orjala, A.D. Mesecar, S.G. Franzblau, and A.P. Kozikowski, Natural product leads for drug discovery: isolation, synthesis and biological evaluation of 6-cyano-5-methoxyindolo[2,3-a]carbazole based ligands as antibacterial agents. Bioorg Med Chem, 2009. 17(20): p. 7126-30; PMID[19783449] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19783449?ordinalpos=172&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19783449?ordinalpos=172&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>

    <p class="ListParagraph">10. Huang, Q., J. Mao, B. Wan, Y. Wang, R. Brun, S.G. Franzblau, and A.P. Kozikowski, Searching for New Cures for Tuberculosis: Design, Synthesis, and Biological Evaluation of 2-Methylbenzothiazoles. J Med Chem, 2009; PMID[19817445] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19817445?ordinalpos=56&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19817445?ordinalpos=56&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">11. Cardoso, SH., et al</p>

    <p class="NoSpacing">SYNTHESIS AND ANTITUBERCULAR ACTIVITY OF ISONIAZID CONDENSED WITH</p>

    <p class="NoSpacing">CARBOHYDRATE DERIVATIVES  </p>

    <p class="NoSpacing">Quimica Nova (Brazil) 2009 32: 1557 - 1560</p> 

    <p class="ListParagraph">12. Lin, G., et al</p>

    <p class="NoSpacing">Inhibitors selective for mycobacterial versus human proteasomes</p>

    <p class="NoSpacing">NATURE    2009 (Sept. 16) 461: 621 -626</p> 

    <p class="ListParagraph">13. Kossakowski, J., et al</p>

    <p class="NoSpacing">Synthesis of new derivatives of 2,2-dimethyl-2,3-dihydro-7-benzo[b]furanol with potential antimicrobial activity</p>

    <p class="NoSpacing">MEDICINAL CHEMISTRY RESEARCH   2009 (Sept.) 18: 555 - 565</p> 

    <p class="ListParagraph">14. Larson, ET., et al</p>

    <p class="NoSpacing">Toxoplasma gondii Cathepsin L Is the Primary Target of the Invasion-inhibitory Compound Morpholinurea-leucyl-homophenyl-vinyl Sulfone Phenyl</p>

    <p class="NoSpacing">JOURNAL OF BIOLOGICAL CHEMISTRY  2009 (Sept.) 284: 26839 - 26850</p> 

    <p class="ListParagraph">15. Wang, WY., et al</p>

    <p class="NoSpacing">Discovery of highly potent novel antifungal azoles by structure-based rational design</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS  2009 (Oct.) 19: 5965 - 5969</p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
