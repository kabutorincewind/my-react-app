

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-325.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tFWvSc96xN+6QDsOhkCBu6qikjqlJA56Bp1zey88oU0byNOAkY+60ODad5O8RgsO8miLFCXT/GJ/Z1D6sNaea9fQjf1V5bH0GoMq0AlnJC6tyrD8ndPIJsJUIBc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE229E17" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-325-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47531   HIV-LS-325; PUBMED-HIV-6/27/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activities of novel N-alkoxy-arylsulfonamide-based HIV protease inhibitors</p>

    <p>          Sherrill, RG, Furfine, ES, Hazen, RJ, Miller, JF, Reynolds, DJ, Sammond, DM, Spaltenstein, A, Wheelan, P, and Wright, LL</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15975788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15975788&amp;dopt=abstract</a> </p><br />

    <p>2.     47532   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Mechanistic Insights into the Suppression of Drug Resistance by Human Immunodeficiency Virus Type 1 Reverse Transcriptase Using a-Boranophosphate Nucleoside Analogs</p>

    <p>          Deval, Jerome, Alvarez, Karine, Selmi, Boulbaba, Bermond, Marielle, Boretto, Joelle, Guerreiro, Catherine, Mulard, Laurence, and Canard, Bruno</p>

    <p>          Journal of Biological Chemistry <b>2005</b>.  280(5): 3838-3846</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     47533   HIV-LS-325; PUBMED-HIV-6/27/2005</p>

    <p class="memofmt1-2">          Novel 1-[2-(Diarylmethoxy)ethyl]-2-methyl-5-nitroimidazoles as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors. A Structure-Activity Relationship Investigation</p>

    <p>          De Martino, G, La Regina, G, Di, Pasquali A, Ragno, R, Bergamini, A, Ciaprini, C, Sinistro, A, Maga, G, Crespan, E , Artico, M, and Silvestri, R</p>

    <p>          J Med Chem <b>2005</b>.  48(13): 4378-4388</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15974590&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15974590&amp;dopt=abstract</a> </p><br />

    <p>4.     47534   HIV-LS-325; PUBMED-HIV-6/27/2005</p>

    <p class="memofmt1-2">          First Synthesis and Evaluation of the Inhibitory Effects of Aza Analogues of TSAO on HIV-1 Replication</p>

    <p>          Nguyen, Van Nhien A, Tomassi, C, Len, C, Marco-Contelles, JL, Balzarini, J, Pannecouque, C, De Clercq, E, and Postel, D</p>

    <p>          J Med Chem <b>2005</b>.  48(13): 4276-4284</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15974581&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15974581&amp;dopt=abstract</a> </p><br />

    <p>5.     47535   HIV-LS-325; PUBMED-HIV-6/27/2005</p>

    <p class="memofmt1-2">          Anti-HIV Bioactive Stilbene Dimers of Caragana rosea</p>

    <p>          Yang, GX, Zhou, JT, Li, YZ, and Hu, CQ</p>

    <p>          Planta Med <b>2005</b>.  71(6): 569-71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15971132&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15971132&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47536   HIV-LS-325; PUBMED-HIV-6/27/2005</p>

    <p class="memofmt1-2">          Thiourea-based non-nucleoside inhibitors of HIV reverse transcriptase as bifunctional organocatalysts in the asymmetric Strecker synthesis</p>

    <p>          Tsogoeva, SB, Hateley, MJ, Yalalov, DA, Meindl, K, Weckbecker, C, and Huthmacher, K</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15964195&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15964195&amp;dopt=abstract</a> </p><br />

    <p>7.     47537   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Diazaindole-dicarbonyl-piperazinyl antiviral agents</p>

    <p>          Bender, John A, Yang, Zhong, Kadow, John F, and Meanwell, Nicholas A</p>

    <p>          PATENT:  US <b>20050124623</b>  ISSUE DATE: 20050609</p>

    <p>          APPLICATION: 2004-62054  PP: 87 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     47538   HIV-LS-325; PUBMED-HIV-6/27/2005</p>

    <p class="memofmt1-2">          Structure and anti-HIV activity of micrandilactones B and C, new nortriterpenoids possessing a unique skeleton from Schisandra micrantha</p>

    <p>          Li, RT, Han, QB, Zheng, YT, Wang, RR, Yang, LM, Lu, Y, Sang, SQ, Zheng, QT, Zhao, QS, and Sun, HD</p>

    <p>          Chem Commun (Camb) <b>2005</b>.(23): 2936-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15957031&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15957031&amp;dopt=abstract</a> </p><br />

    <p>9.     47539   HIV-LS-325; PUBMED-HIV-6/27/2005</p>

    <p class="memofmt1-2">          In vitro HIV type 1 reverse transcriptase inhibitory activities of Thai medicinal plants and Canna indica L. rhizomes</p>

    <p>          Woradulayapinij, W, Soonthornchareonnon, N, and Wiwat, C</p>

    <p>          J Ethnopharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15951145&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15951145&amp;dopt=abstract</a> </p><br />

    <p>10.   47540   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Inhibiting replication of a retrovirus, such as human immunodeficiency virus (HIV), through modulation of HIV Vif- or Vpu-mediated ubiquitylation of host cell proteins, such as CEM15 or CD4</p>

    <p>          Payan, Donald G and Jenkins, Yonchu</p>

    <p>          PATENT:  WO <b>2005047476</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 109 pp.</p>

    <p>          ASSIGNEE:  (Rigel Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>11.   47541   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Methods and compositions for the inhibition of HIV-1 replication in hum an cells using inhibitors of p21/CDKN1A activity or gene expression, and anti-AIDS uses</p>

    <p>          Wahl, Sharon M, Vazquez-Maldonado, Nancy, and Greenwell-Wild, Teresa</p>

    <p>          PATENT:  WO <b>2005046732</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 98 pp.</p>

    <p>          ASSIGNEE:  (The United Sates of America as Represented by the Secretary of Health and Human Services, NIH USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   47542   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Preparation of alkylated pyrogallol calixarene type compounds as anti-viral compounds</p>

    <p>          Coveney, Donal and Costello, Benjamin</p>

    <p>          PATENT:  US <b>20050113454</b>  ISSUE DATE: 20050526</p>

    <p>          APPLICATION: 2003  PP: 13 pp.</p>

    <p>          ASSIGNEE:  (Aids Care Pharma Limited, Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   47543   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          A preparation of pyridinecarboxamide derivatives, useful for inhibiting HIV integrase</p>

    <p>          Kong, Laval Chan Chun, Zhang, Ming-Qiang, Halab, Liliane, Nguyen-Ba, Nghe, and Liu, Bingcan</p>

    <p>          PATENT:  WO <b>2005042524</b>  ISSUE DATE:  20050512</p>

    <p>          APPLICATION: 2004  PP: 139 pp.</p>

    <p>          ASSIGNEE:  (Virochem Pharma Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   47544   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Preparation of bipiperidinyl derivatives as inhibitors of CCR5 receptors</p>

    <p>          Miller, Michael W and Scott, Jack D</p>

    <p>          PATENT:  WO <b>2005042517</b>  ISSUE DATE:  20050512</p>

    <p>          APPLICATION: 2004  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   47545   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Preparation of hydroxy pyridopyrrolopyrazine dione compounds useful as HIV integrase inhibitors</p>

    <p>          Wai, John S, Fisher, Thorsten E, Zhuang, Linghang, Staas, Donnette D, Lyle, Terry A, Kim, Boyoung, Embrey, Mark W, Wiscount, Catherine M, Tran, Lekhanh O, Egbertson, Melissa, and Savage, Kelly L</p>

    <p>          PATENT:  WO <b>2005041664</b>  ISSUE DATE:  20050512</p>

    <p>          APPLICATION: 2004  PP: 181 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   47546   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Identification of compounds that inhibit replication of human immunodeficiency virus</p>

    <p>          Balzarini, Jan Maria Rene, Vahlne, Anders, Hogberg, Marita, and Tong, Weimin</p>

    <p>          PATENT:  US <b>2005096319</b>  ISSUE DATE:  20050505</p>

    <p>          APPLICATION: 92  PP: 91 pp., Cont.-in-part of U.S. Ser. No. 783,053.</p>

    <p>          ASSIGNEE:  (Belg.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>17.   47547   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Potent 1,3,4-trisubstituted pyrrolidine CCR5 receptor antagonists: effects of fused heterocycles on antiviral activity and pharmacokinetic properties</p>

    <p>          Kim, Dooseop, Wang, Liping, Hale, Jeffrey J, Lynch, Christopher L, Budhu, Richard J, MacCoss, Malcolm, Mills, Sander G, Malkowitz, Lorraine, Gould, Sandra L, DeMartino, Julie A, Springer, Martin S, Hazuda, Daria, Miller, Michael, Kessler, Joseph, Hrin, Renee C, Carver, Gwen, Carella, Anthony, Henry, Karen, Lineberger, Janet, Schleif, William A, and Emini, Emilio A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(8): 2129-2134</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   47548   HIV-LS-325; WOS-HIV-6/19/2005</p>

    <p class="memofmt1-2">          Synthesis, antiviral activity, and mechanism of drug resistance of D- and L-2 &#39;,3 &#39;-didehydro-2 &#39;,3 &#39;-dideoxy-2 &#39;-fluorocarbocyclic nucleosides</p>

    <p>          Wang, JN, Jin, YH, Rapp, KL, Bennett, M, Schinazi, RF, and Chu, CK</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(11): 3736-3748, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229443700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229443700011</a> </p><br />

    <p>19.   47549   HIV-LS-325; WOS-HIV-6/19/2005</p>

    <p class="memofmt1-2">          Peptides derived from the reverse transcriptase of human immunodeficiency virus type 1 as novel inhibitors of the viral integrase</p>

    <p>          Gleenberg, IO, Avidan, O, Goldgur, Y, Herschhorn, A, and Hizi, A</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(23): 21987-21996, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229557900041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229557900041</a> </p><br />

    <p>20.   47550   HIV-LS-325; SCIFINDER-HIV-6/20/2005</p>

    <p class="memofmt1-2">          Synthesis of fluoroalkyl end-capped oligomers containing pendant phosphinic and phosphonic acid segments-application to novel fluorinated bioactive polymers possessing antibacterial and anti-hiv-1 activities</p>

    <p>          Sawada, Hideo, Kawai, Yoshikazu, Kawase, Tokuzo, Inaba Masashi Sugiya, Yoshiko, Sugiya, Masashi, Baba, Masanori, and Tomita, Toshio</p>

    <p>          International Journal of Polymeric Materials <b>2005</b>.  54(4): 257-277</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   47551   HIV-LS-325; WOS-HIV-6/19/2005</p>

    <p class="memofmt1-2">          Discovery of natural product inhibitors of HIV-1 integrase at Merck</p>

    <p>          Singh, SB, Pelaez, F, Hazuda, DJ, and Lingham, RB</p>

    <p>          DRUGS OF THE FUTURE <b>2005</b>.  30(3): 277-299, 23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229605600007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229605600007</a> </p><br />

    <p>22.   47552   HIV-LS-325; WOS-HIV-6/19/2005</p>

    <p class="memofmt1-2">          New drug targets for HIV</p>

    <p>          Bean, P</p>

    <p>          CLINICAL INFECTIOUS DISEASES <b>2005</b>.  41: S96-S100, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229530700017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229530700017</a> </p><br />

    <p>23.   47553   HIV-LS-325; WOS-HIV-6/26/2005</p>

    <p class="memofmt1-2">          New inhibitors of the Tat-TAR RNA interaction found with a &quot;fuzzy&quot; pharmacophore model</p>

    <p>          Renner, S, Ludwig, V, Boden, O, Scheffer, U, Gobel, M, and Schneider, G</p>

    <p>          CHEMBIOCHEM <b>2005</b>.  6(6): 1119-1125, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229730000025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229730000025</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
