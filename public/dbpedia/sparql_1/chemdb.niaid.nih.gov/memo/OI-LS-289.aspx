

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-289.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nsq66fkeHW+KHhPa7m7Ff+F6sCMG3L9wOaL+9y35sKUuaQiXP+enSziVnUzTMqEvb2Vb/mnu81V4z9J70xjDj6mCnQRGJWFqF/Xkx3ygsSEWZPv8KZr6C3LKf2A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9C91A051" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-289-MEMO</b> </p>

<p>    1.    42815   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Hepatitis C. Development of new drugs and clinical trials: Promises and pitfalls. Summary of an AASLD hepatitis single topic conference, Chicago, IL, February 27-March 1, 2003</p>

<p>           Pawlotsky, JM and McHutchison, JG</p>

<p>           Hepatology 2004. 39(2): 554-567</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14768012&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14768012&amp;dopt=abstract</a> </p><br />

<p>    2.    42816   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Macro-array and bioinformatic analyses reveal mycobacterial &#39;core&#39; genes, variation in the ESAT-6 gene family and new phylogenetic markers for the Mycobacterium tuberculosis complex</p>

<p>           Marmiesse, M, Brodin, P, Buchrieser, C, Gutierrez, C, Simoes, N, Vincent, V, Glaser, P, Cole, ST, and Brosch, R</p>

<p>           Microbiology 2004. 150(Pt 2): 483-496</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766927&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766927&amp;dopt=abstract</a> </p><br />

<p>    3.    42817   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Genotypic and Phenotypic Characterization of Drug-Resistant Mycobacterium tuberculosis Isolates from Rural Districts of the Western Cape Province of South Africa</p>

<p>           Streicher, EM, Warren, RM, Kewley, C, Simpson, J, Rastogi, N, Sola, C, Van Der Spuy, GD, Van HelDen PD, and Victor, TC</p>

<p>           J Clin Microbiol 2004. 42(2): 891-894</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766882&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766882&amp;dopt=abstract</a> </p><br />

<p>    4.    42818   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           A complete shikimate pathway in Toxoplasma gondii: an ancient eukaryotic innovation</p>

<p>           Campbell, SA, Richards, TA, Mui, EJ, Samuel, BU, Coggins, JR, McLeod, R, and Roberts, CW</p>

<p>           International Journal for Parasitology 2004. 34(1): 5-13</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    5.    42819   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           CryptoDB: the Cryptosporidium genome resource</p>

<p>           Puiu, Daniela, Enomoto, Shinichiro, Buck, Gregory A, Abrahamsen, Mitchell S, and Kissinger, Jessica C</p>

<p>           Nucleic Acids Research 2004. 32(Database): D329-D331</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    6.    42820   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Detection of rifampin-resistant Mycobacterium tuberculosis strains by using a specialized oligonucleotide microarray</p>

<p>           Yue, J, Shi, W, Xie, J, Li, Y, Zeng, E, Liang, L, and Wang, H</p>

<p>           Diagn Microbiol Infect Dis 2004. 48(1): 47-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14761721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14761721&amp;dopt=abstract</a> </p><br />

<p>    7.    42821   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Virtual generation of agents against Mycobacterium tuberculosis. A QSAR study</p>

<p>           Besalu, E, Ponec, R, and de Julian-Ortiz, JV</p>

<p>           Mol Divers 2003. 6(2): 107-20</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14761161&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14761161&amp;dopt=abstract</a> </p><br />

<p>    8.    42822   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Design, Synthesis, and Evaluation of 9-d-Ribitylamino-1,3,7,9-tetrahydro-2,6,8-purinetriones Bearing Alkyl Phosphate and alpha,alpha-Difluorophosphonate Substituents as Inhibitors of Riboflavin Synthase and Lumazine Synthase</p>

<p>           Cushman, M, Sambaiah, T, Jin, G, Illarionov, B, Fischer, M, and Bacher, A</p>

<p>           J Org Chem 2004. 69(3): 601-612</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14750781&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14750781&amp;dopt=abstract</a> </p><br />

<p>    9.    42823   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           History of the development of azole derivatives</p>

<p>           Maertens, JA</p>

<p>           Clin Microbiol Infect 2004. 10 Suppl 1: 1-10</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14748798&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14748798&amp;dopt=abstract</a> </p><br />

<p>  10.    42824   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           Synthesis and antimicrobial activity of some benzazole derivatives</p>

<p>           Karali, N, Cesur, N, Guersoy, A, Ates, Oe, OezDen S, Oetuek, G, and Birteksoez, S</p>

<p>           Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry 2004. 43B(1): 212-216</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  11.    42825   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Citrafungins A and B, Two New Fungal Metabolite Inhibitors of GGTase I with Antifungal Activity</p>

<p>           Singh, SB, Zink, DL, Doss, GA, Polishook, JD, Ruby, C, Register, E, Kelly, TM, Bonfiglio, C, Williamson, JM, and Kelly, R</p>

<p>           Org Lett 2004. 6 (3): 337-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14748587&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14748587&amp;dopt=abstract</a> </p><br />

<p>  12.    42826   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Development of a GB virus B marmoset model and its validation with a novel series of hepatitis C virus NS3 protease inhibitors</p>

<p>           Bright, H, Carroll, AR, Watts, PA, and Fenton, RJ</p>

<p>           J Virol 2004. 78(4): 2062-71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747571&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747571&amp;dopt=abstract</a> </p><br />

<p>  13.    42827   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           A cytomegalovirus inhibitor of gamma interferon signaling controls immunoproteasome induction</p>

<p>           Khan, S, Zimmermann, A, Basler, M, Groettrup, M, and Hengel, H</p>

<p>           J Virol 2004. 78(4): 1831-42</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747547&amp;dopt=abstract</a> </p><br />

<p>  14.    42828   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           P-glycoprotein inhibitors modulate accumulation and efflux of xenobiotics in extra and intracellular Toxoplasma gondii</p>

<p>           Sauvage, V, Aubert, D, Bonhomme, A, Pinon, JM, and Millot, JM</p>

<p>           Mol Biochem Parasitol 2004. 134(1):  89-95</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747146&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747146&amp;dopt=abstract</a> </p><br />

<p>  15.    42829   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Evaluation of the fully automated Bactec MGIT 960 system for the susceptibility testing of Mycobacterium tuberculosis to first-line drugs: a multicenter study</p>

<p>           Kontos, F, Maniati, M, Costopoulos, C, Gitti, Z, Nicolaou, S, Petinaki, E, Anagnostou, S, Tselentis, I, and Maniatis, AN</p>

<p>           J Microbiol Methods 2004. 56(2): 291-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14744458&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14744458&amp;dopt=abstract</a> </p><br />

<p>  16.    42830   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Tuberculosis: not a thing of the past</p>

<p>           Murphy, D</p>

<p>           Posit Aware 2003. 14(6): 35</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14743793&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14743793&amp;dopt=abstract</a> </p><br />

<p>  17.    42831   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Inhibition of the subgenomic hepatitis C virus replicon in huh-7 cells by 2&#39;-deoxy-2&#39;-fluorocytidine</p>

<p>           Stuyver, LJ, McBrayer, TR, Whitaker, T, Tharnish, PM, Ramesh, M, Lostia, S, Cartee, L, Shi, J, Hobbs, A, Schinazi, RF, Watanabe, KA, and Otto, MJ</p>

<p>           Antimicrob Agents Chemother 2004.  48(2): 651-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14742230&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14742230&amp;dopt=abstract</a> </p><br />

<p>  18.    42832   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Effect of interferon alpha and cell cycle progression on translation mediated by the hepatitis C virus 5&#39; untranslated region: a study using a transgenic mouse model</p>

<p>           TakeDa Y, Okoshi, S, Suzuki, K, Yano, M, Gangemi, JD, Jay, G, Asakura, H, and Aoyagi, Y</p>

<p>           J Viral Hepat  2004. 11(1): 33-44</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14738556&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14738556&amp;dopt=abstract</a> </p><br />

<p>  19.    42833   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           A new indolizinone from Polygonatum kingianum</p>

<p>           Wang, YF, Lu, CH, Lai, GF, Cao, JX, and Luo, SD</p>

<p>           Planta Med 2003. 69(11): 1066-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14735451&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14735451&amp;dopt=abstract</a> </p><br />

<p>  20.    42834   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Recombinant Mycobacteria expressing major extracellular non-fusion proteins of Mycobacteria or other intracellular pathogen for inducing immune responses</b>((USA))</p>

<p>           Horwitz, Marcus A, Harth, Gunter, and Tullius, Michael V</p>

<p>           PATENT: US 20040009184 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003-46395; PP: 34 pp., Cont.-in-part of U.S. Pat. Appl. 2003 124,135.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    42835   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           Hepatitis C virus replicons finally get to second base</p>

<p>           Evans, MJ and Rice, CM</p>

<p>           Gastroenterology 2003. 125(6): 1892-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14724844&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14724844&amp;dopt=abstract</a> </p><br />

<p>  22.    42836   OI-LS-289; PUBMED-OI-2/11/2004</p>

<p class="memofmt1-2">           A reporter-based assay for identifying hepatitis C virus inhibitors based on subgenomic replicon cells</p>

<p>           Lee, JC, Chang, CF, Chi, YH, Hwang, DR, and Hsu, JT</p>

<p>           J Virol Methods 2004. 116(1): 27-33</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14715304&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14715304&amp;dopt=abstract</a> </p><br />

<p>  23.    42837   OI-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of antimicrobial activity of new 3-hydroxy-6-methyl-4-oxo-4H-pyran-2-carboxamide derivatives</p>

<p>           Aytemir, MD, Erol, DD, Hider, RC, and Ozalp, M</p>

<p>           TURK J CHEM 2003. 27(6): 757-764</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187955700012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187955700012</a> </p><br />

<p>  24.    42838   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           Individual Mycobacterium tuberculosis resuscitation-promoting factor homologues are dispensable for growth in vitro and in vivo</p>

<p>           Tufariello, JoAnn M, Jacobs, William R Jr, and Chan, John</p>

<p>           Infection and Immunity 2004. 72(1): 515-526</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    42839   OI-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Novel 3 &#39;-deoxy analogs of the anti-HBV agent entecavir: synthesis of enantiomers from a single chiral epoxide</p>

<p>           Ruediger, E, Martel, A, Meanwell, N, Solomon, C, and Turmel, B</p>

<p>           TETRAHEDRON LETT 2004. 45(4): 739-742</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187897900019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187897900019</a> </p><br />

<p>  26.    42840   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           The Prodrug Activator EtaA from Mycobacterium tuberculosis Is a Baeyer-Villiger Monooxygenase</p>

<p>           Fraaije, Marco W, Kamerbeek, Nanne M, Heidekamp, Annelies J, Fortin, Riccardo, and Janssen, Dick B</p>

<p>           Journal of Biological Chemistry 2004. 279(5): 3354-3360</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    42841   OI-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           ANTIMIC: a database of antimicrobial sequences</p>

<p>           Brahmachary, M, Krishnan, SPT, Koh, JLY, Khan, AM, Seah, SH, Tan, TW, Brusic, V, and Bajic, VB</p>

<p>           NUCLEIC ACIDS RES 2004. 32: D586-D589</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188079000139">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188079000139</a> </p><br />

<p>  28.    42842   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Biflavonoids, flavonoids, chalcones and chalcone-like compounds and use against mycobacterium infections</b> ((Advanced Life Sciences, Inc. USA)</p>

<p>           Lin, Yuh-meei </p>

<p>           PATENT: US 6677350 B1;  ISSUE DATE: 20040113</p>

<p>           APPLICATION: 2000-11771; PP: 19 pp., Cont.-in-part of U.S. Provisional Ser. No. 155,519.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  29.    42843   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis Ribose-5-phosphate Isomerase has a Known Fold, but a Novel Active Site</p>

<p>           Roos, Annette K, Andersson, CEvalena, Bergfors, Terese, Jacobsson, Micael, Karlen, Anders, Unge, Torsten, Jones, TAlwyn, and Mowbray, Sherry L</p>

<p>           Journal of Molecular Biology 2004.  335(3): 799-809</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.    42844   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           M. tuberculosis entry and growth using macrophage models</p>

<p>           Toossi, Zahra and Schlesinger, Larry S</p>

<p>           CONFERENCE: Tuberculosis 2004: 1-24</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    42845   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of rifabutine derivatives useful as antimicrobial agents</b>((Instituto Nacional de Engenharia e Tecnologia Industrial, Port.)</p>

<p>           Medeiros, Maria, Costa, Maria, Figueiredo, Ricardo, Rosa, Maria, Curto, Maria, Santos, Lina, Cruz, Maria, Gaspar, Maria, and Feio, Sonia</p>

<p>           PATENT: WO 2004005298 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 44 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    42846   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Alkaloid derivatives as inhibitors of acylglucosaminylinositol amidase and antibacterial agents</b> ((The United States of America, as Represented by the Secretary Department of Health and Human Services USA)</p>

<p>           Bewley, Carole, Nicholas, Gillian, Nicolaou, KC, Barluenga, Sofia, Pfefferkorn, Jeffrey, and Hughes, Robert</p>

<p>           PATENT: WO 2004004659 A2;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 61 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.    42847   OI-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Cyclic voltammetric determination of free radical species from nitroimidazopyran: a new antituberculosis agent</p>

<p>           Bollo, S, Nunez-Vergara, LJ, and Squella, JA</p>

<p>           J ELECTROANAL CHEM 2004. 562(1): 9-14</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188066500002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188066500002</a> </p><br />

<p>  34.    42848   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Reagents and method for diagnosis of active tuberculosis or active acid-fast bacterial diseases, and test tools and kits using the reagents</b>((Japan BCG Laboratory, Japan and Nippon Koketsu Kanso Kenkyusho K. K.) )</p>

<p>           Yano, Ikuya, Sato, Yukihiro, Otsuka, Katsuji, Fujita, Yukiko, and Doi, Takeshi</p>

<p>           PATENT: JP 2004003912 A2;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2002-47148; PP: 20 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  35.    42849   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of acyclic nucleosides and nucleotides as antiviral agents</b>((Taiwan))</p>

<p>           Hakimelahi, Gholam Hossein</p>

<p>           PATENT: US 2004002475 A1;  ISSUE DATE: 20040101</p>

<p>           APPLICATION: 2002-52806; PP: 21 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.    42850   OI-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Non-nucleoside inhibitors of the hepatitis C virus NS5B polymerase: discovery and preliminary SAR of benzimidazole derivatives</p>

<p>           Beaulieu, PL, Bos, M, Bousquet, Y, Fazal, G, Gauthier, J, Gillard, J, Goulet, S, LaPlante, S, Poupart, MA, Lefebvre, S, McKercher, G, Pellerin, C, Austel, V, and Kukolj, G</p>

<p>           BIOORG MED CHEM LETT 2004. 14(1): 119-124</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000023">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000023</a> </p><br />

<p>  37.    42851   OI-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Discovery of a novel bicycloproline P2 bearing peptidyl alpha-ketoamide LY514962 as HCV protease inhibitor</p>

<p>           Yip, Y, Victor, F, Lamar, J, Johnson, R, Wang, QM, Barket, D, Glass, J, Jin, L, Liu, LF, Venable, D, Wakulchik, M, Xie, CP, Heinz, B, Villarreal, E, Colacino, J, Yumibe, N, Tebbe, M, Munroe, J, and Chen, SH</p>

<p>           BIOORG MED CHEM LETT 2004. 14(1): 251-256</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000049">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000049</a> </p><br />

<p>  38.    42852   OI-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           P1 and P3 optimization of novel bicycloproline P2 bearing tetrapeptidyl alpha-ketoamide based HCV protease inhibitors</p>

<p>           Victor, F, Lamar, J, Snyder, N, Yip, Y, Guo, DQ, Yumibe, N, Johnson, RB, Wang, QM, Glass, JI, and Chen, SH</p>

<p>           BIOORG MED CHEM LETT 2004. 14(1): 257-261</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000050">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000050</a> </p><br />

<p>  39.    42853   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of imidazopyridines as viral inhibitors</b>((K.U.Leuven Research &amp; Development, Belg. and Gilead Sciences, Inc.)</p>

<p>           Neyts, Johan, Puerstinger, Gerhard, and De Clercq, Erik</p>

<p>           PATENT: WO 2004005286 A2;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 149 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  40.    42854   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of 1,2-disubstituted 1,4-dihydro-4-oxoquinoline compounds as antiviral agents</b>  ((Maruishi Pharmaceutical Co., Ltd. Japan )</p>

<p>           Tamura, Takashi, Kuriyama, Haruo, Agoh, Masanobu, Agoh, Yumi, Soga, Manabu, and Mori, Teruyo</p>

<p>           PATENT: EP 1380575 A1;  ISSUE DATE: 20040114</p>

<p>           APPLICATION: 2000; PP: 44 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  41.    42855   OI-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           In vitro evaluation of the antimicrobial activity of chlorhexidine and sodium hypochlorite</p>

<p>           Vianna, ME, Gomes, BPFA, Berber, VB, Zaia, AA, Ferraz, CCR, and De Souza, FJ</p>

<p>           ORAL SURG ORAL MED O 2004. 97(1): 79-84</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188115800016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188115800016</a> </p><br />

<p>  42.    42856   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of nucleoside derivatives as antiviral agents and inhibitors of RNA-dependent RNA viral polymerase</b> ((Merck &amp; Co., Inc. USA and Isis Pharmaceuticals, Inc.)</p>

<p>           Bhat, Balkrishen, Carroll, Steven S, Eldrup, Anne B, Maccoss, Malcolm, Olsen, David B, and Prakash, Thazha P</p>

<p>           PATENT: WO 2004007512 A2;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003; PP: 50 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  43.    42857   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Use of hepatitis C virus p7 protein for screening for antiviral drugs</b> ( (University of Leeds, UK)</p>

<p>           Rowlands, David and Griffin, Stephen</p>

<p>           PATENT: WO 2004005333 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 33 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  44.    42858   OI-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           Reverse transcriptase activity of hepatitis B virus (HBV) DNA polymerase within core capsid: Interaction with deoxynucleoside triphosphates and anti-HBV L-deoxynucleoside analog triphosphates</p>

<p>           Lam, W, Li, Y, Liou, JY, Dutschman, GE, and Cheng, YC</p>

<p>           MOL PHARMACOL  2004. 65(2): 400-406</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188329800016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188329800016</a> </p><br />

<p>  45.    42859   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           E2 glycoprotein-based methods for identification of inhibitors of hepatitis C virus (HCV) infection, therapeutic methods, and methods for LDL level reduction</b>((University of Iowa Research Foundation, USA)</p>

<p>           Stapleton, Jack T and Wuenschmann, Sabina</p>

<p>           PATENT: WO 2004003141 A2;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 135 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  46.    42860   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of N-alkenyl carboxamide inhibitors of HCV NS5b polymerase for treating hepatitis C viral infections and associated diseases</b>((Pharmacia &amp; Upjohn Company, USA and Finzel, Barry C.)</p>

<p>           Gao, Hua, Greene, Meredith L, Gross, Rebecca J, Nugent, Richard A, and Pfefferkorn, Jeffrey</p>

<p>           PATENT: WO 2004002977 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 186 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  47.    42861   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           (E)-1-[[(2E, 4E)-1-hexyl-2,4-octadecadienyl]oxy]-2-hydroxydiazine and a pharmaceutical composition for treating hepatitis</b> ((Biokorea Co., Ltd. S. Korea)</p>

<p>           Lea, Young-Sung, Ahn, Dong-Ho, Lim, Yoong-Ho, Yoon, Seung-Kew, Kim, Man-Bae, Kim, Nam-Jin, Chang, Eun-Joo, and Lee, Min-Kyung</p>

<p>           PATENT: WO 2004002945 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 41 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  48.    42862   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of N-(phenylaminophenyl)carboxamide and related compound inhibitors of HCV NS5b polymerase for treating hepatitis C viral infections and associated diseases</b>((Pharmacia &amp; Upjohn Company, USA)</p>

<p>           Finzel, Barry C, Funk, Lee A, Kelly, Robert C, Reding, Matthew T, and Wicnienski, Nancy Anne</p>

<p>           PATENT: WO 2004002944 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 45 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  49.    42863   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           Preparation of N,N&#39;-bis(o-carboxyphenyl)-o-diaminobenzene derivative inhibitors of HCV NS5b polymerase for treating hepatitis C viral infections and associated diseases</b>((Pharmacia &amp; Upjohn Company, USA and Pharmacia Italia S.p.A.))</p>

<p>           Battistini, Carlo, Casuscelli, Francesco, Kelly, Robert C, Maggiora, Linda L, Mitchell, Mark A, Piutti, Claudia, and Reding, Matthew T</p>

<p>           PATENT: WO 2004002940 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 74 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  50.    42864   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p><b>           2&#39;-C-methyl-3&#39;-O-L-valine ester ribofuranosyl cytidine for treatment of flaviviridae infections</b>((Idenix (Cayman) Limited, Cayman I. and Universita Degli Studi di Cagliari))</p>

<p>           Sommadossi, Jean-Pierre and La Colla, Paolo</p>

<p>           PATENT: WO 2004002422 A2;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 110 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  51.    42865   OI-LS-289; SCIFINDER-OI-2/4/2004</p>

<p class="memofmt1-2">           In vitro antiviral susceptibility of full-length clinical hepatitis B virus isolates cloned with a novel expression vector</p>

<p>           Yang, Huiling, Westland, Christopher, Xiong, Shelly, and Delaney, William E</p>

<p>           Antiviral Research 2004. 61(1): 27-36</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  52.    42866   OI-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           Synthesis of a novel class of sulfonium ions as potential inhibitors of UDP-galactopyranose mutase</p>

<p>           Ghavami, A, Chen, JJW, and Pinto, BM</p>

<p>           CARBOHYD RES 2004. 339(2): 401-407</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188118300025">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188118300025</a> </p><br />

<p>  53.    42867   OI-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           Acetylene-based analogues of thiolactomycin, active against Mycobacterium tuberculosis mtFabH fatty acid condensing enzyme</p>

<p>           Senior, SJ, Illarionov, PA, Gurcha, SS, Campbell, IB, Schaeffer, ML, Minnikin, DE, and Besra, GS</p>

<p>           BIOORG MED CHEM LETT 2004. 14(2): 373-376</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188119500016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188119500016</a> </p><br />

<p>  54.    42868   OI-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           Hepatitis CNS3 protease inhibition by peptidyl-alpha-ketoamide inhibitors: kinetic mechanism and structure</p>

<p>           Liu, YY, Stoll, VS, Richardson, PL, Saldivar, A, Klaus, JL, Molla, A, Kohlbrenner, W, and Kati, WM</p>

<p>           ARCH BIOCHEM BIOPHYS 2004. 421(2):  207-216</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188117600004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188117600004</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
