

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-03-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Ue9ttfjjVk4L5SVLEIsyfWJY/1ye7nuFcb/NXFm24C2hpW0AiRYuBHE418zUwNXM0ZniRDjLv0mmCC6JWw1EaAeb//cKuRr9OY28o2b32DsGQZlpGErpCLCuPhc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="32C4D9CB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">March 18 - March 31, 2011</p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21439070">In Vitro Evaluation of Antiviral and Virucidal Activity of a High Molecular Weight Hyaluronic Acid</a>. Cermelli, C., A. Cuoghi, M. Scuri, C. Bettua, R.G. Neglia, A. Ardizzoni, E. Blasi, T. Iannitti, and B. Palmieri. Virology Journal, 2011. 8(1): p. 141; PMID[21439070].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p> 

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21444704">The Inhibitory Effect on Hepatitis C Virus Infection of a Triterpenoid Compound, with or without Interferon-Alpha</a>. Watanabe, T., N. Sakamoto, M. Nakagawa, S. Kakinuma, Y. Itsui, Y. Nishimura-Sakurai, M. Ueyama, Y. Funaoka, A. Kitazume, S. Nitta, K. Kiyohashi, M. Murakawa, S. Azuma, K. Tsuchiya, S. Oooka, and M. Watanabe. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21444704].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21441029">Investigation of the Mode of Binding of a Novel Series of N-Benzyl-4-Heteroaryl-1-(Phenylsulfonyl)Piperazine-2-Carboxamides to the Hepatitis C Virus Polymerase</a>. Gentles, R.G., S. Sheriff, B.R. Beno, C. Wan, K. Kish, M. Ding, X. Zheng, L. Chupak, M.A. Poss, M.R. Witmer, P. Morin, Y.K. Wang, K. Rigat, J. Lemm, S. Voss, M. Liu, L. Pelosi, S.B. Roberts, M. Gao, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21441029].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21440437">Potent Inhibitors of Hepatitis C Core Dimerization as New Leads for Anti-Hepatitis C Agents</a>. Ni, F., S. Kota, V. Takahashi, A.D. Strosberg, and J.K. Snyder. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21440437].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21440018">Inhibition of Hepatitis C Virus Replication by Herbal Extract: Phyllanthus Amarus as Potent Natural Source</a>. Ravikumar, Y.S., U. Ray, M. Nandhitha, A. Perween, H.R. Naika, N. Khanna, and S. Das. Virus Research, 2011. <b>[Epub ahead of print]</b>; PMID[21440018].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21436155">Inhibition of Hepatitis C Virus Replication by Semi-Synthetic Derivatives of Glycopeptide Antibiotics</a>. Obeid, S., S.S. Printsevskaya, E.N. Olsufyeva, K. Dallmeier, D. Durantel, F. Zoulim, M.N. Preobrazhenskaya, J. Neyts, and J. Paeshuyse. The Journal of antimicrobial chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21436155].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21436031">Broad-Spectrum Antiviral That Interferes with De Novo Pyrimidine Biosynthesis</a>. Hoffmann, H.H., A. Kunz, V.A. Simon, P. Palese, and M.L. Shaw. Proceedings of the National Academy of Sciences of the United States of America, 2011. <b>[Epub ahead of print]</b>; PMID[21436031].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21430055">Antiviral Stilbene 1,2-Diamines Prevent Initiation of Hepatitis C Viral Rna Replication at the Outset of Infection</a>. Gastaminza, P., S.M. Pitram, M. Dreux, L.B. Krasnova, C. Whitten-Bauer, J. Dong, J. Chung, V.V. Fokin, K.B. Sharpless, and F.V. Chisari. Journal of Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21430055].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21426157">The Effect of the P1 Side Chain on the Binding of Optimized Carboxylate and Activated Carbonyl Inhibitors of the Hepatitis C Virus Ns3 Protease</a>. Kawai, S.H., S.R. Laplante, M. Llinas-Brunet, and O. Hucke. Future Medicinal Chemistry, 2010. 2(7): p. 1073-1081; PMID[21426157].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21414145">2,3-Dihydro-1,2-Diphenyl-Substituted 4h-Pyridinone Derivatives as New Anti Flaviviridae Inhibitors</a>. Peduto, A., A. Massa, A. Di Mola, P. de Caprariis, P. La Colla, R. Loddo, S. Altamura, G. Maga, and R. Filosa. Chemical Biology &amp; Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21414145].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286965400039">Synthesis and Anti-Hepatitis B Virus Activity of Acyclovir Conjugated Stearic Acid-G-Chitosan Oligosaccharide Micelle</a>. Huang, S.T., Y.Z. Du, H. Yuan, X.G. Zhang, J. Miao, F.D. Cui, and F.Q. Hu. Carbohydrate Polymers, 2011. 83(4): p. 1715-1722; PMID[ISI:000286965400039].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287570400004">Host Sphingolipid Biosynthesis Is a Promising Therapeutic Target for the Inhibition of Hepatitis B Virus Replication</a>. Tatematsu, K., Y. Tanaka, M. Sugiyama, M. Sudoh, and M. Mizokami. Journal of Medical Virology, 2011. 83(4): p. 587-593; PMID[ISI:000287570400004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0318-033111.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
