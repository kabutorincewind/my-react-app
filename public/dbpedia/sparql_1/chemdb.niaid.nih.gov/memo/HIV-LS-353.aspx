

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-353.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hHJOvnvY8VEIyyhKj/86QObPKzkBlYGPsHpqn3KEARiqgyNWmcYeTGfI956mENfPpTNazTnwifnxkjBOpN8i47npuDolSEe/pN1Pot8uVSQOro19/FrEaTldKwQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2EF021F6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-353-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49167   HIV-LS-353; PUBMED-HIV-7/24/2006</p>

    <p class="memofmt1-2">          An antifungal peptide from baby lima bean</p>

    <p>          Wang, HX and Ng, TB</p>

    <p>          Appl Microbiol Biotechnol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16850300&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16850300&amp;dopt=abstract</a> </p><br />

    <p>2.     49168   HIV-LS-353; PUBMED-HIV-7/24/2006</p>

    <p class="memofmt1-2">          A tyrosine-sulfated peptide derived from the heavy-chain CDR3 region of an HIV-1 neutralizing antibody binds GP120 and inhibits HIV-1 infection</p>

    <p>          Dorfman, T, Moore, MJ, Guth, AC, Choe, H, and Farzan, M</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16849323&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16849323&amp;dopt=abstract</a> </p><br />

    <p>3.     49169   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          The influence of natural substrates and inhibitors on the nucleotide-dependent excision activity of HIV-1 reverse transcriptase in the infected cell</p>

    <p>          Smith, Anthony James and Scott, Walter Alvin</p>

    <p>          Current Pharmaceutical Design <b>2006</b>.  12(15): 1827-1841</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     49170   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          Application of stigmastane-3,5,6-triol and its derivatives in preparation of antiviral drugs and their new derivatives</p>

    <p>          Sun, Yanrong, Zhang, Jiajie, Xu, Wei, and Wu, Shuguang</p>

    <p>          PATENT:  CN <b>1726922</b>  ISSUE DATE: 20060201</p>

    <p>          APPLICATION: 1007-4395</p>

    <p>          ASSIGNEE:  (Southern Medical University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     49171   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          Indolyl aryl sulphones as HIV-1 non-nucleoside reverse transcriptase inhibitors: synthesis, biological evaluation and binding mode studies of new derivatives at indole-2-carboxamide</p>

    <p>          De Martino, Gabriella, a Regina, Giuseppe, Ragno, Rino, Coluccia, Antonio, Bergamini, Alberto, Ciaprini, Chiara, Sinistro, Anna, Maga, Giovanni, Crespan, Emmanuele, Artico, Marino, and Silvestri, Romano</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(2): 59-77</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     49172   HIV-LS-353; PUBMED-HIV-7/24/2006</p>

    <p class="memofmt1-2">          Novel broad-spectrum thiourea non-nucleoside inhibitors for the prevention of mucosal HIV transmission</p>

    <p>          D&#39;Cruz, OJ and Uckun, FM</p>

    <p>          Curr HIV Res <b>2006</b>.  4(3): 329-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16842085&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16842085&amp;dopt=abstract</a> </p><br />

    <p>7.     49173   HIV-LS-353; PUBMED-HIV-7/24/2006</p>

    <p class="memofmt1-2">          The Hunt for HIV-1 Integrase Inhibitors</p>

    <p>          Lataillade, M and Kozal, MJ</p>

    <p>          AIDS Patient Care STDS <b>2006</b>.  20(7): 489-501</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16839248&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16839248&amp;dopt=abstract</a> </p><br />

    <p>8.     49174   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          Facile Purification of Honokiol and Its Antiviral and Cytotoxic Properties</p>

    <p>          Amblard, Franck, Delinsky, David, Arbiser, Jack L, and Schinazi, Raymond F</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(11): 3426-3427</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     49175   HIV-LS-353; PUBMED-HIV-7/24/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of the combinatorial library with a new spirodiketopiperazine scaffold. Discovery of novel potent and selective low-molecular-weight CCR5 antagonists</p>

    <p>          Habashita, H, Kokubo, M, Hamano, S, Hamanaka, N, Toda, M, Shibayama, S, Tada, H, Sagawa, K, Fukushima, D, Maeda, K, and Mitsuya, H</p>

    <p>          J Med Chem <b>2006</b>.  49(14): 4140-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16821774&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16821774&amp;dopt=abstract</a> </p><br />

    <p>10.   49176   HIV-LS-353; PUBMED-HIV-7/24/2006</p>

    <p class="memofmt1-2">          Pongamone A-E, five flavonoids from the stems of a mangrove plant, Pongamia pinnata</p>

    <p>          Li, L, Li, X, Shi, C, Deng, Z, Fu, H, Proksch, P, and Lin, W</p>

    <p>          Phytochemistry  <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16814820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16814820&amp;dopt=abstract</a> </p><br />

    <p>11.   49177   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          Synthesis of methylenecyclopropane analogs of antiviral nucleoside phosphonates</p>

    <p>          Yan, Zhaohua, Zhou, Shaoman, Kern, Earl R, and Zemlicka, Jiri</p>

    <p>          Tetrahedron <b>2006</b>.  62(11): 2608-2615</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   49178   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          Patented HIV-1 integrase inhibitors (1998-2005)</p>

    <p>          Cotelle, Philippe</p>

    <p>          Recent Patents on Anti-Infective Drug Discovery <b>2006</b>.  1(1 ): 1-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   49179   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          Computational and synthetic approaches for the discovery of HIV-1 integrase inhibitors</p>

    <p>          Barreca, Maria Letizia, De Luca, Laura, Ferro, Stefania, Rao, Angela, Monforte, Anna-Maria, and Chimirri, Alba</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2006</b>.(7): 224-244 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   49180   HIV-LS-353; SCIFINDER-HIV-7/17/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of novel phenyl branched apiosyl nucleosides</p>

    <p>          Kim Jin Woo and Hong Joon Hee</p>

    <p>          Archives of pharmacal research <b>2006</b>.  29(6): 464-468</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   49181   HIV-LS-353; WOS-HIV-7/16/2006</p>

    <p class="memofmt1-2">          Regioselective synthesis of the novel N-4-substituted pyrazolo[4,5-e][1,2,4]thiadiazines as potent HIV-1NNRTIs</p>

    <p>          Liu, XY, Chen, NG, Yan, RZ, Xu, WF, Molina, MT, and Vega, S</p>

    <p>          HETEROCYCLES <b>2006</b>.  68(6): 1225-+, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238668100014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238668100014</a> </p><br />

    <p>16.   49182   HIV-LS-353; WOS-HIV-7/16/2006</p>

    <p class="memofmt1-2">          GS-9137 - Anti-HIV agent HIV integrase inhibitor</p>

    <p>          Sorbera, LA and Serradell, N</p>

    <p>          DRUGS OF THE FUTURE <b>2006</b>.  31(4): 310-313, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238516600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238516600003</a> </p><br />

    <p>17.   49183   HIV-LS-353; WOS-HIV-7/23/2006</p>

    <p class="memofmt1-2">          An antifungal protein from the pea Pisum sativum var. arvense Poir</p>

    <p>          Wang, HX and Ng, TB</p>

    <p>          PEPTIDES <b>2006</b>.  27(7): 1732-1737, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238780300019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238780300019</a> </p><br />

    <p>18.   49184   HIV-LS-353; WOS-HIV-7/23/2006</p>

    <p class="memofmt1-2">          In vitro anti-HIV activity of a Chinese fungus extract</p>

    <p>          Xiang, ZC, Jiang, Y, and Guo, SX</p>

    <p>          BIOMEDICAL AND ENVIRONMENTAL SCIENCES <b>2006</b>.  19(3): 169-172, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238694500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238694500002</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
