

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-03-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="v/hxLxNr2zZ4oKFq3Ef0bd8llSsmUHgbJ94R6v7kcu+Zj7iHUW4GaBIiRANO91ijtFC4k1HDTQs6f4ZuwupZpzTZ1cOxq7GrwwuPIGfD05n1gpFEJ+AJc1WEMLM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8C8F026F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 1 - March 14, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23298160">A Mutant Tat Protein Provides Strong Protection from HIV-1 Infection in Human CD4+ T Cells.</a> Apolloni, A., M.H. Lin, H. Sivakumaran, D. Li, M.H. Kershaw, and D. Harrich. Human Gene Therapy, 2013. <b>[Epub ahead of print]</b>. PMID[23298160].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23152485">In Vitro Effects of the CCR5 Inhibitor Maraviroc on Human T Cell Function.</a> Arberas, H., A.C. Guardo, M.E. Bargallo, M.J. Maleno, M. Calvo, J.L. Blanco, F. Garcia, J.M. Gatell, and M. Plana. The Journal of Antimicrobial Chemotherapy, 2013. 68(3): p. 577-586. PMID[23152485].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23459357">Solid-state Tautomeric Structure and Invariom Refinement of a Novel and Potent HIV Integrase Inhibitor.</a> Bacsa, J., M. Okello, P. Singh, and V. Nair. Acta Crystallographica. Section C, Crystal Structure Communications, 2013. 69(Pt 3): p. 285-288. PMID[23459357].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23464580">Inhibition of HIV-1 Replication by HTLV-1/2 Tax Proteins in Vitro.</a> Beilke, M., C.S. Barrios, L. Castillo, C.Z. Giam, and L. Wu. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print]</b>. PMID[23464580].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23449229">HIV-2 Susceptibility to Entry Inhibitors.</a> Borrego, P. and N. Taveira. AIDS Reviews, 2013. 15(1): p. 49-61. PMID[23449229].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23233535">Short-peptide Fusion Inhibitors with High Potency against Wild-type and Enfuvirtide-resistant HIV-1.</a> Chong, H., X. Yao, Z. Qiu, J. Sun, M. Zhang, S. Waltersperger, M. Wang, S.L. Liu, S. Cui, and Y. He. FASEB Journal: Official Publication of the Federation of American Societies for Experimental Biology, 2013. 27(3): p. 1203-1213. PMID[23233535].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23470147">A Model of the Peptide Triazole Entry Inhibitor Binding to HIV-1 gp120 and Mechanism of Bridging Sheet Disruption.</a> Emileh, A., F. Tuzer, H.J. Yeh, M. Umashankara, D.R. Moreira, J.M. Lalonde, C.A. Bewley, C.F. Abrams, and I.M. Chaiken. Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23470147].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23274668">Identification of Novel Inhibitors of Human Immunodeficiency Virus Type 1 Replication by in Silico Screening Targeting Cyclin T1/Tat Interaction.</a> Hamasaki, T., M. Okamoto, and M. Baba. Antimicrobial Agents and Chemotherapy, 2013. 57(3): p. 1323-1331. PMID[23274668].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23178400">HIV-1 and HSV-1 Virus Activities of Some New Polycyclic Nucleoside Pyrene Candidates.</a> Khalifa, N.M., M.A. Al-Omar, G. Amr Ael, and M.E. Haiba. International Journal of Biological Macromolecules, 2013. 54: p. 51-56. PMID[23178400].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23375089">9-[2-(R)-(Phosphonomethoxy)propyl]-2,6-diaminopurine (R)-PMPDAP and Its Prodrugs: Optimized Preparation, Including Identification of By-products Formed, and Antiviral Evaluation in Vitro.</a> Krecmerova, M., P. Jansa, M. Dracinsky, P. Sazelova, V. Kasicka, J. Neyts, J. Auwerx, E. Kiss, N. Goris, G. Stepan, and Z. Janeba. Bioorganic &amp; Medicinal Chemistry, 2013. 21(5): p. 1199-1208. PMID[23375089].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23221626">3-Hydroxyphthalic anhydride-Modified Human Serum Albumin as a Microbicide Candidate Inhibits HIV Infection by Blocking Viral Entry.</a> Li, L., J. Qiu, L. Lu, S. An, P. Qiao, S. Jiang, and S. Liu. The Journal of Antimicrobial Chemotherapy, 2013. 68(3): p. 573-576. PMID[23221626].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20570527">Synthesis and anti-HIV Activity of 2-Naphthyl Substituted DAPY Analogues as Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Liang, Y.H., Q.Q. He, Z.S. Zeng, Z.Q. Liu, X.Q. Feng, F.E. Chen, J. Balzarini, C. Pannecouque, and E.D. Clercq. Bioorganic &amp; Medicinal Chemistry, 2010. 18(13): p. 4601-4605. PMID[20570527].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17964796">Synthesis and anti-HIV Evaluation of Novel 1,3-Disubstituted thieno[3,2-c][1,2,6]thiadiazin-4(3H)-one 2,2-dioxides(TTDDs).</a> Lin, Y., X. Liu, R. Yan, J. Li, C. Pannecouque, M. Witvrouw, and E. De Clercq. Bioorganic &amp; Medicinal Chemistry, 2008. 16(1): p. 157-163. PMID[17964796].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23308369">The Potent Human Immunodeficiency Virus Type 1 (HIV-1) Entry Inhibitor HR212 Blocks Formation of the Envelope Glycoprotein gp41 Six-helix Bundle.</a> Ouyang, W., T. An, D. Guo, S. Wu, and P. Tien. AIDS Research and Human Retroviruses, 2013. 29(3): p. 613-620. PMID[23308369].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23375792">Synthesis and Biological Evaluation of Phosphonate Analogues of Nevirapine.</a> Parrish, J., L. Tong, M. Wang, X. Chen, E.B. Lansdon, C. Cannizzaro, X. Zheng, M.C. Desai, and L. Xu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1493-1497. PMID[23375792].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23380374">Bifunctional Inhibition of HIV-1 Reverse Transcriptase: A First Step in Designing a Bifunctional Triphosphate.</a> Piao, D., A. Basavapathruni, P. Iyidogan, G. Dai, W. Hinz, A.S. Ray, E. Murakami, J.Y. Feng, F. You, G.E. Dutschman, D.J. Austin, K.A. Parker, and K.S. Anderson. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1511-1518. PMID[23380374].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23422490">Biological Interaction between Sasa senanensis Rehder Leaf Extract and Toothpaste Ingredients.</a> Sakagami, H., S. Amano, T. Yasui, K. Satoh, S. Shioda, T. Kanamoto, S. Terakubo, H. Nakashima, K. Watanabe, T. Sugiura, M. Kitajima, H. Oizumi, and T. Oizumi. In Vivo, 2013. 27(2): p. 275-284. PMID[23422490].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23445471">Fragment-based Discovery of 8-Hydroxyquinoline Inhibitors of the HIV-1 Integrase-LEDGF/p75 Interaction.</a> Serrao, E., B. Debnath, H. Otake, Y. Kuang, F. Christ, Z. Debyser, and N. Neamati. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23445471].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23398474">Phenotypic Susceptibility to Antiretrovirals among Clades C, F, and B/F Recombinant Antiretroviral-naive HIV Type 1 Strains.</a> Sucupira, M.C., P. Munerato, J. Silveira, A.F. Santos, L.M. Janini, M.A. Soares, and R.S. Diaz. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print]</b>. PMID[23398474].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23458727">Design, Synthesis, and Biological Evaluation of Highly Potent Small Molecule-peptide Conjugates as New HIV-1 Fusion Inhibitors.</a> Wang, C., W. Shi, L. Cai, L. Lu, Q. Wang, T. Zhang, J. Li, Z. Zhang, K. Wang, L. Xu, X. Jiang, S. Jiang, and K. Liu. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23458727].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23460261">Hydrogen-bond-directed Enantioselective Decarboxylative Mannich Reaction of beta-Ketoacids with Ketimines: Application to the Synthesis of anti-HIV Drug DPC 083.</a> Yuan, H.N., S. Wang, J. Nie, W. Meng, Q. Yao, and J.A. Ma. Angewandte Chemie, 2013. <b>[Epub ahead of print]</b>. PMID[23460261].
    <br />
    <b>[PubMed]</b>. HIV_0301-031413.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314718900002">N-terminal Slit2 Inhibits HIV-1 Replication by Regulating the Actin Cytoskeleton.</a> Anand, A.R., H.L. Zhao, T. Nagaraja, L.A. Robinson, and R.K. Ganju. Retrovirology, 2013. 10. ISI[000314718900002].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314283500041">Tautomeric Origin of Dual Effects of N1-Nicotinoyl-3-(4 &#39;-hydroxy-3&#39;-methyl phenyl)-5- (sub)phenyl -2-pyrazolines on Bacterial and Viral Strains: POM Analyses as New Efficient Bioinformatics&#39; Platform to Predict and Optimize Bioactivity of Drugs.</a> Ben Hadda, T., M.A. Ali, V. Masand, S. Gharby, T. Fergoug, and I. Warad. Medicinal Chemistry Research, 2013. 22(3): p. 1438-1449. ISI[000314283500041].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314283500043">Computational POM and 3D-QSAR Evaluation of Experimental in Vitro HIV-1-integrase Inhibition of Amide-containing Diketoacids.</a> Ben Hadda, T., J. Fathi, I. Chafchaouni, V. Masand, Z. Charrouf, Z.H. Chohan, R. Jawarkar, T. Fergoug, and I. Warad. Medicinal Chemistry Research, 2013. 22(3): p. 1456-1464. ISI[000314283500043].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314607100016">BET Bromodomain-targeting Compounds Reactivate HIV from Latency via a Tat-independent Mechanism.</a> Boehm, D., V. Calvanese, R.D. Dar, S.F. Xing, S. Schroeder, L. Martins, K. Aull, P.C. Li, V. Planelles, J.E. Bradner, M.M. Zhou, R.F. Siliciano, L. Weinberger, E. Verdin, and M. Ott. Cell Cycle, 2013. 12(3): p. 452-462. ISI[000314607100016].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314625400038">Optimization of Benzyloxazoles as Non-nucleoside Inhibitors of HIV-1 Reverse Transcriptase to Enhance Y181C Potency.</a> Bollini, M., R. Gallardo-Macias, K.A. Spasov, J. Tirado-Rives, K.S. Anderson, and W.L. Jorgensen. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(4): p. 1110-1113. ISI[000314625400038].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314666700010">Analysis by Substituted Cysteine Scanning Mutagenesis of the Fourth Transmembrane Domain of the CXCR4 Receptor in Its Inactive and Active State.</a> Boulais, P.E., E. Escher, and R. Leduc. Biochemical Pharmacology, 2013. 85(4): p. 541-550. ISI[000314666700010].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314371800055">Heterocycles from Morita-Baylis-Hillman Adducts: Synthesis of 5-Oxopyrazolidines, Arylidene-5-oxopyrazolidines, and Oxo-2,5-dihydro-pyrazols.</a> Correia, J.T.M., M.T. Rodrigues, H. Santos, C.F. Tormena, and F. Coelho. Tetrahedron, 2013. 69(2): p. 826-832. ISI[000314371800055].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314312600001">Identification of a 3-Aminoimidazo [1,2-a]pyridine Inhibitor of HIV-1 Reverse Transcriptase.</a> Elleder, D., T.J. Baiga, R.L. Russell, J.A. Naughton, S.H. Hughes, J.P. Noel, and J.A.T. Young. Virology Journal, 2012. 9. ISI[000314312600001].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314436500011">Effector Kinase Coupling Enables High-throughput Screens for Direct HIV-1 Nef Antagonists with Antiretroviral Activity.</a> Emert-Sedlak, L.A., P. Narute, S.T. Shu, J.A. Poe, H.B. Shi, N. Yanamala, J.J. Alvarado, J.S. Lazo, J.I. Yeh, P.A. Johnston, and T.E. Smithgall. Chemistry &amp; Biology, 2013. 20(1): p. 82-91. ISI[000314436500011].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314699200026">Synthesis of the Reverse Transcriptase Inhibitor L-737,126 and the CB2 Cannabinoid Receptor Agonist Pravadoline by a Copper-catalyzed Intramolecular Cross-coupling.</a> Gao, D.T. and T.G. Back. Synlett, 2013(3): p. 389-393. ISI[000314699200026].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314283500033">Docking and CoMFA Study on Novel Human CCR5 Receptor Antagonists.</a> Ghasemi, J.B. and M. Nouri. Medicinal Chemistry Research, 2013. 22(3): p. 1356-1364. ISI[000314283500033].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314283500018">Computational Evaluation and Experimental in Vitro Antibacterial, Antifungal and Antiviral Activity of bis-Schiff Bases of Isatin and Its Derivatives.</a> Jarrahpour, A., J. Sheikh, I. El Mounsi, H. Juneja, and T. Ben Hadda. Medicinal Chemistry Research, 2013. 22(3): p. 1203-1211. ISI[000314283500018].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314049602031">HIV Triggers Interleukin 21-Mediated Induction of Granzyme B-secreting B Cells with Regulatory and Antiviral Potential.</a> Kaltenmeier, C., A. Gawanbacht, S. Lindner, T. Beyer, G. Haerter, B. Gruener, P. Kern, F. Kirchhoff, H. Schrezenmeier, and B. Jahrsdorfer. Blood, 2012. 120(21). ISI[000314049602031].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314632500019">Microwave-assisted Click Chemistry for Nucleoside Functionalization: Useful Derivatives for Analytical and Biological Applications.</a> Krim, J., M. Taourirte, C. Grunewald, I. Krstic, and J.W. Engels. Synthesis-Stuttgart, 2013. 45(3): p. 396-405. ISI[000314632500019].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314743000015">The First Stereoselective Total Synthesis of Nicotlactone A.</a> Krishna, P.R., S. Prabhakar, and C. Sravanthi. Tetrahedron Letters, 2013. 54(7): p. 669-671. ISI[000314743000015].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314049602021">Novel 2-Phenyl-1-pyridin-2yl-ethanone (PpY) Based Iron Chelators Increase Expression of IkB-alpha and Heme Oxygenase-1 and Inhibit HIV-1.</a> Kumari, N., M. Xu, D. Kovlaskyy, S. Dhawan, and S. Nekhai. Blood, 2012. 120(21). ISI[000314049602021].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314779700001">Effect of Early Anti-retroviral Therapy on the Pathogenic Changes in Mucosal Tissues of SIV Infected Rhesus Macaques.</a> Malzahn, J., C.L. Shen, L. Caruso, P. Ghosh, S.R. Sankapal, S. Barratt-Boyes, P. Gupta, and Y. Chen. Virology Journal, 2012. 9. ISI[000314779700001].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314388300031">A Highly Facile Approach to the Synthesis of Novel 2-(3-Benzyl-2,4-dioxo-1,2,3,4-tetrahydropyrimidin-1-yl)-N-phenylacetamides.</a> Novikov, M.S., D.A. Babkov, M.P. Paramonova, A.O. Chizhov, A.L. Khandazhinskaya, and K.L. Seley-Radtke. Tetrahedron Letters, 2013. 54(6): p. 576-578. ISI[000314388300031].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314283500030">Anti-HIV, Antimycobacterial and Antimicrobial Studies of Newly Synthesized 1,2,4-Triazole Clubbed Benzothiazoles.</a> Patel, N.B., I.H. Khan, C. Pannecouque, and E. De Clercq. Medicinal Chemistry Research, 2013. 22(3): p. 1320-1329. ISI[000314283500030].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313898900016">Divide-and-conquer-based Quantum Chemical Study for Interaction between HIV-1 Reverse Transcriptase and MK-4965 Inhibitor.</a> Saparpakorn, P., M. Kobayashi, S. Hannongbua, and H. Nakai. International Journal of Quantum Chemistry, 2013. 113(4): p. 510-517. ISI[000313898900016].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314831400002">M48U1 CD4 Mimetic Has a Sustained Inhibitory Effect on Cell-associated HIV-1 by Attenuating Virion Infectivity through gp120 Shedding.</a> Selhorst, P., K. Grupping, T. Tong, E.T. Crooks, L. Martin, G. Vanham, J.M. Binley, and K.K. Arien. Retrovirology, 2013. 10. ISI[000314831400002].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313902600005">Synthesis and anti-HIV Activity of Triazolo-fused 2 &#39;,3 &#39;-cyclic Nucleoside Analogs Prepared by an Intramolecular Huisgen 1,3-Dipolar Cycloaddition.</a> Sun, J.B., R.H. Duan, H.M. Li, and J.C. Wu. Helvetica Chimica Acta, 2013. 96(1): p. 59-68. ISI[000313902600005].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314875500007">HIV Downregulates Interferon-stimulated Genes in Primary Macrophages.</a> Wie, S.H., P.Y. Du, T.Q. Luong, S.E. Rought, N. Beliakova-Bethell, J. Lozach, J. Corbeil, R.S. Kornbluth, D.D. Richman, and C.H. Woelk. Journal of Interferon and Cytokine Research, 2013. 33(2): p. 90-95. ISI[000314875500007].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>

    <br />

    <p class="plaintext">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314021500062">Inhibition of P-glycoprotein by HIV Protease Inhibitors Increases Intracellular Accumulation of Berberine in Murine and Human Macrophages.</a> Zha, W.B., G.J. Wang, W.R. Xu, X.Y. Liu, Y. Wang, B.S. Zha, J. Shi, Q.J. Zhao, P.M. Gerk, E. Studer, P.B. Hylemon, W.M. Pandak, and H.P. Zhou. Plos One, 2013. 8(1). ISI[000314021500062].
    <br />
    <b>[WOS]</b>. HIV_0301-031413.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
