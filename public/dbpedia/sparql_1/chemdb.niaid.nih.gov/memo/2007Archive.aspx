

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="2007Archive.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MsCIrJruoQdarovPCI10JfJVQV/sniLExDxGD3+2mIelsLqchCEf34YaR4XwJz5Iz77SKB3v6QwAYcB+rw3p4p6rgIS6fUHLqx9dSw3B1lP66vCdSCN+05bdgCc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EC520519" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="description">
		<h1>Literature Surveillance Memos</h1>

        <p>This page contains links to bi-weekly literature surveillance memos that identify relevant published research therapies on pre-clinical experimental therapies for HIV, the opportunistic infections (OIs) associated with AIDS and other viral pathogens.</p>
        <p>Note: Where available, URL links to article abstracts have been included for the convenience of the user. Abstract links to PubMed (ncbi.nlm.nih.gov) are available to all users.  Abstract links to Web of Science (publishorperish.nih.gov) or Science Direct (sciencedirect.com) are only available to NIH staff inside the NIH firewall or to other registered users of these Websites.</p>
        
        <div id="memo">
            <table id="memotable">
                <tr><th class="MemoColHIVArchive">HIV</th><th class="MemoColOIArchive">Opportunistic Infections</th><th class="MemoColOVArchive">Other Viral Pathogens</th></tr>

			<tr>
			<td><a href="HIV-LS-386.aspx" target="_blank">HIV-LS-386</a>
              (11/21/2007)
			</td>
            <td><a href="OI-LS-386.aspx" target="_blank">OI-LS-386</a>
              (11/21/2007)
			</td>
			<td><a href="DMID-LS-153.aspx" target="_blank">DMID-LS-153</a> (09/13/2007)
			</td>
			</tr>

			<tr>
			<td><a href="HIV-LS-385.aspx" target="_blank">HIV-LS-385</a>
              (11/05/2007)
			</td>
            <td><a href="OI-LS-385.aspx" target="_blank">OI-LS-385</a>
              (11/05/2007)              
			</td>
			<td><a href="DMID-LS-152.aspx" target="_blank">DMID-LS-152</a> (08/30/2007)
			</td>			
			</tr>

			<tr>
			<td><a href="HIV-LS-384.aspx" target="_blank">HIV-LS-384</a>
              (10/10/2007)
			</td>
            <td><a href="OI-LS-384.aspx" target="_blank">OI-LS-384</a>
              (10/10/2007)
			</td>
			<td><a href="DMID-LS-151.aspx" target="_blank">DMID-LS-151</a> (08/16/2007)
			</td>			
			</tr>

			<tr>
			<td><a href="HIV-LS-383.aspx" target="_blank">HIV-LS-383</a>
              (09/20/2007)
			</td>
            <td><a href="OI-LS-383.aspx" target="_blank">OI-LS-383</a>
              (09/20/2007)
			</td>
			<td><a href="DMID-LS-150.aspx" target="_blank">DMID-LS-150</a> (08/02/2007)
			</td>			
			</tr>

			<tr>
			<td><a href="HIV-LS-382.aspx" target="_blank">HIV-LS-382</a>
              (09/06/2007)
			</td>
            <td><a href="OI-LS-382.aspx" target="_blank">OI-LS-382</a>
              (09/06/2007)
			</td>
			<td><a href="DMID-LS-149.aspx" target="_blank">DMID-LS-149</a> (07/19/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-381.aspx" target="_blank">HIV-LS-381</a>
              (08/23/2007)
			</td>
            <td><a href="OI-LS-381.aspx" target="_blank">OI-LS-381</a>
              (08/23/2007)              
			</td>
			<td><a href="DMID-LS-148.aspx" target="_blank">DMID-LS-148</a> (07/05/2007)
			</td>					
			</tr>
			
			<tr>
			<td><a href="HIV-LS-380.aspx" target="_blank">HIV-LS-380</a>
              (08/09/2007)
			</td>
            <td><a href="OI-LS-380.aspx" target="_blank">OI-LS-380</a>
              (08/09/2007)
			</td>
			<td><a href="DMID-LS-147.aspx" target="_blank">DMID-LS-147</a> (06/21/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-379.aspx" target="_blank">HIV-LS-379</a>
              (07/26/2007)
			</td>
            <td><a href="OI-LS-379.aspx" target="_blank">OI-LS-379</a>
              (07/26/2007)
			</td>
			<td><a href="DMID-LS-146.aspx" target="_blank">DMID-LS-146</a> (06/07/2007)
			</td>						
			</tr>


			<tr>
			<td><a href="HIV-LS-378.aspx" target="_blank">HIV-LS-378</a>
              (07/12/2007)
			</td>
            <td><a href="OI-LS-378.aspx" target="_blank">OI-LS-378</a>
              (07/12/2007)
			</td>
			<td><a href="DMID-LS-145.aspx" target="_blank">DMID-LS-145</a> (05/24/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-377.aspx" target="_blank">HIV-LS-377</a>
              (06/28/2007)
			</td>
            <td><a href="OI-LS-377.aspx" target="_blank">OI-LS-377</a>
              (06/28/2007)
			</td>
			<td><a href="DMID-LS-144.aspx" target="_blank">DMID-LS-144</a> (05/10/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-376.aspx" target="_blank">HIV-LS-376</a>
              (06/14/2007)
			</td>
            <td><a href="OI-LS-376.aspx" target="_blank">OI-LS-376</a>
              (06/14/2007)
			</td>
			<td><a href="DMID-LS-143.aspx" target="_blank">DMID-LS-143</a> (04/26/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-375.aspx" target="_blank">HIV-LS-375</a>
              (05/31/2007)
			</td>
            <td><a href="OI-LS-375.aspx" target="_blank">OI-LS-375</a>
              (05/31/2007)
			</td>
			<td><a href="DMID-LS-142.aspx" target="_blank">DMID-LS-142</a> (04/12/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-374.aspx" target="_blank">HIV-LS-374</a>
              (05/17/2007)
			</td>
            <td><a href="OI-LS-374.aspx" target="_blank">OI-LS-374</a>
              (05/17/2007)
			</td>
			<td><a href="DMID-LS-141.aspx" target="_blank">DMID-LS-141</a> (03/29/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-373.aspx" target="_blank">HIV-LS-373</a>
              (05/03/2007)
			</td>
            <td><a href="OI-LS-373.aspx" target="_blank">OI-LS-373</a>
              (05/03/2007)
			</td>
			<td><a href="DMID-LS-140.aspx" target="_blank">DMID-LS-140</a> (03/14/2007)
			</td>						
			</tr>


			<tr>
			<td><a href="HIV-LS-372.aspx" target="_blank">HIV-LS-372</a>
              (04/19/2007)
			</td>
            <td><a href="OI-LS-372.aspx" target="_blank">OI-LS-372</a>
              (04/19/2007)
			</td>
			<td><a href="DMID-LS-139.aspx" target="_blank">DMID-LS-139</a> (03/01/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-371.aspx" target="_blank">HIV-LS-371</a>
              (04/05/2007)
			</td>
            <td><a href="OI-LS-371.aspx" target="_blank">OI-LS-371</a>
              (04/05/2007)
			</td>
			<td><a href="DMID-LS-138.aspx" target="_blank">DMID-LS-138</a>
              (02/15/2007)
			</td>						
			</tr>


			<tr>
			<td><a href="HIV-LS-370.aspx" target="_blank">HIV-LS-370</a>
              (03/22/2007)
			</td>
            <td><a href="OI-LS-370.aspx" target="_blank">OI-LS-370</a>
              (03/22/2007)
			</td>
			<td><a href="DMID-LS-137.aspx" target="_blank">DMID-LS-137</a>
              (02/01/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-369.aspx" target="_blank">HIV-LS-369</a>
              (03/08/2007)
			</td>
            <td><a href="OI-LS-369.aspx" target="_blank">OI-LS-369</a>
              (03/08/2007)
			</td>
			<td><a href="dmid-ls-136.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-136.aspx" target="_blank">136</a>
              (01/18/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-368.aspx" target="_blank">HIV-LS-368</a>
              (02/22/2007)
			</td>
            <td><a href="OI-LS-368.aspx" target="_blank">OI-LS-368</a>
              (02/22/2007)
			</td>
			<td><a href="dmid-ls-135.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-135.aspx" target="_blank">135</a>
              (01/04/2007)
			</td>						
			</tr>

			<tr>
			<td><a href="HIV-LS-367.aspx" target="_blank">HIV-LS-367</a>
              (02/08/2007)
			</td>
            <td><a href="OI-LS-367.aspx" target="_blank">OI-LS-367</a>
              (02/08/2007)
			</td>
			</tr>
			
			<tr>
			<td><a href="HIV-LS-366.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-366.aspx" target="_blank">66</a>
              (01/25/2007)
			</td>
            <td><a href="OI-LS-366.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-366.aspx" target="_blank">66
              </a>(01/25/2007)
			</td>
			</tr>

			<tr>
			<td><a href="HIV-LS-365.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-365.aspx" target="_blank">65</a>
              (01/11/2007)
			</td>
            <td><a href="OI-LS-365.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-365.aspx" target="_blank">65</a>
              (01/11/2007)
			</td>
			</tr>
                
            </table>
			<fieldset id="memobottom">
			    <br /><br /><br />
			    <fieldset class="submitbuttons" id="memobottomrow">
                    <ul id="memobottomrowbar">
                        <li><a href="2004Archive.aspx">2004</a></li> 
	    		    	<li><a href="2005Archive.aspx">2005</a></li>
	    		    	<li><a href="2006Archive.aspx">2006</a></li>
	    			    <li><a href="2009Archive.aspx">2008-2009</a></li>
	    			    <li><a href="2010Archive.aspx">2010</a></li>
	    			    <li><a href="2011Archive.aspx">2011</a></li>
  				        <li><a href="2012Archive.aspx">2012</a></li>
   				        <li><a href="2013Archive.aspx">2013</a></li>
   				        <li><a href="2014Archive.aspx">2014</a></li>    
   				        <li><a href="2015Archive.aspx">2015</a></li>    
   				        <li><a href="2016Archive.aspx">2016</a></li>       				        
   				        <li><a href="memos.aspx">2017</a></li>    
                    </ul>                                      
    			    <br /><br />  				    			    
			    </fieldset>
            </fieldset>
            
        </div>

    </div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
