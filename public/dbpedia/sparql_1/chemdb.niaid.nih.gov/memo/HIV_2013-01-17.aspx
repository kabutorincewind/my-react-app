

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-01-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q+OLOtkF68031VEWtXzqZaWKqs5RDtSisYvJDQ45ABbmGEpEGD3CtsK/Zk1h9IcdVh1gk2RgfHcw3iaW548Uja/vnJpO10LC9ikXsFfayuteDqYU1xpozbwXaB0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2F5DFBBA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 4 - January 17, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22945918">In vitro and Structural Evaluation of PL-100 as a Potential Second-generation HIV-1 Protease Inhibitor.</a> Asahchop, E.L., M. Oliveira, P.K. Quashie, D. Moisi, J.L. Martinez-Cajas, B.G. Brenner, C.L. Tremblay, and M.A. Wainberg. Journal of Antimicrobial Chemotherapy, 2013. 68(1): p. 105-112. PMID[22945918].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23206859">Inhibitors of HIV-1 Attachment. Part 11: The Discovery and Structure-Activity Relationships Associated with 4,6-Diazaindole Cores.</a> Bender, J.A., Z. Yang, B. Eggers, Y.F. Gong, P.F. Lin, D.D. Parker, S. Rahematpura, M. Zheng, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 218-222. PMID[23206859].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23316755">Gardiquimod: A Toll Like Receptor-7 Agonist that Inhibits HIV-1 Infection of Human Macrophages and Activated T Cells.</a> Buitendijk, M., S.K. Eszterhas, and A. Howell. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print];</b> PMID[23316755].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23123342">Ruthenium arene Complexes as HIV-1 Integrase Strand Transfer Inhibitors.</a> Carcelli, M., A. Bacchi, P. Pelagatti, G. Rispoli, D. Rogolino, T.W. Sanchez, M. Sechi, and N. Neamati. Journal of Inorganic Biochemistry, 2013. 118: p. 74-82. PMID[23123342].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23159373">Synthesis of Kifunensine Thioanalogs and Their Inhibitory Activities against HIV-RT and &#945;-Mannosidase.</a> Chen, H., R. Li, Z. Liu, S. Wei, H. Zhang, and X. Li. Carbohydrate Research, 2013. 365: p. 1-8. PMID[23159373].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23298577">Synthesis, Characterization and Targeting Potential of Zidovudine Loaded Sialic acid Conjugated-mannosylated Poly(propyleneimine) Dendrimers.</a> Gajbhiye, V., N. Ganesh, J. Barve, and N.K. Jain. European Journal of Pharmaceutical Sciences, 2013. <b>[Epub ahead of print];</b> PMID[23298577].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23316884">Carbamoyl pyridone HIV-1 Integrase Inhibitors 2. Bi- and Tri-cyclic Derivatives Result in Superior Antiviral and Pharmacokinetic Profiles.</a> Kawasuji, T., B.A. Johns, H. Yoshida, J.G. Weatherhead, T. Akiyama, T. Taishi, Y. Taoda, M. Mikamiyama, H. Murai, R. Kiyama, M. Fuji, N. Tanimoto, T. Yoshinaga, T. Seki, M. Kobayashi, A. Sato, E.P. Garvey, and T. Fujiwara. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print];</b> PMID[23316884].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23177258">Design and Synthesis of HIV-1 Protease Inhibitors for a Long-acting Injectable Drug Application.</a> Kesteleyn, B., K. Amssoms, W. Schepens, G. Hache, W. Verschueren, W. Van De Vreken, K. Rombauts, G. Meurs, P. Sterkens, B. Stoops, L. Baert, N. Austin, J. Wegner, C. Masungi, I. Dierynck, S. Lundgren, D. Jonsson, K. Parkes, G. Kalayanov, H. Wallberg, A. Rosenquist, B. Samuelsson, K. Van Emelen, and J.W. Thuring. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 310-317. PMID[23177258]. <b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23308267">Design, Synthesis and Biological Evaluation of Novel Piperazine Derivatives as CCR5 Antagonists.</a> Liu, T., Z. Weng, X. Dong, L. Chen, L. Ma, S. Cen, N. Zhou, and Y. Hu. PLoS One, 2013. 8(1): p. e53636. PMID[23308267].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23122167">Schisarisanlactones A and B: A New Class of Nortriterpenoids from the Fruits of Schisandra arisanensis.</a> Lo, I.W., Y.C. Lin, T.C. Liao, S.Y. Chen, P.H. Lin, C.T. Chien, S.Y. Chang, and Y.C. Shen. Food Chemistry, 2013. 136(2): p. 1095-1099. PMID[23122167].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23297755">Isolation and anti-HIV-1 Activity of a New Sesquiterpene lactone from Calocephalus brownii F. Muell.</a> Mohammed, M.M., L.P. Christensen, and P.L. Colla. Natural Product Research, 2013.</p>

    <p class="plaintext"><b>[Epub ahead of print];</b> PMID[23297755].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23289671">Polymeric Nanoparticles Containing Combination Antiretroviral Drugs for HIV-1 Treatment.</a> Shibata, A., E. McMullen, A. Pham, M. Belshan, B. Sanford, Y. Zhou, M. Goede, A.A. Date, and C. Destache. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print];</b>  PMID[23289671].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22949064">Discovery of Novel Low-molecular-weight HIV-1 Inhibitors Interacting with Cyclophilin A Using In silico Screening and Biological Evaluations.</a> Tian, Y.S., C. Verathamjamras, N. Kawashita, K. Okamoto, T. Yasunaga, K. Ikuta, M. Kameoka, and T. Takagi. Journal of Molecular Modeling, 2013. 19(1): p. 465-475. PMID[22949064].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200254">Inhibitors of HIV-1 Attachment. Part 10. The Discovery and Structure-Activity Relationships of 4-Azaindole Cores.</a> Wang, T., Z. Yang, Z. Zhang, Y.F. Gong, K.A. Riccardi, P.F. Lin, D.D. Parker, S. Rahematpura, M. Mathew, M. Zheng, N.A. Meanwell, J.F. Kadow, and J.A. Bender. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 213-217. PMID[23200254].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200252">Inhibitors of HIV-1 Attachment. Part 7: Indole-7-carboxamides as Potent and Orally Bioavailable Antiviral Agents.</a> Yeung, K.S., Z. Qiu, Q. Xue, H. Fang, Z. Yang, L. Zadjura, C.J. D&#39;Arienzo, B.J. Eggers, K. Riccardi, P.Y. Shi, Y.F. Gong, M.R. Browning, Q. Gao, S. Hansel, K. Santone, P.F. Lin, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 198-202. PMID[23200252].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200244">Inhibitors of HIV-1 Attachment. Part 9: An Assessment of Oral Prodrug Approaches to Improve the Plasma Exposure of a Tetrazole-containing Derivative.</a> Yeung, K.S., Z. Qiu, Z. Yang, L. Zadjura, C.J. D&#39;Arienzo, M.R. Browning, S. Hansel, X.S. Huang, B.J. Eggers, K. Riccardi, P.F. Lin, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 209-212. PMID[23200244]. <b>[PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200249">Inhibitors of HIV-1 Attachment. Part 8: The Effect of C7-heteroaryl Substitution on the Potency, and In vitro and In vivo Profiles of Indole-based Inhibitors.</a> Yeung, K.S., Z. Qiu, Z. Yin, A. Trehan, H. Fang, B. Pearce, Z. Yang, L. Zadjura, C.J. D&#39;Arienzo, K. Riccardi, P.Y. Shi, T.P. Spicer, Y.F. Gong, M.R. Browning, S. Hansel, K. Santone, J. Barker, T. Coulter, P.F. Lin, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters 2013. 23(1): p. 203-208. PMID[23200249].</p>

    <p class="plaintext">[<b>PubMed].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23298236">Novel P2 tris-tetrahydrofuran Group in Antiviral Compound 1 (GRL-0519) Fills the S2 Binding Pocket of Selected Mutants of HIV-1 Protease.</a> Zhang, H., Y.F. Wang, C.H. Shen, J. Agniswamy, K.V. Rao, A.K. Ghosh, R.W. Harrison, and I.T. Weber. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print];</b> PMID[23298236].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0104-011713.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310583100006">POMA Analyses as New Efficient Bioinformatics&#39; Platform to Predict and Optimize Bioactivity of Synthesized 3a,4-Dihydro-3H-indeno[1,2-c]pyrazole-2-carboxamide/carbothioamide Analogues.</a> Ahsan, M.J., J. Govindasamy, H. Khalilullah, G. Mohan, J.P. Stables, C. Pannecouque, and E. De Clercq. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7029-7035. ISI[000310583100006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_120712-122012.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000312199200013">7,8-Secolignans from Schisandra neglecta and Their anti-HIV-1 Activities.</a> Gao, X.M., H.X. Mu, R.R. Wang, Q.F. Hu, L.M. Yang, Y.T. Zheng, H.D. Sun, and W.L. Xiao. Journal of the Brazilian Chemical Society, 2012. 23(10): p. 1853-1857. ISI[000312199200013].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000312267700025">Synthesis and Biological Evaluation of Triazolothienopyrimidine Derivatives as Novel HIV-1 Replication Inhibitors.</a> Kim, J., J. Kwon, D. Lee, S. Jo, D.S. Park, J. Choi, E. Park, J.Y. Hwang, Y. Ko, I. Choi, M.K. Ju, J. Ahn, S.J. Han, T.H. Kim, J. Cechetto, J. Nam, S. Ahn, P. Sommer, M. Liuzzi, Z. No, and J. Lee. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 153-157. ISI[000312267700025].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000312159900006">Synthesis and Evaluation of Novel 4-Substituted styryl quinazolines as Potential Antimicrobial Agents.</a> Modh, R.P., A.C. Patel, D.H. Mahajan, C. Pannecouque, E. De Clercq, and K.H. Chikhalia. Archiv der Pharmazie, 2012. 345(12): p. 964-972. ISI[000312159900006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000312325700001">Antibacterial, anti-HIV-1 Protease and Cytotoxic Activities of Aqueous Ethanolic Extracts from Combretum adenogonium Steud. ex A. Rich (Combretaceae).</a> Mushi, N.F., Z.H. Mbwambo, E. Innocent, and S. Tewtrakul. BMC Complementary and Alternative Medicine, 2012. 12. ISI[000312325700001].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000312265800004">Synthesis of Betulin Derivatives: N &#39;{N-[3-oxo-20(29)-lupen-28-oyl]-9-aminononanoyl}3-amino-3-phenylpropionic acid.</a> Sysolyatin, S.V., V.N. Surmachev, V.V. Malykhin, A.I. Kalashnikov, I.A. Surmacheva, E.G. Sonina, A.S. Dubkov, G.A. Tolstikov, E.E. Shul&#39;ts, N.F. Salakhutdinov, and U.M. Dzhemilev. Pharmaceutical Chemistry Journal, 2012. 46(8): p. 473-477. ISI[000312265800004].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000311873600037">Anti-HIV Activity of Semisynthetic Derivatives of Andrographolide and Computational Study of HIV-1 gp120 Protein Binding.</a> Uttekar, M.M., T. Das, R.S. Pawar, B. Bhandari, V. Menon, Nutan, S.K. Gupta, and S.V. Bhat. European Journal of Medicinal Chemistry, 2012. 56: p. 368-374. ISI[000311873600037].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0104-011713.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000312231000005">A Laccase with Inhibitory Activity against HIV-1 Reverse Transcriptase from the Mycorrhizal Fungus Lepiota ventriosospora.</a> Zhang, G.Q., Q.J. Chen, H.X. Wang, and T.B. Ng. Journal of Molecular Catalysis B-Enzymatic, 2013. 85-86: p. 31-36. ISI[000312231000005].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0104-011713.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121122&amp;CC=WO&amp;NR=2012156825A1&amp;KC=A1">Peptide-based HIV Pre-integration Complex Inhibitors.</a> Gros, E., M. Fourar, G. Divita, and P. Halfon. Patent. 2012. 2012-IB1194 2012156825: 50pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121212&amp;CC=CN&amp;NR=102816215A&amp;KC=A">Anti-HIV Small Polypeptides and Their Medical Application.</a> He, Y. and H. Chong. Patent. 2012. 2012-10280054 102816215: 31pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121107&amp;CC=CN&amp;NR=102766083A&amp;KC=A">Preparation of Indole Derivatives as HIV Reverse Transcriptase Inhibitors.</a> Jiang, B., C. Zhang, H. Jiang, and X. Cao. Patent. 2012. 2011-10112440 102766083: 18pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>

    <br />

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121115&amp;CC=WO&amp;NR=2012153768A1&amp;KC=A1">Preparation of Pyrmidobenzothiazin-6-imine Derivatives as Antiviral Agents.</a> Maeda, H., T. Kato, M. Matsuoka, K. Shimura, N. Fujii, H. Ohno, S. Oishi, and T. Mizuhara. Patent. 2012. 2012-JP61890 2012153768: 143pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>

    <br />

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121213&amp;CC=WO&amp;NR=2012168336A1&amp;KC=A1">Preparation of beta-Hairpin Peptidomimetics as CXCR4 Antagonists for Treatment of HIV Infection.</a>. Obrecht, D., F.O. Gombert, and J. Zimmermann. Patent. 2012. 2012-EP60763 2012168336: 57pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>

    <br />

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121227&amp;CC=WO&amp;NR=2012177660A2&amp;KC=A2">Leukotoxin E/D as a New Anti-inflammatory Agent and Microbicide for Treating HIV, Inflammation and Staphylococcus aureus Infection.</a> Torres, V.J., D. Unutmaz, and F. Alonzo. Patent. 2012. 2012-US43182 2012177660: 49pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>

    <br />

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121213&amp;CC=WO&amp;NR=2012170792A1&amp;KC=A1">Atazanavir Metabolite Derivatives as Therapeutic Agents.</a> Tung, R.D. Patent. 2012. 2012-US41511 2012170792: 47pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>

    <br />

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121206&amp;CC=US&amp;NR=2012309698A1&amp;KC=A1">Preparation of Pyrimidoazepines as HIV Integrase Inhibitors Useful in the Treatment of HIV Infection and AIDS.</a> Ueda, Y., T.P. Connolly, B.L. Johnson, C. Li, B.N. Naidu, M. Patel, K. Peese, M.E. Sorenson, M.A. Walker, M.S. Bowsher, and R. Li. Patent. 2012. 2011-312483 20120309698: 130pp ; Chemical Indexing Equivalent to 157:76828 (WO).</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0104-011713.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
