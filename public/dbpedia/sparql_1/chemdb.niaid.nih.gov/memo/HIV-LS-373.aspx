

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-373.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sHqwQxJ3Ib0qbnAyRSJfPYQLydzb3RbU2KB4uPabH8Z3MqYyMqlUC6C4lu1Bh7y5wOkOjUi5AALroEXay9a7nSW0evaXRexJR/vOoFDn9SyVzvACnoKs+aahvxM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CCA65C68" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-373-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60729   HIV-LS-373; SCIFINDER-HIV-4/23/2007</p>

    <p class="memofmt1-2">          Piperazine amidines as antiviral agents</p>

    <p>          Wang, Tao, Kadow, John F, Meanwell, Nicholas A, Zhang, Zhongxing, Yin, Zhiwei, James, Clint A, Ruediger, Edward H, and Pearce, Bradley C</p>

    <p>          PATENT:  US <b>2007078141</b>  ISSUE DATE:  20070405</p>

    <p>          APPLICATION: 2006-17538  PP: 67pp.</p>

    <p>          ASSIGNEE:  (Bristol-Myers Squibb Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60730   HIV-LS-373; SCIFINDER-HIV-4/23/2007</p>

    <p class="memofmt1-2">          Inhibitor of the folding of the HIV-1-protease as antiviral agent</p>

    <p>          Broglia, Ricardo, Tiana, Guido, and Provasi, Davide</p>

    <p>          PATENT:  US <b>2007072806</b>  ISSUE DATE:  20070329</p>

    <p>          APPLICATION: 2005-55852  PP: 17pp.</p>

    <p>          ASSIGNEE:  (Universita&#39; Degli Studi di Milano, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60731   HIV-LS-373; SCIFINDER-HIV-4/23/2007</p>

    <p class="memofmt1-2">          Synthetic polyvalent carbohydrates as components of HIV-1-blocking microbicides</p>

    <p>          Wang, Lai-Xi, Wang, Jingsong, and Fouts, Timothy</p>

    <p>          PATENT:  WO <b>2007033329</b>  ISSUE DATE:  20070322</p>

    <p>          APPLICATION: 2006  PP: 21pp.</p>

    <p>          ASSIGNEE:  (University of Maryland Biotechnology Institute Off. of Research Admin./ Tech.Dev., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     60732   HIV-LS-373; SCIFINDER-HIV-4/23/2007</p>

    <p class="memofmt1-2">          Imidazo[1,2-a]pyridine compounds as chemokine receptor ligands and their preparation, pharmaceutical compositions and use in the treatment of HIV infection</p>

    <p>          Gudmundsson, Kristjan and Boggs, Sharon Davis</p>

    <p>          PATENT:  WO <b>2007027999</b>  ISSUE DATE:  20070308</p>

    <p>          APPLICATION: 2006  PP: 91pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     60733   HIV-LS-373; WOS-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hiv-1 Activity of 1,3-Phenylene Bis-Uracil Analogues of Mkc-442</p>

    <p>          Aly, Y. <i>et al.</i></p>

    <p>          Journal of Heterocyclic Chemistry <b>2007</b>.  44(2): 381-387</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245494600016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245494600016</a> </p><br />

    <p>6.     60734   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Drug design: New inhibitors for HIV-1 protease based on Nelfinavir as lead</p>

    <p>          Perez, MA, Fernandes, PA, and Ramos, MJ</p>

    <p>          J Mol Graph Model <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17459746&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17459746&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     60735   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          De Novo Parallel Design, Synthesis and Evaluation of Inhibitors against the Reverse Transcriptase of Human Immunodeficiency Virus Type-1 and Drug-Resistant Variants</p>

    <p>          Herschhorn, A, Lerman, L, Weitman, M, Gleenberg, IO, Nudelman, A, and Hizi, A</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458947&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458947&amp;dopt=abstract</a> </p><br />

    <p>8.     60736   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          New AZT Analogues Having 5&#39;-Alkylsulfonyl Groups: Synthesis and Anti-HIV Activity</p>

    <p>          Al-Masoudi, NA, Al-Soud, YA, Ali, IA, Schuppler, T, Pannecouque, C, and De Clercq, E</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(3): 223-230</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454731&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454731&amp;dopt=abstract</a> </p><br />

    <p>9.     60737   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          HIV protease inhibitors modulate apoptosis signaling in vitro and in vivo</p>

    <p>          Vlahakis, SR, Bennett, SA, Whitehead, SN, and Badley, AD</p>

    <p>          Apoptosis <b>2007</b>.  12(5): 969-77</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17453162&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17453162&amp;dopt=abstract</a> </p><br />

    <p>10.   60738   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Heparin-mimicking sulfonic acid polymers as multitarget inhibitors of HIV-1 Tat and gp120 proteins</p>

    <p>          Bugatti, A, Urbinati, C, Ravelli, C, De Clercq, E, Liekens, S, and Rusnati, M</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17452490&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17452490&amp;dopt=abstract</a> </p><br />

    <p>11.   60739   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Structure-based identification of small molecule compounds targeting cell cyclophilin A with anti-HIV-1 activity</p>

    <p>          Chen, S, Zhao, X, Tan, J, Lu, H, Qi, Z, Huang, Q, Zeng, X, Zhang, M, Jiang, S, Jiang, H, and Yu, L</p>

    <p>          Eur J Pharmacol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17449029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17449029&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   60740   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Synthesis and evaluation of 2&#39;-substituted cyclobutyl nucleosides and nucleotides as potential anti-HIV agents</p>

    <p>          Li, Y, Mao, S, Hager, MW, Becnel, KD, Schinazi, RF, and Liotta, DC</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434736&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434736&amp;dopt=abstract</a> </p><br />

    <p>13.   60741   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          The Novel Fold of Scytovirin Reveals a New Twist For Antiviral Entry Inhibitors</p>

    <p>          McFeeters, RL, Xiong, C, O&#39;keefe, BR, Bokesch, HR, McMahon, JB, Ratner, DM, Castelli, R, Seeberger, PH, and Byrd, RA</p>

    <p>          J Mol Biol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434526&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434526&amp;dopt=abstract</a> </p><br />

    <p>14.   60742   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Tipranavir: a new protease inhibitor for the treatment of antiretroviral-experienced HIV-infected patients</p>

    <p>          de Mendoza, C, Morello, J, Garcia-Gasco, P, Rodriguez-Novoa, S, and Soriano, V</p>

    <p>          Expert Opin Pharmacother <b>2007</b>.  8(6): 839-850</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425479&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425479&amp;dopt=abstract</a> </p><br />

    <p>15.   60743   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Biochemical Studies on the Mechanism of HIV-1 Reverse Transcriptase Resistance to 1-({beta}-D-Dioxolane)thymine Triphosphate</p>

    <p>          Lennerstrand, J, Chu, CK, and Schinazi, RF</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17403997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17403997&amp;dopt=abstract</a> </p><br />

    <p>16.   60744   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Interaction between HIV-1 Rev and Integrase proteins: A basis for the development of anti-HIV peptides</p>

    <p>          Rosenbluh, J, Hayouka, Z, Loya, S, Levin, A, Armon-Omer, A, Britan, E, Hizi, A, Kotler, M, Friedler, A, and Loyter, A</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17403681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17403681&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   60745   HIV-LS-373; PUBMED-HIV-4/30/2007</p>

    <p class="memofmt1-2">          Progesterone Inhibits HIV-1 Replication in Human Trophoblast Cells through Inhibition of Autocrine Tumor Necrosis Factor Secretion</p>

    <p>          Diaz Munoz, L, Serramia, MJ, Fresno, M, and Munoz-Fernandez, MA</p>

    <p>          J Infect Dis <b>2007</b>.  195(9): 1294-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17396998&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17396998&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
