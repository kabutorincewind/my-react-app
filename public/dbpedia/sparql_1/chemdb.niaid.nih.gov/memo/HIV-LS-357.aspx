

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-357.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="02vQrTF32RUUjU4F+C65IEuaAH5dWvM+NXQvUvjVDxJbnoBlsohNzMlAN67f1pFfJcaJhPJ6xJG2v4m49SV7+mC0hng/V6zvCrLc+i/DwxBslQtDGizw8QFOIyk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7EE7C05C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-357-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58969   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Piperidinyl piperidine derivatives useful as inhibitors of chemokine receptors</p>

    <p>          Chan, Tze-Ming, Cox, Kathleen, Feng, Wenqing, Miller, Michael W, Weston, Daniel, and Mccombie, Stuart</p>

    <p>          PATENT:  WO <b>2006091534</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 92pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     58970   HIV-LS-357; PUBMED-HIV-9/18/2006</p>

    <p class="memofmt1-2">          Molecular docking studies on 4-thiazolidinones as HIV-1 RT inhibitors</p>

    <p>          Rawal, RK, Kumar, A, Siddiqi, MI, and Katti, SB</p>

    <p>          J Mol Model (Online) <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16969668&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16969668&amp;dopt=abstract</a> </p><br />

    <p>3.     58971   HIV-LS-357; PUBMED-HIV-9/18/2006</p>

    <p class="memofmt1-2">          Small molecules that bind the inner core of gp41 and inhibit HIV envelope-mediated fusion</p>

    <p>          Frey, G, Rits-Volloch, S, Zhang, XQ, Schooley, RT, Chen, B, and Harrison, SC</p>

    <p>          Proc Natl Acad Sci U S A <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16963566&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16963566&amp;dopt=abstract</a> </p><br />

    <p>4.     58972   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          QSAR studies of anti-HIV-1 Ritonavir analogs</p>

    <p>          Kasara, Raghava Chaitanya, Bhhatarai, Barun, and Garg, Rajni</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: COMP-277</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     58973   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Anti-human immunodeficiency virus type 1 (hiv-1) polypeptide c22, coding gene and use thereof</p>

    <p>          Wang, Shilong, Sun, Xiaoyu, Li, Min, and Zhang, Zhen</p>

    <p>          PATENT:  CN <b>1810830</b>  ISSUE DATE: 20060802</p>

    <p>          APPLICATION: 1002-3542  PP: 12pp.</p>

    <p>          ASSIGNEE:  (Tongji University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     58974   HIV-LS-357; PUBMED-HIV-9/18/2006</p>

    <p class="memofmt1-2">          Inhibition of Human Immunodeficiency Virus (HIV-1) Envelope Glycoprotein-Mediated Single Cell Lysis by Low-Molecular-Weight Antagonists of Viral Entry</p>

    <p>          Madani, N, Hubicki, AM, Perdigoto, AL, Springer, M, and Sodroski, J</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16943294&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16943294&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     58975   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Pharmaceutically active antiviral peptides, particularly anti-AIDS peptides active against drug-resistant HIV</p>

    <p>          Bacher, Gerald, Wiesmueller, Karl Heinrich, Schaefer, Birgit, and Hauber, Joachim</p>

    <p>          PATENT:  WO <b>2006072579</b>  ISSUE DATE:  20060713</p>

    <p>          APPLICATION: 2006  PP: 75 pp.</p>

    <p>          ASSIGNEE:  (Germany)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     58976   HIV-LS-357; PUBMED-HIV-9/18/2006</p>

    <p class="memofmt1-2">          Effect of the extent of thiolation and introduction of phosphorothioate internucleotide linkages on the anti-HIV activity of Suligovir [(s(4)dU)(35)]</p>

    <p>          Horvath, A, Beck, Z, Bardos, TJ, Dunn, JA, and Aradi, J</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.  16(20): 5321-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920358&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920358&amp;dopt=abstract</a> </p><br />

    <p>9.     58977   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Antiviral peptides capable of binding with viral surface glycoprotein gp41</p>

    <p>          Dai, Qiuyun, Cheng, Jianwei, He, Yuxian, and Dong, Mingxin</p>

    <p>          PATENT:  CN <b>1793170</b>  ISSUE DATE: 20060628</p>

    <p>          APPLICATION: 1012-6473  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (Institute of Bioengineering, Academy of Military Medical Sciences of Pla Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   58978   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Preparation of hydroxydihydropyridopyrazine-1,8-diones for inhibiting HIV integrase</p>

    <p>          Chan Chun Kong, Laval, Liu, Bingcan, Nguyen-Ba, Nghe, Cadilhac, Caroline, and Turcotte, Nathalie</p>

    <p>          PATENT:  WO <b>2006066414</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2005  PP: 186 pp.</p>

    <p>          ASSIGNEE:  (Virochem Pharma Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   58979   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Azadideoxyadenosine: Synthesis, enzymology, and anti-HIV activity</p>

    <p>          Nishonov, Abdumalik A, Ma, Xiaohui, and Nair, Vasu</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(15): 4099-4101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   58980   HIV-LS-357; WOS-HIV-9/10/2006</p>

    <p class="memofmt1-2">          Inhibitory activity on HIV-1 reverse transcriptase and integrase of a carmalol derivative from a brown alga, Ishige okamurae</p>

    <p>          Aln, MJ, Yoon, KD, Kim, CY, Kim, JH, Shin, CG, and Kim, J</p>

    <p>          PHYTOTHERAPY RESEARCH <b>2006</b>.  20(8): 711-713, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239857900018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239857900018</a> </p><br />
    <br clear="all">

    <p>13.   58981   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Dual inhibitors of hiv-1 gp-120 interactions</p>

    <p>          Hosahudya, Gopi and Chaiken, Irwin</p>

    <p>          PATENT:  US <b>2006135746</b>  ISSUE DATE:  20060622</p>

    <p>          APPLICATION: 2005-43257  PP: 8 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   58982   HIV-LS-357; WOS-HIV-9/17/2006</p>

    <p class="memofmt1-2">          The design and synthesis of 9-phenylcyclohepta[d]pyrimidine-2,4-dione derivatives as potent non-nucleoside inhibitors of HIV reverse transcriptase</p>

    <p>          Wang, XW, Lou, QH, Guo, Y, Xu, Y, Zhang, ZL, and Liu, JY</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2006</b>.  4(17): 3252-3258, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239915500014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239915500014</a> </p><br />

    <p>15.   58983   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Antiviral compositions containing stigmastane-3,5,6-triol and its derivatives</p>

    <p>          Sun, Yanrong, Zhang, Jiajie, Xu, Wei, and Wu, Shuguang</p>

    <p>          PATENT:  CN <b>1726922</b>  ISSUE DATE: 20060201</p>

    <p>          APPLICATION: 1007-4395  PP: 17pp.</p>

    <p>          ASSIGNEE:  (Southern Medical University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   58984   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Effect of C-H...S and C-H...Cl interactions on the conformational preference of inhibitors of TIBO family</p>

    <p>          Freitas, Renato F and Galembeck, Sergio E</p>

    <p>          Chemical Physics Letters <b>2006</b>.  423(1-3): 131-137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   58985   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Divergent synthesis and biological evaluation of carbocyclic a-, iso- and 3&#39;-epi-nucleosides and their lipophilic nucleotide prodrugs</p>

    <p>          Ludek, Olaf R, Kraemer, Tobias, Balzarini, Jan, and Meier, Chris</p>

    <p>          Synthesis <b>2006</b>.(8): 1313-1324</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   58986   HIV-LS-357; SCIFINDER-HIV-9/11/06</p>

    <p class="memofmt1-2">          Discovery of a Piperidine-4-carboxamide CCR5 Antagonist (TAK-220) with Highly Potent Anti-HIV-1 Activity</p>

    <p>          Imamura, Shinichi, Ichikawa, Takashi, Nishikawa, Youichi, Kanzaki, Naoyuki, Takashima, Katsunori, Niwa, Shinichi, Iizawa, Yuji, Baba, Masanori, and Sugihara, Yoshihiro</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(9): 2784-2793</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   58987   HIV-LS-357; WOS-HIV-9/10/2006</p>

    <p class="memofmt1-2">          Tipranavir: A protease inhibitor for HIV salvage therapy</p>

    <p>          Dong, BJ and Cocohoba, JM</p>

    <p>          ANNALS OF PHARMACOTHERAPY <b>2006</b>.  40(7-8): 1311-1321, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239716500011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239716500011</a> </p><br />
    <br clear="all">

    <p>20.   58988   HIV-LS-357; WOS-HIV-9/17/2006</p>

    <p class="memofmt1-2">          Synthesis of heterocyclic and non-heterocyclic entities as antibacterial and anti-HIV agents</p>

    <p>          Patel, RB and  Chikhalia, KH</p>

    <p>          INDIAN JOURNAL OF CHEMISTRY SECTION B-ORGANIC CHEMISTRY INCLUDING MEDICINAL CHEMISTRY <b>2006</b>.  45(8): 1871-1879, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239974400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239974400001</a> </p><br />

    <p>21.   58989   HIV-LS-357; WOS-HIV-9/17/2006</p>

    <p class="memofmt1-2">          Antihuman immunodeficiency virus type 1 (HIV-1) activity of rare earth metal complexes of 4-hydroxycoumarins in cell culture</p>

    <p>          Manolov, I, Raleva, S, Genova, P, Savov, A, Froloshka, L, Dundarova, D, and Argirova, R</p>

    <p>          BIOINORGANIC CHEMISTRY AND APPLICATIONS <b>2006</b>.: 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240136300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240136300001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
