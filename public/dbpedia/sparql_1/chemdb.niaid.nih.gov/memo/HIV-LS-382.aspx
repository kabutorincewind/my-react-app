

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-382.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="w3+y4fGNlUEPKteOhCJgabxnHQ1Ml2sULelGqR9fMJ4toiFNqvWDXmDyPzwro8ODtkKCaReSEvQMOtvojCHhPrZNd/3w4nfi+tNppZKSIighZAMpidvfDue6INo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A6F7700F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-382-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61551   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Allosteric effects of antagonists on signalling by the chemokine receptor CCR5</p>

    <p>          Haworth, Ben, Lin, Hong, Fidock, Mark, Dorr, Pat, and Strange, Philip G</p>

    <p>          Biochemical Pharmacology <b>2007</b>.  74(6): 891-897</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4P-4P29K7K-3/2/8b33ffb667a62ff8d2b669b225e9142b">http://www.sciencedirect.com/science/article/B6T4P-4P29K7K-3/2/8b33ffb667a62ff8d2b669b225e9142b</a> </p><br />

    <p>2.     61552   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          The Design and Synthesis of N-1-alkylated-5-aminoaryalkylsubstituted-6-methyluracils as potential non-nucleoside HIV-1 RT Inhibitors</p>

    <p>          Lu, Xiao, Chen, Yanli, Guo, Ying, Liu, Zhenming, Shi, Yawei, Xu, Yang, Wang, Xiaowei, Zhang, Zhili, and Liu, Junyi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript: 314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4PHSC4K-1/2/46a0484a54f92f01321e0b3a28c8773c">http://www.sciencedirect.com/science/article/B6TF8-4PHSC4K-1/2/46a0484a54f92f01321e0b3a28c8773c</a> </p><br />

    <p>3.     61553   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Discovery of Potent HIV-1 Protease Inhibitors Incorporating Sulfoximine Functionality</p>

    <p>          Lu, Ding and Vince, Robert</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 882</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4PGPVPY-8/2/10d8d7e9bf68e6b0f7a346fe46cfeee1">http://www.sciencedirect.com/science/article/B6TF9-4PGPVPY-8/2/10d8d7e9bf68e6b0f7a346fe46cfeee1</a> </p><br />

    <p>4.     61554   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Dihydroxypyridopyrazine-1,6-dione HIV-1 Integrase Inhibitors</p>

    <p>          Wai, John S, Kim, Bo-Young, Fisher, Thorsten E, Zhuang, Linghang, Embrey, Mark W, Williams, Peter D, Staas, Donnette D, Culberson, Chris, Lyle, Terry A, Vacca, Joseph P, Hazuda, Daria J, Felock, Peter J, Schleif, William A, Gabryelski, Lori J, Jin, Lixia, Chen, I-Wu, Ellis, Joan D, Mallai, Rama, and Young, Steven D</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 882</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4PGGP17-D/2/77920ed2629e22fb94733d83deb3d02c">http://www.sciencedirect.com/science/article/B6TF9-4PGGP17-D/2/77920ed2629e22fb94733d83deb3d02c</a> </p><br />

    <p>5.     61555   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Non-nucleoside HIV-1 reverse transcriptase inhibitors, Part 11.1) structural modulations of diaryltriazines with potent anti-HIV activity</p>

    <p>          Xiong, Yuan-Zhen, Chen, Fen-Er, Balzarini, Jan, De Clercq, Erik, and Pannecouque, Christophe</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript:  237</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4PD4X7V-1/2/54c87735307ba37f67f8c38b8733ffbd">http://www.sciencedirect.com/science/article/B6VKY-4PD4X7V-1/2/54c87735307ba37f67f8c38b8733ffbd</a> </p><br />
    <br clear="all">

    <p>6.     61556   HIV-LS-382; WOS-HIV-8/24/2007</p>

    <p class="memofmt1-2">          Triterpenoids From Schisandra Rubriflora</p>

    <p>          Xiao, W. <i>et al.</i></p>

    <p>          Journal of Natural Products <b>2007</b>.  70(6): 1056-1059</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247436000031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247436000031</a> </p><br />

    <p>7.     61557   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Analysis of the neutralization breadth of the anti-V3 antibody F425-B4e8 and re-assessment of its epitope fine specificity by scanning mutagenesis</p>

    <p>          Pantophlet, Ralph, Aguilar-Sino, Rowena O, Wrin, Terri, Cavacini, Lisa A, and Burton, Dennis R </p>

    <p>          Virology <b>2007</b>.  364(2): 441-453</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NF2NCP-1/2/0fb390690cd00e3afbc2e35611ecbdf5">http://www.sciencedirect.com/science/article/B6WXR-4NF2NCP-1/2/0fb390690cd00e3afbc2e35611ecbdf5</a> </p><br />

    <p>8.     61558   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Antiviral activity of a Rac GEF inhibitor characterized with a sensitive HIV/SIV fusion assay</p>

    <p>          Pontow, Suzanne, Harmon, Brooke, Campbell, Nancy, and Ratner, Lee</p>

    <p>          Virology <b>2007</b>.  In Press, Corrected Proof: 453</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4P77G1H-1/2/4c0cdc29a49d58ec536eb179e2f3b245">http://www.sciencedirect.com/science/article/B6WXR-4P77G1H-1/2/4c0cdc29a49d58ec536eb179e2f3b245</a> </p><br />

    <p>9.     61559   HIV-LS-382; EMBASE-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Determination of the HIV integrase inhibitor, MK-0518 (raltegravir), in human plasma using 96-well liquid-liquid extraction and HPLC-MS/MS</p>

    <p>          Merschman, SA, Vallano, PT, Wenning, LA, Matuszewski, BK, and Woolf, EJ</p>

    <p>          Journal of Chromatography B <b>2007</b>.  In Press, Corrected Proof: 88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6X0P-4P47GM7-2/2/6ba3c7a86139c862291dd71a60ee9db5">http://www.sciencedirect.com/science/article/B6X0P-4P47GM7-2/2/6ba3c7a86139c862291dd71a60ee9db5</a> </p><br />

    <p>10.   61560   HIV-LS-382; WOS-HIV-8/31/2007</p>

    <p class="memofmt1-2">          Inhibition of Initiation of Reverse Transcription in Hiv-1 by Human Apobec3f</p>

    <p>          Yang, Y. <i>et al.</i></p>

    <p>          Virology <b>2007</b>.  365(1): 92-100</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247584900009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247584900009</a> </p><br />

    <p>11.   61561   HIV-LS-382; WOS-HIV-8/31/2007</p>

    <p class="memofmt1-2">          Inhibition of Human Immunodeficiency Virus Type 1 Transcription by N-Aminoimidazole Derivatives</p>

    <p>          Stevens, M. <i>et al.</i></p>

    <p>          Virology <b>2007</b>.  365(1): 220-237</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247584900022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247584900022</a> </p><br />
    <br clear="all">

    <p>12.   61562   HIV-LS-382; WOS-HIV-8/31/2007</p>

    <p class="memofmt1-2">          In Vitro Selection and Characterization of Human Immunodeficiency Virus Type 1 Resistant to Zidovudine and Tenofovir</p>

    <p>          Park, S. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2007</b>.  26(5): 453-457</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247728400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247728400004</a> </p><br />

    <p>13.   61563   HIV-LS-382; WOS-HIV-8/31/2007</p>

    <p class="memofmt1-2">          Synthesis of Some Novel Heterocyclic Compounds Derived From Diflunisal Hydrazide as Potential Anti-Infective and Anti-Inflammatory Agents</p>

    <p>          Kucukguzel, S. <i>et al.</i></p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(7): 893-901</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247992100001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247992100001</a> </p><br />

    <p>14.   61564   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          V3 Loop Truncations in HIV-1 Envelope Impart Resistance to Coreceptor Inhibitors  and Enhanced Sensitivity to Neutralizing Antibodies</p>

    <p>          Laakso, MM, Lee, FH, Haggarty, B, Agrawal, C, Nolan, KM, Biscone, M, Romano, J, Jordan, AP, Leslie, GJ, Meissner, EG, Su, L, Hoxie, JA, and Doms, RW</p>

    <p>          PLoS Pathog <b>2007</b>.  3(8): e117</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17722977&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17722977&amp;dopt=abstract</a> </p><br />

    <p>15.   61565   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Design of HIV Protease Inhibitors Targeting Protein Backbone: An Effective Strategy for Combating Drug Resistance</p>

    <p>          Ghosh, AK, Chapsal, BD, Weber, IT, and Mitsuya, H</p>

    <p>          Acc Chem Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17722874&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17722874&amp;dopt=abstract</a> </p><br />

    <p>16.   61566   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Anti-AIDS agents 66: Syntheses and anti-HIV activity of phenolic and aza 3&#39;,4&#39;-di-O-(-)-camphanoyl-(+)-cis-khellactone (DCK) derivatives</p>

    <p>          Suzuki, M, Yu, D, Morris-Natschke, SL, Smith, PC, and Lee, KH</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17719228&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17719228&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   61567   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Toward novel HIV-1 integrase binding inhibitors: Molecular modeling, synthesis, and biological studies</p>

    <p>          Mugnaini, C, Rajamaki, S, Tintori, C, Corelli, F, Massa, S, Witvrouw, M, Debyser, Z, Veljkovic, V, and Botta, M</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17716893&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17716893&amp;dopt=abstract</a> </p><br />

    <p>18.   61568   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Design of extended short hairpin RNAs for HIV-1 inhibition</p>

    <p>          Liu, YP, Haasnoot, J, and Berkhout, B</p>

    <p>          Nucleic Acids Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17715143&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17715143&amp;dopt=abstract</a> </p><br />

    <p>19.   61569   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          In vitro Antiviral Effect of Meroditerpenes Isolated from the Brazilian Seaweed Stypopodium zonale (Dictyotales)</p>

    <p>          Soares, AR, Abrantes, JL, Souza, TM, Fontes, CF, Pereira, RC, de, Palmer Paixao Frugulhetti IC, and Teixeira, VL</p>

    <p>          Planta Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713872&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713872&amp;dopt=abstract</a> </p><br />

    <p>20.   61570   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Effects of triterpenoids and flavonoids isolated from Alnus firma on HIV-1 viral  enzymes</p>

    <p>          Yu, YB, Miyashiro, H, Nakamura, N, Hattori, M, and Park, JC</p>

    <p>          Arch Pharm Res  <b>2007</b>.  30(7): 820-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17703732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17703732&amp;dopt=abstract</a> </p><br />

    <p>21.   61571   HIV-LS-382; PUBMED-HIV-9/4/2007</p>

    <p class="memofmt1-2">          Bioavailability of the amino acid-attached prodrug as a new anti-HIV agent in rats</p>

    <p>          Chae, KA, Cho, HJ, Sung, JM, Lee, H, Seo, DC, Kim, JS, and Shin, HC</p>

    <p>          J Vet Sci <b>2007</b>.  8(3): 263-267</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17679773&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17679773&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
