

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-07-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="k1MaCUE3m/3dEAABYeuAAAC4jXmhdH1ziD8QJ3gIcrKb6FMeK31lQLdV1x7nQlZusl8Ga1TPXdRnweIjCuuEKduaHIxS59hvm3ISDykWhaus30xPdpzsDfLckpY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2ACE0F01" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">June 24 - July 7, 2011</p>


    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21704526">Phenylpropenamide Derivatives: Anti-Hepatitis B Virus Activity of the Z Isomer, SAR and the Search for Novel Analogs</a>. Wang, P., D. Naduthambi, R.T. Mosley, C. Niu, P.A. Furman, M.J. Otto, and M.J. Sofia. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>. PMID[21704526].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21700368">Synthesis, Antiviral Activity, Cytotoxicity and Cellular Pharmacology of L-3&#39;-Azido-2&#39;,3&#39;-Dideoxypurine Nucleosides</a>. Zhang, H.W., M. Detorio, B.D. Herman, S. Solomon, L. Bassit, J.H. Nettles, A. Obikhod, S.J. Tao, J.W. Mellors, N. Sluis-Cremer, S.J. Coats, and R.F. Schinazi. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>. PMID[21700368].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21719300">Synthesis and Antiviral Evaluation of 9-(S)-[3-Alkoxy-2-(Phosphonomethoxy)Propyl]Nucleoside Alkoxyalkyl Esters: Inhibitors of Hepatitis C Virus and HIV-1 Replication</a>. Valiaeva, N., D.L. Wyles, R.T. Schooley, J.B. Hwu, J.R. Beadle, M.N. Prichard, and K.Y. Hostetler. Bioorganic &amp; Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>. PMID[21719300].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21699793">Differential Efficacy of Protease Inhibitors against HCV Genotype 2a, 3a, 5a, and 6a Ns3/4a Protease Recombinant Viruses</a>. Gottwein, J.M., T.K. Scheel, T.B. Jensen, L. Ghanem, and J. Bukh. Gastroenterology, 2011. <b>[Epub ahead of print]</b>. PMID[21699793].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21699179">Discovery of Naturally Occurring Aurones That are Potent Allosteric Inhibitors of Hepatitis C Virus RNA-Dependent RNA Polymerase</a>. Haudecoeur, R., A. Ahmed-Belkacem, W. Yi, A. Fortune, R. Brillet, C. Belle, E. Nicolle, C. Pallier, J.M. Pawlotsky, and A. Boumendjel. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>. PMID[21699179].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>
    
    <p class="memofmt2-2">Herpes Simplex virus</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21725588">Inhibition of Multiplication of Herpes Simplex Virus by Caffeic Acid</a>. Ikeda, K., K. Tsujimoto, M. Uozaki, M. Nishide, Y. Suzuki, A.H. Koyama, and H. Yamasaki. International Journal of Molecular Medicine, 2011. <b>[Epub ahead of print]</b>. PMID[21725588].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21722672">Ck2 Inhibitors Increase the Sensitivity of HSV-1 to Interferon-Beta</a>. Smith, M.C., A.M. Bayless, E.T. Goddard, and D.J. Davido. Antiviral Research, 2011. <b>[Epub ahead of print]</b>. PMID[21722672].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21697878">Synthesis of 3-O-Sulfonated Heparan Sulfate Octasaccharides That Inhibit the Herpes Simplex Virus Type 1 Host-Cell Interaction</a>. Hu, Y.P., S.Y. Lin, C.Y. Huang, M.M. Zulueta, J.Y. Liu, W. Chang, and S.C. Hung. Nature chemistry, 2011. 3(7): p. 557-563; PMID[21697878].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21696963">Design, Synthesis and Biological Evaluation of 2&#39;-Deoxy-2&#39;,2&#39;-difluoro-5-halouridine phosphoramidate protides</a>. Quintiliani, M., L. Persoons, N. Solaroli, A. Karlsson, G. Andrei, R. Snoeck, J. Balzarini, and C. McGuigan. Bioorganic &amp; Medicinal Chemistry, 2011. 19(14): p. 4338-4345; PMID[21696963].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0624-070711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291523000002">New Protease Inhibitors for HCV - Help Is on the Way</a>. Fusco, D.N. and R.T. Chung. Journal of Hepatology, 2011. 54(6): p. 1087-1089; ISI[000291523000002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291687900006">Selection, Optimization, and Pharmacokinetic Properties of a Novel, Potent Antiviral Locked Nucleic Acid-Based Antisense Oligomer Targeting Hepatitis C Virus Internal Ribosome Entry Site</a>. Laxton, C., K. Brady, S. Moschos, P. Turnpenny, J. Rawal, D.C. Pryde, B. Sidders, R. Corbau, C. Pickford, and E.J. Murray. Antimicrobial Agents and Chemotherapy, 2011. 55(7): p. 3105-3114; ISI[000291687900006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291687900023">Inhibition of Herpes Simplex Virus Type 1 and Type 2 Infections by Peptide-Derivatized Dendrimers</a>. Luganini, A., S.F. Nicoletto, L. Pizzuto, G. Pirri, A. Giuliani, S. Landolfo, and G. Gribaudo. Antimicrobial Agents and Chemotherapy, 2011. 55(7): p. 3231-3239; ISI[000291687900023].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0624-070711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
