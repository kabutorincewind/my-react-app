

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-06-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="syqmD7C0j+E1IqjmXfa3X4nRkgFV1nr8u2raeTy2C7JKsV1lYbUvWmDg3NQ+3+ZtO1Bt6ikTUGOdVAQGGMV6P2Gj/K8D7gFdzJE7FoA6TKyl324K5Xua562/n4k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CAD36F08" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">May 28- June 10, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20397028">The effect of prolactin (PRL) on the growth of Toxoplasma gondii tachyzoites in vitro.</a> Dzitko, K., J. Gatkowska, P. Plocinski, B. Dziadek, and H. Dlugonska. Parasitol Res. 107(1): p. 199-204; PMID[20397028].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20513704">Uptake of T-2307, a novel arylamidine, in Candida albicans.</a> Nishikawa, H., E. Yamada, T. Shibata, S. Uchihashi, H. Fan, H. Hayakawa, N. Nomura, and J. Mitsuyama. J Antimicrob Chemother. <b>[Epub ahead of print]</b>; PMID[20513704].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20519869">Antifungal activity of essential oils and their constituents against Candida Spp. and their effects on activity of amphotericin B.</a> Nozaki, A., E. Takahashi, K. Okamoto, H. Ito, and T. Hatano. Yakugaku Zasshi. 130(6): p. 895-902; PMID[20519869].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20378674">Potential synergistic activity of antimycotic substances in combination with human platelets against Aspergillus fumigatus.</a> Perkhofer, S., K. Trappl, W. Nussbaumer, M.P. Dierich, and C. Lass-Florl. J Antimicrob Chemother. 65(6): p. 1309-11; PMID[20378674].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20199825">Synthesis and in vitro study of methylene-bis-tetrahydro[1,3]thiazolo[4,5-c]isoxazoles as potential nematicidal agents.</a> Srinivas, A., A. Nagaraj, and C.S. Reddy. Eur J Med Chem. 45(6): p. 2353-8; PMID[20199825].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p>6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20524153">Isavuconazole: A Comprehensive Review of Spectrum of Activity of a New Triazole. Mycopathologia.</a> Thompson, G.R., 3rd and N.P. Wiederhold. <b>[Epub ahead of print]</b>; PMID[20524153].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20530976">In vitro and in vivo Activities of a New Lead Compound I2906 against Mycobacterium tuberculosis.</a> Lu, J., J. Yue, J. Wu, R. Luo, Z. Hu, J. Li, Y. Bai, Z. Tang, Q. Xian, X. Zhang, and H. Wang. Pharmacology. 85(6): p. 365-371; PMID[20530976].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20462762">Inhibition of Mycobacterium tuberculosis tyrosine phosphatase PtpA by synthetic chalcones: kinetics, molecular modeling, toxicity and effect on growth.</a> Mascarello, A., L.D. Chiaradia, J. Vernal, A. Villarino, R.V. Guido, P. Perizzolo, V. Poirier, D. Wong, P.G. Martins, R.J. Nunes, R.A. Yunes, A.D. Andricopulo, Y. Av-Gay, and H. Terenzi. Bioorg Med Chem. 18(11): p. 3783-9; PMID[20462762].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20521931">TMC207: the first compound of a new class of potent anti-tuberculosis drugs.</a> Matteelli, A., A.C. Carvalho, K.E. Dooley, and A. Kritski. Future Microbiol. 5(6): p. 849-58; PMID[20521931].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20479209">Mycobacterium tuberculosis and sulfamethoxazole susceptibility.</a> Ong, W., A. Sievers, and D.E. Leslie. Antimicrob Agents Chemother. 54(6): p. 2748; author reply 2748-9; PMID[20479209].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20516287">In Vitro Antituberculosis Activities of ACH-702, a Novel Isothiazoloquinolone, against Quinolone-Susceptible and Quinolone-Resistant Isolates.</a> Pucci, M.J., M. Ackerman, J.A. Thanassi, C.M. Shoen, and M.H. Cynamon. Antimicrob Agents Chemother. <b>[Epub ahead of print]</b>; PMID[20516287].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20488716">Synthesis and biological evaluation of pyrimidine analogs of antimycobacterial purines.</a> Read, M.L., M. Braendvang, P.O. Miranda, and L.L. Gundersen. Bioorg Med Chem. 18(11): p. 3885-97; PMID[20488716].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20525733">Piperine as an inhibitor of Rv1258c, a putative multidrug efflux pump of Mycobacterium tuberculosis.</a> Sharma, S., M. Kumar, A. Nargotra, S. Koul, and I.A. Khan. J Antimicrob Chemother. <b>[Epub ahead of print]</b>; PMID[20525733].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0528-061010.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277756000028">In Vivo Comparison of the Pharmacodynamic Targets for Echinocandin Drugs against Candida Species.</a> Andes, D., D.J. Diekema, M.A. Pfaller, J. Bohrmuller, K. Marchillo, and A. Lepak. Antimicrobial Agents and Chemotherapy. 54(6): p. 2497-2506; ISI[000277756000028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277887000007">Pyrrole-Based Hydrazones Synthesized and Evaluated In Vitro as Potential Tuberculostatics.</a> Bijev, A. and M. Georgieva. Letters in Drug Design &amp; Discovery. 7(6): p. 430-437; ISI[000277887000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274983700156">New Mycobacterium Tuberculosis DNA Gyrase Inhibitors: Naphthyridone Series Against Tb.</a> Guardia, A., D. Barros, and M.J. Remuinan. Drugs of the Future, 2009. 34: p. 185-185; ISI[000274983700156].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277788900034">Selective one-pot multicomponent synthesis and anti-tubercular evaluation of 5-(aryl/cyclohexylsulfanyl)-2-alkoxy-4,6-diarylnicotinonitriles.</a> Manikannan, R., S. Muthusubramanian, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters. 20(11): p. 3352-3355; ISI[000277788900034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277794500010">In vitro synergistic interactions of the effects of various statins and azoles against some clinically important fungi.</a> Nyilasi, I., S. Kocsube, K. Krizsan, L. Galgoczy, M. Pesti, T. Papp, and C. Vagvolgyi. Fems Microbiology Letters. 307(2): p. 175-184; ISI[000277794500010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277737700012">Spectral characterization and antimicrobial activity of Cu(II) and Fe(III) complexes of 2-(5-Cl/NO2-1H-benzimidazol-2-yl)-4-Br/NO2-phenols.</a> Tavman, A., I. Boz, A.S. Birteksoz, and A. Cinarli. Journal of Coordination Chemistry. 63(8): p. 1398-1410; ISI[000277737700012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277756000069">Madurella mycetomatis Is Not Susceptible to the Echinocandin Class of Antifungal Agents.</a> van de Sande, W.W.J., A.H. Fahal, I. Bakker-Woudenberg, and A. van Belkum. Antimicrobial Agents and Chemotherapy. 54(6): p. 2738-2740; ISI[000277756000069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277761500017">Synthesis, characterization and biocidal activities of heterobimetallic complexes having tin(IV) as a padlock.</a> Husain, A., S.A.A. Nami, and K.S. Siddiqi. Journal of Molecular Structure. 970(1-3): p. 117-127; ISI[000277761500017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277588600004">Antimicrobial and DNA Cleavage Studies of New N2O2 Diazadioxa Macrocyclic Schiff Base Co(II), Ni(II) and Cu(II) Complexes Containing Triazole Head Unit: Synthesis and Spectroscopic Approach.</a> Patil, S.A., U.V. Kamble, and P.S. Badami. Journal of Macromolecular Science Part A-Pure and Applied Chemistry. 47(6): p. 510-520; ISI[000277588600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277829200043">Calcium-dependent protein kinase 1 is an essential regulator of exocytosis in Toxoplasma.</a> Lourido, S., J. Shuman, C. Zhang, K.M. Shokat, R. Hui, and L.D. Sibley. Nature. 465(7296): p. 359-U118; ISI[000277829200043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277545900016">Virtual screening, selection and development of a benzindolone structural scaffold for inhibition of lumazine synthase.</a> Talukdar, A., E. Morgunova, J.X. Duan, W. Meining, N. Foloppe, L. Nilsson, A. Bacher, B. Illarionov, M. Fischer, R. Ladenstein, and M. Cushman. Bioorganic &amp; Medicinal Chemistry. 18(10): p. 3518-3534; ISI[000277545900016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0528-061010.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
