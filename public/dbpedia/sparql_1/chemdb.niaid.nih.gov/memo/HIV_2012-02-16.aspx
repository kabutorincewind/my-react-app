

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-02-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4Sw37Esw5AHClz4VCeLG5xlHYagRMUJLCSb/5SwTmNxWKlMQD6YNDX8cpZc3Q08pg0h7jDG6JTM9y04niijzhgri3sFbhI1SD15MjJCJFySkFm4L43RDVkPIXms=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AF315688" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 3 - February 16, 2012</h1>

    <p class="memofmt2-2">Pubmed citations
    <br />
    <br /></p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22316150">Identification of HIV Inhibitors Guided by Free Energy Perturbation Calculations.</a> Acevedo, O., Z. Ambrose, P.T. Flaherty, H. Aamer, P. Jain, and S.V. Sambasivarao, Current Pharmaceutical Design, 2012. <b>[Epub ahead of print]</b>; PMID[22316150].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22313954">Lower than Expected Maraviroc Concentrations in Cerebrospinal Fluid Exceed the Wild-type CCR5-tropic HIV-1 50% Inhibitory Concentration.</a> Croteau, D., B.M. Best, S. Letendre, S.S. Rossi, R.J. Ellis, D.B. Clifford, A.C. Collier, B.B. Gelman, J.C. McArthur, J.A. McCutchan, S. Morgello, and I. Grant, AIDS, 2012. <b>[Epub ahead of print]</b>; PMID[22313954].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22314266">Polyanionic N-donor Ligands as Chelating Agents in Transition Metal Complexes: Synthesis, Structural Characterization and Antiviral Properties Against HIV.</a> Garcia-Gallego, S., J.S. Rodriguez, J.L. Jimenez, M. Cangiotti, M.F. Ottaviani, M.A. Munoz-Fernandez, R. Gomez, and F.J. de la Mata, Dalton Transactions, 2012. <b>[Epub ahead of print]</b>; PMID[22314266].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22330930">Pre-clinical Evaluation of the HIV-1 Fusion Inhibitor L&#39;644 as a Potential Candidate Microbicide.</a> Harman, S., C. Herrera, N. Armanasco, J. Nuttall, and R.J. Shattock, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22330930].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22321894">Prediction of HIV-1 Protease/Inhibitor Affinity using Rosetta Ligand.</a> Lemmon, G., K. Kaufmann, and J. Meiler, Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22321894].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22314289">SHAPE-directed Discovery of Potent shRNA Inhibitors of HIV-1.</a> Low, J.T., S.A. Knoepfel, J.M. Watts, O. Ter Brake, B. Berkhout, and K.M. Weeks, Molecular Therapy, 2012. <b>[Epub ahead of print]</b>; PMID[22314289].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22239286">Interaction of I50V Mutant and I50L/A71V Double Mutant HIV-Protease with Inhibitor TMC114 (Darunavir): Molecular Dynamics Simulation and Binding Free Energy Studies.</a> Meher, B.R. and Y. Wang, The Journal of Physical Chemistry B, 2012. <b>[Epub ahead of print]</b>; PMID[22239286].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22277590">Conjugation of Cell-penetrating Peptides Leads to Identification of anti-HIV Peptides from Matrix Proteins.</a> Narumi, T., M. Komoriya, C. Hashimoto, H. Wu, W. Nomura, S. Suzuki, T. Tanaka, J. Chiba, N. Yamamoto, T. Murakami, and H. Tamamura, Bioorganic &amp; Medicinal Chemistry, 2012. 20(4): p. 1468-1474; PMID[22277590].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22247043">A Synthetic C34 Trimer of HIV-1 gp41 Shows Significant Increase in Inhibition Potency</a>. Nomura, W., C. Hashimoto, A. Ohya, K. Miyauchi, E. Urano, T. Tanaka, T. Narumi, T. Nakahara, J.A. Komano, N. Yamamoto, and H. Tamamura, ChemMedChem, 2012. 7(2): p. 205-208; PMID[22247043].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22105607">Inhibition of Human Immunodeficiency Virus Replication by Cell Membrane-crossing Oligomers.</a> Posch, W., S. Piper, T. Lindhorst, B. Werner, A. Fletcher, H. Bock, C. Lass-Florl, H. Stoiber, and D. Wilflingseder, Molecular Medicine, 2012. 18(1): p. 111-122; PMID[22105607].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22105607">Truncated Reverse Isoxazolidinyl Nucleosides: A New Class of Allosteric HIV-1 Reverse Transcriptase Inhibitors</a>. Romeo, R., S.V. Giofre, B. Macchi, E. Balestrieri, A. Mastino, P. Merino, C. Carnovale, G. Romeo, and U. Chiacchio, ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22323191].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22330918">The Role of Natural Polymorphisms in HIV-1 CRF02_AG Protease on Protease Inhibitor Hypersusceptibility.</a> Santos, A.F., D.M. Tebit, M.S. Lalonde, A.B. Abecasis, A. Ratcliff, R.J. Camacho, R.S. Diaz, O. Herchenroder, M.A. Soares, and E.J. Arts, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22330918].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22256860">Homology Model-Guided 3D-QSAR Studies of HIV-1 Integrase Inhibitors.</a> Sharma, H., X. Cheng, and J.K. Buolamwini, Journal of Chemical Information and Modeling, 2012. <b>[Epub ahead of print]</b>; PMID[22256860].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22314523">Exploiting the Anti-HIV-1 Activity of Acyclovir: the Suppression of Primary and Drug-Resistant HIV Isolates and its Potentiation by Ribavirin.</a> Vanpouille, C., A. Lisco, A. Introini, J.C. Grivel, A. Munawwar, M. Merbah, R. Schinazi, M. Derudas, C. McGuigan, J. Balzarini, and L. Margolis, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22314523].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22321026">Elvitegravir : A Once-daily Inhibitor of HIV-1 Integrase.</a> Wills, T. and V. Vega, Expert Opinion on Investigational Drugs, 2012. 21(3): p. 395-401; PMID[22321026].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22144113">Molecular Mechanism of HIV-1 Integrase-vDNA Interactions and Strand Transfer Inhibitor Action: A Molecular Modeling Perspective.</a> Xue, W., H. Liu, and X. Yao, Journal of Computational Chemistry, 2012. 33(5): p. 527-536; PMID[22144113].
    <br />
    <b>[Pubmed]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299175500023">Cytotoxicity, Antioxidant, and Antimicrobial Activities of Novel 2-Quinolone Derivatives Derived from Coumarin.</a> Al-Amiery, A.A., R.I.H. Al-Bayati, K.Y. Saour, and M.F. Radi, Research on Chemical Intermediates, 2012. 38(2): p. 559-569; ISI[000299175500023].
    <br />
    <b>[WOS]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299088600011">The Structure-bioresponse Relationships Studies of Nucleoside Derivatives Conjugated with the 1-Adamantane Moiety.</a> Bayat, Z. and A.R.R. Yassavoli, Russian Journal of Physical Chemistry A, 2012. 86(2): p. 210-214; ISI[000299088600011].
    <br />
    <b>[WOS]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298827800005">A Structural Model of the HIV-1 REV-integrase Complex: The Molecular Basis of Integrase Regulation by REV.</a> Benyamini, H., A. Loyter, and A. Friedler, Biochemical and Biophysical Research Communications, 2011. 416(3-4): p. 252-257; ISI[000298827800005].
    <br />
    <b>[WOS]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298926700003">Antiviral Activities of Sulfated Polysaccharides Isolated from Sphaerococcus coronopifolius (Rhodophytha, Gigartinales) and Boergeseniella thuyoides (Rhodophyta, Ceramiales).</a> Bouhlal, R., C. Haslin, J.C. Chermann, S. Colliec-Jouault, C. Sinquin, G. Simon, S. Cerantola, H. Riadi, and N. Bourgougnon, Marine Drugs, 2011. 9(7): p. 1187-1209; ISI[000298926700003].
    <br />
    <b>[WOS]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299177000034">De Novo Design of Small Molecule Inhibitors Targeting the LEDGF/p75-HIV Integrase Interaction.</a> Cavalluzzo, C., A. Voet, F. Christ, B.K. Singh, A. Sharma, Z. Debyser, M. De Maeyer, and E. Van der Eycken, Rsc Advances, 2012. 2(3): p. 974-984; ISI[000299177000034].
    <br />
    <b>[WOS]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299241300009">Recent Advances in Developing Small Molecules Targeting RNA.</a> Guan, L.R. and M.D. Disney, ACS Chemical Biology, 2012. 7(1): p. 73-86; ISI[000299241300009].
    <br />
    <b>[WOS]</b>. HIV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298882100005">Pharmacophore Modeling of Some Novel Indole beta-Diketo acid and Coumarin-based Derivatives as HIV Integrase Inhibitors.</a> Jain, S.V., L.V. Sonawane, R.R. Patil, and S.B. Bari, Medicinal Chemistry Research, 2012. 21(2): p. 165-173; ISI[000298882100005].
    <br />
    <b>[WOS]</b>. HIV_0203-021612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
