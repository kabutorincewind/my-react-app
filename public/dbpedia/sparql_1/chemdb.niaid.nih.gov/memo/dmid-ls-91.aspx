

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-91.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Mh//bF5A2TETq6SWInrco/gJfEQZLSWuoP0vUf5Cpi5bHkHVUJllvUmcgbEBOmrbjygWHCYNTcbjJvBmGNluVXYOIdsM0KMy30DBPNjCzcQjsUtwP0c7gXJ+zCU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="732788F4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-91-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56350   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of ovotransferrin derived peptides</p>

    <p>          Giansanti, F, Massucci, MT, Giardi, MF, Nozza, F, Pulsinelli, E, Nicolini, C, Botti, D, and Antonini, G</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  331(1): 69-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845359&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845359&amp;dopt=abstract</a> </p><br />

    <p>2.     56351   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-hepatitis C virus effect of citrus unshiu peel and its active ingredient nobiletin</p>

    <p>          Suzuki, M, Sasaki, K, Yoshizaki, F, Oguchi, K, Fujisawa, M, and Cyong, JC</p>

    <p>          Am J Chin Med <b>2005</b>.  33(1): 87-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15844836&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15844836&amp;dopt=abstract</a> </p><br />

    <p>3.     56352   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Drug screening for influenza neuraminidase inhibitors</p>

    <p>          Liu, A, Cao, H, and Du, G</p>

    <p>          Sci China C Life Sci <b>2005</b>.  48(1): 1-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15844351&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15844351&amp;dopt=abstract</a> </p><br />

    <p>4.     56353   DMID-LS-91; WOS-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Flavivirus Infections by Antisense Oligorners Specifically Suppressing Viral Translation and Rna Replication</p>

    <p>          Deas, T. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(8): 4599-4609</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228073700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228073700005</a> </p><br />

    <p>5.     56354   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of influenza a virus replication by resveratrol</p>

    <p>          Palamara, AT,  Nencioni, L, Aquilano, K , De, Chiara G, Hernandez, L, Cozzolino, F, Ciriolo, MR, and Garaci, E</p>

    <p>          J Infect Dis <b>2005</b>.  191(10): 1719-1729</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15838800&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15838800&amp;dopt=abstract</a> </p><br />

    <p>6.     56355   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activities of plant triterpenes, their derivatives and ribavirin phosphonates</p>

    <p>          Il&#39;icheva, TN, Kulishova, LA, Shul&#39;ts, EE, Petrenko, NI, Uzenkova, NV, Yas&#39;ko, MV, Serova, OA, Volkov, GN, Belanov, EF, Tolstikov, GA, and Pokrovskii, AG</p>

    <p>          Vestnik Rossiiskoi Akademii Meditsinskikh Nauk <b>2005</b>.(2): 26-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     56356   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p><b>          Antiviral therapy of influenza</b> </p>

    <p>          McCullers, JA</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(3): 305-312</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15833061&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15833061&amp;dopt=abstract</a> </p><br />

    <p>8.     56357   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Methods and compositions for identifying anti-hcv agents</p>

    <p>          Glenn, Jeffrey S, Einav, Shirit, and Elazar, Menashe</p>

    <p>          PATENT:  WO <b>2005032329</b>  ISSUE DATE:  20050414</p>

    <p>          APPLICATION: 2004  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (The Board of Trustees of the Leland Stanford Junior University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     56358   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Combinatorial Optimization of Isatin-beta-Thiosemicarbazones as Anti-poxvirus Agents</p>

    <p>          Pirrung, MC, Pansare, SV, Sarma, KD, Keith, KA, and Kern, ER</p>

    <p>          J Med Chem <b>2005</b>.  48(8): 3045-3050</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828843&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828843&amp;dopt=abstract</a> </p><br />

    <p>10.   56359   DMID-LS-91; PUBMED-DMID-4/25/2005; LR-13604-LWC ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Dimeric Zanamivir Conjugates with Various Linking Groups Are Potent, Long-Lasting Inhibitors of Influenza Neuraminidase Including H5N1 Avian Influenza</p>

    <p>          Macdonald, SJ, Cameron, R, Demaine, DA, Fenton, RJ, Foster, G, Gower, D, Hamblin, JN, Hamilton, S, Hart, GJ, Hill, AP, Inglis, GG, Jin, B, Jones, HT, McConnell, DB, McKimm-Breschkin, J, Mills, G, Nguyen, V, Owens, IJ, Parry, N, Shanahan, SE, Smith, D, Watson, KG, Wu, WY, and Tucker, SP</p>

    <p>          J Med Chem <b>2005</b>.  48(8): 2964-2971</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828835&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828835&amp;dopt=abstract</a> </p><br />

    <p>11.   56360   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antiviral Activity of Adenosine 5&#39;-Phosphonate Analogues as Chain Terminators against Hepatitis C Virus</p>

    <p>          Koh, YH, Shim, JH, Wu, JZ, Zhong, W, Hong, Z, and Girardet, JL</p>

    <p>          J Med Chem <b>2005</b>.  48(8): 2867-2875</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828825&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828825&amp;dopt=abstract</a> </p><br />

    <p>12.   56361   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New conformationally locked bicyclic N,O-nucleoside analogs of antiviral drugs</p>

    <p>          Procopio, Antonio, Alcaro, Stefano, De Nino, Antonio, Maiuolo, Loredana, Ortuso, Francesco, and Sindona, Giovanni</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(3): 545-550</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   56362   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Induction of lytic Epstein-Barr virus (EBV) infection by synergistic action of rituximab and dexamethasone renders EBV-positive lymphoma cells more susceptible to ganciclovir cytotoxicity in vitro and in vivo</p>

    <p>          Daibata, M, Bandobashi, K, Kuroda, M, Imai, S, Miyoshi, I, and Taguchi, H</p>

    <p>          J Virol <b>2005</b> .  79(9): 5875-5879</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15827204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15827204&amp;dopt=abstract</a> </p><br />

    <p>14.   56363   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Concise synthesis and antiviral activity of novel unsaturated acyclic pyrimidine nucleosides</p>

    <p>          Oh, CH, Baek, TR, and Hong, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(2): 153-160</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15822621&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15822621&amp;dopt=abstract</a> </p><br />

    <p>15.   56364   DMID-LS-91; WOS-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Purification and Characterization of Hcv Rna-Dependent Rna Polymerase From Korean Genotype 1b Isolate: Implications for Discovery of Hcv Polymerase Inhibitors</p>

    <p>          Kim, J., Lee, M., and Kim, Y.</p>

    <p>          Bulletin of the Korean Chemical Society <b>2005</b>.  26(2): 285-291</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227625800022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227625800022</a> </p><br />

    <p>16.   56365   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacokinetics of antiviral agents for the treatment of cytomegalovirus infection</p>

    <p>          Baillie, GM</p>

    <p>          Am J Health Syst Pharm <b>2005</b>.  62(8 Suppl 1): S14-S17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15821261&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15821261&amp;dopt=abstract</a> </p><br />

    <p>17.   56366   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The antiviral activity of the combinations of netropsin derivatives with modified nucleosides and phosphonoacetic acid as estimated in the model of herpesvirus type 1 in a Vero cell culture</p>

    <p>          Andronova, VL, Grokhovsky, SL, Surovaya, AN, Gursky, GV, and Galegov, GA</p>

    <p>          Doklady Biochemistry and Biophysics <b>2005</b>.  400: 84-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   56367   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Valganciclovir: a review of its use in the management of CMV infection and disease in immunocompromised patients</p>

    <p>          Cvetkovic, RS and Wellington, K</p>

    <p>          Drugs <b>2005</b>.  65(6): 859-878</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15819597&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15819597&amp;dopt=abstract</a> </p><br />

    <p>19.   56368   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ribonucleotide reductase inhibitors enhance cidofovir-induced apoptosis in EBV-positive nasopharyngeal carcinoma xenografts</p>

    <p>          Wakisaka, N, Yoshizaki, T, Raab-Traub, N, and Pagano, JS</p>

    <p>          Int J Cancer <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15818619&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15818619&amp;dopt=abstract</a> </p><br />

    <p>20.   56369   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Reappearance of influenza B/Victoria/2/87-lineage viruses: epidemic activity, genetic diversity and vaccination efficacy in the Finnish Defence Forces</p>

    <p>          Ikonen, N, Pyhala, R, Axelin, T, Kleemola, M, and Korpela, H</p>

    <p>          Epidemiol Infect <b>2005</b>.  133(2): 263-271</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15816151&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15816151&amp;dopt=abstract</a> </p><br />

    <p>21.   56370   DMID-LS-91; SCIFINDER-DMID-4/25/2005; LR-14643-LWC ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, in vitro cytotoxic and antiviral activity of cis-[Pt(R(-) and S(+)-2-a-hydroxybenzylbenzimidazole)2Cl2] complexes</p>

    <p>          Goekce, M, Utku, S, Guer, S, Oezkul, A, and Guemues, F</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(2): 135-141</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   56371   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of tetrahydro-2H-thiopyran-4-carboxamides as anti-herpesvirus agents</p>

    <p>          Kontani, Toru, Miyata, Junji, Hamaguchi, Wataru, Kawano, Tomoaki, Kamikawa, Akio, Suzuki, Hiroshi, and Sudo, Kenji</p>

    <p>          PATENT:  US <b>2005032855</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004-60264  PP: 13 pp.</p>

    <p>          ASSIGNEE:  (Yamanouchi Pharmaceutical Co., Ltd. Japan and Rational Drug Design Laboratories)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   56372   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Management of influenza virus infections with neuraminidase inhibitors : detection, incidence, and implications of drug resistance</p>

    <p>          McKimm-Breschkin, JL</p>

    <p>          Treat Respir Med <b>2005</b>.  4(2): 107-116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15813662&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15813662&amp;dopt=abstract</a> </p><br />

    <p>24.   56373   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Characterization of recombinant influenza B viruses with key neuraminidase inhibitor resistance mutations</p>

    <p>          Jackson, David, Barclay, Wendy, and Zuercher, Thomas</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2005</b>.  55(2): 162-169</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>25.   56374   DMID-LS-91; WOS-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          1,2-Disubstituted Benzimidazoles as Potential Antiviral Agents</p>

    <p>          Pandey, V., Gupta, V., and Tiwari, D.</p>

    <p>          Indian Journal of Heterocyclic Chemistry <b>2005</b>.  14(3): 217-220</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228194000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228194000009</a> </p><br />

    <p>26.   56375   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Salts of 3,5-dimethyl-1-adamantylamine and water-soluble carboxy-containing polymers as antiviral agents</p>

    <p>          Marchukov, Valery Fleksandrovich, Platonov, Vitaly Georgievich, Selkov, Sergei Alekseevich, and Chernobrovy, Aleksandr Nikolaevich</p>

    <p>          PATENT:  WO <b>2005028528</b>  ISSUE DATE:  20050331</p>

    <p>          APPLICATION: 2003  PP: 21 pp.</p>

    <p>          ASSIGNEE:  (Akciju Sabiedriba &#39;olaines Kimisko-Farmaceitiska Rupnica&#39;, Latvia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   56376   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 1,2,3-triazole analogs of antiviral drugs</p>

    <p>          Danowitz, Amy M and Lindberg, James G</p>

    <p>          229th ACS National Meeting <b>2005</b>.: CHED-1156</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   56377   DMID-LS-91; WOS-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of a Cotton Rat-Human Metapneumovirus (Hmpv) Model for Identifying and Evaluating Potential Hmpv Antivirals and Vaccines</p>

    <p>          Wyde, P. <i>et al.</i></p>

    <p>          Antiviral Research <b>2005</b>.  66(1): 57-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228237800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228237800009</a> </p><br />

    <p>29.   56378   DMID-LS-91; PUBMED-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of a cell-based high-throughput specificity screen using a hepatitis C virus-bovine viral diarrhea virus dual replicon assay</p>

    <p>          O&#39;Boyle, DR, Nower, PT, Lemm, JA, Valera, L, Sun, JH, Rigat, K, Colonno, R, and Gao, M</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(4): 1346-1353</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793110&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793110&amp;dopt=abstract</a> </p><br />

    <p>30.   56379   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Overview of antiviral and anti-inflammatory treatment for severe acute respiratory syndrome</p>

    <p>          Chihrin, Stephen and Loutfy, Mona R</p>

    <p>          Expert Review of Anti-Infective Therapy <b>2005</b>.  3(2): 251-262</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   56380   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral agents for the treatment, control and prevention of infections by coronaviruses</p>

    <p>          Silva, Abelardo and Erickson, John W</p>

    <p>          PATENT:  WO <b>2005032453</b>  ISSUE DATE:  20050414</p>

    <p>          APPLICATION: 2004  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (Sequoia Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   56381   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Promazine analogues as potential anti SARS-CoV drugs</p>

    <p>          Hsieh, Hsing Pang, Wu, Yu-Shan, Lu, Ping-Hsun, Chen, Chi-Min, Chao, Yu-Chan, Jan, Jia-Tsrong, Lo, Huei-Ru, Ma, Shiou-Hwa, Chao, Yu-Sheng, and Hsu, Tsu-An</p>

    <p>          229th ACS National Meeting <b>2005</b>.: MEDI-335</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   56382   DMID-LS-91; SCIFINDER-DMID-4/25/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Advances in Anti-Viral Therapeutics</p>

    <p>          Klebl, Bert M</p>

    <p>          Expert Opinion on Investigational Drugs <b>2005</b>.  14(3): 343-348</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
