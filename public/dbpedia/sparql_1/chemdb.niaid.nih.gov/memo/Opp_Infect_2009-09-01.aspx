

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-09-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="2sai2kl68S4tbYocUgl/zmCzzRlJB3fFTiW4kFXVe447OIfDO8J/bIubpNhs2Z/8+hEFmWmI0vSK3cDaeyNkXvO77EFNCqUh3VWyTmdKoPaOL4YLU2BjqTCrEJM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9FC78E94" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">August 19, 2009 - September 1, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19698800?ordinalpos=14&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Thiolactomycin analogues as potential anti-Toxoplasma gondii agents.</a></p>

    <p class="NoSpacing">Martins-Duarte ES, Jones SM, Gilbert IH, Atella GC, de Souza W, Vommaro RC.</p>

    <p class="NoSpacing">Parasitol Int. 2009 Aug 19. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19698800 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19724014?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Echinocandins: The Newest Class of Antifungals (October) (CE).</a></p>

    <p class="NoSpacing">Sucher AJ, Chahine EB, Balcer HE.</p>

    <p class="NoSpacing">Ann Pharmacother. 2009 Sep 1. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19724014 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19705386?ordinalpos=27&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design and Synthesis of a Series of Piperazine-1-carboxamidine Derivatives with Antifungal Activity Resulting from Accumulation of Endogenous Reactive Oxygen Species.</a></p>

    <p class="NoSpacing">François IE, Thevissen K, Pellens K, Meert EM, Heeres J, Freyne E, Coesemans E, Viellevoye M, Deroose F, Martinez Gonzalez S, Pastor J, Corens D, Meerpoel L, Borgers M, Ausma J, Dispersyn GD, Cammue BP.</p>

    <p class="NoSpacing">ChemMedChem. 2009 Aug 24. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19705386 [PubMed - as supplied by publisher]</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">4. Goldman, R.C. and B.E. Laughon, Discovery and validation of new antitubercular compounds as potential drug leads and probes. Tuberculosis (Edinb), 2009; PMID[19716767] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19716767?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline">http://www.ncbi.nlm.nih.gov/pubmed/19716767?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline</a>.</p> 

    <p class="ListParagraph">5. Nefzi, A., et al., Parallel synthesis of chiral pentaamines and pyrrolidine containing bis-heterocyclic libraries. Multiple scaffolds with multiple building blocks: a double diversity for the identification of new antitubercular compounds. Bioorg Med Chem Lett, 2009. 19(17): p. 5169-75; PMID[19632841] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19632841?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline">http://www.ncbi.nlm.nih.gov/pubmed/19632841?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline</a>.</p> 

    <p class="ListParagraph">6. Ranjith Kumar, R., et al., A facile synthesis and antimycobacterial evaluation of novel spiro-pyrido-pyrrolizines and pyrrolidines. Eur J Med Chem, 2009. 44(9): p. 3821-9; PMID[19524332] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19524332?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline">http://www.ncbi.nlm.nih.gov/pubmed/19524332?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline</a>.</p> 

    <p class="ListParagraph">7. Sanki, A.K., et al., Design, synthesis and biological evaluation of sugar-derived esters, alpha-ketoesters and alpha-ketoamides as inhibitors for Mycobacterium tuberculosis antigen 85C. Mol Biosyst, 2009. 5(9): p. 945-56; PMID[19668859] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19668859?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline">http://www.ncbi.nlm.nih.gov/pubmed/19668859?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline</a>.</p> 

    <p class="ListParagraph">8. Sharma, A., et al., In vitro and ex vivo activity of peptide deformylase inhibitors against Mycobacterium tuberculosis H37Rv. Int J Antimicrob Agents, 2009. 34(3): p. 226-30; PMID[19505802] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19505802?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline">http://www.ncbi.nlm.nih.gov/pubmed/19505802?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline</a>.</p> 

    <p class="ListParagraph">9. Sriram, D., et al., Novel Pthalazinyl Derivatives: Synthesis, Antimycobacterial Activities, and Inhibition of Mycobacterium tuberculosis Isocitrate Lyase Enzyme. Med Chem, 2009; PMID[19534678] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19534678?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline">http://www.ncbi.nlm.nih.gov/pubmed/19534678?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline</a>.</p> 

    <p class="ListParagraph">10. Vergara, F.M., et al., Antitubercular activity of alpha,omega-diaminoalkanes, H2N(CH2)nNH2. Bioorg Med Chem Lett, 2009. 19(17): p. 4937-8; PMID[19648006] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19648006?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline">http://www.ncbi.nlm.nih.gov/pubmed/19648006?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVMedline</a>.</p> 

    <p class="ListParagraph">11. Keri G, Szekelyhidi Z, Banhegyi P, Varga Z, Hegymegi- Barakonyi B, Szantai-Kis C, Hafenbradl D, Klebl B, Muller G, Ullrich A et al.</p>

    <p class="NoSpacing">Drug discovery in the kinase inhibitory field using the Nested Chemical Library technology.</p>

    <p class="NoSpacing">Assay Drug Dev Technol 2005, 3: 543 - 551.</p> 

    <p class="ListParagraph">12. Hegymegi-Barakonyi B, Szekely R, Varga Z, Kiss R, Borbely G, Nemeth G, Banhegyi P, Pato J, Greff Z, Horvath Z et al.</p>

    <p class="NoSpacing">Signalling inhibitors against Mycobacterium tuberculosis - early days of a new therapeutic concept in tuberculosis.</p>

    <p class="NoSpacing">Curr Med Chem 2008, 15: 2760 - 2770.</p>
    
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
