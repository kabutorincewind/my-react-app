

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-03-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ZLa1o4ePFxodvGmbAA4tRUJ3EU80oP2Fwj/uWQalOOgH+BmyEhHyK7dqvd2/HGvMXE6m8O5rfpv84ava1QNPRh3W6iy8KU9wt1HapI/YnUW1fAWRuvvEiD+YeRY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9FA27BF6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 13 - March 26, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25754473">Cell-specific RNA Aptamer against Human CCR5 Specifically Targets HIV-1 Susceptible Cells and Inhibits HIV-1 Infectivity.</a> Zhou, J., S. Satheesan, H. Li, M.S. Weinberg, K.V. Morris, J.C. Burnett, and J.J. Rossi. Chemistry &amp; Biology, 2015. 22(3): p. 379-390. PMID[25754473].
    <br />
    <b>[PubMed]</b>. HIV_0313-032615.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24722454">Histone Deacetylase Inhibitor Romidepsin Induces HIV Expression in CD4 T Cells from Patients on Suppressive Antiretroviral Therapy at Concentrations Achieved by Clinical Dosing.</a> Wei, D.G., V. Chiang, E. Fyne, M. Balakrishnan, T. Barnes, M. Graupe, J. Hesselgesser, A. Irrinki, J.P. Murry, G. Stepan, K.M. Stray, A. Tsai, H. Yu, J. Spindler, M. Kearney, C.A. Spina, D. McMahon, J. Lalezari, D. Sloan, J. Mellors, R. Geleziunas, and T. Cihlar. Plos Pathogens, 2014. 10(4): p. e1004071. PMID[24722454].</p>

    <p class="plaintext">[PubMed]. HIV_0313-032615.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25793526">Colorectal Mucus Binds DC-sign and Inhibits HIV-1 Trans-infection of CD4+ T-lymphocytes.</a> Stax, M.J., E.E. Mouser, T. van Montfort, R.W. Sanders, H.J. de Vries, H.L. Dekker, C. Herrera, D. Speijer, G. Pollakis, and W.A. Paxton. Plos One, 2015. 10(3): p. e0122020. PMID[25793526].
    <br />
    <b>[PubMed]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25728026">(3Z)-3-(2-[4-(Aryl)-1,3-thiazol-2-yl]hydrazin-1-ylidene)-2,3-dihydro-1H-indol-2-one Derivatives as Dual Inhibitors of HIV-1 Reverse Transcriptase.</a> Meleddu, R., S. Distinto, A. Corona, G. Bianco, V. Cannas, F. Esposito, A. Artese, S. Alcaro, P. Matyus, D. Bogdan, F. Cottiglia, E. Tramontano, and E. Maccioni. European Journal of Medicinal Chemistry, 2015. 93: p. 452-460. PMID[25728026].
    <br />
    <b>[PubMed]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25707013">Fused Heterocycles Bearing Bridgehead Nitrogen as Potent HIV-1 NNRTIs. Part 4: Design, Synthesis and Biological Evaluation of Novel Imidazo[1,2-a]pyrazines.</a> Huang, B., X. Liang, C. Li, W. Chen, T. Liu, X. Li, Y. Sun, L. Fu, H. Liu, E. De Clercq, C. Pannecouque, P. Zhan, and X. Liu. European Journal of Medicinal Chemistry, 2015. 93: p. 330-337. PMID[25707013].
    <br />
    <b>[PubMed]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25614104">Herb-drug Interaction between an anti-HIV Chinese Herbal SH Formula and Atazanavir in Vitro and in Vivo.</a> Cheng, B.H., X. Zhou, Y. Wang, J.Y. Chan, H.Q. Lin, P.M. Or, D.C. Wan, P.C. Leung, K.P. Fung, Y.F. Wang, and C.B. Lau. Journal of Ethnopharmacology, 2015. 162: p. 369-376. PMID[25614104].
    <br />
    <b>[PubMed]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25682562">Design, Synthesis of New beta-Carboline Derivatives and Their Selective anti-HIV-2 Activity.</a> Ashok, P., S. Chander, J. Balzarini, C. Pannecouque, and S. Murugesan. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(6): p. 1232-1235. PMID[25682562].
    <br />
    <b>[PubMed]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25701249">Molecular Docking and Antiviral Activity of N-Substituted Benzyl/Phenyl-2-(3,4-dimethyl-5,5-dioxidopyrazolo[4,3-c][1,2]benzothiazin-2(4H)-yl)acetamides.</a> Ahmad, M., S. Aslam, S.U. Rizvi, M. Muddassar, U.A. Ashfaq, C. Montero, O. Ollinger, M. Detorio, J.M. Gardiner, and R.F. Schinazi. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(6): p. 1348-1351. PMID[25701249].
    <br />
    <b>[PubMed]</b>. HIV_0313-032615.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000349167402202">Bicyclic 4-Amino-1-hydroxy-2-oxo-1,8-naphthyridine-containing Compounds Having High Potency against Raltegravir-resistant Integrase Mutants of HIV-1.</a> Zhao, X.Z., S.J. Smith, M. Metifiot, C. Marchand, Y. Pommier, S.H. Hughes, and T.R. Burke. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #402. ISI[000349167402202].
    <br />
    <b>[WOS]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000349573800034">Function-oriented Development of CXCR4 Antagonists as Selective Human Immunodeficiency Virus (HIV)-1 Entry Inhibitors.</a>Wu, C.H., C.J. Wang, C.P. Chang, Y.C. Cheng, J.S. Song, J.J. Jan, M.C. Chou, Y.Y. Ke, J. Ma, Y.C. Wong, T.C. Hsieh, Y.C. Tien, E.A. Gullen, C.F. Lo, C.Y. Cheng, Y.W. Liu, A.A. Sadani, C.H. Tsai, H.P. Hsieh, L.K. Tsou, and K.S. Shia. Journal of Medicinal Chemistry, 2015. 58(3): p. 1452-1465. ISI[000349573800034].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350361400007">Comparison of Antiviral Activity of lambda-Interferons against HIV Replication in Macrophages.</a> Wang, Y., J. Li, X. Wang, Y. Zhou, T. Zhang, and W. Ho. Journal of Interferon and Cytokine Research, 2015. 35(3): p. 213-221. ISI[000350361400007].
    <br />
    <b>[WOS]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000348457602351">Design of Non-planar HIV Integrase Inhibitors.</a> E.J. Velthuisen. Abstracts of Papers of the American Chemical Society, 2014. 247: Poster #186. ISI[000348457602351].
    <br />
    <b>[WOS]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000349669300028">Synthesis, Binding Affinity and Structure-activity Relationships of Novel, Selective and Dual Targeting CCR2 and CCR5 Receptor Antagonists.</a> Junker, A., A.K. Kokornaczyk, A.J. Zweemer, B. Frehland, D. Scehpmann, J. Yamaguchi, K. Itami, A. Faust, S. Hermann, S. Wagner, M. Schaefers, M. Koch, C. Weiss, L.H. Heitman, K. Kopka, and B. Wunsch. Organic &amp; Biomolecular Chemistry, 2015. 13(8): p. 2407-2422. ISI[000349669300028].
    <br />
    <b>[WOS]</b>. HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000349165104621">Identification of Novel Small Molecule CXCR4 Inhibitors as anti-HIV Agents.</a> Das, D., K. Maeda, Y. Hayahsi, N. Gavande, D.V. Desai, S.B. Chang, A.K. Ghosh, and H. Mitsuya. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #406. ISI[000349165104621].
    <br />
    <b>[WOS].</b> HIV_0313-032615.
    <br />
    <br /></p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000348457602496">Toward the Synthesis of Novel Boronates as Potential HIV-1 Protease Inhibitors.</a> Contreras, E.M., K. Sigurjonsson, M.D. Frank, N. Treich, and L. Fabry-Asztalos. Abstracts of Papers of the American Chemical Society, 2014. 247: Poster #344. ISI[000348457602496].
    <br />
    <b>[WOS]</b>. HIV_0313-032615.</p>

    <h2>Special Search Request Citations</h2>

    <p class="plaintext">16. <a href="http://www.croiconference.org/sites/default/files/posters-2015/417.pdf">TLR7 Agonist GS-9620 Activates HIV-1 in PBMCs from HIV-infected Patients on cART.</a> Sloan, D., A. Irrinki, A. Tsai, J. Kaur, J. Lalezeari, J. Murray, and T. Cihlar. Conference on Retroviruses and Opportunistic Infections. 2015. 22: Poster #417.</p>

    <p class="plaintext"><b>[Special Search]</b>. HIV_0313-032615.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
