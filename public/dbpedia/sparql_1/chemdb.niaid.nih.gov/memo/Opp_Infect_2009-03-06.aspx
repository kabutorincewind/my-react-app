

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-03-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="p0N13BIkNpq/i01earBDAZf41SXn4MhzSEEY0tCnXbxgwD01IH4kxRaqKSSBcNWa7GD8YU7GremADs1poEJFoV1hsxLM8fih+h16Iz6VFsKXKMxx4+nCrE5QBl8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8A3CC7AE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">February 18, 2009 - March 3, 2009</p>

    <h2 class="memofmt2-2">TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19253321?ordinalpos=15&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Cerulenin Analogues as Inhibitors of Efflux Pumps in Drug-resistant Candida albicans.</a></p>

    <p class="NoSpacing">Diwischek F, Morschhäuser J, Holzgrabe U.</p>

    <p class="NoSpacing">Arch Pharm (Weinheim). 2009 Feb 27. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19253321 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19249243?ordinalpos=44&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Effect of pyrazinamidase activity on pyrazinamide resistance in Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Sheen P, Ferrer P, Gilman RH, López-Llano J, Fuentes P, Valencia E, Zimic MJ.</p>

    <p class="NoSpacing">Tuberculosis (Edinb). 2009 Feb 25. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19249243 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19246185?ordinalpos=59&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Protein kinases as antibacterial targets.</a></p>

    <p class="NoSpacing">Schreiber M, Res I, Matter A.</p>

    <p class="NoSpacing">Curr Opin Cell Biol. 2009 Feb 24. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19246185 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19246010?ordinalpos=68&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Fumagillin and fumarranol interact with P. falciparum methionine aminopeptidase 2 and inhibit malaria parasite growth in vitro and in vivo.</a></p>

    <p class="NoSpacing">Chen X, Xie S, Bhat S, Kumar N, Shapiro TA, Liu JO.</p>

    <p class="NoSpacing">Chem Biol. 2009 Feb 27;16(2):193-202.</p>

    <p class="NoSpacing">PMID: 19246010 [PubMed - in process]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19233899?ordinalpos=128&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Human Mitochondrial Thymidine Kinase (TK-2) is Selectively Inhibited by 3&#39;-Thiourea Derivatives of {beta}-Thymidine. Identification of residues crucial for both inhibition and catalytic activity.</a></p>

    <p class="NoSpacing">Balzarini J, Van Daele I, Negri A, Solaroli N, Karlsson A, Liekens S, Gago F, Van Calenbergh S.</p>

    <p class="NoSpacing">Mol Pharmacol. 2009 Feb 20. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19233899 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19226140?ordinalpos=155&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">4-Benzyloxy-gamma-Sultone Derivatives: Discovery of a Novel Family of Non-Nucleoside Inhibitors of Human Cytomegalovirus and Varicella Zoster Virus.</a></p>

    <p class="NoSpacing">De Castro S, Garci&#769;a-Aparicio C, Andrei G, Snoeck R, Balzarini J, Camarasa MJ, Vela&#769;zquez S.</p>

    <p class="NoSpacing">J Med Chem. 2009 Feb 18. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19226140 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19208475?ordinalpos=176&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in vitro activity of dicationic bis-benzimidazoles as a new class of anti-MRSA and anti-VRE agents.</a></p>

    <p class="NoSpacing">Hu L, Kully ML, Boykin DW, Abood N.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Mar 1;19(5):1292-5. Epub 2009 Feb 7.</p>

    <p class="NoSpacing">PMID: 19208475 [PubMed - in process]</p> 

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19070992?ordinalpos=244&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel N-aryl- and N-heteryl phenazine-1-carboxamides as potential agents for the treatment of infections sustained by drug-resistant and multidrug-resistant Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">De Logu A, Palchykovska LH, Kostina VH, Sanna A, Meleddu R, Chisu L, Alexeeva IV, Shved AD.</p>

    <p class="NoSpacing">Int J Antimicrob Agents. 2009 Mar;33(3):223-9. Epub 2008 Dec 13.</p>

    <p class="NoSpacing">PMID: 19070992 [PubMed - in process]</p>  

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18718694?ordinalpos=300&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in vitro antimicrobial activity of some novel substituted benzimidazole derivatives having potent activity against MRSA.</a></p>

    <p class="NoSpacing">Tunçbilek M, Kiper T, Altanlar N.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Mar;44(3):1024-33. Epub 2008 Jul 4.</p>

    <p class="NoSpacing">PMID: 18718694 [PubMed - in process]</p> 

    <h2 class="memofmt2-2">Antimicrobials, Antifungals (General)</h2>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18614258?ordinalpos=109&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antimicrobial studies of some novel quinazolinones fused with [1,2,4]-triazole, [1,2,4]-triazine and [1,2,4,5]-tetrazine rings.</a></p>

    <p class="NoSpacing">Pandey SK, Singh A, Singh A, Nizamuddin.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Mar;44(3):1188-97. Epub 2008 Jun 3.</p>

    <p class="NoSpacing">PMID: 18614258 [PubMed - in process]</p>   

    <p class="memofmt2-1">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">11. Maia, Pedro I. da S., et al.</p>

    <p class="NoSpacing">Vanadium complexes with thiosemicarbazones: Synthesis, characterization, crystal structures and anti-Mycobacterium  tuberculosis activity</p>

    <p class="NoSpacing">POLYHEDRON  2009 (Feb 3) 28: 398 - 406</p> 

    <p class="ListParagraph">12. de Souza, M.V.N., et al.</p>

    <p class="NoSpacing">Synthesis and in vitro antitubercular activity of a series of quinoline derivatives</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY  2009 (Febr. 15)  17: 1474 - 1480</p> 

    <p class="ListParagraph">13. Khoshneviszadeh, M., et al.</p>

    <p class="NoSpacing"> Synthesis and biological evaluation of some new 1,4-dihydropyridines containing different ester substitute and diethyl carbamoyl group as anti-tubercular agents</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY  2009 ( Febr. 15) 17: 1579 - 1586</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
