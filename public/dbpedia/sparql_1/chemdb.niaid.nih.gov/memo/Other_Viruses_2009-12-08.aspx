

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-12-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="P6eXXP3RqvZArzBAzRp6qY4DAfebxpiD6gUQnQJDWtpRUpjgUSmlfx3cn1L9WweySz1aOmjKG8+oMbE3Qft1FuQ4vAR2RcbIF2T7mWJqalIyh8ZTTIyt4HnleM0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9A72F5CD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1"><span style='font-size:18.0pt;font-family:"Copperplate Gothic Bold"'>Opportunistic Infections  Citation List: </span></p>

    <p class="memofmt2-h1">November 25, 2009 -December 8, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19956582?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">4-Bromophenacyl Bromide Specifically Inhibits Rhoptry Secretion during Toxoplasma Invasion.</a></p>

    <p class="NoSpacing">Ravindran S, Lodoen MB, Verhelst SH, Bogyo M, Boothroyd JC.</p>

    <p class="NoSpacing">PLoS One. 2009 Dec 2;4(12):e8143.PMID: 19956582 [PubMed - in process]</p>  

    <p class="memofmt2-1">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19963383?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">Fluconazole analogues containing 2H-1,4-benzothiazin-3(4H)-one or 2H-1,4-benzoxazin-3(4H)-one moieties, a novel class of anti-Candida agents.</a></p>

    <p class="NoSpacing">Borate HB, Maujan SR, Sawargave SP, Chandavarkar MA, Vaiude SR, Joshi VA, Wakharkar RD, Iyer R, Kelkar RG, Chavan SP, Kunte SS.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Dec 4. [Epub ahead of print]PMID: 19963383 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19957992?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=12">High Potency and Broad-Spectrum Antimicrobial Peptides Synthesized via Ring-Opening Polymerization of alpha-Aminoacid-N-carboxyanhydrides.</a></p>

    <p class="NoSpacing">Zhou C, Qi X, Li P, Chen WN, Mouad L, Chang MW, Leong SS, Chan-Park MB.</p>

    <p class="NoSpacing">Biomacromolecules. 2009 Dec 3. [Epub ahead of print]PMID: 19957992 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19962895?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">Novel neofusapyrones isolated from Verticillium dahliae as potent antifungal substances.</a></p>

    <p class="NoSpacing">Honma M, Kudo S, Takada N, Tanaka K, Miura T, Hashimoto M.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Dec 3. [Epub ahead of print]PMID: 19962895 [PubMed - as supplied by publisher]</p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19651462?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=77">Synthesis and antimicrobial activity of some new N-acyl substituted phenothiazines.</a></p>

    <p class="NoSpacing">Bansode TN, Shelke JV, Dongre VG.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Dec;44(12):5094-8. Epub 2009 Jul 16.PMID: 19651462 [PubMed - in process]</p> 

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19968290?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=28">Synthesis and Structure-activity Relationships of Antitubercular 2-Nitroimidazooxazines Bearing Heterocyclic Side Chains.</a></p>

    <p class="NoSpacing">Sutherland, H.S., A. Blaser, I. Kmentova, S.G. Franzblau, B. Wan, Y. Wang, Z. Ma, B.D. Palmer, W.A. Denny, and A.M. Thompson</p>

    <p class="NoSpacing">J Med Chem, 2009; PMID[19968290]</p>

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19929750?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=168">5-Nitrothiazolylthiosemicarbazones: Synthesis and antimycobacterial evaluation against tubercular and non-tubercular mycobacterial species.</a></p>

    <p class="NoSpacing">Sriram, D., P. Yogeeswari, P. Senthilkumar, and D. Sangaraju</p>

    <p class="NoSpacing">J Enzyme Inhib Med Chem, 2009; PMID[19929750]</p>

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19928920?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=172">Synthesis and Structure-Activity Studies of Biphenyl Analogues of the Tuberculosis Drug (6S)-2-Nitro-6-{[4-(trifluoromethoxy)benzyl]oxy}-6,7-dihydro-5H-imidazo[2,1-b][1,3]oxazine (PA-824).</a></p>

    <p class="NoSpacing">Palmer, B.D., A.M. Thompson, H.S. Sutherland, A. Blaser, I. Kmentova, S.G. Franzblau, B. Wan, Y. Wang, Z. Ma, and W.A. Denny</p>

    <p class="NoSpacing">J Med Chem, 2009; PMID[19928920]</p>

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19954909?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=88">[R207910 (TMC207): A new antibiotic for the treatment of tuberculosis.].</a></p>

    <p class="NoSpacing">Lounis, N., J. Guillemont, N. Veziris, A. Koul, V. Jarlier, and K. Andries</p>

    <p class="NoSpacing">Med Mal Infect, 2009; PMID[19954909]</p> 

    <p class="ListParagraph">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19696152?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=357">Dastidar, Activity of the phenothiazine methdilazine alone or in combination with isoniazid or streptomycin against Mycobacterium tuberculosis in mice.</a></p>

    <p class="NoSpacing">Dutta, N.K., K. Mazumdar, A. DasGupta, and S.G.</p>

    <p class="NoSpacing">J Med Microbiol, 2009. 58(Pt 12): p. 1667-8; PMID[19696152]</p>

    <p class="NoSpacing">&nbsp;</p> 

    <p class="memofmt2-h1">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">11. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000271724200005&amp;SID=2D79KNp9LC6IDh857eo&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Fragment-based discovery of selective inhibitors of the Mycobacterium tuberculosis protein tyrosine phosphatase PtpA.</a></p>

    <p class="NoSpacing">Rawls, K.A., P.T. Lang, J. Takeuchi, S. Imamura, T.D. Baguley, C. Grundner, T. Alber, and J.A. Ellman</p>

    <p class="NoSpacing">Bioorganic &amp; Medicinal Chemistry Letters, 2009. 19(24): p. 6851-6854; PMID[ISI:000271724200005]</p>

    <p class="ListParagraph">12. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000271619300004&amp;SID=2D79KNp9LC6IDh857eo&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Novel Pthalazinyl Derivatives: Synthesis, Antimycobacterial Activities, and Inhibition of Mycobacterium Tuberculosis Isocitrate Lyase Enzyme.</a></p>

    <p class="NoSpacing">Sriram, D., P. Yogeeswari, P. Senthilkumar, S. Dewakar, N. Rohit, B. Debjani, P. Bhat, B. Veugopal, V.V.S. Pavan, and H.M. Thimmappa</p>

    <p class="NoSpacing">Medicinal Chemistry, 2009. 5(5): p.422-433; PMID[ISI:000271619300004]</p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
