

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-03-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WAW4Y7s/ZuGwqtxx9xp9fgMlmhdoJ7mBqVX2LfKTlJ04O2gGN2JoBX3cIBOmppVLeHZnW4+i+54LPvNOtDKZwLR+Xv3uBsW0u3jexH5dENXiU0guriTh4OyBLRs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3E017DC6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">March 4 - March 17, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21402837">Host defense peptides of human thrombin: structure-activity studies and therapeutic potential.</a> Kasetty, G., P. Papareddy, M. Kalle, V. Rydengard, M. Morgelin, B. Albiger, M. Malmsten, and A. Schmidtchen. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21402837].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21402139">Antimicrobial properties and phenolic contents of medicinal plants used by the Venda people for conditions related to venereal diseases.</a> Mulaudzi, R.B., A.R. Ndhlala, M.G. Kulkarni, J.F. Finnie, and J. Van Staden. Journal of Ethnopharmacology, 2011. <b>[Epub ahead of print]</b>; PMID[21402139].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21401748">Prunus Mume Extract Exhibits Antimicrobial Activity Against Pathogenic Oral Bacteria.</a> Seneviratne, C.J., R.W. Wong, U. Hagg, Y. Chen, T.D. Herath, P. Lakshman Samaranayake, and R. Kao. International Journal of Paediatric Dentistry 2011. Epub ahead of print; PMID[21401748].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21391841">Cytotoxicity, antiviral and antimicrobial activities of alkaloids, flavonoids, and phenolic acids.</a> Ozcelik, B., M. Kartal, and I. Orhan. Pharmaceutical Biology, 2011. <b>[Epub ahead of print]</b>; PMID[21391841].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21384803">Asymmetric Chemoenzymatic Synthesis of Miconazole and Econazole Enantiomers. The Importance of Chirality in Their Biological Evaluation.</a> Mangas-Sanchez, J., E. Busto, V. Gotor-Fernandez, F. Malpartida, and V. Gotor. The Journal of Organic Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21384803].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21370476">Design, Synthesis, and in vitro Antifungal Activity of 1-[(4-Substituted-benzyl)methylamino]-2-(2,4-difluorophenyl)-3-(1H-1,2,4-t riazol-1-yl)propan-2-ols.</a> Guillon, R., F. Pagniez, F. Giraud, D. Crepin, C. Picot, M. Le Borgne, F. Morio, M. Duflos, C. Loge, and P. Le Pape. ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21370476].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21322563">Curcumin nanoparticles: preparation, characterization, and antimicrobial study.</a> Bhawana, R.K. Basniwal, H.S. Buttar, V.K. Jain, and N. Jain. Journal of Agricultural and Food Chemistry, 2011. 59(5): p. 2056-2061; PMID[21322563].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21395486">Discovery of novel methanone derivatives acting as antimycobacterial agents.</a> Ali, M.A., S. Bastian, R. Ismail, T.S. Choon, S. Ali, A. Aubry, S. Pandian, P. Saraswat, A. Singh, and R. Gaur. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21395486].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21394664">Bioactive metabolites of Diaporthe sp. P133, an endophytic fungus isolated from Pandanus amaryllifolius.</a> Bungihan, M.E., M.A. Tan, M. Kitajima, N. Kogure, S.G. Franzblau, T.E. Dela Cruz, H. Takayama, and M.G. Nonato. Journal of Natural Medicines, 2011. <b>[Epub ahead of print]</b>; PMID[21394664].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21334900">Synthesis and biological evaluation of benzimidazole-5-carbohydrazide derivatives as antimalarial, cytotoxic and antitubercular agents.</a> Camacho, J., A. Barazarte, N. Gamboa, J. Rodrigues, R. Rojas, A. Vaisberg, R. Gilman, and J. Charris. Bioorganic &amp; Medicinal Chemistry, 2011. 19(6): p. 2023-2029; PMID[21334900].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21376591">Novel thiolactone-isatin hybrids as potential antimalarial and antitubercular agents.</a> Hans, R.H., I.J. Wiid, P.D. van Helden, B. Wan, S.G. Franzblau, J. Gut, P.J. Rosenthal, and K. Chibale. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(7): p. 2055-2058; PMID[21376591].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21394476">New S-benzylisothiosemicarbazones with antimycobacterial activity.</a> Petrlikova, E., K. Waisser, L. Heinisch, and J. Stolarikova. Folia Microbiologica, 2011. <b>[Epub ahead of print]</b>; PMID[21394476].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21381755">Aptamer-mediated Inhibition of Mycobacterium tuberculosis Polyphosphate Kinase 2.</a> Shum, K.T., E.L. Lui, S.C. Wong, P. Yeung, L. Sam, Y. Wang, R.M. Watt, and J.A. Tanner. Biochemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21381755].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21382653">Synthesis, antibacterial and antimycobacterial activities of some new 4-aryl/heteroaryl-2,6-dimethyl-3,5-bis-N-(aryl)-carbamoyl-1,4-dihydropyrid ines.</a> Sirisha, K., D. Bikshapathi, G. Achaiah, and V.M. Reddy. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21382653].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21384024">Inhibitors of an essential mycobacterial cell wall lipase (Rv3802c) as tuberculosis drug leads.</a> West, N.P., K.M. Cergol, M. Xue, E.J. Randall, W.J. Britton, and R.J. Payne. Chemical Communications, 2011. <b>[Epub ahead of print]</b>; PMID[21384024].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-3">ANTIPARASITIC COMPOUNDS (includes Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18406409">In vitro evaluations of antimalarial drugs and their relevance to clinical outcomes.</a> Ekland, E.H. and D.A. Fidock. International Journal of Parasitology, 2008. 38(7): p. 743-747; PMID[18406409].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20924699">Sensitivity to artemisinin, mefloquine and quinine of Plasmodium falciparum in northwestern Thailand.</a> Huttinger, F., W. Satimai, G. Wernsdorfer, U. Wiedermann, K. Congpuong, and W.H. Wernsdorfer. Wiener Klinische Wochenschrift, 2010. 122 Suppl 3: p. 52-56; PMID[20924699].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20924698">Synergism between mefloquine and artemisinin and its enhancement by retinol in Plasmodium falciparum in vitro.</a> Kerschbaumer, G., G. Wernsdorfer, U. Wiedermann, K. Congpuong, J. Sirichaisinthop, and W.H. Wernsdorfer. Wiener Klinische Wochenschrift, 2010. 122 Suppl 3: p. 57-60; PMID[20924698].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18226262">In vitro atovaquone/proguanil susceptibility and characterization of the cytochrome b gene of Plasmodium falciparum from different endemic regions of Thailand.</a> Khositnithikul, R., P. Tan-Ariya, and M. Mungthin. Malaria Journal, 2008. 7: p. 23; PMID[18226262].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20194689">Decreased in vitro susceptibility of Plasmodium falciparum isolates to artesunate, mefloquine, chloroquine, and quinine in Cambodia from 2001 to 2007.</a> Lim, P., C. Wongsrichanalai, P. Chim, N. Khim, S. Kim, S. Chy, R. Sem, S. Nhem, P. Yi, S. Duong, D.M. Bouth, B. Genton, H.P. Beck, J.G. Gobert, W.O. Rogers, J.Y. Coppee, T. Fandeur, O. Mercereau-Puijalon, P. Ringwald, J. Le Bras, and F. Ariey. Antimicrobial Agents and Chemotherapy, 2010. 54(5): p. 2135-2142; PMID[20194689].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20065051">In vitro sensitivities of Plasmodium falciparum to different antimalarial drugs in Uganda.</a> Nsobya, S.L., M. Kiggundu, S. Nanyunja, M. Joloba, B. Greenhouse, and P.J. Rosenthal. Antimicrobial Agents and Chemotherapy, 2010. 54(3): p. 1200-1206; PMID[20065051].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20876370">In vitro activity of pyronaridine against multidrug-resistant Plasmodium falciparum and Plasmodium vivax.</a> Price, R.N., J. Marfurt, F. Chalfein, E. Kenangalem, K.A. Piera, E. Tjitra, N.M. Anstey, and B. Russell. Antimicrobial Agents and Chemotherapy, 2010. 54(12): p. 5146-5150; PMID[20876370].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18426617">Studies on antimalarial drug susceptibility in Colombia, in relation to Pfmdr1 and Pfcrt.</a> Restrepo-Pineda, E., E. Arango, A. Maestre, V.E. Do Rosario, and P. Cravo. Parasitology, 2008. 135(5): p. 547-553; PMID[18426617].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20070627">In vitro sensitivity of Plasmodium falciparum to conventional and novel antimalarial drugs in Papua New Guinea.</a> Wong, R.P., D. Lautu, L. Tavul, S.L. Hackett, P. Siba, H.A. Karunajeewa, K.F. Ilett, I. Mueller, and T.M. Davis. Tropical Medicine &amp; International Health, 2010. 15(3): p. 342-349; PMID[20070627].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21326960">In vitro susceptibility of Plasmodium falciparum to monodesethylamodiaquine, quinine, mefloquine and halofantrine in Abidjan (Cote d&#39;Ivoire).</a> Yavo, W., K. Bla, A. Djaman, S. Assi, L. Basco, A. Mazabraud, and M. Kone. African Health Sciences, 2010. 10(2): p. 111-116; PMID[21326960].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0302-031611.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287570600009">Screening for Antifungal Activities of Some Medicinal Plants used Traditionally in Saudi Arabia.</a> Aly, M.M. and S.O. Bafeel. Journal of Applied Animal Research, 2010. 38(1): p. 39-44; ISI[000287570600009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286905400013">Novel isatinyl thiosemicarbazones derivatives as potential molecule to combat HIV-TB co-infection.</a> Banerjee, D., P. Yogeeswari, P. Bhat, A. Thomas, M. Srividya, and D. Sriram. European Journal of Medicinal Chemistry, 2011. 46(1): p. 106-121; ISI[000286905400013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287722400017">Antimicrobial and Anti-inflammatory Properties of Various Seaweeds from the Gulf of Thailand.</a> Boonchum, W., Y. Peerapornpisal, D. Kanjanapothi, J. Pekkoh, D. Amornlerdpison, C. Pumas, P. Sangpaiboon, and P. Vacharapiyasophon. International Journal of Agriculture and Biology, 2011. 13(1): p. 100-104; ISI[000287722400017].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287349400033">Antifungal and antibacterial activities of lectin from the seeds of Archidendron jiringa Nielsen.</a> Charungchitrak, S., A. Petsom, P. Sangvanich, and A. Karnchanatat. Food Chemistry, 2011. 126(3): p. 1025-1032; ISI[000287349400033].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285847500020">Antimicrobial activity of the ethanol extracts of some plants natural growing in Aydin, Turkey.</a> Coban, E.P. and H. Biyik. African Journal of Microbiology Research, 2010. 4(21): p. 2318-2323; ISI[000285847500020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287499500010">Synthesis, characterization, and biological activity of complexes derived from E-N &#39;-(3,4,5-trimethoxybenzylidene)benzofuran-2-carbohydrazide and ortho-phenylenediamine/2,6-diaminopyridine.</a> Halli, M.B., V.B. Patil, M. Kinni, and R.B. Sumathi. Journal of Coordination Chemistry, 2011. 64(4): p. 651-662; ISI[000287499500010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285506500022">Antimicrobial potential of some plant extracts against Candida species.</a> Hofling, J.F., P.C. Anibal, G.A. Obando-Pereda, I.A.T. Peixoto, V.F. Furletti, M.A. Foglio, and R.B. Goncalves. Brazilian Journal of Biology, 2010. 70(4): p. 1065-1068; ISI[000285506500022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287175900003">Testing of Glyceryl Monoesters for Their Anti-Microbial Susceptibility and Their Influence in Emulsions.</a> Hung, L.C., R. Ismail, M. Basri, H.L.L. Nang, B.A. Tejo, H. Abu Hassan, and C.Y. May. Journal of Oil Palm Research, 2010. 22: p. 846-855; ISI[000287175900003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286040100100">Antimicrobial activity of crude ethanolic extract from Eleutherine americana.</a> Ifesan, B.O.T., D. Ibrahim, and S.P. Voravuthikunchai. Journal of Food Agriculture &amp; Environment, 2010. 8(3-4): p. 1233-1236; ISI[000286040100100].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285650600013">Leaves Antimicrobial Activity of Glycyrrhiza glabra L.</a> Irani, M., M. Sarmadi, F. Bernard, G.H.E. Pour, and H.S. Bazarnov. Iranian Journal of Pharmaceutical Research, 2010. 9(4): p. 425-428; ISI[000285650600013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285724900066">Antimicrobial Screening of Some Medicinal Plants of Pakistan.</a> Jahan, N., M. Ahmad, Mehjabeen, M. Zia-ul-Haq, S.M. Alam, and M. Qureshi. Pakistan Journal of Botany, 2010. 42(6): p. 4281-4284; ISI[000285724900066].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287044200006">Synthesis, spectral analysis and in vitro microbiological evaluation of novel ethyl 4-(naphthalen-2-yl)-2-oxo-6-arylcyclohex-3-enecarboxylates and 4,5-dihydro-6-(napthalen-2-yl)-4-aryl-2H-indazol-3-ols.</a>  Kanagarajan, V., J. Thanusu, and M. Gopalakrishnan. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. 26(1): p. 56-66; ISI[000287044200006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287313700014">Antibacterial, antifungal, antispasmodic and Ca++ antagonist effects of Caesalpinia bonducella.</a> Khan, H.U., I. Ali, A.U. Khan, R. Naz, and A.H. Gilani. Natural Product Research, 2011. 25(4): p. 444-449; ISI[000287313700014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287613800031">Antimicrobial Activity of the Essential Oil of Greek Endemic Stachys spruneri and its Main Component, Isoabienol.</a> Koutsaviti, A., M. Milenkovic, and O. Tzakou. Natural Product Communications, 2011. 6(2): p. 277-280; ISI[000287613800031].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286847000032">Antifungal activities of essential oils applied by dip-treatment on areca palm (Areca catechu) leaf sheath and persistence of their potency upon storage.</a>  Matan, N. and W. Saengkrajang. International Biodeterioration &amp; Biodegradation, 2011. 65(1): p. 212-216; ISI[000286847000032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285724900043">Biological Screening of Zizyphus Oxyphylla Edgew Leaves.</a> Nisar, M., W.A. Kaleem, M. Qayum, A. Hussain, M. Zia-ul-Haq, I. Ali, and M.I. Choudhary. Pakistan Journal of Botany, 2010. 42(6): p. 4063-4069; ISI[000285724900043].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287722500012">Antimicrobial Activity of Clerodendrum viscosum (Verbenaceae).</a> Oly, W.T., W. Islam, P. Hassan, and S. Parween. International Journal of Agriculture and Biology, 2011. 13(2): p. 222-226; ISI[000287722500012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285724100075">Determination of Antifungal Activity of Cedrus Deodara Root Oil and its Compounds Against Candida Albicans and Aspergillus Fumigatus.</a> Parveen, R., M.A. Azmi, R.M. Tariq, S.M. Mahmood, M. Hijazi, S. Mahmud, and S.N.H. Naqvi. Pakistan Journal of Botany, 2010. 42(5): p. 3645-3649; ISI[000285724100075].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286953100025">Synthesis, characterization, in vitro antimicrobial and DNA cleavage studies of Co(II), Ni(II) and Cu(II) complexes with ONOO donor coumarin Schiff bases.</a> Patil, S.A., S.N. Unki, A.D. Kulkarni, V.H. Naik, and P.S. Badami. Journal of Molecular Structure, 2011. 985(2-3): p. 330-338; ISI[000286953100025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286632300008">A new group of potential antituberculotics: N-(2-pyridylmethyl)salicylamides and N-(3-pyridylmethyl)salicylamides.</a> Petrlikova, E., K. Waisser, K. Palat, J. Kunes, and J. Kaustova. Chemical Papers, 2011. 65(1): p. 52-59; ISI[000286632300008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">46. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287779300010">Synthesis and antimicrobial activity of bis- 2-imino-3- 5-(3-methylbenzo b furan-7-yl)-1,3,4-thiadiazol-2-yl -5- (arylidene)-1,3-thiazolan-4-one methanes.</a> Reddy, C.S., D.C. Rao, V. Yakub, and A. Nagaraj. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2011. 50(2): p. 253-259; ISI[000287779300010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">47. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286471600014">Antifungal activity of Arctic and Antarctic bacteria isolates.</a> Shekh, R.M., P. Singh, S.M. Singh, and U. Roy. Polar Biology, 2011. 34(1): p. 139-143; ISI[000286471600014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">48. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287457200003">The antifungal effect of peptides from hymenoptera venom and their analogs.</a> Slaninova, J., H. Putnova, L. Borovickova, P. Sacha, V. Cerovsky, L. Monincova, and V. Fucik. Central European Journal of Biology, 2011. 6(2): p. 150-159; ISI[000287457200003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>

    <p> </p>

    <p class="NoSpacing">49. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287350800019">Flavonoids of Helichrysum chasmolycicum and its antioxidant and antimicrobial activities.</a> Suzgec-Selcuk, S. and A.S. Birteksoz. South African Journal of Botany, 2011. 77(1): p. 170-174; ISI[000287350800019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0302-031611.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
