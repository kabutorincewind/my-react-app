

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-01-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+tylUrwRb6zCkYpDQsah9exJ0TpZTcp0RhNDqXpOxUuLs+RAbCqP/Ip6gaEuXurNYlKFaRdp1LWgJHt4wkG9fniXznFg4UnLEywTt+IG5eaAdwdhkCQ3UuRPqzY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="10F92B4C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 18 - January 31, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23330927">Polyphenols: A Diverse Class of Multi-target anti-HIV-1 Agents.</a> Andrae-Marobela, K., F.W. Ghislain, H. Okatch, and R.R. Majinda. Current Drug Metabolism, 2013. <b>[Epub ahead of print]</b>. PMID[23330927].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23338523">High-throughput Virtual Screening of Phloroglucinol Derivatives against HIV-Reverse Transcriptase.</a> Belekar, V., A. Shah, and P. Garg. Molecular Diversity, 2013. <b>[Epub ahead of print]</b>. PMID[23338523].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23360764">Identification of the Potential Regions of Epap-1 That Interacts with V3 Loop of HIV-1 gp120.</a> Bhaskar, C., P.S. Reddy, K.S. Chandra, S. Sabde, D. Mitra, and A.K. Kondapi. Biochimica et Biophysica Acta, 2013. <b>[Epub ahead of print]</b>. PMID[23360764].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23324659">An Ancestral HIV-2/SIV Peptide with Potent HIV-1 and HIV-2 Fusion Inhibitor Activity.</a> Borrego, P., R. Calado, J.M. Marcelino, P. Pereira, A. Quintas, H. Barroso, and N. Taveira. AIDS, 2013. <b>[Epub ahead of print]</b>. PMID[23324659].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23084420">Contribution of SUMO-interacting Motifs and SUMOylation to the Antiretroviral Properties of TRIM5alpha.</a> Brandariz-Nunez, A., A. Roa, J.C. Valle-Casuso, N. Biris, D. Ivanov, and F. Diaz-Griffero. Virology, 2013. 435(2): p. 463-471. PMID[23084420].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23353120">Two New Derivatives of 2, 5-Dihydroxyphenylacetic acid from the Kernel of Entada phaseoloides.</a> Chen, L., Y. Zhang, G. Ding, M. Ba, Y. Guo, and Z. Zou. Molecules, 2013. 18(2): p. 1477-1482. PMID[23353120].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23343121">Recent Advances of Diaryl Ether Family as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Chen, X., S. Ding, P. Zhan, and X. Liu. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>. PMID[23343121].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23349893">HIV-1 Fusion Is Blocked through Binding of GB Virus C E2D Peptides to the HIV-1 gp41 Disulfide Loop.</a> Eissmann, K., S. Mueller, H. Sticht, S. Jung, P. Zou, S. Jiang, A. Gross, J. Eichler, B. Fleckenstein, and H. Reil. PloS One, 2013. 8(1): p. e54452. PMID[23349893].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23360470">PXR/CYP3A4-humanized Mice for Studying Drug-drug Interactions Involving Intestinal P-glycoprotein.</a> Holmstock, N., F.J. Gonzalez, M. Baes, P. Annaert, and P. Augustijns. Molecular Pharmaceutics, 2013. <b>[Epub ahead of print]</b>. PMID[23360470].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23273844">Interferon-inducible Cholesterol-25-hydroxylase Broadly Inhibits Viral Entry by Production of 25-Hydroxycholesterol.</a> Liu, S.Y., R. Aliyari, K. Chikere, G. Li, M.D. Marsden, J.K. Smith, O. Pernet, H. Guo, R. Nusbaum, J.A. Zack, A.N. Freiberg, L. Su, B. Lee, and G. Cheng. Immunity, 2013. 38(1): p. 92-105. PMID[23273844].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23327639">Design, Synthesis, Antimicrobial Activity and anti-HIV Activity Evaluation of Novel Hybrid Quinazoline-triazine Derivatives.</a> Modh, R.P., E. De Clercq, C. Pannecouque, and K.H. Chikhalia. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23327639].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23352239">Benzene, Coumarin and Quinolinone Derivatives from Roots of Citrus hystrix.</a> Panthong, K., Y. Srisud, V. Rukachaisirikul, N. Hutadilok-Towatana, S.P. Voravuthikunchai, and S. Tewtrakul. Phytochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23352239].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23359817">HIPdb: A Database of Experimentally Validated HIV Inhibiting Peptides.</a> Qureshi, A., N. Thakur, and M. Kumar. PloS One, 2013. 8(1): p. e54908. PMID[23359817].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23201309">Selectivity of Kinases on the Activation of Tenofovir, an anti-HIV Agent.</a> Varga, A., E. Graczer, L. Chaloin, K. Liliom, P. Zavodszky, C. Lionne, and M. Vas. European Journal of Pharmaceutical Sciences, 2013. 48(1-2): p. 307-315. PMID[23201309].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23327759">Dibenzocyclooctadiene Lignans and Norlignans from Fruits of Schisandra wilsoniana.</a> Yang, G.Y., R.R. Wang, H.X. Mu, Y.K. Li, X.N. Li, L.M. Yang, Y.T. Zheng, W.L. Xiao, and H.D. Sun. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23327759].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23361947">Identification of a Small-molecule Inhibitor of HIV-1 Assembly That Targets the Phosphatidylinositol (4,5)-bisphosphate Binding Site of the HIV-1 Matrix Protein.</a> Zentner, I., L.J. Sierra, A.K. Fraser, L. Maciunas, M.K. Mankowski, A. Vinnik, P. Fedichev, R.G. Ptak, J. Martin-Garcia, and S. Cocklin. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23361947].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312227300008">HIV-1 X4 Activities of Polycationic &quot;Viologen&quot; Based Dendrimers by Interaction with the Chemokine Receptor CXCR4: Study of Structure-Activity Relationship.</a> Asaftei, S., D. Huskens, and D. Schols. Journal of Medicinal Chemistry, 2012. 55(23): p. 10405-10413. ISI[000312227300008].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312518100019">Development and Evaluation of a Thermosensitive Vaginal Gel Containing Raltegravir Plus Efavirenz Loaded Nanoparticles for HIV Prophylaxis.</a> Date, A.A., A. Shibata, M. Goede, B. Sanford, K. La Bruzzo, M. Belshan, and C.J. Destache. Antiviral Research, 2012. 96(3): p. 430-436. ISI[000312518100019].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312517800006">A Pyrophosphatase Activity Associated with Purified HIV-1 Particles.</a> Ducloux, C., M. Mougel, V. Goldschmidt, L. Didierlaurent, R. Marquet, and C. Isel. Biochimie, 2012. 94(12): p. 2498-2507. ISI[000312517800006].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312547200036">Structure-Activity Relationship Studies Using Peptide Arrays: The Example of HIV-1 Rev-integrase Interaction.</a> Gabizon, R., O. Faust, H. Benyamini, S. Nir, A. Loyter, and A. Friedler. MedChemComm, 2013. 4(1): p. 252-259. ISI[000312547200036].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310771100137">A New Flavonoid from Daphne feddei and its anti-HIV-1 Activity.</a> Hu, L., Q.F. Zhang, Y.Q. Ye, J. Zhong, Q. Hu, and Q.F. Hu. Asian Journal of Chemistry, 2012. 24(11): p. 5391-5392. ISI[000310771100137].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312567000032">Inhibition of HIV-1 Replication by RNA with a Microrna-like Function.</a> Kato, K., T. Senoki, and H. Takaku. International Journal of Molecular Medicine, 2013. 31(1): p. 252-258. ISI[000312567000032].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312889900055">The BET Bromodomain Inhibitor JQ1 Activates HIV Latency through Antagonizing Brd4 Inhibition of Tat-transactivation.</a> Li, Z.C., J. Guo, Y.T. Wu, and Q. Zhou. Nucleic Acids Research, 2013. 41(1): p. 277-287. ISI[000312889900055].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312830100005">Synthesis and anti-HIV Evaluation of Novel 1,2,4-Triazole Derivatives as Potential Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Li, Z.Y., Y. Cao, P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, and X.Y. Liu. Letters in Drug Design &amp; Discovery, 2013. 10(1): p. 27-34. ISI[000312830100005].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312684100005">Chemical Constituents of Sambucus L.</a> Ma, Y.M. and H. Wu. Chinese Journal of Organic Chemistry, 2012. 32(11): p. 2063-2072. ISI[000312684100005].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312621700047">Synthesis, Screening and Computational Investigation of Pentacycloundecane-peptoids as Potent CSA-HIV PR Inhibitors.</a> Makatini, M.M., K. Petzold, P.I. Arvidsson, B. Honarparvar, T. Govender, G.E.M. Maguire, R. Parboosing, Y. Sayed, M.E.S. Soliman, and H.G. Kruger. European Journal of Medicinal Chemistry, 2012. 57: p. 459-467. ISI[000312621700047].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312693300009">Pharmacophore Identification, Molecular Docking, Virtual Screening, and in Silico ADME Studies of Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Pirhadi, S. and J.B. Ghasemi. Molecular Informatics, 2012. 31(11-12): p. 856-866. ISI[000312693300009].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312621700013">Synthesis and Antiviral Evaluation of bis(POM) Prodrugs of (E)- [4 &#39;-Phosphono-but-2 &#39;-en-1 &#39;-yl] Purine Nucleosides.</a> Pradere, U., V. Roy, A. Montagu, O. Sari, M. Hamada, J. Balzarini, R. Snoeck, G. Andrei, and L.A. Agrofoglio. European Journal of Medicinal Chemistry, 2012. 57: p. 126-133. ISI[000312621700013].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312133800009">Functional Characterization of the Recombinant HIV-neutralizing Monoclonal Antibody 2F5 Produced in Maize Seeds.</a> Sabalza, M., L. Madeira, C. van Dolleweerd, J.K. Ma, T. Capell, and P. Christou. Plant Molecular Biology, 2012. 80(4-5): p. 477-488. ISI[000312133800009].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312620900030">Synthesis, Complexation and Biological Activity of New Isatin Schiff Bases.</a> Sallam, S.A., E.S.I. Ibrahim, and M.I. Anwar. Journal of the Chilean Chemical Society, 2012. 57(4): p. 1482-1491. ISI[000312620900030].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312607700014">The Antiviral Activities and Mechanisms of Marine Polysaccharides: An Overview.</a> Wang, W., S.X. Wang, and H.S. Guan. Marine Drugs, 2012. 10(12): p. 2795-2816. ISI[000312607700014].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312455500061">Inhibition of Reverse Transcriptase Activity Increases Stability of the HIV-1 Core.</a> Yang, Y., T. Fricke, and F. Diaz-Griffero. Journal of Virology, 2013. 87(1): p. 683-687. ISI[000312455500061].
    <br />
    <b>[WOS]</b>. HIV_0118-013113.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
