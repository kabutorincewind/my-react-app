

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-06-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TeIYak/+ssoFwSSU+6DQ/Tu2X3S3GHXApcLf4wVk/sNLb1nz+BdoZZttT75c/zfwWTFfs3cezND0cD9Q6RJ5IjnnFzVu/hjOEekhibUMi12ixfxfu83hyrF7j1M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8CDB66BB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: May 24 - June 6, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23176059">Microwave-assisted Combinatorial Synthesis of 2-Alkyl-2-(N-arylsulfonylindol- 3-yl)-3-N-acyl-5-aryl-1,3,4-oxadiazolines as anti-HIV-1 Agents.</a> Che, Z.P., N. Huang, X. Yu, L.M. Yang, J.Q. Ran, X. Zhi, H. Xu, and Y.T. Zheng. Combinatorial Chemistry &amp; High Throughput Screening, 2013. 16(5): p. 400-407. PMID[23176059].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23721378">Ledgins Inhibit Late Stage HIV-1 Replication by Modulating Integrase Multimerization in the Virions</a>. Desimmie, B.A., R. Schrijvers, J. Demeulemeester, D. Borrenberghs, C. Weydert, W. Thys, S. Vets, B. Van Remoortel, J. Hofkens, J. De Rijck, J. Hendrix, N. Bannert, R. Gijsbers, F. Christ, and Z. Debyser. Retrovirology, 2013. 10(1): p. 57. PMID[23721378].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23712429">Restricting HIV-1 Pathways for Escape Using Rationally Designed anti-HIV-1 Antibodies.</a> Diskin, R., F. Klein, J.A. Horwitz, A. Halper-Stromberg, D.N. Sather, P.M. Marcovecchio, T. Lee, A.P. West, Jr., H. Gao, M.S. Seaman, L. Stamatatos, M.C. Nussenzweig, and P.J. Bjorkman. The Journal of Experimental Medicine, 2013. <b>[Epub ahead of print]</b>. PMID[23712429].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23601710">Optimization of a 1,5-Dihydrobenzo[b][1,4]diazepine-2,4-dione Series of HIV Capsid Assembly Inhibitors 2: Structure-Activity Relationships (SAR) of the C3-phenyl Moiety.</a> Fader, L.D., S. Landry, S. Goulet, S. Morin, S.H. Kawai, Y. Bousquet, I. Dion, O. Hucke, N. Goudreau, C.T. Lemke, J. Rancourt, P. Bonneau, S. Titolo, M. Amad, M. Garneau, J. Duan, S. Mason, and B. Simoneau. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(11): p. 3401-3405. PMID[23601710].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23583513">Optimization of a 1,5-Dihydrobenzo[b][1,4]diazepine-2,4-dione Series of HIV Capsid Assembly Inhibitors 1: Addressing Configurational Instability through Scaffold Modification.</a> Fader, L.D., S. Landry, S. Morin, S.H. Kawai, Y. Bousquet, O. Hucke, N. Goudreau, C.T. Lemke, P. Bonneau, S. Titolo, S. Mason, and B. Simoneau. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(11): p. 3396-3400. PMID[23583513].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23724015">The Lantibiotic Peptide Labyrinthopeptin A1 Demonstrates Broad anti-HIV and anti-HSV Activity with Potential for Microbicidal Applications.</a> Ferir, G., M.I. Petrova, G. Andrei, D. Huskens, B. Hoorelbeke, R. Snoeck, J. Vanderleyden, J. Balzarini, S. Bartoschek, M. Bronstrup, R.D. Sussmuth, and D. Schols. PLoS One, 2013. 8(5): p. e64010. PMID[23724015].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23583286">Development of a Fluorescence-based HIV-1 Integrase DNA Binding Assay for Identification of Novel HIV-1 Integrase Inhibitors.</a> Han, Y.S., W.L. Xiao, P.K. Quashie, T. Mesplede, H. Xu, E. Deprez, O. Delelis, J.X. Pu, H.D. Sun, and M.A. Wainberg. Antiviral Research, 2013. 98(3): p. 441-448. PMID[23583286].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23405965">Indolizine Derivatives as HIV-1 VIF-elonginC Interaction Inhibitors.</a> Huang, W., T. Zuo, X. Luo, H. Jin, Z. Liu, Z. Yang, X. Yu, L. Zhang, and L. Zhang. Chemical Biology &amp; Drug Design, 2013. 81(6): p. 730-741. PMID[23405965].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23695256">A Novel Inhibitor-binding Site on the HIV-1 Capsid N-terminal Domain Leads to Improved Crystallization Via Compound-mediated Dimerization.</a> Lemke, C.T., S. Titolo, N. Goudreau, A.M. Faucher, S.W. Mason, and P. Bonneau. Acta Crystallographica. Section D, Biological Crystallography, 2013. 69(Pt 6): p. 1115-1123. PMID[23695256].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23711095">3-Hydroxyphthalic anhydride-modified Human Serum Albumin as a Microbicide Candidate against HIV-1 Entry by Targeting Both Viral Envelope Glycoprotein Gp120 and Cellular Receptor Cd4.</a> Li, M., J. Duan, J. Qiu, F. Yu, X. Che, S. Jiang, and L. Li. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print]</b>. PMID[23711095].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23703254">Structure and Bioactivity of Triterpenoids from the Stems of Schisandra sphenanthera.</a> Liang, C.Q., R.H. Luo, J.M. Yan, Y. Li, X.N. Li, Y.M. Shi, S.Z. Shang, Z.H. Gao, L.M. Yang, Y.T. Zheng, W.L. Xiao, H.B. Zhang, and H.D. Sun. Archives of pharmacal research, 2013. <b>[Epub ahead of print]</b>. PMID[23703254].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23545531">Identification and Characterization of a Novel HIV-1 Nucleotide-competing Reverse Transcriptase Inhibitor Series.</a> Rajotte, D., S. Tremblay, A. Pelletier, P. Salois, L. Bourgon, R. Coulombe, S. Mason, L. Lamorte, C.F. Sturino, and R. Bethell. Antimicrobial agents and chemotherapy, 2013. 57(6): p. 2712-2718. PMID[23545531].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23621690">Viral Infectivity Factor: A Novel Therapeutic Strategy to Block HIV-1 Replication.</a> Yang, G. and X. Xiong. Mini reviews in medicinal chemistry, 2013. 13(7): p. 1047-1055. PMID[23621690].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23623492">Synthesis and Evaluation of Janus Type Nucleosides as Potential HCV Ns5b Polymerase Inhibitors.</a> Zhou, L., F. Amblard, H. Zhang, T.R. McBrayer, M.A. Detorio, T. Whitaker, S.J. Coats, and R.F. Schinazi. Bioorganic &amp; medicinal chemistry letters, 2013. 23(11): p. 3385-3388. PMID[23623492].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0524-060613.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317340400002">Isolation of anti-HIV Components from Canarium album Fruits by High-speed Counter-current Chromatography.</a> Duan, W., S. Tan, J. Chen, S. Liu, S. Jiang, H. Xiang, and Y. Xie. Analytical Letters, 2013. 46(7): p. 1057-1068. ISI[000317340400002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317893400160">The NRTIs Lamivudine, Stavudine and Zidovudine Have Reduced HIV-1 Inhibitory Activity in Astrocytes.</a> Gray, L.R., G. Tachedjian, A.M. Ellett, M.J. Roche, W.-J. Cheng, G.J. Guillemin, B.J. Brew, S.G. Turville, S.L. Wesselingh, P.R. Gorry, and M.J. Churchill. Plos One, 2013. 8(4): p. e62196. ISI[000317893400160].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317925100007">Synthesis and anti-HIV-evaluation of Novel Andrographolide Derivatives.</a> Gu, G., H. Lin, Y. Liu, L. Yang, Y. Zheng, B. Wang, C. Tang, H. Qian, and W. Huang. Letters in Drug Design &amp; Discovery, 2013. 10(2): p. 155-163. ISI[000317925100007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318060200009">Identification of Sumoylation Activating Enzyme 1 Inhibitors by Structure-based Virtual Screening.</a> Kumar, A., A. Ito, M. Hirohama, M. Yoshida, and K.Y.J. Zhang. Journal of Chemical Information and Modeling, 2013. 53(4): p. 809-820. ISI[000318060200009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318055800003">Identification of Compounds from the Plant Species Alepidea amatymbica Active against HIV.</a> Louvel, S., N. Moodley, I. Seibert, P. Steenkamp, R. Nthambeleni, V. Vidal, V. Maharaj, and T. Klimkait. South African Journal of Botany, 2013. 86: p. 9-14. ISI[000318055800003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317698901453">Potent and Persistent Activity of a Nucleoside Reverse Transcriptase Inhibitor, 4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine, against HIV-1 Infection in Langerhans Cells.</a> Matsuzawa, T., T. Kawamura, Y. Ogawa, R. Aoki, Y. Koyanagi, A. Blauvelt, H. Mitsuya, and S. Shimada. Journal of Investigative Dermatology, 2013. 133: p. S197-S197. ISI[000317698901453].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318055800157">Scientific Investigation of the anti-HIV Properties of a South African Medicinal Plant.</a> Mbobela, P.F., G. Fouche, C. Kenyon, V. Maharaj, P. Pillay, X. Peter, and N. Van der Berg. South African Journal of Botany, 2013. 86: p. 177-177. ISI[000318055800157].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317941600004">Synthesis of Some Novel Thiosemicarbazone Derivatives Having Anti-cancer, anti-HIV as Well as Anti-bacterial Activity.</a> Patel, H.D., S.M. Divatia, and E. de Clercq. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2013. 52(4): p. 535-545. ISI[000317941600004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318020100033">Design, Synthesis and Antitumor Activity of Novel 4-Methyl-(3&#39;S,4&#39;S)-cis-khellactone Derivatives.</a> Ren, L., X. Du, M. Hu, C. Yan, T. Liang, and Q. Li. Molecules, 2013. 18(4): p. 4158-4169. ISI[000318020100033].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318302500016">Discovery of Novel Pyridazinylthioacetamides as Potent HIV-1 NNRTIs Using a Structure-based Bioisosterism Approach.</a> Song, Y.n., P. Zhan, D. Kang, X. Li, Y. Tian, Z. Li, X. Chen, W. Chen, C. Pannecouque, E. De Clercq, and X. Liu. Medchemcomm, 2013. 4(5): p. 810-816. ISI[000318302500016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318271400022">Phomonaphthalenone A: A Novel Dihydronaphthalenone with anti-HIV Activity from Phomopsis Sp Hccb04730.</a> Yang, Z., J. Ding, K. Ding, D. Chen, S. Cen, and M. Ge. Phytochemistry Letters, 2013. 6(2): p. 257-260. ISI[000318271400022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0524-060613.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130523&amp;CC=WO&amp;NR=2013072917A2&amp;KC=A2">Antiviral Composition.</a> Arsenio do Carmo Sales, M.F., F.S.B. Nuno, B.G. Joao Manuel, and C.S. Ana Catarina. Patent. 2013. 2012-IN153 2013072917: 30pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130502&amp;CC=WO&amp;NR=2013059928A1&amp;KC=A1">Preparation of Thiophene Derivatives as HIV Protease Inhibitors.</a> Boyd, M.J., J.-F. Chiasson, S. Crane, and A. Giroux. Patent. 2013. 2012-CA998 2013059928: 127pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130502&amp;CC=WO&amp;NR=2013060980A1&amp;KC=A1">Purine Nucleoside Analog Inhibitors of Retroviral Reverse Transcriptase for the Treatment of a Viral Infection and Mutations Associated with Sensitivity to These Inhibitors.</a> Calvez, V., A.-G. Marcelin, C. Soulie, M. Etheve-Quelquejeu, and M. Sollogoub. Patent. 2013. 2012-FR52433 2013060980: 26pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130516&amp;CC=WO&amp;NR=2013068551A1&amp;KC=A1">Preparation of Disubstituted Triazine Dimers Useful in the Treatment of Infectious Diseases.</a> Heeres, J., K. Augustyns, P. Van Der Veken, J. Joossens, V. Muthusamy, L. Maes, and P.J. Lewi. Patent. 2013. 2012-EP72308 2013068551: 90pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130502&amp;CC=WO&amp;NR=2013062028A1&amp;KC=A1">Preparation of 2-Phenylacetic acid Derivatives as HIV Replication Inhibitors.</a> Iwaki, T. and K. Tomita. Patent. 2013. 2012-JP77544 2013062028: 488pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130425&amp;CC=WO&amp;NR=2013059530A2&amp;KC=A2">Peptidomimetic Macrocyclic Peptides for Treating and Preventing HIV Infection and Associated Immune Deficiencies Such as AIDS Disease.</a> Kawahata, N. and C. Elkin. Patent. 2013. 2012-US60919 2013059530: 87pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0524-060613.</p>

    <br />

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130425&amp;CC=WO&amp;NR=2013059437A1&amp;KC=A1">Preparation of Aminoboronoaminocyclobutylhexanic acid Derivatives and Analogs for Use as Arginase Inhibitors.</a> Van Zandt, M., A. Golebiowski, M.K. Ji, D. Whitehouse, T. Ryder, and R.P. Beckett. Patent. 2013. 2012-US60789 2013059437: 143pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0524-060613.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
