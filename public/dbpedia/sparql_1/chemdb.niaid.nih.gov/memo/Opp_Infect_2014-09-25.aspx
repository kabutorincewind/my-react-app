

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-09-25.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="qQrLL9xAAEYnL/QCFjKb0XXuHgniHFjnBaTDTU1b0inaJWWjqTg2ZTGAZ0LK63lpCNHFar+XqbSosslGhaj3ocjvtYi8SEj2yFn/y2whRql3kwTlGjWQBlxHn18=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3806DF35" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: September 12 - September 25, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25232704">Synthesis and in Vitro Evaluation of New Nitro-substituted Thiazolyl hydrazone Derivatives as Anticandidal and Anticancer Agents.</a> Altintop, M.D., A. Ozdemir, G. Turan-Zitouni, S. Ilgin, O. Atli, F. Demirci, and Z.A. Kaplancikli. Molecules, 2014. 19(9): p. 14809-14820. PMID[25232704].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25242268">Lemon Grass (Cymbopogon citratus) Essential Oil as a Potent Anti-inflammatory and Antifungal Drugs.</a> Boukhatem, M.N., M.A. Ferhat, A. Kameli, F. Saidi, and H.T. Kebir. Libyan Journal of Medicine, 2014. 9: p. 25431. PMID[25242268].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25050881">Design, Synthesis, Antimicrobial Evaluation and Molecular Docking Studies of Some New Thiophene, Pyrazole and Pyridone Derivatives Bearing Sulfisoxazole Moiety.</a> Nasr, T., S. Bondock, and S. Eid. European Journal of Medicinal Chemistry, 2014. 84: p. 491-504. PMID[25050881].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25055342">Design, Synthesis and 3D-QSAR Studies of New Diphenylamine Containing 1,2,4-Triazoles as Potential Antitubercular Agents.</a> Mohan Krishna, K., B. Inturi, G.V. Pujar, M.N. Purohit, and G.S. Vijaykumar. European Journal of Medicinal Chemistry, 2014. 84: p. 516-529. PMID[25055342].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25221657">2-Phenylindole and Arylsulphonamide: Novel Scaffolds Bactericidal against Mycobacterium tuberculosis.</a>Naik, M., S. Ghorpade, L.K. Jena, G. Gorai, A. Narayan, S. Guptha, S. Sharma, N. Dinesh, P. Kaur, R. Nandishaiah, J. Bhat, G. Balakrishnan, V. Humnabadkar, V. Ramachandran, L.K. Naviri, P. Khadtare, M. Panda, P.S. Iyer, and M. Chatterji. ACS Medicinal Chemistry Letters, 2014. 5(9): p. 1005-1009. PMID[25221657].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25016370">Design, Synthesis and Anti-mycobacterial Activity of 1,2,3,5-Tetrasubstituted pyrrolyl-N-acetic acid Derivatives.</a>Pagadala, L.R., L.D. Mukkara, S. Singireddi, A. Singh, V.R. Thummaluru, P.S. Jagarlamudi, R.S. Guttala, Y. Perumal, S. Dharmarajan, S.M. Upadhyayula, R. Ummanni, V.S. Basireddy, and N. Ravirala. European Journal of Medicinal Chemistry, 2014. 84: p. 118-126. PMID[25016370].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25063946">New Thiophene-1,2,4-triazole-5(3)-ones: Highly Bioactive Thiosemicarbazides, Structures of Schiff Bases and Triazole-thiols.</a> Unver, Y., K. Sancak, F. Celik, E. Birinci, M. Kucuk, S. Soylu, and N.A. Burnaz. European Journal of Medicinal Chemistry, 2014. 84: p. 639-650. PMID[25063946].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25137375">Discovery of Covalent Inhibitors of Glyceraldehyde-3-phosphate Dehydrogenase, a Target for the Treatment of Malaria.</a> Bruno, S., A. Pinto, G. Paredi, L. Tamborini, C. De Micheli, V. La Pietra, L. Marinelli, E. Novellino, P. Conti, and A. Mozzarelli. Journal of Medicinal Chemistry, 2014. 57(17): p. 7465-7471. PMID[25137375].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25019476">Synthesis of Novel Thiadiazolotriazin-4-ones and Study of Their Mosquito-larvicidal and Antibacterial Properties.</a>Castelino, P.A., P. Naik, J.P. Dasappa, R.S. Sujayraj, K. Sharath Chandra, K. Chaluvaiah, R. Nair, M.V. Sandya Kumari, G. Kalthur, and S.K. Adiga. European Journal of Medicinal Chemistry, 2014. 84: p. 194-199. PMID[25019476].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25016227">Design, Synthesis and Characterization of Fluoro Substituted Novel Pyrazolylpyrazolines Scaffold and Their Pharmacological Screening.</a> Karad, S.C., V.B. Purohit, and D.K. Raval. European Journal of Medicinal Chemistry, 2014. 84: p. 51-58. PMID[25016227].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25129616">Substrate Derived Peptidic alpha-Ketoamides as Inhibitors of the Malarial Protease PfSUB1.</a>Kher, S.S., M. Penzo, S. Fulle, P.W. Finn, M.J. Blackman, and A. Jirgensons. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(18): p. 4486-4489. PMID[25129616].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25137549">Discovery of Carbohybrid-based 2-Aminopyrimidine Analogues as a New Class of Rapid-acting Antimalarial Agents Using Image-based Cytological Profiling Assay.</a> Lee, S., D. Lim, E. Lee, N. Lee, H.G. Lee, J. Cechetto, M. Liuzzi, L.H. Freitas-Junior, J.S. Song, M.A. Bae, S. Oh, L. Ayong, and S.B. Park. Journal of Medicinal Chemistry, 2014. 57(17): p. 7425-7434. PMID[25137549].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25038484">Discovery of Highly Selective 7-Chloroquinoline-thiohydantoins with Potent Antimalarial Activity.</a> Raj, R., V. Mehra, J. Gut, P.J. Rosenthal, K.J. Wicht, T.J. Egan, M. Hopper, L.A. Wrischnik, K.M. Land, and V. Kumar. European Journal of Medicinal Chemistry, 2014. 84: p. 425-432. PMID[25038484].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0912-092514.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340330800008">Palladium(II) and Zinc(II) Complexes of Neutral N2O2 Donor Schiff Bases Derived from Furfuraldehyde: Synthesis, Characterization, Fluorescence and Corrosion Inhibitors of Ligands.</a> Ali, O.A.M. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2014. 132: p. 52-60. ISI[000340330800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340551500023">Auranofin is Highly Efficacious against Toxoplasma gondii in Vitro and in an in Vivo Experimental Model of Acute Toxoplasmosis.</a> Andrade, R.M., J.D. Chaparro, E. Capparelli, and S.L. Reed. PLoS Neglected Tropical Diseases, 2014. 8(7): p. e2973. ISI[000340551500023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340865700019">Synthesis of 4,4 &#39;-bis (Pyrimidinedione acetamidoxy) bibenzyls as Antifungal Agents.</a>Dwivedi, S. and I.R. Siddiqui. Indian Journal of Heterocyclic Chemistry, 2014. 23(4): p. 433-436. ISI[000340865700019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340500300058">Synthesis, Characterization and Antimicrobial Studies of Some New Trifluoromethyl Quinoline-3-Carbohydrazide and 1,3,4-Oxadiazoles.</a> Garudachari, B., A.M. Isloor, M.N. Satyanaraya, K. Ananda, and H.K. Fun. RSC Advances, 2014. 4(58): p. 30864-30875. ISI[000340500300058].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340865700006">Synthesis, Antibacterial and Antitubercular Activities of Some Novel Quinoline Derivatives.</a>Kumar, M.R.P., S.D. Joshi, S.R. Dixit, and V.H. Kulkarni. Indian Journal of Heterocyclic Chemistry, 2014. 23(4): p. 353-358. ISI[000340865700006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340865700010">Synthesis of 1,5-Benzothiazepines: Part 41: Single Pot Synthesis and Antimicrobial Studies of 8-Substituted-2,5-dihydro-4-(4-substituted aryl)-2-(2-furyl)-1,5-benzothiazepines.</a> Pant, S., Avinash, and M. Yadav. Indian Journal of Heterocyclic Chemistry, 2014. 23(4): p. 381-386. ISI[000340865700010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340771400001">Antifungal and Antioxidant Activities of the Essential Oil from Angelica koreana Nakai.</a> Roh, J. and S. Shin. Evidence-Based Complementary and Alternative Medicine, 2014. 398503: 7pp. ISI[000340771400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340688300058">Antifungal Activity and Chemical Composition of Twenty Essential Oils against Significant Indoor and Outdoor Toxigenic and Aeroallergenic Fungi.</a> Zabka, M., R. Pavela, and E. Prokinova. Chemosphere, 2014. 112: p. 443-448. ISI[000340688300058].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0912-092514.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
