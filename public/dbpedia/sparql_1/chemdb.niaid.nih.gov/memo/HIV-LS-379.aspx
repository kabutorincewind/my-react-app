

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-379.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="DBxzTEdw+GUQOJAGMyWpxFUegbqDfsmpKuCZK/E0D93HkAur5bUqKjHajhttvC2jess1DkUiZb5IOeh6TFRlAMRoejc/7zkE+je9kMZOBKKChjw5ub0twM8wRBQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="621A1D4E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-379-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61304   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Topochemical models for anti-HIV activity of 1-alkoxy-5-alkyl-6-(arylthio)uracils</p>

    <p>          Bajaj, S and Madan, AK</p>

    <p>          Chem. Pap. <b>2007</b>.  61(2): 127-132</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61305   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          N&#39;-[2-(2-thiophene)ethyl]-N&#39;-[2-(5-bromopyridyl)]thiourea (HI-443), a rationally designed non-nucleoside reverse transcriptase inhibitor compound with potent anti-HIV activity</p>

    <p>          Uckun, Fatih M, Qazi, Sanjive, and Venkatachalam, Taracad</p>

    <p>          Arzneim. Forsch. <b>2007</b>.  57(5): 278-285</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61306   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Synergistic inhibition of protease-inhibitor-resistant HIV type 1 by saquinavir in combination with atazanavir or lopinavir</p>

    <p>          Dam, Elisabeth, Lebel-Binay, Sophie, Rochas, Severine, Thibaut, Laurent, Faudon, Jean-Louis, Thomas, Claire-Marie, Essioux, Laurent, Hill, Andrew, Schutz, Malte, and Clavel, Francois</p>

    <p>          Antiviral Ther. <b>2007</b>.  12(3): 371-380</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61307   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Maribavir: 1263W94, benzimidavir, GW 1263, GW 1263W94, VP41263</p>

    <p>          Anon</p>

    <p>          Drugs R&amp;D <b>2007</b>.  8(3): 188-192</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61308   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Human immunodeficiency virus type 1 variants resistant to first- and second-version fusion inhibitors and cytopathic in ex vivo human lymphoid tissue</p>

    <p>          Chinnadurai, Raghavan, Rajan, Devi, Muench, Jan, and Kirchhoff, Frank</p>

    <p>          J. Virol. <b>2007</b>.  81(12): 6563-6572</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61309   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Preparation of pyrimidine derivatives as HIV integrase inhibitors</p>

    <p>          Naidu, Narasimhulu B, Ueda, Yasutsugu, and Connolly, Timothy P</p>

    <p>          PATENT:  WO <b>2007064619</b>  ISSUE DATE:  20070607</p>

    <p>          APPLICATION: 2006  PP: 129pp.</p>

    <p>          ASSIGNEE:  (Bristol-Myers Squibb Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     61310   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Anti-viral griffithsin proteins and methods of inhibiting viral infections</p>

    <p>          O&#39;Keefe, Barry R, Mori, Toshiyuki, and McMahon, James B</p>

    <p>          PATENT:  WO <b>2007064844</b>  ISSUE DATE:  20070607</p>

    <p>          APPLICATION: 2006  PP: 79pp.</p>

    <p>          ASSIGNEE:  (Government of the United States of America, Represented by the Secretary Department of Health and Human Services USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>8.     61311   HIV-LS-379; WOS-HIV-7/13/2007</p>

    <p class="memofmt1-2">          Synthesis of Novel Pett Analogues: 3,4-Dimethoxy Phenyl Ethyl 1,3,5-Triazinyl Thiourea Derivatives and Their Antibacterial and Anti-Hiv Studies</p>

    <p>          Patel, R. <i>et al.</i></p>

    <p>          Journal of the Brazilian Chemical Society <b>2007</b>.  18(2): 312-321</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246443800010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246443800010</a> </p><br />

    <p>9.     61312   HIV-LS-379; WOS-HIV-7/13/2007</p>

    <p class="memofmt1-2">          C34, a Membrane Fusion Inhibitor, Blocks Hiv Infection of Langerhans Cells and Viral Transmission to T Cells</p>

    <p>          Sugaya, M. <i>et al.</i></p>

    <p>          Journal of Investigative Dermatology <b>2007</b>.  127(6): 1436-1443</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246618300024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246618300024</a> </p><br />

    <p>10.   61313   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Preparation of acyclic nucleotides as antiviral agents</p>

    <p>          Zhao, Mingyan and Zhong, Suling</p>

    <p>          PATENT:  CN <b>1966514</b>  ISSUE DATE: 20070523</p>

    <p>          APPLICATION: 1011-7768  PP: 15pp.</p>

    <p>          ASSIGNEE:  (Beijing Meibeita Pharmaceutical Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61314   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Synthesis and in vitro antiviral studies of bis (pivaloyloxymethyl) ester derivative of 9-{((phosphonomethyl) aziridin-1-yl)methyl}adenine (PMAMA) and analogues</p>

    <p>          Sheikha, Ghassan MAbu, Bkhaitan, Majdi M, Al-Hourani, Rami A, Qaisi, Ali M, Loddo, Roberta, and La Colla, Paolo</p>

    <p>          Saudi Pharm. J. <b>2007</b>.  15(1): 16-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61315   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 multiplication by a modified U7 snRNA inducing Tat and Rev exon skipping</p>

    <p>          Asparuhova, Maria B, Marti, Gabriela, Liu, Songkai, Serhan, Fatima, Trono, Didier, and Schumperli, Daniel</p>

    <p>          J. Gene Med. <b>2007</b>.  9(5): 323-334</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   61316   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Pharmaceutical combination of nucleoside and nucleotide reverse transcriptase inhibitors</p>

    <p>          Lulla, Amar and Malhotra, Geena</p>

    <p>          PATENT:  WO <b>2007068934</b>  ISSUE DATE:  20070621</p>

    <p>          APPLICATION: 2006  PP: 25pp.</p>

    <p>          ASSIGNEE:  (Cipla Limited, India and Curtis, Philip Anthony</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   61317   HIV-LS-379; WOS-HIV-7/13/2007</p>

    <p class="memofmt1-2">          Preclinical Safety Assessments of Uc781 Anti-Human Immunodeficiency Virus Topical Microbicide Formulations</p>

    <p>          Patton, D. <i>et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(5): 1608-1615</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246541500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246541500003</a> </p><br />
    <br clear="all">

    <p>15.   61318   HIV-LS-379; WOS-HIV-7/13/2007</p>

    <p class="memofmt1-2">          Preclinical Testing of Candidate Topical Microbicides for Anti-Human Immunodeficiency Virus Type 1 Activity and Tissue Toxicity in a Human Cervical Explant Culture</p>

    <p>          Cummins, J. <i>et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(5): 1770-1779</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246541500026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246541500026</a> </p><br />

    <p>16.   61319   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          HIV gp41 C-terminal Heptad Repeat Contains Multifunctional Domains: relation to mechanisms of action of anti-HIV peptides</p>

    <p>          Liu, Shuwen, Jing, Weiguo, Cheung, Byron, Lu, Hong, Sun, Jane, Yan, Xuxia, Niu, Jinkui, Farmar, James, Wu, Shuguang, and Jiang, Shibo</p>

    <p>          J. Biol. Chem.  <b>2007</b>.  282(13): 9612-9620</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   61320   HIV-LS-379; SCIFINDER-HIV-7/16/2007</p>

    <p class="memofmt1-2">          Glucuronidation of anti-HIV drug candidate bevirimat: identification of human UDP-glucuronosyltransferases and species differences</p>

    <p>          Wen, Zhiming, Martin, David E, Bullock, Peter, Lee, Kuo-Hsiung, and Smith, Philip C</p>

    <p>          Drug Metab. Dispos. <b>2007</b>.  35(3): 440-448</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   61321   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          Enfuvirtide: the First Step for a New Strategy of Antiretroviral Therapy.</p>

    <p>          Bottaro, E.</p>

    <p>          Medicina-Buenos Aires <b>2007</b>.  67(2): 195-205</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246627900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246627900016</a> </p><br />

    <p>19.   61322   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          Nucleoside Reverse Transcriptase Inhibitors and Human Immunodeficiency Virus Proteins Cause Axonal Injury in Human Dorsal Root Ganglia Cultures</p>

    <p>          Robinson, B., Li, Z., and Nath, A.</p>

    <p>          Journal of Neurovirology <b>2007</b>.  13(2): 160-167</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246763800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246763800008</a> </p><br />

    <p>20.   61323   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          A Dimeric Lactone From Ardisia Japonica With Inhibitory Activity for Hiv-1 and Hiv-2 Ribonuclease H</p>

    <p>          Dat, N. <i>et al.</i></p>

    <p>          Journal of Natural Products <b>2007</b>.  70(5): 839-841</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246764300022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246764300022</a> </p><br />

    <p>21.   61324   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          Development of a Novel Fusion Inhibitor Against T-20-Resistant Hiv-1</p>

    <p>          Qishi, S. <i>et al.</i></p>

    <p>          Biopolymers <b>2007</b>.  88(4): 524</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900046">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900046</a> </p><br />
    <br clear="all">

    <p>22.   61325   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          Synthesis of Biotinyl-Tn-14403, Anti-Hiv Peptide Amide</p>

    <p>          Nedev, H. and Saragovi, H.</p>

    <p>          Biopolymers <b>2007</b>.  88(4): 583</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900281">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900281</a> </p><br />

    <p>23.   61326   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          Anti-Hiv Dendrimeric Peptides</p>

    <p>          Yu, Q. <i>et al.</i></p>

    <p>          Biopolymers <b>2007</b>.  88(4): 603</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900359">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900359</a> </p><br />

    <p>24.   61327   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          Testing Antiretroviral Drug Efficacy in Conventional Mice Infected With Chimeric Hiv-1</p>

    <p>          Hadas, E. <i>et al.</i></p>

    <p>          Aids <b>2007</b>.  21 (8): 905-909</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246754100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246754100002</a> </p><br />

    <p>25.   61328   HIV-LS-379; WOS-HIV-7/20/2007</p>

    <p class="memofmt1-2">          High-Throughput Real-Time Assay Based on Molecular Beacons for Hiv-1 Integrase 3 &#39;-Processing Reaction</p>

    <p>          He, H. <i>et al.</i></p>

    <p>          Acta Pharmacologica Sinica <b>2007</b>.  28(6): 811-817</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246756300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246756300009</a> </p><br />

    <p>26.   61329   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Design of helical, oligomeric HIV-1 fusion inhibitor peptides with potent activity against enfuvirtide-resistant virus</p>

    <p>          Dwyer, JJ, Wilson, KL, Davison, DK, Freel, SA, Seedorff, JE, Wring, SA, Tvermoes, NA, Matthews, TJ, Greenberg, ML, and Delmedico, MK</p>

    <p>          Proc Natl Acad Sci U S A <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17640899&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17640899&amp;dopt=abstract</a> </p><br />

    <p>27.   61330   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          In Vitro Antiviral Activity and Cross-Resistance Profile of PL-100, a Next Generation Protease Inhibitor of Human Immunodeficiency Virus Type 1</p>

    <p>          Dandache, S, Sevigny, G, Yelle, J, Stranix, BR, Parkin, N, Schapiro, JM, Wainberg, MA, and Wu, JJ</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17638694&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17638694&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   61331   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          N(2)-Benzyloxycarbonylguan-9-yl Acetic Acid Derivatives as HIV-1 Reverse Transcriptase Non-Nucleoside Inhibitors with Decreased Loss of Potency Against Common Drug-Resistance Mutations</p>

    <p>          Adebambo, KF, Zanoli, S, Thomas, MG, Cancio, R, Howarth, NM, and Maga, G</p>

    <p>          ChemMedChem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17638375&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17638375&amp;dopt=abstract</a> </p><br />

    <p>29.   61332   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Protein-protein interactions as new targets for drug design: virtual and experimental approaches</p>

    <p>          Ivanov, AS, Gnedenko, OV, Molnar, AA, Mezentsev, YV, Lisitsa, AV, and Archakov, AI</p>

    <p>          J Bioinform Comput Biol <b>2007</b>.  5(2b): 579-92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17636863&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17636863&amp;dopt=abstract</a> </p><br />

    <p>30.   61333   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Potent inhibition of HIV-1 replication by novel non-peptidyl small molecule inhibitors of protease dimerization</p>

    <p>          Koh, Y, Matsumi, S, Das, D, Amano, M, Davis, DA, Li, J, Leschenko, S, Baldridge, A, Shioda, T, Yarchoan, R, Ghosh, AK, and Mitsuya, H</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17635930&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17635930&amp;dopt=abstract</a> </p><br />

    <p>31.   61334   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Iron chelators ICL670 and 311 inhibit HIV-1 transcription</p>

    <p>          Debebe, Z, Ammosova, T, Jerebtsova, M, Kurantsin-Mills, J, Niu, X, Charles, S, Richardson, DR, Ray, PE, Gordeuk, VR, and Nekhai, S</p>

    <p>          Virology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17631934&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17631934&amp;dopt=abstract</a> </p><br />

    <p>32.   61335   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Ketoconazole is inferior to ritonavir as an alternative booster for saquinavir in a once daily regimen in Thai HIV-1 infected patients</p>

    <p>          Autar, RS, Wit, FW, Sankote, J, Sutthichom, D, Kimenai, E, Hassink, E, Hill, A, Cooper, DA, Phanuphak, P, Lange, JM, Burger, DM, and Ruxrungtham, K</p>

    <p>          AIDS <b>2007</b>.  21 (12): 1535-1539</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17630547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17630547&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>33.   61336   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          New developments in diketo-containing inhibitors of HIV-1 integrase</p>

    <p>          Zhao, G, Wang, C, Liu, C, and Lou, H</p>

    <p>          Mini Rev Med Chem <b>2007</b>.  7(7): 707-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627583&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627583&amp;dopt=abstract</a> </p><br />

    <p>34.   61337   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 2,3-diaryl substituted-1,3-thiazolidin-4-ones as anti-HIV agents</p>

    <p>          Rawal, RK, Tripathi, RK, Katti, SB, Pannecouque, C, and De, Clercq E</p>

    <p>          Med Chem <b>2007</b>.  3(4): 355-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627572&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627572&amp;dopt=abstract</a> </p><br />

    <p>35.   61338   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          An Update in the Development of HIV Entry Inhibitors</p>

    <p>          Rusconi, S, Scozzafava, A, Mastrolorenzo, A, and Supuran, CT</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(13): 1273-89</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627557&amp;dopt=abstract</a> </p><br />

    <p>36.   61339   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Bis and tris indole alkaloids from marine organisms: new leads for drug discovery</p>

    <p>          Gupta, L, Talwar, A, and Chauhan, PM</p>

    <p>          Curr Med Chem <b>2007</b>.  14(16): 1789-803</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627517&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627517&amp;dopt=abstract</a> </p><br />

    <p>37.   61340   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          HIV Integrase Inhibitors: From Diketoacids to Heterocyclic Templates: A History of HIV Integrase Medicinal Chemistry at Merck West Point and Merck Rome (IRBM)</p>

    <p>          Egbertson, MS</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(13): 1251-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627556&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627556&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>38.   61341   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          HIV-1 Integrase: From Biology to Chemotherapeutics</p>

    <p>          Zeinalipour-Loizidou, E, Nicolaou, C, Nicolaides, A, and Kostrikis, LG</p>

    <p>          Curr HIV Res <b>2007</b>.  5(4): 365-88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627500&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627500&amp;dopt=abstract</a> </p><br />

    <p>39.   61342   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          In vitro pre- and post-exposure prophylaxis using HIV inhibitors as microbicides against cell-free or cell-associated HIV-1 infection</p>

    <p>          Terrazas-Aranda, K, Van Herrewege, Y, Lewi, PJ, Van Roey, J, and Vanham, G</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(3): 141-151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626598&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626598&amp;dopt=abstract</a> </p><br />

    <p>40.   61343   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          A Versatile Route to C-6 Arylmethyl-Functionalized S-DABO and Related Analogues</p>

    <p>          Radi, M, Contemori, L, Castagnolo, D, Spinosa, R, Este, JA, Massa, S, and Botta, M</p>

    <p>          Org Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17625879&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17625879&amp;dopt=abstract</a> </p><br />

    <p>41.   61344   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by P-TEFb inhibitors DRB, seliciclib and flavopiridol correlates with release of free P-TEFb from the large, inactive form of the complex</p>

    <p>          Biglione, S, Byers, SA, Price, JP, Nguyen, VT, Bensaude, O, Price, DH, and Maury, W</p>

    <p>          Retrovirology <b>2007</b>.  4(1): 47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17625008&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17625008&amp;dopt=abstract</a> </p><br />

    <p>42.   61345   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Anti-Vpr Activities of Heat Shock Protein 27</p>

    <p>          Liang, D, Benko, Z, Agbottah, E, Bukrinsky, M, and Zhao, RY</p>

    <p>          Mol Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622316&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622316&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>43.   61346   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          In vitro Suppression of K65R Reverse Transcriptase-mediated Tenofovir- and Adefovir-5&#39; Diphosphate Resistance Conferred by the Borano-phosphonate Derivatives</p>

    <p>          Frangeul, A, Barral, K, Alvarez, K, and Canard, B</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17620380&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17620380&amp;dopt=abstract</a> </p><br />

    <p>44.   61347   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Evaluation of the In Vitro Antiviral Activity of the Novel, Tyrosyl-based Human Immunodeficiency Virus Type 1 Protease Inhibitor Brecanavir (BCV, GW640385, 385) in Combination with Other Antiretrovirals and Against a Panel of Protease Inhibitor-Resistant HIV</p>

    <p>          Hazen, R, Harvey, R, Ferris, R, Craig, C, Yates, P, Griffin, P, Miller, J, Kaldor, I, Ray, J, Samano, V, Furfine, E, Spaltenstein, A, Hale, M, Tung, R, St, Clair M, Hanlon, M, and Boone, L</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17620375&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17620375&amp;dopt=abstract</a> </p><br />

    <p>45.   61348   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Bioinformatic approaches for modeling the substrate specificity of HIV-1 protease: an overview</p>

    <p>          Rognvaldsson, T, You, L, and Garwicz, D</p>

    <p>          Expert Rev Mol Diagn <b>2007</b>.  7(4): 435-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17620050&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17620050&amp;dopt=abstract</a> </p><br />

    <p>46.   61349   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          HIV entry inhibitors</p>

    <p>          Este, JA and Telenti, A</p>

    <p>          Lancet <b>2007</b>.  370(9581): 81-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17617275&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17617275&amp;dopt=abstract</a> </p><br />

    <p>47.   61350   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Rationally Designed Dual Inhibitors of HIV Reverse Transcriptase and Integrase</p>

    <p>          Wang, Z, Bennett, EM, Wilson, DJ, Salomon, C, and Vince, R</p>

    <p>          J Med Chem <b>2007</b>.  50(15): 3416-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17608468&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17608468&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>48.   61351   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Benzyl amide-ketoacid inhibitors of HIV-integrase</p>

    <p>          Walker, MA, Johnson, T, Naidu, BN, Banville, J, Remillard, R, Plamondon, S, Martel, A, Li, C, Torri, A, Samanta, H, Lin, Z, Dicker, I, Krystal, M, and Meanwell, NA</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17604626&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17604626&amp;dopt=abstract</a> </p><br />

    <p>49.   61352   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Diels-Alder Approach for the Construction of Halogenated, o-Nitro Biaryl Templates and Application to the Total Synthesis of the Anti-HIV Agent Siamenol</p>

    <p>          Naffziger, MR, Ashburn, BO, Perkins, JR, and Carter, RG</p>

    <p>          J Org Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602532&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602532&amp;dopt=abstract</a> </p><br />

    <p>50.   61353   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Suppression of HIV replication using RNA interference against HIV-1 integrase</p>

    <p>          Lau, TS, Li, Y, Kameoka, M, Ng, TB, and Wan, DC</p>

    <p>          FEBS Lett <b>2007</b>.  581(17): 3253-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17592732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17592732&amp;dopt=abstract</a> </p><br />

    <p>51.   61354   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          Pharmacokinetics and antiretroviral response to darunavir/ritonavir and etravirine combination in patients with high-level viral resistance</p>

    <p>          Boffito, M, Winston, A, Jackson, A, Fletcher, C, Pozniak, A, Nelson, M, Moyle, G, Tolowinska, I, Hoetelmans, R, Miralles, D, and Gazzard, B</p>

    <p>          AIDS <b>2007</b>.  21 (11): 1449-1455</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17589191&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17589191&amp;dopt=abstract</a> </p><br />

    <p>52.   61355   HIV-LS-379; PUBMED-HIV-7/23/2007</p>

    <p class="memofmt1-2">          HIV-1 Protease: Structure, Dynamics, and Inhibition</p>

    <p>          Louis, JM, Ishima, R, Torchia, DA, and Weber, IT</p>

    <p>          Adv Pharmacol <b>2007</b>.  55: 261-98</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17586318&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17586318&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
