

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-10-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yh28UFzO8vQpnO49I8BTJBH2KxaI+ZjEHVQUoB1emBIla/bg/kjR4wMhwOQd1NuI9Pj6LWfj59kQ2Nhc9KxVWehXtsvXYHsF3r2OqUxKWO56LHhsdEegQiQq7ME=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A5BD80DF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">October 15 - October 28, 2010</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20973917">Synthesis, Molecular Docking, and Biological Evaluation of Novel Triazole Derivatives as Antifungal Agents.</a> Guan, Z., X. Chai, S. Yu, H. Hu, Y. Jiang, Q. Meng, and Q. Wu. Chemical Biology &amp; Drug Design, 2010. <b>[Epub ahead of print]</b>; PMID[20973917].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1015-102810.</p>

    <p> </p>

    <h2 class="memofmt2-2"> (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">2.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20963667">Effect of exogenous cardiolipin on the growth and viability of Mycobacterium tuberculosis H37Rv in vitro.</a> Andreevskaya, S.N., T.G. Smirnova, Y.A. Zhogina, D.I. Smirnova, Y.L. Mikulovich, G.M. Sorokoumova, L.N. Chernousova, A.A. Selishcheva, and V.I. Shvets. Doklady Biological Sciences, 2010. 434(1): p. 371-374; PMID[20963667].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1015-102810.</p>

    <p> </p>

    <p class="plaintext">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20970222">Synthesis and optimization of antitubercular activities in a series of 4-(aryloxy)phenyl cyclopropyl methanols.</a> Bisht, S.S., N. Dwivedi, V. Chaturvedi, N. Anand, M. Misra, R. Sharma, B. Kumar, R. Dwivedi, S. Singh, S.K. Sinha, V. Gupta, P.R. Mishra, A.K. Dwivedi, and R.P. Tripathi. European Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[20970222].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1015-102810</p>

    <p> </p>

    <p class="plaintext">4.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20833056">Synthesis of non-purine analogs of 6-aryl-9-benzylpurines, and their antimycobacterial activities. Compounds modified in the imidazole ring.</a> Khoje, A.D., A. Kulendrn, C. Charnock, B. Wan, S. Franzblau, and L.L. Gundersen. Bioorganic &amp; Medicinal Chemistry, 2010. 18(20): p. 7274-7282; PMID[20833056].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1015-102810</p>

    <p> </p>

    <p class="plaintext">5.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20965528">Hapalindole-related alkaloids from the cultured cyanobacterium Fischerella ambigua.</a> Mo, S., A. Krunic, B.D. Santarsiero, S.G. Franzblau, and J. Orjala. Phytochemistry, 2010. <b>[Epub ahead of print]</b>;</p>

    <p class="plaintext">PMID[20965528].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1015-102810.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="plaintext">6.<a class="memofmt2-4" href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282582600001">Synthesis and Antitubercular Evaluation of New Bis-1,2,3-Triazoles Derived from D-Mannitol.</a> Ferreira, M.D., M.V.N. de Souza, S. Wardell, J.L. Wardell, T.R.A. Vasconcelos, V.F. Ferreira, and M.C.S. Lourenco. Journal of Carbohydrate Chemistry, 2010. 29(6): p. 265-274; ISI[000282582600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1015-102810.</p>

    <p> </p>

    <p class="plaintext">7.<a class="memofmt2-4" href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282677900032">Synthesis and SAR studies of 1,4-benzoxazine MenB inhibitors: Novel antibacterial agents against Mycobacterium tuberculosis.</a> Li, X.K., N.N. Liu, H.N. Zhang, S.E. Knudson, R.A. Slayden, and P.J. Tonge. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(21): p. 6306-6309; ISI[000282677900032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1015-102810.</p>

    <p> </p>

    <p class="plaintext">8.<a class="memofmt2-4" href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282677900002">Design, synthesis, and biological evaluation of 4-(5-nitrofuran-2-yl) prop-2-en-1-one derivatives as potent antitubercular agents.</a> Tawari, N.R., R. Bairwa, M.K. Ray, M.G.R. Rajan, and M.S. Degani. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(21): p. 6175-6178; ISI[000282677900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1015-102810.</p>

    <p> </p>

    <p class="plaintext">9.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282489200026">Promising Antimicrobial Agents: Synthetic Approaches to Novel Tricyclic and Tetracyclic Pyrimidinones with Antimicrobial Properties.</a> Gaber, H.M., M.C. Bagley, S.M. Sherif, and U.M. Abdul-Raouf. Journal of Heterocyclic Chemistry, 2010. 47(5): p. 1162-1170; ISI[000282489200026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1015-102810.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
