

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-344.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="VEqU5j+dzPJvk+vGGw0RpuEXsbDfV8PaxkEcyRyuKxJcz1JV4PhNKSDhI/6dB/99L2D0rrClQ4zzdncHhATXgpbzRbtu/UUhBrXXc/npwTM+zUvVLsBTQTU8mGE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="15DE2E5A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH -HIV-LS-344-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48605   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          The effect of a methyl or 2-fluoroethyl substituent at the N-3 position of thymidine, 3&#39;-fluoro-3&#39;-deoxythymidine and 1-beta-D-arabinosylthymine on their antiviral and cytostatic activity in cell culture</p>

    <p>          Balzarini, J,  Celen, S, Karlsson, A, de Groot, T, Verbruggen, A, and Bormans, G</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(1): 17-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542002&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542002&amp;dopt=abstract</a> </p><br />

    <p>2.     48606   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by oligonucleotide analogues directed to the packaging signal and trans-activating response region</p>

    <p>          Brown, DE, Arzumanov, A, Syed, S, Gait, MJ, and Lever, AM</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(1): 1-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542000&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542000&amp;dopt=abstract</a> </p><br />

    <p>3.     48607   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Highly Potent and Orally Active CCR5 Antagonists as Anti-HIV-1 Agents: Synthesis and Biological Activities of 1-Benzazocine Derivatives Containing a Sulfoxide Moiety</p>

    <p>          Seto, M, Aikawa, K, Miyamoto, N, Aramaki, Y, Kanzaki, N, Takashima, K, Kuze, Y, Iizawa, Y, Baba, M, and Shiraishi, M</p>

    <p>          J Med Chem <b>2006</b>.  49(6): 2037-2048</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539392&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539392&amp;dopt=abstract</a> </p><br />

    <p>4.     48608   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Novel Bifunctional Quinolonyl Diketo Acid Derivatives as HIV-1 Integrase Inhibitors: Design, Synthesis, Biological Activities, and Mechanism of Action</p>

    <p>          Di Santo, R, Costi, R, Roux, A, Artico, M, Lavecchia, A, Marinelli, L, Novellino, E, Palmisano, L, Andreotti, M, Amici, R, Galluzzo, CM, Nencioni, L, Palamara, AT, Pommier, Y, and Marchand, C</p>

    <p>          J Med Chem <b>2006</b>.  49(6): 1939-1945</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539381&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539381&amp;dopt=abstract</a> </p><br />

    <p>5.     48609   HIV-LS-344; EMBASE-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV entry by carbohydrate-binding proteins</p>

    <p>          Balzarini, J</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JF8G1R-1/2/60befe4b223aa7be66ed1d6de0c3e6bf">http://www.sciencedirect.com/science/article/B6T2H-4JF8G1R-1/2/60befe4b223aa7be66ed1d6de0c3e6bf</a> </p><br />
    <br clear="all">

    <p>6.     48610   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          New HIV-1 reverse transcriptase inhibitors based on a tricyclic benzothiophene scaffold: Synthesis, resolution, and inhibitory activity</p>

    <p>          Krajewski, K, Zhang, Y, Parrish, D, Deschamps, J, Roller, PP, and Pathak, VK</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16527484&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16527484&amp;dopt=abstract</a> </p><br />

    <p>7.     48611   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          A fluorescence polarization assay for screening inhibitors against the ribonuclease H activity of HIV-1 reverse transcriptase</p>

    <p>          Nakayama, GR, Bingham, P, Tan, D, and Maegley, KA</p>

    <p>          Anal Biochem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16527235&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16527235&amp;dopt=abstract</a> </p><br />

    <p>8.     48612   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of chicoric acid analogs as inhibitors of HIV-1 integrase</p>

    <p>          Charvat, TT, Lee, DJ, Robinson, WE, and Chamberlin, AR</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16524737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16524737&amp;dopt=abstract</a> </p><br />

    <p>9.     48613   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Structure-activity relationships of anti-HIV-1 peptides with disulfide linkage between D- and L-cysteine at positions i and i+3, respectively, derived from HIV-1 gp41 C-peptide</p>

    <p>          Lee, MK, Kim, HK, Lee, TY, Hahm, KS, and Kim, KL</p>

    <p>          Exp Mol Med <b>2006</b>.  38(1): 18-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16520549&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16520549&amp;dopt=abstract</a> </p><br />

    <p>10.   48614   HIV-LS-344; EMBASE-HIV-3/20/2006</p>

    <p class="memofmt1-2">          The HIV-1 reverse transcriptase mutants G190S and G190A, which confer resistance to non-nucleoside reverse transcriptase inhibitors, demonstrate reductions in RNase H activity and DNA synthesis from tRNALys, 3 that correlate with reductions in replication efficiency</p>

    <p>          Wang, J, Dykes, C, Domaoal, RA, Koval, CE, Bambara, RA, and Demeter, LM</p>

    <p>          Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4JCBN4M-2/2/a604d73efcfd26101a6d40d48f6e92d4">http://www.sciencedirect.com/science/article/B6WXR-4JCBN4M-2/2/a604d73efcfd26101a6d40d48f6e92d4</a> </p><br />
    <br clear="all">

    <p>11.   48615   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Antiviral properties of two trimeric recombinant gp41 proteins</p>

    <p>          Delcroix-Genete, D, Quan, PL, Roger, MG, Hazan, U, Nisole, S, and Rousseau, C</p>

    <p>          Retrovirology <b>2006</b>.  3(1): 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16515685&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16515685&amp;dopt=abstract</a> </p><br />

    <p>12.   48616   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Dynamic pharmacophore model optimization: identification of novel HIV-1 integrase inhibitors</p>

    <p>          Deng, J, Sanchez, T, Neamati, N, and Briggs, JM</p>

    <p>          J Med Chem <b>2006</b>.  49(5): 1684-92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509584&amp;dopt=abstract</a> </p><br />

    <p>13.   48617   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV-1 and anti-HCMV activity of 1-substituted 3-(3,5-dimethylbenzyl)uracil derivatives</p>

    <p>          Maruyama, T, Kozai, S, Demizu, Y, Witvrouw, M, Pannecouque, C, Balzarini, J, Snoecks, R, Andrei, G, and De, Clercq E</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2006</b>.  54(3): 325-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16508186&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16508186&amp;dopt=abstract</a> </p><br />

    <p>14.   48618   HIV-LS-344; PUBMED-HIV-3/20/2006</p>

    <p class="memofmt1-2">          Rubriflordilactones a and B, two novel bisnortriterpenoids from schisandrarubriflora and their biological activities</p>

    <p>          Xiao, WL, Yang, LM, Gong, NB, Wu, L, Wang, RR, Pu, JX, Li, XL, Huang, SX, Zheng, YT, Li, RT, Lu, Y, Zheng, QT, and Sun, HD</p>

    <p>          Org Lett <b>2006</b>.  8(5): 991-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16494492&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16494492&amp;dopt=abstract</a> </p><br />

    <p>15.   48619   HIV-LS-344; WOS-HIV-3/12/2006</p>

    <p class="memofmt1-2">          Dexelvucitabine - Anti-HIV agent - Reverse transcriptase inhibitor</p>

    <p>          McIntyre, JA and Castaner, J</p>

    <p>          DRUGS OF THE FUTURE <b>2005</b>.  30(12): 1205-1211, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235583600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235583600001</a> </p><br />
    <br clear="all">

    <p>16.   48620   HIV-LS-344; WOS-HIV-3/12/2006</p>

    <p class="memofmt1-2">          Discovery of a highly potent and orally active CCR5 antagonist TAK-652 as an anti-HIV-1 agent: Synthesis and biological activities of 1-benzazocine derivatives containing a sulfoxide moiety</p>

    <p>          Seto, M, Aikawa, K, Aramaki, Y, Miyamoto, N, Kanzaki, N, Kuze, Y, Takashima, K, Baba, M, and Shiraishi, M</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U122-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600088">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600088</a> </p><br />

    <p>17.   48621   HIV-LS-344; WOS-HIV-3/12/2006</p>

    <p class="memofmt1-2">          Prodrugs of 1-(beta-d-dioxolane)thymine (DOT): Synthesis, anti-HIV-1 activity and stability study</p>

    <p>          Liang, YZ, Yadav, V, Schinazi, RF, and Chu, CK</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U168-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600333">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600333</a> </p><br />

    <p>18.   48622   HIV-LS-344; WOS-HIV-3/19/2006</p>

    <p class="memofmt1-2">          In vitro development of resistance to human immunodeficiency virus protease inhibitor GW640385</p>

    <p>          Yates, PJ, Hazen, R, St, Clair M, Boone, L, Tisdale, M, and Elston, RC</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(3): 1092-1095, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300039</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
