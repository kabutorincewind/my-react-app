

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-120.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BkokqQ/Y8jWKj4nD1NWjApLQSG7e/Pfr7ERQOBjk3c0RBpJTtnXjOPdito4ySu1m89K821uGrtOxC6ifS3KBptU6utlVMrXlNmvbqFTDbD89ZtUm2sqgS75WQhM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="22DD3404" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-120-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58469   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-RNA virus activity of polyoxometalates</p>

    <p>          Shigeta, S, Mori, S, Yamase, T, Yamamoto, N, and Yamamoto, N</p>

    <p>          Biomed Pharmacother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16737794&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16737794&amp;dopt=abstract</a> </p><br />

    <p>2.     58470   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-hepatitis B activities of ganoderic acid from Ganoderma lucidum</p>

    <p>          Li, YQ and Wang, SF</p>

    <p>          Biotechnol Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16736272&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16736272&amp;dopt=abstract</a> </p><br />

    <p>3.     58471   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polyomavirus-associated nephropathy: update on antiviral strategies</p>

    <p>          Josephson, MA, Williams, JW, Chandraker, A, and Randhawa, PS</p>

    <p>          Transpl Infect Dis <b>2006</b>.  8(2): 95-101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16734632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16734632&amp;dopt=abstract</a> </p><br />

    <p>4.     58472   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Kinetics and Synergistic Effects of Sirnas Targeting Structural and Replicase Genes of Sars-Associated Coronavirus</p>

    <p>          He, M. <i>et al.</i></p>

    <p>          Febs Letters <b>2006</b>.  580(10): 2414-2420</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237433500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237433500004</a> </p><br />

    <p>5.     58473   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effects of glycosylation and glucose trimming inhibitors on human parainfluenza virus type 3</p>

    <p>          Tanaka, Y, Kato, J, Kohara, M, and Galinski, MS</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16730076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16730076&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     58474   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Analysis of the endocytic pathway mediating the infectious entry of mosquito-borne flavivirus West Nile into Aedes albopictus mosquito (C6/36) cells</p>

    <p>          Chu, JJH, Leong, PWH, and Ng, ML</p>

    <p>          Virology <b>2006</b>.  349(2): 463-475</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4J9VMRV-2/2/61eeb2f9e7e00e2bd0ad898cd7ec1c93">http://www.sciencedirect.com/science/article/B6WXR-4J9VMRV-2/2/61eeb2f9e7e00e2bd0ad898cd7ec1c93</a> </p><br />

    <p>7.     58475   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Drug Discovery Targeting to Viral Proteases</p>

    <p>          Hsu, J. <i>et al.</i></p>

    <p>          Current Pharmaceutical Design <b>2006</b>.  12(11): 1301-1314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237432600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237432600002</a> </p><br />

    <p>8.     58476   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro inhibition of human influenza A virus replication by chloroquine</p>

    <p>          Ooi, EE, Chew, JS, Loh, JP, and Chua, RC</p>

    <p>          Virol J <b>2006</b>.  3(1): 39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16729896&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16729896&amp;dopt=abstract</a> </p><br />

    <p>9.     58477   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-HCV therapies in chimeric scid-Alb/uPA mice parallel outcomes in human clinical application</p>

    <p>          Kneteman, NM, Weiner, AJ, O&#39;connell, J, Collett, M, Gao, T, Aukerman, L, Kovelsky, R, Ni, ZJ, Hashash, A, Kline, J, Hsi, B, Schiller, D, Douglas, D, Tyrrell, DL, and Mercer, DF</p>

    <p>          Hepatology <b>2006</b>.  43(6): 1346-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16729319&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16729319&amp;dopt=abstract</a> </p><br />

    <p>10.   58478   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of thieno[3,2-b]pyrroles as allosteric inhibitors of hepatitis C virus NS5B polymerase</p>

    <p>          Ontoria, Jesus M, Martin Hernando, Jose I, Malancona, Savina, Attenni, Barbara, Stansfield, Ian, Conte, Immacolata, Ercolani, Caterina, Habermann, Jorg, Ponzi, Simona, and Di Filippo, Marcello</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4K128YD-6/2/65f85b1396b7a9ab3f1fd6ad4471cd60">http://www.sciencedirect.com/science/article/B6TF9-4K128YD-6/2/65f85b1396b7a9ab3f1fd6ad4471cd60</a> </p><br />

    <p>11.   58479   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hcv Activity of 4 &#39;-Substituted Ribonucleosides</p>

    <p>          Iversen, P. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A34</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200023</a> </p><br />
    <br clear="all">

    <p>12.   58480   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Flavans from the Leaves of Pithecellobium clypearia</p>

    <p>          Li, Y, Leung, KT, Yao, F, Ooi, LS, and Ooi, VE</p>

    <p>          J Nat Prod <b>2006</b>.  69(5): 833-835</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724853&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724853&amp;dopt=abstract</a> </p><br />

    <p>13.   58481   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nim811: an Hcv Replication Inhibitor of Novel Mechanism, Exhibits Potent Antiviral Activities Alone or in Combination With a Non-Nucleoside Hcv Polymerase Inhibitor</p>

    <p>          Boerner, J. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A34-A35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200024</a> </p><br />

    <p>14.   58482   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          R1479 Is a Highly Selective Inhibitor of Ns5b-Dependent Hcv Replication and Does Not Inhibit Human Dna and Rna Polymerases</p>

    <p>          Ma, H. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200025</a> </p><br />

    <p>15.   58483   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel Small Molecule Inhibitors of Hepatitis B Virus Surface Antigen Secretion</p>

    <p>          Cuconati, A. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A35-A36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200027</a> </p><br />

    <p>16.   58484   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of human coronavirus NL63 infection at early stages of the replication cycle</p>

    <p>          Pyrc, K, Bosch, BJ, Berkhout, B, Jebbink, MF, Dijkman, R, Rottier, P, and van, der Hoek L</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(6): 2000-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723558&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723558&amp;dopt=abstract</a> </p><br />

    <p>17.   58485   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          5-(dimethoxymethyl)-2(&#39;)-deoxyuridine: a novel gem diether nucleoside with anti-orthopoxvirus activity</p>

    <p>          Fan, X, Zhang, X, Zhou, L, Keith, KA, Kern, ER, and Torrence, PF</p>

    <p>          J Med Chem <b>2006</b>.  49(11): 3377-3382</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722657&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722657&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   58486   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of Inhibitors of Ebola Virus With a Subgenomic Replication System</p>

    <p>          Dyall, J. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200035</a> </p><br />

    <p>19.   58487   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of Proline Sulfonamides as Potent and Selective Hepatitis C Virus NS5b Polymerase Inhibitors. Evidence for a New NS5b Polymerase Binding Site</p>

    <p>          Gopalsamy, A, Chopra, R, Lim, K, Ciszewski, G, Shi, M, Curran, KJ, Sukits, SF, Svenson, K, Bard, J, Ellingboe, JW, Agarwal, A, Krishnamurthy, G, Howe, AY, Orlowski, M, Feld, B, O&#39;connell, J, and Mansour, TS</p>

    <p>          J Med Chem <b>2006</b>.  49(11): 3052-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722622&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722622&amp;dopt=abstract</a> </p><br />

    <p>20.   58488   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent Inhibition of Both Hiv and Hcv in Vitro by a Ring-Expanded (&quot;Fat&quot;) Nucleoside: Part I. Mechanistic Studies of Anti-Hiv Activity</p>

    <p>          Zhang, P. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A44-A45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200050</a> </p><br />

    <p>21.   58489   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Inhibition of Maporal Hantavirus</p>

    <p>          Jung, K. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A52-A53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200071">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200071</a> </p><br />

    <p>22.   58490   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, Synthesis, and Biological Evaluation of Novel Nucleoside Phosphoramidates as Potential Anti-Hcv Agents</p>

    <p>          Perrone, P. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200072">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200072</a> </p><br />

    <p>23.   58491   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of Novel Anthranilic Acid Analogs as Hcv Polymerase Inhibitors</p>

    <p>          Curran, K. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200073">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200073</a> </p><br />
    <br clear="all">

    <p>24.   58492   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Optimization of small molecule drugs binding to highly polar target sites: lessons from the discovery and development of neuraminidase inhibitors</p>

    <p>          Klumpp, K and Graves, BJ</p>

    <p>          Curr Top Med Chem <b>2006</b>.  6(5): 423-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16719801&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16719801&amp;dopt=abstract</a> </p><br />

    <p>25.   58493   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Evaluation of Novel Potential Hcv Helicase Inhibitors</p>

    <p>          Brancale, A. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A53-A54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200074">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200074</a> </p><br />

    <p>26.   58494   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hcvns5b Normucleoside Inhibitors Specifically Block Synthesis of Single-Stranded Viral Rna Catalyzed by Hcv Replication Complexes in Vitro</p>

    <p>          Yang, W. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A54-A55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200077</a> </p><br />

    <p>27.   58495   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-SARS drug screening by molecular docking</p>

    <p>          Wei, DQ, Zhang, R, Du, QS, Gao, WN, Li, Y, Gao, H, Wang, SQ, Zhang, X, Li, AX, Sirois, S, and Chou, KC</p>

    <p>          Amino Acids <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16715412&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16715412&amp;dopt=abstract</a> </p><br />

    <p>28.   58496   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent Inhibition of Both Hiv and Hcv in Vitro by a Ring-Expanded (&quot;Fat&quot;) Nucleoside: Part Ii. Mechanistic Studies of Anti-Hcv Activity</p>

    <p>          Zhang, N. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200078">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200078</a> </p><br />

    <p>29.   58497   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and evaluation of acridine- and acridone-based anti-herpes agents with topoisomerase activity</p>

    <p>          Goodell, JR, Madhok, AA, Hiasa, H, and Ferguson, DM</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713270&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713270&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>30.   58498   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Nucleoside Inhibitor of Hepatitis C Virus Replication Efficiently Inhibits the Replication of Flaviviruses in Vitro</p>

    <p>          Leyssen, P. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200142">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200142</a> </p><br />

    <p>31.   58499   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rep 9, a Degenerate Phosphorothioate Oligonucleotide That Inhibits Rift Valley Fever Viral Infection in Vivo</p>

    <p>          Paessler, S. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200145">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200145</a> </p><br />

    <p>32.   58500   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Models for Studying Hepatitis B Virus Drug Resistance</p>

    <p>          Zoulim, F.</p>

    <p>          Seminars in Liver Disease <b>2006</b>.  26(2): 171-180</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237357300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237357300010</a> </p><br />

    <p>33.   58501   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Metabolism, Pharmacokinetics and Mechanisms of Antiviral Activity of Ribavirin Against Hepatitis C Virus</p>

    <p>          Dixit, N. and Perelson, A.</p>

    <p>          Cellular and Molecular Life Sciences <b>2006</b>.  63(7-8): 832-842</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237359800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237359800008</a> </p><br />

    <p>34.   58502   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Arenavirus Z Protein as an Antiviral Target: Virus Inactivation and Protein Ollgomerization by Zinc Finger-Reactive Compounds</p>

    <p>          Garcia, C. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2006</b>.  87: 1217-1228</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237155300019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237155300019</a> </p><br />

    <p>35.   58503   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Roles of neuraminidase in the initial stage of influenza virus infection</p>

    <p>          Ohuchi, M, Asaoka, N, Sakai, T, and Ohuchi, R</p>

    <p>          Microbes Infect <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16682242&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16682242&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>36.   58504   DMID-LS-120; PUBMED-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro efficacies of oseltamivir carboxylate and zanamivir against equine influenza A viruses</p>

    <p>          Yamanaka, T, Tsujimura, K, Kondo, T, and Matsumura, T</p>

    <p>          J Vet Med Sci <b>2006</b>.  68(4): 405-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16679737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16679737&amp;dopt=abstract</a> </p><br />

    <p>37.   58505   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of dengue virus translation and RNA synthesis by a morpholino oligomer targeted to the top of the terminal 3&#39; stem-loop structure</p>

    <p>          Holden, Katherine Lynn, Stein, David A, Pierson, Theodore C, Ahmed, Asim A, Clyde, Karen, Iversen, Patrick L, and Harris, Eva</p>

    <p>          Virology <b>2006</b>.  344(2): 439-452</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4H8MNR4-1/2/84d72913f5bbb6c4b0df0b0ae130df63">http://www.sciencedirect.com/science/article/B6WXR-4H8MNR4-1/2/84d72913f5bbb6c4b0df0b0ae130df63</a> </p><br />

    <p>38.   58506   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-Influenza Agents From Plants and Traditional Chinese Medicine</p>

    <p>          Wang, X. <i>et al.</i></p>

    <p>          Phytotherapy Research <b>2006</b>.  20(5): 335-341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237544600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237544600001</a> </p><br />

    <p>39.   58507   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          White, P. W., Llinas-Brunet, M., and Bos, M.</p>

    <p>          &lt;10 Journal Title&gt; <b>2006</b>.: pp. 65-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7CV8-4JXVCPR-5/2/e793426a86a11ab166e2cf3bf03e899e">http://www.sciencedirect.com/science/article/B7CV8-4JXVCPR-5/2/e793426a86a11ab166e2cf3bf03e899e</a> </p><br />

    <p>40.   58508   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synergistic antiviral activity of acyclovir and vidarabine against herpes simplex virus types 1 and 2 and varicella-zoster virus</p>

    <p>          Suzuki, Mikiko, Okuda, Tomoko, and Shiraki, Kimiyasu</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K2T3YS-1/2/1d8e1b31da03014932ad76621a4e7a3c">http://www.sciencedirect.com/science/article/B6T2H-4K2T3YS-1/2/1d8e1b31da03014932ad76621a4e7a3c</a> </p><br />

    <p>41.   58509   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          l-Nucleoside enantiomers as antivirals drugs: A mini-review</p>

    <p>          Mathe, Christophe and Gosselin, Gilles</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-1/2/7c9e5bd48ebba0d36b3d74cfefe3a1b1">http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-1/2/7c9e5bd48ebba0d36b3d74cfefe3a1b1</a> </p><br />
    <br clear="all">

    <p>42.   58510   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral drugs for cytomegalovirus diseases</p>

    <p>          Biron, Karen K </p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-2/2/ce0b69aeadb891e3e0ca293086c8a5d7">http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-2/2/ce0b69aeadb891e3e0ca293086c8a5d7</a> </p><br />

    <p>43.   58511   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Chemoenzymatic synthesis and application of a sialoglycopolymer with a chitosan backbone as a potent inhibitor of human influenza virus hemagglutination</p>

    <p>          Makimura, Yutaka, Watanabe, Shinya, Suzuki, Takashi, Suzuki, Yasuo, Ishida, Hideharu, Kiso, Makoto, Katayama, Takane, Kumagai, Hidehiko, and Yamamoto, Kenji</p>

    <p>          Carbohydrate Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TFF-4K18VW2-2/2/f46868817f440528409a705035bbec72">http://www.sciencedirect.com/science/article/B6TFF-4K18VW2-2/2/f46868817f440528409a705035bbec72</a> </p><br />

    <p>44.   58512   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Short Enantioselective Pathway for the Synthesis of the Anti-Influenza Neuramidase Inhibitor Oseltamivir From 1,3-Butadiene and Acrylic Acid</p>

    <p>          Yeung, Y., Hong, S., and Corey, E.</p>

    <p>          Journal of the American Chemical Society <b>2006</b>.  128(19): 6310-6311</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237590400021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237590400021</a> </p><br />

    <p>45.   58513   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Purification and characterisation of a protease inhibitor from Streptomyces chromofuscus 34-1 with an antiviral activity</p>

    <p>          Angelova, Lidiya, Dalgalarrondo, Michele, Minkov, Ignat, Danova, Svetla, Kirilov, Nicolai, Serkedjieva, Julia, Chobert, Jean-Marc, Haertle, Thomas, and Ivanova, Iskra</p>

    <p>          Biochimica et Biophysica Acta (BBA) - General Subjects <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1W-4JJGFG2-2/2/acf41b905948327b7df783d188a0498f">http://www.sciencedirect.com/science/article/B6T1W-4JJGFG2-2/2/acf41b905948327b7df783d188a0498f</a> </p><br />

    <p>46.   58514   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Proteomic Profiling of Host Cellular Proteins Incorporated by Severe Acute Respiratory Syndrome (Sars)-Associated Coronavirus Virions: Insights Into Emerging Virus Biology and New Therapeutics Targets</p>

    <p>          Jean, F. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A29-A30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200012</a> </p><br />

    <p>47.   58515   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Sars-Cov Replication by Hydroxyethylrutosides and Mouse Interferon (Muifn-Alpha) in a Mouse Model</p>

    <p>          Barnard, D. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200013</a> </p><br />
    <br clear="all">

    <p>48.   58516   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Small Molecule Inhibitors of Respiratory Syncytial Virus</p>

    <p>          Dyall, J. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200014</a> </p><br />

    <p>49.   58517   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pre-Clinical Development of the Bcnas: the Most Potent Anti-Vzv Agents Reported to Date</p>

    <p>          Mcguigan, C. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200015</a> </p><br />

    <p>50.   58518   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rhinovirus chemotherapy</p>

    <p>          Patick, Amy K</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JRV769-4/2/5fec0069aa053ddad2abc8ee3f6dacf5">http://www.sciencedirect.com/science/article/B6T2H-4JRV769-4/2/5fec0069aa053ddad2abc8ee3f6dacf5</a> </p><br />

    <p>51.   58519   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Foldamer-Based Inhibitors of Cytomegalovirus Entry</p>

    <p>          English, E. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200018</a> </p><br />

    <p>52.   58520   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent Antiviral Activity of Rep9 and Analogs Against Vaginal Hsv-2 Infection</p>

    <p>          Ireland, J. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200021</a> </p><br />

    <p>53.   58521   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity of Novel Isatin Derivatives Against Avian Influenza Virus (H5n1)</p>

    <p>          Selvam, P. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A48-A49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200061">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200061</a> </p><br />

    <p>54.   58522   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparison of the Antiviral Activity of Amantadine Against Hepatitis C Virus of Different Genotypes in Cell-Based Replicon and Infectious Virus Assays</p>

    <p>          Bourne, N. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200079">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200079</a> </p><br />
    <br clear="all">

    <p>55.   58523   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interference of Hepatitis C Virus Replication by Combination of Protein Kinase C-Like 2 Inhibitors and Interferon-Alpha</p>

    <p>          Kim, S., Yun, J., and Oh, J.</p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A56-A57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200082">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200082</a> </p><br />

    <p>56.   58524   DMID-LS-120; EMBASE-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          P2-P4 Macrocyclic inhibitors of hepatitis C virus NS3-4A serine protease</p>

    <p>          Arasappan, Ashok, Njoroge, FGeorge, Chen, Kevin X, Venkatraman, Srikanth, Parekh, Tejal N, Gu, Haining, Pichardo, John, Butkiewicz, Nancy, Prongay, Andrew, Madison, Vincent, and Girijavallabhan, Viyyoor</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4K2SK48-2/2/feba97bacde30f47fe2adcd4b498b62e">http://www.sciencedirect.com/science/article/B6TF9-4K2SK48-2/2/feba97bacde30f47fe2adcd4b498b62e</a> </p><br />

    <p>57.   58525   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity and Metabolic Stability of Branched Methyl Alkoxyalkyl Esters of Cidofovir Against Vaccinia, Cowpox, and Ectromelia Viruses, in Vitro</p>

    <p>          Ruiz, J. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A57-A58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200085">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200085</a> </p><br />

    <p>58.   58526   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development, Validation, and Optimization of a Luminescence-Based High Throughput Screen for Inhibitors of Influenza</p>

    <p>          Noah, J. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200100">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200100</a> </p><br />

    <p>59.   58527   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sar of Alkyloxyphenyl Furano Pyrimidines: Potent and Selective Anti-Vzv Agents</p>

    <p>          Adak, R. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200106">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200106</a> </p><br />

    <p>60.   58528   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Some Modifications to the Bicyclic Pyrimidines: a Novel Class of Anti-Viral Nucleosides</p>

    <p>          Angell, A. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A66-A67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200107">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200107</a> </p><br />
    <br clear="all">

    <p>61.   58529   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sar of Monosubstituted Phenyl Furano Pyrimidine as Potent and Selective Anti Varicella-Zoster Virus (Vzv) Compounds</p>

    <p>          Migliore, M. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200108">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200108</a> </p><br />

    <p>62.   58530   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Murine Cytomegalovirus by Second Generation Ribonucleotide Reductase Inhibitors Didox and Trimidox</p>

    <p>          Inayat, M. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A68-A69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200112">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200112</a> </p><br />

    <p>63.   58531   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Herpesvirus Replication by a Series of Alkoxyalkyl Esters of Purine- and Pyrimidine-Based Nucleoside Phosphonates</p>

    <p>          Hartline, C. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200113">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200113</a> </p><br />

    <p>64.   58532   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Efficacy of St-246 in a Ground Squirrel Model of Severe Monkeypox Virus Infection</p>

    <p>          Byrd, C. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200130">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200130</a> </p><br />

    <p>65.   58533   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity of S22, Natural Herb Extract, Against Influenza a Virus Infection in Vitro and in Vivo</p>

    <p>          Lee, H. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200164">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200164</a> </p><br />

    <p>66.   58534   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Neuraminidase Pharmacophore Model Derived From Diverse Classes of Inhibitors</p>

    <p>          Zhang, J. <i>et al.</i></p>

    <p>          Bioorganic +Acy- Medicinal Chemistry Letters <b>2006</b>.  16(11): 3009-3014</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237407800039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237407800039</a> </p><br />

    <p>67.   58535   DMID-LS-120; WOS-DMID-6/5/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Single Amino Acid Substitution (R441a) in the Receptor-Binding Domain of Sars Coronavirus Spike Protein Disrupts the Antigenic Structure and Binding Activity</p>

    <p>          He, Y., Li, J., and Jiang, S.</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  344(1): 106-113</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237408000017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237408000017</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
