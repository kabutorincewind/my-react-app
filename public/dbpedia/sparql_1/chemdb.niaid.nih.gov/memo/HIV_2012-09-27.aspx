

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-09-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9aG8QLXEPren8z147tD0AM4M4rqtuNlLky6GDXMqqiNd0y9z0gkDB3ZqtzySCU9OSBXtB9XaCPbQ91XHRVblHF27rL2iJRBMfjaNUFXUCvjNGN3uAJ++3I8BGMQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="59B828C0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: September 14 - September 27, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1.<a href="http://www.ncbi.nlm.nih.gov/pubmed/23002902">Bioassay-guided Investigation of the Tanzanian Plant Pyrenacantha kaurabassana for Potential anti-HIV-active Compounds.</a> Omolo, J.J., V. Maharaj, D. Naidoo, T. Klimkait, H.M. Malebo, S. Mtullu, H.V. Lyaruu, and C.B. Koning. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>; PMID[23002902].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">2.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22883027">Arylazolyl(azinyl)thioacetanilides. Part 10: Design, Synthesis and Biological Evaluation of Novel Substituted Imidazopyridinylthioacetanilides as Potent HIV-1 Inhibitors.</a> Li, X., P. Zhan, H. Liu, D. Li, L. Wang, X. Chen, C. Pannecouque, J. Balzarini, E.D. Clercq, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2012. 20(18): p. 5527-5536. PMID[22883027].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22998428">Amniotic Fluid Exhibits an Innate Inhibitory Activity against HIV-1 Replication in Vitro.</a> Farzin, A., B. Ank, P. Boyer, K. Nielsen-Saines, and Y. Bryson. AIDS Research and Human Retroviruses, 2012.</p>

    <p class="plaintext"><b>[Epub ahead of print]</b>; PMID[22998428].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">4.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22871093">Synthesis of Novel Mannoside Glycolipid Conjugates for Inhibition of HIV-1 Trans-infection.</a> Dehuyser, L., E. Schaeffer, O. Chaloin, C.G. Mueller, R. Baati, and A. Wagner. Bioconjugate Chemistry, 2012. 23(9): p. 1731-1739. PMID[22871093].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0914-092712.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307944200002">Synthesis of Some 1-(Flavon-7-yl)-4,5-dihydro-1,2,4-triazin-6(1H)-ones and Related Congeners.</a> Abu-Aisheh, M. N., M. S. Mustafa, M. S. Mubarak, M. M. El-Abadelah, and W. Voelter. Letters in Organic Chemistry, 2012. 9(7): p. 465-473. PMID[000307944200002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307828600024">Synthesis and Evaluation of Hybrid Drugs for a Potential HIV/AIDS-Malaria Combination Therapy.</a> Aminake, M.N., A. Mahajan, V. Kumar, R. Hans, L. Wiesner, D. Taylor, C. de Kock, A. Grobler, P.J. Smith, M. Kirschner, A. Rethwilm, G. Pradel, and K. Chibale. Bioorganic &amp; Medicinal Chemistry, 2012. 20(17): p. 5277-5289. PMID[000307828600024].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307685800009">Effects of Progesterone on the Content of CCR5 and CXCR4 Coreceptors in PBMCs of Seropositive and Exposed but Uninfected Mexican Women to HIV-1.</a> Cabrera-Munoz, E., L.l. Fuentes-Romero, J. Zamora-Chavez, I. Camacho-Arroyo, and L.E. Soto-Ramirez. Journal of Steroid Biochemistry and Molecular Biology, 2012. 132(1-2): p. 66-72. PMID[000307685800009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307871200011">The Development of HEPT-Type HIV Non-Nucleoside Reverse Transcriptase Inhibitors and Its Implications for DABO Family.</a> Chen, W., P. Zhan, J. Wu, Z. Li, and X. Liu. Current Pharmaceutical Design, 2012. 18(27): p. 4165-4186. PMID[000307871200011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308088600022">New anti-HIV Aptamers Based on Tetra-end-linked DNA G-Quadruplexes: Effect of the Base Sequence on anti-HIV Activity.</a> D&#39;Atri, V., G. Oliviero, J. Amato, N. Borbone, S. D&#39;Errico, L. Mayol, V. Piccialli, S. Haider, B. Hoorelbeke, J. Balzarini, and G. Piccialli. Chemical Communications, 2012. 48(76): p. 9516-9518. PMID[000308088600022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307616500043">Synthesis of a New Amphiphilic Glycodendrimer with Antiviral Functionality.</a> Han, S.Q., T. Kanamoto, H. Nakashima, and T. Yoshida. Carbohydrate Polymers, 2012. 90(2): p. 1061-1068. PMID[000307616500043].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307478600019">Discovery of Phenylaminopyridine Derivatives as Novel HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Kim, J., D. Lee, C. Park, W. So, M. Jo, T. Ok, J. Kwon, S. Kong, S. Jo, Y. Kim, J. Choi, H.C. Kim, Y. Ko, I. Choi, Y. Park, J. Yoon, M.K. Ju, J. Kim, S.J. Han, T.H. Kim, J. Cechetto, J. Nam, P. Sommer, M. Liuzzi, J. Lee, and Z. No. ACS Medicinal Chemistry Letters, 2012. 3(8): p. 678-682. PMID[000307478600019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307803300003">Thiazoline Peptides and a Tris-phenethyl urea from Didemnum molle with anti-HIV Activity.</a> Lu, Z.Y., M.K. Harper, C.D. Pond, L.R. Barrows, C.M. Ireland, and R.M. Van Wagoner. Journal of Natural Products, 2012. 75(8): p. 1436-1440. PMID[000307803300003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306946500033">Total Synthesis of Desacetylumuravumbolide, Umuravumbolide and Their Biological Evaluation.</a> Sabitha, G., D.V. Reddy, S.S.S. Reddy, J.S. Yadav, C.G. Kumar, and P. Sujitha. RSC Advances, 2012. 2(18): p. 7241-7247. PMID[000306946500033].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307584300011">Novel Syntheses of Pyrrolo 2,1-a Isoquinolines Via 1,3-Dipolar Cycloaddition between Isoquinoliniums and Alkynes.</a> Shang, Y.J., L.F. Wang, X.W. He, and M. Zhang. RSC Advances, 2012. 2(20): p. 7681-7688. PMID[000307584300011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307877800028">Synthesis and Characterization of Novel N-Acyl Cyclic Urea Derivatives.</a> Yang, T.T. and G.H. Gao. Arkivoc, 2012: p. 304-316. PMID[000307877800028].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306687700058">A Novel Class of anti-HIV Agents with Multiple Copies of Enfuvirtide Enhances Inhibition of Viral Replication and Cellular Transmission in Vitro.</a> Chang, C., J. Hinkula, M. Loo, T. Falkeborn, R.X. Li, T.M. Cardillo, E.A. Rossi, D.M. Goldenberg, and B. Wahren. PLoS One, 2012. 7(7). PMID[000306687700058].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307041600030">Ligand-Free Suzuki Coupling of Arylboronic Acids with Methyl (E)-4-bromobut-2-enoate: Synthesis of Unconventional Cores of HIV-1 Protease Inhibitors.</a> Chiummiento, L., M. Funicello, P. Lupattelli, and F. Tramutola. Organic Letters, 2012. 14(15): p. 3928-3931. PMID[000307041600030].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307433600013">Enantioselective Synthesis of HIV Protease Inhibitor Amprenavir via Co-catalyzed HKR of 2-(1-Azido-2-phenylethyl)oxirane.</a> Gadakh, S.K., R.S. Reddy, and A. Sudalai. Tetrahedron-Asymmetry, 2012. 23(11-12): p. 898-903. PMID[000307433600013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307386000050">Theta-Defensins: Cyclic Peptides with Endless Potential.</a> Lehrer, R.I., A.M. Cole, and M.E. Selsted. Journal of Biological Chemistry, 2012. 287(32): p. 27014-27019. PMID[000307386000050].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307079400001">Synthesis of Novel 2 &#39;-Spirocyclopropyl-5 &#39;-deoxyphosphonic acid furanosyl Nucleoside Analogues as Potent Antiviral Agents.</a> Shen, G.H. and J.H. Hong. Nucleosides Nucleotides &amp; Nucleic Acids, 2012. 31(7): p. 503-521. PMID[000307079400001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0914-092712.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
