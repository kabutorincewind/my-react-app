

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-85.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dzaZOsHhRNe23tFZtt1K8BlhJ3SCEF/GmiQmYopOkd9qSM9GDwlIn48eCunMxNoUwkbslo8EZaPPnW4hAC5LYKQWPhFfog6FRNipE/DTAsEYvjLEDXPvynBr6nc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4AAF6F6D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-85-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56101   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Assessment of Adverse Reactions and Pharmacokinetics of Ribavirin in Combination with Interferon alpha-2b in Patients with Chronic Hepatitis C</p>

    <p>          Uchida, M, Hamada, A, Yamasaki, M, Fujiyama, S, Sasaki, Y, and Saito, H</p>

    <p>          Drug Metab Pharmacokinet <b>2004</b>.  19(6): 438-443</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681898&amp;dopt=abstract</a> </p><br />

    <p>2.     56102   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of CTC-96 for the topical antiviral therapeutic and prophylactic treatment of adenoviruses and their associated diseases</p>

    <p>          Gershon, David </p>

    <p>          PATENT:  WO <b>2005004817</b>  ISSUE DATE:  20050120</p>

    <p>          APPLICATION: 2004  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (Redox Pharmaceutical Corporation Audubon Biomedical Science and Technology Park, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     56103   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity and molecular mechanism of an orally active respiratory syncytial virus fusion inhibitor</p>

    <p>          Cianci, C, Meanwell, N, and Krystal, M</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681582&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681582&amp;dopt=abstract</a> </p><br />

    <p>4.     56104   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Multivalent HSA Conjugates of 3&#39;-Sialyllactose are Potent Inhibitors of Adenoviral Cell Attachment and Infection</p>

    <p>          Johansson, SM, Arnberg, N, Elofsson, M, Wadell, G, and Kihlberg, J</p>

    <p>          Chembiochem <b>2005</b>.  6(2): 358-364</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15678425&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15678425&amp;dopt=abstract</a> </p><br />

    <p>5.     56105   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of modified fluorinated (2&#39;R)-2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methyl nucleoside analogs as antiviral agents</p>

    <p>          Clark, Jeremy</p>

    <p>          PATENT:  WO <b>2005003147</b>  ISSUE DATE:  20050113</p>

    <p>          APPLICATION: 2004  PP: 228 pp.</p>

    <p>          ASSIGNEE:  (Pharmasset, Ltd. Barbados</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     56106   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Chinese medicinal herbs for influenza</p>

    <p>          Chen, X, Wu, T, Liu, G, Wang, Q, Zheng, J, Wei, J, Ni, J, Zhou, L, Duan, X, and Qiao, J</p>

    <p>          Cochrane Database Syst Rev <b>2005</b>.(1): CD004559</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15674953&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15674953&amp;dopt=abstract</a> </p><br />

    <p>7.     56107   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Variable Surface Epitopes in the Crystal Structure of Dengue Virus Type 3 Envelope Glycoprotein</p>

    <p>          Modis, Y. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(2): 1223-1231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226149700056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226149700056</a> </p><br />

    <p>8.     56108   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Korean medicinal plant extracts exhibit antiviral potency against viral hepatitis</p>

    <p>          Jacob, JR, Korba, BE, You, JE, Tennant, BC, and Kim, YH</p>

    <p>          J Altern Complement Med <b>2004</b>.  10(6): 1019-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673997&amp;dopt=abstract</a> </p><br />

    <p>9.     56109   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparison of the Antiviral Activities of Alkoxyalkyl and Alkyl Esters of Cidofovir against Human and Murine Cytomegalovirus Replication In Vitro</p>

    <p>          Wan, WB, Beadle, JR, Hartline, C, Kern, ER, Ciesla, SL, Valiaeva, N, and Hostetler, KY</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(2): 656-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673748&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673748&amp;dopt=abstract</a> </p><br />

    <p>10.   56110   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Triple Combination of Thymalfasin, Peginterferon Alfa-2a and Ribavirin in Patients With Chronic Hepatitis C Who Have Failed Prior Interferon and Ribavirin Treatment: 24-Week Interim Results of a Pilot Study</p>

    <p>          Poo, J. <i>et al.</i></p>

    <p>          Journal of Gastroenterology and Hepatology <b>2004</b>.  19(12): S79-S81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226116000025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226116000025</a> </p><br />

    <p>11.   56111   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Conservation of Amino Acids in Human Rhinovirus 3C Protease Correlates with Broad-Spectrum Antiviral Activity of Rupintrivir, a Novel Human Rhinovirus 3C Protease Inhibitor</p>

    <p>          Binford, SL, Maldonado, F, Brothers, MA, Weady, PT, Zalman, LS, Meador, JW 3rd, Matthews, DA, and Patick, AK</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(2): 619-626</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673742&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673742&amp;dopt=abstract</a> </p><br />

    <p>12.   56112   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of some novel isatin derivatives</p>

    <p>          Selvam, P, Rajasekaran, A, Dharamsi, ALaeeque, Ahmed, K, Musthafa, SMohammed, Poornima, K, Pournami, KA, Murugesh, N, Chandramohan, M, and De Clercq, E</p>

    <p>          Asian Journal of Chemistry <b>2005</b>.  17(1): 443-448</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   56113   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peginterferon-Alpha-2a (40kd) Plus Ribavirin - a Review of Its Use in Hepatitis C Virus and Hiv Co-Infection</p>

    <p>          Plosker, G. and Keating, G.</p>

    <p>          Drugs <b>2004</b>.  64(24): 2823-2843</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225987200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225987200008</a> </p><br />

    <p>14.   56114   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of novel acyclic nucleosides in the 5-alkynyl- and 6-alkylfuro[2,3-d]pyrimidine series</p>

    <p>          Amblard, F, Aucagne, V, Guenot, P, Schinazi, RF, and Agrofoglio, LA</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(4): 1239-48</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15670933&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15670933&amp;dopt=abstract</a> </p><br />

    <p>15.   56115   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Disialoundecasaccharide chain asparagine/fatty acid amide and medical drug containing the same</p>

    <p>          Kajihara, Yasuhiro, Maeda, Hiroaki, and Fukae, Kazuhiro</p>

    <p>          PATENT:  WO <b>2005000906</b>  ISSUE DATE:  20050106</p>

    <p>          APPLICATION: 2004  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (Otsuka Chemical Co., Ltd. Japan and Sanyo Chemical Industries, Ltd.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   56116   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virtual Screening of Novel Noncovalent Inhibitors for SARS-CoV 3C-like Proteinase</p>

    <p>          Liu, Z, Huang, C, Fan, K, Wei, P, Chen, H, Liu, S, Pei, J, Shi, L, Li, B, Yang, K, Liu, Y, and Lai, L</p>

    <p>          J Chem Inf Comput Sci <b>2005</b>.  45(1): 10-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15667124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15667124&amp;dopt=abstract</a> </p><br />

    <p>17.   56117   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of peptidyl protease inhibitors for coronaviruses and SARS-CoV</p>

    <p>          Cai, Sui Xiong, Kemnitzer, William E, Zhang, Hong, and Zhang, Han-Zhong</p>

    <p>          PATENT:  WO <b>2004101742</b>  ISSUE DATE:  20041125</p>

    <p>          APPLICATION: 2004  PP: 64 pp.</p>

    <p>          ASSIGNEE:  (Cytovia, Inc USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.   56118   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Assays for glucosidase inhibitors with potential antiviral activities: secreted alkaline phosphatase as a surrogate marker</p>

    <p>          Norton, PA, Conyers, B, Gong, Q, Steel, LF, Block, TM, and Mehta, AS</p>

    <p>          J Virol Methods <b>2005</b>.  124(1-2):  167-72</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664065&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664065&amp;dopt=abstract</a> </p><br />

    <p>19.   56119   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virus-encoded chemokine receptors - putative novel antiviral drug targets</p>

    <p>          Rosenkilde, Mette M</p>

    <p>          Neuropharmacology <b>2005</b>.  48(1): 1-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   56120   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral compounds derived from naturally occurring proteins</p>

    <p>          Pellegrini, Antonio and Engels, Monika</p>

    <p>          Current Medicinal Chemistry: Anti-Infective Agents <b>2005</b>.  4(1): 55-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   56121   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Genomic and bioinformatics analysis of HAdV-7, a human adenovirus of species B1 that causes acute respiratory disease: implications for vector development in human gene therapy</p>

    <p>          Purkayastha, A, Su, J, Carlisle, S, Tibbetts, C, and Seto, D</p>

    <p>          Virology <b>2005</b>.  332(1): 114-129</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15661145&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15661145&amp;dopt=abstract</a> </p><br />

    <p>22.   56122   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          (Benzoylamino)isoquinolinecarboxamide derivatives as anti-coronavirus drugs</p>

    <p>          Fujii, Nobutaka and Yamamoto, Naoki</p>

    <p>          PATENT:  WO <b>2005004868</b>  ISSUE DATE:  20050120</p>

    <p>          APPLICATION: 2004  PP: 25 pp.</p>

    <p>          ASSIGNEE:  (Japan)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   56123   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Severe acute respiratory syndrome coronavirus infection of golden syrian hamsters</p>

    <p>          Roberts, Anjeanette, Vogel, Leatrice, Guarner, Jeannette, Hayes, Norman, Murphy, Brian, Zaki, Sherif, and Subbarao, Kanta</p>

    <p>          Journal of Virology <b>2005</b>.  79(1): 503-511</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   56124   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of interaction of coronavirus intergenic sequence with polymerase complex</p>

    <p>          Lai, Derhsing and Phounsavan, Say Fone</p>

    <p>          PATENT:  WO <b>2005000234</b>  ISSUE DATE:  20050106</p>

    <p>          APPLICATION: 2004  PP: 42 pp.</p>

    <p>          ASSIGNEE:  (Sars Scientific Corp., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   56125   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Application of human Brd4 protein fusion with E2 protein from papilloma virus or herpes virus for prevention of viral infections or treatment of viral diseases</p>

    <p>          You, Jianxin and Howley, Peter M</p>

    <p>          PATENT:  WO <b>2005002526</b>  ISSUE DATE:  20050113</p>

    <p>          APPLICATION: 2004  PP: 175 pp.</p>

    <p>          ASSIGNEE:  (President and Fellows of Harvard College, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   56126   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of helioxanthin analogues</p>

    <p>          Yeo, H, Li, Y, Fu, L, Zhu, JL, Gullen, EA, Dutschman, GE, Lee, Y, Chung, R, Huang, ES, Austin, DJ, and Cheng, YC</p>

    <p>          J Med Chem <b>2005</b>.  48(2): 534-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15658867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15658867&amp;dopt=abstract</a> </p><br />

    <p>27.   56127   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of virus production in JC virus-infected cells by postinfection RNA interference</p>

    <p>          Orba, Yasuko, Sawa, Hirofumi, Iwata, Hiroshi, Tanaka, Shinya, and Nagashima, Kazuo</p>

    <p>          Journal of Virology <b>2004</b>.  78(13): 7270-7273</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   56128   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Cis-Substituted Cyclohexenyl and Cyclohexanyl Nucleosides</p>

    <p>          Barral, K, Courcambeck, J, Pepe, G, Balzarini, J, Neyts, J, De Clercq, E, and Camplo, M</p>

    <p>          J Med Chem <b>2005</b>.  48(2): 450-456</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15658858&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15658858&amp;dopt=abstract</a> </p><br />

    <p>29.   56129   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Unedited Inhibition of Hbv Replication by Apobec3g</p>

    <p>          Seppen, J</p>

    <p>          Journal of Hepatology <b>2004</b>.  41(6): 1068-1069</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225915600030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225915600030</a> </p><br />

    <p>30.   56130   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of tetrahydrocarbazole derivatives as human papillomaviruses inhibitors</p>

    <p>          Boggs, Sharon Davis, Gudmundsson, Kristjan S, Richardson, Leah D&#39;Aurora, and Sebahar, Paul Richard</p>

    <p>          PATENT:  WO <b>2004110999</b>  ISSUE DATE:  20041223</p>

    <p>          APPLICATION: 2004  PP: 69 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   56131   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The road to new antiviral therapies</p>

    <p>          Jerome, Keith R</p>

    <p>          Clinical and Applied Immunology Reviews <b>2005</b>.  5(1): 65-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   56132   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Aminothiazole Inhibitors of Hcv Rna Polymerase</p>

    <p>          Shipps, G. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(1): 115-119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225773600021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225773600021</a> </p><br />

    <p>33.   56133   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          HCV regulated proteins and anti-hepatitis C virus compounds</p>

    <p>          Berndt, Peter, Kilby, Peter Michael, and Rugman, Paul</p>

    <p>          PATENT:  EP <b>1493750</b>  ISSUE DATE: 20050105</p>

    <p>          APPLICATION: 2004-15098  PP: 346 pp.</p>

    <p>          ASSIGNEE:  (F. Hoffmann-La Roche AG, Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   56134   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel HCV shorter form core+1 proteins and polynucleotides for prophylaxis and diagnosis of HCV infections and for screening of anti-HCV agents</p>

    <p>          Mavromara, Penelope and Niki, Vassilaki</p>

    <p>          PATENT:  EP <b>1493749</b>  ISSUE DATE: 20050105</p>

    <p>          APPLICATION: 2003-29532  PP: 80 pp.</p>

    <p>          ASSIGNEE:  (Institut Pasteur, Fr. and Institut Pasteur Hellenique)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   56135   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of SARS-CoV replication by siRNA</p>

    <p>          Wu, CJ, Huang, HW, Liu, CY, Hong, CF, and Chan, YL</p>

    <p>          Antiviral Res <b>2005</b>.  65(1): 45-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15652970&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15652970&amp;dopt=abstract</a> </p><br />

    <p>36.   56136   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Enhancement of antiviral activity against hepatitis C virus in vitro by interferon combination therapy</p>

    <p>          Okuse, C, Rinaudo, JA, Farrar, K, Wells, F, and Korba, BE</p>

    <p>          Antiviral Res <b>2005</b>.  65(1): 23-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15652968&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15652968&amp;dopt=abstract</a> </p><br />

    <p>37.   56137   DMID-LS-85; SCIFINDER-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Crystals of hepatitis C virus polymerase and/or hepatitis C virus polymerase-like proteins and their use for modeling and design of potential antiviral agents</p>

    <p>          Finzel, Barry C</p>

    <p>          PATENT:  WO <b>2005001417</b>  ISSUE DATE:  20050106</p>

    <p>          APPLICATION: 2004  PP: 459 pp.</p>

    <p>          ASSIGNEE:  (Pharmacia &amp; Upjohn Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>38.   56138   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Increased sensitivity of SARS-coronavirus to a combination of human type I and type II interferons</p>

    <p>          Scagnolari, C, Vicenzi, E, Bellomi, F, Stillitano, MG, Pinna, D, Poli, G, Clementi, M, Dianzani, F, and Antonelli, G</p>

    <p>          Antivir Ther <b>2004</b>.  9(6): 1003-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15651759&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15651759&amp;dopt=abstract</a> </p><br />

    <p>39.   56139   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Effect of Hepatitis C on Progression to Aids Before and After Highly Active Antiretroviral Therapy</p>

    <p>          Dorrucci, M. <i>et al.</i></p>

    <p>          Aids <b>2004</b>.  18(17): 2313-2318</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225656900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225656900011</a> </p><br />

    <p>40.   56140   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Adefovir Dipivoxil as the Rescue Therapy for Lamivudine-Resistant Hepatitis B Post Liver Transplant</p>

    <p>          Wai, C. <i>et al.</i></p>

    <p>          Transplantation Proceedings <b>2004</b>.  36(8): 2313-2314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225476300040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225476300040</a> </p><br />

    <p>41.   56141   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Efficient inhibition of hepatitis B virus infection by acylated peptides derived from the large viral surface protein</p>

    <p>          Gripon, P, Cannie, I, and Urban, S</p>

    <p>          J Virol <b>2005</b>.  79(3): 1613-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650187&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650187&amp;dopt=abstract</a> </p><br />

    <p>42.   56142   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatotoxicity of antiretrovirals : incidence, mechanisms and management</p>

    <p>          Nunez, M and Soriano, V</p>

    <p>          Drug Saf <b>2005</b>.  28(1): 53-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15649105&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15649105&amp;dopt=abstract</a> </p><br />

    <p>43.   56143   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Analogues of acyclic nucleosides derived from tris-(hydroxymethyl)phosphine oxide or bis-(hydroxymethyl)phosphinic acid coupled to DNA nucleobases</p>

    <p>          Nawrot, B, Michalak, O, De Clercq, E, and Stec, WJ</p>

    <p>          Antivir Chem Chemother <b>2004</b>.  15(6): 319-328</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646645&amp;dopt=abstract</a> </p><br />

    <p>44.   56144   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Treatment With Pegylated Interferon and Ribavarin in Hcv Infection With Genotype 2 or 3 for 14 Weeks: a Pilot Study</p>

    <p>          Dalgard, O. <i>et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(6): 1260-1265</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225583800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225583800006</a> </p><br />

    <p>45.   56145   DMID-LS-85; PUBMED-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of chinonin against herpes simplex virus</p>

    <p>          Anon <b>2004</b>.  24(5): 521-524</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15641710&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15641710&amp;dopt=abstract</a> </p><br />

    <p>46.   56146   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure-Based Preliminary Analysis of Immunity and Virulence of Sars Coronavirus</p>

    <p>          Li, Y. <i>et al.</i></p>

    <p>          Viral Immunology <b>2004</b>.  17(4): 528-534</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226043900007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226043900007</a> </p><br />

    <p>47.   56147   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Inhibition of Sars Virus Replication by Human Interferons</p>

    <p>          Dahl, H., Linde, A., and Strannegard, O.</p>

    <p>          Scandinavian Journal of Infectious Diseases <b>2004</b>.  36(11-12): 829-831</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226055700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226055700009</a> </p><br />

    <p>48.   56148   DMID-LS-85; WOS-DMID-2/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Anti-Hsv-2 Activity and Mechanism of Action of Proanthocyanidin a-1 From Vaccinium Vitis-Idaea</p>

    <p>          Cheng, H. <i>et al.</i></p>

    <p>          Journal of the Science of Food and Agriculture <b>2005</b>.  85(1): 10-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226040500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226040500003</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
