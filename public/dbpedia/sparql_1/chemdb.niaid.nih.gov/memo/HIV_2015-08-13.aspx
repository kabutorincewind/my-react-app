

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-08-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="qt0DwXHZDyPsO2VVMuniB7eLNPKefpdUDLrc6fhCkkxUrnt8vQCj7KQWmBpAsu7AinzzTCotdsdqBfIuFF9rfoQ61ILFEDtplru5EZSrG4zOFsRgV5izd9rSnRE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F08E859D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 31 - August 13, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26263487">The Novel Cyclophilin Inhibitor CPI-431-32 Concurrently Blocks HCV and HIV-1 Infections via a Similar Mechanism of Action.</a> Gallay, P.A., M.D. Bobardt, U. Chatterji, D.J. Trepanier, D. Ure, C. Ordonez, and R. Foster. Plos One, 2015. 10(8): p. e0134707. PMID[26263487].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26055365">The HEPT Analogue WPR-6 is Active against a Broad Spectrum of Nonnucleoside Reverse Transcriptase Drug-resistant HIV-1 Strains of Different Serotypes.</a> Xu, W., J. Zhao, J. Sun, Q. Yin, Y. Wang, Y. Jiao, J. Liu, S. Jiang, Y. Shao, X. Wang, and L. Ma. Antimicrobial Agents and Chemotherapy, 2015. 59(8): p. 4882-4888. PMID[26055365].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26247470">Cenicriviroc, a Novel CCR5 (R5) and CCR2 Antagonist, Shows in Vitro Activity against R5 Tropic HIV-2 Clinical Isolates.</a> Visseaux, B., C. Charpentier, G. Collin, M. Bertine, G. Peytavin, F. Damond, S. Matheron, E. Lefebvre, F. Brun-Vezinet, and D. Descamps. Plos One, 2015. 10(8): p. e0134904. PMID[26247470].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26162497">Hybrid Chemistry. Part 4: Discovery of Etravirine-VRX-480773 Hybrids as Potent HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Wan, Z.Y., Y. Tao, Y.F. Wang, T.Q. Mao, H. Yin, F.E. Chen, H.R. Piao, E. De Clercq, D. Daelemans, and C. Pannecouque. Bioorganic &amp; Medicinal Chemistry, 2015. 23(15): p. 4248-4255. PMID[26162497].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0731-081315.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000358415100012">Synthesis, Reactions, and Biological Activity of Some Triazine Derivatives Containing Sulfa Drug Moieties.</a> Aly, M.R.E., A.A. Gobouri, S.H.A. Hafez, and H.A. Saad. Russian Journal of Bioorganic Chemistry, 2015. 41(4): p. 437-450. PMID[000358415100012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357992700120">Natural Plant Alkaloid (Emetine) Inhibits HIV-1 Replication by Interfering with Reverse Transcriptase Activity.</a> Chaves Valadao, A.L., C.M. Abreu, J.Z. Dias, P. Arantes, H. Verli, A. Tanuri, and R.S. de Aguiar. Molecules, 2015. 20(6): p. 11474-11489. PMID[000357992700120].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357468200015">Design and Synthesis of 5-Chloro-2-hydroxy-3-triazolylbenzoic acids as HIV Integrase Inhibitors.</a> Chen, J., C.F. Liu, C.W. Yang, C.C. Zeng, W. Liu, and L.M. Hu. Medicinal Chemistry Research, 2015. 24(7): p. 2950-2959. PMID[000357468200015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>
    
    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357965900005">Pyrazolo-piperidines Exhibit Dual Inhibition of CCR5/CXCR4 HIV Entry and Reverse Transcriptase.</a> Cox, B.D., A.R. Prosser, Y.N. Sun, Z.F. Li, S. Lee, M.B. Huang, V.C. Bond, J.P. Snyder, M. Krystal, L.J. Wilson, and D.C. Liotta. ACS Medicinal Chemistry Letters, 2015. 6(7): p. 753-757. PMID[000357965900005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000358346600001">Alpha-Aminoisobutyric acid Incorporation Induces Cell Permeability and Antiviral Activity of HIV-1 Major Homology Region Fragments.</a> Lampel, A., E. Elis, T. Guterman, S. Shapira, P. Marco, E. Bacharach, and E. Gazit. Chemical Communications, 2015. 51(62): p. 12349-12352. PMID[000358346600001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000358245500020">Paleo-Soraphens: Chemical Total Syntheses and Biological Studies.</a> Lu, H.H., B. Hinkelmann, T. Tautz, J. Li, F. Sasse, R. Franke, and M. Kalesse. Organic &amp; Biomolecular Chemistry, 2015. 13(29): p. 8029-8036. PMID[000358245500020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000358210400001">Synthesis of Some New 2-Amino-6-thiocyanato benzothiazole Derivatives Bearing 2,4-Thiazolidinediones and Screening of Their in Vitro Antimicrobial, Antitubercular and Antiviral Activities.</a> Shaikh, F.M., N.B. Patel, G. Sanna, B. Busonera, P. La Colla, and D.P. Rajani. Medicinal Chemistry Research, 2015. 24(8): p. 3129-3142. PMID[000358210400001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357572000018">Ionic Derivatives of Betulinic acid Exhibit Antiviral Activity against Herpes Simplex Virus Type-2 (HSV-2), but not HIV-1 Reverse Transcriptase.</a> Visalli, R.J., H. Ziobrowski, K.R. Badri, J.J. He, X.G. Zhang, S.R. Arumugam, and H. Zhao. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(16): p. 3168-3171. PMID[000357572000018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357572000031">The Human Immunodeficiency Virus-Reverse Transcriptase Inhibition Activity of Novel Pyridine/Pyridinium-type Fullerene Derivatives.</a>Yasuno, T., T. Ohe, K. Takahashi, S. Nakamura, and T. Mashino. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(16): p. 3226-3229. PMID[000357572000031].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0731-081315.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150729&amp;CC=CN&amp;NR=104803981A&amp;KC=A">Piperidine-4-amino diaryl pyrimidine Derivatives, and Their Preparation Method and Application for Treating AIDS.</a> Chen, F. and Z. Wan. Patent. 2015. 2015-10195926 104803981: 16pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150625&amp;CC=WO&amp;NR=2015089847A1&amp;KC=A1">Preparation of Spirocyclic Heterocycle Derivatives for Use as HIV Integrase Inhibitors.</a> Graham, T.H., W. Liu, T. Yu, Y. Zhang, S.T. Waddell, J.S. Wai, P.J. Coleman, J.M. Sanders, M.W. Embrey, A. Walji, R.D. Ferguson, II, C.N.D. Marco, T. Steele, L. Hu, and X. Peng. Patent. 2015. 2013-CN90156 2015089847: 97pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150625&amp;CC=WO&amp;NR=2015095258A1&amp;KC=A1">Preparation of Spirocyclic Heterocycle Derivatives for Use as HIV Integrase Inhibitors.</a> Graham, T.H., W. Liu, T. Yu, Y. Zhang, S.T. Waddell, J.S. Wai, P.J. Coleman, J.M. Sanders, M.W. Embrey, A.M. Walji, R.D. Ferguson, II, C.N.D. Marco, T.G. Steele, L. Hu, and X. Peng. Patent. 2015. 2014-US70712 2015095258: 138pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150709&amp;CC=WO&amp;NR=2015101265A1&amp;KC=A1">Preparation of 1-(3-Aminopropyl)-substituted Cyclic Amines as CCR5 Antagonists.</a> Liu, H., B. Wu, Y. Zheng, X. Xie, H. Jiang, P. Peng, R. Luo, J. Li, J. Li, Y. Zhu, Y. Chen, H. Zhang, L. Yang, Y. Zhou, and K. Chen. Patent. 2015. 2014-CN95421 2015101265: 115pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150625&amp;CC=WO&amp;NR=2015090201A1&amp;KC=A1">Use of Isoflavones for the Treatment of Retroviral Infection.</a> Liu, L., Z. Chen, J. Chen, J. Liang, X. Zheng, D. Wang, and D.P. Yang. Patent. 2015. 2014-CN94083 2015090201: 49pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150625&amp;CC=WO&amp;NR=2015095265A1&amp;KC=A1">5-Heteroarylmorpholine-amide Derivatives as HIV Protease Inhibitors and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of HIV Infection and AIDS.</a> McCauley, J.A., C. Beaulieu, D.J. Bennett, C.J. Bungard, S. Crane, T.J. Greshock, M.K. Holloway, H. Juteau, D. McKay, C. Molinaro, O.M. Moradei, C. Nadeau, D. Simard, S. Tummanapalli, V.L. Truong, K.M. Villeneuve, and P.D. Williams. Patent. 2015. 2014-US70725 2015095265: 137pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0731-081315.</p>

    <br />

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150625&amp;CC=WO&amp;NR=2015095276A1&amp;KC=A1">HIV Protease Inhibitors.</a> McCauley, J.A., C. Beaulieu, D.J. Bennett, C.J. Bungard, S. Crane, T.J. Greshock, M.K. Holloway, D. McKay, C. Molinaro, O.M. Moradei, S. Tummanapalli, V.L. Truong, and P.D. Williams. Patent. 2015. 2014-US70748 2015095276: 86pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0731-081315.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
