

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-11-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="68Rb4wmPQVpzCGicDmmRWKEb7u7vAGziT9135kej8h8mwEK5cypckNqFVZKR8yYE0uitfWcWsdprfCd8HubVoprXAeAQp9vzlO2fX+GYZwguLM9fEu7K9jkha6I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9744000B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">October 29 - November 11, 2010</p>

    <p class="memofmt2-2">Human Cytomegalovirus (HCMV)</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21059895">Noncytotoxic Inhibition of Cytomegalovirus Replication through Nk Cell Protease Granzyme M-Mediated Cleavage of Viral Phosphoprotein 71.</a> van Domselaar, R., L.E. Philippen, R. Quadir, E.J. Wiertz, J.A. Kummer, and N. Bovenschen. Journal of immunology, 2010. <b>[Epub ahead of print]</b>; PMID[21059895].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21053034">Identification of Novel 5-Hydroxy-1h-Indole-3-Carboxylates with Anti-Hbv Activities Based on 3d Qsar Studies.</a> Chai, H.F., X.X. Liang, L. Li, C.S. Zhao, P. Gong, Z.J. Liang, W.L. Zhu, H.L. Jiang, and C. Luo. Journal of molecular modeling, 2010. <b>[Epub ahead of print]</b>; PMID[21053034].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21031619">In Vitro Antiviral Activity of Lutein against Hepatitis B Virus.</a> Pang, R., J.Y. Tao, S.L. Zhang, L. Zhao, X. Yue, Y.F. Wang, P. Ye, J.H. Dong, Y. Zhu, and J.G. Wu,. Phytotherapy research, 2010. <b>24</b>(11): p. 1627-30; PMID[21031619].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21060866">Deb025 (Alisporivir) Inhibits Hepatitis C Virus Replication by Preventing a Cyclophilin a Induced Cis-Trans Isomerisation in Domain Ii of Ns5a.</a> Coelmont, L., X. Hanoulle, U. Chatterji, C. Berger, J. Snoeck, M. Bobardt, P. Lim, I. Vliegen, J. Paeshuyse, G. Vuagniaux, A.M. Vandamme, R. Bartenschlager, P. Gallay, G. Lippens, and J. Neyts. Plos One, 2010. <b>5</b>(10): p. e13687; PMID[21060866].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21044142">17beta-Estradiol Inhibits the Production of Infectious Particles of Hepatitis C Virus.</a> Hayashida, K., I. Shoji, L. Deng, D.P. Jiang, Y.H. Ide, and H. Hotta. Microbiology and immunology, 2010. <b>54</b>(11): p. 684-90; PMID[21044142].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21040279">Cyclosporin a Inhibits Hepatitis C Virus Replication and Restores Interferon-Alpha Expression in Hepatocytes.</a> Liu, J.P., L. Ye, X. Wang, J.L. Li, and W.Z. Ho. Transplant infectious disease, 2010. <b>[Epub ahead of print]</b>; PMID[21040279].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21036379">Antiviral Activities of Isg20 in Positive-Strand Rna Virus Infections.</a> Zhou, Z., N. Wang, S.E. Woodson, Q. Dong, J. Wang, Y. Liang, R. Rijnbrand, L. Wei, J.E. Nichols, J.T. Guo, M.R. Holbrook, S.M. Lemon, and K. Li. Virology, 2010. <b>[Epub ahead of print]</b>; PMID[21036379].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21033671">2&#39;-Deoxy-2&#39;-Spirocyclopropylcytidine Revisited: A New and Selective Inhibitor of the Hepatitis C Virus Ns5b Polymerase.</a> Jonckers, T.H., T.I. Lin, C. Buyck, S. Lachau-Durand, K. Vandyck, S. Van Hoof, L.A. Vandekerckhove, L. Hu, J.M. Berke, L. Vijgen, L.L. Dillen, M.D. Cummings, H. de Kock, M. Nilsson, C. Sund, C. Rydegard, B. Samuelsson, A. Rosenquist, G. Fanning, K. Van Emelen, K. Simmen, and P. Raboisson. Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[21033671].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1029-111110.</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283388400013">Inhibition of Protease-Inhibitor-Resistant Hepatitis C Virus Replicons and Infectious Virus by Intracellular Intrabodies.</a> Gal-Tanamy, M., R. Zemel, L. Bachmatov, R.K. Jangra, A. Shapira, R.A. Villanueva, M. Yi, S.M. Lemon, I. Benhar, and R. Tur-Kaspa. Antiviral Research, 2010. <b>88</b>(1): p. 95-106; ISI[000283388400013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282904200001">3-O-Arylmethylgalangin, a Novel Isostere for Anti-Hcv 1,3-Diketoacids (Dkas).</a> Lee, H.S., K.S. Park, B. Lee, D.E. Kim, and Y. Chong. Bioorganic &amp; Medicinal Chemistry, 2010. <b>18</b>(21): p. 7331-7337; ISI[000282904200001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283052900092">Antiviral Activity of 2,3 &#39;-Anhydro and Related Pyrimidine Nucleosides against Hepatitis B Virus.</a> Srivastav, N.C., M. Mak, B. Agrawal, D.L.J. Tyrrell, and R. Kumar. Bioorganic &amp; Medicinal Chemistry Letters, 2010. <b>20</b>(22): p. 6790-6793; ISI[000283052900092].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1029-111110.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282750300246">L-Ficolin Binds and Reduces Infectivities of Hepatitis C Virus and Functions as an Antiviral Opsonin.</a> Zhang, X.L. and Y.L. Zhao. Meeting of the Society for Glycobiology 2010: Oxford Univ Press.</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1029-111110. ISI[000282750300246].</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
