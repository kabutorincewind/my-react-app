

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-311.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nAIhYznQJpy6zfaLIs9mCdnBtJ2W9IAFUlC0l3v9t6VJRzVjpIjXcaCONVnQSVr1ajxkAJp5Vg2G9JOj8i4jfdc+I5l7ipAfscZR9TlWVGpb7LVRndOTCLYVSTM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="34D7A3D6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-311-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44502   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Structure of shikimate kinase from Mycobacterium tuberculosis reveals the binding of shikimic acid</p>

    <p>          Pereira, JH, de Oliveira, JS, Canduri, F, Dias, MV, Palma, MS, Basso, LA, Santos, DS, and de Azevedo, WF Jr</p>

    <p>          Acta Crystallogr D Biol Crystallogr <b>2004</b>.  60(Pt 12 Pt 2): 2310-2319</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15583379&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15583379&amp;dopt=abstract</a> </p><br />

    <p>2.         44503   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          A new nonadride derivative, dihydroepiheveadride, as characteristic antifungal agent against filamentous fungi, isolated from unidentified fungus IFM 52672</p>

    <p>          Hosoe, T, Fukushima, K, Itabashi, T, Nozawa, K, Takizawa, K, Okada, K, Takaki, GM, and Kawai, K</p>

    <p>          J Antibiot (Tokyo) <b>2004</b>.  57(9): 573-578</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15580958&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15580958&amp;dopt=abstract</a> </p><br />

    <p>3.         44504   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Microsporidia: How can they invade other cells?</p>

    <p>          Franzen, Caspar</p>

    <p>          Trends in Parasitology <b>2004</b>.  20(6): 275-279</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>4.         44505   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Three Properties of the Hepatitis C Virus RNA Genome Related to Antiviral Strategies Based on RNA-Therapeutics: Variability, Structural Conformation and tRNA Mimicry</p>

    <p>          Gomez, J, Nadal, A, Sabariegos, R, Beguiristain, N, Martell, M, and Piron, M</p>

    <p>          Curr Pharm Des <b>2004</b>.  10(30): 3741-3756</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579068&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579068&amp;dopt=abstract</a> </p><br />

    <p>5.         44506   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Virus attachment and entry offer numerous targets for antiviral therapy</p>

    <p>          Altmeyer, R</p>

    <p>          Curr Pharm Des <b>2004</b>.  10(30): 3701-3712</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579065&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579065&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44507   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          New acridone inhibitors of human herpes virus replication</p>

    <p>          Bastow, KF</p>

    <p>          Curr Drug Targets Infect Disord <b> 2004</b>.  4(4): 323-330</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578973&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578973&amp;dopt=abstract</a> </p><br />

    <p>7.         44508   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          IPC synthase as a useful target for antifungal drugs</p>

    <p>          Sugimoto, Y, Sakoh, H, and Yamada, K</p>

    <p>          Curr Drug Targets Infect Disord <b> 2004</b>.  4(4): 311-322</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578972&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578972&amp;dopt=abstract</a> </p><br />

    <p>8.         44509   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Functional demonstration of reverse transsulphuration in the mycobacterium tuberculosis complex reveals that methionine is the preferred sulphur source for pathogenic mycobacteria</p>

    <p>          Wheeler, PR, Coldham, NG, Keating, L, Gordon, SV, Wooff, EE, Parish, T, and Hewinson, RG</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15576367&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15576367&amp;dopt=abstract</a> </p><br />

    <p>9.         44510   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Anti-tuberculosis activity of some N-pentopyranosylamines</p>

    <p>          Moczulska, A</p>

    <p>          Acta Pol Pharm <b>2004</b>.  61(4): 259-262</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15575591&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15575591&amp;dopt=abstract</a> </p><br />

    <p>10.       44511   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          In vitro activity of bergamot natural essence and furocoumarin-free and distilled extracts, and their associations with boric acid, against clinical yeast isolates</p>

    <p>          Romano, L, Battaglia, F, Masucci, L, Sanguinetti, M, Posteraro, B, Plotti, G, Zanetti, S, and Fadda, G</p>

    <p>          J Antimicrob Chemother <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15574476&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15574476&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44512   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Emerging therapies for herpes viral infections (types 1 - 8)</p>

    <p>          Chakrabarty, A, Pang, KR, Wu, JJ, Narvaez, J, Rauser, M, Huang, DB, Beutner, KR, and Tyring, SK</p>

    <p>          Expert Opin Emerg Drugs <b>2004</b>.  9(2): 237-256</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15571482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15571482&amp;dopt=abstract</a> </p><br />

    <p>12.       44513   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Antibacterial activity of naphthoquinones and triterpenoids from eucleanatalensis root bark</p>

    <p>          Weigenand, O,  Hussein, AA, Lall, N, and Meyer, JJ</p>

    <p>          J Nat Prod <b>2004</b>.  67(11): 1936-1938</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15568795&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15568795&amp;dopt=abstract</a> </p><br />

    <p>13.       44514   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          (2S,3R)-2-Aminododecan-3-ol, a New Antifungal Agent from the Ascidian Clavelina oblonga</p>

    <p>          Kossuga, MH, Macmillan, JB, Rogers, EW, Molinski, TF, Nascimento, GG, Rocha, RM, and Berlinck, RG</p>

    <p>          J Nat Prod <b>2004</b>.  67(11): 1879-1881</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15568780&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15568780&amp;dopt=abstract</a> </p><br />

    <p>14.       44515   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Current issues in the diagnosis of hepatitis B and C virus infections</p>

    <p>          Harrison, TJ</p>

    <p>          Clin Diagn Virol <b>1996</b>.  5(2-3): 187-190</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566877&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566877&amp;dopt=abstract</a> </p><br />

    <p>15.       44516   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Microbial adhesion of Cryptosporidium parvum sporozoites: purification of an inhibitory lipid from bovine mucosa</p>

    <p>          Johnson, JK, Schmidt, J, Gelberg, HB, and Kuhlenschmidt, MS</p>

    <p>          J Parasitol <b>2004</b>.  90(5): 980-990</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15562596&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15562596&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.       44517   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Combination of a hepatitis C virus NS3-NS4A protease inhibitor and alpha interferon synergistically inhibits viral RNA replication and facilitates viral RNA clearance in replicon cells</p>

    <p>          Lin, K, Kwong, AD, and Lin, C</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4784-4792</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561857&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561857&amp;dopt=abstract</a> </p><br />

    <p>17.       44518   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Susceptibility of the human pathogenic fungi Cryptococcus neoformans and Histoplasma capsulatum to g-radiation versus radioimmunotherapy with a- and b-emitting radioisotopes</p>

    <p>          Dadachova, Ekaterina, Howell, Roger W, Bryan, Ruth A, Frenkel, Annie, Nosanchuk, Joshua D, and Casadevall, Arturo</p>

    <p>          Journal of Nuclear Medicine <b>2004</b>.  45(2): 313-320</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>18.       44519   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Anti-Candida effects of estragole in combination with ketoconazole or amphotericin B</p>

    <p>          Shin, S and Pyun, MS</p>

    <p>          Phytother Res <b>2004</b>.  18(10): 827-830</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15551395&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15551395&amp;dopt=abstract</a> </p><br />

    <p>19.       44520   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Preparation of 2,4-Diamino-5-[5-substituted-benzyl]pyrimidines and 2,4-diamino-6-[5-substituted-benzyl] quinazolines as DHFR inhibitors</p>

    <p>          Rosowsky, Andre and Forsch, Ronald A</p>

    <p>          PATENT:  WO <b>2004082613</b>  ISSUE DATE:  20040930</p>

    <p>          APPLICATION: 2004  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (Dana Farber Cancer Institute, USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>20.       44521   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Microsphere technology for chemotherapy of mycobacterial infections</p>

    <p>          Barrow, WW</p>

    <p>          Curr Pharm Des <b>2004</b>.  10(26): 3275-3284</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15544515&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15544515&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.       44522   OI-LS-311; PUBMED-OI-12/14/2004</p>

    <p class="memofmt1-2">          Reduction of hepatitis C virus NS5A hyperphosphorylation by selective inhibition of cellular kinases activates viral RNA replication in cell culture</p>

    <p>          Neddermann, P, Quintavalle, M, Di, Pietro C, Clementi, A, Cerretani, M, Altamura, S, Bartholomew, L, and De, Francesco R</p>

    <p>          J Virol <b>2004</b> .  78(23): 13306-13314</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542681&amp;dopt=abstract</a> </p><br />

    <p>22.       44523   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Synthesis and microbiological activity of some substituted N-(2-hydroxy-4-nitrophenyl) benzamides and phenylacetamides as possible metabolites of antimicrobial active benzoxazoles</p>

    <p>          Oren, IY, Aki-Sener, E, Ertas, C, Arpaci, OT, Yalcin, I, and Altanlar, N</p>

    <p>          TURKISH JOURNAL OF CHEMISTRY <b>2004</b>.  28(4): 441-449, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224772700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224772700005</a> </p><br />

    <p>23.       44524   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Enzymatic inactivation and reactivation of chloramphenicol by Mycobacterium tuberculosis and Mycobacterium bovis</p>

    <p>          Sohaskey, Charles D</p>

    <p>          FEMS Microbiology Letters <b>2004</b>.  240(2): 187-192</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       44525   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Geranylgeraniol Regulates Negatively Caspase-1 Autoprocessing: Implication in the Th1 Response against Mycobacterium tuberculosis</p>

    <p>          Montero, Maria T, Matilla, Joaquin, Gomez-Mampaso, Enrique, and Lasuncion, Miguel A</p>

    <p>          Journal of Immunology <b>2004</b>.  173(8): 4936-4944</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>25.       44526   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Potential use of natural lipases and esterases for inhibition of Mycobacterium tuberculosis</p>

    <p>          Annenkov, GA, Klepikov, NN, Martynova, LP, and Puzanov, VA</p>

    <p>          Problemy Tuberkuleza i Boleznei Legkikh <b>2004</b>.(6): 52-56</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       44527   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Activity of Rifampin against Mycobacterium tuberculosis in a Reference Center</p>

    <p>          Ruiz, P, Gutierrez, J, Rodriguez-Cano, F, Zerolo, FJ, and Casal, M</p>

    <p>          Microbial Drug Resistance (Larchmont, NY, United States) <b>2004</b>.  10(3): 239-242</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       44528   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Requirements for nitric oxide generation from isoniazid activation in vitro and inhibition of mycobacterial respiration in vivo</p>

    <p>          Timmins, Graham S, Master, Sharon, Rusnak, Frank, and Deretic, Vojo</p>

    <p>          Journal of Bacteriology <b>2004</b>.  186(16): 5427-5431</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>28.       44529   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Drugs as materials: Valuing physical form in drug discovery</p>

    <p>          Gardner, CR, Walsh, CT, and Almarsson, O</p>

    <p>          NATURE REVIEWS DRUG DISCOVERY <b>2004</b>.  3(11): 926-934, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224833100017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224833100017</a> </p><br />

    <p>29.       44530   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          7-Oxostaurosporine selectively inhibits the mycelial form of Candida albicans</p>

    <p>          Hwang, EI, Yun, BS, Lee, SH, Kim, SK, Lim, SJ, and Kim, SU</p>

    <p>          JOURNAL OF MICROBIOLOGY AND BIOTECHNOLOGY <b>2004</b>.  14(5): 1067-1070, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224817300029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224817300029</a> </p><br />

    <p>30.       44531   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Synthesis of trehalose-based compounds and their inhibitory activities against Mycobacterium smegmatis</p>

    <p>          Wang, Jinhua, Elchert, Bryan, Hui, Yu, Takemoto, Jon Y, Bensaci, Mekki, Wennergren, John, Chang, Huiwen, Rai, Ravi, and Chang, Cheng-Wei Tom</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  12(24): 6397-6413</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       44532   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Fluconazole binding and sterol demethylation in three CYP51 isoforms indicate differences in active site topology</p>

    <p>          Bellamine, A, Lepesheva, GI, and Waterman, MR</p>

    <p>          JOURNAL OF LIPID RESEARCH <b>2004</b>.  45(11): 2000-2007, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224774200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224774200005</a> </p><br />

    <p>32.       44533   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Survival of Mycobacterium tuberculosis in human macrophages involves inhibition of the phagosomal translocation and activation of sphingosine kinase</p>

    <p>          Thompson, C, Iyer, S, Johnson, K, Obeid, L, and Kusner, D</p>

    <p>          JOURNAL OF LEUKOCYTE BIOLOGY <b>2004</b>.: 39-41</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223683100079">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223683100079</a> </p><br />

    <p>33.       44534   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of new benzothiadiazine dioxide derivatives</p>

    <p>          Tait, A, Luppi, A, and Cermelli, C</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2004</b>.  41(5): 747-753, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224694800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224694800016</a> </p><br />

    <p>34.       44535   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity against strains of Candida albicans of 6-fluoro-4(5 or 7)-chloro-2-(difluorobenzoyl)aminobenzothiazoles</p>

    <p>          Armenise, D, De Laurentis, N, Reho, A, Rosato, A, and Morlacchi, F</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2004</b>.  41(5): 771-775, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224694800020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224694800020</a> </p><br />
    <br clear="all">

    <p>35.       44536   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Antiviral substances from microalgae</p>

    <p>          Koenig, Tanja and Walter, Christian</p>

    <p>          Bioforum <b>2003</b>.  26(12): 798-799</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>36.       44537   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          A generally applicable, high-throughput screening-compatible assay to identify, evaluate, and optimize antimicrobial agents for drug therapy</p>

    <p>          Kleymann, G and Werling, HO</p>

    <p>          JOURNAL OF BIOMOLECULAR SCREENING <b>2004</b>.  9(7): 578-587, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224693900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224693900003</a> </p><br />

    <p>37.       44538   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Novel chemical class of pUL97 protein kinase-specific inhibitors with strong anticytomegaloviral activity</p>

    <p>          Herget, Thomas, Freitag, Martina, Morbitzer, Monika, Kupfer, Regina, Stamminger, Thomas, and Marschall, Manfred</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(11): 4154-4162</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>38.       44539   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Antivirals against DNA viruses (hepatitis B and the herpes viruses)</p>

    <p>          Hewlett, Guy, Hallenberger, Sabine, and Ruebsamen-Waigmann, Helga</p>

    <p>          Current Opinion in Pharmacology <b>2004</b>.  4(5): 453-464</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>39.       44540   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Phylogenomic evidence supports past endosymbiosis, intracellular and horizontal gene transfer in Cryptosporidium parvum</p>

    <p>          Huang, JL, Mullapudi, N, Lancto, CA, Scott, M, Abrahamsen, MS, and Kissinger, JC</p>

    <p>          GENOME BIOLOGY  <b>2004</b>.  5(11): 15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224855300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224855300009</a> </p><br />

    <p>40.       44541   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Esters, amides and substituted derivatives of cinnamic acid: synthesis, antimicrobial activity and QSAR investigations</p>

    <p>          Narasimhan, B, Belsare, D, Pharande, D, Mourya, V, and Dhake, A</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2004</b>.  39(10): 827-834, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224727900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224727900002</a> </p><br />

    <p>41.       44542   OI-LS-311; SCIFINDER-OI-12/8/2004</p>

    <p class="memofmt1-2">          Anti-viral 7-deaza D-nucleosides for HBV infection therapy</p>

    <p>          Mekouar, Khalid and Deziel, Robert</p>

    <p>          PATENT:  WO <b>2004011478</b>  ISSUE DATE:  20040205</p>

    <p>          APPLICATION: 2003  PP: 36 pp.</p>

    <p>          ASSIGNEE:  (Micrologix Biotech Inc., Can.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>42.       44543   OI-LS-311; WOS-OI-12/05/2004</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of 5,8-quinazolinedione derivitives modified at positions 6 and 7</p>

    <p>          Ryu, CK, Shim, JY, Yi, YJ, Choi, IH, Chae, MJ, Han, JY, and Jung, OJ</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2004</b>.  27(10): 990-996, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224770200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224770200002</a> </p><br />

    <p>43.       44544   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          In vitro activity on Cryptosporidium parvum oocyst of different drugs with recognized anticryptosporidial efficacy</p>

    <p>          Castro-Hermida, JA, Porsi, I, Ares-Mazas, E, and Chartier, C</p>

    <p>          REVUE DE MEDECINE VETERINAIRE <b>2004</b>.  155(8-9): 453-456, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224975000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224975000010</a> </p><br />

    <p>44.       44545   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          Antibacterial activity of volatile component and various extracts of Spirulina platensis</p>

    <p>          Ozdemir, G, Karabay, NU, Dalay, MC, and Pazarbasi, B</p>

    <p>          PHYTOTHERAPY RESEARCH <b>2004</b>.  18(9): 754-757, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224872700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224872700012</a> </p><br />

    <p>45.       44546   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          Free 2-propen-1-amine derivative and inclusion complexes with beta-cyclodextrin: Scanning electron microscopy, dissolution, cytotoxicity and antimycobacterial activity</p>

    <p>          de Souza, AO,  Santos-, RR, Sato, DN, de Azevedo, MMM, Ferreira, DA, Melo, PS, Haun, M, Silva, CL, and Duran, N</p>

    <p>          JOURNAL OF THE BRAZILIAN CHEMICAL SOCIETY <b>2004</b>.  15(5): 682-689, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224958800012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224958800012</a> </p><br />

    <p>46.       44547   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          Selection of a moxifloxacin dose that suppresses drug resistance in Mycobacterium tuberculosis, by use of an in vitro pharmacodynamic infection model and mathematical modeling. (vol 190, pg 1642, 2004)</p>

    <p>          Gumbo, T, Louie, A, Deziel, MR, Parsons, LM, Salfinger, M, and Drusano, GL</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2004</b>.  190(11): 2059-2061</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225045800028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225045800028</a> </p><br />

    <p>47.       44548   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          Clinical evaluation of a dried commercially prepared microdilution panel for antifungal susceptibility testing of five antifungal agents against Candida spp. and Cryptococcus neoformans</p>

    <p>          Pfaller, MA, Boyken, L, Hollis, RJ, Messer, SA, Tendolkar, S, and Diekema, DJ</p>

    <p>          DIAGNOSTIC MICROBIOLOGY AND INFECTIOUS DISEASE <b>2004</b>.  50(2 ): 113-117, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224882000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224882000006</a> </p><br />

    <p>48.       44549   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          Recent advances in chemical genomics</p>

    <p>          Darvas, F, Dorman, G, Krajcsi, P, Puskas, LG, Kovari, Z, Lorincz, Z, and Urge, L</p>

    <p>          CURRENT MEDICINAL CHEMISTRY <b>2004</b>.  11(23): 3119-3145, 27</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224899200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224899200009</a> </p><br />

    <p>49.       44550   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          Inhibitory effects of trientine, a copper-chelating agent, on induction of DNA strand breaks in hepatic cells of Long-Evans Cinnamon rats</p>

    <p>          Hayashi, M, Miyane, K, Hirooka, T, Endoh, D, Higuchi, H, Nagahata, H, Nakayama, K, Kon, Y, and Okui, T</p>

    <p>          BIOCHIMICA ET BIOPHYSICA ACTA-GENERAL SUBJECTS <b>2004</b>.  1674(3): 312-318, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224938600011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224938600011</a> </p><br />

    <p>50.       44551   OI-LS-311; WOS-OI-12/12/2004</p>

    <p class="memofmt1-2">          Prodrug research: futile or fertile?</p>

    <p>          Testa, B</p>

    <p>          BIOCHEMICAL PHARMACOLOGY <b>2004</b>.  68(11): 2097-2106, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224952200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224952200001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
