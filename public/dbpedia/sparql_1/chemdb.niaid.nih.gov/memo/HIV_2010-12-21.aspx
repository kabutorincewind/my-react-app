

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-12-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="83xCuWgetpWoKnSZK2cVrDAjkhdXCASstMtIESwzwvzlGOoJeAXTS51IrBgBRfIHEXN1cNINJzk7f9f58XesqFTnHPBHfQtvQ+8sw+bdSQ1NC33QkJlirAcVkz8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="132067EC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  December 10 - December 21, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21142105">Synthesis and Biological Evaluation of Aryl-phospho-indole as Novel HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Alexandre, F.R., A. Amador, S. Bot, C. Caillet, T. Convard, J. Jakubik, C. Musiu, B. Poddesu, L. Vargiu, M. Liuzzi, A. Roland, M. Seifer, D. Standring, R. Storer, and C.B. Dousson. Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>;  PMID[21142105].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21170360">HIV Capsid is a Tractable Target for Small Molecule Therapeutic Intervention.</a> Blair, W.S., C. Pickford, S.L. Irving, D.G. Brown, M. Anderson, R. Bazin, J. Cao, G. Ciaramella, J. Isaacson, L. Jackson, R. Hunt, A. Kjerrstrom, J.A. Nieman, A.K. Patick, M. Perros, A.D. Scott, K. Whitby, H. Wu, and S.L. Butler. PLoS Pathogens, 2010. 6(12): p. e1001220; PMID[21170360].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21143120">Recent Advances in DAPYs and Related Analogues as HIV-1 NNRTIs.</a> Chen, X., P. Zhan, D. Li, E. De Clercq, and X. Liu. Current Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>;  PMID[21143120].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20219513">Discovery and applications of the plant cyclotides.</a>Craik, D.J. Toxicon, 2010. 56(7): p. 1092-1102; PMID[20219513].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21035337">Discovery of N-benzyl-N&#39;-(4-pipyridinyl)urea CCR5 antagonists as anti-HIV-1 agents (I): optimization of the amine portion.</a> Duan, M., J. Peckham, M. Edelstein, R. Ferris, W.M. Kazmierski, A. Spaltenstein, P. Wheelan, and Z. Xiong. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7397-7400; PMID[21035337].</p>

    <p class="NoSpacing"><b>[Pubmed</b>]. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21143001">Potential of polymeric nanoparticles in AIDS treatment and prevention.</a> Khalil, N.M., E. Carraro, L.F. Cotica, and R.M. Mainardes. Expert Opinion on Drug Delivery, 2010. <b>[Epub ahead of print]</b>;  PMID[21143001].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21143121">Novel, Selective CDK9 Inhibitors for the Treatment of HIV Infection.</a> Nemeth, G., Z. Varga, Z. Greff, G. Bencze, A. Sipos, C. Szantai-Kis, F. Baska, A. Gyuris, K. Kelemenics, Z. Szathmary, J. Minarovits, G. Keri, and L. Orfi. Current Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>;  PMID[21143121].</p>

    <p class="NoSpacing"><b>[Pubmed</b>]. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20972507">Tetra-end-linked oligonucleotides forming DNA G-quadruplexes: a new class of aptamers showing anti-HIV activity</a>. Oliviero, G., J. Amato, N. Borbone, S. D&#39;Errico, A. Galeone, L. Mayol, S. Haider, O. Olubiyi, B. Hoorelbeke, J. Balzarini, and G. Piccialli. Chemical Communications (Cambridge, England), 2010. 46(47): p. 8971-8973; PMID[20972507].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21166398">Enzymatic De Novo Pyrimidine Nucleotide Synthesis.</a> Schultheisz, H.L., B.R. Szymczyna, L.G. Scott, and J.R. Williamson. Journal of the American Chemical Society, 2010. <b>[Epub ahead of print]</b>;  PMID[21166398].</p>

    <p class="NoSpacing"><b>[Pubmed</b>]. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937812">Resistance profiles of novel electrostatically constrained HIV-1 fusion inhibitors.</a> Shimura, K., D. Nameki, K. Kajiwara, K. Watanabe, Y. Sakagami, S. Oishi, N. Fujii, M. Matsuoka, S.G. Sarafianos, and E.N. Kodama. Journal of Biological Chemistry, 2010. 285(50): p. 39471-39480; PMID[20937812].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21036042">Azaindole N-methyl hydroxamic acids as HIV-1 integrase inhibitors-II. The impact of physicochemical properties on ADME and PK.</a> Tanis, S.P., M.B. Plewe, T.W. Johnson, S.L. Butler, D. Dalvie, D. DeLisle, K.R. Dress, Q. Hu, B. Huang, J.E. Kuehler, A. Kuki, W. Liu, Q. Peng, G.L. Smith, J. Solowiej, K.T. Tran, H. Wang, A. Yang, C. Yin, X. Yu, J. Zhang, and H. Zhu. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7429-7434; PMID[21036042].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937817">Gyrase B inhibitor impairs HIV-1 replication by targeting Hsp90 and the capsid protein.</a> Vozzolo, L., B. Loh, P.J. Gane, M. Tribak, L. Zhou, I. Anderson, E. Nyakatura, R.G. Jenner, D. Selwood, and A. Fassati. Journal of Biological Chemistry, 2010. 285(50): p. 39314-39328; PMID[20937817].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21165936">Fragment-Based Design of Ligands Targeting a Novel Site on the Integrase Enzyme of Human Immunodeficiency Virus 1.</a> Wielens, J., S.J. Headey, J.J. Deadman, D.I. Rhodes, M.W. Parker, D.K. Chalmers, and M.J. Scanlon. ChemMedChem, 2010. <b>[Epub ahead of print]</b>;  PMID[21165936].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21167302">Huwe1, a novel cellular interactor of Gag-Pol through integrase binding, negatively influences HIV-1 infectivity.</a> Yamamoto, S.P., K. Okawa, T. Nakano, K. Sano, K. Ogawa, T. Masuda, Y. Morikawa, Y. Koyanagi, and Y. Suzuki. Microbes and Infection, 2010. <b>[Epub ahead of print]</b>;  PMID[21167302].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1210-122110.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283845300027">Amyloid-binding Small Molecules Efficiently Block SEVI (Semen-derived Enhancer of Virus Infection)- and Semen-mediated Enhancement of HIV-1 Infection.</a> Olsen, J.S., C. Brown, C.C. Capule, M. Rubinshtein, T.M. Doran, R.K. Srivastava, C.Y. Feng, B.L. Nilsson, J. Yang, and S. Dewhurst. Journal of Biological Chemistry, 2010. 285(46): p. 35488-35496; ISI[000283845300027].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284005600012">Synthesis of Baylis-Hillman-Derived Phosphonated 3-(Benzylaminomethyl)coumarins.</a> Rashamuse, T.J., R. Klein, and P.T. Kaye. Synthetic Communications, 2010. 40(24): p. 3683-3690; ISI[000284005600012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284005600019">Molecular Iodine: An Efficient and Environment Friendly Catalyst for the Synthesis of 3,5-Bis-(arylmethylidene)-tetrahydropyran-4-ones.</a> Reddy, M.B.M., A. Nizam, and M.A. Pasha. Synthetic Communications, 2010. 40(24): p. 3728-3733; ISI[000284005600019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284080400071">Convergent Assembly of the Spiroacetal Subunit of Didemnaketal B.</a> Fuwa, H., S. Noji, and M. Sasaki. Organic Letters, 2010. 12(22): p. 5354-5357; ISI[000284080400071].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284203200002">Synthesis of N-Terminally Linked Protein and Peptide Dimers by Native Chemical Ligation.</a> Xiao, J.P., B.S. Hamilton, and T.J. Tolbert. Bioconjugate Chemistr, 2010. 21(11): p. 1943-1947; ISI[000284203200002].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284261800082">Molecular mechanisms of retroviral integrase inhibition and the evolution of viral resistance.</a> Hare, S., A.M. Vos, R.F. Clayton, J.W. Thuring, M.D. Cummings, and P. Cherepanov. Proceedings of the National Academy of Sciences of the United States of America, 2010. 107(46): p. 20057-20062; ISI[000284261800082].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284287200025">Design, Synthesis, and Biological Evaluation of Novel Hybrid Dicaffeoyltartaric/Diketo Acid and Tetrazole-Substituted L-Chicoric Acid Analogue Inhibitors of Human Immunodeficiency Virus Type 1 Integrase.</a> Crosby, D.C., X.Y. Lei, C.G. Gibbs, B.R. McDougall, W.E. Robinson, and M.G. Reinecke. Journal of Medicinal Chemistry, 2010. 53(22): p. 8161-8175; ISI[000284287200025].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284303200032">Towards the synthesis of coumarin derivatives as potential dual-action HIV-1 protease and reverse transcriptase inhibitors.</a> Olomola, T.O., R. Klein, K.A. Lobb, Y. Sayed, and P.T. Kaye. Tetrahedron Letters, 2010. 51(48): p. 6325-6328; ISI[000284303200032].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284332900047">Discovery of N-benzyl-N &#39;-(4-pipyridinyl)urea CCR5 antagonists as anti-HIV-1 agents (I): Optimization of the amine portion.</a> Duan, M.S., J. Peckham, M. Edelstein, R. Ferris, W.M. Kazmierski, A. Spaltenstein, P. Wheelan, and Z.P. Xiong. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7397-7400; ISI[000284332900047].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284432000015">QSAR analysis of some novel sulfonamides incorporating 1,3,5-triazine derivatives as carbonic anhydrase inhibitors</a>. Jain, A.K., R. Veerasamy, A. Vaidya, V. Mourya, and R.K. Agrawal. Medicinal Chemistry Research, 2010. 19(9): p. 1191-1202; ISI[000284432000015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284435400014">On the design of polymeric 5 &#39;-O-ester prodrugs of 3 &#39;-azido-2 &#39;,3 &#39;-dideoxythymidine (AZT).</a> Troev, K.D., V.A. Mitova, and I.G. Ivanov. Tetrahedron Letters, 2010. 51(47): p. 6123-6125; ISI[000284435400014].</p>

    <p class="NoSpacing"><b>[WOS]</b>., HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284440300009">Modified Rapid Screening Method for Evaluation of HIV-1 Protease Inhibitors.</a> Hinkov, A., V. Atanasov, S. Raleva, and R. Argirova, . Comptes Rendus De L Academie Bulgare Des Sciences, 2010. 63(10): p. 1455-1462; ISI[000284440300009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284585500002">Development of Peptide and Small-Molecule HIV-1 Fusion Inhibitors that Target gp41.</a> Cai, L.F. and S.B. Jiang. ChemMedChem, 2010. 5(11): p. 1813-1824; ISI[000284585500002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284585500003">Inhibition of HIV-1 Entry: Multiple Keys to Close the Door.</a> Hertje, M., M.K. Zhou, and U. Dietrich. ChemMedChem, 2010. 5(11): p. 1825-1835; ISI[000284585500003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284585500008">Probing Multidrug-Resistance and Protein-Ligand Interactions with Oxatricyclic Designed Ligands in HIV-1 Protease Inhibitors.</a> Ghosh, A.K., C.X. Xu, K.V. Rao, A. Baldridge, J. Agniswamy, Y.F. Wang, I.T. Weber, M. Aoki, S.G.P. Miguel, M. Amano, and H. Mitsuya. ChemMedChem, 2010. 5(11): p. 1850-1854; ISI[000284585500008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284622500007">Effects of Plant Extracts on HIV-1 Protease.</a> Ribeiro, J., H.D. Falcao, L.M. Batista, J.M. Barbosa, and M.R. Piuvezam. Current HIV Research, 2010. 8(7): p. 531-544; ISI[000284622500007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284635800010">Thiazolopyrimidines without bridge-head nitrogen: thiazolo 4,5-d pyrimidines.</a> El-Bayouki, K.A.M. and W.M. Basyouni. Journal of Sulfur Chemistry, 2010. 31(6): p. 551-590; ISI[000284635800010].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284642000012">Chemical Modifications Designed to Improve Peptide Stability: Incorporation of Non-Natural Amino Acids, Pseudo-Peptide Bonds, and Cyclization.</a> Gentilucci, L., R. De Marco, and L. Cerisoli. Current Pharmaceutical Design, 2010. 16(28): p. 3185-3203; ISI[000284642000012].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284650400005">Recent Patents on Nucleic Acid-Based Antiviral Therapeutics.</a> Mishra, S., S. Kim, and D.K. Lee. Recent Patents on Anti-Cancer Drug Discovery, 2010. 5(3): p. 255-271; ISI[000284650400005].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284654100011">Targeting Protein-Protein Interactions: A Promising Avenue of Anti-HIV Drug Discovery.</a> Zhan, P., W.J. Li, H.F. Chen, and X.Y. Liu. Current Medicinal Chemistry, 2010. 17(29): p. 3393-3409; ISI[000284654100011].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284664500022">Synthesis of fused dihydropyrido e purines via ring closing metathesis.</a> Litinas, K.E. and A. Thalassitis. Tetrahedron Letters, 2010. 51(49): p. 6451-6453; ISI[000284664500022].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284680100006">Partial selective inhibition of HIV-1 reverse transcriptase and human DNA polymerases gamma and p by thiated 3 &#39;-fluorothymidine analogue 5 &#39;-triphosphates.</a> Winska, P., A. Miazga, J. Poznanski, and T. Kulikowski. Antiviral Research, 2010. 88(2): p. 176-181; ISI[000284680100006].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284728400013">The efficient synthesis of (3R,3aS,6aR)-hexahydrofuro 2,3-b furan-3-ol and its isomers.</a> Kulkarni, M.G., Y.B. Shaikh, A.S. Borhade, A.P. Dhondge, S.W. Chavhan, M.P. Desai, D.R. Birhade, N.R. Dhatrak, and R. Gannimani. Tetrahedron-Asymmetry, 2010. 21(19): p. 2394-2398; ISI[000284728400013].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284738400008">Design, Synthesis, and Evaluation of Diarylpyridines and Diarylanilines as Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Tian, X.T., B.J. Qin, Z.Y. Wu, X.F. Wang, H. Lu, S.L. Morris-Natschke, C.H. Chen, S.B. Jiang, K.H. Lee, and L. Xie. Journal of Medicinal Chemistry, 2010. 53(23): p. 8287-8297; ISI[000284738400008].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284864200015">Synthesis and biological activity of new pyridone diaryl ether non-nucleoside inhibitors of HIV-1 reverse transcriptase.</a> Kennedy-Smith, J.J., N. Arora, J.R. Billedeau, J. Fretland, J.Q. Hang, G.M. Heilek, S.F. Harris, D. Hirschfeld, H. Javanbakht, Y. Li, W.L. Liang, R. Roetz, M. Smith, G.P. Su, J.M. Suh, A.G. Villasenor, J. Wu, D. Yasuda, K. Klumpp, and Z.K. Sweeney. MedChemComm, 2010. 1(1): p. 79-83; ISI[000284864200015].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284891600009">Chemical constituents and biological applications of the genus Symplocos.</a> Badoni, R., D.K. Semwal, S.K. Kothiyal, and U. Rawat. Journal of Asian Natural Products Research, 2010. 12(12): p. 1069-1080; ISI[000284891600009].</p>

    <p class="NoSpacing"><b>[WOS]</b>.HIV_1210-122110.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
