

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-373.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OOFP1swA/2RAVtWJ9ifGK9baxKG1lmRDYvAkeD1vJUBYxeOj7sgXdgkkeR+2Ukm1CA8TBfl6UFwUXjAXIkipTlvNwoHbaSuThuyNVeC8Co/zC7o01bLsGIzOnPs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="576AC410" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-373-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60746   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Low-oxygen-recovery assay for high-throughput screening of compounds against nonreplicating Mycobacterium tuberculosis</p>

    <p>          Cho, Sang Hyun, Warit, Saradee, Wan, Baojie, Hwang, Chang Hwa, Pauli, Guido F, and Franzblau, Scott G</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(4): 1380-1385</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60747   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Study on antituberculosis activity of streptomycin nanoparticles in vivo</p>

    <p>          Gui, Hui, Zou, Long, and Fan, Xue-Gong</p>

    <p>          Jiefangjun Yaoxue Xuebao <b>2007</b>.  23(1): 21-24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60748   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Preparation of oxazolidinones possessing antimicrobial activity and pharmaceutical compositions thereof</p>

    <p>          Sindkhedkar, Milind D, Bhavsar, Satish B, Patil, Vijaykumar J, Deshpande, Prasad K, and Patel, Mahesh V</p>

    <p>          PATENT:  WO <b>2007023507</b>  ISSUE DATE:  20070301</p>

    <p>          APPLICATION: 2006  PP: 210pp.</p>

    <p>          ASSIGNEE:  (India)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     60749   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Chemoenzymatic synthesis and antimicrobial activity evaluation of monoglucosyl diglycerides</p>

    <p>          Cateni, Francesca, Bonivento, Paolo, Procida, Giuseppe, Zacchigna, Marina, Favretto, Luciana Gabrielli, Scialino, Giuditta, and Banfi, Elena</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(2): 815-826</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     60750   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Fluoroquinolone resistance in Mycobacterium tuberculosis isolates: associated genetic mutations and relationship to antimicrobial exposure</p>

    <p>          Wang Jann-Yuan, Lee Li-Na, Lai Hsin-Chih, Wang Shu-Kuan, Jan I-Shiow, Yu Chong-Jen, Hsueh Po-Ren, and Yang Pan-Chyr</p>

    <p>          J Antimicrob Chemother <b>2007</b>.  59(5): 860-5.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     60751   OI-LS-373; WOS-OI-4/30/2007</p>

    <p class="memofmt1-2">          Combinations of Sterol Inhibitors Show Synergistic Effects Against Pneumocystis Carinii Viability in Vitro.</p>

    <p>          Collins, M. and Cushion, M.</p>

    <p>          Journal of Eukaryotic Microbiology <b>2007</b>.  54(2): 37S</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245312600108">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245312600108</a> </p><br />

    <p>7.     60752   OI-LS-373; WOS-OI-4/30/2007</p>

    <p class="memofmt1-2">          Sequence of the Mitochondrial Genome of Pneumocystis Carinii: Implications for Biologic Function and Identification of Potential Drug Targets.</p>

    <p>          Sesterhenn, T. <i>et al.</i></p>

    <p>          Journal of Eukaryotic Microbiology <b>2007</b>.  54(2): 53S</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245312600166">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245312600166</a> </p><br />

    <p>8.     60753   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Inhibitory effect of umbelliferone aminoalkyl derivatives on oxidosqualene cyclases from S. cerevisiae, T. cruzi, P. carinii, H. sapiens, and A. thaliana: a structure-activity study</p>

    <p>          Oliaro-Bosso, Simonetta, Viola, Franca, Taramino, Silvia, Tagliapietra, Silvia, Barge, Alessandro, Cravotto, Giancarlo, and Balliano, Gianni</p>

    <p>          ChemMedChem <b>2007</b>.  2(2): 226-233</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     60754   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Quinoline derivative MC1626, a putative GCN5 histone acetyltransferase (HAT) inhibitor, exhibits HAT-independent activity against Toxoplasma gondii</p>

    <p>          Smith, Aaron T, Livingston, Meredith R, Mai, Antonello, Filetici, Patrizia, Queener, Sherry F, and Sullivan, William J Jr</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(3): 1109-1111</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60755   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Cryptosporidiosis: current views of its pathogenesis and resistance</p>

    <p>          Anon</p>

    <p>          Med Parazitol (Mosk) <b>2007</b>.(1): 56-60.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   60756   OI-LS-373; WOS-OI-4/30/2007</p>

    <p class="memofmt1-2">          The Cell-Wall Core of Mycobacterium Tuberculosis in the Context of Drug Discovery</p>

    <p>          Brennan, P. and Crick, D.</p>

    <p>          Current Topics in Medicinal Chemistry <b>2007</b>.  7(5): 475-488</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245073800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245073800003</a> </p><br />

    <p>12.   60757   OI-LS-373; WOS-OI-4/30/2007</p>

    <p class="memofmt1-2">          Nitrofurans as Novel Anti-Tuberculosis Agents: Identification, Development and Evaluation</p>

    <p>          Tangallapally, R. <i>et al.</i></p>

    <p>          Current Topics in Medicinal Chemistry <b>2007</b>.  7(5): 509-526</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245073800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245073800006</a> </p><br />

    <p>13.   60758   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some novel phenyl and benzimidazole substituted benzyl ethers</p>

    <p>          Gueven, Oezden Oezel, Erdogan, Taner, Goeker, Hakan, and Yildiz, Sulhiye</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(8): 2233-2236</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   60759   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some thiazolyl-pyrazoline derivatives</p>

    <p>          Kaplancikli, Zafer Asim, Turan-Zitouni, Gulhan, Ozdemir, Ahmet, Revial, Gilbert, and Guven, Kiymet</p>

    <p>          Phosphorus, Sulfur and Silicon and the Related Elements <b>2007</b>.  182(4): 749-764</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60760   OI-LS-373; WOS-OI-4/23/2007</p>

    <p class="memofmt1-2">          The Lipophilicity Estimation of 5-Arylidene Derivatives of (2-Thio)Hydantoin With Anti Mycobacterial Activity</p>

    <p>          Lazewska, D. <i>et al.</i></p>

    <p>          Biomedical Chromatography <b>2007</b>.  21(3): 291-298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245165100012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245165100012</a> </p><br />

    <p>16.   60761   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of 1-(4-aryl-2-thiazolyl)-3-(2-thienyl)-5-aryl-2-pyrazoline derivatives</p>

    <p>          Oezdemir, Ahmet, Turan-Zitouni, Guelhan, Asim Kaplancikli, Zafer, Revial, Gilbert, and Gueven, Kiymet</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(3): 403-409</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60762   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Antimicrobial Activities of Some Tetrahydronaphthalene-Benzimidazole Derivatives</p>

    <p>          Ates-Alagoz, Zeynep, Yildiz, Sulhiye, and Buyukbingol, Erdem</p>

    <p>          Chemotherapy (Basel, Switzerland) <b>2007</b>.  53(2): 110-113</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60763   OI-LS-373; WOS-OI-4/30/2007</p>

    <p class="memofmt1-2">          Il-4 Depletion Enhances Host Resistance and Passive Iga Protection Against Tuberculosis Infection in Balb/C Mice</p>

    <p>          Buccheri, S. <i>et al.</i></p>

    <p>          European Journal of Immunology <b>2007</b>.  37(3): 729-737</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245062700015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245062700015</a> </p><br />

    <p>19.   60764   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Microbiocidal activity of chitosan-N-2-hydroxypropyl trimethyl ammonium chloride</p>

    <p>          Chi, Weilin, Qin, Caiqin, Zeng, Lintao, Li, Wei, and Wang, Wei</p>

    <p>          Journal of Applied Polymer Science <b>2007</b>.  103(6): 3851-3856</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   60765   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Synthesis of amide derivatives of quinolone and their antimicrobial studies</p>

    <p>          Patel, NB, Patel, AL, and Chauhan, HI</p>

    <p>          Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry <b>2007</b>.  46B(1): 126-134</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60766   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Synthesis and Structural Studies of 4-Thioxopyrimidines with Antimicrobial Activities</p>

    <p>          Cunha, Silvio, Bastos, Rodrigo M, Silva, Priscila de O, Nobre Costa, Giselle A, Vencato, Ivo, Lariucci, Carlito, Napolitano, Hamilton B, de Oliveira, Cecilia MA, Kato, Lucilia, da Silva, Cleuza C, Menezes, Diego, and Vannier-Santos, Marcos A</p>

    <p>          Monatshefte fuer Chemie <b>2007</b>.  138(2): 111-119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   60767   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Recent trends in interferons. Hepatitis C and interferon therapy</p>

    <p>          Saito, Masaki and Nishiguchi, Shuhei</p>

    <p>          Ensho to Men&#39;eki <b>2007</b>.  15(2): 202-206</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.   60768   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Novel peptides as antifungal agents</p>

    <p>          Nikawa, Hiroki, Nishimura, Masahiro, Tsuji, Koichiro, Kawabata, Ryoko, and Hiromoto, Nobue</p>

    <p>          PATENT:  WO <b>2007034678</b>  ISSUE DATE:  20070329</p>

    <p>          APPLICATION: 2006  PP: 22pp.</p>

    <p>          ASSIGNEE:  (Japan Science and Technology Agency, Japan, Hiroshima University, and Two Cells Co., Ltd.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   60769   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          1,2,4-Thiadiazole derivative antifungal compounds, compositions containing them, and preparation and use thereof</p>

    <p>          Barbey, Sabine, Zarzov, Patrick, Briand, Jean-Francois, Bougeret, Cecile, and Thomas, Dominique</p>

    <p>          PATENT:  WO <b>2007012724</b>  ISSUE DATE:  20070201</p>

    <p>          APPLICATION: 2006  PP: 94pp.</p>

    <p>          ASSIGNEE:  (Cytomics Systems, Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   60770   OI-LS-373; SCIFINDER-OI-4/23/2007</p>

    <p class="memofmt1-2">          Posaconazole (Noxafil): a new triazole antifungal agent</p>

    <p>          Greer Nickie D </p>

    <p>          Proc (Bayl Univ Med Cent) <b>2007</b>.  20(2): 188-96.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   60771   OI-LS-373; WOS-OI-4/23/2007</p>

    <p class="memofmt1-2">          Pleofungins, Novel Inositol Phosphorylceramide Synthase Inhibitors, From Phoma Sp Sank 13899 - I. Taxonomy, Fermentation, Isolation, and Biological Activities</p>

    <p>          Yano, T. <i>et al.</i></p>

    <p>          Journal of Antibiotics <b>2007</b>.  60(2): 136-142</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244928400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244928400004</a> </p><br />

    <p>27.   60772   OI-LS-373; WOS-OI-4/30/2007</p>

    <p class="memofmt1-2">          Therapies for Human Cytomegalovirus</p>

    <p>          Rios, S. <i>et al.</i></p>

    <p>          Expert Opinion on Therapeutic Patents <b>2007</b>.  17(4): 407-418</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245518300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245518300004</a> </p><br />

    <p>28.   60773   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Effect of Cantharidin, Cephalotaxine and Homoharringtonine on &quot;in vitro&quot; Models of Hepatitis B Virus (HBV) and Bovine Viral Diarrhoea Virus (BVDV) Replication</p>

    <p>          Romero, MR, Serrano, MA, Efferth, T, Alvarez, M, and Marin, JJ</p>

    <p>          Planta Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458779&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458779&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>29.   60774   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Fluoroanalogues of anti-cytomegalovirus agent cyclopropavir: synthesis and antiviral activity of (e)- and (z)-9-{[2,2-bis(hydroxymethyl)-3-fluorocyclopropylidene]methyl}-adenines and guanines</p>

    <p>          Zhou, S, Zemlicka, J, Kern, ER, and Drach, JC</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(3): 231-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454732&amp;dopt=abstract</a> </p><br />

    <p>30.   60775   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Efficacy of rifabutin-loaded microspheres for treatment of mycobacterium avium-infected macrophages and mice</p>

    <p>          Barrow, EL, Barrow, WW, Quenelle, DC, Westbrook, L, Winchester, GA, and Staas, JK</p>

    <p>          Drug Deliv <b>2007</b>.  14(3): 119-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454031&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454031&amp;dopt=abstract</a> </p><br />

    <p>31.   60776   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          TB treatment guidelines begin to impact</p>

    <p>          Bateman, C</p>

    <p>          S Afr Med J <b>2007</b>.  97(4): 241-2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17446943&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17446943&amp;dopt=abstract</a> </p><br />

    <p>32.   60777   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          In Vitro antifungal activity of extract and plumbagin from the stem bark of Diospyros crassiflora Hiern (Ebenaceae)</p>

    <p>          Dzoyem, JP, Tangmouo, JG, Lontsi, D, Etoa, FX, and Lohoue, PJ</p>

    <p>          Phytother Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17444575&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17444575&amp;dopt=abstract</a> </p><br />

    <p>33.   60778   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          A background and critical analysis of the treatment of pneumocystis carinii pneumonia (PCP) in HIV/AIDS</p>

    <p>          Mitchell, B</p>

    <p>          Aust Nurs J <b>2007</b>.  14(9): 20-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17441498&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17441498&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   60779   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          A 5-year prospective study of the late resolution of chronic hepatitis C after antiviral therapy</p>

    <p>          Annicchiarico, BE, Siciliano, M, Avolio, AW, Grillo, RL, and Bombardieri, G</p>

    <p>          Aliment Pharmacol Ther <b>2007</b>.  25(9): 1039-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17439504&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17439504&amp;dopt=abstract</a> </p><br />

    <p>35.   60780   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Structural Basis for Selective Inhibition of Mycobacterium tuberculosis Protein Tyrosine Phosphatase PtpB</p>

    <p>          Grundner, C, Perrin, D, Hooft, van Huijsduijnen R, Swinnen, D, Gonzalez, J, Gee, CL, Wells, TN, and Alber, T</p>

    <p>          Structure <b>2007</b>.  15(4): 499-509</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17437721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17437721&amp;dopt=abstract</a> </p><br />

    <p>36.   60781   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Comprehensive Analysis of the Effects of Ordinary Nutrients on Hepatitis C Virus RNA Replication in Cell Culture</p>

    <p>          Yano, M, Ikeda, M, Abe, KI, Dansako, H, Ohkoshi, S, Aoyagi, Y, and Kato, N</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420205&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420205&amp;dopt=abstract</a> </p><br />

    <p>37.   60782   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Toxoplasma gondii: inhibition of the intracellular growth by human lactoferrin</p>

    <p>          Dzitko, K, Dziadek, B, Dziadek, J, and Dlugonska, H</p>

    <p>          Pol J Microbiol <b>2007</b>.  56(1): 25-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17419186&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17419186&amp;dopt=abstract</a> </p><br />

    <p>38.   60783   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Daily interferon induction regimen using different manufactured interferons (alpha-2a or alpha-2b) in combination with ribavirin for treatment of chronic hepatitis C: a prospective randomized study</p>

    <p>          Braga, EL, Lyra, AC, Nascimento, L, Netto, E, Kalabrik, L, and Lyra, LG</p>

    <p>          Arq Gastroenterol <b>2006</b>.  43(4): 275-279</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17406754&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17406754&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>39.   60784   OI-LS-373; PUBMED-OI-4/30/2007</p>

    <p class="memofmt1-2">          Involvement of caspase-9 in the inhibition of necrosis of RAW264 cells infected with Mycobacterium tuberculosis</p>

    <p>          Uchiyama, R, Kawamura, I, Fujimura, T, Kawanishi, M, Tsuchiya, K, Tominaga, T, Kaku, T, Fukasawa, Y, Sakai, S, Nomura, T, and Mitsuyama, M</p>

    <p>          Infect Immun <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17403866&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17403866&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
