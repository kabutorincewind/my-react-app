

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-08-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="erk9ALX7lUy3PbbXJE9VgqaD77ifOahvKuGiaqdCORKrkZvNcbpOtCXTxc2VIR8b9+RLCB5CRgdDNyCpwUjvvoajUUBDlH6AkOysrTmLPgPw+P8BskBci1xaRmE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="799EE5F0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 17 - August 30, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22760295">A Multi-functional Peptide as an HIV-1 Entry Inhibitor Based on Self-concentration, Recognition, and Covalent Attachment.</a> Zhao, L., P. Tong, Y.X. Chen, Z.W. Hu, K. Wang, Y.N. Zhang, D.S. Zhao, L.F. Cai, K.L. Liu, Y.F. Zhao, and Y.M. Li. Organic &amp; Biomolecular Chemistry, 2012. 10(32): p. 6512-6520. PMID[22760295].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22856541">Design, Synthesis, and Preclinical Evaluations of Novel 4-Substituted 1,5-diarylanilines as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitor (NNRTI) Drug Candidates.</a> Sun, L.Q., L. Zhu, K. Qian, B. Qin, L. Huang, C.H. Chen, K.H. Lee, and L. Xie. Journal of Medicinal Chemistry, 2012. 55(16): p. 7219-7229. PMID[22856541]</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22827702">Stereoselective Synthesis and Antiviral Activity of Methyl-substituted cycloSal-pronucleotides.</a> Rios Morales, E.H., J. Balzarini, and C. Meier. Journal of Medicinal Chemistry, 2012. 55(16): p. 7245-7252. PMID[22827702].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22829408">Stereoselective Synthesis of D- and L-carbocyclic nucleosides by Enzymatically Catalyzed Kinetic Resolution.</a> Mahler, M., B. Reichardt, P. Hartjen, J. van Lunzen, and C. Meier. Chemistry, 2012. 18(35): p. 11046-11062. PMID[22829408]</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22845664">CXCR4-Tropic, but Not CCR5-tropic, Human Immunodeficiency Virus Infection is Inhibited by the Lipid Raft-associated Factors, Acyclic Retinoid Analogs, and Cholera Toxin B Subunit.</a> Kamiyama, H., K. Kakoki, S. Shigematsu, M. Izumida, Y. Yashima, Y. Tanaka, H. Hayashi, T. Matsuyama, H. Sato, N. Yamamoto, T. Sano, Y. Shidoji, and Y. Kubo. AIDS Research and Human Retroviruses, 2012. <b>[Epub ahead of print]</b>; PMID[22845664].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22898884">New anti-HIV Aptamers Based on Tetra-end-linked DNA G-quadruplexes: Effect of the Base Sequence on anti-HIV Activity.</a> D&#39;Atri, V., G. Oliviero, J. Amato, N. Borbone, S. D&#39;Errico, L. Mayol, V. Piccialli, S. Haider, B. Hoorelbeke, J. Balzarini, and G. Piccialli. Chemical Communications, 2012. 48(76): p. 9516-9518. PMID[22898884]</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22897429">A Novel Synthetic Bivalent Ligand to Probe Chemokine Receptor CXCR4 Dimerization and Inhibit HIV-1 Entry.</a> Choi, W.T., K.S. Kumar, N. Madani, X. Han, S. Tian, C.Z. Dong, D. Liu, S. Duggineni, J. Yuan, J.G. Sodroski, Z. Huang, and J. An. Biochemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22897429].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22909012">Lentiviral Gene Therapy against HIV-1 Using a Novel Human TRIM21-CyclophilinA Restriction Factor.</a> Chan, E., T. Schaller, A. Eddaoudi, H. Zhan, C.P. Tan, M. Jacobsen, A. Thrasher, G.J. Towers, and W. Qasim. Human Gene Therapy, 2012. <b>[Epub ahead of print]</b>; PMID[22909012].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22917277">Emtricitabine Prodrugs with Improved anti-HIV Activity and Cellular Uptake.</a> Agarwal, H.K., B.S. Chhikara, S. Bhavaraju, D. Mandal, G.F. Doncel, and K. Parang. Molecular Pharmaceutics, 2012. <b>[Epub ahead of print]</b>; PMID[22917277]</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <h2>ISI Web of Knowledge Citations</h2>
    
    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306523100011">One Pot Synthesis of Structurally Different Mono and Dimeric Ni(II) thiosemicarbazone Complexes and N-Arylation on a Coordinated Ligand: A Comparative Biological Study.</a> Prabhakaran, R., P. Kalaivani, P. Poornima, F. Dallemer, G. Paramaguru, V.V. Padma, R. Renganathan, R. Huang, and K. Natarajan. Dalton Transactions, 2012. 41(31): p. 9323-9336. ISI[000306523100011].</p>   
    <p class="plaintext"><b>[WOS].</b> HIV_0817-083012.   </p>
    <p> </p>
    
    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306481100033">Enamino-oxindole HIV Protease Inhibitors.</a> Eissenstat, M., T. Guerassina, S. Gulnik, E. Afonina, A.M. Silva, D. Ludtke, H. Yokoe, B. Yu, and J. Erickson. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(15): p. 5078-5083. ISI[000306481100033].</p>   
    <p class="plaintext"><b>[WOS].</b> HIV_0817-083012.</p>
    <p> </p>
    
    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306749000005">Algal Lectins as Potential HIV Microbicide Candidates.</a> Huskens, D. and D. Schols. Marine Drugs, 2012. 10(7): p. 1476-1497. ISI[000306749000005].   </p>
    <p class="plaintext"><b>[WOS]. </b>HIV_0817-083012</p>
    <p> </p>
        
    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307264100013">Synthesis and Study of 2-(Pyrrolesulfonylmethyl)-N-arylimines: A New Class of Inhibitors for Human Glutathione Transferase A1-1.</a> Koutsoumpli, G.E., V.D. Dimaki, T.N. Thireou, E.E. Eliopoulos, N.E. Labrou, G.I. Varvounis, and Y.D. Conis. Journal of Medicinal Chemistry, 2012. 55(15): p. 6802-6813. ISI[000307264100013].</p>
    <p class="plaintext"><b>[WOS].</b> HIV_0817-083012 </p>
    <p> </p>
        
    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306764600035">New Nitrogen Containing Substituents at the Indole-2-carboxamide Yield High Potent and Broad Spectrum Indolylarylsulfone HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> La Regina, G., A. Coluccia, A. Brancale, F. Piscitelli, V. Famiglini, S. Cosconati, G. Maga, A. Samuele, E. Gonzalez, B. Clotet, D. Schols, J.A. Este, E. Novellino, and R. Silvestri. Journal of Medicinal Chemistry, 2012. 55(14): p. 6634-6638. ISI[000306764600035].</p>
    <p class="plaintext"><b>[WOS].</b> HIV_0817-083012 </p>
    <p> </p>
    
    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307019800256">Antiviral Inhibition of the HIV-1 Matrix Protein.</a> Mercredi, P., P. Bhargava, and M. Summers. Protein Science, 2012. 21: p. 154-155. ISI[000307019800256].</p>
    <p class="plaintext"><b>[WOS].</b> HIV_0817-083012 </p>
    <p> </p>
    
    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306588100010">Inhibition of HIV-1 Integrase gene expression by 10-23 DNAzyme.</a> Singh, N., A. Ranjan, S. Sur, R. Chandra, and V. Tandon. Journal of Biosciences, 2012. 37(3): p. 493-502. ISI[000306588100010].</p>
    <p class="plaintext"><b>[WOS].</b> HIV_0817-083012 </p>
    <p> </p>
        
    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306683600026">The Hematopoietic Cell-Specific Rho GTPase Inhibitor ARHGDIB/D4GDI Limits HIV Type 1 Replication.</a> Watanabe, T., E. Urano, K. Miyauchi, R. Ichikawa, M. Hamatake, N. Misawa, K. Sato, H. Ebina, Y. Koyanagi, and J. Komano. Aids Research and Human Retroviruses, 2012. 28(8): p. 913-922. ISI[000306683600026].</p>
    <p class="plaintext"><b>[WOS]. </b>HIV_0817-083012</p>

    <h2>Patent citations</h2>

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120628&amp;CC=WO&amp;NR=2012085003A1&amp;KC=A1">2-Hydroxyisoquinoline-1,3(2H,4H)-diones and Related Compounds as HIV Integrase Inhibitors and Their Preparation and Use for the Treatment of HIV Infection and AIDS.</a> Bailly, F., M. Billamboz, F. Christ, P. Cotelle, Z. Debyser, C. Lion, and V. Suchaud. Patent. 2012. 2011-EP73480 2012085003: 165pp.</p>
    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>
    <p> </p>

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120802&amp;CC=WO&amp;NR=2012101617A1&amp;KC=A1">Proline-rich Antiviral Peptides for Treatment, Prevention and Eradication of HIV Infection.</a> Cabras, T., C. Casoli, M. Castagnola, R. Inzitari, R. Longhi, I. Messana, P. Ronzi, and A. Vitali. Patent. 2012. 2012-IB50421 2012101617: 48pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120725&amp;CC=CN&amp;NR=102603836A&amp;KC=A">Preparation of Schizandrin C Analogs as Antiviral Drugs.</a> Chang, J., C. Song, and Q. Yang. Patent. 2012. 2012-10029643</p>

    <p class="plaintext">102603836: 28pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120809&amp;CC=WO&amp;NR=2012106534A2&amp;KC=A2">Preparation of Raltegravir-Chelator Derivatives as HIV Integrase Inhibitors.</a> Cohen, S. M., A. Agrawal, J. Desoto, Y. Pommier, and K. Maddali. Patent. 2012. 2012-US23662 2012106534: 115pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120802&amp;CC=WO&amp;NR=2012102985A1&amp;KC=A1">Preparation of Isoquinoline Compounds and Methods for Treating HIV.</a> De La Rosa, M. A., S. N. Haydar, B. A. Johns, and E. Johann Velthuisen. Patent. 2012. 2012-US22161 2012102985: 243pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120628&amp;CC=US&amp;NR=2012165249A1&amp;KC=A1">Stabilized Therapeutic Small Helical Antiviral Peptides for Treatment of HIV Infection.</a> Debnath, A. K., H. Zhang, and Q. Zhao. Patent. 2012. 2012-364667 20120165249: 35pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120705&amp;CC=WO&amp;NR=2012092168A1&amp;KC=A1">Fused 6,5 Bicyclic Ring System P2 Ligands, and Methods for Treating HIV.</a> Ghosh, A. K. Patent. 2012. 2011-US67112 2012092168: 71pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120705&amp;CC=WO&amp;NR=2012092188A1&amp;KC=A1">Compositions Containing HIV Protease Inhibitors for Treatment of HIV Infection.</a> Ghosh, A. K. Patent. 2012. 2011-US67160 2012092188: 52pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120620&amp;CC=CN&amp;NR=102503900A&amp;KC=A">Preparation of 1,2,3-Triazole Derivatives as HIV-1 Integrase Inhibitors.</a> Hu, L., X. He, J. Hu, Z. Luo, S. Zhang, and C. Zeng. Patent. 2012. 2011-10302075 102503900: 19pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120614&amp;CC=US&amp;NR=2012149708A1&amp;KC=A1">Modulators of Viral Transcription, and Methods and Compositions Therewith.</a> Kashanchi, F. Patent. 2012. 2011-329721 20120149708: 58pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120809&amp;CC=WO&amp;NR=2012104793A1&amp;KC=A1">An Antibacterial and Antiviral Compound Isolated from Marine Actinomycete.</a> Kumar, V., M. Doble, B. Ramasamy, S. Ganesan, R. Manikkam, L. E. Hanna, S. Swaminathan, and S. Nagamiah. Patent. 2012. 2012-IB50463 2012104793: 39pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120808&amp;CC=CN&amp;NR=102627654A&amp;KC=A">Preparation of Isosorbide Derivatives as HIV-1 Protease Inhibitors.</a> Liu, Z. and X. Qiu. Patent. 2012. 2012-10140535 102627654: 9pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120705&amp;CC=WO&amp;NR=2012092367A1&amp;KC=A1">Nucleic acid Binding Compounds as anti-HIV Agents.</a> Miller, B. L., L. O. Ofori, and A. V. Gromova. Patent. 2012. 2011-US67576 2012092367: 128pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120627&amp;CC=EP&amp;NR=2468303A1&amp;KC=A1">Functionalized Nucleic acids and Particles Comprising Them for the Treatment of HIV Infections.</a> Mouscadet, J. F., O. Delelis, F. Subra, G. Divita, T. Zatsepin, Y. Agapkina, and M. Gottikh. Patent. 2012. 2010-195020 2468303: 43pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120809&amp;CC=WO&amp;NR=2012106188A1&amp;KC=A1">Preparation of C28 Amines of C3 Modified Betulinic Acid Derivatives as HIV Maturation Inhibitors.</a> Regueiro-Ren, Alicia, Jacob Swidorski, Sing-Yuen Sit, Yan Chen, Jie Chen, Nicholas A. Meanwell, and Zheng Liu. Patent. 2012. 2012-US22847 2012106188: 305pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120621&amp;CC=WO&amp;NR=2012080953A1&amp;KC=A1">Compounds Useful for Treating AIDS.</a> Tazi, Jamal, Florence Mahuteau, Romain Najman, Didier Scherrer, Noeelie Campos, and Aude Garcel. Patent. 2012. 2011-IB55643 2012080953: 73pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120614&amp;CC=WO&amp;NR=2012078834A1&amp;KC=A1">Preparation of Pyrimidoazepines as HIV Integrase Inhibitors Useful in the Treatment of HIV Infection and AIDS.</a> Ueda, Yasutsugu, Timothy P. Connolly, Barry L. Johnson, Chen Li, B. Narasimhulu Naidu, Manoj Patel, Kevin Peese, Margaret E. Sorenson, Michael A. Walker, Michael S. Bowsher, and Rongti Li. Patent. 2012. 2011-US63874 2012078834: 293pp. </p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0817-083012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
