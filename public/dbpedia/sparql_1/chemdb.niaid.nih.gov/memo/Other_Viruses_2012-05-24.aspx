

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-05-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sAMvHxLbxtf4PKzsSWbLUiLO8Nh5f+E+V2ckfoCWXuRATAuYmC7f/CylssAJS1oLHUiOGwr26jjroWpM1xVtt2tiXkX5h6Sx5WKge688x2CqndkCmWN2lBR8tWc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B7745791" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: May 11 - May 24, 2012</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615282">MK-5172, a Selective Inhibitor of Hepatitis C Virus NS3/4A Protease with Broad Activity across Genotypes and Resistant Variants.</a> Summa, V., S.W. Ludmerer, J.A. McCauley, C. Fandozzi, C. Burlein, G. Claudio, P.J. Coleman, J.M. Dimuzio, M. Ferrara, M. Di Filippo, A.T. Gates, D.J. Graham, S. Harper, D.J. Hazuda, C. McHale, E. Monteagudo, V. Pucci, M. Rowley, M.T. Rudd, A. Soriano, M.W. Stahlhut, J.P. Vacca, D.B. Olsen, N.J. Liverton, and S.S. Carroll, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22615282].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22617108">Inhibition of Hepatitis C Virus Replication by Intracellular Delivery of Multiple siRNAs by Nanosomes.</a> Chandra, P.K., A.K. Kundu, S. Hazari, S. Chandra, L. Bao, T. Ooms, G.F. Morris, T. Wu, T.K. Mandal, and S. Dash, Molecular Therapy: The Journal of the American Society of Gene Therapy, 2012. <b>[Epub ahead of print]</b>; PMID[22617108].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615294">Preclinical Characterization of JTK-853, a Novel Non-nucleoside Inhibitor of the Hepatitis C Virus RNA-Dependent RNA Polymerase.</a> Ando, I., T. Adachi, N. Ogura, Y. Toyonaga, K. Sugimoto, H. Abe, M. Kamada, and T. Noguchi, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22615294].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22585215">The Cyclophilin Inhibitor SCY-635 Disrupts HCV NS5A-Cyclophilin A Complexes.</a> Hopkins, S., M. Bobardt, U. Chatterji, J.A. Garcia-Rivera, P. Lim, and P.A. Gallay, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22585215].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22580131">Discovery of Substituted N-Phenylbenzenesulphonamides as a Novel Class of Non-Nucleoside Hepatitis C Virus Polymerase Inhibitors.</a> May, M.M., D. Brohm, A. Harrenga, T. Marquardt, B. Riedl, J. Kreuter, H. Zimmermann, H. Ruebsamen-Schaeff, and A. Urban, Antiviral Research, 2012. <b>[Epub ahead of print]</b>; PMID[22580131].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22578461">Synthesis and Characterization of 2&#39;-C-Me Branched C-nucleosides as HCV Polymerase Inhibitors.</a> Cho, A., L. Zhang, J. Xu, D. Babusis, T. Butler, R. Lee, O.L. Saunders, T. Wang, J. Parrish, J. Perry, J.Y. Feng, A.S. Ray, and C.U. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22578461].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <h2>HSV-1</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22578783">Synthesis and Antiviral Evaluation of C5-Substituted-(1,3-diyne)-2&#39;-deoxyuridines.</a> Sari, O., V. Roy, J. Balzarini, R. Snoeck, G. Andrei, and L.A. Agrofoglio, European Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22578783].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22619617">Cytotoxic, Virucidal, and Antiviral Activity of South American Plant and Algae Extracts.</a> Faral-Tello, P., S. Mirazo, C. Dutra, A. Perez, L. Geis-Asteggiante, S. Frabasile, E. Koncke, D. Davyt, L. Cavallaro, H. Heinzen, and J. Arbiza, The Scientific World Journal, 2012. 2012: p. 174837; PMID[22619617].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22611351">Antiviral Activities and Putative Identification of Compounds in Microbial Extracts from the Hawaiian Coastal Waters.</a> Tong, J., H. Trapido-Rosenthal, J. Wang, Y. Wang, Q.X. Li, and Y. Lu, Marine Drugs, 2012. 10(3): p. 521-538; PMID[22611351].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22584352">In Vitro Antiviral Activity of Dehydroepiandrosterone, 17 Synthetic Analogs and ERK Modulators against Herpes Simplex Virus Type 1.</a> Torres, N.I., V. Castilla, A.C. Bruttomesso, J. Eiras, L.R. Galagovsky, and M.B. Wachsman, Antiviral Research, 2012. <b>[Epub ahead of print]</b>; PMID[22584352].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22584350">Inhibition of Herpes Simplex Virus Infection by Oligomeric Stilbenoids through ROS Generation.</a> Chen, X., H. Qiao, T. Liu, Z. Yang, L. Xu, Y. Xu, H.M. Ge, R.X. Tan, and E. Li, Antiviral Research, 2012. <b>[Epub ahead of print]</b>; PMID[22584350].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22580497">In Vitro and in Vivo Evaluation of a Novel Antiherpetic Flavonoid, 4&#39;-Phenylflavone, and Its Synergistic Actions with Acyclovir.</a> Hayashi, K., M. Iinuma, K. Sasaki, and T. Hayashi, Archives of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22580497].
    <br />
    <b>[PubMed]</b>. OV_0511-052412.</p>
    <p> </p>
    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303256700002">Novel Peptide Mimetics Based on N-protected Amino Acids Derived from Isomannide as Potential Inhibitors of NS3 Serine Protease of Hepatitis C Virus.</a> Barros, T.G., B.C. Zorzanelli, S. Pinheiro, M.A. de Brito, A. Tanuri, E.C.B. da Costa, R.S. Mohana-Borges, C.R. Rodrigues, A.T.M. Souza, V.F. Ferreira, and E.M.F. Muri, Letters in Organic Chemistry, 2012. 9(4): p. 239-249; ISI[000303256700002].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241302279">Antiviral Activity and Resistance Profile of the Novel HCV NS5A Inhibitor GS-5885.</a> Cheng, G., B. Peng, A. Corsa, M. Yu, M. Nash, Y.J. Lee, Y. Xu, T. Kirschberg, Y. Tian, J. Taylor, J.O. Link, and W. Delaney, Journal of Hepatology, 2012. 56: p. S464-S464; ISI[000303241302279].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302768200010">Synthesis and Antiviral Activity of a Series of 1 &#39;-Substituted 4-aza-7,9-dideazaadenosine C-Nucleosides.</a> Cho, A., O.L. Saunders, T. Butler, L.J. Zhang, J. Xu, J.E. Vela, J.Y. Feng, A.S. Ray, and C.U. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2705-2707; ISI[000302768200010].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241302280">Identification and Characterization of PPI-383, a Next Generation HCV NS5B Non-Nucleoside Inhibitor with Potent Activity against all Major HCV Genotypes.</a> Colonno, R., N. Huang, M. Lau, Q. Huang, A. Huq, E. Peng, M. Bencsik, M. Zhong, and L. Li, Journal of Hepatology, 2012. 56: p. S464-S464; ISI[000303241302280].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241300058">Minimal-Length shRNAs are Potent Inhibitors of Hepatitis C Virus in HCV-Infected Chimeric Mice.</a> Dallas, A., H. Ma, H. Ilves, J. Shorenstein, M. Behlke, S.P. Wong, R. Harbottle, I. Maclachlan, S. Kazakov, S. Sankuratri, K. Klumpp, and B.H. Johnston, Journal of Hepatology, 2012. 56: p. S25-S25; ISI[000303241300058].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241302219">Raloxifene Hydrochloride as a Novel Antiviral Agent: Inhibition of Hepatitis C Virus (HCV) Replication.</a> Furusyo, N., E. Ogawa, M. Sudoh, M. Murata, T. Ihara, H. Ikezaki, T. Hayashi, S. Hiramine, and J. Hayashi, Journal of Hepatology, 2012. 56: p. S438-S438; ISI[000303241302219].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302999600026">Direct Binding of a Hepatitis C Virus Inhibitor to the Viral Capsid Protein.</a> Kota, S., V. Takahashi, F. Ni, J.K. Snyder, and A.D. Strosberg, Plos One, 2012. 7(2); ISI[000302999600026].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241301410">Inhibition of Hepatitis C Virus Infectivity by Anti-viral Drugs in Human HCV Infected Liver Slices Culture.</a> Lagaye, S., P. Halfon, H. Shen, J. Gaston, L. Hannoun, P.P. Massault, A. Vallet-Pichard, V. Mallet, and S. Pol, Journal of Hepatology, 2012. 56: p. S332-S332; ISI[000303241301410].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241301420">The Lectin Griffithsin has Antiviral Activity Against Hepatitis C Virus in Vitro and in Vivo.</a> Meuleman, P., A. Albecka, S. Belouzard, K. Vercauteren, L. Verhoye, C. Wychowski, G. Leroux-Roels, K.E. Palmer, and J. Dubuisson, Journal of Hepatology, 2012. 56: p. S335-S336; ISI[000303241301420].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302964300045">Discovery of 4 &#39;-Azido-2 &#39;-deoxy-2 &#39;-C-methyl Cytidine and Prodrugs Thereof: A Potent Inhibitor of Hepatitis C Virus Replication.</a> Nilsson, M., G. Kalayanov, A. Winqvist, P. Pinho, C. Sund, X.X. Zhou, H. Wahling, A.K. Belfrage, M. Pelcman, T. Agback, K. Benckestock, K. Wikstrom, M. Boothee, A. Lindqvist, C. Rydegard, T.H.M. Jonckers, K. Vandyck, P. Raboisson, T.I. Lin, S. Lachau-Durand, H. de Kock, D.B. Smith, J.A. Martin, K. Klumpp, K. Simmen, L. Vrang, Y. Terelius, B. Samuelsson, A. Rosenquist, and N.G. Johansson, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(9): p. 3265-3268; ISI[000302964300045].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241302307">Antiviral Activity of EP-NI266, a Potent Nucleotide HCV Polymerase Inhibitor.</a> Owens, C.M., M.H.J. Rhodin, A. Polemeropoulos, I.J. Kim, Y.L. Qiu, G. Wang, L.J. Jiang, and Y.S. Or, Journal of Hepatology, 2012. 56: p. S475-S476; ISI[000303241302307].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241301426">In Vitro Combinatory Effect of HCV NS3/4A Protease Inhibitor ABT-450, NS5A Inhibitor ABT-267, and Non-Nucleoside NS5B Polymerase Inhibitor ABT-333.</a>Pilot-Matias, T., G. Koev, P. Krishnan, J. Beyer, T. Reisch, R. Mondal, L. Lu, T. Middleton, C. Maring, W. Kati, C. Collins, and A. Molla, Journal of Hepatology, 2012. 56: p. S338-S338; ISI[000303241301426].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302800600009">Anti-hepatitis C Virus Activity of 3-Hydroxy Caruilignan C from Swietenia macrophylla Stems.</a> Wu, S.F., C.K. Lin, Y.S. Chuang, F.R. Chang, C.K. Tseng, Y.C. Wu, and J.C. Lee, Journal of Viral Hepatitis, 2012. 19(5): p. 364-370; ISI[000302800600009].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303261000005">Inhibition of Hepatitis C Virus Replication and Viral Helicase by Ethyl Acetate Extract of the Marine Feather Star Alloeocomatella polycladia.</a> Yamashita, A., K.A. Salam, A. Furuta, Y. Matsuda, O. Fujita, H. Tani, Y. Fujita, Y. Fujimoto, M. Ikeda, N. Kato, N. Sakamoto, S. Maekawa, N. Enomoto, M. Nakakoshi, M. Tsubuki, Y. Sekiguchi, S. Tsuneda, N. Akimitsu, N. Noda, J. Tanaka, and K. Moriishi, Marine Drugs, 2012. 10(4): p. 744-761; ISI[000303261000005].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
    <p> </p>
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303241301404">Preclinical Characteristics of ACH-3102: A Novel HCV NS5A Inhibitor with Improved Potency against Genotype-1A Virus and Variants Resistant to 1st Generation of NS5A Inhibitors.</a> Yang, G., J. Wiles, D. Patel, Y. Zhao, J. Fabrycki, S. Weinheimer, C. Marlor, J. Rivera, Q. Wang, V. Gadhachanda, A. Hashimoto, D. Chen, G. Pais, X. Wang, M. Deshpande, K. Stauber, M. Huang, and A. Phadke, Journal of Hepatology, 2012. 56: p. S330-S330; ISI[000303241301404].
    <br />
    <b>[WOS]</b>. OV_0511-052412.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
