

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-09-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WKUrPnteR8iL1f3C1fR86ywKwu4O+cnj9vfZAagQX0tz+KXxx2My+u6sCDNdVKhx85LcZUfJg7u/1mpZ50zmWX2u9se7amZgiIZGt3XLwK5PW9hpZMTp02wApKI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DB652ECB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 30 - September 12, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23617302">Therapeutic Potential of Furin Inhibitors for the Chronic Infection of Hepatitis B Virus<i>.</i></a> Pang, Y.J., X.J. Tan, D.M. Li, Z.H. Zheng, R.X. Lei, and X.M. Peng. Liver International, 2013. 33(8): p. 1230-1238; PMID[23617302].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23891183">Peroxisome Proliferator-activated Receptor Delta Antagonists Inhibit Hepatitis C Virus RNA Replication<i>.</i></a> Ban, S., Y. Ueda, M. Ohashi, K. Matsuno, M. Ikeda, N. Kato, and H. Miyachi. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4774-4778; PMID[23891183].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23911854">Synthesis and Broad Spectrum Antiviral Evaluation of bis(POM) Prodrugs of Novel Acyclic Nucleosides<i>.</i></a> Hamada, M., V. Roy, T.R. McBrayer, T. Whitaker, C. Urbina-Blanco, S.P. Nolan, J. Balzarini, R. Snoeck, G. Andrei, R.F. Schinazi, and L.A. Agrofoglio. European Journal of Medicinal Chemistry, 2013. 67: p. 398-408; PMID[23911854].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24016850">The Antiviral Activity of Poly-gamma-Glutamic acid, a Polypeptide Secreted by Bacillus sp., through Induction of CD14-Dependent Type I Interferon Responses<i>.</i></a> Lee, W., S.H. Lee, D.G. Ahn, H. Cho, M.H. Sung, S.H. Han, and J.W. Oh. Biomaterials, 2013. <b>[Epub ahead of print]</b>; PMID[24016850].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23519646">Suppressive Effect of the Histone Deacetylase Inhibitor Suberoylanilide hydroxamic acid (SAHA) on Hepatitis C Virus Replication<i>.</i></a> Sato, A., Y. Saito, K. Sugiyama, N. Sakasegawa, T. Muramatsu, S. Fukuda, M. Yoneya, M. Kimura, H. Ebinuma, T. Hibi, M. Ikeda, N. Kato, and H. Saito. Journal of Cellular Biochemistry, 2013. 114(9): p. 1987-1996; PMID[23519646].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23910645">Protein Phosphatase 2A Impairs IFNalpha-induced Antiviral Activity against the Hepatitis C Virus through the Inhibition of STAT1 Tyrosine Phosphorylation<i>.</i></a> Shanker, V., G. Trincucci, H.M. Heim, and H.T. Duong. Journal of Viral Hepatitis, 2013. 20(9): p. 612-621; PMID[23910645].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23827234">Synthesis and Biological Activity of Benzo-fused 7-Deazaadenosine Analogues. 5- and 6-Substituted 4-Amino- or 4-alkylpyrimido[4,5-b]indole ribonucleosides<i>.</i></a> Tichy, M., R. Pohl, E. Tloust&#39;ova, J. Weber, G. Bahador, Y.J. Lee, and M. Hocek. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 5362-5372; PMID[23827234].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24023620">New Preclinical Antimalarial Drugs Potently Inhibit Hepatitis C Virus Genotype 1b RNA Replication<i>.</i></a> Ueda, Y., M. Takeda, K. Mori, H. Dansako, T. Wakita, H.S. Kim, A. Sato, Y. Wataya, M. Ikeda, and N. Kato. Plos One, 2013. 8(8): p. e72519; PMID[24023620].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24013001">In Vitro Efficacy of Approved and Experimental Antivirals against Novel Genotype 3 Hepatitis C Virus Subgenomic Replicons<i>.</i></a> Yu, M., A.C. Corsa, S. Xu, B. Peng, R. Gong, Y.J. Lee, K. Chan, H. Mo, W.t. Delaney, and G. Cheng. Antiviral Research, 2013. <b>[Epub ahead of print]</b>; PMID[24013001].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23669101">Antibody Inhibition of Human Cytomegalovirus Spread in Epithelial Cell Cultures<i>.</i></a> Cui, X., R. Lee, S.P. Adler, and M.A. McVoy. Journal of Virological Methods, 2013. 192(1-2): p. 44-50; PMID[23669101].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24003183">Stability and Antiviral Activity against Human Cytomegalovirus of Artemisinin Derivatives<i>.</i></a> Flobinus, A., N. Taudon, M. Desbordes, B. Labrosse, F. Simon, M.C. Mazeron, and N. Schnepf. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[24003183].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0830-091213.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001444">Fragment Based Drug Discovery of New Cyclophilin Inhibitors Unrelated to Cyclosporine A That Potently Inhibit HCV Replication<i>.</i></a> Ahmed-Belkacem, A., L. Colliandre, N. Ahnou, P. Barthe, W. Bourget, D. Douguet, J.F. Guichou, and J.M. Pawlotsky. Journal of Hepatology, 2013. 58: p. S481-S481; ISI[000322983001444].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001451">GS-5816, a Second Generation HCV NS5A Inhibitor with Potent Antiviral Activity, Broad Genotypic Coverage and a High Resistance Barrier<i>.</i></a> Cheng, G., M. Yu, B. Peng, Y.J. Lee, A. Trejo-Martin, R. Gong, C. Bush, A. Worth, M. Nash, K. Chan, H. Yang, R. Beran, Y. Tian, J. Perry, J. Taylor, C. Yang, M. Paulson, W. Delaney, and J.O. Link. Journal of Hepatology, 2013. 58: p. S484-S485; ISI[000322983001451].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322612400003">Synthesis and anti-HCV Activity of 2,6-Bisarylmethyloxy-5-hydroxy-7-phenylchromones<i>.</i></a> Cho, S.Y., C.W. Lee, and Y.H. Chong. Bulletin of the Korean Chemical Society, 2013. 34(7): p. 1953-1954; ISI[000322612400003].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001682">Synergistic Interactions of HCV NS5A Replication Complex Inhibitors Sensitize Resistant Variants and Enhance the Efficacy of Daclatasvir (DCV, BMS-790052) in Vitro and in Vivo<i>.</i></a> Gao, M., D.R. O&#39;Boyle, J.A. Lemm, R.A. Fridell, C. Wang, S. Roberts, M. Liu, P. Nower, Y.K. Wang, B.M. Johnson, M. Kramer, F. Moulin, M.J. Nophsker, P. Hewawasam, J. Kadow, M. Cockett, N.A. Meanwell, M. Belema, and J.H. Sun. Journal of Hepatology, 2013. 58: p. S573-S574; ISI[000322983001682].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001413">Cyclophilin Inhibitor Alisporivir (ALV) Combinations with Direct Acting Antivirals Reveal Strong Synergistic anti-HCV Effects<i>.</i></a> Garcia-Rivera, J., U. Chatterji, and P. Gallay. Journal of Hepatology, 2013. 58: p. S469-S469; ISI[000322983001413].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001428">BL-8030: A Novel, Potent, Selective, Orally Available Inhibitor of Hepatitis C Virus NS3/4A Protease<i>.</i></a> Halfon, P., J. Courcambeck, T. Whitaker, P.M. Tharnish, T.R. McBrayer, S.J. Coats, Y. Pereg, R. Tabakman, J. Schumann, M. Matto, and R.F. Schinazi. Journal of Hepatology, 2013. 58: p. S475-S475; ISI[000322983001428].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322958900021">Interferon alfa Partially Inhibits HIV Replication in Hepatocytes in Vitro<i>.</i></a> Kong, L. and J.T. Blackard. Journal of Infectious Diseases, 2013. 208(5): p. 865-866; ISI[000322958900021].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001422">Turmeric Curcumin Inhibits Entry of All Hepatitis C Virus Genotypes into Human Liver Cells<i>.</i></a> Kusuma, A., C.C. Colpitts, L.M. Schang, H. Rachmawati, A. Frentzen, S. Pfaender, P. Behrendt, R.J.P. Brown, D. Bankwitz, J. Steinmann, M. Ott, P. Meuleman, T. Pietschmann, and E. Steinmann. Journal of Hepatology, 2013. 58: p. S473-S473; ISI[000322983001422].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001465">Preliminary Evaluation of a New Autophagy Inhibitor Antiviral Agent for Hepatitis C Virus in an ex Vivo Model of Human Liver Slices Culture<i>.</i></a> Lagaye, S., H. Shen, J. Gaston, G. Rousseau, P.P. Massault, A. Vallet-Pichard, P. Sogni, V. Mallet, J. Courcambeck, S. Brun, F. Bassisi, C. Camus, P. Halfon, and S. Pol. Journal of Hepatology, 2013. 58: p. S490-S490; ISI[000322983001465].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323093300005">Unusual nor-Sesquiterpene lactone from the Fruits of Illicium henryi<i>.</i></a> Liu, J.F., Y.F. Wang, Y.P. Bi, H.J. Li, L. Jia, Y.F. Bi, and Y.B. Zhang. Tetrahedron Letters, 2013. 54(36): p. 4834-4836; ISI[000323093300005].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323416000007">A New Role for the Cellular PABP Repressor PAIP2 as an Innate Restriction Factor Capable of Limiting Productive Cytomegalovirus Replication<i>.</i></a> McKinney, C., D. Yu, and I. Mohr. Genes &amp; Development, 2013. 27(16): p. 1809-1820; ISI[000323416000007].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323362100011">A 96-Well Based Analysis of Replicon Elimination with the HCV NS5A Replication Complex Inhibitor Daclatasvir<i>.</i></a> O&#39;Boyle, D.R., P.T. Nower, J.H. Sun, R. Fridell, C.F. Wang, L. Valera, and M. Gao. Journal of Virological Methods, 2013. 193(1): p. 68-76; ISI[000323362100011].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001473">Cyclophilin Inhibitor EDP-546 Is a Potential Cornerstone Drug for Use in Combination with NS5A and Protease Inhibitors Due to Its High Barrier to HCV Resistance<i>.</i></a> Owens, C.M., M.H.J. Rhodin, A. Polemeropoulos, J. Long, G. Wang, L. Jiang, and Y.S. Or. Journal of Hepatology, 2013. 58: p. S493-S493; ISI[000322983001473].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322983001438">A Nanobody Recognizing a Novel Epitope in Hepatitis C Virus Glycoprotein E2 Broadly Neutralizes Virus Entry and Inhibits Cell-Cell Transmission<i>.</i></a> Tarr, A.W., P. Lafaye, L. Damier-Piolle, L. Meredith, R.A. Urbanowicz, A. Meola, J.L. Jestin, R.J.P. Brown, J.A. McKeating, F.A. Rey, J.K. Ball, and T. Krey. Journal of Hepatology, 2013. 58: p. S479-S479; ISI[000322983001438].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323231900001">Potential Role for Statins in the Treatment of Chronic HCV Infection<i>.</i></a> Zhu, Q.Q., Q.Y. Han, and Z.W. Liu. Future Virology, 2013. 8(8): p. 727-729; ISI[000323231900001].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0830-091213.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
