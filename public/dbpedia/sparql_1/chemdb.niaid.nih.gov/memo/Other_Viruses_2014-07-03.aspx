

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-07-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="SFRTWFE26707BO5R3YSdyoOR/EbHVVbE/QN/+SDpxPheQ+RgcA+9w+EwMfdiFdsZx6jlEBpZTfTBS4bSsqPqFW4K32Q//snpTdCR0djshDvnzWpveB4VbUrMhSY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0490687A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: June 20 - July 3, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24769245">Arbidol as a Broad-spectrum Antiviral: An Update</a>. Blaising, J., S.J. Polyak, and E.I. Pecheur. Antiviral Research, 2014. 107C: p. 84-94. PMID[24769245].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24569930">Inhibition of Hepatitis B Virus cccDNA by siRNA in Transgenic Mice</a>. Li, G., G. Jiang, J. Lu, S. Chen, L. Cui, J. Jiao, and Y. Wang. Cell Biochemistry &amp; Biophysics, 2014. 69(3): p. 649-654. PMID[24569930].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24733478">Efficiency of Incorporation and Chain Termination Determines the Inhibition Potency of 2&#39;-Modified Nucleotide Analogs Against Hepatitis C Virus Polymerase</a>. Fung, A., Z. Jin, N. Dyatkina, G. Wang, L. Beigelman, and J. Deval. Antimicrobial Agents and Chemotherapy, 2014. 58(7): p. 3636-3645. PMID[24733478].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24835734">Structural and Energetic Analysis of 2-Aminobenzimidazole Inhibitors in Complex with the Hepatitis C Virus IRES RNA Using Molecular Dynamics Simulations</a>. Henriksen, N.M., H.S. Hayatshahi, D.R. Davis, and T.E. Cheatham, 3rd. Journal of Chemical Information and Modeling, 2014. 54(6): p. 1758-1772. PMID[24835734].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24958333">Small Molecule Inhibitors of HCV Replication from Pomegranate</a>. Reddy, B.U., R. Mullick, A. Kumar, G. Sudha, N. Srinivasan, and S. Das. Scientific Reports, 2014. 4: p. 5411. PMID[24958333].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24821256">Antiviral Activity of Topoisomerase II Catalytic Inhibitors Against Epstein-Barr Virus</a>. Wu, T., Y. Wang, and Y. Yuan. Antiviral Research, 2014. 107: p. 95-101. PMID[24821256].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24844757">Synthesis of Triterpenoid Triazine Derivatives from Allobetulone and Betulonic acid with Biological Activities</a>. Dinh Ngoc, T., N. Moons, Y. Kim, W. De Borggraeve, A. Mashentseva, G. Andrei, R. Snoeck, J. Balzarini, and W. Dehaen. Bioorganic &amp; Medicinal Chemistry, 2014. 22(13): p. 3292-3300. PMID[24844757].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p>

    <h2>Herpes Simplex Virus I</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24799418">Oligolysine-conjugated Zinc(II) Phthalocyanines as Efficient Photosensitizers for Antimicrobial Photodynamic Therapy</a>. Ke, M.R., J.M. Eastel, K.L. Ngai, Y.Y. Cheung, P.K. Chan, M. Hui, D.K. Ng, and P.C. Lo. Chemistry - An Asian Journal, 2014. 9(7): p. 1868-1875. PMID[24799418].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0620-070314.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337089000001">MK-5172: A Second-generation Protease Inhibitor for the Treatment of Hepatitis C Virus Infection</a>. Gentile, I., A.R. Buonomo, and F. Borgia. Expert Opinion on Investigational Drugs, 2014. 23(7): p. 719-728. ISI[000337089000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0620-070314.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336510900020">Anti-HCV Activity of the Chinese Medicinal Fungus Cordyceps militaris</a>. Ueda, Y., K. Mori, S. Satoh, H. Dansako, M. Ikeda, and N. Kato. Biochemical &amp; Biophysical Research Communications, 2014. 447(2): p. 341-345. ISI[000336510900020].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0620-070314.</p><br /> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
