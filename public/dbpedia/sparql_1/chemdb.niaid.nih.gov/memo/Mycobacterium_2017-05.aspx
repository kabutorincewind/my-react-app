

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sgLwoXtQ67YczLazmSQ6o3Es4isq5DCPKjegnAGTn2khneq5Czu7pw94BZOBwKC187f5lK6fVFWT8T5+DQyO/Y8o4EG2KAYA8yqzQOb8uTBmQF1T+vE4+nk4DwU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D81975D2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800"><i>Mycobacterium</i> Citations List: May, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p>1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400365900006">Synthesis and Biological Evaluation of 1,3,4-Trisubstituted pyrazole Analogues as Anti-mycobacterial Agents.</a> Alegaon, S.G., M.B. Hirpara, K.R. Alagawadi, S.S. Jalalpure, V.P. Rasal, P.S. Salve, and V.M. Kumbar. Medicinal Chemistry Research, 2017. 26(6): p. 1127-1138.  ISI[000400365900006].
    <br />
    <b>[WOS]</b>. TB_05_2017.</p>

    <p>2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399414100073">2-(5-Chlorobenzo[d]thiazol-2-ylimino)thiazolidin-4-one Derivatives as an Antimicrobial Agent.</a> B&#39;Bhatt, H. and S. Sharma. Arabian Journal of Chemistry, 2017. 10: p. S531-S538.  ISI[000399414100073]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399237300002">Library Design, Synthesis and Biological Exploration of Novel 3,4&#39;-Bicarbostyril Derivatives as Potent Antimicrobial, Antitubercular and Antimalarial Agents.</a> Jardosh, H.H., N.D. Vala, and M.P. Patel. Medicinal Chemistry Research, 2017. 26(5): p. 881-899.  ISI[000399237300002]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400211800023">Transition Metal Complexes of 2-(2-(1H-Benzo[d]imidazol-2-yl)hydrazono)propan-1-ol: Synthesis, Characterization, Crystal Structures and Anti-tuberculosis Assay with Docking Studies.</a> Kamat, V., D. Kokare, K. Naik, A. Kotian, S. Naveen, S.R. Dixit, N.K. Lokanath, S.D. Joshi, and V.K. Revankar. Polyhedron, 2017. 127: p. 225-237.  ISI[000400211800023]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28326811">Combinations of Avibactam and Carbapenems Exhibit Enhanced Potencies against Drug-resistant Mycobacterium abscessus.</a> Kaushik, A., C. Gupta, S. Fisher, E. Story-Roller, C. Galanis, N. Parrish, and G. Lamichhane. Future Microbiology, 2017. 12: p. 473-480. PMID[28326811]. <b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <p>6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400365900007">Design, Characterization, in Vitro Antibacterial, Antitubercular Evaluation and Structure-Activity Relationships of New Hydrazinyl thiazolyl coumarin Derivatives.</a> KhanYusufzai, S., H. Osman, M.S. Khan, S. Mohamad, O. Sulaiman, T. Parumasivam, J.A. Gansau, N. Johansah, and Noviany. Medicinal Chemistry Research, 2017. 26(6): p. 1139-1148.  ISI[000400365900007]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28293833">Efficient Synthesis and Biological Evaluation of New Benzopyran-annulated Pyrano[2,3-c]pyrazole Derivatives.</a> Labana, B.M., G.C. Brahmbhatt, T.R. Sutariya, N.J. Parmar, J.M. Padron, R. Kant, and V.K. Gupta. Molecular Diversity, 2017. 21(2): p. 339-354. PMID[28293833]. <b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <p>8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28462049">Octahedral Ruthenium(II) polypyridyl Complexes as Antimicrobial Agents against Mycobacterium.</a> Liao, G.J., Z.Y. Ye, Y.L. Liu, B. Fu, and C. Fu. PeerJ, 2017. 5: e3252. PMID[28462049]. PMCID[PMC5410163].<b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <p>9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399414100108">Synthesis and Pharmacological Studies of 1-(2-Amino-1-(4-methoxyphenyl) ethyl) cyclohexanol Analogs as Potential Microbial Agents.</a> Mahyavanshi, V., S.I. Marjadi, and R. Yadav. Arabian Journal of Chemistry, 2017. 10: p. S804-S813.  ISI[000399414100108]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399414100131">Synthesis and Antitubercular Evaluation of Imidazo[2,1-b][1,3,4]thiadiazole Derivatives.</a> Patel, H.M., M.N. Noolvi, N.S. Sethi, A.K. Gadad, and S.S. Cameotra. Arabian Journal of Chemistry, 2017. 10: p. S996-S1002.  ISI[000399414100131]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400365900014">Synthesis, Spectral, Biological Activity, and Crystal Structure Evaluation of Novel Pyrazoline Derivatives Having Sulfonamide Moiety.</a> Sadashiva, R., D. Naral, J. Kudva, N. Shivalingegowda, N.K. Lokanath, and K.J. Pampa. Medicinal Chemistry Research, 2017. 26(6): p. 1213-1227.  ISI[000400365900014]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28274627">Three-component, One-pot Synthesis of Anthranilamide Schiff Bases Bearing 4-Aminoquinoline Moiety as Mycobacterium tuberculosis Gyrase Inhibitors.</a> Salve, P.S., S.G. Alegaon, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(8): p. 1859-1866. PMID[28274627]. <b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <p>13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28096545">Thiazomycin, Nocathiacin and Analogs Show Strong Activity against Clinical Strains of Drug-resistant Mycobacterium tuberculosis.</a> Singh, S.B., L. Xu, P.T. Meinke, N. Kurepina, B.N. Kreiswirth, D.B. Olsen, and K. Young. The Journal of Antibiotics, 2017. 70(5): p. 671-674. PMID[28096545]. <b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <p>14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400545300005">1,5-Diarylpyrroles as Potent Antitubercular and Anti-inflammatory Agents.</a> Venditti, G., G. Poce, S. Consalvi, and M. Biava. Chemistry of Heterocyclic Compounds, 2017. 53(3): p. 281-291.  ISI[000400545300005]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28290692">Amphiphilic Indole Derivatives as Antimycobacterial Agents: Structure-Activity Relationships and Membrane Targeting Properties.</a> Yang, T.M., W. Moreira, S.A. Nyantakyi, H. Chen, D.B. Aziz, M.L. Go, and T. Dick. Journal of Medicinal Chemistry, 2017. 60(7): p. 2745-2763. PMID[28290692]. <b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <p>16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399236900006">Antituberculosis Agents Bearing the 1,2-Disubstituted benzimidazole Scaffold.</a> Yeong, K.Y., C.W. Ang, M.A. Ali, H. Osman, and S.C. Tan. Medicinal Chemistry Research, 2017. 26(4): p. 770-778.  ISI[000399236900006]. <b><br />
    [WOS]</b>. TB_05_2017.</p>

    <p>17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28181080">Madurastatin B3, a Rare Aziridine Derivative from Actinomycete Nocardiopsis sp LS150010 with Potent Anti-tuberculosis Activity.</a> Zhang, X.J., H.T. He, R. Ma, Z.C. Ji, Q. Wei, H.Q. Dai, L.X. Zhang, and F.H. Song. Journal of Industrial Microbiology &amp; Biotechnology, 2017. 44(4-5): p. 589-594. PMID[28181080]. <b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <p>18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28523106">Discovery of Fluorine-containing Benzoxazinyl-oxazolidinones for the Treatment of Multidrug Resistant Tuberculosis.</a> Zhao, H., Y. Lu, L. Sheng, Z. Yuan, B. Wang, W. Wang, Y. Li, C. Ma, X. Wang, D. Zhang, and H. Huang. ACS Medicinal Chemistry Letters, 2017. 8(5): p. 533-537. PMID[28523106]. PMCID[PMC5430409].<b><br />
    [PubMed]</b>. TB_05_2017.</p>

    <h2>Patent Citations</h2>

    <p>19. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170102&amp;CC=MX&amp;NR=2015008582A&amp;KC=A">Peptides from Scorpion Vaejovis punctatus as Antibiotics against Mycobacterium tuberculosis and Further Pathogen Bacteria and Yeasts.</a> Corzo Burguete, G.A., L.D. Possani Postay, E. Ortiz Suri, S. Ramirez Carreto, J.M. Jimenez Vargas, and B. Becerril Lujan. Patent. 2017. 2015-8582 2015008582: 43pp.
    <br />
    <b>[Patent]</b>. TB_05_2017.</p>

    <p>20. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170504&amp;CC=WO&amp;NR=2017070763A1&amp;KC=A1">Pharmaceutical Composition, Use of Mefloquine in a Fixed Dose, and Method for Treating Tuberculosis.</a> De Souza, M.V.N., R.S.B. Goncalves, and M.C.d.S. Lourenco. Patent. 2017. 2016-BR50254 2017070763: 25pp.
    <br />
    <b>[Patent]</b>. TB_05_2017.</p>

    <p>21. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170526&amp;CC=WO&amp;NR=2017087876A1&amp;KC=A1">Compositions and Methods for Treating Bacterial Infections.</a> Perkins, W., V. Malinin, F. Leifer, C. Figueroa, and K. Dipetrillo. Patent. 2017. 2016-US62894 2017087876: 77pp.
    <br />
    <b>[Patent]</b>. TB_05_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
