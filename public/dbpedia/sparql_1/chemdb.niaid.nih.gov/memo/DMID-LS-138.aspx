

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-138.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cg7l0cBIZ4k1qAEJbCx7wbaf6osxOsLpJjYwoSNy1pb/fjr+gwhEf6F3Gw+aDUqQLVv4CAT1zidaCiJ4KZyvyyZ3qpI4wVkYH6zq701CmLRyuhywfK5PCYe0n98=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AEB803E8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-138-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60131   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          An analogue of AICAR with dual inhibitory activity against WNV and HCV NTPase/helicase: Synthesis and in vitro screening of 4-carbamoyl-5-(4,6-diamino-2,5-dihydro-1,3,5-triazin-2-yl)imidazole-1-beta -d-ribofuranoside</p>

    <p>          Ujjinamatada, RK, Baier, A, Borowski, P, and Hosmane, RS</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17289387&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17289387&amp;dopt=abstract</a> </p><br />

    <p>2.     60132   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Design, synthesis, inhibitory activity, and SAR studies of pyrrolidine derivatives as neuraminidase inhibitors</p>

    <p>          Zhang, J, Wang, Q, Fang, H, Xu, W, Liu, A, and Du, G</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17287121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17287121&amp;dopt=abstract</a> </p><br />

    <p>3.     60133   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Inhibition of West Nile virus replication by short interfering RNAs</p>

    <p>          Kachko, AV, Ivanova, AV, Protopopova, EV, Netesov, V, and Loktev, VB</p>

    <p>          Dokl Biochem Biophys <b>2006</b>.  410: 260-2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17286097&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17286097&amp;dopt=abstract</a> </p><br />

    <p>4.     60134   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          The Alkaloid 4-Methylaaptamine Isolated from the Sponge Aaptos aaptos Impairs Herpes simplex Virus Type 1 Penetration and Immediate-Early Protein Synthesis</p>

    <p>          Souza, TM, Abrantes, JL, Epifanio, RD, Fontes, CF, and Frugulhetti, IC</p>

    <p>          Planta Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17285480&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17285480&amp;dopt=abstract</a> </p><br />

    <p>5.     60135   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Nucleic acid-based antiviral drugs against seasonal and avian influenza viruses</p>

    <p>          Wong, JP, Christopher, ME, Salazar, AM, Dale, RM, Sun, LQ, and Wang, M</p>

    <p>          Vaccine <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17280757&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17280757&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60136   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Anti-hepatitis B virus activity of wogonin in vitro and in vivo</p>

    <p>          Guo, Q, Zhao, L, You, Q, Yang, Y, Gu, H, Song, G, Lu, N, and Xin, J</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17280723&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17280723&amp;dopt=abstract</a> </p><br />

    <p>7.     60137   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          In vitro susceptibility of six isolates of equine herpesvirus 1 to acyclovir, ganciclovir, cidofovir, adefovir, PMEDAP and foscarnet</p>

    <p>          Garre, B, van der Meulen, K, Nugent, J , Neyts, J, Croubels, S, De Backer, P, and Nauwynck, H</p>

    <p>          Vet Microbiol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276631&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276631&amp;dopt=abstract</a> </p><br />

    <p>8.     60138   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          In search of effective anti-HHV-6 agents</p>

    <p>          De Clercq, E and Naesens, L</p>

    <p>          J Clin Virol <b>2006</b>.  37 Suppl 1: S82-S86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276375&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276375&amp;dopt=abstract</a> </p><br />

    <p>9.     60139   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Antiviral activity of diverse classes of broad-acting agents and natural compounds in HHV-6-infected lymphoblasts</p>

    <p>          Naesens, L, Bonnafous, P, Agut, H, and De, Clercq E</p>

    <p>          J Clin Virol <b>2006</b>.  37 Suppl 1: S69-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276373&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276373&amp;dopt=abstract</a> </p><br />

    <p>10.   60140   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Thiazolone-acylsulfonamides as novel HCV NS5B polymerase allosteric inhibitors: Convergence of structure-based drug design and X-ray crystallographic study</p>

    <p>          Yan, S, Appleby, T, Larson, G, Wu, JZ, Hamatake, RK, Hong, Z, and Yao, N</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276060&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276060&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60141   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus replication and expression by small interfering RNA targeting host cellular genes</p>

    <p>          Xue, Q, Ding, H, Liu, M, Zhao, P, Gao, J, Ren, H, Liu, Y, and Qi, ZT</p>

    <p>          Arch Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17273891&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17273891&amp;dopt=abstract</a> </p><br />

    <p>12.   60142   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Studies on acyl pyrrolidine inhibitors of HCV RNA-dependent RNA polymerase to identify a molecule with replicon antiviral activity</p>

    <p>          Burton, G, Ku, TW, Carr, TJ, Kiesow, T, Sarisky, RT, Lin-Goerke, J, Hofmann, GA, Slater, MJ, Haigh, D, Dhanak, D, Johnson, VK, Parry, NR, and Thommes, P</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270443&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270443&amp;dopt=abstract</a> </p><br />

    <p>13.   60143   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p><b>          Synthesis and antiviral activities of new acyclic and &quot;double-headed&quot; nucleoside analogues</b> </p>

    <p>          Zhang, X, Amer, A, Fan, X, Balzarini, J, Neyts, J, De, Clercq E, Prichard, M, Kern, E, and Torrence, PF</p>

    <p>          Bioorg Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270235&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270235&amp;dopt=abstract</a> </p><br />

    <p>14.   60144   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          New pandemics: HIV and AIDS, HCV and chronic hepatitis, Influenza virus and flu</p>

    <p>          Gatignol, A, Dubuisson, J, Wainberg, MA, Cohen, EA, and Darlix, JL</p>

    <p>          Retrovirology <b>2007</b>.  4(1): 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270043&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270043&amp;dopt=abstract</a> </p><br />

    <p>15.   60145   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis B Virus Polymerase by Entecavir</p>

    <p>          Langley, DR, Walsh, AW, Baldick, CJ, Eggers, BJ, Rose, RE, Levine, SM, Kapur, AJ, Colonno, RJ, and Tenney, DJ</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17267485&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17267485&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60146   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Study of drug resistance of chicken influenza A virus (H5N1) from homology-modeled 3D structures of neuraminidases</p>

    <p>          Wang, SQ, Du, QS, and Chou, KC</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.  354(3): 634-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266937&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266937&amp;dopt=abstract</a> </p><br />

    <p>17.   60147   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Structure and functions of influenza virus neuraminidase</p>

    <p>          Gong, J, Xu, W, and Zhang, J</p>

    <p>          Curr Med Chem <b>2007</b>.  14(1): 113-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266572&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266572&amp;dopt=abstract</a> </p><br />

    <p>18.   60148   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Antivirals for the treatment of polyomavirus BK replication</p>

    <p>          Rinaldo, CH and Hirsch, HH</p>

    <p>          Expert Rev Anti Infect Ther <b>2007</b>.  5(1): 105-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266458&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266458&amp;dopt=abstract</a> </p><br />

    <p>19.   60149   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Use of molecular assays for the diagnosis of influenza</p>

    <p>          Vernet, G</p>

    <p>          Expert Rev Anti Infect Ther <b>2007</b>.  5(1): 89-104</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266457&amp;dopt=abstract</a> </p><br />

    <p>20.   60150   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Unique Composite Active Site of the Hepatitis C Virus NS2-3 Protease: a New Opportunity for Antiviral Drug Design</p>

    <p>          Suo, Z and Abdullah, MA</p>

    <p>          ChemMedChem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266160&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266160&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   60151   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Therapeutic and Prophylactic Potential of Small Interfering RNAs against Severe Acute Respiratory Syndrome: Progress to Date</p>

    <p>          Chang, Z, Babiuk, LA, and Hu, J</p>

    <p>          BioDrugs <b>2007</b>.  21(1): 9-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17263585&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17263585&amp;dopt=abstract</a> </p><br />

    <p>22.   60152   DMID-LS-138; EMBASE-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antiviral Properties of 4&#39;-Substituted Ribonucleosides as Inhibitors of Hepatitis C Virus Replication. The Discovery of R1479</p>

    <p>          Smith, David B, Martin, Joseph A, Klumpp, Klaus, Baker, Stewart J, Blomgren, Peter A, Devos, Rene, Granycome, Caroline, Hang, Julie, Hobbs, Christopher J, Jiang, Wen-Rong, Laxton, Carl, Pogam, Sophie Le, Leveque, Vincent, Ma, Han, Maile, Graham, Merrett, John H, Pichota, Arkadius, Sarma, Keshab, Smith, Mark, Swallow, Steven, Symons, Julian, Vesey, David, Najera, Isabel, and Cammack, Nick</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: A14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N02J3Y-C/2/28f2e016bddd37805a825776d931e98e">http://www.sciencedirect.com/science/article/B6TF9-4N02J3Y-C/2/28f2e016bddd37805a825776d931e98e</a> </p><br />

    <p>23.   60153   DMID-LS-138; WOS-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Avian Hepatitis B Viruses: Molecular and Cellular Biology, Phylogenesis, and Host Tropism</p>

    <p>          Funk, A. <i>et al.</i></p>

    <p>          World Journal of Gastroenterology <b>2007</b>.  13(1): 91-103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243539200010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243539200010</a> </p><br />

    <p>24.   60154   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Structure-activity relationship (SAR) studies of quinoxalines as novel HCV NS5B RNA-dependent RNA polymerase inhibitors</p>

    <p>          Rong, F, Chow, S, Yan, S, Larson, G, Hong, Z, and Wu, J</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17258458&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17258458&amp;dopt=abstract</a> </p><br />

    <p>25.   60155   DMID-LS-138; WOS-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Famciclovir - a Review of Its Use in Herpes Zoster and Genital and Orolabial Herpes</p>

    <p>          Simpson, D. and Lyseng-Williamson, K.</p>

    <p>          Drugs <b>2006</b>.  66 (18): 2397-2416</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243490900015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243490900015</a> </p><br />

    <p>26.   60156   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Focus on hepatitis. HCV protease inhibitor does well in early clinical trial</p>

    <p>          Carter, M</p>

    <p>          IAPAC Mon <b>2006</b>.  12(2): 50-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17252625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17252625&amp;dopt=abstract</a> </p><br />

    <p>27.   60157   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Effects of cidofovir on a novel cell-based test system for recurrent respiratory papillomatosis</p>

    <p>          Donne, AJ, Hampson, L, He, XT, Rothera, MP, Homer, JJ, and Hampson, IN</p>

    <p>          Head Neck <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17252592&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17252592&amp;dopt=abstract</a> </p><br />

    <p>28.   60158   DMID-LS-138; WOS-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Influenza Neuraminidase Inhibitors as Antiviral Agents</p>

    <p>          Babu, YS, Chand, P, and Kotian, PL</p>

    <p>          Annual Reports in Medicinal Chemistry, Elsevier Academic Press Inc, Editor <b>2007</b>.  41: 287-297 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243457700019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243457700019</a> </p><br />

    <p>29.   60159   DMID-LS-138; EMBASE-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis B virus cccDNA replication by siRNA</p>

    <p>          Li, Gui-Qiu, Gu, Hong-Xi, Li, Di, and Xu, Wei-Zhen</p>

    <p>          Biochemical and Biophysical Research Communications <b>2007</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4N0HSD7-9/2/0ec9b61e66eb7ddbebfe425c17aa6df3">http://www.sciencedirect.com/science/article/B6WBK-4N0HSD7-9/2/0ec9b61e66eb7ddbebfe425c17aa6df3</a> </p><br />

    <p>30.   60160   DMID-LS-138; EMBASE-DMID-2/12/2007</p>

    <p class="memofmt1-2">          SCH 503034, a novel hepatitis C virus protease inhibitor, plus pegylated</p>

    <p>          Sarrazin, Christoph, Rouzier, Regine, Wagner, Frank, Forestier, Nicole, Larrey, Dominique, Gupta, Samir K, Hussain, Musaddeq, Shah, Amrik, Cutler, David, Zhang, Jenny, and Zeuzem, Stefan</p>

    <p>          Gastroenterology <b>2007</b>.  In Press, Accepted Manuscript: 368</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4MWXPS9-C/2/27b46f83bd32f759e0ef62ec04deeec0">http://www.sciencedirect.com/science/article/B6WFX-4MWXPS9-C/2/27b46f83bd32f759e0ef62ec04deeec0</a> </p><br />

    <p>31.   60161   DMID-LS-138; EMBASE-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Evaluation of the anti-hepatitis C virus effect of novel potent, selective, and orally bioavailable JNK and VEGFR kinase inhibitors</p>

    <p>          Raboisson, Pierre, Lenz, Oliver, Lin, Tse-I, Surleraux, Dominique, Chakravarty, Sarvajit, Scholliers, Annick, Vermeiren, Katrien, Delouvroy, Frederic, Verbinnen, Thierry, and Simmen, Kenneth</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  368</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MWPSGM-9/2/9494ec33b7f4be4b7574d4a0d211234b">http://www.sciencedirect.com/science/article/B6TF9-4MWPSGM-9/2/9494ec33b7f4be4b7574d4a0d211234b</a> </p><br />

    <p>32.   60162   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          The Immunobiology of SARS</p>

    <p>          Chen, J and Subbarao, K</p>

    <p>          Annu Rev Immunol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17243893&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17243893&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>33.   60163   DMID-LS-138; WOS-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Structure and Functions of Influenza Virus Neuraminidase</p>

    <p>          Gong, J., Xu, W., and Zhang, J.</p>

    <p>          Current Medicinal Chemistry <b>2007</b>.  14(1): 113-122</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243248500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243248500008</a> </p><br />

    <p>34.   60164   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Avian influenza outbreak investigation and containment</p>

    <p>          Lal, S and Singh, J</p>

    <p>          J Indian Med Assoc <b>2006</b>.  104(8): 462-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17240804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17240804&amp;dopt=abstract</a> </p><br />

    <p>35.   60165   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Introduction: advances in antiviral therapy for chronic hepatitis C infection-the influence of genotype and HIV co-infection</p>

    <p>          Schiff, ER</p>

    <p>          Nat Clin Pract Gastroenterol Hepatol <b>2007</b>.  4 Suppl 1: S1-2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17235279&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17235279&amp;dopt=abstract</a> </p><br />

    <p>36.   60166   DMID-LS-138; PUBMED-DMID-2/12/2007</p>

    <p class="memofmt1-2">          New and Emerging Treatment of Chronic Hepatitis B</p>

    <p>          Keeffe, EB and Marcellin, P</p>

    <p>          Clin Gastroenterol Hepatol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17218162&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17218162&amp;dopt=abstract</a> </p><br />

    <p>37.   60167   DMID-LS-138; WOS-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Impact of Neuraminidase Mutations Conferring Influenza Resistance to Neuraminidase Inhibitors in the N1 and N2 Genetic Backgrounds</p>

    <p>          Abed, Y., Baz, M., and Boivin, G.</p>

    <p>          Antiviral Therapy <b>2006</b>.  11(8): 971-976</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243183100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243183100002</a> </p><br />

    <p>38.   60168   DMID-LS-138; EMBASE-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Inhibitors of HCV NS5B polymerase: Synthesis and structure-activity relationships of unsymmetrical 1-hydroxy-4,4-dialkyl-3-oxo-3,4-dihydronaphthalene benzothiadiazine derivatives</p>

    <p>          Krueger, AChris, Madigan, Darold L, Green, Brian E, Hutchinson, Douglas K, Jiang, Wen W, Kati, Warren M, Liu, Yaya, Maring, Clarence J, Masse, Sherie V, McDaniel, Keith F, Middleton, Tim R, Mo, Hongmei, Molla, Akhteruzzaman, Montgomery, Debra A, Ng, Teresa I, and Kempf, Dale J</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MXBF90-7/2/36dcf2120eab4f3b89763c2e2ecd3e14">http://www.sciencedirect.com/science/article/B6TF9-4MXBF90-7/2/36dcf2120eab4f3b89763c2e2ecd3e14</a> </p><br />

    <p>39.   60169   DMID-LS-138; EMBASE-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Conversion of Some 2(3H)- Furanones Bearing a Pyrazolyl Group Into Other Heterocyclic System With a Study of Their Antiviral Activity</p>

    <p>          Hashem, Ahmed I, Youssef, Ahmed SA, Kandeel, Kamal A, and Abou-Elmagd, Wael SI</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript:  102</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MTK92J-2/2/bafc3d4179ec30bf325346063d30f278">http://www.sciencedirect.com/science/article/B6VKY-4MTK92J-2/2/bafc3d4179ec30bf325346063d30f278</a> </p><br />

    <p>40.   60170   DMID-LS-138; WOS-DMID-2/12/2007</p>

    <p class="memofmt1-2">          Progress in Anti-Sars Coronavirus Chemistry, Biology and Chemotherapy</p>

    <p>          Ghosh, AK, Xi, K, Johnson, ME, Baker, SC, and Mesecar, AD</p>

    <p>          Annual Reports in Medicinal Chemistry, Elsevier Academic Press Inc, Editor <b>2007</b>.  41: 183-196</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243457700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243457700011</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
