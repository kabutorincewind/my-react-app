

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-06-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ILkT/8aHOtwVrGh2oVfRJKFQNH1l+O1HwJuo8OT57/TnD4FbhEhXKxW5BiyqcKwZdF7zMjw3qA+u0xI/XRVAXAa7VTYXTNTg6m3lMekyl6EOS23Yj13Rav5U6D8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F9523AA5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: May 24 - June 6, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23730556">Evaluation of Antifungal Activity of Free Fatty acids methyl esters Fraction Isolated from Algerian Linum usitatissimum L. Seeds against Toxigenic Aspergillus.</a> Abdelillah, A., B. Houcine, D. Halima, C.S. Meriem, Z. Imane, S.D. Eddine, M. Abdallah, C.S. Daoudi, and A.D. Eddine. Asian Pacific Journal of Tropical Biomedicine, 2013. 3(6): p. 443-448. PMID[23730556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23623256">Synthesis and Carbonic Anhydrase Inhibitory Properties of Sulfamides Structurally Related to Dopamine.</a> Aksu, K., M. Nar, M. Tanc, D. Vullo, I. Gulcin, S. Goksu, F. Tumer, and C.T. Supuran. Bioorganic &amp; Medicinal Chemistry, 2013. 21(11): p. 2925-2931. PMID[23623256].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23716054">Single Dose Pharmacodynamics of Amphotericin B against Aspergillus Species in an in Vitro Pharmacokinetic/Pharmacodynamic Model.</a> Al-Saigh, R., M. Siopi, N. Siafakas, A. Velegraki, L. Zerva, and J. Meletiadis. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23716054].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23711519">Antifungal Synergy of Theaflavin and Epicatechin Combinations against Candida albicans.</a> Betts, J.W., D.W. Wareham, S.M. Kelly, and S.J. Haswell. Journal of Microbiology and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23711519].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23722631">In Vitro Evaluation of Portuguese Propolis and Floral Sources for Antiprotozoal, Antibacterial and Antifungal Activity.</a> Falcao, S.I., N. Vale, P. Cos, P. Gomes, C. Freire, L. Maes, and M. Vilas-Boas. Phytotherapy Research, 2013. <b>[Epub ahead of print]</b>. PMID[23722631].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23576540">Comparison of MICs of Fluconazole and Flucytosine when Dissolved in Dimethyl sulfoxide or Water.</a> Fothergill, A.W., C. Sanders, and N.P. Wiederhold. Journal of Clinical Microbiology, 2013. 51(6): p. 1955-1957. PMID[23576540].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23722042">Antifungal Properties of the Anti-hypertensive Drug: Aliskiren.</a> Kathwate, G.H. and S.M. Karuppayil. Archives of Oral Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23722042].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23519707">Dimerization of Aurein 1.2: Effects in Structure, Antimicrobial Activity and Aggregation of Candida albicans Cells.</a> Lorenzon, E.N., P.R. Sanches, L.G. Nogueira, T.M. Bauab, and E.M. Cilli. Amino Acids, 2013. 44(6): p. 1521-1528. PMID[23519707].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23727185">Antifungal Activity and Cytotoxicity of Isolated Compounds from Leaves of Breonadia salicina.</a> Mahlo, S.M., L.J. McGaw, and J.N. Eloff. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23727185].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23573983">Inhibitory Effects of Tea Extract on Aflatoxin Production by Aspergillus flavus.</a> Mo, H.Z., H. Zhang, Q.H. Wu, and L.B. Hu. Letters in Applied Microbiology, 2013. 56(6): p. 462-466. PMID[23573983].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23500435">Novel Terephthaloyl Thiourea Cross-linked Chitosan Hydrogels as Antibacterial and Antifungal Agents.</a> Mohamed, N.A. and N.Y. Al-Mehbad. International Journal of Biological Macromolecules, 2013. 57: p. 111-117. PMID[23500435].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23732327">Quaternized N-substituted carboxymethyl Chitosan Derivatives as Antimicrobial Agents.</a> Mohamed, N.A., M.W. Sabaa, A.H. El-Ghandour, M.M. Abdel-Aziz, and O.F. Abdel-Gawad. International Journal of Biological Macromolecules, 2013. <b>[Epub ahead of print]</b>. PMID[23732327].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23611733">New Trifluoromethyl quinolone Derivatives: Synthesis and Investigation of Antimicrobial Properties.</a> Panda, S.S. and S.C. Jain. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(11): p. 3225-3229. PMID[23611733].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23711469">cgMolluscidin, a Novel Dibasic Residue Repeat Rich Antimicrobial Peptide, Purified from the Gill of the Pacific Oyster, Crassostrea gigas.</a> Seo, J.K., M.J. Lee, B.H. Nam, and N. Park. Fish &amp; Shellfish Immunology, 2013. <b>[Epub ahead of print]</b>. PMID[23711469].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23571544">Pharmacokinetics and Pharmacodynamics of Fluconazole for Cryptococcal Meningoencephalitis: Implications for Antifungal Therapy and in Vitro Susceptibility Breakpoints.</a> Sudan, A., J. Livermore, S.J. Howard, Z. Al-Nakeeb, A. Sharp, J. Goodwin, L. Gregson, P.A. Warn, T.W. Felton, J.R. Perfect, T.S. Harrison, and W.W. Hope. Antimicrobial Agents and Chemotherapy, 2013. 57(6): p. 2793-2800. PMID[23571544].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23563951">Imidazoacridinone Derivatives as Efficient Sensitizers in Photoantimicrobial Chemotherapy.</a> Taraszkiewicz, A., M. Grinholc, K.P. Bielawski, A. Kawiak, and J. Nakonieczna. Applied and Environmental Microbiology, 2013. 79(12): p. 3692-3702. PMID[23563951].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23415652">Medusins: A New Class of Antimicrobial Peptides from the Skin Secretions of Phyllomedusine Frogs.</a> Xi, X., R. Li, Y. Jiang, Y. Lin, Y. Wu, M. Zhou, J. Xu, L. Wang, T. Chen, and C. Shaw. Biochimie, 2013. 95(6): p. 1288-1296. PMID[23415652].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>  

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23733467">Efficacy and Safety of Metronidazole for Pulmonary Multidrug-resistant Tuberculosis.</a> Carroll, M.W., D. Jeon, J.M. Mountz, J.D. Lee, Y.J. Jeong, N. Zia, M. Lee, J. Lee, L.E. Via, S. Lee, S.Y. Eum, S.J. Lee, L.C. Goldfeder, Y. Cai, B. Jin, Y. Kim, T. Oh, R.Y. Chen, L.E. Dodd, W. Gu, V. Dartois, S.K. Park, C.T. Kim, C.E. Barry, 3rd, and S.N. Cho. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23733467].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23447141">Mycobacterium canettii Is Intrinsically Resistant to Both Pyrazinamide and Pyrazinoic acid.</a> Feuerriegel, S., C.U. Koser, E. Richter, and S. Niemann. The Journal of Antimicrobial Chemotherapy, 2013. 68(6): p. 1439-1440. PMID[23447141].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23333815">Anti-mycobacterial Activity of Plumericin and Isoplumericin against MDR Mycobacterium tuberculosis.</a> Kumar, P., A. Singh, U. Sharma, D. Singh, M.P. Dobhal, and S. Singh. Pulmonary Pharmacology &amp; Therapeutics, 2013. 26(3): p. 332-335. PMID[23333815].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23611124">Preliminary Structure-Activity Relationships and Biological Evaluation of Novel Antitubercular Indolecarboxamide Derivatives against Drug-susceptible and Drug-resistant Mycobacterium tuberculosis Strains.</a> Onajole, O.K., M. Pieroni, S.K. Tipparaju, S. Lun, J. Stec, G. Chen, H. Gunosewoyo, H. Guo, N.C. Ammerman, W.R. Bishai, and A.P. Kozikowski. Journal of Medicinal Chemistry, 2013. 56(10): p. 4093-4103. PMID[23611124].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23724039">In Vitro and in Vivo Activities of Ruthenium(II) Phosphine/Diimine/Picolinate Complexes (SCAR) against Mycobacterium tuberculosis.</a> Pavan, F.R., G.V. Poelhsitz, L.V. da Cunha, M.I. Barbosa, S.R. Leite, A.A. Batista, S.H. Cho, S.G. Franzblau, M.S. de Camargo, F.A. Resende, E.A. Varanda, and C.Q. Leite. PloS One, 2013. 8(5): p. e64242. PMID[23724039].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23708673">Study on Antitubercular Constituents from the Seeds of Nigella glandulifera.</a> Sun, L.L., M. Luan, W. Zhu, S. Gao, Q.L. Zhang, C.J. Xu, X.H. Lu, X.D. Xu, J.K. Tian, and L. Zhang. Chemical &amp; Pharmaceutical Bulletin, 2013. <b>[Epub ahead of print]</b>. PMID[23708673].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23727443">Recent Developments in the Antiprotozoal and Anticancer Activities of the 2-Alkynoic fatty acids.</a> Carballeira, N.M. Chemistry and Physics of Lipids, 2013. <b>[Epub ahead of print]</b>. PMID[23727443].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23716053">Interrogating a Hexokinase-selected Small Molecule Library for Inhibitors of Plasmodium falciparum Hexokinase.</a> Harris, M.T., D.M. Walker, M.E. Drew, W.G. Mitchell, K. Dao, C.E. Schroeder, D.P. Flaherty, W.S. Weiner, J.E. Golden, and J.C. Morris. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23716053].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23713488">Synthesis and Structure-Activity Relationships of Phosphonic Arginine Mimetics as Inhibitors of the M1 and M17 Aminopeptidases from Plasmodium falciparum.</a> Kannan Sivaraman, K., A. Paiardini, M. Sienczyk, C. Ruggeri, C.A. Oellig, J.P. Dalton, P.J. Scammells, M. Drag, and S. McGowan. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23713488].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23545535">Anthracene-polyamine Conjugates Inhibit in Vitro Proliferation of Intraerythrocytic Plasmodium falciparum Parasites.</a> Niemand, J., P. Burger, B.K. Verlinden, J. Reader, A.M. Joubert, A. Kaiser, A.I. Louw, K. Kirk, O.t. Phanstiel, and L.M. Birkholtz. Antimicrobial Agents and Chemotherapy, 2013. 57(6): p. 2874-2877. PMID[23545535].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23701465">Quinolin-4(1H)-imines Are Potent Antiplasmodial Drugs Targeting the Liver Stage of Malaria.</a> Rodrigues, T., F.P. da Cruz, M.J. Lafuente-Monasterio, D. Goncalves, A.S. Ressurreicao, A.R. Sitoe, M.R. Bronze, J. Gut, G. Schneider, M.M. Mota, P.J. Rosenthal, M. Prudencio, F.J. Gamo, F. Lopes, and R. Moreira. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23701465].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23602620">Synthesis and Evaluation of 7-Chloro-4-(piperazin-1-yl)quinoline-sulfonamide as Hybrid Antiprotozoal Agents.</a> Salahuddin, A., A. Inam, R.L. van Zyl, D.C. Heslop, C.T. Chen, F. Avecilla, S.M. Agarwal, and A. Azam. Bioorganic &amp; Medicinal Chemistry, 2013. 21(11): p. 3080-3089. PMID[23602620].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23398677">Design, Synthesis, Structural Characterization by IR, (1) H, (13) C, (15) N, 2D-NMR, X-ray Diffraction and Evaluation of a New Class of Phenylaminoacetic acid benzylidene hydrazines as pfENR Inhibitors.</a> Samal, R.P., V.M. Khedkar, R.R. Pissurlenkar, A.G. Bwalya, D. Tasdemir, R.A. Joshi, P.R. Rajamohanan, V.G. Puranik, and E.C. Coutinho. Chemical Biology &amp; Drug Design, 2013. 81(6): p. 715-729. PMID[23398677].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23542146">Antiplasmodial Activity of Sesquiterpene Lactones and a Sucrose Ester from Vernonia guineensis Benth. (Asteraceae).</a> Toyang, N.J., M.A. Krause, R.M. Fairhurst, P. Tane, J. Bryant, and R. Verpoorte. Journal of Ethnopharmacology, 2013. 147(3): p. 618-621. PMID[23542146].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23707448">Evaluation of Protective Effect of pVAX-TgMIC13 Plasmid against Acute and Chronic Toxoplasma gondii Infection in a Murine Model.</a> Yuan, Z.G., D. Ren, D.H. Zhou, X.X. Zhang, E. Petersen, X.Z. Li, Y. Zhou, G.L. Yang, and X.Q. Zhu. Vaccine, 2013. <b>[Epub ahead of print]</b>. PMID[23707448].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0524-060613.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317819800011">Synthesis and Antimicrobial Investigation of Some 5H-Pyridazino 4,5-B indoles.</a> Avan, I., A. Guven, and K. Guven. Turkish Journal of Chemistry, 2013. 37(2): p. 271-291. ISI[000317819800011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317819800005">Antimicrobial Activity and Chemical Composition of the Essential Oils of Mosses (Hylocomium splendens (Hedw.) Schimp. And Leucodon sciuroides (Hedw.) Schwagr.) Growing in Turkey.</a> Cansu, T.B., B. Yayli, T. Ozdemir, N. Batan, S. Alpay Karaoglu, and N. Yayli. Turkish Journal of Chemistry, 2013. 37(2): p. 213-219. ISI[000317819800005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318348800004">Evaluation of Antimicrobial Activity and Cell Viability of Aloe vera Sponges.</a> de Lacerda Gontijo, S.M., A.D. Martins Gomes, A. Gala-Garcia, R.D. Sinisterra, and M.E. Cortes. Electronic Journal of Biotechnology, 2013. 16(1): pp. 10. ISI[000318348800004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317693300018">Essential Oils and Herbal Extracts as Antimicrobial Agents in Cosmetic Emulsion.</a> Herman, A., A.P. Herman, B.W. Domagalska, and A. Mlynarczyk. Indian Journal of Microbiology, 2013. 53(2): p. 232-237. ISI[000317693300018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317919400015">Synthesis and Antifungal Activity of Some Novel (E)-2, 3-Dihydro-3- (phenylamino) methylene -4H-1-benzothiopyran-4-ones.</a> Liu Xiao-Ming, Yang Geng-Liang, Song Ya-Li, Liu Jie-Jie, Wang Yang, and Zhang Dong-Nuan. Letters in Organic Chemistry, 2013. 10(3): p. 228-234. ISI[000317919400015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317893400127">Lipoxin A(4) and 15-Epi-lipoxin A(4) Protect against Experimental Cerebral Malaria by Inhibiting IL-12/IFN-gamma in the Brain.</a> Shryock, N., C. McBerry, R.M.S. Gonzalez, S. Janes, F.T.M. Costa, and J. Aliberti. PloS One, 2013. 8(4): e61882. ISI[000317893400127].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317741500006">Synthesis and Antimicrobial Activity of Amines and Imines with a Cycloheptatriene Fragment.</a> Yunnikova, L.P., T.A. Akent&#39;eva, and G.A. Aleksandrova. Pharmaceutical Chemistry Journal, 2013. 46(12): p. 723-725. ISI[000317741500006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317888400012">Some 2S Albumin from Peanut Seeds Exhibits Inhibitory Activity against Aspergillus flavus.</a>Duan, X.H., R. Jiang, Y.J. Wen, and J.H. Bin. Plant Physiology and Biochemistry, 2013. 66: p. 84-90. ISI[000317888400012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316113300008">Aspergillus niger P6 and Rhodotorula mucilaginosa CH4 Used for Olive Mill Wastewater (OMW) Biological Treatment in Single Pure and Successive Cultures.</a> Jarboui, R., S. Magdich, R.J. Ayadi, A. Gargouri, N. Gharsallah, and E. Ammar. Environmental Technology, 2013. 34(5): p. 629-636. ISI[000316113300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317806400006">Culture Condition-dependent Metabolite Profiling of Aspergillus fumigatus with Antifungal Activity.</a>Kang, D., G.H. Son, H.M. Park, J. Kim, J.N. Choi, H.Y. Kim, S. Lee, S.-B. Hong, and C.H. Lee. Fungal Biology, 2013. 117(3): p. 211-219. ISI[000317806400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318020100053">Hydrophobic Effect of Amphiphilic Derivatives of Chitosan on the Antifungal Activity against Aspergillus flavus and Aspergillus parasiticus.</a> Felix Viegas de Souza, R.H., M. Takaki, R.d.O. Pedro, J.d.S. Gabriel, M.J. Tiera, and V.A. de Oliveira Tiera. Molecules, 2013. 18(4): p. 4437-4450. ISI[000318020100053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317983200014">Synthesis, Characterization and Antimicrobial Screening of Hybrid Molecules Containing Quinoline, Pyrimidine and Morpholine Analogues.</a> Desai, N.C., K.M. Rajpara, V.V. Joshi, H.V. Vaghani, and H.M. Satodiya. Journal of Chemical Sciences, 2013. 125(2): p. 321-333. ISI[000317983200014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317927600039">A Molecular Analysis of the Toxicity of Alkyltributylphosphonium chlorides in Aspergillus nidulans.</a> Hartmann, D.O. and C.S. Pereira. New Journal of Chemistry, 2013. 37(5): p. 1569-1577. ISI[000317927600039].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317988100025">Synthesis Antimicrobial and Anticancer Activity of N &#39;-arylmethylidene-piperazine-1-carbothiohydrazide.</a> Kulandaivelu, U., B. Shireesha, C. Mahesh, J.V. Vidyasagar, T.R. Rao, K.N. Jayaveera, P. Saiko, G. Graser, T. Szekeres, and V. Jayaprakash. Medicinal Chemistry Research, 2013. 22(6): p. 2802-2808. ISI[000317988100025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317988100011">Synthesis of New 3-Aryl-4-(3-aryl-4,5-dihydro-1H-pyrazol-5-yl)-1-phenyl-1H-pyrazole Derivatives as Potential Antimicrobial Agents.</a> Malladi, S., A.M. Isloor, S.K. Peethambar, and H.K. Fun. Medicinal Chemistry Research, 2013. 22(6): p. 2654-2664. ISI[000317988100011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317614000021">Evaluation of Clausena pentaphylla (Roxb.) DC Oil as a Fungitoxicant against Storage Mycoflora of Pigeon Pea Seeds.</a> Pandey, A.K., U.T. Palni, and N.N. Tripathi. Journal of the Science of Food and Agriculture, 2013. 93(7): p. 1680-1686. ISI[000317614000021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317988100012">Synthesis of Some New Glutamine Linked 2,3-Disubstituted Quinazolinone Derivatives as Potent Antimicrobial and Antioxidant Agents.</a> Prashanth, R. Mk, and Hd. Medicinal Chemistry Research, 2013. 22(6): p. 2665-2676. ISI[000317988100012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">50. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318059000005">Bacterial Expression and Antibiotic Activities of Recombinant Variants of Human beta-Defensins on Pathogenic Bacteria and M. tuberculosis.</a> Corrales-Garcia, L., E. Ortiz, J. Castaneda-Delgado, B. Rivas-Santiago, and G. Corzo. Protein Expression and Purification, 2013. 89(1): p. 33-43. ISI[000318059000005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">51. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318055800048">In Vitro Antimycobacterial Activity of Extracts from Plants Used in Traditional Medicine to Treat Tb and Related Symptoms.</a> Madikizela, B., J.F. Finnie, and J. Van Staden. South African Journal of Botany, 2013. 86: p. 146-146. ISI[000318055800048].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">52. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318055800019">A Comparison of the Pharmacological Properties of Garden Cultivated and Muthi Market-sold Bowiea volubilis.</a> Masondo, N.A., A.R. Ndhlala, A.O. Aremu, J. Van Staden, and J.F. Finnie. South African Journal of Botany, 2013. 86: p. 135-138. ISI[000318055800019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">53. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317144400017">Antimicrobial Activity of Honey from Five Species of Brazilian Stingless Bees.</a> Merces, M.D., E.D. Peralta, A.P. Trovatti Uetanabaro, and A.M. Lucchese. Ciencia Rural, 2013. 43(4): p. 672-675. ISI[000317144400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">54. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317988100018">Antimicrobial Activity of Thiazolyl benzenesulfonamide-condensed 2,4-Thiazolidinediones Derivatives.</a> Parekh, N.M., K.V. Juddhawala, and B.M. Rawal. Medicinal Chemistry Research, 2013. 22(6): p. 2737-2745. ISI[000317988100018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">55. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317988100015">Screening of Flavonoids for Antitubercular Activity and Their Structure-activity Relationships.</a> Yadav, A.K., J. Thakur, O. Prakash, F. Khan, D. Saikia, and M.M. Gupta. Medicinal Chemistry Research, 2013. 22(6): p. 2706-2716. ISI[000317988100015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>

    <br />

    <p class="plaintext">56. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317988100044">Design, Synthesis and in Vitro Antimicrobial Activity of Novel Phenylbenzamido-aminothiazole-based Azasterol Mimics.</a> Yadlapalli, R.K., O.P. Chourasia, M.P. Jogi, A.R. Podile, and R.S. Perali. Medicinal Chemistry Research, 2013. 22(6): p. 2975-2983. ISI[000317988100044].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0524-060613.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
