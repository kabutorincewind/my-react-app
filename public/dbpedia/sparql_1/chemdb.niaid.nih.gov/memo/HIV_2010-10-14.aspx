

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-10-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Ubbm7z2hpYBSjOE7wIqjM4Pb0HbMA9KPoWhlJPSDrmokd8z5/CvBa1M4Kx8XTiwhAhnNQU23QBsMVvx10nkPjSr5whBUsDSdHU5rabbrQPR1l5f8+EBvsncSZ60=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9909EBB9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">HIV Citation List:  October 1- October 14, 2010</p>

    <p class="NoSpacing memofmt2-2">Pubmed citations:</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20940031">Three new water-soluble alkaloids from the leaves of suregada glomerulata (Blume) Baill.</a> Yan, R.Y., H.Q. Wang, C. Liu, R.Y. Chen, and D.Q. Yu, Fitoterapia, 2010; PMID[20940031].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20938401">In Vitro Antioxidant Properties, HIV-1 Reverse Transcriptase and Acetylcholinesterase Inhibitory Effects of Traditional Herbal Preparations Sold in South Africa.</a> Ndhlala, A.R., J.F. Finnie, and J. Van Staden, Molecules, 2010. 15(10): p. 6888-6904; PMID[20938401].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937817">A gyrase B inhibitor impairs HIV-1 replication by targeting HSP90 and the capsid protein.</a> Vozzolo, L., B. Loh, P.J. Gane, M. Tribak, L. Zhou, I. Anderson, E. Nyakatura, R.G. Jenner, D. Selwood, and A. Fassati, Journal of Biological Chemistry, 2010; PMID[20937817].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937812">Resistance profiles of novel electrostatically constrained HIV-1 fusion inhibitors.</a> Shimura, K., D. Nameki, K. Kajiwara, K. Watanabe, Y. Sakagami, S. Oishi, N. Fujii, M. Matsuoka, S.G. Sarafianos, and E. Kodama. Journal of Biological Chemistry, 2010; PMID[20937812].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937786">Novel compounds containing multiple guanide groups which bind the HIV co-receptor CXCR4.</a> Wilkinson, R.A., S.H. Pincus, J.B. Shepard, S.K. Walton, E.P. Bergin, M. Labib, and M. Teintze. Antimicrobial Agents and Chemotherapy, 2010; PMID[20937786].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2093733">Complete Inactivation of HIV-1 Using Photo-Labeled Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Rios, A., J. Quesada, D. Anderson, A. Goldstein, T. Fossum, S. Colby-Germinario, and M.A. Wainberg, Virus Research, 2010; PMID[20937333].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20936621">Toward the First Nonpeptidic Molecular Tong Inhibitor of Wild-Type and Mutated HIV-1 Protease Dimerization.</a> Vidu, A., L. Dufau, L. Bannwarth, J.L. Soulier, S. Sicsic, U. Piarulli, M. Reboud-Ravaux, and S. Ongeri. ChemMedChem, 2010; PMID[20936621].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20924954">Design and synthesis of dually branched 5&#39;-norcarbocyclic adenosine phosphonodiester analogue as a new anti-HIV prodrug.</a> Oh, C.H., L.J. Liu, and J.H. Hong. Nucleosides Nucleotides &amp; Nucleic Acids, 2010. 29(10): p. 721-33; PMID[20924954].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="NoSpacing memofmt2-2"> </p>

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282041800006">Quinoline-based compounds as modulators of HIV transcription through NF-kappa B and Sp1 inhibition.</a> Bedoya, L.M., M.J. Abad, E. Calonge, L.A. Saavedra, M. Gutierrez, V.V. Kouznetsov, J. Alcami, and P. Bermejo. Antiviral Research, 2010 87(3): p. 338-344.</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282038800029">Isolation, Sequencing, and Structure-Activity Relationships of Cyclotides.</a> Ireland, D.C., R.J. Clark, N.L. Daly, and D.J. Craik. Journal of Natural Products, 2010. 73(9): p. 1610-1622.</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280165300253">Metabolism and pharmacokinetics of GS-8374: A novel phosphonate-containing HIV protease inhibitor with high barrier to resistance.</a> Mollova, N., J. Tang, C. Bramwell-German, E.I. Lepist, N. Chu, W. Chen, B. Wong, P. Staehr, and K.H. Leung.  Drug Metabolism Reviews, 2009. 41(S3): p. 131-132.</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282100500002">A review on the isolation and structure of tea polysaccharides and their bioactivities.</a> Nie, S.P. and M.Y. Xie. Food Hydrocolloids, 2010. 25(2): p. 144-149.</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281750900008">Synthesis and microbial studies of (4-oxo-thiazolidinyl) sulfonamides bearing quinazolin-4(3h)ones.</a> Patel, N.B., V.N. Patel, H.R. Patel, F.M. Shaikh, and J.C. Patel. Acta Poloniae Pharmaceutica, 2010. 67(3): p. 267-275.</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1001-101410.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281963700012">Synthesis and antiviral evaluation of alpha-D-2 &#39;,3 &#39;-didehydro-2 &#39;,3 &#39;-dideoxy-3 &#39;-C-hydroxymethyl nucleosides.</a> Yamada, K., H. Hayakawa, S. Sakata, N. Ashida, and Y. Yoshimura. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(20): p. 6013-6016.</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1001-101410.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
