

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-358.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="SfmuZDAe+yZuT3aolo8UJOK5+W7QrZM20FfLYycx4UsALGwYFNvMCBdIgnwyTpVe88AZj6cXepPAPlSvQpj492ObRO3AQQDOsKe5azBzG+k0UenFtFiXtYDEzKE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="625B99C2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-358-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59055   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Potent Antiviral Synergy between Monoclonal Antibody and Small-Molecule CCR5 Inhibitors of Human Immunodeficiency Virus Type 1</p>

    <p>          Murga, JD, Franti, M, Pevear, DC, Maddon, PJ, and Olson, WC</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(10): 3289-96</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005807&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005807&amp;dopt=abstract</a> </p><br />

    <p>2.     59056   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          A platform for designing HIV integrase inhibitors. Part 2: A two-metal binding model as a potential mechanism of HIV integrase inhibitors</p>

    <p>          Kawasuji, T, Fuji, M, Yoshinaga, T, Sato, A, Fujiwara, T, and Kiyama, R</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005407&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005407&amp;dopt=abstract</a> </p><br />

    <p>3.     59057   HIV-LS-358; EMBASE-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Discovery of novel, highly potent and selective [beta]-hairpin mimetic CXCR4 inhibitors with excellent anti-HIV activity and pharmacokinetic profiles</p>

    <p>          DeMarco, Steven J, Henze, Heiko, Lederer, Alexander, Moehle, Kerstin, Mukherjee, Reshmi, Romagnoli, Barbara, Robinson, John A, Brianza, Federico, Gombert, Frank O, and Lociuro, Sergio</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-2/2/8bfdbbcf0e7f402e2f8381a6f6771323">http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-2/2/8bfdbbcf0e7f402e2f8381a6f6771323</a> </p><br />

    <p>4.     59058   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV properties of new hydroxyquinoline-polyamine conjugates on cells infected by HIV-1 LAV and HIV-1 BaL viral strains</p>

    <p>          Moret, V, Dereudre-Bosquet, N, Clayette, P, Laras, Y, Pietrancosta, N, Rolland, A, Weck, C, Marc, S, and Kraus, JL</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17000109&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17000109&amp;dopt=abstract</a> </p><br />

    <p>5.     59059   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Synthesis of neplanocin F analogues as potential antiviral agents</p>

    <p>          Zhang, H, Schinazi, RF, and Chu, CK</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16996741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16996741&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59060   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Tripfordines A-C, Sesquiterpene Pyridine Alkaloids from Tripterygium wilfordii, and Structure Anti-HIV Activity Relationships of Tripterygium Alkaloids</p>

    <p>          Horiuch, M, Murakami, C, Fukamiya, N, Yu, D, Chen, TH, Bastow, KF, Zhang, DC, Takaishi, Y, Imakura, Y, and Lee, KH</p>

    <p>          J Nat Prod <b>2006</b>.  69(9): 1271-1274</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16989518&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16989518&amp;dopt=abstract</a> </p><br />

    <p>7.     59061   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          An anti-HIV microbicide engineered in commensal bacteria: secretion of HIV-1 fusion inhibitors by lactobacilli</p>

    <p>          Pusch, O, Kalyanaraman, R, Tucker, LD, Wells, JM, Ramratnam, B, and Boden, D</p>

    <p>          AIDS <b>2006</b>.  20 (15): 1917-1922</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16988512&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16988512&amp;dopt=abstract</a> </p><br />

    <p>8.     59062   HIV-LS-358; EMBASE-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Increasing prevalence of HIV-1 protease inhibitor-associated mutations correlates with long-term non-suppressive protease inhibitor treatment</p>

    <p>          Kagan, RM, Cheung, PK, Huard, TK, and Lewinski, MA</p>

    <p>          Antiviral Research <b>2006</b>.  71(1): 42-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JJ85K4-1/2/36900955318aad7026fe4bce706b4384">http://www.sciencedirect.com/science/article/B6T2H-4JJ85K4-1/2/36900955318aad7026fe4bce706b4384</a> </p><br />

    <p>9.     59063   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          In Vitro HIV-1 Resistance Selections with Combinations of Tenofovir and Emtricitabine or Abacavir and Lamivudine</p>

    <p>          Margot, NA, Waters, JM, and Miller, MD</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16982781&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16982781&amp;dopt=abstract</a> </p><br />

    <p>10.   59064   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          HIV-1 protease mutations and inhibitor modifications monitored on a series of complexes. Structural basis for the effect of the A71V mutation on the active site</p>

    <p>          Skalova, T, Dohnalek, J, Duskova, J, Petrokova, H, Hradilek, M, Soucek, M, Konvalinka, J, and Hasek, J</p>

    <p>          J Med Chem <b>2006</b>.  49(19): 5777-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16970402&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16970402&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59065   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Investigation of baseline susceptibility to protease inhibitors in HIV-1 subtypes C, F, G and CRF02_AG</p>

    <p>          Abecasis, AB, Deforche, K, Bacheler, LT, McKenna, P, Carvalho, AP, Gomes, P, Vandamme, AM, and Camacho, RJ</p>

    <p>          Antivir Ther <b>2006</b>.  11(5): 581-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16964826&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16964826&amp;dopt=abstract</a> </p><br />

    <p>12.   59066   HIV-LS-358; EMBASE-HIV-10/2/2006</p>

    <p class="memofmt1-2">          The design, synthesis and biological evaluation of novel substituted purines as HIV-1 Tat-TAR inhibitors</p>

    <p>          Yuan, Dekai, He, Meizi, Pang, Ruifang, Lin, Shrong-shi, Li, Zhengming, and Yang, Ming</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Accepted Manuscript</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M10PJP-4/2/c9b983da7408a82cae5a5775b1cd9691">http://www.sciencedirect.com/science/article/B6TF8-4M10PJP-4/2/c9b983da7408a82cae5a5775b1cd9691</a> </p><br />

    <p>13.   59067   HIV-LS-358; EMBASE-HIV-10/2/2006</p>

    <p class="memofmt1-2">          A platform for designing HIV integrase inhibitors. Part 1: 2-Hydroxy-3-heteroaryl acrylic acid derivatives as novel HIV integrase inhibitor and modeling of hydrophilic and hydrophobic pharmacophores</p>

    <p>          Kawasuji, Takashi, Yoshinaga, Tomokazu, Sato, Akihiko, Yodo, Mitsuaki, Fujiwara, Tamio, and Kiyama, Ryuichi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-1/2/57003eae34c240644c703c85b166721f">http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-1/2/57003eae34c240644c703c85b166721f</a> </p><br />

    <p>14.   59068   HIV-LS-358; EMBASE-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Three-dimensional quantitative structure-activity relationship studies on diverse structural classes of HIV-1 integrase inhibitors using CoMFA and CoMSIA</p>

    <p>          Nunthaboot, Nadtanet, Tonmunphean, Somsak, Parasuk, Vudhichai, Wolschann, Peter, and Kokpol, Sirirat</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4M04DSV-1/2/444840f27a3bd905f9b341c5c4931141">http://www.sciencedirect.com/science/article/B6VKY-4M04DSV-1/2/444840f27a3bd905f9b341c5c4931141</a> </p><br />

    <p>15.   59069   HIV-LS-358; PUBMED-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Small-molecule HIV-1 gp120 inhibitors to prevent HIV-1 entry: an emerging opportunity for drug development</p>

    <p>          Kadow, J, Wang, HG, and Lin, PF</p>

    <p>          Curr Opin Investig Drugs <b>2006</b>.  7(8): 721-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955683&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955683&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   59070   HIV-LS-358; EMBASE-HIV-10/2/2006</p>

    <p class="memofmt1-2">          Synergistic Inhibition of HIV-1 Envelope-Mediated Membrane Fusion by Inhibitors Targeting the N and C-Terminal Heptad Repeats of gp41</p>

    <p>          Gustchina, Elena, Louis, John M, Bewley, Carole A, and Clore, GMarius</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4KW5FHH-5/2/e52af45883fadd0219d4ac8873fb2630">http://www.sciencedirect.com/science/article/B6WK7-4KW5FHH-5/2/e52af45883fadd0219d4ac8873fb2630</a> </p><br />

    <p>17.   59071   HIV-LS-358; WOS-HIV-9/24/2006</p>

    <p class="memofmt1-2">          Anti-HIV and toxicity of isolated compounds from Elaeodendron transvaalense</p>

    <p>          Tshikalange, TE, Meyer, JJM, and Ivars, F</p>

    <p>          SOUTH AFRICAN JOURNAL OF BOTANY <b>2006</b>.  72(2): 334-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240298800119">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240298800119</a> </p><br />

    <p>18.   59072   HIV-LS-358; WOS-HIV-9/24/2006</p>

    <p class="memofmt1-2">          Stereospecific synthesis of the (2R,3S)- and (2R,3R)-3-amino-2-hydroxy-4-phenylbutanoic acids from D-glucono-delta-lactone</p>

    <p>          Lee, JH, Kim, JH, Lee, BW, Seo, WD, Yang, MS, and Park, KH</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY <b>2006</b>.  27(8): 1211-1218, 8</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240394100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240394100023</a> </p><br />

    <p>19.   59073   HIV-LS-358; WOS-HIV-10/1/2006</p>

    <p class="memofmt1-2">          Novel tight binding PETT, HEPT and DABO-based non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          D&#39;Cruz, OJ and Uckun, FM</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2006</b>.  21(4): 329-350, 22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240322000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240322000001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
