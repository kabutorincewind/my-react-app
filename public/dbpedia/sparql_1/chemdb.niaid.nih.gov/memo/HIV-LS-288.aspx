

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-288.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yyhS4qMhIBbqubxtJ4pEzGsLxK2xrOimS4MesPeGKdQM3qS5JPb7eC2xGr6Oay6NHqJ/tL6mSQ4YVKr1eY+w+hMmTHcPaMhVELtHR+dC6tgrxWXKQMMqFRUJyPc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A258A0C3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-288-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    42682   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Mutations linked to drug resistance, human immunodeficiency virus type 1 biologic phenotype and their association with disease progression in children receiving nucleoside reverse transcriptase inhibitors</p>

<p>           Englund, JA, Raskino, C, Vavro, C, Palumbo, P, Ross, LL, McKinney, R, Nikolic-Djokic, D, Colgrove, RC, and Baker, CJ</p>

<p>           Pediatr Infect Dis J 2004. 23(1): 15-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14743040&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14743040&amp;dopt=abstract</a> </p><br />

<p>     2.    42683   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Quantitative detection of human immunodeficiency virus type 1 (HIV-1) viral load by SYBR green real-time RT-PCR technique in HIV-1 seropositive patients</p>

<p>           Gibellini, Davide, Vitone, Francesca, Gori, Elisa, Placa, Michele La, and Re, Maria Carla</p>

<p>           Journal of Virological Methods 2004.  115(2): 183-189</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T96-4B52V6P-1/2/b8968ce7f5586ce0141971eb2b98069e">http://www.sciencedirect.com/science/article/B6T96-4B52V6P-1/2/b8968ce7f5586ce0141971eb2b98069e</a> </p><br />

<p>     3.    42684   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Transmission of HIV-1 drug resistance</p>

<p>           Tang, Julian W and Pillay, Deenan</p>

<p>           Journal of Clinical Virology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4BJ2FS0-2/2/a4535aaa3174ef354b128575ec9659be">http://www.sciencedirect.com/science/article/B6VJV-4BJ2FS0-2/2/a4535aaa3174ef354b128575ec9659be</a> </p><br />

<p>     4.    42685   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>The Search for the Cd8(+) Cell Anti-Hiv Factor (Caf)</b></p>

<p>           Levy, JA</p>

<p>           Trends in Immunology 2003. 24(12): 628-632</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     5.    42686   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Investigating the effects of stereochemistry on incorporation and removal of 5-fluorocytidine analogs by mitochondrial DNA polymerase gamma: comparison of - and -D4FC-TP</p>

<p>           Murakami, Eisuke, Ray, Adrian S, Schinazi, Raymond F, and Anderson, Karen S</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4BH6H5B-1/2/f5c8f70d4b65c90f5a31dd7953d33e6b">http://www.sciencedirect.com/science/article/B6T2H-4BH6H5B-1/2/f5c8f70d4b65c90f5a31dd7953d33e6b</a> </p><br />

<p>     6.    42687   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Bifunctional anti-human immunodeficiency virus type 1 small molecules with two novel mechanisms of action</p>

<p>           Huang, L, Yuan, X, Aiken, C, and Chen, CH</p>

<p>           Antimicrob Agents Chemother 2004.  48(2): 663-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14742233&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14742233&amp;dopt=abstract</a> </p><br />

<p>    7.     42688   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Novel arylsulfonamides possessing sub-picomolar HIV protease activities and potent anti-HIV activity against wild-type and drug-resistant viral strains</p>

<p>           Miller, John F, Furfine, Eric S, Hanlon, Mary H, Hazen, Richard J, Ray, John A, Robinson, Laurence, Samano, Vicente, and Spaltenstein, Andrew</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BH63MS-7/2/b42a0f4892f981972c4f8c5eac036b73">http://www.sciencedirect.com/science/article/B6TF9-4BH63MS-7/2/b42a0f4892f981972c4f8c5eac036b73</a> </p><br />

<p>     8.    42689   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>The Causes and Consequences of Hiv Evolution</b></p>

<p>           Rambaut, A, Posada, D, Crandall, KA, and Holmes, EC</p>

<p>           Nature Reviews Genetics 2004. 5(1): 52-61</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     9.    42690   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Synthesis and anti-HIV-1 activity of 4-[4-(4,6-bisphenylamino-[1, 3 and 5]triazin-2-ylamino)-5-methoxy-2-methylphenylazo]-5-hydroxynaphthalene-2,7-disulfonic acid and its derivatives</p>

<p>           Naicker, Kannan P, Jiang, Shibo, Lu, Hong, Ni, Jiahong, Boyer-Chatenet, Louise, Wang, Lai-Xi, and Debnath, Asim K</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BF0CC2-2/2/05f8201f9061394d97347e8b811489e8">http://www.sciencedirect.com/science/article/B6TF8-4BF0CC2-2/2/05f8201f9061394d97347e8b811489e8</a> </p><br />

<p>   10.    42691   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Anti-viral cyclam macrocycles : rapid zinc uptake at physiological pH</p>

<p>           Paisey, SJ and Sadler, PJ</p>

<p>           Chem Commun (Camb) 2004(3): 306-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14740050&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14740050&amp;dopt=abstract</a> </p><br />

<p>   11.    42692   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>A Single Injection of Recombinant Measles Virus Vaccines Expressing Human Immunodeficiency Virus (Hiv) Type 1 Clade B Envelope Glycoproteins Induces Neutralizing Antibodies and Cellular Immune Responses to Hiv</b></p>

<p>           Lorin, C, Mollet, L, Delebecque, F, Combredet, C, Hurtrel, B, Charneau, P, Brahic, M, and Tangy, F</p>

<p>           Journal of Virology 2004. 78(1): 146-157</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   12.    42693   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Expression of small hairpin RNA by lentivirus-based vector confers efficient and stable gene-suppression of HIV-1 on human cells including primary non-dividing cells</p>

<p>           Nishitsuji, H, Ikeda, T, Miyoshi, H, Ohashi, T, Kannagi, M, and Masuda, T</p>

<p>           Microbes Infect 2004. 6(1): 76-85</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14738896&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14738896&amp;dopt=abstract</a> </p><br />

<p>  13.     42694   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Anti-AIDS Agents. 52. Synthesis and Anti-HIV Activity of Hydroxymethyl (3&#39;R,4&#39;R)-3&#39;,4&#39;-Di-O-(S)-camphanoyl-(+)-cis-khellactone Derivatives</p>

<p>           Xie, L, Yu, D, Wild, C, Allaway, G, Turpin, J, Smith, PC, and Lee, KH</p>

<p>           J Med Chem 2004. 47(3): 756-760</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14736256&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14736256&amp;dopt=abstract</a> </p><br />

<p>   14.    42695   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>Mechanistic Studies of the Separation of an Hiv Protease Inhibitor From Its Piperazine Diastereomer by Reversed Phase High Performance Liquid Chromatography</b></p>

<p>           Wu, N, Gauthier, DR, Dovletoglou, A, and Yehl, PM</p>

<p>           Journal of Liquid Chromatography &amp; Related Technologies 2003. 26(20): 3343-3355</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   15.    42696   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Synthesis and Antiviral Activity of (Z)- and (E)-2,2-[Bis(hydroxymethyl)cyclopropylidene]methylpurines and -pyrimidines: Second-Generation Methylenecyclopropane Analogues of Nucleosides(1)</p>

<p>           Zhou, S, Breitenbach, JM, Borysko, KZ, Drach, JC, Kern, ER, Gullen, E, Cheng, YC, and Zemlicka, J</p>

<p>           J Med Chem 2004. 47(3): 566-575</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14736238&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14736238&amp;dopt=abstract</a> </p><br />

<p>   16.    42697   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Attenuation of HIV-1 replication in primary human cells with a designed zinc finger transcription factor</p>

<p>           Segal, DJ, Gonclaves, J, Eberhardy, S, Swan, CH, Torbett, BE, and Barbas, CF 3rd</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14734553&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14734553&amp;dopt=abstract</a> </p><br />

<p>   17.    42698   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Treatment of primary HIV-1 infection with nonnucleoside reverse transcriptase inhibitor-based therapy is effective and well tolerated</p>

<p>           Portsmouth, S, Imami, N, Pires, A, Stebbing, J, Hand, J, Nelson, M, Gotch, F, and Gazzard, BG </p>

<p>           HIV Med 2004. 5 (1): 26-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14731166&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14731166&amp;dopt=abstract</a> </p><br />

<p>   18.    42699   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Prognostic value of proliferative responses to HIV-1 antigen in chronically HIV-infected patients under antiretroviral therapy</p>

<p>           Mohm, JM, Rump, J-A, Schulte-Monting, J, and Schneider, J</p>

<p>           Journal of Clinical Virology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4BJ2FS0-1/2/56a793c0b0f5ad6348726ea0eb0e58d0">http://www.sciencedirect.com/science/article/B6VJV-4BJ2FS0-1/2/56a793c0b0f5ad6348726ea0eb0e58d0</a> </p><br />

<p>  19.     42700   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Methylene blue photoinactivation of RNA viruses</p>

<p>           Floyd, Robert A, Schneider, Jr JEdward, and Dittmer, Dirk P</p>

<p>           Antiviral Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4B8K7CB-1/2/db66fa58b1acf404e4923d96d0b5453e">http://www.sciencedirect.com/science/article/B6T2H-4B8K7CB-1/2/db66fa58b1acf404e4923d96d0b5453e</a> </p><br />

<p>   20.    42701   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>Hiv-1 Viremia Prevents the Establishment of Interleukin 2- Producing Hiv-Specific Memory Cd4(+) T Cells Endowed With Proliferative Capacity</b></p>

<p>           Younes, SA, Yassine-Diab, B, Dumont, AR, Boulassel, MR,  Grossman, Z, Routy, JP, and Sekaly, RP</p>

<p>           Journal of Experimental Medicine  2003. 198(12): 1909-1922</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   21.    42702   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Pharmacokinetic interaction between rifampin and the combination of indinavir and low-dose ritonavir in HIV-infected patients</p>

<p>           Justesen, US, Andersen, AB, Klitgaard, NA, Brosen, K, Gerstoft, J, and Pedersen, C</p>

<p>           Clin Infect Dis 2004. 38(3): 426-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14727216&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14727216&amp;dopt=abstract</a> </p><br />

<p>   22.    42703   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Nuclear antisense effects in cyclophilin A pre-mRNA splicing by oligonucleotides: a comparison of tricyclo-DNA with LNA</p>

<p>           Ittig, D, Liu, S, Renneberg, D, Schumperli, D, and Leumann, CJ</p>

<p>           Nucleic Acids Res 2004. 32(1): 346-53</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14726483&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14726483&amp;dopt=abstract</a> </p><br />

<p>   23.    42704   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Cation-selective ion channels formed by p7 of hepatitis C virus are blocked by hexamethylene amiloride</p>

<p>           Premkumar, A, Wilson, L, Ewart, GD, and Gage, PW</p>

<p>           FEBS Letters 2004. 557(1-3): 99-103</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T36-4B7PYYM-3/2/8aa04089709330e37ac30f1d4a5e67a4">http://www.sciencedirect.com/science/article/B6T36-4B7PYYM-3/2/8aa04089709330e37ac30f1d4a5e67a4</a> </p><br />

<p>   24.    42705   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>Mechanistic Basis for Reduced Viral and Enzymatic Fitness of Hiv-1 Reverse Transcriptase Containing Both K65r and M184v Mutations</b></p>

<p>           Deval, J, White, KL, Miller, MD, Parkin, NT, Courcambeck, J, Halfon, P, Selmi, B, Boretto, J, and Canard, B</p>

<p>           Journal of Biological Chemistry 2004. 279(1): 509-516</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.     42706   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Molecular biology of the human immunodeficiency virus: Current and future targets for intervention</p>

<p>           Krogstad, P</p>

<p>           Semin Pediatr Infect Dis 2003. 14(4): 258-68</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14724791&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14724791&amp;dopt=abstract</a> </p><br />

<p>   26.    42707   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Synthesis of 6-arylvinyl analogues of the HIV drugs SJ-3366 and Emivirine</p>

<p>           Wamberg, Michael, Pedersen, Erik B, El-Brollosy, Nasser R, and Nielsen, Claus</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BDW3BR-F/2/a6f2a66f2073bccce730988d223d7b81">http://www.sciencedirect.com/science/article/B6TF8-4BDW3BR-F/2/a6f2a66f2073bccce730988d223d7b81</a> </p><br />

<p>   27.    42708   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Three-dimensional quantitative structure-activity relationship analyses of piperidine-based CCR5 receptor antagonists</p>

<p>           Song, M, Breneman, CM, and Sukumar, N</p>

<p>           Bioorg Med Chem 2004. 12(2): 489-99</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14723967&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14723967&amp;dopt=abstract</a> </p><br />

<p>   28.    42709   HIV-LS-288; EMBASE-HIV-1/29/2004</p>

<p class="memofmt1-3">           Complex patterns of viral load decay under antiretroviral therapy: influence of pharmacokinetics and intracellular delay</p>

<p>           Dixit, Narendra M and Perelson, Alan S</p>

<p>           Journal of Theoretical Biology 2004.  226(1): 95-109</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WMD-4B1WXVT-6/2/577c42c90c67201f88d941a4653ed7ad">http://www.sciencedirect.com/science/article/B6WMD-4B1WXVT-6/2/577c42c90c67201f88d941a4653ed7ad</a> </p><br />

<p>   29.    42710   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral evaluation of novel 3&#39;- and 4&#39;-doubly branched carbocyclic nucleosides as potential antiviral agents</p>

<p>           Hong, JH</p>

<p>           Arch Pharm Res 2003. 26(12): 1109-16</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14723348&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14723348&amp;dopt=abstract</a> </p><br />

<p>   30.    42711   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Compensatory link between fusion and endocytosis of human immunodeficiency virus type 1 in human CD4 T lymphocytes</p>

<p>           Schaeffer, E, Soros, VB, and Greene, WC</p>

<p>           J Virol 2004. 78(3): 1375-83</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14722292&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14722292&amp;dopt=abstract</a> </p><br />

<p>  31.     42712   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Mechanism of HIV-1 Integrase Inhibition by Styrylquinoline Derivatives in Vitro</p>

<p>           Deprez, E, Barbe, S, Kolaski, M, Leh, H, Zouhiri, F, Auclair, C, Brochon, JC, Le, Bret M, and Mouscadet, JF</p>

<p>           Mol Pharmacol  2004. 65(1): 85-98</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14722240&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14722240&amp;dopt=abstract</a> </p><br />

<p>   32.    42713   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>3,5-Di-(Tert-Butyl)-6-Fluoro-Cyclosal-D4tmp - a Pronucleotide With a Considerably Improved Masking Group</b></p>

<p>           Ducho, C, Wendicke, S, Gorbig, U, Balzarini, J, and Meier, C</p>

<p>           European Journal of Organic Chemistry 2003(24): 4786-4791</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   33.    42714   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Application of CoMFA and CoMSIA 3D-QSAR and Docking Studies in Optimization of Mercaptobenzenesulfonamides as HIV-1 Integrase Inhibitors</p>

<p>           Kuo, CL, Assefa, H, Kamath, S, Brzozowski, Z, Slawinski, J, Saczewski, F, Buolamwini, JK, and Neamati, N</p>

<p>           J Med Chem 2004. 47(2): 385-399</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14711310&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14711310&amp;dopt=abstract</a> </p><br />

<p>   34.    42715   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           Intensification of antiretroviral therapy accelerates the decay of the HIV-1 latent reservoir and decreases, but does not eliminate, ongoing virus replication</p>

<p>           Ramratnam, B, Ribeiro, R, He, T, Chung, C, Simon, V, Vanderhoeven, J, Hurley, A, Zhang, L, Perelson, AS, Ho, DD, and Markowitz, M</p>

<p>           J Acquir Immune Defic Syndr 2004.  35(1): 33-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14707789&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14707789&amp;dopt=abstract</a> </p><br />

<p>   35.    42716   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>No Clinically Significant Pharmacokinetic Interactions Between Voriconazole and Indinavir in Healthy Volunteers</b></p>

<p>           Purkins, L, Wood, N, Kleinermans, D, and Love, ER</p>

<p>           British Journal of Clinical Pharmacology 2003. 56: 62-68</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   36.    42717   HIV-LS-288; PUBMED-HIV-1/27/2004</p>

<p class="memofmt1-3">           SC35 and hnRNP A/B proteins bind to a juxtaposed ESE/ESS element to regulate HIV-1 tat exon 2 splicing</p>

<p>           Zahler, AM, Damgaard, CK, Kjems, J, and Caputi, M</p>

<p>           J Biol Chem 2003</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14703516&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14703516&amp;dopt=abstract</a> </p><br />

<p>   37.    42718   HIV-LS-288; WOS-HIV-1/22/2004</p>

<p>           <b>Synthesis of Novel Carboacyclic Nucleosides With Vinyl Bromide Moiety as Open-Chain Analogues of Neplanocin a</b></p>

<p>           Choi, MH and  Kim, HD</p>

<p>           Archives of Pharmacal Research 2003. 26(12): 990-996</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   38.    42719   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Enhanced uptake of a heterologous protein with an HIV-1 tat protein transduction domains (PTD) at both termini</p>

<p>           Ryu, J, Han, K, Park, J, and Choi, SY</p>

<p>           MOL CELLS 2003. 16(3): 385-391</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187603900017">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187603900017</a> </p><br />

<p>   39.    42720   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Induction of intercellular adhesion molecule-1 on human brain endothelial cells by HIV-1 gp120: Role of CD4 and chemokine coreceptors</p>

<p>           Stins, MF, Pearce, D, Di Cello, F, Erdreich-Epstein, A, Pardo, CA, and Kim, KS</p>

<p>           LAB INVEST 2003. 83(12): 1787-1798</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187673800011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187673800011</a> </p><br />

<p>   40.    42721   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Improved inhibition of human immunodeficiency virus type 1 replication by intracellular co-overexpression of TAR and RRE decoys in tandem array</p>

<p>           Lee, SW</p>

<p>           J MICROBIOL 2003. 41(4): 300-305</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187732200006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187732200006</a> </p><br />

<p>   41.    42722   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Incidence of resistance in a double-blind study comparing lopinavir/ritonavir plus stavudine and lamivudine to nelfinavir plus stavudine and lamivudine</p>

<p>           Kempf, DJ, King, MS, Bernstein, B, Cernohous, P, Bauer, E, Moseley, J, Gu, K, Hsu, A, Brun, S, and Sun, E</p>

<p>           J INFECT DIS 2004. 189(1): 51-60</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187723300008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187723300008</a> </p><br />

<p>   42.    42723   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Potent cross-group neutralization of primary human immunodeficiency virus isolates with monoclonal antibodies - Implications for acquired immunodeficiency syndrome vaccine</p>

<p>           Ferrantelli, F, Kitabwalla, M, Rasmussen, RA, Cao, C, Chou, TC, Katinger, H, Stiegler, G, Cavacini, LA, Bai, Y, Cotropia, J, Ugen, KE, and Ruprecht, RM</p>

<p>           J INFECT DIS 2004. 189(1): 71-74</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187723300010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187723300010</a> </p><br />

<p>   43.    42724   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Isolation, structure and HIV-1 integrase inhibitory activity of exophillic acid, a novel fungal metabolite from Exophiala pisciphila</p>

<p>           Ondeyka, JG, Zink, DL, Dombrowski, AW, Polishook, JD, Felock, PJ, Hazudp, DJ, and Singh, SB</p>

<p>           J ANTIBIOT 2003. 56(12): 1018-1023</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187623700006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187623700006</a> </p><br />

<p>   44.    42725   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Polyriboinosinic polyribocytidylic acid [poly(I : C)]/TLR3 signaling allows class I processing of exogenous protein and induction of HIV-specific CD8(+) cytotoxic T lymphocytes</p>

<p>           Fujimoto, C, Nakagawa, Y, Ohara, K, and Takahashi, H</p>

<p>           INT IMMUNOL 2004. 16(1): 55-63</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187826800007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187826800007</a> </p><br />

<p>   45.    42726   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Multiple effects of HIV-1 trans-activator protein on the pathogenesis of HIV-1 infection</p>

<p>           Huigen, MCDG, Kamp, W, and Nottet, HSLM</p>

<p>           EUR J CLIN INVEST 2004. 34(1): 57-66</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187892600009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187892600009</a> </p><br />

<p>   46.    42727   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           CCR5 antagonists as anti-HIV-1 agents. 1. Synthesis and biological evaluation of 5-oxopyrrolidine-3-carboxamide derivatives</p>

<p>           Imamura, S, Ishihara, Y, Hattori, T, Kurasawa, O, Matsushita, Y, Sugihara, Y, Kanzaki, N, Iizawa, Y, Baba, M, and Hashiguchi, S</p>

<p>           CHEM PHARM BULL 2004. 52(1): 63-73</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187736600009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187736600009</a> </p><br />

<p>   47.    42728   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Secondary mutations M36I and A71V in the human immunodeficiency virus type 1 protease can provide an advantage for the emergence of the primary mutation D30N</p>

<p>           Clemente, JC, Hemrajani, R, Blum, LE, Goodenow, MA, and Dunn, BA</p>

<p>           BIOCHEMISTRY-US 2003. 42(51): 15029-15035</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187634900003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187634900003</a> </p><br />

<p>   48.    42729   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Anti-HIV-1 activity and mode of action of mirror image oligodeoxynucleotide analogue of Zintevir</p>

<p>           Urata, H, Kumashiro, T, Kawahata, T, Otake, T, and Akagi, M</p>

<p>           BIOCHEM BIOPH RES CO 2004. 313(1):  55-61</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187745900010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187745900010</a> </p><br />

<p>   49.    42730   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Evolution of human immunodeficiency virus type 1 (HIV-1) resistance mutations in nonnucleoside reverse transcriptase inhibitors (NNRTIs) in HIV-1-infected patients switched to antiretroviral therapy without NNRTIs</p>

<p>           Joly, V, Descamps, D, Peytavin, G, Touati, F, Mentre, F, Duval, X, Delarue, S, Yeni, P, and Brun-Vezinet, F</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(1): 172-175</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187728500025">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187728500025</a> </p><br />

<p>   50.    42731   HIV-LS-288; WOS-HIV-1/25/2004</p>

<p class="memofmt1-3">           Recent advances in anti-HIV pharmacology</p>

<p>           Mouscadet, JF and Deprez, E</p>

<p>           ACTUAL CHIMIQUE 2003(11-12): 128-134</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187571200022">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187571200022</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
