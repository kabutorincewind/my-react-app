

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-115.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="oULw9ziWaQ2ylmTTkulpwQfPQyYGsXPwDy1bn8WkOVmLWpBKQGI27UrZyq0DBZsIHKDTpUkoyVMUiRbyMSB6b8WzaSl/7+qNWqfAsOU1Z3IUxm71dt4i/uwyZxU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CAB216FB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-115-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58280   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of an RNA virus 3&#39;-&gt;5&#39; exoribonuclease that is critically involved in coronavirus RNA synthesis</p>

    <p>          Minskaia, E, Hertzig, T, Gorbalenya, AE, Campanacci, V, Cambillau, C, Canard, B, and Ziebuhr, J</p>

    <p>          Proc Natl Acad Sci U S A <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16549795&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16549795&amp;dopt=abstract</a> </p><br />

    <p>2.     58281   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of compounds with anti-west nile virus activity</p>

    <p>          Goodell, JR, Puig-Basagoiti, F, Forshey, BM, Shi, PY, and Ferguson, DM</p>

    <p>          J Med Chem <b>2006</b>.  49(6): 2127-2137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539402&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539402&amp;dopt=abstract</a> </p><br />

    <p>3.     58282   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus (HBV) replication by pyrimidines bearing an acyclic moiety: effect on wild-type and mutant HBV</p>

    <p>          Semaine, W, Johar, M, Tyrrell, DL, Kumar, R, and Agrawal, B</p>

    <p>          J Med Chem <b>2006</b>.  49(6): 2049-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539393&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539393&amp;dopt=abstract</a> </p><br />

    <p>4.     58283   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Alkoxyalkyl Derivatives of 9-(S)-(3-Hydroxy-2-phosphonomethoxypropyl)adenine against Cytomegalovirus and Orthopoxviruses</p>

    <p>          Beadle, JR, Wan, WB, Ciesla, SL, Keith, KA, Hartline, C, Kern, ER, and Hostetler, KY</p>

    <p>          J Med Chem <b>2006</b>.  49(6): 2010-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539388&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539388&amp;dopt=abstract</a> </p><br />

    <p>5.     58284   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The polymerase complex genes contribute to the high virulence of the human H5N1 influenza virus isolate A/Vietnam/1203/04</p>

    <p>          Salomon, R, Franks, J, Govorkova, EA, Ilyushina, NA, Yen, HL, Hulse-Post, DJ, Humberd, J, Trichet, M, Rehg, JE, Webby, RJ, Webster, RG, and Hoffmann, E</p>

    <p>          J Exp Med <b>2006</b>.  203(3): 689-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16533883&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16533883&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     58285   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ribofuranosyl imidazotriazinone compounds as broad-spectrum inhibitors of viruses in the Flaviviridae family</p>

    <p>          Ojwang, Joshua O</p>

    <p>          PATENT:  US <b>2006035848</b>  ISSUE DATE:  20060216</p>

    <p>          APPLICATION: 2005-3187  PP: 19 pp.</p>

    <p>          ASSIGNEE:  (Zymetx, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     58286   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A guided tour through the antiviral drug field</p>

    <p>          De Clearcq, Erik</p>

    <p>          Future Virology <b>2006</b>.  1(1): 19-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     58287   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of critical determinants on ACE2 for SARS-CoV entry and development of a potent entry inhibitor</p>

    <p>          Han, DP, Penn-Nicholson, A, and Cho, MW</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16510163&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16510163&amp;dopt=abstract</a> </p><br />

    <p>9.     58288   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Compositions and methods for treating or preventing Hepadnaviridae infection</p>

    <p>          Dugourd, Dominique</p>

    <p>          PATENT:  US <b>2006052414</b>  ISSUE DATE:  20060309</p>

    <p>          APPLICATION: 2005-6259  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Migenix, Inc. Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   58289   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rational Development of b-Peptide Inhibitors of Human Cytomegalovirus Entry</p>

    <p>          English, Emily Payne, Chumanov, Robert S, Gellman, Samuel H, and Compton, Teresa</p>

    <p>          Journal of Biological Chemistry <b>2006</b>.  281(5): 2661-2667</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   58290   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inactivation of herpes simplex virus clinical isolates by using a combination microbicide</p>

    <p>          Isaacs, CE, Rohan, L, Xu, W, Jia, JH, Mietzner, T, and Hillier, S</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(3): 1063-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495269&amp;dopt=abstract</a> </p><br />

    <p>12.   58291   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Chemical structure and antiviral activity of carrageenans from Meristiella gelidium against herpes simplex and dengue virus</p>

    <p>          F-Tischer, Paula Cristina de S, Talarico, Laura B, Noseda, Miguel D, Guimaraes, Silvia Maria Pita B, Damonte, Elsa B, and Duarte, Maria Eugenia R</p>

    <p>          Carbohydrate Polymers <b>2006</b>.  63(4): 459-465</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   58292   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The effect of a methyl or 2-fluoroethyl substituent at the N-3 position of thymidine, 3&#39;-fluoro-3&#39;-deoxythymidine and 1-b-D-arabinosylthymine on their antiviral and cytostatic activity in cell culture</p>

    <p>          Balzarini, Jan, Celen, Sofie, Karlsson, Anna, de Groot, Tjibbe, Verbruggen, Alfons, and Bormans, Guy</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(1): 17-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   58293   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of ganciclovir-resistant human cytomegalovirus replication by Kampo (Japanese herbal medicine)</p>

    <p>          Murayama, Tsugiya, Yamaguchi, Nobuo, Iwamoto, Kozo, and Eizuru, Yoshito</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(1): 11-16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   58294   DMID-LS-115; SCIFINDER-DMID-3/27/2006; LR-14644-LWC ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of a carrageenan from Gigartina skottsbergii against intraperitoneal murine Herpes simplex virus infection</p>

    <p>          Pujol, CA, Scolaro, LA, Ciancia, M, Matulewicz, MC, Cerezo, AS, and Damonte, EB</p>

    <p>          Planta Medica <b>2006</b>.  72(2): 121-125</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   58295   DMID-LS-115; WOS-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of New 5-Substituted Pyrimidine Acyclonucleosides</p>

    <p>          Khattab, A.</p>

    <p>          Synthetic Communications <b>2006</b>.  36(8): 1097-1107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235953300015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235953300015</a> </p><br />

    <p>17.   58296   DMID-LS-115; WOS-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interaction of Intestinal Nucleoside Transporter Hcnt2 With Amino Acid Ester Prodrugs of Floxuridine and 2-Bromo-5,6-Dichloro-1-Beta-D-Beta Ribofuranosylbenzimidazole</p>

    <p>          Shin, H. <i>et al.</i></p>

    <p>          Biological &amp; Pharmaceutical Bulletin <b>2006</b>.  29(2): 247-252</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235851000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235851000010</a> </p><br />

    <p>18.   58297   DMID-LS-115; PUBMED-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of Phyllanthus nanus ethanolic extract against hepatitis B virus (HBV) by expression microarray analysis</p>

    <p>          Lam, WY, Leung, KT, Law, PT, Lee, SM, Chan, HL, Fung, KP, Ooi, VE, and Waye, MM</p>

    <p>          J Cell Biochem  <b>2006</b>.  97(4): 795-812</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16237706&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16237706&amp;dopt=abstract</a> </p><br />

    <p>19.   58298   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and syntheses of 8-substitued carbocyclic nucleoside analogs</p>

    <p>          Yin, Xueqiang, Ye, Wei, and Schneller, Stewart W</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-370</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>20.   58299   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Improved anti-viral compositions</p>

    <p>          Smith, Geoffrey Lilley, Carter, Gemma Chevonne, Law, Mansun, and Hollinshead, Michael Stanley</p>

    <p>          PATENT:  WO <b>2006000814</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 54 pp.</p>

    <p>          ASSIGNEE:  (Imperial College Innovations Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   58300   DMID-LS-115; WOS-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Computer-Assisted Analysis of the Interactions of Macrocyclic Inhibitors With Wild Type and Mutant D168a Hepatitis C Virus Ns3 Serine Protease</p>

    <p>          Da Cunha, E. <i>et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2006</b>.  3(1): 17-28</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235724000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235724000004</a> </p><br />

    <p>22.   58301   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral applications of RNAi for coronavirus</p>

    <p>          Wu, Chang-Jer and Chan, Yi-Lin</p>

    <p>          Expert Opinion on Investigational Drugs <b>2006</b>.  15(2): 89-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   58302   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SARS cov main protease inhibitors</p>

    <p>          Wu, Su Ying, Hsieh, Hsing Pang, Hsu, Tsu An, and Lu, ILin</p>

    <p>          PATENT:  US <b>2006019967</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005-54845  PP: 49 pp.</p>

    <p>          ASSIGNEE:  (Taiwan)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   58303   DMID-LS-115; SCIFINDER-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of nucleoside analogs against SARS-Coronavirus (SARS-CoV)</p>

    <p>          Chu, CK, Gadthula, S, Chen, Xin, Choo, H, Barnard, DL, and Sidwell, RW</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-388</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   58304   DMID-LS-115; WOS-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Outbreak of Highly Pathogenic Avian Influenza in Japan and Anti-Influenza Virus Activity of Povidone-Iodine Products</p>

    <p>          Ito, H. <i>et al.</i></p>

    <p>          Dermatology <b>2006</b>.  212: 115-118</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235690900023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235690900023</a> </p><br />

    <p>26.   58305   DMID-LS-115; WOS-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure Activity Relationship of Substituted Pyranoindoles as Hcv Rna Dependent Rna Polymerase Inhibitors.</p>

    <p>          Gopalsamy, A. <i> et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  229: U169</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600338">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600338</a> </p><br />
    <br clear="all">

    <p>27.   58306   DMID-LS-115; WOS-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of N3,5 &#39;-Cyclo-4-(Beta-D-Ribofuranosyl)-Vic-Tria-Zolo[4,5-B]Pyridin-5-One Analogues With Improved Anti-Hepatitis C Activity in Vitro.</p>

    <p>          Wang, P. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  229: U171</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600346">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600346</a> </p><br />

    <p>28.   58307   DMID-LS-115; WOS-DMID-3/27/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity of Cisplatin Derivatives of Tilorone Against Reovirus St3, Vaccinia Virus, Varicella Zoster Virus (Vzv), and Herpes Simplix Virus (Hsv-1).</p>

    <p>          Roner, M., Carraher, C., and Dhanji, S.</p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  229: U1145</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066605272">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066605272</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
