

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-10-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wYPzVkfZn7itSQ/mvV8ztffETbF/ukin14byG5wmY65iI2jnBLu6kxAhGSE88PfKU+PktA1G1tuRTQ3+PX5oQEp0bk8+TzInG/qtYq1sl03sjxvjTcBOjZ0T1G8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D8B7B1B4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">October 1 - October 14, 2010</p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20930383">Anti-hepatitis B virus activities of cinobufacini and its active components bufalin and cinobufagin in HepG2.2.15 cells.</a> Cui, X., Y. Inagaki, H. Xu, D. Wang, F. Qi, N. Kokudo, D. Fang, and W. Tang. Biological and Pharmaceutical Bulliten, 2010. 33(10): p. 1728-1732; PMID[20930383].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1001-101410.</p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20880703">Discovery of potent nucleotide-mimicking competitive inhibitors of hepatitis C virus NS3 helicase</a> Gemma, S., S. Butini, G. Campiani, M. Brindisi, S. Zanoli, M.P. Romano, P. Tripaldi, L. Savini, I. Fiorini, G. Borrelli, E. Novellino, and G. Maga. Bioorganic &amp; Medicinal Chemistry Letters, 2010; <b>[Epub ahead of print]</b>. PMID[20880703].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1001-101410.</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">3. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281735600023">7-O-Arylmethylgalangin as a novel scaffold for anti-HCV agents.</a> Lee, H.S., K.S. Park, C. Lee, B. Lee, D.E. Kim, and Y. Chong. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(19): p. 5709-5712; ISI[000281735600023].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1001-1014.</p>
    
    <p> </p>

    <p class="NoSpacing">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281960800021">Hepatitis C Virus Core-Derived Peptides Inhibit Genotype 1b Viral Genome Replication via Interaction with DDX3X.</a> Sun, C.M., C.T. Pager, G.X. Luo, P. Sarnow, and J.H.D. Cate,<i>.</i> Plos One, 2010. 5(9); e12826. ISI[000281960800021].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1001-1014.</p>
    
    <p> </p>    

    <p class="NoSpacing">5. <i><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281735600020">Novel macrocyclic HCV NS3 protease inhibitors derived from alpha-amino cyclic boronates.</a></i> Li, X.F., Y.K. Zhang, Y. Liu, C.Z. Ding, Y.S. Zhou, Q. Li, J.J. Plattner, S.J. Baker, S.M. Zhang, W.M. Kazmierski, L.L. Wright, G.K. Smith, R.M. Grimes, R.M. Crosby, K.L. Creech, L.H. Carballo, M.J. Slater, R.L. Jarvest, P. Thommes, J.A. Hubbard, M.A. Convery, P.M. Nassau, W. McDowell, T.J. Skarzynski, X.L. Qian, D.Z. Fan, L.A. Liao, Z.J. Ni, L.E. Pennicott, W.X. Zou, and J. Wright<i>.</i> Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(19): p. 5695-5700; ISI[000281735600020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1001-1014.</p>
    
    <p> </p>    

    <p class="NoSpacing">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281803700001">RNA Interference inhibits Hepatitis B Virus of different genotypes in Vitro and in Vivo.</a> Zhang, Y.L., T. Cheng, Y.J. Cai, Q.A. Yuan, C. Liu, T. Zhang, D.Z. Xia, R.Y. Li, L.W. Yang, Y.B. Wang, A.E.T. Yeo, J.W.K. Shih, J. Zhang, and N.S. Xia, BMC Microbiology, 2010. 10; 214-223. ISI[000281803700001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1001-101410.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
