

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-337.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MLCNvP4so+8nO18uzRwJYr7EIShG06Gq3UXYzvgMCAS9gd4Cn7GRn2o3Q+Hpd2Zj/91tGx+dZ6HE1ogNy0FSgWuQoYvvmqjxLkoAUCKGZsFTfTyLVQJGrjgPwrk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CF55FD28" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-337-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48219   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          Effect of a Bound Non-Nucleoside RT Inhibitor on the Dynamics of Wild-Type and Mutant HIV-1 Reverse Transcriptase</p>

    <p>          Zhou, Z, Madrid, M, Evanseck, JD, and Madura, JD</p>

    <p>          J Am Chem Soc <b>2005</b>.  127(49): 17253-17260</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16332074&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16332074&amp;dopt=abstract</a> </p><br />

    <p>2.     48220   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activities of 1,2,3-triazole functionalized thymidines: 1,3-dipolar cycloaddition for efficient regioselective diversity generation</p>

    <p>          Zhou, L, Amer, A, Korn, M, Burda, R, Balzarini, J, De Clercq, E, Kern, ER, and Torrence, PF</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(6): 375-383</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16331842&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16331842&amp;dopt=abstract</a> </p><br />

    <p>3.     48221   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          A novel small molecular weight compound with a carbazole structure that demonstrates potent human immunodeficiency virus type-1 integrase inhibitory activity</p>

    <p>          Yan, H, Mizutani, TC, Nomura, N, Takakura, T, Kitamura, Y, Miura, H, Nishizawa, M, Tatsumi, M, Yamamoto, N, and Sugiura, W</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(6): 363-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16329284&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16329284&amp;dopt=abstract</a> </p><br />

    <p>4.     48222   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of new alkenyldiarylmethane (ADAM) non-nucleoside reverse transcriptase inhibitors (NNRTIs) incorporating benzoxazolone and benzisoxazole rings</p>

    <p>          Deng, BL, Cullen, MD, Zhou, Z, Hartman, TL, Buckheit, RW Jr, Pannecouque, C, Clercq, ED, Fanwick, PE, and Cushman, M</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16321539&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16321539&amp;dopt=abstract</a> </p><br />

    <p>5.     48223   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          Dendritic Catanionic Assemblies: In vitro Anti-HIV Activity of Phosphorus-Containing Dendrimers Bearing Galbeta(1)cer Analogues</p>

    <p>          Blanzat, M, Turrin, CO, Aubertin, AM, Couturier-Vidal, C, Caminade, AM, Majoral, JP, Rico-Lattes, I, and Lattes, A</p>

    <p>          Chembiochem <b>2005</b>.  6(12): 2207-2213</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16317767&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16317767&amp;dopt=abstract</a> </p><br />

    <p>6.     48224   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          Phosphoramidate and phosphate prodrugs of (-)-beta-d-(2R,4R)-dioxolane-thymine: Synthesis, anti-HIV activity and stability studies</p>

    <p>          Liang, Y, Narayanasamy, J, Schinazi, RF, and Chu, CK</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16314108&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16314108&amp;dopt=abstract</a> </p><br />

    <p>7.     48225   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of bi-functional betulinic acid derivatives</p>

    <p>          Huang, L, Ho, P, Lee, KH, and Chen, CH</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16314103&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16314103&amp;dopt=abstract</a> </p><br />

    <p>8.     48226   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          The antiviral activity, mechanism of action, clinical significance and resistance of abacavir in the treatment of pediatric AIDS</p>

    <p>          Melroy, J and Nair, V</p>

    <p>          Curr Pharm Des  <b>2005</b>.  11(29): 3847-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16305515&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16305515&amp;dopt=abstract</a> </p><br />

    <p>9.     48227   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Methods and compositions for the inhibition of HIV-1 replication in hum an cells using inhibitors of p21/CDKN1A activity or gene expression, and anti-AIDS uses</p>

    <p>          Wahl, Sharon M, Vazquez-Maldonado, Nancy, and Greenwell-Wild, Teresa</p>

    <p>          PATENT:  WO <b>2005046732</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 98 pp.</p>

    <p>          ASSIGNEE:  (The United Sates of America as Represented by the Secretary of Health and Human Services, NIH USA</p>

    <br />

    <p>10.   48228   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Novel 4-oxoquinoline compound and use thereof as hiv integrase inhibitor</p>

    <p>          Satoh, Motohide, Matsuda, Takashi, Okuda, Satoshi, Kawakami, Hiroshi, Aramaki, Hisateru, Shinkai, Hisashi, Matsuzaki, Yuji, Watanabe, Wataru, Yamataka, Kazunobu, Kiyonari, Shinichi, Wamaki, Shuichi, Takahashi, Mitsuru, Yamada, Naohito, and Nagao, Akemi</p>

    <p>          PATENT:  WO <b>2005113509</b>  ISSUE DATE:  20051201</p>

    <p>          APPLICATION: 2005</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <br />

    <p>11.   48229   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          HIV integrase inhibitors</p>

    <p>          Wai, John S, Vacca, Joseph P, Zhuang, Linghang, Kim, Boyoung, Lyle, Terry A, Wiscount, Catherine M, Egbertson, Melissa S, Neilson, Lou Anne, Embrey, Mark, Fisher, Thorsten E, and Staas, Donnette D</p>

    <p>          PATENT:  WO <b>2005110415</b>  ISSUE DATE:  20051124</p>

    <p>          APPLICATION: 2005  PP: 76 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <br />

    <p>12.   48230   HIV-LS-337; PUBMED-HIV-12/12/2005</p>

    <p class="memofmt1-2">          Intermolecular interactions in the crystal structures of potential HIV-1 integrase inhibitors</p>

    <p>          Majerz-Maniecka, K, Musiol, R, Nitek, W, Oleksyn, BJ, Mouscadet, JF, Bret, ML, and Polanski, J </p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <br />

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16289813&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16289813&amp;dopt=abstract</a> </p><br />

    <p>13.   48231   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of aminocarbonylphenyl-5H-pyrazolo-benzoxazinyl-phenyl methanone derivs. as HIV replication inhibitors</p>

    <p>          Tahri, Abdellah, Michiels, Lieve Emma Jan, Van Acker, Koenraad Lodewijk August, and Surleraux, Dominique Louis Nestor Ghislain</p>

    <p>          PATENT:  WO <b>2005105810</b>  ISSUE DATE:  20051110</p>

    <p>          APPLICATION: 2005  PP: 46 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <br />

    
    <p>14.   48232   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Use of racemic nor-calanolide A in preparation of anti-HIV medicines</p>

    <p>          Wang, Lin, Liu, Gang, Tao, Peizhen, Zhang, Xingquan, Zhang, Tian, Huang, Biao, and Zhao, Zhizhong</p>

    <p>          PATENT:  CN <b>1565442</b>  ISSUE DATE: 20050119</p>

    <p>          APPLICATION: 2003-11440  PP: 17 pp.</p>

    <p>          ASSIGNEE:  (Institute of Materia Medica, Chinese Academy of Medical Sciences Peop. Rep. China</p>

    <br />

    <p>15.   48233   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of purine derivatives as antiviral agents</p>

    <p>          Wang, Xuechao, Liu, Zhongrong, Li, Bogang, Wu, Fengwei, Zhong, Chaobin, He, Min, and Huang, Yu </p>

    <p>          PATENT:  CN <b>1566118</b>  ISSUE DATE: 20050119</p>

    <p>          APPLICATION: 2003-4232  PP: 15 pp.</p>

    <p>          ASSIGNEE:  (Chengdu Diao Pharmaceutical Group Co., Ltd. Peop. Rep. China</p>

    <br />

    <p>16.   48234   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of substituted alkylamides as antiviral agents</p>

    <p>          Kaplan, Eliahu, Bittner, Shmuel, and Harlev, Eliakim</p>

    <p>          PATENT:  WO <b>2005092305</b>  ISSUE DATE:  20051006</p>

    <p>          APPLICATION: 2005  PP: 83 pp.</p>

    <p>          ASSIGNEE:  (New Era Biotech Ltd., Virgin I. Brit.</p>

    <br />
    <p>17.   48235   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of hydroxynaphthyridinediones as HIV integrase inhibitors</p>

    <p>          Han, Wei, Egbertson, Melissa, Wai, John S, Zhuang, Linghang, Ruzek, Rowena D, Perlow, Debra S, and Obligado, Vanessa E</p>

    <p>          PATENT:  WO <b>2005087767</b>  ISSUE DATE:  20050922</p>

    <p>          APPLICATION: 2005  PP: 83 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <br />

    <p>18.   48236   HIV-LS-337; SCIFINDER-HIV-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of N-arylmethyl or N-heterocyclylmethyl-N-(imidazol-2-ymethyl)amines as antagonists of chemokine receptor CXCR4</p>

    <p>          Saitou, Atsushi, Kikumoto, Shigeyuki, Ono, Masahiro, Matsui, Ryo, Yamamoto, Masashi, Sawa, Tomohiro, Suzuki, Shigeru, and Yanaka, Mikiro</p>

    <p>          PATENT:  WO <b>2005085209</b>  ISSUE DATE:  20050915</p>

    <p>          APPLICATION: 2005  PP: 252 pp.</p>

    <p>          ASSIGNEE:  (Kureha Chemical Industry Company, Limited Japan</p>

    <br />

    <p>19.   48237   HIV-LS-337; WOS-HIV-12/4/2005</p>

    <p class="memofmt1-2">          A short synthetic route to biologically active (+/-)-daurichromenic acid as highly potent anti-HIV agent</p>

    <p>          Lee, YR and Wang, X</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2005</b>.  3(21): 3955-3957, 3</p>

    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233241900017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233241900017</a> </p><br />

    <p>20.   48238   HIV-LS-337; WOS-HIV-12/4/2005</p>

    <p class="memofmt1-2">          Amino acid derivatives, part 3: New peptide and glycopeptide derivatives conjugated naphthalene. Synthesis, antitumor, anti-HIV, and BVDV evaluation</p>

    <p>          Al-Masoudi, NA, Al-Masoudi, IA, Ali, IAI, Saeed, B, and La, Colla P</p>

    <p>          HETEROATOM CHEMISTRY <b>2005</b>.  16(7): 576-586, 11</p>

    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233369500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233369500007</a> </p><br />

    <p>21.   48239   HIV-LS-337; WOS-HIV-12/4/2005</p>

    <p class="memofmt1-2">          Synthesis of novel 4 &#39;alpha-phenyl and 5 &#39;alpha-methyl branched carbocyclic nucleosides</p>

    <p>          Oh, CH and Hong, JH</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY <b>2005</b>.  26(10): 1520-1524, 5</p>

    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233211900012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233211900012</a> </p><br />

    <p>22.   48240   HIV-LS-337; WOS-HIV-12/11/2005</p>

    <p class="memofmt1-2">          Synthesis, spectroscopic, electrochemical and anti-HIV studies of some mononuclear and dinuclear Fe-III complexes of 3-hydroxy-4 &#39;-benzyloxy flavone containing oxygen, nitrogen and sulphur donors as co-ligands</p>

    <p>          Mishra, L, Singh, AK, and Maeda, Y</p>

    <p>          JOURNAL OF THE INDIAN CHEMICAL SOCIETY <b>2005</b>.  82(10): 879-885, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233470300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233470300003</a> </p><br />
    <br clear="all">

    <p>23.   48241   HIV-LS-337; WOS-HIV-12/11/2005</p>

    <p class="memofmt1-2">          Analogues of N-terminal truncated synthetic peptide fragments derived from RANTES inhibit HIV-1 infectivity</p>

    <p>          Ramnarine, EJ, DeViCo, AL, and Vigil-Cruz, SC</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(1): 93-95, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233516200018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233516200018</a> </p><br />

    <p>24.   48242   HIV-LS-337; WOS-HIV-12/11/2005</p>

    <p class="memofmt1-2">          The broad anti-viral agent glycyrrhizin directly modulates the fluidity of plasma membrane and HIV-1 envelope</p>

    <p>          Harada, S</p>

    <p>          BIOCHEMICAL JOURNAL <b>2005</b>.  392: 191-199, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233542800021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233542800021</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
