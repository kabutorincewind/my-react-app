

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-339.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wu9zfHkv49SL3gMFFyCYa1U/j6u3D9J8UPCY2PvSdV7576r7ZNNiGpEGEGQIKp8PFd3zhwMKCt+oJYO48jyLURcTx6SzYzObuEkiM7pyutOVpbYgaSAOFw9ua7Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A45B7774" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-339-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48333   HIV-LS-339; PUBMED-HIV-1/9/2006</p>

    <p class="memofmt1-2">          Three New Lignans, Longipedunins A-C, from Kadsura longipedunculata and Their Inhibitory Activity against HIV-1 Protease</p>

    <p>          Sun, QZ, Chen, DF, Ding, PL, Ma, CM, Kakuda, H, Nakamura, N, and Hattori, M</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2006</b>.  54(1): 129-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16394567&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16394567&amp;dopt=abstract</a> </p><br />

    <p>2.     48334   HIV-LS-339; PUBMED-HIV-1/9/2006</p>

    <p class="memofmt1-2">          Nucleoside and nucleotide inhibitors of HIV-1 replication</p>

    <p>          Vivet-Boudou, V, Didierjean, J, Isel, C, and Marquet, R</p>

    <p>          Cell Mol Life Sci <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16389458&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16389458&amp;dopt=abstract</a> </p><br />

    <p>3.     48335   HIV-LS-339; SCIFINDER-HIV-1/3/2006 </p>

    <p class="memofmt1-2">          Syntheses of cyclodextrin-3&#39;-azido-3&#39;-deoxythymidine conjugates and their sulfates with improved anti-HIV activities</p>

    <p>          Chung, Ildoo, Lee, Chong-Kyo, Ha, Chang-Sik, and Cho, Won-Jei</p>

    <p>          Journal of Polymer Science, Part A: Polymer Chemistry <b>2005</b>.  44(1): 295-303</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     48336   HIV-LS-339; SCIFINDER-HIV-1/3/2006 </p>

    <p class="memofmt1-2">          Preparation of N-Benzyl-3,4-dihyroxypyridine-2-carboxamides useful as HIV integrase inhibitors</p>

    <p>          Jones, Philip, Williams, Peter D, Morrissette, Matthew M, Kuo, Michelle Sparks, and Vacca, Joseph P</p>

    <p>          PATENT:  WO <b>2005074513</b>  ISSUE DATE:  20050818</p>

    <p>          APPLICATION: 2005  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA and Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48337   HIV-LS-339; SCIFINDER-HIV-1/3/2006 </p>

    <p class="memofmt1-2">          Carbocyclic N-3 isonucleosides as SAHase inhibitors for antiviral chemotherapeutics</p>

    <p>          Bakke, Brian A, Mosley, Sylvester L, Sadler, Joshua M, Sunkara, Narsesh K, and Seley, Katherine L</p>

    <p>          Abstracts of Papers, 230th ACS National Meeting, Washington, DC, United States, Aug. 28-Sept. 1, 2005 <b>2005</b>.: MEDI-166</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     48338   HIV-LS-339; PUBMED-HIV-1/9/2006</p>

    <p class="memofmt1-2">          Determination of the novel non-peptidic HIV-protease inhibitor tipranavir by HPLC-UV after solid-phase extraction</p>

    <p>          Colombo, S, Beguin, A, Marzolini, C, Telenti, A, Biollaz, J, and Decosterd, LA</p>

    <p>          J Chromatogr B Analyt Technol Biomed Life Sci <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359932&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359932&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     48339   HIV-LS-339; SCIFINDER-HIV-1/3/2006 </p>

    <p class="memofmt1-2">          2&#39;,3&#39;-Dideoxynucleoside 5&#39;-b,g-(Difluoromethylene) Triphosphates With a-P-Thio or a-P-Seleno Modifications: Synthesis and Their Inhibition of HIV-1 Reverse Transcriptase</p>

    <p>          Boyle, Nicholas, Fagan, Patrick, Brooks, Jennifer, Prhavc, Marija, Lambert, John, and Cook, PDan</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1651-1664</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     48340   HIV-LS-339; SCIFINDER-HIV-1/3/2006 </p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of aryl phosphoramidate derivatives of b-D- and b-L-C-5-Substituted 2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxy-uridine bearing linker arms</p>

    <p>          Laduree, Daniel, Fossey, Christine, Delbederi, Zoica, Sugeac, Elena, Schmidt, Sylvie, Laumond, Geraldine, and Aubertin, Anne-Marie</p>

    <p>          Journal of Enzyme Inhibition and Medicinal Chemistry <b>2005</b>.  20(6): 533-549</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48341   HIV-LS-339; SCIFINDER-HIV-1/3/2006 </p>

    <p class="memofmt1-2">          Preparation of stable crystals of a 4-oxoquinoline anti-HIV compound</p>

    <p>          Satoh, Motohide, Motomura, Takahisa, Matsuda, Takashi, Kondo, Kentaro, Ando, Koji, Matsuda, Koji, Miyake, Shuji, and Uehara, Hideto</p>

    <p>          PATENT:  WO <b>2005113508</b>  ISSUE DATE:  20051201</p>

    <p>          APPLICATION: 2005  PP: 77 pp.</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48342   HIV-LS-339; SCIFINDER-HIV-1/3/2006</p>

    <p class="memofmt1-2">          Anti-HIV-1 activity of betulinol derivatives</p>

    <p>          Saxena, Brij B, Rathnam, Premila, and Bomshteyn, Arkadiy</p>

    <p>          PATENT:  WO <b>2005112929</b>  ISSUE DATE:  20051201</p>

    <p>          APPLICATION: 2005  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (Cornell Research Foundation, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   48343   HIV-LS-339; WOS-HIV-1/1/2006</p>

    <p class="memofmt1-2">          An antifungal peptide from the coconut</p>

    <p>          Wang, HX and Ng, TB</p>

    <p>          PEPTIDES <b>2005</b>.  26(12): 2392-2396, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233976400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233976400006</a> </p><br />

    <p>12.   48344   HIV-LS-339; SCIFINDER-HIV-1/3/2006</p>

    <p class="memofmt1-2">          Combinations comprising a 4-isoquinolone derivative and anti-HIV agents</p>

    <p>          Matsuzaki, Yuji, Watanabe, Wataru, Ikeda, Satoru, and Kano, Mitsuki</p>

    <p>          PATENT:  WO <b>2005112930</b>  ISSUE DATE:  20051201</p>

    <p>          APPLICATION: 2005  PP: 90 pp.</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   48345   HIV-LS-339; SCIFINDER-HIV-1/3/2006</p>

    <p class="memofmt1-2">          Preparation of 1-heterocyclyl-1,5-dihydropyrido[3,2-b]indol-2-ones and analogs as antiviral agents</p>

    <p>          Kesteleyn, Bart Rudolf Romanie, Raboisson, Pierre Jean-Marie Bernard, Surleraux, Dominique Louis Nestor Ghislain, Hache, Geerwin Yvonne Paul, Vendeville, Sandrine Marie Helene, Peeters, Annick Ann, and Wigerinck, Piet Tom Bert Paul</p>

    <p>          PATENT:  WO <b>2005111047</b>  ISSUE DATE:  20051124</p>

    <p>          APPLICATION: 2005  PP: 100 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48346   HIV-LS-339; SCIFINDER-HIV-1/3/2006</p>

    <p class="memofmt1-2">          Preparation of hydroxy substituted pyrazinopyrrolopyridazine dione derivatives as HIV integrase inhibitors</p>

    <p>          Wai, John S, Vacca, Joseph P, Zhuang, Linghang, Kim, Boyoung, Lyle, Terry A, Wiscount, Catherine M, Egbertson, Melissa S, Neilson, Lou Anne, Embrey, Mark, Fisher, Thorsten E, and Staas, Donnette D</p>

    <p>          PATENT:  WO <b>2005110414</b>  ISSUE DATE:  20051124</p>

    <p>          APPLICATION: 2005  PP: 197 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   48347   HIV-LS-339; SCIFINDER-HIV-1/3/2006</p>

    <p class="memofmt1-2">          Non-nucleoside HIV reverse transcriptase inhibitors: Synthesis and anti-HIV activity of novel 2-[(arylcarbonylmethyl)thio]-6-arylthio DABO analogues</p>

    <p>          Sun, Guang-Fu, Kuang, Yun-Yan, Chen, Fen-Er, De Clercq, Erik, Balzarini, Jan, and Pannecouque, Christophe</p>

    <p>          Archiv der Pharmazie (Weinheim, Germany) <b>2005</b>.  338(10): 457-461</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   48348   HIV-LS-339; WOS-HIV-1/1/2006</p>

    <p class="memofmt1-2">          cycloSal-PMEA and cycloAmb-PMEA: Potentially new phosphonate prodrugs based on the cycloSal-pronucleotide approach</p>

    <p>          Meier, C, Gorbig, U, Muller, C, and Balzarini, J</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(25): 8079-8086, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233923000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233923000018</a> </p><br />

    <p>17.   48349   HIV-LS-339; WOS-HIV-1/1/2006</p>

    <p class="memofmt1-2">          A new class of HIV-1 protease inhibitors containing a tertiary alcohol in the transition-state mimicking scaffold</p>

    <p>          Ekegren, JK, Unge, T, Safa, MZ, Wallberg, H, Samuelsson, B, and Hallberg, A</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(25): 8098-8102, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233923000020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233923000020</a> </p><br />

    <p>18.   48350   HIV-LS-339; SCIFINDER-HIV-1/3/2006</p>

    <p class="memofmt1-2">          Mouse HIV-1 model comprising EcoHIV, a chimeric HIV-1 and ecotropic murine leukemia virus clone capable of infecting murine cells and their use in diagnosis and drug screening for antiviral agents</p>

    <p>          Potash, Mary Jane and Volsky, David J</p>

    <p>          PATENT:  US <b>2005241009</b>  ISSUE DATE:  20051027</p>

    <p>          APPLICATION: 2005-41158  PP: 21 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>19.   48351   HIV-LS-339; SCIFINDER-HIV-1/3/2006</p>

    <p><b>          Preparation of pyridopyrazinediones and pyrimidopyrazinediones as HIV integrase inhibitors</b> </p>

    <p>          Donghi, Monica, Gardelli, Cristina, Jones, Philip, and Summa, Vincenzo</p>

    <p>          PATENT:  WO <b>2005087766</b>  ISSUE DATE:  20050922</p>

    <p>          APPLICATION: 2005  PP: 114 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
