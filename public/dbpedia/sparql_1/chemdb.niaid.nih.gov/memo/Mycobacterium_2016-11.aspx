

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pikb1dOcCKP5eKpgWBk+W0P0Xos9T0I4xpTT8kc/PKcLHMp6Dc1z6sAOX2AvB2lMn20WZjhkBSpvUIbjtfHDRDEyW3C9S4RBA5IjuxbBVDoGa74rx1TV2p00z2Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="42E68B5E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: November, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27497074">Comparative Analysis of New Peptide Conjugates of Antitubercular Drug Candidates-model Membrane and in Vitro Studies.</a> Abraham, A., Z. Baranyai, G. Gyulai, E. Pari, K. Horvati, S. Bosze, and E. Kiss. Colloids and Surfaces. B, Biointerfaces, 2016. 147: p. 106-115. PMID[27497074]. <b><br />
    [PubMed].</b> TB_11_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27301022">Evaluation of the Inhibitory Activity of (aza)Isoindolinone-type Compounds: Toward in Vitro InhA Action, Mycobacterium tuberculosis Growth and Mycolic acid Biosynthesis.</a> Chollet, A., J.L. Stigliani, M.R. Pasca, G. Mori, C. Lherbet, P. Constant, A. Quemard, J. Bernadou, G. Pratviel, and V. Bernardes-Genisson. Chemical Biology &amp; Drug Design, 2016. 88(5): p. 740-755. PMID[27301022]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27180996">Reynosin and Santamarine: Two Sesquiterpene lactones from Ambrosia confertiflora with Bactericidal Activity against Clinical Strains of Mycobacterium tuberculosis.</a> Coronado-Aceves, E.W., C. Velazquez, R.E. Robles-Zepeda, M. Jimenez-Estrada, J. Hernandez-Martinez, J.C. Galvez-Ruiz, and A. Garibay-Escobar. Pharmaceutical Biology, 2016. 54(11): p. 2623-2628. PMID[27180996]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27671498">Identification and Optimization of a New Series of Anti-tubercular Quinazolinones.</a> Couturier, C., C.</p>

    <p class="plaintext">Lair, A. Pellet, A. Upton, T. Kaneko, C. Perron, E. Cogo, J. Menegotto, A. Bauer, B. Scheiper, S. Lagrange, and E. Bacque. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(21): p. 5290-5299. PMID[27671498]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385235300006">Bedaquiline (Situro): A Diarylquinoline That Blocks Tuberculosis ATP Synthase for the Treatment of Multi-drug Resistant Tuberculosis.</a> Cruz, E.N., A.N. Moules, and J.J. Li. Innovative Drug Synthesis, 2016: p. 81-97. ISI[000385235300006].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27564239">Synthesis of Curcumin-loaded Chitosan phosphate Nanoparticle and Study of Its Cytotoxicity and Antimicrobial Activity.</a> Deka, C., L. Aidew, N. Devi, A.K. Buragohain, and D.K. Kakati. Journal of Biomaterials Science. Polymers Edition, 2016. 27(16): p. 1659-1673. PMID[27564239]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27517804">FtsZ Inhibition and Redox Modulation with One Chemical Scaffold: Potential Use of Dihydroquinolines against Mycobacteria.</a> Duggirala, S., J.V. Napoleon, R.P. Nankar, V.S. Adeeba, M.K. Manheri, and M. Doble. European Journal of Medicinal Chemistry, 2016. 123: p. 557-567. PMID[27517804]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27508879">Synthesis and Biological Activity of Furoxan Derivatives against Mycobacterium tuberculosis.</a> Fernandes, G.F., P.C. de Souza, L.B. Marino, K. Chegaev, S. Guglielmo, L. Lazzarato, R. Fruttero, M.C. Chung, F.R. Pavan, and J.L. Dos Santos. European Journal of Medicinal Chemistry, 2016. 123: p. 523-531. PMID[27508879]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27665179">Investigation of C-5 Alkynyl (Alkynyloxy or Hydroxymethyl) and/or N-3 Propynyl Substituted Pyrimidine Nucleoside Analogs as a New Class of Antimicrobial Agents.</a> Garg, S., N. Shakya, N.C. Srivastav, B. Agrawal, D.Y. Kunimoto, and R. Kumar. Bioorganic &amp; Medicinal Chemistry, 2016. 24(21): p. 5521-5533. PMID[27665179]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385338500009">Synthesis of Some Novel Pyrrolyl Hydrazone Derivatives as an Antitubercular Agents.</a> Joshi, S.D., N. Tigadi, D. Kumar, S.R. Dixit, and N.M. Jangade. Indian Journal of Heterocyclic Chemistry, 2016. 25(3-4): p. 237-241. ISI[000385338500009].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000384815600021">Synthesis, Evaluation and Molecular Docking Study of Some New 2-[(2,5-Disubstitutedanilino) phenyl]acetic acid Derivatives.</a> Kerur, S., K. Alagawadi, H.L. Zhu, and F. Manvi. Indian Journal of Pharmaceutical Education and Research, 2016. 50(3): p. 465-471. ISI[000384815600021].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27517813">Amphiphilic Xanthones as a Potent Chemical Entity of Anti-mycobacterial Agents with Membrane-targeting Properties.</a> Koh, J.J., H. Zou, D. Mukherjee, S. Lin, F. Lim, J.K. Tan, D.Z. Tan, B.L. Stocker, M.S. Timmer, H.M. Corkran, R. Lakshminarayanan, D.T. Tan, D. Cao, R.W. Beuerman, T. Dick, and S. Liu. European Journal of Medicinal Chemistry, 2016. 123: p. 684-703. PMID[27517813]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26235917">Bisphosphonic acids as Effective Inhibitors of Mycobacterium tuberculosis Glutamine synthetase.</a> Kosikowska, P., M. Bochno, K. Macegoniuk, G. Forlani, P. Kafarski, and L. Berlicki. Journal of Enzyme Inhibition and Medicinal Chemistry, 2016. 31(6): p. 931-938. PMID[26235917]. <b><br />
    [PubMed]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27457685">Preclinical Comprehensive Physicochemical and Pharmacokinetic Profiling of Novel Nitroimidazole Derivative IIIM-019-A Potential Oral Treatment for Tuberculosis.</a> Kour, G., A. Kumar, P.P. Singh, S. Sharma, A. Bhagat, R.A. Vishwakarma, and Z. Ahmed. Pulmonary Pharmacology &amp; Therapeutics, 2016. 40: p. 44-51. PMID[27457685].</p>

    <p class="plaintext"><b>[PubMed]</b>.TB_11_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26526717">Library of Diversely Substituted 2-(Quinolin-4-yl)imidazolines Delivers Novel Non-cytotoxic Antitubercular Leads.</a> Krasavin, M., P. Mujumdar, V. Parchinsky, T. Vinogradova, O. Manicheva, and M. Dogonadze. Journal of Enzyme Inhibition and Medicinal Chemistry, 2016. 31(6): p. 1146-1155. PMID[26526717].</p>

    <p class="plaintext"><b>[PubMed]</b>.TB_11_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27457409">Multicomponent Petasis-borono Mannich Preparation of Alkylaminophenols and Antimicrobial Activity Studies.</a> Neto, I., J. Andrade, A.S. Fernandes, C.P. Reis, J.K. Salunke, A. Priimagi, N.R. Candeias, and P. Rijo. ChemMedChem, 2016. 11(18): p. 2015-2023. PMID[27457409]. <b><br />
    [PubMed].</b> TB_11_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27667550">Anti-tubercular Activities of 5,6,7,8-Tetrahydrobenzo[4,5]thieno[2,3-d]pyrimidin-4-amine Analogues Endowed with High Activity toward Non-replicative Mycobacterium tuberculosis.</a> Samala, G., P. Brindha Devi, S. Saxena, S. Gunda, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2016. 24(21): p. 5556-5564. PMID[27667550]. <b><br />
    [PubMed].</b> TB_11_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27885861">Additional Synthesis on Thiophene-containing Trisubstituted Methanes (TRSMs) as Inhibitors of M. tuberculosis and 3D-QSAR Studies.</a> Singh, P., T. Saha, P. Mishra, M.K. Parai, S. Ireddy, M.S. Lavanya Kumar, S. Krishna, S.K. Kumar, V. Chaturvedi, S. Sinha, M.I. Siddiqi, and G. Panda. SAR and QSAR in Environmental Research, 2016. 27(11): p. 911-937. PMID[27885861]. <b><br />
    [PubMed].</b> TB_11_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385909600008">In Vitro Antimycobacterial and Antimicrobial Activity of Some New Pyrazoline, Isoxazole and Benzodiazepine Derivatives Containing 1,3,5-Triazine Nucleus via Chalcone Series.</a> Solankee, A., R. Tailor, and K. Kapadia. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2016. 55(10): p. 1277-1287. ISI[000385909600008].</p>

    <p class="plaintext"><b>[WOS]</b>.TB_11_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385201400009">Et3NH HSO4 Catalyzed Efficient Synthesis of 5-Arylidene-rhodanine Conjugates and Their Antitubercular Activity.</a> Subhedar, D.D., M.H. Shaikh, L. Nawale, A. Yeware, D. Sarkar, and B.B. Shingate. Research on Chemical Intermediates, 2016. 42(8): p. 6607-6626. ISI[000385201400009].</p>

    <p class="plaintext"><b>[WOS]</b>.TB_11_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000384322700057">Antimicrobial Activities of Phosphonium Containing Polynorbornenes.</a> Suer, N.C., C. Demir, N.A. Unubol, O. Yalcin, T. Kocagoz, and T. Eren. RSC Advances, 2016. 6(89): p. 86151-86157. ISI[000384322700057].</p>

    <p class="plaintext"><b>[WOS]</b>.TB_11_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385801100018">Microwave-assisted Synthesis and Evaluation of Antibacterial Activity of Novel 6-Fluoroaryl-[1,2,4] triazolo[1,5-a]Pyrimidines.</a> Verbitskiy, E.V., S.A. Baskakova, N.A. Rasputin, N.A. Gerasimova, P.G. Amineva, N.P. Evstigneeva, N.V. Zil&#39;berberg, N.V. Kungurov, M.A. Kravchenko, S.N. Skornyakov, G.L. Rusinov, O.N. Chupakhin, and V.N. Charushin. Arkivoc, 2016: p. 268-278. ISI[000385801100018].</p>

    <p class="plaintext"><b>[WOS]</b>.TB_11_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27613365">Interactions of Linezolid and Second-line Anti-tuberculosis Agents against Multidrug-resistant Mycobacterium tuberculosis in Vitro and in Vivo.</a> Zhao, W., M. Zheng, B. Wang, X. Mu, P. Li, L. Fu, S. Liu, and Z. Guo. International Journal of Infectious Diseases, 2016. 52: p. 23-28. PMID[27613365]. <b><br />
    [PubMed].</b> TB_11_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">24. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161110&amp;CC=WO&amp;NR=2016179231A1&amp;KC=A1">Compositions and Methods for Inhibiting Bacterial Growth.</a> Abramovitch, R., B.K. Johnson, and C.J. Colvin. Patent. 2016. 2016-US30689 2016179231: 151pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_11_2016.</p>

    <br />

    <p class="plaintext">25. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160204&amp;CC=US&amp;NR=2016031874A1&amp;KC=A1">Substituted Aminothiazoles for the Treatment of Tuberculosis.</a> Hung, D., M. Serrano-Wu, S. Grant, and T. Kawate. Patent. 2016. 2015-14776521 20160031874.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_11_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
