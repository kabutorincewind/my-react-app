

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-352.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="O07D6wta2IrIGLl7uKQUP+YEn4NLtRsXg5Oe7OpdpolBws9lGAUxUkR/U16jIcuGs1czoeCORTZxN8+NPoCEqxwuj9mbd69ugdXFniaKQV+Vp2Wl5X7SjEBc8nA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D3CD41F7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-352-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49104   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Overcoming HIV-1 resistance to protease inhibitors</p>

    <p>          Freire, Ernesto</p>

    <p>          Drug Discovery Today: Disease Mechanisms <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75D7-4KB1175-3/2/26272115dd5c35ab6feeb8e4ae5dcad0">http://www.sciencedirect.com/science/article/B75D7-4KB1175-3/2/26272115dd5c35ab6feeb8e4ae5dcad0</a> </p><br />

    <p>2.     49105   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Prevalence of antiretroviral drug resistance mutations and HIV-1 non-B subtypes in newly diagnosed drug-naIve patients in Slovenia, 2000-2004</p>

    <p>          Babic, Dunja Z, Zelnikar, Mojca, Seme, Katja, Vandamme, Anne-Mieke, Snoeck, Joke, Tomazic, Janez, Vidmar, Ludvik, Karner, Primoz, and Poljak, Mario</p>

    <p>          Virus Research  <b>2006</b>.  118(1-2): 156-163</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4J2M44H-4/2/de9e94bc40a28e1ab5586c5c7d503771">http://www.sciencedirect.com/science/article/B6T32-4J2M44H-4/2/de9e94bc40a28e1ab5586c5c7d503771</a> </p><br />

    <p>3.     49106   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          A novel real-time PCR assay to determine relative replication capacity for HIV-1 protease variants and/or reverse transcriptase variants</p>

    <p>          van Maarseveen, NM, Huigen, MCDG, de Jong, D, Smits, AM, Boucher, CAB, and Nijhuis, M</p>

    <p>          Journal of Virological Methods <b>2006</b>.  133(2): 185-194</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T96-4HVDJ91-1/2/fae8f6fa3f05de920698c6f2c0695334">http://www.sciencedirect.com/science/article/B6T96-4HVDJ91-1/2/fae8f6fa3f05de920698c6f2c0695334</a> </p><br />

    <p>4.     49107   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          4-Substituted anilino imidazo[1,2-a] and triazolo[4,3-a]quinoxalines. Synthesis and evaluation of in vitro biological activity</p>

    <p>          Corona, Paola, Vitale, Gabriella, Loriga, Mario, Paglietti, Giuseppe, La Colla, Paolo, Collu, Gabriella, Sanna, Giuseppina, and Loddo, Roberta</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4KBX4B8-1/2/c42f411e57268d620ff36d30da9f475f">http://www.sciencedirect.com/science/article/B6VKY-4KBX4B8-1/2/c42f411e57268d620ff36d30da9f475f</a> </p><br />

    <p>5.     49108   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Antiviral effects of mifepristone on human immunodeficiency virus type-1 (HIV-1): Targeting Vpr and its cellular partner, the glucocorticoid receptor (GR)</p>

    <p>          Schafer, Elizabeth A, Venkatachari, Narasimhan J, and Ayyavoo, Velpandi</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4KBWXH7-2/2/9203c508763eceafbe5cc7b1058c501d">http://www.sciencedirect.com/science/article/B6T2H-4KBWXH7-2/2/9203c508763eceafbe5cc7b1058c501d</a> </p><br />
    <br clear="all">

    <p>6.     49109   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Discovery of imidazolidine-2,4-dione-linked HIV protease inhibitors with activity against lopinavir-resistant mutant HIV</p>

    <p>          Flosi, William J, DeGoey, David A, Grampovnik, David J, Chen, Hui-ju, Klein, Larry L, Dekhtyar, Tatyana, Masse, Sherie, Marsh, Kennan C, Mo, Hong Mei, and Kempf, Dale</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4KBX4G8-3/2/5ae513f74cd66519690145860afb5289">http://www.sciencedirect.com/science/article/B6TF8-4KBX4G8-3/2/5ae513f74cd66519690145860afb5289</a> </p><br />

    <p>7.     49110   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Identification of a d-amino acid decapeptide HIV-1 entry inhibitor</p>

    <p>          Boggiano, Cesar, Jiang, Shibo, Lu, Hong, Zhao, Qian, Liu, Shuwen, Binley, James, and Blondelle, Sylvie E</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4KBF19D-8/2/c2752e9f70404bbca7fa1ab5254370be">http://www.sciencedirect.com/science/article/B6WBK-4KBF19D-8/2/c2752e9f70404bbca7fa1ab5254370be</a> </p><br />

    <p>8.     49111   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Tri-substituted triazoles as potent non-nucleoside inhibitors of the HIV-1 reverse transcriptase</p>

    <p>          De La Rosa, Martha, Kim, Hong Woo, Gunic, Esmir, Jenket, Cheryl, Boyle, Uyen, Koh, Yung-hyo, Korboukh, Ilia, Allan, Matthew, Zhang, Weijian, and Chen, Huanming</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4K8S5NP-F/2/6baafd13564c7485ab9a949210e449ee">http://www.sciencedirect.com/science/article/B6TF9-4K8S5NP-F/2/6baafd13564c7485ab9a949210e449ee</a> </p><br />

    <p>9.     49112   HIV-LS-352; EMBASE-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Polycyclic peptide and glycopeptide antibiotics and their derivatives as inhibitors of HIV entry</p>

    <p>          Preobrazhenskaya, Maria N and Olsufyeva, Eugenia N</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JWMSCF-1/2/cec7dc50c0c473bdce99d14d85cfbc69">http://www.sciencedirect.com/science/article/B6T2H-4JWMSCF-1/2/cec7dc50c0c473bdce99d14d85cfbc69</a> </p><br />

    <p>10.   49113   HIV-LS-352; WOS-HIV-7/9/2006</p>

    <p class="memofmt1-2">          Synthesis and biological acitivity of 3-aryloxy-1-propylamine derivatives as HIV-1 Tat/PCAF BRD inhibitor</p>

    <p>          Li, JM, Wang, ZY, Zeng, L, and Zhou, MM</p>

    <p>          ACTA CHIMICA SINICA: Acta Chim. Sin <b>2006</b>.  64(11): 1151-1156, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238321400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238321400010</a> </p><br />

    <p>11.   49114   HIV-LS-352; WOS-HIV-7/9/2006</p>

    <p class="memofmt1-2">          Design, synthesis and structure-activity relationship studies of novel 4,4-disubstituted piperidine based CCR5 antagonists as anti-HIV-1 agents</p>

    <p>          Peckham, J, Anderson, D, Aquino, C, Bifulco, N, Boone, L, Chong, P, Duan, M, Ferris, R, Kazmierski, W, Kenakin, T, Lang, D, McLean, E, Svolto, A, Wheelan, P, and Youngman, M</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY: Abstr. Pap. Am. Chem. Soc <b>2005</b>.  230: U2540-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305057</a> </p><br />
    <br clear="all">

    <p>12.   49115   HIV-LS-352; WOS-HIV-7/2/2006</p>

    <p class="memofmt1-2">          Unique chlorine effect in regioselective one-pot synthesis of 1-alkyl-/allyl-3-(o-chlorobenzyl) uracils: anti-HIV activity of selected uracil derivatives</p>

    <p>          Malik, V, Singh, P, and Kumar, S</p>

    <p>          TETRAHEDRON: Tetrahedron <b>2006</b>.  62(25): 5944-5951, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238171600013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238171600013</a> </p><br />

    <p>13.   49116   HIV-LS-352; WOS-HIV-7/2/2006</p>

    <p class="memofmt1-2">          Modulators of the human CCR5 receptor. Part 3: SAR of substituted 1-[3-(4-methanesulfonylphenyl)-3-phenylpropyl]-piperidinyl phenyl acetamides</p>

    <p>          Cumming, JG, Brown, SJ, Cooper, AE, Faull, AW, Flynn, AP, Grime, K, Oldfield, J, Shaw, JS, Shepherd, E, Tucker, H, and Whittaker, D</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS: Bioorg. Med. Chem. Lett <b>2006</b>.  16(13): 3533-3536, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238182700038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238182700038</a> </p><br />

    <p>14.   49117   HIV-LS-352; WOS-HIV-7/9/2006</p>

    <p class="memofmt1-2">          An increase in viral replicative capacity drives the evolution of protease inhibitor-resistant human immunodeficiency virus type 1 in the absence of drugs</p>

    <p>          van Maarseveen, NM, de Jong, D, Boucher, CAB, and Nijhuis, M</p>

    <p>          JAIDS-JOURNAL OF ACQUIRED IMMUNE DEFICIENCY SYNDROMES: JAIDS <b>2006</b>.  42(2): 162-168, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237910700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237910700005</a> </p><br />

    <p>15.   49118   HIV-LS-352; WOS-HIV-7/9/2006</p>

    <p class="memofmt1-2">          Targeting retroviral Zn finger-DNA interactions: A small-molecule approach using the electrophilic nature of trans-platinum-nucleobase compounds</p>

    <p>          Anzellotti, AI, Liu, Q, Bloemink, MJ, Scarsdale, JN, and Farrell, N</p>

    <p>          CHEMISTRY &amp; BIOLOGY: Chem. Biol <b>2006</b>.  13(5): 539-548, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237988600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237988600012</a> </p><br />

    <p>16.   49119   HIV-LS-352; WOS-HIV-7/2/2006</p>

    <p class="memofmt1-2">          Modulation of human immunodeficiency virus type 1 synergistic inhibition by reverse transcriptase mutations</p>

    <p>          Basavapathruni, A, Vingerhoets, J, de Bethune, MP, Chung, R, Bailey, CM, Kim, J, and Anderson, KS</p>

    <p>          BIOCHEMISTRY: Biochemistry <b>2006</b>.  45(23): 7334-7340, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238047600033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238047600033</a> </p><br />

    <p>17.   49120   HIV-LS-352; WOS-HIV-7/9/2006</p>

    <p class="memofmt1-2">          Single-dose safety and pharmacokinetics of brecanavir, a novel human immunodeficiency virus protease inhibitor</p>

    <p>          Ford, SL, Reddy, YS, Anderson, MT, Murray, SC, Fernandez, P, Stein, DS, and Johnson, MA</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY: Antimicrob. Agents Chemother <b>2006</b>.  50( 6): 2201-2206, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237932200040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237932200040</a> </p><br />
    <br clear="all">

    <p>18.   49121   HIV-LS-352; WOS-HIV-7/2/2006</p>

    <p class="memofmt1-2">          Synergistic in vitro antiretroviral activity of a humanized monoclonal anti-CD4 antibody (TNX-355) and enfuvirtide (T-20)</p>

    <p>          Zhang, XQ, Sorensen, M, Fung, M, and Schooley, RT</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY: Antimicrob. Agents Chemother <b>2006</b>.  50( 6): 2231-2233, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237932200048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237932200048</a> </p><br />

    <p>19.   49122   HIV-LS-352; WOS-HIV-7/9/2006</p>

    <p class="memofmt1-2">          Suppression of HIV-1 replication by a combination of endonucleolytic ribozymes (RNase P and tRNase ZL)</p>

    <p>          Ikeda, M, Habu, Y, Miyano-Kurosaki, N, and Takaku, H</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS: Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(4-6): 427-437, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237847700007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237847700007</a> </p><br />

    <p>20.   49123   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV Env binding to cellular receptors by monoclonal antibody 2G12 as probed by Fc-tagged gp120</p>

    <p>          Binley, JM, Ngo-Abdalla, S, Moore, P, Bobardt, M, Chatterji, U, Gallay, P, Burton, DR, Wilson, IA, Elder, JH, and de, Parseval A</p>

    <p>          Retrovirology <b>2006</b>.  3(1): 39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16817962&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16817962&amp;dopt=abstract</a> </p><br />

    <p>21.   49124   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Human immunodeficiency virus-encoded Tat activates glycogen synthase kinase-3beta to antagonize nuclear factor-kappaB survival pathway in neurons</p>

    <p>          Sui, Z, Sniderhan, LF, Fan, S, Kazmierczak, K, Reisinger, E, Kovacs, AD, Potash, MJ, Dewhurst, S, Gelbard, HA, and Maggirwar, SB</p>

    <p>          Eur J Neurosci  <b>2006</b>.  23(10): 2623-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16817865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16817865&amp;dopt=abstract</a> </p><br />

    <p>22.   49125   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Cyclophilin, TRIM5, and innate immunity to HIV-1</p>

    <p>          Sokolskaja, E and Luban, J</p>

    <p>          Curr Opin Microbiol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16815734&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16815734&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   49126   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Hypersusceptibility to substrate analogs conferred by mutations in human immunodeficiency virus type 1 reverse transcriptase</p>

    <p>          Smith, RA, Anderson, DJ, and Preston, BD</p>

    <p>          J Virol <b>2006</b>.  80(14): 7169-78</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809322&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809322&amp;dopt=abstract</a> </p><br />

    <p>24.   49127   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Dihydroxythiophenes are novel potent inhibitors of human immunodeficiency virus integrase with a diketo Acid-like pharmacophore</p>

    <p>          Kehlenbeck, S, Betz, U, Birkmann, A, Fast, B, Goller, AH, Henninger, K, Lowinger, T, Marrero, D, Paessens, A, Paulsen, D, Pevzner, V, Schohe-Loop, R, Tsujishita, H, Welker, R, Kreuter, J, Rubsamen-Waigmann, H, and Dittmer, F</p>

    <p>          J Virol <b>2006</b>.  80(14): 6883-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809294&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809294&amp;dopt=abstract</a> </p><br />

    <p>25.   49128   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Removal of Arginine 332 Allows Human TRIM5{alpha} To Bind Human Immunodeficiency Virus Capsids and To Restrict Infection</p>

    <p>          Li, Y, Li, X, Stremlau, M, Lee, M, and Sodroski, J</p>

    <p>          J Virol <b>2006</b>.  80(14): 6738-44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809279&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809279&amp;dopt=abstract</a> </p><br />

    <p>26.   49129   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          An Efficient, Stereoselective Synthesis of the Hydroxyethylene Dipeptide Isostere Core for the HIV Protease Inhibitor A-792611</p>

    <p>          Engstrom, K, Henry, R, Hollis, LS, Kotecki, B, Marsden, I, Pu, YM, Wagaw, S, and Wang, W</p>

    <p>          J Org Chem <b>2006</b>.  71(14): 5369-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16808529&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16808529&amp;dopt=abstract</a> </p><br />

    <p>27.   49130   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Differential susceptibility of HIV-1 reverse transcriptase to inhibition by RNA aptamers in enzymatic reactions monitoring specific steps during genome replication</p>

    <p>          Held, DM, Kissel, JD, Saran, D, Michalowski, D, and Burke, DH</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16798747&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16798747&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   49131   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Potent inhibition of HIV-1 gene expression and TAT-mediated apoptosis in human T cells by novel mono- and multitarget anti-TAT/Rev/Env ribozymes and a general purpose RNA-cleaving DNA-enzyme</p>

    <p>          Unwalla, H, Chakraborti, S, Sood, V, Gupta, N, and Banerjea, AC</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16790281&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16790281&amp;dopt=abstract</a> </p><br />

    <p>29.   49132   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p><b>          Mode of inhibition of HIV-1 Integrase by a C-terminal domain-specific monoclonal antibody*</b> </p>

    <p>          Ramcharan, J, Colleluori, DM, Merkel, G, Andrake, MD, and Skalka, AM</p>

    <p>          Retrovirology <b>2006</b>.  3(1): 34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16790058&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16790058&amp;dopt=abstract</a> </p><br />

    <p>30.   49133   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Discovery of HIV-1 integrase inhibitors through a novel combination of ligand and structure-based drug design</p>

    <p>          Brigo, A, Mustata, GI, Briggs, JM, and Moro, S</p>

    <p>          Med Chem <b>2005</b>.  1(3): 263-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787322&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787322&amp;dopt=abstract</a> </p><br />

    <p>31.   49134   HIV-LS-352; PUBMED-HIV-7/10/2006</p>

    <p class="memofmt1-2">          Inhibition of early steps of HIV-1 replication by SNF5/Ini1</p>

    <p>          Maroun, M, Delelis, O, Coadou, G, Bader, T, Segeral, E, Mbemba, G, Petit, C, Sonigo, P, Rain, JC, Mouscadet, JF, Benarous, R, and Emiliani, S</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16772295&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16772295&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
