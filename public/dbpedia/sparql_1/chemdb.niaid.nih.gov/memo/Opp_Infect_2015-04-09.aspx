

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-04-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="02UfCaks0Dc1m5yfAMQXXowPRYo9zDshurn1uchRl2jHfqJYDEG85DaA3QSeBhdJvaGQeYIee5DdLPxMvyAnUzE/PF4vyhpWiBx7l/ZBqArwyCpIpyJksVrZKOc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE6C2485" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 27 - April 9, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25579799">Synthesis, Spectroscopic, Thermal and Antimicrobial Studies of Neodymium(III) and Samarium(III) Complexes Derived from Tetradentate Ligands Containing N and S Donor Atoms.</a> Ain, Q., S.K. Pandey, O.P. Pandey, and S.K. Sengupta. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2015. 140: p. 27-34. PMID[25579799].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25576097">Antifungal Activity of the Essential Oil of Angelica major against Candida, Cryptococcus, Aspergillus and Dermatophyte Species.</a> Cavaleiro, C., L. Salgueiro, M.J. Goncalves, K. Hrimpeng, J. Pinto, and E. Pinto. Journal of Natural Medicines, 2015. 69(2): p. 241-248. PMID[25576097].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25773477">Antimicrobial and Antiquorum-sensing Studies. Part 3: Synthesis and Biological Evaluation of New Series of [1,3,4]Thiadiazoles and Fused [1,3,4]Thiadiazoles.</a> El-Gohary, N.S. and M.I. Shaaban. Archiv der Pharmazie, 2015. 348(4): p. 283-297. PMID[25773477].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25829609">Antimicrobial Activity of Turmeric Extract and its Potential use in Food Industry.</a> Gul, P. and J. Bakht. Journal of Food Science and Technology, 2015. 52(4): p. 2272-2279. PMID[25829609].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25740636">Synthesis and Biological Evaluation of Fluconazole Analogs with Triazole-modified Scaffold as Potent Antifungal Agents.</a> Hashemi, S.M., H. Badali, H. Irannejad, M. Shokrzadeh, and S. Emami. Bioorganic &amp; Medicinal Chemistry, 2015. 23(7): p. 1481-1491. PMID[25740636].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25663372">Clinical Isolates of Candida albicans, Candida tropicalis, and Candida krusei have Different Susceptibilities to Co(II) and Cu(II) Complexes of 1,10-Phenanthroline.</a> Hoffman, A.E., L. Miles, T.J. Greenfield, C. Shoen, M. DeStefano, M. Cynamon, and R.P. Doyle. Biometals, 2015. 28(2): p. 415-423. PMID[25663372].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25849802">Synthesis, Characterization, Crystal Structure and Antimicrobial Activity of Copper(II) Complexes with the Schiff Base Derived from 2-Hydroxy-4-methoxybenzaldehyde.</a> Pahontu, E., D.C. Ilies, S. Shova, C. Paraschivescu, M. Badea, A. Gulea, and T. Rosu. Molecules, 2015. 20(4): p. 5771-5792. PMID[25849802].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25708540">Antibacterial, Antifungal and in Vitro Antileukaemia Activity of Metal Complexes with Thiosemicarbazones.</a> Pahontu, E., F. Julea, T. Rosu, V. Purcarea, Y. Chumakov, P. Petrenco, and A. Gulea. Journal of  Cellular and Molecular Medicine, 2015. 19(4): p. 865-878. PMID[25708540].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25565139">In Vitro Evaluation of the Acquisition of Resistance, Antifungal Activity and Synergism of Brazilian Red Propolis with Antifungal Drugs on Candida Spp.</a> Pippi, B., A.J. Lana, R.C. Moraes, C.M. Guez, M. Machado, L.F. de Oliveira, G. Lino von Poser, and A.M. Fuentefria. Journal of Applied Microbiology, 2015. 118(4): p. 839-850. PMID[25565139].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25829603">Efficacy of Angelica archangelica Essential Oil, Phenyl ethyl Alcohol and alpha-Terpineol against Isolated Molds from Walnut and their Antiaflatoxigenic and Antioxidant Activity.</a> Prakash, B., P. Singh, R. Goni, A.K. Raina, and N.K. Dubey. Journal of Food Science and Technology, 2015. 52(4): p. 2220-2228. PMID[25829603].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25829172">Synthesis of Silver Nanoparticles by Phoma gardeniae and in Vitro Evaluation of their Efficacy against Human Disease-causing Bacteria and Fungi.</a> Rai, M., A.P. Ingle, A. Gade, and N. Duran. IET Nanobiotechnology, 2015. 9(2): p. 71-75. PMID[25829172].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25615679">Assessment of Conformational, Spectral, Antimicrobial Activity, Chemical Reactivity and NLO Application of Pyrrole-2,5-dicarboxaldehyde bis(oxaloyldihydrazone).</a> Rawat, P. and R.N. Singh. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2015. 140: p. 344-355. PMID[25615679].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25631674">Beta-Lactam Antibiotics and Vancomycin Inhibit the Growth of Planktonic and Biofilm Candida spp.: An Additional Benefit of Antibiotic-lock Therapy.</a> Sidrim, J.J., C.E. Teixeira, R.A. Cordeiro, R.S. Brilhante, D.S. Castelo-Branco, S.P. Bandeira, L.P. Alencar, J.S. Oliveira, A.J. Monteiro, J.L. Moreira, T.J. Bandeira, and M.F. Rocha. International Journal of Antimicrobial Agents, 2015. 45(4): p. 420-423. PMID[25631674].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25558078">In Vitro Antifungal Activity and in Vivo Antibiofilm Activity of Cerium nitrate against Candida Species.</a> Silva-Dias, A., I.M. Miranda, J. Branco, L. Cobrado, M. Monteiro-Soares, C. Pina-Vaz, and A.G. Rodrigues. Journal of Antimicrobial Chemotherapy, 2015. 70(4): p. 1083-1093. PMID[25558078].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25848272">Biosynthesis, Characterization, and Antimicrobial Applications of Silver Nanoparticles.</a> Singh, P., Y.J. Kim, H. Singh, C. Wang, K.H. Hwang, A. Farh Mel, and D.C. Yang. International Journal of Nanomedicine, 2015. 10: p. 2567-2577. PMID[25848272].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25829632">Comparison of Essential Oil Composition and Antimicrobial Activity of Coriandrum sativum L. Extracted by Hydrodistillation and Microwave-assisted Hydrodistillation.</a> Sourmaghi, M.H., G. Kiaee, F. Golfakhrabadi, H. Jamalifar, and M. Khanavi. Journal of Food Science and Technology, 2015. 52(4): p. 2452-2457. PMID[25829632].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25641229">Synergistic Activity of Magnolol with Azoles and its Possible Antifungal Mechanism against Candida albicans.</a> Sun, L.M., K. Liao, S. Liang, P.H. Yu, and D.Y. Wang. Journal of Applied Microbiology, 2015. 118(4): p. 826-838. PMID[25641229].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25765910">Rapid &#39;One-Pot&#39; Synthesis of a Novel Benzimidazole-5-carboxylate and its Hydrazone Derivatives as Potential Anti-inflammatory and Antimicrobial Agents.</a> Vasantha, K., G. Basavarajaswamy, M. Vaishali Rai, P. Boja, V.R. Pai, N. Shruthi, and M. Bhat. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1420-1426. PMID[25765910].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25638418">Antitubercular Activity of Ru (II) Isoniazid Complexes.</a> Aguiar, I., A. Tavares, A.C. Roveda, Jr., A.C. da Silva, L.B. Marino, E.O. Lopes, F.R. Pavan, L.G. Lopes, and D.W. Franco. European Journal of Pharmaceutical Sciences, 2015. 70: p. 45-54. PMID[25638418].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25583730">Mycobacterium tuberculosis Gyrase Inhibitors as a New Class of Antitubercular Drugs.</a> Blanco, D., E. Perez-Herran, M. Cacho, L. Ballell, J. Castro, R. Gonzalez Del Rio, J.L. Lavandera, M.J. Remuinan, C. Richards, J. Rullas, M.J. Vazquez-Muniz, E. Woldu, M.C. Zapatero-Gonzalez, I. Angulo-Barturen, A. Mendoza, and D. Barros. Antimicrobial Agents and Chemotherapy, 2015. 59(4): p. 1868-1875. PMID[25583730].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25770781">Nanomolar Inhibitors of Mycobacterium tuberculosis Glutamine Synthetase 1: Synthesis, Biological Evaluation and X-ray Crystallographic Studies.</a> Couturier, C., S. Silve, R. Morales, B. Pessegue, S. Llopart, A. Nair, A. Bauer, B. Scheiper, C. Poverlein, A. Ganzhorn, S. Lagrange, and E. Bacque. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1455-1459. PMID[25770781].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24939095">3-Benzylidene-4-chromanones: A Novel Cluster of Anti-tubercular Agents.</a> Das, U., T. Lorand, S.G. Dimmock, P. Perjesi, and J.R. Dimmock. Journal of Enzyme Inhibition and Medicinal Chemistry, 2015. 30(2): p. 259-263. PMID[24939095].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25686372">Compounds Targeting Disulfide Bond Forming Enzyme DsbB of Gram-negative Bacteria.</a> Landeta, C., J.L. Blazyk, F. Hatahet, B.M. Meehan, M. Eser, A. Myrick, L. Bronstain, S. Minami, H. Arnold, N. Ke, E.J. Rubin, B.C. Furie, B. Furie, J. Beckwith, R. Dutton, and D. Boyd. Nature Chemical Biology, 2015. 11(4): p. 292-298. PMID[25686372].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25754492">Synthesis and Antitubercular Evaluation of 4-Carbonyl Piperazine Substituted 1,3-Benzothiazin-4-one Derivatives.</a> Peng, C.T., C. Gao, N.Y. Wang, X.Y. You, L.D. Zhang, Y.X. Zhu, Y. Xv, W.Q. Zuo, K. Ran, H.X. Deng, Q. Lei, K.J. Xiao, and L.T. Yu. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1373-1376. PMID[25754492].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25765907">Synthesis, in Vitro Anticancer and Antimycobacterial Evaluation of New 5-(2,5-Dimethoxyphenyl)-1,3,4-thiadiazole-2-amino Derivatives.</a> Polkam, N., P. Rayam, J.S. Anireddy, S. Yennam, H.S. Anantaraju, S. Dharmarajan, Y. Perumal, S.S. Kotapalli, R. Ummanni, and S. Balasubramanian. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1398-1402. PMID[25765907].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25766629">Development of 2-Amino-5-phenylthiophene-3-carboxamide Derivatives as Novel Inhibitors of Mycobacterium tuberculosis DNA GyrB Domain.</a> Saxena, S., G. Samala, J. Renuka, J.P. Sridevi, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2015. 23(7): p. 1402-1412. PMID[25766629].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25646964">Enhancement of Antibiotic Activity against Multidrug-resistant Bacteria by the Efflux Pump Inhibitor 3,4-Dibromopyrrole-2,5-dione Isolated from a Pseudoalteromonas sp.</a> Whalen, K.E., K.L. Poulson-Ellestad, R.W. Deering, D.C. Rowley, and T.J. Mincer. Journal of Natural Products, 2015. 78(3): p. 402-412. PMID[25646964].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24697298">Biologically Active Carbazole Derivatives: Focus on Oxazinocarbazoles and Related Compounds.</a> Bouaziz, Z., S. Issa, J. Gentili, A. Gratz, A. Bollacke, M. Kassack, J. Jose, L. Herfindal, G. Gausdal, S.O. Doskeland, C. Mullie, P. Sonnet, C. Desgrouas, N. Taudon, G. Valdameri, A. Di Pietro, M. Baitiche, and M. Le Borgne. Journal of Enzyme Inhibition and Medicinal Chemistry, 2015. 30(2): p. 180-188. PMID[24697298].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25725608">N10,N11-di-Alkylamine indolo[3,2-b]quinolines as Hemozoin Inhibitors: Design, Synthesis and Antiplasmodial Activity.</a> Figueiras, M., L. Coelho, K.J. Wicht, S.A. Santos, J. Lavrado, J. Gut, P.J. Rosenthal, F. Nogueira, T.J. Egan, R. Moreira, and A. Paulo. Bioorganic &amp; Medicinal Chemistry, 2015. 23(7): p. 1530-1539. PMID[25725608].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25823686">Triaminopyrimidine is a Fast-killing and Long-acting Antimalarial Clinical Candidate.</a> Hameed, P.S., S. Solapure, V. Patil, P.P. Henrich, P.A. Magistrado, S. Bharath, K. Murugan, P. Viswanath, J. Puttur, A. Srivastava, E. Bellale, V. Panduga, G. Shanbag, D. Awasthy, S. Landge, S. Morayya, K. Koushik, R. Saralaya, A. Raichurkar, N. Rautela, N. Roy Choudhury, A. Ambady, R. Nandishaiah, J. Reddy, K.R. Prabhakar, S. Menasinakai, S. Rudrapatna, M. Chatterji, M.B. Jimenez-Diaz, M.S. Martinez, L.M. Sanz, O. Coburn-Flynn, D.A. Fidock, A.K. Lukens, D.F. Wirth, B. Bandodkar, K. Mukherjee, R.E. McLaughlin, D. Waterson, L. Rosenbrier-Ribeiro, K. Hickling, V. Balasubramanian, P. Warner, V. Hosagrahara, A. Dudley, P.S. Iyer, S. Narayanan, S. Kavanagh, and V.K. Sambandamurthy. Nature Communications, 2015. 6(6715): 11pp. PMID[25823686].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25583729">Inhibition of Protein Synthesis and Malaria Parasite Development by Drug Targeting of Methionyl-tRNA Synthetases.</a> Hussain, T., M. Yogavel, and A. Sharma. Antimicrobial Agents and Chemotherapy, 2015. 59(4): p. 1856-1867. PMID[25583729].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25699574">Ribifolin, an Orbitide from Jatropha ribifolia, and its Potential Antimalarial Activity.</a> Pinto, M.E., J.M. Batista, Jr., J. Koehbach, P. Gaur, A. Sharma, M. Nakabashi, E.M. Cilli, G.M. Giesel, H. Verli, C.W. Gruber, E.W. Blanch, J.F. Tavares, M.S. Silva, C.R. Garcia, and V.S. Bolzani. Journal of Natural Products, 2015. 78(3): p. 374-380. PMID[25699574].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25653029">Antiplasmodial Activity of Eco-friendly Synthesized Palladium Nanoparticles using Eclipta prostrata Extract against Plasmodium berghei in Swiss Albino Mice.</a> Rajakumar, G., A.A. Rahuman, I.M. Chung, A.V. Kirthi, S. Marimuthu, and K. Anbarasan. Parasitology Research, 2015. 114(4): p. 1397-1406. PMID[25653029].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25746816">Synthesis of 3-Azabicyclo[3.2.2]nonanes and their Antiprotozoal Activities.</a> Seebacher, W., V. Wolkinger, J. Faist, M. Kaiser, R. Brun, R. Saf, F. Bucar, B. Groblacher, A. Brantner, V. Merino, Y. Kalia, L. Scapozza, R. Perozzo, and R. Weis. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(7): p. 1390-1393. PMID[25746816].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25780435">Blockade of Erythrocyte Invasion: New Assessment of Anti-reticulocyte-binding Protein Homolog 5 Antibodies.</a> Shen, Y., J. Wang, X. Liu, J. Liang, Y. Huang, Z. Liu, Y.A. Zhao, and Y. Li. Experimental and Therapeutic Medicine, 2015. 9(4): p. 1357-1362. PMID[25780435].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0327-040915.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350265300013">Antioxidant, Antimicrobial, Antitumor, and Cytotoxic Activities of an Important Medicinal Plant (Euphorbia royleana) from Pakistan.</a> Ashraf, A., R.A. Sarfraz, M.A. Rashid, and M. Shahid. Journal of Food and Drug Analysis, 2015. 23(1): p. 109-115. ISI[000350265300013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350314800005">Nitric Oxide Production, Inhibitory, Antioxidant and Antimycobacterial Activities of the Fruits Extract and Flavonoid Content of Schinus terebinthifolius.</a> Bernardes, N.R., M. Heggdorne-Araujo, I. Borges, F.M. Almeida, E.P. Amaral, E.B. Lasunskaia, M.F. Muzitano, and D.B. Oliveira. Revista Brasileira de Farmacognosia, 2014. 24(6): p. 644-650. ISI[000350314800005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349812000044">Essential Oil Composition and Antimicrobial Activity of Vismia macrophylla Leaves and Fruits Collected in Tachira-Venezuela.</a> Buitrago, A., J. Rojas, L. Rojas, J. Velasco, A. Morales, Y. Penaloza, and C. Diaz. Natural Product Communications, 2015. 10(2): p. 375-377. ISI[000349812000044].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350475300008">Design of Novel Mycobacterium tuberculosis Pantothenate Synthetase Inhibitors: Virtual Screening, Synthesis and in Vitro Biological Activities.</a> Devi, P.B., S. Jogula, A.P. Reddy, S. Saxena, J.P. Sridevi, D. Sriram, and P. Yogeeswari. Molecular Informatics, 2015. 34(2-3): p. 147-159. ISI[000350475300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350222400034">DNA Interaction, Cytotoxicity, Antibacterial and Antituberculosis Activity of Oxovanadium(IV) Complexes Derived from Fluoroquinolones and 4-Hydroxy-5-((4-hydroxyphenyl)diazenyl)thiazole-2(3H)-thione.</a> Gajera, S.B., J.V. Mehta, and M.N. Patel. RSC Advances, 2015. 5(28): p. 21710-21719. ISI[000350222400034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350275000003">In Vitro Antimicrobial and Antioxidant Evaluation of Rare Earth Metal Schiff Base Complexes Derived from Threonine.</a> Logu, L., K.R. Kamatchi, H. Rajmohan, S. Manohar, R. Gurusamy, and E. Deivanayagam. Applied Organometallic Chemistry, 2015. 29(2): p. 90-95. ISI[000350275000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350192000016">In Vitro and in Vivo Antimicrobial Evaluation of Alkaloidal Extracts of Enantia chlorantha Stem Bark and their Formulated Ointments.</a> Nyong, E.E., M.A. Odeniyi, and J.O. Moody. Acta Poloniae Pharmaceutica, 2015. 72(1): p. 147-152. ISI[000350192000016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350261300003">Chemical Composition, Antifungal and Antibacterial Activities of Essential Oil from Lallemantia royleana (Benth. in Wall.) Benth.</a> Sharifi-Rad, J., S.M. Hoseini-Alfatemi, M. Sharifi-Rad, and W.N. Setzer. Journal of Food Safety, 2015. 35(1): p. 19-25. ISI[000350261300003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350143000026">New 5-modified 2&#39;-Deoxyuridine Derivatives: Synthesis and Antituberculosis Activity.</a> Shmalenyuk, E.R., I.L. Karpenko, L.N. Chernousova, A.O. Chizhov, T.G. Smirnova, S.N. Andreevskaya, and L.A. Alexandrova. Russian Chemical Bulletin, 2014. 63(5): p. 1197-1200. ISI[000350143000026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350028600003">Antimicrobial Compounds from Seaweeds-associated Bacteria and Fungi.</a> Singh, R.P., P. Kumari, and C.R.K. Reddy. Applied Microbiology and Biotechnology, 2015. 99(4): p. 1571-1586. ISI[000350028600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350606100002">Self-assembled Amphotericin B-loaded Polyglutamic acid Nanoparticles: Preparation, Characterization and in Vitro Potential against Candida albicans.</a> Zia, Q., A.A. Khan, Z. Swaleha, and M. Owais. International Journal of Nanomedicine, 2015. 10: p. 1769-1790. ISI[000350606100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0327-040915.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
