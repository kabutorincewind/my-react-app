

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-383.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zidGE6GxryU5fxeq/lQWT8DZW1AksOy+PuVrrAQ5F1ISP3QEV5f57QJ+PNMuq02vrt+QAOPcllrlUmQodNuooUBrYtI1PFR6qENXWLWKq4PM/LYy8JK1M2KM/MU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BA7F178E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-383-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61638   HIV-LS-383; WOS-HIV-9/7/2007</p>

    <p class="memofmt1-2">          Synthesis of 2-Benzothienyl Carbonyl 4-Arylpiperazines as Novel Delavirdine Analogs</p>

    <p>          Pessoa-Mahana, H. <i>et al.</i></p>

    <p>          Synthetic Communications <b>2007</b>.  37(7-9): 1227-1235</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247620500020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247620500020</a> </p><br />

    <p>2.     61639   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          CCR5 antagonists useful for treating HIV</p>

    <p>          Ramanathan, Ragulan, Miller, Michael W, Chowdhury, Swapan K, Alton, Kevin B, Grotz, Diane, Rindgen, Diane, and Tendolkar, Amol</p>

    <p>          PATENT:  US <b>2007203149</b>  ISSUE DATE:  20070830</p>

    <p>          APPLICATION: 2007-22529  PP: 17pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61640   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Pyridin-2(1H)-ones: a promising class of HIV-1 non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Medina-Franco, Jose Luis, Martinez-Mayorga, Karina, Juarez-Gordiano, Cecilia, and Castillo, Rafael</p>

    <p>          ChemMedChem <b>2007</b>.  2(8): 1141-1147</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61641   HIV-LS-383; WOS-HIV-9/7/2007</p>

    <p class="memofmt1-2">          Human Polycomb Group Eed Protein Negatively Affects Hiv-1 Assembly and Release</p>

    <p>          Rakotobe, D. <i>et al.</i></p>

    <p>          Retrovirology <b>2007</b>.  4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247632900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247632900001</a> </p><br />

    <p>5.     61642   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          pyrrolo[3,4-c]pyrrole derivatives as heterocyclic antiviral compounds and CCR5 receptor antagonists and their preparation, pharmaceutical compositions and use in the treatment of HIV infection, AIDS and ARC</p>

    <p>          Lemoine, Rcmy, Melville, Chris Richard, Padilla, Fernando, Rotstein, David Mark, and Wanner, Jutta</p>

    <p>          PATENT:  US <b>2007191406</b>  ISSUE DATE:  20070816</p>

    <p>          APPLICATION: 2007-51368  PP: 45pp.</p>

    <p>          ASSIGNEE:  (Roche Palo Alto LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61643   HIV-LS-383; WOS-HIV-9/7/2007</p>

    <p class="memofmt1-2">          Ccr5 Small-Molecule Antagonists and Monoclonal Antibodies Exert Potent Synergistic Antiviral Effects by Cobinding to the Receptor</p>

    <p>          Ji, C. <i>et al.</i></p>

    <p>          Molecular Pharmacology <b>2007</b>.  72(1): 18-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247477600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247477600003</a> </p><br />
    <br clear="all">

    <p>7.     61644   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Use of 6-(3-chloro-2-fluorobenzyl)-1-[(2S)-1-hydroxy-3-methylbutan-2-yl]-7-methoxy-4-oxo-1,4-dihydroquinoline-3-carboxylic acid or salt thereof for treating retrovirus infection</p>

    <p>          Matsuzaki, Yuji, Kano, Mitsuki, and Ikeda, Satoru</p>

    <p>          PATENT:  WO <b>2007089030</b>  ISSUE DATE:  20070809</p>

    <p>          APPLICATION: 2007  PP: 89pp.</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     61645   HIV-LS-383; WOS-HIV-9/7/2007</p>

    <p class="memofmt1-2">          Rational Optimization of the Binding Affinity of Cd4 Targeting Peptidomimetics With Potential Anti Hiv Activity</p>

    <p>          Neffe, A. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2007</b>.  50(15): 3482-3488</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248121300012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248121300012</a> </p><br />

    <p>9.     61646   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Preparation of imidazo[1,2-a]pyridine compds. useful in the treatment of HIV infection and other chemokine receptor-mediated diseases</p>

    <p>          Gudmundsson, Kristjan, Boggs, Sharon Davis, Miller, John Franklin, and Svolto, Angilique Christina</p>

    <p>          PATENT:  WO <b>2007087549</b>  ISSUE DATE:  20070802</p>

    <p>          APPLICATION: 2007  PP: 148pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61647   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Imidazo[1,2-a]pyridine-3-carboxamides as anti-HIV agents and their preparation, pharmaceutical compositions and their use in monotherapy and in combination therapy of diseases</p>

    <p>          Gudmundsson, Kristjan and Turner, Elizabeth Madalena</p>

    <p>          PATENT:  WO <b>2007087548</b>  ISSUE DATE:  20070802</p>

    <p>          APPLICATION: 2007  PP: 104pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA and Svolto, Angilique Christina</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61648   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Synthesis of 8-halogenated adenosine compounds as antiviral agents</p>

    <p>          Chang, Junbiao, Cheng, Senxiang, Wang, Limin, Hao, Jia, Wang, Qiang, and Guo, Xiaohe</p>

    <p>          PATENT:  CN <b>101003550</b>  ISSUE DATE: 20070725</p>

    <p>          APPLICATION: 1016-30  PP: 14pp.</p>

    <p>          ASSIGNEE:  (Henan Center of Analysis and Test, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61649   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Design and cellular kinetics of dansyl-labeled CADA derivatives with anti-HIV and CD4 receptor down-modulating activity</p>

    <p>          Vermeire, Kurt, Lisco, Andrea, Grivel, Jean-Charles, Scarbrough, Emily, Dey, Kaka, Duffy, Noah, Margolis, Leonid, Bell, Thomas W, and Schols, Dominique</p>

    <p>          Biochem. Pharmacol. <b>2007</b>.  74(4): 566-578</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   61650   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Hologram quantitative structure-activity relationships for a class of inhibitors of HIV-1 protease</p>

    <p>          Ferreira, Leonardo G, Leitao, Andrei, Montanari, Carlos A, and Andricopulo, Adriano D</p>

    <p>          Lett. Drug Des. Discovery <b>2007</b>.  4(5): 356-364</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   61651   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          A novel bis-tetrahydrofuranylurethane-containing nonpeptidic protease inhibitor (PI), GRL-98065, is potent against multiple-PI-resistant human immunodeficiency virus in vitro</p>

    <p>          Amano, Masayuki, Koh, Yasuhiro, Das, Debananda, Li, Jianfeng, Leschenko, Sofiya, Wang, Yuan-Fang, Boross, Peter I, Weber, Irene T, Ghosh, Arun K, and Mitsuya, Hiroaki</p>

    <p>          Antimicrob. Agents Chemother. <b>2007</b>.  51(6): 2143-2155</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   61652   HIV-LS-383; SCIFINDER-HIV-9/10/2007</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV-1 activity of new MKC-442 analogues with an alkynyl-substituted 6-benzyl group</p>

    <p>          Aly, Youssef L, Pedersen, Erik B, La Colla, Paolo, and Loddo, Roberta</p>

    <p>          Arch. Pharm. (Weinheim, Ger.) <b>2007</b>.  340(5): 225-235</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   61653   HIV-LS-383; WOS-HIV-9/7/2007</p>

    <p class="memofmt1-2">          Pharmacokinetics of the Anti-Human Immunodeficiency Virus Agent 1-(Beta-D-Dioxolane)Thymine in Rhesus Monkeys</p>

    <p>          Asif, G. <i>et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(7): 2424-2429</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247665800019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247665800019</a> </p><br />

    <p>17.   61654   HIV-LS-383; WOS-HIV-9/14/2007</p>

    <p class="memofmt1-2">          Novel Synthesis and Anti-Hiv-1 Activity of 2-Arylthio-6-Benzyl-2,3-Dihydro-1h-Pyrimidin-4-Ones (Aryl S-Dabos)</p>

    <p>          Aly, Y. <i>et al.</i></p>

    <p>          Synthesis-Stuttgart <b>2007</b>.(13): 1955-1960</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248184200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248184200008</a> </p><br />

    <p>18.   61655   HIV-LS-383; WOS-HIV-9/14/2007</p>

    <p class="memofmt1-2">          Nuclear Factor 90(Nf90) Targeted to Tar Rna Inhibits Transcriptional Activation of Hiv-I</p>

    <p>          Agbottah, E. <i>et al.</i></p>

    <p>          Retrovirology <b>2007</b>.  4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248016200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248016200002</a> </p><br />

    <p>19.   61656   HIV-LS-383; WOS-HIV-9/14/2007</p>

    <p class="memofmt1-2">          Qsar Analysis of Caffeoyl Naphthalene Sulfonamide Derivatives as Hiv-1 Integrase Inhibitors</p>

    <p>          Sahu, K. <i>et al.</i></p>

    <p>          Medicinal Chemistry Research <b>2007</b>.  15(7-8): 418-430</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248743400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248743400003</a> </p><br />
    <br clear="all">

    <p>20.   61657   HIV-LS-383; WOS-HIV-9/14/2007</p>

    <p class="memofmt1-2">          All-Trans Retinoic Acid Attacks Reverse Transcriptase Resulting in Inhibition of Hiv-1 Replication</p>

    <p>          Maeda, Y. <i>et al.</i></p>

    <p>          Hematology <b>2007</b>.  12(3): 263-266</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248115000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248115000013</a> </p><br />

    <p>21.   61658   HIV-LS-383; PUBMED-HIV-9/17/2007</p>

    <p class="memofmt1-2">          Discovery and Synthesis of HIV Integrase Inhibitors: Development of Potent and Orally Bioavailable N-Methyl Pyrimidones</p>

    <p>          Gardelli, C, Nizi, E, Muraglia, E, Crescenzi, B, Ferrara, M, Orvieto, F, Pace, P, Pescatore, G, Poma, M, Ferreira, MD, Scarpelli, R, Homnick, CF, Ikemoto, N, Alfieri, A, Verdirame, M, Bonelli, F, Paz, OG, Taliani, M, Monteagudo, E, Pesci, S, Laufer, R, Felock, P, Stillmock, KA, Hazuda, D, Rowley, M, and Summa, V</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17824681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17824681&amp;dopt=abstract</a> </p><br />

    <p>22.   61659   HIV-LS-383; PUBMED-HIV-9/17/2007</p>

    <p class="memofmt1-2">          Inhibition of Human Immunodeficiency Virus Type-1 Concerted Integration by Strand Transfer Inhibitors Which Recognize a Transient Structural Intermediate</p>

    <p>          Pandey, KK, Bera, S, Zahm, J, Vora, A, Stillmock, K, Hazuda, D, and Grandgenett, DP</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17804497&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17804497&amp;dopt=abstract</a> </p><br />

    <p>23.   61660   HIV-LS-383; PUBMED-HIV-9/17/2007</p>

    <p class="memofmt1-2">          Identification of potential HIV-1 targets of minocycline</p>

    <p>          Jenwitheesuk, E and Samudrala, R</p>

    <p>          Bioinformatics  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17804437&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17804437&amp;dopt=abstract</a> </p><br />

    <p>24.   61661   HIV-LS-383; PUBMED-HIV-9/17/2007</p>

    <p class="memofmt1-2">          Indolyl Aryl Sulfones as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors: Role of Two Halogen Atoms at the Indole Ring in Developing New Analogues with Improved Antiviral Activity</p>

    <p>          Regina, GL, Coluccia, A, Piscitelli, F, Bergamini, A, Sinistro, A, Cavazza, A, Maga, G, Samuele, A, Zanoli, S, Novellino, E, Artico, M, and Silvestri, R</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17803291&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17803291&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>25.   61662   HIV-LS-383; PUBMED-HIV-9/17/2007</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Alkenyldiarylmethane HIV-1 Non-Nucleoside  Reverse Transcriptase Inhibitors That Possess Increased Hydrolytic Stability</p>

    <p>          Cullen, MD, Deng, BL, Hartman, TL, Watson, KM, Buckheit, RW Jr, Pannecouque, C, Clercq, ED, and Cushman, M</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17803290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17803290&amp;dopt=abstract</a> </p><br />

    <p>26.   61663   HIV-LS-383; PUBMED-HIV-9/17/2007</p>

    <p class="memofmt1-2">          Fluorescence correlation spectroscopy at single molecule level on the Tat-TAR complex and its inhibitors</p>

    <p>          Nandi, CK, Parui, PP, Brutschy, B, Scheffer, U, and Gobel, M</p>

    <p>          Biopolymers <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17764074&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17764074&amp;dopt=abstract</a> </p><br />

    <p>27.   61664   HIV-LS-383; PUBMED-HIV-9/17/2007</p>

    <p class="memofmt1-2">          Intracellular Metabolism and Persistence of the Anti-Human Immunodeficiency Virus Activity of 2&#39;, 3&#39;-Didehydro-3&#39;-Deoxy-4&#39;- Ethynylthymidine, A Novel Thymidine Analog</p>

    <p>          Paintsil, E, Dutschman, GE, Hu, R, Grill, SP, Lam, W, Baba, M, Tanaka, H, and Cheng, YC</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724147&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724147&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
