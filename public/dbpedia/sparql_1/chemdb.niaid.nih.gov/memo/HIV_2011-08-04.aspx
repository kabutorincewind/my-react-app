

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-08-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="PUc+XFqii7XRm6xZX65OcZ4BrrWnlFhygZWdF1sUof0h4PwbvwkP8dYn/imHpDVT6xIOGTp0Pg3CV0ABYfDbnsHGVLFECTlnFr2UFynbmdVdNDi89DbgtHltEh4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3B3DA394" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  July 22, 2011 - August 4, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21800876">Design of HIV-1 Protease Inhibitors with C3-Substituted Hexahydrocyclopentafuranyl Urethanes as P2-Ligands: Synthesis, Biological Evaluation, and Protein-Ligand X-ray Crystal Structure.</a> Ghosh, A.K., B.D. Chapsal, G.L. Parham, M. Steffey, J. Agniswamy, Y.F. Wang, M. Amano, I.T. Weber, and H. Mitsuya, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21800876].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21327540">Comparative Docking and CoMFA Analysis of Curcumine Derivatives as HIV-1 Integrase Inhibitors.</a> Gupta, P., P. Garg, and N. Roy, Molecular Diversity, 2011. 15(3): p. 733-750; PMID[21327540].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21600931">Identification of Low-Molecular Weight Inhibitors of HIV-1 Reverse Transcriptase using a Cell-based High-throughput Screening System.</a> Jegede, O., A. Khodyakova, M. Chernov, J. Weber, L. Menendez-Arias, A. Gudkov, and M.E. Quinones-Mateu, Antiviral Research, 2011. 91(2): p. 94-98; PMID[21600931].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21799112">Designed Oligomers of Cyanovirin-N Show Enhanced HIV Neutralization.</a> Keeffe, J.R., P.N. Gnanapragasam, S.K. Gillespie, J. Yong, P.J. Bjorkman, and S.L. Mayo, Proceedings of the National Academy of Sciences of the United States of America, 2011. <b>[Epub ahead of print]</b>; PMID[21799112].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21801309">Rational Design of Peptides with Anti-HCV/HIV Activities and Enhanced Specificity.</a> Li, G.R., L.Y. He, X.Y. Liu, A.P. Liu, Y.B. Huang, C. Qiu, X.Y. Zhang, J.Q. Xu, W. Yang, and Y.X. Chen, Chemical Biology and Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21801309].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21803040">Anti-HIV Siamycin I Directly Inhibits Autophosphorylation Activity of the Bacterial FsrC Quorum Sensor and other ATP-dependent Enzyme Activities.</a> Ma, P., K. Nishiguchi, H.M. Yuille, L.M. Davis, J. Nakayama, and M.K. Phillips-Jones, FEBS Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21803040].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21719299">Synthesis and Biological Activity of Naphthyl-substituted (B-ring) benzophenone Derivatives as Novel Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Ma, X.D., X. Zhang, H.F. Dai, S.Q. Yang, L.M. Yang, S.X. Gu, Y.T. Zheng, Q.Q. He, and F.E. Chen, Bioorganic &amp; Medicinal Chemistry, 2011. 19(15): p. 4601-4607; PMID[21719299].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21698775">Toward the Discovery of Novel Anti-HIV Drugs. Second-Generation Inhibitors of the Cellular ATPase DDX3 with Improved Anti-HIV Activity: Synthesis, Structure-Activity Relationship Analysis, Cytotoxicity Studies, and Target Validation.</a> Maga, G., F. Falchi, M. Radi, L. Botta, G. Casaluce, M. Bernardini, H. Irannejad, F. Manetti, A. Garbelli, A. Samuele, S. Zanoli, J.A. Este, E. Gonzalez, E. Zucca, S. Paolucci, F. Baldanti, J. De Rijck, Z. Debyser, and M. Botta, ChemMedChem, 2011. 6(8): p. 1371-1389; PMID[21698775].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21763500">Celastrol Inhibits Tat-Mediated Human Immunodeficiency Virus (HIV) Transcription and Replication.</a> Narayan, V., K.C. Ravindra, C. Chiaro, D. Cary, B.B. Aggarwal, A.J. Henderson, and K.S. Prabhu, Journal of Molecular Biology, 2011. 410(5): p. 972-983; PMID[21763500].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21599420">New First and Second Generation Inhibitors of Human Immunodeficiency Virus-1 Integrase.</a> Pendri, A., N.A. Meanwell, K.M. Peese, and M.A. Walker, Expert Opinion on Therapeutic Patents, 2011. 21(8): p. 1173-1189; PMID[21599420].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21787242">Abacavir and Lamivudine for the Treatment of Human Immunodeficiency Virus.</a> Rizzardini, G. and P. Zucchi, Expert Opinion on Pharmacotherapry, 2011. <b>[Epub ahead of print]</b>; PMID[21787242].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21565353">Analysis of HIV-1 Fusion peptide Inhibition by Synthetic Peptides from E1 Protein of GB Virus C.</a> Sanchez-Martin, M.J., K. Hristova, M. Pujol, M.J. Gomara, I. Haro, M.A. Alsina, and M.A. Busquets, Journal of Colloid and Interface Science, 2011. 360(1): p. 124-131; PMID[21565353].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21719300">Synthesis and Antiviral Evaluation of 9-(S)-[3-Alkoxy-2-(phosphonomethoxy)propyl]nucleoside alkoxyalkyl esters: Inhibitors of Hepatitis C Virus and HIV-1 Replication.</a> Valiaeva, N., D.L. Wyles, R.T. Schooley, J.B. Hwu, J.R. Beadle, M.N. Prichard, and K.Y. Hostetler, Bioorganic &amp; Medicinal Chemistry, 2011. 19(15): p. 4616-4625; PMID[21719300].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292785900028">Isolation, Identification and Antiviral Activities of Metabolites of Calycosin-7-O-beta-D-glucopyranoside.</a> Chen, L.Y., Z.X. Li, Y.H. Tang, X.L. Cui, R.H. Luo, S.S. Guo, Y.T. Zheng, and C.G. Huang, Journal of Pharmaceutical and Biomedical Analysis, 2011. 56(2): p. 382-389; ISI[000292785900028].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292571200001">Synthesis of a Labeled Inhibitor of HIV-1 Attachment: 1-(4-Benzoylpiperazin-1-yl)-2-(4,7-dimethoxy-1H-pyrrolo[2,3-c]pyridinyl- 3-yl-[U-(14)C]ethane-1,2-dione, BMS-488043-(14)C.</a> Ekhato, I.V. and J.K. Rinehart, Journal of Labelled Compounds &amp; Radiopharmaceuticals, 2011. 54(6): p. 289-291; PMID[000292571200001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292761500211.">Homogeneous Enzyme Immunoassay for HIV-1 Antiretroviral Drugs: Darunavir and Maraviroc.</a> Kasper, K.C., B. Moon, J. Nguyen, A. Orozco, K. Chung, J. Valencia, and J. Valdez, Therapeutic Drug Monitoring, 2011. 33(4): p. 519-519; ISI[000292761500211].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292699000011">Novel PI3K/Akt Inhibitors Screened by the Cytoprotective Function of Human Immunodeficiency Virus Type 1 Tat.</a> Kim, Y., J.A. Hollenbaugh, D.H. Kim, and B. Kim, Plos One, 2011. 6(7); ISI[000292699000011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292795600012">New Substituted Thiazol-2-ylidene-benzamides and Their Reaction with 1-Aza-2-azoniaallene Salts.</a> Saeed, A., N.A. Al-Masoudi, A.A. Ahmed, and C. Pannecouque, Synthesis and anti-HIV Activity. Zeitschrift Fur Naturforschung Section B-a Journal of Chemical Sciences, 2011. 66(5): p. 512-520; ISI[000292795600012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292480300007">Synthesis of 3-Guaninyl- and 3-Adeninyl-5-hydroxymethyl-2-pyrrolidinone nucleosides.</a> Saleh, A., J.G. D&#39;Angelo, M.D. Morton, J. Quinn, K. Redden, R.W. Mielguz, C. Pavlik, and M.B. Smith, Journal of Organic Chemistry, 2011. 76(14): p. 5574-5583; ISI[000292480300007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292586200004.">Effects of Cathelicidin and its Fragments on Three Key Enzymes of HIV-1.</a> Wong, J.H., A. Legowska, K. Rolka, T.B. Ng, M. Hui, C.H. Cho, W.W.L. Lam, S.W.N. Au, O.W. Gu, and D.C.C. Wan, Peptides, 2011. 32(6): p. 1117-1122; ISI[000292586200004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292531900010">Modeling and Hybrid Virtual Screening for the Discovery of Novel I kappa B Kinase 2 (IKK2) Inhibitors.</a> Xie, H.Z., L.Y. Liu, J.X. Ren, J.P. Zhou, R.L. Zheng, L.L. Li, and S.Y. Yang, Pharmacophore Journal of Biomolecular Structure &amp; Dynamics, 2011. 29(1): p. 165-179; ISI[000292531900010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292547900070">Phosphonoformic Acid Inhibits Viral Replication by Trapping the Closed Form of the DNA Polymerase.</a> Zahn, K.E., E.P. Tchesnokov, M. Gotte, and S. Doublie, Journal of Biological Chemistry, 2011. 286(28): p. 25246-25255; ISI[000292547900070].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="memofmt2-2">Patent citations</p>

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110609&amp;CC=WO&amp;NR=2011066869A1&amp;KC=A1">Preparation of &#946;-Hairpin Peptidomimetics as CXCR4 Antagonists for Treatment of HIV and Other Diseases.</a> Barthelemy, S., C. Bisang, F.O. Gombert, A. Lederer, D. Obrecht, B. Romagnoli, J. Zumbrunn, T. Remus, and G. Lemercier. Patent. 2011. 2009-EP66462 2011066869.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110331&amp;CC=PL&amp;NR=208177B1&amp;KC=B1">Preparation of New Derivatives of 1,4,2-Benzodithiazine.</a> Brzozowski, Z., F. Saczewski, and N. Neamati. Patent. 2011. 2005-377955 208177.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110623&amp;CC=US&amp;NR=2011152229A1&amp;KC=A1">Betulinic acid Derivatives as anti-HIV Agents.</a> Chen, C.H., L. Huang, and K.-H. Lee. Patent. 2011. 2007-943956 20110152229.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110512&amp;CC=US&amp;NR=2011112068A1&amp;KC=A1">Preparation of 2,3,4,5-Tetrahydrobenzo[f][1,2]thiazepine Derivatives as Human Immunodeficiency Virus Protease Inhibitors.</a> Ganguly, A.K., D. Biswas, S.S. Alluri, D. Caroccia, C.-H. Wang, and E.H. Kang. Patent. 2011. 2009-615903 20110112068.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110518&amp;CC=CN&amp;NR=102060786A&amp;KC=A">Preparation of Heterocycles as Antiviral Agents.</a> Ma, J., F. Zhang, and J. Nie. Patent. 2011. 2011-10023093 102060786.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110526&amp;CC=WO&amp;NR=2011061590A1&amp;KC=A1">Novel Carboxamide Derivatives as HIV Inhibitors and Their Preparation and Use in the Treatment of Viral Mediated Diseases.</a> Parthasaradhi Reddy, B., B. Vamsi Krishna, V. Manohar Sharma, K. Rathnakar Reddy, M. Madhanmohan Reddy, L. Vl Subrahmanyam, and M. Prem Kumar. Patent. 2011. 2010-IB2907 2011061590.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110519&amp;CC=WO&amp;NR=2011060395A1&amp;KC=A1">Preparation of Pyrimidine Derivatives as HIV Inhibitors.</a> Shipps, G.W., Jr., C.C. Cheng, R.J. Herr, and J. Yang. Patent. 2011. 2010-US56767</p>

    <p class="plaintext">2011060395.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110616&amp;CC=WO&amp;NR=2011070533A1&amp;KC=A1">Sodium-Potassium Pump &#945;1 Subunit-derived Peptides and their Derivatives Inhibiting Extracellular Release of HIV-1 Tat Protein and HIV-1 Replication.</a> Agostini, S. and M. Giacca. Patent. 2011. 2010-IB55715 2011070533.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110630&amp;CC=WO&amp;NR=2011077093A1&amp;KC=A1">Virus Receptor Fusion Proteins for Inhibition of Viral Entry into Target Cells.</a> Bahrami, S., M. Tolstrup, M.D. Ryttergmrd, F.S. Pedersen, and L.J. Ostergaard. Patent. 2011. 2010-GB2321 2011077093.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110630&amp;CC=WO&amp;NR=2011076765A1&amp;KC=A1">Novel Antiviral Compounds.</a> Carlens, G., P. Chaltin, F. Christ, Z. Debyser, A. Marchand, D. Marchand, A. Voet, and M. De Maeyer. Patent. 2011. 2010-EP70306 2011076765.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110616&amp;CC=WO&amp;NR=2011070131A1&amp;KC=A1">Preparation of Peptide Isosteres, Derivatives of 5-Amino-4-hydroxypentanoylamides, as HIV Protease Inhibitors.</a> Kalayanov, G., B.R.R. Kesteleyn, K. Parkes, B.B. Samuelsson, W.B.G. Schepens, J.W.J. Thuring, H.K. Wallberg, and J.K. Wegner. Patent. 2011. 2010-EP69328 2011070131.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110707&amp;CC=WO&amp;NR=2011080562A1&amp;KC=A1">Preparation of AZA-Peptides Containing 2,2-Disubstituted Cyclobutyl and/or Substituted Alkoxybenzyl Derivatives as Antivirals.</a> Parthasaradhi Reddy, B., B. Vamsi Krishna, V. Manohar Sharma, K. Rathnakar Reddy, and M. Madhanmohan Reddy. Patent. 2011. 2010-IB3313 2011080562.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110721&amp;CC=WO&amp;NR=2011086423A1&amp;KC=A1">Pharmaceutical use of Multicyclic Compounds as anti-AIDS Agents.</a> Pianowski, L.F., J.B. Calixto, and C.P. Chaves. Patent. 2011. 2010-IB50198 2011086423.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110623&amp;CC=WO&amp;NR=2011075052A1&amp;KC=A1">Preparation of 3&#39;-Deoxy-3&#39;-methylidene-&#946;-L-nucleosides as Antiviral Agents.</a> Zhou, X.X., S. Torssell, O. Wallner, and P. Sun. Patent. 2011. 2010-SE51374 2011075052.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0722-080411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
