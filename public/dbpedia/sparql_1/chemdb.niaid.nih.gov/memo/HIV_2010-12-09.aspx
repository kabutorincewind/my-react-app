

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-12-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="qRuVIJ5nkos8wg2oDJquMg09QrlstfGlmvnRQHb51EMwJLRtSYiieRZH7qIb2PJyDFdGorP00iO2PyRmZYF75IBnrhZc8o09NjxE63YkvSU698Ce3+vQg/nQs0g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="334C909B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  November 24 - December 9, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21054396">Multidrug resistance reversal properties and cytotoxic evaluation of representatives of a novel class of HIV-1 protease inhibitors.</a> Coburger, C., H. Lage, J. Molnar, A. Langner, and A. Hilgeroth. Journal of Pharmacy and Pharmacology, 2010. 62(12): p. 1704-10; PMID[21054396].</p>

    <p class="NoSpacing"><b>[Pubmed].</b> HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21124745">The Formulated Microbicide RC-101 Was Safe and Antivirally Active Following Intravaginal Application in Pigtailed Macaques.</a> Cole, A.M., D.L. Patton, L.C. Rohan, A.L. Cole, Y. Cosgrove-Sweeney, N.A. Rogers, D. Ratner, A.B. Sassi, C. Lackman-Smith, P. Tarwater, B. Ramratnam, P. Ruchala, R.I. Lehrer, A.J. Waring, and P. Gupta. PLoS One, 2010. 5(11): p. e15111; PMID[21124745].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21117063">Suppression of HIV-1 transcriptional elongation by a DING phosphatase</a> Darbinian, N., R. Gomberg, L. Mullen, S. Garcia, M.K. White, K. Khalili, and S. Amini. Journal of Cellular Biochemistry, 2010.  <b>[Epub ahead of print]</b>; PMID[21117063].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21121618">Design and Synthesis of alpha-Carboxy Phosphononucleosides.</a> Debarge, S., J. Balzarini, and A.R. Maguire. Journal of Organic Chemistry, 2010.  <b>[Epub ahead of print]</b>; PMID[21121618].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21128887">Inhibition of HIV-1 by Fusion Inhibitors.</a> Eggink, D., B. Berkhout, and R.W. Sanders. Current Pharmaceutical Design, 2010.  <b>[Epub ahead of print]</b>; PMID[21128887].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21118814">New insights into the mechanisms whereby low molecular weight CCR5 ligands inhibit HIV-1 infection.</a> Garcia-Perez, J., P. Rueda, I. Staropoli, E. Kellenberger, J. Alcami, F. Arenzana-Seisdedos, and B. Lagane. Journal of Biological Chemistry, 2010.  <b>[Epub ahead of print]</b>; PMID[21118814].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21114787">Interaction of HIV-1 Reverse Transcriptase Ribonuclease H with an Acylhydrazone Inhibitor.</a> Gong, Q., L. Menon, T. Ilina, L.G. Miller, J. Ahn, M.A. Parniak, and R. Ishima. Chemical Biology &amp;  Drug Design, 2011. 77(1): p. 39-47; PMID[21114787].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21135183">Alkylated Porphyrins Have Broad Antiviral Activity against Hepadnaviuses, Flaviviruses, Filoviruses and Arenaviruses.</a> Guo, H., X. Pan, R. Mao, X. Zhang, L. Wang, X. Lu, J. Chang, J.T. Guo, S. Passic, F.C. Krebs, B. Wigdahl, T.K. Warren, C.J. Retterer, S. Bavari, X. Xu, A. Cuconati, and T.M. Block. Antimicrobial Agents and Chemotherapy, 2010.  <b>[Epub ahead of print]</b>; PMID[21135183].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20974536">Cyclic peptide inhibitors of HIV-1 integrase derived from the LEDGF/p75 protein.</a> Hayouka, Z., M. Hurevich, A. Levin, H. Benyamini, A. Iosub, M. Maes, D.E. Shalev, A. Loyter, C. Gilon, and A. Friedler. Bioorganic &amp; Medicinal Chemistry, 2010. 18(23): p. 8388-95; PMID[20974536].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20810746">C-C chemokine receptor type 5 (CCR5) utilization of transmitted and early founder human immunodeficiency virus type 1 envelopes and sensitivity to small-molecule CCR5 inhibitors.</a> Hu, Q., X. Huang, and R.J. Shattock. Journal of General Virology, 2010. 91(Pt 12): p. 2965-73; PMID[20810746].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20832370">The evaluation of catechins that contain a galloyl moiety as potential HIV-1 integrase inhibitors.</a> Jiang, F., W. Chen, K. Yi, Z. Wu, Y. Si, W. Han, and Y. Zhao. Clinical Immunology, 2010. 137(3): p. 347-56; PMID[20832370].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21110817">New Therapeutic Approaches Targeted at the Late Stages of the HIV-1 Replication Cycle.</a> Jiang, Y., X. Liu, and E. De Clercq. Current Medicinal Chemistry, 2010.  <b>[Epub ahead of print]</b>; PMID[21110817].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21114474">Immunoregulatory and anti-HIV-1 enzyme activities of antioxidant components from lotus (Nelumbo nucifera Gaertn) rhizome.</a> Jiang, Y., T.B. Ng, Z. Liu, C. Wang, N. Li, W. Qiao, and F. Liu. Bioscience Reports, 2010.  <b>[Epub ahead of print]</b>; PMID[21114474].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20502318">Triplex-forming MicroRNAs form stable complexes with HIV-1 provirus and inhibit its replication.</a> Kanak, M., M. Alseiari, P. Balasubramanian, K. Addanki, M. Aggarwal, S. Noorali, A. Kalsum, K. Mahalingam, G. Pace, N. Panasik, and O. Bagasra. Applied Immunohistochemistry Molecular Morphology, 2010. 18(6): p. 532-45; PMID[20502318].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21121641">Discovery of a small molecule PDI inhibitor that inhibits reduction of HIV-1 envelope glycoprotein gp120.</a> Khan, M.M., S. Siro, S. Lai, M. Kawatani, T. Shimizu, and H. Osada. ACS Chemical Biology, 2010.  <b>[Epub ahead of print]</b>; PMID[21121641].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21115794">In Vitro Virology of S/GSK1349572, a Next-Generation HIV Integrase Inhibitor.</a> Kobayashi, M., T. Yoshinaga, T. Seki, C. Wakasa-Morimoto, K.W. Brown, R. Ferris, S.A. Foster, R.J. Hazen, S. Miki, A. Suyama-Kagitani, S. Kawauchi-Miki, T. Taishi, T. Kawasuji, B.A. Johns, M.R. Underwood, E.P. Garvey, A. Sato, and T. Fujiwara. Antimicrobial Agents and Chemotherapy, 2010.  <b>[Epub ahead of print]</b>; PMID[21115794].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20876377">EASY-HIT: HIV full-replication technology for broad discovery of multiple classes of HIV inhibitors.</a> Kremb, S., M. Helfer, W. Heller, D. Hoffmann, H. Wolff, A. Kleinschmidt, S. Cepok, B. Hemmer, J. Durner, and R. Brack-Werner. Antimicrobial Agents and Chemotherapy, 2010. 54(12): p. 5257-68; PMID[20876377].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20975512">Anti-HIV-1 activity of the neurokinin-1 receptor antagonist aprepitant and synergistic interactions with other antiretrovirals.</a> Manak, M.M., D.A. Moshkoff, L.T. Nguyen, J. Meshki, P. Tebas, F. Tuluc, and S.D. Douglas. AIDS, 2010. 24(18): p. 2789-96; PMID[20975512].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20926575">Excessive RNA Splicing and Inhibition of HIV-1 Replication Induced by Modified U1 Small Nuclear RNAs.</a> Mandal, D., Z. Feng, and C.M. Stoltzfus. Journal of Virology, 2010. 84(24): p. 12790-800; PMID[20926575].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20953192">Small-molecule inactivation of HIV-1 NCp7 by repetitive intracellular acyl transfer.</a> Miller Jenkins, L.M., D.E. Ott, R. Hayashi, L.V. Coren, D. Wang, Q. Xu, M.L. Schito, J.K. Inman, D.H. Appella, and E. Appella. Natural Chemical Biology, 2010. 6(12): p. 887-9; PMID[20953192].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21106536">Protection of HIV-neutralizing aptamers against rectal and vaginal nucleases: implications for RNA-based therapeutics.</a> Moore, M.D., J. Cookson, V.K. Coventry, B. Sproat, L. Rabe, R.D. Cranston, I. McGowan, and W. James. Journal of Biological Chemistry, 2010.  <b>[Epub ahead of print]</b>; PMID[21106536].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21035347">1-Benzyl derivatives of 5-(arylamino)uracils as anti-HIV-1 and anti-EBV agents.</a> Novikov, M.S., R.W. Buckheit, Jr., K. Temburnikar, A.L. Khandazhinskaya, A.V. Ivanov, and K.L. Seley-Radtke. Bioorganic &amp;  Medicinal Chemistry, 2010. 18(23): p. 8310-4; PMID[21035347].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21108978">Analyses of nanoformulated antiretroviral drug charge, size, shape and content for uptake, drug release and antiviral activities in human monocyte-derived macrophages.</a> Nowacek, A.S., S. Balkundi, J. McMillan, U. Roy, A. Martinez-Skinner, R.L. Mosley, G. Kanmogne, A.V. Kabanov, T. Bronich, and H.E. Gendelman. Journal of Controlled Release, 2010.  <b>[Epub ahead of print]</b>; PMID[21108978].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21106331">Structure-activity relationships of polybiguanides with activity against human immunodeficiency virus type 1.</a> Passic, S.R., M.L. Ferguson, B.J. Catalone, T. Kish-Catalone, V. Kholodovych, W. Zhu, W. Welsh, R. Rando, M.K. Howett, B. Wigdahl, M. Labib, and F.C. Krebs. Biomedicine and Pharmacotherapy, 2010. 64(10): p. 723-32; PMID[21106331].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21121632">Antiretroviral Activity of Thiosemicarbazone Metal Complexes.</a> Pelosi, G., F. Bisceglie, F. Bignami, P. Ronzi, P. Schiavone, M.C. Re, C. Casoli, and E. Pilotti. Journal of Medicinal Chemistry, 2010.  [Epub <b>ahead of print]</b>; PMID[21121632].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20738178">Protective effect of partially purified 35 kDa protein from silk worm (Bombyx mori) fecal matter against carbon tetrachloride induced hepatotoxicity and in vitro anti-viral properties.</a> Raghavendra, R., S. Neelagund, G. Kuluvar, V. Bhanuprakash, and Y. Revanaiah. Pharmaceutical Biology, 2010. 48(12): p. 1426-31; PMID[20738178].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21128663">An Imidazopiperidine Series of CCR5 Antagonists for the Treatment of HIV: The Discovery of N-{(1S)-1-(3-Fluorophenyl)-3-[(3-endo)-3-(5-isobutyryl-2-methyl-4,5,6,7-tetrahydr o-1H-imidazo[4,5-c], Pubmed, HIV_1124-121010.pyridin-1-yl)-8-azabicyclo[3.2.1], Pubmed, HIV_1124-121010.oct-8-yl], Pubmed, HIV_1124-121010.propyl}acetamide (PF-232798).</a> Stupple, P.A., D.V. Batchelor, M. Corless, P.K. Dorr, D. Ellis, D.R. Fenwick, S.R. Galan, R.M. Jones, H.J. Mason, D.S. Middleton, M. Perros, F. Perruccio, M.Y. Platts, D.C. Pryde, D. Rodrigues, N.N. Smith, P.T. Stephenson, R. Webster, M. Westby, and A. Wood. Journal of Medicinal Chemistry, 2010.  <b>[Epub ahead of print]</b>; PMID[21128663].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21110816">Functional Roles of Azoles Motif in Anti-HIV Agents.</a> Zhan, P., D. Li, X. Chen, X. Liu, and E. De Clercq. Current Medicinal Chemistry, 2010.  <b>[Epub ahead of print]</b>; PMID[21110816].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.HIV_1124-121010.</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284131200005">High-Affinity Glycopolymer Binding to Human DC-SIGN and Disruption of DC-SIGN Interactions with HIV Envelope Glycoprotein.</a> Becer, C.R., M.I. Gibson, J. Geng, R. Ilyas, R. Wallis, D.A. Mitchell, and D.M. Haddleton. Journal of the American Chemical Society, 2010. 132(43): p. 15130-15132; ISI[000283621700012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283687900010">A cardiac glucoside with invitro anti-HIV activity isolated from Elaeodendron croceum.</a> Prinsloo, G., J.J.M. Meyer, A.A. Hussein, E. Munoz, and R. Sanchez. Natural Product Research, 2010. 24(18): p. 1743-1746; ISI[000283687900010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283703000016">Diastereoselective Synthesis of Aryloxy Phosphoramidate Prodrugs of 3 &#39;-Deoxy-2 &#39;,3 &#39;-didehydrothymidine Monophosphate.</a> Roman, C.A., J. Balzarini, and C. Meier. Journal of Medicinal Chemistry, 2010. 53(21): p. 7675-7681; ISI[000283703000016].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283882100010">Microwave-Assisted Synthesis of Some Novel Thiazolidinone and Thiohydantoin Derivatives of Isatins.</a> Raghuvanshi, D.S. and K.N. Singh. Phosphorus Sulfur and Silicon and the Related Elements, 2010. 185(11): p. 2243-2248; ISI[000283882100010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283895200002">Synthesis and bioactivity of erythro-nordihydroguaiaretic acid, threo-(-)-saururenin and their analogues.</a> Xia, Y.M., Y.Y. Zhang, W. Wang, Y.N. Ding, and R. He. Journal of the Serbian Chemical Society, 2010. 75(10): p. 1325-1335; ISI[000283895200002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283908200012">Four New Nortriterpenoids from Schisandra lancifolia.</a> Xiao, W.L., Y.L. Wu, S.Z. Shang, F. He, X.A. Luo, G.Y. Yang, J.X. Pu, G.Q. Chen, and H.D. Sun. Helvetica Chimica Acta, 2010. 93(10): p. 1975-1982; ISI[000283908200012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284104800020">Amino acid derivatives. Part 6. New analogs of the angiotensin-converting enzyme &#39;Captopril&#39;. Synthesis and anti-HIV activity.</a> Al-Masoudi, N.A., N.S. Hamad, S. Hameed, and C. Pannecouque. Arkivoc, 2010: p. 242-253; ISI[000284104800020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284131200005">Antiretroviral activity of two polyisoprenylated acylphloroglucinols, 7-epi-nemorosone and plukenetione A, isolated from Caribbean propolis.</a>  Diaz-Carballo, D., K. Ueberla, V. Kleff, S. Ergun, S. Malak, M. Freistuehler, S. Somogyi, C. Kucherer, W. Bardenheuer, and D. Strumberg. International Journal of Clinical Pharmacology and Therapeutics, 2010. 48(10): p. 670-677; ISI[000284131200005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1124-121010.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
