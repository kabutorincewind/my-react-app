

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-377.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vy8kaWMmbMjNpCdN3r+g2CgL3ijIVr7OAfYPaLUpdRyqFD9izLPO+zp6GfXBik5PFf7B9tp7EK0g2P+y1qnfCyhwqIRzQdr79Jn4MbIfvmU4lIYpgMPXgyZn9cc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A7626276" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-377-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61139   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Candidate biomarkers for discrimination between infection and disease caused by Mycobacterium tuberculosis</p>

    <p>          Jacobsen, Marc, Repsilber, Dirk, Gutschmidt, Andrea, Neher, Albert, Feldmann, Knut, Mollenkopf, Hans J, Ziegler, Andreas, and Kaufmann, Stefan HE</p>

    <p>          J. Mol. Med. (Heidelberg, Ger.) <b>2007</b>.  85(6): 613-621</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61140   OI-LS-377; WOS-OI-6/15/2007</p>

    <p class="memofmt1-2">          Hairpin Extensions Enhance the Efficacy of Mycolyl Transferase-Specific Antisense Oligonucleotides Targeting Mycobacterium Tuberculosis</p>

    <p>          Harth, G. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2007</b>.  104(17): 7199-7204</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246024700057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246024700057</a> </p><br />

    <p>3.     61141   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Design, synthesis, and biochemical evaluation of Lumazine synthase inhibitors as potential antimicrobial agents</p>

    <p>          Talukdar, Arindam, Bacher, Adelbert, Fisher, Markus, Illarionov, Boris, and Cushman, Mark</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-450</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61142   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Synthesis, characterization and antimicrobial activity of new aliphatic sulfonamide</p>

    <p>          Oezbek, Neslihan, Katircioglu, Hikmet, Karacan, Nurcan, and Baykal, Tuelay</p>

    <p>          Bioorg. Med. Chem. <b>2007</b>.  15(15): 5105-5109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61143   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Uses of 2-Amino-3-cyano-5,5-dimethyl-7-oxocyclohexano[b]thiophene in Heterocyclic Synthesis</p>

    <p>          Wardakhan, Wagnat W, Yousef, Adel M, and Hamed, Faten I</p>

    <p>          Phosphorus, Sulfur Silicon Relat. Elem. <b>2007</b>.  182(7): 1507-1523</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61144   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Synthesis of novel pyrazole, coumarin and pyridazine derivatives evaluated as potential antimicrobial and antifungal agents</p>

    <p>          Wardakhan, Wagnat W and Louca, Nadia A</p>

    <p>          J. Chil. Chem. Soc. <b>2007</b>.  52(2): 1145-1149</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     61145   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Synthesis, stereochemistry and antimicrobial evaluation of some N-morpholinoacetyl-2,6-diarylpiperidin-4-ones</p>

    <p>          Aridoss, G, Balasubramanian, S, Parthiban, P, and Kabilan, S</p>

    <p>          Eur. J. Med. Chem. <b>2007</b>.  42(6): 851-860</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>8.     61146   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          In vitro activities of isavuconazole and other antifungal agents against Candida bloodstream isolates</p>

    <p>          Seifert, H, Aurbach, U, Stefanik, D, and Cornely, O</p>

    <p>          Antimicrob. Agents Chemother. <b>2007</b>.  51(5): 1818-1821</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61147   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Antifungal triazole derivatives, processes for preparing them, and pharmaceutical compositions containing them</p>

    <p>          Park, Joon Seok, Yu, Kyung A, Kim, Sun Young, Song, Yeon Jung, Kim, Kang-Pil, Yoon, Yun Soo, and Han, Mi Ryeong</p>

    <p>          PATENT:  WO <b>2007052943</b>  ISSUE DATE:  20070510</p>

    <p>          APPLICATION: 2006  PP: 62pp.</p>

    <p>          ASSIGNEE:  (Daewoong Pharmaceutical Co., Ltd. S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61148   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Synthesis, antifungal and haemolytic activity of a series of bis(pyridinium)alkanes</p>

    <p>          Ng, Clarissa KL, Singhal, Vatsala, Widmer, Fred, Wright, Lesley C, Sorrell, Tania C, and Jolliffe, Katrina A</p>

    <p>          Bioorg. Med. Chem. <b>2007</b>.  15(10): 3422-3429</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61149   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Topical antifungal compositions containing azoles combined with other antifungal agents</p>

    <p>          Tsuji, Ari and Koga, Hiroyasu</p>

    <p>          PATENT:  JP <b>2007084496</b>  ISSUE DATE:  20070405</p>

    <p>          APPLICATION: 2005-14703  PP: 12pp.</p>

    <p>          ASSIGNEE:  (Nihon Nohyaku Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61150   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of naturally occurring O- and C-prenylated flavanones</p>

    <p>          Kenez Agnes, Juhasz Laszlo, Lestar Zsombor, Lenkey Bela, and Antus Sandor</p>

    <p>          Acta Pharm Hung <b>2007</b>.  77(1): 5-10.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   61151   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          A new in-vitro kinetic model to study the pharmacodynamics of antifungal agents: inhibition of the fungicidal activity of amphotericin B against Candida albicans by voriconazole</p>

    <p>          Lignell A, Johansson A, Lowdin E, Cars O, and Sjolin J</p>

    <p>          Clin Microbiol Infect <b>2007</b>.  13(6): 613-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   61152   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Hepatitis C virus: quasispecies dynamics, virus persistence and antiviral therapy</p>

    <p>          Cristina, Juan </p>

    <p>          Expert Opin. Ther. Pat. <b>2007</b>.  17(5): 499-510</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   61153   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          In vitro model to assess effect to antimicrobial agents on Encephalitozoon cuniculi</p>

    <p>          Beauvais, Bernadette, Sarfati, Claudine, Challier, Svetlana, and Derouin, Francis</p>

    <p>          Antimicrob. Agents Chemother. <b>1994</b>.  38(10): 2440-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>16.   61154   OI-LS-377; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          A new Aspergillus fumigatus resistance mechanism conferring in vitro cross-resistance to azole antifungals involves a combination of cyp51A alterations</p>

    <p>          Mellado, E, Garcia-Effron, G, Alcazar-Fuoli, L, Melchers, WJG, Verweij, PE, Cuenca-Estrella, M, and Rodriguez-Tudela, JL</p>

    <p>          Antimicrob. Agents Chemother. <b>2007</b>.  51(6): 1897-1904</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   61155   OI-LS-377; WOS-OI-6/22/2007</p>

    <p class="memofmt1-2">          Development and Validation of a Non-Radioactive Dna Polymerase Assay for Studying Cytomegalovirus Resistance to Foscarnet</p>

    <p>          Ducancelle, A. <i>et al.</i></p>

    <p>          Journal of Virological Methods <b>2007</b>.  141(2): 212-215</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246083200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246083200013</a> </p><br />

    <p>18.   61156   OI-LS-377; WOS-OI-6/22/2007</p>

    <p class="memofmt1-2">          Synthesis and Anti Tuberculostatic Activity of Novel 1,3,4-Oxadiazole Derivatives</p>

    <p>          Yar, M., Siddiqui, A., and Ali, M.</p>

    <p>          Journal of the Chinese Chemical Society <b>2007</b>.  54(1): 5-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246082100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246082100002</a> </p><br />

    <p>19.   61157   OI-LS-377; WOS-OI-6/22/2007</p>

    <p class="memofmt1-2">          Synthesis of 3-Thioheterocycloyl-1,3,4,5-Tetrahdro-2-Oxo-1h-Benazepine</p>

    <p>          Qiu, Z. <i>et al.</i></p>

    <p>          Chinese Journal of Organic Chemistry <b>2007</b>.  27(5): 607-618</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246645800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246645800008</a> </p><br />

    <p>20.   61158   OI-LS-377; WOS-OI-6/22/2007</p>

    <p class="memofmt1-2">          Viral Hepatitis and Hiv in Africa</p>

    <p>          Modi, A. and Feld, J.</p>

    <p>          Aids Reviews <b>2007</b>.  9(1): 25-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245909500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245909500003</a> </p><br />

    <p>21.   61159   OI-LS-377; PUBMED-OI-6/25/2007</p>

    <p class="memofmt1-2">          Does cyclosporine inhibit in vivo hepatitis C virus replication? - a pilot study</p>

    <p>          Wawrzynowicz-Syczewska, M, Herman, E, Jurczyk, K, Karpinska, E, Laurans, L, Lubikowski, J, and Boron-Kaczmarska, A</p>

    <p>          Transpl Int <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17578456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17578456&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   61160   OI-LS-377; PUBMED-OI-6/25/2007</p>

    <p class="memofmt1-2">          Structural insights into catalysis and inhibition of O-acetylserine sulfhydrylase from Mycobacterium tuberculosis: Crystal structures of the enzyme - alpha -aminoacrylate intermediate and an enzyme-inhibitor complex</p>

    <p>          Schnell, R, Oehlmann, W, Singh, M, and Schneider, G</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17567578&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17567578&amp;dopt=abstract</a> </p><br />

    <p>23.   61161   OI-LS-377; PUBMED-OI-6/25/2007</p>

    <p class="memofmt1-2">          Antiviral action of interferon-alpha against hepatitis C virus replicon and its modulation by interferon-gamma and interleukin-8</p>

    <p>          Jia, Y, Wei, L, Jiang, D, Wang, J, Cong, X, and Fei, R</p>

    <p>          J Gastroenterol Hepatol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17565587&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17565587&amp;dopt=abstract</a> </p><br />

    <p>24.   61162   OI-LS-377; PUBMED-OI-6/25/2007</p>

    <p class="memofmt1-2">          Characterization of S-adenosylhomocysteine hydrolase from Cryptosporidium parvum</p>

    <p>          Ctrnacta, V, Stejskal, F, Keithly, JS, and Hrdy, I</p>

    <p>          FEMS Microbiol Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17559404&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17559404&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
