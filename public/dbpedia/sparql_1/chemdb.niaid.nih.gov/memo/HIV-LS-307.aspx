

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-307.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CQuuCC7eFAGSrsMW8VV6RKPYdmH9EbXuzoV9Wf3rFhun2TMgOEVkNLf9BgOU926ATzrTefs5odJeLPmIh40Xhn7PgjuLMs/YcqXQ6ae1mejUV3TeFqJkIWRiblU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C90D4FB9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-307-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44188   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Application of the Synthetic Amino Sugars for Glyco-Diversification: Synthesis and Antimicrobial Studies of Pyranmycin</p>

    <p>          Elchert, Bryan, Li, Jie, Wang, Jinhua, Hui, Yu, Rai, Ravi, Ptak, Roger, Ward, Priscilla, Takemoto, Jon Y, Bensaci, Mekki, and Chang, Cheng-Wei Tom</p>

    <p>          Journal of Organic Chemistry <b>2004</b>.  69(5): 1513-1523</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>2.         44189   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Punica granatum (Pomegranate) juice provides an HIV-1 entry inhibitor and candidate topical microbicide</p>

    <p>          Neurath, AR, Strick, N, Li, YY, and Debnath, AK</p>

    <p>          BMC Infect Dis  <b>2004</b>.  4(1): 41</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15485580&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15485580&amp;dopt=abstract</a> </p><br />

    <p>3.         44190   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Use of multivalent glyco-dendrimers to inhibit the activity of human immunodeficiency virus</p>

    <p>          Schengrund, Cara-Lynne, Kensinger, Richard D, and Rosa, Borges Andrew</p>

    <p>          PATENT:  US <b>2004180852</b>  ISSUE DATE:  20040916</p>

    <p>          APPLICATION: 2004-10275  PP: 22 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>4.         44191   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Discovery of potent pyrrolidone-based HIV-1 protease inhibitors with enhanced drug-like properties</p>

    <p>          Kazmierski, WM, Andrews, W, Furfine, E, Spaltenstein, A, and Wright, L</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(22): 5689-92</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482949&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482949&amp;dopt=abstract</a> </p><br />

    <p>5.         44192   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Potent inhibitors of the HIV-1 protease incorporating cyclic urea P1-P2 scaffold</p>

    <p>          Kazmierski, WM, Furfine, E, Gray-Nunez, Y, Spaltenstein, A, and Wright, L</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(22): 5685-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482948&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482948&amp;dopt=abstract</a> </p><br />

    <p>6.         44193   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of pyrazole derivatives as non-nucleoside reverse transcriptase inhibitors for the treatment of HIV disorders and compositions thereof</p>

    <p>          Dunn, James Patrick, Hogg, Joan Heather, Mirzadegan, Taraneh, and Swallow, Steven</p>

    <p>          PATENT:  WO <b>2004074257</b>  ISSUE DATE:  20040902</p>

    <p>          APPLICATION: 2004  PP: 90 pp.</p>

    <p>          ASSIGNEE:  (F. Hoffmann-La Roche A.-G., Switz.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>7.         44194   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Synthesis and HIV-1 integrase inhibitory activity of dimeric and tetrameric analogs of indolicidin</p>

    <p>          Krajewski, K, Marchand, C, Long, YQ, Pommier, Y, and Roller, PP</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(22): 5595-5598</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482931&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482931&amp;dopt=abstract</a> </p><br />

    <p>8.         44195   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Antiviral agents containing nitrogen-containing heteroaromatic compounds</p>

    <p>          Fuji, Masahiro, Matsushita, Shihaku, and Mikamiyama, Hidenori</p>

    <p>          PATENT:  JP <b>2004244320</b>  ISSUE DATE:  20040902</p>

    <p>          APPLICATION: 2003-32772  PP: 54 pp.</p>

    <p>          ASSIGNEE:  (Shionogi and Co., Ltd. Japan</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>9.         44196   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 reverse transcription: basic principles of drug action and resistance</p>

    <p>          Gotte, M</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(5): 707-16</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482234&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482234&amp;dopt=abstract</a> </p><br />

    <p>10.       44197   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Structure Modifications of 6-Aminoquinolones with Potent Anti-HIV Activity(1)</p>

    <p>          Tabarrini, O, Stevens, M, Cecchetti, V, Sabatini, S, Dell&#39;uomo, M, Manfroni, G, Palumbo, M, Pannecouque, C, De, Clercq E, and Fravolini, A</p>

    <p>          J Med Chem <b>2004</b>.  47(22): 5567-5578</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481992&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481992&amp;dopt=abstract</a> </p><br />

    <p>11.       44198   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          4-Benzyl- and 4-Benzoyl-3-dimethylaminopyridin-2(1H)-ones, a New Family of Potent Anti-HIV Agents: Optimization and in Vitro Evaluation against Clinically Important HIV Mutant Strains</p>

    <p>          Benjahad, A, Courte, K, Guillemont, J, Mabire, D, Coupa, S, Poncelet, A, Csoka, I, Andries, K, Pauwels, R, de, Bethune MP, Monneret, C, Bisagni, E, Nguyen, CH, and Grierson, DS</p>

    <p>          J Med Chem <b>2004</b>.  47(22): 5501-5514</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481987&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481987&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.       44199   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          RNase S complex bearing arginine-rich peptide and anti-HIV activity</p>

    <p>          Futaki, S, Nakase, I, Suzuki, T, Nameki, D, Kodama, EI, Matsuoka, M, and Sugiura, Y</p>

    <p>          J Mol Recognit  <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476294&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476294&amp;dopt=abstract</a> </p><br />

    <p>13.       44200   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Zinc coupling potentiates anti-HIV-1 activity of baicalin</p>

    <p>          Wang, Q, Wang, YT, Pu, SP, and Zheng, YT</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  324(2): 605-610</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474470&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474470&amp;dopt=abstract</a> </p><br />

    <p>14.       44201   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Two ellagitannins from the leaves of Terminalia tri fl ora with inhibitory activity on HIV-1 reverse transcriptase</p>

    <p>          Martino, V, Morales, J, Martinez-Irujo, JJ, Font, M, Monge, A, and Coussio, J</p>

    <p>          Phytother Res <b>2004</b>.  18(8): 667-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15472920&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15472920&amp;dopt=abstract</a> </p><br />

    <p>15.       44202   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Targeting a binding pocket within the trimer-of-hairpins: Small-molecule inhibition of viral fusion</p>

    <p>          Cianci, C, Langley, DR, Dischino, DD, Sun, Y, Yu, KL, Stanley, A, Roach, J, Li, Z, Dalterio, R, Colonno, R, Meanwell, NA, and Krystal, M</p>

    <p>          Proc Natl Acad Sci U S A <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469910&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469910&amp;dopt=abstract</a> </p><br />

    <p>16.       44203   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Cytotoxic ribosome-inactivating lectins from plants</p>

    <p>          Hartley, MR and Lord, JM</p>

    <p>          Biochim Biophys Acta <b>2004</b>.  1701(1-2): 1-14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15450171&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15450171&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.       44204   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          HIV-1 Vpr enhances production of receptor of activated NF-kappaB ligand (RANKL) via potentiation of glucocorticoid receptor activity</p>

    <p>          Fakruddin, JM and Laurence, J</p>

    <p>          Arch Virol <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15449141&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15449141&amp;dopt=abstract</a> </p><br />

    <p>18.       44205   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Agents with leukotriene B4-like antiviral (DNA) and anti-neoplastic activities</p>

    <p>          Gosselin, Jean and Borgeat, Pierre</p>

    <p>          PATENT:  US <b>2004132820</b>  ISSUE DATE:  20040708</p>

    <p>          APPLICATION: 2003-28522  PP: 26 pp., Cont.-in-part of U.S. Ser. No. 548,187, abandoned.</p>

    <p>          ASSIGNEE:  (Can.)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>19.       44206   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Synthesis and biological properties of fullerene-containing amino acids and peptides</p>

    <p>          Pantarotto, D, Tagmatarchis, N, Bianco, A, and Prato, M</p>

    <p>          Mini Rev Med Chem <b>2004</b>.  4(7): 805-14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15379647&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15379647&amp;dopt=abstract</a> </p><br />

    <p>20.       44207   HIV-LS-307; PUBMED-HIV-10/20/2004</p>

    <p class="memofmt1-2">          Comparing the accumulation of active- and nonactive-site mutations in the HIV-1 protease</p>

    <p>          Clemente, JC, Moose, RE, Hemrajani, R, Whitford, LR, Govindasamy, L, Reutzel, R, McKenna, R, Agbandje-McKenna, M, Goodenow, MM, and Dunn, BM</p>

    <p>          Biochemistry <b>2004</b>.  43(38): 12141-51</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15379553&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15379553&amp;dopt=abstract</a> </p><br />

    <p>21.       44208   HIV-LS-307; WOS-HIV-10/10/2004</p>

    <p class="memofmt1-2">          Heat shock protein 70 protects cells from cell cycle arrest and apoptosis induced by human immunodeficiency virus type 1 viral protein R</p>

    <p>          Iordanskiy, S, Zhao, YP, Dubrovsky, L, Iordanskaya, T, Chen, MZ, Liang, D, and Bukrinsky, M</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(18): 9697-9704, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100013</a> </p><br />

    <p>22.       44209   HIV-LS-307; WOS-HIV-10/10/2004</p>

    <p class="memofmt1-2">          Independent evolution of human immunodeficiency virus (HIV) drug resistance mutations in diverse areas of the brain in HIV-infected patients, with and without dementia, on antiretroviral treatment</p>

    <p>          Smit, TK, Brew, BJ, Tourtellotte, W, Morgello, S, Gelman, BB, and Saksena, NK</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(18): 10133-10148, 16</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100057</a> </p><br />
    <br clear="all">

    <p>23.       44210   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Genotype and phenotype patterns of human immunodeficiency virus type 1 resistance to enfuvirtide during long-term treatment</p>

    <p>          Menzo, Stefano, Castagna, Antonella, Monachetti, Alessia, Hasson, Hamid, Danise, Anna, Carini, Elisabetta, Bagnarelli, Patrizia, Lazzarin, Adriano, and Clementi, Massimo</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(9): 3253-3259</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       44211   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Stereoselective Synthesis of Sugar-Modified Enyne Analogues of Adenosine and Uridine. Interaction with S-Adenosyl-L-homocysteine Hydrolase and Antiviral and Cytotoxic Effects</p>

    <p>          Wnuk, Stanislaw F, Lewandowska, Elzbieta, Sacasa, Pablo R, Crain, Leigh N, Zhang, Jinsong, Borchardt, Ronald T, and De Clercq, Erik</p>

    <p>          Journal of Medicinal Chemistry <b>2004</b>.  47(21): 5251-5257</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>25.       44212   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Finke, Paul E</p>

    <p>          &lt;10 Journal Title&gt; <b>2004</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       44213   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of b-carboline hydroxamic acids as HIV-integrase inhibitors</p>

    <p>          Kuki, Atsuo, Li, Xinqiang, Plewe, Michael Bruno, Wang, Hai, and Zhang, Junhu</p>

    <p>          PATENT:  WO <b>2004067531</b>  ISSUE DATE:  20040812</p>

    <p>          APPLICATION: 2004  PP: 57 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       44214   HIV-LS-307; WOS-HIV-10/10/2004</p>

    <p class="memofmt1-2">          Hyper-responsiveness to stimulation of human immunodeficiency virus-infected CD4(+) T cells requires Nef and Tat virus gene products and results from higher NFAT, NF-kappa B, and AP-1 induction</p>

    <p>          Fortin, JF, Barat, C, Beausejour, Y, Barbeau, B, and Tremblay, MJ</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2004</b>.  279(38): 39520-39531, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223791500042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223791500042</a> </p><br />

    <p>28.       44215   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of cyclopeptides as chemokine receptor CXCR4 antagonists</p>

    <p>          Fujii, Nobutaka, Nakajima, Hideki, and Piper, Stephen C</p>

    <p>          PATENT:  JP <b>2004196769</b>  ISSUE DATE:  20040715</p>

    <p>          APPLICATION: 2003-2308  PP: 26 pp.</p>

    <p>          ASSIGNEE:  (Ofan Link Co., Ltd. Japan</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>29.       44216   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p><b>          A preparation of (piperidinyl-N-carboxy)pyrimidine derivatives, useful as CCR5 antagonists</b> </p>

    <p>          Miller, Michael W and Scott, Jack D</p>

    <p>          PATENT:  WO <b>2004056770</b>  ISSUE DATE:  20040708</p>

    <p>          APPLICATION: 2003  PP: 50 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>30.       44217   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of nitrogen-containing heterocyclic compounds as CXCR4 regulators</p>

    <p>          Habashita, Hiromu, Kokubo, Masaya, Shibayama, Shiro, Tada, Hideaki, and Tanihiro, Tatsuya</p>

    <p>          PATENT:  WO <b>2004052862</b>  ISSUE DATE:  20040624</p>

    <p>          APPLICATION: 2003  PP: 641 pp.</p>

    <p>          ASSIGNEE:  (Ono Pharmaceutical Co., Ltd. Japan</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       44218   HIV-LS-307; WOS-HIV-10/10/2004</p>

    <p class="memofmt1-2">          The role of nucleoside and nucleotide reverse transcriptase inhibitor backbones in antiretroviral therapy</p>

    <p>          Young, B</p>

    <p>          JAIDS-JOURNAL OF ACQUIRED IMMUNE DEFICIENCY SYNDROMES <b>2004</b>.  37: S13-S20, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223742100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223742100003</a> </p><br />

    <p>32.       44219   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of 8-hydroxy-1-oxo-tetrahydropyrrolopyrazine compounds as HIV integrase inhibitors</p>

    <p>          Wai, John S</p>

    <p>          PATENT:  WO <b>2004047725</b>  ISSUE DATE:  20040610</p>

    <p>          APPLICATION: 2003  PP: 85 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>33.       44220   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of 4-heteroarylpyrimidines as specific cyclin-dependent kinase inhibitors for treating viruses</p>

    <p>          Wang, Shudong, Meades, Christopher, Wood, Gavin, Blake, David, and Fischer, Peter</p>

    <p>          PATENT:  WO <b>2004043467</b>  ISSUE DATE:  20040527</p>

    <p>          APPLICATION: 2003  PP: 142 pp.</p>

    <p>          ASSIGNEE:  (Cyclacel Limited, UK</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>34.       44221   HIV-LS-307; SCIFINDER-HIV-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of di- and tri-substituted 8-azapurine derivatives as cyclin-dependent kinase inhibitors</p>

    <p>          Fuksova, Kveta, Havlicek, Libor, Krystof, Vladimir, Lenobel, Rene, and Strnad, Miroslav</p>

    <p>          PATENT:  WO <b>2004018473</b>  ISSUE DATE:  20040304</p>

    <p>          APPLICATION: 2003  PP: 143 pp.</p>

    <p>          ASSIGNEE:  (Institute of Experimental Botany ASCR, Czech Rep.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>35.       44222   HIV-LS-307; WOS-HIV-10/10/2004</p>

    <p class="memofmt1-2">          An approach towards the quantitative structure-activity relationships of caffeic acid and its derivatives</p>

    <p>          Verma, RR and Hansch, C</p>

    <p>          CHEMBIOCHEM <b>2004</b>.  5(9): 1188-1195, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223811800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223811800005</a> </p><br />

    <p>36.       44223   HIV-LS-307; WOS-HIV-10/10/2004</p>

    <p class="memofmt1-2">          Small molecules that mimic components of bioactive protein surfaces</p>

    <p>          Fairlie, DP</p>

    <p>          AUSTRALIAN JOURNAL OF CHEMISTRY <b>2004</b>.  57(9): 855-857, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223736700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223736700005</a> </p><br />

    <p>37.       44224   HIV-LS-307; WOS-HIV-10/17/2004</p>

    <p class="memofmt1-2">          Conformational changes in HIV-1 reverse transcriptase induced by nonnucleoside reverse transcriptase inhibitor binding</p>

    <p>          Sluis-Cremer, N, Temiz, NA, and Bahar, I</p>

    <p>          CURRENT HIV RESEARCH <b>2004</b>.  2(4): 323-332, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854200004</a> </p><br />

    <p>38.       44225   HIV-LS-307; WOS-HIV-10/17/2004</p>

    <p class="memofmt1-2">          Stimulation of toll-like receptor 2 in mononuclear cells from HIV-infected patients induces chemokine responses: possible pathogenic consequences</p>

    <p>          Heggelund, L, Damas, JK, Yndestad, A, Holm, AM, Muller, F, Lien, E, Espevik, T, Aukrust, P, and Froland, SS</p>

    <p>          CLINICAL AND EXPERIMENTAL IMMUNOLOGY <b>2004</b>.  138(1): 116-121, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223953500016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223953500016</a> </p><br />

    <p>39.       44226   HIV-LS-307; WOS-HIV-10/17/2004</p>

    <p class="memofmt1-2">          A simple and highly efficient preparation of structurally diverse Aryl beta-diketoacids as HIV-1 integrase inhibitors</p>

    <p>          Jiang, XH and Long, YQ</p>

    <p>          CHINESE JOURNAL OF CHEMISTRY <b>2004</b>.  22(9): 978-983, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223842000020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223842000020</a> </p><br />

    <p>40.       44227   HIV-LS-307; WOS-HIV-10/17/2004</p>

    <p class="memofmt1-2">          Heat-shock protein 70 exerts opposing effects on Vpr-dependent and Vpr-independent HIV-1 replication in macrophages</p>

    <p>          Iordanskiy, S, Zhao, YQ, DiMarzio, P, Agostini, I, Dubrovsky, L, and Bukrinsky, M</p>

    <p>          BLOOD <b>2004</b>.  104(6): 1867-1872, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223818600046">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223818600046</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
