

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="us5X+zWr6A0TMAYzI5wbG+6ltfx27CB9wjLWeDLAw4ELxfBBu7EonITSH+dx2CY8meDpcSFejzvity2TXXouZ9r1H3DRGD6f5srkFZsfDoOW91B4Swk6E7XGm2U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="98C5240C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: December, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26540494">Anti-HIV-1 Activity of a Tripodal Receptor That Recognizes Mannose Oligomers.</a> Rivero-Buceta, E., P. Carrero, E. Casanova, E.G. Doyaguez, A. Madrona, E. Quesada, M.J. Perez-Perez, R. Mateos, L. Bravo, L. Mathys, S. Noppen, E. Kiselev, C. Marchand, Y. Pommier, S. Liekens, J. Balzarini, M.J. Camarasa, and A. San-Felix. European Journal of Medicinal Chemistry, 2015. 106: p. 132-143. PMID[26540494].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26613134">Design and Synthesis of Diselenobisbenzamides (Disebas) as Nucleocapsid Protein 7 (Ncp7) Inhibitors with anti-HIV Activity.</a> Sancineto, L., A. Mariotti, L. Bagnoli, F. Marini, J. Desantis, N. Iraci, C. Santi, C. Pannecouque, and O. Tabarrini. Journal of Medicinal Chemistry, 2015. 58(24): p. 9601-9614. PMID[26613134].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26462551">Design, Synthesis, Biological Evaluation and X-ray Structural Studies of HIV-1 Protease Inhibitors Containing Substituted Fused-tetrahydropyranyl Tetrahydrofuran as P2-ligands.</a> Ghosh, A.K., C.D. Martyr, L.A. Kassekert, P.R. Nyalapatla, M. Steffey, J. Agniswamy, Y.F. Wang, I.T. Weber, M. Amano, and H. Mitsuya. Organic &amp; Biomolecular Chemistry, 2015. 13(48): p. 11607-11621. PMID[26462551]. PMCID[PMC4666783].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26632657">Flueggether A and Virosinine A, anti-HIV Alkaloids from Flueggea virosa.</a> Zhang, H., K.K. Zhu, Y.S. Han, C. Luo, M.A. Wainberg, and J.M. Yue. Organic Letters, 2015. 17(24): p. 6274-6277. PMID[26632657].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26565115">Nanogel-conjugated Reverse Transcriptase Inhibitors and Their Combinations as Novel Antiviral Agents with Increased Efficacy against HIV-1 Infection.</a> Senanayake, T.H., S. Gorantla, E. Makarov, Y. Lu, G. Warren, and S.V. Vinogradov. Molecular Pharmaceutics, 2015. 12(12): p. 4226-4236. PMID[26565115].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26529558">Nanoparticle-based ARV Drug Combinations for Synergistic Inhibition of Cell-free and Cell-Cell HIV Transmission.</a> Jiang, Y., S. Cao, D.K. Bright, A.M. Bever, A.K. Blakney, I.T. Suydam, and K.A. Woodrow. Molecular Pharmaceutics, 2015. 12(12): p. 4363-4374. PMID[26529558].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26643614">A Novel Class of HIV-1 Antiviral Agents Targeting HIV via a SUMOylation-dependent Mechanism.</a> Madu, I.G., S. Li, B. Li, H. Li, T. Chang, Y.J. Li, R. Vega, J. Rossi, J.K. Yee, J. Zaia, and Y. Chen. Scientific Reports, 2015. 5(17808): 8pp. PMID[26643614]. PMCID[PMC4672295].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26392486">The Nucleoside Analog BMS-986001 Shows Greater in Vitro Activity against HIV-2 than against HIV-1.</a> Smith, R.A., D.N. Raugi, V.H. Wu, S.S. Leong, K.M. Parker, M.K. Oakes, P.S. Sow, S. Ba, M. Seydi, and G.S. Gottlieb. Antimicrobial Agents and Chemotherapy, 2015. 59(12): p. 7437-7446. PMID[26392486]. PMCID[PMC4649195].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25989218">A Resveratrol Analog Termed 3,3&#39;,4,4&#39;,5,5&#39;-Hexahydroxy-trans-stilbene is a Potent HIV-1 Inhibitor.</a> Han, Y.S., P.K. Quashie, T. Mesplede, H. Xu, Y. Quan, W. Jaeger, T. Szekeres, and M.A. Wainberg. Journal of Medical Virology, 2015. 87(12): p. 2054-2060. PMID[25989218].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26630969">SJP-L-5, a Novel Small-molecule Compound, Inhibits HIV-1 Infection by Blocking Viral DNA Nuclear Entry.</a> Bai, R., X.J. Zhang, Y.L. Li, J.P. Liu, H.B. Zhang, W.L. Xiao, J.X. Pu, H.D. Sun, Y.T. Zheng, and L.X. Liu. BMC Microbiology, 2015. 15(274): 13pp. PMID[26630969]. PMCID[PMC4667461].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26669976">A Small Molecule Compound IMB-LA Inhibits HIV-1 Infection by Preventing Viral Vpu from Antagonizing the Host Restriction Factor BST-2.</a> Mi, Z., J. Ding, Q. Zhang, J. Zhao, L. Ma, H. Yu, Z. Liu, G. Shan, X. Li, J. Zhou, T. Wei, L. Zhang, F. Guo, C. Liang, and S. Cen. Scientific Reports, 2015. 5(18499): 13pp. PMID[26669976]. PMCID[PMC4680884].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26701275">Synthesis of a Vpr-binding Derivative for Use as a Novel HIV-1 Inhibitor.</a> Hagiwara, K., H. Ishii, T. Murakami, S.N. Takeshima, N. Chutiwitoonchai, E.N. Kodama, K. Kawaji, Y. Kondoh, K. Honda, H. Osada, Y. Tsunetsugu-Yokota, M. Suzuki, and Y. Aida. Plos One, 2015. 10(12): p. e0145573. PMID[26701275]. PMCID[PMC4689350].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26602829">Synthesis, Antiviral Activity and Resistance of a Novel Small Molecule HIV-1 Entry Inhibitor.</a> Curreli, F., K. Haque, L. Xie, Q. Qiu, J. Xu, W. Yong, X. Tong, and A.K. Debnath. Bioorganic &amp; Medicinal Chemistry, 2015. 23(24): p. 7618-7628. PMID[26602829]. PMCID[PMC4684972].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26599611">Synthesis, Binding and Antiviral Properties of Potent Core-extended Naphthalene diimides Targeting the HIV-1 Long Terminal Repeat Promoter G-quadruplexes.</a> Perrone, R., F. Doria, E. Butovskaya, I. Frasson, S. Botti, M. Scalabrin, S. Lago, V. Grande, M. Nadai, M. Freccero, and S.N. Richter. Journal of Medicinal Chemistry, 2015. 58(24): p. 9639-9652. PMID[26599611].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_12_2015.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26077260">Effective Inhibition of HIV-1 Production by Short Hairpin RNAs and Small Interfering RNAs Targeting a Highly Conserved Site in HIV-1 Gag RNA is Optimized by Evaluating Alternative Length Formats.</a> Scarborough, R.J., K.L. Adams, A. Daher, and A. Gatignol. Antimicrobial Agents and Chemotherapy, 2015. 59(9): p. 5297-5305. PMID[26077260]. PMCID[PMC4538472].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26598461">Fluorinated Betulinic acid Derivatives and Evaluation of their anti-HIV Activity.</a> Li, J.Z., M. Goto, X.M. Yang, S.L. Morris-Natschke, L. Huang, C.H. Chen, and K.H. Lee. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(1): p. 68-71. PMID[26598461].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26610490">Henrin A: A New anti-HIV Ent-Kaurane Diterpene from Pteris henryi.</a> Li, W.F., J. Wang, J.J. Zhang, X. Song, C.F. Ku, J. Zou, J.X. Li, L.J. Rong, L.T. Pan, and H.J. Zhang. International Journal of Molecular Sciences, 2015. 16(11): p. 27978-27987. PMID[26610490]. PMCID[PMC4661929].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26584882">Inhibitors of HIV-1 Attachment: The Discovery and Structure-activity Relationships of Tetrahydroisoquinolines as Replacements for the Piperazine Benzamide in the 3-Glyoxylyl 6-azaindole Pharmacophore.</a> Swidorski, J.J., Z. Liu, Z.W. Yin, T. Wang, D.J. Carini, S. Rahematpura, M. Zheng, K. Johnson, S. Zhang, P.F. Lin, D.D. Parker, W.Y. Li, N.A. Meanwell, L.G. Hamann, and A. Regueiro-Ren. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(1): p. 160-167. PMID[26584882].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26615887">Synthesis of the Biotinylated anti-HIV Compound BMMP and the Target Identification Study.</a> Kamo, M., H. Tateishi, R. Koga, Y. Okamoto, M. Otsuka, and M. Fujita. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(1): p. 43-45. PMID[26615887].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26474665">Synthetic, Structural Mimetics of the beta-Hairpin Flap of HIV-1 Protease Inhibit Enzyme Function.</a> Chauhan, J., S.E. Chen, K.J. Fenstermacher, A. Naser-Tavakolian, T. Reingewertz, R. Salmo, C. Lee, E. Williams, M. Raje, E. Sundberg, J.J. DeStefano, E. Freire, and S. Fletcher. Bioorganic &amp; Medicinal Chemistry, 2015. 23(21): p. 7095-7109. PMID[26474665]. PMCID[PMC4661106].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26513643">Tryptophan Dendrimers that Inhibit HIV Replication, Prevent Virus Entry and Bind to the HIV Envelope Glycoproteins gp120 and gp41.</a> Rivero-Buceta, E., E.G. Doyaguez, I. Colomer, E. Quesada, L. Mathys, S. Noppen, S. Liekens, M.J. Camarasa, M.J. Perez-Perez, J. Balzarini, and A. San-Felix. European Journal of Medicinal Chemistry, 2015. 106: p. 34-43. PMID[26513643].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26465962">Unciaphenol, an Oxygenated Analogue of the Bergman Cyclization Product of Uncialamycin Exhibits anti-HIV Activity.</a> Williams, D.E., H. Bottriell, J. Davies, I. Tietjen, M.A. Brockman, and R.J. Andersen. Organic Letters, 2015. 17(21): p. 5304-5307. PMID[26465962].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_12_2015.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151126&amp;CC=WO&amp;NR=2015179448A1&amp;KC=A1">Preparation of Quinoline Derivatives Useful in Treatment of HIV Infection.</a> Babaoglu, K., R. McFadden, and M.L. Mitchell. Patent. 2015. 2015-US31640 2015179448: 150pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151211&amp;CC=FR&amp;NR=3021871A1&amp;KC=A1">Composition Containing used Anthostema senegalense Like Drug against the AIDS [Machine Translation].</a> Balde, A.M. Patent. 2015. 2014-55226 3021871.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151119&amp;CC=WO&amp;NR=2015175855A1&amp;KC=A1">Preparation of Benzylpyrazolylpiperidine Compounds as Chemokine CXCR4 and CCR5 Receptor Modulators.</a> Cox, B.D., L.J. Wilson, A. Prosser, D.C. Liotta, and J.P. Snyder. Patent. 2015. 2015-US30923 2015175855: 98pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151104&amp;CC=CN&amp;NR=105017139A&amp;KC=A">Preparation and Application of 3,4-Disubstituted-6-phenethylpyridone-type HIV-1 Reverse Transcriptase Inhibitor.</a> Liu, J., X. Wang, Y. Cao, Z. Zhang, Y. Guo, and C. Tian. Patent. 2015. 2014-10154784 105017139: 24pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151203&amp;CC=WO&amp;NR=2015181387A1&amp;KC=A1">Preparation of Diaminopiperidine Derivatives and Analogs for Use as anti-HIV Agents.</a> Micouin, L., A. Blond, V. Calvez, A.-G. Marcellin, C. Soulie, and E. Corrot. Patent. 2015. 2015-EP62046 2015181387: 74pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151210&amp;CC=WO&amp;NR=2015187998A2&amp;KC=A2">Preparation of Peptide Analogs as Inhibitor of Apoptosis Protein (IAP) Antagonists in HIV Therapy.</a> Pache, L., S.K. Chanda, M.D. Vamos, N.D.P. Cosford, P. Teriete, J. Marlett, A. Diaz, and J.A.T. Young. Patent. 2015. 2015-US34281 2015187998: 245pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2015.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151217&amp;CC=US&amp;NR=2015361152A1&amp;KC=A1">Dimerization-resistant CXCl121 Variants and Their Therapeutic Uses.</a> Volkman, B., J. Ziarek, C. Veldkamp, and F. Peterson. Patent. 2015. 2015-14736535 20150361152: 69pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
