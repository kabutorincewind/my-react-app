

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-324.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XtTFNhivMNEMlSasXx1QoP/wYteI+NbBvoJIbkjV38ImqU8EyFdOITxQh/86SeVrCP2FkRaInqx17Td/Pqo2fY/o4pPXXnzgovAVyH/6JbGW7ZlLnB0GDQCfXf8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7BD56C91" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-324-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45390   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Anti-HIV Activity of (-)-(2R,4R)-1- (2-Hydroxymethyl-1,3-dioxolan-4-yl)- thymine against Drug-Resistant HIV-1 Mutants and Studies of Its Molecular Mechanism</p>

    <p>          Chu, CK, Yadav, V, Chong, YH, and Schinazi, RF</p>

    <p>          J Med Chem <b>2005</b>.  48(12): 3949-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15943470&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15943470&amp;dopt=abstract</a> </p><br />

    <p>2.     45391   HIV-LS-324; EMBASE-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Synthesis, biological activity and modelling studies of two novel anti HIV PR inhibitors with a thiophene containing hydroxyethylamino core</p>

    <p>          Bonini, Carlo, Chiummiento, Lucia, Bonis, Margherita De, Funicello, Maria, Lupattelli, Paolo, Suanno, Gerardina, Berti, Federico, and Campaner, Pietro</p>

    <p>          Tetrahedron <b>2005</b>.  61(27): 6580-6589</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4G94HCD-2/2/05865399520191d9c542c145eb68dae8">http://www.sciencedirect.com/science/article/B6THR-4G94HCD-2/2/05865399520191d9c542c145eb68dae8</a> </p><br />

    <p>3.     45392   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Combination of a mutagenic agent with a reverse transcriptase inhibitor results in systematic inhibition of HIV-1 infection</p>

    <p>          Tapia, N, Fernandez, G, Parera, M, Gomez-Mariano, G, Clotet, B, Quinones-Mateu, M, Domingo, E, and Martinez, MA</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15939449&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15939449&amp;dopt=abstract</a> </p><br />

    <p>4.     45393   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Generation and properties of a human immunodeficiency virus type 1 isolate resistant to the small molecule CCR5 inhibitor, SCH-417690 (SCH-D)</p>

    <p>          Marozsan, AJ, Kuhmann, SE, Morgan, T, Herrera, C, Rivera-Troche, E, Xu, S, Baroudy, BM, Strizki, J, and Moore, JP</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15935415&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15935415&amp;dopt=abstract</a> </p><br />

    <p>5.     45394   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Two new chiral nucleoside analogues: (4R,5R)-4-(4,6-dimethylpyrimidin-2-ylsulfanyl)-5-[(2S,5R)-2-isopropyl-5-me thylcyclohexyloxy]-2,3,4,5-tetrahydrofuran-2-one and (4R,5R)-5-[(2S,5R)-isopropyl-5-methylcyclohexyloxy]-4-(4-methyl-1,3-thiazo l-2-ylamino)-2,3,4,5-tetrahydrofuran-2-one</p>

    <p>          He, L, Liu, YM, Zhang, W, and Chen, QH</p>

    <p>          Acta Crystallogr C <b>2005</b>.  61(Pt 6): 390-2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15930693&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15930693&amp;dopt=abstract</a> </p><br />

    <p>6.     45395   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Design, synthesis and bioactivities of TAR RNA targeting beta-carboline derivatives based on Tat-TAR interaction</p>

    <p>          Yu, X, Lin, W, Pang, R, and Yang, M</p>

    <p>          Eur J Med Chem  <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15925430&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15925430&amp;dopt=abstract</a> </p><br />

    <p>7.     45396   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of novel 4-hydroxypyrone derivatives as HIV-1 protease inhibitors</p>

    <p>          Sun, CL, Pang, RF, Zhang, H, and Yang, M</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15923115&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15923115&amp;dopt=abstract</a> </p><br />

    <p>8.     45397   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Bromophenols Coupled with Derivatives of Amino Acids and Nucleosides from the Red Alga Rhodomela confervoides</p>

    <p>          Zhao, J, Ma, M, Wang, S, Li, S, Cao, P, Yang, Y, Lu, Y, Shi, J, Xu, N, Fan, X, and He, L</p>

    <p>          J Nat Prod <b>2005</b>.  68(5): 691-694</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15921411&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15921411&amp;dopt=abstract</a> </p><br />

    <p>9.     45398   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Highly potent anti-HIV-1 activity isolated from fermented Polygonum tinctorium Aiton</p>

    <p>          Zhong, Y, Yoshinaka, Y, Takeda, T, Shimizu, N, Yoshizaki, S, Inagaki, Y, Matsuda, S, Honda, G, Fujii, N, and Yamamoto, N</p>

    <p>          Antiviral Res <b>2005</b>.  66(2-3): 119-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911029&amp;dopt=abstract</a> </p><br />

    <p>10.   45399   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Synthesis of new phorbol derivatives having ethereal side chain and evaluation of their anti-HIV activity</p>

    <p>          Matsuya, Y, Yu, Z, Yamamoto, N, Mori, M, Saito, H, Takeuchi, M, Ito, M, and Nemoto, H</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908223&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908223&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   45400   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Lancifodilactone G: a unique nortriterpenoid isolated from Schisandra lancifolia and its anti-HIV activity</p>

    <p>          Xiao, WL, Zhu, HJ, Shen, YH, Li, RT, Li, SH, Sun, HD, Zheng, YT, Wang, RR, Lu, Y, Wang, C, and Zheng, QT</p>

    <p>          Org Lett <b>2005</b>.  7(11): 2145-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15901155&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15901155&amp;dopt=abstract</a> </p><br />

    <p>12.   45401   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          DNA aptamers as potential anti-HIV agents</p>

    <p>          Chou, SH, Chin, KH, and Wang, AH</p>

    <p>          Trends Biochem Sci <b>2005</b>.  30(5): 231-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15896739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15896739&amp;dopt=abstract</a> </p><br />

    <p>13.   45402   HIV-LS-324; PUBMED-HIV-6/13/2005</p>

    <p class="memofmt1-2">          Identification of Amino Acid Residues in the Human Immunodeficiency Virus Type-1 Reverse Transcriptase Tryptophan-repeat Motif that are Required for Subunit Interaction Using Infectious Virions</p>

    <p>          Mulky, A, Sarafianos, SG, Jia, Y, Arnold, E, and Kappes, JC</p>

    <p>          J Mol Biol <b>2005</b>.  349(4): 673-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893326&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893326&amp;dopt=abstract</a> </p><br />

    <p>14.   45403   HIV-LS-324; WOS-HIV-6/5/2005</p>

    <p class="memofmt1-2">          Design and expression of a Trojan horse anti-HIV peptide</p>

    <p>          Barnes, DR, Nalivaika, EA, Schiffer, CA, and Osterhout, JJ</p>

    <p>          PROTEIN SCIENCE <b>2004</b>.  13: 197-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224796100447">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224796100447</a> </p><br />

    <p>15.   45404   HIV-LS-324; WOS-HIV-6/5/2005</p>

    <p class="memofmt1-2">          Application of phosphoramidate pronucleotide technology to abacavir leads to a significant enhancement of antiviral potency</p>

    <p>          McGuigan, C, Harris, SA, Daluge, SM, Gudmundsson, KS, McLean, EW, Burnette, TC, Marr, H, Hazen, R, Condreay, LD, Johnson, L, De, Clercq E, and Balzarini, J</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(10): 3504-3515, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229129700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229129700009</a> </p><br />

    <p>16.   45405   HIV-LS-324; WOS-HIV-6/5/2005</p>

    <p class="memofmt1-2">          The fusion activity of HIV-1 gp41 depends on interhelical interactions</p>

    <p>          Suntoke, TR and Chan, DC</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(20): 19852-19857, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229113700053">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229113700053</a> </p><br />

    <p>17.   45406   HIV-LS-324; WOS-HIV-6/5/2005</p>

    <p class="memofmt1-2">          Suppression of HIV-1 infection in primary CD4 T cells transduced with a self-inactivating lentiviral vector encoding a membrane expressed gp41-derived fusion inhibitor</p>

    <p>          Perez, EE, Riley, JL, Carroll, RG, von Laer, D, and June, CH</p>

    <p>          CLINICAL IMMUNOLOGY <b>2005</b>.  115(1): 26-32, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229096800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229096800005</a> </p><br />

    <p>18.   45407   HIV-LS-324; WOS-HIV-6/12/2005</p>

    <p class="memofmt1-2">          Natural resistance of human immunodeficiency virus type 2 to zidovudine</p>

    <p>          Reid, P, MacInnes, H, Cong, ME, Heneine, W, and Garcia-Lerma, JG</p>

    <p>          VIROLOGY <b>2005</b>.  336(2): 251-264, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229304600013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229304600013</a> </p><br />

    <p>19.   45408   HIV-LS-324; WOS-HIV-6/12/2005</p>

    <p class="memofmt1-2">          Modification and structure-activity relationship of a small molecule HIV-1 inhibitor targeting the viral envelope glycoprotein gp120</p>

    <p>          Wang, JS, Le, N, Heredia, A, Song, HJ, Redfield, R, and Wang, LX</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2005</b>.  3(9): 1781-1786, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228719200044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228719200044</a> </p><br />

    <p>20.   45409   HIV-LS-324; WOS-HIV-6/12/2005</p>

    <p class="memofmt1-2">          Resistance of human immunodeficiency virus type 1 to the high-mannose binding agents cyanovirin N and concanavalin A</p>

    <p>          Witvrouw, M, Fikkert, V, Hantson, A, Pannecouque, C, O&#39;Keefe, BR, McMahon, J, Stamatatos, L, de, Clercq E, and Bolmstedt, A</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(12): 7777-7784, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100049">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100049</a> </p><br />

    <p>21.   45410   HIV-LS-324; WOS-HIV-6/12/2005</p>

    <p class="memofmt1-2">          Natural antibodies to CCR5 from breast milk block infection of macrophages and dendritic cells with primary R5-tropic HIV-1</p>

    <p>          Bouhlal, H, Latry, V, Requena, M, Aubry, S, Kaveri, SV, Kazatchkine, MA, Belec, L, and Hocini, H</p>

    <p>          JOURNAL OF IMMUNOLOGY <b>2005</b>.  174(11): 7202-7209, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229298400080">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229298400080</a> </p><br />

    <p>22.   45411   HIV-LS-324; WOS-HIV-6/12/2005</p>

    <p class="memofmt1-2">          An old target revisited: Two new privileged skeletons and an unexpected binding mode for HIV-protease inhibitors</p>

    <p>          Specker, E, Bottcher, J, Lilie, H, Heine, A, Schoop, A, Muller, G, Griebenow, N, and Klebe, G</p>

    <p>          ANGEWANDTE CHEMIE-INTERNATIONAL EDITION <b>2005</b>.  44(20): 3140-3144, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229342600032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229342600032</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
