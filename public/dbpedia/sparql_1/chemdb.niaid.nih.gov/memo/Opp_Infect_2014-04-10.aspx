

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-04-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Mqtw8D1n29bTsViATjYnBPz+biXrURLBySEVN02pXf57Q87yyc2BRGzpJlzT3T1svqeEbQmBKXTY/5VNvJWF4ZmnPd/QYYKup8smADS2iqLa3f98QmKDMHdBFgY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B3AEC9AA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 28 - April 10, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24284780">Repurposing as a Means to Increase the Activity of Amphotericin B and Caspofungin against Candida albicans Biofilms.</a>Delattin, N., K. De Brucker, K. Vandamme, E. Meert, A. Marchand, P. Chaltin, B.P. Cammue, and K. Thevissen. Journal of Antimicrobial Chemotherapy, 2014. 69(4): p. 1035-1044. PMID[24284780].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24387763">In Vitro Activity of Natural Phenolic Compounds against Fluconazole-resistant Candida Species: A Quantitative Structure-Activity Relationship Analysis.</a> Gallucci, M.N., M.E. Carezzano, M.M. Oliva, M.S. Demo, R.P. Pizzolitto, M.P. Zunino, J.A. Zygadlo, and J.S. Dambolena. Journal of Applied Microbiology, 2014. 116(4): p. 795-804. PMID[24387763].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23870852">Antimicrobial Activity of Amazon Astrocaryum aculeatum Extracts and its Association to Oxidative Metabolism.</a> Jobim, M.L., R.C. Santos, C.F. dos Santos Alves, R.M. Oliveira, C.P. Mostardeiro, M.R. Sagrillo, O.C. de Souza Filho, L.F. Garcia, M.F. Manica-Cattani, E.E. Ribeiro, and I.B. da Cruz. Microbiological Research, 2014. 169(4): p. 314-323. PMID[23870852].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24594353">Design and Synthesis of Novel 2H-Chromen-2-one Derivatives Bearing 1,2,3-Triazole Moiety as Lead Antimicrobials.</a>Kushwaha, K., N. Kaushik, Lata, and S.C. Jain. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(7): p. 1795-1801. PMID[24594353].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24514088">In Vitro and in Vivo Activities of Pterostilbene against Candida albicans Biofilms.</a> Li, D.D., L.X. Zhao, E. Mylonakis, G.H. Hu, Y. Zou, T.K. Huang, L. Yan, Y. Wang, and Y.Y. Jiang. Antimicrobial Agents and Chemotherapy, 2014. 58(4): p. 2344-2355. PMID[24514088].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24378871">Hylaranins: Prototypes of a New Class of Amphibian Antimicrobial Peptide from the Skin Secretion of the Oriental Broad-folded Frog, Hylarana latouchii.</a> Lin, Y., N. Hu, P. Lyu, J. Ma, L. Wang, M. Zhou, S. Guo, T. Chen, and C. Shaw. Amino Acids, 2014. 46(4): p. 901-909. PMID[24378871].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24583354">Synthesis, Structures, Spectroscopy and Antimicrobial Properties of Complexes of Copper (II) with Salicylaldehyde N-substituted thiosemicarbazones and 2,2&#39;-Bipyridine or 1,10-Phenanthroline.</a> Lobana, T.S., S. Indoria, A.K. Jassal, H. Kaur, D.S. Arora, and J.P. Jasinski. European Journal of Medicinal Chemistry, 2014. 76: p. 145-154. PMID[24583354].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24568657">Propargyl-linked Antifolates are Dual Inhibitors of Candida albicans and Candida glabrata.</a>N, G.D., J.L. Paulsen, K. Viswanathan, S. Keshipeddy, M.N. Lombardo, W. Zhou, K.M. Lamb, A.E. Sochia, J.B. Alverson, N.D. Priestley, D.L. Wright, and A.C. Anderson. Journal of Medicinal Chemistry, 2014. 57(6): p. 2643-2656. PMID[24568657].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24705564">Antiprotozoal Activities of Millettia richardiana (Fabaceae) from Madagascar.</a> Rajemiarimiraho, M., J.T. Banzouzi, M.L. Nicolau-Travers, S. Ramos, Z. Cheikh-Ali, C. Bories, O.L. Rakotonandrasana, S. Rakotonandrasana, P.A. Andrianary, and F. Benoit-Vical. Molecules, 2014. 19(4): p. 4200-4211. PMID[24705564].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24224742">Effects of Ambroxol on Candida albicans Growth and Biofilm Formation.</a> Rene, H.D., M.S. Jose, S.N. Isela, and C.R. Claudio. Mycoses, 2014. 57(4): p. 228-232. PMID[24224742].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24583356">Bifunctional Ethyl 2-Amino-4-methylthiazole-5-carboxylate Derivatives: Synthesis and in Vitro Biological Evaluation as Antimicrobial and Anticancer Agents.</a> Rostom, S.A., H.M. Faidallah, M.F. Radwan, and M.H. Badr. European Journal of Medicinal Chemistry, 2014. 76: p. 170-181. PMID[24583356].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24694570">Theonellamide G, a Potent Antifungal and Cytotoxic Bicyclic Glycopeptide from the Red Sea Marine Sponge Theonella swinhoei.</a>Youssef, D.T., L.A. Shaala, G.A. Mohamed, J.M. Badr, F.H. Bamanie, and S.R. Ibrahim. Marine Drugs, 2014. 12(4): p. 1911-1923. PMID[24694570].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24414399">Aaptamines, Marine Spongean Alkaloids, as Anti-dormant Mycobacterial Substances.</a> Arai, M., C. Han, Y. Yamano, A. Setiawan, and M. Kobayashi. Journal of Natural Medicines, 2014. 68(2): p. 372-376. PMID[24414399].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24619984">In Vitro Activity of Amikacin against Isolates of Mycobacterium avium Complex with Proposed MIC Breakpoints and Finding of a 16s rRNA Gene Mutation in Treated Isolates.</a> Brown-Elliott, B.A., E. Iakhiaeva, D.E. Griffith, G.L. Woods, J.E. Stout, C.R. Wolfe, C.Y. Turenne, and R.J. Wallace, Jr. Journal of Clinical Microbiology, 2014. 52(4): p. 1311. PMID[24619984].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24681629">Synthesis and Anti-tuberculosis Activity of the Marine Natural Product Caulerpin and its Analogues.</a>Chay, C.I., R.G. Cansino, C.I. Pinzon, R.O. Torres-Ochoa, and R. Martinez. Marine Drugs, 2014. 12(4): p. 1757-1772. PMID[24681629].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24658308">Antibacterial Drugs: Redesigned Antibiotic Combats Drug-resistant Tuberculosis.</a> Cully, M. Nature Reviews Drug Discovery, 2014. 13(4): p. 256-257. PMID[24658308].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24330002">In Vitro Activity of Isoimperatorin, Alone and in Combination, against Mycobacterium tuberculosis.</a>Guo, N., J. Wu, J. Fan, P. Yuan, Q. Shi, K. Jin, W. Cheng, X. Zhao, Y. Zhang, W. Li, X. Tang, and L. Yu. Letters in Applied Microbiology, 2014. 58(4): p. 344-349. PMID[24330002].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24636345">Discovery and Structure Optimization of a Series of Isatin Derivatives as Mycobacterium tuberculosis Chorismate mutase Inhibitors.</a> Jeankumar, V.U., R. Alokam, J.P. Sridevi, P. Suryadevara, S.S. Matikonda, S. Peddi, S. Sahithi, M. Alvala, P. Yogeeswari, and D. Sriram. Chemical Biology &amp; Drug Design, 2014. 83(4): p. 498-506. PMID[24636345].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24631185">Synthesis and Antimycobacterial Activities of Some New Thiazolylhydrazone Derivatives.</a> Ozadali, K., O. Unsal Tan, P. Yogeeswari, S. Dharmarajan, and A. Balkan. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(7): p. 1695-1697. PMID[24631185].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24686575">N-Substituted 2-isonicotinoylhydrazinecarboxamides - New Antimycobacterial Active Molecules.</a> Rychtarcikova, Z., M. Kratky, M. Gazvoda, M. Komloova, S. Polanc, M. Kocevar, J. Stolarikova, and J. Vinsova. Molecules, 2014. 19(4): p. 3851-3868. PMID[24686575].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24530503">Antibacterial Activity of Rifamycins for M. smegmatis with Comparison of Oxidation and Binding to Tear Lipocalin.</a>Staudinger, T., B. Redl, and B.J. Glasgow. Biochimica et Biophysica Acta, 2014. 1844(4): p. 750-758. PMID[24530503].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24699133">Treatment of Erythrocytes with the 2-Cys peroxiredoxin Inhibitor, Conoidin a, Prevents the Growth of Plasmodium falciparum and Enhances Parasite Sensitivity to Chloroquine.</a>Brizuela, M., H.M. Huang, C. Smith, G. Burgio, S.J. Foote, and B.J. McMorran. Plos One, 2014. 9(4): p. e92411. PMID[24699133].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24488078">Ovicidal, Larvicidal and Adulticidal Properties of Asparagus racemosus (Willd.) (Family: Asparagaceae) Root Extracts against Filariasis (Culex quinquefasciatus), Dengue (Aedes aegypti) and Malaria (Anopheles stephensi) Vector Mosquitoes (Diptera: Culicidae).</a>Govindarajan, M. and R. Sivakumar. Parasitology Research, 2014. 113(4): p. 1435-1449. PMID[24488078].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24568587">Medicinal Chemistry Optimization of Antiplasmodial Imidazopyridazine Hits from High Throughput Screening of a SoftFocus Kinase Library: Part 1.</a> Le Manach, C., D. Gonzalez Cabrera, F. Douelle, A.T. Nchinda, Y. Younis, D. Taylor, L. Wiesner, K.L. White, E. Ryan, C. March, S. Duffy, V.M. Avery, D. Waterson, M.J. Witty, S. Wittlin, S.A. Charman, L.J. Street, and K. Chibale. Journal of Medicinal Chemistry, 2014. 57(6): p. 2789-2798. PMID[24568587].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24630564">Synthesis and Evaluation of New Diaryl ether and Quinoline Hybrids as Potential Antiplasmodial and Antimicrobial Agents.</a> Mishra, A., H. Batchu, K. Srivastava, P. Singh, P.K. Shukla, and S. Batra. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(7): p. 1719-1723. PMID[24630564].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24631188">Optimization of Plasmepsin Inhibitor by Focusing on Similar Structural Feature with Chloroquine to Avoid Drug-resistant Mechanism of Plasmodium falciparum.</a> Miura, T., K. Hidaka, Y. Azai, K. Kashimoto, Y. Kawasaki, S.E. Chen, R.F. de Freitas, E. Freire, and Y. Kiso. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(7): p. 1698-1701. PMID[24631188].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24641010">Design and Synthesis of High Affinity Inhibitors of Plasmodium falciparum and Plasmodium vivax N-Myristoyltransferases Directed by Ligand Efficiency Dependent Lipophilicity (Lelp).</a>Rackham, M.D., J.A. Brannigan, K. Rangachari, S. Meister, A.J. Wilkinson, A.A. Holder, R.J. Leatherbarrow, and E.W. Tate. Journal of Medicinal Chemistry, 2014. 57(6): p. 2773-2788. PMID[24641010].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24549754">Antimalarial Efficacy, Cytotoxicity, and Genotoxicity of Methanolic Stem Bark Extract from Hintonia latiflora in a Plasmodium yoelii yoelii Lethal Murine Malaria Model.</a> Rivera, N., P.Y. Lopez, M. Rojas, T.I. Fortoul, D.Y. Reynada, A.J. Reyes, E. Rivera, H.I. Beltran, and F. Malagon. Parasitology Research, 2014. 113(4): p. 1529-1536. PMID[24549754].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24524185">New Orally Active Amino- and Hydroxy-functionalized 11-Azaartemisinins and their Derivatives with High Order of Antimalarial Activity against Multidrug-resistant Plasmodium yoelii in Swiss Mice1.</a>Singh, C., V.P. Verma, M. Hassam, A.S. Singh, N.K. Naikade, and S.K. Puri. Journal of Medicinal Chemistry, 2014. 57(6): p. 2489-2497. PMID[24524185].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0328-041014.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331666900033">Composition of Essential Oil from Tagetes minuta and Its Cytotoxic, Antioxidant and Antimicrobial Activities.</a> Ali, N.A.A., F.S. Sharopov, A.G. Al-kaf, G.M. Hill, N. Arnold, S.S. Al-Sokari, W.N. Setzer, and L. Wessjohann. Natural Product Communications, 2014. 9(2): p. 265-268. ISI[000331666900033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331886300013">Synthesis of Novel and Highly Functionalized 4-Hydroxycoumarin chalcone and Their Pyrazoline Derivatives as Anti-tuberculosis Agents.</a> Asad, M., F. Beevi, S.P. Ganesan, C.W. Oo, R.S. Kumar, V. Laxmipathi, H. Osman, and M.A. Ali. Letters in Drug Design &amp; Discovery, 2014. 11(2): p. 222-230. ISI[000331886300013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332434100010">Antibacterial Activities of Doped ZnO Nano-powder with M2+(M=Cu, Cd, Ag and Fe).</a> Ding, Y., G. Ma, L.C. Li, Y.H. Wang, Y.Y. Chen, and X. Wu. Chinese Journal of Inorganic Chemistry, 2014. 30(2): p. 293-302. ISI[000332434100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">33.<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331672400001">Antimycobacterial Activity of Ellagitannin and Ellagic acid Derivate Rich Crude Extracts and Fractions of Five Selected Species of Terminalia Used for Treatment of Infectious Diseases in African Traditional Medicine.</a> Fyhrquist, P., I. Laakso, S.G. Marco, R. Julkunen-Tiitto, and R. Hiltunen. South African Journal of Botany, 2014. 90: p. 1-16. ISI[000331672400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331729500003">Design, Synthesis and Preliminary Bioactivity Studies of 1,2-Dihydrobenzo[d]isothiazol-3-one-1,1-dioxide hydroxamic acid Derivatives as Novel Histone Deacetylase Inhibitors.</a> Han, L.Q., L. Wang, X.B. Hou, H.S. Fu, W.G. Song, W.P. Tang, and H. Fang. Bioorganic &amp; Medicinal Chemistry, 2014. 22(5): p. 1529-1538. ISI[000331729500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331497600049">1-Hydroxyquinolones Inhibit de Novo Pyrimidine Synthesis in Toxoplasma gondii.</a> Hegewald, J., U. Gross, and W. Bohne. International Journal of Medical Microbiology, 2013. 303: p. 15-16. ISI[000331497600049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332964200013">Chemical Composition and Antimicrobial Activity of Essential Oil of Salvia bicolor Desf. Growing in Egypt.</a> Ibrahim, T.A. Journal of Essential Oil Bearing Plants, 2014. 17(1): p. 104-111. ISI[000332964200013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332153800006">Synthesis of Halogenated Derivatives of Thymol and Their Antimicrobial Activities.</a> Kaur, R., M.P. Darokar, S.K. Chattopadhyay, V. Krishna, and A. Ahmad. Medicinal Chemistry Research, 2014. 23(5): p. 2212-2217. ISI[000332153800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331805000043">Synthesis, Crystal Structure Investigation, DFT Analyses and Antimicrobial Studies of Silver(I) Complexes with N,N,N &#39;,N &#39;&#39;-Tetrakis(2-hydroxyethyl/propyl)ethylenediamine and Tris(2-Hydroxyethyl)amine.</a> Kumar, R., S. Obrai, A. Kaur, M.S. Hundal, H. Meehnian, and A.K. Jana. New Journal of Chemistry, 2014. 38(3): p. 1186-1198. ISI[000331805000043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331689000006">Antifungal Effect and Mode of Action of Glochidioboside against Candida albicans Membranes.</a> Lee, H., H. Choi, H.J. Ko, E.R. Woo, and D.G. Lee. Biochemical and Biophysical Research Communications, 2014. 444(1): p. 30-35. ISI[000331689000006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332389500008">Towards a New Combination Therapy for Tuberculosis with Next Generation Benzothiazinones.</a> Makarov, V., B. Lechartier, M. Zhang, J. Neres, A.M. van der Sar, S.A. Raadsen, R.C. Hartkoorn, O.B. Ryabova, A. Vocat, L.A. Decosterd, N. Widmer, T. Buclin, W. Bitter, K. Andries, F. Pojer, P.J. Dyson, and S.T. Cole. EMBO Molecular Medicine, 2014. 6(3): p. 372-383. ISI[000332389500008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331496100006">Synthesis of 3-Heteryl substituted pyrrolidine-2,5-diones via Catalytic Michael Reaction and Evaluation of Their Inhibitory Activity against InhA and Mycobacterium tuberculosis.</a> Matviiuk, T., G. Mori, C. Lherbet, F. Rodriguez, M.R. Pasca, M. Gorichko, B. Guidetti, Z. Voitenko, and M. Baltas. European Journal of Medicinal Chemistry, 2014. 71: p. 46-52. ISI[000331496100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331596500026">Synthesis, Characterization and Thermal Behavior of Antibacterial and Antifungal Active Zinc Complexes of bis (3(4-Dimethylaminophenyl)-allylidene-1,2-diaminoethane.</a> Montazerozohori, M., S. Zahedi, A. Naghiha, and M.M. Zohour. Materials Science &amp; Engineering C-Materials for Biological Applications, 2014. 35: p. 195-204. ISI[000331596500026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331496100020">Design, Synthesis, Molecular Docking and 3D-QSAR Studies of Potent Inhibitors of Enoyl-acyl Carrier Protein Reductase as Potential Antimycobacterial Agents.</a> More, U.A., S.D. Joshi, T.M. Aminabhavi, A.K. Gadad, M.N. Nadagouda, and V.H. Kulkarni. European Journal of Medicinal Chemistry, 2014. 71: p. 199-218. ISI[000331496100020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331916500025">Design, Synthesis and Evaluation of Acridine and Fused-quinoline Derivatives as Potential Anti-tuberculosis Agents.</a> Muscia, G.C., G.Y. Buldain, and S.E. Asis. European Journal of Medicinal Chemistry, 2014. 73: p. 243-249. ISI[000331916500025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332059300007">Antimicrobial Polycarbonates: Investigating the Impact of Nitrogen-containing Heterocycles as Quaternizing Agents.</a> Ng, V.W.L., J.P.K. Tan, J.Y. Leong, Z.X. Voo, J.L. Hedrick, and Y.Y. Yang. Macromolecules, 2014. 47(4): p. 1285-1291. ISI[000332059300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332153800016">Synthesis and Biological Evaluation of Novel Quinazoline Derivatives Obtained by Suzuki C-C Coupling.</a> Patel, A.B., K.H. Chikhalia, and P. Kumari. Medicinal Chemistry Research, 2014. 23(5): p. 2338-2346. ISI[000332153800016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331677900004">Design, Synthesis and Investigation on the Structure-Activity Relationships of N-Substituted 2-aminothiazole Derivatives as Antitubercular Agents.</a> Pieroni, M., B.J. Wan, S.H. Cho, S.G. Franzblau, and G. Costantino. European Journal of Medicinal Chemistry, 2014. 72: p. 26-34. ISI[000331677900004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332153800007">Discovery of New Antitubercular Agents by Combining Pyrazoline and Benzoxazole Pharmacophores: Design, Synthesis and Insights into the Binding Interactions.</a> Rana, D.N., M.T. Chhabria, N.K. Shah, and P.S. Brahmkshatriya. Medicinal Chemistry Research, 2014. 23(5): p. 2218-2228. ISI[000332153800007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331496100033">Synthesis and Characterization of New N-(4-(4-Chloro-1H-imidazol-1-yl)-3-methoxyphenyl)amide/sulfonamide Derivatives as Possible Antimicrobial and Antitubercular Agents.</a> Ranjith, P.K., R. Pakkath, K.R. Haridas, and S.N. Kumari. European Journal of Medicinal Chemistry, 2014. 71: p. 354-365. ISI[000331496100033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332409500002">Synthesis and in Vitro Antibacterial Activity of New Oxoethylthio-1,3,4-oxadiazole Derivatives.</a> Raval, J.P., T.N. Akhaja, D.M. Jaspara, K.N. Myangar, and N.H. Patel. Journal of Saudi Chemical Society, 2014. 18(2): p. 101-106. ISI[000332409500002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331849300014">Environmentally Benign Synthesis of 2-Pyrazolines and Cyclohexenones Incorporating Naphthalene Moiety and their Antimicrobial Evaluation.</a> Saad, A.B.A. Chemical Research in Chinese Universities, 2014. 30(1): p. 68-74. ISI[000331849300014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332437600013">Anti-candida Activity of Two-peptide Bacteriocins, Plantaricins (Pin E/F and J/K) and Their Mode of Action.</a> Sharma, A. and S. Srivastava. Fungal Biology, 2014. 118(2): p. 264-275. ISI[000332437600013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331496100031">Synthesis and Evaluation of 1-Cyclopropyl-6-fluoro-1,4-dihydro-4-oxo-7-(4-(2-(4-substitutedpiperazin -1-yl)acetyl)piperazin-1-yl) quinoline-3-carboxylic acid Derivatives as Anti-tubercular and Antibacterial Agents.</a> Suresh, N., H.N. Nagesh, J. Renuka, V. Rajput, R. Sharma, I.A. Khan, and C. Gowri. European Journal of Medicinal Chemistry, 2014. 71: p. 324-332. ISI[000331496100031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>

    <br />

    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331496100017">Rational Design and Synthesis of Novel Dibenzo[b,d]furan-1,2,3-triazole Conjugates as Potent Inhibitors of Mycobacterium tuberculosis.</a> Yempala, T., J.P. Sridevi, P. Yogeeswari, D. Sriram, and S. Kantevari. European Journal of Medicinal Chemistry, 2014. 71: p. 160-167. ISI[000331496100017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0328-041014.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
