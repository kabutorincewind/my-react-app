

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zZwnhiVNYPNdyxzBArtfH4h4fhnF6L6WCryj5B31sE15eOjyD/iauR1+pQ9JL3QjHfzU6RY1oOEJprJZzB+aMdqP7qJPrMIrq2d5LO1kxF65tzBpksHcp8FraAw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="40CAC6FC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: October, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26076622">Synthetic Galactomannans with Potent anti-HIV Activity.</a> Budragchaa, D., S. Bai, T. Kanamoto, H. Nakashima, S. Han, and T. Yoshida. Carbohydrate Polymers, 2015. 130: p. 233-242. PMID[26076622].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26503889">Lipophilic Prodrugs of Nucleoside Triphosphates as Biochemical Probes and Potential Antivirals.</a> Gollnest, T., T.D. de Oliveira, D. Schols, J. Balzarini, and C. Meier. Nature Communications, 2015. 6: p. 8716. PMID[26503889].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26487915">Potent Inhibitors Active against HIV Reverse Transcriptase with K101p, a Mutation Conferring Rilpivirine Resistance.</a> Gray, W.T., K.M. Frey, S.B. Laskey, A.C. Mislak, K.A. Spasov, W.G. Lee, M. Bollini, R.F. Siliciano, W.L. Jorgensen, and K.S. Anderson. ACS Medicinal Chemistry Letters, 2015. 6(10): p. 1075-1079. PMID[26487915].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26397965">Discovery of 2-pyridinone Aminals: A Prodrug Strategy to Advance a Second Generation of HIV-1 Integrase Strand Transfer Inhibitors.</a> Raheem, I.T., A.M. Walji, D. Klein, J.M. Sanders, D.A. Powell, P. Abeywickrema, G. Barbe, A. Bennet, S.D. Clas, D. Dubost, M. Embrey, J. Grobler, M.J. Hafey, T.J. Hartingh, D.J. Hazuda, M.D. Miller, K.P. Moore, N. Pajkovic, S. Patel, V. Rada, P. Rearden, J.D. Schreier, J. Sisko, T.G. Steele, J.F. Truchon, J. Wai, M. Xu, and P.J. Coleman. Journal of Medicinal Chemistry, 2015. 58(20): p. 8154-8165. PMID[26397965].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_10_2015.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26408695" target="_blank">Quantitative Structure-cytotoxicity Relationship of Oleoylamides.</a> Sakagami, H., Y. Uesawa, M. Ishihara, H. Kagaya, T. Kanamoto, S. Terakubo, H. Nakashima, K. Takao, and Y. Sugita. Anticancer Research, 2015. 35(10): p. 5341-51. PMID[26408695]. 
    <br />
    <b>[PubMed].</b> HIV_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26487913">Design, Synthesis, and Biological Evaluation of 1,2-dihydroisoquinolines as HIV-1 Integrase Inhibitors.</a> Tandon, V., Urvashi, P. Yadav, S. Sur, S. Abbat, V. Tiwari, R. Hewer, M.A. Papathanasopoulos, R. Raja, A.C. Banerjea, A.K. Verma, S. Kukreti, and P.V. Bharatam. ACS Medicinal Chemistry Letters, 2015. 6(10): p. 1065-1070. PMID[26487913].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_10_2015.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26489856">PLGA-PEG Nanoparticles Coated with anti-CD45RO and Loaded with HDAC Plus Protease Inhibitors Activate Latent HIV and Inhibit Viral Spread.</a> Tang, X., Y. Liang, X. Liu, S. Zhou, L. Liu, F. Zhang, C. Xie, S. Cai, J. Wei, Y. Zhu, and W. Hou. Nanoscale Research Letters, 2015. 10(1): p. 413. PMID[26489856].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26474627">Synthesis of 6-Benzyl-1-[(benzyloxy)methyl]-3-hydroxy-5-(hydroxymethyl)pyrimidine-2,4(1H,3H)-dione.</a> Tang, X.W., L. Zhang, J.X. Zhao, Y. Zhang, Y. Guo, Z.L. Zhang, C. Tian, X.W. Wang, and J.Y. Liu. Beijing Da Xue Xue Bao, 2015. 47(5): p. 838-841. PMID[26474627].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_10_2015.
    <br />
    <br /></p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26408690" target="_blank">Quantitative Structure-cytotoxicity Relationship of 3-Styryl-2H-chromenes.</a> Uesawa, Y., H. Sakagami, M. Ishihara, H. Kagaya, T. Kanamoto, S. Terakubo, H. Nakashima, H. Yahagi, K. Takao, and Y. Sugita. Anticancer Research, 2015. 35(10): p. 5299-307. PMID[26408690]. 
    <br />
    <b>[PubMed].</b> HIV_10_2015. </p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">10.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/25556966" target="_blank">Synthesis and Biological Evaluation of New Eugenol Mannich Bases as Promising Antifungal Agents.</a> Abrao, P., R. Pizi, T. de Souza, N. Silva, A. Fregnan, F. Silva, L. Coelho, L. Malaquias, A. Dias, D. Dias, M. Veloso, and D. Carvalho. Chemical Biology &amp; Drug Design, 2015. 86(4): p. 459-465. PMID[25556966]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26407080" target="_blank">A Multiple siRNA-based anti-HIV/SHIV Microbicide Shows Protection in Both in Vitro and in Vivo Models.</a> Boyapalle, S., W. Xu, P. Raulji, S. Mohapatra, and S. Mohapatra. Plos One, 2015. 10(9): e0135288. PMID[26407080]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. </p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26256479" target="_blank">Repurposing Hsp104 to Antagonize Seminal Amyloid and Counter HIV Infection.</a> Castellano, L., S. Bart, V. Holmes, D. Weissman, and J. Shorter. Chemistry &amp; Biology, 2015. 22(8): p. 1074-1086. PMID[26256479]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000361862400236">Il-37 Inhibits HIV-1 Replication in Human T-cells.</a> Chattergoon, M.A., N. Sangal, and A. Cox. Cytokine, 2015. 76(1): p. 108-108. ISI[000361862400236].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">14. <span class="memofmt2-2"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000361254700006" target="_blank">Ethnomedicinal Plants Used by Traditional Healers in the Management of HIV/AIDS Opportunistic Diseases in Rundu, Kavango East Region, Namibia.</a></span> Chinsembu, K., A. Hijarunguru, and A. Mbangu. South African Journal of Botany, 2015. 100: p. 33-42. ISI[000361254700006]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25757618" target="_blank">Editing CCR5: A Novel Approach to HIV Gene Therapy.</a> Cornu, T., C. Mussolino, K. Bloom, and T. Cathomen. Gene Therapy for HIV and Chronic Infections, 2015. 848: p. 117-130. PMID[25757618]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    16.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000361862400053" target="_blank">PML-dependent Inhibition of HIV-1 Reverse-transcription by Daxx.</a> Dutrieux, J., G. Maarifi, D. Portilho, N. Arhel, M. Chelbi-Alix, and S. Nisole. Cytokine, 2015. 76(1): p. 68-68. ISI[000361862400053]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25626467" target="_blank">Synthesis and Biological Evaluation of a Series of 2-((1-Substituted-1H-1,2,3-triazol-4-yl)methylthio)-6-(naphthalen-1-ylmethyl)pyrimidin-4(3H)-one as Potential HIV-1 Inhibitors.</a> Fang, Z., D. Kang, L. Zhang, B. Huang, H. Liu, C. Pannecouque, E. De Clercq, P. Zhan, and X. Liu. Chemical Biology &amp; Drug Design, 2015. 86(4): p. 614-618. PMID[25626467]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000360870400052" target="_blank">Synthesis of Novel (2E)-1-[4-(2,4-Difluorophenyl)phenyl]3-arylprop-2-en-1-ones: Investigation on Spectral, Antibacterial, Molecular Docking and Theoretical Studies.</a> Fathimunnisa, M., H. Manikandan, and S. Selvanayagam. Journal of Molecular Structure, 2015. 1099: p. 407-418. ISI[000360870400052]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26358281" target="_blank">Isolation and Anticancer, Anthelminthic, and Antiviral (HIV) Activity of Acylphloroglucinols, and Regioselective Synthesis of Empetrifranzinans from Hypericum roeperianum.</a> Fobofou, S., K. Franke, G. Sanna, A. Porzel, E. Bullita, P. La Colla, and L. Wessjohann. Bioorganic &amp; Medicinal Chemistry, 2015. 23(19): p. 6327-6334. PMID[26358281]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362453700012">A Resveratrol Analog Termed 3,3,4,4,5,5-hexahydroxy-trans-stilbene is a Potent HIV-1 Inhibitor.</a> Han, Y.S., P.K. Quashie, T. Mesplede, H.T. Xu, Y.D. Quan, W. Jaeger, T. Szekeres, and M.A. Wainberg. Journal of Medical Virology, 2015. 87(12): p. 2054-2060. ISI[000362453700012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362409800021">Anti-HIV Terpenoids from Daphne aurantiaca Diels. Stems.</a> Huang, S.Z., X. Zhang, Q.Y. Ma, Y.T. Zheng, H.F. Dai, Q. Wang, J. Zhou, and Y.X. Zhao. RSC Advances, 2015. 5(98): p. 80254-80263. ISI[000362409800021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000361564800006" target="_blank">Synthesis of Novel Spiro[indol-2,2-pyrroles] Using Isocyanide-based Multicomponent Reaction.</a> Jalli, V., S. Krishnamurthy, H. Kawasaki, T. Moriguchi, and A. Tsuge. Synthetic Communications, 2015. 45(19): p. 2216-2226. ISI[000361564800006]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    23.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/25600073" target="_blank">Synthesis and Preliminary Antiviral Activities of Piperidine-substituted Purines against HIV and Influenza A/H1N1 Infections.</a> Kang, D.,Z. Fang, B. Huang, L. Zhang, H. Liu, C. Pannecouque, L. Naesens, E. De Clercq, P. Zhan, and X. Liu. Chemical Biology &amp; Drug Design, 2015. 86(4): p. 568-577. PMID[25600073]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362749900013">The Synthesis of Novel 5-Deoxy-3,5,5-trifluoro-apiosyl nucleoside phosphonic acid Analogs as Antiviral Agents.</a> Kim, S. and J.H. Hong. Bulletin of the Korean Chemical Society, 2015. 36(10): p. 2484-2493. ISI[000362749900013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <p class="plaintext"><br />
    25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000361862400015" target="_blank">IFN-induced factors with anti-HIV activity.</a> Malim, M. Cytokine, 2015. 76(1): p. 59-59. ISI[000361862400015]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26345647" target="_blank">The (5Z)-5-Pentacosenoic and 5-Pentacosynoic acids Inhibit the HIV-1 Reverse Transcriptase.</a> Moreira, L.,E. Orellano, K. Rosado, R. Guido, A. Andricopulo, G. Soto, J. Rodriguez, D. Sanabria-Rios, and N. Carballeira. Lipids, 2015. 50(10): p. 1043-1050. PMID[26345647]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25868621" target="_blank">Anti-HIV-1 Integrase Activity and Molecular Docking of Compounds from Albizia procera Bark.</a> Panthong, P., K. Bunluepuech, N. Boonnak, P. Chaniad, S. Pianwanit, C. Wattanapiromsakul, and S. Tewtrakul. Pharmaceutical Biology, 2015. 53(12): p. 1861-1866. PMID[25868621]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26375010" target="_blank">A Concise Synthesis of (+)-Batzelladine B from Simple Pyrrole-based Starting Materials.</a> Parr, B., C. Economou, and S. Herzon. Nature, 2015. 525(7570): p. 507-510. PMID[26375010]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    29. <span class="memofmt2-2"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26141568" target="_blank">Redoxal, an Inhibitor of de Novo Pyrimidine Biosynthesis, Augments APOBEC3G Antiviral Activity against Human Immunodeficiency Virus Type 1.</a></span> Pery, E.,A. Sheehy, N. Nebane, V. Misra, M. Mankowski, L. Rasmussen, E. White, R. Ptak, and D. Gabuzda. Virology, 2015. 484: p. 276-287. PMID[26141568]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25613403" target="_blank">Antiviral Properties from Plants of the Mediterranean Flora.</a> Sanna, G., P. Farci, B. Busonera, G. Murgia, P. La Colla, and G. Giliberti. Natural Product Research, 2015. 29(22): p. 2065-2070. PMID[25613403]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362568300012">Synthesis and Antiviral Evaluation of 2&#39;,3&#39;-Dideoxy-2&#39;,3&#39;-difluoro-D-arabinofuranosyl 2,6-disubstituted purine Nucleosides.</a> Schinazi, R.F., G.G. Sivets, M.A. Detorio, T.R. McBrayer, T. Whitaker, S.J. Coats, and F. Amblard. Heterocyclic Communications, 2015. 21(5): p. 315-327. ISI[000362568300012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362533900027">Stellettapeptins A and B, HIV-inhibitory Cyclic Depsipeptides from the Marine Sponge Stelletta Sp.</a> Shin, H.J., M.A. Rashid, L.K. Cartner, H.R. Bokesch, J.A. Wilson, J.B. McMahon, and K.R. Gustafson. Tetrahedron Letters, 2015. 56(40): p. 5482-5482. ISI[000362533900027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362568300009">Optimized Synthesis of 3&#39;-O-aminothymidine and Evaluation of its Oxime Derivative as an anti-HIV Agent.</a> Solyev, P.N., M.V. Jasko, T.A. Martynova, L.B. Kalnina, D.N. Nosik, and M.K. Kukhanova. Heterocyclic Communications, 2015. 21(5): p. 291-295. ISI[000362568300009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26026377" target="_blank">Inhibition of HIV Infection by Caerin 1 Antimicrobial Peptides.</a> Van Compernolle, S., P. Smith, J. Bowie, M. Tyler, D. Unutmaz, and L. Rollins-Smith. Peptides, 2015. 71: p. 296-303. PMID[26026377]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. </p>

    <p class="plaintext"><br />
    35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26276435" target="_blank">Pyrimidine sulfonylacetanilides with Improved Potency against Key Mutant Viruses of HIV-1 by Specific Targeting of a Highly Conserved Residue.</a> Wan, Z., J. Yao, T. Mao, X. Wang, H. Wang, W. Chen, H. Yin, F. Chen, E. De Clercq, D. Daelemans, and C. Pannecouque. European Journal of Medicinal Chemistry, 2015. 102: p. 215-222. PMID[26276435]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362094500001">Inhibition of HIV-1 Assembly by Coiled-coil Domain Containing Protein 8 in Human Cells.</a> Wei, M., X. Zhao, M. Liu, Z. Huang, Y. Xiao, M.J. Niu, Y.M. Shao, and L. Kleiman. Scientific Reports, 2015. (14724): 12pp. ISI[000362094500001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2015.</p>

    <br />

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26185005" target="_blank">Multistage Virtual Screening and Identification of Novel HIV-1 Protease Inhibitors by Integrating SVM, Shape, Pharmacophore and Docking Methods.</a> Wei, Y., J. Li, Z. Chen, F. Wang, W. Huang, Z. Hong, and J. Lin. European Journal of Medicinal Chemistry, 2015. 101: p. 409-418. PMID[26185005]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. 
    <br />
    <br />
    38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26241003" target="_blank">Identification of an HIV-1 Replication Inhibitor Which Rescues Host Restriction Factor APOBEC3G in Vif-APOBEC3G Complex.</a> Zhang, S., L. Zhong, B. Chen, T. Pan, X. Zhang, L. Liang, Q. Li, Z. Zhang, H. Chen, J. Zhou, H. Luo, H. Zhang, and C. Bai. Antiviral Research, 2015. 122: p. 20-27. PMID[26241003]. 
    <br />
    <b>[WOS].</b> HIV_10_2015. </p>

    <h2>Patent Citations</h2>

    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151008&amp;CC=WO&amp;NR=2015153304A1&amp;KC=A1">Preparation of Phenoxypyrimidinylmethylpyridazinylmethyl dihydrogen phosphates as Prodrugs of HIV Reverse Transcriptase Inhibitors.</a> Burgey, C.S., J.F. Fritzen, J. Balsells, and M. Patel. Patent. 2015. 2015-US22868 2015153304: 59pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_10_2015.</p>

    <br />

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151001&amp;CC=WO&amp;NR=2015148746A1&amp;KC=A1">Preparation of 4&#39;-Substituted nucleoside Derivatives as HIV Reverse Transcriptase Inhibitor.</a> Girijavallabhan, V.M., D.B. Olsen, Z. Zhang, J. Fu, and B.-Y. Tang. Patent. 2015. 2015-US22621 2015148746: 127pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_10_2015.</p>

    <br />

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151001&amp;CC=WO&amp;NR=2015143535A1&amp;KC=A1">Small Molecule Inhibitors of gp120-Mediated HIV Infection and Methods of Use.</a> Lingwood, C., M. Huesca, R. Dayam, and S. Singh. Patent. 2015. 2015-CA171 2015143535: 67pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_10_2015.</p>

    <br />

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150923&amp;CC=CN&amp;NR=104926829A&amp;KC=A">Preparation of Thienopyrimidine Derivatives and for Treating HIV Infection.</a> Liu, X., D. Kang, P. Zhan, Z. Fang, and Z. Li. Patent. 2015. 2015-10309662 104926829: 27pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_10_2015.</p>

    <br />

    <p class="plaintext">43. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151015&amp;CC=US&amp;NR=2015291542A1&amp;KC=A1">Antiviral Water-soluble Product with anti-HIV Effect, Based on Ionic Silver and Methylene Blue Compound; Method of Its Production and Examples of HIV-infected Patients&#39; Treatment.</a> Svetlik, H.E., V.V. Tretiakov, O.V. Tretiakova, V.N. Silnikov, V.I. Bouroumov, and N.G. Zhevachevsky. Patent. 2015. 2015-14680577 20150291542: 6pp.</p>

    <p class="plaintext"><b>[Patent].</b> HIV_10_2015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
