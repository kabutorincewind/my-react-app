

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-08-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5cLNn7VcdoJXpDJyZolxiVYJcbP5Nej3AHQi/JocPvXhBtSNNoYMKgnTjC/aAnAXxOBb+0tkFvJ/Osjcx50LBqtovBcyVe/c+22tRfYk3Zj+GfXLjQof8luA7sY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E36B30F0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 17 - August 30, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22918868">In Vitro Antifungal Activity of the Flavonoid Baicalein against Candida Species.</a> Serpa, R., E.J. Franca, L. Furlaneto-Maia, C.G. Andrade, A. Diniz, and M.C. Furlaneto. Journal of Medical Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[22918868].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22921108">New Quinolinyl-1,3,4-oxadiazoles: Synthesis, in Vitro Antibacterial, Antifungal and Antituberculosis Studies.</a> Patel, R.V., P. Kumari, and K.H. Chikhalia. Medicinal Chemistry (Shariqah (United Arab Emirates)), 2012. <b>[Epub ahead of print]</b>. PMID[22921108].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22915202">Identification of Antifungal Niphimycin from Streptomyces Sp. Kp6107 by Screening Based on Adenylate Kinase Assay.</a> Kim, H.Y., J.D. Kim, J.S. Hong, J.H. Ham, and B.S. Kim. Journal of Basic Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[22915202].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22904206">Effects of Undecylenic acid Released from Denture Liner on Candida Biofilms.</a> Goncalves, L.M., A.A. Del Bel Cury, A. Sartoratto, V.L. Garcia Rehder, and W.J. Silva. Journal of Dental Research, 2012. <b>[Epub ahead of print]</b>. PMID[22904206].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22923048">Gain-of-function Mutations in Upc2 Are a Frequent Cause of Erg11 Upregulation in Azole-resistant Clinical Isolates of Candida albicans.</a> Flowers, S.A., K.S. Barker, E.L. Berkow, G. Toner, S.G. Chadwick, S.E. Gygax, J. Morschhauser, and P.D. Rogers. Eukaryotic Cell, 2012. <b>[Epub ahead of print]</b>. PMID[22923048].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22922280">Activity of Spray-dried Microparticles Containing Pomegranate Peel Extract against Candida albicans.</a> Endo, E.H., T. Ueda-Nakamura, C.V. Nakamura, and B.P. Filho. Molecules, 2012. 17(9): p. 10094-10107. PMID[22922280].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22918865">The Fungicide Fludioxonil Antagonizes Fluconazole Activity in the Human Fungal Pathogen Candida albicans.</a> Buschart, A., A. Burakowska, and U. Bilitewski. Journal of Medical Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[22918865].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22877569">Design of a Candida albicans Disaccharide Conjugate Vaccine by Reverse Engineering a Protective Monoclonal Antibody.</a> Bundle, D.R., C. Nycholat, C. Costello, R. Rennie, and T. Lipinski. ACS Chemical Biology, 2012. <b>[Epub ahead of print]</b>. PMID[22877569].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p>  
    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22905783">The Effect and Safety of Dressing Composed by Nylon Threads Covered with Metallic Silver in Wound Treatment.</a> Brogliato, A.R., P.A. Borges, J.F. Barros, M. Lanzetti, S. Valenca, N.C. Oliveira, H.J. Izario-Filho, and C.F. Benjamim. International Wound Journal, 2012. <b>[Epub ahead of print]</b>. PMID[22905783].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22903379">Differential Chlorate Inhibition of Chaetomium globosum Germination, Hyphal Growth, and Perithecia Synthesis.</a> Biles, C.L., D. Wright, M. Fuego, A. Guinn, T. Cluck, J. Young, M. Martin, J. Biles, and S. Poudyal. Mycopathologia, 2012. <b>[Epub ahead of print]</b>. PMID[22903379].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22899575">Evaluation of Bioactive Potential of an Aloe Vera Sterol Extract.</a> Bawankar, R., V.C. Deepti, P. Singh, R. Subashkumar, G. Vivekanandhan, and S. Babu. Phytotherapy Research, 2012. <b>[Epub ahead of print]</b>. PMID[22899575].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p>
    <p> </p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22913407">The First Synthesis of an Epidiselenodiketopiperazine.</a> McMahon, T.C., S. Stanley, E. Kazyanskaya, D. Hung, and J.L. Wood. Organic letters, 2012. <b>[Epub ahead of print]</b>. PMID[22913407].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22926573">In Vitro Combination Studies of Benzothiazinone Lead Compound BTZ043 against Mycobacterium tuberculosis.</a> Lechartier, B., R.C. Hartkoorn, and S.T. Cole. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[22926573].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22906310">Meropenem Inhibits D,D-Carboxypeptidase Activity in Mycobacterium tuberculosis.</a> Kumar, P., K. Arora, J.R. Lloyd, I.Y. Lee, V. Nair, E. Fischer, H.I. Boshoff, and C.E. Barry, 3rd. Molecular Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[22906310].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22916795">Identification of Anti-tubercular Benzothiazinone Compounds by Ligand-based Design.</a> Karoli, T., B. Becker, J. Zuegg, U. Moellmann, S. Ramu, J.X. Huang, and M.A. Cooper. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22916795].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22895786">Syntheses of Mycobactin Analogs as Potent and Selective Inhibitors of Mycobacterium tuberculosis.</a> Juarez-Hernandez, R.E., S.G. Franzblau, and M.J. Miller. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22895786].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22899555">Antimycobacterials from Lovage Root (Ligusticum officinale Koch).</a> Guzman, J.D., D. Evangelopoulos, A. Gupta, J.M. Prieto, S. Gibbons, and S. Bhakta. Phytotherapy Research, 2012. <b>[Epub ahead of print]</b>. PMID[22899555].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p>
    <p> </p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22877245">Bioisosteric Transformations and Permutations in the Triazolopyrimidine Scaffold to Identify the Minimum Pharmacophore Required for Inhibitory Activity against Plasmodium falciparum Dihydroorotate Dehydrogenase.</a> Marwaha, A., J. White, F. El Mazouni, S.A. Creason, S. Kokkonda, F.S. Buckner, S.A. Charman, M.A. Phillips, and P.K. Rathod. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22877245].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22909422">Anti-plasmodial Activity of Dicoma tomentosa (Asteraceae) and Identification of Urospermal A-15- O-acetate as the Main Active Compound.</a> Jansen, O., M. Tits, L. Angenot, J.P. Nicolas, P. De Mol, J.B. Nikiema, and M. Frederich. Malaria journal, 2012. 11(1): p. 289. PMID[22909422].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22903383">Pentacyclic Ingamine Alkaloids, a New Antiplasmodial Pharmacophore from the Marine Sponge Petrosid ng5 sp5.</a> Ilias, M., M.A. Ibrahim, S.I. Khan, M.R. Jacob, B.L. Tekwani, L.A. Walker, and V. Samoylenko. Planta Medica, 2012. <b>[Epub ahead of print]</b>. PMID[22903383].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22901533">Sickle Cell Micrornas Inhibit the Malaria Parasite.</a> Duraisingh, M.T. and H.F. Lodish. Cell host &amp; Microbe, 2012. 12(2): p. 127-128. PMID[22901533].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22899089">In Vitro and in Vivo Evaluation of a Primaquine Prodrug without Red Blood Cells Membrane Destabilization Property.</a> Davanco, M.G., M.L. de Campos, M.A. Nogueira, S.L. Campos, R.V. Marques, J.L. Dos Santos, C.M. Chin, L.M. da Fonseca, and R.G. Peccinini. Biopharmaceutics &amp; Drug Disposition, 2012. <b>[Epub ahead of print]</b>. PMID[22899089].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22866915">Goniomedines A and B: Unprecedented Bisindole Alkaloids Formed through Fusion of Two Indole Moieties Via a Dihydropyran Unit.</a> Beniddir, M.A., M.T. Martin, M.E. Tran Huu Dau, P. Grellier, P. Rasoanaivo, F. Gueritte, and M. Litaudon. Organic Letters, 2012. 14(16): p. 4162-4165. PMID[22866915].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p>
    <p> </p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22795629">Benzoylbenzimidazole-based Selective Inhibitors Targeting Cryptosporidium parvum and Toxoplasma gondii Calcium-dependent Protein Kinase-1.</a> Zhang, Z., K.K. Ojo, S.M. Johnson, E.T. Larson, P. He, J.A. Geiger, A. Castellanos-Gonzalez, A.C. White, Jr., M. Parsons, E.A. Merritt, D.J. Maly, C.L. Verlinde, W.C. Van Voorhis, and E. Fan. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(16): p. 5264-5267. PMID[22795629].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22907279">Burden of Disease from Cryptosporidiosis.</a> Shirley, D.A., S.N. Moonah, and K.L. Kotloff. Current Opinion in Infectious Diseases, 2012. <b>[Epub ahead of print]</b>. PMID[22907279].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22908155">A Focused Small Molecule Screen Identifies 14 Compounds with Distinct Effects on Toxoplasma gondii.</a> Kamau, E.T., A.R. Srinivasan, M.J. Brown, M.G. Fair, E.C. Caraher, and J.P. Boyle. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[22908155].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0817-0830.</p>
    <p> </p>

    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306626300002">Biochemical and Molecular Characterization of the Pyrimidine Biosynthetic Enzyme Dihydroorotate Dehydrogenase from Toxoplasma gondii.</a> Triana, M.A.H., M.H. Huynh, M.F. Garavito, B.A. Fox, D.J. Bzik, V.B. Carruthers, M. Loffler, and B.H. Zimmermann. Molecular and Biochemical Parasitology, 2012. 184(2): p. 71-81. ISI[000306626300002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306538900007">Antioxidant Activity, Polyphenols Content and Antimicrobial Activity of Several Native Pteridophytes of Romania.</a> Soare, L.C., M. Ferdes, S. Stefanov, Z. Denkova, R. Nicolova, P. Denev, C. Bejan, and A. Paunescu. Notulae Botanicae Horti Agrobotanici Cluj-Napoca, 2012. 40(1): p. 53-57. ISI[000306538900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306789400007">The Fatty acid 8,11-diol Synthase of Aspergillus fumigatus Is Inhibited by Imidazole Derivatives and Unrelated to Ppob.</a> Jerneren, F. and E.H. Oliw. Lipids, 2012. 47(7): p. 707-717. ISI[000306789400007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306543300019">Antifungal Activity and Cytotoxicity of Zinc, Calcium, or Copper Alginate Fibers.</a>  Gong, Y., G.T. Han, Y.M. Zhang, Y. Pan, X.B. Li, Y.Z. Xia, and Y. Wu. Biological Trace Element Research, 2012. 148(3): p. 415-419. ISI[000306543300019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306695700033">Alpha-amino acid-derived 2-Phenylimidazoles with Potential Antimycobacterial Activity.</a> Cvejn, D., V. Klimesova, and F. Bures. Central European Journal of Chemistry, 2012. 10(5): p. 1681-1687. ISI[000306695700033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306562300027">Synthesis, Characterization, Antibacterial and Antifungal Activity of Yttrium(III) Complexes Including 1,10-Phenanthroline.</a> Cai, M.J., J.D. Chen, and M. Taha. Chinese Journal of Chemistry, 2012. 30(7): p. 1531-1538. ISI[000306562300027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0817-0830.</p> 
    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306681100006">Antifungal Activity and Detailed Chemical Characterization of Cistus ladanifer Phenolic Extracts.</a>  Barros, L., M. Duenas, C.T. Alves, S. Silva, M. Henriques, C. Santos-Buelga, and I. Ferreira. Industrial Crops and Products, 2012. 41: p. 41-45. ISI[000306681100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0817-0830.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
