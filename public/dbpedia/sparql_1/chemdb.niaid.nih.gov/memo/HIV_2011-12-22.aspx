

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-12-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6R7xx2bGKJUQPyV1k395VI0Nlns8tQ7/e/ClOdNxGla4yce9/A7O59qyCuSNf2x5FCOHreviFggy6jeymx5Xib7GAPprE/2044tsEqKX7IqG547dqGyyqOVJBjM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="40DA317C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  December 23, 2011 - January 5, 2012</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22149368">Combination Nucleoside/Nucleotide Reverse Transcriptase Inhibitors for Treatment of HIV Infection.</a> Akanbi, M.O., K. Scarci, B. Taiwo, and R.L. Murphy, Expert Opinion on Pharmacotherapy, 2012. 13(1): p. 65-79; PMID[22149368].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22209231">The Lectins Griffithsin, Cyanovirin-N and Scytovirin Inhibit HIV-1 Binding to the DC-SIGN Receptor and Transfer to CD4(+) Cells.</a> Alexandre, K.B., E.S. Gray, H. Mufhandu, J.B. McMahon, E. Chakauya, B.R. O&#39;Keefe, R. Chikwamba, and L. Morris, Virology, 2011. <b>[Epub ahead of print]</b>; PMID[22209231].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21987241">CNS Effects of a CCR5 Inhibitor in HIV-infected Subjects: A Pharmacokinetic and Cerebral Metabolite Study.</a> Garvey, L., M. Nelson, N. Latch, O.W. Erlwein, J.M. Allsop, A. Mitchell, S. Kaye, V. Watson, D. Back, S.D. Taylor-Robinson, and A. Winston, Journal of Antimicrobial Chemotherapy, 2012. 67(1): p. 206-212; PMID[21987241].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21914723">Substrate Mimicry: HIV-1 Reverse Transcriptase Recognizes 6-Modified-3&#39;-azido-2&#39;,3&#39;-dideoxyguanosine-5&#39;-triphosphates as Adenosine Analogs.</a> Herman, B.D., R.F. Schinazi, H.W. Zhang, J.H. Nettles, R. Stanton, M. Detorio, A. Obikhod, U. Pradere, S.J. Coats, J.W. Mellors, and N. Sluis-Cremer, Nucleic Acids Research, 2012. 40(1): p. 381-390; PMID[21914723].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22154762">Design and Synthesis of Novel Beta-diketo Derivatives as HIV-1 Integrase Inhibitors.</a> Hu, L., S. Zhang, X. He, Z. Luo, X. Wang, W. Liu, and X. Qin, Bioorganic &amp; Medicinal Chemistry, 2012. 20(1): p. 177-182; PMID[22154762].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22200908">Mutational Patterns in the Frameshift-regulating Site of HIV-1 Selected by Protease Inhibitors.</a> Knops, E., L. Brakier-Gingras, E. Schulter, H. Pfister, R. Kaiser, and J. Verheyen, Medical Microbiology and Immunology, 2011. <b>[Epub ahead of print]</b>; PMID[22200908].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22149610">Elvitegravir: A Once-daily, Boosted, HIV-1 Integrase Inhibitor.</a> Lampiris, H.W., Expert Review of Anti-Infective Therapy, 2012. 10(1): p. 13-20; PMID[22149610].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22104436">Modification of HIV-1 Reverse Transcriptase and Integrase Activity by Gold(III) Complexes in Direct Biochemical Assays.</a> Mphahlele, M., M. Papathanasopoulos, M.A. Cinellu, M. Coyanis, S. Mosebi, T. Traut, R. Modise, J. Coates, and R. Hewer, Bioorganic &amp; Medicinal Chemistry, 2012. 20(1): p. 401-407; PMID[22104436].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22211658">Induction of Strong anti-HIV Cellular Immunity by a Combination of Clostridium Perfringens Expressing HIV gag and Virus Like Particles.</a> Pegu, P., R. Helmus, P. Gupta, P. Tarwater, L. Caruso, C. Shen, T. Ross, and Y. Chen, Current HIV Research, 2011. <b>[Epub ahead of print]</b>; PMID[22211658].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22214582">Development of Anti-HIV Agents Based on Chemical Biology.</a> Tamamura, H., Yakugaku Zasshi, 2012. 132(1): p. 69-78; PMID[22214582].
    <br />
    <b>[Pubmed]</b>. HIV_1223-010512.</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297344600001">4&#39;-C-Nucleoside Derivatives: Synthesis and Antiviral Properties.</a> Alexandrova, L.A., Russian Journal of Bioorganic Chemistry, 2011. 37(6): p. 651-671; ISI[000297344600001].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297545200001">Electronic Structure and Spectral and Luminescent Properties of anti-HIV-active Aminophenols.</a> Bazyl, O.K., V.Y. Artyukhov, and G.V. Mayer, Russian Physics Journal, 2011. 54(6): p. 619-626; ISI[000297545200001].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297555300043">The Identification of a Small Molecule Compound That Reduces HIV-1 Nef-Mediated Viral Infectivity Enhancement.</a> Chutiwitoonchai, N., M. Hiyoshi, P. Mwimanzi, T. Ueno, A. Adachi, H. Ode, H. Sato, O.T. Fackler, S. Okada, and S. Suzu, Plos One, 2011. 6(11): e27696; ISI[000297555300043].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297317000020">Complex Drug Interactions of HIV Protease Inhibitors 2: In Vivo Induction and in Vitro to in Vivo Correlation of Induction of Cytochrome P450 1A2, 2B6, and 2C9 by Ritonavir or Nelfinavir.</a> Kirby, B.J., A.C. Collier, E.D. Kharasch, V. Dixit, P. Desai, D. Whittington, K.E. Thummel, and J.D. Unadkat, Drug Metabolism and Disposition, 2011. 39(12): p. 2329-2337; ISI[000297317000020].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297777600012">Synthesis of New Acyclic Nucleoside Phosphonates (ANPs) Substituted on the 1 &#39; and/or 2 &#39; positions.</a> Kundarapu, M., D. Marchand, S.G. Dumbre, and P. Herdewijn, Tetrahedron Letters, 2011. 52(51): p. 6896-6898; ISI[000297777600012].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297789900038">Modeling of Human Prokineticin Receptors: Interactions with Novel Small-molecule Binders and Potential Off-target Drugs.</a> Levit, A., T. Yarnitzky, A. Wiener, R. Meidan, and M.Y. Niv, Plos One, 2011. 6(11); ISI[000297789900038].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297929800248">Toxicity of anti-HIV Drugs and the Use of Tempol as a Therapeutic Agent.</a> Nguyen, P.G., Y.M. Liu, and M.C. Poirier, Environmental and Molecular Mutagenesis, 2011. 52: p. S74-S74; ISI[000297929800248].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297450000047">CpG Protects Human Monocytic Cells against HIV-V pr-Induced Apoptosis by Cellular Inhibitor of Apoptosis-2 through the Calcium-activated JNK Pathway in a TLR9-Independent Manner.</a> Saxena, M., A. Busca, S. Pandey, M. Kryworuchko, and A. Kumar, Journal of Immunology, 2011. 187(11): p. 5865-5878; ISI[000297450000047].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297537800002">Plant as a Source of Natural Antiviral Agents.</a> Sohail, M.N., F. Rasul, A. Karim, U. Kanwal, and I.H. Attitalla, Asian Journal of Animal and Veterinary Advances, 2011. 6(12): p. 1125-1152; ISI[000297537800002].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297523700032">An Efficient and Selective Route to Hybrid Trifluoromethyl-substituted Gamma-lactones or Fused Nitrogen Derivatives via Cascade Reactions.</a> Tucaliuc, R., V.V. Cotea, C. Moldoveanu, G. Zbancioc, C. Deleanu, P.G. Jones, and Mangalagiu, II, Tetrahedron Letters, 2011. 52(48): p. 6439-6442; ISI[000297523700032].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297755400001">Human HERC5 Restricts an Early Stage of HIV-1 Assembly by a Mechanism Correlating with the ISGylation of gag.</a> Woods, M.W., J.N. Kelly, C.J. Hattlmann, J.G.K. Tong, L.S. Xu, M.D. Coleman, G.R. Quest, J.R. Smiley, and S.D. Barr, Retrovirology, 2011. 8; ISI[000297755400001].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297589900024">Synthesis and in Vitro Enzymatic and Antiviral Evaluation of Phosphoramidate d4T Derivatives as Chain Terminators.</a> Yang, S.Q., C. Pannecouque, E. Lescrinier, A. Girauta, and P. Herdewijn, Organic &amp; Biomolecular Chemistry, 2012. 10(1): p. 146-153; ISI[000297589900024].
    <br />
    <b>[WOS]</b>. HIV_1223-010512.</p>

    <p class="memofmt2-2">Patent citations</p>

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111215&amp;CC=WO&amp;NR=2011156674A2&amp;KC=A2">Thioether Prodrug Compositions as anti-HIV and Anti-retroviral Agents.</a> Appella, D., E. Appella, J.K. Inman, L.M. Miller Jenkins, R. Hayashi, and D. Wang. Patent. 2011-US39909 2011156674, 56pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111124&amp;CC=WO&amp;NR=2011143772A1&amp;KC=A1">Pyrrolopyrazole Derivatives as HIV Replication Inhibitors and Their Preparation and Use for the Treatment of HIV Infection.</a> Lepage, O., P.K. Bhardwaj, A.-M. Faucher, C. Grand-Maitre, J.-E. Lacoste, L. Lamorte, and J.-F. Mercier. Patent. 2011-CA50308 2011143772, 78pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1223-010512.</p>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111117&amp;CC=WO&amp;NR=2011143218A2&amp;KC=A2">Plant-derived Compositions Comprising an Extract of Rubia cordifolia for Treatment of HIV Infection.</a> Nair, M.P.N., Z.M. Saiyed, and N.H. Gandhi. Patent. 2011-US35922 2011143218, 15pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111215&amp;CC=WO&amp;NR=2011154580A1&amp;KC=A1">Novel CXCR4 Inhibitors as anti-HIV Agents.</a> Puig de la Bellacasa Cazorla, R., J.I. Borrell Bilbao, J. Teixido Closa, and J. Este Araque. Patent. 2011-ES70407 2011154580, 38pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111208&amp;CC=WO&amp;NR=2011153319A1&amp;KC=A1">Preparation of C28 Amides of Modified C3 Betulinic acid Derivatives as HIV Maturation Inhibitors.</a> Regueiro-Ren, A., Z. Liu, J. Swidorski, N.A. Meanwell, S.-Y. Sit, J. Chen, Y. Chen, and N. Sin. Patent. 2011-US38884</p>

    <p class="plaintext">2011153319, 338pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111208&amp;CC=WO&amp;NR=2011153315A1&amp;KC=A1">Preparation of Modified C3 Betulinic acid Derivatives as HIV Maturation Inhibitors.</a> Regueiro-Ren, A., J. Swidorski, Z. Liu, N.A. Meanwell, S.-Y. Sit, and J. Chen. Patent. 2011-US38879 2011153315, 157pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111129&amp;CC=US&amp;NR=8067430B1&amp;KC=B1">Anti-HIV Activity of the Opioid Antagonist Naloxone.</a> Ugen, K.E., S. Specter, S.B. Nyland, and C. Cao. Patent. 2004-902471 8067430, 10pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1223-010512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
