

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-09-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eI3aPuT5vZK4hThFKZpKmJSGvkfqAuQ3lYs85uK9aa5B9EIeQkzKNuPbF3nvBPLA4Srh3bHWrIndaSkTDOqoWBDPqGzSYzIApcbxhsYCZYvdb0GglQJ5EZ0u7iY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8E523BC8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 28 - September 10, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25511999">Design, Synthesis, and Biological Evaluation of Novel Benzoyl Diarylamine/ether Derivatives as</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25511999">Potential anti-HIV-1 Agents.</a> Zhang, L., J. Guo, X. Liu, H. Liu, E. De Clercq, C. Pannecouque, and X.</p>

    <p class="plaintext">Liu. Chemical Biology &amp; Drug Design, 2015. 86(3): p. 333-343. PMID[25511999].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26150289">Discovery of N-Aryl-naphthylamines as in Vitro Inhibitors of the Interaction between HIV Integrase and the Cofactor Ledgf/p75.</a> Cuzzucoli Crucitti, G., L. Pescatori, A. Messore, V.N. Madia, G. Pupo, F. Saccoliti, L. Scipione, S. Tortorella, F.S. Di Leva, S. Cosconati, E. Novellino, Z. Debyser, F. Christ, R. Costi, and R. Di Santo. European Journal of Medicinal Chemistry, 2015. 101: p. 288-294. PMID[26150289].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26252631">Synthesis and anti-HIV Activity of Novel 4&#39;-Trifluoromethylated 5&#39;-deoxycarbocyclic Nucleoside Phosphonic Acids.</a> Jee, J.P., S. Kim, and J.H. Hong. Nucleosides, Nucleotides &amp; Nucleic Acids, 2015. 34(9): p. 620-638. PMID[26252631].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0828-091015.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359739600004">Antiviral Activities of Whey Proteins.</a> Ng, T., R. Cheung, J. Wong, Y. Wang, D. Ip, D. Wan, and J. Xia. Applied Microbiology and Biotechnology, 2015. 99(17): p. 6997-7008. ISI[000359739600004].</p>
     
    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359951700066">Broad-spectrum anti-HIV RT RNA Aptamers.</a> Lange, M.J., P.D.M. Nguyen, M.K. Callaway, and D.H. Burke. Molecular Therapy, 2015. 23: p. S28-S28. ISI[000359951700066].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359678300016">Flavonoids Isolated from Heat-processed Epimedium koreanum and their anti-HIV-1 Activities.</a> Li, H.M., C. Zhou, C.H. Chen, R.T. Li, and K.H. Lee. Helvetica Chimica Acta, 2015. 98(8): p. 1177-1187. ISI[000359678300016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359967000248">HIV Inhibitory Activity from Papua New Guinean Medicinal Plants.</a> Pond, C.D., P.P. Rai, L.R. Barrows, and K. Matainaho. Planta Medica, 2015. 81(11): p. 907-907. ISI[000359967000248].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359274400022">Microwave Assisted Synthesis and anti-HIV-RT Activity of Diaryl benzo[1,3]thiazin-4-ones.</a></p>

    <p class="plaintext">Feng, J.N., X.H. Li, J. Shao, M. Zhu, Y. Li, H. Chen, and X.L. Li. Chinese Journal of Organic Chemistry, 2015. 35(6): p. 1370-1374. ISI[000359274400022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359313000017">Synthesis and Anti-retroviral Activity of Novel 5-Deoxy-5,5-difluoro-threosyl Nucleoside Phosphonic acid Analogs.</a> Kim, S., J.P. Jee, and J.H. Hong. Bulletin of the Korean Chemical Society, 2015. 36(8): p. 2020-2026. ISI[000359313000017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359747700023">Synthesis and Molecular Docking Studies of Oxochromenyl xanthenone and Indolyl xanthenone Derivatives as anti-HIV-1 RT Inhibitors.</a> Kasralikar, H.M., S.C. Jadhavar, and S.R. Bhusare. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(18): p. 3882-3886. ISI[000359747700023].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000359998200007">Synthesis and Molecular Docking Study of Novel Chromeno-chromenones as anti-HIV-1 NNRT Inhibitors.</a> Kasralikar, H.M., S.C. Jadhavar, and S.R. Bhusare. Synlett, 2015. 26(14): p. 1969-1972. ISI[000359998200007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0828-091015.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">12. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150806&amp;CC=US&amp;NR=2015216926A1&amp;KC=A1">Methods of Reactivating a Latent Human Immunodeficiency Virus (HIV) Using Proteasome Inhibitors.</a> Kutsch, O. Patent. 2015. 2014-14316144 20150216926: 21pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">13. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150820&amp;CC=WO&amp;NR=2015123230A1&amp;KC=A1">Benzothiazole Macrocycles as Inhibitors of Human Immunodeficiency Virus Replication.</a> Naidu, B.N. and M. Patel. Patent. 2015. 2015-US15307 2015123230: 45pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150820&amp;CC=US&amp;NR=2015232463A1&amp;KC=A1">Preparation of Pyridinyl acetic acid Derivatives for Use as Human Immunodeficiency Virus Replication Inhibitors.</a> Naidu, B.N., M. Patel, J.L. Romine, D.R. St. Laurent, T. Wang, Z. Zhang, and J.F. Kadow. Patent. 2015. 2015-14619438 20150232463: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150820&amp;CC=US&amp;NR=2015232480A1&amp;KC=A1">Preparation of Imidazopyridine Macrocycles as Inhibitors of Human Immunodeficiency Virus Replication.</a> Peese, K., Z. Wang, J.F. Kadow, P. Sivaprakasam, and B.N. Naidu. Patent. 2015. 2015-14619213 20150232480: 74pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150820&amp;CC=WO&amp;NR=2015123182A1&amp;KC=A1">Preparation of Pyrazolopyrimidine Macrocycles as Inhibitors of Human Immunodeficiency Virus Replication.</a> Peese, K., Z. Wang, D.R. Langley, J.F. Kadow, and N.B. Naidu. Patent. 2015. 2015-US15154 2015123182: 60pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0828-091015.</p>

    <br />

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150903&amp;CC=WO&amp;NR=2015130947A1&amp;KC=A1">Benzende sulfonamide Derivatives as HIV Integrase Inhibitors.</a> Wang, X.S. Patent. 2015. 2015-US17781 2015130947: 35pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0828-091015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
