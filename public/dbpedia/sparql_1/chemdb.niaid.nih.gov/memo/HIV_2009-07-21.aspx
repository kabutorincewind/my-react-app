

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-07-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="itLvncPClSW9HRUqZo46zgR0PbRX9tet5/WqtGw3GgR/bshwXjf8v9fom1H445YNwWDGMseRB5f6p9eCneHv855yJ28drypbLacP9xYob39LgHWBbKt/bIbaVfc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="798D76D0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  July 8 - July 21, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19615335?ordinalpos=14&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A large library based on a novel (CH2) scaffold: Identification of HIV-1 inhibitors.</a>  Xiao X, Feng Y, Vu BK, Ishima R, Dimitrov DS.  Biochem Biophys Res Commun. 2009 Jul 15. [Epub ahead of print]  PMID: 19615335</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19603446?ordinalpos=30&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of Flavopiridol Analogues that Selectively Inhibit Positive Transcription Elongation Factor (P-TEFb) and Block HIV-1 Replication.</a>  Ali A, Ghosh A, Nathans RS, Sharova N, O&#39;Brien S, Cao H, Stevenson M, Rana TM.  Chembiochem. 2009 Jul 14. [Epub ahead of print]  PMID: 19603446</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19596885?ordinalpos=31&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anti-HIV Activity, Cross-Resistance, Cytotoxicity and Intracellular Pharmacology of the 3&#39;-Azido-2&#39;,3&#39;-Dideoxypurine Nucleosides.</a>  Sluis-Cremer N, Koontz D, Bassit L, Hernandez-Santiago BI, Detorio M, Rapp KL, Amblard F, Bondada L, Grier J, Coats SJ, Schinazi RF, Mellors JW.  Antimicrob Agents Chemother. 2009 Jul 13. [Epub ahead of print]  PMID: 19596885</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19594941?ordinalpos=34&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of light-independent inhibition of human immunodeficiency virus-1 infection through bioguided fractionation of Hypericum perforatum.</a>  Maury W, Price JP, Brindley MA, Oh C, Neighbors JD, Wiemer DF, Wills N, Carpenter S, Hauck C, Murphy P, Widrlechner MP, Delate K, Kumar G, Kraus GA, Rizshsky L, Nikolau B.  Virol J. 2009 Jul 13;6(1):101. [Epub ahead of print]  PMID: 19594941</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19593361?ordinalpos=35&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Membrane-anchored HIV-1 N-heptad repeat peptides are highly potent cell fusion inhibitors via an altered mode of action.</a>  Wexler-Cohen Y, Shai Y.  PLoS Pathog. 2009 Jul;5(7):e1000509. Epub 2009 Jul 10.  PMID: 19593361</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19591191?ordinalpos=38&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, Synthesis, and SAR of Naphthyl-Substituted Diarylpyrimidines as Non-Nucleoside Inhibitors of HIV-1 Reverse Transcriptase.</a>  Liang YH, Feng XQ, Zeng ZS, Chen FE, Balzarini J, Pannecouque C, De Clercq E.  ChemMedChem. 2009 Jul 9. [Epub ahead of print]  PMID: 19591191</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19587055?ordinalpos=48&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">HIV-1 Nucleocapsid Inhibitors Impede Trans Infection in Cellular and Explant Models and Protect Non-Human Primates from Infection.</a>  Wallace GS, Cheng-Mayer C, Schito ML, Fletcher P, Miller Jenkins LM, Hayashi R, Neurath AR, Appella E, Shattock RJ.  J Virol. 2009 Jul 8. [Epub ahead of print]  PMID: 19587055</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19582296?ordinalpos=53&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Peptide bond mimicry by (E)-alkene and (Z)-fluoroalkene peptide isosteres: synthesis and bioevaluation of alpha-helical anti-HIV peptide analogues.</a>  Oishi S, Kamitani H, Kodera Y, Watanabe K, Kobayashi K, Narumi T, Tomita K, Ohno H, Naito T, Kodama E, Matsuoka M, Fujii N.  Org Biomol Chem. 2009 Jul 21;7(14):2872-7. Epub 2009 Jun 4.  PMID: 19582296</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19527935?ordinalpos=63&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Diketoacid-genre HIV-1 integrase inhibitors containing enantiomeric arylamide functionality.</a>  Zhao XZ, Maddali K, Marchand C, Pommier Y, Burke TR Jr.  Bioorg Med Chem. 2009 Jul 15;17(14):5318-24. Epub 2009 May 8.  PMID: 19527935</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19515569?ordinalpos=65&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel 3alpha-methoxyserrat-14-en-21beta-ol (PJ-1) and 3beta-methoxyserrat-14-en-21beta-ol (PJ-2)-curcumin, kojic acid, quercetin, and baicalein conjugates as HIV agents.</a>  Tanaka R, Tsujii H, Yamada T, Kajimoto T, Amano F, Hasegawa J, Hamashima Y, Node M, Katoh K, Takebe Y.  Bioorg Med Chem. 2009 Jul 15;17(14):5238-46. Epub 2009 May 27.  PMID: 19515569</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19497744?ordinalpos=67&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of a beta3-peptide HIV fusion inhibitor with improved potency in live cells.</a>  Bautista AD, Stephens OM, Wang L, Domaoal RA, Anderson KS, Schepartz A.  Bioorg Med Chem Lett. 2009 Jul 15;19(14):3736-8. Epub 2009 May 15.  PMID: 19497744</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19409474?ordinalpos=73&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vitro anti-HIV activity of five selected South African medicinal plant extracts.</a>  Klos M, van de Venter M, Milne PJ, Traore HN, Meyer D, Oosthuizen V.  J Ethnopharmacol. 2009 Jul 15;124(2):182-8. Epub 2009 May 3.  PMID: 19409474</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p class="title">13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267587600023">Synthesis of 2,6-dioxatricyclo[3.3.1.0(3,7)]nonanes by intramolecular haloetherification and/or transannular hydroxycyclization of alkenes in [4+3]-cycloadducts.</a> Montana, AM; Barcia, JA; Kociok-Kohn, G; Font-Bardia, M.  TETRAHEDRON, 2009; 65(7): 5308-5321. </p>

    <p class="title">14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267432600037">Chemical Constituents of Heteroplexis micocephala.</a>  Fan, XN; Zi, JC; Zhu, CG; Xu, WD; Cheng, W; Yang, S; Guo, Y; Shi, JG.  JOURNAL OF NATURAL PRODUCTS, 2009; 72(6): 1184-1190. </p>

    <p class="title">15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267480500005">Synthesis, Antiviral and Cytostatic Evaluation of Unsaturated Exomethylene and Keto D-Lyxopyranonucleoside Analogues.</a>  Tzioumaki, N; Tsoukala, E; Manta, S; Agelis, G; Balzarini, J; Komiotis, D.  ARCHIV DER PHARMAZIE, 2009; 342(6): 353-360. </p>      
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
