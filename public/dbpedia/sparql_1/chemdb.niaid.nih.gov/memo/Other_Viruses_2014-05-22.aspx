

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-05-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uVOmxf38bk366AuDRfkhFr7agqUilEqbVcMqV2zDseUYGwXP1cAtdBuztae1MA06MG2lH+qtUE08dS1Tzs+lsWiXeUqknKb+LTIDPx7SWYm7ji5QT5UyitRR+O8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7984A851" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: May 9 - May 22, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24731274">Synthesis, Structure-Activity Relationships and Biological Evaluation of Dehydroandrographolide and Andrographolide Derivatives as Novel anti-Hepatitis B Virus Agents<i>.</i></a> Chen, H., Y.B. Ma, X.Y. Huang, C.A. Geng, Y. Zhao, L.J. Wang, R.H. Guo, W.J. Liang, X.M. Zhang, and J.J. Chen. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(10): p. 2353-2359; PMID[24731274].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0509-052214.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24725239">Ledipasvir and Sofosbuvir for Untreated HCV Genotype 1 Infection<i>.</i></a> Afdhal, N., S. Zeuzem, P. Kwo, M. Chojkier, N. Gitlin, M. Puoti, M. Romero-Gomez, J.P. Zarski, K. Agarwal, P. Buggisch, G.R. Foster, N. Brau, M. Buti, I.M. Jacobson, G.M. Subramanian, X. Ding, H. Mo, J.C. Yang, P.S. Pang, W.T. Symonds, J.G. McHutchison, A.J. Muir, A. Mangia, and P. Marcellin. The New England Journal of Medicine, 2014. 370(20): p. 1889-1898; PMID[24725239].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24824429">MicroRNA-27a Modulates HCV Infection in Differentiated Hepatocyte-like Cells from Adipose Tissue-derived Mesenchymal Stem Cells<i>.</i></a> Choi, J.E., W. Hur, J.H. Kim, T.Z. Li, E.B. Lee, S.W. Lee, W. Kang, E.C. Shin, T. Wakita, and S.K. Yoon. Plos One, 2014. 9(5): p. e91958; PMID[24824429].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24731273">Design and Synthesis of Spirocyclic Compounds as HCV Replication Inhibitors by Targeting Viral NS4B Protein<i>.</i></a> Tai, V.W., D. Garrido, D.J. Price, A. Maynard, J.J. Pouliot, Z. Xiong, J.W. Seal, 3rd, K.L. Creech, L.H. Kryn, T.M. Baughman, and A.J. Peat. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(10): p. 2288-2294; PMID[24731273].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24830295">Hepatitis C Virus Cell-Cell Transmission and Resistance to Direct-acting Antiviral Agents<i>.</i></a> Xiao, F., I. Fofana, L. Heydmann, H. Barth, E. Soulier, F. Habersetzer, M. Doffoel, J. Bukh, A.H. Patel, M.B. Zeisel, and T.F. Baumert. Plos Pathogens, 2014. 10(5): p. e1004128; PMID[24830295].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24745970">Synthesis and anti-BVDV Activity of Novel delta-Sultones in Vitro: Implications for HCV Therapies<i>.</i></a> Xu, H.W., L.J. Zhao, H.F. Liu, D. Zhao, J. Luo, X.P. Xie, W.S. Liu, J.X. Zheng, G.F. Dai, H.M. Liu, L.H. Liu, and Y.B. Liang. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(10): p. 2388-2391; PMID[24745970].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0509-052214.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24629600">In Vitro Inhibition of Herpes Simplex Virus Type 1 Replication by Mentha suaveolens Essential Oil and Its Main Component Piperitenone Oxide<i>.</i></a> Civitelli, L., S. Panella, M.E. Marcocci, A. De Petris, S. Garzoli, F. Pepi, E. Vavala, R. Ragno, L. Nencioni, A.T. Palamara, and L. Angiolella. Phytomedicine, 2014. 21(6): p. 857-865; PMID[24629600].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0509-052214.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333780800054">The Chemistry and Biological Potential of Azetidin-2-ones<i>.</i></a> Arya, N., A.Y. Jagdale, T.A. Patil, S.S. Yeramwar, S.S. Holikatti, J. Dwivedi, C.J. Shishoo, and K.S. Jain. European Journal of Medicinal Chemistry, 2014. 74: p. 619-656; ISI[000333780800054].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334836500012">Potent Hepatitis C Inhibitors Bind Directly to NS5A and Reduce Its Affinity for RNA<i>.</i></a> Ascher, D.B., J. Wielens, T.L. Nero, L. Doughty, C.J. Morton, and M.W. Parker. Scientific Reports, 2014. 4: 4765; ISI[000334836500012].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334916800002">Synthesis and Antiviral Activity of Ethyl 1,2-Dimethyl-5-hydroxy-1H-indole-3-carboxylates and Their Derivatives<i>.</i></a> Ivashchenko, A.V., P.M. Yamanushkin, O.D. Mit&#39;kin, V.M. Kisil, O.M. Korzinov, V.Y. Vedenskii, I.A. Leneva, E.A. Bulanova, V.V. Bychko, and I.M. Okun. Pharmaceutical Chemistry Journal, 2014. 47(12): p. 636-650; ISI[000334916800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334507600036">Inhibition of HCV Replication by Cyclophilin Antagonists Is Linked to Replication Fitness and Occurs by Inhibition of Membranous Web Formation<i>.</i></a> Madan, V., D. Paul, V. Lohmann, and R. Bartenschlager. Gastroenterology, 2014. 146(5): p. 1361-1372; ISI[000334507600036].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334744100026">Protides of N-(3-(5-(2 &#39;-Deoxyuridine))prop-2-ynyl)octanamide as Potential Anti-tubercular and Anti-viral Agents<i>.</i></a> McGuigan, C., M. Derudas, B. Gonczy, K. Hinsinger, S. Kandil, F. Pertusati, M. Serpi, R. Snoeck, G. Andrei, J. Balzarini, T.D. McHugh, A. Maitra, E. Akorli, D. Evangelopoulos, and S. Bhakta. Bioorganic &amp; Medicinal Chemistry, 2014. 22(9): p. 2816-2824; ISI[000334744100026].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335011900007">Synthesis and Antiviral Evaluation of 4 &#39;-(1,2,3-Triazol-1-yl)thymidines<i>.</i></a> Vernekar, S.K.V., L. Qiu, J. Zacharias, R.J. Geraghty, and Z.Q. Wang. MedChemComm, 2014. 5(5): p. 603-608; ISI[000335011900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0509-052214.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334364300018">Robust and Persistent Replication of the Genotype 6a Hepatitis C Virus Replicon in Cell Culture<i>.</i></a> Yu, M., B. Peng, K. Chan, R.Y. Gong, H.L. Yang, W. Delaney, and G.F. Cheng. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2638-2646; ISI[000334364300018].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0509-052214.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
