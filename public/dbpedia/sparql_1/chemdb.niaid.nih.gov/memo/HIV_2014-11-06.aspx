

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-11-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KgMvojFVHPZdaHoeauQmBif+P2Yda/BIQw7tXIlJWAOs34IO6e7oSpzXHvNOfIPWD7ZgANKrWDJDYdLfJGza7SmCqH7iXn3p3stwexthaQUdsHfkSj10W7y7xzA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5AEF53DB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: October 24 - November 6, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25091947">Dilazep Synergistically Reactivates Latent HIV-1 in Latently Infected Cells.</a> Zeng, H., S. Liu, P. Wang, X. Qu, H. Ji, X. Wang, X. Zhu, Z. Song, X. Yang, Z. Ma, and H. Zhu. Molecular Biology Reports, 2014. 41(11): p. 7697-7704. PMID[25091947].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25203778">Discovery of Small Molecular Inhibitors Targeting HIV-1 gp120-CD4 Interaction Derived from BMS-378806.</a> Liu, T., B. Huang, P. Zhan, E. De Clercq, and X. Liu. European Journal of Medicinal Chemistry, 2014. 86: p. 481-490. PMID[25203778].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25155598">Phenyl-1-pyridin-2yl-ethanone-based Iron Chelators Increase IkappaB-Alpha Expression, Modulate CDK2 and CDK9 Activities, and Inhibit HIV-1 Transcription.</a> Kumari, N., S. Iordanskiy, D. Kovalskyy, D. Breuer, X. Niu, X. Lin, M. Xu, K. Gavrilenko, F. Kashanchi, S. Dhawan, and S. Nekhai. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6558-6571. PMID[25155598].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%2025285337">Disulfide Reshuffling Triggers the Release of a Thiol-free anti-HIV Agent to Make Up Fast-acting, Potent Macromolecular Prodrugs.</a> Kock, A., K. Zuwala, A.A. Smith, P. Ruiz-Sanchis, B.M. Wohl, M. Tolstrup, and A.N. Zelikin. Chemical Communications, 2014. 50(93): p. 14498-14500. PMID[25285337].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25218908">A Cyclic GB Virus C Derived Peptide with anti-HIV-1 Activity Targets the Fusion Peptide of HIV-1.</a> Galatola, R., A. Vasconcelos, Y. Perez, A. Cruz, M. Pujol, M.A. Alsina, M.J. Gomara, and I. Haro. European Journal of Medicinal Chemistry, 2014. 86: p. 589-604. PMID[25218908].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23835833">Synthesis and anti-HIV-1 Screening of Novel N&#39;-(1-(Aryl)ethylidene)-2-(5,5-dioxido-3-phenylbenzo[e]pyrazolo[4,3-C][1,2]thiazi N-4(1H)-yl)acetohydrazides.</a> Aslam, S., M. Ahmad, M. Zia-Ur-Rehman, C. Montero, M. Detorio, M. Parvez, and R.F. Schinazi. Archives of Pharmacal Research, 2014. 37(11): p. 1380-1393. PMID[23835833].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25073485">1E7-03, a Low MW Compound Targeting Host Protein Phosphatase-1, Inhibits HIV-1 Transcription.</a> Ammosova, T., M. Platonov, A. Ivanov, Y.S. Kont, N. Kumari, K. Kehn-Hall, M. Jerebtsova, A.A. Kulkarni, A. Uren, D. Kovalskyy, and S. Nekhai. British Journal of Pharmacology, 2014. 171(22): p. 5059-5075. PMID[25073485].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22066494">Investigating the Role of Metal Chelation in HIV-1 Integrase Strand Transfer Inhibitors.</a> Bacchi, A., M. Carcelli, C. Compari, E. Fisicaro, N. Pala, G. Rispoli, D. Rogolino, T.W. Sanchez, M. Sechi, V. Sinisi, and N. Neamati. Journal of Medicinal Chemistry, 2011. 54(24): p. 8407-8420. PMID[22066494].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1024-110614.</p>


    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000342977300011">Design, Synthesis and Biological Evaluation of Substituted Guanidine Indole Derivatives as Potential Inhibitors of HIV-1 Tat-Tar Interaction.</a> Wang, J., Y. Wang, Z.Y. Li, P. Zhan, R.J. Bai, C. Pannecouque, J. Balzarini, C.E. De, and X.Y. Liu. Medicinal Chemistry, 2014. 10(7): p. 738-746. ISI[000342977300011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1024-110614.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000342760800021">Synthesis and SARs of Indole-based Alpha-amino Acids as Potent HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Han, X., H.M. Wu, W. Wang, C.N. Dong, P. Tien, S.W. Wu, and H.B. Zhou. Organic &amp; Biomolecular Chemistry, 2014. 12(41): p. 8308-8317. ISI[000342760800021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1024-110614.</p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
