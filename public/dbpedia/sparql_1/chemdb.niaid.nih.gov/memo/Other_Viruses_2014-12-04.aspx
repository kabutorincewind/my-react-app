

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-12-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XPDoHGtoIywj7qGCvIBWSJEBATQkZP6ijkn1Ztwy/VJ6JNr22swPYjuZYKS7Ex0ameori9kUly+gJfu2yvDGj6ZL6Z6zZUQQHWVTysR2n+YojKlb0HE6fB55CCk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="80875E11" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: November 21 - December 4, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25173609">miR-125b Inhibits Hepatitis B Virus Expression in Vitro through Targeting of the SCNN1A Gene.</a> Zhang, Z., J. Chen, Y. He, X. Zhan, R. Zhao, Y. Huang, H. Xu, Z. Zhu, and Q. Liu. Archives of Virology, 2014. 159(12): p. 3335-3343. PMID[25173609].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25139683">Binding Kinetics, Potency, and Selectivity of the Hepatitis C Virus NS3 Protease Inhibitors GS-9256 and Vedroprevir.</a> Barauskas, O., A.C. Corsa, R. Wang, S. Hluhanich, D. Jin, M. Hung, H. Yang, W.E.t. Delaney, and B.E. Schultz. Biochimica et Biophysica Acta, 2014. 1840(12): p. 3292-3298. PMID[25139683].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25234393">Production and Evaluation of Antimycotic and Antihepatitis C Virus Potential of Fusant MERV6270 Derived from Mangrove Endophytic Fungi Using Novel Substrates of Agroindustrial Wastes.</a> El-Gendy, M.M., A.M. El-Bondkly, and S.M. Yahya. Applied Biochemistry and Biotechnology, 2014. 174(8): p. 2674-2701. PMID[25234393].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25246395">Cross-genotypic Examination of Hepatitis C Virus Polymerase Inhibitors Reveals a Novel Mechanism of Action for Thumb Binders.</a> Eltahla, A.A., E. Tay, M.W. Douglas, and P.A. White. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7215-7224. PMID[25246395].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25450683">Anti-interferon-alpha Neutralizing Antibody Induced Telaprevir Resistance under the Interferon-alpha Plus Telaprevir Treatment in Vitro.</a> Kuga, C., H. Enomoto, N. Aizawa, T. Takashima, N. Ikeda, A. Ishii, Y. Sakai, Y. Iwata, H. Tanaka, M. Saito, H. Iijma, and S. Nishiguchi. Biochemical and Biophysical Research Communications, 2014. 454(3): p. 453-458. PMID[25450683].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25267677">Potency and Resistance Analysis of Hepatitis C Virus NS5B Polymerase Inhibitor BMS-791325 on All Major Genotypes.</a> Liu, M., M. Tuttle, M. Gao, and J.A. Lemm. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7416-7423. PMID[25267677].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25224012">NS5A Inhibitors Impair NS5A-Phosphatidylinositol 4-Kinase IIIalpha Complex Formation and Cause a Decrease of Phosphatidylinositol 4-Phosphate and Cholesterol Levels in Hepatitis C Virus-associated Membranes.</a> Reghellin, V., L. Donnici, S. Fenu, V. Berno, V. Calabrese, M. Pagani, S. Abrignani, F. Peri, R. De Francesco, and P. Neddermann. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7128-7140. PMID[25224012].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br />    

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25301950">Mechanism of Inhibition for BMS-791325, a Novel Non-nucleoside Inhibitor of Hepatitis C Virus NS5B Polymerase.</a> Rigat, K.L., H. Lu, Y.K. Wang, A. Argyrou, C. Fanslau, B. Beno, Y. Wang, J. Marcinkeviciene, M. Ding, R.G. Gentles, M. Gao, L.M. Abell, and S.B. Roberts. The Journal of Biological Chemistry, 2014. 289(48): p. 33456-33468. PMID[25301950].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25347030">Ombitasvir (ABT-267), a Novel NS5A Inhibitor for the Treatment of Hepatitis C.</a> Stirnimann, G. Expert Opinion on Pharmacotherapy, 2014. 15(17): p. 2609-2622. PMID[25347030].</p>

    <p class="plaintext">[PubMed]. OV_1121-120414.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25175944">Sofosbuvir, a Novel Nucleotide Analogue Inhibitor Used for the Treatment of Hepatitis C Virus.</a> Summers, B.B., J.W. Beavers, and O.M. Klibanov. Journal Pharmacology and Pharmacotherapeutics, 2014. 66(12): p. 1653-1666. PMID[25175944].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25267682">Spectrum of Activity and Mechanisms of Resistance of Various Nucleoside Derivatives against Gammaherpesviruses.</a> Coen, N., S. Duraffour, D. Topalis, R. Snoeck, and G. Andrei. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7312-7323. PMID[25267682].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25267681">Inhibitors of Nucleotidyltransferase Superfamily Enzymes Suppress Herpes Simplex Virus Replication.</a> Tavis, J.E., H. Wang, A.E. Tollefson, B. Ying, M. Korom, X. Cheng, F. Cao, K.L. Davis, W.S. Wold, and L.A. Morrison. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7451-7461. PMID[25267681].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25139839">Prophylactic Treatment with a Novel Bioadhesive Gel Formulation Containing Aciclovir and Tenofovir Protects from HSV-2 Infection.</a> Shankar, G.N. and C. Alt. The Journal of Antimicrobial Chemotherapy, 2014. 69(12): p. 3282-3293. PMID[25139839].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1121-120414.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344204600015">Synthesis and anti-HCV Activity of 1-(1&#39;,3&#39;-O-Anhydro-3&#39;-C-methyl-beta-D-psicofuranosyl)uracil.</a> Komsta, Z., B. Mayes, A. Moussa, M. Shelbourne, A. Stewart, A.J. Tyrrell, L.L. Wallis, A.C. Weymouth-Wilson, and A. Yurek-George. Tetrahedron Letters, 2014. 55(45): p. 6216-6219. ISI[000344204600015].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1121-120414.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
