

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-383.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="53dwxf1wFXA7kr5njun3NSg544CCiV/uu6SSM8dRv3ED2TFRN1x9U7zrfNsSsvvyQTkneWQRcTKj2IHAzGMALMRfObB9G65oiDFQbmsyT/dv/xqQsLhRoqkjYgk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="319F4DEA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-383-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61665   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Programs to facilitate tuberculosis drug discovery: the Tuberculosis Antimicrobial Acquisition and Coordinating Facility</p>

    <p>          Goldman, RC, Laughon, BE, Reynolds, RC, Secrist, JA III, Maddry, JA, Guie, M-A, Poffenberger, AC, Kwong, CA, and Ananthan, S</p>

    <p>          Infect. Disord.: Drug Targets <b>2007</b>.  7(2): 92-104</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61666   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Anti-mycobacterial formulation</p>

    <p>          Parrish, Nicole M, Owens, Albert H Jr, and Dick, James D</p>

    <p>          PATENT:  WO <b>2007092590</b>  ISSUE DATE:  20070816</p>

    <p>          APPLICATION: 2007  PP: 38pp.</p>

    <p>          ASSIGNEE:  (Fasgen, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61667   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Novel agents in the management of Mycobacterium tuberculosis disease</p>

    <p>          Barry, PJ and O&#39;Connor, TM</p>

    <p>          Curr. Med. Chem. <b>2007</b>.  14(18): 2000-2008</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61668   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Resazurin reduction assays for screening of anti-tubercular compounds against dormant and actively growing Mycobacterium tuberculosis, Mycobacterium bovis BCG and Mycobacterium smegmatis</p>

    <p>          Taneja, Neetu Kumra and Tyagi, Jaya Sivaswami</p>

    <p>          J. Antimicrob. Chemother. <b>2007</b>.  60(2): 288-293</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61669   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Recombinant Mycobacterium tuberculosis antigen multi-epitope and applications for treatment of tuberculosis</p>

    <p>          Deng, Anmei, Zhong, Renqian, Lou, Jiatao, Wu, Chuanyong, Chen, Sunxiao, Zhou, Ye, Chen, Yan, Chen, Bo, Jiang, Tianshu, and Gu, Mingli</p>

    <p>          PATENT:  CN <b>1900111</b>  ISSUE DATE: 20070124</p>

    <p>          APPLICATION: 1002-9287  PP: 32pp.</p>

    <p>          ASSIGNEE:  (Second Military Medical University, The Chinese People&#39;s Liberation Army Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61670   OI-LS-383; WOS-OI-9/7/2007</p>

    <p class="memofmt1-2">          Antimicrobial Activity of the Methanolic Extract and of the Chemical Constituents Isolated From Newbouldia Laevis</p>

    <p>          Kuete, V. <i>et al.</i></p>

    <p>          Pharmazie <b>2007</b>.  62(7): 552-556</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248064800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248064800016</a> </p><br />
    <br clear="all">

    <p>7.     61671   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluations of new benzofuran 1,3,5-trisubstituted pyrazoline derivatives of paracetamol as potential antitubercular, antimicrobial agents</p>

    <p>          Agarwal, YK, Manna, K, Bhatt, H, Gogai, P, Babu, VH, and Srinivasan, KK</p>

    <p>          Indian J. Heterocycl. Chem. <b>2007</b>.  16(3): 263-266</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     61672   OI-LS-383; WOS-OI-9/7/2007</p>

    <p class="memofmt1-2">          Immunotherapeutics for Tuberculosis in Experimental Animals: Is There a Common Pathway Activated by Effective Protocols?</p>

    <p>          Rook, G., Lowrie, D., and Hernandez-Pando, R.</p>

    <p>          Journal of Infectious Diseases <b>2007</b>.  196(2): 191-198</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247803100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247803100005</a> </p><br />

    <p>9.     61673   OI-LS-383; WOS-OI-9/7/2007</p>

    <p class="memofmt1-2">          Genornics and the Evolution, Pathogenesis, and Diagnosis of Tuberculosis</p>

    <p>          Ernst, J., Trevejo-Nunez, G., and Banaiee, N.</p>

    <p>          Journal of Clinical Investigation <b>2007</b>.  117(7): 1738-1745</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247837700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247837700002</a> </p><br />

    <p>10.   61674   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          In vitro antimicrobial and cytotoxic activities of ferrocene derivative compounds</p>

    <p>          Al-Bari, MAbdul Alim, Hossen, MFaruk, Khan, Alam, Islam, MRobiul, Kudrat-E-Zahan, M, Mosaddik, MAshik, Zakaria, Choudhury M, and Islam, MAnwar Ul</p>

    <p>          Pak. J. Biol. Sci. <b>2007</b>.  10(15): 2423-2429</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61675   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          New sphingolipids with a previously unreported 9-methyl-C20-sphingosine moiety from a marine algous endophytic fungus Aspergillus niger EN-13</p>

    <p>          Zhang, Yi, Wang, Song, Li, Xiao-Ming, Cui, Chuan-Ming, Feng, Chao, and Wang, Bin-Gui</p>

    <p>          Lipids <b>2007</b>.  42 (8): 759-764</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61676   OI-LS-383; WOS-OI-9/7/2007</p>

    <p class="memofmt1-2">          Antimycobacterial Activity of Novel 1-(5-Cyclobutyl-1,3-Oxazol-2-Yl)-3-(Sub)Phenyl/Pyridylthiourea Compounds Endowed With High Activity Toward Multidrug-Resistant Mycobacterium Tuberculosis</p>

    <p>          Sriram, D. <i>et al.</i></p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2007</b>.  59(6): 1194-1196</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247727300024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247727300024</a> </p><br />

    <p>13.   61677   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of some azole derivatives</p>

    <p>          Bakht, Mohammad Afroz, Mojahidul-Islam, and Siddiqui, Anees A</p>

    <p>          Orient. J. Chem. <b>2007</b>.  23(1): 283-286</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   61678   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Fungicidal-fungistatic action of some 2-aril-thiazol compounds</p>

    <p>          Palage, Mariana, Parvu, M, Oniga, Smaranda, and Muresan, Ana</p>

    <p>          Farmacia (Bucharest, Rom.) <b>2007</b>.  55(2): 203-206</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   61679   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Detection of in vitro anti-fungal activity of 20 tetralin compounds</p>

    <p>          Liang, Rong-mei, Cao, Yong-bing, Zhou, You-jun, Wang, Tao, Lu, Jia, Yao, Bin, and Jiang, Yuan-ying</p>

    <p>          Dier Junyi Daxue Xuebao <b>2007</b>.  28(3): 286-293</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   61680   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Synthesis of O-b-D-Glucopyranosides of 7-Hydroxy-3-(imidazol-2-yl)-4H-chromen-4-ones</p>

    <p>          Ingle, VN, Hatzade, KM, Taile, VS, Gaidhane, PK, and Kharche, ST</p>

    <p>          J. Carbohydr. Chem. <b>2007</b>.  26(2): 107-123</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   61681   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          In vitro activity of the synthetic lipopeptide PAL-Lys-Lys-NH2 alone and in combination with antifungal agents against clinical isolates of Cryptococcus neoformans</p>

    <p>          Barchiesi, Francesco, Giacometti, Andrea, Cirioni, Oscar, Arzeni, Daniela, Silvestri, Carmela, Kamysz, Wojciech, Abbruzzetti, Alessandra, Riva, Alessandra, Kamysz, Elzbieta, and Scalise, Giorgio</p>

    <p>          Peptides (N. Y., NY, U. S.) <b>2007</b>.  28(8): 1509-1513</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   61682   OI-LS-383; WOS-OI-9/7/2007</p>

    <p class="memofmt1-2">          Efficient Regulation of Viral Replication by Sirna in a Non-Human Primate Surrogate Model for Hepatitis C</p>

    <p>          Yokota, T. <i>et al.</i></p>

    <p>          Biochemical and Biophysical Research Communications <b>2007</b>.  361(2): 294-300</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248738400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248738400006</a> </p><br />

    <p>19.   61683   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          The priorities for antiviral drug resistance surveillance and research</p>

    <p>          Pillay, Deenan </p>

    <p>          J. Antimicrob. Chemother. <b>2007</b>.  60(Suppl. 1): i57-i58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   61684   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Heterocyclic antiviral compounds</p>

    <p>          Fell, Jay Bradford, Mohr, Peter, and Stengel, Peter J</p>

    <p>          PATENT:  WO <b>2007093541</b>  ISSUE DATE:  20070823</p>

    <p>          APPLICATION: 2007  PP: 53pp.</p>

    <p>          ASSIGNEE:  (F. Hoffmann-La Roche A.-G., Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   61685   OI-LS-383; SCIFINDER-OI-9/10/2007</p>

    <p class="memofmt1-2">          Preparation of alkynyl nucleosides via Sonogashira coupling and palladium/copper-catalyzed alkynylation reaction as antiviral agents</p>

    <p>          Fairlamb, Ian James Stewart, Baumann, Christoph George, and Firth, Andrew Graeme</p>

    <p>          PATENT:  WO <b>2007091056</b>  ISSUE DATE:  20070816</p>

    <p>          APPLICATION: 2007  PP: 71pp.</p>

    <p>          ASSIGNEE:  (The University of York, UK and Replizyme Limited)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>22.   61686   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Antibacterial Activity of Ergosterol Peroxide Against Mycobacterium Tuberculosis: Dependence Upon System and Medium Employed</p>

    <p>          Duarte, N. <i>et al.</i></p>

    <p>          Phytotherapy Research <b>2007</b>.  21(7): 601-604</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247984800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247984800001</a> </p><br />

    <p>23.   61687   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Antibacterial Activity of Two Canthin-6-One Alkaloids From Allium Neapolitanum</p>

    <p>          O&#39;donnell, G. and Gibbons, S.</p>

    <p>          Phytotherapy Research <b>2007</b>.  21(7): 653-657</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247984800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247984800011</a> </p><br />

    <p>24.   61688   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Large Compound Databases for Structure-Activity Relationships Studies in Drug Discovery</p>

    <p>          Scior, T. <i>et al.</i></p>

    <p>          Mini-Reviews in Medicinal Chemistry <b>2007</b>.  7(8): 851-860</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248742800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248742800009</a> </p><br />

    <p>25.   61689   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Synthesis and Microbial Evaluation of Novel N(1)-Arilidene-N(2)-T(3)-Methyl-R(2), C(6)-Diaryl-Piperidin-4-One Azine Derivatives</p>

    <p>          Jayabharathi, J. <i>et al.</i></p>

    <p>          Medicinal Chemistry Research <b>2007</b>.  15(7-8): 431-442</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248743400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248743400004</a> </p><br />

    <p>26.   61690   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Differential Requirements of Ns4a for Internal Ns3 Cleavage and Polyprotein Processing of Hepatitis C Virus</p>

    <p>          Kou, Y. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(15): 7999-8008</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248027400018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248027400018</a> </p><br />

    <p>27.   61691   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Synthesis of the Docosanasaccharide Arabinan Domain of Mycobacterial Arabinogalactan and a Proposed Octadecasaccharide Biosynthetic Precursor</p>

    <p>          Joe, M. <i>et al.</i></p>

    <p>          Journal of the American Chemical Society <b>2007</b>.  129(32): 9885-9901</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248708500020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248708500020</a> </p><br />

    <p>28.   61692   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          New Tuberculosis Therapeutics: a Growing Pipeline</p>

    <p>          Spigelman, M.</p>

    <p>          Journal of Infectious Diseases <b>2007</b>.  196: S28-S34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248045700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248045700005</a> </p><br />
    <br clear="all">

    <p>29.   61693   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Targeting Enzyme Inhibitors in Drug Discovery</p>

    <p>          Copeland, R., Harpel, M., and Tummino, P.</p>

    <p>          Expert Opinion on Therapeutic Targets <b>2007</b>.  11(7): 967-978</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247881600010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247881600010</a> </p><br />

    <p>30.   61694   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Structure-Based Approaches to Drug Discovery Against Tuberculosis</p>

    <p>          Holton, S. <i>et al.</i></p>

    <p>          Current Protein &amp; Peptide Science <b>2007</b>.  8(4): 365-375</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248747100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248747100005</a> </p><br />

    <p>31.   61695   OI-LS-383; WOS-OI-9/14/2007</p>

    <p class="memofmt1-2">          Pharmacophore-Based Screening for the Successful Identification of Bio-Active Natural Products</p>

    <p>          Langer, T. <i>et al.</i></p>

    <p>          Chimia <b>2007</b>.  61 (6): 350-354</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247911200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247911200008</a> </p><br />

    <p>32.   61696   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          Small Molecule Scaffolds for CYP51 Inhibitors Identified by High Throughput Screening and Defined by X-Ray Crystallography</p>

    <p>          Podust, LM, von, Kries JP, Eddine, AN, Kim, Y, Yermalitskaya, LV, Kuehne, R, Ouellet, H, Warrier, T, Altekoster, M, Lee, JS, Rademann, J, Oschkinat, H, Kaufmann, SH, and Waterman, MR</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17846131&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17846131&amp;dopt=abstract</a> </p><br />

    <p>33.   61697   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          Targeted drug delivery to enhance efficacy and shorten treatment duration in disseminated Mycobacterium avium infection in mice</p>

    <p>          de Steenwinkel, JE, van Vianen, W, Ten Kate, MT, Verbrugh, HA, van Agtmael, MA, Schiffelers, RM, and Bakker-Woudenberg, IA</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17846106&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17846106&amp;dopt=abstract</a> </p><br />

    <p>34.   61698   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          Antimalarial and Antituberculous Poly-O-acylated Jatrophane Diterpenoids from Pedilanthus tithymaloides</p>

    <p>          Mongkolvisut, W and Sutthivaiyakit, S</p>

    <p>          J Nat Prod <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17844996&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17844996&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>35.   61699   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          A novel mechanism for substrate inhibition in mycobacterium tuberculosis D-3-phosphoglycerate dehydrogenase</p>

    <p>          Burton, RL, Chen, S, Xu, XL, and Grant, GA</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17761677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17761677&amp;dopt=abstract</a> </p><br />

    <p>36.   61700   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          Evaluation of antiprotozoal and antimycobacterial activities of the resin glycosides and the other metabolites of Scrophularia cryptophila</p>

    <p>          Tasdemir, D, Brun, R, Franzblau, SG, Sezgin, Y, and Calis, I</p>

    <p>          Phytomedicine <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17761408&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17761408&amp;dopt=abstract</a> </p><br />

    <p>37.   61701   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          Activity against drug resistant-tuberculosis strains of plants used in Mexican traditional medicine to treat tuberculosis and other respiratory diseases</p>

    <p>          Camacho-Corona, MD, Ramirez-Cabrera, MA, Santiago, OG, Garza-Gonzalez, E, Palacios, ID, and Luna-Herrera, J</p>

    <p>          Phytother Res <b>2007</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17726732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17726732&amp;dopt=abstract</a> </p><br />

    <p>38.   61702   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          Fatty Acid Synthesis Is Essential for Survival of Cryptococcus neoformans and a Potential Fungicidal Target</p>

    <p>          Chayakulkeeree, M, Rude, TH, Toffaletti, DL, and Perfect, JR</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17698629&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17698629&amp;dopt=abstract</a> </p><br />

    <p>39.   61703   OI-LS-383; PUBMED-OI-9/17/2007</p>

    <p class="memofmt1-2">          PEG-interferon alpha-2b for acute hepatitis C: a review</p>

    <p>          Palumbo, E</p>

    <p>          Mini Rev Med Chem <b>2007</b>.  7(8): 839-843</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17692045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17692045&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
