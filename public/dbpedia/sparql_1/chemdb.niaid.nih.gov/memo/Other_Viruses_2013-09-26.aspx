

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-09-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JP12nD8QGqCwkiRz3rKZse40c618a+gj8QZAM5VYBykGRgK72owglDFHUNEYU9OyqI+egfR2Q/ckPcmpdrm8T3eZ8jCurvaGEV0rfhwN+iTp7MbfbOae4nT6srE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ED770E2F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: September 13 - September 26, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2402750">An Efficient Antiviral Strategy for Targeting Hepatitis B Virus Genome Using Transcription Activator-like Effector Nucleases<i>.</i></a> Chen, J., W. Zhang, J. Lin, F. Wang, M. Wu, C. Chen, Y. Zheng, X. Peng, J. Li, and Z. Yuan. Molecular Therapy, 2013. <b>[Epub ahead of print]</b>; PMID[24025750].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0913-092613.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24055834">Antiviral Activity of Methyl Helicterate Isolated from Helicteres angustifolia (Sterculiaceae) against Hepatitis B Virus<i>.</i></a> Huang, Q., R. Huang, L. Wei, Y. Chen, S. Lv, C. Lian, X. Zhang, F. Yin, H. Li, L. Zhuo, and X. Lin. Antiviral Research, 2013. <b>[Epub ahead of print]</b>; PMID[24055834].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0913-092613.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24051027">Isolation and Identification of an anti-Hepatitis B Virus Compound from Hydrocotyle sibthorpioides Lam<i>.</i></a> Huang, Q., S. Zhang, R. Huang, L. Wei, Y. Chen, S. Lv, C. Liang, S. Tan, S. Liang, L. Zhuo, and X. Lin. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>; PMID[24051027].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0913-092613.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24051024">Chemical Composition and Antinociceptive, Anti-inflammatory and Antiviral Activities of Gallesia gorazema (Phytolaccaceae), a Potential Candidate for Novel Anti-herpetic Phytomedicines<i>.</i></a> de Jesus Silva Junior, A., F. de Campos-Buzzi, M.T. Romanos, T.M. Wagner, A.F. de Paula Costa Guimaraes, V.C. Filho, and R. Batista. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>; PMID[24051024].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0913-092613.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323856500019">Chemical Constituents of Swertia yunnanensis and Their anti-Hepatitis B Virus Activity<i>.</i></a> Cao, T.W., C.A. Geng, F.Q. Jiang, Y.B. Ma, K. He, N.J. Zhou, X.M. Zhang, J. Zhou, and J.J. Chen. Fitoterapia, 2013. 89: p. 175-182; ISI[000323856500019].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0913-092613.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323013700018">Three-dimensional Structure and Interaction Studies of Hepatitis C Virus p7 in 1,2-Dihexanoyl-sn-glycero-3-phosphocholine by Solution Nuclear Magnetic Resonance<i>.</i></a> Cook, G.A., L.A. Dawson, Y. Tian, and S.J. Opella. Biochemistry, 2013. 52(31): p. 5295-5303; ISI[000323013700018].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0913-092613.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323628500011">Regioselective Synthesis, Characterization and Antimicrobial Evaluation of S-Glycosides and S,N-Diglycosides of 1,2-Dihydro-5-(1H-indol-2-yl)-1,2,4-triazole-3-thione<i>.</i></a> El Ashry, E.H., E.H. El Tamany, M.E. Abd El Fattah, A.T.A. Boraei, and H.M. Abd El-Nabi. European Journal of Medicinal Chemistry, 2013. 66: p. 106-113; ISI[000323628500011].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0913-092613.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323858100014">Modification of Selected anti-HCMV Drugs with Lipophilic Boron Cluster Modulator<i>.</i></a> Olejniczak, A.B., A.M. Adamska, E. Paradowska, M. Studzinska, P. Suski, and Z.J. Lesnikowski. Acta Poloniae Pharmaceutica, 2013. 70(3): p. 489-504; ISI[000323858100014].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0913-092613.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
