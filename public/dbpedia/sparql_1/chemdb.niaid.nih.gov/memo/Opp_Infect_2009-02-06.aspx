

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-02-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mZleM1PTbtSPj/JwI+DL/cAnMsQQznBz0T8YFzhdMeADjLpd890qU2NuFAfPKtAPHtiuZc7Z7WGk/4FL3mjAaCqsl2Y7Re9sRmceZ/JPQYJt7/2f03bV1FzG1LI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9DF15F57" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">January 21, 2009 - February 3, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p> 

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19168759?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Towards New Antifolates Targeting Eukaryotic Opportunistic Infections.</a></p>

    <p class="NoSpacing">Liu J, Bolstad DB, Bolstad ES, Wright DL, Anderson AC.</p>

    <p class="NoSpacing">Eukaryot Cell. 2009 Jan 23. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19168759 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19038979?ordinalpos=53&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Curcumin as a promising antifungal of clinical interest.</a></p>

    <p class="NoSpacing">Martins CV, da Silva DL, Neres AT, Magalhães TF, Watanabe GA, Modolo LV, Sabino AA, de Fátima A, de Resende MA.</p>

    <p class="NoSpacing">J Antimicrob Chemother. 2009 Feb;63(2):337-9. Epub 2008 Nov 26.</p>

    <p class="NoSpacing">PMID: 19038979 [PubMed - in process]</p>  

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19171796?ordinalpos=25&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In Vitro Activities of Retigeric acid B Alone and in Combination with Azole Antifungal Agents against Candida albicans.</a></p>

    <p class="NoSpacing">Sun L, Sun S, Cheng A, Wu X, Zhang Y, Lou H.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Jan 26. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19171796 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19112025?ordinalpos=47&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Isolation, structure and biological activity of phomafungin, a cyclic lipodepsipeptide from a widespread tropical Phoma sp.</a></p>

    <p class="NoSpacing">Herath K, Harris G, Jayasuriya H, Zink D, Smith S, Vicente F, Bills G, Collado J, González A, Jiang B, Kahn JN, Galuska S, Giacobbe R, Abruzzo G, Hickey E, Liberator P, Xu D, Roemer T, Singh SB.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Feb 1;17(3):1361-9. Epub 2008 Dec 14.</p>

    <p class="NoSpacing">PMID: 19112025 [PubMed - in process]</p>

    <p class="memofmt2-2">TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</p>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19169353?ordinalpos=104&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Mycobacterium tuberculosis Rv3802c encodes a phospholipase/thioesterase and is inhibited by the antimycobacterial agent tetrahydrolipstatin.</a></p>

    <p class="NoSpacing">Parker SK, Barkley RM, Rino JG, Vasil ML.</p>

    <p class="NoSpacing">PLoS ONE. 2009;4(1):e4281. Epub 2009 Jan 26.</p>

    <p class="NoSpacing">PMID: 19169353 [PubMed - in process]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19005871?ordinalpos=236&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Efavirenz Mannich bases: synthesis, anti-HIV and antitubercular activities.</a></p>

    <p class="NoSpacing">Sriram D, Banerjee D, Yogeeswari P.</p>

    <p class="NoSpacing">J Enzyme Inhib Med Chem. 2009 Feb;24(1):1-5.</p>

    <p class="NoSpacing">PMID: 19005871 [PubMed - in process]</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18945598?ordinalpos=248&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vitro activity of linezolid against multidrug-resistant tuberculosis (MDR-TB) and extensively drug-resistant (XDR)-TB isolates.</a></p>

    <p class="NoSpacing">Prammananan T, Chaiprasert A, Leechawengwongs M.</p>

    <p class="NoSpacing">Int J Antimicrob Agents. 2009 Feb;33(2):190-1. Epub 2008 Oct 21. No abstract available.</p>

    <p class="NoSpacing">PMID: 18945598 [PubMed - in process]</p> 

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18629678?ordinalpos=275&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and antitubercular activity of a series of hydrazone and nitrovinyl analogs derived from heterocyclic aldehydes.</a></p>

    <p class="NoSpacing">Sonar VN, Crooks PA.</p>

    <p class="NoSpacing">J Enzyme Inhib Med Chem. 2009 Feb;24(1):117-24.</p>

    <p class="NoSpacing">PMID: 18629678 [PubMed - in process]</p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="NoSpacing">*also present in the ISI Web of Knowledge OI Alert provided for this period</p>  

    <p class="NoSpacing">&nbsp;</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
