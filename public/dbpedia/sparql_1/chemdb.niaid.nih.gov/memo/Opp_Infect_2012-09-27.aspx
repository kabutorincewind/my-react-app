

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-09-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Y3BkI4n8At4NowVL06J2OQbQ2tNlbQQna2ARHb+VV5JC77kduMm1bAMjqAL0sXCb6i2FOHUUfuGcLnnE685PeqEJIMHbhfWuuxzXC2zv/Hr0VyYVXcqNE9lmdmM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="33D945A0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: September 14 - September 27, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22724584">Inhibiting GPI Anchor Biosynthesis in Fungi Stresses the Endoplasmic Reticulum and Enhances Immunogenicity.</a> McLellan, C.A., L. Whitesell, O.D. King, A.K. Lancaster, R. Mazitschek, and S. Lindquist. ACS Chemical Biology, 2012. 7(9): p. 1520-1528. PMID[22724584].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22995097">Effect of Alkylphospholipids on Candida albicans Biofilm Formation and Maturation.</a> Vila, T.V., K. Ishida, W. de Souza, K. Prousis, T. Calogeropoulou, and S. Rozental. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[22995097].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22997218">Efficacy and Safety of Meropenem/Clavunate Added to Linezolid Containing Regimens in the Treatment of M/XDR-Tb.</a> De Lorenzo, S., J.W. Alffenaar, G. Sotgiu, R. Centis, L. D&#39;Ambrosio, S. Tiberi, M.S. Bolhuis, R. van Altena, P. Viggiani, A. Piana, A. Spanevello, and G.B. Migliori. The European Respiratory Journal, 2012. <b>[Epub ahead of print]</b>. PMID[22997218].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22977884">Design, Synthesis and Evaluation of Small Molecule Reactive Oxygen Species Generators as Selective Mycobacterium tuberculosis Inhibitors.</a> Dharmaraja, A.T., M. Alvala, D. Sriram, P. Yogeeswari, and H. Chakrapani. Chemical Communications, 2012. 48(83): p. 10325-10327. PMID[22977884].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23002243">A Common Mechanism of Inhibition of the Mycobacterium tuberculosis Mycolic acid Biosynthetic Pathway by Isoxyl and Thiacetazone.</a> Grzegorzewicz, A.E., J. Kordulakova, V. Jones, S.E. Born, J.M. Belardinelli, A. Vaquie, V.A. Gundi, J. Madacki, N. Slama, F. Laval, J. Vaubourgeix, R.M. Crew, B. Gicquel, M. Daffe, H.R. Morbidoni, P.J. Brennan, A. Quemard, M.R. McNeil, and M. Jackson. The Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23002234].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22987724">Towards a New Tuberculosis Drug: Pyridomycin - Nature&#39;s Isoniazid.</a> Hartkoorn, R.C., C. Sala, J. Neres, F. Pojer, S. Magnet, R. Mukherjee, S. Uplekar, S. Boy-Rottger, K.H. Altmann, and S.T. Cole. EMBO Molecular Medicine, 2012. <b>[Epub ahead of print]</b>. PMID[22987724].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22750060">OSI-930 Analogues as Novel Reversal Agents for ABCG2-Mediated Multidrug Resistance.</a> Kuang, Y.H., J.P. Patel, K. Sodani, C.P. Wu, L.Q. Liao, A. Patel, A.K. Tiwari, C.L. Dai, X. Chen, L.W. Fu, S.V. Ambudkar, V.L. Korlipara, and Z.S. Chen. Biochemical Pharmacology, 2012. 84(6): p. 766-774. PMID[22750060].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23000473">Mycobacterium tuberculosis Surface Protein Rv0227c Contains High Activity Binding Peptides Which Inhibit Cell Invasion.</a> Rodriguez, D.M., M. Ocampo, H. Curtidor, M. Vanegas, M.E. Patarroyo, and M.A. Patarroyo. Peptides, 2012. <b>[Epub ahead of print]</b>. PMID[23000473].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22744719">The Pharmacokinetics and Pharmacodynamics of Pulmonary Mycobacterium avium Complex Disease Treatment.</a> van Ingen, J., E.F. Egelund, A. Levin, S.E. Totten, M.J. Boeree, J.W. Mouton, R.E. Aarnoutse, L.B. Heifets, C.A. Peloquin, and C.L. Daley. American Journal of Respiratory and Critical Care Medicine, 2012. 186(6): p. 559-565. PMID[22744719].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22995710">Arginine-rich Self-assembling Peptides as Potent Antibacterial Gels.</a> Veiga, A.S., C. Sinthuvanich, D. Gaspar, H.G. Franquelim, M.A. Castanho, and J.P. Schneider. Biomaterials, 2012. <b>[Epub ahead of print]</b>. PMID[22995710].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22884110">Synthesis, Antimycobacterial and Antibacterial Activity of Ciprofloxacin Derivatives Containing a N-Substituted Benzyl Moiety.</a> Wang, S., X.D. Jia, M.L. Liu, Y. Lu, and H.Y. Guo. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(18): p. 5971-5975. PMID[22884110].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22910226">Synthesis and Antimalarial Activity of New Haemanthamine-type Derivatives.</a> Cedron, J.C., D. Gutierrez, N. Flores, A.G. Ravelo, and A. Estevez-Braun. Bioorganic &amp; Medicinal Chemistry, 2012. 20(18): p. 5464-5472. PMID[22910226].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p>
    <p> </p> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22986092">First-time Comparison of the in Vitro Antimalarial Activity of Artemisia Annua Herbal Tea and Artemisinin.</a> De Donno, A., T. Grassi, A. Idolo, M. Guido, P. Papadia, A. Caccioppola, L. Villanova, A. Merendino, F. Bagordo, and F.P. Fanizzi. Transactions of the Royal Society of Tropical Medicine and Hygiene, 2012. <b>[Epub ahead of print]</b>. PMID[22986092].</p>

    <p class="plaintext"> [<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22990456">Antitrypanosomal Alkaloids from the Marine Bacterium Bacillus pumilus.</a> Martinez-Luis, S., J.F. Gomez, C. Spadafora, H.M. Guzman, and M. Gutierrez. Molecules, 2012. 17(9): p. 11146-11155. PMID[22990456].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22000555">Plasmodium falciparum: The Potential of the Cancer Chemotherapeutic Agent Cisplatin and Its Analogues as Anti-malarials.</a> Murray, V., H.M. Campbell, and A.M. Gero. Experimental Parasitology, 2012. <b>[Epub ahead of print]</b>. PMID[23000555].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p>      
    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22978674">Synthesis and Biological Evaluation of Epidithio-, Epitetrathio- and bis-(Methylthio)diketopiperazines. Synthetic Methodology, Enanti-oselective Total Synthesis of Epicoccin G, 8,8&#39;-Epi-Ent-Rostratin B, Gliotoxin, Gliotoxin G, Emethallicin E and Haematocin, and Discovery of New Antiviral and Antimalarial Agents.</a> Nicolaou, K.C., M. Lu, S. Totokotsopoulos, P.M. Heretsch, D. Giguere, Y.P. Sun, D. Sarlah, T.H. Nguyen, I.C. Wolf, D.F. Smee, C.W. Day, S. Bopp, and E.A. Winzeler. Journal of the American Chemical Society, 2012. <b>[Epub ahead of print]</b>. PMID[22978674].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22884991">Antiplasmodial Activities of 4-Aminoquinoline-statine Compounds.</a> Vaiana, N., M. Marzahn, S. Parapini, P. Liu, M. Dell&#39;agli, A. Pancotti, E. Sangiovanni, N. Basilico, E. Bosisio, B.M. Dunn, D. Taramelli, and S. Romeo. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(18): p. 5915-5918. PMID[22884991].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0914-092712.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307756100001">Synthesis and Antibacterial, Antimycobacterial Activity of 7- 4-{5-(2-Oxo-2-P-substituted-phenylethylthio)-1,3,4-thiadiazol-2yl}-3 &#39;-methylpiperazinyl quinolone Derivatives.</a> Agrawal, K.M. and G.S. Talele. Journal of Chemistry, 2013. <b>[Epub ahead of print]</b>. ISI[000307756100001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307828600010">Synthesis and Evaluation of Small Libraries of Triazolylmethoxy Chalcones, Flavanones and 2-Aminopyrimidines as Inhibitors of Mycobacterial FAS-II and PknG.</a> Anand, N., P. Singh, A. Sharma, S. Tiwari, V. Singh, D.K. Singh, K.K. Srivastava, B.N. Singh, and R.P. Tripathi. Bioorganic &amp; Medicinal Chemistry, 2012. 20(17): p. 5150-5163. ISI[000307828600010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307435700005">MurD Enzymes from Different Bacteria: Evaluation of Inhibitors.</a> Barreteau, H., I. Sosic, S. Turk, J. Humljan, T. Tomasic, N. Zidar, M. Herve, A. Boniface, L. Peterlin-Masic, D. Kikelj, D. Mengin-Lecreulx, S. Gobec, and D. Blanot. Biochemical Pharmacology, 2012. 84(5): p. 625-632. ISI[000307435700005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307768200001">Antibacterial Activity of Daucus crinitus Essential Oils Along the Vegetative Life of the Plant.</a> Bendiabdellah, A., M.E. Dib, N. Meliani, A. Muselli, D. Nassim, B. Tabti, and J. Costa. Journal of Chemistry, 2013. Article ID 149502, pp. 7. ISI[000307768200001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306687700128">Retigeric acid B Attenuates the Virulence of Candida albicans via Inhibiting Adenylyl cyclase Activity Targeted by Enhanced Farnesol Production.</a> Chang, W.Q., Y. Li, L. Zhang, A.X. Cheng, and H.X. Lou. Plos One, 2012. 7(7): e41624. ISI[000306687700128].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306848600008">A Marine-derived Streptomyces Sp MS449 Produces High Yield of Actinomycin X-2 and Actinomycin D with Potent Anti-tuberculosis Activity.</a> Chen, C.X., F.H. Song, Q. Wang, W.M. Abdel-Mageed, H. Guo, C.Z. Fu, W.Y. Hou, H.Q. Dai, X.T. Liu, N. Yang, F. Xie, K. Yu, R.X. Chen, and L.X. Zhang. Applied Microbiology and Biotechnology, 2012. 95(4): p. 919-927. ISI[000306848600008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307509800008">Recombinant Expression, Purification, and Antimicrobial Activity of a Novel Hybrid Antimicrobial Peptide LFT33.</a> Feng, X.J., C.L. Liu, J.Y. Guo, X.Y. Song, J. Li, W.S. Xu, and Z.Q. Li. Applied Microbiology and Biotechnology, 2012. 95(5): p. 1191-1198. ISI[000307509800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307828600025">Indole Alkaloids from Two Cultured Cyanobacteria, Westiellopsis Sp and Fischerella muscicola.</a> Kim, H., D. Lantvit, C.H. Hwang, D.J. Kroll, S.M. Swanson, S.G. Franzblau, and J. Orjala. Bioorganic &amp; Medicinal Chemistry, 2012. 20(17): p. 5290-5295. ISI[000307828600025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307598200003">Efficacy of Essential Oil Combination of Curcuma longa L. And Zingiber officinale Rosc. As a Postharvest Fungitoxicant, Aflatoxin Inhibitor and Antioxidant Agent.</a> Prakash, B., P. Singh, A. Kedia, A. Singh, and N.K. Dubey. Journal of Food Safety, 2012. 32(3): p. 279-288. ISI[000307598200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307598200009">Chemical Composition, Radical Scavenging, Antibacterial and Antifungal Activities of Zataria multiflora Bioss Essential Oil and Aqueous Extract.</a> Purfard, A.M. and G. Kavoosi. Journal of Food Safety, 2012. 32(3): p. 326-332. ISI[000307598200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307168300005">6-Gingerol Inhibits Fungal Alpha Amylase: Enzyme Kinetic and Molecular Modeling Studies.</a> Tintu, I., K.V. Dileep, C. Remya, A. Augustine, and C. Sadasivan. Starch, 2012. 64(8): p. 607-612. ISI[000307168300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307508200012">Chemical Composition and Antimicrobial Activities of Essential Oils (Eo) Extracted from Leaves of Lippia rugosa A. Chev against Foods Pathogenic and Adulterated Microorganisms.</a> Yehouenou, B., E. Ahoussi, P. Sessou, G.A. Alitonou, F. Toukourou, and D. Sohounhloue. African Journal of Microbiology Research, 2012. 6(26): p. 5496-5505. ISI[000307508200012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p> 
    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306962800021">Benzoylbenzimidazole-based Selective Inhibitors Targeting Cryptosporidium parvum and Toxoplasma gondii Calcium-dependent Protein Kinase-1.</a>  Zhang, Z.S., K.K. Ojo, S.M. Johnson, E.T. Larson, P.Q. He, J.A. Geiger, A. Castellanos-Gonzalez, A.C. White, M. Parsons, E.A. Merritt, D.J. Maly, C. Verlinde, W.C. Van Voorhis, and E.K. Fan. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(16): p. 5264-5267. ISI[000306962800021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0914-092712.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
