

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-01-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nv5hB4tgp5h0sFFn21PKlMTZS0O49Yp2P41o/BevJ7UfQAi2u5xv2DwVgP9LJj4X90ZfJNbTR4QzRIy74sd6YgegYjaLp+6QCHgLmpn7AXljS8pR8S9/018wySQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2737B1E8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">January 6 - January 19, 2012</h1>

    <p class="memofmt2-2">Herpes simplex virus 1</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22222893">Design of Xanthone propionate Photolabile Protecting Group Releasing Acyclovir for the Treatment of Ocular Herpes Simplex Virus.</a> Blake, J.A., B. Bareiss, L. Jimenez, M. Griffith, and J.C. Scaiano, Photochemical &amp; photobiological sciences: Official Journal of the European Photochemistry Association and the European Society for Photobiology, 2012. <b>[Epub ahead of print]</b>; PMID[22222893].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0106-011912.</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22247956">A Novel Class of Highly Potent Irreversible Hepatitis C Virus (HCV) NS5B Polymerase Inhibitors.</a> Chen, K.X., C.A. Lesburg, B. Vibulbhan, W. Yang, T.Y. Chan, S. Venkatraman, F. Velazquez, Q. Zeng, F. Bennett, G.N. Anilkumar, J. Duca, Y. Jiang, P. Pinto, L. Wang, Y. Huang, O. Selyutin, S. Gavalas, H. Pu, S. Agrawal, B. Feld, H.C. Huang, C. Li, K.C. Cheng, N.Y. Shih, J.A. Kozlowski, S.B. Rosenblum, and F.G. Njoroge, Journal of medicinal chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22247956].</p>

    <p class="plaintext"><b>[Pubmed]</b>.</p>

    <p class="plaintext">OV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22244938">Discovery of GS-9256: A Novel Phosphinic acid Derived Inhibitor of the Hepatitis C Virus NS3/4A Protease with Potent Clinical Activity.</a> Sheng, X.C., A. Casarez, R. Cai, M.O. Clarke, X. Chen, A. Cho, W.E.t. Delaney, E. Doerffler, M. Ji, M. Mertzman, R. Pakdaman, H.J. Pyun, T. Rowe, Q. Wu, J. Xu, and C.U. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22244938].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22239530">Suppression of Hepatitis C Virus by the Flavonoid Quercetin is Mediated by Inhibition of NS3 protease Activity.</a> Bachmetov, L., M. Gal-Tanamy, A. Shapira, M. Vorobeychik, T. Giterman-Galam, P. Sathiyamoorthy, A. Golan-Goldhirsh, I. Benhar, R. Tur-Kaspa, and R. Zemel, Journal of Viral Hepatitis, 2012. 19(2): p. e81-e88; PMID[22239530].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22220815">Synthesis of New 4,5-Dihydrofuranoindoles and Their Evaluation as HCV NS5B Polymerase Inhibitors.</a> Velazquez, F., S. Venkatraman, C.A. Lesburg, J. Duca, S.B. Rosenblum, J.A. Kozlowski, and F.G. Njoroge, Organic letters, 2012. <b>[Epub ahead of print]</b>; PMID[22220815].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22219365">Tiling Genomes of Pathogenic Viruses Identifies Potent Antiviral shRNAs and Reveals a Role for Secondary Structure in shRNA Efficacy.</a> Tan, X., Z.J. Lu, G. Gao, Q. Xu, L. Hu, C. Fellmann, M.Z. Li, H. Qu, S.W. Lowe, G.J. Hannon, and S.J. Elledge, Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>; PMID[22219365].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0106-011912.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297974900007">The Green Tea Polyphenol, Epigallocatechin-3-gallate, Inhibits Hepatitis C Virus Entry.</a> Ciesek, S., T. von Hahn, C.C. Colpitts, L.M. Schang, M. Friesland, J. Steinmann, M.P. Manns, M. Ott, H. Wedemeyer, P. Meuleman, T. Pietschmann, and E. Steinmann, Hepatology, 2011. 54(6): p. 1947-1955; ISI[000297974900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297950900020">Novel anti-HCV Therapy: Single shRNA Targeting both Strands of HCV.</a> Lisowski, L., M. Elazar, K. Chu, J.S. Glenn, and M.A. Kay, Journal of Gene Medicine, 2011. 13(7-8): p. 418-419; ISI[000297950900020].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297974900006">Effects of Hypolipidemic Agent Nordihydroguaiaretic acid on Lipid Droplets and Hepatitis C Virus.</a> Syed, G.H. and A. Siddiqui, Hepatology, 2011. 54(6): p. 1936-1946; ISI[000297974900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0106-011912.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
