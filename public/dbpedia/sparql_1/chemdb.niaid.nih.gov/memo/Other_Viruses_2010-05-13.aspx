

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-05-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MfzAyHAbDLc77EW5YhlS/Jxrzex2iqjO7ykGHduimegq9hr5RcmZJvmCWir+eCF3TNCAkLOBaPu7p3qAzLInSXS7ZGsreLZzFmEHfhY8IfG83f0WpU95GSm8ECA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EA8451F5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">April 30 - May 13, 2010</p>

    <p><strong> </strong></p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20456634">Estimation of inhibitory quotient using a comparative equilibrium dialysis assay for prediction of viral response to hepatitis C virus inhibitors.</a></p>

    <p class="NoSpacing">Mo H, Yang C, Wang K, Wang Y, Huang M, Murray B, Qi X, Sun SC, Deshpande M, Rhodes G, Miller MD.</p>

    <p class="NoSpacing">J Viral Hepat. 2010 Apr 27. [Epub ahead of print]PMID: 20456634 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20440850">Inhibition of hepatitis C virus replication by single-stranded RNA structural mimics.</a></p>

    <p class="NoSpacing">Smolic R, Smolic M, Andorfer JH, Wu CH, Smith RM, Wu GY.</p>

    <p class="NoSpacing">World J Gastroenterol. 2010 May 7;16(17):2100-8.PMID: 20440850 [PubMed - in process]</p>

    <p />

    <h2 class="memofmt2-2">Hepatitis B</h2> 

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20434445">Curcumin inhibits hepatitis B virus via down-regulation of the metabolic coactivator PGC-1alpha.</a></p>

    <p class="NoSpacing">Mouler Rechtman M, Har-Noy O, Bar-Yishay I, Fishman S, Adamovich Y, Shaul Y, Halpern Z, Shlomai A.</p>

    <p class="NoSpacing">FEBS Lett. 2010 Apr 29. [Epub ahead of print]PMID: 20434445 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20430009">Inhibitory effect on hepatitis B virus in vitro by a peroxisome proliferator-activated receptor-gamma ligand, rosiglitazone.</a></p>

    <p class="NoSpacing">Wakui Y, Inoue J, Ueno Y, Fukushima K, Kondo Y, Kakazu E, Obara N, Kimura O, Shimosegawa T.</p>

    <p class="NoSpacing">Biochem Biophys Res Commun. 2010 Apr 27. [Epub ahead of print]PMID: 20430009 [PubMed - as supplied by publisher]</p>

    <h2 class="memofmt2-2">Herpes Simplex Virus</h2> 

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20439611">Syndecan-Fc Hybrid Molecule as a Potent In Vitro Microbicidal Anti-HIV-1 Agent.</a></p>

    <p class="NoSpacing">Bobardt MD, Chatterji U, Schaffer L, de Witte L, Gallay PA.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2010 May 3. [Epub ahead of print]PMID: 20439611 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20438761">Immunomodulatory and Antiviral Activity of Almond Skins.</a></p>

    <p class="NoSpacing">Arena A, Bisignano C, Stassi G, Mandalari G, Wickham MS, Bisignano G.</p>

    <p class="NoSpacing">Immunol Lett. 2010 Apr 30. [Epub ahead of print]PMID: 20438761 [PubMed - as supplied by publisher]</p>

    <h2 class="memofmt2-2">Cytomegalovirus</h2> 

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20442781">Artemisinin-derived dimers have greatly improved anti-cytomegalovirus activity compared to artemisinin monomers.</a></p>

    <p class="NoSpacing">Arav-Boger R, He R, Chiou CJ, Liu J, Woodard L, Rosenthal A, Jones-Brando L, Forman M, Posner G.</p>

    <p class="NoSpacing">PLoS One. 2010 Apr 28;5(4):e10370.PMID: 20442781 [PubMed - in process]</p>

    <p />

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20430633">Bioactive norditerpenoids from the soft coral Sinularia gyrosa.</a></p>

    <p class="NoSpacing">Cheng SY, Chuang CT, Wen ZH, Wang SK, Chiou SF, Hsu CH, Dai CF, Duh CY.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2010 Apr 8. [Epub ahead of print]PMID: 20430633 [PubMed - as supplied by publisher]</p>  

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p>9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276804300031">In Vitro Resistance Profile of the Hepatitis C Virus NS3/4A Protease Inhibitor TMC435.</a></p>

    <p class="plaintext">Lenz, Oliver, et al.</p>

    <p class="plaintext">ANTIMICROBIAL AGENTS AND CHEMOTHERAPY. 54(5):2010. P. 1878-87.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276972200027">Swerilactones E-G, three unusual lactones from Swertia mileensis.</a></p>

    <p class="plaintext">Geng, Chang-An, et al.</p>

    <p class="plaintext">TETRAHEDRON LETTERS. 51(18):2010. P. 2483-5.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
