

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-03-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ox3NYqmXhZh7arXeIqiiswyNzKSGdTW6ncSinFTDJdVA5vl1qvtwZ0aRXIGVbUpVvTar+kPU5rnKgFsqDkDc2GgjsiXCkqW5mqi5makED80GPvNv2X9dd8aCpvc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CB7BC12C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 18, 2011- March 31, 2011</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21413917">Analysis of Cyclotides in Viola ignobilis by Nano Liquid Chromatography Fourier Transform Mass Spectrometry.</a> Hashempour, H., A. Ghassempour, N.L. Daly, B. Spengler, and A. Rompp. Protein &amp; Peptide Letters, 2011.  <b>[Epub ahead of print]</b>; PMID[21413917].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21445582">Roles for Biological Membranes in Regulating Human Immunodeficiency Virus Replication and Progress in the Development of HIV Therapeutics that Target Lipid Metabolism.</a> Haughey, N.J., Y.R.L.B. Tovar, and V.V. Bamdaru. Journal of Neuroimmune Pharmacology, 2011.  <b>[Epub ahead of print]</b>; PMID[21445582].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21436031">Broad-spectrum antiviral that interferes with de novo pyrimidine biosynthesis.</a> Hoffmann, H.H., A. Kunz, V.A. Simon, P. Palese, and M.L. Shaw. Proceedings of the National Academies of Science, 2011.  <b>[Epub ahead of print]</b>; PMID[21436031].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21443452">Inhibition of HIV-1 and M-MLV reverse transcriptases by a major polyphenol (3,4,5 tri-O-galloylquinic acid) present in the leaves of the South African resurrection plant, Myrothamnus flabellifolia.</a> Johns, B.A., HIV-1 Integrase Strand Transfer Inhibitors, in Annual Reports in Medicinal Chemistry, Vol 45. 2010. p. 263-+. 5. Kamng&#39;ona, A., J.P. Moore, G. Lindsey, and W. Brandt. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21443452].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21435237">Antiviral Propierties of 5,5&#39;-Dithiobis-2-Nitrobenzoic Acid and Bacitracin Against T-Tropic Human Immunodeficiency Virus Type 1.</a> Lara, H.H., L. Ixtepan-Turrent, E.N. Garza-Trevino, S.M. Flores-Trevino, G. Borkow, and C. Rodriguez-Padilla. Virology Journal, 2011. 8(1): p. 137; PMID[21435237].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21438533">Diarylpyrimidine-Dihydrobenzyloxopyrimidine Hybrids: New, Wide-Spectrum Anti-HIV-1 Agents Active at (Sub)-Nanomolar Level.</a> Rotili, D., D. Tarantino, M. Artico, M.B. Nawrozkij, E. Gonzalez-Ortega, B. Clotet, A. Samuele, J.A. Este, G. Maga, and A. Mai. Journal of Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21438533].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21430048">Caveolin 1 inhibits HIV replication by transcriptional repression mediated through NF-{kappa}B.</a> Wang, X.M., P.E. Nadeau, S. Lin, J.R. Abbott, and A. Mergia. Journal of Virology, 2011.  <b>[Epub ahead of print]</b>; PMID[21430048].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/15890415">KP-1212/1461, a nucleoside designed for the treatment of HIV by viral mutagenesis.</a> Harris, K.S., W. Brabant, S. Styrchak, A. Gall, and R. Daifuku.  Antiviral Research, 2005. 67(1): p. 1-9; PMID[15890415].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21264288">Mutation of HIV-1 Genomes in a Clinical Population Treated with the Mutagenic Nucleoside KP1461.</a> Mullins, J.I., L. Heath, J.P. Hughes, J. Kicha, S. Styrchak, K.G. Wong, U. Rao, A. Hansen, K.S. Harris, and J.P. Laurent. PLoS ONE, 2011. 6(1): p. 483-489; PMID[21264288].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0318-033011.</p>  

    <p class="memofmt2-2"> ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286516500013">Analysis of the Ex Vivo and In Vivo Antiretroviral Activity of Gemcitabine.</a> Clouser, C.L., C.M. Holtz, M. Mullett, D.L. Crankshaw, J.E. Briggs, J. Chauhan, I.M. VanHoutan, S.E. Patterson, and L.M. Mansky. PLoS ONE, 2011. 6(1); ISI[000286516500013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286516500030">The Antiviral Spectra of TRIM5 alpha Orthologues and Human TRIM Family Proteins against Lentiviral Production.</a> Ohmine, S., R. Sakuma, T. Sakuma, T. Thatava, H. Takeuchi, and Y. Ikeda. PLoS ONE, 2011. 6(1); ISI[000286516500030].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287295300017">HIV Protease-Mediated Activation of Sterically Capped Proteasome Inhibitors and Substrates.</a> Buckley, D.L., T.W. Corson, N. Aberle, and C.M. Crews. Journal of the American Chemical Society, 2011. 133(4): p. 698-700; ISI[000287295300017].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287740200004">X-ray and molecular modelling in fragment-based design of three small quinoline scaffolds for HIV integrase inhibitors.</a> Majerz-Maniecka, K., R. Musiol, A. Skorska-Stania, D. Tabak, P. Mazur, B.J. Oleksyn, and J. Polanski. Bioorganic &amp; Medicinal Chemistry, 2011. 19(5): p. 1606-1612; ISI[000287740200004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287792900016">Cycloartane Triterpenes from the Apical Buds of Gardenia obtusifolia.</a> Nuanyai, T., R. Sappapan, T. Vilaivan, and K. Pudhom, Gardenoins E-H. Chemical &amp; Pharmaceutical Bulletin, 2011. 59(3): p. 385-387; ISI[000287792900016].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287848200013">Synthesis and biological evaluation of novel n- 3-(4-phenylpip-erazin-1-yl)-propyl -carboxamide derivatives.</a> Yin, H.J., Y.X. Li, Y.T. Zheng, X.Y. Ye, C.Y. Li, Z.H. Zou, and Y. Zhao. Anti-HIV-1 activities of photodynamic therapy using hematoporphyrin monomethyl ether . 16. Weng, Z.Y., Y.P. Gao, J.K. Zhang, X.W. Dong, and T. Liu. Journal of Chemical Research, 2011(1): p. 43-46; ISI[000287848200013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287981400007">Therapeutic Targets for Inhibitors of Glycosylation.</a> Alonzi, D.S. and T.D. Butters. Chimia, 2011. 65(1-2): p. 35-39; ISI[000287981400007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287997000001">Dithiothreitol Causes HIV-1 Integrase Dimer Dissociation While Agents Interacting With the Integrase Dimer Interface Promote Dimer Formation.</a> Tsiang, M., G.S. Jones, M. Hung, D. Samuel, N. Novikov, S. Mukund, K.M. Brendza, A. Niedziela-Majka, D.B. Jin, X.H. Liu, M. Mitchell, R. Sakowicz, and R. Geleziunas. Biochemistry, 2011. 50(10): p. 1567-1581; ISI[000287997000001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288037600005">From Conventional to Unusual Enzyme Inhibitor Scaffolds: The Quest for Target Specificity.</a> Meggers, E. Angewandte Chemie-International Edition, 2011. 50(11): p. 2442-2448; ISI[000288037600005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288093200001">Chemical Constituents of Plants from the Genus Geum.</a> Serbin, A., E. Karaseva, V. Tsvetkov, O. Alikhanova, and I. Rodionov. Hybrid Polymeric Systems for Nano-Selective Counter Intervention in Virus Life Cycle . 21. Cheng, X.R., H.Z. Jin, J.J. Qin, J.J. Fu, and W.D. Zhang. Chemistry &amp; Biodiversity, 2011. 8(2): p. 203-222; ISI[000288093200001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288148800077">Henrischinins A-C: Three New Triterpenoids from Schisandra henryi.</a> Xue, Y.B., J.H. Yang, X.N. Li, X. Du, J.X. Pu, W.L. Xiao, J. Su, W. Zhao, Y. Li, and H.D. Sun. Organic Letters, 2011. 13(6): p. 1564-1567; ISI[000288148800077].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288184700003">Synthetic Methods and Biological Activities of Clitocine and Its Analogues.</a> Guo, X.H., H. Kang, L.X. Zheng, and S.D. Jiang. Chinese Journal of Organic Chemistry, 2011. 31(2): p. 176-186; ISI[000288184700003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288297100008">Biological evaluation of imidazolium- and ammonium-based salts as HIV-1 integrase inhibitors.</a> Maddali, K., V. Kumar, C. Marchand, Y. Pommier, and S.V. Malhotra. Medchemcomm, 2011. 2(2): p. 143-150; ISI[000288297100008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288437700009">Synthesis and Properties of 3 &#39;-Azido-3 &#39;-Deoxythymidine Derivatives of Glycerolipids.</a> Lonshakov, D.V., E.O. Baranova, A.I. Lyutik, N.S. Shastina, and V.I. Shvets. Pharmaceutical Chemistry Journal, 2011. 44(10): p. 557-563; ISI[000288437700009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300116">Strong inhibition of wild-type and integrase inhibitor (INI)-resistant HIV integrase (IN) strand transfer reaction by the novel INI S/GSK1349572.</a> Yoshinaga, T., M. Kanamori-Koyama, T. Seki, A. Sato, and T. Fujiwara. Antiviral Therapy, 2010. 15(S2):  p.A141; ISI[000287431300116].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300010">Differential resistance in HIV-1 protease: the role of residue 50 in pathways for amprenavir/darunavir and atazanavir resistance.</a> Bandaranayake, R.M., S. Mittal, N.M. King, M. Prabu-Jeyabalan, M.N.L. Nalam, and C.A. Schiffer.  Antiviral Therapy, 2010.  15(S2):  p.A14; ISI[000287431300010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300011">Substrate envelope based design of potent HIV-1 protease inhibitors resilient to resistance.</a> Nalam, M.N.L., A. Ali, G. Reddy, S.G. Anjum, T.M. Rana, and C.A. Schiffer. Antiviral Therapy, 2010.  15(S2):  pA15; ISI[000287431300011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300048">Resistance induction to the HIV-1 CD4 binding site inhibitor M48-U1.</a> Grupping, K., L. Heyndrickx, K. Arien, G. Martin, L. Martin, and G. Vanham. Antiviral Therapy, 2010.  15(S2):  p.A55;  ISI[000287431300048].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300050">New insights into the mechanism of action of a new HIV-1 maturation inhibitor extracted from Brazilian brown algae Dictyota pfaffii.</a> Abreu, C.M., A.L. Valadao, R. Delvechio, L.J. da Costa, D.C. Bou-Habib, C.C. Cirne-Santos, V.L. Teixeira, I. Frugulhetti, W. Ferreira, A. Tanuri, and R.M. Brindeiro. Antiviral Therapy, 2010.  15(S2): p.A57;  ISI[000287431300050].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300059">Antiviral activity profile and characterization of mutations associated with resistance to a novel NRTI (OBP-601, Festinavir (R)) and their effect on HIV-1 replicative fitness.</a> Vazquez, A.C., J. Weber, M. Baba, D. Winner, J.D. Rose, A.M. Rhea, D. Wylie, Y. Urata, and M.E. Quinones-Mateu.  Antiviral Therapy, 2010.  15(S2): p.A66;  ISI[000287431300059].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300073">Full-length HIV-1 gag determines protease inhibitor susceptibility within in vitro assays.</a> Gupta, R.K., A. Kohli, A. McCormick, G. Towers, D. Pillay, and C.M. Parry.  Antiviral Therapy, 2010.  15(S2): pA69; ISI[000287431300073].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300074">The amino acid substitution A376S in the HIV-1 reverse transcriptase connection subdomain confers low-level phenotypic resistance to nevirapine.</a> Puertas, M.C., M. Kisic, A. Cozzi-Lepri, W. Bannister, R. Bellido, C. Pou, B. Clotet, R. Paredes, J. Lundgren, L. Menendez-Arias, J. Martinez-Picado, and S.S.G. Euro.  Antiviral Therapy, 2010.  15(S2): p.A70;   ISI[000287431300074].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287431300116">In vitro HIV-1 cross-resistance against reverse transcriptase inhibitor-based microbicides</a>. Selhorst, P., A.C. Vazquez, K. Terrazas-Aranda, J. Michiels, L. Heyndrickx, K. Arien, M.E. Quinones-Mateu, and G. Vanham. Antiviral Therapy, 2010.  15(S2): p.A112; ISI[000287431300116].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0318-033011.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Patent  citations</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110224&amp;CC=WO&amp;NR=2011020913A2&amp;KC=A2">Preparation of cyclodepsipeptide antiviral compounds for use in the treatment of HIV infections, AIDS and ARC.</a> Alcami Pertejo, J., L.M. Bedoya del Olmo, and M.d.C. Cuevas Marchante. Patent. 2011. 2010-EP62174</p>

    <p class="plaintext">2011020913.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0318-033111.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110224&amp;CC=US&amp;NR=2011046982A1&amp;KC=A1">Methods and compositions of triazolyl oligosaccharides related to viral inhibition.</a> Arya, D.P., N. Ranjan, and S. Kumar. Patent. 2011. 2010-857425 20110046982.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0318-033111.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110217&amp;CC=WO&amp;NR=2011018793A1&amp;KC=A1">A novel standardized plant composition comprising procyanidins for treatment of RNA virus infection including AIDS.</a> Bhaskaran, S. and M. Vishwaraman. Patent. 2011. 2009-IN519 2011018793.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0318-033111.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110210&amp;CC=WO&amp;NR=2011015641A1&amp;KC=A1">Pyrimidine derivatives as novel viral replication inhibitors and their preparation and use in the treatment of HIV infection.</a> Chaltin, P., Z. Debyser, M. De Maeyer, A. Marchand, D. Marchand, W. Smets, A. Voet, and F. Christ. Patent. 2011. 2010-EP61453 2011015641.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0318-033111.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110303&amp;CC=WO&amp;NR=2011024175A1&amp;KC=A1">Preparation of peptidyl macrocycles and their compositions for preventing or treating HIV infection.</a> Gilon, C., A. Hoffman, M. Kotler, M. Hurevich, S. Joubran, and A. Swed. Patent. 2011. 2010-IL708 2011024175.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0318-033111.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110224&amp;CC=WO&amp;NR=2011020822A1&amp;KC=A1">Substituted (thiazolylcarbonyl)imidazolidinones as antiretroviral agents and their preparation and use in the treatment of retroviral diseases.</a> Thede, K., S. Greschat, K.M. Gericke, S. Wildum, and D. Paulsen. Patent. 2011. 2010-EP61923 2011020822.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0318-033111.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110303&amp;CC=WO&amp;NR=2011025683A1&amp;KC=A1">HIV integrase inhibitors.</a> Wai, J.S., D.-S. Su, and C.M. Wiscount. Patent. 2011. 2010-US45589 2011025683.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0318-033111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
