

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-12-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Nie0s6uAd2T2GG+lxn9FJ0hu3dpUBz1TOrMqEbit1bEa6H2wO5C/tklGMJDpNSz+pbprGcak2AXWBZG5nhWIbDEgXkJXPhY5+/zXGyRpzMyARlPms38VZektOkA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FF2DA408" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: November 23 - December 6, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23037306">Antitumor, Antibiotic and Antileishmanial Properties of the Pyranonaphthoquinone Psychorubrin from Mitracarpus frigidus.</a> Fabri, R.L., R.M. Grazul, L.O. Carvalho, E.S. Coimbra, G.M. Cardoso, E.M. Souza-Fagundes, A.D. Silva, and E. Scio. Anais da Academia Brasileira de Ciencias, 2012. 84(4): p. 1081-1090. PMID[23037306].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23181593">Antibacterial, Antifungal and Antioxidant Activities of the Ethanol Extract of the Stem Bark of Clausena heptaphylla.</a> Fakruddin, M., K.S. Mannan, R.M. Mazumdar, and H. Afroz. BMC Complementary and Alternative Medicine, 2012. 12(1): p. 232. PMID[23181593].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23192753">Effect of Corilagin on Membrane Permeability of Escherichia coli, Staphylococcus aureus and Candida albicans.</a> Li, N., M. Luo, Y.J. Fu, Y.G. Zu, W. Wang, L. Zhang, L.P. Yao, C.J. Zhao, and Y. Sun. Phytotherapy Research, 2012. <b>[Epub ahead of print]</b>. PMID[23192753].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23163331">Biochemical-, Biophysical-, and Microarray-based Antifungal Evaluation of the Buffer-mediated Synthesized Nano Zinc Oxide: An in Vivo and in Vitro Toxicity Study.</a> Patra, P., S. Mitra, N. Debnath, and A. Goswami. Langmuir, 2012. <b>[Epub ahead of print]</b>. PMID[23163331].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22296180">The Effect of Various Concentrations of Iodine potassium iodide on the Antimicrobial Properties of Mineral Trioxide Aggregate - A Pilot Study.</a> Saatchi, M., H.S. Hosseini, A.R. Farhad, and T. Narimany. Dental Traumatology, 2012. 28(6): p. 474-477. PMID[22296180].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23193085">Chemical and Biological Characterization of Novel Essential Oils from Eremophila bignoniiflora (F. Muell) (myoporaceae): A Traditional Aboriginal Australian Bush Medicine.</a> Sadgrove, N.J., M. Hitchcock, K. Watson, and G.L. Jones. Phytotherapy Research, 2012. <b>[Epub ahead of print]</b>. PMID[23193085].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22982389">Metal Based New Triazoles: Their Synthesis, Characterization and Antibacterial/Antifungal Activities.</a> Sumrra, S.H. and Z.H. Chohan. Spectrochimica acta. Part A, Molecular and Biomolecular Spectroscopy, 2012. 98: p. 53-61. PMID[22982389].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p>

    <h2> ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22991970">Antimycobacterial Drugs Currently in Phase II Clinical Trials and Preclinical Phase for Tuberculosis Treatment.</a> Engohang-Ndong, J. Expert Opinion on Investigational Drugs, 2012. 21(12): p. 1789-1800. PMID[22991970].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23176193">Antibacterial Activities of the Extracts, Fractions and Compounds from Dioscorea bulbifera.</a> Kuete, V., R. Betrandteponno, A.T. Mbaveng, L.A. Tapondjou, J.J. Meyer, L. Barboni, and N. Lall. BMC Complementary and Alternative Medicine, 2012. 12(1): p. 228. PMID[23176193].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23006761">Screening of Antifungal Azole Drugs and Agrochemicals with an Adapted alamaBlue-based Assay Demonstrates Antibacterial Activity of Croconazole against Mycobacterium ulcerans.</a> Scherr, N., K. Roltgen, M. Witschel, and G. Pluschke. Antimicrobial Agents and Chemotherapy, 2012. 56(12): p. 6410-6413. PMID[23006761].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23169689">Discovery of Schaeffer&#39;s Acid Analogues as Lead Structures of Mycobacterium tuberculosis Type II Dehydroquinase Using a Rational Drug Design Approach.</a> Schmidt, M.F., O. Korb, N.I. Howard, M.V. Dias, T.L. Blundell, and C. Abell. ChemMedChem, 2012. <b>[Epub ahead of print]</b>. PMID[23169689].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23006755">Noninvasive Determination of 2-[18F]-Fluoroisonicotinic acid hydrazide Pharmacokinetics by Positron Emission Tomography in Mycobacterium tuberculosis-infected Mice.</a> Weinstein, E.A., L. Liu, A.A. Ordonez, H. Wang, J.M. Hooker, P.J. Tonge, and S.K. Jain. Antimicrobial Agents and Chemotherapy, 2012. 56(12): p. 6284-6290. PMID[23006755].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23042656">Recent Advances in the Research of Heterocyclic Compounds as Antitubercular Agents.</a> Yan, M. and S. Ma. ChemMedChem, 2012. 7(12): p. 2063-2075. PMID[23042656].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23208888">Antimalaria Action of Antiretroviral Drugs on Plasmodium berghei in Mice.</a> Akinyede, A., A. Akintonwa, O. Awodele, S. Olayemi, I. Oreagba, O. Aina, S. Akindele, and C. Okany. The American Journal of Tropical Medicine and Hygiene, 2012. <b>[Epub ahead of print]</b>. PMID[23208888].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23197075">Watsonianone a-C, Anti-plasmodial beta-Triketones from the Australian Tree, Corymbia watsoniana.</a> Carroll, A.R., V.M. Avery, S. Duffy, P.I. Forster, and G.P. Guymer. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23197075].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23207699">In Vitro Antimalarial Activity of Six Aspidosperma Species from the State of Minas Gerais (Brazil).</a> Dolabela, M.F., S.G. Oliveira, J.M. Peres, J.M. Nascimento, M.M. Povoa, and A.B. Oliveira. Anais da Academia Brasileira de Ciencias, 2012. 84(4): p. 899-910. PMID[23207699].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23167812">8,8-Dialkyldihydroberberines with Potent Antiprotozoal Activity.</a> Endeshaw, M., X. Zhu, S. He, T. Pandharkar, E. Cason, K.V. Mahasenan, H. Agarwal, C. Li, M. Munde, W.D. Wilson, M. Bahar, R.W. Doskotch, A.D. Kinghorn, M. Kaiser, R. Brun, M.E. Drew, and K.A. Werbovetz. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>. PMID[23167812].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23145816">Mimicking the Intramolecular Hydrogen Bond: Synthesis, Biological Evaluation, and Molecular Modeling of Benzoxazines and Quinazolines as Potential Antimalarial Agents.</a> Gemma, S., C. Camodeca, M. Brindisi, S. Brogi, G. Kukreja, S. Kunjir, E. Gabellieri, L. Lucantoni, A. Habluetzel, D. Taramelli, N. Basilico, R. Gualdani, F. Tadini-Buoninsegni, G. Bartolommei, M.R. Moncelli, R.E. Martin, R.L. Summers, S. Lamponi, L. Savini, I. Fiorini, M. Valoti, E. Novellino, G. Campiani, and S. Butini. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23145816].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23208707">Chemobiosynthesis of New Antimalarial Macrolides.</a> Goodman, C.D., M. Useglio, S. Peiru, G.R. Labadie, G.I. McFadden, E. Rodriguez, and H. Gramajo. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23208707].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23171238">Compounds from Sorindeia juglandifolia (Anacardiaceae) Exhibit Potent Anti-plasmodial Activities in Vitro and in Vivo.</a> Kamkumo, R.G., A.M. Ngoutane, L.R. Tchokouaha, P.V. Fokou, E.A. Madiesse, J. Legac, J.J. Kezetas, B.N. Lenta, F.F. Boyom, T. Dimo, W.F. Mbacham, J. Gut, and P.J. Rosenthal. Malaria Journal, 2012. 11(1): p. 382. PMID[23171238].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23169890">Antimalarial Activity of Isoquine against Kenyan Plasmodium falciparum Clinical Isolates and Association with Polymorphisms in pfcrt and pfmdr1 Genes.</a> Okombo, J., S.M. Kiara, A. Abdirahman, L. Mwai, E. Ohuma, S. Borrmann, A. Nzila, and S. Ward. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23169890].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23207061">Antimalarial and Safety Evaluation of Extracts from Toddalia asiatica (L) lam.</a> (Rutaceae). Orwa, J.A., L. Ngeny, N.M. Mwikwabe, J. Ondicho, and I.J. Jondiko. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[23207061].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23084276">Linear and Cyclic Dipeptides with Antimalarial Activity.</a> Perez-Picaso, L., H.F. Olivo, R. Argotte-Ramos, M. Rodriguez-Gutierrez, and M.Y. Rios. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7048-7051. PMID[23084276].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23170970">Discovery of Novel and Ligand-efficient Inhibitors of Plasmodium falciparum and Plasmodium vivax N-myristoyltransferase.</a> Rackham, M.D., J.A. Brannigan, D.K. Moss, Z. Yu, A.J. Wilkinson, A.A. Holder, E.W. Tate, and R.J. Leatherbarrow. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23170970].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23163291">Bile acid-based 1,2,4-Trioxanes: Synthesis and Antimalarial Assessment (1).</a> Singh, C., M. Hassam, V.P. Verma, A.S. Singh, N.K. Naikade, S.K. Puri, P.R. Maulik, and R. Kant. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23163291].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23178659">Dehydroepiandrosterone Effect on Plasmod48. ium falciparum and Its Interaction with Antimalarial Drugs.</a> Zuluaga, L., S. Parra, E. Garrido, R. Lopez-Munoz, J.D. Maya, and S. Blair. Experimental Parasitology, 2012. <b>[Epub ahead of print]</b>. PMID[23178659].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20718073">Synthesis and Antiplasmodial Activity of Highly Active Reverse Analogues of the Antimalarial Drug Candidate Fosmidomycin.</a> Behrendt, C.T., A. Kunfermann, V. Illarionova, A. Matheeussen, T. Grawert, M. Groll, F. Rohdich, A. Bacher, W. Eisenreich, M. Fischer, L. Maes, and T. Kurz. ChemMedChem, 2010. 5(10): p. 1673-1676. PMID[20718073].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21866890">Reverse Fosmidomycin Derivatives against the Antimalarial Drug Target Ispc (Dxr).</a> Behrendt, C.T., A. Kunfermann, V. Illarionova, A. Matheeussen, M.K. Pein, T. Grawert, J. Kaiser, A. Bacher, W. Eisenreich, B. Illarionov, M. Fischer, L. Maes, M. Groll, and T. Kurz. Journal of Medicinal Chemistry, 2011. 54(19): p. 6796-6802. PMID[21866890].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19296600">Novel Peptidomimetics Containing a Vinyl Ester Moiety as Highly Potent and Selective Falcipain-2 Inhibitors.</a> Ettari, R., N. Micale, T. Schirmeister, C. Gelhaus, M. Leippe, E. Nizi, M.E. Di Francesco, S. Grasso, and M. Zappala. Journal of Medicinal Chemistry, 2009. 52(7): p. 2157-2160. PMID[19296600].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19296651">Identification of a Metabolically Stable Triazolopyrimidine-based Dihydroorotate Dehydrogenase Inhibitor with Antimalarial Activity in Mice.</a> Gujjar, R., A. Marwaha, F. El Mazouni, J. White, K.L. White, S. Creason, D.M. Shackleford, J. Baldwin, W.N. Charman, F.S. Buckner, S. Charman, P.K. Rathod, and M.A. Phillips. Journal of Medicinal Chemistry, 2009. 52(7): p. 1864-1872. PMID[19296651].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18522386">Triazolopyrimidine-based Dihydroorotate Dehydrogenase Inhibitors with Potent and Selective Activity against the Malaria Parasite Plasmodium falciparum.</a> Phillips, M.A., R. Gujjar, N.A. Malmquist, J. White, F. El Mazouni, J. Baldwin, and P.K. Rathod. Journal of Medicinal Chemistry, 2008. 51(12): p. 3649-3653. PMID[18522386].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23000485">New Naphthoquinones and an Alkaloid with in Vitro Activity against Toxoplasma gondii RH and EGS Strains.</a> Ferreira, R.A., A.B. de Oliveira, S.A. Gualberto, J.M. Miguel Del Corral, R.T. Fujiwara, P.H. Gazzinelli Guimaraes, and R.W. de Almeida Vitor. Experimental Parasitology, 2012. 132(4): p. 450-457. PMID[23000485].
    <br />
    <b>[PubMed]</b>. OI_1123-120612.</p> 

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309494100047">Laurene-type Sesquiterpenes from the Red Sea Red Alga Laurencia obtusa as Potential Antitumor-Antimicrobial Agents.</a> Alarif, W.M., S.S. Al-Lihaibi, S.E.N. Ayyad, M.H. Abdel-Rhman, and F.A. Badria. European Journal of Medicinal Chemistry, 2012. 55: p. 462-466. ISI[000309494100047].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310532400008">Antimycobacterial Agents: Synthesis and Biological Evaluation of Novel 4-(Substituted-phenyl)-6-methyl-2-oxo-N-(pyridin-2-yl)-1,2,3,4-tetrahydr opyrimidine-5-carboxamide derivatives by Using One-Pot Multicomponent Method.</a> Almansour, A.I., M.A. Ali, S. Ali, A.C. Wei, Y.K. Yoon, R. Ismail, T.S. Choon, S. Pandian, R.S. Kumar, N. Arumugam, and H. Osman. Letters in Drug Design &amp; Discovery, 2012. 9(10): p. 953-957. ISI[000310532400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310281100010">Anti-Candida albicans Action of the Glyco-protein Complex Purified from Metabolites of Gut Bacterium Raoultella ornithinolytica Isolated from Earthworms Dendrobaena veneta.</a> Fiolka, M.J., K. Grzywnowicz, K. Chlebiej, E. Szczuka, E. Mendyk, R. Keller, and J. Rzymowska. Journal of Applied Microbiology, 2012. 113(5): p. 1106-1119. ISI[000310281100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310353700024">Synthesis and Antituberculosis Activity of Novel Unfolded and Macrocyclic Derivatives of Ent-Kaurane steviol.</a> Khaybullin, R.N., I.Y. Strobykina, A.B. Dobrynin, A.T. Gubaydullin, R.V. Chestnova, V.M. Babaev, and V.E. Kataev. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(22): p. 6909-6913. ISI[000310353700024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310353700012">Design and Synthesis of 1H-1,2,3-Triazoles Derived from Econazole as Antitubercular Agents.</a> Kim, S., S.N. Cho, T. Ohc, and P. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(22): p. 6844-6847. ISI[000310353700012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310538300007">Synthesis and Antimicrobial Activity of Some Novel Terephthaloyl thiourea Cross-linked Carboxymethyl chitosan Hydrogels.</a>  Mohamed, N.A. and N.A. Abd El-Ghany. Cellulose, 2012. 19(6): p. 1879-1891. ISI[000310538300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310030200047">Synthesis, Characterization and Biological Evaluation of Some Pyridine and Quinoline Fused Chromenone Derivatives.</a> Patel, M.A., V.G. Bhila, N.H. Patel, A.K. Patel, and D.I. Brahmbhatt. Medicinal Chemistry Research, 2012. 21(12): p. 4381-4388. ISI[000310030200047].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310030200027">Discovery of 2-(4-Cyano-3-trifluoromethylphenyl amino)-4-(4-quinazolinyloxy)-6-piperazinyl(piperidinyl)-S-triazines as Potential Antibacterial Agents.</a> Patel, R.V., P. Kumari, D.P. Rajani, and K.H. Chikhalia. Medicinal Chemistry Research, 2012. 21(12): p. 4177-4192. ISI[000310030200027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310030200010">In Vitro Antibacterial, Antifungal, and DNA Cleavage Studies of Coumarin Schiff Bases and Their Metal Complexes: Synthesis and Spectral Characterization.</a> Patil, S.A., S.N. Unki, and P.S. Badami. Medicinal Chemistry Research, 2012. 21(12): p. 4017-4027. ISI[000310030200010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310413600003">Microwave-assisted Synthesis of Novel 4H-Chromene Derivatives Bearing Phenoxypyrazole and Their Antimicrobial Activity Assessment.</a> Sangani, C.B., N.M. Shah, M.P. Patel, and R.G. Patel. Journal of the Serbian Chemical Society, 2012. 77(9): p. 1165-1174. ISI[000310413600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310030200041">Anti-microbial Benzimidazole Derivatives: Synthesis and in Vitro Biological Evaluation.</a> Soni, L.K., T. Narsinghani, and A. Sethi. Medicinal Chemistry Research, 2012. 21(12): p. 4330-4334. ISI[000310030200041].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309973900174">MmPPOX Inhibits Mycobacterium tuberculosis Lipolytic Enzymes Belonging to the Hormone-sensitive Lipase Family and Alters Mycobacterial Growth.</a> Delorme, V., S.V. Diomande, L. Dedieu, J.F. Cavalier, F. Carriere, L. Kremer, J. Leclaire, F. Fotiadu, and S. Canaan. Plos One, 2012. 7(9). ISI[000309973900174].
    <br />
    <b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307508800025">Antioxidant, Antimicrobial and Cytotoxicity Studies of Russelia equisetiformis.</a> Riaz, M., N. Rasool, I.H. Bukhari, M. Shahid, A.F. Zahoor, M.A. Gilani, and M. Zubair. African Journal of Microbiology Research, 2012. 6(27): p. 5700-5707. ISI[000307508800025].
    <br />
    <b>[WOS]</b>. OI_1123-120612.</p> 
    <p> </p>
    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309719800003">Endophytic Fungi Occurring in Ipomoea carnea Tissues and Their Antimicrobial Potentials.</a> Tayung, K., M. Sarkar, and P. Baruah. Brazilian Archives of Biology and Technology, 2012. 55(5): p. 653-660. ISI[000309719800003].
    <br />
    <b>[WOS]</b>. OI_1123-120612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
