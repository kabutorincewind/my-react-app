

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-06-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9WSwGPfKxrfxJQ367dTqtfh4v8PXZOJy8oPITySm4KgwbGKsmGiIxSoEuUV8wzghkCZ2A7xQckDpvvWMyuzSnw/RL5J2cyL2BTmkzWEXf8WxpAfnNNWnpsmCTeM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BDE0AC12" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  June 10 - June 23, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19544345?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Induced-Fit Docking Approach Provides Insight into the Binding Mode and Mechanism of Action of HIV-1 Integrase Inhibitors.</a>Barreca ML, Iraci N, De Luca L, Chimirri A. ChemMedChem. 2009 Jun 18. [Epub ahead of print] PMID: 19544345</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19538939?ordinalpos=16&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Enhanced HIV-1 neutralization by a CD4-VH3-IgG1 fusion protein.</a> Meyuhas R, Noy H, Fishman S, Margalit A, Montefiori DC, Gross G. Biochem Biophys Res Commun. 2009 Jun 16. [Epub ahead of print] PMID: 19538939</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19534674?ordinalpos=27&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of HIV-1 Ribonuclease H Activity by Novel Frangula-emodine Derivatives.</a> Kharlamova T, Esposito F, Zinzula L, Floris G, Cheng YC, Dutschman GE, Tramontano E. Med Chem. 2009 Sep 1. [Epub ahead of print] PMID: 19534674</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19534463?ordinalpos=33&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Heterobiaryl Human Immunodeficiency Virus Entry Inhibitors.</a> Lu RJ, Tucker JA, Pickens J, Ma YA, Zinevitch T, Kirichenko O, Konoplev V, Kuznetsova S, Sviridov S, Brahmachary E, Khasanov A, Mikel C, Yang Y, Liu C, Wang J, Freel S, Fisher S, Sullivan A, Zhou J, Stanfield-Oakley S, Baker B, Sailstad J, Greenberg M, Bolognesi D, Bray B, Koszalka B, Jeffs P, Jeffries C, Chucholowski A, Sexton C. J Med Chem. 2009 Jun 17. [Epub ahead of print] PMID: 19534463</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19533724?ordinalpos=34&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">North- and South-Bicyclo[3.1.0]Hexene Nucleosides: The Effect of Ring Planarity on Anti-HIV Activity.</a>Russ PL, Gonzalez-Moa MJ, Vu BC, Sigano DM, Kelley JA, Lai CC, Deschamps JR, Hughes SH, Marquez VE. ChemMedChem. 2009 Jun 16. [Epub ahead of print] PMID: 19533724</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19529000?ordinalpos=36&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibitory effect of glycyrrhizin on the neutrophil-dependent increase of R5 HIV replication in cultures of macrophages.</a>Yoshida T, Kobayashi M, Li XD, Pollard RB, Suzuki F. Immunol Cell Biol. 2009 Jun 16. [Epub ahead of print] PMID: 19529000</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19526990?ordinalpos=46&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Betulinic Acid Derivatives as Human Immunodeficiency Virus Type 2 (HIV-2) Inhibitors ( parallel).</a> Dang Z, Lai W, Qian K, Ho P, Lee KH, Chen CH, Huang L. J Med Chem. 2009 Jun 15. [Epub ahead of print] PMID: 19526990</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19524486?ordinalpos=49&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Effect of (r)-9-[4-hydroxy-2-(hydroxymethyl)butyl]guanine (H2G) and AZT-lipid-PFA on human herpesvirus-6B infected cells.</a> Yao K, Hoest C, Rashti F, Schott TC, Jacobson S. J Clin Virol. 2009 Jun 11. [Epub ahead of print] PMID: 19524486</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19515239?ordinalpos=61&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Characterization of a potent non-cytotoxic shRNA directed to the HIV-1 co-receptor CCR5.</a> Shimizu S, Kamata M, Kittipongdaja P, Chen KN, Kim S, Pang S, Boyer J, Qin FX, An DS, Chen IS. Genet Vaccines Ther. 2009 Jun 10;7(1):8. [Epub ahead of print] PMID: 19515239</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19514109?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Capture and <b>transmission</b> of HIV-1 by the C-type lectin L-SIGN (DC-SIGNR) is inhibited by carbohydrate-binding agents and polyanions.</a> Auwerx J, François KO, Vanstreels E, Van Laethem K, Daelemans D, Schols D, Balzarini J. Antiviral Res. 2009 Jul;83(1):61-70. PMID: 19514109</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19503928?ordinalpos=65&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">An investigation into the anti-HIV activity of 2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxyuridine (d4U) and 2&#39;,3&#39;-dideoxyuridine (ddU) phosphoramidate &#39;ProTide&#39; derivatives.</a> Mehellou Y, Balzarini J, McGuigan C. Org Biomol Chem. 2009 Jun 21;7(12):2548-53. Epub 2009 Apr 30. PMID: 19503928</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19501260?ordinalpos=66&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Rapamycin enhances aplaviroc anti-HIV activity: implications for the clinical development of novel CCR5 antagonists.</a>Latinovic O, Heredia A, Gallo RC, Reitz M, Le N, Redfield RR. Antiviral Res. 2009 Jul;83(1):86-9. Epub 2009 Mar 9. PMID: 19501260</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19494300?ordinalpos=70&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Two MHC class I molecules associated with elite control of immunodeficiency virus replication, Mamu-B*08 and HLA-B*2705, bind peptides with sequence similarity.</a> Loffredo JT, Sidney J, Bean AT, Beal DR, Bardet W, Wahl A, Hawkins OE, Piaskowski S, Wilson NA, Hildebrand WH, Watkins DI, Sette A. J Immunol. 2009 Jun 15;182(12):7763-75. PMID: 19494300</p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19482481?ordinalpos=74&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antiviral and cytotoxic activities of aminoarylazo compounds and aryltriazene derivatives.</a> Tonelli M, Vazzana I, Tasso B, Boido V, Sparatore F, Fermeglia M, Paneni MS, Posocco P, Pricl S, La Colla P, Ibba C, Secci B, Collu G, Loddo R. Bioorg Med Chem. 2009 Jul 1;17(13):4425-40. Epub 2009 May 15. PMID: 19482481</p>

    <p class="title">15.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19469474?ordinalpos=77&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">New pyridinone derivatives as potent HIV-1 nonnucleoside reverse transcriptase inhibitors.</a> Le Van K, Cauvin C, de Walque S, Georges B, Boland S, Martinelli V, Demonté D, Durant F, Hevesi L, Van Lint C. J Med Chem. 2009 Jun 25;52(12):3636-43. PMID: 19469474</p>

    <p class="title">16.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19447621?ordinalpos=80&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Exploration of novel thiobarbituric acid-, rhodanine- and thiohydantoin-based HIV-1 integrase inhibitors.</a>Rajamaki S, Innitzer A, Falciani C, Tintori C, Christ F, Witvrouw M, Debyser Z, Massa S, Botta M. Bioorg Med Chem Lett. 2009 Jul 1;19(13):3615-8. Epub 2009 May 3. PMID: 19447621</p>

    <p class="title">17.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19426715?ordinalpos=96&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Proteasome inhibitors block HIV-1 replication by affecting both cellular and viral targets.</a> Yu L, Mohanram V, Simonson OE, Smith CI, Spetz AL, Mohamed AJ. Biochem Biophys Res Commun. 2009 Jul 17;385(1):100-5. Epub 2009 May 6. PMID: 19426715</p>

    <p class="title">18.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19409485?ordinalpos=102&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Nrf2 is involved in inhibiting Tat-induced HIV-1 long terminal repeat transactivation.</a> Zhang HS, Li HY, Zhou Y, Wu MR, Zhou HS. Free Radic Biol Med. 2009 Aug 1;47(3):261-8. Epub 2009 May 3. PMID: 19409485</p>

    <p class="title">19.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19356827?ordinalpos=117&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Bis-14-membered ring diketal diamines: synthesis and evaluation of their anti-HIV and anti-tumoral activities.</a>Affani R, Pélissier F, Aubertin AM, Dugat D. Eur J Med Chem. 2009 Aug;44(8):3138-46. Epub 2009 Mar 24. PMID: 19356827</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alering&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266500000003">Reawakening Retrocyclins: Ancestral Human Defensins Active Against HIV-1</a>.  Venkataraman, N; Cole, AL; Ruchala, P; Waring, AJ; Lehrer, RI; Stuchlik, O; Pohl, J; Cole, AM. PLOS BIOLOGY, 2009; 7(4): 720-729. </p>

    <p>21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266381200024">New Macrocyclic Amines Showing Activity as HIV Entry Inhibitors Against Wild Type and Multi-Drug Resistant Viruses</a>.  Rusconi, S; Lo Cicero, M; Vigano, O; Sirianni, F; Bulgheroni, E; Ferramosca, S; Bencini, A; Bianchi, A; Ruiz, L; Cabrera, C; Martinez-Picado, J; Supuran, CT; Galli, M. MOLECULES, 2009; 14(5): 1927-1937.</p>

    <p>22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266324800010">Antiviral Activities of Extracts of Euphorbia hirta L. against HIV-1, HIV-2 and SIVmac251.</a>  Gyuris, A; Szlavik, L; Minarovits, J; Vasas, A; Molnar, J; Hohmann, J.  IN VIVO, 2009; 23(3): 429-432. </p>

    <p>23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266449400018">Design, Synthesis and Anti-HIV Integrase Evaluation of 1,2,3-Triazol-4-yl-substituted 1,4-Dihydro-4-oxo-1,5-napthyridine-3-carboxylic Acids.</a> Zeng, J; Lu, XH; Zeng, CC; Hu, LM; Zhong, RG. CHINESE JOURNAL OF CHEMISTRY, 2009; 27(5): 953-962. </p>

    <p>24.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266724300006">Trichopyrone and Other Constituents from the Marine Sponge-Derived Fungus Trichoderma sp.</a>  Abdel-Lateff, A; Fisch, K; Wright, AD. ZEITSCHRIFT FUR NATURFORSCHUNG SECTION C-A JOURNAL OF BIOSCIENCES, 2009; 64(3-4): 186-192. </p>

    <p>25.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266467900012">Facile Synthesis of Non-nucleoside Compounds Starting from alpha-Chlorocarbenium Ions and Isocyanates as Potential HIV-1 Reverse Transcriptase Inhibitors.</a>  Hamed, AA; Zeid, IF; Manaa, AA.  ZEITSCHRIFT FUR NATURFORSCHUNG SECTION B-A JOURNAL OF CHEMICAL SCIENCES, 2009; 64(5): 555-564. </p>

    <p>26.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266696500010">Phosphoroamidate derivatives of N,O-nucleosides as inhibitors of reverse transcriptase.</a> Borrello, L; Chiacchio, U; Corsaro, A; Pistara, V; Iannazzo, D. ARKIVOC, 2009; Part 8: 112-124. </p>

    <p>27.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266514400170">Methyl 3 beta-methoxycarbonyloxy-4,4-dimethyl-17-oxo-16 alpha-(3-oxobutyl)-16 beta-carboxylate.</a>  Yan, X; Xu, SQ; Wang, JM; Chen, Y; Xia, P.  ACTA CRYSTALLOGRAPHICA SECTION E-STRUCTURE REPORTS ONLINE, 2009; 65 (Part 6): O1283-U1914. </p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
