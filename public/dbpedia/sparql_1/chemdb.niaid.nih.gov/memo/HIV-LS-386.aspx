

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-386.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ZJ1ZW/ujOI1LpiM7xF/v9J1mAx8rj8oeJ5t8QzXWZf8zv8JeDKHaV8Gt2AQZeHCuNYNZ+79/TNsASwuHp16TVOm1TDsgdpYzd7BxkZutJwS2eaxbSpzwrffb++E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="550C5995" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">     ONLINE DATABASE SEARCH - HIV-LS-386-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.</p>

    <p class="memofmt1-2">        Synthesis and Biological Properties of Novel 2-Aminopyrimidin-4(3h)-Ones Highly Potent Against Hiv-1 Mutant Strains</p>

    <p>          Mai, A. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2007</b>.  50(22): 5412-5424               </p>

    <p>          <span class="memofmt1-3">http:; publishorperishnihgov/Gatewaycgi?KeyUT=000250557100019</span></p>

    <p>2<b>.</b>         62029   HIV - LS - 386; PUBMED-HIV-11/26/2007</p>

    <p class="memofmt1-2">          Some new acyclic nucleotide analogues as antiviral prodrugs: Synthesis and bioactivities in vitro</p>

    <p>          Tang YB, Peng ZG, Liu ZY, Li YP, Jiang JD, and Li ZR</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.  17(22): 6350-3                    </p>

    <p>            <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17888662&amp;dopt=abstract</span></p>

    <p> </p>

    <p>3.         62039   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Organic thiophosphate antiretroviral agents</p>

    <p>          Walker, Dale M, Walker, Vernon E, Poirier, Miriam C, and Shearer, Gene M</p>

    <p>          PATENT:  WO <b>2007123868</b>  ISSUE DATE:  20071101</p>

    <p>          APPLICATION: 2007  PP: 48pp.</p>

    <p>          ASSIGNEE:  (United States Dept. of Health and Human Services, USA and Lovelace Respiratory Research Institute)     </p>

    <p> </p>

    <p>4.         62042   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Imidazolecarboxamide compounds as inhibitors of c-Fms kinase and their preparation, pharmaceutical compositions and use in the treatment of diseases</p>

    <p>          Illig, Carl R, Ballentine, Shelley K, Chen, Jinsheng, Desjarlais, Renee Louise, Meegalla, Sanath K, Wall, Mark, and Wilson, Kenneth</p>

    <p>          PATENT:  US <b>2007249608</b>  ISSUE DATE:  20071025</p>

    <p>          APPLICATION: 2007-15757  PP: 60pp.                         </p>

    <p>            ASSIGNEE:  (USA)</p>

    <p>         </p>

    <p>5.         62043   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Preparation of diketo-piperazine and piperidine derivatives as antiviral agents</p>

    <p>          Wang, Tao, Kadow, John F, Zhang, Zhongxing, Yin, Zhiwei, Meanwell, Nicholas A, Regueiro-Ren, Alicia, Swidorski, Jacob, Han, Ying, and Carini, David J</p>

    <p>          PATENT:  US <b>2007249579</b>  ISSUE DATE:  20071025              </p>

    <p>            APPLICATION: 2007-12387  PP: 277pp.</p>

    <p>          ASSIGNEE:  (Bristol-Myers Squibb Company, USA</p>

    <p>         </p>

    <p>6.         62044   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Preparation of 2-({4-chloro-2-[(3-chloro-5-cyanophenyl)carbonyl]phenyl}oxy)-N-(4-{[(2S)-2,3-dihydroxy-3-methylbutyl}oxy}-2-methylphenyl}acetamide as a non-nucleoside reverse transcriptase inhibitor</p>

    <p>          Aquino, Christopher Joseph, Freeman, George Andrew, and Martin, Michael Tolar</p>

    <p>          PATENT:  WO <b>2007121415</b>  ISSUE DATE:  20071025</p>

    <p>          APPLICATION: 2007  PP: 34pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA                      </p>

    <p> </p>

    <p>7.         62053   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          QSAR analysis of some phthalimide analogues based inhibitors of HIV-1 integrase</p>

    <p>          Bansal, Ruchi, Karthikeyan, C, Moorthy, N S Hari Narayana, and Trivedi, Piyush</p>

    <p>          ARKIVOC (Gainesville, FL, U. S.) <b>2007</b>.(15): 66-81          </p>

    <p>         </p>

    <p>8.         62054   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Identification and characterization of UK-201844, a novel inhibitor that interferes with human immunodeficiency virus type 1 gp160 processing</p>

    <p>          Blair, Wade S, Cao, Joan, Jackson, Lynn, Jimenez, Judith, Peng, Qinghai, Wu, Hua, Isaacson, Jason, Butler, Scott L, Chu, Alex, Graham, Joanne, Malfait, Anne-Marie, Tortorella, Micky, and Patick, Amy K</p>

    <p>          Antimicrob. Agents Chemother. <b>2007</b>.  51(10): 3554-3561              </p>

    <p> </p>

    <p>9.         62055   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV replication and expression of p24 with eIF-5A1 antisense nucleic acid</p>

    <p>          Thompson, John E, Taylor, Catherine, Dinarello, Charles, and Dondero, Richard S</p>

    <p>          PATENT:  WO <b>2007115047</b>  ISSUE DATE:  20071011</p>

    <p>          APPLICATION: 2007  PP: 46pp.</p>

    <p>          ASSIGNEE:  (Senesco Technologies, Inc. USA               </p>

    <p> </p>

    <p>10.       62056   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          HIV inhibiting 5-(hydroxymethylene and aminomethylene) substituted pyrimidines</p>

    <p>          Guillemont, Jerome Emile Georges, Mordant, Celine Isabelle, and Schmitt, Benoit Antoine</p>

    <p>          PATENT:  WO <b>2007113256</b>  ISSUE DATE:  20071011</p>

    <p>          APPLICATION: 2007  PP: 49pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.               </p>

    <p> </p>

    <p>11.       62075   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          SAMMA, a mandelic acid condensation polymer, inhibits dendritic cell-mediated HIV transmission</p>

    <p>          Chang, Theresa L, Teleshova, Natalia, Rapista, Aprille, Paluch, Maciej, Anderson, Robert A, Waller, Donald P, Zaneveld, Lourens J D, Granelli-Piperno, Angela, and Klotman, Mary E</p>

    <p>          FEBS Lett. <b>2007</b>.  581(24): 4596-4602   </p>

    <p> </p>

    <p> </p>

    <p>12.       62081   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Apolipoprotein E-derived antimicrobial peptide analogues with altered membrane affinity and increased potency and breadth of activity</p>

    <p>          Kelly, Bridie A, Neil, Stuart J, McKnight, Aine, Santos, Joana M, Sinnis, Photini, Jack, Edward R, Middleton, David A, and Dobson, Curtis B</p>

    <p>          FEBS J. <b>2007</b>.  274(17): 4511-4525                       </p>

    <p> </p>

    <p>13.       62083   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Entry of hepatitis C virus and human immunodeficiency virus is selectively inhibited by carbohydrate-binding agents but not by polyanions</p>

    <p>          Bertaux, Claire, Daelemans, Dirk, Meertens, Laurent, Cormier, Emmanuel G, Reinus, John F, Peumans, Willy J, Van Damme, Els J M, Igarashi, Yasuhiro, Oki, Toshikazu, Schols, Dominique, Dragic, Tatjana, and Balzarini, Jan</p>

    <p>          Virology <b>2007</b>.  366(1): 40-50               </p>

    <p> </p>

    <p>14.       62084   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Active site binding and sequence requirements for inhibition of HIV-1 reverse transcriptase by the RT1 family of single-stranded DNA aptamers</p>

    <p>          Kissel, Jay D, Held, Daniel M, Hardy, Richard W, and Burke, Donald H</p>

    <p>          Nucleic Acids Res. <b>2007</b>.  35(15): 5039-5050                     </p>

    <p>           </p>

    <p>15.       62088   HIV - LS - 386; SCIFINDER-HIV-11/21/2007</p>

    <p class="memofmt1-2">          Virucidal materials comprising nanoparticles of metal and/or metal compounds</p>

    <p>          Ren, Guogang, Oxford, John Sidney, Reip, Paul William, Lambkin-Williams, Robert, and Mann, Alexander</p>

    <p>          PATENT:  WO <b>2007093808</b>  ISSUE DATE:  20070823            </p>

    <p>            APPLICATION: 2007  PP: 37pp.</p>

    <p>          ASSIGNEE:  (Queen Mary &amp; Westfield College, UK, Qinetiq Nanomaterials Limited, and Retroscreen Virology Limited)</p>

    <p>         </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
