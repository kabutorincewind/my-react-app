

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-299.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jo08eOz3hzWQjjN6pVpjKBtxDs6LSl4/09t2iPPesaKlfR9oCyKDI+lsJmzeKLHNESvcOzUOfPDSZwNopW2ppuyNIbhw1iRq9MIGD9YTyengzq1wzopGEh7iNHk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A6E01383" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-299-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43609   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Dynamics of HIV replication in lymphocytes and the efficacy of protease inhibitors</p>

<p>           Bermejo, M, Sanchez-Palomino, S, Usan, L, and Alcami, J</p>

<p>           J Med Virol 2004. 73(4): 502-7</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15221892&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15221892&amp;dopt=abstract</a> </p><br />

<p>     2.    43610   HIV-LS-299; SCIFINDER-HIV-6/23/2004</p>

<p class="memofmt1-3">           Activity of a- and q-Defensins against Primary Isolates of HIV-1</p>

<p>           Wang, Wei, Owen, Sherry M, Rudolph, Donna L, Cole, Alexander M, Hong, Teresa, Waring, Alan J, Lal, Renu B, and Lehrer, Robert I</p>

<p>           Journal of Immunology 2004. 173(1):  515-520</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     3.    43611   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           PSC-RANTES Blocks R5 Human Immunodeficiency Virus Infection of Langerhans Cells Isolated from Individuals with a Variety of CCR5 Diplotypes</p>

<p>           Kawamura, T, Bruce, SE, Abraha, A, Sugaya, M, Hartley, O, Offord, RE, Arts, EJ, Zimmerman, PA, and Blauvelt, A</p>

<p>           J Virol 2004. 78(14): 7602-9</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220435&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220435&amp;dopt=abstract</a> </p><br />

<p>     4.    43612   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           The Phenylmethylthiazolylthiourea Nonnucleoside Reverse Transcriptase (RT) Inhibitor MSK-076 Selects for a Resistance Mutation in the Active Site of Human Immunodeficiency Virus Type 2 RT</p>

<p>           Auwerx, J, Stevens, M, Van Rompay, AR, Bird, LE, Ren, J, De Clercq, E, Oberg, B, Stammers, DK, Karlsson, A, and Balzarini, J</p>

<p>           J Virol 2004. 78(14): 7427-7437</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220416&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220416&amp;dopt=abstract</a> </p><br />

<p>     5.    43613   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Down-Regulation of CXCR(4) Expression in MT4 Cells by a Recombinant Vector Expressing Antisense RNA to CXCR(4) and Its Potential Anti-HIV-1 Effect</p>

<p>           Xing, HC, Xu, XY, Liu, Z, Wang, QH, Yu, M, and Si, CW</p>

<p>           Jpn J Infect Dis 2004. 57(3): 91-6</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15218216&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15218216&amp;dopt=abstract</a> </p><br />

<p>     6.    43614   HIV-LS-299; SCIFINDER-HIV-6/23/2004</p>

<p><b>           8-Hydroxy-1-oxo-tetrahydropyrrolopyrazine compounds useful as HIV integrase inhibitors</b>((Merck &amp; Co., Inc. USA)</p>

<p>           Wai, John S</p>

<p>           PATENT: WO 2004047725 A2;  ISSUE DATE: 20040610</p>

<p>           APPLICATION: 2003; PP: 85 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    7.     43615   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Imperatorin inhibits HIV-1 replication through an Sp1-dependent pathway</p>

<p>           Sancho, R, Marquez, N, Gomez-Gonzalo, M, Calzado, MA, Bettoni, G, Coiras, MT, Alcami, J, Lopez-Cabrera, M, Appendino, G, and Munoz, E</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15218031&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15218031&amp;dopt=abstract</a> </p><br />

<p>     8.    43616   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           New 3-O-Acyl Betulinic Acids from Strychnos vanprukii Craib</p>

<p>           Chien, NQ, Hung, NV, Santarsiero, BD, Mesecar, AD, Cuong, NM, Soejarto, DD, Pezzuto, JM, Fong, HH, and Tan, GT</p>

<p>           J Nat Prod 2004. 67(6): 994-998</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217281&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217281&amp;dopt=abstract</a> </p><br />

<p>     9.    43617   HIV-LS-299; SCIFINDER-HIV-6/23/2004</p>

<p><b>           Preparation of a,a-disubstituted benzylglycine derivatives as HIV protease inhibitors</b>((Bristol-Myers Squibb Company, USA)</p>

<p>           Carini, David J</p>

<p>           PATENT: WO 2004043355 A2;  ISSUE DATE: 20040527</p>

<p>           APPLICATION: 2003; PP: 81 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   10.    43618   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Uncharged AZT and D4T Derivatives of Phosphonoformic and Phosphonoacetic Acids as Anti-HIV Pronucleosides</p>

<p>           Shirokova, EA, Jasko, MV, Khandazhinskaya, AL, Ivanov, AV, Yanvarev, DV, Skoblov, YS, Mitkevich, VA, Bocharov, EV, Pronyaeva, TR, Fedyuk, NV, Kukhanova, MK, and Pokrovsky, AG</p>

<p>           J Med Chem 2004. 47(14): 3606-3614</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15214788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15214788&amp;dopt=abstract</a> </p><br />

<p>   11.    43619   HIV-LS-299; SCIFINDER-HIV-6/23/2004</p>

<p><b>           Anti-viral multi-quinone compounds and regiospecific synthesis thereof</b>((USA))</p>

<p>           Stagliano, Kenneth William and Emadi, Ashkan</p>

<p>           PATENT: US 2004087663 A1;  ISSUE DATE: 20040506</p>

<p>           APPLICATION: 2002-26541; PP: 13 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   12.    43620   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Temperature-Dependent Intermediates in HIV-1 Envelope Glycoprotein-Mediated Fusion Revealed by Inhibitors that Target N- and C-Terminal Helical Regions of HIV-1 gp41</p>

<p>           Gallo, SA, Clore, GM, Louis, JM, Bewley, CA, and Blumenthal, R</p>

<p>           Biochemistry 2004. 43(25): 8230-3</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15209519&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15209519&amp;dopt=abstract</a> </p><br />

<p>  13.     43621   HIV-LS-299; SCIFINDER-HIV-6/23/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral activities of the major metabolites of the HIV protease inhibitor ABT-378 (Lopinavir) [Bioorg. Med. Chem. Lett. 11 (2001) 1351]</p>

<p>           Sham, Hing L, Betebenner, David A, Herrin, Thomas, Kumar, Gondi, Saldivar, Ayda, Vasavanonda, Sudthida, Molla, Akhter, Kempf, Dale J, Plattner, Jacob J, and Norbeck, Daniel W</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(10): 2695</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   14.    43622   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Inhibition of HIV-1 TAR RNA-Tat peptide complexation using poly(acrylic acid)</p>

<p>           Zhao, H, Li, J, and Jiang, L</p>

<p>           Biochem Biophys Res Commun 2004. 320(1):  95-9</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15207707&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15207707&amp;dopt=abstract</a> </p><br />

<p>   15.    43623   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Suppression of human immunodeficiency virus replication in human brain tissue by nucleoside reverse transcriptase inhibitors</p>

<p>           Kandanearatchi, A, Vyakarnam, A, Landau, S, and Everall, IP</p>

<p>           J Neurovirol 2004. 10(2): 136-9</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15204933&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15204933&amp;dopt=abstract</a> </p><br />

<p>   16.    43624   HIV-LS-299; SCIFINDER-HIV-6/23/2004</p>

<p class="memofmt1-3">           Asymmetric Synthesis of Novel Thioiso Dideoxynucleosides with Exocyclic Methylene as Potential Antiviral Agents</p>

<p>           Gunaga, Prashantha, Baba, Masanori, and Jeong, Lak Shin</p>

<p>           Journal of Organic Chemistry 2004.  69(9): 3208-3211</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   17.    43625   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Inhibition of SARS coronavirus infection in vitro with clinically approved antiviral drugs</p>

<p>           Tan, EL, Ooi, EE, Lin, CY, Tan, HC, Ling, AE, Lim, B, and Stanton, LW</p>

<p>           Emerg Infect Dis 2004. 10(4): 581-6</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200845&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200845&amp;dopt=abstract</a> </p><br />

<p>   18.    43626   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Synthesis and biological evaluation of novel apio nucleosides with thiazole-4-carboxamide and 1,2,4-triazole-3-carboxamide</p>

<p>           Kim, MJ, Jeong, LS, Kim, JH, Shin, JH, Chung, SY, Lee, SK, and Chun, MW</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(4): 715-24</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200033&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200033&amp;dopt=abstract</a> </p><br />

<p>  19.     43627   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Chemoenzymatic syntheses of homo- and heterodimers of AZT and d4T, and evaluation of their anti-HIV activity</p>

<p>           Taourirte, M, Mohamed, LA, Rochdi, A, Vasseur, JJ, Fernandez, S, Ferrero, M, Gotor, V, Pannecouque, C, De, Clercq E, and Lazrek, HB</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(4): 701-14</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200032&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200032&amp;dopt=abstract</a> </p><br />

<p>   20.    43628   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Design, efficient synthesis, and anti-HIV activity of 4&#39;-C-cyano- and 4&#39;-C-ethynyl-2&#39;-deoxy purine nucleosides</p>

<p>           Kohgo, S, Yamada, K, Kitano, K, Iwai, Y, Sakata, S, Ashida, N, Hayakawa, H, Nameki, D, Kodama, E, Matsuoka, M, Mitsuya, H, and Ohrui, H</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(4): 671-90</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200030&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200030&amp;dopt=abstract</a> </p><br />

<p>   21.    43629   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           Synthesis and anti-HIV activity of 4&#39;-cyano-2&#39;,3&#39;-didehydro-3&#39;-deoxythymidine</p>

<p>           Haraguchi, K, Itoh, Y, Takeda, S, Honma, Y, Tanaka, H, Nitanda, T, Baba, M, Dutschman, GE, and Cheng, YC</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(4): 647-54</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200028&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200028&amp;dopt=abstract</a> </p><br />

<p>   22.    43630   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           HIV-1 Inhibition by Extracts of Clusiaceae Species from Mexico</p>

<p>           Huerta-Reyes, M, Basualdo, Mdel C, Lozada, L, Jimenez-Estrada, M, Soler, C, and Reyes-Chilpa, R</p>

<p>           Biol Pharm Bull 2004. 27(6): 916-20</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187447&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187447&amp;dopt=abstract</a> </p><br />

<p>   23.    43631   HIV-LS-299; PUBMED-HIV-6/29/2004</p>

<p class="memofmt1-3">           In vitro and in vivo transport and delivery of phosphorothioate oligonucleotides with cationic liposomes</p>

<p>           Miyano-Kurosaki, N, Barnor, JS, Takeuchi, H, Owada, T, Nakashima, H, Yamamoto, N, Matsuzaki, T, Shimada, F, and Takaku, H</p>

<p>           Antivir Chem Chemother 2004. 15(2): 93-100</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185727&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185727&amp;dopt=abstract</a> </p><br />

<p>   24.    43632   HIV-LS-299; SCIFINDER-HIV-6/23/2004</p>

<p class="memofmt1-3">           Synthesis of 6-arylvinyl analogues of the HIV drugs SJ-3366 and Emivirine</p>

<p>           Wamberg, Michael, Pedersen, Erik B, El-Brollosy, Nasser R, and Nielsen, Claus</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(5): 1141-1149</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.     43633   HIV-LS-299; WOS-HIV-6/21/2004</p>

<p class="memofmt1-3">           Styrylquinolines, integrase inhibitors acting prior to integration: a new mechanism of action for anti-integrase agents</p>

<p>           Bonnenfant, S, Thomas, CM, Vita, C, Subra, F, Deprez, E, Zouhiri, F, Desmaele, D, d&#39;Angelo, J, Mouscadet, JF, and Leh, H</p>

<p>           J VIROL 2004. 78(11): 5728-5736</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221513400021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221513400021</a> </p><br />

<p>   26.    43634   HIV-LS-299; WOS-HIV-6/21/2004</p>

<p class="memofmt1-3">           Synthesis and anti-HIV activity of metal complexes of SRR-SB3</p>

<p>           Bhowon, MG and Laulloo, BSJ</p>

<p>           INDIAN J CHEM A 2004. 43(5): 1131-1133</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221518400021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221518400021</a> </p><br />

<p>   27.    43635   HIV-LS-299; WOS-HIV-6/21/2004</p>

<p class="memofmt1-3">           Stereospecific chemical and enzymatic stability of phosphoramidate triester prodrugs of d4T in vitro</p>

<p>           Siccardi, D, Gumbleton, M, Omidi, Y, and McGuigan, C</p>

<p>           EUR J PHARM SCI 2004. 22(1): 25-31</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221502500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221502500003</a> </p><br />

<p>   28.    43636   HIV-LS-299; WOS-HIV-6/28/2004</p>

<p class="memofmt1-3">           Development and application of gemini surfactants</p>

<p>           Zhang, QS, Guo, BN, and Zhang, HM</p>

<p>           PROG CHEM 2004. 16(3): 343-348</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221701500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221701500004</a> </p><br />

<p>   29.    43637   HIV-LS-299; WOS-HIV-6/28/2004</p>

<p class="memofmt1-3">           Engagement of ICAM-3 provides a costimulatory signal for human immunodeficiency virus type 1 replication in both activated and quiescent CD4(+) T lymphocytes: Implications for virus pathogenesis</p>

<p>           Barat, C, Gervais, P, and Tremblay, MJ</p>

<p>           J VIROL 2004. 78(12): 6692-6697</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221772000061">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221772000061</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
