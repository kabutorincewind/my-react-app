

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-376.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="W6QTuv/8U4BM/bHRHG0uf1MFor8PG4L9cYvnGZ/8YDwKtqc1tXrIA3bBlJ7B+fMHIjeR2KIqqMgsvq1cTUBjDPVAWdJNvwkG4v6havXOfYMjslFWBAFCokR5rL8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7F66C9BB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-376-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61019   HIV-LS-376; WOS-HIV-6/1/2007</p>

    <p class="memofmt1-2">          The Hiv Protease Inhibitor Nelfinavir Downregulates Akt Phosphorylation by Inhibiting Proteasomal Activity and Inducing the Unfolded Protein Response</p>

    <p>          Gupta, A. <i>et al.</i></p>

    <p>          Neoplasia <b>2007</b>.  9(4): 271-278</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245802000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245802000002</a> </p><br />

    <p>2.     61020   HIV-LS-376; WOS-HIV-6/1/2007</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hiv Activity of New Chiral 1,2,4-Triazoles and 1,3,4-Thiadiazoles</p>

    <p>          Akhtar, T. <i>et al.</i></p>

    <p>          Heteroatom Chemistry <b>2007</b>.  18(3): 316-322</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245716800018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245716800018</a> </p><br />

    <p>3.     61021   HIV-LS-376; WOS-HIV-6/1/2007</p>

    <p class="memofmt1-2">          Current Developments in the Synthesis and Biological Activity of Hiv-1 Double-Drug Inhibitors</p>

    <p>          Muhanji, C. and Hunter, R.</p>

    <p>          Current Medicinal Chemistry <b>2007</b>.  14(11): 1207-1220</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245794200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245794200005</a> </p><br />

    <p>4.     61022   HIV-LS-376; EMBASE-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Evolution of a novel 5-amino-acid insertion in the [beta]3-[beta]4 loop of HIV-1 reverse transcriptase</p>

    <p>          Huigen, Marleen CDG, de Graaf, Loek, Eggink, Dirk, Schuurman, Rob, Muller, Viktor, Stamp, Anna, Stammers, David K, Boucher, Charles AB, and Nijhuis, Monique</p>

    <p>          Virology <b>2007</b>.  364(2): 395-406</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NJP3H9-5/2/a20eb76fccd513530a68b2caa3a59c62">http://www.sciencedirect.com/science/article/B6WXR-4NJP3H9-5/2/a20eb76fccd513530a68b2caa3a59c62</a> </p><br />

    <p>5.     61023   HIV-LS-376; EMBASE-HIV-6/11/2007</p>

    <p class="memofmt1-2">          A High Throughput HIV-1 Full Replication Assay that Includes HIV-1 Vif as an Antiviral Target</p>

    <p>          Cao, Joan, Isaacson, Jason, Patick, Amy, and Blair, Wade</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A64-A65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-38/2/317f2df7b2e1332cf5ab719950a0a9f7">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-38/2/317f2df7b2e1332cf5ab719950a0a9f7</a> </p><br />

    <p>6.     61024   HIV-LS-376; WOS-HIV-6/8/2007</p>

    <p class="memofmt1-2">          Dynamin 2 Is Required for the Enhancement of Hiv-1 Infectivity by Nef</p>

    <p>          Pizzato, M. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2007</b>.  104(16): 6812-6817</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245869200058">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245869200058</a> </p><br />
    <br clear="all">

    <p>7.     61025   HIV-LS-376; EMBASE-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Structural features and anti-HIV-1 activity of novel polysaccharides from red algae Grateloupia longifolia and Grateloupia filicina</p>

    <p>          Wang, SC, Bligh, SWA, Shi, SS, Wang, ZT, Hu, ZB, Crowded, J, Branford-White, C, and Vella, C</p>

    <p>          International Journal of Biological Macromolecules <b>2007</b>.  In Press, Accepted Manuscript: 85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7J-4NVH7WR-1/2/0a2a19bcf984b165c10cd9a54c3c2712">http://www.sciencedirect.com/science/article/B6T7J-4NVH7WR-1/2/0a2a19bcf984b165c10cd9a54c3c2712</a> </p><br />

    <p>8.     61026   HIV-LS-376; EMBASE-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV activity, and resistance profile of thymidine phosphonomethoxy nucleosides and their bis-isopropyloxymethylcarbonyl (bisPOC) prodrugs</p>

    <p>          Mackman, Richard L, Zhang, Lijun, Prasad, Vidya, Boojamra, Constantine G, Douglas, Janet, Grant, Deborah, Hui, Hon, Kim, Choung U, Laflamme, Genevieve, Parrish, Jay, Stoycheva, Antitsa D, Swaminathan, Swami, Wang, KeYu, and Cihlar, Tomas</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 4624</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NTJGXV-1/2/5d743c104697d014fa278c0fe44a0cc4">http://www.sciencedirect.com/science/article/B6TF8-4NTJGXV-1/2/5d743c104697d014fa278c0fe44a0cc4</a> </p><br />

    <p>9.     61027   HIV-LS-376; EMBASE-HIV-6/11/2007</p>

    <p class="memofmt1-2">          3-Hydroxy-1,5-dihydro-pyrrol-2-one derivatives as advanced inhibitors of HIV integrase</p>

    <p>          Kawasuji, Takashi, Fuji, Masahiro, Yoshinaga, Tomokazu, Sato, Akihiko, Fujiwara, Tamio, and Kiyama, Ryuichi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 4624</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NTJGXV-6/2/b4520220452c475fbec2ec62d798a96e">http://www.sciencedirect.com/science/article/B6TF8-4NTJGXV-6/2/b4520220452c475fbec2ec62d798a96e</a> </p><br />

    <p>10.   61028   HIV-LS-376; WOS-HIV-6/8/2007</p>

    <p class="memofmt1-2">          Ccr5 Receptor Antagonists: Discovery and Sar of Novel 4-Hydroxypiperidine Derivatives</p>

    <p>          Lu, S. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(7): 1883-1887</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245827900010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245827900010</a> </p><br />

    <p>11.   61029   HIV-LS-376; PUBMED-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Antiviral Activity against HIV-1, Intracellular Metabolism, and Effects on Human DNA Polymerases of 4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine</p>

    <p>          Nakata, H, Amano, M, Koh, Y, Kodama, E, Yang, G, Bailey, CM, Kohgo, S, Hayakawa, H, Matsuoka, M, Anderson, KS, Cheng, YC, and Mitsuya, H</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548498&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548498&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   61030   HIV-LS-376; PUBMED-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Kinetics of inhibition of HIV type 1 reverse transcriptase-bearing NRTI-associated mutations by apricitabine triphosphate</p>

    <p>          Frankel, FA, Coutsinos, D, Xu, H, and Wainberg, MA</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(2): 93-101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542154&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542154&amp;dopt=abstract</a> </p><br />

    <p>13.   61031   HIV-LS-376; PUBMED-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Mechanism of action of (-)-(2R,4R)-1-(2-hydroxymethyl-1,3-dioxolan-4-yl) thymine as an anti-HIV agent</p>

    <p>          Murakami, E, Bao, H, Basavapathruni, A, Bailey, CM, Du, J, Steuer, HM, Niu, C, Whitaker, T, Anderson, KS, Otto, MJ, and Furman, PA</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(2): 83-92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542153&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542153&amp;dopt=abstract</a> </p><br />

    <p>14.   61032   HIV-LS-376; PUBMED-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Apricitabine: a novel deoxycytidine analogue nucleoside reverse transcriptase inhibitor for the treatment of nucleoside-resistant HIV infection</p>

    <p>          Wainberg, MA, Cahn, P, Bethell, RC, Sawyer, J, and Cox, S</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(2): 61-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542150&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542150&amp;dopt=abstract</a> </p><br />

    <p>15.   61033   HIV-LS-376; PUBMED-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Synthesis and antiviral property of allophenylnorstatine-based HIV protease inhibitors incorporating d-cysteine derivatives as P(2)/P(3) moieties</p>

    <p>          Ami, E, Nakahara, K, Sato, A, Nguyen, JT, Hidaka, K, Hamada, Y, Nakatani, S, Kimura, T, Hayashi, Y, and Kiso, Y</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17537628&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17537628&amp;dopt=abstract</a> </p><br />

    <p>16.   61034   HIV-LS-376; PUBMED-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Potent inhibition of HIV-1 replication by backbone cyclic peptides bearing the Rev arginine rich motif</p>

    <p>          Chaloin, L, Smagulova, F, Hariton-Gazal, E, Briant, L, Loyter, A, and Devaux, C</p>

    <p>          J Biomed Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17520355&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17520355&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   61035   HIV-LS-376; PUBMED-HIV-6/11/2007</p>

    <p class="memofmt1-2">          Selection of Mutations in the Connection and RNase H Domains of Human Immunodeficiency Virus Type 1 Reverse Transcriptase that Increase Resistance to 3&#39;-Azido-3&#39;-Dideoxythymidine</p>

    <p>          Brehm, JH, Koontz, D, Meteer, JD, Pathak, V, Sluis-Cremer, N, and Mellors, JW</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507476&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507476&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
