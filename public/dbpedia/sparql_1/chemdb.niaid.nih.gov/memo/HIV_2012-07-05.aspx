

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-07-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="V8gWvbGz53gBD6qTcuYPypYMmQ8gTg5DzFs018XqggRLTkNzBKLHL7Qu7s402oT6Hv/xay2g0ONX52Hdlrz66ntJXI57uyPIGKQN4ove5385dzKwPgXJFscxlq8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AE74C5AE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 22 - July 5, 2012</h1>

    <p class="memofmt2-2">PubMed citations</p>

    <p class="plaintext"> 1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22520838">Effects of Sequence Changes in the HIV-1 gp41 Fusion Peptide on CCR5 Inhibitor Resistance.</a> Anastassopoulou, C.G., T.J. Ketas, R.W. Sanders, P. Johan Klasse, and J.P. Moore, Virology, 2012. 428(2): p. 86-97; PMID[22520838].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22727351">In Vitro anti-HIV-1 Activity in Cervicovaginal Secretions from Pregnant and Nonpregnant Women.</a> Anderson, B.L., M. Ghosh, C. Raker, J. Fahey, Y. Song, D.J. Rouse, C.R. Wira, and S. Cu-Uvin, American Journal of Obstetrics and Gynecology, 2012. 207(1): p. 65 e1-65 e10; PMID[22727351].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22739040">HIV-1 Gag-virus-like Particles Inhibit HIV-1 Replication in Dendritic Cells and T Cells through IFN-alpha-dependent Upregulation of APOBEC3G and 3F.</a> Chang, M.O., T. Suzuki, N. Yamamoto, M. Watanabe, and H. Takaku, Journal of Innate Immunity, 2012. <b>[Epub ahead of Print]</b>; PMID[22739040].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22698070">A Peptide Nucleic Acid-aminosugar Conjugate Targeting Transactivation Response Element of HIV-1 RNA Genome Shows a High Bioavailability in Human Cells and Strongly Inhibits Tat-mediated Transactivation of HIV-1 Transcription.</a> Das, I., J. Desire, D. Manvar, I. Baussanne, V.N. Pandey, and J.L. Decout, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of Print]</b>; PMID[22698070].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22732882">Residue-ligand Interaction Energy (ReLIE) on a Receptor-Dependent 3D-QSAR Analysis of S- and NH-DABOS as Non-nucleoside Reverse Transcriptase Inhibitors.</a> de Brito, M.A., C.R. Rodrigues, J.J. Cirino, J.Q. Araujo, T. Honorio, L.M. Cabral, R.B. de Alencastro, H.C. Castro, and M.G. Albuquerque, Molecules, 2012. 17(7): p. 7666-7694; PMID[22732882].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22760863">A Conformationally Frozen Peptoid Boosts CXCR4 Affinity and anti-HIV Activity.</a> Demmer, O., A.O. Frank, F. Hagn, M. Schottelius, L. Marinelli, S. Cosconati, R. Brack-Werner, S. Kremb, H.J. Wester, and H. Kessler, Angewandte Chemie International Edition English, 2012. <b>[Epub ahead of Print]</b>; PMID[22760863].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22743598">Enhanced anti-HIV Efficacy of Indinavir after Inclusion in CD4 Targeted Lipid Nanoparticles.</a> Endsley, A.N. and R.J. Ho, Journal of Acquired Immune Deficiency Syndrome, 2012. <b>[Epub ahead of Print]</b>; PMID[22743598].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22607556">Combinations of Griffithsin with Other Carbohydrate-binding Agents Demonstrate Superior Activity against HIV Type 1, HIV Type 2, and Selected Carbohydrate-binding Agent-resistant HIV Type 1 Strains.</a> Ferir, G., D. Huskens, K.E. Palmer, D.M. Boudreaux, M.D. Swanson, D.M. Markovitz, J. Balzarini, and D. Schols, AIDS Research and Human Retroviruses, 2012. <b>[Epub ahead of Print]</b>; PMID[22607556].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22575532">Chiral Resolution, Absolute Configuration Assignment and Biological Activity of Racemic Diarylpyrimidine CH(OH)-DAPY as Potent Nonnucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Gu, S.X., Z.M. Li, X.D. Ma, S.Q. Yang, Q.Q. He, F.E. Chen, E. De Clercq, J. Balzarini, and C. Pannecouque, European Journal of Medicinal Chemistry, 2012. 53C: p. 229-234; PMID[22575532].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22733274">Identification of Old Drugs as Potential Inhibitors of HIV-1 Integrase - Human LEDGF/p75 Interaction via Molecular Docking.</a> Hu, G., X. Li, X. Sun, W. Lu, G. Liu, J. Huang, X. Shen, and Y. Tang, Journal of Molecular Modeling, 2012. <b>[Epub ahead of Print]</b>; PMID[22733274].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22741800">Synthesis, Crystal Structure and anti-HIV Activity of 2-Adamantyl/Adamantylmethyl-5-aryl-1,3,4-oxadiazoles.</a> Khan, M.U., T. Akhtar, N.A. Al-Masoudi, H. Stoeckli-Evans, and S. Hameed, Medicinal Chemistry, 2012. <b>[Epub ahead of Print]</b>; PMID[22741800].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22742518">Peptides That Inhibit HIV-1 Integrase by Blocking Its Protein-protein Interactions.</a> Maes, M., A. Loyter, and A. Friedler, FEBS Journal, 2012. <b>[Epub ahead of Print]</b>; PMID[22742518].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22467632">Intravaginal Ring Delivery of Tenofovir Disoproxil Fumarate for Prevention of HIV and Herpes Simplex Virus Infection.</a> Mesquita, P.M., R. Rastogi, T.J. Segarra, R.S. Teller, N.M. Torres, A.M. Huber, P.F. Kiser, and B.C. Herold, Journal of Antimicrobial Chemotherapy, 2012. 67(7): p. 1730-1738; PMID[22467632].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22542107">Synthesis and Molecular Modelling Studies of Novel Carbapeptide Analogs for Inhibition of HIV-1 Protease.</a> Pawar, S.A., A.M. Jabgunde, P. Govender, G.E. Maguire, H.G. Kruger, R. Parboosing, M.E. Soliman, Y. Sayed, D.D. Dhavale, and T. Govender, European Journal of Medicinal Chemistry, 2012. 53C: p. 13-21; PMID[22542107].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22578783">Synthesis and Antiviral Evaluation of C5-Substituted-(1,3-diyne)-2&#39;-deoxyuridines.</a> Sari, O., V. Roy, J. Balzarini, R. Snoeck, G. Andrei, and L.A. Agrofoglio, European Journal of Medicinal Chemistry, 2012. 53C: p. 220-228; PMID[22578783].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22750986">Inhibition of HIV-1 Integrase Gene Expression by 10-23 DNAzyme.</a> Singh, N., A. Ranjan, S. Sur, R. Chandra, and V. Tandon, Journal of Biosciences, 2012. 37(3): p. 493-502; PMID[22750986].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22465243">Rapid Assessment of Antiviral Activity and Cytotoxicity of Silver Nanoparticles Using a Novel Application of the Tetrazolium-based Colorimetric Assay.</a> Trefry, J.C. and D.P. Wooley, Journal of Virological Methods, 2012. 183(1): p. 19-24; PMID[22465243].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22741604">Anti HIV-1 Agents 6. Synthesis and anti-HIV-1 Activity of Indolyl glyoxamides.</a> Wang, Y., N. Huang, X. Yu, L.M. Yang, X.Y. Zhi, Y.T. Zheng, and H. Xu, Medicinal Chemistry, 2012; PMID[22741604].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22546200">Design, Synthesis and Antiviral Activity of Novel Quinazolinones.</a> Wang, Z., M. Wang, X. Yao, Y. Li, J. Tan, L. Wang, W. Qiao, Y. Geng, Y. Liu, and Q. Wang, European Journal of Medicinal Chemistry, 2012. 53C: p. 275-282; PMID[22546200].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22760295">A Multi-functional Peptide as an HIV-1 Entry Inhibitor Based on Self-voncentration, Recognition, and Covalent Attachment.</a> Zhao, L., P. Tong, Y.X. Chen, Z.W. Hu, K. Wang, Y.N. Zhang, D.S. Zhao, L.F. Cai, K.L. Liu, Y.F. Zhao, and Y.M. Li, Organic and Biomolecular Chemistry, 2012. <b>[Epub ahead of Print]</b>; PMID[22760295].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304463600019">Synthesis, Anticancer, anti-HIV-1, and Antimicrobial Activity of Some Tricyclic Triazino and Triazolo 4,3-E Purine Derivatives.</a> Ashour, F.A., S.M. Rida, S.A.M. El-Hawash, M.M. ElSemary, and M.H. Badr, Medicinal Chemistry Research, 2012. 21(7): p. 1107-1119; ISI[000304463600019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003004651800020">Synthesis and anti-HIV-1 Activity of New Conjugates of 18 Beta- and 18 Alpha-Glycyrrhizic acids with Aspartic acid Esters.</a><i>.</i> Baltina, L.A., E.S. Chistoedova, R.M. Kondratenko, and O.A. Plyasunova, Chemistry of Natural Compounds, 2012. 48(2): p. 262-266; ISI[000304651800020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304486500020">Design, Synthesis, anti-HIV Evaluation and Molecular Modeling of Piperidine-linked Amino-triazine Derivatives as Potent Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Chen, X.W., P. Zhan, X. Liu, Z.H. Cheng, C.C. Meng, S.Y. Shao, C. Pannecouque, E. De Clercq, and X.Y. Liu, Bioorganic &amp; Medicinal Chemistry, 2012. 20(12): p. 3856-3864; ISI[000304486500020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304463600002">QSAR Modeling of Bifunctional Quinolonyl diketo acid Derivatives as Integrase Inhibitors.</a> Dubey, S., N. Abbas, G. Goutham, and P.A. Bhosle, Medicinal Chemistry Research, 2012. 21(7): p. 964-973; ISI[000304463600002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304657800001">Inhibition of HIV-1 Replication with Stable RNAi-mediated Knockdown of Autophagy Factors.</a> Eekels, J.J.M., S. Sagnier, D. Geerts, R.E. Jeeninga, M. Biard-Piechaczyk, and B. Berkhout, Virology Journal, 2012. 9; ISI[000304657800001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304816700016">A Potential in Vitro and in Vivo anti-HIV Drug Screening System for Chinese Herbal Medicines.</a> Feng, L., L. Wang, Y.Y. Ma, M. Li, and G.Q. Zhao, Phytotherapy Research, 2012. 26(6): p. 899-907; ISI[000304816700016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304570000003">A Multivalent HIV-1 Fusion Inhibitor Based on Small Helical Foldamers.</a> Guarise, C., S. Shinde, K. Kibler, G. Ghirlanda, L.J. Prins, and P. Scrimin, Tetrahedron, 2012. 68(23): p. 4346-4352; ISI[000304570000003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304438900007">Pentacycloundecane-diol-based HIV-1 Protease Inhibitors: Biological Screening, 2d NMR, and Molecular Simulation Studies.</a> Honarparvar, B., M.M. Makatini, S.A. Pawar, K. Petzold, M.E.S. Soliman, P.I. Arvidsson, Y. Sayed, T. Govender, G.E.M. Maguire, and H.G. Kruger, ChemMedChem, 2012. 7(6): p. 1009-1019; ISI[000304438900007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304651800006">Phenolic Compounds from Clinopodium urticifolium.</a> Hu, Q.F., H.X. Mu, H.T. Huang, H.Y. Lv, S.L. Li, and G.Y. Yang, Chemistry of Natural Compounds, 2012. 48(2): p. 198-201; ISI[000304651800006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304939700001">A Novel Aspartic Protease with HIV-1 Reverse Transcriptase Inhibitory Activity from Fresh Fruiting Bodies of the Wild Mushroom Xylaria hypoxylon.</a> Hu, Q.X., G.Q. Zhang, R.Y. Zhang, D.D. Hu, H.X. Wang, and T.B. Ng, Journal of Biomedicine and Biotechnology, 2012; ISI[000304939700001].</p>

    <p class="plaintext"> <b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304484600042">Pre-steady State Kinetic Analysis of Cyclobutyl Derivatives of 2 &#39;-Deoxyadenosine 5 &#39;-triphosphate as Inhibitors of HIV-1 Reverse Transcriptase.</a> Kim, J., L.G. Wang, Y.F. Li, K.D. Becnel, K.M. Frey, S.J. Garforth, V.R. Prasad, R.F. Schinazi, D.C. Liotta, and K.S. Anderson, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(12): p. 4064-4067; ISI[000304484600042].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304780500032">Development of a Unique Small Molecule Modulator of CXCR4.</a> Liang, Z.X., W.Q. Zhan, A.Z. Zhu, Y. Yoon, S.B. Lin, M. Sasaki, J.M.A. Klapproth, H. Yang, H.E. Grossniklaus, J.G. Xu, M. Rojas, R.J. Voll, M.M. Goodman, R.F. Arrendale, J. Liu, C.C. Yun, J.P. Snyder, D.C. Liotta, and H. Shim, PLoS One, 2012. 7(4); ISI[000304780500032].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304777700061">Characterization of an HIV-Targeted Transcriptional Gene-silencing RNA in Primary Cells.</a> Turner, A.M.W., A.M. Ackley, M.A. Matrone, and K.V. Morris, Human Gene Therapy, 2012. 23(5): p. 473-483; ISI[000304777700061].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304727000011">Structural Modulation Study of Inhibitory Compounds for Ribonuclease H Activity of Human Immunodeficiency Virus Type 1 Reverse Transcriptase.</a> Yanagita, H., S. Fudo, E. Urano, R. Ichikawa, M. Ogata, M. Yokota, T. Murakami, H.G. Wu, J. Chiba, J. Komano, and T. Hoshino, Chemical and Pharmaceutical Bulletin, 2012. 60(6): p. 764-771; ISI[000304727000011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305034100004">Phenolic Compounds from Cestrum aurantiacum.</a> Yin, H.C., Z. Liao, L. Hu, and Q. Hu, Asian Journal of Chemistry, 2012. 24(7): p. 2847-2849; ISI[000305034100004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305094100014">Catechins Containing a Galloyl Moiety as Potential anti-HIV-1 Compounds.</a> Zhao, Y.L., F. Jiang, P. Liu, W. Chen, and K.J. Yi, Drug Discovery Today, 2012. 17(11-12): p. 630-635; ISI[000305094100014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <h2> </h2>

    <h2>Latent HIV Reactivation Citations</h2>

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22739395">Disulfiram Reactivates Latent HIV-1 Expression through Depletion of the Phosphatase and Tensin Homolog (PTEN).</a> Doyon, G., J. Zerbato, J.W. Mellors, and N. Sluis-Cremer, AIDS, 2012; PMID[22739395].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304654200009">Latently Infected Cell Activation: A Way to Reduce the Size of the HIV Reservoir?</a> Forde, J., J.M. Volpe, and S.M. Ciupe, Bulletin of Mathematical Biology, 2012. 74(7): p. 1651-1672; ISI[000304654200009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0622-070512.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120524&amp;CC=WO&amp;NR=2012066442A1&amp;KC=A1">Preparation of 5,6,7,8-Tetrahydro[1]benzothieno[2,3-B]pyridin-3-ylacetic acid Derivatives for Treatment of HIV Infection.</a> Bell, A.S., I.B. Gardner, D.C. Pryde, F.M. Wakenhut, and K.R. Gibson. Patent. 2012. 2011-IB54852 2012066442: 64pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120524&amp;CC=WO&amp;NR=2012065963A2&amp;KC=A2">Preparation of Phenylpyrimidoindazolylpentanoic acid Derivatives and Analogs for Use as Antiviral Agents.</a> Chaltin, P., F. Christ, Z. Debyser, M. De Maeyer, A. Marchand, D. Marchand, and A. Voet. Patent. 2012. 2011-EP70089 2012065963: 110pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120518&amp;CC=WO&amp;NR=2012065062A1&amp;KC=A1">Preparation of Novel Amides of Amino Acids and Analogs as Antiviral Agents.</a> Chen, P., D. Zhou, D. Pryde, A. Bell, T. Li, Z. He, and Z.-W. Cai. Patent. 2012. 2011-US60370 2012065062: 182pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120607&amp;CC=WO&amp;NR=2012075122A2&amp;KC=A2">A Process for the Preparation of Hexahydrofurofuran Derivatives for Use as anti-HIV Agents.</a> Ghosh, A.K. and C.D. Martyr. Patent. 2012. 2011-US62611 2012075122: 41pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120614&amp;CC=WO&amp;NR=2012076822A1&amp;KC=A1">Peptide Ligands for the Nucleocapsid Protein Ncp7 for Use as AIDS Drugs.</a> Mely, Y., J. Langedijk, V. Shvadchak, and J.-L. Darlix. Patent. 2012. 2011-FR52909 2012076822: 70pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">44. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120614&amp;CC=WO&amp;NR=2012078844A1&amp;KC=A1">Preparation of Pyrimidoazepines as HIV Integrase Inhibitors Useful in the Treatment of Hiv Infection and AIDS.</a> Peese, K., B.N. Naidu, M. Patel, C. Li, and M.A. Walker. Patent. 2012. 2011-US63910 2012078844: 67pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">45. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120607&amp;CC=US&amp;NR=2012142627A1&amp;KC=A1">Monophosphate Prodrugs of DAPD and Analogs for Viral Infection and Cancer Therapy.</a> Schinazi, R.F. and S.J. Coats. Patent. 2012. 2011-306967</p>

    <p class="plaintext">20120142627: 48pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">46. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120607&amp;CC=WO&amp;NR=2012075235A1&amp;KC=A1">Preparation of Piperazine Alkyl Amides as HIV Attachment Inhibitors.</a> Wang, T., Z. Zhang, Z. Yin, J.F. Kadow, and N.A. Meanwell. Patent. 2012. 2011-US62804 2012075235: 107pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">47. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120607&amp;CC=WO&amp;NR=2012075362A2&amp;KC=A2">Preparation of Tetrahydroisoquinoline Derivatives as CXCR4 Receptor Modulators.</a> Wilson, L., D.C. Liotta, J. Wiseman, and M.G. Natchus. Patent. 2012. 2011-US63007 2012075362: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0622-070512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
