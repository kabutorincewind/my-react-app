

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-01-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+JzoiD9kELfkc7cCy/0Dh9bVh5D256NIexwDlr+nPfSiwVXcec3z0QJjtteNSv3osGp38mfoElrZ7MdOmFL4BI+67gmhfq0vfRLJx03CXByAqeokKLTTsgJ+wJQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C937D188" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 17 - January 30, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24471816">Bicyclic 1-Hydroxy-2-oxo-1,2-dihydropyridine-3-carboxamide-containing HIV-1 Integrase Inhibitors Having High Antiviral Potency against Cells Harboring Raltegravir-resistant Integrase Mutants.</a> Zhao, X.Z., S.J. Smith, M. Metifiot, B.C. Johnson, C. Marchand, Y. Pommier, S.H. Hughes, and T.R. Burke. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24471816].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24333452">Development of a Vaginal Delivery Film Containing EFdA, a Novel anti-HIV Nucleoside Reverse Transcriptase Inhibitor.</a> Zhang, W., M.A. Parniak, S.G. Sarafianos, M.R. Cost, and L.C. Rohan. International Journal of Pharmaceutics, 2014. 461(1-2): p. 203-213. PMID[24333452].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24446345">Rational Design of Substrate-based Multivalent Inhibitors of the Histone Acetyltransferase Tip60.</a> Yang, C., L. Ngo, and Y.G. Zheng. ChemMedChem, 2014. <b>[Epub ahead of print]</b>. PMID[24446345].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0117-013014</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24444350">Passive Immunization of Macaques with Polyclonal anti-SHIV IgG against a Heterologous Tier 2 SHIV: Outcome Depends on IgG Dose.</a> Sholukh, A.M., S.N. Byrareddy, V. Shanmuganathan, G. Hemashettar, S.K. Lakhashe, R.A. Rasmussen, J.D. Watkins, H.K. Vyas, S. Thorat, T. Brandstoetter, M.M. Mukhtar, J.K. Yoon, F.J. Novembre, F. Villinger, G. Landucci, D.N. Forthal, S. Ratcliffe, I. Tuero, M. Robert-Guroff, V.R. Polonis, M. Bilska, D.C. Montefiori, W.E. Johnson, H.C. Ertl, and R.M. Ruprecht. Retrovirology, 2014. 11(1): p. 8. PMID[24444350].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0117-013014</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24418542">Inhibition of HIV by Legalon-SIL Is Independent of Its Effect on Cellular Metabolism.</a>McClure, J., D.H. Margineantu, I.R. Sweet, and S.J. Polyak. Virology, 2014. 449: p. 96-103. PMID[24418542].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0117-013014</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24328502">Toll-like Receptor Agonists Are Potent Inhibitors of Human Immunodeficiency Virus-type 1 Replication in Peripheral Blood Mononuclear Cells.</a>Buitendijk, M., S.K. Eszterhas, and A.L. Howell. AIDS Research and Human Retroviruses, 2014. <b>[Epub ahead of print]</b>. PMID[24328502].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0117-013014</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329068400060">Design, Synthesis and Preliminary SAR Studies of Novel N-Arylmethyl Substituted Piperidine-linked Aniline Derivatives as Potent HIV-1 NNRTIs.</a>Zhang, L.Z., P. Zhan, X.W. Chen, Z.Y. Li, Z.M. Xie, T. Zhao, H.Q. Liu, E. De Clercq, C. Pannecouque, J. Balzarini, and X.Y. Liu. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 633-642. ISI[000329068400060].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329046300001">Dual-acting Stapled Peptides Target Both HIV-1 Entry and Assembly.</a>Zhang, H.T., F. Curreli, A.A. Waheed, P.Y. Mercredi, M. Mehta, P. Bhargava, D. Scacalossi, X.H. Tong, S. Lee, A. Cooper, M.F. Summers, E.O. Freed, and A.K. Debnath. Retrovirology, 2013. 10. ISI[000329046300001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329043500004">New Insights into Inhibition of Human Immunodeficiency Virus Type 1 Replication through Mutant tRNA(Lys3).</a> Wu, C.X., V.R. Nerurkar, and Y.N. Lu. Retrovirology, 2013. 10. ISI[000329043500004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329050400001">Protease Inhibitors Effectively Block Cell-to-Cell Spread of HIV-1 between T Cells.</a> Titanji, B.K., M. Aasa-Chapman, D. Pillay, and C. Jolly. Retrovirology, 2013. 10. ISI[000329050400001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328930400003">Stereoselective Synthesis of (Z)-1-Benzhydryl-4-cinnamylpiperazines via the Wittig Reaction.</a> Shivprakash, S. and G.C. Reddy. Synthetic Communications, 2014. 44(5): p. 600-609. ISI[000328930400003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328926400014">Design, Synthesis and Activity Evaluation of Novel Bivalent HIV-1 Fusion Inhibitor.</a> Ling, Y.B., K. Wang, X.F. Jiang, L.F. Cai, and K.L. Liu. Chemical Journal of Chinese Universities, 2013. 34(9): p. 2102-2107. ISI[000328926400014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328808300013">Synthesis of Novel 4&#39;-C-Methyl-1&#39;,3&#39;-dioxolane Pyrimidine Nucleosides and Evaluation of Its anti-HIV-1 Activity.</a> Kubota, Y., Y. Kaneda, K. Haraguchi, M. Mizuno, H. Abe, S. Shuto, T. Hamasaki, M. Baba, and H. Tanaka. Tetrahedron, 2013. 69(51): p. 10884-10892. ISI[000328808300013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329050300001">Interactions of Peptide Triazole thiols with Env gp120 Induce Irreversible Breakdown and Inactivation of Hiv-1 Virions.</a> Bastian, A.R., M. Contarino, L.D. Bailey, R. Aneja, D.R.M. Moreira, K. Freedman, K. McFadden, C. Duffy, A. Emileh, G. Leslie, J.M. Jacobson, J.A. Hoxie, and I. Chaiken. Retrovirology, 2013. 10. ISI[000329050300001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328149300026">Copper-catalyzed Domino Synthesis of 4-Oxopyrimido[1,2-a]indole Derivatives.</a> Wang, Y.Y., R.J. Wang, Y.Y. Jiang, C.Y. Tan, and H. Fu. Advanced Synthesis &amp; Catalysis, 2013. 355(14-15): p. 2928-2935. ISI[000328149300026].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328425400004">Unique Characteristics of Histone Deacetylase Inhibitors in Reactivation of Latent HIV-1 in Bcl-2-transduced Primary Resting CD4+ T Cells.</a>Shan, L., S.F. Xing, H.C. Yang, H. Zhang, J.B. Margolick, and R.F. Siliciano. Journal of Antimicrobial Chemotherapy, 2014. 69(1): p. 28-33. ISI[000328425400004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328839300002">Synthesis and anti-HIV-1 Activity of 1- omega-(Phenoxy)alkyl and -alkenyl uracil Derivatives.</a> Paramonova, M.P., D.A. Babkov, V.T. Valuev-Elliston, A.V. Ivanov, S.N. Kochetkov, C. Pannecouque, A.A. Ozerov, J. Balzarini, and M.S. Novikov. Pharmaceutical Chemistry Journal, 2013. 47(9): p. 459-463. ISI[000328839300002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328483900030">GANP Interacts with APOBEC3G and Facilitates Its Encapsidation into the Virions to Reduce HIV-1 Infectivity.</a> Maeda, K., S.A. Almofty, S.K. Singh, M.M.A. Eid, M. Shimoda, T. Ikeda, A. Koito, P. Pham, M.F. Goodman, and N. Sakaguchi. Journal of Immunology, 2013. 191(12): p. 6030-6039. ISI[000328483900030].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328526200006">F(ab&#39;)(2) Fragment of a gp41 NHR-trimer-induced IgM Monoclonal Antibody Neutralizes HIV-1 Infection and Blocks Viral Fusion by Targeting the Conserved gp41 Pocket.</a> Lu, L., M.L. Wei, Y.X. Chen, W.L. Xiong, F. Yu, Z. Qi, S.B. Jiang, and C.E. Pan. Microbes and Infection, 2013. 15(13): p. 887-894. ISI[000328526200006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328529400015">Design, Synthesis, and Antiviral Evaluation of Chimeric Inhibitors of HIV Reverse Transcriptase.</a> Iyidogan, P., T.J. Sullivan, M.D. Chordia, K.M. Frey, and K.S. Anderson. ACS medicinal chemistry letters, 2013. 4(12): p. 1183-1188. ISI[000328529400015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328703200017">Novel Bioactive Dibenzocyclooctadiene Lignans from Schisandra neglecta.</a>Gao, X.M., D.Y. Niu, C.Y. Meng, F.Q. Yao, B. Zhou, R.R. Wang, L.M. Yang, Y.T. Zheng, Q.F. Hu, H.D. Sun, and W.L. Xiao. Journal of the Brazilian Chemical Society, 2013. 24(12): p. 2021-+. ISI[000328703200017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328519000007">Hydrazones and 1,3-Thiazolidin-4-ones Incorporating Furman Moiety Synthesized from Eugenol, the Main Constituent of Ocimum santum L. Oil.</a> Dinh, N.H., T.T. Huan, H.T.T. Lan, and S.B. Han. Heterocycles, 2013. 87(11): p. 2319-2332. ISI[000328519000007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0117-013014.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
