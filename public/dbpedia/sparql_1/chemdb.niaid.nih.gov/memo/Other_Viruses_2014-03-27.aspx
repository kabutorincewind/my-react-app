

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-03-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vlsPm7zywga3OfmhvlOnIhlZBy+ZG7GMX7khhTFV7EDNiNtXG6h2F2LG8818ouRPLPNcKOT54Grxv4V9F3YBgtjsKzZFfItRSbIlyI/eORcrPQqwFdjcWEhVE4M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AB36F32A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 14 - March 27, 2014</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24621191">Discovery and Development of Hepatitis C Virus NS5A Replication Complex Inhibitors<i>.</i></a> Belema, M., O.D. Lopez, J.A. Bender, J.L. Romine, D.R. St Laurent, D.R. Langley, J.A. Lemm, D.R. O&#39;Boyle, 2nd, J.H. Sun, C. Wang, R.A. Fridell, and N.A. Meanwell. Journal of Medicinal Chemistry, 2014. 57(5): p. 1643-1672; PMID[24621191].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24521299">Hepatitis C Virus NS5A Replication Complex Inhibitors: The Discovery of Daclatasvir<i>.</i></a> Belema, M., V.N. Nguyen, C. Bachand, D.H. Deon, J.T. Goodrich, C.A. James, R. Lavoie, O.D. Lopez, A. Martel, J.L. Romine, E.H. Ruediger, L.B. Snyder, D.R. Laurent, F. Yang, J. Zhu, H.S. Wong, D.R. Langley, S.P. Adams, G.H. Cantor, A. Chimalakonda, A. Fura, B.M. Johnson, J.O. Knipe, D.D. Parker, K.S. Santone, R.A. Fridell, J.A. Lemm, D.R. O&#39;Boyle, 2nd, R.J. Colonno, M. Gao, N.A. Meanwell, and L.G. Hamann. Journal of Medicinal Chemistry, 2014. 57(5): p. 2013-2032; PMID[24521299].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24646941">Up-regulation of the ATP-binding Cassette Transporter A1 Inhibits Hepatitis C Virus Infection<i>.</i></a> Bocchetta, S., P. Maillard, M. Yamamoto, C. Gondeau, F. Douam, S. Lebreton, S. Lagaye, S. Pol, F. Helle, W. Plengpanich, M. Guerin, M. Bourgine, M.L. Michel, D. Lavillette, P. Roingeard, W. le Goff, and A. Budkowska. Plos One, 2014. 9(3): p. e92140; PMID[24646941].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24595428">Design and Synthesis of Imidazo[1,2-&#945;][1,8]naphthyridine Derivatives as anti-HCV Agents via Direct C-H Arylation<i>.</i></a> Huang, S., J. Qing, S. Wang, H. Wang, L. Zhang, and Y. Tang. Organic &amp; Biomolecular Chemistry, 2014. 12(15): p. 2344-2348; PMID[24595428].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23773186">Molecular Dynamics Simulations and Structure-based Rational Design Lead to Allosteric HCV NS5B Polymerase Thumb Pocket 2 Inhibitor with Picomolar Cellular Replicon Potency<i>.</i></a> Hucke, O., R. Coulombe, P. Bonneau, M. Bertrand-Laperle, C. Brochu, J. Gillard, M.A. Joly, S. Landry, O. Lepage, M. Llinas-Brunet, M. Pesant, M. Poirier, M. Poirier, G. McKercher, M. Marquis, G. Kukolj, P.L. Beaulieu, and T.A. Stammers. Journal of Medicinal Chemistry, 2014. 57(5): p. 1932-1943; PMID[23773186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24561671">Chemical Genetics-based Discovery of Indole Derivatives as HCV NS5B Polymerase Inhibitors<i>.</i></a> Jin, G., S. Lee, M. Choi, S. Son, G.W. Kim, J.W. Oh, C. Lee, and K. Lee. European Journal of Medicinal Chemistry, 2014. 75: p. 413-425; PMID[24561671].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24476391">Discovery of a Novel Class of Potent HCV NS4B Inhibitors: SAR Studies on Piperazinone Derivatives<i>.</i></a> Kakarla, R., J. Liu, D. Naduthambi, W. Chang, R.T. Mosley, D. Bao, H.M. Steuer, M. Keilman, S. Bansal, A.M. Lam, W. Seibel, S. Neilson, P.A. Furman, and M.J. Sofia. Journal of Medicinal Chemistry, 2014. 57(5): p. 2136-2160; PMID[24476391].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24568313">Novel Spiroketal Pyrrolidine GSK2336805 Potently Inhibits Key Hepatitis C Virus Genotype 1b Mutants: From Lead to Clinical Compound<i>.</i></a> Kazmierski, W.M., A. Maynard, M. Duan, S. Baskaran, J. Botyanszki, R. Crosby, S. Dickerson, M. Tallant, R. Grimes, R. Hamatake, M. Leivers, C.D. Roberts, and J. Walker. Journal of Medicinal Chemistry, 2014. 57(5): p. 2058-2073; PMID[24568313].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23944386">Discovery of Selective Small Molecule Type III Phosphatidylinositol 4-Kinase Alpha (PI4kIII&#945;) Inhibitors as Anti Hepatitis C (HCV) Agents<i>.</i></a> Leivers, A.L., M. Tallant, J.B. Shotwell, S. Dickerson, M.R. Leivers, O.B. McDonald, J. Gobel, K.L. Creech, S.L. Strum, A. Mathis, S. Rogers, C.B. Moore, and J. Botyanszki. Journal of Medicinal Chemistry, 2014. 57(5): p. 2091-2106; PMID[23944386].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24659881">Hepatitis C Virus NS5A Inhibitors and Drug Resistance Mutations<i>.</i></a> Nakamoto, S., T. Kanda, S. Wu, H. Shirasawa, and O. Yokosuka. World Journal of Gastroenterology, 2014. 20(11): p. 2902-2912; PMID[24659881].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24662079">Synthesis of New 1,2,3-Triazol-4-yl-quinazoline nucleoside and Acyclonucleoside Analogues<i>.</i></a> Ouahrouch, A., M. Taourirte, J.W. Engels, S. Benjelloun, and H.B. Lazrek. Molecules, 2014. 19(3): p. 3638-3653; PMID[24662079].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24512292">Highly Potent HCV NS4B Inhibitors with Activity against Multiple Genotypes<i>.</i></a> Phillips, B., R. Cai, W. Delaney, Z. Du, M. Ji, H. Jin, J. Lee, J. Li, A. Niedziela-Majka, M. Mish, H.J. Pyun, J. Saugier, N. Tirunagari, J. Wang, H. Yang, Q. Wu, C. Sheng, and C. Zonte. Journal of Medicinal Chemistry, 2014. 57(5): p. 2161-2166; PMID[24512292].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24564672">The Discovery of Asunaprevir (BMS-650032), an Orally Efficacious NS3 Protease Inhibitor for the Treatment of Hepatitis C Virus Infection<i>.</i></a> Scola, P.M., L.Q. Sun, A.X. Wang, J. Chen, N. Sin, B.L. Venables, S.Y. Sit, Y. Chen, A. Cocuzza, D.M. Bilder, S.V. D&#39;Andrea, B. Zheng, P. Hewawasam, Y. Tu, J. Friborg, P. Falk, D. Hernandez, S. Levine, C. Chen, F. Yu, A.K. Sheaffer, G. Zhai, D. Barry, J.O. Knipe, Y.H. Han, R. Schartman, M. Donoso, K. Mosure, M.W. Sinz, T. Zvyaga, A.C. Good, R. Rajamani, K. Kish, J. Tredup, H.E. Klei, Q. Gao, L. Mueller, R.J. Colonno, D.M. Grasela, S.P. Adams, J. Loy, P.C. Levesque, H. Sun, H. Shi, L. Sun, W. Warner, D. Li, J. Zhu, N.A. Meanwell, and F. McPhee. Journal of Medicinal Chemistry, 2014. 57(5): p. 1730-1752; PMID[24564672].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24555570">Discovery and Early Clinical Evaluation of BMS-605339, a Potent and Orally Efficacious Tripeptidic Acylsulfonamide NS3 Protease Inhibitor for the Treatment of Hepatitis C Virus Infection<i>.</i></a> Scola, P.M., A.X. Wang, A.C. Good, L.Q. Sun, K.D. Combrink, J.A. Campbell, J. Chen, Y. Tu, N. Sin, B.L. Venables, S.Y. Sit, Y. Chen, A. Cocuzza, D.M. Bilder, S. D&#39;Andrea, B. Zheng, P. Hewawasam, M. Ding, J. Thuring, J. Li, D. Hernandez, F. Yu, P. Falk, G. Zhai, A.K. Sheaffer, C. Chen, M.S. Lee, D. Barry, J.O. Knipe, W. Li, Y.H. Han, S. Jenkins, C. Gesenberg, Q. Gao, M.W. Sinz, K.S. Santone, T. Zvyaga, R. Rajamani, H.E. Klei, R.J. Colonno, D.M. Grasela, E. Hughes, C. Chien, S. Adams, P.C. Levesque, D. Li, J. Zhu, N.A. Meanwell, and F. McPhee. Journal of Medicinal Chemistry, 2014. 57(5): p. 1708-1729; PMID[24555570].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24195700">Discovery of N-[4-[6-tert-Butyl-5-methoxy-8-(6-methoxy-2-oxo-1H-pyridin-3-yl)-3-quinolyl]phenyl]methanesulfonamide (RG7109), a Potent Inhibitor of the Hepatitis C Virus NS5B Polymerase<i>.</i></a> Talamas, F.X., S.C. Abbot, S. Anand, K.A. Brameld, D.S. Carter, J. Chen, D. Davis, J. de Vicente, A.D. Fung, L. Gong, S.F. Harris, P. Inbar, S.S. Labadie, E.K. Lee, R. Lemoine, S. Le Pogam, V. Leveque, J. Li, J. McIntosh, I. Najera, J. Park, A. Railkar, S. Rajyaguru, M. Sangi, R.C. Schoenfeld, L.R. Staben, Y. Tan, J.P. Taygerly, A.G. Villasenor, and P.E. Weller. Journal of Medicinal Chemistry, 2014. 57(5): p. 1914-1931; PMID[24195700].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24529869">Discovery and Structure-Activity Relationships Study of Novel Thieno[2,3-b]pyridine Analogues as Hepatitis C Virus Inhibitors<i>.</i></a> Wang, N.Y., W.Q. Zuo, Y. Xu, C. Gao, X.X. Zeng, L.D. Zhang, X.Y. You, C.T. Peng, Y. Shen, S.Y. Yang, Y.Q. Wei, and L.T. Yu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(6): p. 1581-1588; PMID[24529869].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24529868">Sinugyrosanolide A, an Unprecedented C-4 Norcembranoid, from the Formosan Soft Coral Sinularia gyrosa<i>.</i></a> Cheng, S.Y., N.L. Shih, C.T. Chuang, S.F. Chiou, C.N. Yang, S.K. Wang, and C.Y. Duh. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(6): p. 1562-1564; PMID[24529868].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0314-032714.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332091200002">Modulation of PI3K-LXR alpha-Dependent Lipogenesis Mediated by Oxidative/Nitrosative Stress Contributes to Inhibition of HCV Replication by Quercetin<i>.</i></a> Pisonero-Vaquero, S., M.V. Garcia-Mediavilla, F. Jorquera, P.L. Majano, M. Benet, R. Jover, J. Gonzalez-Gallego, and S. Sanchez-Campos. Laboratory Investigation, 2014. 94(3): p. 262-274; ISI[000332091200002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0314-032714.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331441100010">Kaempferol Derivatives as Antiviral Drugs against the 3a Channel Protein of Coronavirus<i>.</i></a> Schwarz, S., D. Sauter, K. Wang, R.H. Zhang, B. Sun, A. Karioti, A.R. Bilia, T. Efferth, and W. Schwarz. Planta Medica, 2014. 80(2-3): p. 177-182; ISI[000331441100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0314-032714.</p><br />  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
