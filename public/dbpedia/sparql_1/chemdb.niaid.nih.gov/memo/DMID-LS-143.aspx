

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-143.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KoRs9/k6EdqBxYfXpRqem+qW463/I6+Cf9dXwND79C4GFLnGyrEwZ9kVQThlUKC09mv344NC3j6YBohFi52K6Sw48SzBz6tg4a6gZ6dfnUK6E4G8ESxuRAKeOts=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5DAEEB3A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-143-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60705   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Antiviral effect of Arbidol hydrochloride on adenovirus type 7 in vitro</p>

    <p>          Shi, Liqiao, Yang, Zhanqiu, He, Jing, Liu, Yuanyuan, and Xiao, Hong</p>

    <p>          Wuhan Daxue Xuebao, Yixueban <b>2006</b>.  27(1): 66-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60706   DMID-LS-143; PUBMED-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Discovery of the HCV NS3/4A Protease Inhibitor (1R,5S)-N-[3-Amino-1-(cyclobutylmethyl)-2,3-dioxopropyl]-3- [2(S)-[[[(1,1-dimethylethyl)amino]carbonyl]amino]-3,3-dimethyl-1-oxobutyl] 6,6-dimethyl-3-azabicyclo[3.1.0]hexan-2(S)-carboxamide (Sch 503034) II. Key Steps in Structure-Based Optimization</p>

    <p>          Prongay, AJ, Guo, Z, Yao, N, Pichardo, J, Fischmann, T, Strickland, C, Myers, J Jr, Weber, PC, Beyer, BM, Ingram, R, Hong, Z, Prosise, WW, Ramanathan, L, Taremi, SS, Yarosh-Tomaine, T, Zhang, R, Senior, M, Yang, RS, Malcolm, B, Arasappan, A, Bennett, F, Bogen, SL, Chen, K, Jao, E, Liu, YT, Lovey, RG, Saksena, AK, Venkatraman, S, Girijavallabhan, V, Njoroge, FG, and Madison, V</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17444623&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17444623&amp;dopt=abstract</a> </p><br />

    <p>3.     60707   DMID-LS-143; PUBMED-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Altered Interaction of the Matrix Protein with the Cytoplasmic Tail of Hemagglutinin Modulates Measles Virus Growth by Affecting Virus Assembly and Cell-Cell Fusion</p>

    <p>          Tahara, M, Takeda, M, and Yanagi, Y</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17442724&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17442724&amp;dopt=abstract</a> </p><br />

    <p>4.     60708   DMID-LS-143; PUBMED-DMID-4/23/2007</p>

    <p class="memofmt1-2">          The rtL80I Substitution in the Hepatitis B Virus Polymerase is Associated with Lamivudine Resistance and Enhanced Viral Replication in Vitro</p>

    <p>          Warner, N, Locarnini, S, Kuiper, M, Bartholomeusz, A, Ayres, A, Yuen, L, and Shaw, T</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17438047&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17438047&amp;dopt=abstract</a> </p><br />

    <p>5.     60709   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Carbohydrate-binding compounds for antiviral therapy</p>

    <p>          Balzarini, Jan </p>

    <p>          PATENT:  WO <b>2007033444</b>  ISSUE DATE:  20070329</p>

    <p>          APPLICATION: 2006  PP: 61pp.</p>

    <p>          ASSIGNEE:  (K.U. Leuven Research and Development, Belg.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     60710   DMID-LS-143; PUBMED-DMID-4/23/2007</p>

    <p class="memofmt1-2">          The threat of avian influenza A (H5N1). Part III: antiviral therapy</p>

    <p>          Cinatl, J Jr, Michaelis, M, and Doerr, HW</p>

    <p>          Med Microbiol Immunol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17431677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17431677&amp;dopt=abstract</a> </p><br />

    <p>7.     60711   DMID-LS-143; PUBMED-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Antiviral activity of oxidized polyamines</p>

    <p>          Bachrach, U</p>

    <p>          Amino Acids <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17429570&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17429570&amp;dopt=abstract</a> </p><br />

    <p>8.     60712   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Pyrazoline derivatives for treatment of viral infections</p>

    <p>          Ferguson, David M and Goodell, John</p>

    <p>          PATENT:  WO <b>2007038425</b>  ISSUE DATE:  20070405</p>

    <p>          APPLICATION: 2006  PP: 78pp.</p>

    <p>          ASSIGNEE:  (Regents of the University of Minnesota, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     60713   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Antiviral peptides targeting the west nile virus envelope protein</p>

    <p>          Bai, Fengwei, Town, Terrence, Pradhan, Deepti, Cox, Jonathan, Ashish, Ledizet, Michel, Anderson, John F, Flavell, Richard A, Krueger, Joanna K, Koski, Raymond A, and Fikrig, Erol</p>

    <p>          Journal of Virology <b>2007</b>.  81(4): 2047-2055</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60714   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Modified 4&#39;-nucleosides as antiviral agents for human immunodeficiency virus and hepatitis B virus</p>

    <p>          Du, Jinfa, Furman, Phillip, and Sofia, Michael</p>

    <p>          PATENT:  WO <b>2007038507</b>  ISSUE DATE:  20070405</p>

    <p>          APPLICATION: 2006  PP: 36pp.</p>

    <p>          ASSIGNEE:  (Pharmasset, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   60715   DMID-LS-143; PUBMED-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Differential antiviral activity of benzastatin C and its dechlorinated derivative from Streptomyces nitrosporeus</p>

    <p>          Lee, JG, Yoo, ID, and Kim, WG</p>

    <p>          Biol Pharm Bull <b>2007</b>.  30(4): 795-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17409523&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17409523&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   60716   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Application of ring-free ribonucleoside phosphonate as antiviral drug</p>

    <p>          Wang, Jinjing</p>

    <p>          PATENT:  CN <b>1935817</b>  ISSUE DATE: 20070328</p>

    <p>          APPLICATION: 1010-3528  PP: 16pp.</p>

    <p>          ASSIGNEE:  (Beijing Fullcan Bio-Pharmaceutical Technology Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   60717   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Antiviral application of non-nucleoside compound capable of inhibiting viral reverse transcriptase of hepatitis B virus</p>

    <p>          Wen, Yumei, Wang, Yongxiang, Viger, HDulan, and Kenyan, George Jr</p>

    <p>          PATENT:  CN <b>1927399</b>  ISSUE DATE: 20070314</p>

    <p>          APPLICATION: 1011-6516  PP: 11pp.</p>

    <p>          ASSIGNEE:  (Fudan University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   60718   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Medical application of 2a, 3b-dihydroxy-5,11(13)-dien-eudesman-12-oic acid in inhibiting hepatitis B virus</p>

    <p>          Zhao, Yu, Zhang, Lihe, Sun, Handong, Li, Haibo, Wu, Xiumei, Sun, Xianfeng, and Setokeshite, Joas</p>

    <p>          PATENT:  CN <b>1923188</b>  ISSUE DATE: 20070307</p>

    <p>          APPLICATION: 1005-3601  PP: 9pp.</p>

    <p>          ASSIGNEE:  (Peop. Rep. China)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60719   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Use of acyl thiouracil derivatives as antiviral drug</p>

    <p>          Zhou, Pei, Feng, Meiqing, Sun, Chuanwen, Huang, Hai, and Zhou, Wei</p>

    <p>          PATENT:  CN <b>1903203</b>  ISSUE DATE: 20070131</p>

    <p>          APPLICATION: 1002-8153  PP: 11pp.</p>

    <p>          ASSIGNEE:  (Fudan University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   60720   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Significance of novel HBV expression vectors in selecting antiviral drugs in clinical therapy</p>

    <p>          Lu Yin-ping, Dong Ji-hua, Liu Zhao, Guan Shi-he, Lu Meng-ji, and Yang Dong-liang</p>

    <p>          Zhonghua Gan Zang Bing Za Zhi <b>2007</b>.  15(1): 8-12.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60721   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Telbivudine (Tyzeka) for chronic Hepatitis B</p>

    <p>          Anon</p>

    <p>          Med Lett Drugs Ther <b>2007</b>.  49(1253): 11-12.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60722   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Hemin-peptide pharmaceutically acceptable salts thereof and using as anti-viral and virulicide agent</p>

    <p>          Nebol&#39;sin, VE, Zheltukhina, GA, Lobanova, TN, Nosik, DN, and Nosik, NN</p>

    <p>          PATENT:  RU <b>2296131</b>  ISSUE DATE: 20070327</p>

    <p>          APPLICATION: 2004-3341  PP: 8pp.</p>

    <p>          ASSIGNEE:  (OOO \&quot;Farminterpraisez\&quot;, Russia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>19.   60723   DMID-LS-143; SCIFINDER-DMID-4/23/2007</p>

    <p class="memofmt1-2">          Inhibitor design for SARS coronavirus main protease based on &quot;distorted key theory&quot;</p>

    <p>          Du Qi-Shi, Sun Hao, and Chou Kuo-Chen</p>

    <p>          Med Chem <b>2007</b>.  3(1): 1-6.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   60724   DMID-LS-143; WOS-DMID-4/24/2007</p>

    <p class="memofmt1-2">          A novel class of modified nucleosides: Synthesis of alkylidene isoxazolidinyl nucleosides containing thymine</p>

    <p>          Piperno, A, Rescifina, A, Corsaro, A, Chiacchio, MA, Procopio, A, and Romeo, R</p>

    <p>          EUROPEAN JOURNAL OF ORGANIC CHEMISTRY: Eur. J. Org. Chem <b>2007</b>.(9): 1517-1521, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245364200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245364200013</a> </p><br />

    <p>21.   60725   DMID-LS-143; WOS-DMID-4/24/2007</p>

    <p class="memofmt1-2">          Antiviral activity of HPMPC (cidofovir) against orf virus infected lambs</p>

    <p>          Scagliarini, A, McInnes, CJ, Gallina, L, Dal Pozzo, F, Scagliarini, L, Snoeck, R, Prosperi, S, Sales, J, Gilray, JA, and Nettleton, PF</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2007</b>.  73(3): 169-174, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245015800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245015800003</a> </p><br />

    <p>22.   60726   DMID-LS-143; WOS-DMID-4/24/2007</p>

    <p class="memofmt1-2">          Dengue drug screen</p>

    <p>          Anon</p>

    <p>          JAMA-JOURNAL OF THE AMERICAN MEDICAL ASSOCIATION: JAMA-J. Am. Med. Assoc <b>2007</b>.  297(13): 1423-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245404100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245404100005</a> </p><br />

    <p>23.   60727   DMID-LS-143; WOS-DMID-4/24/2007</p>

    <p class="memofmt1-2">          Immunosuppression reactivates viral replication long after resolution of woodchuck hepatitis virus infection</p>

    <p>          Menne, S, Cote, PJ, Butler, SD, Toshkov, IA, Gerin, JL, and Tennant, BC</p>

    <p>          HEPATOLOGY: Hepatology <b>2007</b>.  45(3): 614-622, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244794600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244794600008</a> </p><br />

    <p>24.   60728   DMID-LS-143; WOS-DMID-4/24/2007</p>

    <p class="memofmt1-2">          Helicases as antiviral and anticancer drug targets</p>

    <p>          Xu, GX</p>

    <p>          CURRENT MEDICINAL CHEMISTRY: Curr. Med. Chem <b>2007</b>.  14(8): 883-915, 33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244879100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244879100004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
