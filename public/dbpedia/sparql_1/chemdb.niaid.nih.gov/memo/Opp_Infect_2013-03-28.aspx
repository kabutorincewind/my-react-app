

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-03-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TuV8ZgMovRFJO2sSUkX+mnaXPGx9k7dQRuGHNfLwCLpjP2S71dfOIe/7X2xboDiqH6eKkaz16V+PETux+IuizgCviRFbcf6iMEuNVyciz5CsaKqIrDoPxh9BFKc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5569E569" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 15 - March 28, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23507777">Synthesis, Bioevaluation and Structural Study of Substituted Phthalazin-1(2H)-ones Acting as Antifungal Agents.</a> Derita, M., E. Del Olmo, B. Barboza, A.E. Garcia-Cadenas, J.L. Lopez-Perez, S. Andujar, D. Enriz, S. Zacchino, and A. San Feliciano. Molecules, 2013. 18(3): p. 3479-3501. PMID[23507777].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23509361">Monocyte- and Macrophage-Targeted NADPH Oxidase Mediates Antifungal Host Defense and Regulation of Acute Inflammation in Mice.</a> Grimm, M.J., R.R. Vethanayagam, N.G. Almyroudis, C.G. Dennis, A.N. Khan, A.C. D&#39;Auria, K.L. Singel, B.A. Davidson, P.R. Knight, T.S. Blackwell, T.M. Hohl, M.K. Mansour, J.M. Vyas, M. Rohm, C.F. Urban, T. Kelkka, R. Holmdahl, and B.H. Segal. Journal of Immunology, 2013. <b>[Epub ahead of print]</b>. PMID[23509361].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23259972">Isolation, Structure, and Biological Activity of Phaeofungin, a Cyclic Lipodepsipeptide from a Phaeosphaeria sp. Using the Genome-wide Candida albicans Fitness Test.</a> Singh, S.B., J. Ondeyka, G. Harris, K. Herath, D. Zink, F. Vicente, G. Bills, J. Collado, G. Platas, A. Gonzalez Del Val, J. Martin, F. Reyes, H. Wang, J.N. Kahn, S. Galuska, R. Giacobbe, G. Abruzzo, T. Roemer, and D. Xu. Journal of Natural Products, 2013. 76(3): p. 334-345. PMID[23259972].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23507286">Susceptibility Testing of Extensively Drug Resistant and Pre-Extensively Drug Resistant Mycobacterium tuberculosis against Levofloxacin, Linezolid, and Amoxicillin/Clavulanate.</a> Ahmed, I., K. Jabeen, R. Inayat, and R. Hasan. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b> PMID[23507286].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23521795">Bayesian Models Leveraging Bioactivity and Cytotoxicity Information for Drug Discovery.</a> Ekins, S., R.C. Reynolds, H. Kim, M.S. Koo, M. Ekonomidis, M. Talaue, S.D. Paget, L.K. Woolhiser, A.J. Lenaerts, B.A. Bunin, N. Connell, and J.S. Freundlich. Chemistry &amp; Biology, 2013. 20(3): p. 370-378. PMID[23521795].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23523640">New Fluoroquinolones Active against Fluoroquinolones-resistant Mycobacterium tuberculosis Strains.</a> Guerrini, V., M. De Rosa, S. Pasquini, C. Mugnaini, A. Brizzi, A.M. Cuppone, G. Pozzi, and F. Corelli. Tuberculosis, 2013. <b>[Epub ahead of print]</b> PMID[23523640].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br />  

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23486719">Colistin MIC Variability by Method for Contemporary Clinical Isolates of Multidrug Resistant Gram-Negative Bacilli.</a> Hindler, J.A. and R.M. Humphries. Journal of Clinical Microbiology, 2013. <b>[Epub ahead of print]</b> PMID[23486719].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23360475">Design, Synthesis, and Structure-Activity Relationship Studies of Tryptanthrins as Antitubercular Agents.</a> Hwang, J.M., T. Oh, T. Kaneko, A.M. Upton, S.G. Franzblau, Z. Ma, S.N. Cho, and P. Kim. Journal of Natural Products, 2013. 76(3): p. 354-367. PMID[23360475].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23529058">Antibacterial Activity of Salicylanilide 4-(trifluoromethyl)-benzoates.</a> Kratky, M., J. Vinsova, E. Novotna, J. Mandikova, F. Trejtnar, and J. Stolakova. Molecules, 2013. 18(4): p. 3674-3688. PMID[23529028].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23501157">The Canadian Medicinal Plant Heracleum maximum Contains Antimycobacterial Diynes and Furanocoumarins.</a> Neill, T.O., J.A. Johnson, D. Webster, and C.A. Gray. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b> PMID[23501157].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23414843">An Efficient Domino Reaction in Ionic Liquid: Synthesis and Biological Evaluation of Some Pyrano- and Thiopyrano-fused Heterocycles.</a> Parmar, N.J., R.A. Patel, B.D. Parmar, and N.P. Talpada. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(6): p. 1656-1661. PMID[23414843].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23356207">Quantitative Purity-Activity Relationships of Natural Products: The Case of Anti-tuberculosis Active Triterpenes from Oplopanax horridus.</a> Qiu, F., G. Cai, B.U. Jaki, D.C. Lankin, S.G. Franzblau, and G.F. Pauli. Journal of Natural Products, 2013. 76(3): p. 413-419. PMID[23356207].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23509148">Dipterinyl Calcium Pentahydrate Inhibits Intracellular Mycobacterial Growth in Human Monocytes Via the C-C Chemokine MIP-1beta and Nitric Oxide.</a> Sakala, I.G., C.S. Eickhoff, A. Blazevic, P. Moheno, R.F. Silver, and D.F. Hoft. Infection and immunity, 2013. <b>[Epub ahead of print]</b> PMID[23509148].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23516649">Microbiological, Histological, Immunological, and Toxin Response to Antibiotic Treatment in the Mouse Model of Mycobacterium Ulcerans Disease.</a> Sarfo, F.S., P.J. Converse, D.V. Almeida, J. Zhang, C. Robinson, M. Wansbrough-Jones, and J.H. Grosset. PLOS Neglected Tropical Diseases, 2013. 7(3): p. e2101. PMID[23516649].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23507275">Efficacy of Nitazoxanide against Clinical Isolates of Mycobacterium tuberculosis.</a> Shigyo, K., O. Ocheretina, Y.M. Merveille, W.D. Johnson, J.W. Pape, C.F. Nathan, and D.W. Fitzgerald. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b> PMID[23507275].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br />    

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23507276">In Vitro and in Vivo Efficacy of beta-Lactams against Replicating and Slowly Growing/Non Replicating M. tuberculosis.</a> Solapure, S., N. Dinesh, R. Shandil, V. Ramachandran, S. Sharma, D. Bhattacharjee, S. Ganguly, J. Reddy, V. Ahuja, V. Panduga, M. Parab, K.G. Vishwas, N. Kumar, M. Balganesh, and V. Balasubramanian. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b> PMID[23507276].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23497141">Antimalarial Screening via Large-scale Purification of PfATP6 and in Vitro Studies.</a> David-Bosne, S., I. Florent, A.M. Lund Winther, J. Bondo Hansen, M. Buch-Pedersen, P. Machillot, M. le Maire, and C. Jaxel. The FEBS Journal, 2013. <b>[Epub ahead of print]</b>. PMID[23497141].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">18, <a href="http://www.ncbi.nlm.nih.gov/pubmed/23286240">Antiproliferative and Antiplasmodial Dimeric Phloroglucinols from Mallotus oppositifolius from the Madagascar Dry Forest (1).</a> Harinantenaina, L., J.D. Bowman, P.J. Brodie, C. Slebodnick, M.W. Callmander, E. Rakotobe, R. Randrianaivo, V.E. Rasamison, A. Gorka, P.D. Roepe, M.B. Cassera, and D.G. Kingston. Journal of Natural Products, 2013. 76(3): p. 388-393. PMID[23286240].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23376043">In Vivo Antimalarial Activity, Toxicity and Phytochemical Screening of Selected Antimalarial Plants.</a> Musila, M.F., S.F. Dossaji, J.M. Nguta, C.W. Lukhoba, and J.M. Munyao. Journal of Ethnopharmacology, 2013. 146(2): p. 557-561. PMID[23376043].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23515079">Quinolone-3-diarylethers: A New Class of Antimalarial Drug.</a> Nilsen, A., A.N. Lacrue, K.L. White, I.P. Forquer, R.M. Cross, J. Marfurt, M.W. Mather, M.J. Delves, D.M. Shackleford, F.E. Saenz, J.M. Morrisey, J. Steuten, T. Mutka, Y. Li, G. Wirjanata, E. Ryan, S. Duffy, J.X. Kelly, B.F. Sebayang, A.M. Zeeman, R. Noviyanti, R.E. Sinden, C.H. Kocken, R.N. Price, V.M. Avery, I. Angulo-Barturen, M.B. Jimenez-Diaz, S. Ferrer, E. Herreros, L.M. Sanz, F.J. Gamo, I. Bathurst, J.N. Burrows, P. Siegl, R.K. Guy, R.W. Winter, A.B. Vaidya, S.A. Charman, D.E. Kyle, R. Manetsch, and M.K. Riscoe. Science Translational Medicine, 2013. 5(177): p. 177ra137. PMID[23515079].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0315-032813.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314693400033">Syntheses of Lipophilic Chalcones and Their Conformationally Restricted Analogues as Antitubercular Agents.</a> Ahmad, I., J.P. Thakur, D. Chanda, D. Saikia, F. Khan, S. Dixit, A. Kumar, R. Konwar, A.S. Negi, and A. Gupta. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1322-1325. ISI[000314693400033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813</p> 

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314484100003">22. Composition, and Antioxidant and Antimicrobial Activities of the Essential Oils of a Full-grown Pinus cembra L. Tree from the Calimani Mountains (Romania).</a> Apetrei, C.L., A. Spac, M. Brebu, C. Tuchilus, and A. Miron. Journal of the Serbian Chemical Society, 2013. 78(1): p. 27-37. ISI[000314484100003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314969900001">Synthesis and Biological Evaluation of Propargyl Acetate Derivatives as Anti-mycobacterial Agents.</a> Azerang, P., A.H. Rezayan, S. Sardari, F. Kobarfard, M. Bayat, and K. Tabib. Daru-Journal of Pharmaceutical Sciences, 2012. 20. ISI[000314969900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315093600038">Microwave-assisted Synthesis and Antimicrobial Screening of New Imidazole Derivatives Bearing 4-Thiazolidinone Nucleus.</a>  Desai, N.C., V.V. Joshi, K.M. Rajpara, H.V. Vaghani, and H.M. Satodiya. Medicinal Chemistry Research, 2013. 22(4): p. 1893-1908. ISI[000315093600038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00031525900015">Microwave Induced Synthesis of Fluorobenzamides Containing Thiazole and Thiazolidine as Promising Antimicrobial Analogs.</a> Desai, N.C., K.M. Rajpara, and V.V. Joshi. Journal of Fluorine Chemistry, 2013. 145: p. 102-111. ISI[000315252900015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315131200006">Chemical Composition and Biological Activities of the Essential Oil of Mentha suaveolens Ehrh.</a> El-Kashoury, E.S.A., H.I. El-Askary, Z.A. Kandil, M.A. Salem, and A.A. Sleem. Zeitschrift fur Naturforschung Section C-A Journal of Biosciences, 2012. 67(11-12): p. 571-579. ISI[000315131200006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003154944800001">Hydrogen Bond Acceptors and Additional Cationic Charges in Methylene Blue Derivatives: Photophysics and Antimicrobial Efficiency.</a> Felgentrager, A., T. Maisch, D. Dobler, and A. Spath. Biomed Research International, 2013. 12pp. ISI[000314944800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003154924700046">Assessment and Characterization of Antifungal and Antialgal Performances for Biocide-Enhanced Linear Low-Density Polyethylene.</a> Gitchaiwat, A., A. Kositchaiyong, K. Sombatsompop, B. Prapagdee, K. Isarangkura, and N. Sombatsompop. Journal of Applied Polymer Science, 2013. 128(1): p. 371-379. ISI[000314924700046].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315187400008">Antimicrobial Lectin from Schinus terebinthifolius Leaf.</a> Gomes, F.S., T.F. Procopio, T.H. Napoleao, L. Coelho, and P.M.G. Paiva. Journal of Applied Microbiology, 2013. 114(3): p. 672-679. ISI[000315187400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315182000034">Discovery of a Novel Broad-spectrum Antifungal Agent Derived from Albaconazole.</a> Guillon, R., F. Pagniez, C. Picot, D. Hedou, A. Tonnerre, E. Chosson, M. Duflos, T. Besson, C. Loge, and P. Le Pape. ACS Medicinal Chemistry Letters, 2013. 4(2): p. 288-292. ISI[000315182000034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315182000029">Synthesis and Antimycobacterial Activity of 2,1 &#39;-Dihydropyridomycins.</a> Horlacher, O.P., R.C. Hartkoorn, S.T. Cole, and K.H. Altmann. ACS Medicinal Chemistry Letters, 2013. 4(2): p. 264-268. ISI[000315182000029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br />    

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314693400004">Discovery of Novel Inhibitors Targeting the Mycobacterium tuberculosis O-Acetylserine sulfhydrylase (CysK1) Using Virtual High-Throughput Screening.</a> Kumar, V.U.J., O. Poyraz, S. Saxena, R. Schnell, P. Yogeeswari, G. Schneider, and D. Sriram. Bioorganic &amp; Medicinal Chemistry letters, 2013. 23(5): p. 1182-1186. ISI[000314693400004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315099600006">Chemical Composition and Antimicrobial and Allelopathic Activity of Tunisian Conyza sumatrensis (Retz.) E.Walker Essential Oils.</a> Mabrouk, S., K.B.H. Salah, A. Elaissi, L. Jlaiel, H. Ben Jannet, M. Aouni, and F. Harzallah-Skhiri. Chemistry &amp; Biodiversity, 2013. 10(2): p. 209-223. ISI[000315099600006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315331500049">Synthesis and Anti-candida Activity of Novel 2-Hydrazino-1,3-thiazole Derivatives.</a> Maillard, L.T., S. Bertout, O. Quinonero, G. Akalin, G. Turan-Zitouni, P. Fulcrand, F. Demirci, J. Martinez, and N. Masurier. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(6): p. 1803-1807. ISI[000315331500049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315228900001">Triterpenes and the Antimycobacterial Activity of Duroia Macrophylla huber (Rubiaceae).</a> Martins, D., L.L. Carrion, D.F. Ramos, K.S. Salome, P.E.A. da Silva, A. Barison, and C.V. Nunez. BioMed Research International, 2013. 7pp. ISI[000315228900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314893700021">Mycobacterium Marinum Infection in Drosophila melanogaster for Antimycobacterial Activity Assessment.</a> Oh, C.T., C. Moon, T.H. Choi, B.S. Kim, and J. Jang. Journal of Antimicrobial Chemotherapy, 2013. 68(3): p. 601-609. ISI[000314893700021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315093600024">Synthesis, Antimicrobial and Cytotoxic Activities of Novel 4-Trifluoromethyl-(1,2,3)-thiadiazolo-5-carboxylic Acid Hydrazide Schiff&#39;s Bases.</a> Rao, P.S., C. Kurumurthy, B. Veeraswamy, G.S. Kumar, B. Narsaiah, K.P. Kumar, U.S.N. Murthy, S. Karnewar, and S. Kotamraju. Medicinal Chemistry Research, 2013. 22(4): p. 1747-1755. ISI[000315093600024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315286400009">Antimicrobial Activity of Some Medicinal Plants from the Cerrado of the Central-western Region of Brazil.</a> Violante, I.M.P., L. Hamerski, W.S. Garcez, A.L. Batista, M.R. Chang, V.J. Pott, and F.R. Garcez. Brazilian Journal of Microbiology, 2012. 43(4): p. 1302-1308. ISI[000315286400009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314693400054">3H-1,2,4-Dithiazol-3-one Compounds as Novel Potential Affordable Antitubercular Agents.</a> Yang, J.Z., W.Y. Pi, L. Xiong, W. Ang, T. Yang, J. He, Y.Y. Liu, Y. Chang, W.W. Ye, Z.L. Wang, Y.F. Luo, and Y.Q. Wei. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1424-1427. ISI[000314693400054].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0315-032813.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
