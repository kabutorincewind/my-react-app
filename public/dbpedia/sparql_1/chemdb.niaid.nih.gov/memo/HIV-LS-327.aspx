

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-327.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="a68VmJfzIcy0BE5AaQe/BJVzOZFMyuLFKAVkZ9r4IRQUKIIlJmyXu49rptwY6uk6OGV1Dirnr2oPLAH45IjGZ4goHetCryFuqWRfmwxHBTKPTiR44qFUULck5bM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C2C757A9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-327-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47645   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          Pyridine N-oxide derivatives: unusual anti-HIV compounds with multiple mechanisms of antiviral action</p>

    <p>          Balzarini, Jan, Stevens, Miguel, De Clercq, Erik, Schols, Dominique, and Pannecouque, Christophe</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2005</b>.  55(2): 135-138</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     47646   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          Emtricitabine: A novel nucleoside reverse transcriptase inhibitor</p>

    <p>          Molina, JM and Cox, SL</p>

    <p>          Drugs Today (Barc) <b>2005</b>.  41(4): 241-252</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16034488&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16034488&amp;dopt=abstract</a> </p><br />

    <p>3.     47647   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          4&#39;-substituted carbovir-and abacavir-derivatives as well as related compounds with hiv and hcv antiviral activity</p>

    <p>          Fardis, Maria and Kim, Choung U</p>

    <p>          PATENT:  WO <b>2005063751</b>  ISSUE DATE:  20050714</p>

    <p>          APPLICATION: 2004</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     47648   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          Synthesis of 5-haloethynyl- and 5-(1,2-dihalo)vinyluracil nucleosides: Antiviral activity and cellular toxicity</p>

    <p>          Escuret, V, Aucagne, V, Joubert, N, Durantel, D, Rapp, KL, Schinazi, RF, Zoulim, F, and Agrofoglio, LA</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16023859&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16023859&amp;dopt=abstract</a> </p><br />

    <p>5.     47649   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          Rational design of novel diketoacid-containing ferrocene inhibitors of HIV-1 integrase</p>

    <p>          da Silva, CH,  Ponte, GD, Neto, AF, and Taft, CA</p>

    <p>          Bioorg Chem <b>2005</b>.  33(4): 274-84</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16023487&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16023487&amp;dopt=abstract</a> </p><br />

    <p>6.     47650   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          In vitro inhibition of R5 HIV-1 infectivity by X4 V3-derived synthetic peptides</p>

    <p>          Baritaki, S, Dittmar, MT, Spandidos, DA, and Krambovitis, E</p>

    <p>          Int J Mol Med <b>2005</b>.  16(2): 333-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16012771&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16012771&amp;dopt=abstract</a> </p><br />

    <p>7.     47651   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          Antiviral activities of membranotropic compounds modified by adamantane and norbornene pharmacophores to different HIV-1 strains</p>

    <p>          Kiseleva, YaYu, Perminova, NG, Plyasunova, OA, Timofeev, DI, Serbin, AV, Kasyan, LI, Egorov, YuA, Grebinik, TS, and Timofeev, IV</p>

    <p>          Molekulyarnaya Genetika, Mikrobiologiya i Virusologiya <b>2005</b>.(2): 33-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     47652   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          AMD3465, a monomacrocyclic CXCR4 antagonist and potent HIV entry inhibitor</p>

    <p>          Hatse, S, Princen, K, Clercq, ED, Rosenkilde, MM, Schwartz, TW, Hernandez-Abad, PE, Skerlj, RT, Bridger, GJ, and Schols, D</p>

    <p>          Biochem Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16011832&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16011832&amp;dopt=abstract</a> </p><br />

    <p>9.     47653   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          Physiologically stable vanadium(iv) porphyrins as a new class of anti-HIV agents</p>

    <p>          Wong, SY, Wai-Yin, Sun R, Chung, NP, Lin, CL, and Che, CM</p>

    <p>          Chem Commun (Camb) <b>2005</b>.(28): 3544-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16010318&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16010318&amp;dopt=abstract</a> </p><br />

    <p>10.   47654   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          The discovery of a class of novel HIV-1 maturation inhibitors and their potential in the therapy of HIV</p>

    <p>          Yu, D, Wild, CT, Martin, DE, Morris-Natschke, SL, Chen, CH, Allaway, GP, and Lee, KH</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(6): 681-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004596&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004596&amp;dopt=abstract</a> </p><br />

    <p>11.   47655   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          High-Speed Synthesis of Potent C(2)-Symmetric HIV-1 Protease Inhibitors by In-Situ Aminocarbonylations</p>

    <p>          Wannberg, J, Kaiser, NF, Vrang, L, Samuelsson, B, Larhed, M, and Hallberg, A</p>

    <p>          J Comb Chem <b>2005</b>.  7(4): 611-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004505&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004505&amp;dopt=abstract</a> </p><br />

    <p>12.   47656   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          Peptides, peptide analogs, and peptide mimetics as inhibitors of HIV</p>

    <p>          North, Thomas W, Lam, Kit, Luciw, Paul A, and Duong, Yen</p>

    <p>          PATENT:  WO <b>2005056577</b>  ISSUE DATE:  20050623</p>

    <p>          APPLICATION: 2004  PP: 56 pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   47657   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          TSAO derivatives the first non-peptide inhibitors of HIV-1 RT dimerization</p>

    <p>          Camarasa, MJ,  Velazquez, S, San-Felix, A, and Perez-Perez, MJ</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(3): 147-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004078&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004078&amp;dopt=abstract</a> </p><br />

    <p>14.   47658   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          Delivery of antiviral agents in liposomes</p>

    <p>          Duzgunes, Nejat, Simoes, Sergio, Slepushkin, Vladimir, Pretzer, Elizabeth, Flasher, Diana, Salem, Isam I, Steffan, Gerhard, Konopka, Krystyna, and Pedroso de Lima, Maria C</p>

    <p>          Methods in Enzymology <b>2005</b>.  391: 351-373</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   47659   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          Plasma pharmacokinetics of once- versus twice-daily lamivudine and abacavir: Simplification of combination treatment in HIV-1-infected children (PENTA-13)</p>

    <p>          Bergshoeff, Alina, Burger, David, Verweij, Corrien, Farrelly, Laura, Flynn, Jacquie, Le Prevost, Marthe, Walker, Sarah, Novelli, Vas, Lyall, Hermione, Khoo, Saye, and Gibb, Di</p>

    <p>          Antiviral Therapy <b>2005</b>.  10(2): 239-246</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   47660   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          A preparation of diazaindole derivatives, useful as antiviral agents</p>

    <p>          Bender, John A, Yang, Zhong, Kadow, John F, and Meanwell, Nicholas A</p>

    <p>          PATENT:  US <b>2005124623</b>  ISSUE DATE:  20050609</p>

    <p>          APPLICATION: 2004-62054  PP: 87 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   47661   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          Safety and antiviral activity of lopinavir/ritonavir-based therapy in human immunodeficiency virus type 1 (HIV-1) infection</p>

    <p>          Kaplan, SS and Hicks, CB</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994247&amp;dopt=abstract</a> </p><br />

    <p>18.   47662   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          Hydrophobicity in the design of P2/P2&#39; tetrahydropyrimidinone HIV protease inhibitors</p>

    <p>          Garg, R and Patel, D</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993582&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993582&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   47663   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          From SAR to comparative QSAR: role of hydrophobicity in the design of 4-hydroxy-5,6-dihydropyran-2-ones HIV-1 protease inhibitors</p>

    <p>          Bhhatarai, Barun and Garg, Rajni</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(12): 4078-4084</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   47664   HIV-LS-327; PUBMED-HIV-7/25/2005</p>

    <p class="memofmt1-2">          Isolation of TAK-779-resistant HIV-1 from an R5 HIV-1 gp120 V3 loop library</p>

    <p>          Yusa, K, Maeda, Y, Fujioka, A, Monde, K, and Harada, S</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15983047&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15983047&amp;dopt=abstract</a> </p><br />

    <p>21.   47665   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          Rhodanine compounds and compositions for use as antiviral agents</p>

    <p>          Rajinder, Singh, Usha, Ramesh, Clough, Jeffrey, Issakani, Sarkiz D, and Look, Gary Charles</p>

    <p>          PATENT:  WO <b>2005041951</b>  ISSUE DATE:  20050512</p>

    <p>          APPLICATION: 2004  PP: 82 pp.</p>

    <p>          ASSIGNEE:  (Rigel Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   47666   HIV-LS-327; WOS-HIV-7/17/2005</p>

    <p class="memofmt1-2">          Sesquin, a potent defensin-like antimicrobial peptide from ground beans with inhibitory activities toward tumor cells and HIV-1 reverse transcriptase</p>

    <p>          Wong, JH and Ng, TB</p>

    <p>          PEPTIDES <b>2005</b>.  26(7): 1120-1126, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230155600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230155600004</a> </p><br />

    <p>23.   47667   HIV-LS-327; SCIFINDER-HIV-7/18/2005</p>

    <p class="memofmt1-2">          Preparation of phenanthridine derivatives as anti-viral agents</p>

    <p>          Tor, Yitzhak and Luedtke, Nathan</p>

    <p>          PATENT:  WO <b>2005016343</b>  ISSUE DATE:  20050224</p>

    <p>          APPLICATION: 2004  PP: 58 pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   47668   HIV-LS-327; WOS-HIV-7/17/2005</p>

    <p class="memofmt1-2">          First synthesis and evaluation of the inhibitory effects of Aza analogues of TSAO on HIV-1 replication</p>

    <p>          Van Nhien, AN, Tomassi, C, Len, C, Marco-Contelles, JL,  Balzarini, J, Pannecouque, C, De Clercq, E, and Postel, D</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(13): 4276-4284, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230121800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230121800014</a> </p><br />
    <br clear="all">

    <p>25.   47669   HIV-LS-327; WOS-HIV-7/17/2005</p>

    <p><b>          Subtype analysis and mutations to antiviral drugs in HIV-1-infected patients from Mozambique before initiation of antiretroviral therapy: Results from the DREAM programme</b> </p>

    <p>          Bellocchi, MC, Forbici, F, Palombi, L, Gori, C, Coelho, E, Svicher, V, D&#39;Arrigo, R, Emberti-Gialloreti, L, Ceffa, S, Erba, F, Marazzi, MC, Silberstein, FC, and Perno, CF</p>

    <p>          JOURNAL OF MEDICAL VIROLOGY <b>2005</b>.  76(4): 452-458, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230215000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230215000004</a> </p><br />

    <p>26.   47670   HIV-LS-327; WOS-HIV-7/17/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of beta-D-3 &#39;-azido-2 &#39;,3 &#39;-unsaturated nucleosides and beta-D-3 &#39;-azido-3 &#39;-deoxyribofuranosylnucleosides</p>

    <p>          Gadthula, S, Schinazi, RF, and Chu, CK</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U260-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228177701458">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228177701458</a> </p><br />

    <p>27.   47671   HIV-LS-327; WOS-HIV-7/17/2005</p>

    <p class="memofmt1-2">          Role of quantitative structure activity relationship (QSAR) in understanding HIV-1 binding domain and designing protease inhibitors</p>

    <p>          Bhhatarai, B and Garg, R</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U777-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228177705470">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228177705470</a> </p><br />

    <p>28.   47672   HIV-LS-327; WOS-HIV-7/24/2005</p>

    <p class="memofmt1-2">          The first sorbicillinoid alkaloids, the antileukemic sorbicillactones A and B, from a sponge-derived Penicillium chrysogenum strain</p>

    <p>          Bringmann, G,  Lang, G, Gulder, TAM, Tsuruta, H, Muhlbacher, J, Maksimenka, K, Steffens, S, Schaumann, K, Stohr, R, Wiese, J, Imhoff, JF, Perovic-Ottstadt, S, Boreiko, O, and Muller, WEG</p>

    <p>          TETRAHEDRON <b>2005</b>.  61(30): 7252-7265, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230327300021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230327300021</a> </p><br />

    <p>29.   47673   HIV-LS-327; WOS-HIV-7/24/2005</p>

    <p class="memofmt1-2">          The interaction of HIV-1 with the host factors</p>

    <p>          Komano, J, Futahashi, Y, Urano, E, Miyauchi, K, Murakami, T, Matsuda, Z, and Yamamoto, N</p>

    <p>          JAPANESE JOURNAL OF INFECTIOUS DISEASES <b>2005</b>.  58(3): 125-130, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230217500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230217500001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
