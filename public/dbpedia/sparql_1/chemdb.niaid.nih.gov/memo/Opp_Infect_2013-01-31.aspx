

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-01-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="n+/XySmB6yr6zZ+kp5t8I/0lPRnFjohrhl9RRRrSwFSNcd7CumG7zIKXxwBHxF5Mi0Ys61okbbaN8qukawmDAiOXK/0iryvN+5pqt/nMr+n0kVQTAceQLuIjUqA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A87F5E14" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:<br />January 18 - January 31, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23111348">Reversal of Efflux Mediated Antifungal Resistance Underlies Synergistic Activity of Two Monoterpenes with Fluconazole.</a> Ahmad, A., A. Khan, and N. Manzoor. European Journal of Pharmaceutical Sciences, 2013. 48(1-2): p. 80-86. PMID[23111348].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23350629">In Vitro Antimicrobial Activity of Pistachio (Pistacia vera L.) Polyphenols.</a> Bisignano, C., A. Filocamo, R.M. Faulks, and G. Mandalari. FEMS Microbiology Letters, 2013. <b>[Epub ahead of print]</b>. PMID[23350629].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23335742">In Vitro Antifungal Susceptibility of Clinically Relevant Species Belonging to Aspergillus Section Flavi.</a> Goncalves, S.S., A.M. Stchigel, J. Cano, J. Guarro, and A.L. Colombo. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23335742].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23347830">Antifungal, Anti-inflammatory and Cytotoxicity Activities of Three Varieties of Labisia pumila Benth. from Microwave Obtained Extracts.</a> Karimi, E., H.Z. Jaafar, and S. Ahmad. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 20. PMID[23347830].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23320824">Evaluation of Antioxidant and Antimicrobial Activities of Essential Oils from Carum copticum Seed and Ferula assafoetida Latex.</a> Kavoosi, G., A. Tafsiry, A.A. Ebdam, and V. Rowshan. Journal of Food Science, 2013. <b>[Epub ahead of print]</b>. PMID[23320824].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23353126">Synergism of Antifungal Activity between Mitochondrial Respiration Inhibitors and Kojic acid.</a> Kim, J.H., B.C. Campbell, K.L. Chan, N. Mahoney, and R.P. Haff. Molecules, 2013. 18(2): p. 1564-1581. PMID[23353126].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23344786">Gallium(III) Complexes with 2-Acetylpyridine-derived Thiosemicarbazones: Antimicrobial and Cytotoxic Effects and Investigation on the Interactions with Tubulin.</a> Lessa, J.A., M.A. Soares, R.G. Dos Santos, I.C. Mendes, L.B. Salum, H.N. Daghestani, A.D. Andricopulo, B.W. Day, A. Vogt, and H. Beraldo. Biometals, 2013. <b>[Epub ahead of print]</b>. PMID[23344786].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23351181">Antifungal Activity of Volatile Organic Compounds from Streptomyces alboflavus Td-1.</a> Wang, C., Z. Wang, X. Qiao, Z. Li, F. Li, M. Chen, Y. Wang, Y. Huang, and H. Cui. FEMS Microbiology Letters, 2013. <b>[Epub ahead of print]</b>. PMID[23351181].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23341047">Antifungal Activity of Geldanamycin Alone or in Combination with Fluconazole against Candida Species.</a> Zhang, J., W. Liu, J. Tan, Y. Sun, Z. Wan, and R. Li. Mycopathologia, 2013. <b>[Epub ahead of print]</b>. PMID[23341047].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22257727">Pyrazinamide Susceptibility Testing in All Isolates of the Mycobacterium tuberculosis Complex - a Critical Analysis.</a> Cunha, J. Revista Portuguesa de Pneumologia, 2012. 18(4): p. 188-189. PMID[22257727].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22188377">Benzothiazinones Are Suicide Inhibitors of Mycobacterial Decaprenylphosphoryl-beta-D-ribofuranose 2&#39;-oxidase DprE1.</a> Trefzer, C., H. Skovierova, S. Buroni, A. Bobovska, S. Nenci, E. Molteni, F. Pojer, M.R. Pasca, V. Makarov, S.T. Cole, G. Riccardi, K. Mikusova, and K. Johnsson. Journal of the American Chemical Society, 2012. 134(2): p. 912-915. PMID[22188377].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22148514">Fluorocyclines. 1. 7-Fluoro-9-pyrrolidinoacetamido-6-demethyl-6-deoxytetracycline: A Potent, Broad Spectrum Antibacterial Agent.</a> Xiao, X.Y., D.K. Hunt, J. Zhou, R.B. Clark, N. Dunwoody, C. Fyfe, T.H. Grossman, W.J. O&#39;Brien, L. Plamondon, M. Ronn, C. Sun, W.Y. Zhang, and J.A. Sutcliffe. Journal of Medicinal Chemistry, 2012. 55(2): p. 597-605. PMID[22148514].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22306820">Hainanenins: A Novel Family of Antimicrobial Peptides with Strong Activity from Hainan Cascade-frog, Amolops hainanensis.</a> Zhang, S., H. Guo, F. Shi, H. Wang, L. Li, X. Jiao, Y. Wang, and H. Yu. Peptides, 2012. 33(2): p. 251-257. PMID[22306820].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23361649">The Synthesis and Antiparasitic Activity of Aryl- and Ferrocenyl-derived Thiosemicarbazone Ruthenium(II)-arene Complexes.</a> Adams, M., Y. Li, H. Khot, C. De Kock, P.J. Smith, K. Land, K. Chibale, and G.S. Smith. Dalton Transactions, 2013. <b>[Epub ahead of print]</b>. PMID[23361649].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23331618">4-Aminoquinoline-triazine Based Hybrids with Improved in-Vitro Antimalarial Activity against Cq-Sensitive and Cq-Resistant Strains of P. falciparum.</a> Manohar, S., S.I. Khan, and D.S. Rawat. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[23331618].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23301592">Aminoalkyl Derivatives of Guanidine Diaromatic Minor Groove Binders with Antiprotozoal Activity.</a> McKeever, C., M. Kaiser, and I. Rozas. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23301592].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p>   
    
    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23360309">Synthesis and in Vitro Antimalarial Testing of Neocryptolepines: SAR Study for Improved Activity by Introduction and Modifications of Side Chains at C2 and C11 on Indolo[2,3-b]quinolines.</a> Mei, Z.W., L. Wang, W.J. Lu, C.Q. Pang, T. Maeda, W. Peng, M. Kaiser, I.E. El-Sayed, and T. Inokuchi. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23360309].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23338979">Biological Assay of a Novel Quinoxalinone with Antimalarial Efficacy on Plasmodium yoelii yoelii.</a> Rivera, N., Y.M. Ponce, V.J. Aran, C. Martinez, and F. Malagon. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23338979].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23341167">Novel Type II Fatty Acid Biosynthesis (FAS II) Inhibitors as Multistage Antimalarial Agents.</a> Schrader, F.C., S. Glinca, J.M. Sattler, H.M. Dahse, G.A. Afanador, S.T. Prigge, M. Lanzer, A.K. Mueller, G. Klebe, and M. Schlitzer. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23341167].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23340660">Isolation and Synthesis of Falcitidin, a Novel Myxobacterial-derived Acyltetrapeptide with Activity against the Malaria Target Falcipain-2.</a> Somanadhan, B., S.R. Kotturi, C. Yan Leong, R.P. Glover, Y. Huang, H. Flotow, A.D. Buss, M.J. Lear, and M.S. Butler. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[23340660].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23340720">Antimalarial Activities of Medicinal Plants and Herbal Formulations Used in Thai Traditional Medicine.</a> Thiengsusuk, A., W. Chaijaroenkul, and K. Na-Bangchang. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23340720].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22310229">Structure-Activity Relationship Study of Selective Benzimidazole-based Inhibitors of Cryptosporidium parvum IMPDH.</a> Kirubakaran, S., S.K. Gorla, L. Sharling, M. Zhang, X. Liu, S.S. Ray, I.S. Macpherson, B. Striepen, L. Hedstrom, and G.D. Cuny. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(5): p. 1985-1988. PMID[22310229].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0118-013113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312481900017">New Antifungal Compounds from Aspergillus terreus Isolated from Desert Soil.</a> Awaad, A.S., A.J.A. Nabilah, and M.E. Zain. Phytotherapy Research, 2012. 26(12): p. 1872-1877. ISI[000312481900017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312603600001">Synthesis, in Vitro Antifungal Evaluation and in Silico Study of 3-Azolyl-4-chromanone phenylhydrazones.</a> Ayati, A., M. Falahati, H. Irannejad, and S. Emami. Daru-Journal of Pharmaceutical Sciences, 2012. 20. ISI[000312603600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311808100021">Essential Oil of Mitracarpus frigidus as a Potent Source of Bioactive Compounds.</a> Fabri, R.L., E.S. Coimbra, A.C. Almeida, E.P. Siqueira, T.M.A. Alves, C.L. Zani, and E. Scio. Anais da Academia Brasileira de Ciencias, 2012. 84(4): p. 1073-1080. ISI[000311808100021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312608200069">Synthesis and Antimicrobial Activity of Some New 1,3,4-Thiadiazole Derivatives.</a> Farghaly, T.A., M.A. Abdallah, and M.R.A. Aziz. Molecules, 2012. 17(12): p. 14625-14636. ISI[000312608200069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312423300017">A Microwave Promoted Solvent-free Approach to Steroidal Quinolines and Their in Vitro Evaluation for Antimicrobial Activities.</a> Gogoi, S., K. Shekarrao, A. Duarah, T.C. Bora, and R.C. Boruah. Steroids, 2012. 77(13): p. 1438-1445. ISI[000312423300017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313013600009">Photosynthesis of Dimeric Cinnamaldehyde, Eugenol, and Safrole as Antimicrobial Agents.</a> Khayyat, S.A. Journal of Saudi Chemical Society, 2013. 17(1): p. 61-65. ISI[000313013600009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312621700020">Synthesis, Characterization, Biological Evaluation and QSAR Studies of 11-p-Substituted phenyl-12-phenyl-11a,12-dihydro-11H-indeno [2,1-c] [1,5] benzothiazepines as Potential Antimicrobial Agents.</a> Mor, S., P. Pahal, and B. Narasimhan. European Journal of Medicinal Chemistry, 2012. 57: p. 196-210. ISI[000312621700020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312715100008">Prediction of Anti-tuberculosis Activity of 3-Phenyl-2H-1,3-benzoxazine-2,4(3H)-dione Derivatives.</a> Nemecek, P., J. Mocak, J. Lehotay, and K. Waisser. Chemical Papers, 2013. 67(3): p. 305-312. ISI[000312715100008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312647800017">Efficacy of Acorus calamus L. Essential Oil as a Safe Plant-based Antioxidant, Aflatoxin B-1 Suppressor and Broad Spectrum Antimicrobial against Food-Infesting Fungi.</a> Shukla, R., P. Singh, B. Prakash, and N.K. Dubey. International Journal of Food Science and Technology, 2013. 48(1): p. 128-135. ISI[000312647800017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
