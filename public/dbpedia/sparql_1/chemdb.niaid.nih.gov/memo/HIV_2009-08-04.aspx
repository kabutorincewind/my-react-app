

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-08-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MpZf7BUiLocN6os+AaIEIocQU25ff6xEIvv8eHiuCZsLsT0u7ewKbzzxbFw23gNncg9MVSBYwsxSUr55sV7belM49EBwU5P4WYkRQt2pNJ6Uc+FTjT5QoSHHdE0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="35F2B11D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  July 22 - August 4, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:       </p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19364649?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Examination of halogen substituent effects on HIV-1 integrase inhibitors derived from 2,3-dihydro-6,7-dihydroxy-1H-isoindol-1-ones and 4,5-dihydroxy-1H-isoindole-1,3(2H)-diones.</a>  Zhao XZ, Maddali K, Vu BC, Marchand C, Hughes SH, Pommier Y, Burke TR Jr.  Bioorg Med Chem Lett. 2009 May 15;19(10):2714-7. Epub 2009 Mar 28.  PMID: 19364649</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19649929?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Vicriviroc, a CCR5 receptor antagonist for the potential treatment of HIV infection.</a>  Klibanov OM.  Curr Opin Investig Drugs. 2009 Aug;10(8):845-59.  PMID: 19649929</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19645483?ordinalpos=14&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and Biological Evaluation of C-5 Methyl Substituted 4-Arylthio and 4-Aryloxy-3-Iodopyridin-2(1H)-one Type Anti-HIV Agents (dagger).</a>  Guillemont J, Benjahad A, Oumouch S, Decrane L, Palandjian P, Vernier D, Queguiner L, Andries K, de Be&#769;thune MP, Hertogs K, Grierson DS, Nguyen CH.  J Med Chem. 2009 Jul 31. [Epub ahead of print]  PMID: 19645483</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19640982?ordinalpos=23&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Small molecule targets Env for ER-associated protein degradation and inhibits HIV-1 propagation.</a>  Jejcic A, Daniels R, Goobar-Larsson L, Hebert DN, Vahlne A.  J Virol. 2009 Jul 29. [Epub ahead of print]  PMID: 19640982</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19638533?ordinalpos=26&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">An allosteric mechanism for inhibiting HIV-1 integrase with a small molecule.</a>  Kessl JJ, Eidahl JO, Shkriabai N, Zhao Z, McKee CJ, Hess S, Burke TR, Kvaratskhelia M.  Mol Pharmacol. 2009 Jul 28. [Epub ahead of print]  PMID: 19638533</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19637180?ordinalpos=27&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and Anti-HIV-1 Activity of 1-Substiuted 6-(3-Cyanobenzoyl) and [(3-Cyanophenyl)fluoromethyl]-5-ethyl-uracils.</a>  Loksha YM, Pedersen EB, Loddo R, La Colla P.  Arch Pharm (Weinheim). 2009 Jul 27. [Epub ahead of print]  PMID: 19637180</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19636442?ordinalpos=28&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A novel lectin with antiproliferative activity from the medicinal mushroom Pholiota adiposa.</a>  Zhang GQ, Sun J, Wang HX, Ng TB.  Acta Biochim Pol. 2009 Jul 28. [Epub ahead of print]  PMID: 19636442</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19630958?ordinalpos=31&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">9-aminoacridine Inhibition of HIV-1 Tat Dependent Transcription.</a>  Guendel I, Carpio L, Easley R, Van Duyne R, Coley W, Agbottah E, Dowd C, Kashanchi F, Kehn-Hall K.  Virol J. 2009 Jul 24;6(1):114. [Epub ahead of print]  PMID: 19630958</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19625465?ordinalpos=44&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Human immunodeficiency type 1 (HIV-1) protease inhibitors block the cell-to-cell HIV-1 endocytosis in dendritic cells.</a>  Muratori C, Ruggiero E, Sistigu A, Bona R, Federico M.  J Gen Virol. 2009 Jul 22. [Epub ahead of print]  PMID: 19625465</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19549583?ordinalpos=61&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vitro and in vivo pharmacokinetic characterization of two novel prodrugs of zidovudine.</a>  Quevedo MA, Briñón MC.  Antiviral Res. 2009 Aug;83(2):103-11. Epub 2009 Apr 5.  PMID: 19549583</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19549086?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Quantitative structure-activity relationship study on the anti-HIV-1 activity of novel 6-naphthylthio HEPT analogs.</a>  Riahi S, Pourbasheer E, Dinarvand R, Ganjali MR, Norouzi P.  Chem Biol Drug Des. 2009 Aug;74(2):165-72. Epub 2009 Jun 22.  PMID: 19549086</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19523819?ordinalpos=77&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">N-(4-Fluorobenzyl)-3-hydroxy-9,9-dimethyl-4-oxo-6,7,8,9-tetrahydro-4H-pyrazino[1,2-a]pyrimidine-2-carboxamides a novel class of potent HIV-1 integrase inhibitors.</a>  Petrocchi A, Jones P, Rowley M, Fiore F, Summa V.  Bioorg Med Chem Lett. 2009 Aug 1;19(15):4245-9. Epub 2009 May 29.  PMID: 19523819</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19493996?ordinalpos=88&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Combinations of the first and next generations of human immunodeficiency virus (HIV) fusion inhibitors exhibit a highly potent synergistic effect against enfuvirtide- sensitive and -resistant HIV type 1 strains.</a>  Pan C, Cai L, Lu H, Qi Z, Jiang S.  J Virol. 2009 Aug;83(16):7862-72. Epub 2009 Jun 3. PMID: 19493996</p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19445965?ordinalpos=109&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antiviral activity of indole derivatives.</a>  Giampieri M, Balbi A, Mazzei M, La Colla P, Ibba C, Loddo R.  Antiviral Res. 2009 Aug;83(2):179-85. Epub 2009 May 13.  PMID: 19445965</p>

    <p class="title">15.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19653145?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Screening of Tanzanian Medicinal Plants against Plasmodium falciparum and Human Immunodeficiency Virus.</a>  Maregesi S, Van Miert S, Pannecouque C, Feiz Haddad MH, Hermans N, Wright CW, Vlietinck AJ, Apers S, Pieters L.  Planta Med. 2009 Aug 3. [Epub ahead of print]  PMID: 19653145</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="title">16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268098300020">Retention of Metabolites of 2 &#39;,3 &#39;-Didehydro-3 &#39;-Deoxy-4 &#39;-Ethynylthymidine, a Novel Anti-Human Immunodeficiency Virus Type 1 Thymidine Analog, in Cells.</a>  Wang, X; Tanaka, H; Baba, M; Cheng, YC.  ANTIMICROBIAL AGENTS AND CHEMOTHERAPY, 2009; 53(8): 3317-3324. </p>

    <p class="title">17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268030400019">Small Molecule CCR5 and CXCR4-Based Viral Entry Inhibitors for Anti-HIV Therapy Currently in Development.</a>Kazmierski, WM; Gudmundsson, KS; Piscitelli, SC.  ANNUAL REPORTS IN MEDICINAL CHEMISTRY, 2009; 42: 301-320.</p>

    <p class="title">18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267823800218">Peptides derived from HIV-1 protease inhibit Vif activity.</a>  Wexselblatt, E; Britan-Rosich, E; Hutoran, M; Nowarski, R; Katzhendler, J; Kotler, M.  AMINO ACIDS, 2009; 37: 64. </p>

    <p class="title">19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268217000016">New indolic non-peptidic HIV protease inhibitors from (S)-glycidol: synthesis and preliminary biological activity.</a>  Chiummiento, L; Funicello, M; Lupattelli, P; Tramutola, F; Campaner, P.  TETRAHEDRON, 2009; 65(31): 5984-5989.</p>

    <p class="title">20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268234400004">SYNTHESIS AND ANTI-HIV ACTIVITY OF 4 &#39;-MODIFIED CYCLOPENTENYL PYRIMIDINE C-NUCLEOSIDES.</a>  Liu, LJ; Hong, JH.  NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS, 2009; 28(4): 303-314. </p>

    <p class="title">21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267069900155">HIV-1 assembly and its inhibition.</a>  Krausslich, HG; Bartonova, V; Carlson, LA; Muller, B; Morales, I; Grunewald, K; Briggs, J.  FEBS JOURNAL, 2009; 276: 57. </p>

    <p class="title">22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267069900210">Design, structure and activity of potent HIV protease inhibitors based on inorganic polyhedral metallacarboranes.</a>  Pokorna, J; Brynda, J; Kozisek, M; Cigler, P; Lepsik, M; Plesek, J; Gruner, B; Oberwinkler, H; Krausslich, HG; Kral, V; Konvalinka, J; Rezacova, P.  FEBS JOURNAL, 2009; 276: 78. </p>

    <p class="title">23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267069901085">Pyrophosphate analogs as inhibitors of phosphorolytic activity of HIV-1 reverse transcriptase.</a>  Yanvarev, D; Smirnova, O; Ivanov, A; Tatarintsev, A; Kukhanova, M.  FEBS JOURNAL, 2009; 276: 352-353. </p>

    <p class="title">24.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268158700007">Synthesis of 6-sulfamoyl-4-oxoquinoline-3-carboxylic acid derivatives as integrase antagonists with anti-HIV activity.</a>  Luo, ZG; Zeng, CC; Yang, LF; He, HQ; Wang, CX; Hu, LM.  CHINESE CHEMICAL LETTERS, 2009; 20(7): 789-792. </p>

    <p class="title">25.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267102500339">Anti-HIV Activity of LXR Agonists.</a>  Grant, AM; Morrow, MP; Dubrovsky, L; Kashanchi, F; Sviridov, D; Bukrinsky, M.  ARTERIOSCLEROSIS THROMBOSIS AND VASCULAR BIOLOGY, 2009; 29(7): E73. </p>

 </div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
