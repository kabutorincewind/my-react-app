

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-04-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4aVLSt0YsT0P4N8s4bg//iOeMopjrwqqCclO9el4YLdkXDQmp7KkeeYAA+gc/GpODd5z9VaBJDmw4k3Cjs3uWUYvMIuPvyp9fwmonbs7qsyQe0d7mQ3ecUSVFDo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E7EC74AF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 19 - April 1, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20334651">Inhibition of human immunodeficiency virus type-1 by cdk inhibitors.</a>  Guendel I, Agbottah ET, Kehn-Hall K, Kashanchi F.  AIDS Res Ther. 2010 Mar 24;7(1):7. [Epub ahead of print]PMID: 20334651</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20329730">Anti-AIDS Agents 81. Design, Synthesis, and Structure-Activity Relationship Study of Betulinic Acid and Moronic Acid Derivatives as Potent HIV Maturation Inhibitors (dagger).</a>  Qian K, Kuo RY, Chen CH, Huang L, Morris-Natschke SL, Lee KH.  J Med Chem. 2010 Mar 23. [Epub ahead of print]PMID: 20329730</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20308377">Telbivudine exhibits no inhibitory activity against HIV-1 clinical isolates in vitro.</a>  Lin K, Karwowska S, Lam E, Limoli K, Evans TG, Avila C.  Antimicrob Agents Chemother. 2010 Mar 22. [Epub ahead of print]PMID: 20308377</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20307579">Slow binding-tight binding interaction between benzimidazol-2-one inhibitors and HIV-1 reverse transcriptase containing the Lysine 103 to Asparagine mutation.</a>  Samuele A, Crespan E, Vitellaro S, Monforte AM, Logoteta P, Chimirri A, Maga G.  Antiviral Res. 2010 Mar 19. [Epub ahead of print]PMID: 20307579</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20307578">Lipophile-conjugated sulfated oligosaccharides as novel microbicides against HIV-1.</a>  Said J, Trybala E, Andersson E, Johnstone K, Liu L, Wimmer N, Ferro V, Bergström T.  Antiviral Res. 2010 Mar 19. [Epub ahead of print]PMID: 20307578</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20305815">Cyclic and acyclic defensins inhibit human immunodeficiency virus type-1 replication by different mechanisms.</a>  Seidel A, Ye Y, de Armas LR, Soto M, Yarosh W, Marcsisin RA, Tran D, Selsted ME, Camerini D.  PLoS One. 2010 Mar 17;5(3):e9737.PMID: 20305815</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20302887">The antiretroviral potency of emtricitabine is approximately 3-fold higher compared to lamivudine in dual human immunodeficiency virus type 1 infection/competition experiments in vitro.</a>  Drogan D, Rauch P, Hoffmann D, Walter H, Metzner KJ.  Antiviral Res. 2010 Mar 17. [Epub ahead of print]PMID: 20302887</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20300783">Protection against HIV-envelope-induced neuronal cell destruction by HIV attachment inhibitors.</a>  Zhang S, Alexander L, Wang T, Agler M, Zhou N, Fang H, Kadow J, Clapham P, Lin PF.  Arch Virol. 2010 Mar 19. [Epub ahead of print]PMID: 20300783</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20298574">Anti-infective activity of apolipoprotein domain derived peptides in vitro: identification of novel antimicrobial peptides related to apolipoprotein B with anti-HIV activity.</a>  Kelly BA, Harrison I, McKnight A, Dobson CB.  BMC Immunol. 2010 Mar 18;11(1):13. [Epub ahead of print]PMID: 20298574</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20297846">Discovery of Novel Small Molecule Orally Bioavailable C-X-C Chemokine Receptor 4 Antagonists That Are Potent Inhibitors of T-Tropic (X4) HIV-1 Replication.</a>  Skerlj RT, Bridger GJ, Kaller A, McEachern EJ, Crawford JB, Zhou Y, Atsma B, Langille J, Nan S, Veale D, Wilson T, Harwig C, Hatse S, Princen K, De Clercq E, Schols D.   J Med Chem. 2010 Mar 19. [Epub ahead of print]PMID: 20297846</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20237088">Evaluating the Substrate-Envelope Hypothesis: Structural Analysis of Novel HIV-1 Protease Inhibitors Designed to be Robust against Drug Resistance.</a>  Nalam MN, Ali A, Altman MD, Reddy GS, Chellappan S, Kairys V, Ozen A, Cao H, Gilson MK, Tidor B, Rana TM, Schiffer CA.  J Virol. 2010 Mar 17. [Epub ahead of print]PMID: 20237088</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20204192">Synthesis of nucleoside 5&#39;-O-alpha,beta-methylene-beta-triphosphates and evaluation of their potency towards inhibition of HIV-1 reverse transcriptase.</a>  Ahmadibeni Y, Dash C, Hanley MJ, Le Grice SF, Agarwal HK, Parang K.  Org Biomol Chem. 2010 Mar 21;8(6):1271-4. Epub 2010 Jan 13.PMID: 20204192</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20121237">Synthesis and Biological Evaluation of 12-Aminoacylphorboids ( parallel).</a>  Pagani A, Navarrete C, Fiebich BL, Mun&#771;oz E, Appendino G.  J Nat Prod. 2010 Mar 26;73(3):447-451.PMID: 20121237</p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20117763">Anti-HIV-1 activity of low molecular weight sulfated chitooligosaccharides.</a>  Artan M, Karadeniz F, Karagozlu MZ, Kim MM, Kim SK.  Carbohydr Res. 2010 Mar 30;345(5):656-62. Epub 2009 Dec 23.PMID: 20117763</p>

    <p class="title">15.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20085340">Inhibition of DC-SIGN-mediated HIV infection by a linear trimannoside mimic in a tetravalent presentation.</a>  Sattin S, Daghetti A, Thépaut M, Berzi A, Sánchez-Navarro M, Tabarani G, Rojo J, Fieschi F, Clerici M, Bernardi A.  ACS Chem Biol. 2010 Mar 19;5(3):301-12.PMID: 20085340</p>

    <p class="title">16.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19645484">The application of phosphoramidate protide technology to acyclovir confers anti-HIV inhibition.</a>  Derudas M, Carta D, Brancale A, Vanpouille C, Lisco A, Margolis L, Balzarini J, McGuigan C.  J Med Chem. 2009 Sep 10;52(17):5520-30.PMID: 19645484</p>

    <p class="title">17.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18779052">Acyclovir is activated into a HIV-1 reverse transcriptase inhibitor in herpesvirus-infected human tissues.</a>  Lisco A, Vanpouille C, Tchesnokov EP, Grivel JC, Biancotto A, Brichacek B, Elliott J, Fromentin E, Shattock R, Anton P, Gorelick R, Balzarini J, McGuigan C, Derudas M, Götte M, Schinazi RF, Margolis L.  Cell Host Microbe. 2008 Sep 11;4(3):260-70.PMID: 18779052</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275493400002">A Novel Use for an Old Drug: The Potential for Minocycline as Anti-HIV Adjuvant Therapy.</a>  Copeland, KFT; Brooks, JI. JOURNAL OF INFECTIOUS DISEASES, 2010; 201(8): 1115-1117.</p>

    <p>19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275388300015">Crystal Structure of HIV-1 Reverse Transcriptase Bound to a Non-Nucleoside Inhibitor with a Novel Mechanism of Action.</a>  Freisz, S; Bec, G; Radi, M; Wolff, P; Crespan, E; Angeli, L; Dumas, P; Maga, G; Botta, M; Ennifar, E.  ANGEWANDTE CHEMIE-INTERNATIONAL EDITION, 2010; 49(10): 1805-1808.</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275187000003">Synthesis of nucleoside 5 &#39;-O-alpha,beta-methylene-beta-triphosphates and evaluation of their potency towards inhibition of HIV-1 reverse transcriptase.</a>  Ahmadibeni, Y; Dash, C; Hanley, MJ; Le Grice, SFJ; Agarwal, HK; Parang, K.  ORGANIC &amp; BIOMOLECULAR CHEMISTRY, 2010; 8(6): 1271-1274.</p>

    <p>21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275161700049">Trichosanthin inhibits integration of human immunodeficiency virus type 1 through depurinating the long-terminal repeats.</a>  Zhao, WL; Feng, D; Wu, J; Sui, SF.  MOLECULAR BIOLOGY REPORTS, 2010; 37(4): 2093-2098.</p>

    <p>22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275013500011">Studies of Antiviral Activity and Cytotoxicity of Wrightia tinctoria and Morinda citrifolia.</a>  Selvam, P; Murugesh, N; Witvrouw, M; Keyaerts, E; Neyts, J.  INDIAN JOURNAL OF PHARMACEUTICAL SCIENCES, 2010; 71(6): 670-U1.</p>

    <p>23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275150300012">Peptide-Based Inhibitors of the HIV Envelope Protein and Other Class I Viral Fusion Proteins.</a>  Steffen, I; Pohlmann, S. CURRENT PHARMACEUTICAL DESIGN, 2010; 16(9): 1143-1158.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
