

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-04-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WyYfG5BML8e/MfoYrTGX2SCADZcCH4G6j9RhI2zusgd9g1f/3djPi7Mg2TGlD0NUy1yQtZs2bjRhAs46ESfWu+1hAcw8xP2t+t7dFqtCsCETW8/7nbk8w6ck1L8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CB0F8BC4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 15, 2011 - April 28, 2011</h1>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="memofmt2-h1"> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21506277">Inhibitors of the Interactions between HIV-1 in and the Cofactor Ledgf/P75.</a> De Luca, L., S. Ferro, F. Morreale, S. De Grazia, and A. Chimirri. ChemMedChem, 2011.  <b>[Epub ahead of print]</b>; PMID[21506277].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21414776">Asymmetric Total Synthesis of (+)- and (-)-Clusianone and (+)- and (-)-Clusianone Methyl Enol Ether Via Acc Alkylation and Evaluation of Their Anti-HIV Activity.</a> Garnsey, M.R., J.A. Matous, J.J. Kwiek, and D.M. Coltart. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2406-2409; PMID[21414776].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21446745">Design and Synthesis of Novel N-Hydroxy-Dihydronaphthyridinones as Potent and Orally Bioavailable HIV-1 Integrase Inhibitors.</a> Johnson, T.W., S.P. Tanis, S.L. Butler, D. Dalvie, D.M. Delisle, K.R. Dress, E.J. Flahive, Q. Hu, J.E. Kuehler, A. Kuki, W. Liu, G.A. McClellan, Q. Peng, M.B. Plewe, P.F. Richardson, G.L. Smith, J. Solowiej, K.T. Tran, H. Wang, X. Yu, J. Zhang, and H. Zhu. Journal of Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21446745].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21514616">Inhibition of Tat-Mediated HIV-1 Replication and Neurotoxicity by Novel Gsk3-Beta Inhibitors.</a> Kehn-Hall, K., I. Guendel, L. Carpio, L. Skaltsounis, L. Meijer, L. Al-Harthi, J.P. Steiner, A. Nath, O. Kutsch, and F. Kashanchi. Virology, 2011.  <b>[Epub ahead of print]</b>; PMID[21514616].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21518262">5&#39;-Phosphonate Derivatives of 2&#39;,3&#39;-Dideoxy-3&#39;-Thiacytidine as New Anti-HIV Prodrugs.</a> Khandazhinskaya, A.L., M.V. Jasko, I.L. Karpenko, P.N. Solyev, N.A. Golubeva, and M.K. Kukhanova. Chemical Biology and Drug Design, 2011.  <b>[Epub ahead of print]</b>; PMID[21518262].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21504732">Increasing Hydrophobicity of Residues in an Anti-HIV-1 Env Peptide Synergistically Improves Potency.</a> Leung, M.Y. and F.S. Cohen. Biophysical Journal, 2011. 100(8): p. 1960-1968; PMID[21504732].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21502630">A Single Polymorphism in HIV-1 Subtype C Sp1 Is Sufficient to Confer Natural Resistance to the Maturation Inhibitor, Bevirimat.</a>  Lu, W., K. Salzwedel, D. Wang, S. Chakravarty, E.O. Freed, C.T. Wild, and F. Li. Antimicrobial Agents and Chemotherapy, 2011.  <b>[Epub ahead of print]</b>; PMID[21502630].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21429747">Pentacycloundecane-Based Inhibitors of Wild-Type C-South African HIV-Protease.</a> Makatini, M.M., K. Petzold, S.N. Sriharsha, M.E. Soliman, B. Honarparvar, P.I. Arvidsson, Y. Sayed, P. Govender, G.E. Maguire, H.G. Kruger, and T. Govender. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2274-2277; PMID[21429747].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21498519">High-Throughput Screening Uncovers a Compound That Activates Latent HIV-1 and Acts Cooperatively with a Hdac Inhibitor.</a> Micheva-Viteva, S., Y. Kobayashi, L.C. Edelstein, A.L. Pacchia, H.L. Lee, J.D. Graci, J. Breslin, B.D. Phelan, L.K. Miller, J.M. Colacino, Z. Gu, Y. Ron, S.W. Peltz, and J.P. Dougherty. Journal of Biological Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21498519].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21513560">Inhibition of HIV-1 Infection by Aqueous Extracts of Prunella Vulgaris L.</a> Oh, C., J.P. Price, M.A. Brindley, M.P. Widrlechner, L. Qu, J.A. McCoy, P. Murphy, C. Hauck, and W. Maury. Virology Journal, 2011. 8(1): p. 188; PMID[21513560].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21494268">Overcoming Hurdles in Developing Successful Drugs Targeting Chemokine Receptors.</a> Schall, T.J. and A.E. Proudfoot. Nature Reviews Immunology, 2011. 11(5): p. 355-363; PMID[21494268].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21398122">Design and Synthesis of Pyridin-2-Yloxymethylpiperidin-1-Ylbutyl Amide Ccr5 Antagonists That Are Potent Inhibitors of M-Tropic (R5) HIV-1 Replication.</a> Skerlj, R., G. Bridger, Y. Zhou, E. Bourque, J. Langille, M.D. Fluri, D. Bogucki, W. Yang, T. Li, L. Wang, S. Nan, I. Baird, M. Metz, M. Darkes, J. Labrecque, G. Lau, S. Fricker, D. Huskens, and D. Schols. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2450-2455; PMID[21398122].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21392991">6-Benzoyl-3-Hydroxypyrimidine-2,4-Diones as Dual Inhibitors of HIV Reverse Transcriptase and Integrase.</a> Tang, J., K. Maddali, C.D. Dreis, Y.Y. Sham, R. Vince, Y. Pommier, and Z. Wang. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2400-2402; PMID[21392991].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21466163">The Role of Conserved Glu Residue on Cyclotide Stability and Activity: A Structural and Functional Study of Kalata B12, a Naturally Occurring Glu to Asp Mutant.</a> Wang, C.K., R.J. Clark, P.J. Harvey, K. Johan Rosengren, M. Cemazar, and D.J. Craik. Biochemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21466163].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288892000010">Sulfated Polysaccharides in Marine Sponges: Extraction Methods and Anti-HIV Activity.</a> Esteves, A.I.S., M. Nicolai, M. Humanes, and J. Goncalves. Marine Drugs, 2011. 9(1): p. 139-153; ISI[000288892000010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288900100006">Poly(HEMA-Zidovudine) Conjugate: A Macromolecular Pro-Drug for Improvement in the Biopharmaceutical Properties of the Drug.</a> Neeraj, A., M.J.N. Chandrasekar, U.V.S. Sara, and A. Rohini. Drug Delivery, 2011. 18(4): p. 272-280; ISI[000288900100006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289008400005">A Novel Metalloprotease from the Wild Basidiomycete Mushroom Lepista nuda.</a> Wu, Y.Y., H.X. Wang, and T.B. Ng. Journal of Microbiology and Biotechnology, 2011. 21(3): p. 256-262; ISI[000289008400005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289017700002">Synthesis and Biological Activity of Lamellarin Alkaloids: an Overview.</a> Fukuda, T., F. Ishibashi, and M. Iwao. Heterocycles, 2011. 83(3): p. 491-529; ISI[000289017700002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289038200007">Role of Hydroxymethyl Group as a New Hydrophilic 4 &#39;-Pocket in 5 &#39;-Norcarbocyclic Nucleoside Analogues.</a> Liu, L.J. and J.H. Hong. Bulletin of the Korean Chemical Society, 2011. 32(2): p. 411-416; ISI[000289038200007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289040100010">Carba-LNA Modified siRNAs Targeting HIV-1 TAR Region Downregulate HIV-1 Replication Successfully with Enhanced Potency.</a> Dutta, S., N. Bhaduri, N. Rastogi, S.G. Chandel, J.K. Vandavasi, R.S. Upadhayaya, and J. Chattopadhyaya. Medchemcomm, 2011. 2(3): p. 206-216; ISI[000289040100010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289048400001">Viral Surface Glycoproteins, gp120 and gp41, as Potential Drug Targets Against HIV-1: Brief Overview One Quarter of a Century Past the Approval of Zidovudine, the first anti-retroviral drug.</a> Teixeira, C., J.R.B. Gomes, P. Gomes, and F. Maurel. European Journal of Medicinal Chemistry, 2011. 46(4): p. 979-992; ISI[000289048400001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289048400043">Synthesis, Biopharmaceutical Characterization, Antimicrobial and Antioxidant Activities of 1-(4 &#39;-O-beta-D-Glucopyranosyloxy-2 &#39;-hydroxyphenyl)-3-aryl-propane-1,3-diones.</a> Sheikh, J., A. Parvez, H. Juneja, V. Ingle, Z. Chohan, M. Youssoufi, and T. Ben Hadda. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1390-1399; ISI[000289048400043].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289057800027">Novel Histone Deacetylase Inhibitor NCH-51 Activates Latent HIV-1 Gene Expression.</a> Victoriano, A.F.B., K. Imai, H. Togami, T. Ueno, K. Asamitsu, T. Suzuki, N. Miyata, K. Ochiai, and T. Okamoto. FEBS Letters, 2011. 585(7): p. 1103-1111; ISI[000289057800027].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289074700020">PentacycloundecaneBbased Inhibitors of Wild-Type C-South African HIV-Protease.</a> Makatini, M.M., K. Petzold, S.N. Sriharsha, M.E.S. Soliman, B. Honarparvar, P.I. Arvidsson, Y. Sayed, P. Govender, G.E.M. Maguire, H.G. Kruger, and T. Govender. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2274-2277; ISI[000289074700020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289074700047">6-Benzoyl-3-hydroxypyrimidine-2,4-diones as Dual Inhibitors of HIV Reverse Transcriptase and Integrase.</a> Tang, J., K. Maddali, C.D. Dreis, Y.Y. Sham, R. Vince, Y. Pommier, and Z.Q. Wang. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2400-2402; ISI[000289074700047].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289074700058">Design and Synthesis of Pyridin-2-yloxymethylpiperidin-1-ylbutyl amide CCR5 Antagonists that are Potent Inhibitors of M-Tropic (R5) HIV-1 Replication.</a> Skerlj, R., G. Bridger, Y.X. Zhou, E. Bourque, J. Langille, M. Di Fluri, D. Bogucki, W. Yang, T.S. Li, L.T. Wang, S. Nan, I. Baird, M. Metz, M. Darkes, J. Labrecque, G. Lau, S. Fricker, D. Huskens, and D. Schols. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2450-2455; ISI[000289074700058].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289166400048">Enhanced Anti-HIV-1 Activity of G-Quadruplexes Comprising Locked Nucleic Acids and Intercalating Nucleic Acids.</a> Pedersen, E.B., J.T. Nielsen, C. Nielsen, and V.V. Filichev. Nucleic Acids Research, 2011. 39(6): p. 2470-2481; ISI[000289166400048].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289215700011">RNA-Binding and Viral Reverse Transcriptase Inhibitory Activity of a Novel Cationic Diamino Acid-Based Peptide.</a> Roviello, G.N., S. Di Gaetano, D. Capasso, S. Franco, C. Crescenzo, E.M. Bucci, and C. Pedonet. Journal of Medicinal Chemistry, 2011. 54(7): p. 2095-2101; ISI[000289215700011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289215700025">3-Hydroxypyrimidine-2,4-diones as an Inhibitor Scaffold of HIV Integrase.</a> Tang, J., K. Maddali, M. Metifiot, Y.Y. Sham, R. Vince, Y. Pommier, and Z.Q. Wang. Journal of Medicinal Chemistry, 2011. 54(7): p. 2282-2292; ISI[000289215700025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289267800010">Synthesis and Preliminary Anti-HIV Activities of Andrographolide Derivatives.</a> Wang, B., L.A. Ge, W.L. Huang, H.B. Zhang, H. Qian, J. Li, and Y.T. Zheng. Medicinal Chemistry, 2010. 6(4): p. 252-258; ISI[000289267800010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288832500024">Value of Antiretroviral CNS Penetration-Effectiveness (CPE) 2010 Rankings in Predicting CSF HIV Replication According With Rate of Viral Supression in Plasma.</a> Picchi, G., M.L. Giancola, P. Lorenzini, F. Baldini, L. Monno, A. Ammassari, A.D. Monforte, P. Cinque, V. Tozzi, and A. Antinori. Infection, 2011. 39(S1): p. S20-S20; ISI[000288832500024].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288832500029">Dual/Mixed (But not X4) Tropic-HIV-1 Isolates Can Replicate in Human Primary Macrophages and are Inhibited by CCR5-Inhibitors.</a> Surdo, M., E. Balestra, P. Saccomandi, F. Di Santo, V. Svicher, C. Alteri, V. Canto, M. Pollicita, F. Scopelliti, M. Andreoni, S. Aquaro, F. Ceccherini-Silberstein, and C.F. Perno. Infection, 2011. 39(S1): p. S23-S23; ISI[000288832500029].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288832500066">Development of Recombinant Virus Assays for Phenotypic Evaluation of the Susceptibility to Integrase Inhibitors and HIV Coresceptor Tropism.</a> Saladini, F., A. Rosi, I. Vincenti, G. Meini, and M. Zazzi. Infection, 2011. 39(S1): p. S39-S40; ISI[000288832500066].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288832500172">Protease Inhibitors and HIV-Related Demyelinating Polyneuropathies: a New Possible Treatment?</a> Ceccarelli, G., A. Clemenzi, G. Antonini, C. Rizza, G. D&#39;Ettorre, and V. Vullo. Infection, 2011. 39(S1): p. S83-S83; ISI[000288832500172].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288832500003">A Glycomimetic Compound Inhibits DC-SIGN Mediated HIV Infection in Cellular and Cervical Explant Models.</a> Berzi, A., S. Sattin, J. Reina, M. Sanchez-Navarro, J. Rojo, D. Trabattoni, P. Antonazzo, I. Cetin, A. Bernardi, and M. Clerici. Infection, 2011. 39(S1): p. S11-S12; ISI[000288832500003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Patent citations</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110406&amp;CC=CN&amp;NR=102001994A&amp;KC=A">Preparation of 2(1H)-pyridinone derivatives as HIV-1 reverse transcriptase inhibitors.</a> Li, A., X. Wang, L. Ma, Y. Shao, and J. Liu. Patent. 2011. 2010-10515280 102001994.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110407&amp;CC=WO&amp;NR=2011039735A2&amp;KC=A2">Preparation of heterocyclic compounds and N-aryl urea derivatives with DDX3 inhibitory activity and their use in the treatment of neoplasm and viral infection.</a> Radi, M., M. Botta, F. Falchi, G. Maga, F. Baldanti, and S. Paolucci. Patent. 2011. 2010-IB54475 2011039735.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0415-042811.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110407&amp;CC=WO&amp;NR=2011041325A2&amp;KC=A2">Peptides derived from &#945;1-antitrypsin for the treatment of viral infection.</a>. Shapiro, L. Patent. 2011. 2010-US50581 2011041325.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0415-042811.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
