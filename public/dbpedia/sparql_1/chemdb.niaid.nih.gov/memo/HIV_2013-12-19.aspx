

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-12-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="oYQyNkeh22lHX4VepnvAYNjiss7CuDC9aMBwY/gsxldFs9u0TZ39yPG6ck0owMo48iwwo3psIga77MzNjPPZfgvsIPHqcWOSK5HqVD61Sv91ozVZOQi8cHP/fSg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F142C036" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: December 6 - December 19, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24317724">Anti-HIV-1 Activity of Trim 37.</a> Tabah, A.A., K. Tardif, and L.M. Mansky. The Journal of General Virology, 2013. <b>[Epub ahead of print]</b>. PMID[24317724].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24308675">Phenylspirodrimanes with anti-HIV Activity from the Sponge-derived Fungus Stachybotrys chartarum MXH-X73.</a> Ma, X., L. Li, T. Zhu, M. Ba, G. Li, Q. Gu, Y. Guo, and D. Li. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[24308675].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24326355">Targeting Host Nucleotide Biosynthesis with Resveratrol Inhibits Emtricitabine-resistant HIV-1.</a> Heredia, A., C. Davis, M.N. Amin, N.M. Le, M.A. Wainberg, M. Oliveira, S.G. Deeks, L.X. Wang, and R.R. Redfield. AIDS, 2013. <b>[Epub ahead of print]</b>. PMID[24326355].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24239481">Novel Piperidinylamino-diarylpyrimidine Derivatives with Dual Structural Conformations as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a>Chen, X., X. Liu, Q. Meng, D. Wang, H. Liu, E. De Clercq, C. Pannecouque, J. Balzarini, and X. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6593-6597. PMID[24239481].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1206-121913.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327110700002">Discovery of Tetrahydroisoquinoline-based CXCR4 Antagonists.</a> Truax, V.M., H.Y. Zhao, B.M. Katzman, A.R. Prosser, A.A. Alcaraz, M.T. Saindane, R.B. Howard, D. Culver, R.F. Arrendale, P.R. Gruddanti, T.J. Evers, M.G. Natchus, J.P. Snyder, D.C. Liotta, and L.J. Wilson. ACS Medicinal Chemistry Letters, 2013. 4(11): p. 1025-1030. ISI[000327110700002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326921200028">Strategic Addition of an N-linked Glycan to a Monoclonal Antibody Improves Its HIV-1-neutralizing Activity.</a> Song, R.J., D.A. Oren, D. Franco, M.S. Seaman, and D.D. Ho. Nature Biotechnology, 2013. 31(11): p. 1047-+. ISI[000326921200028].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327111100042">Clicking 3&#39;-Azidothymidine into Novel Potent Inhibitors of Human Immunodeficiency Virus.</a> Sirivolu, V.R., S.K.V. Vernekar, T. Ilina, N.S. Myshakina, M.A. Parniak, and Z.Q. Wang. Journal of Medicinal Chemistry, 2013. 56(21): p. 8765-8780. ISI[000327111100042].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327174900007">Preparation of Phosphonooxymethyl Prodrugs of HIV-1 Attachment Inhibitors.</a> Leahy, D.K. and S.K. Pack. Organic Process Research &amp; Development, 2013. 17(11): p. 1440-1444. ISI[000327174900007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327004100012">Isolation and Characterization of Bioactive Compounds from the Bark of Litsea Costalis.</a> Hosseinzadeh, M., J. Mohamad, M.A. Khalilzadeh, M.R. Zardoost, J. Haak, and M. Rajabi. Journal of Photochemistry and Photobiology B-Biology, 2013. 128: p. 85-91. ISI[000327004100012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327222400008">Arylsulfone-based HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a>Famiglini, V., A. Coluccia, A. Brancale, S. Pelliccia, G. La Regina, and R. Silvestri. Future Medicinal Chemistry, 2013. 5(18): p. 2141-2156. ISI[000327222400008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327111100029">6-(1-Benzyl-1H-pyrrol-2-yl)-2,4-dioxo-5-hexenoic acids as Dual Inhibitors of Recombinant HIV-1 Integrase and Ribonuclease H, Synthesized by a Parallel Synthesis Approach.</a> Costi, R., M. Metifiot, F. Esposito, G.C. Crucitti, L. Pescatori, A. Messore, L. Scipione, S. Tortorella, L. Zinzula, E. Novellino, Y. Pommier, E. Tramontano, C. Marchand, and R. Di Santo. Journal of Medicinal Chemistry, 2013. 56(21): p. 8588-8598. ISI[000327111100029].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327008600001">Gold- and Silver-based Nano-particles Influence Pseudo-typed Lenti-viral Infection.</a> Altunbek, M., M.E. Yalvac, K. Keseroglu, A. Palotas, M. Culha, and A.A. Rizvanov. Current Nanoscience, 2013. 9(6): p. 693-697. ISI[000327008600001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326613600028">Toward the Natural Didemnaketal A: Total Synthesis of the Isomer of Didemnaketal A.</a> Peng, L., F.M. Zhang, B.M. Yang, X.B. Zhang, W.X. Liu, S.Y. Zhang, and Y.Q. Tu. Tetrahedron Letters, 2013. 54(48): p. 6514-6516. ISI[000326613600028].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326095300020">Synthesis and Study of New Derivatives of 6-Methoxy(phenyl)methyl-2-(nitroamino)pyrimidin-4(3H)-one.</a> Novakov, I.A., B.S. Orlinson, I.Y. Kameneva, E.K. Zakharova, and M.B. Nawrozkij. Russian Chemical Bulletin, 2013. 62(1): p. 138-141. ISI[000326095300020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326766700180">A Sorghum Xylanase Inhibitor-like Protein with Highly Potent Antifungal, Antitumor and HIV-1 Reverse Transcriptase Inhibitory Activities.</a>Lin, P., J.H. Wong, T.B. Ng, V.S.M. Ho, and L.X. Xia. Food Chemistry, 2013. 141(3): p. 2916-2922. ISI[000326766700180].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />
    
    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326774300071">Picomolar Inhibitors of HIV Reverse Transcriptase Featuring Bicyclic Replacement of a Cyanovinylphenyl Group.</a> Lee, W.G., R. Gallardo-Macias, K.M. Frey, K.A. Spasov, M. Bollini, K.S. Anderson, and W.L. Jorgensen. Journal of the American Chemical Society, 2013. 135(44): p. 16705-16713. ISI[000326774300071].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324461400015">A Facile and Efficient Method for the Synthesis of N-Substituted 3-Oxoisoindoline-1-carbonitrile Derivatives Catalyzed by Sulfamic acid.</a>Hu, L.J., Z.J. Zhan, M. Lei, and L.H. Hu. Arkivoc, 2013. 2013: p. 189-198. ISI[000324461400015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326702700004">A Novel Bivalent HIV-1 Entry Inhibitor Reveals Fundamental Differences in CCR5-mu-opioid Receptor Interactions between Human Astroglia and Microglia.</a> El-Hage, N., S.M. Dever, E.M. Podhaizer, C.K. Arnatt, Y. Zhang, and K.F. Hauser. AIDS, 2013. 27(14): p. 2181-2190. ISI[000326702700004]. <b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326702700006">High Multiplicity HIV-1 Cell-to-cell Transmission from Macrophages to CD4(+) T Cells Limits Antiretroviral Efficacy.</a> Duncan, C.J.A., R.A. Russell, and Q.J. Sattentau. AIDS, 2013. 27(14): p. 2201-2206. ISI[000326702700006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326929100001">Bioactive Metabolites from Macrofungi: Ethnopharmacology, Biological Activities and Chemistry.</a> De Silva, D.D., S. Rapior, E. Sudarman, M. Stadler, J.C. Xu, S.A. Alias, and K.D. Hyde. Fungal Diversity, 2013. 62(1): p. 1-40. ISI[000326929100001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326897400008">Facile Synthesis of Derivatives of 1,1,3-Trioxo-2H,4H-pyrrolo[1,2-b][1,2,4,6]thiatriazine: A New Heterocyclic System.</a> Chen, W.M., P. Zhan, D. Rai, B.S. Huang, J.P. Yang, and X.Y. Liu. Heteroatom Chemistry, 2013. 24(6): p. 495-501. ISI[000326897400008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326319800070">Gold(I)-phosphine-N-heterocycles: Biological Activity and Specific (Ligand) Interactions on the C-terminal HIVNCp7 Zinc Finger.</a>Abbehausen, C., E.J. Peterson, R.E.F. de Paiva, P.P. Corbi, A.L.B. Formiga, Y. Qu, and N.P. Farrell. Inorganic Chemistry, 2013. 52(19): p. 11280-11287. ISI[000326319800070].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1206-121913.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
