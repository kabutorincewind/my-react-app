

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-04-25.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dVX20hrMICqsUNGbkFIWNaHiN8VGAX+rWU3CAv503G7ana1Fxxrpbx0DL1cHBse32k5O4Bg62LmBe6Z6dYlt58uDmpxbad4YuvSq38lodFNZMZln21mPGrRw8Mw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E8E15065" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: April 12 - April 25, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23590295">Extreme Multidrug Resistant HIV-1 Protease with 20 Mutations Is Resistant to Novel Protease Inhibitors with P1-pyrrolidinone or P2-tris-THF.</a> Agniswamy, J., C.H. Shen, Y.F. Wang, A.K. Ghosh, K.V. Rao, C.X. Xu, J.M. Sayer, J.M. Louis, and I.T. Weber. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23590295].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23601085">Increasing Extracellular Protein Concentration Reduces Intracellular Antiretroviral Drug Concentration and Antiviral Effect.</a> Avery, L.B., M.A. Zarr, R.P. Bakshi, R.F. Siliciano, and C.W. Hendrix. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print]</b>. PMID[23601085].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23592256">Highly Stereoselective Cyclopropanation of alpha,beta-Unsaturated Carbonyl Compounds with Methyl (Diazoacetoxy)acetate Catalyzed by a Chiral Ruthenium(II) Complex.</a> Chanthamath, S., S. Takaki, K. Shibatomi, and S. Iwasa. Angewandte Chemie, 2013. <b>[Epub ahead of print]</b>. PMID[23592256].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23602554">Phosphorylation of SAMHD1 by Cyclin A2/CDK1 Regulates Its Restriction Activity toward HIV-1.</a> Cribier, A., B. Descours, A.L. Valadao, N. Laguette, and M. Benkirane. Cell Reports, 2013. <b>[Epub ahead of print]</b>. PMID[23602554].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23598281">Formation of a Quaternary Complex of HIV-1 Reverse Transcriptase with a Nucleotide-competing Inhibitor and Its ATP Enhancer.</a> Ehteshami, M., M. Nijhuis, J.A. Bernatchez, C.J. Ablenas, S. McCormick, D. de Jong, D. Jochmans, and M. Gotte. The Journal of Biological Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23598281].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23589366">Time-dependent Interaction of Ritonavir in Chronic Use: The Power Balance between Inhibition and Induction of P-glycoprotein and Cytochrome P450 3A.</a> Fukushima, K., S. Kobuchi, K. Mizuhara, H. Aoyama, K. Takada, and N. Sugioka. Journal of Pharmaceutical Sciences, 2013. <b>[Epub ahead of print]</b>. PMID[23589366].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23006751">A 90-Day Tenofovir Reservoir Intravaginal Ring for Mucosal HIV Prophylaxis.</a> Johnson, T.J., M.R. Clark, T.H. Albright, J.S. Nebeker, A.L. Tuitupou, J.T. Clark, J. Fabian, R.T. McCabe, N. Chandra, G.F. Doncel, D.R. Friend, and P.F. Kiser. Antimicrobial Agents &amp; Chemotherapy, 2012. 56(12): p. 6272-6283. PMID[23006751].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23610442">Allosteric Integrase Inhibitor Potency Is Determined through the Inhibition of HIV-1 Particle Maturation.</a> Jurado, K.A., H. Wang, A. Slaughter, L. Feng, J.J. Kessl, Y. Koh, W. Wang, A. Ballandras-Colas, P.A. Patel, J.R. Fuchs, M. Kvaratskhelia, and A. Engelman. Proceedings of the National Academy of Sciences of the United States of America, 2013. <b>[Epub ahead of print]</b>. PMID[23610442].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23591718">Protective Effect of Vaginal Application of Neutralizing and Nonneutralizing Inhibitory Antibodies against Vaginal SHIV Challenge in Macaques.</a> Moog, C., N. Dereuddre-Bosquet, J.L. Teillaud, M.E. Biedma, V. Holl, G. Van Ham, L. Heyndrickx, A. Van Dorsselaer, D. Katinger, B. Vcelar, S. Zolla-Pazner, I. Mangeot, C. Kelly, R.J. Shattock, and R. Le Grand. Mucosal Immunology, 2013. <b>[Epub ahead of print]</b>. PMID[23591718].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23602851">A Novel anti-HIV Active Integrase Inhibitor with a Favorable in Vitro Cytochrome P450 and Uridine 5&#39;-diphospho-glucuronosyltransferase Metabolism Profile.</a> Okello, M.O., S. Mishra, M. Nishonov, M.K. Mankowski, J.D. Russell, J. Wei, P.A. Hogan, R.G. Ptak, and V. Nair. Antiviral Research, 2013. <b>[Epub ahead of print]</b>. PMID[23602851].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23602714">Saquinavir-NO Inhibits S6 Kinase Activity, Impairs Secretion of the Encephalytogenic Cytokines Interleukin-17 and Interferon-gamma and Ameliorates Experimental Autoimmune Encephalomyelitis.</a> Petkovic, F., J. Blazevski, M. Momcilovic, G. Timotijevic, M.B. Zocca, S. Mijatovic, D. Maksimovic-Ivanic, K. Mangano, P. Fagone, S. Stosic-Grujicic, F. Nicoletti, and D. Miljkovic. Journal of Neuroimmunology, 2013. <b>[Epub ahead of print]</b>. PMID[23602714].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23586711">Pyridine-substituted Desoxyritonavir Is a More Potent Inhibitor of Cytochrome P450 3A4 Than Ritonavir.</a> Sevrioukova, I.F. and T.L. Poulos. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23586711].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23600754">Microbiological Transformation of the Triterpene Nigranoic Acid by the Freshwater Fungus Dictyosporium heptasporum.</a> Sun, R., H.C. Song, Y.H. Yang, P. Yang, D.Y. Yang, K.Z. Shen, Y.B. Xu, Y.X. Gao, Y.G. Chen, and J.Y. Dong. Journal of Asian Natural Products Research, 2013. <b>[Epub ahead of print]</b>. PMID[23600754].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23443742">Towards the Enantioselective Synthesis of (-)-Euonyminol - Preparation of a Fully Functionalised Lower-rim Model.</a> Webber, M.J., S.A. Warren, D.M. Grainger, M. Weston, S. Clark, S.J. Woodhead, L. Powell, S. Stokes, A. Alanine, J.P. Stonehouse, C.S. Frampton, A.J. White, and A.C. Spivey. Organic &amp; Biomolecular Chemistry, 2013. 11(15): p. 2514-2533. PMID[23443742].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23601106">The Retroviral Restriction Ability of SAMHD1, but Not Its Deoxynucleotide Triphosphohydrolase Activity, Is Regulated by Phosphorylation.</a> White, T.E., A. Brandariz-Nunez, J.C. Valle-Casuso, S. Amie, L.A. Nguyen, B. Kim, M. Tuzova, and F. Diaz-Griffero. Cell Host &amp; Microbe, 2013. 13(4): p. 441-451. PMID[23601106].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23426366">Tetramerization of SAMHD1 Is Required for Biological Activity and Inhibition of HIV Infection.</a> Yan, J., S. Kaur, M. Delucia, C. Hao, J. Mehrens, C. Wang, M. Golczak, K. Palczewski, A.M. Gronenborn, J. Ahn, and J. Skowronski. The Journal of Biological Chemistry, 2013. 288(15): p. 10406-10417. PMID[23426366].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23494988">Sequence-controlled Multi-block Glycopolymers to Inhibit DC-SIGN-gp120 Binding.</a> Zhang, Q., J. Collins, A. Anastasaki, R. Wallis, D.A. Mitchell, C.R. Becer, and D.M. Haddleton. Angewandte Chemie, 2013. 52(16): p. 4435-4439. PMID[23494988].
    <br />
    <b>[PubMed]</b>. HIV_0412-042513.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316008300034">POM as a Quick Bioinformatic Platform to Select Flavonoids and Their Metabolites as Potential and Efficient HIV-1 Integrase Inhibitors.</a> Ben Hadda, T., T. Fergoug, I. Warad, V. Masand, and J. Sheikh. Research on Chemical Intermediates, 2013. 39(3): p. 1227-1244. ISI[000316008300034].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316516500024">Synthesis and Biological Activity of Analogues of Batzelladine F.</a> Bennett, E.L., G.P. Black, P. Browne, A. Hizi, M. Jaffar, J.P. Leyland, C. Martin, I. Oz-Gleenberg, P.J. Murphy, T.D. Roberts, A.J. Thornhill, and S.A. Vale. Tetrahedron, 2013. 69(14): p. 3061-3066. ISI[000316516500024].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316351800047">High-throughput Sequence Analysis Reveals Structural Diversity and Improved Potency among RNA Inhibitors of HIV Reverse Transcriptase.</a> Ditzler, M.A., M.J. Lange, D. Bose, C.A. Bottoms, K.F. Virkler, A.W. Sawyer, A.S. Whatley, W. Spollen, S.A. Givan, and D.H. Burke. Nucleic Acids Research, 2013. 41(3): p. 1873-1884. ISI[000316351800047].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <p class="plaintext"><br />
    21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316455200011">From the Chemistry of Epoxy-sugar Nucleosides to the Discovery of anti-HIV Agent 4 &#39;-Ethynylstavudine--Festinavir.</a> Haraguchi, K., S. Takeda, Y. Kubota, H. Kumamoto, H. Tanaka, T. Hamasaki, M. Baba, E. Paintsil, and Y.C. Cheng. Current Pharmaceutical Design, 2013. 19(10): p. 1880-1897. ISI[000316455200011].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316609900011">Synthesis and HIV-1 Inhibitory Activities of Dicaffeoyl and Digalloyl Esters of Quinic Acid Derivatives.</a> Junior, C.O.R., S.C. Verde, C.A.M. Rezende, W. Caneschi, M.R.C. Couri, B.R. McDougall, W.E. Robinson, and M.V. de Almeida. Current Medicinal Chemistry, 2013. 20(5): p. 724-733. ISI[000316609900011].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316619300007">Antiretroviral Effect of 4-Thio-uridylate against Human Immunodeficiency Virus Type 1.</a> Kanizsai, S., A. Ghidan, J. Ongradi, and K. Nagy. Acta Microbiologica Et Immunologica Hungarica, 2012. 59(4): p. 499-510. ISI[000316619300007].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316189400008">Iron-mediated Total Synthesis of 2,7-Dioxygenated Carbazole Alkaloids.</a> Krahl, M.P., O. Kataeva, A.W. Schmidt, and H.J. Knolker. European Journal of Organic Chemistry, 2013. 2013(1): p. 59-64. ISI[000316189400008].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316515900002">High Throughput LC-MS/MS Method for Simultaneous Determination of Zidovudine, Lamivudine and Nevirapine in Human Plasma.</a> Kumar, V.R., B.P.B. Reddy, B.R. Kumar, K. Sreekanth, and K.N. Babu. Journal of Chromatography B-Analytical Technologies in the Biomedical and Life Sciences, 2013. 921: p. 9-14. ISI[000316515900002].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316652900006">Sp100 Interacts with HIV-1 Integrase and Inhibits Viral Integration.</a> Lin, Y., Z.H. Li, R. Wang, W.J. Li, J.J. Wang, C.N. Ji, J.L. Xue, and J.Z. Chen. Progress in Biochemistry and Biophysics, 2013. 40(3): p. 240-248. ISI[000316652900006].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316275500016">Synthesis of Phospholipid--Ribavirin Conjugates.</a> Oleynikova, I.A., T.I. Kulak, D.A. Bolibrukh, and E.N. Kalinichenko. Helvetica Chimica Acta, 2013. 96(3): p. 463-472. ISI[000316275500016].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316610600004">Bioprocessing of Plant in Vitro Systems for the Mass Production of Pharmaceutically Important Metabolites: Paclitaxel and Its Derivatives.</a> Onrubia, M., R.M. Cusido, K. Ramirez, L. Hernandez-Vazquez, E. Moyano, M. Bonfill, and J. Palazon. Current Medicinal Chemistry, 2013. 20(7): p. 880-891. ISI[000316610600004].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316245400001">A Novel Method for Screening beta-Glucosidase Inhibitors.</a> Pandey, S., A. Sree, S.S. Dash, and D.P. Sethi. BMC Microbiology, 2013. 13. ISI[000316245400001].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316546400014">A Readily Applicable Strategy to Convert Peptides to Peptoid-based Therapeutics.</a> Park, M., M. Wetzler, T.S. Jardetzky, and A.E. Barron. Plos One, 2013. 8(3). ISI[000316546400014].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316244300009">Ionotropically Gelled Chitosan-alginate Complex Hydrogel Beads: Preparation, Characterization and in-Vitro Evaluation.</a> Patil, J.S., M.V. Kamalapur, S.C. Marapur, S.S. Shiralshetti, and D.V. Kadam. Indian Journal of Pharmaceutical Education and Research, 2012. 46(3): p. 248-252. ISI[000316244300009].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316242700016">Linear and Cyclic Glycopeptide as HIV Protease Inhibitors.</a> Pawar, S.A., A.M. Jabgunde, G.E.M. Maguire, H.G. Kruger, Y. Sayed, M.E.S. Soliman, D.D. Dhavale, and T. Govender. European Journal of Medicinal Chemistry, 2013. 60: p. 144-154. ISI[000316242700016].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316292900006">Synthesis and Crystal Structure Studies of Three N-phenylphthalimide Derivatives.</a> Rauf, M.K., R. Mushtaq, A. Badshah, R. Kingsford-Adaboh, J. Harrison, and H. Ishida. Journal of Chemical Crystallography, 2013. 43(3): p. 144-150. ISI[000316292900006].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316455200001">The Development of Novel Inhibitors for the Treatment of HIV Infection.</a> Tan, J.J. and C.X. Wang. Current Pharmaceutical Design, 2013. 19(10): p. 1765-1766. ISI[000316455200001].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316455200006">Discovery of Small Molecule Fusion Inhibitors Targeting HIV-1 gp41.</a> Zhou, G.Y. and S.D. Chu. Current Pharmaceutical Design, 2013. 19(10): p. 1818-1826. ISI[000316455200006].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316610600009">Marine Natural Products with anti-HIV Activities in the Last Decade.</a> Zhou, X.F., J. Liu, B. Yang, X.P. Lin, X.W. Yang, and Y.H. Liu. Current Medicinal Chemistry, 2013. 20(7): p. 953-973. ISI[000316610600009].
    <br />
    <b>[WOS]</b>. HIV_0412-042513.</p>

    <h2>Patent Search Citations</h2>

    <p class="plaintext">37. <a href="http://worldwide.espacenet.com/publicationDetails/originalDocument?FT=D&amp;date=20121018&amp;DB=EPODOC&amp;locale=en_EP&amp;CC=US&amp;NR=2012260922A1&amp;KC=A1&amp;ND=4">Topical Use of Hydroxytyrosol and Derivatives for the Prevention of HIV Infection.</a> Gomez-Acebo, E., J.A. Pertejo, and D.A. Calles. Patent. 2012. US 20120260922A1.
    <br />
    <b>[Espacenet]</b>. HIV_0412-042513.</p>

    <br />

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
