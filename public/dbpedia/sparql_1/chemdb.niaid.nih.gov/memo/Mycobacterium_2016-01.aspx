

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XIoRrd7fMpIk2YdNiVZ85MaBFvD/pDJ7iCW2talq5NCQ9I0I9rKBjGUXJ3lKStlb3le0UtslO4WhgDsBjV0gLNBXXMjawtXfjez4xTvUSbFNnthvOsc3JtcezUI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5B672308" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: January, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26833071">In Vitro Antibacterial Activity and in Vivo Efficacy of Hydrated Clays on Mycobacterium ulcerans Growth.</a> Adusumilli, S. and S.E. Haydel. BMC Complementary and Alternative Medicine, 2016. 16(1): p. 40. PMID[26833071].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26699807">Total Synthesis of Cyclomarin A, a Marine Cycloheptapeptide with Anti-tuberculosis and Anti-malaria Activity.</a> Barbie, P. and U. Kazmaier. Organic Letters, 2016. 18(2): p. 204-207. PMID[26699807].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26699277">Discovery of Bicyclic Inhibitors against Menaquinone Biosynthesis.</a> Choi, S.R., M.A. Larson, S.H. Hinrichs, A.M. Bartling, J. Frandsen, and P. Narayanasamy. Future Medicinal Chemistry, 2016. 8(1): p. 11-16. PMID[26699277].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26649907">Synthesis and Evaluation of Antibacterial and Antitumor Activities of New Galactopyranosylated Amino Alcohols.</a> de Souza Fernandes, F., T.S. Fernandes, L.S. da Silveira, W. Caneschi, M.C. Lourenco, C.G. Diniz, P.F. de Oliveira, P. Martins Sde, D.E. Pereira, D.C. Tavares, M. Le Hyaric, M.V. de Almeida, and M.R. Couri. European Journal of Medicinal Chemistry, 2016. 108: p. 203-210. PMID[26649907].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26562541">Hybrid Triazoles: Design and Synthesis as Potential Dual Inhibitor of Growth and Efflux Inhibition in Tuberculosis.</a> Dixit, P.P., P.P. Dixit, and S.N. Thore. European Journal of Medicinal Chemistry, 2016. 107: p. 38-47. PMID[26562541].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26779539">Update on Medicinal Plants with Potency on Mycobacterium ulcerans.</a> Fokou, P.V.T., A.K. Nyarko, R. Appiah-Opong, L.R.T. Yamthe, M. Ofosuhene, and F.F. Boyom. BioMed Research International, 2015. 917086: 16pp. PMID[26779539]. PMCID[PMC4686629].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000367621300007">Synthesis, and Antitubercular and Antimicrobial Activity of 1 &#39;-(4-Chlorophenyl)pyrazole Containing 3,5-Disubstituted Pyrazoline Derivatives.</a> Harikrishna, N., A.M. Isloor, K. Ananda, A. Obaid, and H.K. Fun. New Journal of Chemistry, 2016. 40(1): p. 73-76. ISI[000367621300007].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26675440">Identification of a Novel Class of Quinoline-oxadiazole Hybrids as Anti-tuberculosis Agents.</a> Jain, P.P., M.S. Degani, A. Raju, A. Anantram, M. Seervi, S. Sathaye, M. Ray, and M.G. Rajan. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(2): p. 645-649. PMID[26675440].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26386972">In Vitro Antituberculosis Activity of Diterpenoids from the Vietnamese Medicinal Plant Croton tonkinensis.</a> Jang, W.S., M.A. Jyoti, S. Kim, K.W. Nam, T.K. Ha, W.K. Oh, and H.Y. Song. Journal of Natural Medicines, 2016. 70(1): p. 127-132. PMID[26386972].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26642200">Identification of New Molecular Entities (NMEs) as Potential Leads against Tuberculosis from Open Source Compound Repository.</a> Kotapalli, S.S., S.S.A. Nallam, L. Nadella, T. Banerjee, H.B. Rode, P.S. Mainkar, and R. Ummanni. Plos One, 2015. 10(12): e0144018. PMID[26642200]. PMCID[PMC4671662].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26691020">In Vitro Time-kill Curves Study of Three Antituberculous Combinations against Mycobacterium tuberculosis Clinical Isolates.</a> Lopez-Gavin, A., G. Tudo, E. Rey-Jurado, A. Vergara, J.C. Hurtado, and J. Gonzalez-Martin. International Journal of Antimicrobial Agents, 2016. 47(1): p. 97-100. PMID[26691020].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26061192">5-Arylaminouracil Derivatives: New Inhibitors of Mycobacterium tuberculosis</a>. Matyugina, E., M. Novikov, D. Babkov, A. Ozerov, L. Chernousova, S. Andreevskaya, T. Smirnova, I. Karpenko, A. Chizhov, P. Murthu, S. Lutz, S. Kochetkov, K.L. Seley-Radtke, and A.L. Khandazhinskaya. Chemical Biology &amp; Drug Design, 2015. 86(6): p. 1387-1396. PMID[26061192].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26787274">Development of Acridine Derivatives as Selective Mycobacterium tuberculosis DNA Gyrase Inhibitors.</a> Medapi, B., N. Meda, P. Kulkarni, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2016. 24(4): p. 877-885. PMID[26787274].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">14. Arrival of Imidazo[2,1-b]thiazole-5-carboxyamides as Potent Inhibitors of Mycobacterium tuberculosis. Moraski, G.C. Boshoff, H. Miller, P. Cho, S. Franzblau, S.G. and M.J. Miller. Pacfichem Conference Poster, 2015.</p>

    <p class="plaintext">TB_01_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000367274300021">Spirochromone-chalcone Conjugates as Antitubercular Agents: Synthesis, Bio Evaluation and Molecular Modeling Studies.</a> Mujahid, M., P. Yogeeswari, D. Sriram, U.M.V. Basavanag, E. Diaz-Cervantes, L. Cordoba-Bahena, J. Robles, R.G. Gonnade, M. Karthikeyan, R. Vyas, and M. Muthukrishnan. RSC Advances, 2015. 5(129): p. 106448-106460. ISI[000367274300021].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26711150">Synthesis and Evaluation of Pretomanid (PA-824) Oxazolidinone Hybrids.</a> Rakesh, D.F. Bruhn, M.S. Scherman, A.P. Singh, L. Yang, J. Liu, A.J. Lenaerts, and R.E. Lee. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(2): p. 388-391. PMID[26711150]. PMCID[PMC4706794].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26642067">Release of 50 New, Drug-like Compounds and Their Computational Target Predictions for Open Source Anti-tubercular Drug Discovery.</a> Rebollo-Lopez, M.J., J. Lelievre, D. Alvarez-Gomez, J. Castro-Pichel, F. Martinez-Jimenez, G. Papadatos, V. Kumar, G. Colmenarejo, G. Mugumbate, M. Hurle, V. Barroso, R.J. Young, M. Martinez-Hoyos, R.G. del Rio, R.H. Bates, E.M. Lopez-Roman, A. Mendoza-Losana, J.R. Brown, E. Alvarez-Ruiz, M.A. Marti-Renom, J.P. Overington, N. Cammack, L. Ballell, and D. Barros-Aguire. Plos One, 2015. 10(12): e0142293. PMID[26642067]. PMCID[PMC4671658].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26642768">Synthesis and Bioactivity of Novel Triazole Incorporated Benzothiazinone Derivatives as Antitubercular and Antioxidant Agent.</a> Shaikh, M.H., D.D. Subhedar, M. Arkile, V.M. Khedkar, N. Jadhav, D. Sarkar, and B.B. Shingate. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(2): p. 561-569. PMID[26642768].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26478333">Synthesis and Characterization of the Antitubercular Phenazine Lapazine and Development of PLGA and PCL Nanoparticles for its Entrapment.</a> Silveira, N., M.M. Longuinho, S.G. Leitao, R.S. Silva, M.C. Lourenco, P.E. Silva, C. Pinto Mdo, L.G. Abracado, and P.V. Finotelli. Materials Science and Engineering C, Materials for Biological Applications, 2016. 58: p. 458-466. PMID[26478333].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26256431">Synthesis and Biological Activity of Alkynoic acids Derivatives against Mycobacteria.</a> Vilcheze, C., L.W. Leung, R. Bittman, and W.R. Jacobs, Jr. Chemistry and Physics of Lipids, 2016. 194: p. 125-138. PMID[26256431]. PMCID[PMC4718898].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_01_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160121&amp;CC=WO&amp;NR=2016008381A1&amp;KC=A1">Preparation of Pyridine Derivatives as Anti-mycobacterial Agents.</a> Ding, Z., S. Chen, and Z. Huang. Patent. 2016. 2015-CN83626 2016008381: 176pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_01_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
