

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pa7yj6BhDrQNSXpBDhqip0Q1joFKNnKAxzmQTbW14GK01+IktxbbXgUZ61mIbLUAXHs8R9/7Wis0FkHaDN5KIxZU03YvystpVb8Jz/0fbYdKGSQIYgEfxBz3fh4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9A445048" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: October, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27595420">One Pot Click Chemistry: A Three Component Reaction for the Synthesis of 2-Mercaptobenzimidazole Linked Coumarinyl Triazoles as Anti-tubercular Agents.</a> Anand, A., M.V. Kulkarni, S.D. Joshi, and S.R. Dixit. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(19): p. 4709-4713. PMID[27595420]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">2.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27716160">Extracts of Six Rubiaceae Species Combined with Rifampicin Have Good in Vitro Synergistic Antimycobacterial Activity and Good Anti-inflammatory and Antioxidant Activities.</a> Aro, A.O., J.P. Dzoyem, J.N. Eloff, and L.J. McGaw. BMC Complementary and Alternative Medicine, 2016. 16(385): 8pp. PMID[27716160]. PMCID[PMC5048625].<b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">3.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27721538">Evaluation of in Vivo Antimycobacterial Activity of Some Folklore Medicinal Plants and Enumeration of Colony Forming Unit in Murine Model.</a> Barua, A.G., H. Raj, P. Konch, P. Hussain, and C.C. Barua. Indian Journal of Pharmacology, 2016. 48(5): p. 526-530. PMID[27721538]. PMCID[PMC5051246].<b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">4.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27530867">Mycobacterial Carbonic Anhydrase Inhibition with Phenolic acids and esters: Kinetic and Computational Investigations.</a> Cau, Y., M. Mori, C.T. Supuran, and M. Botta. Organic &amp; Biomolecular Chemistry, 2016. 14(35): p. 8322-8330. PMID[27530867]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">5.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27203404">Design, Synthesis and Biological Evaluation of Novel Quinoline-based Carboxylic Hydrazides as Anti-tubercular Agents.</a> Chander, S., P. Ashok, D. Cappoen, P. Cos, and S. Murugesan. Chemical Biology &amp; Drug Design, 2016. 88(4): p. 585-591. PMID[27203404]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">6.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27628709">Synthesis of Unprecedented Sulfonylated Phosphono-exo-glycals Designed as Inhibitors of the Three Mycobacterial Galactofuranose Processing Enzymes.</a> Frederic, C.J., A. Tikad, J. Fu, W. Pan, R.B. Zheng, A. Koizumi, X. Xue, T.L. Lowary, and S.P. Vincent. Chemistry, 2016. 22(44): p. 15913-15920. PMID[27628709]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">7.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27423637">Design, Synthesis and Biological Evaluation of Novel Azaspiro Analogs of Linezolid as Antibacterial and Antitubercular Agents.</a> Gadekar, P.K., A. Roychowdhury, P.S. Kharkar, V.M. Khedkar, M. Arkile, H. Manek, D. Sarkar, R. Sharma, V. Vijayakumar, and S. Sarveswari. European Journal of Medicinal Chemistry, 2016. 122: p. 475-487. PMID[27423637]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">8.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27775177">Recent Advances and Structural Features of Enoyl-ACP Reductase Inhibitors of Mycobacterium tuberculosis.</a> Inturi, B., G.V. Pujar, and M.N. Purohit. Archiv der Pharmazie, 2016. 349(11): p. 817-826. PMID[27775177]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">9.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27371925">Engineering Another Class of Anti-tubercular Lead: Hit to Lead Optimization of an Intriguing Class of Gyrase ATPase Inhibitors.</a> Jeankumar, V.U., R.S. Reshma, R. Vats, R. Janupally, S. Saxena, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2016. 122: p. 216-231. PMID[27371925]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27371912">Synthesis, Characterization and Pharmacological Studies of Copper Complexes of Flavone Derivatives as Potential Anti-tuberculosis Agents.</a> Joseph, J., K. Nagashri, and A. Suman. Journal of Photochemistry and Photobiology B-Biology, 2016. 162: p. 125-145. PMID[27371912]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27214509">Synthesis, Characterization and Antitubercular Activities of Novel Pyrrolyl hydrazones and Their Cu-complexes.</a> Joshi, S.D., D. Kumar, S.R. Dixit, N. Tigadi, U.A. More, C. Lherbet, T.M. Aminabhavi, and K.S. Yang. European Journal of Medicinal Chemistry, 2016. 121: p. 21-39. PMID[27214509]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27801810">Novel Halogenated Pyrazine-based Chalcones as Potential Antimicrobial Drugs.</a> Kucerova-Chlupacova, M., V. Vyskovska-Tyllova, L. Richterova-Finkova, J. Kunes, V. Buchta, M. Vejsova, P. Paterova, L. Semelkova, O. Jandourek, and V. Opletalova. Molecules, 2016. 21(11). PMID[27801810]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27737555">Design, Synthesis, and Evaluation of Novel Hybrid Efflux Pump Inhibitors for Use against Mycobacterium tuberculosis.</a> Kumar, M., K. Singh, K. Naran, F. Hamzabegovic, D.F. Hoft, D.F. Warner, P. Ruminski, G. Abate, and K. Chibale. ACS Infectious Diseases, 2016. 2(10): p. 714-725. PMID[27737555]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27768711">Screening and Development of New Inhibitors of FtsZ from M. tuberculosis.</a> Mathew, B., J.V. Hobrath, L. Ross, M.C. Connelly, H. Lofton, M. Rajagopalan, R.K. Guy, and R.C. Reynolds. Plos One, 2016. 11(10): p. e0164100. PMID[27768711]. PMCID[PMC5074515].<b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27617631">Stereocontrolled Total Synthesis of Muraymycin D1 Having a Dual Mode of Action against Mycobacterium tuberculosis.</a> Mitachi, K., B.A. Aleiwi, C.M. Schneider, S. Siricilla, and M. Kurosu. Journal of the American Chemical Society, 2016. 138(39): p. 12975-12980. PMID[27617631]. PMCID[PMC5053896].<b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27530653">Fluorescence-based Assay for Polyprenyl Phosphate-GlcNAc-1-phosphate Transferase (WecA) and Identification of Novel Antimycobacterial WecA Inhibitors.</a> Mitachi, K., S. Siricilla, D. Yang, Y. Kong, K. Skorupinska-Tudek, E. Swiezewska, S.G. Franzblau, and M. Kurosu. Analytical Biochemistry, 2016. 512: p. 78-90. PMID[27530653]. PMCID[PMC5012913].<b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27656168">Fragment-based Whole Cell Screen Delivers Hits against M-tuberculosis and Non-tuberculous Mycobacteria.</a> Moreira, W., J.J. Lim, S.Y. Yeo, P.M. Ramanujulu, B.W. Dymock, and T. Dick. Frontiers in Microbiology, 2016. 7. PMID[27656168]. PMCID[PMC5013068].<b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27537519">Design, Synthesis, Structural Elucidation, Pharmacological Evaluation of Metal Complexes with Pyrazoline Derivatives.</a> Muneera, M.S. and J. Joseph. Journal of Photochemistry and Photobiology B, 2016. 163: p. 57-68. PMID[27537519]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">19.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/27498028">Protein Kinase C-delta Inhibitor, Rottlerin Inhibits Growth and Survival of Mycobacteria Exclusively through Shikimate Kinase.</a> Pandey, S., A. Chatterjee, S. Jaiswal, S. Kumar, R. Ramachandran, and K.K. Srivastava. Biochemical and Biophysical Research Communications, 2016. 478(2): p. 721-726. PMID[27498028]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">20.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/27422209">In Vitro Evaluation of Novel Inhalable Dry Powders Consisting of Thioridazine and Rifapentine for Rapid Tuberculosis Treatment.</a> Parumasivam, T., J.G. Chan, A. Pang, D.H. Quan, J.A. Triccas, W.J. Britton, and H.K. Chan. European Journal of Pharmaceutics and Biopharmaceutics, 2016. 107: p. 205-214. PMID[27422209]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000384034900008">Selective Synthesis of 10,11-Dihydrochromeno[4,3-b]chromene-6,8(7H,9H)-dione Using Copper oxide Nanoparticles for Potential Inhibitors of beta-Ketoacyl-[acyl-carrier-protein] Synthase III of Mycobacterium tuberculosis.</a> Patil, K.T., L.S. Walekar, S.S. Undare, G.B. Kolekar, M.B. Deshmukh, P.B. Choudhari, and P.V. Anbhule. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2016. 55(9): p. 1151-1159. ISI[000384034900008]. <b><br />
    [WOS].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27476117">Repurposing of a Drug Scaffold: Identification of Novel Sila Analogues of Rimonabant as Potent Antitubercular Agents.</a> Ramesh, R., R.D. Shingare, V. Kumar, A. Anand, S. B, S. Veeraraghavan, S. Viswanadha, R. Ummanni, R. Gokhale, and D. Srinivasa Reddy. European Journal of Medicinal Chemistry, 2016. 122: p. 723-730. PMID[27476117]. <b><br />
    [PubMed].</b> TB_10_2016.</p>
    

    <p class="plaintext">23.   <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000384256800016">Novel Tetrazoloquinoline-thiazolidinone Conjugates as Possible Antitubercular Agents: Synthesis and Molecular Docking.</a> Subhedar, D.D., M.H. Shaikh, B.B. Shingate, L. Nawale, D. Sarkar, and V.M. Khedkar. MedChemComm, 2016. 7(9): p. 1832-1848. ISI[000384256800016]. <b><br />
    [WOS].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">24.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/27263096">Synthetic Arabinomannan Glycolipids Impede Mycobacterial Growth, Sliding Motility and Biofilm Structure.</a> Syal, K., K. Maiti, K. Naresh, P.G. Avaji, D. Chatterji, and N. Jayaraman. Glycoconjugate Journal, 2016. 33(5): p. 763-777. PMID[27263096]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27406903">Structure Elucidation and Biological Activity of Antibacterial Compound from Micromonospora auratinigra, a Soil Actinomycetes.</a> Talukdar, M., M. Bordoloi, P.P. Dutta, S. Saikia, B. Kolita, S. Talukdar, S. Nath, A. Yadav, R. Saikia, D.K. Jha, and T.C. Bora. Journal of Applied Microbiology, 2016. 121(4): p. 973-987. PMID[27406903]. <b><br />
    [PubMed].</b> TB_10_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27776487">The COX- Inhibitor Indomethacin Reduces Th1 Effector and T Regulatory Cells in Vitro in Mycobacterium tuberculosis Infection.</a> Tonby, K., I. Wergeland, N.V. Lieske, D. Kvale, K. Tasken, and A.M. Dyrhol-Riise. BMC Infectious Diseases, 2016. 16(1): p. 599. PMID[27776487]. PMCID[PMC5078976].<b><br />
    [PubMed].</b> TB_10_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161027&amp;CC=WO&amp;NR=2016171743A1&amp;KC=A1">Inhibitors of Drug-resistant Mycobacterium.</a> Bishai, W.R., S. Lun, H. Guo, A. Kozikowski, O. Onajole, and J. Stec. Patent. 2016. 2015-US45834 2016171743: 80pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_10_2016.</p>

    <br />

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161027&amp;CC=WO&amp;NR=2016172498A1&amp;KC=A1">Substituted Benzofuran Derivatives as Novel Antimycobacterial Agents.</a> Sacchettini, J.C., A. Aggarwal, and M.K. Parai. Patent. 2016. 2016-US28867 2016172498: 113pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_10_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
