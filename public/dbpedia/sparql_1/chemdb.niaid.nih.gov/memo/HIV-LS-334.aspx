

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-334.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="rX1L5c9P6yVE8WbNm8hHPjgnak4GCyJZvitASRAvpRxVGsGHjRdiE8Ar0ch8r2+1d2UH9i3ZZvBEHpa2tGq67aVFlAraqgxBvydghOGE1Z1hT1CoearTzexsIXI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FBF7F490" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-334-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48029   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Discovery of 2,5-dimethoxy-substituted 5-bromopyridyl thiourea (PHI-236) as a potent broad-spectrum anti-human immunodeficiency virus microbicide</p>

    <p>          D&#39;Cruz, OJ and Uckun, FM</p>

    <p>          Mol Hum Reprod  <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254003&amp;dopt=abstract</a> </p><br />

    <p>2.     48030   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Maraviroc (UK-427,857), a Potent, Orally Bioavailable, and Selective Small-Molecule Inhibitor of Chemokine Receptor CCR5 with Broad-Spectrum Anti-Human Immunodeficiency Virus Type 1 Activity</p>

    <p>          Dorr, P, Westby, M, Dobbs, S, Griffin, P, Irvine, B, Macartney, M, Mori, J, Rickett, G, Smith-Burchnell, C, Napier, C, Webster, R, Armour, D, Price, D, Stammen, B, Wood, A, and Perros, M</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4721-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251317&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251317&amp;dopt=abstract</a> </p><br />

    <p>3.     48031   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          TAK-652 Inhibits CCR5-Mediated Human Immunodeficiency Virus Type 1 Infection In Vitro and Has Favorable Pharmacokinetics in Humans</p>

    <p>          Baba, M, Takashima, K, Miyake, H, Kanzaki, N, Teshima, K, Wang, X, Shiraishi, M, and Iizawa, Y </p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4584-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251299&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251299&amp;dopt=abstract</a> </p><br />

    <p>4.     48032   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Anti-Human Immunodeficiency Virus Type 1 Activity of the Nonnucleoside Reverse Transcriptase Inhibitor GW678248 in Combination with Other Antiretrovirals against Clinical Isolate Viruses and In Vitro Selection for Resistance</p>

    <p>          Hazen, RJ, Harvey, RJ, St, Clair MH, Ferris, RG, Freeman, GA, Tidwell, JH, Schaller, LT, Cowan, JR, Short, SA, Romines, KR, Chan, JH, and Boone, LR</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4465-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251284&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251284&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>5.     48033   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Rapid access to 2&#39;-branched-carbocyclic nucleosides and their 4&#39;-epimers from 2-alkyl-cyclopentene-1-ones</p>

    <p>          Meillon, JC, Griffe, L, Storer, R, and Gosselin, G</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 695-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248017&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248017&amp;dopt=abstract</a> </p><br />

    <p>6.     48034   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Novel phorbol esters exert dichotomous effects on inhibition of HIV-1 infection and activation of latent HIV-1 expression</p>

    <p>          Zhong, Y, Matsuya, Y, Nemoto, H, Mori, M, Saito, H, and Yamamoto, N</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(5): 303-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16245646&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16245646&amp;dopt=abstract</a> </p><br />

    <p>7.     48035   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          In vitro activity of SPD754, a new deoxycytidine nucleoside reverse transcriptase inhibitor (NRTI), against 215 HIV-1 isolates resistant to other NRTIs</p>

    <p>          Bethell, RC, Lie, YS, and Parkin, NT</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(5): 295-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16245645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16245645&amp;dopt=abstract</a> </p><br />

    <p>8.     48036   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus-1 (HIV-1) by beta-chemokine analogues in mononuclear cells from HIV-1-infected patients with active tuberculosis</p>

    <p>          Toossi, Z, Mayanja-Kizza, H, Baseke, J, Peters, P, Wu, M, Abraha, A, Aung, H, Okwera, A, Hirsch, C, and Arts, E</p>

    <p>          Clin Exp Immunol <b>2005</b>.  142(2): 327-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16232220&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16232220&amp;dopt=abstract</a> </p><br />

    <p>9.     48037   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          From nonpeptide toward noncarbon protease inhibitors: Metallacarboranes as specific and potent inhibitors of HIV protease</p>

    <p>          Cigler, P, Kozisek, M, Rezacova, P, Brynda, J, Otwinowski, Z, Pokorna, J, Plesek, J, Gruner, B, Doleckova-Maresova, L, Masa, M, Sedlacek, J, Bodem, J, Krausslich, HG, Kral, V, and Konvalinka, J</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.  102(43): 15394-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16227435&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16227435&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>10.   48038   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of phosphonated carbocyclic 2&#39;-oxa-3&#39;-aza-nucleosides</p>

    <p>          Chiacchio, U, Iannazzo, D, Piperno, A, Romeo, R, Romeo, G, Rescifina, A, and Saglimbeni, M</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213735&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213735&amp;dopt=abstract</a> </p><br />

    <p>11.   48039   HIV-LS-334; PUBMED-HIV-10/31/2005</p>

    <p class="memofmt1-2">          Orally bioavailable highly potent HIV protease inhibitors against PI-resistant virus</p>

    <p>          Lu, Z, Bohn, J, Rano, T, Rutkowski, CA, Simcoe, AL, Olsen, DB, Schleif, WA, Carella, A, Gabryelski, L, Jin, L, Lin, JH, Emini, E, Chapman, K, and Tata, JR</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(23): 5311-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16203148&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16203148&amp;dopt=abstract</a> </p><br />

    <p>12.   48040   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          Design of HIV-1-PR inhibitors that do not create resistance: Blocking the folding of single monomers</p>

    <p>          Broglia, RA, Tiana, G, Sutto, L, Provasi, D, and Simona, F</p>

    <p>          PROTEIN SCIENCE <b>2005</b>.  14(10): 2668-2681, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232233400017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232233400017</a> </p><br />

    <p>13.   48041   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          Advances in L-nucleosides as anti-HIV and anti-HBV agents</p>

    <p>          Dong, CH and Chang, JB</p>

    <p>          PROGRESS IN CHEMISTRY <b>2005</b>.  17(5): 916-923, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232294200021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232294200021</a> </p><br />

    <p>14.   48042   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          Hiv-1 mutagenesis during antiretroviral therapy: Implications for successful drug treatment</p>

    <p>          Chen, RX, Quinones-Mateu, ME, and Mansky, LM</p>

    <p>          FRONTIERS IN BIOSCIENCE <b>2005</b>.  10: 743-750, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232319300069">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232319300069</a> </p><br />

    <p>15.   48043   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          HIV-1 gp120 V3 loop for structure-based drug design</p>

    <p>          Sirois, S, Sing, T, and Chou, KC</p>

    <p>          CURRENT PROTEIN &amp; PEPTIDE SCIENCE <b>2005</b>.  6(5): 413-422, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232388000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232388000003</a> </p><br />

    <p>16.   48044   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel 6 &#39;(alpha)-methyl-branched Carbovir analogues</p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          ARCHIV DER PHARMAZIE <b>2005</b>.  338(9): 399-404, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232313800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232313800001</a> </p><br />

    <p>17.   48045   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          Sophorolipids, microbial glycolipids with anti-human immunodeficiency virus and sperm-immobilizing activities</p>

    <p>          Shah, V, Doncel, GF, Seyoum, T, Eaton, KM, Zalenskaya, I, Hagver, R, Azim, A, and Gross, R</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(10): 4093-4100, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700015</a> </p><br />

    <p>18.   48046   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          Newly designed six-membered azasugar nucleotide-containing phosphorothioate oligonucleotides as potent human immunodeficiency virus type 1 inhibitors</p>

    <p>          Lee, DS, Jung, KE, Yoon, CH, Lim, H, and Bae, YS</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(10): 4110-4120, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700017</a> </p><br />

    <p>19.   48047   HIV-LS-334; WOS-HIV-10/23/2005</p>

    <p class="memofmt1-2">          Pharmacokinetics of sifuvirtide, a novel anti-HIV-1 peptide, in monkeys and its inhibitory concentration in vitro</p>

    <p>          Dai, SJ, Dou, GF, Qian, XH, Song, HF, Tang, ZM, Liu, DS, Liu, XW, Yang, LM, Zheng, YT, and Liang, Q</p>

    <p>          ACTA PHARMACOLOGICA SINICA <b>2005</b>.  26(10): 1274-1280, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232346000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232346000018</a> </p><br />

    <p>20.   48048   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          Efficient and general method for the synthesis of benzopyrans by ethylenediamine diacetate-catalyzed reactions of resorcinols with alpha,beta-unsaturated aldehydes. One step synthesis of biologically active (+/-)-confluentin and (+/-)-daurichromenic acid</p>

    <p>          Lee, YR, Choi, JH, and Yoon, SH</p>

    <p>          TETRAHEDRON LETTERS <b>2005</b>.  46(44): 7539-7543, 5</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232572200016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232572200016</a> </p><br />

    <p>21.   48049   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          Antiviral potential of a new generation of acyclic nucleoside phosphonates, the 6-[2-(phosphonomethoxy)alkoxy]-2,4-diaminopyrimidines</p>

    <p>          De Clercq, E,  Andrei, G, Balzarini, J, Leyssen, P, Naesens, L, Neyts, J, Pannecouque, C, Snoeck, R, Ying, C, Hockova, D, and Holy, A</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 331-341, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500003</a> </p><br />

    <p>22.   48050   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          A new approach to the synthesis of 4 &#39;-carbon-substituted nucleosides: Development of a highly active anti-HIV agent 2 &#39;, 3 &#39;-didehydro-3 &#39;-deoxy-4 &#39;-ethynylthymidine</p>

    <p>          Haraguchi, K, Takeda, S, Sumino, M, Tanaka, H, Dutschman, GE, Cheng, YC, Nitanda, T, and Baba, M</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 343-347, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500004</a> </p><br />
    <br clear="all">

    <p>23.   48051   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          Synthesis and study of conformationally restricted 3 &#39;-deoxy-3 &#39;,4 &#39;-exo-methylene nucleoside analogues</p>

    <p>          Gagneron, J, Gosselin, G, and Mathe, C</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 383-385, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500010</a> </p><br />

    <p>24.   48052   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          Design and synthesis of specific inhibitors of the 3 &#39;-processing step of HIV-1 integrase</p>

    <p>          Chi, GC, Seo, BI, and Nair, V</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 481-484, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500030</a> </p><br />

    <p>25.   48053   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          HIV-1 inactivation by nucleic acid aptamers</p>

    <p>          Held, DM, Kissel, JD, Patterson, JT, Nickens, DG, and Burke, DH</p>

    <p>          FRONTIERS IN BIOSCIENCE <b>2006</b>.  11: 89-112, 24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232528000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232528000008</a> </p><br />

    <p>26.   48054   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          Anti-HIV agents targeting the interaction of gp120 with the cellular CD4 receptor</p>

    <p>          Vermeire, K and Schols, D</p>

    <p>          EXPERT OPINION ON INVESTIGATIONAL DRUGS <b>2005</b>.  14(10): 1199-1212, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232509700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232509700002</a> </p><br />

    <p>27.   48055   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          Development of anti-HIV agents targeting dynamic supramolecular mechanism: Entry and fusion inhibitors based on CXCR4/CCR5 antagonists and gp41-C34-remodeling peptides</p>

    <p>          Tamamura, H, Otaka, A, and Fujii, N</p>

    <p>          CURRENT HIV RESEARCH <b>2005</b>.  3(4): 289-301, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232477900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232477900001</a> </p><br />

    <p>28.   48056   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          Neamine dimers targeting the HIV-1 TAR RNA</p>

    <p>          Riguet, E, Desire, J, Boden, O, Ludwig, V, Gobel, M, Bailly, C, and Decout, JL</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(21): 4651-4655, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232465900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232465900001</a> </p><br />

    <p>29.   48057   HIV-LS-334; WOS-HIV-10/30/2005</p>

    <p class="memofmt1-2">          In vitro generation of HIV type 1 subtype C isolates resistant to enfuvirtide</p>

    <p>          Cilliers, T, Moore, P, Coetzer, M, and Morris, L</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2005</b>.  21(9): 776-783, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232528300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232528300005</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
