

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-10-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gOFWSpGNLnSLoBFen6iMq6xncPdeZtjUfCVp8Vc+ZLiJGdluAlavlcBQCmNQPmXXHWdSIs+k4gA87f10JQiPdVKKA/QrFx9dIFvMh6cBl6XA2vHgHhd/Cfby29Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3382CB7B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">

<div class="Section1">

<h1 class="memofmt2-h1">Opportunistic Infections Citation List:</h1>
<h1 class="memofmt2-h1">September 30 – October 13, 2011</h1>

<p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

<p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21987482">Synthesis and Anti-Candida Potential of Certain Novel 1-[(3-Substituted-3-phenyl)propyl]-1H-imidazoles.</a> Aboul-Enein, M.N., A.A. El-Azzouny, M.I. Attia, O.A. Saleh, and A.L. Kansoh. Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21987482].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21892787">Influence of Hyaluronic acid on Bacterial and Fungal Species, Including Clinically Relevant Opportunistic Pathogens.</a> Ardizzoni, A., R.G. Neglia, M.C. Baschieri, C. Cermelli, M. Caratozzolo, E. Righi, B. Palmieri, and E. Blasi, Journal of Materials Science. Materials in Medicine, 2011. 22(10): p. 2329-2338; PMID[21892787].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21987208">Synthesis and Biological Evaluation of Antifungal Activities of Novel 1,2-trans Glycosphingolipids.</a> Ding, N., W. Zhang, G. Lv, and Y. Li, Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21987208].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21748853">Design, Synthesis, and Biological Evaluation of 1-[(Biarylmethyl)methylamino]-2-(2,4-difluorophenyl)-3-(1H-1,2,4-triazol-1-yl)pro pan-2-ols as Potent Antifungal Agents: New Insights into Structure-Activity Relationships.</a> Guillon, R., F. Pagniez, C. Rambaud, C. Picot, M. Duflos, C. Loge, and P. Le Pape, ChemMedChem, 2011. 6(10): p. 1806-1815; PMID[21748853].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21986543">A New Chitosan-thymine Conjugate: Synthesis, Characterization and Biological Activity.</a> Kumar, S., H. Kim, M.K. Gupta, P.K. Dutta, and J. Koh, International Journal of Biological Macromolecules, 2011. <b>[Epub ahead of print]</b>; PMID[21986543].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21987259">Synthesis and Biological Activity of Ethyl 2-(substituted benzylthio)-4-(3'-(ethoxycarbonyl)biphenyl-4-yl)-6-methyl-1,4-dihydropyrimidine-5 -carboxylate Derivatives.</a> Maddila, S. and S.B. Jonnalagadda, Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21987259].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21986822">In Vitro Analyses of the Effect of Heparin and Parabens on Candida albicans Biofilms and Planktonic Cells.</a> Miceli, M.H., S.M. Bernardo, T.S. Ku, C. Walraven, and S.A. Lee, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21986822].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21975803">Antimicrobial Activity of a New Series of Benzimidazole Derivatives.</a> Ozkay, Y., Y. Tunali, H. Karaca, and I. Isikdag, Archives of Pharmacal Research, 2011. 34(9): p. 1427-1435; PMID[21975803]. </p>
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21696907">In Vitro Activity of a Novel Broad-spectrum Antifungal, E1210, Tested against Candida spp. as Determined by CLSI Broth Microdilution Method.</a> Pfaller, M.A., K. Hata, R.N. Jones, S.A. Messer, G.J. Moet, and M. Castanheira, Diagnostic Microbiology and Infectious Disease, 2011. 71(2): p. 167-170; PMID[21696907].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21899929">Synthesis and Biological Evaluation of Dihydroindeno and Indeno [1,2-e] [1,2,4]triazolo [3,4-b] [1,3,4]thiadiazines as Antimicrobial Agents.</a> Prakash, O., D.K. Aneja, K. Hussain, P. Lohan, P. Ranjan, S. Arora, C. Sharma, and K.R. Aneja, European Journal of Medicinal Chemistry, 2011. 46(10): p. 5065-5073; PMID[21899929].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21984016">Design, Synthesis and Antifungal Activity of some New Imidazole and Triazole Derivatives.</a> Rezaei, Z., S. Khabnadideh, K. Zomorodian, K. Pakshir, G. Kashi, N. Sanagoei, and S. Gholami, Archiv Der Pharmazie, 2011. 344(10): p. 658-665; PMID[21984016].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21768514">The Quorum-Sensing Molecule Farnesol Is a Modulator of Drug Efflux Mediated by ABC Multidrug Transporters and Synergizes with Drugs in Candida albicans.</a> Sharma, M. and R. Prasad, Antimicrobial Agents and Chemotherapy, 2011. 55(10): p. 4834-4843; PMID[21768514].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21917009">Antimicrobial Properties of Natural Coniferous Rosin in the European Pharmacopoeia Challenge Test.</a> Sipponen, A. and K. Laitinen, APMIS : Acta Pathologica, Microbiologica, et Immunologica Scandinavica, 2011. 119(10): p. 720-724; PMID[21917009].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21968902">Methylxanthine Inhibit Fungal Chitinases and Exhibit Antifungal Activity.</a> Tsirilakis, K., C. Kim, A.G. Vicencio, C. Andrade, A. Casadevall, and D.L. Goldman, Mycopathologia, 2011. <b>[Epub ahead of print]</b>; PMID[21968902].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  

<p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,&nbsp; Drug Resistant, MAC, MDR)</p>

<p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21855182">Synthesis of 3-Heteroarylthioquinoline Derivatives and Their in Vitro Antituberculosis and Cytotoxicity Studies.</a> Chitra, S., N. Paul, S. Muthusubramanian, P. Manisankar, P. Yogeeswari, and D. Sriram, European Journal of Medicinal Chemistry, 2011. 46(10): p. 4897-4903; PMID[21855182].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21733966">Comparison of the Bactericidal Activity of Various Fluoroquinolones against Mycobacterium tuberculosis in an in Vitro Experimental Model.</a> Cremades, R., J.C. Rodriguez, E. Garcia-Pachon, A. Galiana, M. Ruiz-Garcia, P. Lopez, and G. Royo, The Journal of Antimicrobial Chemotherapy, 2011. 66(10): p. 2281-2283; PMID[21733966].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21985673">Design and Synthesis of Conformationally Constrained Inhibitors of Non-Nucleoside Reverse Transcriptase.</a> Gomez, R., S. Jolly, T.M. Williams, J.P. Vacca, M. Torrent, G. McGaughey, M.T. Lai, P.J. Felock, V. Munshi, D. Distefano, J.A. Flynn, M.D. Miller, Y. Yan, J.C. Reid, R. Sanchez, Y. Liang, B. Paton, B.L. Wan, and N.J. Anthony, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21985673].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21989627">Synthesis and In-Vitro Antimycobacterial Activity of Fluoroquinolone Derivatives Containing a Coumarin Moiety.</a> &nbsp;Guo, Q., M.L. Liu, L.S. Feng, K. Lv, Y. Guan, H.Y. Guo, and C.L. Xiao, Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21989627].</p>   
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21880400">Design, Synthesis and Docking Studies of Quinoline-oxazolidinone Hybrid Molecules and Their Antitubercular Properties.</a> &nbsp;Thomas, K.D., A.V. Adhikari, I.H. Chowdhury, T. Sandeep, R. Mahmood, B. Bhattacharya, and E. Sumesh, European Journal of Medicinal Chemistry, 2011. 46(10): p. 4834-4845; PMID[21880400].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21846109">Synthesis and Structure-Activity Relationships of Varied Ether Linker Analogues of the Antitubercular Drug (6S)-2-Nitro-6-{[4-(trifluoromethoxy)benzyl]oxy}-6,7-dihydro-5H-imidazo[2,1-b][1, 3]oxazine (PA-824).</a> Thompson, A.M., H.S. Sutherland, B.D. Palmer, I. Kmentova, A. Blaser, S.G. Franzblau, B. Wan, Y. Wang, Z. Ma, and W.A. Denny, Journal of Medicinal Chemistry, 2011. 54(19): p. 6563-6585; PMID[21846109].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21855181">New Fluorine-containing Hydrazones Active against MDR-Tuberculosis.</a> Vavrikova, E., S. Polanc, M. Kocevar, K. Horvati, S. Bosze, J. Stolarikova, K. Vavrova, and J. Vinsova, European Journal of Medicinal chemistry, 2011. 46(10): p. 4937-4945; PMID[21855181].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  

<p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

<p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21985233">In Vitro Anti-plasmodial Activity of Dicoma anomala subsp. gerrardii (Asteraceae): Identification of Its Main Active Constituent, Structure-Activity Relationship Studies and Gene Expression Profiling.</a> Becker, J.V., M.M. van der Merwe, A.C. Van Brummelen, P. Pillay, B.G. Crampton, E.M. Mmutlane, C. Parkinson, F.R. van Heerden, N.R. Crouch, P.J. Smith, D.T. Mancama, and V.J. Maharaj, Malaria Journal, 2011. 10(1): p. 295; PMID[21985233].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21989651">Synthesis and Biological Evaluation of Some Pyrazole Derivatives as Anti-Malarial Agents.</a> Bekhit, A.A., A. Hymete, H. Asfaw, and A.E. Bekhit, Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21989651].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21977916">Antimalarial beta-Carboline and Indolactam Alkaloids from Marinactinospora thermotolerans , a Deep Sea Isolate.</a> Huang, H., Y. Yao, Z. He, T. Yang, J. Ma, X. Tian, Y. Li, C. Huang, X. Chen, W. Li, S. Zhang, C. Zhang, and J. Ju, Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21977916].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21985060">Structure and in Vitro Antiparasitic Activity of Constituents of Citropsis articulata Root Bark.</a> Lacroix, D., S. Prado, D. Kamoga, J. Kasenene, and B. Bodo, Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21985060].</p> 
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21907583">CNS and Antimalarial Activity of Synthetic Meridianin and Psammopemmin Analogs.</a> Lebar, M.D., K.N. Hahn, T. Mutka, P. Maignan, J.B. McClintock, C.D. Amsler, A. van Olphen, D.E. Kyle, and B.J. Baker, Bioorganic &amp; Medicinal Chemistry, 2011. 19(19): p. 5756-5762; PMID[21907583].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21955244">Optimization of Propafenone Analogues as Antimalarial Leads. </a>&nbsp;Lowes, D.J., W.A. Guiguemde, M.C. Connelly, F. Zhu, M.S. Sigal, J.A. Clark, A.S. Lemoff, J.L. Derisi, E.B. Wilson, and R.K. Guy, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21955244].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21968362">Cyclopropyl Carboxamides a Novel Chemical Class of Antimalarial Agents Identified in a Phenotypic Screen.</a> Sanz, L.M., M.B. Jimenez-Diaz, B. Crespo, C. De-Cozar, M.J. Almela, I. Angulo-Barturen, P. Castaneda, J. Ibanez, E.P. Fernandez, S. Ferrer, E. Herreros, S. Lozano, M.S. Martinez, L. Rueda, J.N. Burrows, J.F. Garcia-Bustos, and F.J. Gamo, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21968362].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21868222">Synthesis and in Vitro Antiprotozoal Activities of 5-Phenyliminobenzo[a]phenoxazine Derivatives.</a> Shi, X.L., J.F. Ge, B.Q. Liu, M. Kaiser, S. Wittlin, R. Brun, and M. Ihara, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(19): p. 5804-5807; PMID[21868222].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21852132">4-Thiophenoxy-2-trichloromethyquinazolines Display in Vitro Selective Antiplasmodial Activity against the Human Malaria Parasite Plasmodium falciparum.</a> Verhaeghe, P., A. Dumetre, C. Castera-Ducros, S. Hutter, M. Laget, C. Fersing, M. Prieri, J. Yzombard, F. Sifredi, S. Rault, P. Rathelot, P. Vanelle, and N. Azas, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(19): p. 6003-6006; PMID[21852132].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21875063">Quinoline Antimalarials Containing a Dibemethin Group Are Active against Chloroquinone-Resistant Plasmodium falciparum and Inhibit Chloroquine Transport via the P. falciparum Chloroquine-Resistance Transporter (PfCRT).</a> Zishiri, V.K., M.C. Joshi, R. Hunter, K. Chibale, P.J. Smith, R.L. Summers, R.E. Martin, and T.J. Egan, Journal of Medicinal Chemistry, 2011. 54(19): p. 6956-6968; PMID[21875063].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  

<p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>

<p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21641971">New Cassane Diterpenes from Caesalpinia echinata.</a> Cota, B.B., D.M. de Oliveira, E.P. de Siqueira, E.M. Souza-Fagundes, A.M. Pimenta, D.M. Santos, A. Rabello, and C.L. Zani, Fitoterapia, 2011. 82(7): p. 969-975; PMID[21641971].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21788471">In Vitro and in Vivo Studies of the Utility of Dimethyl and Diethyl carbaporphyrin ketals in Treatment of Cutaneous Leishmaniasis.</a> Taylor, V.M., D.L. Cedeno, D.L. Munoz, M.A. Jones, T.D. Lash, A.M. Young, M.H. Constantino, N. Esposito, I.D. Velez, and S.M. Robledo, Antimicrobial Agents and Chemotherapy, 2011. 55(10): p. 4755-4764; PMID[21788471].</p>  
<p class="plaintext"><b>[PubMed]</b>. OI_0930-101311.</p>  

<p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.I.</p>

<p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295040600011">Hybrid Compounds of Ent-beyerane diterpenoid isosteviol with Pyridinecarboxylic acid hydrazides. Synthesis, Structure, and Antitubercular Activity.</a> &nbsp;Andreeva, O.V., R.R. Sharipova, I.Y. Strobykina, O.A. Lodochnikova, A.B. Dobrynin, V.M. Babaev, R.V. Chestnova, V.F. Mironov, and V.E. Kataev. Russian Journal of General Chemistry, 2011. 81(8): p. 1643-1650; ISI[000295040600011].</p>  
<p class="plaintext"><b>[WOS]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>  

<p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293355700001">Byrsonima fagifolia Niedenzu Apolar Compounds with Antitubercular Activity.</a> Higuchi, C.T., M. Sannomiya, F.R. Pavan, S.R. Leite, D.N. Sato, S.G. Franzblau, L.V. Sacramento, W. Vilegas, and C.Q. Leite. Evidence-Based Complementary and Alternative Medicine, 2011. 1741-427X; ISI[000293355700001].</p> 
<p class="plaintext">OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295018700005">Detailed Comparison of Candida albicans and Candida glabrata Biofilms under Different Conditions and Their Susceptibility to Caspofungin and Anidulafungin.</a> Kucharikova, S. H. Tournu, K. Lagrou, P. Van Dijck, and H. Bujdakova. Journal of Medical Microbiology, 2011. 60(9): p. 1261-1269; ISI[000295018700005].</p> 
<p class="plaintext"><b>[WOS]</b>. OI_0930-101311.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294990900014">Design, Synthesis, and Evaluation of Thiazolidinone Derivatives as Antimicrobial and Anti-viral Agents.</a> Ravichandran, V., A. Jain, K.S. Kumar, H. Rajak, and R.K. Agrawal. Chemical Biology &amp; Drug Design, 2011. 78(3): p. 464-470; ISI[000294990900014].</p>  
<p class="plaintext"><b>[WOS]</b>. OI_0930-101311.</p> 
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294963300009">Efficacy of Zosteric Acid Sodium Salt on the Yeast Biofilm Model Candida albicans.</a> Villa, F., B. Pitts, P.S. Stewart, B. Giussani, S. Roncoroni, D. Albanese, C. Giordano, M. Tunesi, and F. Cappitelli. Microbial Ecology, 2011. 62(3): p. 584-598; ISI[000294963300009].</p>  
<p class="plaintext"><b>[WOS]</b>. OI_0930-101311.</p> 

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
