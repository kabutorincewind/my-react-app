

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="T4bFAcBOon/Yn/VP7SCLhsn4Y0DhL2VaKMowvOazDlm/1MMF2zxnQfWaa1i9FADIdk+FQDYV/mmfZyMXX7E8N2I++jlDku7Z7GlG7nmZbbLL0ssEtCISw3cqnu0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D7FEAF03" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: August, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27494027">Dual Targeting of Intracellular Pathogenic Bacteria with a Cleavable Conjugate of Kanamycin and an Antibacterial Cell-penetrating Peptide.</a> Brezden, A., M.F. Mohamed, M. Nepal, J.S. Harwood, J. Kuriakose, M.N. Seleem, and J. Chmielewski. Journal of the American Chemical Society, 2016. 138(34): p. 10945-10949. PMID[27494027]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27262564">Antimycobacterial Activity of Medicinal Plants Used by the Mayo People of Sonora, Mexico.</a> Coronado-Aceves, E.W., J.J. Sanchez-Escalante, J. Lopez-Cervantes, R.E. Robles-Zepeda, C. Velazquez, D.I. Sanchez-Machado, and A. Garibay-Escobar. Journal of Ethnopharmacology, 2016. 190: p. 106-115. PMID[27262564]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000379638600007">A Small-molecule Screening Platform for the Discovery of Inhibitors of Undecaprenyl diphosphate Synthase.</a> Czarny, T.L. and E.D. Brown. ACS Infectious Diseases, 2016. 2(7): p. 489-499. ISI[000379638600007]. <b><br />
    [WOS]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27397497">Preparation, Biological Evaluation and Molecular Docking Study of Imidazolyl dihydropyrimidines as Potential Mycobacterium tuberculosis Dihydrofolate Reductase Inhibitors.</a> Desai, N.C., A.R. Trivedi, and V.M. Khedkar. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(16): p. 4030-4035. PMID[27397497].  <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27301367">Synthesis and Antitubercular Activity of New 1,3,4-Oxadiazoles Bearing Pyridyl and Thiazolyl Scaffolds.</a> Dhumal, S.T., A.R. Deshmukh, M.R. Bhosle, V.M. Khedkar, L.U. Nawale, D. Sarkar, and R.A. Mane. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(15): p. 3646-3651. PMID[27301367]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27121350">A New Piperidinol Derivative Targeting Mycolic acid Transport in Mycobacterium abscessus.</a> Dupont, C., A. Viljoen, F. Dubar, M. Blaise, A. Bernut, A. Pawlik, C. Bouchier, R. Brosch, Y. Guerardel, J. Lelievre, L. Ballell, J.L. Herrmann, C. Biot, and L. Kremer. Molecular Microbiology, 2016. 101(3): p. 515-529. PMID[27121350]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27246555">Activity of Phosphino Palladium(II) and Platinum(II) Complexes against HIV-1 and Mycobacterium tuberculosis.</a> Gama, N.H., A.Y. Elkhadir, B.G. Gordhan, B.D. Kana, J. Darkwa, and D. Meyer. Biometals: An International Journal on the Role of Metal Ions in Biology, Biochemistry, and Medicine, 2016. 29(4): p. 637-650. PMID[27246555]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27267064">Synthesis and Antituberculosis Activity of the First Macrocyclic Glycoterpenoids Comprising Glucosamine and Diterpenoid Isosteviol.</a> Garifullin, B.F., I.Y. Strobykina, R.R. Sharipova, M.A. Kravchenko, O.V. Andreeva, O.B. Bazanova, and V.E. Kataev. Carbohydrate Research, 2016. 431: p. 15-24. PMID[27267064]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27537867">N-Alkoxyphenylhydroxynaphthalenecarboxamides and Their Antimycobacterial Activity.</a> Gonec, T., S. Pospisilova, T. Kauerova, J. Kos, J. Dohanosova, M. Oravec, P. Kollar, A. Coffey, T. Liptaj, A. Cizek, and J. Jampilek. Molecules, 2016. 21(8): E1068. PMID[27537867]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26992112">Bioassay-guided Isolation and Structural Modification of the anti-TB Resorcinols from Ardisia gigantifolia.</a> Guan, Y.F., X. Song, M.H. Qiu, S.H. Luo, B.J. Wang, N. Van Hung, N.M. Cuong, D.D. Soejarto, H.H. Fong, S.G. Franzblau, S.H. Li, Z.D. He, and H.J. Zhang. Chemical Biology &amp; Drug Design, 2016. 88(2): p. 293-301. PMID[26992112]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27580226">Propargyl-linked Antifolates Are Potent Inhibitors of Drug-sensitive and Drug-resistant Mycobacterium tuberculosis.</a> Hajian, B., E. Scocchera, S. Keshipeddy, G.D. N, C. Shoen, J. Krucinska, S. Reeve, M. Cynamon, A.C. Anderson, and D.L. Wright. Plos One, 2016. 11(8): p. e0161740. PMID[27580226]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27486307">Rational Design and Synthesis of Novel Diphenyl ether Derivatives as Antitubercular Agents.</a> Kar, S.S., V. Bhat, P.P.N. Rao, V.P. Shenoy, I. Bairy, and G.G. Shenoy. Drug Design Development and Therapy, 2016. 10: p. 2299-2310. PMID[27486307]. PMCID[PMC4958353].<b><br />
    [PubMed]</b>.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27329794">An Evolved Oxazolidinone with Selective Potency against Mycobacterium tuberculosis and Gram Positive Bacteria.</a> Kaushik, A., A.M. Heuer, D.T. Bell, J.C. Culhane, D.C. Ebner, N. Parrish, J.T. Ippoliti, and G. Lamichhane. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(15): p. 3572-3576. PMID[27329794]. PMCID[PMC4955536].<b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27467118">Total Synthesis and Stereochemical Revision of the Anti-tuberculosis Peptaibol Trichoderin A.</a> Kavianinia, I., L. Kunalingam, P.W. Harris, G.M. Cook, and M.A. Brimble. Organic Letters, 2016. 18(15): p. 3878-3881. PMID[27467118]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27118774">Unnatural Amino acid Analogues of Membrane-active Helical Peptides with Anti-mycobacterial Activity and Improved Stability.</a> Khara, J.S., M. Priestman, I. Uhia, M.S. Hamilton, N. Krishnan, Y. Wang, Y.Y. Yang, P.R. Langford, S.M. Newton, B.D. Robertson, and P.L. Ee. The Journal of Antimicrobial Chemotherapy, 2016. 71(8): p. 2181-2191. PMID[27118774]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27406794">Synthesis and Antibacterial Evaluation of Macrocyclic Diarylheptanoid Derivatives.</a> Lin, H., D.F. Bruhn, M.M. Maddox, A.P. Singh, R.E. Lee, and D. Sun. Bioorganic Medicinal Chemistry Letters, 2016. 26(16): p. 4070-4076. PMID[27406794]. PMCID[PMC4969171].<b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27529206">Synthesis and Evaluation of Bicyclo[3.1.0]hexane-based UDP-Galf Analogues as Inhibitors of the Mycobacterial Galactofuranosyltransferase GlfT2.</a> Lowary, T.L. and J. Li. Molecules, 2016. 21(8): E1053. PMID[27529206]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27563398">Synthesis and Antitubercular Activity of New Benzo[b]thiophenes.</a> Mahajan, P.S., M.D. Nikam, L.U. Nawale, V.M. Khedkar, D. Sarkar, and C.H. Gill. ACS Medicinal Chemistry Letters, 2016. 7(8): p. 751-756. PMID[27563398]. PMCID[PMC4983740].<b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000379431300016">Synthesis and Molecular Docking Studies of a New Series of Bipyrazol-yl-thiazol-ylidene-hydrazinecarbothioamide Derivatives as Potential Antitubercular Agents.</a> Mogle, P.P., R.J. Meshram, S.V. Hese, R.D. Kamble, S.S. Kamble, R.N. Gacche, and B.S. Dawane. MedChemComm, 2016. 7(7): p. 1405-1420. ISI[000379431300016]. <b><br />
    [WOS]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27346745">Design, Synthesis, and in Vitro Antituberculosis Activity of 2(5H)-Furanone Derivatives.</a> Ngwane, A.H., J.L. Panayides, F. Chouteau, L. Macingwana, A. Viljoen, B. Baker, E. Madikane, C. de Kock, L. Wiesner, K. Chibale, C.J. Parkinson, E.M. Mmutlane, P. van Helden, and I. Wiid. IUBMB Life, 2016. 68(8): p. 612-620. PMID[27346745]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27426298">Synthesis, 3D-QSAR Analysis and Biological Evaluation of Quinoxaline 1,4-di-N-oxide Derivatives as Antituberculosis Agents.</a> Pan, Y., P. Li, S. Xie, Y. Tao, D. Chen, M. Dai, H. Hao, L. Huang, Y. Wang, L. Wang, Z. Liu, and Z. Yuan. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(16): p. 4146-4153. PMID[27426298]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27278443">Design, Synthesis and Biological Evaluation of Novel 1,2,3-Triazolyl-hydroxy Alkyl/Carbazole Hybrid Molecules.</a> Rad, M.N.S., S. Behrouz, M. Behrouz, A. Sami, M. Mardkhoshnood, A. Zarenezhad, and E. Zarenezhad. Molecular Diversity, 2016. 20(3): p. 705-718. PMID[27278443]. <b><br />
    [PubMed]</b>.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27289321">Identification of Novel 2-Aminothiazole Conjugated Nitrofuran as Antitubercular and Antibacterial Agents.</a> Ran, K., C. Gao, H. Deng, Q. Lei, X. You, N. Wang, Y. Shi, Z. Liu, W. Wei, C. Peng, L. Xiong, K. Xiao, and L. Yu. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(15): p. 3669-3674. PMID[27289321]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000379368900007">Helvolic acid, a Secondary Metabolite Produced by Neosartorya spinosa KKU-1NK1 and Its Biological Activities.</a> Sanmanoch, W., W. Mongkolthanaruk, S. Kanokmedhakul, T. Aimi, and S. Boonlue. Chiang Mai Journal of Science, 2016. 43(3): p. 484-494. ISI[000379368900007]. <b><br />
    [WOS]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27320965">Fragment Discovery for the Design of Nitrogen Heterocycles as Mycobacterium Tuberculosis Dihydrofolate Reductase Inhibitors.</a> Shelke, R.U., M.S. Degani, A. Raju, M.K. Ray, and M.G. Rajan. Archiv der Pharmazie, 2016. 349(8): p. 602-613. PMID[27320965]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27501775">Inhibition of Mycobacterium Tuberculosis Dihydrodipicolinate Synthase by alpha-Ketopimelic acid and Its Other Structural Analogues.</a> Shrivastava, P., V. Navratna, Y. Silla, R.P. Dewangan, A. Pramanik, S. Chaudhary, G. Rayasam, A. Kumar, B. Gopal, and S. Ramachandran. Scientific Reports, 2016. 6: 30827. PMID[27501775]. PMCID[PMC4977564].<b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27317646">Click-based Synthesis and Antitubercular Evaluation of Dibenzofuran Tethered Thiazolyl-1,2,3-triazolyl acetamides.</a> Surineni, G., P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(15): p. 3684-3689. PMID[27317646]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27141819">Mycobacterial Antigen 85 Complex (Ag85) as a Target for Ficolins and Mannose-binding Lectin.</a> Swierzko, A.S., M.A. Bartlomiejczyk, A. Brzostek, J. Tukasiewicz, M. Michalski, J. Dziadek, and M. Cedzynski. International Journal of Medical Microbiology, 2016. 306(4): p. 212-221. PMID[27141819]. <b><br />
    [PubMed]</b>.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27338658">Synthesis and Evaluation of Antitubercular Activity of Fluorinated 5-Aryl-4-(hetero)aryl Substituted Pyrimidines.</a> Verbitskiy, E.V., S.A. Baskakova, M.A. Kravchenko, S.N. Skornyakov, G.L. Rusinov, O.N. Chupakhin, and V.N. Charushin. Bioorganic &amp; Medicinal Chemistry, 2016. 24(16): p. 3771-3780. PMID[27338658]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27324769">Wild-type and Non-wild-type Mycobacterium tuberculosis MIC Distributions for the Novel Fluoroquinolone Antofloxacin Compared with Those for Ofloxacin, Levofloxacin, and Moxifloxacin.</a> Yu, X., G. Wang, S. Chen, G. Wei, Y. Shang, L. Dong, T. Schon, D. Moradigaravand, J. Parkhill, S.J. Peacock, C.U. Koser, and H. Huang. Antimicrobial Agents and Chemotherapy, 2016. 60(9): p. 5232-5237. PMID[27324769]. <b><br />
    [PubMed]</b>. TB_08_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">31. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=WO&amp;NR=2016128948A1&amp;KC=A1">4-Substituted Benzoxaborole Compounds and Uses Thereof.</a> Alemparte-Gallardo, C., M.R.K. Alley, D. Barros-Aguirre, I. Giordano, V. Hernandez, X. Li, and J.J. Plattner. Patent. 2016. 2016-IB50775 2016128948: 110pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">32. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=WO&amp;NR=2016128949A1&amp;KC=A1">Benzoxaborole Compounds and Uses Thereof.</a> Alley, M.R.K., D. Barros-Aguirre, I. Giordano, V. Hernandez, X. Li, and J.J. Plattner. Patent. 2016. 2016-IB50776 2016128949: 152pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">33. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160803&amp;CC=EP&amp;NR=3050565A1&amp;KC=A1">Benzimidazole Sulfide Derivatives for the Treatment or Prevention of Tuberculosis.</a> Cole, S. and J.L. Rybniker. Patent. 2016. 2015-152650 3050565: 31pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160811&amp;CC=WO&amp;NR=2016127003A1&amp;KC=A1">Bortezomib as an Inhibitor of Mycobacterial Caseinolytic Protease (CLP) for Treatment of Tuberculosis.</a> Dick, T., W. Moreira, E.J. Rubin, R. Raju, and C.S.B. Chia. Patent. 2016. 2016-US16658 2016127003: 56pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_08_2016.</p>

    <br />

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160804&amp;CC=WO&amp;NR=2016119706A1&amp;KC=A1">Nitroimidazole Derivatives as Anti-pulmonary Tuberculosis Agents and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of Tuberculosis.</a> Luo, W., C.Z. Ding, and Z. Huang. Patent. 2016. 2016-CN72447 2016119706: 180pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_08_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
