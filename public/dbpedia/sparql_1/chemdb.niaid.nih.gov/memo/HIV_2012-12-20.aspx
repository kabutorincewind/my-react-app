

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-12-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vnZJNbtbr23E+fzZaPhJzoKiMCgegcu23XvObGsea/TTwOUZZyhNw0swqM6RTAk+XQtWTilToqBfY3lu6eJFjm23M4eNBAHD8HTFlckuKZ7sKnUcIoPRVq+5vBw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0069E0A6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: December 7 - December 20, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23234699">Antiviral Agents Derived from Novel 1-Adamantyl Singlet Nitrenes.</a> Antiviral Chemistry &amp; Chemotherapy, 2012. <b>[Epub ahead of print];</b> PMID[23234699].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23233535">Short-peptide Fusion Inhibitors with High Potency against Wild-type and Enfuvirtide-resistant HIV-1.</a> Chong, H., X. Yao, Z. Qiu, J. Sun, M. Zhang, S. Waltersperger, M. Wang, S.L. Liu, S. Cui, and Y. He. Federation of American Societies for Experimental Biology Journal, 2012. <b>[Epub ahead of print]</b>; PMID[23233535].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23236282">MiniCD4 Microbicide Prevents HIV Infection of Human Mucosal Explants and Vaginal Transmission of SHIV(162P3) in Cynomolgus macaques.</a> Dereuddre-Bosquet, N., L. Morellato-Castillo, J. Brouwers, P. Augustijns, K. Bouchemal, G. Ponchel, O.H. Ramos, C. Herrera, M. Stefanidou, R. Shattock, L. Heyndrickx, G. Vanham, P. Kessler, R. Le Grand, and L. Martin. PLoS Pathogens, 2012. 8(12): p. e1003071. PMID[23236282].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23231773">Identification of a 3-Aminoimidazo[1,2-a]pyridine Inhibitor of HIV-1 Reverse Transcriptase.</a> Elleder, D., T.J. Baiga, R.L. Russell, J.A. Naughton, S.H. Hughes, J.P. Noel, and J.A. Young. Virology Journal, 2012. 9(1): p. 305. PMID[23231773].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22998428">Amniotic Fluid Exhibits an Innate Inhibitory Activity Against HIV Type 1 Replication In vitro.</a> Farzin, A., P. Boyer, B. Ank, K. Nielsen-Saines, and Y. Bryson. AIDS Research Human Retroviruses, 2012. <b>[Epub ahead of print]</b>; PMID[22998428].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23221626">3-Hydroxyphthalic Anhydride-modified Human Serum Albumin as a Microbicide Candidate Inhibits HIV Infection by Blocking Viral Entry.</a> Li, L., J. Qiu, L. Lu, S. An, P. Qiao, S. Jiang, and S. Liu. J Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[23221626].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23230829">Kadcoccitones a and B, Two New 6/6/5/5-fused Tetracyclic Triterpenoids from Kadsura coccinea.</a> Liang, C.Q., Y.M. Shi, R.H. Luo, X.Y. Li, Z.H. Gao, X.N. Li, L.M. Yang, S.Z. Shang, Y. Li, Y.T. Zheng, H.B. Zhang, W.L. Xiao, and H.D. Sun. Organic Letters, 2012. <b>[Epub ahead of print]</b>; PMID[23230829].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23221980">Efficient Mucosal Transmissibility but Limited Pathogenicity of R5 SHIVSF162P3N in Chinese Origin Rhesus macaques.</a> Mumbauer, A., A. Gettie, J. Blanchard, and C. Cheng-Mayer. Journal of Acquired Immune Deficiency Syndromes, 2012. <b>[Epub ahead of print]</b>; PMID[23221980].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23093045">Design of Highly Potent HIV Fusion Inhibitors Based on Artificial Peptide Sequences.</a> Shi, W., L. Cai, L. Lu, C. Wang, K. Wang, L. Xu, S. Zhang, H. Han, X. Jiang, B. Zheng, S. Jiang, and K. Liu. Chemical Communications, 2012. 48(94): p. 11579-11581. PMID[23093045].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23122820">Inhibition of HIV-1 Capsid Assembly: Optimization of the Antiviral Potency by Site Selective Modifications at N1, C2 and C16 of a 5-(5-Furan-2-yl-pyrazol-1-yl)-1H-benzimidazole Scaffold.</a> Tremblay, M., P. Bonneau, Y. Bousquet, P. Deroy, J. Duan, M. Duplessis, A. Gagnon, M. Garneau, N. Goudreau, I. Guse, O. Hucke, S.H. Kawai, C.T. Lemke, S.W. Mason, B. Simoneau, S. Surprenant, S. Titolo, and C. Yoakim. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7512-7517. PMID[23122820].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23149229">6,7-Dihydroxy-1-oxoisoindoline-4-sulfonamide-containing HIV-1 Integrase Inhibitors.</a> Zhao, X.Z., K. Maddali, S.J. Smith, M. Metifiot, B.C. Johnson, C. Marchand, S.H. Hughes, Y. Pommier, and T.R. Burke, Jr. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7309-7313. PMID[23149229]. <b>[PubMed].</b> HIV_1207-122012.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311190000001">Discovery of Novel 2-(3-(2-Chlorophenyl)pyrazin-2-ylthio)-N-arylacetamides as Potent HIV-1 Inhibitors Using a Structure-based Bioisosterism Approach.</a> Zhan, P., W.M. Chen, Z.Y. Li, X. Li, X.W. Chen, Y. Tian, C. Pannecouque, E. De Clercq, and X.Y. Liu. Bioorganic &amp; Medicinal Chemistry, 2012. 20(23): p. 6795-6802. ISI[000311190000001].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310583100034">Structure-based Bioisosterism Design, Synthesis and Biological Evaluation of Novel 1,2,4-Triazin-6-ylthioacetamides as Potent HIV-1 NNTRIs.</a> Zhan, P., X. Li, Z.Y. Li, X.W. Chen, Y. Tian, W.M. Chen, X.Y. Liu, C. Pannecouque, and E. De Clercq. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7155-7162. ISI[000310583100034].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310583100006">POMA Analyses as New Efficient Bioinformatics&#39; Platform to Predict and Optimize Bioactivity of Synthesized 3a,4-Dihydro-3H-indeno[1,2-c]pyrazole-2-carboxamide/carbothioamide Analogues.</a> Ahsan, M.J., J. Govindasamy, H. Khalilullah, G. Mohan, J.P. Stables, C. Pannecouque, and E. De Clercq. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7029-7035. ISI[000310583100006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310754100011">The Nonnucleoside Reverse Transcription Inhibitor MIV-160 Delivered from an Intravaginal Ring, but Not from a Carrageenan Gel, Protects against Simian/Human Immunodeficiency Virus-RT Infection.</a> Aravantinou, M., R. Singer, N. Derby, G. Calenda, P. Mawson, C.J. Abraham, R. Menon, S. Seidor, D. Goldman, J. Kenney, G. Villegas, A. Gettie, J. Blanchard, J.D. Lifson, M. Piatak, J.A. Fernandez-Romero, T.M. Zydowsky, N. Teleshova, and M. Robbiani. AIDS Research and Human Retroviruses, 2012. 28(11): p. 1467-1475. ISI[000310754100011].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310574200024">Sphingopeptides: Dihydrosphingosine-based Fusion Inhibitors against Wild-type and Enfuvirtide-resistant HIV-1.</a> Ashkenazi, A., M. Viard, L. Unger, R. Blumenthal, and Y. Shai. Federation of American Societies for Experimental Biology Journal, 2012. 26(11): p. 4628-4636. ISI[000310574200024].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310771200120">Design and Docking Studies of Some Novel 1-Phenyl-2,3,4,9-tetrahydro-1h-pyrido[3,4-b]Indole-3-carboxylic Acids as Inhibitors of Human Immunodeficiency Virus Type-1 Reverse Transcriptase.</a> Ashok, P., K.K. Babu, S. Murugesan, and S. Ganguly. Asian Journal of Chemistry, 2012. 24(12): p. 5857-5860. ISI[000310771200120].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310765400008">Platinum(II) and Gold(I) Complexes Based on 1,1&#39;-bis(Diphenylphosphino)metallocene Derivatives: Synthesis, Characterization and Biological Activity of the Gold Complexes.</a> Bjelosevic, H., I.A. Guzei, L.C. Spencer, T. Persson, F.H. Kriel, R. Hewer, M.J. Nell, J. Gut, C.E.J. van Rensburg, P.J. Rosenthal, J. Coates, J. Darkwa, and S.K.C. Elmroth. Journal of Organometallic Chemistry, 2012. 720: p. 52-59. ISI[000310765400008].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310585300006">Ultrasensitive Allele-specific PCR Reveals Rare Preexisting Drug-resistant Variants and a Large Replicating Virus Population in macaques Infected with a Simian Immunodeficiency Virus Containing Human Immunodeficiency Virus Reverse Transcriptase.</a> Boltz, V.F., Z. Ambrose, M.F. Kearney, W. Shao, V.N. KewalRamani, F. Maldarelli, J.W. Mellors, and J.M. Coffin. Journal of Virology, 2012. 86(23): p. 12525-12530. ISI[000310585300006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310583100008">Synthetic Bicyclic Iminosugar Derivatives Fused Thiazolidin-4-one as New Potential HIV-RT Inhibitors.</a> Chen, H., T.Y. Yang, S.N. Wei, H.Z. Zhang, R. Li, Z.B. Qin, and X.L. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7041-7044. ISI[000310583100008].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310353600022">Anti-HIV-1 Activity of Resveratrol Derivatives and Synergistic Inhibition of HIV-1 by the Combination of Resveratrol and Decitabine.</a> Clouser, C.L., J. Chauhan, M.A. Bess, J.L. van Oploo, D. Zhou, S. Dimick-Gray, L.M. Mansky, and S.E. Patterson. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(21): p. 6642-6646. ISI[000310353600022].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310750800002">Long-acting Nanoformulated Antiretroviral Therapy Elicits Potent Antiretroviral and Neuroprotective Responses in HIV-1-Infected Humanized Mice.</a> Dash, P.K., H.E. Gendelman, U. Roy, S. Balkundi, Y. Alnouti, R.L. Mosley, H.A. Gelbard, J. McMillan, S. Gorantla, and L.Y. Poluektova. AIDS, 2012. 26(17): p. 2135-2144. ISI[000310750800002].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310364400357">Membrane-anchored and Secreted C Peptides as HIV Entry Inhibitors.</a> Egerer, L., J. Kimpel, K. Schmidt, F. Brauer, A. Volk, and D. von Laer. Human Gene Therapy, 2012. 23(10): p. A118-A119. ISI[000310364400357].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311043700006">New Anthraquinone Derivatives as Inhibitors of the HIV-1 Reverse Transcriptase-associated Ribonuclease H Function.</a> Esposito, F., A. Corona, L. Zinzula, T. Kharlamova, and E. Tramontano. Chemotherapy, 2012. 58(4): p. 299-307. ISI[000311043700006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310583100025">Structural Modifications of 5,6-Dihydroxypyrimidines with anti-HIV Activity.</a> Guo, D.L., X.J. Zhang, R.R. Wang, Y. Zhou, Z. Li, J.Y. Xu, K.X. Chen, Y.T. Zheng, and H. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7114-7118. ISI[000310583100025].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311400100004">Identification of Old Drugs as Potential Inhibitors of HIV-1 Integrase-Human Ledgf/P75 Interaction Via Molecular Docking.</a> Hu, G.P., X. Li, X.Q. Sun, W.Q. Lu, G.X. Liu, J. Huang, X. Shen, and Y. Tang. Journal of Molecular Modeling, 2012. 18(12): p. 4995-5003. ISI[000311400100004].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311083200007">Characterization of Peripheral and Mucosal Immune Responses in Rhesus macaques on Long-term Tenofovir and Emtricitabine Combination Antiretroviral Therapy.</a> Jasny, E., S. Geer, I. Frank, P. Vagenas, M. Aravantinou, A.M. Salazar, J.D. Lifson, M. Piatak, A. Gettie, J.L. Blanchard, and M. Robbiani. Journal of Acquired Immune Deficiency Syndromes, 2012. 61(4): p. 425-435. ISI[000311083200007].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310368700358">Novel anti-HIV-1 Activity Produced by Conjugating Unsulfated Dextran with Poly-L-lysine and Characterization of the Unique Antiviral Mechanism.</a> Kano, F., T. Otsuki, K. Nakamura, H. Mori, H. Hoshino, H. Sakagami, and H. Ogawa. Glycobiology, 2012. 22(11): p. 1638-1639. ISI[000310368700358].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310754100012">A Single Dose of a MIV-150/Zinc Acetate Gel Provides 24 H of Protection against Vaginal Simian Human Immunodeficiency Virus Reverse Transcriptase Infection, with More Limited Protection Rectally 8-24 H after Gel Use.</a> Kenney, J., R. Singer, N. Derby, M. Aravantinou, C.J. Abraham, R. Menon, S. Seidor, S.M. Zhang, A. Gettie, J. Blanchard, M. Piatak, J.D. Lifson, J.A. Fernandez-Romero, T.M. Zydowsky, and M. Robbiani. AIDS Research and Human Retroviruses, 2012. 28(11): p. 1476-1484. ISI[000310754100012].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311190000012">Synthesis, Evaluation of anti-HIV-1 and anti-HCV Activity of Novel 2&#39;,3&#39;-Dideoxy-2&#39;,2&#39;-difluoro-4&#39;-azanucleosides.</a> Martinez-Montero, S., S. Fernandez, Y.S. Sanghvi, E.A. Theodorakis, M.A. Detorio, T.R. McBrayer, T. Whitaker, R.F. Schinazi, V. Gotor, and M. Ferrero. Bioorganic &amp; Medicinal Chemistry, 2012. 20(23): p. 6885-6893. ISI[000311190000012].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310364400356">Expression of a Fusion Inhibitor and a CCR5 Agonist by Lentiviral Vectors Confers Protection against HIV Infection in vitro: Assessment of Efficacy In vivo in a New Humanized Mice Model of HIV Infection.</a> Nicolas, P., L. Sidonie, B. Aude, D. Karim, M. Anne-Genevieve, G. Guy, and M. Gilles. Human Gene Therapy, 2012. 23(10): p. A118-A118. ISI[000310364400356].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310368700339">Human Milk Glycosaminoglycans Inhibit HIV-1 Primary Isolates In vitro Infection</a>. Rosas-Alquicira, G., I. Acosta-Blanco, S. Ortega-Francisco, L. Fuentes-Romero, D.S. Newburg, G.M. Ruiz-Palacios, Y. Mao, J. Zaia, and M. Viveros-Rogel. Glycobiology, 2012. 22(11): p. 1632-1632. ISI[000310368700339].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308675800019">Noncyclam Tetraamines Inhibit CXC Chemokine Receptor Type 4 and Target Glioma-initiating Cells.</a> Ros-Blanco, L., J. Anido, R. Bosser, J. Este, B. Clotet, A. Kosoy, L. Ruiz-Avila, J. Teixido, J. Seoane, and J.I. Borrell. Journal of Medicinal Chemistry, 2012. 55(17): p. 7560-7570. ISI[000308675800019].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1207-122012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
