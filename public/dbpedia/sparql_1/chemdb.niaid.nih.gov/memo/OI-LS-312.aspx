

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-312.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="UsoIi3XD607Kg56nbRtTJ6Mrn+PK327Xx7xi63L9LOb2/hdsX2kU2UXB7u0vQTgBozIU9mp3KQBDAFtVjg6GhyofYRTP4Kgo1jaco2lU6V43/qz5Kr8QXfw7CM8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="99FC519C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-312-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44587   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          Replication Dynamics of Mycobacterium tuberculosis in Chronically Infected Mice</p>

    <p>          Munoz-Elias, EJ, Timm, J, Botha, T, Chan, WT, Gomez, JE, and McKinney, JD</p>

    <p>          Infect Immun <b>2005</b>.  73(1): 546-551</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15618194&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15618194&amp;dopt=abstract</a> </p><br />

    <p>2.         44588   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          The search for new antifungal drugs and strategies continues</p>

    <p>          Jack, DB</p>

    <p>          Drug News Perspect <b>1998</b>.  11(5): 306-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616651&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616651&amp;dopt=abstract</a> </p><br />

    <p>3.         44589   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p><b>          Non-Nucleoside Benzimidazole-Based Allosteric Inhibitors of the Hepatitis C Virus NS5B Polymerase: Inhibition of Subgenomic Hepatitis C Virus RNA Replicons in Huh-7 Cells</b> </p>

    <p>          Beaulieu, PL, Bousquet, Y, Gauthier, J, Gillard, J, Marquis, M, McKercher, G, Pellerin, C, Valois, S, and Kukolj, G</p>

    <p>          J Med Chem <b>2004</b>.  47(27): 6884-92</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615537&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615537&amp;dopt=abstract</a> </p><br />

    <p>4.         44590   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Synthesis, surface active and antimicrobial properties of new alkyl 2,6-dideoxy-L-arabino-hexopyranosides</p>

    <p>          Rauter, Amelia P, Lucas, Susana, Almeida, Tania, Sacoto, Diana, Ribeiro, Veronica, Justino, Jorge, Neves, Ana, Silva, Filipa VM, Oliveira, Maria C, and Ferreira, Maria J</p>

    <p>          Carbohydrate Research <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TFF-4F3NXXJ-1/2/3dd1cc7e1df23c3c5c76d7cf4437241a">http://www.sciencedirect.com/science/article/B6TFF-4F3NXXJ-1/2/3dd1cc7e1df23c3c5c76d7cf4437241a</a> </p><br />

    <p>5.         44591   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Inhibition of the Carpobrotus edulis methanol extract on the growth of phagocytosed multidrug-resistant Mycobacterium tuberculosis and methicillin-resistant Staphylococcus aureus</p>

    <p>          Martins, Marta, Ordway, Diane, Kristiansen, Malthe, Viveiros, Miguel, Leandro, Clara, Molnar, Joseph, and Amaral, Leonard</p>

    <p>          Fitoterapia <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VSC-4F1SVTP-2/2/78b5823271285ca5567dca045c3a09ab">http://www.sciencedirect.com/science/article/B6VSC-4F1SVTP-2/2/78b5823271285ca5567dca045c3a09ab</a> </p><br />
    <br clear="all">

    <p>6.         44592   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Synthesis of pure stereoisomers of benzo[b]thienyl dehydrophenylalanines by Suzuki cross-coupling. Preliminary studies of antimicrobial activity</p>

    <p>          Abreu, Ana S, Ferreira, Paula MT, Monteiro, Luis S, Queiroz, Maria-Joao RP, Ferreira, Isabel CFR, Calhelha, Ricardo C, and Estevinho, Leticia M</p>

    <p>          Tetrahedron <b>2004</b>.  60(51): 11821-11828</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4DK6DGD-4/2/0bb8ee512c24f6b043d75116453f9cf7">http://www.sciencedirect.com/science/article/B6THR-4DK6DGD-4/2/0bb8ee512c24f6b043d75116453f9cf7</a> </p><br />

    <p>7.         44593   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Comparative protein modeling of methionine S-adenosyltransferase (MAT) enzyme from Mycobacterium tuberculosis: a potential target for antituberculosis drug discovery</p>

    <p>          Khedkar, Santosh A, Malde, Alpeshkumar K, and Coutinho, Evans C</p>

    <p>          Journal of Molecular Graphics and Modelling <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGP-4F05GDP-4/2/61ed561ea261877b93876d87ef5a97f3">http://www.sciencedirect.com/science/article/B6TGP-4F05GDP-4/2/61ed561ea261877b93876d87ef5a97f3</a> </p><br />

    <p>8.         44594   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          Synthesis and anti-microbial activity evaluation of some new 1-benzoyl-isothiosemicarbazides</p>

    <p>          Plumitallo, A, Cardia, MC, Distinto, S, Delogu, A, and Maccioni, E</p>

    <p>          Farmaco <b>2004</b>.  59(12): 945-52</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15598429&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15598429&amp;dopt=abstract</a> </p><br />

    <p>9.         44595   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          Drug susceptibility of Mycobacterium tuberculosis to primary antitubercular drugs by nitrate reductase assay</p>

    <p>          Sethi, S, Sharma, S, Sharma, SK, Meharwal, SK, Jindal, SK, and Sharma, M</p>

    <p>          Indian J Med Res <b>2004</b>.  120(5): 468-71</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15591631&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15591631&amp;dopt=abstract</a> </p><br />

    <p>10.       44596   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          Evaluation of three methods to determine the antimicrobial susceptibility of Mycobacterium tuberculosis</p>

    <p>          Muralidhar, S and Srivastava, L</p>

    <p>          Indian J Med Res <b>2004</b>.  120(5): 463-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15591630&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15591630&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44597   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Identification of a novel peptidoglycan hydrolase CwlM in Mycobacterium tuberculosis</p>

    <p>          Deng, Lingyi Lynn, Humphries, Donald E, Arbeit, Robert D, Carlton, Laura E, Smole, Sandra C, and Carroll, JDavid</p>

    <p>          Biochimica et Biophysica Acta (BBA) - Proteins &amp; Proteomics <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B73DJ-4DJBGWN-1/2/223462979853a3cf457c83e00dd21612">http://www.sciencedirect.com/science/article/B73DJ-4DJBGWN-1/2/223462979853a3cf457c83e00dd21612</a> </p><br />

    <p>12.       44598   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          A Diarylquinoline Drug Active on the ATP Synthase of Mycobacterium tuberculosis</p>

    <p>          Andries, K, Verhasselt, P, Guillemont, J, Gohlmann, HW, Neefs, JM, Winkler, H, Van, Gestel J, Timmerman, P, Zhu, M, Lee, E, Williams, P, de, Chaffoy D, Huitric, E, Hoffner, S, Cambau, E, Truffot-Pernot, C, Lounis, N, and Jarlier, V</p>

    <p>          Science <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15591164&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15591164&amp;dopt=abstract</a> </p><br />

    <p>13.       44599   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          Enhanced anticryptococcal activity of chloroquine in phosphatidylserine-containing liposomes in a murine model</p>

    <p>          Khan, MA, Jabeen, R, Nasti, TH, and Mohammad, O</p>

    <p>          J Antimicrob Chemother <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15590713&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15590713&amp;dopt=abstract</a> </p><br />

    <p>14.       44600   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          Competitive inhibitors of mycobacterium tuberculosis ribose-5-phosphate isomerase B reveal new information about the reaction mechanism</p>

    <p>          Roos, AK, Burgos, E, Ericsson, DJ, Salmon, L, and Mowbray, SL</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15590681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15590681&amp;dopt=abstract</a> </p><br />

    <p>15.       44601   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p><b>          Immunotherapy with fragmented Mycobacterium tuberculosis cells increases the effectiveness of chemotherapy against a chronical infection in a murine model of tuberculosis</b> </p>

    <p>          Cardona, Pere-Joan, Amat, Isabel, Gordillo, Sergi, Arcos, Virginia, Guirado, Evelyn, Diaz, Jorge, Vilaplana, Cristina, Tapia, Gustavo, and Ausina, Vicenc</p>

    <p>          Vaccine <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4DGMJ72-6/2/71637a402099b21e16df1a1172d260db">http://www.sciencedirect.com/science/article/B6TD4-4DGMJ72-6/2/71637a402099b21e16df1a1172d260db</a> </p><br />
    <br clear="all">

    <p>16.       44602   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          A systematic approach to the optimization of substrate-based inhibitors of the hepatitis C virus NS3 protease: discovery of potent and specific tripeptide inhibitors</p>

    <p>          Llinas-Brunet, M, Bailey, MD, Ghiro, E, Gorys, V, Halmos, T, Poirier, M, Rancourt, J, and Goudreau, N</p>

    <p>          J Med Chem <b>2004</b>.  47(26): 6584-94</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588093&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588093&amp;dopt=abstract</a> </p><br />

    <p>17.       44603   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          The design and enzyme-bound crystal structure of indoline based peptidomimetic inhibitors of hepatitis C virus NS3 protease</p>

    <p>          Ontoria, JM, Di, Marco S, Conte, I, Di, Francesco ME, Gardelli, C, Koch, U, Matassa, VG, Poma, M, Steinkuhler, C, Volpari, C, and Harper, S</p>

    <p>          J Med Chem <b>2004</b>.  47(26): 6443-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588076&amp;dopt=abstract</a> </p><br />

    <p>18.       44604   OI-LS-312; PUBMED-OI-12/27/2004</p>

    <p class="memofmt1-2">          2-methoxylated fatty acids in marine sponges: defense mechanism against mycobacteria?</p>

    <p>          Carballeira, NM, Cruz, H, Kwong, CD, Wan, B, and Franzblau, S</p>

    <p>          Lipids <b>2004</b>.  39 (7): 675-80</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588025&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588025&amp;dopt=abstract</a> </p><br />

    <p>19.       44605   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Evaluation of multiple genomic targets for identification and confirmation of Mycobacterium avium subsp. paratuberculosis isolates using real-time PCR</p>

    <p>          Rajeev, Sreekumari, Zhang, Yan, Sreevatsan, Srinand, Motiwala, Alifiya S, and Byrum, Beverly</p>

    <p>          Veterinary Microbiology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD6-4F320M1-2/2/9244979a7f3b74b9225830314318bef3">http://www.sciencedirect.com/science/article/B6TD6-4F320M1-2/2/9244979a7f3b74b9225830314318bef3</a> </p><br />

    <p>20.       44606   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Inhibition of native hepatitis C virus replicase by nucleotide and non-nucleoside inhibitors</p>

    <p>          Ma, Han, Leveque, Vincent, De Witte, Anniek, Li, Weixing, Hendricks, Than, Clausen, Saundra M, Cammack, Nick, and Klumpp, Klaus</p>

    <p>          Virology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4F1J8M9-3/2/7d0382a0e9cc853ed219a714db6c2379">http://www.sciencedirect.com/science/article/B6WXR-4F1J8M9-3/2/7d0382a0e9cc853ed219a714db6c2379</a> </p><br />
    <br clear="all">

    <p>21.       44607   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Inhibition of herpesvirus replication by a series of 4-oxo-dihydroquinolines with viral polymerase activity</p>

    <p>          Hartline, Caroll B, Harden, Emma A, Williams-Aziz, Stephanie L, Kushner, Nicole L, Brideau, Roger J, and Kern, Earl R</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DYVTS1-1/2/ebaa56c03a881bbae746693f0df85c41">http://www.sciencedirect.com/science/article/B6T2H-4DYVTS1-1/2/ebaa56c03a881bbae746693f0df85c41</a> </p><br />

    <p>22.       44608   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          Parallel synthesis of pteridine derivatives as potent inhibitors for hepatitis C virus NS5B RNA-dependent RNA polymerase</p>

    <p>          Ding, Yili, Girardet, Jean-Luc, Smith, Kenneth L, Larson, Gary, Prigaro, Brett, Lai, Vicky CH, Zhong, Weidong, and Wu, Jim Z</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DXT7R7-9/2/1249a8386743330f18623e5ba00a505a">http://www.sciencedirect.com/science/article/B6TF9-4DXT7R7-9/2/1249a8386743330f18623e5ba00a505a</a> </p><br />

    <p>23.       44609   OI-LS-312; EMBASE-OI-12/27/2004</p>

    <p class="memofmt1-2">          The impact of HIV-protease inhibitors on opportunistic parasites</p>

    <p>          Pozio, Edoardo and Morales, Maria Angeles Gomez</p>

    <p>          Trends in Parasitology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7G-4DW336B-1/2/56fbd972fce1fb5b292e5f55a8880dfb">http://www.sciencedirect.com/science/article/B6W7G-4DW336B-1/2/56fbd972fce1fb5b292e5f55a8880dfb</a> </p><br />

    <p>24.       44610   OI-LS-312; WOS-OI-12/19/2004</p>

    <p class="memofmt1-2">          Characterisation of benzimidazole binding with recombinant tubulin from Giardia duodenalis, Encephalitozoon intestinalis, and Cryptosporidium parvum</p>

    <p>          MacDonald, LM, Armson, A, Thompson, RCA, and Reynoldson, JA</p>

    <p>          MOLECULAR AND BIOCHEMICAL PARASITOLOGY <b>2004</b>.  138(1): 89-96, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225128100011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225128100011</a> </p><br />

    <p>25.       44611   OI-LS-312; WOS-OI-12/19/2004</p>

    <p class="memofmt1-2">          Synthesis of a new class of furan-fused tetracyclic compounds using o-quinodimethane chemistry and investigation of their antiviral activity</p>

    <p>          Matsuya, Y, Sasaki, K, Nagaoka, M, Kakuda, H, Toyooka, N, Imanishi, N, Ochiai, H, and Nemoto, H</p>

    <p>          JOURNAL OF ORGANIC CHEMISTRY <b>2004</b>.  69(23): 7989-7993, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225024200030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225024200030</a> </p><br />

    <p>26.       44612   OI-LS-312; WOS-OI-12/19/2004</p>

    <p class="memofmt1-2">          Stereoselective synthesis and antifungal activity of (Z)-trans-3-azolyl-2-methylchromanone oxime ethers</p>

    <p>          Emami, S, Falahati, M, Banifatemi, A, and Shafiee, A</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(22): 5881-5889, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225050500016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225050500016</a> </p><br />
    <br clear="all">

    <p>27.       44613   OI-LS-312; WOS-OI-12/19/2004</p>

    <p class="memofmt1-2">          Novel mutations within the embB gene in ethambutol-susceptible clinical isolates of Mycobacterium tuberculosis</p>

    <p>          Lee, ASG, Othman, SNK, Ho, YM, and Wong, SY</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(11): 4447-4449, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900056</a> </p><br />

    <p>28.       44614   OI-LS-312; WOS-OI-12/19/2004</p>

    <p class="memofmt1-2">          Antimicrobial and antileishmanial activities of hypocrellins A and B</p>

    <p>          Ma, GY, Khan, SI, Jacob, MR, Tekwani, BL, Li, ZQ, Pasco, DS, Walker, LA, and Khan, LA</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(11): 4450-4452, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900057</a> </p><br />

    <p>29.       44615   OI-LS-312; WOS-OI-12/19/2004</p>

    <p class="memofmt1-2">          Evaluation of a colorimetric antifungal susceptibility test by using 2,3-diphenyl-5-thienyl-(2)-tetrazolium chloride</p>

    <p>          Shin, JH, Choi, JC, Lee, JN, Kim, HH, Lee, EY, and Chang, CL</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(11): 4457-4459, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900059</a> </p><br />

    <p>30.       44616   OI-LS-312; WOS-OI-12/19/2004</p>

    <p class="memofmt1-2">          Moxifloxacin-containing regimens of reduced duration produce a stable cure in murine tuberculosis</p>

    <p>          Nuermberger, EL, Yoshimatsu, T, Tyagi, S, Williams, K, Rosenthal, I, O&#39;Brien, RJ, Vernon, AA, Chaisson, RE, Bishai, WR, and Grosset, JH</p>

    <p>          AMERICAN JOURNAL OF RESPIRATORY AND CRITICAL CARE MEDICINE <b>2004</b>.  170(10): 1131-1134, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225076700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225076700016</a> </p><br />

    <p>31.       44617   OI-LS-312; WOS-OI-12/26/2004</p>

    <p class="memofmt1-2">          Antimycobacterial activity of Taxus baccata</p>

    <p>          Erdemoglu, N, Sener, B, and Palittapongarnpim, P</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2003</b>.  41(8): 614-615, 2</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225297500011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225297500011</a> </p><br />

    <p>32.       44618   OI-LS-312; WOS-OI-12/26/2004</p>

    <p class="memofmt1-2">          Protein-protein interactions within the Fatty Acid Synthase-II system of Mycobacterium tuberculosis are essential for mycobacterial viability</p>

    <p>          Veyron-Churlet, R, Guerrini, O, Mourey, L, Daffe, M, and Zerbib, D</p>

    <p>          MOLECULAR MICROBIOLOGY <b>2004</b>.  54(5): 1161-1172, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225264900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225264900003</a> </p><br />

    <p>33.       44619   OI-LS-312; WOS-OI-12/26/2004</p>

    <p class="memofmt1-2">          RNA interference - small RNAs effectively fight viral hepatitis</p>

    <p>          Shlomai, A and Shaul, Y</p>

    <p>          LIVER INTERNATIONAL <b>2004</b>.  24(6): 526-531, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225357900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225357900002</a> </p><br />

    <p>34.       44620   OI-LS-312; WOS-OI-12/26/2004</p>

    <p class="memofmt1-2">          Antimycobacterial screening of some Turkish plants</p>

    <p>          Tosun, F, Kizilay, CA, Sener, B, Vural, M, and Palittapongarnpim, P</p>

    <p>          JOURNAL OF ETHNOPHARMACOLOGY <b>2004</b>.  95(2-3): 273-275, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225154500027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225154500027</a> </p><br />

    <p>35.       44621   OI-LS-312; WOS-OI-12/26/2004</p>

    <p class="memofmt1-2">          A new steroidal saponin from Dioscorea cayenensis</p>

    <p>          Sautour, M, Mitaine-Offer, AC, Miyamoto, T, Dongmo, A, and Lacaille-Dubois, MA</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2004</b>.  52(11): 1353-1355, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225154700019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225154700019</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
