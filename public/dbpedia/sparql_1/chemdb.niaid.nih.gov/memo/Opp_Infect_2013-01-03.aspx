

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-01-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3+18YlofjP/8LQ+S5Fg0CpDYw2bcj+3d25uCEkFJ3yKjiQy1sCaY4JWPZnDQNflQ8SthCAYehGsW06fDOP1hd+JAsvkkSdLn840DmdXslOt0bRuJLzDtbXLfdTQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="733B8CCA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: December 21 - January 3, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23261729">Synthesis, in Vitro Antibacterial and Antifungal Activity of Some N-Acetylated and non-Acetylated Pyrazolines.</a> Baseer, M., F.L. Ansari, and Z. Ashraf. Pakistan Journal of Pharmaceutical Sciences, 2013. 26(1): p. 67-73. PMID[23261729].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23262278">Design, Synthesis, Inhibition Studies, and Molecular Modeling of Pepstatin Analogues Adressing Different Secreted Aspartic Proteinases of Candida albicans.</a> Cadicamo, C.D., J. Mortier, G. Wolber, M. Hell, I.E. Heinrich, D. Michel, L. Semlin, U. Berger, H.C. Korting, H.D. Holtje, B. Koksch, and C. Borelli. Biochemical Pharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[23262278].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23262192">The Antifungal Activity and Membrane-disruptive Action of Dioscin Extracted from Dioscorea nipponica.</a> Cho, J., H. Choi, J. Lee, M.S. Kim, H.Y. Sohn, and D.G. Lee. Biochimica et Biophysica Acta, 2012. <b>[Epub ahead of print]</b>. PMID[23262192].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22494896">A Novel Anticancer and Antifungus Phenazine Derivative from a Marine Actinomycete BM-17.</a> Gao, X., Y. Lu, Y. Xing, Y. Ma, J. Lu, W. Bao, Y. Wang, and T. Xi. Microbiological Research, 2012. 167(10): p. 616-622. PMID[22494896].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23044194">Influence of DMSO on Antifungal Activity During Susceptibility Testing in Vitro.</a> Hazen, K.C. Diagnostic Microbiology and Infectious Disease, 2013. 75(1): p. 60-63. PMID[23044194].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23269921">Stable Redox-cycling Nitroxide tempol Inhibits Net Formation.</a> Hosseinzadeh, A., P.K. Messer, and C.F. Urban. Frontiers in Immunology, 2012. 3: p. 391. PMID[23269921].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23129051">Antimicrobial Photodynamic Inactivation Inhibits Candida albicans Virulence Factors and Reduces in Vivo Pathogenicity.</a> Kato, I.T., R.A. Prates, C.P. Sabino, B.B. Fuchs, G.P. Tegos, E. Mylonakis, M.R. Hamblin, and M.S. Ribeiro. Antimicrobial Agents and Chemotherapy, 2013. 57(1): p. 445-451. PMID[23129051].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23237384">Sulfonylureas Have Antifungal Activity and Are Potent Inhibitors of Candida albicans Acetohydroxyacid Synthase.</a> Lee, Y.T., C.J. Cui, E.W. Chow, N. Pue, T. Lonhienne, J.G. Wang, J.A. Fraser, and L.W. Guddat. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23237384].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23147740">Posaconazole Pharmacodynamic Target Determination against Wild-type and Cyp51 Mutant Isolates of Aspergillus fumigatus in an in Vivo Model of Invasive Pulmonary Aspergillosis.</a> Lepak, A.J., K. Marchillo, J. Vanhecker, and D.R. Andes. Antimicrobial Agents and Chemotherapy, 2013. 57(1): p. 579-585. PMID[23147740].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22746405">Postantifungal Effect of the Combination of Caspofungin with Voriconazole and Amphotericin b against Clinical Candida krusei Isolates.</a> Oz, Y., A. Kiremitci, I. Dag, S. Metintas, and N. Kiraz. Medical Mycology, 2013. 51(1): p. 60-65. PMID[22746405].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23275516">Prothioconazole and Prothioconazole-desthio Activity against Candida albicans Sterol 14-&#945; Demethylase (Cacyp51).</a> Parker, J.E., A.G. Warrilow, H.J. Cools, B.A. Fraaije, J.A. Lucas, K. Rigdova, W.J. Griffiths, D.E. Kelly, and S.L. Kelly. Applied and Environmental Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[23275516].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221_010313.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23141113">Biological Evaluation of Novel Substituted Chloroquinolines Targeting Mycobacterial ATP Synthase.</a> Khan, S.R., S. Singh, K.K. Roy, M.S. Akhtar, A.K. Saxena, and M.Y. Krishnan. International Journal of Antimicrobial Agents, 2013. 41(1): p. 41-46. PMID[23141113].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23249609">Novel Therapeutic Vaccines [(HSP65+IL-12)DNA-, Granulysin- and Ksp37-Vaccine] against Tuberculosis and Synergistic Effects in the Combination with Chemotherapy.</a> Kita, Y., S. Hashimoto, T. Nakajima, H. Nakatani, S. Nishimatsu, Y. Nishida, N. Kanamaru, Y. Kaneda, Y. Takamori, D. McMurray, E.V. Tan, M.L. Cang, P. Saunderson, E.C. Dela Cruz, and M. Okada. Human Vaccines &amp; Immunotherapeutics, 2012. <b>[Epub ahead of print]</b>. PMID[23249609].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23261599">Structure-guided Discovery of Phenyl-diketo acids as Potent Inhibitors of M. tuberculosis Malate Synthase.</a> Krieger, I.V., J.S. Freundlich, V.B. Gawandi, J.P. Roberts, Q. Sun, J.L. Owen, M.T. Fraile, S.I. Huss, J.L. Lavandera, T.R. Ioerger, and J.C. Sacchettini. Chemistry &amp; Biology, 2012. 19(12): p. 1556-1567. PMID[23261599].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23211970">Structure-Activity Relationships of Antitubercular Salicylanilides Consistent with Disruption of the Proton Gradient via Proton Shuttling.</a> Lee, I.Y., T.D. Gruber, A. Samuels, M. Yun, B. Nam, M. Kang, K. Crowley, B. Winterroth, H.I. Boshoff, and C.E. Barry, 3rd. Bioorganic &amp; Medicinal Chemistry, 2013. 21(1): p. 114-126. PMID[23211970].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23268609">Aminopyrazinamides: Novel and Specific GyrB Inhibitors That Kill Replicating and Nonreplicating Mycobacterium tuberculosis.</a> Shirude, P.S., P. Madhavapeddi, J.A. Tucker, K. Murugan, V. Patil, H. Basavarajappa, A.V. Raichurkar, V. Humnabadkar, S. Hussein, S. Sharma, V.K. Ramya, C.B. Narayan, T.S. Balganesh, and V.K. Sambandamurthy. ACS Chemical Biology, 2012. <b>[Epub ahead of print]</b>. PMID[23268609].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23199481">Synthesis and Evaluation of 5&#39;-Modified thymidines and 5-Hydroxymethyl-2&#39;-deoxyuridines as Mycobacterium tuberculosis Thymidylate Kinase Inhibitors.</a> Toti, K.S., F. Verbeke, M.D. Risseeuw, V. Frecer, H. Munier-Lehmann, and S. Van Calenbergh. Bioorganic &amp; Medicinal Chemistry, 2013. 21(1): p. 257-268. PMID[23199481].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23164712">Synthesis and Antiplasmodial Activity of Some 1-Azabenzanthrone Derivatives.</a> Castro-Castillo, V., C. Suarez-Rozas, A. Pabon, E.G. Perez, B.K. Cassels, and S. Blair. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 327-329. PMID[23164712].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23240776">Synthesis and Evaluation of &#945;-Thymidine Analogues as Novel Antimalarials.</a> Cui, H., J. Carrero-Lerida, A.P. Silva, J.L. Whittingham, J.A. Brannigan, L.M. Ruiz-Perez, K.D. Read, K.S. Wilson, D. Gonzalez-Pacanowska, and I.H. Gilbert. Journal of Medicinal Chemistry, 2012. 55(24): p. 10948-10957. PMID[23240776].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23214499">3,6-Diamino-4-(2-halophenyl)-2-benzoylthieno[2,3-b]pyridine-5-carbonitriles Are Selective Inhibitors of Plasmodium falciparum Glycogen Synthase Kinase-3.</a> Fugel, W., A.E. Oberholzer, B. Gschloessl, R. Dzikowski, N. Pressburger, L. Preu, L.H. Pearl, B. Baratte, M. Ratin, I. Okun, C. Doerig, S. Kruggel, T. Lemcke, L. Meijer, and C. Kunick. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23214499].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23189922">Structure-Activity Relationship Studies of Orally Active Antimalarial 3,5-Substituted 2-aminopyridines.</a> Gonzalez Cabrera, D., F. Douelle, Y. Younis, T.S. Feng, C. Le Manach, A.T. Nchinda, L.J. Street, C. Scheurer, J. Kamber, K.L. White, O.D. Montagnat, E. Ryan, K. Katneni, K.M. Zabiulla, J.T. Joseph, S. Bashyam, D. Waterson, M.J. Witty, S.A. Charman, S. Wittlin, and K. Chibale. Journal of Medicinal Chemistry, 2012. 55(24): p. 11022-11030. PMID[23189922].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23236186">Targeting the ERAD Pathway Via Inhibition of Signal Peptide Peptidase for Antiparasitic Therapeutic Design.</a> Harbut, M.B., B.A. Patel, B.K. Yeung, C.W. McNamara, A.T. Bright, J. Ballard, F. Supek, T.E. Golde, E.A. Winzeler, T.T. Diagana, and D.C. Greenbaum. Proceedings of the National Academy of Sciences of the United States of America, 2012. 109(52): p. 21486-21491. PMID[23236186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23129047">4(1H)-Quinolones with Liver Stage Activity against Plasmodium berghei.</a> Lacrue, A.N., F.E. Saenz, R.M. Cross, K.O. Udenze, A. Monastyrskyi, S. Stein, T.S. Mutka, R. Manetsch, and D.E. Kyle. Antimicrobial Agents and Chemotherapy, 2013. 57(1): p. 417-424. PMID[23129047].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/223262447">Stability and Antioxidant Activity of Semi-synthetic Derivatives of 4-Nerolidylcatechol.</a> Lima, E.S., A.C. Pinto, K.L. Nogueira, L.F. Silva, P.D. Almeida, M.C. Vasconcellos, F.C. Chaves, W.P. Tadei, and A.M. Pohlit. Molecules, 2012. 18(1): p. 178-189. PMID[23262447].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23218718">Synthesis, Antimalarial Activity and Cytotoxic Potential of New Monocarbonyl Analogues of Curcumin.</a> Manohar, S., S.I. Khan, S.K. Kandi, K. Raj, G. Sun, X. Yang, A.D. Calderon Molina, N. Ni, B. Wang, and D.S. Rawat. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 112-116. PMID[23218718].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23273038">N-Cinnamoylated Chloroquine Analogues as Dual-stage Antimalarial Leads.</a> Perez, B.C., C. Teixeira, I.S. Albuquerque, J. Gut, P.J. Rosenthal, J.R. Gomes, M. Prudencio, and P. Gomes. Journal of Medicinal Chemistry, 2012. PMID[23273038].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23168082">Synthesis, Characterization and Antimalarial Activity of Quinoline-Pyrimidine Hybrids.</a> Pretorius, S.I., W.J. Breytenbach, C. de Kock, P.J. Smith, and D. N&#39;Da D. Bioorganic &amp; Medicinal Chemistry, 2013. 21(1): p. 269-277. PMID[23168082].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22985530">Correlation between Structure, Retention, Property, and Activity of Biologically Relevant 1,7-bis(Aminoalkyl)diazachrysene Derivatives.</a> Segan, S., J. Trifkovic, T. Verbic, D. Opsenica, M. Zlatovic, J. Burnett, B. Solaja, and D. Milojkovic-Opsenica. Journal of Pharmaceutical and Biomedical Analysis, 2013. 72: p. 231-239. PMID[22985530].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23270565">Synthesis and Insight into the Structure-Activity Relationships of Chalcones as Antimalarial Agents.</a> Tadigoppula, N., V. Korthikunta, S. Gupta, P. Kancharla, T. Khaliq, A. Soni, R.K. Srivastava, K. Srivastava, S.K. Puri, K.S. Raju, Wahajuddin, P.S. Sijwali, V. Kumar, and I.S. Mohammad. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23270565].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23195733">Synthesis and Antiplasmodial Evaluation of Novel (4-Aminobutyloxy)quinolines.</a> Vandekerckhove, S., C. Muller, D. Vogt, C. Lategan, P.J. Smith, K. Chibale, N. De Kimpe, and M. D&#39;Hooghe. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 318-322. PMID[23195733].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p> 
    <p> </p>
    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23266332">Ethnobotanical Study of Medicinal Plants Used for the Treatment of Malaria in Plateau of Allada, Benin (West Africa).</a> Yetein, M.H., L.G. Houessou, T.O. Lougbegnon, O. Teka, and B. Tente. Journal of ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[23266332].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">32.   <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23270807">Toxoplasma gondii: The Effect of Fluconazole Combined with Sulfadiazine and Pyrimethamine against Acute Toxoplasmosis in Murine Model.</a> Martins-Duarte, E.S., W.D. Souza, and R.C. Vommaro. Experimental Parasitology, 2012. <b>[Epub ahead of print]</b>. PMID[23270807].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1221-010313.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311425500039">Design, Synthesis and Antimicrobial Evaluation of Novel 1-Benzyl 2-butyl-4-chloroimidazole Embodied 4-Azafluorenones Via Molecular Hybridization Approach.</a> Addla, D., Bhima, B. Sridhar, A. Devi and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7475-7480. ISI[000311425500039].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311425500026">A Regio- and Stereoselective 1,3-Dipolar Cycloaddition for the Synthesis of New-fangled Dispiropyrrolothiazoles as Antimycobacterial Agents.</a> Almansour, A.I., S. Ali, M.A. Ali, R. Ismail, T.S. Choon, V. Sellappan, K.K. Elumalai and S. Pandian. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7418-7421. ISI[000311425500026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310968700022">The Nonsteroidal Antiinflammatory Drug Diclofenac Potentiates the in Vivo Activity of Caspofungin against Candida albicans Biofilms.</a> Bink, A., S. Kucharikova, B. Neirinck, J. Vleugels, P. Van Dijck, B.P. Cammue and K. Thevissen. Journal of Infectious Diseases, 2012. 206(11): p. 1790-1797. ISI[000310968700022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311185700019">Screening of Indigenous Knowledge of Herbal Remedies for Skin Diseases among Local Communities of North West Punjab, Pakistan.</a> Gul, F., Z.K. Shinwari and I. Afzal. Pakistan Journal of Botany, 2012. 44(5): p. 1609-1616. ISI[000311185700019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311428400026">Antimycobacterial Assessment of Salicylanilide benzoates Including Multidrug-Resistant Tuberculosis Strains.</a> Kratky, M., J. Vinsova and J. Stolarikova. Molecules, 2012. 17(11): p. 12812-12820. ISI[000311428400026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311052900049">Interaction of the Echinocandin caspofungin with Amphotericin b or Voriconazole against Aspergillus Biofilms in Vitro.</a> Liu, W., L. Li, Y. Sun, W. Chen, Z. Wan, R. Li and W. Liu. Antimicrobial Agents and Chemotherapy, 2012. 56(12): p. 6414-6416. ISI[000311052900049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311461500018">Tricyclic Sulfonamides Incorporating Benzothiopyrano 4,3-c pyrazole and Pyridothiopyrano 4,3-c pyrazole Effectively Inhibit &#945;- and &#946;-Carbonic Anhydrase: X-ray Crystallography and Solution Investigations on 15 Isoforms.</a> Marini, A.M., A. Maresca, M. Aggarwal, E. Orlandini, S. Nencetti, F. Da Settimo, S. Salerno, F. Simorini, C. La Motta, S. Taliani, E. Nuti, A. Scozzafava, R. McKenna, A. Rossello and C.T. Supuran. Journal of Medicinal Chemistry, 2012. 55(22): p. 9619-9629. ISI[000311461500018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311425500051">Microwave-Assisted Synthesis, Molecular Docking and Antitubercular Activity of 1,2,3,4-Tetrahydropyrimidine-5-carbonitrile Derivatives.</a> Mohan, S.B., B.V. Ravi Kumar, S.C. Dinda, D. Naik, S. Prabu Seenivasan, V. Kumar, D.N. Rana and P.S. Brahmkshatriya. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7539-7542. ISI[000311425500051].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311425500091">Synthesis and Antimycobacterial Activity of Calpinactam Derivatives.</a> Nagai, K., N. Koyama, N. Sato, C. Yanagisawa and H. Tomoda. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7739-7741. ISI[000311425500091].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310947900007">Synthesis of Isomeric Corniculatolides.</a> Raut, G.N., K. Chakraborty, P. Verma, R.S. Gokhale, and D.S. Reddy. Tetrahedron Letters, 2012. 53(47): p. 6343-6346. ISI[000310947900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311428400054">Substituted N-Benzylpyrazine-2-carboxamides: Synthesis and Biological Evaluation.</a> Servusova, B., D. Eibinova, M. Dolezal, V. Kubicek, P. Paterova, M. Pesko and K. Kral&#39;ova. M. Molecules, 2012. 17(11): p. 13183-13198. ISI[000311428400054].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p> 
    <p> </p>
    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311425500028">Molecular Hybridization of Bioactives: Synthesis and Antitubercular Evaluation of Novel Dibenzofuran Embodied Homoisoflavonoids Via Baylis-Hillman Reaction.</a> Yempala, T., D. Sriram, P. Yogeeswari and S. Kantevari. Bioorganic &amp; medicinal chemistry letters, 2012. 22(24): p. 7426-7430. ISI[000311425500028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221_010313.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
