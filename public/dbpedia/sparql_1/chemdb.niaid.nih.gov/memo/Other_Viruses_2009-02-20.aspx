

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-02-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lY4RlmT74aHZrnVfRUyde8Ie4SijGegvh9g+bmG46WSs0fyq6t1wLQgukAU4dY8jtbWHD8NRXDt5epBChqxWQRR2dZxvrvTSIk6ZDvY3pKpI+97e0kpVU9CYHkU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6CAC9302" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses  Citation List:  February 4, 2009 - February 17, 2009</h1>

    <p class="memofmt2-2">Hepatitis Virus</p>

    <p class="ListParagraphCxSpFirst">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19209845?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Discovery of (R)-6-Cyclopentyl-6-(2-(2,6-diethylpyridin-4-yl)ethyl)-3-((5,7-dimethyl-[1,2,4]triazolo[1,5-a]pyrimidin-2-yl)methyl)-4-hydroxy-5,6-dihydropyran-2-one (PF-00868554) as a Potent and Orally Available Hepatitis C Virus Polymerase Inhibitor.</a>
    <br />
    Li H, Tatlock J, Linton A, Gonzalez J, Jewell T, Patel L, Ludlum S, Drowns M, Rahavendran SV, Skor H, Hunter R, Shi ST, Herlihy KJ, Parge H, Hickey M, Yu X, Chau F, Nonomiya J, Lewis C.
    <br />
    J Med Chem. 2009 Feb 11. [Epub ahead of print]
    <br />
    PMID: 19209845 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19199832?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Amidinoanthracyclines - a new group of potential anti-hepatitis C virus compounds.</a>
    <br />
    Krawczyk M, Wasowska-Lukawska M, Oszczapowicz I, Boguszewska-Chachulska AM.
    <br />
    Biol Chem. 2009 Feb 7. [Epub ahead of print]
    <br />
    PMID: 19199832 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19196021?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Second-Generation Highly Potent and Selective Inhibitors of the Hepatitis C Virus NS3 Serine Protease.</a>
    <br />
    Chen KX, Nair L, Vibulbhan B, Yang W, Arasappan A, Bogen SL, Venkatraman S, Bennett F, Pan W, Blackman ML, Padilla AI, Prongay A, Cheng KC, Tong X, Shih NY, Njoroge FG.
    <br />
    J Med Chem. 2009 Feb 5. [Epub ahead of print]
    <br />
    PMID: 19196021 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19193060?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Structure-Activity Relationship of New Anti-Hepatitis C Virus Agents: Heterobicycle-Coumarin Conjugates.</a>
    <br />
    Neyts J, Clercq ED, Singha R, Chang YH, Das AR, Chakraborty SK, Hong SC, Tsay SC, Hsu MH, Hwu JR.
    <br />
    J Med Chem. 2009 Feb 4. [Epub ahead of print]
    <br />
    PMID: 19193060 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpLast">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19168351?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Novel potent inhibitors of hepatitis C virus (HCV) NS3 protease with cyclic sulfonyl P3 cappings.</a>
    <br />
    Chen KX, Vibulbhan B, Yang W, Nair LG, Tong X, Cheng KC, Njoroge FG.
    <br />
    Bioorg Med Chem Lett. 2009 Feb 15;19(4):1105-1109. Epub 2009 Jan 8.
    <br />
    PMID: 19168351 [PubMed - as supplied by publisher]</p> 

    <p class="NoSpacing">&nbsp;</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
