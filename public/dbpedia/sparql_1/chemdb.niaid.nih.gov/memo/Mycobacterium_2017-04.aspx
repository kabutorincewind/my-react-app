

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QO+QWVhOw8lMdZUdwib6ujXVGFE3OUy65tGTG0SdpAxt0jFeQo7agIX4FsbTCyelD6WgK0jKTtaMj6bxlULp0ZTtVyVjhzN8TTTS7dDr7Whr6Iu9weAHI8t62EM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="552F58AC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479676103">Mycobacterium Citations List: April, 2017</a></h1>

    <h2><a name="_Toc479676104">Literature Citations</a></h2>

    <p class="plaintext"><a name="_Toc479676105">1.</a> <a href="http://www.ncbi.nlm.nih.gov/pubmed/28423019">Design, Synthesis and Structure-Activity Relationship Study of Wollamide B; a New Potential Anti TB Agent.</a> Asfaw, H., K. Laqua, A.M. Walkowska, F. Cunningham, M.S. Martinez-Martinez, J.C. Cuevas-Zurita, L. Ballell-Pages, and P. Imming. Plos One, 2017. 12(4): p. e0176088. PMID[28423019]. PMCID[PMC5397059].<b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395675400006">Design, Synthesis and Biological Evaluation of Novel Tetrahydroquinoline Based Propanehydrazides as Antitubercular Agents.</a> Chander, S., P. Ashok, B.D. Maira, P. Cos, D. Cappoen, and S. Murugesan. Letters in Drug Design &amp; Discovery, 2017. 14(3): p. 293-300. ISI[000395675400006]. <b><br />
    [WOS]</b>. TB_04_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28223393">Novel Bacterial Topoisomerase Inhibitors with Potent Broad-spectrum Activity against Drug-resistant Bacteria.</a> Charrier, C., A.M. Salisbury, V.J. Savage, T. Duffy, E. Moyo, N. Chaffer-Malam, N. Ooi, R. Newman, J. Cheung, R. Metzger, D. McGarry, M. Pichowicz, R. Sigerson, I.R. Cooper, G. Nelson, H.S. Butler, M. Craighead, A.J. Ratcliffe, S.A. Best, and N.R. Stokes. Antimicrobial Agents and Chemotherapy, 2017. 61(5): e02100-16. PMID[28223393]. <b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28167548">Ga(III) Nanoparticles Inhibit Growth of Both Mycobacterium tuberculosis and HIV and Release of Interleukin-6 (IL-6) and IL-8 in Coinfected Macrophages.</a> Choi, S.R., B.E. Britigan, and P. Narayanasamy. Antimicrobial Agents and Chemotherapy, 2017. 61(4): e02505-16. PMID[28167548]. PMCID[PMC5365726].<b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000398533700007">Design, Synthesis, Molecular-docking and Antimycobacterial Evaluation of Some Novel 1,2,3-Triazolyl xanthenones.</a> Goud, G.L., S. Ramesh, D. Ashok, V.P. Reddy, P. Yogeeswari, D. Sriram, B. Saikrishna, and V. Manga. MedChemComm, 2017. 8(3): p. 559-570. ISI[000398533700007]. <b><br />
    [WOS]</b>. TB_04_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28285910">Synthesis and Antibacterial Activity of 3-Benzylamide Derivatives as FtsZ Inhibitors.</a> Hu, Z., S. Zhang, W. Zhou, X. Ma, and G. Xiang. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(8): p. 1854-1858. PMID[28285910]. <b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28245905">Synthesis of Readily Available Fluorophenylalanine Derivatives and Investigation of Their Biological Activity.</a> Kratky, M., S. Stepankova, K. Vorcakova, L. Navratilova, F. Trejtnar, J. Stolarikova, and J. Vinsova. Bioorganic Chemistry, 2017. 71: p. 244-256. PMID[28245905]. <b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28436453">Functional, Thermodynamics, Structural and Biological Studies of in Silico-identified Inhibitors of Mycobacterium tuberculosis Enoyl-ACP(CoA) Reductase Enzyme.</a> Martinelli, L.K.B., M. Rotta, A.D. Villela, V.S. Rodrigues-Junior, B.L. Abbadi, R.V. Trindade, G.O. Petersen, G.M. Danesi, L.R. Nery, I. Pauli, M.M. Campos, C.D. Bonan, O.N. de Souza, L.A. Basso, and D.S. Santos. Scientific Reports, 2017. 7(46696): 15pp. PMID[28436453]. PMCID[PMC5402281].<b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28193668">Towards Selective Mycobacterial ClpP1P2 Inhibitors with Reduced Activity against the Human Proteasome.</a> Moreira, W., S. Santhanakrishnan, G.J.Y. Ngan, C.B. Low, K. Sangthongpitag, A. Poulsen, B.W. Dymock, and T. Dick. Antimicrobial Agents and Chemotherapy, 2017. 61(5): e02307-16. PMID[28193668]. <b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28103726">Antibacterial Activities of Chemical Constituents from the Aerial Parts of Hedyotis pilulifera.</a> Nguyen, H.T., D.V. Ho, H.Q. Vo, A.T. Le, H.M. Nguyen, T. Kodama, T. Ito, H. Morita, and A. Raal. Pharmaceutical Biology, 2017. 55(1): p. 787-791. PMID[28103726]. <b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28242674">In Vitro Activity of Bedaquiline against Nontuberculous Mycobacteria in China.</a> Pang, Y., H. Zheng, Y. Tan, Y. Song, and Y. Zhao. Antimicrobial Agents and Chemotherapy, 2017. 61(5): e02627-16. PMID[28242674]. <b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28027449">Novel Chemical Scaffolds for Inhibition of Rifamycin-resistant RNA Polymerase Discovered from High-throughput Screening.</a> Scharf, N.T., V. Molodtsov, A. Kontos, K.S. Murakami, and G.A. Garcia. SLAS Discovery, 2017. 22(3): p. 287-297. PMID[28027449]. PMCID[PMC5323270].<b><br />
    [PubMed]</b>. TB_04_2017.</p><br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000396527800010">Synthesis of New Pyrido-benzodiazepine salts and Their Antimicrobial Activities.</a> Seebacher, W., F. Belaj, J. Faist, R. Saf, F. Bucar, I. Turek, A. Brantner, M. Alajlani, M. Kaiser, P. Maser, and R. Weis. Monatshefte fur Chemie, 2017. 148(2): p. 263-274. ISI[000396527800010]. <b><br />
    [WOS]</b>. TB_04_2017.</p><br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395407100030">Design, Synthesis and Biological Evaluation of New Substituted Sulfonamide Tetrazole Derivatives as Antitubercular Agents.</a> Suresh, A., N. Suresh, S. Misra, M.M.K. Kumar, and K. Sekhar. ChemistrySelect, 2016. 1(8): p. 1705-1710. ISI[000395407100030]. <b><br />
    [WOS]</b>. TB_04_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28355052">Design, Synthesis, and Experimental Validation of Peptide Ligands Targeting Mycobacterium tuberculosis Sigma Factors.</a> Vishwanath, S., S. Banerjee, A.K. Jamithireddy, N. Srinivasan, B. Gopal, and J. Chatterjee. Biochemistry, 2017. 56(16): p. 2209-2218. PMID[28355052]. <b><br />
    [PubMed]</b>. TB_04_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">16. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170427&amp;CC=WO&amp;NR=2017070024A1&amp;KC=A1">Oxazolidinone Compounds and Methods of Use Thereof as Antibacterial Agents.</a> Mandal, M.B., D.B. Olsen, J. Su, L. Yang, K. Young, T. Suzuki, and L. You. Patent. 2017. 2016-US57257 2017070024: 87pp.
    <br />
    <b>[Patent]</b>. TB_04_2017.</p><br />

    <p class="plaintext">17. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170420&amp;CC=WO&amp;NR=2017066053A1&amp;KC=A1">Combination Antibacterial Composition and Short-course Antibacterial Regimen.</a> Mdluli, K., Jr., C.M. Mendel, and E. Nuermberger. Patent. 2017. 2016-US55414 2017066053: 36pp.
    <br />
    <b>[Patent]</b>. TB_04_2017.</p><br />

    <p class="plaintext">18. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170427&amp;CC=WO&amp;NR=2017069960A1&amp;KC=A1">Inhibitors of Sulfur Metabolism with Potent Bactericidal Activity against MDR and XDR M. tuberculosis.</a> Palde, P.B. and K.S. Carroll. Patent. 2017. 2016-US56228 2017069960: 43pp.
    <br />
    <b>[Patent]</b>. TB_04_2017.</p><br />

    <p class="plaintext">19. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170427&amp;CC=WO&amp;NR=2017066964A1&amp;KC=A1">Oxazolidinone Compounds and Methods of Use Thereof as Antibacterial Agents.</a> Yang, L., D.B. Olsen, K. Young, J. Su, M.B. Mandal, T. Suzuki, and L. You. Patent. 2017. 2015-CN92563 2017066964: 85pp.
    <br />
    <b>[Patent]</b>. TB_04_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
