

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-299.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xop1OnWPT0sLIkeMBIwS3U2p3YGUklPaa+ZTgRFHW5ph2Kh1F3PdEgE4U1Xv1Oxy7BUPaHgcOoxkq3DedIWUHw9INI4Cd9k5V/a8mhI69sWo+GGmRPAHkMVDzI8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AF0A657F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-299-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43638   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Sensitivity of NS3 Serine Proteases from Hepatitis C Virus Genotypes 2 and 3 to the Inhibitor BILN 2061</p>

<p>           Thibeault, D, Bousquet, C, Gingras, R, Lagace, L, Maurice, R, White, PW, and Lamarre, D</p>

<p>           J Virol 2004. 78(14): 7352-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220408&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220408&amp;dopt=abstract</a> </p><br />

<p>     2.    43639   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           FR171456, a novel cholesterol synthesis inhibitor produced by Sporormiella minima No. 15604. I. Taxonomy, fermentation, isolation, physico-chemical properties</p>

<p>           Hatori, H, Shibata, T, Tsurumi, Y, Nakanishi, T, Katsuoka, M, Ohtsu, Y, Sakamoto, K, Takase, S, Ueda, H, Hino, M, and Fujii, T</p>

<p>           J Antibiot (Tokyo) 2004. 57(4): 253-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217189&amp;dopt=abstract</a> </p><br />

<p>     3.    43640   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Role of Cellular Activation and Tumor Necrosis Factor- alpha in the Early Expression of Mycobacterium tuberculosis 85B mRNA in Human Alveolar Macrophages</p>

<p>           Islam, N, Kanost, AR, Teixeira, L, Johnson, J, Hejal, R, Aung, H, Wilkinson, RJ, Hirsch, CS, and Toossi, Z</p>

<p>           J Infect Dis 2004. 190(2): 341-51</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15216471&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15216471&amp;dopt=abstract</a> </p><br />

<p>     4.    43641   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Rhesus cytomegalovirus is similar to human cytomegalovirus in susceptibility to benzimidazole nucleosides</p>

<p>           North, TW, Sequar, G, Townsend, LB, Drach, JC, and Barry, PA</p>

<p>           Antimicrob Agents Chemother 2004.  48(7): 2760-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215146&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215146&amp;dopt=abstract</a> </p><br />

<p>     5.    43642   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Direct evidence for cyanide-insensitive quinol oxidase (alternative oxidase) in apicomplexan parasite Cryptosporidium parvum: phylogenetic and therapeutic implications</p>

<p>           Suzuki, Takashi, Hashimoto, Tetsuo, Yabu, Yoshisada, Kido, Yasutoshi, Sakamoto, Kimitoshi, Nihei, Coh-ichi, Hato, Mariko, Suzuki, Shu-ichi, Amano, Yuko, Nagai, Kazuo, Hosokawa, Tomoyoshi, Minagawa, Nobuko, Ohta, Nobuo, and Kita, Kiyoshi</p>

<p>           Biochemical and Biophysical Research Communications 2004. 313(4): 1044-1052</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    6.     43643   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Inhibition of Mycobacterium tuberculosis AhpD, an Element of the Peroxiredoxin Defense against Oxidative Stress</p>

<p>           Koshkin, A, Zhou, XT, Kraus, CN, Brenner, JM, Bandyopadhyay, P, Kuntz, ID, Barry, CE 3rd, and Ortiz, De Montellano PR</p>

<p>           Antimicrob Agents Chemother 2004.  48(7): 2424-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215090&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215090&amp;dopt=abstract</a> </p><br />

<p>     7.    43644   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           In-vitro activity of rifabutin against rifampicin-resistant Mycobacterium tuberculosis isolates with known rpoB mutations</p>

<p>           Cavusoglu, C, Karaca-Derici, Y, and Bilgic, A</p>

<p>           Clin Microbiol Infect 2004. 10(7): 662-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15214882&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15214882&amp;dopt=abstract</a> </p><br />

<p>     8.    43645   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Antimycobacterial activity and cytotoxicity of flavonoids from the flowers of Chromolaena odorata</p>

<p>           Suksamrarn, A, Chotipong, A, Suavansri, T, Boongird, S, Timsuksai, P, Vimuttipong, S, and Chuaynugul, A</p>

<p>           Arch Pharm Res 2004. 27(5): 507-11</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15202555&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15202555&amp;dopt=abstract</a> </p><br />

<p>     9.    43646   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Multiple effects of green tea catechin on the antifungal activity of antimycotics against Candida albicans</p>

<p>           Hirasawa, Masatomo and Takada, Kazuko</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(2): 225-229</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   10.    43647   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Antituberculosis agents X. Synthesis and evaluation of in vitro antituberculosis activity of 2-(5-nitro-2-furyl)- and 2-(1-methyl-5-nitro-1H-imidazol-2-yl)-1,3,4-thiadiazole derivatives</p>

<p>           Foroumadi, A, Soltani, F, Jabini, R, Moshafi, MH, and Rasnani, FM</p>

<p>           Arch Pharm Res 2004. 27(5): 502-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15202554&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15202554&amp;dopt=abstract</a> </p><br />

<p>   11.    43648   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Candida and candidiasis: The cell wall as a potential molecular target for antifungal therapy</p>

<p>           Gozalbo, Daniel, Roig, Patricia, Villamon, Eva, and Gil, Maria Luisa</p>

<p>           Current Drug Targets: Infectious Disorders 2004. 4(2): 117-135</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.     43649   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Antifungal and antimycobacterial activity of new N1-[1-aryl-2-(1H-imidazol-1-yl and 1H-1,2,4-triazol-1-yl)-ethylidene]-pyridine-2-carboxamidrazone derivatives: a combined experimental and computational approach</p>

<p>           Mamolo, Maria Grazia, Zampieri, Daniele, Falagiani, Valeria, Vio, Luciano, Fermeglia, Maurizio, Ferrone, Marco, Pricl, Sabrina, Banfi, Elena, and Scialino, Giuditta</p>

<p>           ARKIVOC (Gainesville, FL, United States) 2004(5): 231-250 </p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   13.    43650   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p><b>           Preparation of novel macrolide derivatives having effect of potentiating antifungal activity of azole fungicides</b> ((The Kitasato Institute, Japan)</p>

<p>           Omura, Satoshi, Tomoda, Hiroshi, Sunazuka, Toshiaki, Arai, Masayoshi, and Nagamitsu, Tohru</p>

<p>           PATENT: WO 2004039823 A1;  ISSUE DATE: 20040513</p>

<p>           APPLICATION: 2002; PP: 58 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   14.    43651   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Novel pyridazino[4,3-b]indoles with dual inhibitory activity against Mycobacterium tuberculosis and monoamine oxidase</p>

<p>           Velezheva, VS, Brennan, PJ, Marshakov, VY, Gusev, DV, Lisichkina, IN, Peregudov, AS, Tchernousova, LN, Smirnova, TG, Andreevskaya, SN, and Medvedev, AE</p>

<p>           J Med Chem 2004. 47(13): 3455-61</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15189042&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15189042&amp;dopt=abstract</a> </p><br />

<p>   15.    43652   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Synthesis of 3,5-disubstituted isoxazolines as potential antibacterial and antifungal agents</p>

<p>           Imran, Mohd and Khan, Suroor A</p>

<p>           Indian Journal of Heterocyclic Chemistry 2004. 13(3): 213-216</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   16.    43653   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Tuberculosis drug resistance: summary report for 2003</p>

<p>           Anon</p>

<p>           Can Commun Dis Rep 2004. 30(10): 93-95</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15174496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15174496&amp;dopt=abstract</a> </p><br />

<p>   17.    43654   OI-LS-299; PUBMED-OI-6/29/2004</p>

<p class="memofmt1-3">           Cryptosporidium parvum: effect of multi-drug reversing agents on the expression and function of ATP-binding cassette transporters</p>

<p>           Bonafonte, MT, Romagnoli, PA, McNair, N, Shaw, AP, Scanlon, M, Leitch, GJ, and Mead, JR</p>

<p>           Exp Parasitol  2004. 106(3-4): 126-34</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15172220&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15172220&amp;dopt=abstract</a> </p><br />

<p>  18.     43655   OI-LS-299; WOS-OI-6/21/2004</p>

<p class="memofmt1-3">           Antimicrobial and antiviral activities of an actinomycete (Streptomyces sp.) isolated from a Brazilian tropical forest soil</p>

<p>           Sacramento, DR, Coelho, RRR, Wigg, MD, Linhares, LFDL, dos, Santos MGM, Samedo, LTDS, and da, Silva AJR</p>

<p>           WORLD J MICROB BIOT 2004. 20(3): 225-229</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221544200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221544200002</a> </p><br />

<p>   19.    43656   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Synthesis and antifungal activity of 3,5-disubstituted phenyl imino-1,2,4-triazoles</p>

<p>           Siddiqui, Anees A, Islam, Mojahid Ul, and Siddiqui, Nadeem</p>

<p>           Asian Journal of Chemistry 2004. 16(1): 534-536</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   20.    43657   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Antifungal cobalt(II), copper(II), nickel(II) and zinc(II) complexes of furanyl-, thiophenyl-, pyrrolyl-, salicylyl- and pyridyl-derived cephalexins</p>

<p>           Chohan, Zahid H, Pervez, Humayun, Khan, Khalid M, Rauf, A, Maharvi, Ghulam M, and Supuran, Claudiu T</p>

<p>           Journal of Enzyme Inhibition and Medicinal Chemistry 2004. 19(1): 85-90</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   21.    43658   OI-LS-299; WOS-OI-6/21/2004</p>

<p class="memofmt1-3">           Inability of Pneumocystis organisms to incorporate bromodeoxyuridine suggests the absence of a salvage pathway for thymidine</p>

<p>           Vestereng, VH and Kovacs, JA</p>

<p>           MICROBIOL-SGM  2004. 150: 1179-1182</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221538000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221538000013</a> </p><br />

<p>   22.    43659   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Role of KatG catalase-peroxidase in mycobacterial pathogenesis: Countering the phagocyte oxidative burst</p>

<p>           Ng, Vincent H, Cox, Jeffery S, Sousa, Alexandra O, MacMicking, John D, and McKinney, John D</p>

<p>           Molecular Microbiology 2004. 52(5): 1291-1302</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   23.    43660   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p><b>           Anti-microbial agents derived from methionine sulfoximine analogues and use for treating mycobacterial infections</b> ((Regents of the University of California, USA)</p>

<p>           Harth, Gunter, Griffith, Owen W, and Horwitz, Marcus A</p>

<p>           PATENT: WO 2004045539 A2;  ISSUE DATE: 20040603</p>

<p>           APPLICATION: 2003; PP: 40 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   24.    43661   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Antineoplastic Agents. 499. Synthesis of Hystatin 2 and Related 1H-Benzo[de][1,6]naphthyridinium Salts from Aaptamine</p>

<p>           Pettit, George R, Hoffmann, Holger, Herald, Delbert L, Blumberg, Peter M, Hamel, Ernest, Schmidt, Jean M, Chang, Yung, Pettit, Robin K, Lewin, Nancy E, and Pearce, Larry V</p>

<p>           Journal of Medicinal Chemistry 2004.  47(7): 1775-1782</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.     43662   OI-LS-299; WOS-OI-6/21/2004</p>

<p class="memofmt1-3">           RUTI, a useful new immunotherapie coadjuvant for treatment of Mycobacterium tuberculosis infection</p>

<p>           Cardona, PJ, Amat, I, Gordillo, S, Arcos, V, Guirado, E, Diaz, J, Pujol-Borrell, R, and Ausina, V</p>

<p>           FASEB J 2004. 18(5): A825-A826</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220470700322">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220470700322</a> </p><br />

<p>   26.    43663   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Mechanisms of drug resistance in Mycobacterium tuberculosis</p>

<p>           Wade, Mary Margaret and Zhang, Ying</p>

<p>           Frontiers in Bioscience 2004. 9: 975-994</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   27.    43664   OI-LS-299; WOS-OI-6/21/2004</p>

<p class="memofmt1-3">           Natural products in the process of finding new drug candidates</p>

<p>           Vuorela, P, Leinonen, M, Saikku, P, Tammela, P, Rauha, JP, Wennberg, T, and Vuorela, H</p>

<p>           CURR MED CHEM  2004. 11(11): 1375-1389</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221574400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221574400003</a> </p><br />

<p>   28.    43665   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Interaction of antimycobacterial drugs with the anti-Mycobacterium avium complex effects of antimicrobial effectors, reactive oxygen intermediates, reactive nitrogen intermediates, and free fatty acids produced by macrophages</p>

<p>           Sano, Keisuke, Tomioka, Haruaki, Sato, Katsumasa, Sano, Chiaki, Kawauchi, Hideyuki, Cai, Shanshan, and Shimizu, Toshiaki</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(6): 2132-2139</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   29.    43666   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Novel phosphonate nucleosides as antiviral agents</p>

<p>           Hwang, Jae-Taeg and Choi, Jong-Ryoo</p>

<p>           Drugs of the Future 2004. 29(2): 163-177</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   30.    43667   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p><b>           Aromatic polyketide intermediates as selective anticancer and antiviral agents</b>((Galilaeus OY, Finland)</p>

<p>           Kunnari, Tero and Vuento, Matti</p>

<p>           PATENT: WO 2004045600 A1;  ISSUE DATE: 20040603</p>

<p>           APPLICATION: 2003; PP: 18 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   31.    43668   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           New synthetic glutathione derivatives with increased antiviral activities</p>

<p>           Palamara, Anna Teresa, Brandi, Giorgio, Rossi, Luigia, Millo, Enrico, Benatti, Umberto, Nencioni, Lucia, Iuvara, Alessandra, Garaci, Enrico, and Magnani, Mauro</p>

<p>           Antiviral Chemistry &amp; Chemotherapy 2004. 15(2): 83-91</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   32.    43669   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Antiviral activity of antimicrobial cationic peptides against Junin virus and herpes simplex virus</p>

<p>           Albiol Matanic, Vanesa C and Castilla, Viviana</p>

<p>           International Journal of Antimicrobial Agents 2004. 23(4): 382-389</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.     43670   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           In vitro activity of expanded-spectrum pyridazinyl oxime ethers related to pirodavir: Novel capsid-binding inhibitors with potent antipicornavirus activity</p>

<p>           Barnard, DL, Hubbard, VD, Smee, DF, Sidwell, RW, Watson, KGW, Tucker, SPT, and Reece, PAR</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(5): 1766-1772</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   34.    43671   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           A Study on the Antipicornavirus Activity of Flavonoid Compounds (Flavones) by Using Quantum Chemical and Chemometric Methods</p>

<p>           Souza, Jaime Jr, Molfetta, Fabio A, Honorio, Kathia M, Santos, Regina HA, and da Silva, Alberico BF</p>

<p>           Journal of Chemical Information and Computer Sciences 2004. 44(3): 1153-1161</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   35.    43672   OI-LS-299; SCIFINDER-OI-6/23/2004</p>

<p class="memofmt1-3">           Voriconazole for serious fungal infections</p>

<p>           Gothard, P and Rogers, TR</p>

<p>           International Journal of Clinical Practice 2004. 58(1): 74-80</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
