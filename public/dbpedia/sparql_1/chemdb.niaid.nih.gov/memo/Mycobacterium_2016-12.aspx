

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Y0Tbv71YGaCf9IrwcgSZE1Pix9xxCBOLvLHGjVDENbS1A2PP0NlzHd3fkKmA0yng46kZ0lU8JYZTWT43QjUH/CsX+fs4S3e3RfMxBayd5ltFWLM8cIRNzrKzAWw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5C502E98" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: December, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27973453">Evaluation of Chemical Composition and Antileishmanial and Antituberculosis Activities of Essential Oils of Piper Species.</a> Bernuci, K.Z., C.C. Iwanaga, C.M. Fernadez-Andrade, F.B. Lorenzetti, E.C. Torres-Santos, V.D. Faioes, J.E. Goncalves, W. do Amaral, C. Deschamps, R.B. Scodro, R.F. Cardoso, V.P. Baldin, and D.A. Cortez. Molecules, 2016. 21(1698): 12pp. PMID[27973453].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27671498">Identification and Optimization of a New Series of Anti-tubercular Quinazolinones.</a> Couturier, C., C. Lair, A. Pellet, A. Upton, T. Kaneko, C. Perron, E. Cogo, J. Menegotto, A. Bauer, B. Scheiper, S. Lagrange, and E. Bacque. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(21): p. 5290-5299. PMID[27671498].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27982051">Novel Inhibitors of Mycobacterium tuberculosis GuaB2 Identified by a Target Based High-throughput Phenotypic Screen.</a> Cox, J.A., G. Mugumbate, L.V. Del Peral, M. Jankute, K.A. Abrahams, P. Jervis, S. Jackenkroll, A. Perez, C. Alemparte, J. Esquivias, J. Lelievre, F. Ramon, D. Barros, L. Ballell, and G.S. Besra. Scientific Reports, 2016. 6( 38986): 10pp. PMID[27982051]. PMCID[PMC5159837].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27876209">Synthesis and Biological Evaluation against Mycobacterium tuberculosis and Leishmania amazonensis of a Series of Diaminated Terpenoids.</a> Dos Reis, D.B., T.C. Souza, M.C. Lourenco, M.V. de Almeida, A. Barbosa, I. Eger, and M.F. Saraiva. Biomedicine &amp; Pharmacotherapy, 2016. 84: p. 1739-1747. PMID[27876209]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27591998">Synthesis and Evaluation of Copper(II) Complexes with Isoniazid-derived Hydrazones as Anticancer and Antitubercular Agents.</a> Firmino, G.S., M.V. de Souza, C. Pessoa, M.C. Lourenco, J.A. Resende, and J.A. Lessa. Biometals, 2016. 29(6): p. 953-963. PMID[27591998]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27976349">Activity of Selected West African Medicinal Plants against Mycobacterium ulcerans Disease.</a> Fokou, P., A.A. Kissi-Twum, D. Yeboah-Manu, R. Appiah-Oppong, P. Addo, L. Yamthe, A.N. Mfopa, F.F. Boyom, and A.K. Nyarko. Planta Medica, 2016. 81(S 01): p. S1-S381. PMID[27976349]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27994767">Discovery and Optimization of Two Eis Inhibitor Families as Kanamycin Adjuvants against Drug-resistant M. tuberculosis.</a> Garzan, A., M.J. Willby, K.D. Green, O.V. Tsodikov, J.E. Posey, and S. Garneau-Tsodikova. ACS Medicinal Chemistry Letters, 2016. 7(12): p. 1219-1221. PMID[27994767].  PMCID[PMC5150678].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27619156">Antimycobacterial Activity and Low Cytotoxicity of Leaf Extracts of Some African Anacardiaceae Tree Species.</a> Kabongo-Kayoka, P.N., J.N. Eloff, C.L. Obi, and L.J. McGaw. Phytotherapy Research, 2016. 30(12): p. 2001-2011. PMID[27619156]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27773800">In Vitro and in Vivo Antimycobacterial, Hepatoprotective and Immunomodulatory Activity of Euclea natalensis and Its Mode of Action.</a> Lall, N., V. Kumar, D. Meyer, N. Gasa, C. Hamilton, M. Matsabisa, and C. Oosthuizen. Journal of Ethnopharmacology, 2016. 194: p. 740-748. PMID[27773800]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27847948">Synthesis of Ramariolide Natural Products and Discovery of Their Targets in Mycobacteria.</a> Lehmann, J., J. Richers, A. Pothig, and S.A. Sieber. Chemical Communications, 2016. 53(1): p. 107-110. PMID[27847948]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27866817">Discovery of Novel N-Phenyl 1,4-dihydropyridines with a Dual Mode of Antimycobacterial Activity.</a> Lentz, F., M. Hemmer, N. Reiling, and A. Hilgeroth. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(24): p. 5896-5898. PMID[27866817]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27839917">Discovery of New Chemical Entities as Potential Leads against Mycobacterium tuberculosis.</a> Lu, X., J. Tang, Z. Liu, M. Li, T. Zhang, X. Zhang, and K. Ding. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(24): p. 5916-5919. PMID[27839917]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000387621500026">Synthesis and Evaluation of Novel Imidazo[4,5-c]pyridine Derivatives as Antimycobacterial Agents against Mycobacterium tuberculosis.</a> Madaiah, M., M.K. Prashanth, H.D. Revanasiddappa, and B. Veeresh. New Journal of Chemistry, 2016. 40(11): p. 9194-9204. ISI[000387621500026]. <b><br />
    [WOS].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000388111900092">N-(2-Bromo-4-fluorophenyl)-3-(3,4-dihydroxyphenyl)-acrylamide (CPAM), a Small Catecholic amide as an Antioxidant, Anti Diabetic and Antibacterial Compound.</a> Misra, K., A. Nag, and A. Sonawane. RSC Advances, 2016. 6(106): p. 104632-104641. ISI[000388111900092]. <b><br />
    [WOS].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26454420">Anti-mycobacterium tuberculosis Activity of Antituberculosis Drugs and Amoxicillin/Clavulanate Combination.</a> Pagliotto, A.D., K.R. Caleffi-Ferracioli, M.A. Lopes, V.P. Baldin, C.Q. Leite, F.R. Pavan, R.B. Scodro, V.L. Siqueira, and R.F. Cardoso. Journal of Microbiology Immunology, and Infection, 2016. 49(6): p. 980-983. PMID[26454420]. <b><br />
    [PubMed].</b> TB_12_2016.</p>
    
    <br />
 
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27622287">Discovery of Allosteric and Selective Inhibitors of Inorganic Pyrophosphatase from Mycobacterium tuberculosis.</a> Pang, A.H., A. Garzan, M.J. Larsen, T.J. McQuade, S. Garneau-Tsodikova, and O.V. Tsodikov. ACS Chemical Biology, 2016. 11(11): p. 3084-3092. PMID[27622287]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389761200002">Discovery of Novel Phthalimide Analogs: Synthesis, Antimicrobial and Antitubercular Screening with Molecular Docking Studies.</a> Rateb, H.S., H.E.A. Ahmed, S. Ahmed, S. Ihmaid, and T.H. Afifi. EXCLI Journal, 2016. 15: p. 781-796. ISI[000389761200002]. <b><br />
    [WOS].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28028468">The Tuberculocidal Activity of Polyaniline and Functionalised Polyanilines.</a> Robertson, J., J. Dalton, S. Wiles, M. Gizdavic-Nikolaidis, and S. Swift. PeerJ, 2016. 4(e2795): 15pp. PMID[28028468]. PMCID[PMC5178338].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27689997">Preparation, Characterization and in Vivo Antimycobacterial Studies of Panchovillin-chitosan Nanocomposites.</a> Rwegasila, E., E.B. Mubofu, S.S. Nyandoro, P. Erasto, and J.J.E. Munissi. International Journal of Molecular Sciences, 2016. 17(1559): 16pp. PMID[27689997]. PMCID[PMC5085621].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26730986">In Silico Driven Design and Synthesis of Rhodanine Derivatives as Novel Antibacterials Targeting the Enoyl Reductase InhA.</a> Slepikas, L., G. Chiriano, R. Perozzo, S. Tardy, A. Kranjc, O. Patthey-Vuadens, H. Ouertatani-Sakouhi, S. Kicka, C.F. Harrison, T. Scrignari, K. Perron, H. Hilbi, T. Soldati, P. Cosson, E. Tarasevicius, and L. Scapozza. Journal of Medicinal Chemistry, 2016. 59(24): p. 10917-10928. PMID[26730986]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27864515">Discovery of Cofactor-specific, Bactericidal Mycobacterium tuberculosis InhA Inhibitors Using DNA-encoded Library Technology.</a> Soutter, H.H., P. Centrella, M.A. Clark, J.W. Cuozzo, C.E. Dumelin, M.A. Guie, S. Habeshian, A.D. Keefe, K.M. Kennedy, E.A. Sigel, D.M. Troast, Y. Zhang, A.D. Ferguson, G. Davies, E.R. Stead, J. Breed, P. Madhavapeddi, and J.A. Read. Proceedings of the National Academy of Sciences of the United States of America, 2016. 113(49): p. E7880-E7889. PMID[27864515]. PMCID[PMC5150407].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27936766">Discovery of Mycobacterium tuberculosis InhA Inhibitors by Binding Sites Comparison and Ligands Prediction.</a> Stular, T., S. Lesnik, K. Rozman, J. Schink, M. Zdouc, A. Ghysels, F. Liu, C.C. Aldrich, V.J. Haupt, S. Salentin, S. Daminelli, M. Schroeder, T. Langer, S. Gobec, D. Janezic, and J. Konc. Journal of Medicinal Chemistry, 2016. 59(24): p. 11069-11078. PMID[27936766]. <b><br />
    [PubMed].</b> TB_12_2016.</p>
    
    <br />
 
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27769668">The Synthesis and Biological Evaluation of Alkyl and Benzyl Naphthyridinium Analogs of Eupolauridine as Potential Antimicrobial and Cytotoxic Agents.</a> Taghavi-Moghadam, S., C.D. Kwong, J.A. Secrist, 3rd, S.I. Khan, and A.M. Clark. Bioorganic &amp; Medicinal Chemistry, 2016. 24(23): p. 6119-6130. PMID[27769668]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27839684">Design, Development of New Synthetic Methodology, and Biological Evaluation of Substituted Quinolines as New Anti-tubercular Leads.</a> Tanwar, B., A. Kumar, P. Yogeeswari, D. Sriram, and A.K. Chakraborti. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(24): p. 5960-5966. PMID[27839684]. <b><br />
    [PubMed].</b> TB_12_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27994751">Identification of N-(2-Phenoxyethyl)imidazo[1,2-a]pyridine-3-carboxamides as New Antituberculosis Agents.</a> Wu, Z., Y. Lu, L. Li, R. Zhao, B. Wang, K. Lv, M. Liu, and X. You. ACS Medicinal Chemistry Letters, 2016. 7(12): p. 1130-1133. PMID[27994751]. PMCID[PMC5150680].<b><br />
    [PubMed].</b> TB_12_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161215&amp;CC=WO&amp;NR=2016198842A1&amp;KC=A1">An Amino Thiol for Use in the Treatment of an Infection Caused by the Bacterium Mycobacterium spp.</a> O&#39;Neil, D. and D. Fraser-Pitt. Patent. 2016. 2016-GB51662 2016198842: 31pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2016.</p>

    <br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161208&amp;CC=WO&amp;NR=2016196569A1&amp;KC=A1">Small Molecule Inhibitors of Protein Tyrosine Phosphatases and Uses Thereof.</a> Zhang, Z.-Y. and R. He. Patent. 2016. 2016-US35181 2016196569: 130pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
