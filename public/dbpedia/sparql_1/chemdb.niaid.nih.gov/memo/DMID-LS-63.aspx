

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-63.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="SVudqkDtTbxXtPlvYbnLeJCFr1fV03AC74Y2T+FWFTmDBfco/fL/SQctJYr6n0nv3qQZtUR7kIFgNEptJqwtm1wQ5u4eROmZfmhSnSZ28uNAi1GiCAmKweILRu8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6E6C0902" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - DMID-LS-63-MEMO</b> </p>

<p>    1.    5792   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           A novel method of resistance for influenza against a channel-blocking antiviral drug</p>

<p>           Astrahan, P, Kass, I, Cooper, MA, and Arkin, IT</p>

<p>           Proteins 2004. 55(2): 251-7</p>

<p>           <!--HYPERLNK:-->  <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15048819&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15048819&amp;dopt=abstract</a> </p><br />

<p>    2.    5793   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Inhibition of Different Lassa Virus Strains by Alpha and Gamma Interferons and Comparison With a Less Pathogenic Arenavirus</b></p>

<p>           Asper, M, Sternsdorf, T, Hass, M, Drosten, C, Rhode, A, Schmitz, H, and Gunther, S</p>

<p>           Journal of Virology 2004. 78(6): 3162-3169</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    3.    5794   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Microbial Transformation of Isosteviol and Inhibitory Effects on Epstein-Barr Virus Activation of the Transformation Products</p>

<p>           Akihisa, T, Hamasaki, Y, Tokuda, H, Ukiya, M, Kimura, Y, and Nishino, H</p>

<p>           J Nat Prod 2004. 67(3): 407-410</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043419&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043419&amp;dopt=abstract</a> </p><br />

<p>    4.    5795   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Nucleoside analogues exerting antiviral activity through a non-nucleoside mechanism</p>

<p>           De Clercq, E</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 457-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043166&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043166&amp;dopt=abstract</a> </p><br />

<p>    5.    5796   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Anti-cowpox virus activities of certain adenosine analogs, arabinofuranosyl nucleosides, and 2&#39;-fluoro-arabinofuranosyl nucleosides</p>

<p>           Smee, DF and Sidwell, RW</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 375-83</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043161&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043161&amp;dopt=abstract</a> </p><br />

<p>    6.    5797   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           1-Deaza-5&#39;-noraisteromycin</p>

<p>           Yin, X and Schneller, SW</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 67-76</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043137&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043137&amp;dopt=abstract</a> </p><br />

<p>    7.    5798   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Synthesis and antiviral activity of novel fluorinated 2&#39;,3&#39;-dideoxynucleosides</p>

<p>           Kumar, P, Ohkura, K, Balzarini, J, De Clercq, E, Seki, K, and Wiebe, LI</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 7-29</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043133&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043133&amp;dopt=abstract</a> </p><br />

<p>    8.    5799   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           Pharmaceutical composition for the treatment of viral, fungal and bacterial infections</b>((Spain))</p>

<p>           Buil Torres, Luis and Laza Garcia, Jose Maria</p>

<p>           PATENT: WO 2004019682 A1;  ISSUE DATE: 20040311</p>

<p>           APPLICATION: 2002; PP: 36 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    9.    5800   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Synthesis and antiviral evaluation of some 3&#39;-fluoro bicyclic nucleoside analogues</p>

<p>           McGuigan, C, Carangio, A, Snoeck, R, Andrei, G, De Clercq, E, and Balzarini, J</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 1-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043132&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043132&amp;dopt=abstract</a> </p><br />

<p>  10.    5801   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Ribozymes as antiviral agents</p>

<p>           Bartolome, J, Castillo, I, and Carreno, V</p>

<p>           Minerva Med 2004. 95(1): 11-24</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15041923&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15041923&amp;dopt=abstract</a> </p><br />

<p>  11.    5802   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Inhibition of Dugbe nairovirus replication by human MxA protein</p>

<p>           Bridgen, Anne, Dalrymple, David A, Weber, Friedemann, and Elliott, Richard M</p>

<p>           Virus Research 2004. 99(1): 47-50</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    5803   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Hepatitis C virus NS5A protein interacts with 2&#39;,5&#39;-oligoadenylate synthetase and inhibits antiviral activity of IFN in an IFN sensitivity-determining region-independent manner</p>

<p>           Taguchi, T, Nagano-Fujii, M, Akutsu, M, Kadoya, H, Ohgimoto, S, Ishido, S, and Hotta, H</p>

<p>           J Gen Virol 2004. 85(Pt 4): 959-69</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15039538&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15039538&amp;dopt=abstract</a> </p><br />

<p>  13.    5804   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Influenza infection of the embryonated hen&#39;s egg/an alternative model for in vivo evaluation of antiviral compounds</p>

<p>           Hartl, A, Sauerbrei, A, Stelzner, A, and Wutzler, P</p>

<p>           Arzneimittelforschung 2004. 54(2): 130-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038464&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038464&amp;dopt=abstract</a> </p><br />

<p>   14.   5805   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           Glycopeptide antibiotic derivatives, their preparation and their use as antiviral agents</b>((K.U. Leuven Research and Development, Belg.)</p>

<p>           Balzarini, Jan, Preobrazhenskaya, Maria, and De Clercq, Erik</p>

<p>           PATENT: WO 2004019970 A2;  ISSUE DATE: 20040311</p>

<p>           APPLICATION: 2003; PP: 91 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    5806   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           SARS--beginning to understand a new virus</p>

<p>           Stadler, K, Masignani, V, Eickmann, M, Becker, S, Abrignani, S, Klenk, HD, and Rappuoli, R</p>

<p>           Nat Rev Microbiol 2003. 1(3): 209-18</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15035025&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15035025&amp;dopt=abstract</a> </p><br />

<p>  16.    5807   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Inhibition of Epstein-Barr virus-induced growth proliferation by a nuclear antigen EBNA2-TAT peptide</p>

<p>           Farrell, CJ, Lee, JM, Shin, EC, Cebrat, M, Cole, PA, and Hayward, SD</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15034184&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15034184&amp;dopt=abstract</a> </p><br />

<p>  17.    5808   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Containing pandemic influenza with antiviral agents</p>

<p>           Longini, IM Jr, Halloran, ME, Nizam, A, and Yang, Y</p>

<p>           Am J Epidemiol 2004. 159(7): 623-33</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15033640&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15033640&amp;dopt=abstract</a> </p><br />

<p>  18.    5809   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Antiviral therapy for chronic hepatitis B: a review</p>

<p>           Hanazaki, K</p>

<p>           Curr Drug Targets Inflamm Allergy 2004. 3(1): 63-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15032642&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15032642&amp;dopt=abstract</a> </p><br />

<p>  19.    5810   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Inhibition of Hepatitis B Virus Replication by APOBEC3G</p>

<p>           Turelli, P, Mangeat, B, Jost, S, Vianin, S, and Trono, D</p>

<p>           Science 2004. 303(5665): 1829</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15031497&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15031497&amp;dopt=abstract</a> </p><br />

<p>  20.    5811   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Discovery of Biln 2061 - a Small Molecule Inhibitor of the Hepatitis C Virus Serine Protease.</b></p>

<p>           Llinas-Brunet, M, Bailey, M, Bolger, G, Cameron, D, Cartier, M, Faucher, AM, Goudreau, N, Kukolj, G, Lagace, L, Pause, A, Rancourt, J, Thibeault, D, Tsantrizos, Y, and Lamarre, D</p>

<p>           Abstracts of Papers of the American Chemical Society 2003. 225: 320-MEDI</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    5812   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Synthesis and antimicrobial activity of some novel 6-bromo-2-methyl/phenyl-3-(sulphonamido) quinazolin-4(3H)-ones</p>

<p>           Selvam, P, Vanitha, K, Chandramohan, M, and De Clerco, E</p>

<p>           Indian Journal of Pharmaceutical Sciences 2004. 66(1): 82-86</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    5813   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Discovery of a New Family of Inhibitors of Human Cytomegalovirus (HCMV) Based upon Lipophilic Alkyl Furano Pyrimidine Dideoxy Nucleosides: Action via a Novel Non-Nucleosidic Mechanism</p>

<p>           McGuigan, C, Pathirana, RN, Snoeck, R, Andrei, G, De Clercq, E, and Balzarini, J</p>

<p>           J Med Chem 2004. 47(7): 1847-51</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027877&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027877&amp;dopt=abstract</a> </p><br />

<p>  23.    5814   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Remission of HHV-8 and HIV-associated multi-centric Castleman&#39;s disease with ganciclovir treatment</p>

<p>           Casper, Corey, Nichols, WGarrett, Huang, Meei-Li, Corey, Lawrence, and Wald, Anna</p>

<p>           Blood 2004. 103(5): 1632-1634</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    5815   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Antiviral agents for treatment of herpes</p>

<p>           Hong, Jian Eric</p>

<p>           Frontiers of Biotechnology &amp; Pharmaceuticals 2004. 4: 147-180</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    5816   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Structure-Activity Study on a Novel Series of Macrocyclic Inhibitors of the Hepatitis C Virus NS3 Protease Leading to the Discovery of BILN 2061</p>

<p>           Llinas-Brunet, M, Bailey, MD, Bolger, G, Brochu, C, Faucher, AM, Ferland, JM, Garneau, M, Ghiro, E, Gorys, V, Grand-Maitre, C, Halmos, T, Lapeyre-Paquette, N, Liard, F, Poirier, M, Rheaume, M, Tsantrizos, YS, and Lamarre, D</p>

<p>           J Med Chem 2004. 47(7): 1605-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027850&amp;dopt=abstract</a> </p><br />

<p>  26.    5817   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Oligonucleotide-based strategies to inhibit human hepatitis C virus</p>

<p>           Martinand-Mari, C, Lebleu, B, and Robbins, I</p>

<p>           Oligonucleotides 2003. 13(6): 539-48</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025918&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025918&amp;dopt=abstract</a> </p><br />

<p>  27.    5818   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Viral and cellular kinases are potential antiviral targets and have a central role in varicella zoster virus pathogenesis</p>

<p>           Moffat, JF, McMichael, MA, Leisenfelder, SA, and Taylor, SL</p>

<p>           Biochim Biophys Acta 2004. 1697(1-2): 225-31</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15023363&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15023363&amp;dopt=abstract</a> </p><br />

<p>  28.    5819   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Adefovir Dipivoxil as a Treatment for Hepatic Failure Caused by Lamivudine-Resistant Hbv Strains</b></p>

<p>           Kuwahara, R,  Kumashiro, R, Inoue, H, Tanabe, R, Tanaka, E, Hino, T, Ide, T, Koga, Y, and Sata, M</p>

<p>           Digestive Diseases and Sciences 2004. 49(2): 300-303</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  29.    5820   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Zoonotic poxvirus infections in humans</p>

<p>           Lewis-Jones, S</p>

<p>           Curr Opin Infect Dis 2004. 17(2): 81-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15021045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15021045&amp;dopt=abstract</a> </p><br />

<p>  30.    5821   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Inhibition of hepatitis C virus IRES-mediated translation by small RNAs analogous to stem-loop structures of the 5&#39;-untranslated region</p>

<p>           Ray, PS and Das, S</p>

<p>           Nucleic Acids Res 2004. 32(5): 1678-1687</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15020704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15020704&amp;dopt=abstract</a> </p><br />

<p>  31.    5822   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Antiviral activity of hop constituents against a series of DNA and RNA viruses</p>

<p>           Buckwold, Victor E, Wilson, Richard JH, Nalca, Aysegul, Beer, Brigitte B, Voss, Thomas G, Turpin, Jim A, Buckheit, Robert W III, Wei, Jiayi, Wenzel-Mathers, Michelle, Walton, Eve M, Smith, Robert J, Pallansch, Melanie, Ward, Priscilla, Wells, Jay, Chuvala, Lara, Sloane, Sandra, Paulman, Robin, Russell, Julie, Hartman, Tracy, and Ptak, Roger</p>

<p>           Antiviral Research 2004. 61(1): 57-62</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    5823   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           In vitro and in vivo antimicrobial activity of two alpha-helical cathelicidin peptides and of their synthetic analogs</p>

<p>           Benincasa, M, Skerlavaj, B, Gennaro, R, Pellegrini, A, and Zanetti, M</p>

<p>           Peptides 2003. 24(11): 1723-31</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15019203&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15019203&amp;dopt=abstract</a> </p><br />

<p>  33.    5824   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Design of N-acetyl-6-sulfo-beta-d-glucosaminide-based inhibitors of influenza virus sialidase</p>

<p>           Sasaki, K, Nishida, Y, Kambara, M, Uzawa, H, Takahashi, T, Suzuki, T, Suzuki, Y, and Kobayashi, K</p>

<p>           Bioorg Med Chem 2004. 12(6): 1367-75</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15018909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15018909&amp;dopt=abstract</a> </p><br />

<p>  34.    5825   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           RNase P Ribozyme As an Antiviral Agent Against Human Cytomegalovirus</p>

<p>           Trang, P and Liu, F</p>

<p>           Methods Mol Biol 2004. 252: 437-50</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15017069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15017069&amp;dopt=abstract</a> </p><br />

<p>  35.    5826   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           Use of triptans as antiviral agents</b> ((Italy))</p>

<p>           Cassar Scalia, Antonio</p>

<p>           PATENT: WO 2004019934 A1;  ISSUE DATE: 20040311</p>

<p>           APPLICATION: 2003; PP: 10 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.    5827   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Antiviral activity of polyphenolic compounds KV8 and G6</p>

<p>           Levandovskaya, SV, Khudyakova, SS, Muzychkina, RA, Korulkin, DYu, Zhusupova, GE, Tolmacheva, VP, Bogoyavlenskiy, AP, and Berezin, VE</p>

<p>           Izvestiya Natsional&#39;noi Akademii Nauk Respubliki Kazakhstan, Seriya Biologicheskaya i Meditsinskaya 2003(2): 97-102</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  37.    5828   DMID-LS-63; PUBMED-DMID-3/30/2004</p>

<p class="memofmt1-2">           Characterization of severe acute respiratory syndrome-associated coronavirus (SARS-CoV) spike glycoprotein-mediated viral entry</p>

<p>           Simmons, G, Reeves, JD, Rennekamp, AJ, Amberg, SM, Piefer, AJ, and Bates, P</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(12): 4240-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15010527&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15010527&amp;dopt=abstract</a> </p><br />

<p>  38.    5829   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Activity of acyclovir, ciprofloxacin, and organotin polymers derived from acyclovir and ciprofloxacin against herpes simplex virus (HSV-1) and varicella zoster virus (VZV)</p>

<p>           Roner, Michael R, Carraher, Charles E, Zhao, Anna, Roehr, Joanne L, Bassett, Kelly D, and Siegmann-Louda, Deborah</p>

<p>           CONFERENCE: Abstracts of Papers, 227th ACS National Meeting, Anaheim, CA, United States, March 28-April 1, 2004 2004: PMSE-307</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  39.    5830   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Coronaviridae and Sars-Associated Coronavirus Strain Hsr1</b></p>

<p>           Vicenzi, E, Canducci, F, Pinna, D, Mancini, N, Carletti, S, Lazzarin, A, Bordignon, C, Poli, G, and Clementi, M</p>

<p>           Emerging Infectious Diseases 2004. 10(3): 413-418</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  40.    5831   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>A Randomized Controlled Trial of Amantadine Plus Interferon- Alpha 2a Vs. Interferon-Alpha 2a Alone in Naive Patients With Chronic Hepatitis C Randomized According to the Early Virological Response to Interferon-Alpha 2a Monotherapy</b></p>

<p>           Angelico, M,  Cepparulo, M, Angelico, F, Francioso, S, Barlattani, A, Di Candilo, F, Della Vecchia, R, Demelia, L, De Sanctis, G, Gentile, S, Grieco, A, Parruti, G, Sabusco, G, Tarquini, L, Tosti, A, and Zaru, S</p>

<p>           Alimentary Pharmacology &amp; Therapeutics 2004. 19(3): 339-347</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  41.    5832   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Inhibitory Effect of Ginseng Polysaccharides on Rotavirus Infection</b></p>

<p>           Bae, EA, Shin, JE, Park, SH, and Kim, DH</p>

<p>           Journal of Microbiology and Biotechnology 2004. 14(1): 202-204</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  42.    5833   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Dendritic Cells Pulsed with Polyomavirus BK Antigen Induce Ex Vivo Polyoma BK Virus-Specific Cytotoxic T-Cell Lines in Seropositive Healthy Individuals and Renal Transplant Recipients</p>

<p>           Comoli, Patrizia, Basso, Sabrina, Azzi, Alberta, Moretta, Antonia, De Santis, Riccardo, Del Galdo, Francesco, De Palma, Raffaele, Valente, Umberto, Nocera, Arcangelo, Perfumo, Francesco, Locatelli, Franco, Maccario, Rita, and Ginevri, Fabrizio</p>

<p>           Journal of the American Society of Nephrology 2003. 14(12): 3197-3204</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  43.    5834   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Catalytic Inactivation of Sars Coronavirus, Escherichia Coli and Yeast on Solid Surface</b></p>

<p>           He, H, Dong, XP, Yang, M, Yang, QX, Duan, SM, Yu, YB, Han, J, Zhang, CB, Chen, L, and Yang, X</p>

<p>           Catalysis Communications 2004. 5(3): 170-172</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  44.    5835   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Identification of a Novel Class of Inhibitors of Hepatitis Cns5b Polymerase.</b></p>

<p>           Proulx, M, Chan, L, Reddy, TJ, Das, SK, Pereira, SZ, Wang, WY, Siddiqui, A, Turcotte, N, Drouin, A, Yannopoulos, C, Poisson, C, Alaoui, HMA, Bethell, R, Hamel, M, Bilimoria, D, Nguyen-Ba, N, and L&#39;heureux, L</p>

<p>           Abstracts of Papers of the American Chemical Society 2003. 225: 40-MEDI</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  45.    5836   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Hepatitis B Virus Epidemiology, Disease Burden, Treatment, and Current and Emerging Prevention and Control Measures</b></p>

<p>           Lavanchy, D</p>

<p>           Journal of Viral Hepatitis 2004. 11(2): 97-107</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  46.    5837   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Viral Hepatitis: From a to E, and Beyond?</b></p>

<p>           Chen, DS</p>

<p>           Journal of the Formosan Medical Association 2003. 102(10): 671-679</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  47.    5838   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Analysis of hepatitis B virus dynamics and its impact on antiviral development</p>

<p>           Tsiang, Manuel and Gibbs, Craig S</p>

<p>           Methods in Molecular Medicine 2004.  96(Hepatitis B and D Protocols, Volume 2): 361-377</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  48.    5839   DMID-LS-63; WOS-DMID-3/30/2004</p>

<p>           <b>Phase Iii Study of Ranimustine, Cyclophosphamide, Vincristine, Melphalan, and Prednisolone (Mcnu-Cop/Mp) Versus Modified Cop/Mp in Multiple Myeloma: a Japan Clinical Oncology Group Study, Jcog 9301</b></p>

<p>           Takenaka, T,  Itoh, K, Suzuki, T, Utsunomiya, A, Matsuda, S, Chou, T, Sai, T, Sano, M, Konda, S, Ohno, T, Mikuni, C, Deura, K, Yamada, T, Mizorogi, F, Nagoshi, H, Tomonaga, M, Hotta, T, Kawano, K, Tsushita, K, Hirano, M, and Shimoyama, M</p>

<p>           International Journal of Hematology 2004. 79(2): 165-173</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  49.    5840   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Hepavir B (MB06866): A novel Hepatitis B antiviral therapy using the HepDirect- prodrug technology for targeting adefovir to the liver</p>

<p>           Reddy, KRaja, Ugarkar, Bheemarao G, Matelich, Michael C, Gomez-Galeno, Jorge E, Colby, Timothy J, Fujitaki, James M, van Poelje, Paul D, and Erion, Mark D</p>

<p>           CONFERENCE: Abstracts of Papers, 227th ACS National Meeting, Anaheim, CA, United States, March 28-April 1, 2004 2004: MEDI-342</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  50.    5841   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           Nucleoside derivatives as inhibitors of RNA-dependent RNA viral polymerase</b>((Merck &amp; Co., Inc. USA and Isis Pharmaceuticals, Inc.)</p>

<p>           Olsen, David B, Maccoss, Malcolm, Bhat, Balkrishen, and Eldrup, Anne B</p>

<p>           PATENT: WO 2004003138 A2;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 42 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  51.    5842   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Implications of finding synergic in vitro drug-drug interactions between interferon-a and ribavirin for the treatment of hepatitis C virus</p>

<p>           Buckwold, Victor E</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(3): 413-414</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  52.    5843   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p class="memofmt1-2">           Signal transduction pathways that inhibit hepatitis B virus replication</p>

<p>           Robek, Michael D, Boyd, Bryan S, Wieland, Stefan F, and Chisari, Francis V</p>

<p>           Proceedings of the National Academy of Sciences of the United States of America 2004. 101(6): 1743-1747</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  53.    5844   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           A cell-based dual assay for screening both HCV RNA replication inhibitory activity and cytotoxicity of a compound using cells transfected with HCV replicon</b>((Bristol-Myers Squibb Company, USA)</p>

<p>           Gao, Min, Lemm, Julie A, O&#39;Boyle, Donald R, Nower, Peter, Rigat, Karen, and Sun, Jin-hua</p>

<p>           PATENT: WO 2004015131 A2;  ISSUE DATE: 20040219</p>

<p>           APPLICATION: 2003; PP: 45 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  54.    5845   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           Preparation of iminothiazolidinone amino acid derivatives as combination pharmaceutical agents for use as inhibitors of HCV replication</b>((Bristol-Myers Squibb Company, USA)</p>

<p>           Colonno, Richard, Lemm, Julie, O&#39;Boyle, Donald, Gao, Min, Romine, Jeffrey Lee, Martin, Scott W, Snyder, Lawrence B, Serrano-Wu, Michael, Deshpande, Milind, and Whitehouse, Darren</p>

<p>           PATENT: WO 2004014313 A2;  ISSUE DATE: 20040219</p>

<p>           APPLICATION: 2003; PP: 129 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  55.    5846   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           Preparation of 1-arylnaphthalenes and related compounds as antiviral agents for the treatment of herpesviral infections</b>((Taiwan))</p>

<p>           Hsu, Tsu-an, Hsieh, Hsing-pang, Juan, Li-jung, Chang, Sui-yuan, and Kuo, Yueh-hsiung</p>

<p>           PATENT: US 20040044069 A1;  ISSUE DATE: 20040304</p>

<p>           APPLICATION: 2003-52052; PP: 10 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  56.    5847   DMID-LS-63; SCIFINDER-DMID-3/30/2004</p>

<p><b>           Preparation of vinylpyrimidones as antiviral agents</b>((Axxima Pharmaceuticals Ag, Germany and 4sc Ag))</p>

<p>           Missio, Andrea, Herget, Thomas, Aschenbrenner, Andrea, Kramer, Bernd, Leban, Johann, and Wolf, Kristina</p>

<p>           PATENT: WO 2004016271 A1;  ISSUE DATE: 20040226</p>

<p>           APPLICATION: 2003; PP: 89 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
