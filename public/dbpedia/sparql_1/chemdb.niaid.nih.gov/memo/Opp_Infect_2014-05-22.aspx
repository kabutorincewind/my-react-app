

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-05-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dWMy+hP7X5QI7TXK6ad7RBtvjtcg9y7tYyBOcGqqcHcpjOyL0Y9BJQ8Ug/D4OAFTmj6PCHXkAkDyQo7imbrFQsh29kP5dhb/rpkEKCJTExWiuOSoqRh4C7fdPtw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="63826A7A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: May 9 - May 22, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24564525">Design, Synthesis, and Structure-Activity Relationship Studies of Novel Fused Heterocycles-linked Triazoles with Good Activity and Water Solubility.</a>Cao, X., Z. Sun, Y. Cao, R. Wang, T. Cai, W. Chu, W. Hu, and Y. Yang. Journal of Medicinal Chemistry, 2014. 57(9): p. 3687-3706. PMID[24564525].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24817320">Antifungal Activity of Naphthoquinoidal Compounds in Vitro against Fluconazole-resistant Strains of Different Candida Species: A Special Emphasis on Mechanisms of Action on Candida tropicalis.</a>Neto, J.B., C.R. da Silva, M.A. Neta, R.S. Campos, J.T. Siebra, R.A. Silva, D.M. Gaspar, H.I. Magalhaes, M.O. de Moraes, M.D. Lobo, T.B. Grangeiro, T.S. Carvalho, E.B. Diogo, E.N. da Silva Junior, F.A. Rodrigues, B.C. Cavalcanti, and H.V. Junior. Plos One, 2014. 9(5): p. e93698. PMID[24817320].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24690774">Medicinal Compounds, Chemically and Biologically Characterised from Extracts of Australian Callitris endlicheri and C. glaucophylla (Cupressaceae): Used Traditionally in Aboriginal and Colonial Pharmacopoeia.</a> Sadgrove, N.J. and G.L. Jones. Journal of Ethnopharmacology, 2014. 153(3): p. 872-883. PMID[24690774].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24588790">Haloemodin as Novel Antibacterial Agent Inhibiting DNA Gyrase and Bacterial Topoisomerase I.</a> Duan, F., X. Li, S. Cai, G. Xin, Y. Wang, D. Du, S. He, B. Huang, X. Guo, H. Zhao, R. Zhang, L. Ma, Y. Liu, Q. Du, Z. Wei, Z. Xing, Y. Liang, X. Wu, C. Fan, C. Ji, D. Zeng, Q. Chen, Y. He, X. Liu, and W. Huang. Journal of Medicinal Chemistry, 2014. 57(9): p. 3707-3714. PMID[24588790].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24749867">SB-RA-2001 Inhibits Bacterial Proliferation by Targeting FtsZ Assembly.</a> Singh, D., A. Bhattacharya, A. Rai, H.P. Dhaked, D. Awasthi, I. Ojima, and D. Panda. Biochemistry, 2014. 53(18): p. 2979-2992. PMID[24749867].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24809681">A Genome Wide Association Study of Plasmodium falciparum Susceptibility to 22 Antimalarial Drugs in Kenya.</a>Wendler, J.P., J. Okombo, R. Amato, O. Miotto, S.M. Kiara, L. Mwai, L. Pole, J. O&#39;Brien, M. Manske, D. Alcock, E. Drury, M. Sanders, S.O. Oyola, C. Malangone, D. Jyothi, A. Miles, K.A. Rockett, B.L. Macinnis, K. Marsh, P. Bejon, A. Nzila, and D.P. Kwiatkowski. Plos One, 2014. 9(5): p. e96486. PMID[24809681].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24694175">Synthetic Calanolides with Bactericidal Activity against Replicating and Nonreplicating Mycobacterium tuberculosis.</a> Zheng, P., S. Somersan-Karakaya, S. Lu, J. Roberts, M. Pingle, T. Warrier, D. Little, X. Guo, S.J. Brickner, C.F. Nathan, B. Gold, and G. Liu. Journal of Medicinal Chemistry, 2014. 57(9): p. 3755-3772. PMID[24694175].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24838073">Antiprotozoal Activity of Buxus sempervirens and Activity-guided Isolation of O-Tigloylcyclovirobuxeine-B as the Main Constituent Active against Plasmodium falciparum.</a> Althaus, J.B., G. Jerz, P. Winterhalter, M. Kaiser, R. Brun, and T.J. Schmidt. Molecules, 2014. 19(5): p. 6184-6201. PMID[24838073].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24823881">In Vitro Antiprotozoal Activity of Abietane diterpenoids Isolated from Plectranthus barbatus Andr.</a> Mothana, R.A., M.S. Al-Said, N.M. Al-Musayeib, A.A. Gamal, S.M. Al-Massarani, A.J. Al-Rehaily, M. Abdulkader, and L. Maes. International Journal of Molecular Sciences, 2014. 15(5): p. 8360-8371. PMID[24823881].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24720377">Discovery, Synthesis, and Optimization of Antimalarial 4(1H)-Quinolone-3-diarylethers.</a> Nilsen, A., G.P. Miley, I.P. Forquer, M.W. Mather, K. Katneni, Y. Li, S. Pou, A.M. Pershing, A.M. Stickles, E. Ryan, J.X. Kelly, J.S. Doggett, K.L. White, D.J. Hinrichs, R.W. Winter, S.A. Charman, L.N. Zakharov, I. Bathurst, J.N. Burrows, A.B. Vaidya, and M.K. Riscoe. Journal of Medicinal Chemistry, 2014. 57(9): p. 3818-3834. PMID[24720377].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24810805">Comparative Study on the Antioxidant and Anti-toxoplasma Activities of Vanillin and Its Resorcinarene Derivative.</a>Oliveira, C.B., Y.S. Meurer, M.G. Oliveira, W.M. Medeiros, F.O. Silva, A.C. Brito, L. Pontes Dde, and V.F. Andrade-Neto. Molecules, 2014. 19(5): p. 5898-5912. PMID[24810805].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24773558">Nitrogen-containing Dimeric nor-Multiflorane Triterpene from a Turraea Sp.</a> Rasamison, V.E., L.H. Rakotondraibe, C. Slebodnick, P.J. Brodie, M. Ratsimbason, K. TenDyke, Y. Shen, L.M. Randrianjanaka, and D.G. Kingston. Organic Letters, 2014. 16(10): p. 2626-2629. PMID[24773558].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0509-052214.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334288300005">A Simple Approach to Fused Pyrido[2,3-d]pyrimidines Incorporating Khellinone and Trimethoxyphenyl Moieties as New Scaffolds for Antibacterial and Antifungal Agents.</a> Abu-Zied, K.M., T.K. Mohamed, O.K. Al-Duiaj, and M.E.A. Zaki. Heterocyclic Communications, 2014. 20(2): p. 93-102. ISI[000334288300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334226300001">Essential Oil of Artemisia annua L.: An Extraordinary Component with Numerous Antimicrobial Properties.</a> Bilia, A.R., F. Santomauro, C. Sacco, M.C. Bergonzi, and R. Donato. Evidence-based Complementary and Alternative Medicine, 2014. 159819: p. 7. ISI[000334226300001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333959600041">Effect of Alcohols on Filamentation, Growth, Viability and Biofilm Development in Candida albicans.</a> Chauhan, N.M., R.B. Shinde, and S.M. Karuppayil. Brazilian Journal of Microbiology, 2013. 44(4): p. 1315-1320. ISI[000333959600041].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334014700008">3D-QSAR and Molecular Docking Studies of Amino-pyrimidine Derivatives as PknB Inhibitors.</a> Damre, M.V., R.P. Gangwal, G.V. Dhoke, M. Lalit, D. Sharma, K. Khandelwal, and A.T. Sangamwar. Journal of the Taiwan Institute of Chemical Engineers, 2014. 45(2): p. 354-364. ISI[000334014700008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333884300045">Task-specific Ionic Liquid-mediated Facile Synthesis of 1,3,5-Triaryltriazines by Cyclotrimerization of Imines and their Biological Evaluation.</a> Dandia, A., A.K. Jain, and S. Sharma. Chemistry Letters, 2014. 43(4): p. 521-523. ISI[000333884300045].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334491200014">Antifungal Activity of Hypericum havvae against Some Medical Candida Yeast and Cryptococcus Species.</a> Dulger, G. and B. Dulger. Tropical Journal of Pharmaceutical Research, 2014. 13(3): p. 405-408. ISI[000334491200014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334060900003">Chromone-fused Cytosine Analogues: Synthesis, Biological Activity, and Structure-activity Relationship.</a> Haveliwala, D.D., N.R. Kamdar, P.T. Mistry, and S.K. Patel. Nucleosides, Nucleotides and Nucleic Acids, 2014. 33(2): p. 80-91. ISI[000334060900003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334572000025">Multitarget Drug Discovery for Tuberculosis and Other Infectious Diseases.</a> Li, K., L.A. Schurig-Briccio, X.X. Feng, A. Upadhyay, V. Pujari, B. Lechartier, F.L. Fontes, H.L. Yang, G.D. Rao, W. Zhu, A. Gulati, J.H. No, G. Cintra, S. Bogue, Y.L. Liu, K. Molohon, P. Orlean, D.A. Mitchell, L. Freitas-Junior, F.F. Ren, H. Sun, T. Jiang, Y.J. Li, R.T. Guo, S.T. Cole, R.B. Gennis, D.C. Crick, and E. Oldfield. Journal of Medicinal Chemistry, 2014. 57(7): p. 3126-3139. ISI[000334572000025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334408700003">Antifungal Activity of Plant Defensin AFP1 in Brassica juncea involves the Recognition of the Methyl Residue in Glucosylceramide of Target Pathogen Candida albicans.</a> Oguro, Y., H. Yamazaki, M. Takagi, and H. Takaku. Current Genetics, 2014. 60(2): p. 89-97. ISI[000334408700003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334325800020">In Vitro Antitubercular, Antibacterial and Antifungal Activities of 4-(3,4-Disubstituted) benzylidene-2-phenyloxazolin-5-ones and their Phenyl acrylohydrazides.</a> Ramalingam, P., D.R. Reddy, C.B. Rao, R.H. Babu, and R. Vasudev. Indian Journal of Heterocyclic Chemistry, 2014. 23(3): p. 321-330. ISI[000334325800020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334097400010">The Variation in Antimicrobial and Antioxidant Activities of Acetone Leaf Extracts of 12 Moringa oleifera (Moringaceae) Trees Enables the Selection of Trees with Additional Uses.</a> Ratshilivha, N., M.D. Awouafack, E.S. du Toit, and J.N. Eloff. South African Journal of Botany, 2014. 92: p. 59-64. ISI[000334097400010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334130500068">Combined Experimental and Theoretical Studies on the X-ray Crystal Structure, FT-IR, H-1 NMR, C-13 NMR, UV-Vis Spectra, NLO Behavior and Antimicrobial Activity of 2-Hydroxyacetophenone benzoylhydrazone.</a> Sheikhshoaie, I., S.Y. Ebrahimipour, M. Sheikhshoaie, H.A. Rudbari, M. Khaleghi, and G. Bruno. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2014. 124: p. 548-555. ISI[000334130500068].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334325800003">Synthesis, Characterization and Biological Evaluation of N-(2-(1-Benzo[d]imidazol-2-yl)phenyl)-substituted benzamines.</a> Singh, A.C., N. Mittel, and D. Pathak. Indian Journal of Heterocyclic Chemistry, 2014. 23(3): p. 225-232. ISI[000334325800003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334097400002">Essential Oil Composition and Antimicrobial Interactions of Understudied Tea Tree Species.</a> Van Vuuren, S.F., Y. Docrat, G.P.P. Kamatou, and A.M. Viljoen. South African Journal of Botany, 2014. 92: p. 7-14. ISI[000334097400002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334619500001">Bioactive Terpenoids and Flavonoids from Daucus littoralis Smith Subsp. hyrcanicus Rech.f, an Endemic Species of Iran.</a> Yousefbeyk, F., A.R. Gohari, Z. Hashemighahderijani, S.N. Ostad, M.H.S. Sourmaghi, M. Amini, F. Golfakhrabadi, H. Jamalifar, and G. Amin. DARU Journal of Pharmaceutical Sciences, 2014. 22: p. 12. ISI[000334619500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0509-052214.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
