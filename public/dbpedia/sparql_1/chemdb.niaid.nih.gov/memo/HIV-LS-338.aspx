

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-338.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Mq58sgWpuDWtkIukj02OVSicxq1Mi3J5CB5TxJG3uw8ht+AZPYGoCKbjKgEvcaLG3Z7g6D/qxZmIaTFs5H8X4Hk/95FzWenn8HHQ8Q7qbS1utA82nIKqASAjOTQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DC241F4E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-338-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48289   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          New targets and new drugs in the treatment of HIV</p>

    <p>          Lopez-Aldeguer, J, Aguirrebengoa, K, Arribas, JR, Este, JA, and Kindelan, JM</p>

    <p>          Enferm Infecc Microbiol Clin <b>2005</b>.  23(Supl.2): 33-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16373002&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16373002&amp;dopt=abstract</a> </p><br />

    <p>2.     48290   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Anti-HIV activity against immunodeficiency virus type 1 (HIV-I) and type II (HIV-II) of compounds isolated from the stem bark of Combtetum molle</p>

    <p>          Asres, K and Bucar, F</p>

    <p>          Ethiop Med J <b>2005</b>.  43(1): 15-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16370525&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16370525&amp;dopt=abstract</a> </p><br />

    <p>3.     48291   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Anti-HIV-1 activities of extracts from the medicinal plant Rhus chinensis</p>

    <p>          Wang, RR, Gu, Q, Yang, LM, Chen, JJ, Li, SY, and Zheng, YT</p>

    <p>          J Ethnopharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368204&amp;dopt=abstract</a> </p><br />

    <p>4.     48292   HIV-LS-338; EMBASE-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Fenretinide inhibits HIV infection by promoting viral endocytosis</p>

    <p>          Finnegan, Catherine M and Blumenthal, Robert</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4HS3GFC-1/2/e5ad704e9048f26ff1b11c3cc1e18eaf">http://www.sciencedirect.com/science/article/B6T2H-4HS3GFC-1/2/e5ad704e9048f26ff1b11c3cc1e18eaf</a> </p><br />

    <p>5.     48293   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel 4&#39;-hydroxymethyl branched thioapiosyl nucleosides</p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.  338(12): 577-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16353276&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16353276&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48294   HIV-LS-338; EMBASE-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Identification of N-phenyl-N&#39;-(2,2,6,6-tetramethyl-piperidin-4-yl)-oxalamides as a new class of HIV-1 entry inhibitors that prevent gp120 binding to CD4</p>

    <p>          Zhao, Qian, Ma, Liying, Jiang, Shibo, Lu, Hong, Liu, Shuwen, He, Yuxian, Strick, Nathan, Neamati, Nouri, and Debnath, Asim Kumar</p>

    <p>          Virology <b>2005</b>.  339(2): 213-225</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4GJK858-3/2/99ccdb511ff38fc3085fe98209ac277d">http://www.sciencedirect.com/science/article/B6WXR-4GJK858-3/2/99ccdb511ff38fc3085fe98209ac277d</a> </p><br />

    <p>7.     48295   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Structural Analysis of Silanediols as Transition-State-Analogue Inhibitors of the Benchmark Metalloprotease Thermolysin(,)</p>

    <p>          Juers, DH, Kim, J, Matthews, BW, and Sieburth, SM</p>

    <p>          Biochemistry <b>2005</b>.  44(50): 16524-16528</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16342943&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16342943&amp;dopt=abstract</a> </p><br />

    <p>8.     48296   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Diketo Acid Pharmacophore. 2. Discovery of Structurally Diverse Inhibitors of HIV-1 Integrase</p>

    <p>          Dayam, R, Sanchez, T, and Neamati, N</p>

    <p>          J Med Chem <b>2005</b>.  48(25): 8009-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16335925&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16335925&amp;dopt=abstract</a> </p><br />

    <p>9.     48297   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Parallel Solution-Phase and Microwave-Assisted Synthesis of New S-DABO Derivatives Endowed with Subnanomolar Anti-HIV-1 Activity</p>

    <p>          Manetti, F, Este, JA, Clotet-Codina, I, Armand-Ugon, M, Maga, G, Crespan, E, Cancio, R, Mugnaini, C, Bernardini, C, Togninelli, A, Carmi, C, Alongi, M, Petricci, E, Massa, S, Corelli, F, and Botta, M</p>

    <p>          J Med Chem <b>2005</b>.  48(25): 8000-8008</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16335924&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16335924&amp;dopt=abstract</a> </p><br />

    <p>10.   48298   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Different Stereochemical Requirements for CXCR4 Binding and Signaling Functions As Revealed by an Anti-HIV, d-Amino Acid-Containing SMM-Chemokine Ligand</p>

    <p>          Dong, CZ, Kumar, S, Choi, WT, Madani, N, Tian, S, An, J, Sodroski, JG, and Huang, Z</p>

    <p>          J Med Chem <b>2005</b>.  48(25): 7923-7924</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16335916&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16335916&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48299   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Evaluation of a neural networks QSAR method based on ligand representation using substituent descriptors Application to HIV-1 protease inhibitors</p>

    <p>          Milac, AL, Avram, S, and Petrescu, AJ</p>

    <p>          J Mol Graph Model <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16325439&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16325439&amp;dopt=abstract</a> </p><br />

    <p>12.   48300   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Emtricitabine, a new antiretroviral agent with activity against HIV and hepatitis B virus</p>

    <p>          Saag, MS</p>

    <p>          Clin Infect Dis <b>2006</b>.  42(1): 126-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16323102&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16323102&amp;dopt=abstract</a> </p><br />

    <p>13.   48301   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          Effects of Diterpenes Isolated from the Brazilian Marine Alga Dictyota menstrualis on HIV-1 Reverse Transcriptase</p>

    <p>          Pereira, Hde S, Leao-Ferreira, LR, Moussatche, N, Teixeira, VL, Cavalcanti, DN, da, Costa LJ, Diaz, R, and Frugulhetti, IC</p>

    <p>          Planta Med <b>2005</b>.  71(11): 1019-24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16320202&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16320202&amp;dopt=abstract</a> </p><br />

    <p>14.   48302   HIV-LS-338; PUBMED-HIV-12/27/2005</p>

    <p class="memofmt1-2">          A beta-galactose-specific lectin isolated from the marine worm Chaetopterus variopedatus possesses anti-HIV-1 activity</p>

    <p>          Wang, JH, Kong, J, Li, W, Molchanova, V, Chikalovets, I, Belogortseva, N, Luk&#39;yanov, P, and Zheng, YT</p>

    <p>          Comp Biochem Physiol C Toxicol Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16316787&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16316787&amp;dopt=abstract</a> </p><br />

    <p>15.   48303   HIV-LS-338; WOS-HIV-12/18/2005</p>

    <p class="memofmt1-2">          Structure-activity relationship studies on CXCR4 antagonists having cyclic pentapeptide scaffolds</p>

    <p>          Tamamura, H, Esaka, A, Ogawa, T, Araki, T, Ueda, S, Wang, ZX, Trent, JO, Tsutsumi, H, Masuno, H, Nakashima, H, Yamamoto, N, Peiper, SC, Otaka, A, and Fujii, N</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2005</b>.  3(24): 4392-4394, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233721500014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233721500014</a> </p><br />
    <br clear="all">

    <p>16.   48304   HIV-LS-338; WOS-HIV-12/18/2005</p>

    <p class="memofmt1-2">          HIV-1 drug resistance in Thailand: Before and after National Access to Antiretroviral Program</p>

    <p>          Sutthent, R, Arworn, D, Kaoriangudom, S, Chokphaibulkit, K, Chaisilwatana, P, Wirachsilp, P, Thiamchai, V, Sirapraphasiri, T, and Tanprasertsuk, S</p>

    <p>          JOURNAL OF CLINICAL VIROLOGY <b>2005</b>.  34(4): 272-276, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233769700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233769700006</a> </p><br />

    <p>17.   48305   HIV-LS-338; WOS-HIV-12/18/2005</p>

    <p class="memofmt1-2">          Carbohydrate-binding agents cause deletions of highly conserved glycosylation sites in HIV GP120 - A new therapeutic concept to hit the Achilles heel of HIV</p>

    <p>          Balzarini, J,  Van, Laethem K, Hatse, S , Froeyen, M, Peumans, W, Van Damme, E, and Schols, D</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b> 2005</b>.  280(49): 41005-41014, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233666600075">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233666600075</a> </p><br />

    <p>18.   48306   HIV-LS-338; WOS-HIV-12/18/2005</p>

    <p class="memofmt1-2">          Bioengineering lactic acid bacteria to secrete the HIV-1 virucide cyanovirin</p>

    <p>          Pusch, O, Boden, D, Hannify, S, Lee, F, Tucker, LD, Boyd, MR, Wells, JM, and Ramratnam, B</p>

    <p>          JAIDS-JOURNAL OF ACQUIRED IMMUNE DEFICIENCY SYNDROMES <b>2005</b>.  40(5): 512-520, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233694600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233694600002</a> </p><br />

    <p>19.   48307   HIV-LS-338; WOS-HIV-12/18/2005</p>

    <p class="memofmt1-2">          Synthesis of hydantoin, thiohydantoin and desulphuration of thiohydantoin to hydantoin</p>

    <p>          Dubey, VS</p>

    <p>          ASIAN JOURNAL OF CHEMISTRY <b>2006</b>.  18(1): 155-158, 4</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233608100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233608100023</a> </p><br />

    <p>20.   48308   HIV-LS-338; WOS-HIV-12/18/2005</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type 1 reverse transcriptase, RNase H, and integrase activities by hydroxytropolones</p>

    <p>          Didierjean, J, Isel, C, Querre, F, Mouscadet, JF, Aubertin, AM, Valnot, JY, Piettre, SR, and Marquet, R</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(12): 4884-4894, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233692100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233692100008</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
