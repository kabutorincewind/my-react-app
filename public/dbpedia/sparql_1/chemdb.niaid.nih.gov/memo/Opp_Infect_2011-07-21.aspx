

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-07-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lPFqL9DLqyzl+2DRzKN3tc6iQ06Lrvj8Y9wm5TbptQKrH70gLcfzDTzg0G7fJ2vQuHdIWTXvTcmZaZ298EG0CuAJ2kFG/6qitku8nD6mREYwyOEFuhhP5Q58lZQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="653A97EF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">July 8 - July 21, 2011</p>

    <p class="memofmt2-2"> </p> 

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21757344">Novel hybrids of Fluconazole and Furanones: Design, Synthesis and Antifungal Activity.</a> Borate, H.B., S.P. Sawargave, S.P. Chavan, M.A. Chandavarkar, R. Iyer, A. Tawte, D. Rao, J.V. Deore, A.S. Kudale, P.S. Mahajan, and G.S. Kangire. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21757344].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21756287">Synthesis of Aryl Aldimines and Their Activity against Fungi of Clinical Interest.</a>da Silva, C.M., D.L. da Silva, C.V. Martins, M.A. de Resende, E.S. Dias, T.F. Magalhaes, L.P. Rodrigues, A.A. Sabino, R.B. Alves, and A. de Fatima. Chemical Biology &amp; Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21756287].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext"> 3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21741130">Aryl-2-[1H-benzotriazol-1-yl]acrylonitriles: A Novel Class of Potent Tubulin Inhibitors.</a> Carta, A., I. Briguglio, S. Piras, G. Boatto, P. La Colla, R. Loddo, M. Tolomeo, S. Grimaudo, A. Di Cristina, R.M. Pipitone, E. Laurini, M.S. Paneni, P. Posocco, M. Fermeglia, and S. Pricl. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21741130].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21755942">Structure-Activity Relationships of Antitubercular Nitroimidazoles. 3. Exploration of the Linker and Lipophilic Tail of ((S)-2-nitro-6,7-dihydro-5H-imidazo[2,1-b][1,3]oxazin-6-yl)-(4-trifluoromethoxybe nzyl)amine (6-amino PA-824).</a> Cherian, J., I. Choi, A. Nayyar, U. Manjunatha, T. Mukherjee, Y.S. Lee, H.I. Boshoff, R. Singh, Y.H. Ha, M. Goodwin, S.B. Lakshminarayana, P. Niyomrattanakit, J. Jiricek, S. Ravindran, T. Dick, T.H. Keller, V. Dartois, and C.E. Barry 3<sup>rd</sup>. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21755942].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21766389">Total Synthesis of a Depsidomycin Analogue by Convergent Solid-phase Peptide Synthesis and Macrolactonization Strategy for Antitubercular Activity.</a> Narayanaswamy, V.K., F. Albericio, Y.M. Coovadia, H.G. Kruger, G.E. Maguire, M. Pillay, and T. Govender. Journal of Peptide Science: an Official Publication of the European Peptide Society, 2011. <b>[Epub ahead of print]</b>; PMID[21766389].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21740100">A New Class of 2-(4-Cyanophenyl amino)-4-(6-bromo-4-quinolinyloxy)-6-piperazinyl (piperidinyl)-1,3,5-triazine Analogues with Antimicrobial/Antimycobacterial Activity.</a> Patel, R.V., P. Kumari, D.P. Rajani, and K.H. Chikhalia. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21740100].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21755528">Synthesis of Some Pyrazolines and Pyrimidines Derived from Polymethoxy Chalcones as Anticancer and Antimicrobial Agents.</a> Rostom, S.A., M.H. Badr, H.A. Abd El Razik, H.M. Ashour, and A.E. Abdel Wahab. Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21755528].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p class="MsoNormal"></p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21739106">Synthesis and Minimum Inhibitory Concentrations of SK-03-92 against Staphylococcus aureus and Other Gram-positive Bacteria.</a> Schwan, W.R., M.S. Kabir, M. Kallaus, S. Krueger, A. Monte, and J.M. Cook. Journal of Infection and Chemotherapy : Official Journal of the Japan Society of Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21739106].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21684158">Synthesis and Evaluation of Chromenyl Barbiturates and Thiobarbiturates as Potential Antitubercular Agents.</a> Vijaya Laxmi, S., Y. Thirupathi Reddy, B. Suresh Kuarm, P. Narsimha Reddy, P.A. Crooks, and B. Rajitha. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(14): p. 4329-4331; PMID[21684158].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706072011.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20600776">Antiplasmodial, Anti-inflammatory and Cytotoxic Activities of Various Plant Extracts from the Mascarene Archipelago.</a> Jonville, M.C., H. Kodja, D. Strasberg, A. Pichette, E. Ollivier, M. Frederich, L. Angenot, and J. Legault. Journal of Ethnopharmacology, 2011. 136(3): p. 525-531; PMID[20600776].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21739316">In vitro sensitivity of Plasmodium falciparum Field Isolates to Extracts from Cameroonian Annonaceae Plants.</a> Kemgne, E.A., W.F. Mbacham, F.F. Boyom, P.H. Zollo, E. Tsamo, and P.J. Rosenthal. Parasitology Research, 2011. <b>[Epub ahead of print]</b>; PMID[21739316].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21751074">Characterization of In vivo Metabolites of WR319691, a Novel Compound with Activity Against Plasmodium falciparum.</a> Milner, E., J. Sousa, B. Pybus, V. Melendez, S. Gardner, K. Grauer, J. Moon, D. Carroll, J. Auschwitz, M. Gettayacamin, P. Lee, S. Leed, W. McCalmont, S. Norval, A. Tungtaeng, Q. Zeng, M. Kozar, K.D. Read, Q. Li, and G. Dow. European Journal of Drug Metabolism and Pharmacokinetics, 2011. <b>[Epub ahead of print]</b>; PMID[21751074].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21736388">Antimalarial Activity of Natural and Synthetic Prodiginines.</a> Papireddy, K., M. Smilkstein, J.X. Kelly, Shweta, S.M. Salem, M. Alhamadsheh, S.W. Haynes, G.L. Challis, and K.A. Reynolds. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21736388].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21739204">In vitro Activity of Antifungal Drugs against Plasmodium falciparum Field Isolates.</a> Pongratz, P., F. Kurth, G.M. Ngoma, A. Basra, and M. Ramharter. Wien Klin Wochenschr, 2011. <b>[Epub ahead of print]</b>; PMID[21739204].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTILEISHMANIA COMPOUNDS</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21684751">Design, Synthesis and Antileishmanial In vitro Activity of New Series of Chalcones-like Compounds: A Molecular Hybridization Approach.</a> Barbosa, T.P., S.C. Sousa, F.M. Amorim, Y.K. Rodrigues, P.A. de Assis, J.P. Caldas, M.R. Oliveira, and M.L. Vasconcellos. Bioorganic &amp; Medicinal Chemistry, 2011. 19(14): p. 4250-4256; PMID[21684751].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p class="ListParagraphCxSpFirst"> </p>

    <p class="ListParagraphCxSpLast"> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21683592">Anti-leishmanial Activity of Disubstituted Purines and Related Pyrazolo[4,3-d]pyrimidines.</a> Jorda, R., N. Sacerdoti-Sierra, J. Voller, L. Havlicek, K. Kracalikova, M.W. Nowicki, A. Nasereddin, V. Krystof, M. Strnad, M.D. Walkinshaw, and C.L. Jaffe.  Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(14): p. 4233-4237; PMID[21683592].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291863100010">Honey Flavonoids, Natural Antifungal Agents Against Candida Albicans.</a> Candiracci, M., B. Citterio, G. Diamantini, M. Blasa, A. Accorsi, and E. Piatti. International Journal of Food Properties, 2011. 14(4): p. 799-808; ISI[000291863100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291895200061">Design, Synthesis and Molecular Docking Studies of Novel Triazole as Antifungal Agent.</a> Chai, X.Y., J. Zhang, Y.B. Cao, Y. Zou, Q.Y. Wu, D.Z. Zhang, Y.Y. Jiang, and Q.Y. Sun. European Journal of Medicinal Chemistry, 2011. 46(7): p. 3167-3176; ISI[000291895200061].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291895200055">Total Synthesis of Racemic and (R) and (S)-4-Methoxyalkanoic acids and Their Antifungal Activity.</a> Das, B., D.B. Shinde, B.S. Kanth, A. Kamle, and C.G. Kumar. European Journal of Medicinal Chemistry, 2011. 46(7): p. 3124-3129; ISI[000291895200055].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291982806218">Thiazole, Oxadiazole, and Carboxamide Derivatives of Artemisinin are Highly Selective and Potent Inhibitors of Toxoplasma gondii.</a> Hencken, C.P., L. Jones-Brando, C. Bordon, R. Stohler, B.T. Mott, R. Yolken, G.H. Posner, and L.E. Woodard. Abstracts of Papers of the American Chemical Society, 2011. ISI[000291982806218].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291982802329">Design, Synthesis and Characterization of Thiophene-arabinoside Conjugates as Inhibitors of Mycobacterium tuberculosis Antigen 85C.</a> Ibrahim, D.A., K.R. Trabbic, S.S. Adams, A.K. Sanki, J. Boucau, D.H. Lajiness, D.R. Ronning, and S.J. Sucheck. Abstracts of Papers of the American Chemical Society, 2011.ISI[000291982802329].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292186400043">Synthesis and Antitubercular Evaluation of Amidoalkyl dibenzofuranols and 1H-Benzo 2,3 benzofuro 4,5-e 1,3 oxazin-3(2H)-ones.</a> Kantevari, S., T. Yempala, P. Yogeeswari, D. Sriram, and B. Sridhar. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(14): p. 4316-4319; ISI[000292186400043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291895200054">Mycobacterium tuberculosis and Cholinesterase Inhibitors from Voacanga globosa.</a> Macabeo, A.P.G., W.S. Vidar, X.Y. Chen, M. Decker, J. Heilmann, B.J. Wan, S.G. Franzblau, E.V. Galvez, M.A.M. Aguinaldo, and G.A. Cordell. European Journal of Medicinal Chemistry, 2011. 46(7): p. 3118-3123; ISI[000291895200054].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291888600017">Studies on the Antimicrobial Activity and Chemical Composition of the Essential Oils and Alcoholic Extracts of Gentiana asclepiadea L</a>. Mihailovic, V., N. Vukovic, N. Niciforovic, S. Solujic, M. Mladenovic, P. Maskovic, and M.S. Stankovic. Journal of Medicinal Plants Research, 2011. 5(7): p. 1164-1174; ISI[000291888600017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291886800014">Bio-activities of Extracts from Some Axenically Farmed and Naturally Grown Bryophytes.</a> Sabovljevic, A., M. Sokovic, J. Glamoclija, A. Ciric, M. Vujicic, B. Pejin, and M. Sabovljevic. Journal of Medicinal Plants Research, 2011. 5(4): p. 565-571; ISI[000291886800014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292003400006">Synthesis of 4-Hydroxycoumarin Heteroarylhybrids as Potential Antimicrobial Agents.</a> Siddiqui, Z.N., T.N.M. Musthafa, A. Ahmad, and A.U. Khan. Archiv Der Pharmazie, 2011. 344(6): p. 394-401; ISI[000292003400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291889500016">Estimation of Phytoconstituents from Cryptostegia grandiflora (Roxb.) R. Br. In vivo and In vitro. II. Antimicrobial Screening.</a> Singh, B., R.A. Sharma, G.K. Vyas, and P. Sharma. Journal of Medicinal Plants Research, 2011. 5(9): p. 1598-1605; ISI[000291889500016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291895200026">Synthesis and Antimicrobial Activities of Hexahydroimidazo 1,5-a Pyridinium bromides with Varying Benzyl Substituents.</a>Turkmen, H., N. Ceyhan, N.U.K. Yavasoglu, G. Ozdemir, and B. Cetinkaya. European Journal of Medicinal Chemistry, 2011. 46(7): p. 2895-2900; ISI[000291895200026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291895200058">Design, Synthesis and Antifungal Activities of Novel 1,2,4-Triazole Derivatives.</a> Xu, J.M., Y.B. Cao, J. Zhang, S.C. Yu, Y. Zou, X.Y. Chai, Q.Y. Wu, D.Z. Zhang, Y.Y. Jiang, and Q.Y. Sun. European Journal of Medicinal Chemistry, 2011. 46(7): p. 3142-3148; ISI[000291895200058].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0706-072011.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
