

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-02-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bqvh92+5XWPKvIASU14/VFHVNYIeXzEognlkniQXqsG2r6MZh1RgfNZ4KaezeMqU9kM1K16d7scNxXDwilm1sLtbG18ERaYP/WIvZ9rGqe/HL/VzrxQ8yHTfa8M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0204852B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: February 13 - February 26, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25585716">2-(2-Oxo-morpholin-3-yl)-acetamide Derivatives as Broad-spectrum Antifungal Agents.</a> Bardiot, D., K. Thevissen, K. De Brucker, A. Peeters, P. Cos, C.P. Taborda, M. McNaughton, L. Maes, P. Chaltin, B.P. Cammue, and A. Marchand. Journal of Medicinal Chemistry, 2015. 58(3): p. 1502-1512. PMID[25585716].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25689641">Spectroscopic (FT-IR, FT-Raman, 1H- and 13C-NMR), Theoretical and Microbiological Study of Trans O-Coumaric acid and Alkali Metal O-Coumarates.</a> Kowczyk-Sadowy, M., R. Swislocka, H. Lewandowska, J. Piekut, and W. Lewandowski. Molecules, 2015. 20(2): p. 3146-3169. PMID[25689641].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25650957">Nylon-3 Polymers Active against Drug-resistant Candida albicans Biofilms.</a> Liu, R., X. Chen, S.P. Falk, K.S. Masters, B. Weisblum, and S.H. Gellman. Journal of the American Chemical Society, 2015. 137(6): p. 2183-2186. PMID[25650957].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25689642">Synthesis and Antimicrobial Activity of N-Substituted-beta-amino acid Derivatives Containing 2-Hydroxyphenyl, Benzo[b]phenoxazine and Quinoxaline Moieties.</a> Mickeviciene, K., R. Baranauskaite, K. Kantminiene, M. Stasevych, O. Komarovska-Porokhnyavets, and V. Novikov. Molecules, 2015. 20(2): p. 3170-3189. PMID[25689642].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25585381">AP-57/C10orf99 is a New Type of Mutifunctional Antimicrobial Peptide.</a> Yang, M., M. Tang, X. Ma, L. Yang, J. He, X. Peng, G. Guo, L. Zhou, N. Luo, Z. Yuan, and A. Tong. Biochemical and Biophysical Research Communications, 2015. 457(3): p. 347-352. PMID[25585381].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25689777">In Vitro Activity of Rifampicin and Verapamil Combination in Multidrug-resistant Mycobacterium tuberculosis.</a> de Oliveira Demitto, F., R.C. do Amaral, F.G. Maltempe, V.L. Siqueira, R.B. Scodro, M.A. Lopes, K.R. Caleffi-Ferracioli, P.H. Canezin, and R.F. Cardoso. Plos One, 2015. 10(2): p. e0116545. PMID[25689777].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25608020">Cell Penetrating Synthetic Antimicrobial Peptides (SAMPs) Exhibiting Potent and Selective Killing of Mycobacterium by Targeting its DNA.</a> Sharma, A., A.A. Pohane, S. Bansal, A. Bajaj, V. Jain, and A. Srivastava. Chemistry, 2015. 21(9): p. 3540-3545. PMID[25608020].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25614114">Novel, Potent, Orally Bioavailable and Selective Mycobacterial ATP Synthase Inhibitors that Demonstrated Activity against both Replicating and Non-replicating M. tuberculosis.</a> Singh, S., K.K. Roy, S.R. Khan, V.K. Kashyap, A. Sharma, S. Jaiswal, S.K. Sharma, M.Y. Krishnan, V. Chaturvedi, J. Lal, S. Sinha, A.D. Gupta, R. Srivastava, and A.K. Saxena. Bioorganic &amp; Medicinal Chemistry, 2015. 23(4): p. 742-752. PMID[25614114].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25699152">Improved Synthesis of Biotinol-5&#39;-AMP: Implications for Antibacterial Discovery.</a> Tieu, W., S.W. Polyak, A.S. Paparella, M.Y. Yap, T.P. Soares da Costa, B. Ng, G. Wang, R. Lumb, J.M. Bell, J.D. Turnidge, M.C. Wilce, G.W. Booker, and A.D. Abell. ACS Medicinal Chemistry Letters, 2015. 6(2): p. 216-220. PMID[25699152].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25699139">Syntheses and Antituberculosis Activity of 1,3-Benzothiazinone sulfoxide and sulfone Derived from BTZ043.</a> Tiwari, R., P.A. Miller, S. Cho, S.G. Franzblau, and M.J. Miller. ACS Medicinal Chemistry Letters, 2015. 6(2): p. 128-133. PMID[25699139].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25599834">11. Synthesis and Biological Evaluation of Adamantane-based Aminophenols as a Novel Class of Antiplasmodial Agents.</a> Chinnapattu, M., K.I. Sathiyanarayanan, and P.S. Iyer. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 952-955. PMID[25599834].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25322084">Mutations in the P-Type Cation-transporter ATPase 4, PfATP4, Mediate Resistance to Both Aminopyrazole and Spiroindolone Antimalarials.</a> Flannery, E.L., C.W. McNamara, S.W. Kim, T.S. Kato, F. Li, C.H. Teng, K. Gagaring, M.J. Manary, R. Barboa, S. Meister, K. Kuhen, J.M. Vinetz, A.K. Chatterjee, and E.A. Winzeler. ACS Chemical Biology, 2015. 10(2): p. 413-420. PMID[25322084].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25700165">Screening the Medicines for Malaria Venture &quot;Malaria Box&quot; against the Plasmodium falciparum Aminopeptidases, M1, M17 and M18.</a> Paiardini, A., R.S. Bamert, K. Kannan-Sivaraman, N. Drinkwater, S.N. Mistry, P.J. Scammells, and S. McGowan. Plos One, 2015. 10(2): p. e0115859. PMID[25700165].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0213-022615.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347582900031">14. Synthesis, Theoretical Calculations and Antimicrobial Studies of Copper(I) Complexes of Cysteamine, Cysteine and 2-Mercaptonicotinic acid.</a> Ahmad, S., A. Espinosa, T. Ahmad, M. Sohail, A.A. Isab, M. Saleem, A. Hameed, M. Monim-ul-Mehboob, and E. de las Heras. Polyhedron, 2015. 85: p. 239-245. ISI[000347582900031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347832000005">Rare Pyrane-based Cembranoids from the Red Sea Soft Coral Sarcophyton trocheliophorum as Potential Antimicrobial-antitumor Agents.</a> Al-Footy, K.O., W.M. Alarif, F. Asiri, M.M. Aly, and S.E.N. Ayyad. Medicinal Chemistry Research, 2015. 24(2): p. 505-512. ISI[000347832000005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347495400066">Synthesis, Molecular Structure Investigations and Antimicrobial Activity of 2-Thioxothiazolidin-4-one derivatives.</a> Barakat, A., H.J. Al-Najjar, A.M. Al-Majid, S.M. Soliman, Y.N. Mabkhot, M.H.M. Al-Agamy, H.A. Ghabbour, and H.K. Fun. Journal of Molecular Structure, 2015. 1081: p. 519-529. ISI[000347495400066].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347582900047">Ruthenium(II) Complexes with Hydroxypyridinecarboxylates: Screening Potential Metallodrugs against Mycobacterium tuberculosis.</a> Barbosa, M.I.F., R.S. Correa, L.V. Pozzi, E.D. Lopes, F.R. Pavan, C.Q.F. Leite, J. Ellena, S.D. Machado, G. Von Poelhsitz, and A.A. Batista. Polyhedron, 2015. 85: p. 376-382. ISI[000347582900047].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347585300061">Coordination Mode of Pentadentate Ligand Derivative of 5-Amino-1,3,4-thiadiazole-2-thiol with Nickel(II) and Copper(II) Metal Ions: Synthesis, Spectroscopic Characterization, Molecular Modeling and Fungicidal Study.</a> Chandra, S., S. Gautam, A. Kumar, and M. Madan. Spectrochimica Acta Part A-Molecular and Biomolecular Spectroscopy, 2015. 136: p. 672-681. ISI[000347585300061].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348189000012">Synthesis of Novel Antimicrobial Aryl Himachalene Derivatives from Naturally Occurring Himachalenes.</a> Chaudhary, A., S. Sood, P. Das, P. Kaur, I. Mahajan, A. Gulati, and B. Singh. EXCLI Journal, 2014. 13: p. 1216-1225. ISI[000348189000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347833800006">Synthesis, Molecular Structures, and Antimicrobial Activities of N&#39;-(3,5-Dibromo-2-hydroxybenzylidene)-2-fluorobenzohydrazide and N&#39;-(4-Diethylamino-2-hydroxybenzylidene)-2-fluorobenzohydrazide.</a> Feng, Y.X., L.W. Xue, and C.X. Zhang. Journal of the Chilean Chemical Society, 2014. 59(3): p. 2555-2558. ISI[000347833800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347715500007">Modified Pyridine-substituted Coumarins: A New Class of Antimicrobial and Antitubercular Agents.</a> Giri, R.R., H.B. Lad, V.G. Bhila, C.V. Patel, and D.I. Brahmbhatt. Synthetic Communications, 2015. 45(3): p. 363-375. ISI[000347715500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347626000006">Cold-active Antibacterial and Antifungal Activities and Antibiotic Resistance of Bacteria Isolated from an Alpine Hydrocarbon-contaminated Industrial Site.</a> Hemala, L., D.C. Zhang, and R. Margesin. Research in Microbiology, 2014. 165(6): p. 447-456. ISI[000347626000006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347626000002">Ocimum sanctum (L.) Essential Oil and its Lead Molecules Induce Apoptosis in Candida albicans.</a> Khan, A., A. Ahmad, L.A. Khan, and N. Manzoor. Research in Microbiology, 2014. 165(6): p. 411-419. ISI[000347626000002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347976500086">Antioxidative and Antimicrobial Activities of the Extracts from the Seed Coat of Bambara groundnut (Voandzeia subterranea).</a> Klompong, V. and S. Benjakul. RSC Advances, 2015. 5(13): p. 9973-9985. ISI[000347976500086].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347906300006">Synthesis, Antimicrobial and Antioxidant Activities of some 5-Pyrazolone Based Schiff Bases.</a> Parmar, N., S. Teraiya, R. Patel, H. Barad, H. Jajda, and V. Thakkar. Journal of Saudi Chemical Society, 2015. 19(1): p. 36-41. ISI[000347906300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348405900007">Preparation, Characterization and Antimicrobial Activity of Chitosan Microparticles with Thyme Essential Oil.</a> Pecarski, D., Z. Kneievic-Jugovic, S. Dimitrijevic-Brankovic, K. Mihajilovski, and S. Jankovic. Hemijska Industrija, 2014. 68(6): p. 721-729. ISI[000348405900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347837200010">Composition and Antimicrobial Activity of the Essential Oils of Laserpitium latifolium L. And L. ochridanum Micevski (Apiaceae).</a> Popovic, V.B., S.D. Petrovic, M.T. Milenkovic, M.M. Drobac, M.A. Couladis, and M.S. Niketic. Chemistry &amp; Biodiversity, 2015. 12(1): p. 170-177. ISI[000347837200010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347906300012">Synthesis of Some New 2&#39;-Amino-2-fluoro-5&#39;-oxo-1&#39;-(4-phenylthiazole-2-yl)-1&#39;,4&#39;,5&#39;,6&#39;,7&#39;,8&#39;-hexahydro-3,4&#39;-biquinoline-3&#39;-carbonitrile derivatives and Biological Evaluation as Antimicrobial Agents.</a> Shah, A. and D. Raj. Journal of Saudi Chemical Society, 2015. 19(1): p. 73-82. ISI[000347906300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347832000034">Antimycobacterial Activity and in Silico Study of Highly Functionalised Dispiropyrrolidines.</a> Wei, A.C., M.A. Ali, Y.K. Yoon, S.B. Choi, H. Osman, V.H. Masand, and T.S. Choon. Medicinal Chemistry Research, 2015. 24(2): p. 818-828. ISI[000347832000034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348144100055">Syntheses, Crystal Structures and Antimicrobial Activities of Cu(II), Ru(II), and Pt(II) Compounds with an Anthracene-containing Tripodal Ligand.</a> Yuan, Z.L., X.M. Shen, and J.D. Huang. RSC Advances, 2015. 5(14): p. 10521-10528. ISI[000348144100055].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0213-022615.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
