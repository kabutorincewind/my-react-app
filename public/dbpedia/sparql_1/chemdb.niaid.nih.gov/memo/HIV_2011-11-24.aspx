

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-11-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1PxKZGrkFzD2Awoy0tsmrlyQsluQmZvvQdgWMnALB2t3PKr9apT1XNda5LosEM+jiCuhD/2Vfka8uqEJg+2JpA1LOBQY74fbxEUASkfbpHpZBzOCwvsbzF/SEyk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D8B1E23F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">

    <h1 class="memofmt2-h1">HIV Citation List:  November 11, 2011 - November 24, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22081993">Computationally-Guided Optimization of a Docking Hit to Yield Catechol Diethers as Potent anti-HIV Agents.</a> Bollini, M., R.A. Domaoal, V.V. Thakur, R. Gallardo-Macias, K.A. Spasov, K.S. Anderson, and W.L. Jorgensen, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22081993].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22085645">Interactions Between Different Generation HIV-1 Fusion Inhibitors and the Putative Mechanism Underlying the Synergistic anti-HIV-1 Effect Resulting from Their Combination.</a> Cai, L., C. Pan, L. Xu, Y. Shui, K. Liu, and S. Jiang, The FASEB Journal, 2011. <b>[Epub ahead of print]</b>; PMID[22085645].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22032494">Reactive Aldehyde Metabolites from the anti-HIV Drug Abacavir: Amino Acid Adducts as Possible Factors in Abacavir Toxicity.</a> Charneira, C., A.L. Godinho, M.C. Oliveira, S.A. Pereira, E.C. Monteiro, M.M. Marques, and A.M. Antunes, Chemical Research in Toxicology 2011. <b>[Epub ahead of print]</b>; PMID[22032494].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22101518">Role of CXCR4 Internalization in the anti-HIV Activity of Stromal Cell-derived Factor-1alpha Probed by a Novel Synthetically and Modularly Modified-chemokine Analog.</a> Dong, C.Z., S. Tian, N. Madani, W.T. Choi, S. Kumar, D. Liu, J.G. Sodroski, Z. Huang, and J. An, Experimental Biology and Medicine, 2011. <b>[Epub ahead of print]</b>; PMID[22101518].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22078030">Inhibitory Effect of Aqueous Dandelion Extract on HIV-1 Replication and Reverse Transcriptase Activity.</a> Han, H., W. He, W. Wang, and B. Gao, BMC Complementary and Alternative Medicine, 2011. 11(1): p. 112; PMID[22078030].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22014749">Design, Synthesis and Biological Evaluation of 3-Substituted 2,5-Dimethyl-N-(3-(1H-tetrazol-5-yl)phenyl)pyrroles as Novel Potential HIV-1 gp41 Inhibitors.</a> He, X.Y., P. Zou, J. Qiu, L. Hou, S. Jiang, S. Liu, and L. Xie, Bioorganic &amp; Medicinal Chemistry, 2011. 19(22): p. 6726-34; PMID[22014749].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22083488">Interplay Between Single Resistance-associated Mutations in the HIV-1 Protease and Viral Infectivity, Protease Activity, and Inhibitor Sensitivity.</a> Henderson, G.J., S.K. Lee, D.M. Irlbeck, J. Harris, M. Kline, E. Pollom, N. Parkin, and R. Swanstrom, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22083488].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22095518">HIV-1 Integrase Inhibitors: A Review of Their Chemical Development.</a> Ingale, K.B. and M.S. Bhatia, Antiviral Chemistry &amp; Chemotherapy, 2011. 22(3): p. 95-105; PMID[22095518].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22095129">Pre-steady-state Kinetics of Interaction of Wild-type and Multiple Drug-resistant HIV Protease with First and Second Generation Inhibitory Drugs.</a> Kuznetsov, N.A., A.V. Kozyr, M.A. Dronina, I.V. Smirnov, E.N. Kaliberda, A.G. Mikhailova, L.D. Rumsh, O.S. Fedorova, A.G. Gabibov, and A.V. Kolesnikov, Doklady Biochemistry and Biophysics, 2011. 440(1): p. 239-43; PMID[22095129].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22083481">Antiviral Breadth and Combination Potential of Peptide Triazole HIV-1 Entry Inhibitors.</a> McFadden, K., P. Fletcher, F. Rossi, Kantharaju, M. Umashankara, V. Pirrone, S. Rajagopal, H. Gopi, F.C. Krebs, J. Martin-Garcia, R.J. Shattock, and I. Chaiken, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22083481].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21982685">Lead Optimization at C-2 and N-3 Positions of Thiazolidin-4-ones as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Murugesan, V., V.S. Tiwari, R. Saxena, R. Tripathi, R. Paranjape, S. Kulkarni, N. Makwana, R. Suryawanshi, and S.B. Katti, Bioorganic &amp; Medicinal Chemistry, 2011. 19(22): p. 6919-26; PMID[21982685].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22014753">Small Molecular CD4 Mimics as HIV Entry Inhibitors.</a> Narumi, T., H. Arai, K. Yoshimura, S. Harada, W. Nomura, S. Matsushita, and H. Tamamura, Bioorganic &amp; Medicinal Chemistry, 2011. 19(22): p. 6735-42; PMID[22014753].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22086403">Acetylcholinesterase-inhibiting Alkaloids from Zephyranthes concolor.</a> Reyes-Chilpa, R., S. Berkov, S. Hernandez-Ortega, C.K. Jankowski, S. Arseneau, I. Clotet-Codina, J.A. Este, C. Codina, F. Viladomat, and J. Bastida, Molecules, 2011. 16(11): p. 9520-33; PMID[22086403].
    <br />
    <b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22083472">In Vitro Activity of Candidate Microbicides Against Cell-associated HIV.</a> Selhorst, P., K. Grupping, T. Bourlet, O. Delezay, K.K. Arien, and G. Vanham, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22083472].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296045300075">Anti-HIV Effect in Vitro of Human Milk Glycoconjugates Glycobiology</a>. Acosta-Blanco, I.O.-F., S Dionisio-Vicuna, M Hernandez-Flores, M Fuentes-Romero, L Newburg, D Soto-Ramirez, LE Ruiz-Palacios, G Viveros-Rogel, M, 2011. 21(11): p. 1475-1476; ISI[000296045300075].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296203700033">Synthesis and Oxidation of 2-Hydroxynevirapine, a Metabolite of the HIV Reverse Transcriptase Inhibitor Nevirapine.</a> Antunes, A.N., DA da Silva, JLF Santos, PP Oliveira, MC Beland, FA Marques, MM, Organic &amp; Biomolecular Chemistry, 2011. 9(22): p. 7822-7835; ISI[000296203700033].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296374900004">Evaluation of the Immunomodulatory and Antiviral Effects of the Cytokine Combination IFN-alpha and IL-7 in the Lymphocytic Choriomeningitis Virus and Friend Retrovirus Mouse Infection Models</a>. Audige, A.H., U Dittmer, U van den Broek, M Speck, RF, Viral Immunology, 2011. 24(5): p. 375-385; ISI[000296374900004].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296025900034">Discovery of a Novel Series of Cyclic urea as Potent CCR5 Antagonists.</a> Duan, M.K., WM Tallant, M Jun, JH Edelstein, M Ferris, R Todd, D Wheelan, P Xiong, ZP, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): p. 6381-6385; ISI[000296025900034].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296044200015">Combined Approach Using Ligand Efficiency, Cross-Docking, and Antitarget Hits for Wild-Type and Drug-Resistant Y181C HIV-1 Reverse Transcriptase.</a> Garcia-Sosa, A.S., S Takkis, K Maran, U, Journal of Chemical Information and Modeling, 2011. 51(10): p. 2595-2611; ISI[000296044200015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296045300095">Exploiting Multivalency: A Designed Dimer of CV-N Shows Improved anti-HIV Activity.</a> Ghirlanda, G., B. Woodrum, M. Ruben, and B. O&#39;Keefe, Glycobiology, 2011. 21(11): p. 1481-1482; ISI[000296045300095].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295993900020">Linear and Nonlinear Quantitative Structure-Activity Relationship Modeling of the HIV-1 Reverse Transcriptase Inhibiting Activities of Thiocarbamates.</a> Goodarzi, M., M.P. Freitas, and Y. Vander Heyden, Analytica Chimica Acta, 2011. 705(1-2): p. 166-173; ISI[000295993900020].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296423700026">Synthesis and anti-HIV-1 Activity of 4-Substituted-7-(2 &#39;-deoxy-2 &#39;-fluoro-4 &#39;-azido-beta-D-ribofuranosyl) pyrrolo[2,3-d] pyrimidine Analogues.</a> Guo, X.H., Y.J. Li, L. Tao, Q. Wang, S.Y. Wang, W.D. Hu, Z.L. Pan, Q.H. Yang, Y.M. Cui, Z.P. Ge, L.H. Dong, X.J. Yu, H.Y. An, C.J. Song, and J.B. Chang, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(22): p. 6770-6772; ISI[000296423700026].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295950100009">Triterpene hexahydroxydiphenoyl esters and a Quinic acid purpurogallin carbonyl ester from the Leaves of Castanopsis fissa.</a> Huang, Y.T., T Tanaka, T Matsuo, Y Kouno, I Li, DP Nonaka, G, Phytochemistry, 2011. 72(16): p. 2006-2014; ISI[000295950100009].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296423700052">Design, Synthesis, and Biological Activity of a Novel Series of 2,5-Disubstituted furans/pyrroles as HIV-1 Fusion Inhibitors Targeting gp41.</a> Jiang, S.B., S.R. Tala, H. Lu, P. Zou, I. Avan, T.S. Ibrahim, N.E. Abo-Dya, A. Abdelmajeid, A.K. Debnath, and A.R. Katritzky, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(22): p. 6895-6898; ISI[000296423700052].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296072200017">Modulation of HIV-1 Integrase Activity by Single-stranded Oligonucleotides and Their Conjugates with Eosin Nucleosides.</a> Korolev, S.K., E Anisenko, A Tashlitskii, V Zatsepin, T Gottikh, M Agapkina, J, Nucleotides &amp; Nucleic Acids, 2011. 30(7-9): p. 651-666; ISI[000296072200017].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296423700017">Rational Design of 2-Pyrrolinones as Inhibitors of HIV-1 Integrase.</a> Ma, K.W., PH Fu, W Wan, XL Zhou, L Chu, Y Ye, DY, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(22): p. 6724-6727; ISI[000296423700017].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295997500037">Prospective CCR5 Small Molecule Antagonist Compound Design Using a Combined Mutagenesis/Modeling Approach</a>. Metz, M.B., E Labrecque, J Danthi, SJ Langille, J Harwig, C Yang, W Darkes, MC Lau, G Santucci, Z Bridger, GJ Schols, D Fricker, SP Skerlj, RT, Journal of the American Chemical Society, 2011. 133(41): p. 16477-16485; ISI[000295997500037].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296422800005">In Vitro Effect of Anti-Human Immunodeficiency Virus CCR5 Antagonist Maraviroc on Chemotactic Activity of Monocytes, Macrophages and Dendritic Cells.</a> Rossi, R., M. Lichtner, A. De Rosa, I. Sauzullo, F. Mengoni, A.P. Massetti, C.M. Mastroianni, and V. Vullo, Clinical and Experimental Immunology, 2011. 166(2): p. 184-190; ISI[000296422800005].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296097400001">Cross-validated Stepwise Regression for Identification of Novel Non-Nucleoside Reverse Transcriptase Inhibitor Resistance Associated Mutations.</a> Van der Borght, K., E. Van Craenenbroeck, P. Lecocq, M. Van Houtte, B. Van Kerckhove, L. Bacheler, G. Verbeke, and H. van Vlijmen, Bmc Bioinformatics, 2011. 12; ISI[000296097400001].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p class="MsoNoSpacing"></p>

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296300900005">Neolignans from Schisandra wilsoniana and Their Anti-human Immunodeficiency Virus-1 Activities.</a> Yang, G.Y., R.R. Wang, H.X. Mu, Y.K. Li, W.L. Xiao, L.M. Yang, J.X. Pu, Y.T. Zheng, and H.D. Sun, Chemical &amp; Pharmaceutical Bulletin, 2011. 59(11): p. 1344-1347; ISI[000296300900005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296231700066">Oligonucleotide Conjugates as Inhibitors of HIV-1 Integrase and Reverse Transcriptase.</a> Zatsepin, T., J. Agapkina, S. Korolev, and M. Gottikh, Nucleic Acid Therapeutics, 2011. 21(5): p. A31-A31; ISI[000296231700066].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296470200015">A Novel Ribonuclease with Potent HIV-1 Reverse Transcriptase Inhibitory Activity from Cultured Mushroom Schizophyllum commune</a>. Zhao, Y.Z., GQ Ng, TB Wang, HX, Journal of Microbiology, 2011. 49(5): p. 803-808; ISI[000296470200015].
    <br />
    <b>[WOS]</b>. HIV_1111-112411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
