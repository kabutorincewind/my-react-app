

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-287.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7QBHox5Gu71WN6RQw/OOqr5jVVSfPB5dYC4yUbT8lzkf5rrs9DNgLKb/05KvqorVWfZi2JxMW8txXYn5cWrNs9RtCEITo3PlMrLKkTWUnRKA0x93HKOE8uKnGx4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C42D4F3D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-287</b> </p>

<p>     1.    42606   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Indole, azaindole and related heterocyclic sulfonylureido piperazine derivatives</b>((Bristol-Myers Squibb Company, USA)</p>

<p>           Kadow, John F, Regueiro-Ren, Alicia, and Xue, Qiufen May</p>

<p>           PATENT: WO 2004000210 A2;  ISSUE DATE: 20031231</p>

<p>           APPLICATION: 2003; PP: 106 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     2.    42607   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Antiviral drugs: PA-457: a potent HIV inhibitor that disrupts core condensation by targeting a late step in Gag processing</p>

<p>           Anon</p>

<p>           Nature Reviews Drug Discovery 2003.  2(12): 948</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     3.    42608   HIV-LS-287; WOS-HIV-1/5/2004</p>

<p class="memofmt1-2">           Viral protein U counteracts a human host cell restriction that inhibits HIV-1 particle production</p>

<p>           Varthakavi, V, Smith, RM, Bour, SP, Strebel, K, and Spearman, P</p>

<p>           P NATL ACAD SCI USA 2003. 100(25): 15154-15159</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187227200098">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187227200098</a> </p><br />

<p>     4.    42609   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Preparation of 2,2-bis(hydroxymethyl)cyclopropylidenemethyl-purines and -pyrimidines as antiviral agents</b> ((Wayne State University, USA, The Regents of the University of Michigan, and Drach, john C.)</p>

<p>           Zemlicka, Jiri, Zhou, Shaoman, and Drach, John C</p>

<p>           PATENT: WO 2003104440 A2;  ISSUE DATE: 20031218</p>

<p>           APPLICATION: 2003; PP: 45 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     5.    42610   HIV-LS-287; WOS-HIV-1/5/2004</p>

<p class="memofmt1-2">           Synthesis and studies of 3&#39;-C-trifluoromethyl-beta-D-ribonucleosides bearing the five naturally occurring nucleic acid bases</p>

<p>           Jeannot, F, Gosselin, G, and Mathe, C</p>

<p>           NUCLEOS NUCLEOT NUCL 2003. 22(12):  2195-2202</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187216100010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187216100010</a> </p><br />

<p>     6.    42611   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Use of defensins as antiviral agents</b> ((Ciphergen Biosystems, Inc. USA, Aaron Diamond Aids Research Center, and The Rockefeller University))</p>

<p>           Zhang, Linqi, Ho, David D, Caffrey, Rebecca E, Dalmasso, Enrique A, and Mei, Jianfeng</p>

<p>           PATENT: WO 2003101394 A2;  ISSUE DATE: 20031211</p>

<p>           APPLICATION: 2003; PP: 107 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     7.    42612   HIV-LS-287; WOS-HIV-1/5/2004</p>

<p class="memofmt1-2">           Chemokine-induced cell death in CCR5-expressing neuroblastoma cells</p>

<p>           Cartier, L, Dubois-Dauphin, M, Hartley, O, Irminger-Finger, I, and Krause, KH</p>

<p>           J NEUROIMMUNOL 2003. 145(1-2): 27-39</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187219800004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187219800004</a> </p><br />

<p>    8.     42613   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Anti-CCR5 antibody and conjugates for treating human immunodeficiency virus 1 infection</b>((USA))</p>

<p>           Olson, William C, Maddon, Paul J, Tsurushita, Naoya, Hinton, Paul R, and Vasquez, Maximiliano </p>

<p>           PATENT: US 20030228306 A1;  ISSUE DATE: 20031211</p>

<p>           APPLICATION: 2003-43803; PP: 52 pp., Cont.-in-part of U.S. Provisional Ser. No. 358,886.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     9.    42614   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Preparation of pyrrolinylbenzothiadiazine dioxides as antiviral agents</b>((Smithkline Beecham Corporation, USA)</p>

<p>           Duffy, Kevin J, Evans, Karen A, Fitch, Duke M, Gates, Adam T, Johnston, Victor K, and Sarisky, Robert T</p>

<p>           PATENT: WO 2003099801 A1;  ISSUE DATE: 20031204</p>

<p>           APPLICATION: 2003; PP: 89 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   10.    42615   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Preparation of calixarene-derivatives having anti-viral activity</b>((Aids Care Pharma, Limited Ire.)</p>

<p>           Coveney, Donal and Costello, Benjamin</p>

<p>           PATENT: EP 1367044 A1;  ISSUE DATE: 20031203</p>

<p>           APPLICATION: 2003-11002; PP: 22 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   11.    42616   HIV-LS-287; WOS-HIV-1/5/2004</p>

<p class="memofmt1-2">           HIV-1 preferentially binds receptors copatched with cell-surface elastase</p>

<p>           Bristow, CL, Mercatante, DR, and Kole, R</p>

<p>           BLOOD 2003. 102(13): 4479-4486</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187122000044">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187122000044</a> </p><br />

<p>   12.    42617   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Stable helical viral C peptides for inhibiting viral or HIV-1 entry and drug screening</b>((Whitehead Institute for Biomedical Research, USA)</p>

<p>           Sia, Samuel K and Kim, Peter S</p>

<p>           PATENT: US 2003219451 A1;  ISSUE DATE: 20031127</p>

<p>           APPLICATION: 2002-21259; PP: 27 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   13.    42618   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Use of a small molecule CCR5 inhibitor in macaques to treat simian immunodeficiency virus infection or prevent simian-human immunodeficiency virus infection</p>

<p>           Veazey, Ronald S, Klasse, Per Johan, Ketas, Thomas J, Reeves, Jacqueline D, Piatak, Michael Jr, Kunstman, Kevin, Kuhmann, Shawn E, Marx, Preston A, Lifson, Jeffrey D, Dufour, Jason, Mefford, Megan, Pandrea, Ivona, Wolinsky, Steven M, Doms, Robert W, DeMartino, Julie A, Siciliano, Salvatore J, Lyons, Kathy, Springer, Martin S, and Moore, John P</p>

<p>           Journal of Experimental Medicine 2003. 198(10): 1551-1562</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   14.    42619   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Therapeutic Drug Monitoring of the HIV/AIDS Drugs Abacavir, Zidovudine, Efavirenz, Nevirapine, Indinavir, Lopinavir, and Nelfinavir</p>

<p>           Donnerer, J, Kronawetter, M, Kapper, A, Haas, I, and Kessler, HH</p>

<p>           Pharmacology 2003. 69(4): 197-204</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.     42620   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Preparation of piperazine derivatives as antiviral agents</b>((Bristol-Myers Squibb Company, USA)</p>

<p>           Wang, Tao, Wallace, Owen B, Meanwell, Nicholas A, Kadow, John F, Zhang, Zhongxing, and Yang, Zhong</p>

<p>           PATENT: WO 2003092695 A1;  ISSUE DATE: 20031113</p>

<p>           APPLICATION: 2003; PP: 121 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   16.    42621   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Preparation of phosphonate analogs of HIV protease inhibitors and methods for identifying anti-HIV therapeutic compounds</b>((Gilead Sciences, Inc. USA)</p>

<p>           Birkus, Gabriel, Chen, James M, Chen, Xiaowu, Cihlar, Tomas, Eisenberg, Eugene J, Hatada, Marcos, He, Gong-Xin, Kim, Choung U, Lee, William A, McDermott, Martin J, and Swaminathan, Sundaramoorthi</p>

<p>           PATENT: WO 2003090691 A2;  ISSUE DATE: 20031106</p>

<p>           APPLICATION: 2003; PP: 814 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   17.    42622   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Antiviral inhibitions of capsid proteins</b>((University of Maryland, Baltimore County USA)</p>

<p>           Summers, Michael F, Tang, Chun, and Huang, Mingjun</p>

<p>           PATENT: WO 2003089615 A2;  ISSUE DATE: 20031030</p>

<p>           APPLICATION: 2003; PP: 59 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   18.    42623   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Design and synthesis of phosphonoacetic acid (PPA) ester and amide bioisosters of ribofuranosylnucleo-side diphosphates as potential ribonucleotide reductase inhibitors and evaluation of their enzyme inhibitory, cytostatic and antiviral activity</p>

<p>           Manfredini, Stefano, Solaroli, Nicola, Angusti, Angela, Nalin, Federico, Durini, Elisa, Vertuani, Silvia, Pricl, Sabrina, Ferrone, Marco, Spadari, Silvio, Focher, Federico, Verri, Annalisa, De Clercq, Erik, and Balzarini, Jan</p>

<p>           Antiviral Chemistry &amp; Chemotherapy 2003. 14(4): 183-194</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   19.    42624   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Cytostatic activity of antiviral acyclic nucleoside phosphonates in rodent lymphocytes</p>

<p>           Zidek, Zdenek, Potmesil, Petr, and Holy, Antonin</p>

<p>           Toxicology and Applied Pharmacology 2003. 192(3): 246-253</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   20.    42625   HIV-LS-287; WOS-HIV-1/10/2004</p>

<p class="memofmt1-2">           HIV-1 transmission and cytokine-induced expression of DC-SIGN in human monocyte-derived macrophages</p>

<p>           Chehimi, J, Luo, Q, Azzoni, L, Shawver, L, Ngoubilly, N, June, R, Jerandi, G, Farabaugh, M, and Montaner, LJ</p>

<p>           J LEUKOCYTE BIOL 2003. 74(5): 757-763</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392400016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392400016</a> </p><br />

<p>   21.    42626   HIV-LS-287; WOS-HIV-1/10/2004</p>

<p class="memofmt1-2">           Drug-loaded red blood cell-mediated clearance of HIV-1 macrophage reservoir by selective inhibition of STAT1 expression</p>

<p>           Magnani, M, Balestra, E, Fraternale, A, Aquaro, S, Paiardini, M, Cervasi, B, Casabianca, A, Garaci, E, and Pernot, CF</p>

<p>           J LEUKOCYTE BIOL 2003. 74(5): 764-771</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392400017">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392400017</a> </p><br />

<p>  22.     42627   HIV-LS-287; WOS-HIV-1/10/2004</p>

<p class="memofmt1-2">           Macrophages and lymphocytes differentially modulate the ability of RANTES to inhibit HIV-1 infection</p>

<p>           Gross, E, Amella, CA, Pompucci, L, Franchin, G, Sherry, B, and Schmidtmayerova, H</p>

<p>           J LEUKOCYTE BIOL 2003. 74(5): 781-790</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392400019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392400019</a> </p><br />

<p>   23.    42628   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           N&#39;-{n-[3-oxo-20[29]lupen-28-oyl]-9-aminononanoyl}-3-amino-3-phenylpropionic acid eliciting immuno-stimulating and antiviral activity</b>((Novosibirskii Institut Organicheskoi Khimii im. N. N. Vorozhtsova SO RAN, Russia and Gosudarstvennyi Nauchnyi Tsentr Virusologii i Biotekhnologii \&quot;Vektor\&quot;))</p>

<p>           Tolstikov, GA, Petrenko, NI, Elantseva, NV, Shul&#39;ts, EE, Plyasunova, OA, Il&#39;icheva, TN, Borisova, OA, Pronyaeva, TR, and Pokrovskii, AG</p>

<p>           PATENT: RU 2211843 C1;  ISSUE DATE: 20030910</p>

<p>           APPLICATION: 2002-36802; PP: No pp. given</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   24.    42629   HIV-LS-287; WOS-HIV-1/10/2004</p>

<p class="memofmt1-2">           Selective inactivation of CCR5 and decreased infectivity of R5 HIV-1 strains mediated by opioid-induced heterologous desensitization</p>

<p>           Szabo, I, Wetzel, MA, Zhang, N, Steele, AD, Kaminsky, DE, Chen, CG, Liu-Chen, LY, Bednar, F, Henderson, EE, Howard, OMZ, Oppenheim, JJ, and Rogers, TJ</p>

<p>           J LEUKOCYTE BIOL 2003. 74(6): 1074-1082</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392600015">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187392600015</a> </p><br />

<p>   25.    42630   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Pokeweed antiviral protein polypeptides with antiviral activity</b>((Parker Hughes Institute, USA)</p>

<p>           Uckun, Faith M</p>

<p>           PATENT: WO 2003106479 A2;  ISSUE DATE: 20031224</p>

<p>           APPLICATION: 2003; PP: 62 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   26.    42631   HIV-LS-287; WOS-HIV-1/10/2004</p>

<p class="memofmt1-2">           Combinational therapy for potential HIV-1 eradication and vaccination</p>

<p>           Lin, SL and Ying, SY</p>

<p>           INT J ONCOL 2004. 24(1): 81-88</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187359500010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187359500010</a> </p><br />

<p>   27.    42632   HIV-LS-287; WOS-HIV-1/10/2004</p>

<p class="memofmt1-2">           Pharmacodynamics and clinical use of anti-HIV drugs</p>

<p>           Preston, SL, Piliero, PJ, and Drusano, GL</p>

<p>           INFECT DIS CLIN N AM 2003. 17(3): 651-+</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187439100010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187439100010</a> </p><br />

<p>   28.    42633   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Antiviral Activity of Enteric-Coated Didanosine, Stavudine, and Nelfinavir Versus Zidovudine Plus Lamivudine and Nelfinavir</p>

<p>           Gathe, Joseph Jr, Badaro, Roberto, Grimwood, Ashraf, Abrams, Lori, Klesczewski, Ken, Cross, Anne, and McLaren, Colin</p>

<p>           JAIDS, Journal of Acquired Immune Deficiency Syndromes 2002. 31(4): 399-403</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  29.     42634   HIV-LS-287; WOS-HIV-1/10/2004</p>

<p class="memofmt1-2">           HIV-1 integrase can process a 3 &#39;-end crosslinked substrate - Implications of DNA end-fraying requirement during the 3 &#39;-processing reaction</p>

<p>           Agapkina, J, Smolov, M, Zubin, E, Mouscadet, JF, and Gottikh, M</p>

<p>           EUR J BIOCHEM  2004. 271(1): 205-211</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187449200020">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187449200020</a> </p><br />

<p>   30.    42635   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Antiviral drug development and the impact of drug resistance</p>

<p>           Darby, Graham </p>

<p>           Symposium of the Society for General Microbiology 2001. 60th(New Challenges to Health: The Threat of Virus Infection): 311-339</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   31.    42636   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Substituted 1,3-oxathiolanes with antiviral properties</b>((Biochem Pharma Inc., Can.)</p>

<p>           Mansour, Tarek S and Jin, Haolun</p>

<p>           PATENT: US 6228860 B1;  ISSUE DATE: 20010508</p>

<p>           APPLICATION: 96-63195; PP: 20 pp., Cont.-in-part of U.S. 5,587,480.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   32.    42637   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Isolation, structure, and HIV-1-integrase inhibitory activity of structurally diverse fungal metabolites</p>

<p>           Singh, SB, Jayasuriya, H, Dewey, R, Polishook, JD, Dombrowski, AW, Zink, DL, Guan, Z, Collado, J, Platas, G, Pelaez, F, Felock, PJ, and Hazuda, DJ</p>

<p>           J Ind Microbiol Biotechnol 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14714192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14714192&amp;dopt=abstract</a> </p><br />

<p>   33.    42638   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p class="memofmt1-2">           Mutations conferring resistance to zidovudine diminish the antiviral effect of stavudine plus didanosine</p>

<p>           Izopet, J, Bicart-See, A, Pasquier, C, Sandres, K, Bonnet, E, Marchou, B, Puel, J, and Massip, P</p>

<p>           Journal of Medical Virology 99. 59(4): 507-511</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   34.    42639   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Peptide, nucleic acid encoding it, recombinant vectors, transformants, manufacture of the peptide, and antiviral agents</b>((Japan))</p>

<p>           Okamoto, Tamotsu</p>

<p>           PATENT: JP 11322794 A2;  ISSUE DATE: 19991124</p>

<p>           APPLICATION: 98-3637; PP: 7 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   35.    42640   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Antiviral agents for control of human immunodeficiency virus</b>((Nippon Eye Drop Kenkyusho K. K., Japan)</p>

<p>           Nozaki, Junko, Numata, Ryuichi, and Ito, Koichi</p>

<p>           PATENT: JP 11147806 A2;  ISSUE DATE: 19990602</p>

<p>           APPLICATION: 97-50360; PP: 4 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.     42641   HIV-LS-287; SCIFIND-HIV-01/07/2004</p>

<p><b>           Preparation of 2-mercapto-N-(5-amino-1,2,4-triazol-3-yl)benzenesulfonamides as anti-AIDS and antitumor agents</b> ((Akademia Medyczna, Pol.)</p>

<p>           Brzozowski, Zdzislaw</p>

<p>           PATENT: PL 173244 B1;  ISSUE DATE: 19980227</p>

<p>           APPLICATION: 93-39608; PP: 7 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   37.    42642   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           New bicyclam-GalCer analogue conjugates: synthesis and in vitro anti-HIV activity</p>

<p>           Daoudi, JM, Greiner, J, Aubertin, AM, and Vierling, P</p>

<p>           Bioorg Med Chem Lett 2004. 14(2): 495-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14698189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14698189&amp;dopt=abstract</a> </p><br />

<p>   38.    42643   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           2,6-Bis(3,4,5-trihydroxybenzylydene) derivatives of cyclohexanone. novel potent HIV-1 integrase inhibitors that prevent HIV-1 multiplication in cell-based assays</p>

<p>           Costi, R, Santo, RD, Artico, M, Massa, S, Ragno, R, Loddo, R, La, Colla M, Tramontano, E, La, Colla P, and Pani, A</p>

<p>           Bioorg Med Chem 2004. 12(1): 199-215</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14697785&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14697785&amp;dopt=abstract</a> </p><br />

<p>   39.    42644   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Water-soluble prodrugs of dipeptide HIV protease inhibitors based on O--&gt;N intramolecular acyl migration: Design, synthesis and kinetic study</p>

<p>           Hamada, Y, Matsumoto, H, Yamaguchi, S, Kimura, T, Hayashi, Y, and Kiso, Y</p>

<p>           Bioorg Med Chem 2004. 12(1): 159-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14697781&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14697781&amp;dopt=abstract</a> </p><br />

<p>   40.    42645   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Small-Molecule Inhibition of Human Immunodeficiency Virus Type 1 Replication by Specific Targeting of the Final Step of Virion Maturation</p>

<p>           Zhou, J, Yuan, X, Dismuke, D, Forshey, BM, Lundquist, C, Lee, KH, Aiken, C, and Chen, CH</p>

<p>           J Virol 2004. 78(2): 922-929</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14694123&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14694123&amp;dopt=abstract</a> </p><br />

<p>   41.    42646   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Role of the Ectodomain of the gp41 Transmembrane Envelope Protein of Human Immunodeficiency Virus Type 1 in Late Steps of the Membrane Fusion Process</p>

<p>           Bar, S and Alizon, M</p>

<p>           J Virol 2004. 78(2): 811-20</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14694113&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14694113&amp;dopt=abstract</a> </p><br />

<p>  42.     42647   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Inhibition of Human Immunodeficiency Virus Type 1 Entry in Cells Expressing gp41-Derived Peptides</p>

<p>           Egelhofer, M, Brandenburg, G, Martinius, H, Schult-Dietrich, P, Melikyan, G, Kunert, R, Baum, C, Choi, I, Alexandrov, A, and Von, Laer D</p>

<p>           J Virol 2004. 78(2): 568-575</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14694088&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14694088&amp;dopt=abstract</a> </p><br />

<p>   43.    42648   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           In vitro evaluation of nonnucleoside reverse transcriptase inhibitors UC-781 and TMC120-R147681 as human immunodeficiency virus microbicides</p>

<p>           Van, Herrewege Y, Michiels, J, Van, Roey J, Fransen, K, Kestens, L, Balzarini, J, Lewi, P, Vanham, G, and Janssen, P</p>

<p>           Antimicrob Agents Chemother 2004.  48(1): 337-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693562&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693562&amp;dopt=abstract</a> </p><br />

<p>   44.    42649   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Single-dose and steady-state pharmacokinetics of tenofovir disoproxil fumarate in human immunodeficiency virus-infected children</p>

<p>           Hazra, R, Balis, FM, Tullio, AN, DeCarlo, E, Worrell, CJ, Steinberg, SM, Flaherty, JF, Yale, K, Poblenz, M, Kearney, BP, Zhong, L, Coakley, DF, Blanche, S, Bresson, JL, Zuckerman, JA, and Zeichner, SL</p>

<p>           Antimicrob Agents Chemother 2004.  48(1): 124-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693529&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693529&amp;dopt=abstract</a> </p><br />

<p>   45.    42650   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Genotypic resistance of antiretroviral drugs among drug-naive HIV type 1 patients with the background of long-term access-easy zidovudine therapy</p>

<p>           Park, SW, Kim, HB, Choi, YJ, Kim, NJ, Oh, MD, and Choe, KW</p>

<p>           AIDS Res Hum Retroviruses 2003. 19(11):  1039-43</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14686324&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14686324&amp;dopt=abstract</a> </p><br />

<p>   46.    42651   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Thermolytic carbonates for potential 5&#39;-hydroxyl protection of deoxyribonucleosides</p>

<p>           Chmielewski, MK, Marchan, V, Cieslak, J, Grajkowski, A, Livengood, V, Munch, U, Wilk, A, and Beaucage, SL</p>

<p>           J Org Chem 2003. 68(26): 10003-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14682694&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14682694&amp;dopt=abstract</a> </p><br />

<p>   47.    42652   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Low level expression of the HIV-1 Nef protein in transiently transfected COS-1 cells in culture</p>

<p>           Hartz, PA, McMiller, T, Scott-Wright, D, and Samuel, KP</p>

<p>           Cell Mol Biol (Noisy-le-grand) 2003.  49(7): 1101-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14682392&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14682392&amp;dopt=abstract</a> </p><br />

<p>  48.     42653   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Meeting notes from the 43rd Interscience Conference on Antimicrobial Agents and Chemotherapy (ICAAC). New CCR5 antagonist shows antiretroviral effect</p>

<p>           Feinberg, J</p>

<p>           AIDS Clin Care 2003. 15(11): 94-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14682273&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14682273&amp;dopt=abstract</a> </p><br />

<p>   49.    42654   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           Substitution for protease inhibitors in HIV therapy</p>

<p>           Hirschel, B</p>

<p>           N Engl J Med 2003. 349(25): 2460-1; author reply 2460-1</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14681514&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14681514&amp;dopt=abstract</a> </p><br />

<p>   50.    42655   HIV-LS-287; PUBMED-HIV-1/12/2004</p>

<p class="memofmt1-2">           The integrase of the human immunodeficiency virus as a novel target for the antiviral therapy of AIDS</p>

<p>           Witvrouw, M and Debyser, Z</p>

<p>           Verh K Acad Geneeskd Belg 2003. 65(5): 325-34</p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14671848&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14671848&amp;dopt=abstract</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
