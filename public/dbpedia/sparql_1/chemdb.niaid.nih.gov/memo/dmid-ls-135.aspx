

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-135.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JFwfmXLKUdouxs1Ilv2e4uqm5ECNR4ndDfYFlDA57HupjSf9tx2pJA+q06xheHY6zuMR7VR9Z13EudLk+wbQlNQRejQWfKOCTZjpphHIAIYMgcnD+/Hcj/NjBic=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A1308C80" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-135-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59831   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Efficacy of Orally Administered T-705 on Lethal Avian Influenza A (H5N1) Virus Infections in Mice</p>

    <p>          Sidwell, RW, Barnard, DL, Day, CW, Smee, DF, Bailey, KW, Wong, MH, Morrey, JD, and Furuta, Y</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17194832&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17194832&amp;dopt=abstract</a> </p><br />

    <p>2.     59832   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          prepn of substituted phosphate esters of nucleoside phosphonates as antiviral and antitumor agents</p>

    <p>          Hostetler, Karl Y, Beadle, James R, and Ruiz, Jacqueline C</p>

    <p>          PATENT:  WO <b>2006130217</b>  ISSUE DATE:  20061207</p>

    <p>          APPLICATION: 2006  PP: 79pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     59833   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Antiviral/antiinflammatory agents containing phenol derivatives</p>

    <p>          Higuchi, Tomokazu, Higuchi, Masanori, Higuchi, Noriyuki, Koyama, Hajime, Shibata, Hirofumi, and Higuchi, Tomihiko</p>

    <p>          PATENT:  JP <b>2006306836</b>  ISSUE DATE:  20061109</p>

    <p>          APPLICATION: 2006-4252  PP: 23pp.</p>

    <p>          ASSIGNEE:  (Micro Biotech K. K., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     59834   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Aurintricarboxylic acid inhibits the early stage of vaccinia virus replication by targeting both cellular and viral factors</p>

    <p>          Myskiw, C, Deschambault, Y, Jefferies, K, He, R, and Cao, J</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17192307&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17192307&amp;dopt=abstract</a> </p><br />

    <p>5.     59835   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Anti-Inflammatory, Anti-Tumor-Promoting, and Cytotoxic Activities of Constituents of Marigold (Calendula officinalis) Flowers</p>

    <p>          Ukiya, M, Akihisa, T, Yasukawa, K, Tokuda, H, Suzuki, T, and Kimura, Y</p>

    <p>          J Nat Prod <b>2006</b>.  69(12): 1692-1696</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17190444&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17190444&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59836   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Characterization of the West Nile virus protease substrate specificity and inhibitors</p>

    <p>          Mueller, NH, Yon, C, Ganesh, VK, and Padmanabhan, R</p>

    <p>          Int J Biochem Cell Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188926&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188926&amp;dopt=abstract</a> </p><br />

    <p>7.     59837   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Methods and compositions for the treatment of viral diseases</p>

    <p>          Veas, Francisco, Misse, Dorothee, Bray, Dorothy, and Clerici, Mario</p>

    <p>          PATENT:  EP <b>1723971</b>  ISSUE DATE: 20061122</p>

    <p>          APPLICATION: 29  PP: 19pp.</p>

    <p>          ASSIGNEE:  (Immunoclin Limited, UK and Institut De Recherche Pour Le Developpement)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     59838   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Substrate specificity of recombinant dengue 2 virus NS2B-NS3 protease: Influence of natural and unnatural basic amino acids on hydrolysis of synthetic fluorescent substrates</p>

    <p>          Gouvea, IE, Izidoro, MA, Judice, WA, Cezari, MH, Caliendo, G, Santagada, V, Dos, Santos CN, Queiroz, MH, Juliano, MA, Young, PR, Fairlie, DP, and Juliano, L</p>

    <p>          Arch Biochem Biophys <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17184724&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17184724&amp;dopt=abstract</a> </p><br />

    <p>9.     59839   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Inhibitory activities of bovine macromolecular whey proteins on rotavirus infections in vitro and in vivo</p>

    <p>          Bojsen, A, Buesa, J, Montava, R, Kvistgaard, AS, Kongsbak, MB, Petersen, TE, Heegaard, CW, and Rasmussen, JT</p>

    <p>          J Dairy Sci <b>2007</b>.  90(1): 66-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17183076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17183076&amp;dopt=abstract</a> </p><br />

    <p>10.   59840   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Inhibition of filovirus replication by the zinc finger antiviral protein</p>

    <p>          Muller, S, Moller, P, Bick, MJ, Wurr, S, Becker, S, Gunther, S, and Kummerer, BM</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17182693&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17182693&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59841   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Synergy of small molecular inhibitors of HCV replication directed at multiple viral targets</p>

    <p>          Wyles, DL, Kaihara, KA, Vaida, F, and Schooley, RT</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17182685&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17182685&amp;dopt=abstract</a> </p><br />

    <p>12.   59842   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Evaluation of immunomodulators, interferons and known in vitro SARS-coV inhibitors for inhibition of SARS-coV replication in BALB/c mice</p>

    <p>          Barnard, DL, Day, CW, Bailey, K, Heiner, M, Montgomery, R, Lauridsen, L, Chan, PK, and Sidwell, RW</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(5): 275-284</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17176632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17176632&amp;dopt=abstract</a> </p><br />

    <p>13.   59843   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Anti-influenza virus activities of 4-[(1,2-dihydro-2-oxo-3H-indol-3-ylidene)amino]-N-(4,6-dimethyl-2-pyrimidi n-2-yl)benzenesulphonamide and its derivatives</p>

    <p>          Selvam, P, Murugesh, N, Chandramohan, M, Sidwell, RW, Wandersee, MK, and Smee, DF</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(5): 269-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17176631&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17176631&amp;dopt=abstract</a> </p><br />

    <p>14.   59844   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Simultaneously inhibition of HIV and HBV replication through a dual small interfering RNA expression system</p>

    <p>          Wu, K, Mu, Y, Hu, J, Lu, L, Zhang, X, Yang, Y, Li, Y, Liu, F, Song, D, Zhu, Y, and Wu, J</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17173982&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17173982&amp;dopt=abstract</a> </p><br />

    <p>15.   59845   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Preparation of carbazoles and related compounds for treatment of dengue fever, yellow fever, west nile virus, and hepatitis C virus infection</p>

    <p>          Gudmundsson, Kristjan</p>

    <p>          PATENT:  WO <b>2006121467</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2005  PP: 59pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>16.   59846   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Treatment or prophylaxis of Flaviviridae viruses using substituted 2,3,4,9-tetrahydro-1H-carbazoles and related compounds</p>

    <p>          Gudmundsson, Kristjan</p>

    <p>          PATENT:  WO <b>2006121466</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2005  PP: 70pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   59847   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Progress in computational approach to drug development against SARS</p>

    <p>          Chou, KC, Wei, DQ, Du, QS, Sirois, S, and Zhong, WZ</p>

    <p>          Curr Med Chem <b>2006</b>.  13(27): 3263-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168850&amp;dopt=abstract</a> </p><br />

    <p>18.   59848   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Drug design targeting the main protease, the Achilles&#39; heel of coronaviruses</p>

    <p>          Yang, H, Bartlam, M, and Rao, Z</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(35): 4573-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168763&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168763&amp;dopt=abstract</a> </p><br />

    <p>19.   59849   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Influenza virion-derived viral ribonucleoproteins synthesize both mRNA and cRNA in vitro</p>

    <p>          Vreede, FT and Brownlee, GG</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166911&amp;dopt=abstract</a> </p><br />

    <p>20.   59850   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Synthesis of the 5-phosphono-pent-2-en-1-yl nucleosides: A new class of antiviral acyclic nucleoside phosphonates</p>

    <p>          Choo, H, Beadle, JR, Chong, Y, Trahan, J, and Hostetler, KY</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166725&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166725&amp;dopt=abstract</a> </p><br />

    <p>21.   59851   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Antivirotic function of 17-(tetrahydrofuran-2&#39;-yl) methanamine-17- demethoxy geldanamycin</p>

    <p>          Li, Zhuorong, Tao, Peizhen, Shan, Guangzhi, Li, Yuhuan, and You, Xuefu</p>

    <p>          PATENT:  CN <b>1827617</b>  ISSUE DATE: 20060906</p>

    <p>          APPLICATION: 1007-2996  PP: 6pp.</p>

    <p>          ASSIGNEE:  (Institute of Medicinal Biotechnology, Chinese Academy of Medical Sciences Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   59852   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Inhibition of multiple strains of Venezuelan equine encephalitis virus by a pool of four short interfering RNAs</p>

    <p>          O&#39;brien, L</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157930&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157930&amp;dopt=abstract</a> </p><br />

    <p>23.   59853   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Respiratory syncytial virus bronchiolitis : current and future strategies for treatment and prophylaxis</p>

    <p>          Chavez-Bueno, S, Mejias, A, and Welliver, RC</p>

    <p>          Treat Respir Med <b>2006</b>.  5(6): 483-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17154675&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17154675&amp;dopt=abstract</a> </p><br />

    <p>24.   59854   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          RNAi expression mediated inhibition of HCV replication</p>

    <p>          Hamazaki, H, Ujino, S, Abe, E, Miyano-Kurosaki, N, Shimotohno, K, and Takaku, H</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2004</b>.(48): 307-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150601&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150601&amp;dopt=abstract</a> </p><br />

    <p>25.   59855   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Selection of thioaptamers for diagnostics and therapeutics</p>

    <p>          Yang, X, Wang, H, Beasley, DW, Volk, DE, Zhao, X, Luxon, BA, Lomas, LO, Herzog, NK, Aronson, JF, Barrett, AD, Leary, JF, and Gorenstein, DG</p>

    <p>          Ann N Y Acad Sci <b>2006</b>.  1082: 116-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17145932&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17145932&amp;dopt=abstract</a> </p><br />

    <p>26.   59856   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Antiviral etiotropic chemicals: efficacy against influenza A viruses A subtype H5N1</p>

    <p>          Leneva I A and Shuster A M</p>

    <p>          Vopr Virusol <b>2006</b>.  51(5): 4-7.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   59857   DMID-LS-135; PUBMED-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Conclusions and recommendations of the Advisory Committee on Poliomyelitis eradication, Geneva, 11-12 October 2006, Part I</p>

    <p>          Anon</p>

    <p>          Wkly Epidemiol Rec <b>2006</b>.  81(48): 453-460</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139799&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139799&amp;dopt=abstract</a> </p><br />

    <p>28.   59858   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Flavivirus NS5A proteins for the treatment of HIV</p>

    <p>          Stapleton, Jack T, Xiang, Jinhua, Chang, Qing, and Mclinden, James</p>

    <p>          PATENT:  WO <b>2006088664</b>  ISSUE DATE:  20060824</p>

    <p>          APPLICATION: 2006  PP: 112 pp.</p>

    <p>          ASSIGNEE:  (The University of Iowa Research Foundation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   59859   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Inhibition of infections by viruses having class II fusion proteins with alphavirus class II fusion protein domain III peptides</p>

    <p>          Kielian, Margaret and Liao, Maofu</p>

    <p>          PATENT:  WO <b>2006121809</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2006  PP: 62pp.</p>

    <p>          ASSIGNEE:  (Albert Einstein College of Medicine of Yeshiva University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   59860   DMID-LS-135; SCIFINDER-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Peptides derived from HIV-1, HIV-2, Ebola virus, SARS coronavirus and coronavirus 229E exhibit high affinity binding to the formyl peptide receptor</p>

    <p>          Mills, John S</p>

    <p>          Biochimica et Biophysica Acta, Molecular Basis of Disease <b>2006</b>.  1762(7): 693-703</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   59861   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Syntheses of 5-Fluoro and (E)-5-(2-Fluorovinyl) Arabinosyl Uridine Analogues as Potential Probes for the Hsv-1 Thymidine Kinase Gene</p>

    <p>          Yu, C. <i>et al.</i></p>

    <p>          Synthesis-Stuttgart <b>2006</b>.(22): 3835-3840</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242535300011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242535300011</a> </p><br />

    <p>32.   59862   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Synthesis, Characterization, and Antiviral Activities of Pyridopyrazolotriazines</p>

    <p>          Attaby, F. <i>et al.</i></p>

    <p>          Phosphorus Sulfur and Silicon and the Related Elements <b>2007</b>.  182(1): 133-149</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242549600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242549600012</a> </p><br />

    <p>33.   59863   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Inhibition of Influenza Virus Infection by a Novel Antiviral Peptide That Targets Viral Attachment to Cells</p>

    <p>          Jones, J. <i>et al.</i></p>

    <p>          Journal of Virology <b>2006</b>.  80(24): 11960-11967</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242617900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242617900006</a> </p><br />

    <p>34.   59864   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Grid-Enabled High-Throughput in Silico Screening Against Influenza a Neuraminidase</p>

    <p>          Lee, H. <i>et al.</i></p>

    <p>          Ieee Transactions on Nanobioscience <b>2006</b>.  5(4): 288-295</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242751900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242751900011</a> </p><br />
    <br clear="all">

    <p>35.   59865   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Future Perspectives and Strategies in Managing Resistance to Analogue Antiviral Drugs in the Treatment of Chronic Hepatitis B</p>

    <p>          Bourliere, M.</p>

    <p>          Gastroenterologie Clinique Et Biologique <b>2006</b>.  30(10): S34-S36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242601200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242601200009</a> </p><br />

    <p>36.   59866   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Vaccination of Healthy Volunteers With Human Papillomavirus Type 16 L2e7e6 Fusion Protein Induces Serum Antibody That Neutralizes Across Papillomavirus Species</p>

    <p>          Gambhira, R. <i>et al.</i></p>

    <p>          Cancer Research <b>2006</b>.  66(23): 11120-11124</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242614300008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242614300008</a> </p><br />

    <p>37.   59867   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Type I Interferons and Their Antiviral Function</p>

    <p>          Leroy, M. and Desmecht, D.</p>

    <p>          Annales De Medecine Veterinaire <b>2006</b>.  150(2): 73-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242614700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242614700001</a> </p><br />

    <p>38.   59868   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Development and Characterization of a Rift Valley Fever Virus Cell-Cell Fusion Assay Using Alphavirus Replicon Vectors</p>

    <p>          Filone, C. <i>et al.</i></p>

    <p>          Virology <b>2006</b>.  356(1-2): 155-164</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242424800017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242424800017</a> </p><br />

    <p>39.   59869   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Cross-Protective Immunity in Mice Induced by Live-Attenuated or Inactivated Vaccines Against Highly Pathogenic Influenza a (H5n1) Viruses</p>

    <p>          Lu, X. <i>et al.</i></p>

    <p>          Vaccine <b>2006</b>.  24(44-46): 6588-6593</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242444000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242444000006</a> </p><br />

    <p>40.   59870   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Hepatitis C Virus Ns4a Inhibits Cap-Dependent and the Viral Ires-Mediated Translation Through Interacting With Eukaryotic Elongation Factor 1a</p>

    <p>          Kou, Y. <i>et al.</i></p>

    <p>          Journal of Biomedical Science <b>2006</b>.  13(6): 861-874</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242509600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242509600012</a> </p><br />

    <p>41.   59871   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Hepatitis B Virus Inhibiting Constituents From Herpetospermum Caudigerum</p>

    <p>          Yuan, H. <i>et al.</i></p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2006</b>.  54(11): 1592-1594</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242481000017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242481000017</a> </p><br />

    <p>42.   59872   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Cyclooxygenase-2 Inhibition Attenuates Antibody Responses Against Human Papillomavirus-Like Particles</p>

    <p>          Ryan, E. <i>et al.</i></p>

    <p>          Journal of Immunology <b>2006</b>.  177(11): 7811-7819</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242261800039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242261800039</a> </p><br />

    <p>43.   59873   DMID-LS-135; WOS-DMID-1/2/2007</p>

    <p class="memofmt1-2">          Human Papillomavirus Reactivation Following Topical Tacrolimus Therapy of Anogenital Lichen Sclerosus</p>

    <p>          Bilenchi, R. <i>et al.</i></p>

    <p>          International Journal of Std &amp; Aids <b>2006</b>.  17: 58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242128800185">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242128800185</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
