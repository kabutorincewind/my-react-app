

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-03-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OlWWyqINg9h95IQ8HS3lztNRmtWFNjJNS2qUe0uus1JmMRPoM6ON/seHTlGDbG/WBWQ6YXMeoG8BEnmuh/dLnDdkUDGAe+3QDhntZNLHFs41fHWf5aTjnB9acMo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="21E1BF12" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: February 27 - March 12, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25503363">Isolation of a Ribonuclease with Antiproliferative and HIV-1 Reverse Transcriptase Inhibitory Activities from Japanese Large Brown Buckwheat Seeds.</a> Yuan, S., J. Yan, X. Ye, Z. Wu, and T. Ng. Applied Biochemistry and Biotechnology, 2015. 175(5): p. 2456-2467. PMID[25503363]. 
    <br />
    <b>[PubMed]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25055732">Molecular Modeling, Synthesis and Biological Evaluation of N-Heteroaryl Compounds as Reverse Transcriptase Inhibitors against HIV-1.</a> Singh, A., D. Yadav, M. Yadav, A. Dhamanage, S. Kulkarni, and R.K. Singh. Chemical Biology &amp; Drug Design, 2015. 85(3): p. 336-347. PMID[25055732]. 
    <br />
    <b>[PubMed]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25682937">Synthesis, Biological Evaluation and Molecular Docking of Calix[4]arene-based beta-Diketo Derivatives as HIV-1 Integrase Inhibitors.</a> Luo, Z., Y. Zhao, C. Ma, Z. Li, X. Xu, L. Hu, N. Huang, and H. He. Archiv der Pharmazie, 2015. 348(3): p. 206-213. PMID[25682937]. 
    <br />
    <b>[PubMed]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25623024">Azasugar-containing Phosphorothioate Oligonucleotide (Azpson) DBM-2198 Inhibits Human Immunodeficiency Virus Type 1 (HIV-1) Replication by Blocking HIV-1 gp120 without Affecting the V3 Region.</a> Lee, J., S.E. Byeon, J.Y. Jung, M.H. Kang, Y.J. Park, K.E. Jung, and Y.S. Bae. Molecules and Cells, 2015. 38(2): p. 122-129. PMID[25623024]. 
    <br />
    <b>[PubMed]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25710354">The First Synthesis of 4&#39;-Branched 5&#39;-Deoxycarbocyclic 9-deazaadenosine and Phosphonic acids as Antiviral Agents.</a> Kim, S., E. Kim, and J.H. Hong. Nucleosides, Nucleotides &amp;Nucleic Acids, 2015. 34(3): p. 163-179. PMID[25710354]. 
    <br />
    <b>[PubMed]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25532055">Design and Synthesis of alpha-Carboxy Nucleoside Phosphonate Analogues and Evaluation as HIV-1 Reverse Transcriptase-targeting Agents.</a> Keane, S.J., A. Ford, N.D. Mullins, N.M. Maguire, T. Legigan, J. Balzarini, and A.R. Maguire. Journal of Organic Chemistry, 2015. 80(5): p. 2479-2493. PMID[25532055]. 
    <br />
    [<b>PubMed]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25638498">Design, Synthesis, and Biological Evaluation of Novel 2-Methylpiperazine Derivatives as Potent CCR5 Antagonists.</a> Hu, S., Z. Wang, T. Hou, X. Ma, J. Li, T. Liu, X. Xie, and Y. Hu. Bioorganic &amp; Medicinal Chemistry, 2015. 23(5): p. 1157-1168. PMID[25638498]. 
    <br />
    <b>[PubMed]</b>. HIV_0227-031215.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000349353400002">Endogenous CCL2 Neutralization Restricts HIV-1 Replication in Primary Human Macrophages by Inhibiting Viral DNA Accumulation.</a> Sabbatucci, M. Covino, D.A. Purificato, C. Mallano, A. Federioc, M. Lu, J. Rinaldi, A.O. Pellegrini, M. Bona, R. Michelini, Z. Cara, A. Vella, S. Gessani, S. Andreotti, M. Fantuzzi. Retrovirology, 2015. 12(4): 22pp. ISI[000349353400002]. 
    <br />
    <b>[WOS]</b>. HIV_0227-031215.
    <br />
    <br /></p>

    <p class="plaintext">9. Aspartic <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000348951900038">Protease Inhibitors Containing Tertiary Alcohol Transition-state Mimics.</a> Motwani, H.V. De Rosa, M. Odell, L.R. Hallberg, A. Larhed. European Journal of Medicinal Chemistry, 2015. 90: p. 462-490. ISI[000348951900038]. 
    <br />
    <b>[WOS]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000349331700009">Design, Synthesis and Biological Activity of Aromatic Diketone Derivatives as HIV-1 Integrase Inhibitors.</a> Hu, L.M., Z.P. Li, Z.Y. Wang, G.X. Liu, X.Z. He, X.L. Wang, and C.C. Zeng. Medicinal Chemistry, 2015. 11(2): p. 180-187. ISI[000349331700009].
    <br />
     <b>[WOS]</b>. HIV_0227-031215.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">11. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150115&amp;CC=WO&amp;NR=2015006731A1&amp;KC=A1">Preparation of Polycyclic Carbamoylpyridone Derivatives Useful in the Treatment of HIV Infection.</a> Ji, M., S.E. Lazerwith, and H.-J. Pyun. Patent. 2015. 2014-US46413 2015006731: 103pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">12. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150115&amp;CC=WO&amp;NR=2015006733A1&amp;KC=A1">Preparation of Polycyclic Carbamoylpyridone Derivatives Useful for the Treatment of HIV Infection.</a> Ji, M., T.A.T. Martin, M.C. Desai, H. Jin, and H.-J. Pyun. Patent. 2015. 2014-US46415</p>

    <p class="plaintext">2015006733: 96pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">13. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150205&amp;CC=WO&amp;NR=2015017399A1&amp;KC=A1">Compositions and Methods for Modulating HIV Activation.</a> Karn, J. and B. Das. Patent. 2015. 2014-US48596 2015017399: 52pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150219&amp;CC=WO&amp;NR=2015022698A1&amp;KC=A1">Transitmycin Effectiveness against Bacterial and Viral Pathogens.</a> Kumar, V., M. Doble, B. Ramasamy, S. Ganesan, R. Manikkam, L.E. Hanna, S. Swaminathan, and S. Nagamiah. Patent. 2015. 2014-IN20 2015022698: 68pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150108&amp;CC=WO&amp;NR=2015001125A1&amp;KC=A1">Inhibitors of Viral Replication, Their Process of Preparation and Their Therapeutical Uses.</a> Ledoussal, B., F. Le Strat, S. Chasset, J. Barbion, J. Brias, A. Caravano, F. Faivre, and S. Vomscheid. Patent. 2015. 2014-EP64446 2015001125: 202pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150219&amp;CC=WO&amp;NR=2015022351A1&amp;KC=A1">Medicament Comprising a Pharmaceutical Combination of Dolutegravir, Emtricitabine and Tenofovir for Treatment of Viral Infections.</a> Renner, J. Patent. 2015. 2014-EP67305 2015022351: 31pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150205&amp;CC=WO&amp;NR=2015017393A2&amp;KC=A2">Preparation of Piperazine Derivatives as HIV Protease Inhibitors.</a> Williams, P.D., J.A. McCauley, D.J. Bennett, C.J. Bungard, L. Chang, X.-J. Chu, M.P. Dwyer, M.K. Holloway, K.M. Keertikar, H.M. Loughran, J.J. Manikowski, G.J. Morriello, D.-M. Shen, E.C. Sherer, J. Schulz, S.T. Waddell, C.M. Wiscount, N. Zorn, T. Satyanarayana, S. Vijayasaradhi, B. Hu, T. Ji, and B. Zhong. Patent. 2015. 2014-US48581 2015017393: 326pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>

    <br />

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150205&amp;CC=WO&amp;NR=2015013835A1&amp;KC=A1">Preparation of Piperazine Derivatives as HIV Protease Inhibitors.</a> Williams, P.D., J.A. McCauley, D.J. Bennett, C.J. Bungard, L. Chang, X.-J. Chu, M.P. Dwyer, M.K. Holloway, K.M. Keertikar, H.M. Loughran, J.J. Manikowski, G.J. Morriello, D.-M. Shen, E.C. Sherer, J. Schulz, S.T. Waddell, C.M. Wiscount, N. Zorn, S. Tummanapalli, V. Sivalenka, B. Hu, T. Ji, and B. Zhong. Patent. 2015. 2013-CN899 2015013835: 340pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0227-031215.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
