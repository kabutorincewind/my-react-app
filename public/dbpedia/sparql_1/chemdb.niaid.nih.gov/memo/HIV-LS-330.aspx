

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-330.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="E4eCIlUvdzm2WB9RAG8r3MpQKorAKLr+Iw11eUjIDYw67hwTJj26yz8VeK7wOe+3mhctPGh68NgwDpxhds+bWFEjHte5anQlHwvuueF0vpDcWUj+O4kcM84VSio=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2773D173" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-330-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47820   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          Antimicrobial Peptides from Amphibian Skin Potently Inhibit Human Immunodeficiency Virus Infection and Transfer of Virus from Dendritic Cells to T Cells</p>

    <p>          Vancompernolle, SE, Taylor, RJ, Oswald-Richter, K, Jiang, J, Youree, BE, Bowie, JH, Tyler, MJ, Conlon, JM, Wade, D, Aiken, C, Dermody, TS, Kewalramani, VN, Rollins-Smith, LA, and Unutmaz, D</p>

    <p>          J Virol <b>2005</b>.  79(18): 11598-11606</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140737&amp;dopt=abstract</a> </p><br />

    <p>2.     47821   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          Two new sesquiterpenoids and anti-HIV principles from the root bark of Zanthoxylum ailanthoides</p>

    <p>          Cheng, MJ, Lee, KH, Tsai, IL, and Chen, IS</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140017&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140017&amp;dopt=abstract</a> </p><br />

    <p>3.     47822   HIV-LS-330; EMBASE-HIV-9/6/2005</p>

    <p class="memofmt1-2">          4-Phenylcoumarins as HIV transcription inhibitors</p>

    <p>          Bedoya, Luis M, Beltran, Manuela, Sancho, Rocio, Olmedo, Dionisio A, Sanchez-Palomino, Sonsoles, del Olmo, Esther, Lopez-Perez, Jose L, Munoz, Eduardo, Feliciano, Arturo San, and Alcami, Jose</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4H0J8H5-5/2/473bd743477fc084218d83a6e44d06cd">http://www.sciencedirect.com/science/article/B6TF9-4H0J8H5-5/2/473bd743477fc084218d83a6e44d06cd</a> </p><br />

    <p>4.     47823   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          4&#39;-Ethynylstavudine (4&#39;-Ed4T) has potent anti-HIV-1 activity with reduced toxicity and shows a unique activity profile against drug-resistant mutants</p>

    <p>          Tanaka, H, Haraguchi, K, Kumamoto, H, Baba, M, and Cheng, YC</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(4): 217-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130520&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130520&amp;dopt=abstract</a> </p><br />

    <p>5.     47824   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          High-throughput human immunodeficiency virus type 1 (HIV-1) full replication assay that includes HIV-1 Vif as an antiviral target</p>

    <p>          Cao, J, Isaacson, J, Patick, AK, and Blair, WS</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(9): 3833-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127060&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127060&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47825   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          An anti-HIV microbicide comes alive</p>

    <p>          Lagenaur, LA and Berger, EA</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.  102(35): 12294-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16118279&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16118279&amp;dopt=abstract</a> </p><br />

    <p>7.     47826   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p><b>          Synthesis and evaluation of anti-HIV activity of isatin beta-thiosemicarbazone derivatives</b> </p>

    <p>          Bal, TR, Anand, B, Yogeeswari, P, and Sriram, D</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115762&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115762&amp;dopt=abstract</a> </p><br />

    <p>8.     47827   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel methyl branched cyclopropyl phosphonic acid nucleosides</p>

    <p>          Kim, JW, Ko, OH, and Hong, JH</p>

    <p>          Arch Pharm Res  <b>2005</b>.  28(7): 745-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16114485&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16114485&amp;dopt=abstract</a> </p><br />

    <p>9.     47828   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          Anti-HIV activity of dibenzylbutyrolactone-type lignans from Phenax species endemic in Costa Rica</p>

    <p>          Piccinelli, AL, Mahmood, N, Mora, G, Poveda, L, De, Simone F, and Rastrelli, L</p>

    <p>          J Pharm Pharmacol <b>2005</b>.  57(9): 1109-16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16105232&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16105232&amp;dopt=abstract</a> </p><br />

    <p>10.   47829   HIV-LS-330; PUBMED-HIV-9/6/2005</p>

    <p class="memofmt1-2">          ANTI-AIDS AGENTS 65: INVESTIGATION OF THE IN VITRO OXIDATIVE METABOLISM OF 3&#39;,4&#39;-DI-O-(-)-CAMPHANOYL-(+)-CIS-KHELLACTONE (DCK) DERIVATIVES AS POTENT ANTI-HIV AGENTS</p>

    <p>          Suzuki, M, Li, Y, Smith, PC, Swenberg, JA, Martin, DE, Morris-Natschke, SL, and Lee, KH</p>

    <p>          Drug Metab Dispos <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087699&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087699&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   47830   HIV-LS-330; EMBASE-HIV-9/6/2005</p>

    <p class="memofmt1-2">          Polybiguanides, particularly polyethylene hexamethylene biguanide, have activity against human immunodeficiency virus type 1</p>

    <p>          Krebs, Fred C, Miller, Shendra R, Ferguson, Mary Lee, Labib, Mohamed, Rando, Robert F, and Wigdahl, Brian</p>

    <p>          Biomedecine &amp; Pharmacotherapy <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKN-4GX1P40-1/2/4dc11309bda3f5ec82595725968edb5a">http://www.sciencedirect.com/science/article/B6VKN-4GX1P40-1/2/4dc11309bda3f5ec82595725968edb5a</a> </p><br />

    <p>12.   47831   HIV-LS-330; EMBASE-HIV-9/6/2005</p>

    <p class="memofmt1-2">          Polysulfated sialic acid derivatives as anti-human immunodeficiency virus</p>

    <p>          Terada, Masaki, Fujita, Shuji, Suda, Isao, and Mastico, Robert</p>

    <p>          Biomedecine &amp; Pharmacotherapy <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKN-4GVH2GS-1/2/cfd9e43117ab7c4aef5602bc56d07269">http://www.sciencedirect.com/science/article/B6VKN-4GVH2GS-1/2/cfd9e43117ab7c4aef5602bc56d07269</a> </p><br />

    <p>13.   47832   HIV-LS-330; WOS-HIV-8/28/2005</p>

    <p class="memofmt1-2">          Interferon-induced exonuclease ISG20 exhibits an antiviral activity against human immunodeficiency virus type 1</p>

    <p>          Espert, L, Degols, G, Lin, YL, Vincent, T, Benkirane, M, and Mechti, N</p>

    <p>          JOURNAL OF GENERAL VIROLOGY <b>2005</b>.  86: 2221-2229, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300011</a> </p><br />

    <p>14.   47833   HIV-LS-330; WOS-HIV-8/28/2005</p>

    <p class="memofmt1-2">          Antigenic properties of peptide mimotopes of HIV-1-associated carbohydrate antigens</p>

    <p>          Pashov, A, Canziani, G, Monzavi-Karbassi, B, Kaveri, SV, MacLeod, S, Saha, R, Perry, M, VanCott, TC, and Kieber-Emmons, T</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(32): 28959-28965, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231021300017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231021300017</a> </p><br />

    <p>15.   47834   HIV-LS-330; WOS-HIV-8/28/2005</p>

    <p class="memofmt1-2">          The 3 &#39;-azido group is not the primary determinant of 3 &#39;-azido-3 &#39;-deoxythymidine (AZT) responsible for the excision phenotype of AZT-resistant HIV-1</p>

    <p>          Sluis-Cremer, N, Arion, D, Parikh, U, Koontz, D, Schinazi, RF, Mellors, JW, and Parniak, MA</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(32): 29047-29052, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231021300029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231021300029</a> </p><br />

    <p>16.   47835   HIV-LS-330; WOS-HIV-8/28/2005</p>

    <p class="memofmt1-2">          Anti-HIV natural products</p>

    <p>          Singh, IP, Bharate, SB, and Bhutani, KK</p>

    <p>          CURRENT SCIENCE <b>2005</b>.  89(2): 269-290, 22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230981300022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230981300022</a> </p><br />
    <br clear="all">

    <p>17.   47836   HIV-LS-330; WOS-HIV-8/28/2005</p>

    <p class="memofmt1-2">          Highly potent inhibition of human immunodeficiency virus type 1 replication by TAK-220, an orally bioavailable small-molecule CCR5 antagonist</p>

    <p>          Takashima, K, Miyake, H, Kanzaki, N, Tagawa, Y, Wang, X, Sugihara, Y, Iizawa, Y, and Baba, M</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(8): 3474-3482, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230950700054">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230950700054</a> </p><br />

    <p>18.   47837   HIV-LS-330; WOS-HIV-9/4/2005</p>

    <p class="memofmt1-2">          Effects of CAPE-like compounds on HIV replication in vitro and modulation of cytokines in vivo</p>

    <p>          Ho, CC, Lin, SS, Chou, MY, Chen, FL, Hu, CC, Chen, CS, Lu, GY, and Yang, CC</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2005</b>.  56(2): 372-379, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231094000022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231094000022</a> </p><br />

    <p>19.   47838   HIV-LS-330; WOS-HIV-9/4/2005</p>

    <p class="memofmt1-2">          Prospects for the resistance to HIV protease inhibitors: Current drug design approaches and perspectives</p>

    <p>          Burlet, S, Pietrancosta, N, Laras, Y, Garino, C, Quelever, G, and Kraus, JL</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2005</b>.  11(24): 3077-3090, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231229700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231229700002</a> </p><br />

    <p>20.   47839   HIV-LS-330; WOS-HIV-9/4/2005</p>

    <p class="memofmt1-2">          A once-daily HAART regimen containing indinavir plus ritonavir plus one or two nucleoside reverse transcriptase inhibitors (PIPO study)</p>

    <p>          Burger, DM, Aarnoutse, RE, Dieleman, JP, Gyssens, IC, Nouwen, J, de, Marie S, Koopmans, PP, Stek, M, and van, der Ende ME</p>

    <p>          ANTIVIRAL THERAPY <b>2003</b>.  8(5): 455-461, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231266700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231266700012</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
