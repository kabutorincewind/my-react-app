

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-94.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="w1ZnSklc4DGm0/2kNHNIEXevzMbv7xRFd6RX6jxZKOqNkVrMYJrHwQ0jeKpcLh09dWKEQ+RvdADPQBcwKX7BRnEbQBCJI7A02MyDXtjrJk7RDA9BAlDqR8CvpE4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4D7BE061" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-94-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56474   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of SARS-CoV 3C-like Protease Activity by Theaflavin-3,3&#39;-digallate (TF3)</p>

    <p>          Chen, CN, Lin, CP, Huang, KK, Chen, WC, Hsieh, HP, Liang, PH, and Hsu, JT</p>

    <p>          Evid Based Complement Alternat Med <b>2005</b>.  2(2): 209-215</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15937562&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15937562&amp;dopt=abstract</a> </p><br />

    <p>2.     56475   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Application of Phosphoramidate Pronucleotide Technology to Abacavir Leads to a Significant Enhancement of Antiviral Potency</p>

    <p>          Mcguigan, C, Harris, SA, Daluge, SM, Gudmundsson, KS, Mclean, EW, Burnette, TC, Marr, H, Hazen, R, Condreay, LD, Johnson, L, De Clercq, E, and Balzarini, J</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(10): 3504-3515</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229129700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229129700009</a> </p><br />

    <p>3.     56476   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Analysis of Ebola virus and VLP release using an immunocapture assay</p>

    <p>          Kallstrom, George, Warfield, Kelly L, Swenson, Dana L, Mort, Shannon, Panchal, Rekha G, Ruthel, Gordon, Bavari, Sina, and Aman, MJavad</p>

    <p>          Journal of Virological Methods <b>2005</b>.  127(1): 1-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T96-4G1GF43-1/2/5b7c0a3aec2880943e66851d5ac843d6">http://www.sciencedirect.com/science/article/B6T96-4G1GF43-1/2/5b7c0a3aec2880943e66851d5ac843d6</a> </p><br />

    <p>4.     56477   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antitubercular Activity and Inhibitory Effect on Epstein-Barr Virus Activation of Sterols and Polyisoprenepolyols from an Edible Mushroom, Hypsizigus marmoreus</p>

    <p>          Akihisa, T, Franzblau, SG, Tokuda, H, Tagata, M, Ukiya, M, Matsuzawa, T, Metori, K, Kimura, Y, Suzuki, T, and Yasukawa, K</p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(6): 1117-1119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15930759&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15930759&amp;dopt=abstract</a> </p><br />

    <p>5.     56478   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Investigation into the ability of GB virus B to replicate in various immortalized cell lines</p>

    <p>          Buckwold, Victor E, Collins, Barbara, Hogan, Priscilla, Rippeon, Sherry, and Wei, Jiayi</p>

    <p>          Antiviral Research <b>2005</b>.  66(2-3): 165-168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4FNW1HG-1/2/05d3d66c654609957a125d7ad2bc9692">http://www.sciencedirect.com/science/article/B6T2H-4FNW1HG-1/2/05d3d66c654609957a125d7ad2bc9692</a> </p><br />
    <br clear="all">

    <p>6.     56479   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptide inhibitors of dengue virus and West Nile virus infectivity</p>

    <p>          Hrobowski, YM, Garry, RF, and Michael, SF</p>

    <p>          Virol J <b>2005</b>.  2(1): 49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15927084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15927084&amp;dopt=abstract</a> </p><br />

    <p>7.     56480   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The hepatitis C virus replicon system: From basic research to clinical application</p>

    <p>          Bartenschlager, Ralf</p>

    <p>          Journal of Hepatology <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4G98WPF-5/2/91334867db0cd129f8ae931c60959a7d">http://www.sciencedirect.com/science/article/B6W7C-4G98WPF-5/2/91334867db0cd129f8ae931c60959a7d</a> </p><br />

    <p>8.     56481   DMID-LS-94; WOS-DMID-6/7/2005 2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hcv Anti-Viral Agents</p>

    <p>          Griffith, RC,  Lou, L, Roberts, CD, and Schmitz, U</p>

    <p>          Annual Reports in Medicinal Chemistry(Elsevier Academic Press Inc) <b>2005</b>.  39: 223-237</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228477500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228477500018</a> </p><br />

    <p>9.     56482   DMID-LS-94; WOS-DMID-6/7/2005 2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Prodrug Strategies in the Design of Nucleoside and Nucleotide Antiviral Therapeutics</p>

    <p>          Mackman, RL and Cihlar, T</p>

    <p>          Annual Reports in Medicinal Chemistry(Elsevier Academic Press Inc) <b>2005</b>.   39: 305-321</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228477500023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228477500023</a> </p><br />

    <p>10.   56483   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human Rhinovirus 3C Protease: Generation of Pharmacophore Models for Peptidic and Nonpeptidic Inhibitors and Their Application in Virtual Screening</p>

    <p>          Steindl, T, Laggner, C, and Langer, T</p>

    <p>          J Chem Inf Model <b>2005</b>.  45(3): 716-724</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15921461&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15921461&amp;dopt=abstract</a> </p><br />

    <p>11.   56484   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inactivation of Hantaan Virus-Containing Samples for Subsequent Investigations outside Biosafety Level 3 Facilities</p>

    <p>          Kraus, AA, Priemer, C, Heider, H, Kruger, DH, and Ulrich, R</p>

    <p>          Intervirology <b>2005</b>.  48(4): 255-261</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15920350&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15920350&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   56485   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Zanamivir: an influenza virus neuraminidase inhibitor</p>

    <p>          Colman, PM</p>

    <p>          Expert Rev Anti Infect Ther <b>2005</b>.  3(2): 191-199</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15918777&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15918777&amp;dopt=abstract</a> </p><br />

    <p>13.   56486   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polycation-Mediated Delivery of Sirnas for Prophylaxis and Treatment of Influenza Virus Infection</p>

    <p>          Thomas, M. <i>et al.</i></p>

    <p>          Expert Opinion on Biological Therapy <b>2005</b>.  5(4): 495-505</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228646200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228646200007</a> </p><br />

    <p>14.   56487   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Dynamics of Viremia in Early Hepatitis C Virus Infection</p>

    <p>          Glynn, S. <i>et al.</i></p>

    <p>          Transfusion <b>2005</b>.  45(6): 994-1002</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229224400023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229224400023</a> </p><br />

    <p>15.   56488   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthetic peptides outside the spike protein heptad repeat regions as potent inhibitors of SARS-associated coronavirus</p>

    <p>          Zheng, BJ, Guan, Y, Hez, ML, Sun, H, Du, L, Zheng, Y, Wong, KL, Chen, H, Chen, Y, Lu, L, Tanner, JA, Watt, RM, Niccolai, N, Bernini, A, Spiga, O, Woo, PC, Kung, HF, Yuen, KY, and Huang, JD</p>

    <p>          Antivir Ther <b>2005</b>.  10(3): 393-403</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15918330&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15918330&amp;dopt=abstract</a> </p><br />

    <p>16.   56489   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of influenza antivirals during 2003-2004 and monitoring of neuraminidase inhibitor resistance</p>

    <p>          Anon</p>

    <p>          Wkly Epidemiol Rec <b>2005</b>.  80(17): 156</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15918290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15918290&amp;dopt=abstract</a> </p><br />

    <p>17.   56490   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Design and Characterization of Helical Peptides That Inhibit the E6 Protein of Papillomavirus</p>

    <p>          Liu, Y. <i>et al.</i></p>

    <p>          Protein Science <b>2004</b>.  13: 96-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224796100142">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224796100142</a> </p><br />
    <br clear="all">

    <p>18.   56491   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Small molecules VP-14637 and JNJ-2408068 inhibit respiratory syncytial virus fusion by similar mechanisms</p>

    <p>          Douglas, JL, Panis, ML, Ho, E, Lin, KY, Krawczyk, SH, Grant, DM, Cai, R, Swaminathan, S, Chen, X, and Cihlar, T</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(6): 2460-2466</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917547&amp;dopt=abstract</a> </p><br />

    <p>19.   56492   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Total synthesis and bioactivity of unique flavone desmosdumotin B and its analogs</p>

    <p>          Nakagawa-Goto, Kyoko, Bastow, Kenneth F, Wu, Jiu-Hong, Tokuda, Harukuni, and Lee, Kuo-Hsiung</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(12): 3016-3019</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4G7G4G8-8/2/b343ac37a82ce6d78cc7a0aeb75d32e1">http://www.sciencedirect.com/science/article/B6TF9-4G7G4G8-8/2/b343ac37a82ce6d78cc7a0aeb75d32e1</a> </p><br />

    <p>20.   56493   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro antiviral activity and single-dose pharmacokinetics in humans of a novel, orally bioavailable inhibitor of human rhinovirus 3C protease</p>

    <p>          Patick, AK, Brothers, MA, Maldonado, F , Binford, S, Maldonado, O, Fuhrman, S, Petersen, A, Smith, GJ 3rd, Zalman, LS, Burns-Naas, LA, and Tran, JQ</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(6): 2267-2275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917520&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917520&amp;dopt=abstract</a> </p><br />

    <p>21.   56494   DMID-LS-94; PUBMED-DMID-6/7/2005; LR-13591-LWC ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Herpes Simplex Virus Thymidine Kinases by 2-Phenylamino-6-oxopurines and Related Compounds: Structure-Activity Relationships and Antiherpetic Activity in Vivo</p>

    <p>          Manikowski, A, Verri, A, Lossani, A, Gebhardt, BM, Gambino, J, Focher, F, Spadari, S, and Wright, GE</p>

    <p>          J Med Chem <b>2005</b>.  48(11): 3919-3929</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15916444&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15916444&amp;dopt=abstract</a> </p><br />

    <p>22.   56495   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of selective SRPK-1 inhibitors: Novel tricyclic quinoxaline derivatives</p>

    <p>          Szekelyhidi, Zsolt, Pato, Janos, Waczek, Frigyes, Banhegyi, Peter, Hegymegi-Barakonyi, Balint, Ero[double acute accent]s, Daniel, Meszaros, Gyorgy, Hollosy, Ferenc, Hafenbradl, Doris, and Obert, Sabine</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4G94HG5-2/2/acdc85819992fb41eda027816f658d1b">http://www.sciencedirect.com/science/article/B6TF9-4G94HG5-2/2/acdc85819992fb41eda027816f658d1b</a> </p><br />
    <br clear="all">

    <p>23.   56496   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Susceptibility to antivirals of a human HBV strain with mutations conferring resistance to both lamivudine and adefovir</p>

    <p>          Brunelle, MN,  Jacquard, AC, Pichoud, C , Durantel, D, Carrouee-Durantel, S, Villeneuve, JP, Trepo, C, and Zoulim, F</p>

    <p>          Hepatology <b>2005</b>.  41(6): 1391-1398</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15915463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15915463&amp;dopt=abstract</a> </p><br />

    <p>24.   56497   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cancer preventive agents, Part 2: Synthesis and evaluation of 2-phenyl-4-quinolone and 9-oxo-9,10-dihydroacridine derivatives as novel antitumor promoters</p>

    <p>          Nakamura, Seikou, Kozuka, Mutsuo, Bastow, Kenneth F, Tokuda, Harukuni, Nishino, Hoyoku, Suzuki, Madoka, Tatsuzaki, Jin, Morris Natschke, Susan L, Kuo, Sheng-Chu, and Lee, Kuo-Hsiung</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4G7G4FN-1/2/5e1f1f9d67eaa0dc3dd12bb5f44b77e3">http://www.sciencedirect.com/science/article/B6TF8-4G7G4FN-1/2/5e1f1f9d67eaa0dc3dd12bb5f44b77e3</a> </p><br />

    <p>25.   56498   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Targeting the Sars Coronavirus Helicase - Three Approaches to Inhibitor Development</p>

    <p>          Tanner, J. <i>et al.</i></p>

    <p>          Faseb Journal <b>2005</b>.  19(4): A278</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227610701658">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227610701658</a> </p><br />

    <p>26.   56499   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human papillomavirus, viral load and proliferation rate in recurrent respiratory papillomatosis in response to alpha interferon treatment</p>

    <p>          Szeps, M, Dahlgren, L, Aaltonen, LM, Ohd, J, Kanter-Lewenshon, L, Dahlstrand, H, Munck-Wikland, E, Grander, D, and Dalianis, T</p>

    <p>          J Gen Virol <b>2005</b>.  86(Pt 6): 1695-1702</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15914847&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15914847&amp;dopt=abstract</a> </p><br />

    <p>27.   56500   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pretreatment of Hela Cells With the Glutathione Synthesis Inhibitor, L-Buthionine-Sulfoximine (Bso), Inhibits Replication of Picornaviruses</p>

    <p>          Smith, A.</p>

    <p>          Faseb Journal <b>2005</b>.  19(4): A898</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227610706322">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227610706322</a> </p><br />
    <br clear="all">

    <p>28.   56501   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evidence of the concurrent circulation of H1N2, H1N1 and H3N2 influenza A viruses in densely populated pig areas in Spain</p>

    <p>          Maldonado, J,  Van Reeth, K, Riera, P, Sitja, M, Saubi, N, Espuna, E, and Artigas, C</p>

    <p>          Vet J <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15914047&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15914047&amp;dopt=abstract</a> </p><br />

    <p>29.   56502   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Disruption of the Interactions Between the Subunits of Herpesvirus Dna Polymerases as a Novel Antiviral Strategy</p>

    <p>          Loregian, A. and Palu, G.</p>

    <p>          Clinical Microbiology and Infection <b>2005</b>.  11(6): 437-446</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228977000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228977000003</a> </p><br />

    <p>30.   56503   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory effect of mizoribine and ribavirin on the replication of severe acute respiratory syndrome (SARS)-associated coronavirus</p>

    <p>          Saijo, M, Morikawa, S, Fukushi, S, Mizutani, T, Hasegawa, H, Nagata, N, Iwata, N, and Kurane, I</p>

    <p>          Antiviral Res <b>2005</b>.  66(2-3): 159-163</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911031&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911031&amp;dopt=abstract</a> </p><br />

    <p>31.   56504   LR-13533; DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The antiviral activity of sulfated polysaccharides against dengue virus is dependent on virus serotype and host cell</p>

    <p>          Talarico, LB,  Pujol, CA, Zibetti, RG, Faria, PC, Noseda, MD, Duarte, ME, and Damonte, EB</p>

    <p>          Antiviral Res <b>2005</b>.  66(2-3): 103-110</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911027&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911027&amp;dopt=abstract</a> </p><br />

    <p>32.   56505   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interferons alpha, beta, gamma each inhibit hepatitis C virus replication at the level of internal ribosome entry site-mediated translation</p>

    <p>          Dash, S, Prabhu, R, Hazari, S, Bastian, F, Garry, R, Zou, W, Haque, S, Joshi, V, Regenstein, FG, and Thung, SN</p>

    <p>          Liver Int <b>2005</b>.  25(3): 580-594</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15910496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15910496&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>33.   56506   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of the NTPase/helicases of hepatitis C and related Flaviviridae viruses</p>

    <p>          Bretner, M, Najda, A, Podwinska, R, Baier, A, Paruch, K, Lipniacki, A, Piasek, A, Borowski, P, and Kulikowski, T</p>

    <p>          Acta Pol Pharm <b>2004</b>.  61 Suppl: 26-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15909930&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15909930&amp;dopt=abstract</a> </p><br />

    <p>34.   56507   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Glycoprotein D Receptor-Dependent, Low-Ph-Independent Endocytic Entry of Herpes Simplex Virus Type 1</p>

    <p>          Milne, R. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(11): 6655-6663</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400008</a> </p><br />

    <p>35.   56508   DMID-LS-94; WOS-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of Protein Superfamily-Targeted Chemical Libraries for Lead Identification and Optimization</p>

    <p>          Shuttleworth, S. <i>et al.</i></p>

    <p>          Current Medicinal Chemistry <b>2005</b>.  12(11): 1239-1281</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229012900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229012900001</a> </p><br />

    <p>36.   56509   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ebola virus: The role of macrophages and dendritic cells in the pathogenesis of Ebola hemorrhagic fever</p>

    <p>          Bray, M and Geisbert, TW</p>

    <p>          Int J Biochem Cell Biol <b>2005</b>.  37(8): 1560-1566</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15896665&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15896665&amp;dopt=abstract</a> </p><br />

    <p>37.   56510   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Resistance to influenza A virus infection by antigen-conjugated CpG oligonucleotides, a novel antigen-specific immunomodulator</p>

    <p>          Hayashi, Mieko, Satou, Emi, Ueki, Ryouji, Yano, Mayuka, Miyano-Kurosaki, Naoko, Fujii, Masayuki, and Takaku, Hiroshi</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  329(1): 230-236</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4FDJ49M-1/2/b8b880d9b71a23577ef84e363f1f0302">http://www.sciencedirect.com/science/article/B6WBK-4FDJ49M-1/2/b8b880d9b71a23577ef84e363f1f0302</a> </p><br />
    <br clear="all">

    <p>38.   56511   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Expanding the frontiers of existing antiviral drugs: Possible effects of HIV-1 protease inhibitors against SARS and avian influenza</p>

    <p>          Savarino, A</p>

    <p>          J Clin Virol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893956&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893956&amp;dopt=abstract</a> </p><br />

    <p>39.   56512   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structural studies of the parainfluenza virus 5 hemagglutinin-neuraminidase tetramer in complex with its receptor, sialyllactose</p>

    <p>          Yuan, P, Thompson, TB, Wurzburg, BA, Paterson, RG, Lamb, RA, and Jardetzky, TS</p>

    <p>          Structure (Camb) <b>2005</b>.  13(5): 803-815</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893670&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893670&amp;dopt=abstract</a> </p><br />

    <p>40.   56513   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An inhibition enzyme-linked immunosorbent assay for the detection of antibody to Rift Valley fever virus in humans, domestic and wild ruminants</p>

    <p>          Paweska, JT, Mortimer, E, Leman, PA, and Swanepoel, R</p>

    <p>          J Virol Methods <b>2005</b>.  127(1): 10-18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893560&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893560&amp;dopt=abstract</a> </p><br />

    <p>41.   56514   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An animal model of SARS produced by infection of Macaca mulatta with SARS coronavirus</p>

    <p>          Qin, C, Wang, J, Wei, Q, She, M, Marasco, WA, Jiang, H, Tu, X, Zhu, H, Ren, L, Gao, H, Guo, L, Huang, L, Yang, R, Cong, Z, Guo, L, Wang, Y, Liu, Y, Sun, Y, Duan, S, Qu, J, Chen, L, Tong, W, Ruan, L, Liu, P, Zhang, H, Zhang, J, Zhang, H, Liu, D, Liu, Q, Hong, T, and He, W</p>

    <p>          J Pathol <b>2005</b>.  206(3): 251-259</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892035&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892035&amp;dopt=abstract</a> </p><br />

    <p>42.   56515   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mutational and inhibitive analysis of SARS coronavirus 3C-like protease by fluorescence resonance energy transfer-based assays</p>

    <p>          Kuang, Wan-Fen, Chow, Lu-Ping, Wu, Mei-Hua, and Hwang, Lih-Hwa</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  331(4): 1554-1559</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4G27SMH-4/2/226aef6b5c97ac140c29815d7aabb115">http://www.sciencedirect.com/science/article/B6WBK-4G27SMH-4/2/226aef6b5c97ac140c29815d7aabb115</a> </p><br />
    <br clear="all">

    <p>43.   56516   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of novel 4-hydro-quinoline-3-carboxamide derivatives as an immunomodulator</p>

    <p>          He, Jun-Feng, Yun, Liu-Hong, Yang, Ri-Fang, Xiao, Zhi-Yong, Cheng, Jun-Ping, Zhou, Wen-Xia, and Zhang, Yong-Xiang</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(12): 2980-2985</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4G7DY1M-8/2/62474ed5572e9d0d3a97b93cba585075">http://www.sciencedirect.com/science/article/B6TF9-4G7DY1M-8/2/62474ed5572e9d0d3a97b93cba585075</a> </p><br />

    <p>44.   56517   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and evaluation of isatin derivatives as effective SARS coronavirus 3CL protease inhibitors</p>

    <p>          Chen, Li-Rung, Wang, Yu-Chin, Lin, Yi Wen, Chou, Shan-Yen, Chen, Shyh-Fong, Liu, Lee Tai, Wu, Ying-Ta, Kuo, Chih-Jung, Chen, Tom Shieh-Shung, and Juang, Shin-Hun</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(12): 3058-3062</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4G65C3Y-2/2/afaf5dbbfdb56be54e4253dacdd21c14">http://www.sciencedirect.com/science/article/B6TF9-4G65C3Y-2/2/afaf5dbbfdb56be54e4253dacdd21c14</a> </p><br />

    <p>45.   56518   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Role of the E1--E4 protein in the differentiation-dependent life cycle of human papillomavirus type 31</p>

    <p>          Wilson, R, Fehrmann, F, and Laimins, LA</p>

    <p>          J Virol <b>2005</b> .  79(11): 6732-6740</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15890911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15890911&amp;dopt=abstract</a> </p><br />

    <p>46.   56519   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rhinoviruses infect human epithelial cells via ceramide-enriched membrane platforms</p>

    <p>          Grassme, H, Riehle, A, Wilker, B, and Gulbins, E</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888438&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888438&amp;dopt=abstract</a> </p><br />

    <p>47.   56520   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Telbivudine: a new nucleoside analogue for the treatment of chronic hepatitis B</p>

    <p>          Han, SH</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(4): 511-519</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15882124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15882124&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>48.   56521   DMID-LS-94; PUBMED-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of antiviral therapy for severe acute respiratory syndrome</p>

    <p>          Cinatl, J, Michaelis, M, Hoever, G, Preiser, W, and Doerr, HW</p>

    <p>          Antiviral Res <b>2005</b>.  66(2-3): 81-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15878786&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15878786&amp;dopt=abstract</a> </p><br />

    <p>49.   56522   DMID-LS-94; EMBASE-DMID-6/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of small molecule inhibitors of the hepatitis C virus RNA-dependent RNA polymerase from a pyrrolidine combinatorial mixture</p>

    <p>          Burton, George, Ku, Thomas W, Carr, Thomas J, Kiesow, Terry, Sarisky, Robert T, Lin-Goerke, Juili, Baker, Audrey, Earnshaw, David L, Hofmann, Glenn A, Keenan, Richard M, and Dhanak, Dashyant</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(6): 1553-1556</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FJKWPF-3/2/e25ee5ac33b9370f2c69c19f8ae7ddf5">http://www.sciencedirect.com/science/article/B6TF9-4FJKWPF-3/2/e25ee5ac33b9370f2c69c19f8ae7ddf5</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
