

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-09-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Cfiqijgt7BmD3+Rgi3hgs1UH7R3vmccUh16x/XVlKYKRL8ggy8uJsTqvNMH4vtzM04k7Eg5pPEu8l1s1CD4AzhJgIVHbDrIaREa5Ze/BLgcEyE2De+raj6mXP6Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AD09A79A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">September 17 - September  30, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20015626">Coordination to gallium(III) strongly enhances the potency of 2-pyridineformamide thiosemicarbazones against Cryptococcus opportunistic fungi.</a> Bastos Tde, O., B.M. Soares, P.S. Cisalpino, I.C. Mendes, R.G. dos Santos, and H. Beraldo. Microbiological Research. 165(7): p. 573-577; PMID[20015626].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20680940">Curvularides A-E: antifungal hybrid peptide-polyketides from the endophytic fungus Curvularia geniculata.</a> Chomcheon, P., S. Wiyakrutta, T. Aree, N. Sriubolmas, N. Ngamrojanavanich, C. Mahidol, S. Ruchirawat, and P. Kittakoop. Chemistry. 16(36): p. 11178-11185; PMID[20680940].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20873734">Light Insensitive Silver(I) Cyanoximates As Antimicrobial Agents for Indwelling Medical Devices.</a> Gerasimchuk, N., A. Gamian, G. Glover, and B. Szponar. Inorganic Chemistry. <b>[Epub ahead of print]</b>; PMID[20873734].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20875694">Efficient synthesis of novel 1,2,4-triazole fused acyclic and 21-28 membered macrocyclic and/or lariat macrocyclic oxaazathia crown compounds with potential antimicrobial activity.</a> Khalil, N.S. European Journal of Medicinal Chemistry. <b>[Epub ahead of print]</b>; PMID[20875694].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20868749">Ocimum sanctum essential oil and its active principles exert their antifungal activity by disrupting ergosterol biosynthesis and membrane integrity.</a> Khan, A., A. Ahmad, F. Akhtar, S. Yousuf, I. Xess, L.A. Khan, and N. Manzoor. Research in Microbiology. <b>[Epub ahead of print]</b>; PMID[20868749].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20875745">1,3-Benzoxazole-4-carbonitrile as a novel antifungal scaffold of beta-1,6-glucan synthesis inhibitors.</a> Kuroyanagi, J.I., K. Kanai, Y. Sugimoto, T. Horiuchi, I. Achiwa, H. Takeshita, and K. Kawakami. Bioorganic &amp; Medicinal Chemistry. <b>[Epub ahead of print]</b>; PMID[20875745].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20718450">Steroidal saponins from fresh stems of Dracaena angustifolia.</a>  Xu, M., Y.J. Zhang, X.C. Li, M.R. Jacob, and C.R. Yang. Journal of Natural  Products. 73(9): p. 1524-1528; PMID[20718450].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20855731">Strong In Vitro Activity of Two New Rifabutin Analogs against Multidrug-resistant Mycobacterium tuberculosis.</a> Garcia, A.B., J.J. Palacios, M.J. Ruiz, J. Barluenga, F. Aznar, M.P. Cabal, J.M. Garcia, and N. Diaz. Antimicrobial Agents and Chemotherapy. <b>[Epub ahead of print]</b>; PMID[20855731].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20858205">Anti-tuberculosis Activity of a-Helical Antimicrobial Peptides: De novo Designed L- and D-Enantiomers Versus L- and D-LL-37.</a> Jiang, Z., M.P. Higgins, J. Whitehurst, K.O. Kisich, M.I. Voskuil, and R.S. Hodges. Protein &amp; Peptide Letters. <b>[Epub ahead of print]</b>; PMID[20858205].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0917-093010.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281428800002">Carbocyclic 6-benzylthioinosine analogues as subversive substrates of Toxoplasma gondii adenosine kinase: Biological activities and selective toxicities.</a> Al Safarjalani, O.N., R.H. Rais, Y.A. Kim, C.K. Chu, F.N.M. Naguib, and M.H. el Kouni. Biochemical Pharmacology. 80(7): p. 955-963; ISI[000281428800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281493800013">Chemical Composition and Antimicrobial Activity of the Essential Oil from Rollinia sericea (REFr.) REFr. (Annonaceae) Leaves.</a> Ito, R.K., I. Cordeiro, M.E.L. Lima, and P.R.H. Moreno. Journal of Essential Oil Research. 22(5): p. 419-421; ISI[000281493800013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281405200005">Influence of the Hydrophobic Amino Acids in the N- and C-Terminal Regions of Pleurocidin on Antifungal Activity.</a> Lee, J. and D.G. Lee. Journal of Microbiology and Biotechnology. 20(8): p. 1192-1195; ISI[000281405200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281482000016">Antimicrobial Activity of Some Iranian Medicinal Plants.</a> Pirbalouti, A.G., P. Jahanbazi, S. Enteshari, F. Malekpoor, and B. Hamedi. Archives of Biological Sciences. 62(3): p. 633-641; ISI[000281482000016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281489400016">Antituberculosis Agents and an Inhibitor of the para-Aminobenzoic Acid Biosynthetic Pathway from Hydnocarpus anthelminthica Seeds.</a> Wang, J.F., H.Q. Dai, Y.L. Wei, H.J. Zhu, Y.M. Yan, Y.H. Wang, C.L. Long, H.M. Zhong, L.X. Zhang, and Y.X. Cheng. Chemistry &amp; Biodiversity. 7(8): p. 2046-2053; ISI[000281489400016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281574900023">Naturally Occurring Antifungal Aromatic Esters and Amides.</a> Ali, M.S., Shahnaz, S. Tabassum, I.A. Ogunwande, M.K. Pervez, and O.A. Ibrahim. Journal of the Chemical Society of Pakistan. 32(4): p. 565-570; ISI[000281574900023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281528900013">Discovery of anti-TB agents that target the cell-division protein FtsZ.</a> Kumar, K., D. Awasthi, W.T. Berger, P.J. Tonge, R.A. Slayden, and I. Ojima. Future Medicinal Chemistry. 2(8): p. 1305-1323; ISI[000281528900013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281616500007">Fellutamide B is a potent inhibitor of the Mycobacterium tuberculosis proteasome.</a> Lin, G., D.Y. Li, T. Chidawanyika, C. Nathan, and H.L. Li. Archives of Biochemistry and Biophysics. 501(2): p. 214-220; ISI[000281616500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281568600002">Design, synthesis and antifungal activity of isosteric analogues of benzoheterocyclic N-myristoyltransferase inhibitors.</a> Sheng, C.Q., H. Xu, W.Y. Wang, Y.B. Cao, G.Q. Dong, S.Z. Wang, X.Y. Che, H.T. Ji, Z.Y. Miao, J.Z. Yao, and W.N. Zhang. European Journal of Medicinal Chemistry. 45(9): p. 3531-3540; ISI[000281568600002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0917-093010.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
