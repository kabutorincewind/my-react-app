

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-371.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nYnO7NX9BqN0Q54d97VLIdzuKxKcDx7hYqBPG8+FaiMeik1dqX6HHiVeVYU//gDt3zUKOiCmbTD/oTjVp3fO0bkd4B3gnUndqdnNlw6CPuWAUL/mzQinn595wig=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A6E9016F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-371-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60537   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of heterocyclic phenoxy compounds as HIV reverse transcriptase inhibitors</p>

    <p>          Saggar, Sandeep A, Sisko, John T, Tucker, Thomas J, Tynebor, Robert M, Su, Dai-Shi, and Anthony, Neville J</p>

    <p>          PATENT:  US <b>2007021442</b>  ISSUE DATE:  20070125</p>

    <p>          APPLICATION: 2006-29133  PP: 72pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60538   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          Drug interactions of tipranavir, a new HIV protease inhibitor</p>

    <p>          Morello, Judit, Rodriguez-Novoa, Sonia, Jimenez-Nacher, Inmaculada, and Soriano, Vincent</p>

    <p>          Drug Metabolism Letters <b>2007</b>.  1(1): 81-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60539   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          A new era in HIV treatment: the entry inhibitors</p>

    <p>          Berger, DS</p>

    <p>          Posit Aware <b>2005</b>.  16(1): 51-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17390505&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17390505&amp;dopt=abstract</a> </p><br />

    <p>4.     60540   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          Design, synthesis, and antiviral evaluation of some 3&#39;-carboxymethyl-3&#39;-deoxyadenosine derivatives</p>

    <p>          Shi, Houguang, Ke, Pucheng, and Peterson, Matt A</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: ORGN-389</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     60541   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          3&#39;-Fluoro-2&#39;,3&#39;-unsaturated carbocyclic nucleosides: Synthesis, anti-HIV activity and molecular modeling studies</p>

    <p>          Wang, Jianing, Jin, Yunho, Rapp, Kimberly L, Schinazi, Raymond F, and Chu, CK</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-073</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     60542   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          Synthesis and Anti-HIV-1 Activity Evaluation of 5-Alkyl-2-alkylthio-6-(arylcarbonyl or alpha-cyanoarylmethyl)-3,4-dihydropyrimidin-4(3H)-ones as Novel Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors</p>

    <p>          Ji, L, Chen, FE, DeClercq, E, Balzarini, J, and Pannecouque, C</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17381078&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17381078&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     60543   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          Newer tetracycline derivatives: Synthesis, anti-HIV, antimycobacterial activities and inhibition of HIV-1 integrase</p>

    <p>          Sriram, D, Yogeeswari, P, Senchani, G, and Banerjee, D</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.  17(8): 2372-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17376679&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17376679&amp;dopt=abstract</a> </p><br />

    <p>8.     60544   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          Novel 2-chloro-8-arylthiomethyldipyridodiazepinone derivatives with activity against HIV-1 reverse transcriptase</p>

    <p>          Khunnawutmanotham, Nisachon, Chimnoi, Nitirat, Saparpakorn, Patchareenart, Pungpo, Pornpan, Louisirirotchanakul, Suda, Hannongbua, Supa, and Techasakul, Supanna</p>

    <p>          Molecules <b>2007</b>.  12(2): 218-230</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     60545   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          d- and l-2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-dideoxy-3&#39;-fluoro-carbocyclic Nucleosides: Synthesis, Anti-HIV Activity and Mechanism of Resistance</p>

    <p>          Wang, J, Jin, Y, Rapp, KL, Schinazi, RF, and Chu, CK</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17373782&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17373782&amp;dopt=abstract</a> </p><br />

    <p>10.   60546   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          A NOVEL bis-TETRAHYDROFURANYLURETHANE-CONTAINING NONPEPTIDIC PROTEASE INHIBITOR (PI) GRL-98065 POTENT AGAINST MULTI-PI-RESISTANT HIV IN VITRO</p>

    <p>          Amano, M, Koh, Y, Das, D, Li, J, Leschenko, S, Wang, YF, Boross, PI, Weber, IT, Ghosh, AK, and Mitsuya, H</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371811&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371811&amp;dopt=abstract</a> </p><br />

    <p>11.   60547   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          Protein Design of a Bacterially Expressed HIV-1 gp41 Fusion Inhibitor</p>

    <p>          Deng, Y, Zheng, Q, Ketas, TJ, Moore, JP, and Lu, M</p>

    <p>          Biochemistry <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371053&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   60548   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          HIV-1 reverse transcriptase inhibitors</p>

    <p>          El Safadi, Y,  Vivet-Boudou, V, and Marquet, R</p>

    <p>          Appl Microbiol Biotechnol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17370068&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17370068&amp;dopt=abstract</a> </p><br />

    <p>13.   60549   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          Development of integrase inhibitors for treatment of AIDS: An overview</p>

    <p>          Dubey, S, Satyanarayana, YD, and Lavania, H</p>

    <p>          Eur J Med Chem  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17367896&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17367896&amp;dopt=abstract</a> </p><br />

    <p>14.   60550   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          The Vif accessory protein alters the cell cycle of human immunodeficiency virus type 1 infected cells</p>

    <p>          Wang, Jiangfang, Shackelford, Jason M, Casella, Carolyn R, Shivers, Debra K, Rapaport, Eric L, Liu, Bindong, Yu, Xiao-Fang, and Finkel, Terri H</p>

    <p>          Virology <b>2007</b>.  359(2): 243-252</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60551   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          1,5-Naphthyridin-2(1H)-one derivative HIV integrase inhibitors, and use with other agents</p>

    <p>          Johns, Brian A and Spaltenstein, Andrew</p>

    <p>          PATENT:  WO <b>2007019130</b>  ISSUE DATE:  20070215</p>

    <p>          APPLICATION: 2006  PP: 47pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   60552   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          2-oxonaphthyridine-3-carboxamides as HIV integrase inhibitors and their preparation and use in the prevention and treatment of HIV infection, AIDS and ARC</p>

    <p>          Johns, Brian A and Spaltenstein, Andrew</p>

    <p>          PATENT:  WO <b>2007019101</b>  ISSUE DATE:  20070215</p>

    <p>          APPLICATION: 2006  PP: 78pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60553   HIV-LS-371; PUBMED-HIV-4/3/2007</p>

    <p class="memofmt1-2">          Enzymatically Activated cycloSal-d4T-monophosphates: The Third Generation of cycloSal-Pronucleotides</p>

    <p>          Gisch, N, Balzarini, J, and Meier, C</p>

    <p>          J Med Chem <b>2007</b>.  50(7): 1658-1667</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335187&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335187&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   60554   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          Noninfectious papilloma virus-like particles inhibit HIV-1 replication: implications for immune control of HIV-1 infection by IL-27</p>

    <p>          Mohamad Fakruddin, J, Lempicki, Richard A, Gorelick, Robert J, Yang, Jun, Adelsberger, Joseph W, Garcia-Pineres, Alfonso J, Pinto, Ligia A, Lane, HClifford, and Imamichi, Tomozumi</p>

    <p>          Blood <b>2007</b>.  109(5): 1841-1849</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   60555   HIV-LS-371; WOS-HIV-3/25/2007</p>

    <p class="memofmt1-2">          1,2,3-Triazoles as peptide bond isosteres: synthesis and biological evaluation of cyclotetrapeptide mimics</p>

    <p>          Bock, VD, Speijer, D, Hiemstra, H, and van Maarseveen, JH</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2007</b>.  5(6): 971-975, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244703800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244703800014</a> </p><br />

    <p>20.   60556   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          naphthyridinecarboxamides as HIV integrase inhibitors and their preparation, pharmaceutical compositions and use in the treatment of HIV infections, AIDS and ARC</p>

    <p>          Johns, Brian A </p>

    <p>          PATENT:  WO <b>2007019098</b>  ISSUE DATE:  20070215</p>

    <p>          APPLICATION: 2006  PP: 132pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA, Shionogi &amp; Co. Ltd, and Kawasuji, Takashi</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60557   HIV-LS-371; SCIFINDER-HIV-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of alanine phosphonate derivatives as antiviral agents</p>

    <p>          Cai, Zhenhong R, Jabri, Salman Y, Jin, Haolun, Kim, Choung U, Metobo, Samuel E, Mish, Michael R, and Pastor, Richard M</p>

    <p>          PATENT:  WO <b>2007014352</b>  ISSUE DATE:  20070201</p>

    <p>          APPLICATION: 2006  PP: 195pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   60558   HIV-LS-371; WOS-HIV-3/25/2007</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type-1 reverse transcriptase by a novel peptide derived from the viral integrase</p>

    <p>          Gleenberg, IO, Herschhorn, A, Goldgur, Y, and Hizi, A</p>

    <p>          ARCHIVES OF BIOCHEMISTRY AND BIOPHYSICS <b>2007</b>.  458(2): 202-212, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244385000014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244385000014</a> </p><br />

    <p>23.   60559   HIV-LS-371; WOS-HIV-3/25/2007</p>

    <p class="memofmt1-2">          Chemokine antagonists as therapeutics: Focus on HIV-1</p>

    <p>          Tsibris, AMN and Kuritzkes, DR</p>

    <p>          ANNUAL REVIEW OF MEDICINE <b>2007</b>.  58: 445-459, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244461500030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244461500030</a> </p><br />

    <p>24.   60560   HIV-LS-371; WOS-HIV-3/25/2007</p>

    <p class="memofmt1-2">          Elimination of HIV-1 infection by treatment with a doxorubicin-conjugated anti-envelope antibody</p>

    <p>          Johansson, S, Goldenberg, DM, Griffiths, GL, Wahren, B, and Hinkula, J</p>

    <p>          AIDS <b>2006</b>.  20 (15): 1911-1915, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244509800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244509800002</a> </p><br />

    <p>25.   60561   HIV-LS-371; WOS-HIV-3/25/2007</p>

    <p class="memofmt1-2">          Synthesis of guanidinoxylopyranosides and their anti-HIV PR activity</p>

    <p>          Cao, LH and Lian, ZB</p>

    <p>          ACTA CHIMICA SINICA <b>2007</b>.  65(4): 349-354, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244500200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244500200013</a> </p><br />

    <p>26.   60562   HIV-LS-371; WOS-HIV-4/2/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 integrase by modified oligonucleotides: Optimization of the inhibitor structure</p>

    <p>          Prikazchikova, TA, Volkov, EM, Zubin, EM, Romanova, EA, and Gottikh, MB</p>

    <p>          MOLECULAR BIOLOGY <b>2007</b>.  41(1): 118-125, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244508100016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244508100016</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
