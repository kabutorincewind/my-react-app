

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-04-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Fjo7bHzIE6Hg/C26JqHzSd9FACDzXdJLTkJLOAZhCkn5H4QOk5kGSqLz5MCUFp0Nmu2YqOVQJEcM/ovUHuFZrihtsaVcgpOqnyNgvoGq00ldNZ6BnAhevBNqx80=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E5F1E076" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 28 - April 10, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24618511">A New Potential Approach to Block HIV-1 Replication via Protein-Protein Interaction and Strand-transfer Inhibition.</a> Ferro, S., L. De Luca, G. Lo Surdo, F. Morreale, F. Christ, Z. Debyser, R. Gitto, and A. Chimirri. Bioorganic &amp; Medicinal Chemistry, 2014. 22(7): p. 2269-2279. PMID[24618511].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23859152">Synthesis and Biological Evaluation of Novel Antiviral Agents as Protein-Protein Interaction Inhibitors.</a> Ferro, S., L. De Luca, F. Morreale, F. Christ, Z. Debyser, R. Gitto, and A. Chimirri. Journal of  Enzyme Inhibition and Medicinal Chemistry, 2014. 29(2): p. 237-242. PMID[23859152].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24419350">Ruxolitinib and Tofacitinib are Potent and Selective Inhibitors of HIV-1 Replication and Virus Reactivation in Vitro.</a> Gavegnano, C., M. Detorio, C. Montero, A. Bosque, V. Planelles, and R.F. Schinazi. Antimicrobial Agents and Chemotherapy, 2014. 58(4): p. 1977-1986. PMID[24419350].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24650642">Synthesis and Structure-activity Analysis of Diphenylpyrazolodiazene Inhibitors of the HIV-1 Nef Virulence Factor.</a> Iyer, P.C., J. Zhao, L.A. Emert-Sedlak, K.K. Moore, T.E. Smithgall, and B.W. Day. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(7): p. 1702-1706. PMID[24650642].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24275119">A Novel Cell-based High-throughput Screen for Inhibitors of HIV-1 Gene Expression and Budding Identifies the Cardiac Glycosides.</a> Laird, G.M., E.E. Eisele, S.A. Rabi, D. Nikolaeva, and R.F. Siliciano. The Journal of Antimicrobial Chemotherapy, 2014. 69(4): p. 988-994. PMID[24275119].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24384694">EFdA, a Reverse Transcriptase Inhibitor, Potently Blocks HIV-1 Ex Vivo Infection of Langerhans Cells within Epithelium.</a> Matsuzawa, T., T. Kawamura, Y. Ogawa, K. Maeda, H. Nakata, K. Moriishi, Y. Koyanagi, H. Gatanaga, S. Shimada, and H. Mitsuya. The Journal of Investigative Dermatology, 2014. 134(4): p. 1158-1161. PMID[24384694].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24613163">Design of Antiviral Stapled Peptides Containing a Biphenyl Cross-linker.</a> Muppidi, A., H. Zhang, F. Curreli, N. Li, A.K. Debnath, and Q. Lin. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(7): p. 1748-1751. PMID[24613163].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24616176">The DiPPro Approach: Synthesis, Hydrolysis, and Antiviral Activity of Lipophilic d4T Diphosphate Prodrugs.</a> Schulz, T., J. Balzarini, and C. Meier. ChemMedChem, 2014. 9(4): p. 762-775. PMID[24616176].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>
 
    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24135563">Synergistic Activity Profile of Carbosilane Dendrimer G2-STE16 in Combination with Other Dendrimers and Antiretrovirals as Topical anti-HIV-1 Microbicide.</a> Sepulveda-Crespo, D., R. Lorente, M. Leal, R. Gomez, F.J. De la Mata, J.L. Jimenez, and M.A. Munoz-Fernandez. Nanomedicine, 2014. 10(3): p. 609-618. PMID[24135563].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24419343">Competitive Fitness Assays Indicate That the E138A Substitution in HIV-1 Reverse Transcriptase Decreases in Vitro Susceptibility to Emtricitabine.</a> Sluis-Cremer, N., K.D. Huber, C.J. Brumme, and P.R. Harrigan. Antimicrobial Agents and Chemotherapy, 2014. 58(4): p. 2430-2433. PMID[24419343].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24317724">Anti-HIV-1 Activity of Trim 37.</a> Tabah, A.A., K. Tardif, and L.M. Mansky. Journal of General Virology, 2014. 95(Pt 4): p. 960-967. PMID[24317724].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24256218">Characterization of VRC01, a Potent and Broadly Neutralizing anti-HIV MAb, Produced in Transiently and Stably Transformed Tobacco.</a> Teh, A.Y., D. Maresch, K. Klein, and J.K. Ma. Plant Biotechnology Journal, 2014. 12(3): p. 300-311. PMID[24256218].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24631361">Fused Heterocyclic Compounds Bearing Bridgehead Nitrogen as Potent HIV-1 NNRTIs. Part 1: Design, Synthesis and Biological Evaluation of Novel 5,7-Disubstituted pyrazolo[1,5-a]pyrimidine Derivatives.</a> Tian, Y., D. Du, D. Rai, L. Wang, H. Liu, P. Zhan, E. De Clercq, C. Pannecouque, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2014. 22(7): p. 2052-2059. PMID[24631361].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24508075">A Multivalent Inhibitor of the DC-SIGN Dependent Uptake of HIV-1 and Dengue Virus.</a> Varga, N., I. Sutkeviciute, R. Ribeiro-Viana, A. Berzi, R. Ramdasi, A. Daghetti, G. Vettoretti, A. Amara, M. Clerici, J. Rojo, F. Fieschi, and A. Bernardi. Biomaterials, 2014. 35(13): p. 4175-4184. PMID[24508075].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24602795">Discovery of Nitropyridine Derivatives as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors via a Structure-based Core Refining Approach.</a> Wang, J., P. Zhan, Z. Li, H. Liu, E. De Clercq, C. Pannecouque, and X. Liu. European Journal of Medicinal Chemistry, 2014. 76: p. 531-538. PMID[24602795].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24136666">A Novel Laccase with Inhibitory Activity Towards HIV-I Reverse Transcriptase and Antiproliferative Effects on Tumor Cells from the Fermentation Broth of Mushroom Pleurotus cornucopiae.</a> Wu, X., C. Huang, Q. Chen, H. Wang, and J. Zhang. Biomedical Chromatography, 2014. 28(4): p. 548-553. PMID[24136666].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0328-041014.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331986600032">Pungent and Bitter, Cytotoxic and Antiviral Terpenoids from Some Bryophytes and Inedible Fungi.</a> Asakawa, Y., F. Nagashima, T. Hashimoto, M. Toyota, A. Ludwiczuk, I. Komala, T. Ito, and Y. Yagi. Natural Product Communications, 2014. 9(3): p. 409-417. ISI[00331986600032].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331672400002">Antiplasmodial, HIV-1 Reverse Transcriptase Inhibitory and Cytotoxicity Properties of Centratherum punctatum Cass. and It&#39;s Fractions.</a> Chukwujekwu, J.C., A.R. Ndhlala, C.A. de Kock, P.J. Smith, and J. Van Staden. South African Journal of Botany, 2014. 90: p. 17-19. ISI[000331672400002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331496100025">Design, Synthesis, and Biological Evaluation of Novel Piperidine-4-carboxamide Derivatives as Potent CCR5 Inhibitors.</a> Hu, S.W., Q. Gu, Z.L. Wang, Z.Y. Weng, Y.R. Cai, X.W. Dong, Y.Z. Hu, T. Liu, and X. Xie. European Journal of Medicinal Chemistry, 2014. 71: p. 259-266. ISI[000331496100025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331957700003">Macromolecular Prodrugs for Controlled Delivery of Ribavirin.</a> Kryger, M.B.L., A.A.A. Smith, B.M. Wohl, and A.N. Zelikin. Macromolecular Bioscience, 2014. 14(2): p. 173-185. ISI[000331957700003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000332453600028">Stereospecific 3+2 Cycloaddition of 1,2-Cyclopropanated Sugars and Ketones Catalyzed by SnCl4: An Efficient Synthesis of Multi-substituted Perhydrofuro[2,3-b]furans and Perhydrofuro[2,3-b]pyrans.</a> Ma, X.F., J.C. Zhang, Q. Tang, J. Ke, W. Zou, and H.W. Shao. Chemical Communications, 2014. 50(26): p. 3505-3508. ISI[000332453600028].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331505900033">A Facile Synthesis of (Z)-5-(Substituted)-2-(methylthio)thiazol-4(5H)-one Using Microwave Irradiation and Conventional Method.</a> Pansare, D.N. and D.B. Shinde. Tetrahedron Letters, 2014. 55(5): p. 1107-1110. ISI[000331505900033].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000332060200001">Synthesis and Antiviral Evaluation of Novel 2&#39;,2&#39;-Difluoro 5&#39;-norcarbocyclic phosphonic acid nucleosides as Antiviral Agents.</a> Shen, G.H. and J.H. Hong. Nucleosides Nucleotides &amp; Nucleic Acids, 2014. 33(1): p. 1-17. ISI[000332060200001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000332115400009">Copper-promoted Reductive Homocoupling of Quasi-aromatic Iron(II) Clathrochelates: Boosting the Inhibitory Activity in a Transcription Assay.</a> Varzatskii, O.A., V.V. Novikov, S.V. Shulga, A.S. Belov, A.V. Vologzhanina, V.V. Negrutska, I.Y. Dubey, Y.N. Bubnov, and Y.Z. Voloshin. Chemical Communications, 2014. 50(24): p. 3166-3168. ISI[000332115400009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0328-041014.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140327&amp;CC=WO&amp;NR=2014046732A1&amp;KC=A1">Cyclotide-based CXCR4 Antagonists with anti-HIV Activity.</a> Camarero, J.A., T.L. Aboye, N. Neamati, and H. Ha. Patent. 2014. 2013-US31775 2014046732: 66pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140313&amp;CC=WO&amp;NR=2014040087A1&amp;KC=A1">A Virucidal Small Molecule Comprising PD 404,182 and Uses Thereof for Inactivating Enveloped DNA Virus Particles.</a> Chen, Z., K. Chockalingam, A.M. Chamoun-Emanuelli, R. Simeon, M. Bobardt, and P. Gallay. Patent. 2014. 2013-US59103 2014040087: 93pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140220&amp;CC=US&amp;NR=2014051731A1&amp;KC=A1">Method for Treatment of Disease Caused or Aggravated by Microorganisms or Relieving Symptoms Thereof by Administering a Barrier-forming Compn. Contg. An Antimicrobial to a Surface.</a> Ghannoum, A.M. and B.V. Sokol. Patent. 2014. 2013-14063185 20140051731: 64pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140306&amp;CC=WO&amp;NR=2014036443A2&amp;KC=A2">Heterocyclyl Carboxamides for Treating Viral Diseases.</a> Huberman, E. Patent. 2014. 2013-US57585 2014036443: 61pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140320&amp;CC=WO&amp;NR=2014043252A2&amp;KC=A2">Preparation of Therapeutic Hydroxypyridinones, Hydroxypyrimidinones and Hydroxypyridazinones as Antiviral Agents.</a> Lavoie, E.J., J.D. Bauman, A. Parhi, H.Y. Sagong, D. Patel, E. Arnold, K. Das, and S.K. Vijayan. Patent. 2014. 2013-US59287 2014043252: 142pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140320&amp;CC=WO&amp;NR=2014043019A1&amp;KC=A1">Preparation of Amino Acid Amides Containing a Morpholine Residue as HIV Protease Inhibitors.</a> McCauley, J.A., S. Crane, C. Beaulieu, D.J. Bennett, C.J. Bungard, R.K. Chang, T.J. Greshock, L. Hao, K. Holloway, J.J. Manikowski, D. McKay, C. Molinaro, O.M. Moradei, P.G. Nantermet, C. Nadeau, T. Satyanarayana, W. Shipe, S.K. Singh, V.L. Truong, S. Vijayasaradhi, P.D. Williams, and C.M. Wiscount. Patent. 2014. 2013-US58724 2014043019: 237pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140220&amp;CC=WO&amp;NR=2014028384A1&amp;KC=A1">Preparation of Alkylene-bridged (S)-2-[7-(3,4-Dihydro-2H-benzo[b][1,4]oxazin-6-yl)- or 7-(Piperidin-1-yl)-2-(biphenyl-3-yl)pyrazolo[1,5-a]pyrimidin-6-yl]-2-(tert-butoxy)acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Naidu, B.N., M. Patel, S. D&#39;Andrea, Z.B. Zheng, T.P. Connolly, D.R. Langley, K. Peese, Z. Wang, M.A. Walker, and J.F. Kadow. Patent. 2014. 2013-US54532 2014028384: 263pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140313&amp;CC=US&amp;NR=2014073631A1&amp;KC=A1">Preparation of Guanidine and Biguanidine Derivatives as Antiviral and Antimicrobial Agents.</a> Shetty, B.V. Patent. 2014. 2013-14022433 20140073631: 121pp.</p>

    <p class="plaintext">[Patent]. HIV_0328-041014.</p>

    <br />

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140220&amp;CC=WO&amp;NR=2014027112A1&amp;KC=A1">Tris(Hetero)arylpyrazoles as Antiviral Agents and Their Preparation and Use in the Treatment of Retroviral Infection.</a> Wildum, S., B. Klenke, and A. Wendt. Patent. 2014. 2013-EP67201 2014027112: 115pp. </p>
    
    <p class="plaintext">[Patent]. HIV_0328-041014.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
