

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-302.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8+9lxzHnHIZEgD3SaDXehnzbbQI8rE+l+8hlHWQ788E2iuw4v4abAOUszcDgZShn2R/sJZaBSfw1G0QmEmKg5YVNwPxAaisLIElapiiqIIg/+ctjzg49toobCX4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="34D3F0C6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-302-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43882   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Antimycobacterial arylidenecyclohexanones and related Mannich bases</p>

<p>           Dimmock, JR, Kandepu, NM, Das, U, Zello, GA, and Nienaber, KH</p>

<p>           Pharmazie 2004. 59(7): 502-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296085&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296085&amp;dopt=abstract</a> </p><br />

<p>     2.    43883   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Mycobacterium tuberculosis LprG (Rv1411c): A Novel TLR-2 Ligand That Inhibits Human Macrophage Class II MHC Antigen Processing</p>

<p>           Gehring, AJ, Dobos, KM, Belisle, JT, Harding, CV, and Boom, WH</p>

<p>           J Immunol 2004. 173(4): 2660-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15294983&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15294983&amp;dopt=abstract</a> </p><br />

<p>     3.    43884   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           In vitro selection of RNA aptamers against the HCV NS3 helicase domain</p>

<p>           Nishikawa, F, Funaji, K, Fukuda, K, and Nishikawa, S</p>

<p>           Oligonucleotides 2004. 14(2): 114-29</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15294075&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15294075&amp;dopt=abstract</a> </p><br />

<p>     4.    43885   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Requirements for nitric oxide generation from isoniazid activation in vitro and inhibition of mycobacterial respiration in vivo</p>

<p>           Timmins, GS, Master, S, Rusnak, F, and Deretic, V</p>

<p>           J Bacteriol 2004. 186(16): 5427-31</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15292144&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15292144&amp;dopt=abstract</a> </p><br />

<p>     5.    43886   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           The activity of grepafloxacin in two murine models of Mycobacterium avium infection</p>

<p>           Cynamon, MH, Sklaney, M, and Yeo, AE</p>

<p>           J Infect Chemother 2004. 10(3): 185-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15290460&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15290460&amp;dopt=abstract</a> </p><br />

<p>     6.    43887   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           The role of ribavirin in the combination therapy of hepatitis C virus infection</p>

<p>           Picardi, A, Gentilucci, UV, Zardi, EM, D&#39;Avola, D, Amoroso, A, and Afeltra, A</p>

<p>           Curr Pharm Des 2004. 10(17): 2081-92</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15279547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15279547&amp;dopt=abstract</a> </p><br />

<p>        </p>

<p>    7.     43888   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Crystal structure of the Mycobacterium tuberculosis dUTPase: insights into the catalytic mechanism</p>

<p>           Chan, S, Segelke, B, Lekin, T, Krupka, H, Cho, US, Kim, MY, So, M, Kim, CY, Naranjo, CM, Rogers, YC, Park, MS, Waldo, GS, Pashkov, I, Cascio, D, Perry, JL, and Sawaya, MR</p>

<p>           J Mol Biol 2004. 341(2): 503-17</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15276840&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15276840&amp;dopt=abstract</a> </p><br />

<p>     8.    43889   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Effect of granulocyte colony-stimulating factor combination therapy on efficacy of posaconazole (SCH56592) in an inhalation model of murine pulmonary aspergillosis</p>

<p>           Patera, AC, Menzel, F, Jackson, C, Brieland, JK, Halpern, J, Hare, R, Cacciapuoti, A, and Loebenberg, D</p>

<p>           Antimicrob Agents Chemother 2004.  48(8): 3154-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273138&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273138&amp;dopt=abstract</a> </p><br />

<p>     9.    43890   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Inhibition of hepatitis C virus replication by arsenic trioxide</p>

<p>           Hwang, DR, Tsai, YC, Lee, JC, Huang, KK, Lin, RK, Ho, CH, Chiou, JM, Lin, YT, Hsu, JT, and Yeh, CT</p>

<p>           Antimicrob Agents Chemother 2004.  48(8): 2876-82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273095&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273095&amp;dopt=abstract</a> </p><br />

<p>   10.    43891   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           FDA approves Hep C drugs</p>

<p>           Anon</p>

<p>           Aids Alert 2004. 19(6): 71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15272452&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15272452&amp;dopt=abstract</a> </p><br />

<p>   11.    43892   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Metal complexes of 1,10-phenanthroline-5,6-dione alter the susceptibility of the yeast Candida albicans to amphotericin B and miconazole</p>

<p>           Eshwika, A, Coyle, B, Devereux, M, McCann, M, and Kavanagh, K</p>

<p>           Biometals 2004. 17(4): 415-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15259362&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15259362&amp;dopt=abstract</a> </p><br />

<p>   12.    43893   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Prophylaxis against opportunistic infections in persons infected with human immunodeficiency virus</p>

<p>           Martin, JE and Besch, CL</p>

<p>           Am J Med Sci 2004. 328(1): 64-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254443&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254443&amp;dopt=abstract</a> </p><br />

<p>        </p>

<p>  13.     43894   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           Structural analysis of hepatitis C RNA genome using DNA microarrays</p>

<p>           Martell, M, Briones, C, de Vicente, A , Piron, M, Esteban, JI, Esteban, R, Guardia, J, and Gomez, J</p>

<p>           Nucleic Acids Res 2004. 32(11): e90</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247323&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247323&amp;dopt=abstract</a> </p><br />

<p>   14.    43895   OI-LS-302; PUBMED-OI-8/10/2004</p>

<p class="memofmt1-3">           In vitro activity of protegrin-1 and beta-defensin-1, alone and in combination with isoniazid, against Mycobacterium tuberculosis</p>

<p>           Fattorini, L, Gennaro, R, Zanetti, M, Tan, D, Brunori, L, Giannoni, F, Pardini, M, and Orefici, G</p>

<p>           Peptides 2004. 25(7): 1075-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245864&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245864&amp;dopt=abstract</a> </p><br />

<p>   15.    43896   OI-LS-302; WOS-OI-8/2/2004</p>

<p class="memofmt1-3">           Characterization of the hepatitis C virus RNA replication complex associated with lipid rafts</p>

<p>           Aizaki, H, Lee, KJ, Sung, VMH, Ishiko, H, and Lai, MMC</p>

<p>           VIROLOGY 2004. 324(2): 450-461</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222376800019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222376800019</a> </p><br />

<p>   16.    43897   OI-LS-302; WOS-OI-8/2/2004</p>

<p class="memofmt1-3">           Naturally occurring horizontal gene transfer and homologous recombination in Mycobacterium</p>

<p>           Krzywinska, E, Krzywinski, J, and Schorey, JS</p>

<p>           MICROBIOL-SGM  2004. 150: 1707-1712</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222437300012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222437300012</a> </p><br />

<p>   17.    43898   OI-LS-302; WOS-OI-8/2/2004</p>

<p class="memofmt1-3">           Structural variations in keto-glutamines for improved inhibition against hepatitis A virus 3C proteinase</p>

<p>           Jain, RP and Vederas, JC</p>

<p>           BIOORG MED CHEM LETT 2004. 14(14):  3655-3658</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222367300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222367300007</a> </p><br />

<p>   18.    43899   OI-LS-302; WOS-OI-8/2/2004</p>

<p class="memofmt1-3">           Efficacy of ganciclovir and cidofovir against human cytomegalovirus replication in SCID mice implanted with human retinal tissue</p>

<p>           Bidanset, DJ, Rybak, RJ, Hartline, CB, and Kern, ER</p>

<p>           ANTIVIR RES 2004. 63(1): 61-64</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222423500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222423500008</a> </p><br />

<p>   19.    43900   OI-LS-302; WOS-OI-8/2/2004</p>

<p class="memofmt1-3">           Inhibitory role of cyclosporin A and its derivatives on replication of hepatitis C virus.</p>

<p>           Shimotohno, K and Watashi, K</p>

<p>           AM J TRANSPLANT 2004. 4: 334-335</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221322500648">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221322500648</a> </p><br />

<p>   20.    43901   OI-LS-302; WOS-OI-8/8/2004</p>

<p class="memofmt1-3">           Stationary phase gene expression of Mycobacterium tuberculosis following a progressive nutrient depletion: a model for persistent organisms?</p>

<p>           Hampshire, T, Soneji, S, Bacon, J, James, BW, Hinds, J, Laing, K, Stabler, RA, Marsh, PD, and Butcher, PD</p>

<p>           TUBERCULOSIS 2004. 84(3-4): 228-238</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222557800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222557800011</a> </p><br />

<p>   21.    43902   OI-LS-302; WOS-OI-8/8/2004</p>

<p class="memofmt1-3">           Evaluation of the line probe assay (LiPA) for rapid detection of rifampicin resistance in Mycobacterium tuberculosis</p>

<p>           Jureen, P, Werngren, J, and Hoffner, SE</p>

<p>           TUBERCULOSIS 2004. 84(5): 311-316</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222557900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222557900004</a> </p><br />

<p>   22.    43903   OI-LS-302; WOS-OI-8/8/2004</p>

<p class="memofmt1-3">           Metabolism of 2-methyladenosine in Mycobacterium tuberculosis</p>

<p>           Parker, WB, Barrow, EW, Allan, PW, Shaddix, SC, Long, MC, Barrow, WW, Bansal, N, and Maddry, JA</p>

<p>           TUBERCULOSIS 2004. 84(5): 327-336</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222557900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222557900006</a> </p><br />

<p>   23.    43904   OI-LS-302; EMBASE-OI-8/10/2004</p>

<p class="memofmt1-3">           Quinones as antimycobacterial agents</p>

<p>           Tran, Thuyanh, Saheba, Ekta, Arcerio, Ariana V, Chavez, Violeta, Li, Qing-yi, Martinez, Luis E, and Primm, Todd P</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4D16S1H-5/2/356cd1f943792cc9f9e186f183436dad">http://www.sciencedirect.com/science/article/B6TF8-4D16S1H-5/2/356cd1f943792cc9f9e186f183436dad</a> </p><br />

<p>   24.    43905   OI-LS-302; EMBASE-OI-8/10/2004</p>

<p class="memofmt1-3">           Drug susceptibility testing of Mycobacterium tuberculosis with nitrate reductase assay</p>

<p>           Coban, Ahmet Yilmaz, Birinci, Asuman, Ekinci, Bora, and Durupinar, Belma</p>

<p>           International Journal of Antimicrobial Agents 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T7H-4D04VNN-6/2/bbe4c3ff458d539a91ff99451a13c690">http://www.sciencedirect.com/science/article/B6T7H-4D04VNN-6/2/bbe4c3ff458d539a91ff99451a13c690</a> </p><br />

<p>   25.    43906   OI-LS-302; EMBASE-OI-8/10/2004</p>

<p class="memofmt1-3">           Mycobacterium tuberculosis and Escherichia coli nucleoside diphosphate kinases lack multifunctional activities to process uracil containing DNA</p>

<p>           Kumar, Pradeep, Krishna, Kurthkoti, Srinivasan, Ramanujam, Ajitkumar, Parthasarathi, and Varshney, Umesh</p>

<p>           DNA Repair 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6X17-4CTSKP7-1/2/c0c525a2c50b0805008721faff53cba8">http://www.sciencedirect.com/science/article/B6X17-4CTSKP7-1/2/c0c525a2c50b0805008721faff53cba8</a> </p><br />

<p>   26.    43907   OI-LS-302; EMBASE-OI-8/10/2004</p>

<p class="memofmt1-3">           Ring-substituted imidazoles as a new class of anti-tuberculosis agents</p>

<p>           Gupta, Preeti, Hameed, Shahul, and Jain, Rahul</p>

<p>           European Journal of Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VKY-4CYNTR0-1/2/d1fee10770eef9828e73b6bc4a230b9d">http://www.sciencedirect.com/science/article/B6VKY-4CYNTR0-1/2/d1fee10770eef9828e73b6bc4a230b9d</a> </p><br />

<p>   27.    43908   OI-LS-302; WOS-OI-8/8/2004</p>

<p class="memofmt1-3">           Rational design of new antituberculosis agents: Receptor-independent four-dimensional quantitative structure-activity relationship analysis of a set of isoniazid derivatives</p>

<p>           Pasqualoto, KFM, Ferreira, EI, Santos, OA, and Hopfinger, AJ</p>

<p>           J MED CHEM 2004. 47(15): 3755-3764</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222530700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222530700006</a> </p><br />

<p>   28.    43909   OI-LS-302; WOS-OI-8/8/2004</p>

<p class="memofmt1-3">           Novel azapeptide inhibitors of hepatitis c virus serine protease</p>

<p>           Bailey, MD, Halmos, T, Goudreau, N, Lescop, E, and Llinas-Brunet, M</p>

<p>           J MED CHEM 2004. 47(15): 3788-3799</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222530700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222530700009</a> </p><br />

<p>   29.    43910   OI-LS-302; WOS-OI-8/8/2004</p>

<p class="memofmt1-3">           Rapid alternative methods for detection of rifampicin resistance in Mycobacterium tuberculosis</p>

<p>           Lemus, D, Martin, A, Montoro, E, Portaels, F, and Palomino, JC</p>

<p>           J ANTIMICROB CHEMOTH 2004. 54(1): 130-133</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222558100022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222558100022</a> </p><br />

<p>   30.    43911   OI-LS-302; EMBASE-OI-8/10/2004</p>

<p class="memofmt1-3">           Capped dipeptide phenethylamide inhibitors of the HCV NS3 protease</p>

<p>           Nizi, Emanuela, Koch, Uwe, Ontoria, Jesus M, Marchetti, Antonella, Narjes, Frank, Malancona, Savina, Matassa, Victor G, and Gardelli, Cristina</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(9): 2151-2154</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BVP39W-H/2/319d20d2d1d7da83bb0a0721446a7a02">http://www.sciencedirect.com/science/article/B6TF9-4BVP39W-H/2/319d20d2d1d7da83bb0a0721446a7a02</a> </p><br />

<p>   31.    43912   OI-LS-302; EMBASE-OI-8/10/2004</p>

<p class="memofmt1-3">           Analogues of antifungal tjipanazoles from rebeccamycin</p>

<p>           Voldoire, Aline, Moreau, Pascale, Sancelme, Martine, Matulova, Maria, Leonce, Stephane, Pierre, Alain, Hickman, John, Pfeiffer, Bruno, Renard, Pierre, and Dias, Nathalie</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(8): 1955-1962</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BV4PNF-3/2/0659ca23fab5dd7c02ef7a96fbc129b9">http://www.sciencedirect.com/science/article/B6TF8-4BV4PNF-3/2/0659ca23fab5dd7c02ef7a96fbc129b9</a> </p><br />

<p>   32.    43913   OI-LS-302; EMBASE-OI-8/10/2004</p>

<p class="memofmt1-3">           Preformed antifungal compounds in strawberry fruit and flower tissues</p>

<p>           Terry, Leon A, Joyce, Daryl C, Adikaram, Nimal KB, and Khambay, Bhupinder PS</p>

<p>           Postharvest Biology and Technology 2004. 31(2): 201-212</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TBJ-4BFXTHG-3/2/0334b66dde7b7703ce41b1f62fc13025">http://www.sciencedirect.com/science/article/B6TBJ-4BFXTHG-3/2/0334b66dde7b7703ce41b1f62fc13025</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
