

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-02-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="d0T+NtMM1jw4riX/63HshNxm+HWARa97qgEEAwOhsD3432xi+0H5/ENTaEbCB0xary5XqpYxsFy/0GjBYRa09vMFWtYeqjdV8SaPDzuBK+NlHWRXisKfwWjiyks=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="26F14CC8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">February 3- February 16, 2012</h1>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22316569">Cerium, Chitosan and Hamamelitannin as Novel Biofilm Inhibitors?</a> Cobrado, L., M.M. Azevedo, A. Silva-Dias, J.P. Ramos, C. Pina-Vaz, and A.G. Rodrigues. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22316569].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22315984">Ambroxol Influences Voriconazole Resistance of Candida parapsilosis Biofilm.</a> Giovanna, P., P. Dimitrios, D.D. Giovanni, R. Fabio, and C.M. Rosaria. FEMS Yeast Research, 2012. <b>[Epub ahead of print]</b>; PMID[22315984].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22169028">Spectral, Biological Screening of Metal Chelates of Chalcone Based Schiff Bases of N-(3-Aminopropyl) imidazole.</a>Kalanithi, M., M. Rajarajan, P. Tharmaraj, and C.D. Sheela. Spectrochimica acta. Part A, Molecular and Biomolecular Spectroscopy, 2012. 87: p. 155-162; PMID[22169028].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22264148">Syntheses, Structures, and Antimicrobial Activities of Remarkably Light-stable and Water-soluble Silver Complexes with Amino Acid Derivatives, Silver(I) N-Acetylmethioninates.</a> Kasuga, N.C., R. Yoshikawa, Y. Sakai, and K. Nomiya. Inorganic Chemistry, 2012. 51(3): p. 1640-1647; PMID[22264148].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22324794">Inhibitory Effects of Gossypol, Gossypolone and Apogossypolone on a Collection of Economically Important Filamentous Fungi.</a> Mellon, J., C.A. Zelaya, M. Dowd, S.B. Beltz, and M.A. Klich. Journal of Agricultural and Food Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22324794].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22154966">Pharmacological, Genotoxic and Phytochemical Properties of Selected South African Medicinal Plants Used in Treating Stomach-related Ailments.</a> Okem, A., J.F. Finnie, and J. Van Staden. Journal of Ethnopharmacology, 2012. 139(3): p. 712-720; PMID[22154966].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22281187">Synthesis and Antimycobacterial Evaluation of N-Substituted 3-aminopyrazine-2,5-dicarbonitriles.</a> Zitko, J., J. Jampilek, L. Dobrovolny, M. Svobodova, J. Kunes, and M. Dolezal. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(4): p. 1598-1601; PMID[22281187].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22204607">Drug-to-genome-to-drug, Step 2: Reversing Selectivity in a Series of Antiplasmodial Compounds.</a> Beghyn, T.B., J. Charton, F. Leroux, A. Henninot, I. Reboule, P. Cos, L. Maes, and B. Deprez. Journal of Medicinal Chemistry, 2012. 55(3): p. 1274-1286; PMID[22204607].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22269277">Synthesis and Antiplasmodial Evaluation of Novel Chromeno[2,3-B]chromene Derivatives.</a> Devakaram, R., D.S. Black, V. Choomuenwai, R.A. Davis, and N. Kumar. Bioorganic &amp; Medicinal Chemistry, 2012. 20(4): p. 1527-1534; PMID[22269277].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22328964">Diversity-oriented Synthesis Yields a Novel Lead for the Treatment of Malaria.</a> Heidebrecht, R.W., Jr., C. Mulrooney, C.P. Austin, R.H. Barker, Jr., J.A. Beaudoin, K.C. Cheng, E. Comer, S. Dandapani, J. Dick, J.R. Duvall, E.H. Ekland, D.A. Fidock, M.E. Fitzgerald, M. Foley, R. Guha, P. Hinkson, M. Kramer, A.K. Lukens, D. Masi, L.A. Marcaurelle, X.Z. Su, C.J. Thomas, M. Weiwer, R.C. Wiegand, D. Wirth, M. Xia, J. Yuan, J. Zhao, M. Palmer, B. Munoz, and S. Schreiber. ACS Medicinal Chemistry Letters, 2012. 3(2): p. 112-117; PMID[22328964].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22285027">Synthesis and Antimalarial and Antituberculosis Activities of a Series of Natural and Unnatural 4-Methoxy-6-styryl-pyran-2-ones, Dihydro Analogues and Photo-Dimers.</a> McCracken, S.T., M. Kaiser, H.I. Boshoff, P.D. Boyd, and B.R. Copp. Bioorganic &amp; Medicinal Chemistry, 2012. 20(4): p. 1482-1493; PMID[22285027].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22314528">Mk-4815: A Potential New Oral Agent for the Treatment of Malaria.</a> Powles, M.A., J. Allocco, L. Yeung, B. Nare, P. Liberator, and D. Schmatz. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22314528].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22331612">Synthesis and Biological Evaluation of Acridine Derivatives as Antimalarial Agents.</a> Yu, X.M., F. Ramiandrasoa, L. Guetzoyan, B. Pradines, E. Quintino, D. Gadelle, P. Forterre, T. Cresteil, J.P. Mahy, and S. Pethe. ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22331612].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22248858">Synthesis and Antikinetoplastid Activity of a Series of N,N&#39;-Substituted diamines.</a> Caminos, A.P., E.A. Panozzo-Zenere, S.R. Wilkinson, B.L. Tekwani, and G.R. Labadie. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(4): p. 1712-1715; PMID[22248858].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22297912">Antileishmanial Activity of Cryptolepine Analogues and Apoptotic Effects of 2,7-Dibromocryptolepine against Leishmania donovani Promastigotes.</a> Hazra, S., S. Ghosh, S. Debnath, S. Seville, V.K. Prajapati, C.W. Wright, S. Sundar, and B. Hazra. Parasitology Research, 2012. <b>[Epub ahead of print]</b>; PMID[22297912].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIPROTOZOAL COMPOUNDS</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22320388">Development of Toxoplasma gondii Calcium-dependent Protein Kinase 1 (Tgcdpk1) Inhibitors with Potent Anti-toxoplasma Activity.</a> Johnson, S.M., R.C. Murphy, J.A. Geiger, A. Derocher, Z. Zhang, K. Ojo, E. Larson, B.G. Perera, E. Dale, P. He, A. Fox, N. Mueller, E.A. Merritt, E. Fan, M. Reid, M. Parsons, W.C. Van Voorhis, and D.J. Maly. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22320388].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299574700016">Antimicrobial Activity of Crude Methanolic Extract of Periploca aphylla.</a> Ahmed, M., N. Aslam, R.A. Khan, M.R. Khan, F.U. Khan, A. SaboorShah, and M.S. Shah. Journal of Medicinal Plants Research, 2011. 5(32): p. 7017-7021; ISI[000299574700016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299677800014">Phytopharmacological Assessment of Medicinal Properties of Psoralea corylifolia.</a> Anwar, M., M. Ahmad, Mehjabeen, N. Jahan, O.A. Mohiuddin, and M. Qureshi. African Journal of Pharmacy and Pharmacology, 2011. 5(23): p. 2627-2638; ISI[000299677800014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299154400003">Antimicrobial Efficacy of Achillea ligustica All. (Asteraceae) Essential Oils against Reference and Isolated Oral Microorganisms.</a> Cecchini, C., S. Silvi, A. Cresci, A. Piciotti, G. Caprioli, F. Papa, G. Sagratini, S. Vittori, and F. Maggi. Chemistry &amp; Biodiversity, 2012. 9(1): p. 12-24; ISI[000299154400003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299096600019">Evaluation of Gyrase B as a Drug Target in Mycobacterium tuberculosis.</a> Chopra, S., K. Matsuyama, T. Tran, J.P. Malerich, B.J. Wan, S.G. Franzblau, S. Lun, H.D. Guo, M.C. Maiga, W.R. Bishai, and P.B. Madrid. Journal of Antimicrobial Chemotherapy, 2012. 67(2): p. 415-421; ISI[000299096600019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299674500003">Cultural Importance and Antibacterial Activity of Ziziphus mucronata (Willd.) in the Umlazi Community in Durban.</a>Coopoosamy, R.M., K.K. Naidoo, and N.J. Ndlazi. African Journal of Pharmacy and Pharmacology, 2011. 5(17): p. 1979-1982; ISI[000299674500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299158800007">Studies on Pyrazine Derivatives LII: Antibacterial and Antifungal Activity of Nitrogen Heterocyclic Compounds Obtained by Pyrazinamidrazone Usage.</a> Foks, H., L. Balewski, K. Gobis, M. Dabrowska-Szponar, and K. Wisniewska. Heteroatom Chemistry, 2012. 23(1): p. 49-58; ISI[000299158800007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299373800006">Synthesis and Comparative Study of Anti-mycobacterium Activity of a Novel Series of Fluoronitrobenzothiazolopyrazoline Regioisomers.</a> Hazra, K., L.V.G. Nargund, P. Rashmi, J. Chandra, B. Nandha, and M.S. Harish. Archiv Der Pharmazie, 2012. 345(2): p. 137-146; ISI[000299373800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299675300006">Phytochemical, Physiochemical and Anti-fungal Activity of Eclipta alba.</a> Hussain, I., N. Khan, R. Ullah, Shanzeb, S. Ahmed, F.A. Khan, and S. Yaz. African Journal of Pharmacy and Pharmacology, 2011. 5(19): p. 2150-2155; ISI[000299675300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299541800014">Aurantoside K, a New Antifungal Tetramic Acid Glycoside from a Fijian Marine Sponge of the Genus Melophlus.</a>Kumar, R., R. Subramani, K.D. Feussner, and W. Aalbersberg. Marine Drugs, 2012. 10(1): p. 200-208; ISI[000299541800014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298921600018">Screening for Antimicrobial Activities of Marine Molluscs Thais tissoti (Petit, 1852) and Babylonia spirata (Linnaeus, 1758) against Human, Fish and Biofilm Pathogenic Microorganisms.</a>Kumaran, N.S., S. Bragadeeswaran, and S. Thangaraj. African Journal of Microbiology Research, 2011. 5(24): p. 4155-4161; ISI[000298921600018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299572400002">Antimicrobial and Anticholinesterase Activities of the Essential Oils Isolated from Salvia dicroantha Stapf., Salvia verticillata L. Subsp Amasiaca (Freyn and Bornm.) Bornm. And Salvia wiedemannii Boiss.</a> Kunduhoglu, B., M. Kurkcuoglu, M.E. Duru, and K.H.C. Baser. Journal of Medicinal Plants Research, 2011. 5(29): p. 6484-6490; ISI[000299572400002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299573100007">Anti-microbial Efficacy and Biochemical Analysis from Different Parts of Acacia nilotica L. And Ricinus communis L. Extracts.</a> Naqvi, S.H., M.U. Dahot, M. Rafiq, M.Y. Khan, I. Ibrahim, K.H. Lashari, A. Ali, and A.L. Korai. Journal of Medicinal Plants Research, 2011. 5(27): p. 6299-6308; ISI[000299573100007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299572400011">Allicin and Alliin Content and Antifungal Activity of Allium senescens L. Ssp Montanum (F. W. Schmidt) Holub Ethanol Extract.</a> Parvu, M., A.E. Parvu, L. Vlase, O. Rosca-Casian, O. Parvu, and M. Puscas. Journal of Medicinal Plants Research, 2011. 5(29): p. 6544-6549; ISI[000299572400011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298659700002">Synthesis and Antimicrobial Activities of Some New 5-((3-(Aryl)-1-phenyl-1H-pyrazol-4-yl)methylene)-3-phenylthiazolidine-2, 4-diones.</a>Prakash, O., D. Aneja, S. Arora, C. Sharma, and K. Aneja. Medicinal Chemistry Research, 2012. 21(1): p. 10-15; ISI[000298659700002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298173000018">In Vitro Inhibition Potential of Four Chenopod Halophytes against Microbial Growth.</a> Samiullah and A. Bano. Pakistan Journal of Botany, 2011. 43: p. 123-127; ISI[000298173000018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298171400091">Discovery of Novel MDR-Mycobacterium tuberculosis Inhibitor by New Frigate Computational Screen.</a> Scheich, C., Z. Szabadka, B. Vertessy, V. Putter, V. Grolmusz, and M. Schade. Plos One, 2011. 6(12); ISI[000298171400091].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298919300034">Antimicrobial Activity of Crude Venom Extracts in Honeybees (Apis cerana, Apis dorsata, Apis florea) Tested against Selected Pathogens.</a> Surendra, N.S., G.N. Jayaram, and M.S. Reddy. African Journal of Microbiology Research, 2011. 5(18): p. 2765-2772; ISI[000298919300034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299771900072">Rapid Evaluation in Whole Blood Culture of Regimens for XDR-Tb Containing Pnu-100480 (Sutezolid), TMC207, PA-824, SQ109, and Pyrazinamide.</a> Wallis, R.S., W. Jakubiec, M. Mitton-Fry, L. Ladutko, S. Campbell, D. Paige, A. Silvia, and P.F. Miller. Plos One, 2012. 7(1); ISI[000299771900072].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299335000002">Comparison of Anidulafungin&#39;s and Fluconazole&#39;s in Vivo Activity in Neutropenic and Non-Neutropenic Models of Invasive Candidiasis.</a> Wiederhold, N.P., L.K. Najvar, R. Bocanegra, W.R. Kirkpatrick, and T.F. Patterson. Clinical Microbiology and Infection, 2012. 18(2): p. E20-E23; ISI[000299335000002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0203-021612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
