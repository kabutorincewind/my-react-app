

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-08-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MrpY+0QLFiND6Z6BMmCapkEYIVmQSrC/sVu32oYt78nuNz0gLpojPvI+35ygGurrUyYT6kRiiKzr+1LN/9xAgwz5KUKOHnV91qiMmIWxN3Ogfrpm3nyc9MAXrgc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EA41C02A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 19 - August 1, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23877703">P2&#39; Benzene Carboxylic Acid Moiety Is Associated with Decrease in Cellular Uptake: Evaluation of Novel Non-peptidic HIV-1 Protease Inhibitors Containing P2 Bis-tetrahydrofuran Moiety.</a> Yedidi, R.S., K. Maeda, W.S. Fyvie, M. Steffey, D.A. Davis, I. Palmer, M. Aoki, J.D. Kaufman, S.J. Stahl, H. Garimella, D. Das, P.T. Wingfield, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23877703].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23878231">Bispecific Antibodies Directed to CD4 Domain 2 and HIV Envelope Exhibit Exceptional Breadth and Picomolar Potency against HIV-1.</a> Pace, C.S., R. Song, C. Ochsenbauer, C.D. Andrews, D. Franco, J. Yu, D.A. Oren, M.S. Seaman, and D.D. Ho. Proceedings of the National Academy of Sciences of the United States of America, 2013. <b>[Epub ahead of print]</b>. PMID[23878231].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23813855">Identification of Hck Inhibitors as Hits for the Development of Antileukemia and anti-HIV Agents.</a> Tintori, C., I. Laurenzana, F. La Rocca, F. Falchi, F. Carraro, A. Ruiz, J.A. Este, M. Kissova, E. Crespan, G. Maga, M. Biava, C. Brullo, S. Schenone, and M. Botta. ChemMedChem, 2013. 8(8): p. 1353-1360. PMID[23813855].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23777826">Molecular Docking Guided Structure Based Design of Symmetrical N,N&#39;-Disubstituted Urea/Thiourea as HIV-1 gp120-CD4 Binding Inhibitors.</a> Sivan, S.K., R. Vangala, and V. Manga. Bioorganic &amp; Medicinal Chemistry, 2013. 21(15): p. 4591-4599. PMID[23777826].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23669381">Synthesis and Antiviral Activities of Methylenecyclopropane Analogs with 6-Alkoxy and 6-Alkylthio Substitutions That Exhibit Broad-spectrum Antiviral Activity against Human Herpesviruses.</a> Prichard, M.N., J.D. Williams, G. Komazin-Meredith, A.R. Khan, N.B. Price, G.M. Jefferson, E.A. Harden, C.B. Hartline, N.P. Peet, and T.L. Bowlin. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3518-3527. PMID[23669381].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23800723">Multimerized CHR-derived Peptides as HIV-1 Fusion Inhibitors.</a> Nomura, W., C. Hashimoto, T. Suzuki, N. Ohashi, M. Fujino, T. Murakami, N. Yamamoto, and H. Tamamura. Bioorganic &amp; Medicinal Chemistry, 2013. 21(15): p. 4452-4458. PMID[23800723].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23898787">Cell-permeable Stapled Peptides Based on HIV-1 Integrase Inhibitors Derived from HIV-1 Gene Products.</a> Nomura, W., H. Aikawa, N. Ohashi, E. Urano, M. Metifiot, M. Fujino, K. Maddali, T. Ozaki, A. Nozue, T. Narumi, C. Hashimoto, T. Tanaka, Y. Pommier, N. Yamamoto, J.A. Komano, T. Murakami, and H. Tamamura. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23898787].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22468749">Synthesis and Biological Evaluation of 5-Fluoroquinolone-3-carboxylic Acids as Potential HIV-1 Integrase Inhibitors.</a> He, Q.Q., X. Zhang, L.M. Yang, Y.T. Zheng, and F. Chen. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. 28(4): p. 671-676. PMID[22468749].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23821410">Hypericum Hircinum L. Components as New Single-molecule Inhibitors of Both HIV-1 Reverse Transcriptase-associated DNA Polymerase and Ribonuclease H Activities.</a> Esposito, F., C. Sanna, C. Del Vecchio, V. Cannas, A. Venditti, A. Corona, A. Bianco, A.M. Serrilli, L. Guarcini, C. Parolin, M. Ballero, and E. Tramontano. Pathogens and Disease, 2013. 68(3): p. 116-124. PMID[23821410].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0719-080113.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320296900015">Synthesis and anti-HIV Activity of Triazolo-fused 3 &#39;,4 &#39;-Cyclic and 4 &#39;-Spiro Nucleoside Analogues.</a> Sun, J.B., R.W. Liu, L.L. Xuan, W.C. Leng, and J.C. Wu. Chemical Research in Chinese Universities, 2013. 29(3): p. 473-476. ISI[000320296900015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320296400002">A Novel anti-HIV Active Integrase Inhibitor with a Favorable in Vitro Cytochrome P450 and Uridine 5 &#39;-Diphospho-glucuronosyltransferase Metabolism Profile.</a> Okello, M.O., S. Mishra, M. Nishonov, M.K. Mankowski, J.D. Russell, J.Y. Wei, P.A. Hogan, R.G. Ptak, and V. Nair. Antiviral Research, 2013. 98(3): p. 365-372. ISI[000320296400002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320157400001">The Utility of Self-emulsifying Oil Formulation to Improve the Poor Solubility of the Anti HIV Drug CSIC.</a> Obitte, N.C., L.C. Rohan, C.M. Adeyeye, M.A. Parniak, and C.O. Esimone. AIDS research and therapy, 2013. 10. ISI[000320157400001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319953100007">Two New Tryptophan Derivatives from the Seed Kernels of Entada rheedei: Effects on Cell Viability and HIV Infectivity.</a> Nzowa, L.K., R.B. Teponno, L.A. Tapondjou, L. Verotta, Z. Liao, D. Graham, M.C. Zink, and L. Barboni. Fitoterapia, 2013. 87: p. 37-42. ISI[000319953100007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320087100006">Size Matters, So Does Shape: Inhibition of Transcription of T7 RNA Polymerase by Iron(II) Clathrochelates.</a> Novikov, V.V., O.A. Varzatskii, V.V. Negrutska, Y.N. Bubnov, L.G. Palchykovska, I.Y. Dubey, and Y.Z. Voloshin. Journal of Inorganic Biochemistry, 2013. 124: p. 42-45. ISI[000320087100006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320508300022">Inhibitory Activity of Avicennia marina, a Medicinal Plant in Persian Folk Medicine, against HIV and HSV.</a> Namazi, R., R. Zabihollahi, M. Behbahani, and A. Rezaei. Iranian Journal of Pharmaceutical Research, 2013. 12(2): p. 435-443. ISI[000320508300022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320296400006">A New Chimeric Protein Represses HIV-1 LTR-mediated Expression by DNA Methylase.</a> Martinez-Colom, A., S. Lasarte, A. Fernandez-Pineda, M. Relloso, and M.A. Munoz-Fernandez. Antiviral research, 2013. 98(3): p. 394-400. ISI[000320296400006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320275600020">Synthesis of a Long Acting HIV Protease Inhibitor Via Metal or Enzymatic Reduction of the Appropriate Chloro Ketone and Selective Zinc Enolate Condensation with an Amino Epoxide.</a> Houpis, I.N., R.M. Liu, L. Liu, Y.F. Wang, N.F. Dong, X.A. Zhao, Y. Zhang, T.T. Xiao, Y.C. Wang, D. Depre, U. Nettekoven, M. Vogel, R. Wilson, and S. Collier. Advanced Synthesis &amp; Catalysis, 2013. 355(9): p. 1829-1839. ISI[000320275600020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319799900016">Role of the Carbohydrate-binding Sites of Griffithsin in the Prevention of DC-SIGN-mediated Capture and Transmission of HIV-1.</a> Hoorelbeke, B., J. Xue, P.J. LiWang, and J. Balzarini. PloS one, 2013. 8(5). ISI[000319799900016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320380600027">Formation of a Quaternary Complex of HIV-1 Reverse Transcriptase with a Nucleotide-competing Inhibitor and Its ATP Enhancer.</a> Ehteshami, M., M. Nijhuis, J.A. Bernatchez, C.J. Ablenas, S. McCormick, D. de Jong, D. Jochmans, and M. Gotte. Journal of Biological Chemistry, 2013. 288(24): p. 17336-17346. ISI[000320380600027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319799900240">Identification of a Human Protein-derived HIV-1 Fusion Inhibitor Targeting the gp41 Fusion Core Structure.</a> Chao, L.J., L. Lu, H.W. Yang, Y. Zhu, Y. Li, Q. Wang, X.W. Yu, S.B. Jiang, and Y.H. Chen. PloS one, 2013. 8(5). ISI[000319799900240].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0719-080113.</p>

    <br />

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
