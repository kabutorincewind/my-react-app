

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-05-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="plkyz52YLCkDCmM9CxkEbh0Dv87dYXJ4jSkU6e344QXBYz8Gw1aqgjmXe+YeDAuLNmRiltvK7p4+/mR+JgTXJzK9om74E/QXdWgrXkmhonwJmCPdMKHWB32Pv8M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0903E301" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: April 25 - May 8, 2014</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24480521">1,4-Bis(5-(Naphthalen-1-yl)thiophen-2-yl)naphthalene, a Small Molecule, Functions as a Novel anti-HIV-1 Inhibitor Targeting the Interaction between Integrase and Cellular Lens Epithelium-derived Growth Factor.</a> Gu, W.G., D.T. Ip, S.J. Liu, J.H. Chan, Y. Wang, X. Zhang, Y.T. Zheng, and D.C. Wan. Chemico-biological Interactions, 2014. 213: p. 21-27. PMID[24480521].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24556119">Anti-HIV Activities of Novel Synthetic Peptide Conjugated Chitosan Oligomers.</a> Karagozlu, M.Z., F. Karadeniz, and S.K. Kim. International Journal of Biological Macromolecules, 2014. 66: p. 260-266. PMID[24556119].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24704028">Cepharanthine Inhibited HIV-1 Cell-cell Transmission and Cell-free Infection via Modification of Cell Membrane Fluidity.</a> Matsuda, K., S. Hattori, Y. Komizu, R. Kariya, R. Ueoka, and S. Okada. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(9): p. 2115-2117. PMID[24704028].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24631022">The Fungal Metabolite Galiellalactone Interferes with the Nuclear Import of NF-kappaB and Inhibits HIV-1 Replication.</a> Perez, M., R. Soler-Torronteras, J.A. Collado, C.G. Limones, R. Hellsten, M. Johansson, O. Sterner, A. Bjartell, M.A. Calzado, and E. Munoz. Chemico-biological Interactions, 2014. 214: p. 69-76. PMID[24631022].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24686012">PMPA and PMEA Prodrugs for the Treatment of HIV Infections and Human Papillomavirus (HPV) Associated Neoplasia and Cancer.</a> Pertusati, F., K. Hinsinger, A.S. Flynn, N. Powell, A. Tristram, J. Balzarini, and C. McGuigan. European Journal of Medicinal Chemistry, 2014. 78: p. 259-268. PMID[24686012].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24704615">Diketoacid Chelating Ligands as Dual Inhibitors of HIV-1 Integration Process.</a> Rogolino, D., M. Carcelli, C. Compari, L. De Luca, S. Ferro, E. Fisicaro, G. Rispoli, N. Neamati, Z. Debyser, F. Christ, and A. Chimirri. European Journal of Medicinal Chemistry, 2014. 78: p. 425-430. PMID[24704615].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0425-050814.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000332043200006">Synthesis of Anionic Carbosilane Dendrimers via &quot;Click Chemistry&quot; and Their Antiviral Properties against HIV.</a> Arnaiz, E., E. Vacas-Cordoba, M. Galan, M. Pion, R. Gomez, M.A.A. Munoz-Fernandez, and F.J. de la Mata. Journal of Polymer Science Part A-Polymer Chemistry, 2014. 52(8): p. 1099-1112. ISI[000332043200006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333662700027">Study of Marine Natural Products Including Resorcyclic acid Lactones from Humicola fuscoatra That Reactivate Latent HIV-1 Expression in an in Vitro Model of Central Memory CD4+T Cells.</a> Mejia, E.J., S.T. Loveridge, G. Stepan, A. Tsai, G.S. Jones, T. Barnes, K.N. White, M. Draskovic, K. Tenney, M. Tsiang, R. Geleziunas, T. Cihlar, N. Pagratis, Y. Tian, H. Yu, and P. Crews. Journal of Natural Products, 2014. 77(3): p. 618-624. ISI[000333662700027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333662700033">Bioactive Compounds from Vitex leptobotrys.</a> Pan, W.H., K.L. Liu, Y.F. Guan, G.T. Tan, N.V. Hung, N.M. Cuong, D.D. Soejarto, J.M. Pezzuto, H.H.S. Fong, and H.J. Zhang. Journal of Natural Products, 2014. 77(3): p. 663-667. ISI[000333662700033].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333477400041">Aminodiols via Stereocontrolled Oxidation of Methyleneaziridines.</a> Rigoli, J.W., I.A. Guzei, and J.M. Schomaker. Organic Letters, 2014. 16(6): p. 1696-1699. ISI[000333477400041].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333775600032">Monovalent Mannose-based DC-SIGN Antagonists: Targeting the Hydrophobic Groove of the Receptor.</a> Tomasic, T., D. Hajsek, U. Svajger, J. Luzar, N. Obermajer, I. Petit-Haertlein, F. Fieschi, and M. Anderluh. European Journal of Medicinal Chemistry, 2014. 75: p. 308-326. ISI[000333775600032].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333580000009">Synthesis and Biological Evaluation of New Conformationally Restricted S-DABO Hybrids as Non-nucleoside Inhibitors of HIV-1 Reverse Transcriptase.</a> Wu, H.Q., C. Pannecouque, Z.H. Yan, W.X. Chen, Q.Q. He, F.E. Chen, J. Balzarini, D. Daelemans, and E. De Clercq. MedChemComm, 2014. 5(4): p. 468-473. ISI[000333580000009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333580000004">Synthesis and Biological Evaluation of Novel Quinoxalinone-based HIV-1 Reverse Transcriptase Inhibitors.</a> Zhou, J., M.Y. Ba, B. Wang, H.B. Zhou, J.B. Bie, D.C. Fu, Y.L. Cao, B.L. Xu, and Y. Guo. MedChemComm, 2014. 5(4): p. 441-444. ISI[000333580000004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0425-050814.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140410&amp;CC=US&amp;NR=2014100231A1&amp;KC=A1">Preparation of Pyrimidinyl Non-Nucleoside Reverse Transcriptase Inhibitors for Treatment of HIV Infection and AIDS.</a> Arrington, K.L., C. Burgey, R. Gilfillan, Y. Han, M. Patel, C.S. Li, Y. Li, Y. Luo, and Z. Lei. Patent. 2014. 2013-14047515 20140100231: 132pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140417&amp;CC=WO&amp;NR=2014057103A1&amp;KC=A1">Inhibitors of Viral Replication, Their Process of Preparation and Their Therapeutical Uses.</a>Benarous, R., F. Chevreuil, B. Ledoussal, S. Chasset, and F. Le Strat. Patent. 2014. 2013-EP71321 2014057103: 229pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140410&amp;CC=WO&amp;NR=2014053666A1&amp;KC=A1">Preparation of Substituted Thienylacetic acids as Inhibitors of Viral Replication.</a> Benarous, R., F. Chevreuil, B. Ledoussal, S. Chasset, and F. Le Strat. Patent. 2014. 2013-EP70862 2014053666: 243pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0425-050814.</p>

    <br />

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140410&amp;CC=WO&amp;NR=2014053665A1&amp;KC=A1">Preparation of Substituted Pyrazolylacetic acids as Inhibitors of Viral Replication.</a>Benarous, R., F. Chevreuil, B. Ledoussal, S. Chasset, and F. Le Strat. Patent. 2014. 2013-EP70861 2014053665: 168pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0425-050814.</p>

    <br />

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
