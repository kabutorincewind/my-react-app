

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-02-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Or6sm/fr4okdKVDR+0mw2L7hM2GFWaP0MRtKytsr4NCQf8Ipv5982s9w42OI+ZDe5Rr1QvJaRLdTYi2UFvVU53eyWqlTwNy0R2ib0aHRwp140RkSN1OyHQuLbqc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DCEB6AB5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 21, 2011- February 3, 2011</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21185110">2-Hydroxyisoquinoline-1,3(2H,4H)-diones as inhibitors of HIV-1 integrase and reverse transcriptase RNase H domain: Influence of the alkylation of position 4.</a> Billamboz, M., F. Bailly, C. Lion, C. Calmels, M.L. Andreola, M. Witvrouw, F. Christ, Z. Debyser, L. De Luca, A. Chimirri, and P. Cotelle. European Journal of Medicinal Chemistry, 2011. 46(2): p. 535-546; PMID[21185110].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21227704">Design and synthesis of caffeoyl-anilides as portmanteau inhibitors of HIV-1 integrase and CCR5.</a> Bodiwala, H.S., S. Sabde, P. Gupta, R. Mukherjee, R. Kumar, P. Garg, K.K. Bhutani, D. Mitra, and I.P. Singh. Bioorganic and Medicinal Chemistry, 2011. 19(3): p. 1256-1263; PMID[21227704].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21227550">HIV-1 integrase strand-transfer inhibitors: Design, synthesis and molecular modeling investigation.</a> De Luca, L., S. De Grazia, S. Ferro, R. Gitto, F. Christ, Z. Debyser, and A. Chimirri. European Journal of Medicinal Chemistry, 2011. 46(2): p. 756-764; PMID[21227550].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21282450">Novel HIV-1 protease inhibitors (PIs) containing a bicyclic P2 functional moiety, tetrahydropyrano-tetrahydrofuran, that are potent against multi-PI-resistant HIV-1 variants.</a> Ide, K., M. Aoki, M. Amano, Y. Koh, R.S. Yedidi, D. Das, S. Leschenko, B. Chapsal, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>. PMID[21282450].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21115794">In Vitro Antiretroviral Properties of S/GSK1349572, a Next-Generation HIV Integrase Inhibitor.</a> Kobayashi, M., T. Yoshinaga, T. Seki, C. Wakasa-Morimoto, K.W. Brown, R. Ferris, S.A. Foster, R.J. Hazen, S. Miki, A. Suyama-Kagitani, S. Kawauchi-Miki, T. Taishi, T. Kawasuji, B.A. Johns, M.R. Underwood, E.P. Garvey, A. Sato, and T. Fujiwara. Antimicrobial Agents and Chemotherapy, 2011. 55(2): p. 813-821; PMID[21115794].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21280591">Mirabamides E-H, HIV-Inhibitory Depsipeptides from the Sponge Stelletta clavosa.</a> Lu, Z., R.M. Van Wagoner, M.K. Harper, H.L. Baker, J.N. Hooper, C.A. Bewley, and C.M. Ireland. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>. PMID[21280591].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21282419">The impact of the N348I mutation in HIV-1 reverse transcriptase, on non-nucleoside reverse transcriptase inhibitor resistance in non subtype B HIV-1.</a> McCormick, A.L., C.M. Parry, A. Crombe, R.L. Goodall, R.K. Gupta, P. Kaleebu, C. Kityo, M. Chirara, G.J. Towers, and D. Pillay. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>. PMID[21282419].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21133347">Rational Approaches for the Design of Effective Human Immunodeficiency Virus Type 1 Nonnucleoside Reverse Transcriptase Inhibitors.</a> Ribone, S.R., M.A. Quevedo, M. Madrid, and M.C. Brinon. Journal of Chemical Information and Modeling, 2011. 51(1): p. 130-138; PMID[21133347].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21282453">Human Immunodeficiency Virus Type 1 (Cross-) Resistance to Non-Nucleoside Reverse Transcriptase Inhibitors Currently Under Development as Microbiocides.</a> Selhorst, P., A.C. Vazquez, K. Terrazas-Aranda, J. Michiels, K. Vereecken, L. Heyndrickx, J. Weber, M.E. Quinones-Mateu, K.K. Arien, and G. Vanham. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>. PMID[21282453].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21289114">Single Nucleotide Changes in the HIV RRE Mediate Resistance to Compounds That Inhibit Rev Function.</a> Shuck-Lee, D., H. Chang, E.A. Sloan, M.L. Hammarskjold, and D. Rekosh. Journal of Virology, 2011. <b>[Epub ahead of print]</b>. PMID[21289114].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21167206">Simple and rapid determination of the enzyme kinetics of HIV-1 reverse transcriptase and anti-HIV-1 agents by a fluorescence based method.</a> Silprasit, K., R. Thammaporn, S. Tecchasakul, S. Hannongbua, and K. Choowongkomon. Journal of Virology Methods, 2011. 171(2): p. 381-387; PMID[21167206].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21232955">Compounds from Kadsura angustifolia with anti-HIV activity.</a> Sun, R., H.C. Song, C.R. Wang, K.Z. Shen, Y.B. Xu, Y.X. Gao, Y.G. Chen, and J.Y. Dong. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(3): p. 961-965; PMID[21232955].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21250823">Synthesis, QSAR and anti-HIV activity of new 5-benzylthio-1,3,4-oxadiazoles derived from alpha-amino acids.</a> Syed, T., T. Akhtar, N.A. Al-Masoudi, P.G. Jones, and S. Hameed. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>. PMID[21250823].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21159409">Structure-activity relationship studies of 1-(4-chloro-2,5-dimethoxyphenyl)-3-(3-propoxypropyl)thiourea, a non-nucleoside reverse transcriptase inhibitor of human immunodeficiency virus type-1.</a> Weitman, M., K. Lerman, A. Nudelman, D.T. Major, A. Hizi, and A. Herschhorn. European Journal of Medicinal Chemistry, 2011. 46(2): p. 447-467; PMID[21159409].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21272602">A novel retinoic acid, catechin hydrate and mustard oil-based emulsion for enhanced cytokine and antibody responses against multiple strains of HIV-1 following mucosal and systemic vaccinations.</a> Yu, M. and M. Vajdy. Vaccine, 2011. <b>[Epub ahead of print]</b>. PMID[21272602].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21256218">Dual functional RNA nanoparticles containing phi29 motor pRNA and anti-gp120 aptamer for cell-type specific delivery and HIV-1 Inhibition.</a> Zhou, J., Y. Shu, P. Guo, D.D. Smith, and J.J. Rossi. Methods, 2011. [EPUB AHEAD OF PRINT]. PMID[21256218].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21078948">In Vivo Patterns of Resistance to the HIV Attachment Inhibitor BMS-488043.</a> Zhou, N., B. Nowicka-Sans, S. Zhang, L. Fan, J. Fang, H. Fang, Y.F. Gong, B. Eggers, D.R. Langley, T. Wang, J. Kadow, D. Grasela, G.J. Hanna, L. Alexander, R. Colonno, M. Krystal, and P.F. Lin. Antimicrobial Agents and Chemotherapy, 2011. 55(2): p. 729-737; PMID[21078948].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285311000015">Efficient and Practical One-Pot Conversions of N-Tosyltetrahydroisoquinolines into Isoquinolines and of N-Tosyltetrahydro-beta-carbolines into beta-Carbolines through Tandem beta-Elimination and Aromatization.</a> Dong, J., X.X. Shi, J.J. Yan, J. Xing, Q.A. Zhang, and S. Xiao. European Journal of Organic Chemistry, 2010(36): p. 6987-6992; ISI[000285311000015].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285403000022">Rapid synthesis of 2 &#39;,3 &#39;-dideoxy-3 &#39;beta-fluoro-pyrimidine nucleosides from 2 &#39;-deoxypyrimidine nucleosides.</a> Khalil, A., C. Mathe, and C. Perigaud. Bioorganic Chemistry, 2010. 38(4-6): p. 271-274; ISI[000285403000022].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285381200010">Grifonin-1: A Small HIV-1 Entry Inhibitor Derived from the Algal Lectin, Griffithsin.</a> Micewicz, E.D., A.L. Cole, C.L. Jung, H. Luong, M.L. Phillips, P. Pratikhya, S. Sharma, A.J. Waring, A.M. Cole, and P. Ruchala. PLoS ONE, 2010. 5(12): e14360; ISI[000285381200010].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0121-020411.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285544400051">Synthesis and SAR of novel CXCR4 antagonists that are potent inhibitors of T tropic (X4) HIV-1 replication.</a> Skerlj, R., G. Bridger, E. McEachern, C. Harwig, C. Smith, T. Wilson, D. Veale, H. Yee, J. Crawford, K. Skupinska, R. Wauthy, W. Yang, Y.B. Zhu, D. Bogucki, M. Di Fluri, J. Langille, D. Huskens, E. De Clercq, and D. Schols. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(1): p. 262-266; ISI[000285544400051].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0121-020411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
