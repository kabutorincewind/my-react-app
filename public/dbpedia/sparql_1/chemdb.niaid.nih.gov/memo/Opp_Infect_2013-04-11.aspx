

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-04-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eJTjvUzZyX0d++mpXEMj9sPVn3uNHjha9sDXq3e87p1zZl51SGCy8Snvl6TH/8Rnn/aPeBi8lJOh6G4I8QbhJZmcuEMZBs17oEIHv3jTGlO8HMbsRa1yfXgvk9w=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CE61CB9E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 29 - April 11, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23565827">Cytotoxic and Antimicrobial Activity of Selected Cameroonian Edible Plants.</a> Dzoyem, J.P., S.K. Guru, C.A. Pieme, V. Kuete, A. Sharma, I.A. Khan, A.K. Saxena, and R.A. Vishwakarma. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 78. PMID[23565827].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23415966">Antimicrobial and Antioxidant Activities of Alcoholic Extracts of Rumex dentatus L.</a> Humeera, N., A.N. Kamili, S.A. Bandh, S.U. Amin, B.A. Lone, and N. Gousia. Microbial Pathogenesis, 2013. 57: p. 17-20. PMID[23415966].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23538639">Antimicrobial Activity of Calcium Hydroxide and Chlorhexidine on Intratubular Candida albicans.</a> Jacques Rezende Delgado, R., T. Helena Gasparoto, C. Renata Sipert, C. Ramos Pinheiro, I. Gomes de Moraes, R. Brandao Garcia, M. Antonio Hungaro Duarte, C. Monteiro Bramante, S. Aparecido Torres, G. Pompermaier Garlet, A. Paula Campanelli, and N. Bernardineli. International Journal of Oral Science, 2013. 5. PMID[23538639].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23434639">Synthesis, Biological Evaluation of New Oxazolidino-sulfonamides as Potential Antimicrobial Agents.</a> Kamal, A., P. Swapna, R.V. Shetti, A.B. Shaik, M.P. Narasimha Rao, and S. Gupta. European Journal of Medicinal Chemistry, 2013. 62: p. 661-669. PMID[23434639].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23547967">Nylon-3 Polymers with Selective Antifungal Activity.</a> Liu, R., X. Chen, Z. Hayouka, S. Chakraborty, S.P. Falk, B. Weisblum, K.S. Masters, and S.H. Gellman. Journal of the American Chemical Society, 2013. 135(14): p. 5270-5273. PMID[23547967].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23238369">IQ-Motif Peptides as Novel Anti-microbial Agents.</a> McLean, D.T., F.T. Lundy, and D.J. Timson. Biochimie, 2013. 95(4): p. 875-880. PMID[23238369].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23551511">Evaluation of the Antifungal Activity of Four Solutions Used as a Final Rinse in Vitro.</a> Mohammadi, Z., L. Giardino, and F. Palazzi. Australian Endodontic Journal, 2013. 39(1): p. 31-34. PMID[23551511].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23454016">Synthesis of Novel 2-Amino-4-(5&#39;-substituted 2&#39;-phenyl-1H-indol-3&#39;-yl)-6-aryl-4H-pyran-3-carbonitrile Derivatives as Antimicrobial and Antioxidant Agents.</a> Saundane, A.R., K. Vijaykumar, and A.V. Vaijinath. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 1978-1984. PMID[23454016].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br />   

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23556501">Identification and Antifungal Susceptibility of Fungi Isolated from Dermatomycoses.</a> Silva, L.B., D.B. de Oliveira, B.V. da Silva, R.A. de Souza, P.R. da Silva, K. Ferreira-Paim, L.E. Andrade-Silva, M.L. Silva-Vergara, and A.A. Andrade. Journal of the European Academy of Dermatology and Venereology, 2013. <b>[Epub ahead of print]</b>. PMID[23556501].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23551508">Evaluation of the Antimicrobial Activities of Chlorhexidine Gluconate, Sodium Hypochlorite and Octenidine Hydrochloride in Vitro.</a> Tirali, R.E., H. Bodur, B. Sipahi, and E. Sungurtekin. Australian Endodontic Journal, 2013. 39(1): p. 15-18. PMID[23551508].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23551835">Comparative in Vitro Fungicidal Activity of Echinocandins against Candida albicans in Peritoneal Dialysis Fluids.</a> Tobudic, S., C. Forstner, H. Schranz, W. Poeppl, A. Vychytil, and H. Burgmann. Mycoses, 2013. <b>[Epub ahead of print]</b>. PMID[23551835].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23541983">Chemosensitization Potential of P-Glycoprotein Inhibitors in Malaria Parasites.</a> Alcantara, L.M., J. Kim, C.B. Moraes, C.H. Franco, K.D. Franzoi, S. Lee, L.H. Freitas-Junior, and L.S. Ayong. Experimental Parasitology, 2013. <b>[Epub ahead of print]</b>. PMID[23541983].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23454512">Synthesis and Biological Evaluation of a New Class of 4-Aminoquinoline-rhodanine hybrid as Potent Anti-infective Agents.</a> Chauhan, K., M. Sharma, J. Saxena, S.V. Singh, P. Trivedi, K. Srivastava, S.K. Puri, J.K. Saxena, V. Chaturvedi, and P.M. Chauhan. European Journal of Medicinal Chemistry, 2013. 62: p. 693-704. PMID[23454512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23552829">The Use of Chitosan to Enhance Photodynamic Inactivation against Candida albicans and Its Drug-Resistant Clinical Isolates.</a> Chien, H.F., C.P. Chen, Y.C. Chen, P.H. Chang, T. Tsai, and C.T. Chen. International Journal of Molecular Sciences, 2013. 14(4): p. 7445-7456. PMID[23552829].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23434367">6-Oxo and 6-Thio Purine Analogs as Antimycobacterial Agents.</a> Pathak, A.K., V. Pathak, L.E. Seitz, W.J. Suling, and R.C. Reynolds. Bioorganic &amp; Medicinal Chemistry, 2013. 21(7): p. 1685-1695. PMID[23434367].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23220041">Capreomycin Supergenerics for Pulmonary Tuberculosis Treatment: Preparation, in Vitro, and in Vivo Characterization.</a> Schoubben, A., P. Blasi, M.L. Marenzoni, L. Barberini, S. Giovagnoli, C. Cirotto, and M. Ricci. European Journal of Pharmaceutics and Biopharmaceutics, 2013. 83(3): p. 388-395. PMID[23220041].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23549356">Syntheses and Evaluation of Macrocyclic Engelhardione Analogs as Antitubercular and Antibacterial Agents.</a> Shen, L., M.M. M, S. Adhikari, D.F. Bruhn, M. Kumar, R.E. Lee, J.G. Hurdle, R.E. Lee, and D. Sun. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[23549356].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23228936">Antitubercular Pharmacodynamics of Phenothiazines.</a> Warman, A.J., T.S. Rito, N.E. Fisher, D.M. Moss, N.G. Berry, P.M. O&#39;Neill, S.A. Ward, and G.A. Biagini. The Journal of Antimicrobial Chemotherapy, 2013. 68(4): p. 869-880. PMID[23228936].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23538901">Synthesis, in Vitro Antimycobacterial and Antibacterial Evaluation of IMB-070593 Derivatives Containing a Substituted Benzyloxime Moiety.</a> Wei, Z., J. Wang, M. Liu, S. Li, L. Sun, H. Guo, B. Wang, and Y. Lu. Molecules, 2013. 18(4): p. 3872-3893. PMID[23538901].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23547773">Effective Treatment with a Tetrandrine/Chloroquine Combination for Chloroquine-resistant Falciparum Malaria in Aotus Monkeys.</a> Ye, Z., K. Van Dyke, and R.N. Rossan. Malaria Journal, 2013. 12: p. 117. PMID[23547773].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23568546">A Facile Hydroxyindole Carboxylic Acid Based Focused Library Approach for Potent and Selective Inhibitors of Mycobacterium Protein Tyrosine Phosphatase B.</a> Zeng, L.F., J. Xu, Y. He, R. He, L. Wu, A.M. Gunawan, and Z.Y. Zhang. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23568546]. </p>
    
    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23353738">Synthesis, Cytotoxicity and Antiplasmodial Activity of Novel ent-Kaurane Derivatives.</a> Batista, R., P.A. Garcia, M.A. Castro, J.M. Miguel Del Corral, N.L. Speziali, P.V.F. de, R.C. de Paula, L.F. Garcia-Fernandez, A. Francesch, A. San Feliciano, and A.B. de Oliveira. European Journal of Medicinal Chemistry, 2013. 62: p. 168-176. PMID[23353738].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23549355">Saccharosporones A, B and C, Cytotoxic Antimalarial Angucyclinones from Saccharopolyspora sp. BCC 21906.</a> Boonlarppradab, C., C. Suriyachadkun, P. Rachtawee, and W. Choowong. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[23549355].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2343528">Azide-Alkyne Cycloaddition en Route to 1H-1,2,3-Triazole-tethered 7-chloroquinoline-isatin chimeras: Synthesis and Antimalarial Evaluation.</a> Raj, R., P. Singh, P. Singh, J. Gut, P.J. Rosenthal, and V. Kumar. European Journal of Medicinal Chemistry, 2013. 62: p. 590-596. PMID[23434528].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23555851">New Targets for Drug Discovery against Malaria.</a> Santos, G. and N.V. Torres. Plos One, 2013. 8(3): p. e59968. PMID[23555851].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23428849">Expression, Characterization and Inhibition of Toxoplasma Gondii 1-Deoxy-d-xylulose-5-phosphate reductoisomerase.</a> Cai, G., L. Deng, J. Xue, S.N. Moreno, B. Striepen, and Y. Song. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2158-2161. PMID[23428849].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23453069">Design, Synthesis, and Biological Activity of Diaryl Ether Inhibitors of Toxoplasma Gondii Enoyl Reductase.</a> Cheng, G., S.P. Muench, Y. Zhou, G.A. Afanador, E.J. Mui, A. Fomovska, B.S. Lai, S.T. Prigge, S. Woods, C.W. Roberts, M.R. Hickman, P.J. Lee, S.E. Leed, J.M. Auschwitz, D.W. Rice, and R. McLeod. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2035-2043. PMID[23453069].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23380722">Inhibitors of eIF2alpha Dephosphorylation Slow Replication and Stabilize Latency in Toxoplasma Gondii.</a> Konrad, C., S.F. Queener, R.C. Wek, and W.J. Sullivan, Jr. Antimicrobial Agents and Chemotherapy, 2013. 57(4): p. 1815-1822. PMID[23380722].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0329-041113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315928500001">In Vitro Biological Effects of Two Anti-diabetic Medicinal Plants Used in Benin as Folk Medicine.</a> Bothon, F.T.D., E. Debiton, F. Avlessi, C. Forestier, J.C. Teulade, and D.K.C. Sohounhloue. BMC Complementary and Alternative Medicine, 2013. 13. ISI[000315928500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315653300016">30. Synthesis and Testing of 3-Acetyl-2,5-disubstituted-2,3-dihydro-1,3,4-oxadiazole Derivatives for Antifungal Activity against Selected Candida Species.</a>  de Oliveira, C.S., B.F. Lira, J.M. Barbosa, J.G.F. Lorenzo, C.P. de Menezes, J. dos Santos, E.D. Lima, and P.F. de Athayde. Journal of the Brazilian Chemical Society, 2013. 24(1): p. 115-U200. ISI[000315653300016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315182100018">Discovery and Evaluation of Novel Inhibitors of Mycobacterium Protein Tyrosine Phosphatase B from the 6-Hydroxy-benzofuran-5-carboxylic Acid Scaffold.</a>  He, Y.T., J. Xu, Z.H. Yu, A.M. Gunawan, L. Wu, L.N. Wang, and Z.Y. Zhang. Journal of Medicinal Chemistry, 2013. 56(3): p. 832-842. ISI[000315182100018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00031547600012">A New Isoflavone Derivative from Streptomyces sp YIM GS3536.</a> Huang, R., Z.G. Ding, Y.F. Long, J.Y. Zhao, M.G. Li, X.L. Cui, and M.L. Wen. Chemistry of Natural Compounds, 2013. 48(6): p. 966-969. ISI[000315417600012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315602700031">The Synthetic Melanocortin (CKPV)(2) Exerts Anti-fungal and Anti-inflammatory Effects against Candida albicans Vaginitis via Inducing Macrophage M-2 Polarization.</a>  Ji, H.X., Y.L. Zou, J.J. Duan, Z.R. Jia, X.J. Li, Z. Wang, L. Li, Y.W. Li, G.Y. Liu, M.Q. Tong, X.Y. Li, G.H. Zhang, X.R. Dai, L. He, Z.Y. Li, C. Cao, and Y. Yang. Plos One, 2013. 8(2). ISI[000315602700031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315554100004">Synthesis and Antibacterial and Antifungal Evaluation of Some Chalcone Based Sulfones and Bisulfones.</a> Konduru, N.K., S. Dey, M. Sajid, M. Owais, and N. Ahmed. European Journal of Medicinal Chemistry, 2013. 59: p. 23-30. ISI[000315554100004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315931200009">The Antimicrobial Activity of Thymus vulgaris and Origanum syriacum Essential Oils on Staphylococcus aureus, Streptococcus pneumoniae and Candida albicans.</a> Lakis, Z., D. Mihele, I. Nicorescu, V. Vulturescu, and D.I. Udeanu. Farmacia, 2012. 60(6): p. 857-865. ISI[000315931200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315494000009">Cluster-based Molecular Docking Study for in Silico Identification of Novel 6-Fluoroquinolones as Potential Inhibitors against Mycobacterium tuberculosis.</a> Minovski, N., A. Perdih, M. Novic, and T. Solmajer. Journal of Computational Chemistry, 2013. 34(9): p. 790-801. ISI[000315494000009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315931200003">Synthesis and Antimicrobial Activity of Some New 2-Hydrazone-thiazoline-4-ones.</a> Oniga, O., J.T. Ndongo, C. Moldovan, B. Tiperciuc, S. Oniga, A. Pirnau, L. Vlase, and P. Verite. Farmacia, 2012. 60(6): p. 785-797. ISI[000315931200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00031570300033">Cloning, Characterization, and Sulfonamide and Thiol Inhibition Studies of an alpha-Carbonic Anhydrase from Trypanosoma cruzi, the Causative Agent of Chagas Disease.</a> Pan, P.W., A.B. Vermelho, G.C. Rodrigues, A. Scozzafava, M.E.E. Tolvanen, S. Parkkila, C. Capasso, and C.T. Supuran. Journal of Medicinal Chemistry, 2013. 56(4): p. 1761-1771. ISI[000315707300033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315818200021">Electrosynthesis and Screening of Novel 1,3,4-Oxadiazoles as Potent and Selective Antifungal Agents.</a> Singh, S., L.K. Sharma, A. Saraswat, I.R. Siddiqui, H.K. Kehri, and R.K.P. Singh. RSC Advances, 2013. 3(13): p. 4237-4245. ISI[000315818200021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315400600063">Antibacterial/Antifungal Activity and Synergistic Interactions between Polyprenols and Other Lipids Isolated from Ginkgo biloba L. Leaves.</a> Tao, R., C.Z. Wang, and Z.W. Kong. Molecules, 2013. 18(2): p. 2166-2182. ISI[000315400600063].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315468200005">Interleukin-12 and Interleukin-2 Alone or in Combination against the Infection in Invasive Pulmonary Aspergillosis Mouse Model.</a> Zhang, C.R., J.C. Lin, W.M. Xu, M. Li, H.S. Ye, W.L. Cui, and Q. Lin. Mycoses, 2013. 56(2): p. 117-122. ISI[000315468200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315839300001">A Novel Cysteine-rich Antimicrobial Peptide from the Mucus of the Snail of Achatina fulica.</a> Zhong, J., W.H. Wang, X.M. Yang, X.W. Yan, and R. Liu. Peptides, 2013. 39: p. 1-5. ISI[000315839300001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0329-041113.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
