

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-06-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nw2Nf4KAP3tR53u2JNoLQTGW/THBUXhtiSgIzSG2lugevyXRbqOtkGbyGQRDHaImue9gU3ce6e8NAKnouAPSwFnqMnCgcfGet5A9YnMrJRB9kmpC7KGYrMgv290=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="96A66F98" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: May 22 - June 4, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25779585">Subtype-specific Analysis of the K65r Substitution in HIV-1 That Confers Hypersusceptibility to a Novel Nucleotide-competing Reverse Transcriptase Inhibitor.</a> Xu, H.T., S.P. Colby-Germinario, P.K. Quashie, R. Bethell, and M.A. Wainberg. Antimicrobial Agents and Chemotherapy, 2015. 59(6): p. 3189-3196. PMID[25779585].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25873391">MicroRNA-155 Reinforces HIV Latency.</a> Ruelas, D.S., J.K. Chan, E. Oh, A.J. Heidersbach, A.M. Hebbeler, L. Chavez, E. Verdin, M. Rape, and W.C. Greene. Journal of Biological Chemistry, 2015. 290(22): p. 13736-13748. PMID[25873391].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25841639">Identification of anti-HIV Active Dicaffeoylquinic- and Tricaffeoylquinic acids in Helichrysum populifolium by NMR-based Metabolomic Guided Fractionation.</a> Heyman, H.M., F. Senejoux, I. Seibert, T. Klimkait, V.J. Maharaj, and J.J. Meyer. Fitoterapia, 2015. 103: p. 155-164. PMID[25841639].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25328020">Design, Synthesis, and Biological Evaluation of 1-(Thiophen-2-yl)-9H-pyrido[3,4-b]Indole Derivatives as anti-HIV-1 Agents.</a> Ashok, P., C.L. Lu, S. Chander, Y.T. Zheng, and S. Murugesan. Chemical Biology &amp; Drug Design, 2015. 85(6): p. 722-728. PMID[25328020].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0522-060415.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000354316800001">Antiviral Activity of a Zymolytic Grain Based Extract on Human Immunodeficiency Virus Type 1 in Vitro.</a> Wang, C., D. Liu, X.H. Guo, B. Yu, H. Wu, H.H. Zhang, J.X. Wu, C.L. Jiang, W. Kong, and X.H. Yu. Evidence-Based Complementary and Alternative Medicine, 2015. 642327: 9pp . ISI[000354316800001].
    <br />
    <b>[WOS]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000354698100001">Novel 3&#39;-Processing Integrase Activity Assay by Real-time PCR for Screening and Identification of HIV-1 Integrase Inhibitors.</a> Sakkhachornphop, S., W. Thongkum, and C. Tayapiwatana. Biomed Research International, 2015. 853891: 7pp. ISI[000354698100001].
    <br />
    <b>[WOS]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000354247900001">Griffithsin Tandemers: Flexible and Potent Lectin Inhibitors of the Human Immunodeficiency Virus.</a> Moulaei, T., K.B. Alexandre, S.R. Shenoy, J.R. Meyerson, L.R.H. Krumpe, B. Constantine, J. Wilson, R.W. Buckheit, J.B. McMahon, S. Subramaniam, A. Wlodawer, and B.R. O&#39;Keefe. Retrovirology, 2015. 12(6): 14pp. ISI[000354247900001].
    <br />
    <b>[WOS]</b>. HIV_0522-060415.</p>
    
    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000354119800077">Inhibition of Drug-resistant HIV by Targeting Cellular CDK9 in Humanized Mice.</a> Heredia, A., N. Le, S. Medina-Moreno, J. Zapata, M. Reitz, J. Bryant, and R. Redfield. JAIDS-Journal of Acquired Immune Deficiency Syndromes, 2014. 67: p. 77-77. ISI[000354119800077].
    <br />
    <b>[WOS]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000353610200004">New Hybrids between Triterpenoid acids and Nucleoside HIV-RT Inhibitors.</a> Dang, A.T.T., C.T. Pham, T.A. Le, H.H. Truong, H.T.T. Vu, A.T. Soldatenkov, and T.V. Nguyen. Mendeleev Communications, 2015. 25(2): p. 96-98. ISI[000353610200004].
    <br />
    <b>[WOS]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000354212000015">Armochaetoglobins K-R, anti-HIV Pyrrole-based Cytochalasans from Chaetomium globosum TWL-1.</a> Chen, C.M., H.C. Zhu, J.P. Wang, J. Yang, X.N. Li, J. Wang, K.L. Chen, Y.Y. Wang, Z.W. Luo, G.M. Yao, Y.B. Xue, and Y.H. Zhang. European Journal of Organic Chemistry, 2015(14): p. 3086-3094. ISI[000354212000015].
    <br />
    <b>[WOS]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000353711600148">Potent anti-HIV Chemokine Analogs Direct Post-endocytic Sorting of CCR5.</a> Bonsch, C., M. Munteanu, I. Rossitto-Borlat, A. Furstenberg, and O. Hartley. PloS One, 2015. 10(4). ISI[000353711600148].
    <br />
    <b>[WOS]</b>. HIV_0522-060415.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">12. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150416&amp;CC=US&amp;NR=2015105351A1&amp;KC=A1">Preparation of Heterocyclyloxyphenoxyethyl Substituted Pyrimidine-2,4(1H,3H)-diones for Treating HIV Infections.</a> Jorgensen, W.L. and K.S. Anderson. Patent. 2015. 2014-14509301 20150105351: 66pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0522-060415.</p>

    <br />

    <p class="plaintext">13. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150430&amp;CC=WO&amp;NR=2015061518A1&amp;KC=A1">Preparation of Sulfonylaminocarbonyl amino acid amides as Inhibitors of Human Immunodeficiency Virus Replication and Use for the Treatment of HIV Infection.</a> Pendri, A., G. Li, J.A. Bender, Z. Yang, A.X. Wang, B.R. Beno, R.A. Fridell, M. Belema, N.A. Meanwell, and R.G. Gentles. Patent. 2015. 2014-US61870 2015061518: 378pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0522-060415.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
