

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-01-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gRJZEGmk/mI/2awAwUm/8jHkLzq7AFLA85GN6oNEUvhTL4rDs0Z4WXd34nMOZ6ZUsyTE9XlM6sAek9kfcYtzEK0/OC+h0zvvWBFoVYsyQVAYhxbwkvwyrlAJpx4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DE2B3F52" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: January 3 - January 16, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24393620">Astershionones A-F, Six New anti-HBV Shionane-type Triterpenes from Aster tataricus.</a> Zhou, W.B., G.Z. Zeng, H.M. Xu, W.J. He, Y.M. Zhang, and N.H. Tan. Fitoterapia, 2014. <b>[Epub ahead of print]</b>. PMID[24393620].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24397541">Anti-Hepatitis C Virus Compounds Obtained from Glycyrrhiza uralensis and Other Glycyrrhiza Species.</a> Adianti, M., C. Aoki, M. Komoto, L. Deng, I. Shoji, T.S. Wahyuni, M.I. Lusida, Soetjipo, H. Fuchino, N. Kawahara, and H. Hotta. Microbiology and Immunology, 2014. PMID[24397541]. <b>[PubMed]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24388689">Trinorditerpenes from the Roots of Flueggea virosa.</a> Chao, C.H., J.C. Cheng, T.L. Hwang, D.Y. Shen, and T.S. Wu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(2): p. 447-449. PMID[24388689].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24400834">Anti-Hepatitis C Virus Dinorditerpenes from the Roots of Flueggea virosa.</a> Chao, C.H., J.C. Cheng, D.Y. Shen, and T.S. Wu. Journal of Natural Products, 2014. <b>[Epub ahead of print]</b>. PMID[24400834].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24400777">Discovery of ABT-267, a Pan-genotypic Inhibitor of HCV NS5A.</a> Degoey, D.A., J. Randolph, D. Liu, J. Pratt, C. Hutchins, P. Donner, C. Krueger, M. Matulenko, S. Patel, C. Motter, L. Nelson, R. Keddy, M. Tufano, D. Caspi, P. Krishnan, N. Mistry, G. Koev, T. Reisch, R. Mondal, T. Pilot-Matias, G. Yi, D. Beno, C. Maring, A. Molla, E. Dumas, A. Campbell, L. Williams, C. Collins, R. Wagner, and W. Kati. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24400777].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24419349">Inhibition of Hepatitis C Virus Replication by GS-6620, a Potent C-Nucleoside monophosphate Prodrug.</a> Feng, J.Y., G. Cheng, J. Perry, O. Barauskas, Y. Xu, M. Fenaux, S. Eng, N. Tirunagari, B. Peng, M. Yu, Y. Tian, Y.J. Lee, G. Stepan, L.L. Lagpacan, D. Jin, M. Hung, K.S. Ku, B. Han, K. Kitrinos, M. Perron, G. Birkus, K.A. Wong, W. Zhong, C.U. Kim, A. Carey, A. Cho, and A.S. Ray. Antimicrobial Agents and Chemotherapy, 2014. <b>[Epub ahead of print]</b>. PMID[24419349].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24397558">Discovery and Preclinical Characterization of the Cyclopropylindolobenzazepine BMS-791325, a Potent Allosteric Inhibitor of the Hepatitis C Virus NS5B Polymerase.</a> Gentles, R.G., M. Ding, J.A. Bender, C.P. Bergstrom, K. Grant-Young, P. Hewawasam, T. Hudyma, S. Martin, A. Nickel, A. Regueiro-Ren, Y. Tu, Z. Yang, K.S. Yeung, X. Zheng, B.R. Beno, D.M. Camac, C.H. Chang, M. Gao, P.E. Morin, S. Sheriff, J. Tredup, J. Wan, M.R. Witmer, D. Xie, U. Hanumegowda, J. Knipe, K. Mosure, K.S. Santone, D.D. Parker, X. Zhuo, J. Lemm, M. Liu, L. Pelosi, K. Rigat, S. Voss, Y. Wang, Y.K. Wang, R.C. Colonno, M. Gao, S.B. Roberts, Q. Gao, A. Ng, N.A. Meanwell, and J.F. Kadow. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24397558].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24345201">Nucleotide Prodrugs of 2&#39;-Deoxy-2&#39;-spirooxetane Ribonucleosides as Novel Inhibitors of the HCV NS5B Polymerase.</a> Jonckers, T.H., K. Vandyck, L. Vandekerckhove, L. Hu, A. Tahri, S. Van Hoof, T.I. Lin, L. Vijgen, J.M. Berke, S. Lachau-Durand, B. Stoops, L. Leclercq, G. Fanning, B. Samuelsson, M. Nilsson, A. Rosenquist, K. Simmen, and P. Raboisson. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24345201].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24360997">UK-1 and Structural Analogs are Potent Inhibitors of Hepatitis C Virus Replication.</a> Ward, D.N., D.C. Talley, M. Tavag, S. Menji, P. Schaughency, A. Baier, and P.J. Smith. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(2): p. 609-612. PMID[24360997].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0103-011614.</p>

    <h2>HCMV</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24419339">DSTP-27 Prevents Entry of Human Cytomegalovirus.</a> Paeschke, R., I. Woskobojnik, V. Makarov, M. Schmidtke, and E. Bogner. Antimicrobial Agents and Chemotherapy, 2014. <b>[Epub ahead of print]</b>. PMID[24419339].</p>

    <p class="plaintext">[PubMed]. OV_0103-011614.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000327695900006">Synthesis and Biological Evaluation of bis-Imidazolidineiminothiones: A Comparative Study.</a> El-Sharief, M.A.M.S., Z. Moussa, and A.M.S. El-Sharief. Archiv der Pharmazie, 2013. 346(7): p. 542-555. ISI[000327695900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0103-011614.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000327831500019">Inhibition of Hepatitis C Virus by the Cyanobacterial Protein Microcystis viridis Lectin: Mechanistic Differences between the High-mannose Specific Lectins MVL, CV-N, and GNA.</a> Kachko, A., S. Loesgen, S. Shahzad-ul-Hussan, W. Tan, I. Zubkova, K. Takeda, F. Wells, S. Rubin, C.A. Bewley, and M.E. Major. Molecular Pharmaceutics, 2013. 10(12): p. 4590-4602. ISI[000327831500019].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0103-011614.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
