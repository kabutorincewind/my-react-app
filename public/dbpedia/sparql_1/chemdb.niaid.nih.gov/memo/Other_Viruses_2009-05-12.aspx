

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-05-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Hskaf32SpRPcU4GOC3Dile/5xVzaUOvtmehRPnUtEolSLCK+mUpqLqNNprwyfGpWOSmsAXT6DnAse2BuD8jSqa5qit/QsbF1d3nfoWCXgfq5NryYDOvraRHnit8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BA3ED415" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List:  April 29, 2009 - May 12, 2009</h1>

    <p class="memofmt2-2">CYTOMEGALOVIRUS</p>

    <p class="ListParagraph">1.      <a href="http://www.sciencedirect.com/science?_ob=ArticleURL&amp;_udi=B6T2H-4W6XSV2-3S&amp;_user=4429&amp;_coverDate=05%2F31%2F2009&amp;_rdoc=106&amp;_fmt=high&amp;_orig=browse&amp;_srch=doc-info%28%23toc%234919%232009%23999179997%231024528%23FLA%23display%23Volume%29&amp;_cdi=4919&amp;_sort=d&amp;_docanchor=&amp;view=c&amp;_ct=188&amp;_acct=C000059602&amp;_version=1&amp;_urlVersion=0&amp;_userid=4429&amp;md5=1cd8c8a086d6b48b97fe076969674768">Anti-cytomegalovirus Activity of Membranotropic Polyacidic Agents Effects In Vitro</a></p>
    
    <p class="NoSpacing">M. Pavlova, A. Serbin, N. Fedorova, E. Karaseva, E. Klimova, A. Kushch</p>
    
    <p class="NoSpacing">Antiviral Research. 2009 May; 82(2): A50-51.   PMID: None</p>

    <p class="memofmt2-2">HEPATITIS VIRUS</p>

    <p class="ListParagraph">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19419173?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Stereoselective Synthesis and Biological Evaluations of Novel 3&#39;-Deoxy-4&#39;-azaribonucleosides as Inhibitors of Hepatitis C Virus RNA Replication.</a></p>

    <p class="NoSpacing">Chiacchio U, Borrello L, Crispino L, Rescifina A, Merino P, Macchi B, Balestrieri E, Mastino A, Piperno A, Romeo G.</p>

    <p class="NoSpacing">J Med Chem. 2009 May 6. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19419173 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19402666?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Synthesis and Biological Characterization of B-Ring Amino Analogues of Potent Benzothiadiazine Hepatitis C Virus Polymerase Inhibitors.</a></p>
    
    <p class="NoSpacing">Randolph JT, Flentge CA, Huang PP, Hutchinson DK, Klein LL, Lim HB, Mondal R, Reisch T, Montgomery DA, Jiang WW, Masse SV, Hernandez LE, Henry RF, Liu Y, Koev G, Kati WM, Stewart KD, Beno DW, Molla A, Kempf DJ.</p>
    
    <p class="NoSpacing">J Med Chem. 2009 Apr 29. [Epub ahead of print]         </p>

    <p class="NoSpacing">PMID: 19402666 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19342234?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">1,5-Benzodiazepine inhibitors of HCV NS5B polymerase.</a></p>

    <p class="NoSpacing">McGowan D, Nyanguile O, Cummings MD, Vendeville S, Vandyck K, Van den Broeck W, Boutton CW, De Bondt H, Quirynen L, Amssoms K, Bonfanti JF, Last S, Rombauts K, Tahri A, Hu L, Delouvroy F, Vermeiren K, Vandercruyssen G, Van der Helm L, Cleiren E, Mostmans W, Lory P, Pille G, Van Emelen K, Fanning G, Pauwels F, Lin TI, Simmen K, Raboisson P.</p>
    
    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 May 1;19(9):2492-6. Epub 2009 Mar 14.</p>
    
    <p class="NoSpacing">PMID: 19342234 [PubMed - in process]          </p>

    <p class="ListParagraph">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19328685?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Phosphorous acid analogs of novel P2-P4 macrocycles as inhibitors of HCV-NS3 protease.</a></p>
    
    <p class="NoSpacing">Pompei M, Francesco ME, Koch U, Liverton NJ, Summa V.</p>
    
    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 May 1;19(9):2574-8. Epub 2009 Mar 17.</p>
    
    <p class="NoSpacing">PMID: 19328685 [PubMed - in process]          </p>

    <p class="ListParagraph">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19303654?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Substituted imidazopyridines as potent inhibitors of HCV replication.</a></p>
    
    <p class="NoSpacing">Vliegen I, Paeshuyse J, De Burghgraeve T, Lehman LS, Paulson M, Shih IH, Mabery E, Boddeker N, De Clercq E, Reiser H, Oare D, Lee WA, Zhong W, Bondy S, Pürstinger G, Neyts J.</p>
    
    <p class="NoSpacing">J Hepatol. 2009 May;50(5):999-1009. Epub 2009 Feb 26.</p>
    
    <p class="NoSpacing">PMID: 19303654 [PubMed - in process]          </p>
    
    <p class="NoSpacing">EPSTEIN BARR VIRUS</p>

    <p class="ListParagraph">7.    <a href="http://www.sciencedirect.com/science?_ob=ArticleURL&amp;_udi=B6T2H-4W6XSV2-3P&amp;_user=4429&amp;_coverDate=05%2F31%2F2009&amp;_rdoc=104&amp;_fmt=high&amp;_orig=browse&amp;_srch=doc-info%28%23toc%234919%232009%23999179997%231024528%23FLA%23display%23Volume%29&amp;_cdi=4919&amp;_sort=d&amp;_docanchor=&amp;view=c&amp;_ct=188&amp;_acct=C000059602&amp;_version=1&amp;_urlVersion=0&amp;_userid=4429&amp;md5=dd88f52fa36a0101b2c9efa773b751ad">Study of Anti-Epstein-Barr Virus Activity of Novel Fluorinated Heterocyclic Nucleoside Analogues</a></p>

    <p class="NoSpacing">N. Nesterova, Yu. Shermolovich, S. Zagorodnya, A. Golovan, O. Kanishchev, G. Baranova</p>

    <p class="NoSpacing">Antiviral Research. 2009 May; 82(2): A50.</p>

    <p class="NoSpacing">PMID: None</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
