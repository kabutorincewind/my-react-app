

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-12-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vnAqhklDH5tppgI8fEUVNpHiXFpJIfZfglL+m0z3bFTuXNTTuaLSot5lNbLCcy8XhMhQxu1fma3x0V8dpUJQSkV+wcYBWSRLF27pnr0gQSXD/OeBu2mMAOVsBQ0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1D08DAE9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: December 5 - December 18, 2014</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25453811">Potent Bisimidazole-based HCV NS5A Inhibitors Bearing Annulated Tricyclic Motifs.</a> Zhong, M., E. Peng, N. Huang, Q. Huang, A. Huq, M. Lau, R. Colonno, and L. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(24): p. 5738-5742. PMID[25453811].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25453810">Discovery of Functionalized Bisimidazoles Bearing Cyclic Aliphatic-phenyl Motifs as HCV NS5A Inhibitors.</a> Zhong, M., E. Peng, N. Huang, Q. Huang, A. Huq, M. Lau, R. Colonno, and L. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(24): p. 5731-5737. PMID[25453810].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25467815">Interferon-inducible Cholesterol-25-hydroxylase Inhibits Hepatitis C Virus Replication via Distinct Mechanisms.</a> Chen, Y., S. Wang, Z. Yi, H. Tian, R. Aliyari, Y. Li, G. Chen, P. Liu, J. Zhong, X. Chen, P. Du, L. Su, F.X. Qin, H. Deng, and G. Cheng. Scientific Reports, 2014. 4: p. 7242. PMID[25467815].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. OV_1205-121814.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805066">HCV Nucleoside Inhibitors Can Exhibit Genotype Specific Differences in Activity.</a> Anton, E.D., K. Strommen, W. Huang, A.A. Rivera, C.J. Petropoulos, F. Amblard, R.F. Schinazi, and J.D. Reeves. Hepatology, 2014. 60: p. 1169A-1170A. ISI[000344483805066].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805054">MK-8408, a Potent and Selective NS5A Inhibitor with a High Genetic Barrier to Resistance and Activity against HCV Genotypes 1-6.</a> Asante-Appiah, E., R. Liu, S. Curry, P. McMonagle, S. Agrawal, D. Carr, L. Rokosz, F. Lahser, K. Bystol, R. Chase, S. Black, E.B. Ferrari, P. Ingravallo, S.Y. Chen, L. Tong, W.S. Yu, and J. Kozlowski. Hepatology, 2014. 60: p. 1163A-1164A. ISI[000344483805054].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805072">IDX21459, a Uridine Nucleotide Prodrug, Shows a Favorable Preclinical Profile as a Direct-acting Antiviral Agent (DAA) against HCV.</a> Chapron, C.D., K.S. Gupta, M. La Colla, B. Hernandez-Santiago, M. Seifer, H. Rashidzadeh, I. Serra, S.Q. Luo, X.R. Pan-Zhou, C. Brynczka, B. Heinrich, J.S. Lim, F.R. Alexandre, and R. Rush. Hepatology, 2014. 60: p. 1172A-1173A. ISI[000344483805072].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805071">The Antiviral Profile of TG-2349, a Novel HCV Protease Inhibitor with Pan-Genotypic Activity.</a> Chen, C.M., Y.F. Chen, C.C. Lin, C.H.R. King, and M.C. Hsu. Hepatology, 2014. 60: p. 1172A-1172A. ISI[000344483805071].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344583300017">Facile Synthesis of Mollugin by Kinetic Control and anti-HCV (Hepatitis C Virus) Activity of Its Analogues.</a> Choi, D.H., N.R. Lee, C.G. Kim, J.W. Kim, S.W. Lee, and J.G. Jun. Bulletin of the Korean Chemical Society, 2014. 35(11): p. 3232-3238. ISI[000344583300017].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483804489">CPI-431-32, a Novel Cyclophilin A Inhibitor, Simultaneously Blocks Replication of HCV and HIV-1 Viruses in a Novel in Vitro Co-infection Model.</a> Gallay, P., M. Bobardt, D. Trepanier, D. Ure, C. Ordonez, and R.T. Foster. Hepatology, 2014. 60: p. 1128A-1128A. ISI[000344483804489].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000345185100015">(-)-Epigallocatechin-3-gallate Inhibits Entry of Hepatitis B Virus into Hepatocytes.</a> Huang, H.C., M.H. Tao, T.M. Hung, J.C. Chen, Z.J. Lin, and C. Huang. Antiviral Research, 2014. 111: p. 100-111. ISI[000345185100015].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805069">Evaluation of TG-2349, a Novel HCV Protease Inhibitor with Pan-genotypic Activity, in Replicon-mouse Models with Luciferase Reporter.</a> Huang, Y.H., C.M. Chen, H.M. Hsu, C.C. Lin, C.H.R. King, and M.C. Hsu. Hepatology, 2014. 60: p. 1171A-1171A. ISI[000344483805069].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805063">The Combination of MK-5172, an NS3 Inhibitor, and MK-8408, an NS5A Inhibitor, Presents a High Genetic Barrier to Resistance in HCV Genotypes.</a> Lahser, F., K. Bystol, S. Curry, P. McMonagle, R. Liu, E. Xia, R. Chase, S. Black, E.B. Ferrari, L. Tong, W.S. Yu, J. Kozlowski, and E. Asante-Appiah. Hepatology, 2014. 60: p. 1168A-1168A. ISI[000344483805063].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000345287300007">Vinylated Linear P2 Pyrimidinyloxyphenylglycine Based Inhibitors of the HCV NS3/4A Protease and Corresponding Macrocycles.</a> Lampa, A., H. Alogheli, A.E. Ehrenberg, E. Akerblom, R. Svensson, P. Artursson, U.H. Danielson, A. Karlen, and A. Sandstrom. Bioorganic &amp; Medicinal Chemistry, 2014. 22(23): p. 6595-6615. ISI[000345287300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805021">A Next Generation HCV DAA Combination: Potent, Pangenotypic Inhibitors ABT-493 and ABT-530 with High Barriers to Resistance.</a> Ng, T., T. Pilot-Matias, L.J. Lu, T. Reisch, T. Dekhtyar, P. Krishnan, J. Beyer, R. Tripathi, R.B. Pithawalla, A. Asatryan, A.L. Campbell, J. Kort, and C. Collins. Hepatology, 2014. 60: p. 1142A-1142A. ISI[000344483805021].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344804200014">Downregulation of Inducible Nitric Oxide Synthase (iNOS) Expression is implicated in the Antiviral Activity of Acetylsalicylic acid in HCV-expressing Cells.</a> Rios-Ibarra, C.P., S. Lozano-Sepulveda, L. Munoz-Espinosa, A.R. Rincon-Sanchez, C. Cordova-Fletes, and A.M.G. Rivas-Estilla. Archives of Virology, 2014. 159(12): p. 3321-3328. ISI[000344804200014].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483804391">Quercetin Inhibits Both Replication and Assembly of Hepatitis C Virus by Blocking the Viral NS3 and the Host Diacylglycerol Acyltransferase Type 1.</a> Rojas, A., S. Clement, M. Lemasson, J.A. Del Campo, M. Garcia-Valdecasas, A. Gil-Gomez, I. Ranchal, J. Bautista, A.R. Rosenberg, F. Negro, and M. Romero-Gomez. Hepatology, 2014. 60: p. 1078A-1078A. ISI[000344483804391].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483804331">Mipomersen, an FDA-approved Anti-sense Inhibitor of apoB100, has anti-HCV Effect in-Vitro.</a> Schaefer, E.A., J. Meixiong, D.L. Motola, D. Fusco, C. Brisac, S. Salloum, J. Luther, W.Y. Lin, M.P. McGowan, L.F. Peng, and R.T. Chung. Hepatology, 2014. 60: p. 1050A-1050A. ISI[000344483804331].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805050">A 4-N-Hydroxycytidine ribonucleoside phosphoramidate delivers Intracellularly Three Distinct Active 5&#39;-Triphosphate Nucleosides That are Potent Inhibitors of HCV Polymerase.</a> Schinazi, R.F., F. Amblard, S.J. Tao, M. Ehteshami, S. Amiralaei, H. Li, J. Shelton, T. Whitaker, T.R. McBrayer, and S.J. Coats. Hepatology, 2014. 60: p. 1162A-1162A. ISI[000344483805050].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344471800046">Synthesis of 3&#39;,4&#39;-Difluoro-3&#39;-deoxyribonucleosides and Its Evaluation of the Biological Activities: Discovery of a Novel Type of anti-HCV Agent 3&#39;,4&#39;-Difluorocordycepin.</a> Shimada, H., K. Haraguchi, K. Hotta, T. Miyaike, Y. Kitagawa, H. Tanaka, R. Kaneda, H. Abe, S. Shuto, K. Mori, Y. Ueda, N. Kato, R. Snoeck, G. Andrei, and J. Balzarini. Bioorganic &amp; Medicinal Chemistry, 2014. 22(21): p. 6174-6182. ISI[000344471800046].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344931600011">New Lignans from the Roots of Didymocarpus hedyotideus and Their Antiviral Effect on Hepatitis B Virus.</a> Tang, W.J., X. Chen, and L.H. Yang. Latin American Journal of Pharmacy, 2014. 33(6): p. 948-953. ISI[000344931600011].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000344483805053">ACH-3422, A Novel HCV NS5B RNA Polymerase Nucleotide Inhibitor, Demonstrates Improved Potency over Sofosbuvir against HCV Genotype-3 Replicons in Vitro.</a> Zhao, Y.S., S. Podos, J.L. Fabrycki, D. Patel, W.G. Yang, G.W. Yang, J. Wiles, A. Phadke, and M.J. Huang. Hepatology, 2014. 60: p. 1163A-1163A. ISI[000344483805053].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1205-121814.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
