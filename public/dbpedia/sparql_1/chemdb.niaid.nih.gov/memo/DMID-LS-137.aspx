

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-137.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Cy05jIcrCaUjMTOS6ka/eXwyXNDUjTgDKZeFjGUFBBvVuPSwjQdgv8dsaCrazDsL4ZNSuXr6icbwrfWM3InetGywaKl66oJR7BshyAF1tNh+PpJOrnfXVbaeni0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="76576D62" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-137-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60020   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Novel human, feline, chicken and other animal interferons and their antiproliferative and antiviral uses</p>

    <p>          Pestka, Sidney and Krause, Christopher D</p>

    <p>          PATENT:  WO <b>2006099451</b>  ISSUE DATE:  20060921</p>

    <p>          APPLICATION: 2006  PP: 156pp.</p>

    <p>          ASSIGNEE:  (University of Medicine &amp; Dentistry of New Jersey, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60021   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          4-Aminoquinoline compounds for treating virus-related conditions</p>

    <p>          Olivo, Paul D, Buscher, Benjamin A, Dyall, Julie, Jocket-Balsarotti, Jennifer I, O&#39;Guin, Andrew K, Roth, Robert M, Zhou, Yi, Franklin, Gary W, and Starkey, Gale W</p>

    <p>          PATENT:  WO <b>2006121767</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2006  PP: 93pp.</p>

    <p>          ASSIGNEE:  (Apath, LLC USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60022   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Small molecule inhibitors against West Nile virus replication</p>

    <p>          Gu, Baohua, Block, Tim, and Cuconati, Andy</p>

    <p>          PATENT:  WO <b>2007005541</b>  ISSUE DATE:  20070111</p>

    <p>          APPLICATION: 2006  PP: 81pp.</p>

    <p>          ASSIGNEE:  (Philadelphia Health &amp; Education Corporation, d b a Drexel University College of Medicine USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     60023   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of 2,3,4,9-tetrahydro-1H-b-carbolines for treatment of dengue fever, yellow fever, West Nile virus, and hepatitis C virus infection</p>

    <p>          Gudmundsson, Kristjan</p>

    <p>          PATENT:  WO <b>2007002051</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 53pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     60024   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Yellow Fever: A Disease that Has Yet to be Conquered</p>

    <p>          Barrett Alan D T and Higgs Stephen</p>

    <p>          Annu Rev Entomol <b>2007</b>.  52: 209-29.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     60025   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Viral and cellular determinants involved in hepadnaviral entry</p>

    <p>          Glebe Dieter and Urban Stephan</p>

    <p>          World J Gastroenterol <b>2007</b>.  13(1): 22-38.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     60026   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p><b>          Inhibitors of Epstein-Barr virus nuclear antigen 1, screening methods, and therapeutic use</b> </p>

    <p>          Kieff, Elliott and Kang, Myung-Soo</p>

    <p>          PATENT:  WO <b>2007002587</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 66pp.</p>

    <p>          ASSIGNEE:  (The Brigham and Women&#39;s Hospital, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     60027   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Reactivation of Herpes Simplex Virus Type 1 and Varicella-Zoster Virus and Therapeutic Effects of Combination Therapy With Prednisolone and Valacyclovir in Patients With Bell&#39;s Palsy</p>

    <p>          Kawaguchi, Kazuhiro, Inamura, Hiroo, Abe, Yasuhiro, Koshu, Hidehiro, Takashita, Emi, Muraki, Yasushi, Matsuzaki, Yoko, Nishimura, Hidekazu, Ishikawa, Hitoshi, Fukao, Akira, Hongo, Seiji, and Aoyagi, Masaru</p>

    <p>          Laryngoscope <b>2007</b>.  117(1): 147-156</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     60028   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Lethal infection of K18-hACE2 mice infected with severe acute respiratory syndrome coronavirus</p>

    <p>          McCray, Paul B Jr, Pewe, Lecia, Wohlford-Lenane, Christine, Hickey, Melissa, Manzel, Lori, Shi, Lei, Netland, Jason, Jia, Hong Peng, Halabi, Carmen, Sigmund, Curt D, Meyerholz, David K, Kirby, Patricia, Look, Dwight C, and Perlman, Stanley</p>

    <p>          Journal of Virology <b>2007</b>.  81(2): 813-821</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60029   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Severe acute respiratory syndrome coronavirus infection of mice transgenic for the human Angiotensin-converting enzyme 2 virus receptor</p>

    <p>          Tseng Chien-Te K, Huang Cheng, Newman Patrick, Wang Nan, Narayanan Krishna, Watts Douglas M, Makino Shinji, Packard Michelle M, Zaki Sherif R, Chan Teh-Sheng, and Peters Clarence J</p>

    <p>          J Virol <b>2007</b>.  81(3): 1162-73.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   60030   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Naturally-occurring hepatitis C virus protease variants: implications for resistance to new antivirals</p>

    <p>          Lopez-Labradorl, FX, Moya, A, and Gonzalez-Candelas, F</p>

    <p>          JOURNAL OF CLINICAL VIROLOGY: J. Clin. Virol <b>2006</b>.  36: S35-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240804100140">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240804100140</a> </p><br />

    <p>12.   60031   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Low-toxicity and long-circulating analogs of human interferon-a modified with PEG for treating cancer and viral infection</p>

    <p>          Villarete, Lorelie H and Liu, Chih-Ping</p>

    <p>          PATENT:  WO <b>2007002233</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 72pp.</p>

    <p>          ASSIGNEE:  (Pepgen Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   60032   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Methods of use of non-ATP competitive tyrosine kinase inhibitors to treat pathogenic infection</p>

    <p>          Kalman, Daniel and Bornmann, William Gerard</p>

    <p>          PATENT:  WO <b>2007002441</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 85pp.</p>

    <p>          ASSIGNEE:  (Emory University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   60033   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Antiviral 2,5-disubstituted imidazo[4,5-c]pyridines: From anti-pestivirus to anti-hepatitis C virus activity</p>

    <p>          Puerstinger, Gerhard, Paeshuyse, Jan, De Clercq, Erik, and Neyts, Johan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(2): 390-393</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60034   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of quinolones as antivirals</p>

    <p>          Kumar, Dange Vijay and Rai, Roopa</p>

    <p>          PATENT:  WO <b>2007005779</b>  ISSUE DATE:  20070111</p>

    <p>          APPLICATION: 2006  PP: 50pp.</p>

    <p>          ASSIGNEE:  (Virobay, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   60035   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of prolyl peptides as HCV inhibitors</p>

    <p>          Graupe, Michael, Link, John O, and Venkataramani, Chandrasekar</p>

    <p>          PATENT:  WO <b>2007005838</b>  ISSUE DATE:  20070111</p>

    <p>          APPLICATION: 2006  PP: 87pp.</p>

    <p>          ASSIGNEE:  (Virobay, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60036   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of amino acid amides as non-nucleoside anti-hepacivirus agents</p>

    <p>          Boyd, Vincent A, Cameron, Dale R, Jia, Qi, Sgarbi, Paulo WM, and Wacowich-Sgarbi, Shirley A</p>

    <p>          PATENT:  WO <b>2007002639</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 303pp.</p>

    <p>          ASSIGNEE:  (Migenix Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60037   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of pyrazolopyridinecarboxamides as inhibitors of beta-adrenergic receptor kinase 1</p>

    <p>          Steinhagen, Henning, Huber, Jochen, Ritter, Kurt, Pirard, Bernard, Bjergarde, Kirsten, Patek, Marcel, Smrcina, Martin, and Wei, Linli</p>

    <p>          PATENT:  WO <b>2007000241</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 71pp.</p>

    <p>          ASSIGNEE:  (Sanofi-Aventis Deutschland G.m.b.H., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   60038   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Manufacture of anti-HCV drugs with Fusarium</p>

    <p>          Aoki, Masahiro, Nagahashi, Yoshie, Kato, Hideyuki, Ito, Tatsuya, Masubuchi, Miyako, and Okuda, Toru</p>

    <p>          PATENT:  WO <b>2007000994</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 81pp.</p>

    <p>          ASSIGNEE:  (Chugai Seiyaku Kabushiki Kaisha, Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   60039   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          In vitro infection of immortalized primary hepatocytes by HCV genotype 4a and inhibition of virus replication by cyclosporin</p>

    <p>          El-Farrash Mohamed A, Aly Hussein H, Watashi Koichi, Hijikata Makoto, Egawa Hiroto, and Shimotohno Kunitada</p>

    <p>          Microbiol Immunol <b>2007</b>.  51(1): 127-33.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60040   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          The role of peginterferon alpha-2a (40kD) plus ribavirin in the management of chronic hepatitis C mono-infection</p>

    <p>          Simpson, D and Curran, MP</p>

    <p>          DISEASE MANAGEMENT &amp; HEALTH OUTCOMES: Dis. Manag. Health Outcomes <b>2006</b>.  14(5): 303-320, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242720100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242720100005</a> </p><br />

    <p>22.   60041   DMID-LS-137; SCIFINDER-DMID-1/29/2007</p>

    <p class="memofmt1-2">          A cell-based, high-throughput screen for small molecule regulators of hepatitis C virus replication</p>

    <p>          Kim Sun Suk, Peng Lee F, Lin Wenyu, Choe Won-Hyeok, Sakamoto Naoya, Schreiber Stuart L, and Chung Raymond T</p>

    <p>          Gastroenterology <b>2007</b>.  132(1): 311-20.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   60042   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          HBV drug resistance</p>

    <p>          Locarnini, S</p>

    <p>          ANTIVIRAL THERAPY: Antivir. Ther <b>2006</b>.  11(5): P4-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700020</a> </p><br />

    <p>24.   60043   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Tenofovir disoproxil fumarate (TDF) and adefovir dipivoxil (ADV) are effective in chronic hepatitis B virus (HBV) infection in subjects who are co-infected with HIV: HBV and HIV drug resistance results of ACTG Protocol A5127</p>

    <p>          Johnson, VA, Hazelwood, JD, Andersen, J, Miller, AB, Liu, T, Alston-Smith, B, Brosgart, CL, Rooney, JF, Polsky, B, and Peters, MG</p>

    <p>          ANTIVIRAL THERAPY: Antivir. Ther <b>2006</b>.  11(5): S11-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700029</a> </p><br />

    <p>25.   60044   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Ribavirin treatment, but not interferon pharmacokinetics, is associated with suppression of viral rebounds during high dose interferon-alpha-2a therapy of hepatitis C infection</p>

    <p>          Frost, SDW, Ruffini, L, Richman, DD, and Lake-Bakaar, G</p>

    <p>          ANTIVIRAL THERAPY: Antivir. Ther <b>2006</b>.  11(5): S13-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700031</a> </p><br />

    <p>26.   60045   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Aberrant innate immune response in lethal infection of macaques with the 1918 influenza virus</p>

    <p>          Kobasa, D, Jones, SM, Shinya, K, Kash, JC, Copps, J, Ebihara, H, Hatta, Y, Kim, JH, Halfmann, P, Hatta, M, Feldmann, F, Alimonti, JB, Fernando, L, Li, Y, Katze, MG, Feldmann, H, and Kawaoka, Y</p>

    <p>          NATURE: Nature  <b>2007</b>.  445(7125): 319-323, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243504700046">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243504700046</a> </p><br />
    <br clear="all">

    <p>27.   60046   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Docking and binding mode analysis of aryl diketoacids (ADK) at the active site of HCV RNA-dependent RNA polymerase</p>

    <p>          Kim, J and Chong, Y</p>

    <p>          MOLECULAR SIMULATION: Mol. Simul <b>2006</b>.  32(14): 1131-1138, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243257900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243257900002</a> </p><br />

    <p>28.   60047   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Cytokine-activated natural killer cells exert direct killing of hepatoma cells harboring hepatitis C virus replicons</p>

    <p>          Larkin, J, Bost, A, Glass, JI, and Tan, SL</p>

    <p>          JOURNAL OF INTERFERON AND CYTOKINE RESEARCH: J. Interferon Cytokine Res <b>2006</b>.  26(12): 854-865, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243076600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243076600002</a> </p><br />

    <p>29.   60048   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Prediction of binding for a kind of non-peptic HCVNS3 serine protease inhibitors from plants by molecular docking and MM-PBSA method</p>

    <p>          Li, XD, Zhang, W, Qiao, XB, and Xu, XJ</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY: Bioorg. Med. Chem <b>2007</b>.  15(1): 220-226, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243087200019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243087200019</a> </p><br />

    <p>30.   60049   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Antiviral activity of proteasome inhibitors in herpes simplex virus-1 infection: role of nuclear factor-kappa B</p>

    <p>          La Frazia, S, Amici, C, and Sontoro, MG</p>

    <p>          ANTIVIRAL THERAPY: Antivir. Ther <b>2006</b>.  11(8): 995-1004, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243183100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243183100005</a> </p><br />

    <p>31.   60050   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Hepatitis C virus (HCV) NS5B nonnucleoside inhibitors specifically block single-stranded viral RNA synthesis catalyzed by HCV replication complexes in vitro</p>

    <p>          Yang, WG, Sun, YN, Phadke, A, Deshpande, M, and Huang, MJ</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY: Antimicrob. Agents Chemother <b>2007</b>.  51( 1): 338-342, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243214200044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243214200044</a> </p><br />

    <p>32.   60051   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Hydantoin derivatives of L- and D-amino acids: Synthesis and evaluation of their antiviral and antitumoral activity</p>

    <p>          Rajic, Z, Zorc, B, Raic-Malic, S, Ester, K, Kralj, M, Pavelic, K, Balzarini, J, De Clercq, E, and Mintas, M</p>

    <p>          MOLECULES: Molecules <b>2006</b>.  11(11): 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242971700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242971700002</a> </p><br />
    <br clear="all">

    <p>33.   60052   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          N-1-substituted thymine derivatives as mitochondrial thymidine kinase (TK-2) inhibitors</p>

    <p>          Hernandez, AI, Familiar, O, Negri, A, Rodriguez-Barrios, F, Gago, F, Karlsson, A, Camarasa, MJ, Balzarini, J, and Perez-Perez, MJ</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY: J. Med. Chem <b>2006</b>.  49(26): 7766-7773, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242974100022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242974100022</a> </p><br />

    <p>34.   60053   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Resveratrol inhibition of varicella-zoster virus replication in vitro</p>

    <p>          Docherty, JJ, Sweet, TJ, Bailey, E, Faith, SA, and Booth, T</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  72(3): 171-177, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242735800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242735800002</a> </p><br />

    <p>35.   60054   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Pharmacokinetics of anti-SARS-CoV agent niclosamide and its analogs in rats</p>

    <p>          Chang, YW, Yeh, TK, Lin, KT, Chen, WC, Yao, HT, Lan, SJ, Wu, YS, Hsieh, HP, Chen, CM, and Chen, CT</p>

    <p>          JOURNAL OF FOOD AND DRUG ANALYSIS: J. Food Drug Anal <b>2006</b>.  14(4): 329-333, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243186700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243186700003</a> </p><br />

    <p>36.   60055   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Indomethacin has a potent antiviral activity against SARS coronavirus</p>

    <p>          Amici, C, Di Caro, A, Ciucci, A, Chioppa, L, Castilletti, C, Martella, V, Decaro, N, Buonavoglia, C, Capobianchi, MR, and Santoro, MG</p>

    <p>          ANTIVIRAL THERAPY: Antivir. Ther <b>2006</b>.  11(8): 1021-1030, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243183100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243183100008</a> </p><br />

    <p>37.   60056   DMID-LS-137; WOS-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Inhibitory effect of gamma interferon on BK virus gene expression and replication</p>

    <p>          Abend, JR, Low, JA, and Imperiale, MJ</p>

    <p>          JOURNAL OF VIROLOGY: J. Virol <b>2007</b>.  81(1): 272-279, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242958600025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242958600025</a> </p><br />

    <p>38.   60057   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          In vitro activity evaluation of Parkia pendula seed lectin against human cytomegalovirus and herpes virus 6</p>

    <p>          Favacho, AR, Cintra, EA, Coelho, LC, and Linhares, MI</p>

    <p>          Biologicals <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17254798&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17254798&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>39.   60058   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Antiviral Resistance and the Control of Pandemic Influenza</p>

    <p>          Lipsitch, M, Cohen, T, Murray, M, and Levin, BR</p>

    <p>          PLoS Med <b>2007</b>.  4(1): e15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253900&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253900&amp;dopt=abstract</a> </p><br />

    <p>40.   60059   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Bicyclol for chronic hepatitis C</p>

    <p>          Yang, X, Zhuo, Q, Wu, T, and Liu, G</p>

    <p>          Cochrane Database Syst Rev <b>2007</b>.(1): CD004994</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253534&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253534&amp;dopt=abstract</a> </p><br />

    <p>41.   60060   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Neuraminidase inhibitors for preventing and treating influenza in children</p>

    <p>          Matheson, Nj, Harnden, A, Perera, R, Sheikh, A, and Symmonds-Abrahams, M</p>

    <p>          Cochrane Database Syst Rev <b>2007</b>.(1): CD002744</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253479&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253479&amp;dopt=abstract</a> </p><br />

    <p>42.   60061   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Design, synthesis, and preliminary in vitro and in silico antiviral activity of [4,7]phenantrolines and 1-oxo-1,4-dihydro-[4,7]phenantrolines against single-stranded positive-sense RNA genome viruses</p>

    <p>          Carta, A, Loriga, M, Paglietti, G, Ferrone, M, Fermeglia, M, Pricl, S, Sanna, T, Ibba, C, La, Colla P, and Loddo, R</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251029&amp;dopt=abstract</a> </p><br />

    <p>43.   60062   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Five years of progress on cyclin-dependent kinases and other cellular proteins as potential targets for antiviral drugs</p>

    <p>          Schang, LM, St, Vincent MR, and Lacasse, JJ</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(6): 293-320</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17249245&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17249245&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>44.   60063   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus replication by shRNAs in stably HBV expressed HEPG2 2.2.15 cell lines</p>

    <p>          Kayhan, H, Karatayli, E, Turkyilmaz, AR, Sahin, F, Yurdaydin, C, and Bozdayi, AM</p>

    <p>          Arch Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17245534&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17245534&amp;dopt=abstract</a> </p><br />

    <p>45.   60064   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          West Nile virus</p>

    <p>          Kramer, LD, Li, J, and Shi, PY</p>

    <p>          Lancet Neurol <b>2007</b>.  6(2): 171-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17239804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17239804&amp;dopt=abstract</a> </p><br />

    <p>46.   60065   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Antiviral activity of berberine and related compounds against human cytomegalovirus</p>

    <p>          Hayashi, K, Minoda, K, Nagaoka, Y, Hayashi, T, and Uesato, S</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17239594&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17239594&amp;dopt=abstract</a> </p><br />

    <p>47.   60066   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          Pegylation of IFN-alpha and antiviral activity</p>

    <p>          Boulestin, A, Kamar, N, Sandres-Saune, K, Alric, L, Vinel, JP, Rostaing, L, and Izopet, J</p>

    <p>          J Interferon Cytokine Res <b>2006</b>.  26(12): 849-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17238827&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17238827&amp;dopt=abstract</a> </p><br />

    <p>48.   60067   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          From actually toxic to highly specific--novel drugs against poxviruses</p>

    <p>          Sliva, K and Schnierle, B</p>

    <p>          Virol J <b>2007</b>.  4: 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17224068&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17224068&amp;dopt=abstract</a> </p><br />

    <p>49.   60068   DMID-LS-137; PUBMED-DMID-1/29/2007</p>

    <p class="memofmt1-2">          N-Long-chain Monoacylated Derivatives of 2,6-Diaminopyridine with Antiviral Activity</p>

    <p>          Mibu, N, Yokomizo, K, Kashige, N, Miake, F, Miyata, T, Uyeda, M, and Sumoto, K</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2007</b>.  55(1): 111-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17202712&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17202712&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
