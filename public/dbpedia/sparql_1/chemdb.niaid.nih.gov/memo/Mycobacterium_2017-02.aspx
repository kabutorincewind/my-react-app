

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3/T0wkDoNYUCfN/vWy8oPfGzXLfxrGvEZDd5QGu7rVfivm8UCPMM9PP4FID54KPNpMuistMxRh1xKBrZo9H5k+LtSGjSL0iIdHO7N8LLuDupexhgxn8iAJ7mW1g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="87DD28EF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: February, 2017</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28028696">Synthesis and Anti-mycobacterial Activity of 4-(4-Phenyl-1H-1,2,3-triazol-1-yl)salicylhydrazones: Revitalizing an Old Drug.</a> Abdu-Allah, H.H., B.G. Youssif, M.H. Abdelrahman, M.K. Abdel-Hamid, R.S. Reshma, P. Yogeeswari, T. Aboul-Fadl, and D. Sriram. Archives of Pharmacal Research, 2017. 40(2): p. 168-179. PMID[28028696]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000393044600013">5-(4-Alkyl-1,2,3-triazol-1-yl)methyl Derivatives of 2&#39;-Deoxyuridine as Inhibitors of Viral and Bacterial Growth.</a> Alexandrova, L.A., O.V. Efremenkova, V.L. Andronova, G.A. Galegov, P.N. Solyev, I.L. Karpenko, and S.N. Kochetkov. Russian Journal of Bioorganic Chemistry, 2016. 42(6): p. 677-684. ISI[000393044600013]. <b><br />
    [WOS]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000391451500040">Identification and Development of Pyrazolo[4,3-c]pyridine carboxamides as Mycobacterium tuberculosis Pantothenate Synthetase Inhibitors.</a> Amaroju, S., M.N. Kalaga, S. Srinivasarao, A. Napiorkowska, E. Augustynowicz-Kopec, S. Murugesan, S. Chander, R. Krishnan, and K. Sekhar. New Journal of Chemistry, 2017. 41(1): p. 347-357. ISI[000391451500040]. <b><br />
    [WOS]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28117200">Synthesis, Antimycobacterial and Cytotoxic Activity of alpha,beta-Unsaturated amides and 2,4-Disubstituted oxazoline Derivatives.</a> Avalos-Alanis, F.G., E. Hernandez-Fernandez, P. Carranza-Rosales, S. Lopez-Cortina, J. Hernandez-Fernandez, M. Ordonez, N.E. Guzman-Delgado, A. Morales-Vargas, V.M. Velazquez-Moreno, and M.G. Santiago-Mauricio. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(4): p. 821-825. PMID[28117200]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27914801">New Bithiazolyl hydrazones: Novel Synthesis, Characterization and Antitubercular Evaluation.</a> Bhalerao, M.B., S.T. Dhumal, A.R. Deshmukh, L.U. Nawale, V. Khedkar, D. Sarkar, and R.A. Mane. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(2): p. 288-294. PMID[27914801]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28148252">Potential of Zanthoxylum leprieurii as a Source of Active Compounds against Drug Resistant Mycobacterium tuberculosis.</a> Bunalema, L., G.W. Fotso, P. Waako, J. Tabuti, and S.O. Yeboah. BMC  Complementary and Alternative Medicine. 17(89): 6pp. PMID[28148252]. PMCID[PMC5289037].<b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28118738">Design of Potent Fluoro-1 Substituted Chalcones as Antimicrobial Agents.</a> Burmaoglu, S., O. Algul, A. Gobek, D.A. Anil, M. Ulger, B.G. Erturk, E. Kaplan, A. Dogen, and G. Aslan. Journal of Enzyme Inhibition and Medicinal Chemistry, 2017. 32(1): p. 490-495. PMID[28118738]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000391709300004">Novel Microwave Assisted Synthesis and Antimicrobial Activity of New Quinolone-hybrids.</a> Ceylan, S., H. Bayrak, S.B. Ozdemir, Y. Uygun, A. Mermer, N. Demirbas, and S. Ulker. Letters in Organic Chemistry, 2016. 13(9): p. 636-651. ISI[000391709300004]. <b><br />
    [WOS]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27754712">Antibacterial Properties of Endophytic Bacteria Isolated from a Fern Species Equisetum arvense L. against Foodborne Pathogenic Bacteria Staphylococcus aureus and Escherichia coli O157:H7.</a> Das, G., J.K. Patra, and K.H. Baek. Foodborne Pathogens and Disease, 2017. 14(1): p. 50-58. PMID[27754712]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28243274">Novel Thiazolidinone-azole Hybrids: Design, Synthesis and Antimycobacterial Activity Studies.</a> Eroglu, B., K. Ozadali-Sari, O. Unsal-Tan, S. Dharmarajan, P. Yogeeswari, and A. Balkan. Iranian Journal of Pharmaceutical Research, 2016. 15(4): p. 783-790. PMID[28243274]. PMCID[PMC5316256].<b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28243275">Design, Synthesis and Anti-tubercular Activity of Novel 1, 4-Dihydropyrine-3, 5-dicarboxamide Containing 4(5)-Chloro-2-ethyl-5(4)-imidazolyl Moiety.</a> Iman, M., A. Davood, M. Lotfinia, G. Dehqani, S. Sardari, P. Azerang, and M. Amini. Iranian Journal of Pharmaceutical Research, 2016. 15(4): p. 791-799. PMID[28243275]. PMCID[PMC5316257].<b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28157178">Synthesis of Novel Pyrazinamide Derivatives Based on 3-Chloropyrazine-2-carboxamide and Their Antimicrobial Evaluation.</a> Jandourek, O., M. Tauchman, P. Paterova, K. Konecna, L. Navratilova, V. Kubicek, O. Holas, J. Zitko, and M. Dolezal. Molecules, 2017. 22(E223): 20pp. PMID[28157178]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28228097">Alkaloid Extracts from Combretum zeyheri inhibit the Growth of Mycobacterium smegmatis.</a> Nyambuya, T., R. Mautsa, and S. Mukanganyama BMC Complementary and Alternative Medicine, 2017. 17(124): 11pp. PMID[28228097]. PMCID[PMC5322641].<b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28126437">Novel Salicylanilides from 4,5-Dihalogenated Salicylic acids: Synthesis, Antimicrobial Activity and Cytotoxicity.</a> Paraskevopoulos, G., S. Monteiro, R. Vosatka, M. Kratky, L. Navratilova, F. Trejtnar, J. Stolarikova, and J. Vinsova. Bioorganic &amp; Medicinal Chemistry, 2017. 25(4): p. 1524-1532. PMID[28126437]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000392819300003">Uncatalyzed Four-component Synthesis of Pyrazolopyranopyrimidine Derivatives and Their Antituberculosis Activities.</a> Patil, K.T., D.K. Jamale, N.J. Valekar, P.T. Patil, P.P. Warekar, G.B. Kolekar, and P.V. Anbhule. Synthetic Communications, 2017. 47(2): p. 111-120. ISI[000392819300003]. <b><br />
    [WOS]</b>. TB_02_2017.</p>
   
    <br />    

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28103537">Hybrids of Thienopyrimidinones and Thiouracils as Anti-tubercular Agents: SAR and Docking Studies.</a> Pisal, M.M., L.U. Nawale, M.D. Patil, S.G. Bhansali, J.M. Gajbhiye, D. Sarkar, S.P. Chavan, and H.B. Borate. European Journal of Medicinal Chemistry, 2017. 127: p. 459-469. PMID[28103537]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000392643400012">Antimicrobial Activity and Analyses of Six Geranium L. Species with Headspace SPME and Hydrodistillation.</a> Renda, G., G. Celik, B. Korkmaz, S.A. Karaoglu, and N. Yayli. Journal of Essential Oil Bearing Plants, 2016. 19(8): p. 2003-2016. ISI[000392643400012]. <b><br />
    [WOS]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28117201">Development of Gallic acid Formazans as Novel Enoyl acyl Carrier Protein Reductase Inhibitors for the Treatment of Tuberculosis.</a> Saharan, V.D. and S.S. Mahajan. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(4): p. 808-815. PMID[28117201]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28039773">Synthesis and SAR Evaluation of Novel Thioridazine Derivatives Active against Drug-resistant Tuberculosis.</a> Scalacci, N., A.K. Brown, F.R. Pavan, C.M. Ribeiro, F. Manetti, S. Bhakta, A. Maitra, D.L. Smith, E. Petricci, and D. Castagnolo. European Journal of Medicinal Chemistry, 2017. 127: p. 147-158. PMID[28039773]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28108242">Evaluation of the Antimicrobial Activity of the Mastoparan Polybia-MPII Isolated from Venom of the Social Wasp Pseudopolybia vespiceps testacea (Vespidae, Hymenoptera).</a> Silva, J.C., L.M. Neto, R.C. Neves, J.C. Goncalves, M.M. Trentini, R. Mucury-Filho, K.S. Smidt, I.C. Fensterseifer, O.N. Silva, L.D. Lima, P.B. Clissa, N. Vilela, F. Guilhelmelli, L.P. Silva, M. Rangel, A. Kipnis, I. Silva-Pereira, O.L. Franco, A.P. Junqueira-Kipnis, A.L. Bocca, and M.R. Mortari. International Journal of Antimicrobial Agents, 2017. 49(2): p. 167-175. PMID[28108242]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28110868">Quinolidene Based Monocarbonyl curcumin Analogues as Promising Antimycobacterial Agents: Synthesis and Molecular Docking Study.</a> Subhedar, D.D., M.H. Shaikh, L. Nawale, D. Sarkar, V.M. Khedkar, and B.B. Shingate. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(4): p. 922-928. PMID[28110868]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28196957">A Novel Small-molecule Inhibitor of the Mycobacterium tuberculosis Demethylmenaquinone methyltransferase MenG Is Bactericidal to Both Growing and Nutritionally Deprived Persister Cells.</a> Sukheja, P., P. Kumar, N. Mittal, S.G. Li, E. Singleton, R. Russo, A.L. Perryman, R. Shrestha, D. Awasthi, S. Husain, P. Soteropoulos, R. Brukh, N. Connell, J.S. Freundlich, and D. Alland. mBio, 2017. 8(1): e02022-16. PMID[28196957]. PMCID[PMC5312080].<b><br />
    [PubMed]</b>. TB_02_2017.</p>
   
    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28075132">Discovery of Imidazo[1,2-a]pyridine ethers and Squaramides as Selective and Potent Inhibitors of Mycobacterial Adenosine Triphosphate (ATP) Synthesis.</a> Tantry, S.J., S.D. Markad, V. Shinde, J. Bhat, G. Balakrishnan, A.K. Gupta, A. Ambady, A. Raichurkar, C. Kedari, S. Sharma, N.V. Mudugal, A. Narayan, C.N. Naveen Kumar, R. Nanduri, S. Bharath, J. Reddy, V. Panduga, K.R. Prabhakar, K. Kandaswamy, R. Saralaya, P. Kaur, N. Dinesh, S. Guptha, K. Rich, D. Murray, H. Plant, M. Preston, H. Ashton, D. Plant, J. Walsh, P. Alcock, K. Naylor, M. Collier, J. Whiteaker, R.E. McLaughlin, M. Mallya, M. Panda, S. Rudrapatna, V. Ramachandran, R. Shandil, V.K. Sambandamurthy, K. Mdluli, C.B. Cooper, H. Rubin, T. Yano, P. Iyer, S. Narayanan, S. Kavanagh, K. Mukherjee, V. Balasubramanian, V.P. Hosagrahara, S. Solapure, S. Ravishankar, and P.S. Hameed. Journal of Medicinal Chemistry, 2017. 60(4): p. 1379-1399. PMID[28075132]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28065567">Synthesis, Identification and in Vitro Biological Evaluation of Some Novel Quinoline Incorporated 1,3-Thiazinan-4-one Derivatives.</a> Umamatheswari, S. and C. Sankar. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(3): p. 695-699. PMID[28065567]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28063354">Diaryltriazenes as Antibacterial Agents against Methicillin Resistant Staphylococcus aureus (MRSA) and Mycobacterium smegmatis.</a> Vajs, J., C. Proud, A. Brozovic, M. Gazvoda, A. Lloyd, D.I. Roper, M. Osmak, J. Kosmrlj, and C.G. Dowson. European Journal of Medicinal Chemistry, 2017. 127: p. 223-234. PMID[28063354]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28057421">Preparation and Biological Evaluation of Ethionamide-mesoporous Silicon Nanoparticles against Mycobacterium tuberculosis.</a> Vale, N., A. Correia, S. Silva, P. Figueiredo, E. Makila, J. Salonen, J. Hirvonen, J. Pedrosa, H.A. Santos, and A. Fraga. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(3): p. 403-405. PMID[28057421]. <b><br />
    [PubMed]</b>. TB_02_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017027768A1&amp;KC=A1">Compositions and Methods for Treating Tuberculosis.</a> Serrano-Wu, M.H. and C. Fang. Patent. 2017. 2016-US46676 2017027768: 103pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_02_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
