

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-346.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Uq7ixWjzvl9LM2DJocuYhLwyznl+njNBcgzjM3TKDkXR42bwfb3OGybbUSc0P5206ShjVZR37h5sENG2TpWDVBKYqii8KwXNIi7Sem7kpx9ZAb+ph59pv+ECaNc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AF3771AF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-346-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48722   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          Ileabethoxazole: a novel benzoxazole alkaloid with antimycobacterial activity</p>

    <p>          Rodriguez, Ileana I, Rodriguez, Abimael D, Wang, Yuehong, and Franzblau, Scott G</p>

    <p>          Tetrahedron Letters <b>2006</b>.  47(19): 3229-3232</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THS-4JJGB0S-B/2/736d60a19fe2382b77e00eafa6c30c04">http://www.sciencedirect.com/science/article/B6THS-4JJGB0S-B/2/736d60a19fe2382b77e00eafa6c30c04</a> </p><br />

    <p>2.     48723   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Diverse effects of cyclosporine on hepatitis C virus strain replication</p>

    <p>          Ishii, N, Watashi, K, Hishiki, T, Goto, K, Inoue, D, Hijikata, M, Wakita, T, Kato, N, and Shimotohno, K</p>

    <p>          J Virol <b>2006</b>.  80(9): 4510-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611911&amp;dopt=abstract</a> </p><br />

    <p>3.     48724   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Synthesis and Anti-BVDV Activity of Acridones As New Potential Antiviral Agents(1)</p>

    <p>          Tabarrini, O, Manfroni, G, Fravolini, A, Cecchetti, V, Sabatini, S, De, Clercq E, Rozenski, J, Canard, B, Dutartre, H, Paeshuyse, J, and Neyts, J</p>

    <p>          J Med Chem <b>2006</b>.  49(8): 2621-2627</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610805&amp;dopt=abstract</a> </p><br />

    <p>4.     48725   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Conserved fungal genes as potential targets for broad-spectrum antifungal drug discovery</p>

    <p>          Liu, M, Healy, MD, Dougherty, BA, Esposito, KM, Maurice, TC, Mazzucco, CE, Bruccoleri, RE, Davison, DB, Frosco, M, Barrett, JF, and Wang, YK</p>

    <p>          Eukaryot Cell <b>2006</b>.  5(4): 638-49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16607011&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16607011&amp;dopt=abstract</a> </p><br />

    <p>5.     48726   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          1.6 A Crystal Structure of the Secreted Chorismate Mutase from Mycobacterium tuberculosis: Novel Fold Topology Revealed</p>

    <p>          Okvist, Mats, Dey, Raja, Sasso, Severin, Grahn, Elin, Kast, Peter, and Krengel, Ute</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  357(5): 1483-1499</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4J6NGM0-4/2/ce1ad9c82fe8f0d7c2bbbb8b22c7434c">http://www.sciencedirect.com/science/article/B6WK7-4J6NGM0-4/2/ce1ad9c82fe8f0d7c2bbbb8b22c7434c</a> </p><br />
    <br clear="all">

    <p>6.     48727   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          Novel 24-nor-, 24-nor-2,3-seco-, and 3,24-dinor-2,4-seco-ursane triterpenes from Diospyros decandra: evidences for ring A biosynthetic transformations</p>

    <p>          Nareeboon, Parichat, Kraus, Wolfgang, Beifuss, Uwe, Conrad, Juergen, Klaiber, Iris, and Sutthivaiyakit, Somyote</p>

    <p>          Tetrahedron <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4JPHS81-B/2/5611b52e4a033eca655ed40176055fa8">http://www.sciencedirect.com/science/article/B6THR-4JPHS81-B/2/5611b52e4a033eca655ed40176055fa8</a> </p><br />

    <p>7.     48728   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Ceanothane- and Lupane-Type Triterpenes with Antiplasmodial and Antimycobacterial Activities from Ziziphus cambodiana</p>

    <p>          Suksamrarn, S, Panseeta, P, Kunchanawatta, S, Distaporn, T, Ruktasing, S, and Suksamrarn, A</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2006</b>.  54(4): 535-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16595959&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16595959&amp;dopt=abstract</a> </p><br />

    <p>8.     48729   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Identification of a novel arabinofuranosyl transferase (AftA) involved in cell wall arabinan biosynthesis in mycobacterium tuberculosis</p>

    <p>          Alderwick, LJ, Seidel, M, Sahm, H, Besra, GS, and Eggeling, L</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16595677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16595677&amp;dopt=abstract</a> </p><br />

    <p>9.     48730   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          In Vivo Efficacy of Phage Therapy for Mycobacterium avium Infection As Delivered by a Nonvirulent Mycobacterium</p>

    <p>          Danelishvili, L, Young, LS, and Bermudez, LE</p>

    <p>          Microb Drug Resist <b>2006</b>.  12(1): 1-6</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16584300&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16584300&amp;dopt=abstract</a> </p><br />

    <p>10.   48731   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          The preponderance of P450s in the Mycobacterium tuberculosis genome</p>

    <p>          McLean, KJ, Clift, D, Lewis, DG, Sabri, M, Balding, PR, Sutcliffe, MJ, Leys, D, and Munro, AW</p>

    <p>          Trends Microbiol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16581251&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16581251&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48732   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Synthesis and preliminary screening of carbohydrazides and hydrazones of pyrrole derivatives as potential tuberculostatics</p>

    <p>          Bijev, A</p>

    <p>          Arzneimittelforschung <b>2006</b>.  56(2): 96-103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16572924&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16572924&amp;dopt=abstract</a> </p><br />

    <p>12.   48733   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Time course of microbiologic outcome and gene expression in Candida albicans during and following in vitro and in vivo exposure to fluconazole</p>

    <p>          Lepak, A, Nett, J, Lincoln, L, Marchillo, K, and Andes, D</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1311-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569846&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569846&amp;dopt=abstract</a> </p><br />

    <p>13.   48734   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          Catalase-peroxidase of Mycobacterium bovis BCG converts isoniazid to isonicotinamide, but not to isonicotinic acid: Differentiation parameter between enzymes of Mycobacterium bovis BCG and Mycobacterium tuberculosis</p>

    <p>          Kang, Sung-Koo, Lee, Jong-Ho, Lee, Young-Choon, and Kim, Cheorl-Ho</p>

    <p>          Biochimica et Biophysica Acta (BBA) - General Subjects <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1W-4J9MRFR-1/2/64d07b8449950bfeecd5f2aa85bd9c7b">http://www.sciencedirect.com/science/article/B6T1W-4J9MRFR-1/2/64d07b8449950bfeecd5f2aa85bd9c7b</a> </p><br />

    <p>14.   48735   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus RNA replication by short hairpin RNA synthesized by T7 RNA polymerase in hepatitis C virus subgenomic replicons</p>

    <p>          Hamazaki, H, Ujino, S, Miyano-Kurosaki, N, Shimotohno, K, and Takaku, H</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  343(3): 988-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566896&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566896&amp;dopt=abstract</a> </p><br />

    <p>15.   48736   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          Antifungal activity of synthetic peptide derived from halocidin, antimicrobial peptide from the tunicate, Halocynthia aurantium</p>

    <p>          Jang, Woong Sik, Kim, Hong Ki, Lee, Ki Young, Kim, Sun Am, Han, Yeon Soo, and Lee, In Hee</p>

    <p>          FEBS Letters <b>2006</b>.  580(5): 1490-1496</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4J3NX0N-3/2/6d0d49ea638881123826b5c377af9eb4">http://www.sciencedirect.com/science/article/B6T36-4J3NX0N-3/2/6d0d49ea638881123826b5c377af9eb4</a> </p><br />
    <br clear="all">

    <p>16.   48737   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of antifungal activity of naphthoquinone derivatives</p>

    <p>          Errante, G, La, Motta G, Lagana, C, Wittebolle, V, Sarciron, ME, and Barret, R</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16563569&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16563569&amp;dopt=abstract</a> </p><br />

    <p>17.   48738   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          The non-immunosuppressive cyclosporin DEBIO-025 is a potent inhibitor of hepatitis C virus replication in vitro</p>

    <p>          Paeshuyse, J, Kaul, A, De, Clercq E, Rosenwirth, B, Dumont, JM, Scalfaro, P, Bartenschlager, R, and Neyts, J</p>

    <p>          Hepatology <b>2006</b>.  43(4): 761-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16557546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16557546&amp;dopt=abstract</a> </p><br />

    <p>18.   48739   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Emergence of Mycobacterium tuberculosis with extensive resistance to second-line drugs--worldwide, 2000-2004</p>

    <p>          Anon</p>

    <p>          MMWR Morb Mortal Wkly Rep <b>2006</b>.  55(11): 301-305</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16557213&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16557213&amp;dopt=abstract</a> </p><br />

    <p>19.   48740   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Gatifloxacin derivatives: Synthesis, antimycobacterial activities, and inhibition of Mycobacterium tuberculosis DNA gyrase</p>

    <p>          Sriram, D, Aubry, A, Yogeeswari, P, and Fisher, LM</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16554151&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16554151&amp;dopt=abstract</a> </p><br />

    <p>20.   48741   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          5&#39;-Adenosinephosphosulphate reductase (CysH) protects Mycobacterium tuberculosis against free radicals during chronic infection phase in mice</p>

    <p>          Senaratne, RH, De, Silva AD, Williams, SJ, Mougous, JD, Reader, JR, Zhang, T, Chan, S, Sidders, B, Lee, DH, Chan, J, Bertozzi, CR, and Riley, LW</p>

    <p>          Mol Microbiol <b>2006</b>.  59(6): 1744-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16553880&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16553880&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   48742   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          Passive serum therapy with polyclonal antibodies against Mycobacterium tuberculosis protects against post-chemotherapy relapse of tuberculosis infection in SCID mice</p>

    <p>          Guirado, Evelyn, Amat, Isabel, Gil, Olga, Diaz, Jorge, Arcos, Virginia, Caceres, Neus, Ausina, Vicenc, and Cardona, Pere-Joan</p>

    <p>          Microbes and Infection <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4J4HJ7C-2/2/6e84fb27d53596247cfdf0f411b4535d">http://www.sciencedirect.com/science/article/B6VPN-4J4HJ7C-2/2/6e84fb27d53596247cfdf0f411b4535d</a> </p><br />

    <p>22.   48743   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Call for new hepatitis C strategy</p>

    <p>          Garmaise, D</p>

    <p>          HIV AIDS Policy Law Rev <b>2005</b>.  10(3): 19-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16548071&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16548071&amp;dopt=abstract</a> </p><br />

    <p>23.   48744   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          RshA mimetic peptides inhibiting the transcription driven by a Mycobacterium tuberculosis sigma factor SigH</p>

    <p>          Jeong, Eun Hee, Son, Young Mi, Hah, Young-Sool, Choi, Yeon Jin, Lee, Kon Ho, Song, Taeksun, and Kim, Deok Ryong</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  339(1): 392-398</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4HJRXDY-6/2/77da5b379dead695802d9100bdf5c4ae">http://www.sciencedirect.com/science/article/B6WBK-4HJRXDY-6/2/77da5b379dead695802d9100bdf5c4ae</a> </p><br />

    <p>24.   48745   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Efficacy of mangiferin against Cryptosporidium parvum in a neonatal mouse model</p>

    <p>          Perrucci, S, Fichi, G, Buggiani, C, Rossi, G, and Flamini, G</p>

    <p>          Parasitol Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>          <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16547730&amp;dopt=abstract</span></p>

    <p>25.   48746   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Priorities in tuberculosis research</p>

    <p>          Onyebujoh, P, Rodriguez, W, and Mwaba, P</p>

    <p>          Lancet <b>2006</b>.  367(9514): 940-2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16546543&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16546543&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>26.   48747   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Turning the tide against tuberculosis</p>

    <p>          Zumla, A and Mullan, Z</p>

    <p>          Lancet <b>2006</b>.  367(9514): 877-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16546520&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16546520&amp;dopt=abstract</a> </p><br />

    <p>27.   48748   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis inhibition of phagolysosome biogenesis and autophagy as a host defence mechanism</p>

    <p>          Deretic, V, Singh, S, Master, S, Harris, J, Roberts, E, Kyei, G, Davis, A, de, Haro S, Naylor, J, Lee, HH, and Vergne, I</p>

    <p>          Cell Microbiol  <b>2006</b>.  8(5): 719-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611222&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611222&amp;dopt=abstract</a> </p><br />

    <p>28.   48749   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          Evaluation of the anti-hepatitis C virus effects of cyclophilin inhibitors, cyclosporin A, and NIM811</p>

    <p>          Goto, Kaku, Watashi, Koichi, Murata, Takayuki, Hishiki, Takayuki, Hijikata, Makoto, and Shimotohno, Kunitada</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  343(3): 879-884</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4JHMH33-B/2/d774f28f8abcdc6b37a89e997c4e6ca8">http://www.sciencedirect.com/science/article/B6WBK-4JHMH33-B/2/d774f28f8abcdc6b37a89e997c4e6ca8</a> </p><br />

    <p>29.   48750   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p class="memofmt1-2">          Antiviral therapy targeting viral polymerase</p>

    <p>          Tsai, CH, Lee, PY, Stollar, V, and Li, ML</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(11): 1339-55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611119&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611119&amp;dopt=abstract</a> </p><br />

    <p>30.   48751   OI-LS-346; PUBMED-OI-4/17/2006</p>

    <p><b>          Recent advances in the medical and surgical treatment of multi-drug resistant tuberculosis</b> </p>

    <p>          Lalloo, UG, Naidoo, R, and Ambaram, A</p>

    <p>          Curr Opin Pulm Med <b>2006</b>.  12(3): 179-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16582672&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16582672&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>31.   48752   OI-LS-346; EMBASE-OI-4/17/2006</p>

    <p class="memofmt1-2">          Novel approaches for therapy of chronic hepatitis C</p>

    <p>          Stauber, Rudolf E and Stadlbauer, Vanessa</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4JHMY9N-1/2/96e02f063d7ed33f6b6b11234026beb2">http://www.sciencedirect.com/science/article/B6VJV-4JHMY9N-1/2/96e02f063d7ed33f6b6b11234026beb2</a> </p><br />

    <p>32.   48753   OI-LS-346; WOS-OI-4/9/2006</p>

    <p class="memofmt1-2">          Antiviral compounds and one new iridoid glycoside from Cornus officinalis</p>

    <p>          Wang, Y, Li, ZQ, Chen, LR, and Xu, XJ</p>

    <p>          PROGRESS IN NATURAL SCIENCE <b>2006</b>.  16(2): 142-146, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236287000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236287000005</a> </p><br />

    <p>33.   48754   OI-LS-346; WOS-OI-4/9/2006</p>

    <p class="memofmt1-2">          Antibacterial drug discovery - Then, now and the genomics future</p>

    <p>          Monaghan, RL and Barrett, JF</p>

    <p>          BIOCHEMICAL PHARMACOLOGY <b>2006</b>.  71(7): 901-909, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236228000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236228000003</a> </p><br />

    <p>34.   48755   OI-LS-346; WOS-OI-4/9/2006</p>

    <p class="memofmt1-2">          Dihydrofolate reductase inhibitors as antibacterial agents</p>

    <p>          Hawser, S, Lociuro, S, and Islam, K</p>

    <p>          BIOCHEMICAL PHARMACOLOGY <b>2006</b>.  71(7): 941-948, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236228000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236228000007</a> </p><br />

    <p>35.   48756   OI-LS-346; WOS-OI-4/9/2006</p>

    <p class="memofmt1-2">          Use of genomics to select antibacterial targets</p>

    <p>          Pucci, MJ</p>

    <p>          BIOCHEMICAL PHARMACOLOGY <b>2006</b>.  71(7): 1066-1072, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236228000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236228000021</a> </p><br />

    <p>36.   48757   OI-LS-346; WOS-OI-4/16/2006</p>

    <p class="memofmt1-2">          Stereoselective alpha-C-substituted synthesis of 1,4-dideoxy-1 4-imino-D-galactitols. Toward original UDP-Galf mimics via cross-metathesis</p>

    <p>          Liautard, V, Desvergnes, V, and Martin, OR</p>

    <p>          ORGANIC LETTERS <b>2006</b>.  8(7): 1299-1302, 4</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236397000012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236397000012</a> </p><br />

    <p>37.   48758   OI-LS-346; WOS-OI-4/16/2006</p>

    <p class="memofmt1-2">          In vitro properties of antimicrobial bromotyrosine alkaloids</p>

    <p>          Pick, N, Rawat, M, Arad, D, Lan, J, Fan, JF, Kende, AS, and Av-Gay, Y</p>

    <p>          JOURNAL OF MEDICAL MICROBIOLOGY <b>2006</b>.  55(4): 407-415, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236489800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236489800008</a> </p><br />
    <br clear="all">

    <p>38.   48759   OI-LS-346; WOS-OI-4/16/2006</p>

    <p class="memofmt1-2">          Antiviral effect of alpha-glucosidase inhibitors on viral morphogenesis and binding properties of hepatitis C virus-like particles</p>

    <p>          Chapel, C, Garcia, C, Roingeard, P, Zitzmann, N, Dubuisson, J, Dwek, RA, Trepo, C, Zoulim, F, and Durantel, D</p>

    <p>          JOURNAL OF GENERAL VIROLOGY <b>2006</b>.  87: 861-871, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236446400016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236446400016</a> </p><br />

    <p>39.   48760   OI-LS-346; WOS-OI-4/16/2006</p>

    <p class="memofmt1-2">          Dual inhibition of mycobacterial fatty acid biosynthesis and degradation by 2-alkynoic acids</p>

    <p>          Morbidoni, HR, Vilcheze, C, Kremer, L, Bittman, R, Sacchettini, JC, and Jacobs, WR</p>

    <p>          CHEMISTRY &amp; BIOLOGY <b>2006</b>.  13(3): 297-307, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236482100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236482100010</a> </p><br />

    <p>40.   48761   OI-LS-346; WOS-OI-4/16/2006</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of 6-hydroxycinnolines</p>

    <p>          Ryu, CK and Lee, JY</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(7): 1850-1853, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236402500017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236402500017</a> </p><br />

    <p>41.   48762   OI-LS-346; WOS-OI-4/16/2006</p>

    <p class="memofmt1-2">          Crystallization and preliminary X-ray diffraction analysis of prephenate dehydratase from Mycobacterium tuberculosis H37Rv</p>

    <p>          Vivan, AL, Dias, MVB, Schneider, CZ, de, Azevedo WF, Basso, LA, and Santos, DS</p>

    <p>          ACTA CRYSTALLOGRAPHICA SECTION F-STRUCTURAL BIOLOGY AND CRYSTALLIZATION COMMUNICATIONS <b>2006</b>.  62 : 357-360, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236470000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236470000011</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
