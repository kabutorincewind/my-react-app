

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-10-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ypLl9OQYZdPfD9RP1ISLHuctudQ4ydsM/MqWH3MUujBD3KSj12t46Hz4/pLlCv6RRBUEEcLsoRkn48m4ox1SXi5xJ5EDys/NVbjxRRtDSJtEeC86PWsQR1ac+k0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2F73DD3A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: September 28 - October 11, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22922309">Synthesis, Crystal Structure, Characterisation, and Antifungal Activity of 3-Thiophene aldehyde semicarbazone (3STCH), 2,3-Thiophene dicarboxaldehyde bis(semicarbazone) (2,3BSTCH(2)) and Their Nickel (II) Complexes.</a> Alomar, K., V. Gaumet, M. Allain, G. Bouet, and A. Landreau. Journal of Inorganic Biochemistry, 2012. 115: p. 36-43. PMID[22922309].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22906851">Antiarrhythmic Drug Amiodarone Displays Antifungal Activity, Induces Irregular Calcium Response and Intracellular Acidification of Aspergillus niger - Amiodarone Targets Calcium and Ph Homeostasis of A. niger.</a> Bagar, T. and M. Bencina. Fungal Genetics and Biology : FG &amp; B, 2012. 49(10): p. 779-791. PMID[22906851].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 

    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23027188">Two Clinical Isolates of Candida Glabrata Exhibiting Reduced Sensitivity to Amphotericin B Both Harbor Mutations in ERG2.</a> Hull, C.M., O. Bader, J.E. Parker, M. Weig, U. Gross, A.G. Warrilow, D.E. Kelly, and S.L. Kelly. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23027188].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22825124">Combination of Voriconazole and Anidulafungin for Treatment of Triazole-resistant Aspergillus fumigatus in an in Vitro Model of Invasive Pulmonary Aspergillosis.</a> Jeans, A.R., S.J. Howard, Z. Al-Nakeeb, J. Goodwin, L. Gregson, P.A. Warn, and W.W. Hope. Antimicrobial Agents and Chemotherapy, 2012. 56(10): p. 5180-5185. PMID[22825124].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23026014">Design and Synthesis of Novel Antimicrobial Peptide: Modified N-terminal Bovine lactoferrin against Multidrug Resistant Gram-negative Bacteria and Candida Species.</a> Mishra, B., G.D. Leishangthem, K. Gill, A.K. Singh, S. Das, K. Singh, I. Xess, A. Dinda, A. Kapil, I.K. Patro, and S. Dey. Biochimica et Biophysica Acta, 2012. <b>[Epub ahead of print]</b>. PMID[23026014].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22867638">Antimicrobial Activity of Plants Used as Medicinals on an Indigenous Reserve in Rio Das Cobras, Parana, Brazil.</a> Moura-Costa, G.F., S.R. Nocchi, L.F. Ceole, J.C. Mello, C.V. Nakamura, B.P. Dias Filho, L.G. Temponi, and T. Ueda-Nakamura. Journal of Ethnopharmacology, 2012. 143(2): p. 631-638. PMID[22867638].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23025288">Influence of the Variation of Alkyl Chain Length of N-Alkyl-beta-D-glycosylamine Derivatives on Antifungal Properties.</a> Neto, V., A. Voisin, V. Heroguez, S. Grelier, and V. Coma. Journal of Agricultural and Food Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23025288].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22939695">Oxazolo[3,2-a]Pyridine. A New Structural Scaffold for the Reversal of Multi-Drug Resistance in Leishmania.</a> Caballero, E., J.I. Manzano, P. Puebla, S. Castanys, F. Gamarro, and A. San Feliciano. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(19): p. 6272-6275. PMID[22939695].</p>
    
    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22841846">Experimental and Theoretical Nmr Determination of Isoniazid and Sodium P-Sulfonatocalix[N]arenes Inclusion Complexes.</a> de Assis, J.V., M.G. Teixeira, C.G. Soares, J.F. Lopes, G.S. Carvalho, M.C. Lourenco, M.V. de Almeida, W.B. de Almeida, and S.A. Fernandes. European Journal of Pharmaceutical Sciences, 2012. 47(3): p. 539-548. PMID[22841846].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22939698">Three New 12-Carbamoylated streptothricins from Streptomyces Sp. I08a 1776.</a> Gan, M., X. Zheng, Y. Liu, Y. Guan, and C. Xiao. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(19): p. 6151-6154. PMID[22939698].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23012453">Nonsteroidal Anti-inflammatory Drug Sensitizes Mycobacterium tuberculosis to Endogenous and Exogenous Antimicrobials.</a> Gold, B., M. Pingle, S.J. Brickner, N. Shah, J. Roberts, M. Rundell, W.C. Bracken, T. Warrier, S. Somersan, A. Venugopal, C. Darby, X. Jiang, J.D. Warren, J. Fernandez, O. Ouerfelli, E.L. Nuermberger, A. Cunningham-Bussel, P. Rath, T. Chidawanyika, H. Deng, R. Realubit, J.F. Glickman, and C.F. Nathan. Proceedings of the National Academy of Sciences of the United States of America, 2012. 109(40): p. 16004-16011. PMID[23012453].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22986997">Synthesis, Antibacterial Activity and Mode of Action of Novel Linoleic acid-dipeptide-spermidine Conjugates.</a> Joshi, S., R.P. Dewangan, S. Yadav, D.S. Rawat, and S. Pasha. Organic &amp; Biomolecular Chemistry, 2012. 10(41): p. 8326-8335. PMID[22986997].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23045703">Identification of Antituberculosis Agents That Target Ribosomal Protein Interactions Using a Yeast Two-hybrid System.</a> Lin, Y., Y. Li, Y. Zhu, J. Zhang, X. Liu, W. Jiang, S. Yu, X.F. You, C. Xiao, B. Hong, Y. Wang, J.D. Jiang, and S. Si. Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>. PMID[23045703].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/230453915">Antitubercular Activity of the Semi-polar Extractives of Uvaria rufa.</a> Macabeo, A.P., F.A. Tudla, K. Krohn, and S.G. Franzblau. Asian Pacific Journal of Tropical Medicine, 2012. 5(10): p. 777-780. PMID[23043915].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23027189">In Vitro Synergy between Clofazimine and Amikacin in Nontuberculous Mycobacterial Disease.</a> van Ingen, J., S.E. Totten, N.K. Helstrom, L.B. Heifets, M.J. Boeree, and C.L. Daley. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23027189].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p>   
    
    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22869378">Conformational Flexibility Determines Selectivity and Antibacterial, Antiplasmodial, and Anticancer Potency of Cationic Alpha-helical Peptides.</a> Vermeer, L.S., Y. Lan, V. Abbate, E. Ruh, T.T. Bui, L.J. Wilkinson, T. Kanno, E. Jumagulova, J. Kozlowska, J. Patel, C.A. McIntyre, W.C. Yam, G. Siu, R.A. Atkinson, J.K. Lam, S.S. Bansal, A.F. Drake, G.H. Mitchell, and A.J. Mason. The Journal of Biological Chemistry, 2012. 287(41): p. 34120-34133. PMID[22869378].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p>
    
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23025282">Antibiotic and Antimalarial Quinones from Fungus-growing Ant-associated Pseudonocardia Sp.</a> Carr, G., E.R. Derbyshire, E. Caldera, C.R. Currie, and J. Clardy. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>. PMID[23025282].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22917857">Synthesis and Antiplasmodial Activity of New Heteroaryl Derivatives of 7-Chloro-4-aminoquinoline.</a> Casagrande, M., A. Barteselli, N. Basilico, S. Parapini, D. Taramelli, and A. Sparatore. Bioorganic &amp; Medicinal Chemistry, 2012. 20(19): p. 5965-5979. PMID[22917857].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23029306">Small-molecule Histone Methyltransferase Inhibitors Display Rapid Antimalarial Activity against All Blood Stage Forms in Plasmodium falciparum.</a> Malmquist, N.A., T.A. Moss, S. Mecheri, A. Scherf, and M.J. Fuchter. Proceedings of the National Academy of Sciences of the United States of America, 2012. 109(41): p. 16708-16713. PMID[23011794].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23041700">In Vitro Antiplasmodial Activity of Selected Luo and Kuria Medicinal Plants.</a> Owuor, B.O., J.O. Ochanda, J.O. Kokwaro, A.C. Cheruiyot, R.A. Yeda, C.A. Okudo, and H.M. Akala. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[23041700].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23039077">Exploring Inhibition of Pdx1, a Component of the Plp Synthase Complex of the Human Malaria Parasite Plasmodium Falciparum.</a> Reeksting, S.B., I.B. Muller, P.B. Burger, E.S. Burgos, L. Salmon, A.I. Louw, L.M. Birkholtz, and C. Wrenger. The Biochemical Journal, 2012. <b>[Epub ahead of print]</b>. PMID[23039077]. [<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22901673">Synthesis and Comparison of Antiplasmodial Activity of (+), (-) and Racemic 7-Chloro-4-(N-lupinyl)aminoquinoline.</a> Rusconi, C., N. Vaiana, M. Casagrande, N. Basilico, S. Parapini, D. Taramelli, S. Romeo, and A. Sparatore. Bioorganic &amp; Medicinal Chemistry, 2012. 20(19): p. 5980-5985. PMID[22901673].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23035716">Design and Synthesis of Inhibitors of Plasmodium falciparum N-Myristoyltransferase, a Promising Target for Anti-malarial Drug Discovery.</a> Yu, Z., J.A. Brannigan, D.K. Moss, A.M. Brzozowski, A.J. Wilkinson, A.A. Holder, E.W. Tate, and R.J. Leatherbarrow. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23035716].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p>   
    
    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23035243">Malarial Dihydrofolate Reductase as a Paradigm for Drug Development against a Resistance-compromised Target.</a> Yuthavong, Y., B. Tarnchompoo, T. Vilaivan, P. Chitnumsub, S. Kamchonwongpaisan, S.A. Charman, D.N. McLennan, K.L. White, L. Vivas, E. Bongard, C. Thongphanchang, S. Taweechai, J. Vanichtanankul, R. Rattanajak, U. Arwon, P. Fantauzzi, J. Yuvaniyama, W.N. Charman, and D. Matthews. Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>. PMID[23035243].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p>
    
    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23019377">Endochin-like Quinolones Are Highly Efficacious against Acute and Latent Experimental Toxoplasmosis.</a> Doggett, J.S., A. Nilsen, I. Forquer, K.W. Wegmann, L. Jones-Brando, R.H. Yolken, C. Bordon, S.A. Charman, K. Katneni, T. Schultz, J.N. Burrows, D.J. Hinrichs, B. Meunier, V.B. Carruthers, and M.K. Riscoe. Proceedings of the National Academy of Sciences of the United States of America, 2012. 109(39): p. 15936-15941. PMID[23019377].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22970937">Salicylanilide Inhibitors of Toxoplasma gondii.</a> Fomovska, A., R.D. Wood, E. Mui, J.P. Dubey, L.R. Ferreira, M.R. Hickman, P.J. Lee, S.E. Leed, J.M. Auschwitz, W.J. Welsh, C. Sommerville, S. Woods, C. Roberts, and R. McLeod. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22970937].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0928-101112.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307504500006">Antimicrobial Properties of Skin Mucus from Four Freshwater Cultivable Fishes (Catla catla, Hypophthalmichthys molitrix, Labeo rohita and Ctenopharyngodon idella).</a> Balasubramanian, S., P.B. Rani, A.A. Prakash, M. Prakash, P. Senthilraja, and G. Gunasekaran. African Journal of Microbiology Research, 2012. 6(24): p. 5110-5120. ISI[000307504500006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307268100023">Antimicrobial Screening of Novel Synthesized Benzimidazole Nucleus Containing 4-Oxo-thiazolidine Derivatives.</a> Desai, N.C., A.M. Dodiya, and A.H. Makwana. Medicinal Chemistry Research, 2012. 21(9): p. 2320-2328. ISI[000307268100023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00030728100049">A Search of Novel Antimicrobial Based on Benzimidazole and 2-Pyridone Heterocycles.</a> Desai, N.C., A.M. Dodiya, and N.R. Shihory. Medicinal Chemistry Research, 2012. 21(9): p. 2579-2586. ISI[000307268100049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307040600020">A New Sphingolipid and Furanocoumarins with Antimicrobial Activity from Ficus exasperata.</a> Dongfack, M.D.J., M.C. Lallemand, V. Kuete, C.D. Mbazoa, J.D. Wansi, T.V.D. Hanh, S. Michel, and J. Wandji. Chemical &amp; Pharmaceutical Bulletin, 2012. 60(8): p. 1072-1075. ISI[000307040600020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307920600032">Hybrids of Ravuconazole: Synthesis and Biological Evaluation.</a> Gaikwad, N.D., S.V. Patil, and V.D. Bobade. European Journal of Medicinal Chemistry, 2012. 54: p. 295-302. ISI[000307920600032].</p>
    
    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307995200038">Antimicrobial and Antiprotozoal Activities of Secondary Metabolites from the Fungus Eurotium repens.</a> Gao, J.T., M.M. Radwan, F. Leon, X.N. Wang, M.R. Jacob, B.L. Tekwani, S.I. Khan, S. Lupien, R.A. Hill, F.M. Dugan, H.G. Cutler, and S.J. Cutler. Medicinal Chemistry Research, 2012. 21(10): p. 3080-3086. ISI[000307995200038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308382800001">Synthesis and Characterization of New 2-Amino-4-(3,4-dihydro-7-methoxy-2,2-dimethyl-2h-benzopyran-6-yl)-6-(sub stituted phenyl)pyrimidines and Their Bioevaluation.</a> Murthy, Y.L.N., K.P. Suhasini, and A. Jha. Journal of the Serbian Chemical Society, 2012. 77(7): p. 859-866. ISI[000308382800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307995200050">Design, Synthesis, Characterization, and in Vitro Antimicrobial Action of Novel Trisubstituted S-Triazines.</a> Patel, P.K., R.V. Patel, D.H. Mahajan, P.A. Parikh, G.N. Mehta, and K.H. Chikhalia. Medicinal Chemistry Research, 2012. 21(10): p. 3182-3194. ISI[000307995200050].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307995200024">Synthesis and Antimicrobial Activity of 5-((3-Aryl-1-phenyl-1h-pyrazol-4-Yl)methylene)thiazolidine-2,4-diones.</a> Prakash, O., D.K. Aneja, P. Lohan, K. Hussain, S. Arora, C. Sharma, and K.R. Aneja. Medicinal Chemistry Research, 2012. 21(10): p. 2961-2968. ISI[000307995200024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">36.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307973400016">Tuberculosis: Finding a New Potential Antimycobacterium Derivative in a Aldehyde-Arylhydrazone-Oxoquinoline Series.</a> Santos, F.D., H.C. Castro, M.C.S. Lourenco, P.A. Abreu, P.N. Batalha, A.C. Cunha, G.S.L. Carvalho, C.R. Rodrigues, C.A. Medeiros, S.D. Souza, V.F. Ferreira, and M. de Souza. Current Microbiology, 2012. 65(4): p. 455-460. ISI[000307973400016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307995200022">Synthesis of 1-(4-Aminosulfonylphenyl)-3,5-diarylpyrazoline Derivatives as Potent Antiinflammatory and Antimicrobial Agents.</a> Sharma, P.K., S. Kumar, P. Kumar, P. Kaushik, C. Sharma, D. Kaushik, and K.R. Aneja. Medicinal Chemistry Research, 2012. 21(10): p. 2945-2954. ISI[000307995200022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p>     
    
    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307268100030">Synthesis and Antimycobacterial Activities of Some New N-Acylhydrazone and Thiosemicarbazide Derivatives of 6-Methyl-4,5-dihydropyridazin-3(2h)-one.</a> &lt;Go to ISI&gt;://WOS:000307268100030. Tan, O.U., K. Ozadali, P. Yogeeswari, D. Sriram, and A. Balkan. Medicinal Chemistry Research, 2012. 21(9): p. 2388-2394. ISI[000307268100030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p>     
    
    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307995200029">Synthesis and Evaluation of Fused Pyrimidine Derivatives as Anti-Inflammatory, Antiproliferative and Antimicrobial Agents.</a> Vachala, S.D., K.K. Srinivasan, and P.Y. Prakash. Medicinal Chemistry Research, 2012. 21(10): p. 2998-3005. ISI[000307995200029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p>     
    
    <p> </p>

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307920600037">Design, Synthesis, and Molecular Hybrids of Caudatin and Cinnamic Acids as Novel anti-Hepatitis B Virus Agents.</a> Wang, L.J., C.A. Geng, Y.B. Ma, J. Luo, X.Y. Huang, H. Chen, N.J. Zhou, X.M. Zhang, and J.J. Chen. European Journal of Medicinal Chemistry, 2012. 54: p. 352-365. ISI[000307920600037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p>      
 
    <p> </p>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307996700005">Enhancement of Anti-candidal Activity of Endophytic Fungus Phomopsis Sp Ed2, Isolated from Orthosiphon stamineus Benth, by Incorporation of Host Plant Extract in Culture Medium.</a> Yenn, T.W., C.C. Lee, D. Ibrahim, and L. Zakaria. Journal of Microbiology, 2012. 50(4): p. 581-585. ISI[000307996700005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p> 
    
    <p> </p>

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308115300066">Selective anti-HCV Activity of 6,7-bis-O-Arylmethyl-5,6,7-trihydroxychromone Derivatives.</a> Yoon, H., M.K. Kim, H. Mok, and Y. Chong. Bulletin of the Korean Chemical Society, 2012. 33(8): p. 2803-2805. ISI[000308115300066].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0928-101112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
