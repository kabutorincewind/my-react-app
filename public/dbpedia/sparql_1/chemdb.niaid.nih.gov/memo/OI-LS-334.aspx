

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-334.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QrpDhVZC3hrkSaaFHTp42zBGfZ7d0p00P8RH7ttVkunfEXT8UV2c5zCVuSYM0HrASm4hEfAHxrsHgHgAjdqc7eOgy96h/Q7SJ1bIvzyxRbEF7pc6hT1Ys8xaOe0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AB809338" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-334-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48058   OI-LS-334; PUBMED-OI-10/31/2005</p>

    <p class="memofmt1-2">          Effectiveness of anidulafungin in eradicating Candida species in invasive candidiasis</p>

    <p>          Pfaller, MA, Diekema, DJ, Boyken, L, Messer, SA, Tendolkar, S, Hollis, RJ, and Goldstein, BP</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4795-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251335&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251335&amp;dopt=abstract</a> </p><br />

    <p>2.     48059   OI-LS-334; PUBMED-OI-10/31/2005</p>

    <p class="memofmt1-2">          Cloning of the Gene Encoding a Protective Mycobacterium tuberculosis Secreted Protein Detected In Vivo during the Initial Phases of the Infectious Process</p>

    <p>          Mukherjee, S, Kashino, SS, Zhang, Y, Daifalla, N, Rodrigues, V Jr, Reed, SG, and Campos-Neto, A</p>

    <p>          J Immunol <b>2005</b>.  175(8): 5298-305</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16210635&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16210635&amp;dopt=abstract</a> </p><br />

    <p>3.     48060   OI-LS-334; PUBMED-OI-10/31/2005</p>

    <p class="memofmt1-2">          Design and studies of novel 5-substituted alkynylpyrimidine nucleosides as potent inhibitors of mycobacteria</p>

    <p>          Rai, D, Johar, M, Manning, T, Agrawal, B, Kunimoto, DY, and Kumar, R</p>

    <p>          J Med Chem <b>2005</b>.  48(22): 7012-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250660&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250660&amp;dopt=abstract</a> </p><br />

    <p>4.     48061   OI-LS-334; PUBMED-OI-10/31/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-hepatitis C virus activity of nucleoside derivatives of N3, 5&#39;-anhydro-4-(beta-D-ribofuranosyl)-8-aza-purin-2-ones</p>

    <p>          Hassan, AE, Wang, P, McBrayer, TR, Tharnish, PM, Stuyver, LJ, Schinazi, RF, Otto, MJ, and Watanabe, KA</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 961-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248072&amp;dopt=abstract</a> </p><br />

    <p>5.     48062   OI-LS-334; PUBMED-OI-10/31/2005</p>

    <p class="memofmt1-2">          Synthesis of 5-aza-7-deazaguanine nucleoside derivatives as potential anti-flavivirus agents</p>

    <p>          Dukhan, D, Leroy, F, Peyronnet, J, Bosc, E, Chaves, D, Durka, M, Storer, R, La, Colla P, Seela, F, and Gosselin, G</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 671-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248011&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248011&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48063   OI-LS-334; WOS-OI-10/23/2005</p>

    <p class="memofmt1-2">          DNAzymes targeting the icl gene inhibit ICL expression and decrease Mycobacterium tuberculosis survival in macrophages</p>

    <p>          Li, JM, Zhu, DY, Yi, ZJ, He, YL, Chun, Y, Liu, YH, and Li, N</p>

    <p>          OLIGONUCLEOTIDES <b>2005</b>.  15(3): 215-222, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232253600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232253600006</a> </p><br />

    <p>7.     48064   OI-LS-334; WOS-OI-10/23/2005</p>

    <p class="memofmt1-2">          Total synthesis and further scrutiny of the in vitro antifungal activity of 6-nonadecynoic acid</p>

    <p>          Carballeira, NM, Sanabria, D, and Parang, K</p>

    <p>          ARCHIV DER PHARMAZIE <b>2005</b>.  338(9): 441-443, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232313800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232313800007</a> </p><br />

    <p>8.     48065   OI-LS-334; WOS-OI-10/23/2005</p>

    <p class="memofmt1-2">          Efficient intermittent rifapentine-moxifloxacin-containing short-course regimen for treatment of tuberculosis in mice</p>

    <p>          Veziris, N, Lounis, N, Chauffour, A, Truffot-Pernot, C, and Jarlier, V</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(10): 4015-4019, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700004</a> </p><br />

    <p>9.     48066   OI-LS-334; WOS-OI-10/23/2005</p>

    <p class="memofmt1-2">          Oral therapy using nanoparticle-encapsulated antituberculosis drugs in guinea pigs infected with Mycobacterium tuberculosis</p>

    <p>          Johnson, CM, Pandey, R, Sharma, S, Khuller, GK, Basaraba, RJ, Orme, IM, and Lenaerts, AJ</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(10): 4335-4338, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232244700045</a> </p><br />

    <p>10.   48067   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of N- and O-alkylated bicyclic furanopyrimidines as non-nucleosidic inhibitors of human cytomegalovirus</p>

    <p>          Kelleher, MR, McGuigan, C, Andrei, G, Snoeck, R, De, Clercq E, and Balzarini, J</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 639-641, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500058">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500058</a> </p><br />

    <p>11.   48068   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          The journey towards elucidating the anti-HCMV activity of alkylated bicyclic furano pyrimidines</p>

    <p>          Kelleher, MR, McGuigan, C, Bidet, O, Carangio, A, Weldon, H, Andrei, G, Snoeck, R, De, Clercq E, and Balzarini, J</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 643-645, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500059</a> </p><br />

    <p>12.   48069   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          NM 283, an efficient prodrug of the potent anti-HCV agent 2 &#39;-C-methylcytidine</p>

    <p>          Pierra, C, Benzaria, S, Amador, A, Moussa, A, Mathieu, S, Storer, R, and Gosselin, G</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 767-770, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500088">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500088</a> </p><br />

    <p>13.   48070   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-HCV activity of beta-D- and L-2 &#39;-deoxy-2 &#39;-fluororibonucleosides</p>

    <p>          Shi, JX, Du, JF, Ma, TW, Pankiewicz, KW, Patterson, SE, Hassan, AEA, Tharnish, PM, McBrayer, TR, Lostia, S, Stuyver, LJ, Watanabe, KA, Chu, CK, Schinazi, RF, and Otto, MJ</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(5-7): 875-879, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500109">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500109</a> </p><br />

    <p>14.   48071   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          Synthesis and structure-activity relationships of novel anti-hepatitis C agents: N-3,5 &#39;-cyclo-4-(beta-D-ribofuranosy )-vic-triazolo[4,5-b]pyridin-5-one derivatives</p>

    <p>          Wang, PY, Du, JF, Rachakonda, S, Chun, BK, Tharnish, PM, Stuyver, LJ, Otto, MJ, Schinazi, RF, and Watanabe, KA</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(20): 6454-6460, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232406600029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232406600029</a> </p><br />

    <p>15.   48072   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          Discovery of Mycobacterium tuberculosis protein tyrosine phosphatase A (MptpA) inhibitors based on natural products and a fragment-based approach</p>

    <p>          Manger, M, Scheck, M, Prinz, H, von, Kries JP, Langer, T, Saxena, K, Schwalbe, H, Furstner, A, Rademann, J, and Waldmann, H</p>

    <p>          CHEMBIOCHEM <b>2005</b>.  6(10): 1749-1753, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232570100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232570100005</a> </p><br />

    <p>16.   48073   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          Biocidal activity of some Mannich base cationic derivatives</p>

    <p>          Negm, NA, Morsy, SM, and Said, MM</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(21): 5921-5926, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232517800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232517800004</a> </p><br />

    <p>17.   48074   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          Synthesis of zidovudine prodrugs with broad-spectrum chemotherapeutic properties for the effective treatment of HIV/AIDS</p>

    <p>          Sriram, D, Srichakravarthy, N, Bal, TR, and Yogeeswari, P</p>

    <p>          BIOMEDICINE &amp; PHARMACOTHERAPY <b>2005</b>.  59(8): 452-455, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232464500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232464500005</a> </p><br />

    <p>18.   48075   OI-LS-334; WOS-OI-10/30/2005</p>

    <p class="memofmt1-2">          Nevirapine derivatives with broad-spectrum chemotherapeutic properties for the effective treatment of HIV/AIDS</p>

    <p>          Sriram, D, Srichakravarthy, N, Bal, TR, and Yogeeswari, P</p>

    <p>          BIOMEDICINE &amp; PHARMACOTHERAPY <b>2005</b>.  59(8): 456-459, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232464500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232464500006</a> </p><br />
    <br clear="all">

    <p>19.   48076   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          Exploring drug action on Mycobacterium tuberculosis using affymetrix oligonucleotide genechips</p>

    <p>          Fu, Li M</p>

    <p>          Tuberculosis <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXK-4HD8BCS-1/2/c23cf55265996e79b4e624a66f70de78">http://www.sciencedirect.com/science/article/B6WXK-4HD8BCS-1/2/c23cf55265996e79b4e624a66f70de78</a> </p><br />

    <p>20.   48077   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          Syntheses, in vitro antibacterial and antifungal activities of a series of N-alkyl, 1,4-dithiines</p>

    <p>          Zentz, F, Labia, R, Sirot, D, Faure, O, Grillot, R, and Valla, A</p>

    <p>          Il Farmaco <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJ8-4HC0R54-1/2/dfe0f8b00af9195c79ba95e7c38d1ad3">http://www.sciencedirect.com/science/article/B6VJ8-4HC0R54-1/2/dfe0f8b00af9195c79ba95e7c38d1ad3</a> </p><br />

    <p>21.   48078   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          Anti-tubercular agents. Part 3. Benzothiadiazine as a novel scaffold for anti-Mycobacterium activity</p>

    <p>          Kamal, Ahmed, Srinivasa Reddy, K, Kaleem Ahmed, S, Khan, MNaseer A, Sinha, Rakesh K, Yadav, JS, and Arora, Sudershan K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H7TCVW-5/2/5287e438804795dc891e2d21219311cc">http://www.sciencedirect.com/science/article/B6TF8-4H7TCVW-5/2/5287e438804795dc891e2d21219311cc</a> </p><br />

    <p>22.   48079   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          Potential drug targets in Mycobacterium tuberculosis through metabolic pathway analysis</p>

    <p>          Anishetty, Sharmila, Pulimi, Mrudula, and Pennathur, Gautam</p>

    <p>          Computational Biology and Chemistry <b>2005</b>.  29(5): 368-378</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B73G2-4H8FPH9-2/2/66482a0baa5c698a7f163fc0e90de8ec">http://www.sciencedirect.com/science/article/B73G2-4H8FPH9-2/2/66482a0baa5c698a7f163fc0e90de8ec</a> </p><br />

    <p>23.   48080   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of 5-hydroxymethyl- 8-methyl-2-(N-arylimino)-pyrano[2,3-c]pyridine-3-(N-aryl)-carboxamides</p>

    <p>          Zhuravel&#39;, Irina O, Kovalenko, Sergiy M, Ivachtchenko, Alexandre V, Balakin, Konstantin V, and Kazmirchuk, Victor V</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4H5MYGM-G/2/1e161feeed01fa2428281aa2bf64be66">http://www.sciencedirect.com/science/article/B6TF9-4H5MYGM-G/2/1e161feeed01fa2428281aa2bf64be66</a> </p><br />

    <p>24.   48081   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          Thiourea, triazole and thiadiazine compounds and their metal complexes as antifungal agents</p>

    <p>          Rodriguez-Fernandez, E, Manzano, Juan L, Benito, Juan J, Hermosa, Rosa, Monte, Enrique, and Criado, Julio J</p>

    <p>          Journal of Inorganic Biochemistry <b>2005</b>.  99(8): 1558-1572</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGG-4GKW70H-1/2/630b1e01a0dec2bb6f5037e5a625cc25">http://www.sciencedirect.com/science/article/B6TGG-4GKW70H-1/2/630b1e01a0dec2bb6f5037e5a625cc25</a> </p><br />
    <br clear="all">

    <p>25.   48082   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          A phase I trial of an antisense inhibitor of Hepatitis c virus (ISIS 14803), administered to chronic Hepatitis C patients</p>

    <p>          McHutchison, John G, Patel, Keyur, Pockros, Paul, Nyberg, Lisa, Pianko, Stephen, Yu, Rosie Z, Andrew Dorr, F, and Jesse Kwoh, T</p>

    <p>          Journal of Hepatology <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4HDGKDR-2/2/858210d2decef93cd50088c4d0a1998d">http://www.sciencedirect.com/science/article/B6W7C-4HDGKDR-2/2/858210d2decef93cd50088c4d0a1998d</a> </p><br />

    <p>26.   48083   OI-LS-334; EMBASE-OI-10/31/2005</p>

    <p class="memofmt1-2">          Synthesis, physicochemical properties and antiviral activities of ester prodrugs of ganciclovir</p>

    <p>          Patel, Kunal, Trivedi, Shrija, Luo, Shuanghui, Zhu, Xiaodong, Pal, Dhananjay, Kern, Earl R, and Mitra, Ashim K</p>

    <p>          International Journal of Pharmaceutics <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7W-4HC6KVN-1/2/2d414ff1a5050e3ad9fee0b2206e44e4">http://www.sciencedirect.com/science/article/B6T7W-4HC6KVN-1/2/2d414ff1a5050e3ad9fee0b2206e44e4</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
