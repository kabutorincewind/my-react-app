

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-11-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dqlt+V4bMk9mRMnRLgxjHOQVvxfRRk4ifhWpB5jTGz68NnsBmaoenVtyXkHB/rvE+8NudD/NbBW2oXXhZjJCPl/RMDIoNQy+icuI1ai4t6nCIyLbeYKwr9LnpGE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D67FA111" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">November 11 - November 24, 2011</h1>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>
    <p class="plaintext">
1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22088496">Effect of Amphotericin B Alone or in Combination with Rifampicin or Dlarithromycin against Candida Species Biofilms.</a> Del Pozo JL, Frances ML, Hernaez S, Serrera A, Alonso M, Rubio MF, International Journal of Artificial Organs, 2011. 34(9): 766-770; PMID[22094555].
</p>

    <p class="plaintext"><b>[PubMed].</b> OI_1111-112411.</p>

    <p> </p>

    <p class="memofmt2-3"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>
    <p class="plaintext">
2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22078007">Synthesis and Antitubercular Activity of Novel Amino acid Derivatives.</a> Da Costa CF, Pinheiro AC, De Almeida MV, Lourenco MC, De Souza MV, Chemical Biology &amp; Drug Design, 2011.<b> [Epub ahead of print]</b>; PMID[22078007].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>
    <p> </p>
     
    <p class="plaintext">
3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22014754">C4-Alkylthiols with Activity against Moraxella catarrhalis and Mycobacterium tuberculosis.</a> Kostova MB, Myers CJ, Beck TN, Plotkin BJ, Green JM, Boshoff HI, Barry CE, 3rd, Deschamps JR, Konaklieva MI, Bioorganic &amp;  Medicinal Chemistry, 2011. 19(22): 6842-6852; PMID[22014754].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>
    <p> </p>
     
    <p class="plaintext">
4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22001325">The Structure-antituberculosis Activity Relationships Study in a Series of 5-Aryl-2-thio-1,3,4-oxadiazole Derivatives.</a> Macaev F, Ribkovskaia Z, Pogrebnoi S, Boldescu V, Rusu G, Shvets N, Dimoglo A, Geronikaki A, Reynolds R, Bioorganic &amp;  Medicinal Chemistry, 2011. 19(22): 6792-6807; PMID[22001325].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>
    <p> </p>
    
    <p class="plaintext">
5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22079533">An Antibacterial from Hypericum acmosepalum Inhibits ATP-dependent MurE Ligase from Mycobacterium tuberculosis.</a> Osman K, Evangelopoulos D, Basavannacharya C, Gupta A, McHugh TD, Bhakta S, Gibbons S, International Journal of Antimicrobial Agents, 2011.<b> [Epub ahead of print]</b>; PMID[22079533].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p> </p>
    <p class="plaintext">
6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22085112">Tomentosones A and B, Hexacyclic Phloroglucinol Derivatives from the Thai Shrub Rhodomyrtus tomentosa.</a> Hiranrat A, Mahabusarakam W, Carroll AR, Duffy S, Avery VM, Journal of Organic Chemistry, 2011.<b> [Epub ahead of print]</b>; PMID[22085112].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>
    <p> </p>
     
    <p class="plaintext">
7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21696970">3-Bromohomofascaplysin A, a Fascaplysin Analogue from a Fijian Didemnum sp. ascidian.</a> Lu Z, Ding Y, Li XC, Djigbenou DR, Grimberg BT, Ferreira D, Ireland CM, Van Wagoner RM, Bioorganic &amp;  Medicinal Chemistry, 2011. 19(22): 6604-6607; PMID[21696970].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>
    <p> </p>
    
    <p class="plaintext">
8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22095896">Identification of 1,3-Diiminoisoindoline Carbohydrazides as Potential Antimalarial Candidates.</a> Mombelli P, Witschel MC, van Zijl AW, Geist JG, Rottmann M, Freymond C, Rohl F, Kaiser M, Illarionova V, Fischer M, Siepe I, Schweizer WB, Brun R, Diederich F, ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[22095896].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>
    <p> </p>    
    
    <p class="plaintext">
9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22083475">In Vitro and in Vivo Activity of Solithromycin (CEM-101) against Plasmodium Species.</a> Wittlin S, Ekland E, Craft JC, Lotharius J, Bathurst I, Fidock DA, Fernandes P, Antimicrobial Agents and Chemotherapy, 2011.<b> [Epub ahead of print]</b>; PMID[22083475].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>
    <p> </p>

    <h2 class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</h2>

    <p class="memofmt2-3"> </p>
    <p class="plaintext">
10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22000949">Pterocarpanquinones, aza-pterocarpanquinone and Derivatives: Synthesis, Antineoplasic Activity on Human Malignant Cell Lines and Antileishmanial Activity on Leishmania amazonensis.</a> Buarque CD, Militao GC, Lima DJ, Costa-Lotufo LV, Pessoa C, de Moraes MO, Cunha-Junior EF, Torres-Santos EC, Netto CD, Costa PR, Bioorganic &amp;  Medicinal Chemistry, 2011. 19(22): 6885-6891; PMID[22000949].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>

    <p> </p>
    <p class="plaintext">
11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22072308">Leishmanicidal and Antitumoral Activities of Endophytic Fungi Associated with the Antarctic angiosperms Deschampsia antarctica Desv. and Colobanthus quitensis (Kunth) Bartl.</a> Santiago IF, Alves TM, Rabello A, Sales Junior PA, Romanha AJ, Zani CL, Rosa CA, Rosa LH, Extremophiles, 2011. <b>[Epub ahead of print]</b>; PMID[22072308].
</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1111-112411.</p>

    <p> </p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296219000015">Synthesis, Spectroscopic, Electrochemical and Biological Studies of Co(II), Ni(II) and Cu(II) Complexes of 2-Hydroxybenzylidene-2&#39;-hydroxyaniline.</a> Begum FRM, Jayamani V. Hemalatha R., Asian Journal of Chemistry, 2011. 23(12): 5258-5260; ISI[000296219000015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296005700013">Kinetics of Thermal Decomposition and Antimicrobial Screening of Terpolymer Resins.</a> Burkanudeen AR, Azarudeen R. S., Ahamed M. A. R., Gurnule W. B., Polymer Bulletin, 2011. 67(8): 1553-1568; ISI[000296005700013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296085200009">The Activity of Flavones and Oleanolic acid from Lippia lacunosa against Susceptible and Resistant Mycobacterium tuberculosis Strains.</a> Castellar A, Coelho T. S., Silva P. E. A., Ramos D. F., Lourenco M. C. S., Lage C. L. S., Juliao L. S., Barbosa Y. G., Leitao S. G., Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2011. 21(5): 835-840; ISI[000296085200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296025900066">Chemical Scaffolds with structural similarities to Siderophores of Nonribosomal Peptide-polyketide Origin as Novel Antimicrobials against Mycobacterium tuberculosis and Yersinia pestis.</a>Ferreras JA, Gupta A., Amin N. D., Basu A., Sinha B. N., Worgall S., Jayaprakash V., Quadri L. E. N., Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): 6533-6537; ISI[000296025900066].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296219000059">Synthesis, Characterization and Antifungal Activity of Some 5-Substituted 4-Amino-1,2,4-triazole-3-thiols.</a> Hasan A, Akhtar M. N., Gapil S., Asian Journal of Chemistry, 2011. 23(12): 5471-5476; ISI[000296219000059].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295989500018">Combinatorial Activities of Ionic Silver and Sodium hexametaphosphate against Microorganisms Associated with Chronic Wounds.</a> Humphreys G, Lee G. L., Percival S. L., McBain A. J., Journal of Antimicrobial Chemotherapy, 2011. 66(11): 2556-2561; ISI[000295989500018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296281700008">Isolation of Endophytic Actinomycetes from Catharanthes roseus (L.) G. Don Leaves and Their Antimicrobial Activity.</a> Kafur A, Khan A. B., Iranian Journal of Biotechnology, 2011. 9(4): 302-306; ISI[000296281700008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296025900064">Synthesis and Preliminary Biological Evaluation of Novel N-(3-Aryl-1,2,4-triazol-5-yl) Cinnamamide Derivatives as Potential Antimycobacterial Agents: An Operational Topliss Tree Approach.</a> Kakwani MD, Desai N. H. P., Lele A. C., Ray M., Rajan M. G. R., Degani M. S., Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): 6523-6526; ISI[000296025900064].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295803600014">Phytochemical Screening and in Vitro Evaluation of Anticandidal Activity of Dodonaea viscosa (L.) Jaeq. (Sapindaceae).</a> Khurram M, Hameed A., Amin M. U., Gul A., Ullah N., Hassan M., Qayum A., Chishti K. A., Manzoor W., African Journal of Pharmacy and Pharmacology, 2011. 5(11): 1422-1426; ISI[000295803600014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295932100037">Small Molecule Inhibitors of the Candida albicans Budded-to-Hyphal Transition Act through Multiple Signaling Pathways.</a> Midkiff J, Borochoff-Porte N., White D., Johnson D. I., Plos One, 2011. 6(9): ISI[000295932100037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295989500021">Sodium butyrate Inhibits Pathogenic Yeast Growth and Enhances the Functions of Macrophages.</a> Nguyen LN, Lopes L. C. L., Cordero R. J. B., Nosanchuk J. D., Journal of Antimicrobial Chemotherapy, 2011. 66(11): 2573-2580; ISI[000295989500021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296085200003">Ethnopharmacological Versus Random Plant Selection Methods for the Evaluation of the Antimycobacterial Activity.</a> Oliveira DR, Leitao G. G., Coelho T. S., da Silva P. E. A., Lourenco M. C. S., Leitao S. G., Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2011. 21(5): 793-806; ISI[000296085200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296122700005">Synthesis and Antimicrobial Properties of Naphthylamine Derivatives Having a Thiazolidinone Moiety.</a>Petrikaite V, Tarasevicius E., Pavilonis A., Medicina-Lithuania, 2011. 47(6): 334-339; ISI[000296122700005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296038700019">Novel Silver Nano-wedges for Killing Microorganisms.</a> Pourjavadi A, Soleyman R., Materials Research Bulletin, 2011. 46(11): 1860-1865; ISI[000296038700019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295609700024">Silver Nanoparticles with Gelatin Nanoshells: Photochemical Facile Green Synthesis and Their Antimicrobial Activity.</a> Pourjavadi A, Soleyman R., Journal of Nanoparticle Research, 2011. 13(10): 4647-4658; ISI[000295609700024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p class="MsoNormal memofmt2-4"></p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296025900016">Synthesis, Stereochemistry and in Vitro Antimicrobial Evaluation of Novel 2- (2,4-Diaryl-3-azabicyclo 3.3.1 nonan-9-ylidene)hydrazono -4-phenyl-2, 3-dihydrothiazoles.</a>Ramachandran R, Parthiban P., Rani M., Jayanthi S., Kabilan S., Jeong Y. T., Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): 6301-6304; ISI[000296025900016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296025900046">A Pd-mediated New Strategy to Functionalized 2-Aminochromenes: Their in Vitro Evaluation as Potential Anti Tuberculosis Agents.</a> Reddy TR, Reddy L. S., Reddy G. R., Nuthalapati V. S., Lingappa Y., Sandra S., Kapavarapu R., Misra P., Pal M., Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): 6433-6439; ISI[000296025900046].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296113500034">The Effect of Thiol Functional Group Incorporation into Cationic Helical,</a> Wiradharma N, Biomaterials, 2011. 32(34): 9100-9108; ISI[000296113500034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1111-112411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
