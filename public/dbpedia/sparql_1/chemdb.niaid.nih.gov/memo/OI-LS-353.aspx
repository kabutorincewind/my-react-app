

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-353.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wP0cltV6IeLe+yWzoXYNzx26znJcq6fcsdGWxTMZ3ChCstCDqxxam+PMZnjHVQAP35h3rUMnEZYGUuFNKngr8AFXj7CgYPGg0KUTIkhcOCfWZ7wxJVTPE/HdYA8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9160A0D2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-353-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49185   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Future therapies for hepatitis C</p>

    <p>          Pawlotsky, JM and Gish, RG</p>

    <p>          Antivir Ther <b>2006</b>.  11(4): 397-408</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16856613&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16856613&amp;dopt=abstract</a> </p><br />

    <p>2.     49186   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Synthesis of some new coumarino [4,3-b] pyrido [6,5-c] cinnolines as potent antitubercular agents</p>

    <p>          Ramalingam, P, Ganapaty, S, Rao, ChBabu, and Ravi, TK</p>

    <p>          Indian Journal of Heterocyclic Chemistry <b>2006</b>.  15(4): 359-362</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     49187   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Arbidol: a broad-spectrum antiviral that inhibits acute and chronic HCV infection</p>

    <p>          Boriskin, YS, Pecheur, EI, and Polyak, SJ</p>

    <p>          Virol J <b>2006</b>.  3(1): 56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854226&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854226&amp;dopt=abstract</a> </p><br />

    <p>4.     49188   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          The structure - antituberculosis activity relationships study in a series of 5-(4-aminophenyl)-4-substituted-2,4-dihydro-3h-1,2,4-triazole-3-thione derivatives. A combined electronic-topological and neural networks approach</p>

    <p>          Kandemirli, Fatma, Shvets, Nathali, Unsalan, Seda, Kucukguzel, Ilkay, Rollas, Sevim, Kovalishyn, Vasyl, and Dimoglo, Anatholy</p>

    <p>          Medicinal Chemistry <b>2006</b>.  2(4): 415-422</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     49189   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Macrophages Acquire Neutrophil Granules for Antimicrobial Activity against Intracellular Pathogens</p>

    <p>          Tan, BH, Meinken, C, Bastian, M, Bruns, H, Legaspi, A, Ochoa, MT, Krutzik, SR, Bloom, BR, Ganz, T, Modlin, RL, and Stenger, S</p>

    <p>          J Immunol <b>2006</b>.  177(3): 1864-71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16849498&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16849498&amp;dopt=abstract</a> </p><br />

    <p>6.     49190   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Synthesis and in vitro antimycobacterial activity of N 1-nicotinoyl-3-(4&#39;-hydroxy-3&#39;-methyl phenyl)-5-[(sub)phenyl]-2-pyrazolines</p>

    <p>          Shaharyar, Mohammad, Siddiqui, Anees Ahamed, Ali, Mohamed Ashraf, Sriram, Dharmarajan, and Yogeeswari, Perumal</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(15): 3947-3949</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     49191   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          New trends in development of antimycobacterial compounds</p>

    <p>          Biava, M, Porretta, GC, Deidda, D, and Pompei, R</p>

    <p>          Infectious Disorders: Drug Targets <b>2006</b>.  6(2): 159-172</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     49192   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Global incidence of multidrug-resistant tuberculosis</p>

    <p>          Zignol, M, Hosseini, MS, Wright, A, Weezenbeek, CL, Nunn, P, Watt, CJ, Williams, BG, and Dye, C</p>

    <p>          J Infect Dis <b>2006</b>.  194(4): 479-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16845631&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16845631&amp;dopt=abstract</a> </p><br />

    <p>9.     49193   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Hepatitis C virus non-structural protein NS5A interacts with FKBP38 and inhibits apoptosis in Huh7 hepatoma cells</p>

    <p>          Wang, J, Tong, W, Zhang, X, Chen, L, Yi, Z, Pan, T, Hu, Y, Xiang, L, and Yuan, Z</p>

    <p>          FEBS Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16844119&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16844119&amp;dopt=abstract</a> </p><br />

    <p>10.   49194   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          The potential of azole antifungals against latent/persistent tuberculosis</p>

    <p>          Ahmad, Zahoor, Sharma, Sadhna, and Khuller, Gopal K</p>

    <p>          FEMS Microbiology Letters <b>2006</b>.  258(2): 200-203</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   49195   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Synthesis, anti-tuberculosis activity, and 3D-QSAR study of ring-substituted-2/4-quinolinecarbaldehyde derivatives</p>

    <p>          Nayyar, A, Malde, A, Coutinho, E, and Jain, R</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16843663&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16843663&amp;dopt=abstract</a> </p><br />

    <p>12.   49196   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Slow-onset inhibition of 2-trans-enoyl-ACP (CoA) reductase from Mycobacterium tuberculosis by an inorganic complex</p>

    <p>          Oliveira, JS,  de Sousa, EH, de Souza, ON, Moreira, IS, Santos, DS, and Basso, LA</p>

    <p>          Curr Pharm Des <b>2006</b>.  12(19): 2409-2424</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16842188&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16842188&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.   49197   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Replication of Hepatitis C Virus (HCV) RNA in Mouse Embryonic Fibroblasts: Protein Kinase R (PKR)-Dependent and PKR-Independent Mechanisms for Controlling HCV RNA Replication and Mediating Interferon Activities</p>

    <p>          Chang, KS, Cai, Z, Zhang, C, Sen, GC, Williams, BR, and Luo, G</p>

    <p>          J Virol <b>2006</b>.  80(15): 7364-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16840317&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16840317&amp;dopt=abstract</a> </p><br />

    <p>14.   49198   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Outcome of pulmonary multidrug-resistant tuberculosis: a six year follow-up study</p>

    <p>          Chiang, CY, Enarson, DA, Yu, MC, Bai, KJ, Huang, RM, Hsu, CJ, Suo, J, and Lin, TP</p>

    <p>          Eur Respir J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16837502&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16837502&amp;dopt=abstract</a> </p><br />

    <p>15.   49199   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Antimycobacterial quaternary ammonium salts of piperidinylethyl esters of alkoxysubstituted phenylcarbamic acids</p>

    <p>          Waisser, Karel, Cizmarik, Jozef, and Kaustova, Jarmila</p>

    <p>          Folia Pharmaceutica Universitatis Carolinae <b>2006</b>.  33: 19-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   49200   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Non-nucleoside Inhibitors Binding to Hepatitis C Virus NS5B Polymerase Reveal a Novel Mechanism of Inhibition</p>

    <p>          Biswal, BK, Wang, M, Cherney, MM, Chan, L, Yannopoulos, CG, Bilimoria, D, Bedard, J, and James, MN</p>

    <p>          J Mol Biol <b>2006</b>.  361(1): 33-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828488&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828488&amp;dopt=abstract</a> </p><br />

    <p>17.   49201   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Identification and structure-based optimization of novel dihydropyrones as potent HCV RNA polymerase inhibitors</p>

    <p>          Li, H, Tatlock, J, Linton, A, Gonzalez, J, Borchardt, A, Dragovich, P, Jewell, T, Prins, T, Zhou, R, Blazel, J, Parge, H, Love, R, Hickey, M, Doan, C, Shi, S, Duggal, R, Lewis, C, and Fuhrman, S</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16824756&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16824756&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   49202   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Antimycobacterial scalarane-based sesterterpenes from the Red Sea sponge Hyrtios erecta. [Erratum to document cited in CA144:084421]</p>

    <p>          Youssef, Diaa TA, Shaala, Lamiaa A, and Emara, Samy</p>

    <p>          Journal of Natural Products <b>2006</b>.  69(1): 172</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   49203   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Antimycobacterial activity of a dry birch bark extract on a model of experimental pulmonary tuberculosis</p>

    <p>          Demikhova O V, Balakshin V V, Presnova G A, Bocharova I V, Lepekha L N, Chernousova L N, Smirnova T G, Pospelov L E, and Chistiakov A N</p>

    <p>          Problemy tuberkuleza i bolezne  legkikh <b>2006</b>.(1): 55-7.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   49204   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Cyanovirin-N inhibits hepatitis C virus entry by binding to envelope protein glycans</p>

    <p>          Helle, F, Wychowski, C, Vu-Dac, N, Gustafson, KR, Voisset, C, and Dubuisson, J</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809348&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809348&amp;dopt=abstract</a> </p><br />

    <p>21.   49205   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          TB screening and anti-TNFalpha treatment</p>

    <p>          Dhasmana, DJ, Nash, J, Bradley, JC, Creer, D, and Provenzano, G</p>

    <p>          Thorax <b>2006</b>.  61 (7): 641; author reply 641</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16807395&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16807395&amp;dopt=abstract</a> </p><br />

    <p>22.   49206   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          Identification of a respiratory-type nitrate reductase and its role for survival of Mycobacterium smegmatis in Wayne model</p>

    <p>          Khan, A and Sarkar, D</p>

    <p>          Microb Pathog <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16806798&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16806798&amp;dopt=abstract</a> </p><br />

    <p>23.   49207   OI-LS-353; PUBMED-OI-7/24/2006</p>

    <p class="memofmt1-2">          In vitro selection and in vivo efficacy of piperazine- and alkanediamide-linked bisbenzamidines against Pneumocystis pneumonia in mice</p>

    <p>          Cushion, MT, Walzer, PD, Ashbaugh, A, Rebholz, S, Brubaker, R, Vanden, Eynde JJ, Mayence, A, and Huang, TL</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(7): 2337-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801410&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801410&amp;dopt=abstract</a> </p><br />

    <p>24.   49208   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Antimycobacterial susceptibility against nontuberculous mycobacteria using brothmic NTM</p>

    <p>          Kawata, Noriko, Kawahara, Shin, Tada, Atsuhiko, Takigawa, Nagio, Shibayama, Takuo, Soda, Ryo, and Takahashi, Kiyoshi</p>

    <p>          Kekkaku <b>2006</b>.  81(4): 329-335</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   49209   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          QSAR analysis of antimycobacterial N-pyridinylsalicylamides</p>

    <p>          Waisser, Karel, Drazkova, Katerina, Matyk, Josef, and Palat, Karel</p>

    <p>          Folia Pharmaceutica Universitatis Carolinae <b>2006</b>.  33: 7-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   49210   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Development of the method of screening anti-eukaryotic microorganism agents by 14-3-3 protein subunit binding assay</p>

    <p>          Inoue, Masahiro, Doi, Hirofumi, and Saito, Seishi</p>

    <p>          PATENT:  JP <b>2006081413</b>  ISSUE DATE:  20060330</p>

    <p>          APPLICATION: 2004-5116  PP: 31 pp.</p>

    <p>          ASSIGNEE:  (Celestar Lexico-Sciences, Inc. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   49211   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Dihydrofolate reductase inhibition by epigallocatechin gallate compounds, and therapeutic use</p>

    <p>          Rodriguez-Lopez, Jose Neptuno, Navarro-Peran, Encarnacion Maria, and Cabezas-Herrera, Juan</p>

    <p>          PATENT:  WO <b>2006021888</b>  ISSUE DATE:  20060302</p>

    <p>          APPLICATION: 2005  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Universidad de Murcia, Spain</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   49212   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of novel series of 2-aroyl-6-nitrobenzoyl amino acids and peptides</p>

    <p>          Dahiya, Rajiv and Pathak, Devender</p>

    <p>          Oriental Journal of Chemistry <b>2006</b>.  22(1): 123-128</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   49213   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Examination of Mycobacterium tuberculosis sigma factor mutants using low-dose aerosol infection of guinea pigs suggests a role for SigC in pathogenesis</p>

    <p>          Karls, RK, Guarner, J, McMurray, DN, Birkness, KA, and Quinn, FD</p>

    <p>          MICROBIOLOGY-SGM <b>2006</b>.  152: 1591-1600, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238563800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238563800003</a> </p><br />

    <p>30.   49214   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Antifungal azaphilones from the fungus Chaetomium cupreum CC3003</p>

    <p>          Kanokmedhakul, S, Kanokmedhakul, K, Nasomjai, P, Louangsysouphanh, S, Soytong, K, Isobe, M, Kongsaeree, P, Prabpai, S, and Suksamrarn, A</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2006</b>.  69(6): 891-895, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238478800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238478800006</a> </p><br />
    <br clear="all">

    <p>31.   49215   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Chromone derivatives from the filamentous fungus Lachnum sp BCC 2424</p>

    <p>          Rukachaisirikul, V, Chantaruk, S, Pongcharoen, W, Isaka, M, and Lapanun, S</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2006</b>.  69(6): 980-982, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238478800025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238478800025</a> </p><br />

    <p>32.   49216   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Synthesis and antifungal properties of some benzimidazole derivatives</p>

    <p>          Ayhan Kilcigil, Gulgun and Altanlar, Nurten</p>

    <p>          Turkish Journal of Chemistry <b>2006</b>.  30(2): 223-228</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   49217   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Antimicrobial agents containing (Z)-alkylthioalkenyl isothiocyanates</p>

    <p>          Fuchigami, Takamasa, Liu, Guo Bin, Kimura, Hiroshi, and Yoshimura, Kenji</p>

    <p>          PATENT:  JP <b>2006143672</b>  ISSUE DATE:  20060608</p>

    <p>          APPLICATION: 2004-9913  PP: 7 pp.</p>

    <p>          ASSIGNEE:  (Chemicrea Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   49218   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Synthesis, characterization, and antimicrobial studies of newly developed metal-chelated epoxy resins</p>

    <p>          Nishat, N, Ahmad, S, and Ahamad, T</p>

    <p>          JOURNAL OF APPLIED POLYMER SCIENCE <b>2006</b>.  101(3): 1347-1355, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238637300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238637300014</a> </p><br />

    <p>35.   49219   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Synthetic Porcine Lactoferricin with a 20-Residue Peptide Exhibits Antimicrobial Activity against Escherichia coli, Staphylococcus aureus, and Candida albicans</p>

    <p>          Chen, Hsiao-Ling, Yen, Chih-Ching, Lu, Chien-Yu, Yu, Chia-Hen, and Chen, Chuan-Mu</p>

    <p>          Journal of Agricultural and Food Chemistry <b>2006</b>.  54(9): 3277-3282</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   49220   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Glycobiology against viruses: antiviral drug discovery</p>

    <p>          Zitzmann, Nicole, O&#39;Leary, Joanne M, and Dwek, Raymond A</p>

    <p>          Biochemist <b>2006</b>.  28(3): 23-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   49221   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Preparation of the antiviral compound 5-[[3-(2,4-trifluoromethyphenyl)isoxazol-5-yl]methyl]-2-(2-fluorophenyl)-5H-imidazo[4,5-c]pyridine and its use in the treatment of HCV viral infections</p>

    <p>          Bondy, Steven S, Oare, David A, and Tse, Winston C</p>

    <p>          PATENT:  WO <b>2006069193</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2005  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>38.   49222   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Synthesis and antifungal activities of some aryl [3-(imidazol-1-yl/triazol-1-ylmethyl) benzofuran-2-yl] ketoximes</p>

    <p>          Gundogdu-Karaburun, N, Benkli, K, Tunali, Y, Ucucu, U, and Demirayak, S</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(5): 651-656, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238460300012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238460300012</a> </p><br />

    <p>39.   49223   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Preparation of pyridines and pyrimidines as inhibitors of HCV RNA polymerases for treating liver diseases</p>

    <p>          Wobbe, CRichard</p>

    <p>          PATENT:  WO <b>2006065590</b>  ISSUE DATE:  20060622</p>

    <p>          APPLICATION: 2005  PP: 42 pp.</p>

    <p>          ASSIGNEE:  (Xtl Biopharmaceuticals Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   49224   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Synthesis and studies on some new fluorine containing triazolothiadiazines as possible antibacterial, antifungal and anticancer agents</p>

    <p>          Holla, BS, Rao, BS, Sarojini, BK, Akberali, PM, and Kumari, NS</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(5): 657-663, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238460300013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238460300013</a> </p><br />

    <p>41.   49225   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Preparation of cyclobutyl nucleosides for use in the treatment of infections including Retroviridae, Hepadnaviridae, or Flaviviridae in animals and humans</p>

    <p>          Liotta, Dennis C, Mao, Shuli, and Hager, Michael</p>

    <p>          PATENT:  WO <b>2006063281</b>  ISSUE DATE:  20060615</p>

    <p>          APPLICATION: 2005  PP: 138 pp.</p>

    <p>          ASSIGNEE:  (Emory University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   49226   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Preparation of Nucleotides analogs and their use as antiviral agents and inhibitors of RNA viral polymerases</p>

    <p>          Babu, Yarlagadda S, Chand, Pooran, El-Kattan, Yahya, and Wu, Minwan</p>

    <p>          PATENT:  US <b>2006122391</b>  ISSUE DATE:  20060608</p>

    <p>          APPLICATION: 2005-7675  PP: 48 pp., Cont.-in-part of U.S. Ser. No. 437,179.</p>

    <p>          ASSIGNEE:  (Biocryst Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   49227   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Synthesis, stereochemistry, and antimicrobial activity of 2,6-diaryl-3-(arylthio)piperidin-4-ones</p>

    <p>          Srinivasan, M, Perumal, S, and Selvaraj, S</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2006</b>.  54(6): 795-801, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238470200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238470200006</a> </p><br />
    <br clear="all">

    <p>44.   49228   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Preparation of pyrazoles and [1,2,4]triazoles as antiviral agents</p>

    <p>          Shipps, Gerald W Jr, Wang, Tong, Rosner, Kristen E, Curran, Patrick J, Cooper, Alan B, and Girijavallabhan, Viyyoor Moopil</p>

    <p>          PATENT:  WO <b>2006050035</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 80 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>45.   49229   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Preparation of therapeutic furopyrimidine and thienopyrimidine nucleosides as antitumor and antiviral agents</p>

    <p>          Babu, Yarlagadda S, Chand, Pooran, Wu, Minwan, Kotian, Pravin L, Kumar, VSatish, Lin, Tsu-Hsing, El-Kattan, Yahya, and Ghosh, Ajit K</p>

    <p>          PATENT:  WO <b>2006050161</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 152 pp.</p>

    <p>          ASSIGNEE:  (Biocryst Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   49230   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;-O-methyl-5-alkynyl and alkenyl substituted uridine derivatives to screen for inhibitors of HCV</p>

    <p>          Ding, Yili, Girardet, Jean-Luc, Hong, Zhi, Shaw, Stephanie Z, and Yao, Nanhua</p>

    <p>          Heterocycles <b>2006</b>.  68(3): 521-530</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>47.   49231   OI-LS-353; SCIFINDER-OI-7/17/2006-SEARCH</p>

    <p class="memofmt1-2">          A phase 2 study to evaluate the antiviral activity, safety, and pharmacokinetics of recombinant human albumin-interferon alfa fusion protein in genotype 1 chronic hepatitis C patients</p>

    <p>          Bain, Vincent G, Kaita, Kelly D, Yoshida, Eric M, Swain, Mark G, Heathcote, EJenny, Neumann, Avidan U, Fiscella, Michele, Yu, Ren, Osborn, Blaire L, Cronin, Patrick W, Freimuth, William W, McHutchison, John G, and Subramanian, GMani</p>

    <p>          Journal of Hepatology <b>2006</b>.  44(4): 671-678</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>48.   49232   OI-LS-353; WOS-OI-7/16/2006</p>

    <p class="memofmt1-2">          Potent twice-weekly rifapentine-containing regimens in murine tuberculosis</p>

    <p>          Rosenthal, IM, Williams, K, Tyagi, S, Peloquin, CA, Vernon, AA, Bishai, WR, Grosset, JH, and Nuermberger, EL</p>

    <p>          AMERICAN JOURNAL OF RESPIRATORY AND CRITICAL CARE MEDICINE <b>2006</b>.  174(1): 94-101, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238502600015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238502600015</a> </p><br />

    <p>49.   49233   OI-LS-353; WOS-OI-7/23/2006</p>

    <p class="memofmt1-2">          Hijacking of a substrate-binding protein scaffold for use in mycobacterial cell wall biosynthesis</p>

    <p>          Marland, Z, Beddoe, T, Zaker-Tabrizi, L, Lucet, IS, Brammananth, R, Whisstock, JC, Wilce, MCJ, Coppel, RL, Crellin, PK, and Rossjohn, J</p>

    <p>          JOURNAL OF MOLECULAR BIOLOGY <b>2006</b>.  359(4): 983-997, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238682800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238682800014</a> </p><br />
    <br clear="all">

    <p>50.   49234   OI-LS-353; WOS-OI-7/23/2006</p>

    <p class="memofmt1-2">          Antibacterial, antifungal and cytotoxic properties of some sulfonamide-derived chromones</p>

    <p>          Chohan, ZH, Rauf, A, Naseer, MM, Somra, MA, and Supuran, CT</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2006</b>.  21(2): 173-177, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238761100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238761100007</a> </p><br />

    <p>51.   49235   OI-LS-353; WOS-OI-7/23/2006</p>

    <p class="memofmt1-2">          Characterization of mRNA interferases from Mycobacterium tuberculosis</p>

    <p>          Zhu, L, Zhang, YL, Teh, JS, Zhang, JJ, Connell, N, Rubin, H, and Inouye, M</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2006</b>.  281(27): 18638-18643, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238687300043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238687300043</a> </p><br />

    <p>52.   49236   OI-LS-353; WOS-OI-7/23/2006</p>

    <p class="memofmt1-2">          Evaluation of the resazurin microtiter assay for rapid detection of of loxacin resistance in M-tuberculosis</p>

    <p>          Umubyeyi, AN, Martin, A, Zissis, G, Struelens, M, Karita, E, and Portaels, F</p>

    <p>          INTERNATIONAL JOURNAL OF TUBERCULOSIS AND LUNG DISEASE <b>2006</b>.  10(7): 808-811, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238718400017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238718400017</a> </p><br />

    <p>53.   49237   OI-LS-353; WOS-OI-7/23/2006</p>

    <p class="memofmt1-2">          Lamivudine enabled isoniazid and rifampicin treatment in pulmonary tuberculosis and hepatitis B co-infection</p>

    <p>          Yu, WC, Lai, ST, Chiu, MC, Chau, TN, Ng, TK, and Tam, CM</p>

    <p>          INTERNATIONAL JOURNAL OF TUBERCULOSIS AND LUNG DISEASE <b>2006</b>.  10(7): 824-825, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238718400022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238718400022</a> </p><br />

    <p>54.   49238   OI-LS-353; WOS-OI-7/23/2006</p>

    <p class="memofmt1-2">          Cathepsin L maturation and activity is impaired in macrophages harboring M-avium and M-tuberculosis</p>

    <p>          Nepal, RM, Mampe, S, Shaffer, B, Erickson, AH, and Bryant, P</p>

    <p>          INTERNATIONAL IMMUNOLOGY <b>2006</b>.  18(6): 931-939, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238769400011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238769400011</a> </p><br />

    <p>55.   49239   OI-LS-353; WOS-OI-7/23/2006</p>

    <p class="memofmt1-2">          Inhibitors of HCVNS5B polymerase: Synthesis and structure-activity relationships of N-1-benzyl and N-1-[3-methylbutyl]-4-hydroxy-1,8-naphthyridon-3-yl benzothiadiazine analogs containing substituents on the aromatic ring</p>

    <p>          Rockway, TW, Zhang, R, Liu, D, Betebenner, DA, McDaniel, KF, Pratt, JK, Beno, D, Montgomery, D, Jiang, WW, Masse, S, Kati, WM, Middleton, T, Molla, A, Maring, CJ, and Kempf, DJ</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(14): 3833-3838, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238698400044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238698400044</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
