

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-06-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="rhNENR1QkdqVx7GGfpgsgsCw8z+tmdi2MCSin6NTCdF2CS+C09TAtkeOWMwbf110K5yNW4GzhEAdM+YQdBySuVq3WJLc34BAuMUjDjIovzdo8Qh9VY5zBLX30dc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E47E950E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">June 10 - June 23, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Epstein-Barr virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21670518">Synthesis and Biological Evaluation of the 12,12-Dimethyl Derivative of Aplog-1, an Anti-Proliferative Analog of Tumor-Promoting Aplysiatoxin</a>. Nakagawa, Y., M. Kikumori, R.C. Yanagita, A. Murakami, H. Tokuda, H. Nagai, and K. Irie. Bioscience, Biotechnology, and Biochemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21670518].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0610-062311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21692941">Interference of Hepatitis C Virus Replication in Cell Culture by Antisense Peptide Nucleic Acids Targeting the X-Rna</a>. Ahn, D.G., S.B. Shim, J.E. Moon, J.H. Kim, S.J. Kim, and J.W. Oh. Journal of Viral Hepatitis, 2011. 18(7): p. e298-306; PMID[21692941].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21671627">Correction to 3&#39;-Deoxy Phosphoramidate Dinucleosides as Improved Inhibitors of Hepatitis C Virus Subgenomic Replicon and Ns5b Polymerase Activity</a>. Priet, S., I. Zlatev, I. Barvik, K. Geerts, P. Leyssen, J. Neyts, H. Dutartre, B. Canard, J.J. Vasseur, F. Morvan, and K. Alvarez. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21671627].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21663667">Effect of Combined Sirna of HCV E2 Gene and HCV Receptors against HCV</a>. Jahan, S., S. Khaliq, B. Samreen, B. Ijaz, M. Khan, W. Ahmad, U.A. Ashfaq, and S. Hassan. Virology Journal, 2011. 8(1): p. 295; PMID[21663667].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0610-062311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes Simplex virus</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21664010">Synthesis and Antiviral Evaluation of Alpha-L-2&#39;-Deoxythreofuranosyl Nucleosides</a>. Toti, K.S., M. Derudas, C. McGuigan, J. Balzarini, and S. Van Calenbergh. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21664010].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0610-062311.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291145900033">Indole 5-Carboxamide Thumb Pocket I Inhibitors of HCV Ns5b Polymerase with Nanomolar Potency in Cell-Based Subgenomic Replicons (Part 2): Central Amino Acid Linker and Right-Hand-Side Sar Studies</a>. Beaulieu, P.L., C. Chabot, J.M. Duan, M. Garneau, J. Gillard, E. Jolicoeur, M. Poirier, M.A. Poupart, T.A. Stammers, G. Kukolj, and Y.S. Tsantrizos. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(12): p. 3664-3670; ISI[000291145900033].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291145900032">From Benzimidazole to Indole-5-Carboxamide Thumb Pocket I Inhibitors of HCV Ns5b Polymerase. Part 1: Indole C-2 Sar and Discovery of Diamide Derivatives with Nanomolar Potency in Cell-Based Subgenomic Replicons</a>. Beaulieu, P.L., J. Gillard, E. Jolicoeur, J.M. Duan, M. Garneau, G. Kukolj, and M.A. Poupart. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(12): p. 3658-3663; ISI[000291145900032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500113">Nitazoxanide Is an Indirect Inhibitor of HCV Replication through Modulation of Cellular Kinase Cki Alpha to Enhance HCV Ns5a Hyperphosphorylation</a>. Montero, A., P. Viswanathan, C. Yon, and B. Korba. Conference, Antiviral research, May 2011. 90: p. A59-A59; ISI[000291182500113].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500115">Inhibition of Human Cytomegalovirus Replication by Tricin (4 &#39;,5,7-Trihydroxy-3 &#39;,5 &#39;-Dimethoxyflavone)</a>. Murayama, T., Y. Li, R. Yamada, H. Sadanari, K. Matsubara, Y. Tuchida, M. Koketsu, and K. Watanabe. Conference, Antiviral research, May 2011. 90: p. A60-A60; ISI[000291182500115].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500119">Triterpenoids from Platycodon Grandiflorum Inhibit Hepatitis C Virus Replication</a>. Park, S.J., J.H. Lim, J.W. Yang, J.C. Shin, E.J. Kim, J.W. Suh, S.B. Hwang, and J.W. Kim. Conference, Antiviral research, May 2011. 90: p. A61-A62; ISI[000291182500119].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500010">Cyclopropavir Inhibits the Normal Function of the Human Cytomegalovirus Ul97 Kinase</a>. Prichard, M., C. Hartline, R. Gill, T. Bowlin, and S. James. Conference, Antiviral research, May 2011. 90: p. A24-A24; ISI[000291182500010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500156">Phosphoramidate Dinucleosides as Inhibitors of Hepatitis C Virus Subgenomic Replicon and Ns5b Polymerase Activity</a>. Priet, S., I. Zlatev, I. Barvik, J. Neyts, H. Dutartre, B. Canard, F. Morvan, and K. Alvarez. Conference, Antiviral research, May 2011. 90: p. A75-A75; ISI[000291182500156].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291118600038">Facile Transformation of Biginelli Pyrimidin-2(1h)-Ones to Pyrimidines. In vitro Evaluation as Inhibitors of Mycobacterium Tuberculosis and Modulators of Cytostatic Activity</a>. Singh, K., B.J. Wan, S. Franzblau, K. Chibale, and J. Balzarini. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2290-2294; ISI[000291118600038].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291341800010">Grape Seed Extract for Control of Human Enteric Viruses</a>. Su, X.W. and D.H. D&#39;Souza. Applied and Environmental Microbiology, 2011. 77(12): p. 3982-3987; ISI[000291341800010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500163">Synthesis and Antiviral Activity of 3-Methoxy-2-(Phosphonomethoxy)propyl Nucleoside Esters against HCV and HIV-1</a>. Valiaeva, N., J.R. Beadle, D.L. Wyles, R.T. Schooley, and K.Y. Hostetler. Conference, Antiviral research, May 2011. 90: p. A77-A77; ISI[000291182500163].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500129">In Vitro Combination Therapy with Tegobuvir (Gs-9190) Is Highly Efficient in Curing Cells from HCV Replicon and in Delaying/Preventing the Development of Antiviral Resistance</a>. Vliegen, I., J. Paeshuyse, I.H. Shih, G. Purstinger, S. Bondy, W.A. Lee, W.D. Zhong, and J. Neyts. Conference, Antiviral research, May 2011. 90: p. A65-A65; ISI[000291182500129].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0610-062311.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
