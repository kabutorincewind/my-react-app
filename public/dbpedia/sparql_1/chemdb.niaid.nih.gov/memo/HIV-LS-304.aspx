

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-304.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Ugp7MVbVgqHY1GIJStD7PdMpc/gjbDOlL0SeG5A2BDMSIYRJ522XQzdjN/U2PF2qTdSjNEUvCgoyY2N+qfcoLRPse0P25iznvhGMfo8ENeMaGiYj1lFJzGZPhqc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4A28A9A1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-304-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         43989   HIV-LS-304; EMBASE-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Tat-dependent repression of human immunodeficiency virus type 1 long terminal repeat promoter activity by fusion of cellular transcription factors,</p>

    <p>          Zhao, Cunyou, Chen, Yali, Park, Jiyoung, Kim, Jae Bum, and Tang, Hong</p>

    <p>          Biochemical and Biophysical Research Communications <b>2004</b>.  322(2): 614-622</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4D4M825-4/2/136f93a677d05d4659bcdf31865f666f">http://www.sciencedirect.com/science/article/B6WBK-4D4M825-4/2/136f93a677d05d4659bcdf31865f666f</a> </p><br />

    <p>2.         43990   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Inhibition of the strand transfer step of HIV-1 integrase by non-natural dinucleotides</p>

    <p>          Chi, G, Neamati, N, and Nair, V</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(19): 4815-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15341930&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15341930&amp;dopt=abstract</a> </p><br />

    <p>3.         43991   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 envelope glycoprotein-mediated cell fusion by a D,L-amino acid-containing fusion peptide: Possible recognition of the fusion complex</p>

    <p>          Gerber, D, Pritsker, M, Gunther-Ausborn, S, Johnson, B, Blumenthal, R, and Shai, Y</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15339935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15339935&amp;dopt=abstract</a> </p><br />

    <p>4.         43992   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Anti-HIV and cytotoxic ruthenium(II) complexes containing flavones: biochemical evaluation in mice</p>

    <p>          Mishra, L, Singh, AK, Trigun, SK, Singh, SK, and Pandey, SM</p>

    <p>          Indian J Exp Biol <b>2004</b>.  42(7): 660-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15339029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15339029&amp;dopt=abstract</a> </p><br />

    <p>5.         43993   HIV-LS-304; EMBASE-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Relative replication fitness of multi-nucleoside analogue-resistant HIV-1 strains bearing a dipeptide insertion in the fingers subdomain of the reverse transcriptase and mutations at codons 67 and 215</p>

    <p>          Prado, Julia G, Franco, Sandra, Matamoros, Tania, Ruiz, Lidia, Clotet, Bonaventura, Menendez-Arias, Luis, Martinez, Miguel Angel, and Martinez-Picado, Javier</p>

    <p>          Virology <b>2004</b>.  326(1): 103-112</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4CS4GSD-6/2/7def68f8f66d12d6e200f09a3713a8ff">http://www.sciencedirect.com/science/article/B6WXR-4CS4GSD-6/2/7def68f8f66d12d6e200f09a3713a8ff</a> </p><br />
    <br clear="all">

    <p>6.         43994   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Neamphamide A, a New HIV-Inhibitory Depsipeptide from the Papua New Guinea Marine Sponge Neamphius huxleyi</p>

    <p>          Oku, N, Gustafson, KR, Cartner, LK, Wilson, JA, Shigematsu, N, Hess, S, Pannell, LK, Boyd, MR, and McMahon, JB</p>

    <p>          J Nat Prod <b>2004</b>.  67(8): 1407-1411</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332865&amp;dopt=abstract</a> </p><br />

    <p>7.         43995   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 Envelope-Mediated Fusion by Synthetic Batzelladine Analogues</p>

    <p>          Bewley, CA, Ray, S, Cohen, F, Collins, SK, and Overman, LE</p>

    <p>          J Nat Prod <b>2004</b>.  67(8): 1319-1324</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332849&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332849&amp;dopt=abstract</a> </p><br />

    <p>8.         43996   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Effects of the {Delta}67 Complex of Mutations in Human Immunodeficiency Virus Type 1 Reverse Transcriptase on Nucleoside Analog Excision</p>

    <p>          Boyer, PL, Imamichi, T, Sarafianos, SG, Arnold, E, and Hughes, SH</p>

    <p>          J Virol <b>2004</b>.  78(18): 9987-9997</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15331732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15331732&amp;dopt=abstract</a> </p><br />

    <p>9.         43997   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Improved antiviral activity of the aryloxymethoxyalaninyl phosphoramidate (APA) prodrug of abacavir (ABC) is due to the formation of markedly increased carbovir 5 [Formula: see text] -triphosphate metabolite levels</p>

    <p>          Balzarini, J, Aquaro, S, Hassan-Abdallah, A, Daluge, SM, Perno, CF, and McGuigan, C</p>

    <p>          FEBS Lett <b>2004</b>.  573(1-3): 38-44</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15327972&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15327972&amp;dopt=abstract</a> </p><br />

    <p>10.       43998   HIV-LS-304; EMBASE-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Effects of the G190A substitution of HIV reverse transcriptase on phenotypic susceptibility of patient isolates to delavirdine</p>

    <p>          Uhlmann, Erik J, Tebas, Pablo, Storch, Gregory A, Powderly, William G, Lie, Yolanda S, Whitcomb, Jeannette M, Hellmann, Nicholas S, and Arens, Max Q</p>

    <p>          Journal of Clinical Virology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4CGNTGV-2/2/7b64e83e1f78b693752b7c5450191925">http://www.sciencedirect.com/science/article/B6VJV-4CGNTGV-2/2/7b64e83e1f78b693752b7c5450191925</a> </p><br />
    <br clear="all">

    <p>11.       43999   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Interactions among human immunodeficiency virus (HIV)-1, interferon-gamma and receptor of activated NF-kappa B ligand (RANKL): implications for HIV pathogenesis</p>

    <p>          Fakruddin, JM and Laurence, J</p>

    <p>          Clin Exp Immunol <b>2004</b>.  137(3): 538-45</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320903&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320903&amp;dopt=abstract</a> </p><br />

    <p>12.       44000   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Glycosidase inhibitors as potential HIV entry inhibitors?</p>

    <p>          Robina, I, Moreno-Vargas, AJ, Carmona, AT, and Vogel, P</p>

    <p>          Curr Drug Metab <b>2004</b>.  5(4): 329-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320705&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320705&amp;dopt=abstract</a> </p><br />

    <p>13.       44001   HIV-LS-304; PUBMED-HIV-9/7/2004</p>

    <p class="memofmt1-2">          Mechanism of inhibition of the human immunodeficiency virus type 1 by the oxygen radical generating agent bleomycin</p>

    <p>          Georgiou, NA,  Van Der Bruggen, T, Oudshoorn, M, De Bie, P, Jansen, CA, Nottet, HS, Marx, JJ, and Van Asbeck, BS</p>

    <p>          Antiviral Res <b>2004</b>.  63(2): 97-106</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302138&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302138&amp;dopt=abstract</a> </p><br />

    <p>14.       44002   HIV-LS-304; WOS-HIV-9/2/2004</p>

    <p class="memofmt1-2">          A new anti-HIV alkaloid, drymaritin, and a new C-glycoside flavonoid, diandraflavone, from Drymaria diandra</p>

    <p>          Hsieh, PW, Chang, FR, Lee, KH, Hwang, TL, Chang, SM, and Wu, YC</p>

    <p>          J NAT PROD <b>2004</b>.  67(7): 1175-1177</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222901000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222901000021</a> </p><br />

    <p> </p>

    <p>15.       44003   HIV-LS-304; WOS-HIV-9/2/2004</p>

    <p class="memofmt1-2">          4,4-Disubstituted-3,4-dihydro-2(1H)-quinazolinonesas HIV reverse transcriptase inhibitors - Bristol-Myers Squibb Co.: WO04013110</p>

    <p>          Anon</p>

    <p>          EXPERT OPIN THER PAT <b>2004</b>.  14(8): 1237-1240</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222965300008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222965300008</a> </p><br />

    <p>16.       44004   HIV-LS-304; WOS-HIV-9/2/2004</p>

    <p class="memofmt1-2">          Inhibition of the functions of the nucleocapsid protein of human immunodeficiency virus-1 by an RNA aptamer</p>

    <p>          Kim, MY and Jeong, S</p>

    <p>          BIOCHEM BIOPH RES CO <b>2004</b>.  320(4): 1181-1186</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222923400020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222923400020</a> </p><br />

    <p>17.       44005   HIV-LS-304; WOS-HIV-9/2/2004</p>

    <p class="memofmt1-2">          Estimation of serum-free 50-percent inhibitory concentrations for human immunodeficiency virus protease inhibitors lopinavir and ritonavir</p>

    <p>          Hickman, D, Vasavanonda, S, Nequist, G, Colletti, L, Kati, WM, Bertz, R, Hsu, A, and Kempf, DJ </p>

    <p>          ANTIMICROB AGENTS CH <b>2004</b>.  48(8): 2911-2917</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300019</a> </p><br />

    <p>18.       44006   HIV-LS-304; WOS-HIV-9/2/2004</p>

    <p class="memofmt1-2">          Inhibition of herpesvirus-induced HIV-1 replication by cyclopentenone prostaglandins: role of I kappa B kinase (IKK)</p>

    <p>          Amici, C, Belardo, G, Rozera, C, Bernasconi, D, and Santoro, MG</p>

    <p>          AIDS <b>2004</b>.  18 (9): 1271-1280</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222894700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222894700005</a> </p><br />

    <p>19.       44007   HIV-LS-304; WOS-HIV-9/5/2004</p>

    <p class="memofmt1-2">          Classical QSAR modeling of CCR5 receptor binding affinity of substituted benzylpyrazoles</p>

    <p>          Leonard, JT and Roy, K</p>

    <p>          QSAR COMB SCI <b>2004</b>.  23(6): 387-398</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223150000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223150000001</a> </p><br />

    <p>20.       44008   HIV-LS-304; WOS-HIV-9/5/2004</p>

    <p class="memofmt1-2">          Prediction of carbonic anhydrase activation by tri-/tetrasubstituted-pyridinium-azole compounds: A computational approach using novel topochemical descriptor</p>

    <p>          Bajaja, S, Sambi, SS, and Madan, AK</p>

    <p>          QSAR COMB SCI <b>2004</b>.  23(6): 431-439</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223150000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223150000006</a> </p><br />

    <p>21.       44009   HIV-LS-304; WOS-HIV-9/5/2004</p>

    <p class="memofmt1-2">          RNA interference directed against poly(ADP-Ribose) polymerase 1 efficiently suppresses human immunodeficiency virus type 1 replication in human cells</p>

    <p>          Kameoka, M, Nukuzuma, S, Itaya, A, Tanaka, Y, Ota, K, Ikuta, K, and Yoshihara, K</p>

    <p>          J VIROL <b>2004</b>.  78(16): 8931-8934</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223046000054">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223046000054</a> </p><br />

    <p>22.       44010   HIV-LS-304; WOS-HIV-9/5/2004</p>

    <p class="memofmt1-2">          Susceptibility of HIV type I to the fusion inhibitor T-20 is reduced on insertion of host intercellular adhesion molecule 1 in the virus membrane</p>

    <p>          Beausejour, Y and Tremblay, MJ</p>

    <p>          J INFECT DIS <b>2004</b>.  190(5): 894-902</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223114800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223114800004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
