

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-365.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Yiy9fXk8fHDH7YtdGwx48sGR7/x6vANmjzurkZ9cSkn2W1L9pCEKXVrg/oKsTaWwAC3IYUj0jDlQuKI/PvzqFaegXE6DqwOXLKsH3Fi4SbW0eKGi04DLprxbwYQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="38556535" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-365-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59889   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Pegylated arginine deiminase lowers hepatitis C viral titers and inhibits nitric oxide synthesis</p>

    <p>          Izzo, F, Montella, M, Orlando, AP, Nasti, G, Beneduce, G, Castello, G, Cremona, F, Ensor, CM, Holtzberg, FW, Bomalaski, JS, Clark, MA, Curley, SA, Orlando, R, Scordino, F, and Korba, BE</p>

    <p>          J Gastroenterol Hepatol <b>2007</b>.  22(1): 86-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17201887&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17201887&amp;dopt=abstract</a> </p><br />

    <p>2.     59890   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Isoniazid&#39;s Bactericidal Activity Ceases because of the Emergence of Resistance, Not Depletion of Mycobacterium tuberculosis in the Log Phase of Growth</p>

    <p>          Gumbo Tawanda, Louie Arnold, Liu Weiguo, Ambrose Paul G, Bhavnani Sujata M, Brown David, and Drusano George L</p>

    <p>          J Infect Dis <b>2006</b>.  195(2): 194-201.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     59891   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Discovery of uracil-based histone deacetylase inhibitors able to reduce acquired antifungal resistance and trailing growth in Candida albicans</p>

    <p>          Mai, A, Rotili, D, Massa, S, Brosch, G, Simonetti, G, Passariello, C, and Palamara, AT</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17196388&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17196388&amp;dopt=abstract</a> </p><br />

    <p>4.     59892   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Bioactive constituents from Turkish Pimpinella species</p>

    <p>          Tabanca, N, Bedir, E, Ferreira, D, Slade, D, Wedge, DE, Jacob, MR, Khan, SI, Kirimer, N, Baser, KH, and Khan, IA</p>

    <p>          Chem Biodivers  <b>2005</b>.  2(2): 221-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191975&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191975&amp;dopt=abstract</a> </p><br />

    <p>5.     59893   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Bioactive deoxypreussomerins and dimeric naphthoquinones from Diospyros ehretioides fruits: deoxypreussomerins may not be plant metabolites but may be from fungal epiphytes or endophytes</p>

    <p>          Prajoubklang, A, Sirithunyalug, B, Charoenchai, P, Suvannakad, R, Sriubolmas, N, Piyamongkol, S, Kongsaeree, P, and Kittakoop, P</p>

    <p>          Chem Biodivers  <b>2005</b>.  2(10): 1358-67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191937&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191937&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59894   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Talosins A and B: new isoflavonol glycosides with potent antifungal activity from Kitasatospora kifunensis MJM341. I. Taxonomy, fermentation, isolation, and biological activities</p>

    <p>          Yoon, TM, Kim, JW, Kim, JG, Kim, WG, and Suh, JW</p>

    <p>          J Antibiot (Tokyo) <b>2006</b>.  59(10): 633-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191678&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191678&amp;dopt=abstract</a> </p><br />

    <p>7.     59895   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Growth of Mycobacterium avium subps. paratuberculosis in the presence of hexadecylpyridinium chloride, natamycin, and vancomycin</p>

    <p>          Johansen, Kristine A, Hugen, Erin E, and Payeur, Janet B</p>

    <p>          Journal of Food Protection <b>2006</b>.  69(4): 878-883</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     59896   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Viral Gene Expression in HIV Transgenic Mice Is Activated by Mycobacterium tuberculosis and Suppressed after Antimycobacterial Chemotherapy</p>

    <p>          Scanga, CA, Bafica, A, and Sher, A</p>

    <p>          J Infect Dis <b>2007</b>.  195(2): 246-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191170&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191170&amp;dopt=abstract</a> </p><br />

    <p>9.     59897   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Antimicrobial activity of diterpenoids from hairy roots of Salvia sclarea L.: Salvipisone as a potential anti-biofilm agent active against antibiotic resistant Staphylococci</p>

    <p>          Kuzma, L, Rozalski, M, Walencka, E, Rozalska, B, and Wysokinska, H</p>

    <p>          Phytomedicine <b>2007</b>.  14(1): 31-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17190643&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17190643&amp;dopt=abstract</a> </p><br />

    <p>10.   59898   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of thiazolidine-2-one 1,1-dioxide as inhibitors of Escherichia coli beta-ketoacyl-ACP-synthase III (FabH)</p>

    <p>          Alhamadsheh, MM, Waters, NC, Huddler, DP, Kreishman-Deitrick, M, Florova, G, and Reynolds, KA</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17189694&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17189694&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59899   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Aspergillus fumigatus does not require fatty acid metabolism via isocitrate lyase for development of invasive aspergillosis</p>

    <p>          Schobel, F, Ibrahim-Granet, O, Ave, P, Latge, JP, Brakhage, AA, and Brock, M</p>

    <p>          Infect Immun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178786&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178786&amp;dopt=abstract</a> </p><br />

    <p>12.   59900   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Diterpenoid alkaloids from some Turkish Consolida species and their antiviral activities</p>

    <p>          Sener, Bilge, Orhan, Ilkay, and Ozcelik, Berrin</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2007</b>.(7): 265-272 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   59901   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Antiprotozoal and antimicrobial activities of O-alkylated and formylated acylphloroglucinols</p>

    <p>          Bharate, Sandip B, Khan, Shabana I, Yunus, Nafees AM, Chauthe, Siddheshwar K, Jacob, Melissa R, Tekwani, Babu L, Khan, Ikhlas A, and Singh, Inder Pal</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(1): 87-96</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   59902   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Anethole, a potential antimicrobial synergist, converts a fungistatic dodecanol to a fungicidal agent</p>

    <p>          Fujita Ken-Ichi, Fujita Tomoko, and Kubo Isao</p>

    <p>          Phytother Res <b>2006</b>.  21(1): 47-51.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   59903   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          X-Ray Crystal Structure of Mycobacterium tuberculosis beta-Ketoacyl Acyl Carrier Protein Synthase II (mtKasB)</p>

    <p>          Sridharan, S, Wang, L, Brown, AK, Dover, LG, Kremer, L, Besra, GS, and Sacchettini, JC</p>

    <p>          J Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17174327&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17174327&amp;dopt=abstract</a> </p><br />

    <p>16.   59904   OI-LS-365; PUBMED-OI-1/8/2007</p>

    <p class="memofmt1-2">          Annexin 2-mediated enhancement of cytomegalovirus infection opposes inhibition by annexin 1 or annexin 5</p>

    <p>          Derry, MC, Sutherland, MR, Restall, CM, Waisman, DM, and Pryzdial, EL</p>

    <p>          J Gen Virol <b>2007</b>.  88(Pt 1): 19-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17170432&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17170432&amp;dopt=abstract</a> </p><br />

    <p>17.   59905   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Antifungal activity of ozonized olive oil (Oleozone)</p>

    <p>          Geweely, Neveen SI</p>

    <p>          International Journal of Agriculture and Biology <b>2006</b>.  8(5): 670-675</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   59906   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          In vitro and in vivo activities of HQQ-3, a new triazole antifungal agent</p>

    <p>          Zhao, Jing-Xia, Cao, Ying-Ying, Quan, Hua, Liu, Chao-Mei, He, Qiu-Qing, Wu, Qiu-Ye, Gao, Ping-Hui, Cao, Yong-Bing, Liu, Wen-Xia, and Jiang, Yuan-Ying</p>

    <p>          Biological &amp; Pharmaceutical Bulletin <b>2006</b>.  29(10): 2031-2034</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   59907   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Antimicrobial and antioxidant activities of the methanolic extracts of three Salvia species from Tunisia</p>

    <p>          Salah, Karima Bel Hadj, Ali Mahjoub, Mohamed, Ammar, Samia, Michel, Laura, Millet-Clerc, Joelle, Chaumont, Jean Pierre, Mighri, Zine, and Aouni, Mahjoub</p>

    <p>          Natural Product Research, Part A: Structure and Synthesis <b>2006</b>.  20(12): 1110-1120</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   59908   OI-LS-365; WOS-OI-12/31/2006</p>

    <p class="memofmt1-2">          Antifungal property and phytochemical screening of the crude extracts of Funtumia elastica and Mallotus oppositifolius</p>

    <p>          Adekunle, AA and Ikumapayi, AM</p>

    <p>          WEST INDIAN MEDICAL JOURNAL <b>2006</b>.  55(4): 219-223, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242632300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242632300003</a> </p><br />

    <p>21.   59909   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Synthesis, structure and biological activity of cobalt(II) and copper(II) complexes of valine-derived Schiff bases</p>

    <p>          Lv, Jian, Liu, Tingting, Cai, Sulan, Wang, Xin, Liu, Lei, and Wang, Yongmei</p>

    <p>          Journal of Inorganic Biochemistry <b>2006</b>.  100(11): 1888-1896</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   59910   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Table Olives from Portugal: Phenolic Compounds, Antioxidant Potential, and Antimicrobial Activity</p>

    <p>          Pereira, Jose Alberto, Pereira, Ana PG, Ferreira, Isabel CFR, Valentao, Patricia, Andrade, Paula B, Seabra, Rosa, Estevinho, Leticia, and Bento, Albino</p>

    <p>          Journal of Agricultural and Food Chemistry <b>2006</b>.  54(22): 8425-8431</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   59911   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          2,6-hexadecadiynoic acid and 2,6-nonadecadiynoic acid: novel synthesized acetylenic fatty acids as potent antifungal agents</p>

    <p>          Carballeira Nestor M, Sanabria David, Cruz Clarisa, Parang Keykavous, Wan Baojie, and Franzblau Scott</p>

    <p>          Lipids <b>2006</b>.  41 (5): 507-11.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   59912   OI-LS-365; WOS-OI-12/31/2006</p>

    <p class="memofmt1-2">          Gene essentiality in Aspergillus fumigatus</p>

    <p>          Turner, G, Allen, GJ, Keszenman-Pereyra, D, and Bromley, MJ</p>

    <p>          MEDICAL MYCOLOGY <b>2006</b>.  44: S87-S90, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242601400016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242601400016</a> </p><br />
    <br clear="all">

    <p>25.   59913   OI-LS-365; WOS-OI-12/31/2006</p>

    <p class="memofmt1-2">          Synthesis and selective antitubercular and antimicrobial inhibitory activity of 1-acetyl-3,5-diphenyl-4,5-dihydro-(1H)-pyrazole derivatives</p>

    <p>          Chovatia, PT, Akabari, JD, Kachhadia, PK, Zalavadia, PD, and Joshi, HS</p>

    <p>          JOURNAL OF THE SERBIAN CHEMICAL SOCIETY <b>2006</b>.  71(7): 713-720, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242606200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242606200002</a> </p><br />

    <p>26.   59914   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Discontinued drugs in 2005: anti-infectives</p>

    <p>          Georgopapadakou, Nafsika</p>

    <p>          Expert Opinion on Investigational Drugs <b>2007</b>.  16(1): 1-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   59915   OI-LS-365; WOS-OI-12/31/2006</p>

    <p class="memofmt1-2">          Synthesis of 2,6-diamino-5-[(2-substituted phenylamino)ethyl]pyrimidin-4(3H)-one as inhibitors of folate metabolizing enzymes [1a,b]</p>

    <p>          Gangjee, A, Wang, Y, Queener, SF, and Kisliuk, RL</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2006</b>.  43(6): 1523-1531, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242742000015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242742000015</a> </p><br />

    <p>28.   59916   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Microsporidiosis: current status</p>

    <p>          Didier Elizabeth S and Weiss Louis M</p>

    <p>          Curr Opin Infect Dis <b>2006</b>.  19(5): 485-92.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   59917   OI-LS-365; WOS-OI-12/31/2006</p>

    <p class="memofmt1-2">          Antituberculotic and antiprotozoal activities of primin, a natural benzoquinone: In vitro and in vivo studies</p>

    <p>          Tasdemir, D, Brun, R, Yardley, V, Franzblau, SG, and Ruedi, P</p>

    <p>          CHEMISTRY &amp; BIODIVERSITY <b>2006</b>.  3(11): 1230-1237, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242557900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242557900006</a> </p><br />

    <p>30.   59918   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Identification and comparative analysis of sixteen fungal peptidyl-prolyl cis/trans isomerase repertoires</p>

    <p>          Pemberton, Trevor J</p>

    <p>          BMC Genomics <b>2006</b>.  7: No pp. given</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   59919   OI-LS-365; WOS-OI-12/31/2006</p>

    <p class="memofmt1-2">          In vitro antifungal activity of the fourth generation fluoroquinolones against Candida isolates from human ocular infections</p>

    <p>          Ozdek, SC, Miller, D, Flynn, PM, and Flynn, HW</p>

    <p>          OCULAR IMMUNOLOGY AND INFLAMMATION <b>2006</b>.  14(6): 347-351, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242712000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242712000004</a> </p><br />
    <br clear="all">

    <p>32.   59920   OI-LS-365; SCIFINDER-OI-1/2/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of 1H-indole-4,7-diones</p>

    <p>          Ryu Chung-Kyu, Lee Jung Yoon, Park Rae-Eun, Ma Mi-Young, and Nho Ji-Hee</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.  17(1): 127-31.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   59921   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          Rapid and low-cost colorimetric method using 2,3,5-triphenyltetrazolium chloride for detection of multidrug-resistant Mycobacterium tuberculosis</p>

    <p>          Mohammadzadeh, A, Farnia, P, Ghazvini, K, Behdani, M, Rashed, T, and Ghanaat, J</p>

    <p>          JOURNAL OF MEDICAL MICROBIOLOGY <b>2006</b>.  55(12): 1657-1659, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242675400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242675400007</a> </p><br />

    <p>34.   59922   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          Carbonic anhydrase inhibitors and activators and their use in therapy</p>

    <p>          Scozzafava, A, Mastrolorenzo, A, and Supuran, CT</p>

    <p>          EXPERT OPINION ON THERAPEUTIC PATENTS <b>2006</b>.  16(12): 1627-1664, 38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242773900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242773900003</a> </p><br />

    <p>35.   59923   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          New achievements in hepatitis C virus research and their implications for antiviral therapy</p>

    <p>          Bartenschlager, R</p>

    <p>          ANTIVIRAL THERAPY <b>2006</b>.  11(5): P3-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700019</a> </p><br />

    <p>36.   59924   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          Selection and characterization of hepatitis C virus replicon variants dually resistant to thumb and palm binding non-nucleoside polymerase inhibitors</p>

    <p>          Le Pogam, S, Kang, H, Harris, SF, Leveque, V, Giannetti, AM, Ali, S, Jiang, WR, Rajyaguru, S, Tavares, G, Oshiro, C, Hendricks, T, Klumpp, K, Symons, J, Browner, MF, Cammack, N, and Najera, I</p>

    <p>          ANTIVIRAL THERAPY <b>2006</b>.  11(5): S5-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700023</a> </p><br />

    <p>37.   59925   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          Treatment of Cryptosporidium parvum gut infection with nitazoxanide prevents long term ileal hypersensitivity in immunocompetent rats</p>

    <p>          Baishanbo, A, Marion, R, Gargala, G, Francois, A, Ducrotte, P, Fioramonti, J, Ballet, JJ, and Favennec, L</p>

    <p>          ACTA PHARMACOLOGICA SINICA <b>2006</b>.  27: 436-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239590003619">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239590003619</a> </p><br />

    <p>38.   59926   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          Selection and characterization of mutations conferring resistance to a HCV RNA dependent RNA polymerase inhibitor in vitro</p>

    <p>          Molla, A, Lu, L, Krishnan, P, Pithawalla, R, Dekhtyar, T, Ng, T, He, W, Pilot-Matias, T, Jiang, W, Liu, Y, Koev, G, Stewart, K, Larson, D, Bosse, T, Kati, W, Wagner, R, Kempf, D, and Mo, H</p>

    <p>          ANTIVIRAL THERAPY <b>2006</b>.  11(5): S6-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700024</a> </p><br />

    <p>39.   59927   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          Robust suppression of viral replication in HCV infected chimpanzees by a nucleoside inhibitor of the NS5B polymerase</p>

    <p>          Olsen, DB, Carroll, SS, Davies, ME, Handt, L, Koeplinger, K, Zhang, R, Ludmerer, S, MacCoss, M, and Hazuda, DJ</p>

    <p>          ANTIVIRAL THERAPY <b>2006</b>.  11(5): S7-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700025</a> </p><br />

    <p>40.   59928   OI-LS-365; WOS-OI-1/8/2007</p>

    <p class="memofmt1-2">          Development of a rapid phenotypic susceptibility assay for HCV polymerase inhibitors</p>

    <p>          Penuel, E, Han, D, Favero, K, Lam, E, Liu, Y, and Parkin, NT</p>

    <p>          ANTIVIRAL THERAPY <b>2006</b>.  11(5): S12-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700030</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
