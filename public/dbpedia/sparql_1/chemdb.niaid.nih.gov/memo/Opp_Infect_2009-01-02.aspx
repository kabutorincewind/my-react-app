

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-01-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JNAL06gy7IVE/FAI0xzbOk4Sq3byiqQ+QzJ72rB1x1dlhSLk/Q2Gygyj65/KJL9QfMILs7RP9CQwkP1x5t8bfdtJFZVNCSEUWJbnqu9urTAzg6hO9kEaQ+AivlU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D9B0781B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Infections  Citation List:  December 11 - December 24, 2008</h1>

    <h2>Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</h2>

    <p class="ListParagraph">1.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19101515?ordinalpos=19&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Isocryptomerin, a novel membrane-active antifungal compound from Selaginella tamariscina.</a></p>

    <p class="NoSpacing">Lee J, Choi Y, Woo ER, Lee DG.</p>

    <p class="NoSpacing">Biochem Biophys Res Commun. 2008 Dec 18. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19101515 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">2.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19092132?ordinalpos=32&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antimicrobial properties of melanocortins: comment to the manuscript &quot;Anti-Candida activity of {alpha}-melanocyte-stimulating hormone ({alpha}-MSH) peptides&quot; by Isabella Rauch et al.</a></p>

    <p class="NoSpacing">Catania A, Lipton JM.</p>

    <p class="NoSpacing">J Leukoc Biol. 2008 Dec 17. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19092132 [PubMed - as supplied by publisher]    </p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">3.<b>        </b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/18977139?ordinalpos=65&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in vitro evaluation of the antifungal activities of dihydropyrimidinones.</a></p>

    <p class="NoSpacing">Singh OM, Singh SJ, Devi MB, Devi LN, Singh NI, Lee SG.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2008 Dec 15;18(24):6462-7. Epub 2008 Oct 18.</p>

    <p class="NoSpacing">PMID: 18977139 [PubMed - in process]            </p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">4.<b>        </b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/18974000?ordinalpos=66&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Carbon analogs of antifungal dioxane-triazole derivatives: synthesis and in vitro activities.</a></p>

    <p class="NoSpacing">Uchida T, Somada A, Kagoshima Y, Konosu T, Oida S.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2008 Dec 15;18(24):6538-41. Epub 2008 Oct 15.</p>

    <p class="NoSpacing">PMID: 18974000 [PubMed - in process]            </p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">5.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18556241?ordinalpos=70&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis, biological and computational study of new Schiff base hydrazones bearing 3-(4-pyridine)-5-mercapto-1,2,4-triazole moiety.</a></p>

    <p class="NoSpacing">Khanmohammadi H, Abnosi MH, Hosseinzadeh A, Erfantalab M.</p>

    <p class="NoSpacing">Spectrochim Acta A Mol Biomol Spectrosc. 2008 Dec 15;71(4):1474-80. Epub 2008 May 14.</p>

    <p class="NoSpacing">PMID: 18556241 [PubMed - in process]            </p>

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">6.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19107862?ordinalpos=15&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antiretroviral activity of fucoidans extracted from the brown seaweed Adenocystis utricularis.</a></p>

    <p class="NoSpacing">Trinchero J, Ponce NM, Córdoba OL, Flores ML, Pampuro S, Stortz CA, Salomón H, Turk G.</p>

    <p class="NoSpacing">Phytother Res. 2008 Dec 23. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19107862 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">7.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19099398?ordinalpos=49&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis, Reduction Potentials, and Antitubercular Activity of Ring A/B Analogues of the Bioreductive Drug (6S)-2-Nitro-6-{[4-(trifluoromethoxy)benzyl]oxy}-6,7-dihydro-5H-imidazo[2,1-b][1,3]oxazine (PA-824).</a></p>

    <p class="NoSpacing">Thompson AM, Blaser A, Anderson RF, Shinde SS, Franzblau SG, Ma Z, Denny WA, Palmer BD.</p>

    <p class="NoSpacing">J Med Chem. 2008 Dec 17. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19099398 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">8.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19075058?ordinalpos=113&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Promising anti-tuberculosis activity of the oxazolidinone PNU-100480 relative to linezolid in the murine model.</a></p>

    <p class="NoSpacing">Williams KN, Stover CK, Zhu T, Tasneen R, Tyagi S, Grosset JH, Nuermberger E.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2008 Dec 15. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19075058 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">9.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19070992?ordinalpos=126&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel N-aryl- and N-heteryl phenazine-1-carboxamides as potential agents for the treatment of infections sustained by drug-resistant and multidrug-resistant Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">De Logu A, Palchykovska LH, Kostina VH, Sanna A, Meleddu R, Chisu L, Alexeeva IV, Shved AD.</p>

    <p class="NoSpacing">Int J Antimicrob Agents. 2008 Dec 12. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19070992 [PubMed - as supplied by publisher]    </p>  

    <p class="ListParagraph">10.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/19053755?ordinalpos=131&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Development and preclinical studies of broad-spectrum anti-HIV agent (3&#39;R,4&#39;R)-3-cyanomethyl-4-methyl-3&#39;,4&#39;-di-O-(S)-camphanoyl-(+)-cis-khellactone (3-cyanomethyl-4-methyl-DCK).</a></p>

    <p class="NoSpacing">Xie L, Guo HF, Lu H, Zhuang XM, Zhang AM, Wu G, Ruan JX, Zhou T, Yu D, Qian K, Lee KH, Jiang S.</p>

    <p class="NoSpacing">J Med Chem. 2008 Dec 25;51(24):7689-96.       </p>

    <p class="NoSpacing">PMID: 19053755 [PubMed - in process]</p> 

    <p class="ListParagraph">11.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/19008098?ordinalpos=142&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Peptide deformylase inhibitors of Mycobacterium tuberculosis: synthesis, structural investigations, and biological results.</a></p>

    <p class="NoSpacing">Pichota A, Duraiswamy J, Yin Z, Keller TH, Alam J, Liung S, Lee G, Ding M, Wang G, Chan WL, Schreiber M, Ma I, Beer D, Ngew X, Mukherjee K, Nanjundappa M, Teo JW, Thayalan P, Yap A, Dick T, Meng W, Xu M, Koehn J, Pan SH, Clark K, Xie X, Shoen C, Cynamon M.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2008 Dec 15;18(24):6568-72. Epub 2008 Oct 14.</p>

    <p class="NoSpacing">PMID: 19008098 [PubMed - in process]            </p> 

    <p class="ListParagraph">12.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18952418?ordinalpos=150&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A facile synthesis, antibacterial, and antitubercular studies of some piperidin-4-one and tetrahydropyridine derivatives.</a></p>

    <p class="NoSpacing">Aridoss G, Amirthaganesan S, Kumar NA, Kim JT, Lim KT, Kabilan S, Jeong YT.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2008 Dec 15;18(24):6542-8. Epub 2008 Oct 14.</p>

    <p class="NoSpacing">PMID: 18952418 [PubMed - in process]            </p> 

    <h2>Antimicrobials, Antifungals (General)</h2>

    <p class="ListParagraph">13.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/19096993?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antifungal Triterpene Glycosides from the Sea Cucumber Bohadschia marmorata.</a></p>

    <p class="NoSpacing">Yuan WH, Yi YH, Tang HF, Liu BS, Wang ZL, Sun GQ, Zhang W, Li L, Sun P.</p>

    <p class="NoSpacing">Planta Med. 2008 Dec 18. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19096993 [PubMed - as supplied by publisher]    </p> 

    <p class="NoSpacing">&nbsp;</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
