

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-366.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MN1AFTsFtUkhlpDbzdgmUY84pDUychj3AOv4rgJTu7NLTsjs5hDwC/QzmhH6F0Ik8tM0Y8cz9FU+IXilXGZ/3jgM8CBQ+oj42F0jUV1fSqPEEiqGZK8o+J3237E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="73E44A3A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-366-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59950   HIV-LS-366; EMBASE-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 infection by synthetic peptides derived CCR5 fragments</p>

    <p>          Imai, Masaki, Baranyi, Lajos, Okada, Noriko, and Okada, Hidechika</p>

    <p>          Biochemical and Biophysical Research Communications <b>2007</b>.  353(4): 851-856</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4MMFRH8-2/2/b882e26891c7ee211710ebf48b2223d7">http://www.sciencedirect.com/science/article/B6WBK-4MMFRH8-2/2/b882e26891c7ee211710ebf48b2223d7</a> </p><br />

    <p>2.     59951   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          Non-nucleoside reverse transcriptase inhibitors: a review</p>

    <p>          Waters, L, John, L, and Nelson, M</p>

    <p>          Int J Clin Pract <b>2007</b>.  61(1): 105-18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17229185&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17229185&amp;dopt=abstract</a> </p><br />

    <p>3.     59952   HIV-LS-366; EMBASE-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Discovery of small-molecule HIV-1 fusion and integrase inhibitors oleuropein and hydroxytyrosol: II. Integrase inhibition</p>

    <p>          Lee-Huang, Sylvia, Huang, Philip Lin, Zhang, Dawei, Lee, Jae Wook, Bao, Ju, Sun, Yongtao, Chang, Young-Tae, Zhang, John, and Huang, Paul Lee</p>

    <p>          Biochemical and Biophysical Research Communications <b>2007</b>.  In Press, Uncorrected Proof: 180</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4MW8XVH-1/2/208a2cd3182149ac9d79d450980fb590">http://www.sciencedirect.com/science/article/B6WBK-4MW8XVH-1/2/208a2cd3182149ac9d79d450980fb590</a> </p><br />

    <p>4.     59953   HIV-LS-366; EMBASE-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Discovery of novel benzimidazolones as potent non-nucleoside reverse transcriptase inhibitors active against wild-type and mutant HIV-1 strains</p>

    <p>          Barreca, Maria Letizia, Rao, Angela, Luca, Laura De, Iraci, Nunzio, Monforte, Anna-Maria, Maga, Giovanni, Clercq, Erik De, Pannecouque, Christophe, Balzarini, Jan, and Chimirri, Alba</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 180</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MVN0XT-8/2/7a62c6d501cc914fa534e1e5e7f023d1">http://www.sciencedirect.com/science/article/B6TF9-4MVN0XT-8/2/7a62c6d501cc914fa534e1e5e7f023d1</a> </p><br />

    <p>5.     59954   HIV-LS-366; EMBASE-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Ring substituent effects on biological activity of vinyl sulfones as inhibitors of HIV-1</p>

    <p>          Meadows, DChristopher, Sanchez, Tino, Neamati, Nouri, North, Thomas W, and Gervay-Hague, Jacquelyn</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(2): 1127-1137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M3R29H-7/2/76b51f269c645300917d3a9af78ee93b">http://www.sciencedirect.com/science/article/B6TF8-4M3R29H-7/2/76b51f269c645300917d3a9af78ee93b</a> </p><br />
    <br clear="all">

    <p>6.     59955   HIV-LS-366; EMBASE-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Development of a one-step immunochromatographic strip test for the rapid detection of nevirapine (NVP), a commonly used antiretroviral drug for the treatment of HIV/AIDS</p>

    <p>          Pattarawarapan, M, Nangola, S, Cressey, TR, and Tayapiwatana, C</p>

    <p>          Talanta <b>2007</b>.  71(1): 462-470</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THP-4K9C6DY-2/2/345a53399eb565d81f962077e6a2c408">http://www.sciencedirect.com/science/article/B6THP-4K9C6DY-2/2/345a53399eb565d81f962077e6a2c408</a> </p><br />

    <p>7.     59956   HIV-LS-366; EMBASE-HIV-1/21/2007</p>

    <p class="memofmt1-2">          HIV1 protease inhibitors selectively induce inflammatory chemokine expression in primary human osteoblasts</p>

    <p>          Malizia, Andrea P, Vioreanu, Mihai H, Doran, Peter P, and Powderly, William G</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 309</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4MSJNJY-1/2/0bb713819591d1148865967d41337441">http://www.sciencedirect.com/science/article/B6T2H-4MSJNJY-1/2/0bb713819591d1148865967d41337441</a> </p><br />

    <p>8.     59957   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          A broad antiviral neutral glycolipid, fattiviracin FV-8, is a membrane fluidity modulator</p>

    <p>          Harada, S, Yokomizo, K, Monde, K, Maeda, Y, and Yusa, K</p>

    <p>          Cell Microbiol  <b>2007</b>.  9(1): 196-203</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17222192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17222192&amp;dopt=abstract</a> </p><br />

    <p>9.     59958   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          N88D Facilitates the Co-occurrence of D30N and L90M and the Development of Multidrug Resistance in HIV Type 1 Protease following Nelfinavir Treatment Failure</p>

    <p>          Mitsuya, Y, Winters, MA, Fessel, WJ, Rhee, SY, Hurley, L, Horberg, M, Schiffer, CA, Zolopa, AR, and Shafer, RW</p>

    <p>          AIDS Res Hum Retroviruses <b>2006</b>.  22(12): 1300-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17209774&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17209774&amp;dopt=abstract</a> </p><br />

    <p>10.   59959   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          Drug interactions. Brecanavir--a new protease inhibitor</p>

    <p>          Anon</p>

    <p>          TreatmentUpdate <b>2006</b>.  18(3): 6-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17209240&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17209240&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59960   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 infection by viral chemokine U83A via high affinity CCR5 interactions which block human chemokine-induced leukocyte chemotaxis and receptor internalisation</p>

    <p>          Catusse, J, Parry, CM, Dewin, DR, and Gompels, UA</p>

    <p>          Blood <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17209056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17209056&amp;dopt=abstract</a> </p><br />

    <p>12.   59961   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          Modulation of human BCRP (ABCG2) activity by anti-HIV drugs</p>

    <p>          Weiss, J, Rose, J, Storch, CH, Ketabi-Kiyanvash, N, Sauer, A, Haefeli, WE, and Efferth, T</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17202245&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17202245&amp;dopt=abstract</a> </p><br />

    <p>13.   59962   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          Non-nucleoside reverse transcriptase inhibitors (NNRTIs): past, present, and future</p>

    <p>          De Clercq, E</p>

    <p>          Chem Biodivers <b>2004</b>.  1(1): 44-64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191775&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191775&amp;dopt=abstract</a> </p><br />

    <p>14.   59963   HIV-LS-366; PUBMED-HIV-1/22/2007</p>

    <p class="memofmt1-2">          Antibodies against a multiple-peptide conjugate comprising chemically modified human immunodeficiency virus type-1 functional Tat peptides inhibit infection</p>

    <p>          Devadas, K, Boykins, RA, Hewlett, IK, Wood, OL, Clouse, KA, Yamada, KM, and Dhawan, S</p>

    <p>          Peptides <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188401&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188401&amp;dopt=abstract</a> </p><br />

    <p>15.   59964   HIV-LS-366; WOS-HIV-1/15/2007</p>

    <p class="memofmt1-2">          Halichondria sulfonic acid, a new HIV-1 inhibitory guanidino-sulflonic acid, and halistanol sulfate isolated from the marine sponge Halichondria rugosa Ridley &amp; Dendy</p>

    <p>          Jin, Y, Fotso, S, Yongtang, Z, Sevvana, M, Laatsch, H, and Zhang, W</p>

    <p>          NATURAL PRODUCT RESEARCH <b>2006</b>.  20(12): 1129-1135, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242898600016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242898600016</a> </p><br />

    <p>16.   59965   HIV-LS-366; WOS-HIV-1/15/2007</p>

    <p class="memofmt1-2">          Inhibitors of the chemokine receptor CXCR4: Chemotherapy of AIDS, metastatic cancer, leukemia and rheumatoid arthritis</p>

    <p>          Tsutsumi, H, Tamamura, H, and Fujii, N</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2007</b>.  4(1): 20-26, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242910300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242910300004</a> </p><br />

    <p>17.   59966   HIV-LS-366; WOS-HIV-1/15/2007</p>

    <p class="memofmt1-2">          Downregulation of CCR5 expression on cells by recombinant adenovirus containing antisense CCR5, a possible measure to prevent HIV-1 from entering target cells</p>

    <p>          Li, WG, Yu, M, Bai, L, Bu, DF, and Xu, XY</p>

    <p>          JAIDS-JOURNAL OF ACQUIRED IMMUNE DEFICIENCY SYNDROMES <b>2006</b>.  43(5): 516-522, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242793600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242793600003</a> </p><br />

    <p>18.   59967   HIV-LS-366; WOS-HIV-1/15/2007</p>

    <p class="memofmt1-2">          Identification of a novel circulating recombinant form (CRF33_01B) disseminating widely among various risk populations in Kuala Lumpur, Malaysia</p>

    <p>          Tee, KK, Li, XJ, Nohtomi, K, Ng, KP, Kamarulzaman, A, and Takebe, Y</p>

    <p>          JAIDS-JOURNAL OF ACQUIRED IMMUNE DEFICIENCY SYNDROMES <b>2006</b>.  43(5): 523-529, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242793600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242793600004</a> </p><br />

    <p>19.   59968   HIV-LS-366; WOS-HIV-1/15/2007</p>

    <p class="memofmt1-2">          Metabolism and disposition studies of potent HIV integrase inhibitors using 19F NMR</p>

    <p>          Monteagudo, E, Pesci, S, Taliani, M, Fonsi, M, Verdirame, M, Naimo, F, Bonelli, F, Di Marco, A, Fiore, F, Gonzalez-Paz, O, Summa, V, Rowley, M, and Laufer, R</p>

    <p>          DRUG METABOLISM REVIEWS <b>2006</b>.  38: 160-161, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239659200244">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239659200244</a> </p><br />

    <p>20.   59969   HIV-LS-366; WOS-HIV-1/15/2007</p>

    <p class="memofmt1-2">          Activity of HIV entry and fusion inhibitors expressed by the human vaginal colonizing probiotic Lactobacillus reuteri RC-14</p>

    <p>          Liu, JJ, Reid, G, Jiang, YH, Turner, MS, and Tsai, CC</p>

    <p>          CELLULAR MICROBIOLOGY <b>2007</b>.  9(1): 120-130, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242780900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242780900011</a> </p><br />

    <p>21.   59970   HIV-LS-366; WOS-HIV-1/15/2007</p>

    <p class="memofmt1-2">          Synthesis of styrylquinoline carboxamides for HIV-1 integrase inhibitors</p>

    <p>          Lee, SU, Park, JH, Lee, JY, Shin, CG, Chung, BY, and Lee, YS</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY <b>2006</b>.  27(11): 1888-1890, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242822500034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242822500034</a> </p><br />

    <p>22.   59971   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          New derivatives of alkyl- and aminocarbonylphosphonic acids containing 3 &#39;-azido-3 &#39;-deoxythymidine</p>

    <p>          Jasko, MV, Shipitsyn, AV, Khandazhinskaya, AL, Shirokova, EA, Sol&#39;yev, PN, Plyasunova, OA, and Pokrovskii, AG</p>

    <p>          RUSSIAN JOURNAL OF BIOORGANIC CHEMISTRY <b>2006</b>.  32(6): 542-546, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242884000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242884000005</a> </p><br />

    <p>23.   59972   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Nuclear Factor 90, a cellular dsRNA binding protein inhibits the HIV Rev-export function</p>

    <p>          Urcuqui-Inchima, S, Castano, ME, Hernandez-Verdun, D, St-Laurent, G, and Kumar, A</p>

    <p>          RETROVIROLOGY <b>2006</b>.  3: 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243003700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243003700001</a> </p><br />

    <p>24.   59973   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Release of autoinhibition converts ESCRT-III components into potent inhibitors of HIV-1 budding</p>

    <p>          Zamborlini, A, Usami, Y, Radoshitzky, SR, Popova, E, Palu, G, and Gottlinger, H</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2006</b>.  103(50): 19140-19145, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242884200048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242884200048</a> </p><br />

    <p>25.   59974   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Identification of novel non-nucleoside reverse transcriptase inhibitors using fragment-based lead generation</p>

    <p>          Ramajayam, R, Giridhar, R, Yadav, MR, De Clereq, E, Pannecouque, C, and Prajapati, DG</p>

    <p>          MEDICINAL CHEMISTRY RESEARCH <b>2005</b>.  14(8-9): 475-487, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243027900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243027900004</a> </p><br />

    <p>26.   59975   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Pradimicin A, a carbohydrate-binding nonpeptidic lead compound for treatment of infections with viruses with highly glycosylated envelopes, such as human immunodeficiency virus</p>

    <p>          Balzarini, J, Van Laethem, K, Daelemans, D, Hatse, S, Bugatti, A, Rusnati, M, Igarashi, Y, Oki, T, and Schols, D</p>

    <p>          JOURNAL OF VIROLOGY <b>2007</b>.  81(1): 362-373, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242958600033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242958600033</a> </p><br />

    <p>27.   59976   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Modulation of specific surface receptors and activation sensitization in primary resting CD4(+) T lymphocytes by the Nef protein of HIV-1</p>

    <p>          Keppler, OT, Tibroni, N, Venzke, S, Rauch, S, and Fackler, OT</p>

    <p>          JOURNAL OF LEUKOCYTE BIOLOGY <b>2006</b>.  79(3): 616-627, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243015000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243015000021</a> </p><br />

    <p>28.   59977   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by RNA interference of p53 expression</p>

    <p>          Pauls, E, Senserrich, J, Clotet, B, and Este, JA</p>

    <p>          JOURNAL OF LEUKOCYTE BIOLOGY <b>2006</b>.  80(3): 659-667, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243015700024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243015700024</a> </p><br />

    <p>29.   59978   HIV-LS-366; WOS-HIV-1/21/2007</p>

    <p class="memofmt1-2">          Mechanisms underlying activity of antiretroviral drugs in HIV-1-infected macrophages: new therapeutic strategies</p>

    <p>          Aquaro, S, Svicher, V, Schols, D, Pollicita, M, Antinori, A, Balzarini, J, and Perno, CF</p>

    <p>          JOURNAL OF LEUKOCYTE BIOLOGY <b>2006</b>.  80(5): 1103-1110, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243016100015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243016100015</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
