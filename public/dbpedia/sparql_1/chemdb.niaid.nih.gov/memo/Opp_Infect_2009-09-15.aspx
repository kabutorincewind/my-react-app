

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-09-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CNR8GQEoxEnlVTvjvGkQS+WK0/cJK0FO93ugKDrqikQvuh3Ba0RFAW3qGt78+HyDyZHRiKKSNTxZwRAWw36yFfp9NwA9BFeQ31fyPCxXDsOHadoxi95RGCbpoYs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4659F411" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">September 2, 2009 - September 15, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19557435?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Effects of curcumin on Cryptosporidium parvum in vitro.</a></p>

    <p class="NoSpacing">Shahiduzzaman M, Dyachenko V, Khalafalla RE, Desouky AY, Daugschies A.</p>

    <p class="NoSpacing">Parasitol Res. 2009 Oct;105(4):1155-61. Epub 2009 Jun 26.</p>

    <p class="NoSpacing">PMID: 19557435 [PubMed - in process]</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19740639?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Isolation and characterization of a French bean hemagglutinin with antitumor, antifungal, and anti-HIV-1 reverse transcriptase activities and an exceptionally high yield.</a></p>

    <p class="NoSpacing">Lam SK, Ng TB.</p>

    <p class="NoSpacing">Phytomedicine. 2009 Sep 7. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19740639 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19673482?ordinalpos=30&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Occidiofungin, a unique antifungal glycopeptide produced by a strain of Burkholderia contaminans.</a></p>

    <p class="NoSpacing">Lu SE, Novak J, Austin FW, Gu G, Ellis D, Kirk M, Wilson-Stanford S, Tonelli M, Smith L.</p>

    <p class="NoSpacing">Biochemistry. 2009 Sep 8;48(35):8312-21.</p>

    <p class="NoSpacing">PMID: 19673482 [PubMed - in process]</p>  

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19545230?ordinalpos=51&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Biological Activities of Pyrazoline Derivatives -A Recent Development.</a></p>

    <p class="NoSpacing">Kumar S, Bawa S, Drabu S, Kumar R, Gupta H.</p>

    <p class="NoSpacing">Recent Pat Antiinfect Drug Discov. 2009 Nov 1. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19545230 [PubMed - as supplied by publisher]</p> 

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19683922?ordinalpos=43&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">5-(2-Pyrimidinyl)-imidazo[1,2-a]pyridines are antibacterial agents targeting the ATPase domains of DNA gyrase and topoisomerase IV.</a></p>

    <p class="NoSpacing">Starr JT, Sciotti RJ, Hanna DL, Huband MD, Mullins LM, Cai H, Gage JW, Lockard M, Rauckhorst MR, Owen RM, Lall MS, Tomilo M, Chen H, McCurdy SP, Barbachyn MR.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Sep 15;19(18):5302-6. Epub 2009 Aug 3.</p>

    <p class="NoSpacing">PMID: 19683922 [PubMed - in process]</p>  

    <p class="ListParagraph">6. Braendvang, M., V. Bakken, and L.L. Gundersen, Synthesis, structure, and antimycobacterial activity of 6-[1(3H)-isobenzofuranylidenemethyl]purines and analogs. Bioorg Med Chem, 2009. 17(18): p. 6512-6; PMID[19709890] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19709890?ordinalpos=105&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19709890?ordinalpos=105&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a></p>  

    <p class="ListParagraph">7. Podust, L.M., H. Ouellet, J.P. von Kries, and P.R. Ortiz de Montellano, Interaction of Mycobacterium tuberculosis CYP130 with Heterocyclic Arylamines. J Biol Chem, 2009. 284(37): p. 25211-9; PMID[19605350] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19605350?ordinalpos=124&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19605350?ordinalpos=124&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a></p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">8. Correia, C., et al</p>

    <p class="NoSpacing">Synthesis and in vitro activity of 6-amino-2,9-diarylpurines for Mycobacterium tuberculosis</p>

    <p class="NoSpacing">TETRAHEDRON  2009 (August 22) 65: 6903 - 6911</p> 

    <p class="ListParagraph">9. Khumkomkhet, P., et al</p>

    <p class="NoSpacing">Antimalarial and Cytotoxic Depsidones from the Fungus Chaetomium brasiliense</p>

    <p class="NoSpacing">JOURNAL OF NATURAL PRODUCTS  2009 (August) 72: 1487 - 1491</p> 

    <p class="ListParagraph">10. Kuete, V., et al</p>

    <p class="NoSpacing">Diospyrone, crassiflorone and plumbagin: three antimycobacterial and antigonorrhoeal naphthoquinones from two Diospyros spp.</p>

    <p class="NoSpacing">INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS  2009 (October) 34: 322 - 325</p> 

    <p class="ListParagraph">11. Vieira, LMM., et al</p>

    <p class="NoSpacing">Synthesis and antitubercular activity of palladium and platinum complexes with fluoroquinolones</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (October) 44: 4107 - 4111</p> 

    <p class="ListParagraph">12. Wei, Y., et al</p>

    <p class="NoSpacing">Synthesis and evaluation of A-seco type triterpenoids for anti-HIV-1 protease activity</p>
    
    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY 2009 (October) 44: 4112 - 4120</p> 

    <p class="ListParagraph">13. Aridoss, G., et al</p>

    <p class="NoSpacing">Synthesis, spectral and biological evaluation of some new thiazolidinones and thiazoles based on t-3-alkyl-r-2,c-6-diarylpiperidin-4-ones</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (October) 44: 4199 - 4210</p> 

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
