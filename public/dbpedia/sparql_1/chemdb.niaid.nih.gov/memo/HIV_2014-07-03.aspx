

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-07-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kUUgLCOVXqp7t/hLNnN3pxc8AfkSEMh3SICcqrURXlYWZMdMPyh68hG9J4Oz67qa/ij626yA6kPA6oOpbAl6RjEO38rqM86u8fxFXUocHTbGAdroHOodmU4vrmY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D9383633" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 20 - July 3, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24856833">Structure-Activity Relationship Studies of Indole-based Compounds as Small Molecule HIV-1 Fusion Inhibitors Targeting Glycoprotein 41.</a> Zhou, G., V. Sofiyev, H. Kaur, B.A. Snyder, M.K. Mankowski, P.A. Hogan, R.G. Ptak, and M. Gochin. Journal of Medicinal Chemistry, 2014. 57(12): p. 5270-5281. PMID[24856833].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24901667">4-Amino-1-hydroxy-2-oxo-1,8-naphthyridine-containing Compounds Having High Potency against Raltegravir-resistant Integrase Mutants of HIV-1.</a> Zhao, X.Z., S.J. Smith, M. Metifiot, C. Marchand, P.L. Boyer, Y. Pommier, S.H. Hughes, and T.R. Burke, Jr. Journal of Medicinal Chemistry, 2014. 57(12): p. 5190-5202. PMID[24901667].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24963618">Genistein as Antiviral Drug against HIV Ion Channel.</a> Sauter, D., S. Schwarz, K. Wang, R. Zhang, B. Sun, and W. Schwarz. Planta Medica, 2014. 80(8-9): p. 682-687. PMID[24963618].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24933420">Exploring the Role of 2-Chloro-6-fluoro Substitution in 2-Alkylthio-6-benzyl-5-alkylpyrimidin-4(3H)-ones: Effects in HIV-1-infected Cells and in HIV-1 Reverse Transcriptase Enzymes.</a> Rotili, D., D. Tarantino, M.B. Nawrozkij, A.S. Babushkin, G. Botta, B. Marrocco, R. Cirilli, S. Menta, R. Badia, E. Crespan, F. Ballante, R. Ragno, J.A. Este, G. Maga, and A. Mai. Journal of Medicinal Chemistry, 2014. 57(12): p. 5212-5225. PMID[24933420].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24922269">Synthesis of the Pitstop Family of Clathrin Inhibitors.</a> Robertson, M.J., F.M. Deane, W. Stahlschmidt, L. von Kleist, V. Haucke, P.J. Robinson, and A. McCluskey. Nature Protocols, 2014. 9(7): p. 1592-1606. PMID[24922269].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24726448">A Mechanistic and Structural Investigation of Modified Derivatives of the Diaryltriazine Class of NNRTIs Targeting HIV-1 Reverse Transcriptase.</a> Mislak, A.C., K.M. Frey, M. Bollini, W.L. Jorgensen, and K.S. Anderson. Biochimica et Biophysica Acta, 2014. 1840(7): p. 2203-2211. PMID[24726448].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24805780">Synthesis of Novel Fluoro Analogues of MKC442 as Microbicides.</a> Loksha, Y.M., E.B. Pedersen, R. Loddo, G. Sanna, G. Collu, G. Giliberti, and P.L. Colla. Journal of Medicinal Chemistry, 2014. 57(12): p. 5169-5178. PMID[24805780].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24784528">Genus Kadsura, a Good Source with Considerable Characteristic Chemical Constituents and Potential Bioactivities.</a> Liu, J., Y. Qi, H. Lai, J. Zhang, X. Jia, H. Liu, B. Zhang, and P. Xiao. Phytomedicine, 2014. 21(8-9): p. 1092-1097. PMID[24784528].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24927059">Multivalent Agents Containing 1-Substituted 2,3,4-Trihydroxyphenyl Moieties as Novel Synthetic Polyphenols Directed against HIV-1.</a> Flores, A., M.J. Camarasa, M.J. Perez-Perez, A. San-Felix, J. Balzarini, and E. Quesada. Organic &amp; Biomolecular Chemistry, 2014. 12(28): p. 5278-5294. PMID[24927059].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0620-070314.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336710200013">Asymmetric Synthesis of 1,3-Oxathiolan-5-one Derivatives through Dynamic Covalent Kinetic Resolution.</a> Zhang, Y., F. Schaufelberger, M. Sakulsombat, C. Liu, and O. Ramstrom. Tetrahedron, 2014. 70(24): p. 3826-3831. ISI[000336710200013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336613500001">Bioactivities of Compounds from Elephantopus scaber, an Ethnomedicinal Plant from Southwest China.</a> Wang, J.J., P. Li, B.S. Li, Z.Y. Guo, E.J. Kennelly, and C.L. Long. Evidence-Based Complementary and Alternative Medicine, 2014. 569594: p. 7. ISI[000336613500001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336382300015">Synthesis and anti-HIV Activity of Triazolo-fused, Medium-sized Cyclic Nucleoside Analogs Prepared by an Intramolecular Huisgen 1,3-Dipolar Cycloaddition.</a> Sun, J.B., R.W. Liu, Q. Fu, J. Zang, Q.Q. Tao, J.C. Wu, and H. Zhu. Helvetica Chimica Acta, 2014. 97(5): p. 733-743. ISI[000336382300015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336358000004">Anthraquinones from Vismia mexicana.</a> Reyes-Chilpa, R., R. Gomez-Cansino, S.L. Guzman-Gutierrez, S. Hernandez-Ortega, M. Campos-Lara, E. Vega-Avila, and A. Nieto-Camacho. Zeitschrift fur Naturforschung Section C-A Journal of Biosciences, 2014. 69(1-2): p. 29-34. ISI[000336358000004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333939500002">Isolation and anti-HIV-1 Activity of a New Sesquiterpene Lactone from Calocephalus brownii F. Muell.</a>. Mohammed, M.M.D., L.P. Christensen, and P.L. Colla. Natural Product Research, 2014. 28(4): p. 221-229. ISI[000333939500002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336616600001">Fluorine Substituted 1,2,4-Triazinones as Potential anti-HIV-1 and CDK2 Inhibitors.</a> Makki, M.S.I., R.M. Abdel-Rahman, and K.A. Khan. Journal of Chemistry, 2014. 430573: p. 14. ISI[000336616600001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">16.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334241600014">C-3-symmetric Cinchonine-squaramide-catalyzed Asymmetric Chlorolactonization of Styrene-type Carboxylic Acids with 1,3-Dichloro-5,5-dimethylhydantoin: An Efficient Method to Chiral Isochroman-1-ones.</a> Han, X., C.N. Dong, and H.B. Zhou. Advanced Synthesis &amp; Catalysis, 2014. 356(6): p. 1275-1280. ISI[000334241600014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336527900006">Cycloviolacin O2 (CyO2) Suppresses Productive Infection and Augments the Antiviral Efficacy of Nelfinavir in HIV-1 Infected Monocytic Cells.</a> Gerlach, S.L., M. Yeshak, U. Goransson, U. Roy, R. Izadpanah, and D. Mondal. Biopolymers, 2013. 100(5): p. 471-479. ISI[000336527900006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336107900014">Synthesis and anti-HIV-RT Activity of Novel Thiazolindin-4-one Derivatives Possessing Hydrophilic Groups</a>. Chen, H., C.J. Huang, M. Zhu, and X.L. Li. Chinese Journal of Organic Chemistry, 2014. 34(4): p. 756-760. ISI[000336107900014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335664000005">Antiproliferative and Antiviral Activity of Three Libraries of Adamantane Derivatives.</a> Basaric, N., M. Sohora, N. Cindro, K. Mlinaric-Majerski, E. De Clercq, and J. Balzarini. Archiv der Pharmazie, 2014. 347(5): p. 334-340. ISI[000335664000005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335433500006">Hypoiodite-mediated Cyclopropanation of Alkenes.</a> Yoshimura, A., S.R. Koski, B.J. Kastern, J.M. Fuchs, T.N. Jones, R.Y. Yusubova, V.N. Nemykin, and V.V. Zhdankin. Chemistry, 2014. 20(20): p. 5895-5898. ISI[000335433500006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336390100011">Antiviral Potential of Bulgarian Medicinal Plants.</a> Todorov, D., A. Hinkov, K. Shishkova, and S. Shishkov. Phytochemistry Reviews, 2014. 13(2): p. 525-538. ISI[000336390100011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335743300004">Four New Phenolic Acids from Clerodendranthus spicatus.</a> Sun, Z.C., Q.X. Zheng, G.X. Ma, X.P. Zhang, J.Q. Yuan, H.F. Wu, H.L. Liu, J.S. Yang, and X.D. Xu. Phytochemistry Letters, 2014. 8: p. 16-21. ISI[000335743300004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">23.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336199200045">Palladium-catalyzed C(sp(2))-H Pyridocarbonylation of N-Aryl-2-aminopyridines: Dual Function of the Pyridyl Moiety.</a> Liang, D.D., Y.M. He, and Q. Zhu. Organic Letters, 2014. 16(10): p. 2748-2751. ISI[000336199200045].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0620-070314.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140612&amp;CC=US&amp;NR=2014163055A1&amp;KC=A1">Methods and Compositions Comprising Phospholipase D Inhibitors for Treatment of HIV Infection.</a> Brown, H.A. and C.W. Lindsley. Patent. 2014. 2013-14103795 20140163055: 112pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">25. <a href="http://http/worldwide.espacenet.com/publicationDetails/biblio?DB=worldwide.espacenet.com&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140514&amp;CC=CN&amp;NR=103787985A&amp;KC=A">Preparation of Diaryl Pyrimidine Derivatives Useful in the Treatment of HIV Infection.</a> Chen, F., Z. Yan, and Q. He. Patent. 2014. 2014-10019849 103787985: 8pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140530&amp;CC=WO&amp;NR=2014079903A1&amp;KC=A1">Phosphonucleotide Phosphonate Analogs as Antiviral Agents for Treatment of Chronic Viral Diseases.</a> Dehaen, W., J. Balzarini, A. Maguire, S.J. Keane, A. Ford, N. Maguire, and N.D. Mullins. Patent. 2014. 2013-EP74321 2014079903: 106pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140522&amp;CC=WO&amp;NR=2014075676A1&amp;KC=A1">Pharmaceutical Compositions Containing Amines Extracted from Trigonella Foenum-Graceum for Treatment of Various Diseases.</a> Olsen, J.S. Patent. 2014. 2012-DK50419 2014075676: 58pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140618&amp;CC=CN&amp;NR=103860552A&amp;KC=A">Application of Antiviral Compound in Preparation of anti-HIV-1 Virus Drugs [Machine Translation].</a> Zhang, H., C. Bai, and T. Pan. Patent. 2014. 2014-10120208 103860552: 9pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0620-070314.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140604&amp;CC=CN&amp;NR=103830232A&amp;KC=A">Application of Antiviral Compound with Good Antiviral Activity in Preparation of anti-HIV-1 Virus Drugs.</a> Zhang, H., C. Bai, and T. Pan. Patent. 2014. 2014-10120209 103830232: 9pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0620-070314.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
