

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-11-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8z9kQUyoIcJOT//uBlbmBqZ1tvqYuNZxOdbCIZEpBvmxTd19m8vphnF3LE8V9DFzYpRMyOO8EM7pzHU69G+K60v0K9OjziLn0USbSshTkdDd6LZ4qcdxgxI5/8w=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F3E476B4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">October 29 - November 11, 2010</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21055432">In Vitro Activity of the Lipopeptide PAL-Lys-Lys-NH2; Alone and in Combination with Antifungal Agents; Against Clinical Isolates of Candida Spp.</a> Kamysz, E., O. Simonetti, O. Cirioni, D. Arzeni, G. Ganzetti, A. Campanati, A. Giacometti, E. Gabrielli, C. Silvestri, W. Kamysz, A. Offidani, and F. Barchies. Peptides, 2010. <b>[Epub ahead of print]</b>; PMID[21055432].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110      </p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21039663">Ciclopirox olamine: an antifungal alternative against cryptococcosis.</a> Oliveira, P.C., C.S. Medeiros, D.P. Macedo, S.L. Andrade, M.T. Correia, S.D. Mesquita, R.G. Lima-Neto, and R.P. Neves. Letters in Applied Microbiology, 2010. 51(5): p. 485-9; PMID[21039663].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20828885">Synthesis of new pyrrolo[2,3-d]pyrimidine derivatives as antibacterial and antifungal agents.</a> Hilmy, K.M., M.M. Khalifa, M.A. Hawata, R.M. Keshk, and A.A. el-Torgman. European Journal of Medicinal Chemistry 2010. 45(11): p. 5243-50; PMID[20828885].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20805011">Design, synthesis and in vitro evaluation of antitubercular and antimicrobial activity of some novel pyranopyrimidines.</a> Kamdar, N.R., D.D. Haveliwala, P.T. Mistry, and S.K. Patel. European Journal of Medicinal Chemistry, 2010. 45(11): p. 5056-63; PMID[20805011].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20392152">Novel antifungal activity of purpurin against Candida species in vitro.</a> Kang, K., W.P. Fong, and P.W. Tsang. Medical Mycology, 2010. 48(7): p. 904-11; PMID[20392152].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20813433">Synthesis and antituberculosis activity of some new pyridazine derivatives.</a> Mantu, D., M.C. Luca, C. Moldoveanu, G. Zbancioc, and Mangalagiu. Part II. European Journal of Medicinal Chemistry, 2010. 45(11): p. 5164-8; PMID[20813433].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20797808">Synthesis and pharmacological evaluation of clubbed isopropylthiazole derived triazolothiadiazoles, triazolothiadiazines and mannich bases as potential antimicrobial and antitubercular agents.</a> Suresh Kumar, G.V., Y. Rajendra Prasad, B.P. Mallikarjuna, and S.M. Chandrashekar. European Journal of Medicinal Chemistry, 2010. 45(11): p. 5120-9; PMID[20797808].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20701975">Design of novel iron compounds as potential therapeutic agents against tuberculosis.</a> Tarallo, M.B., C. Urquiola, A. Monge, B.P. Costa, R.R. Ribeiro, A.J. Costa-Filho, R.C. Mercader, F.R. Pavan, C.Q. Leite, M.H. Torre, and D. Gambino. Journal of Inorganic Biochemistry, 2010. 104(11): p. 1164-70; PMID[20701975].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="../../../Citations%20Listing%20%2355%20October%2029%20-%20November%2011,%202010/%5d%20http:/www.ncbi.nlm.nih.gov/pubmed/21053347">Synthesis and Evaluation of Potent Ene-yne Inhibitors of Type II Dehydroquinases as Tuberculosis Drug Leads.</a> Tran, A.T., K.M. Cergol, N.P. West, E.J. Randall, W.J. Britton, S.A. Bokhari, M. Ibrahim, A.J. Lapthorn, and R.J. Payne. ChemMedChem, 2010. <b>[Epub ahead of print]</b>; PMID[21053347.</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21053346">Inhibitors of the Salicylate Synthase (MbtI) from Mycobacterium tuberculosis Discovered by High-Throughput Screening.</a> Vasan, M., J. Neres, J. Williams, D.J. Wilson, A.M. Teitelbaum, R.P. Remmel, and C.C. Aldrich. ChemMedChem, 2010. <b>[Epub ahead of print]</b>; PMID[21053346].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1029-111110</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283326700006">Antimicrobial activity of highly stable silver nanoparticles embedded in agar-agar matrix as a thin film.</a> Ghosh, S., R. Kaushik, K. Nagalakshmi, S.L. Hoti, G.A. Menezes, B.N. Harish, and H.N. Vasan. Carbohydrate Research, 2010. 345(15): p. 2220-2227; PMID[ISI:000282922900015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_1029-111110.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
