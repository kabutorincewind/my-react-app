

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-292.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hga5df5d6M113UdAeUQ4LeG6CLddhlcoMNkBzaIaqZh3z+8wTtc8oIS37vnlIqgjDTOiDfnLw7DYiWTwIDIc/xMNY1m0+kOtn7VwIdJU4K1Bi0A6NcyfC86R+ts=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8724F274" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-292-MEMO</b> </p>

<p>    1.    43040   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Analysis of the Interactions Between HIV-1 and the Cellular Prion Protein in a Human Cell Line</p>

<p>           Leblanc, P, Baas, D, and Darlix, JL</p>

<p>           J Mol Biol 2004. 337(4): 1035-51</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15033368&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15033368&amp;dopt=abstract</a> </p><br />

<p>    2.    43041   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p><b>           The HIV-1 Nef Protein: How An AIDS Pathogenetic Factor Turns to a Tool for Combating AIDS</b> </p>

<p>           Schiavoni, I, Muratori, C, Piacentini, V, Giammarioli, AM, and Federico, M</p>

<p>           Curr Drug Targets Immune Endocr Metabol Disord 2004. 4(1): 19-27</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15032622&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15032622&amp;dopt=abstract</a> </p><br />

<p>    3.    43042   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Classification and Regression Trees-Studies of HIV Reverse Transcriptase Inhibitors</p>

<p>           Daszykowski, M, Walczak, B, Xu, QS, Daeyaert, F, De Jonge, MR, Heeres, J, Koymans, LM, Lewi, PJ, Vinkers, HM, Janssen, PA, and Massart, DL</p>

<p>           J Chem Inf Comput Sci 2004. 44(2): 716-726</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15032554&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15032554&amp;dopt=abstract</a> </p><br />

<p>    4.    43043   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Synthesis and Biological Evaluation in Human Monocyte-Derived Macrophages of N-(N-Acetyl-l-cysteinyl)-S-acetylcysteamine Analogues with Potent Antioxidant and Anti-HIV Activities</p>

<p>           Oiry, J, Mialocq, P, Puy, JY, Fretier, P, Dereuddre-Bosquet, N, Dormont, D, Imbach, JL, and Clayette, P</p>

<p>           J Med Chem 2004. 47(7): 1789-1795</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027871&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027871&amp;dopt=abstract</a> </p><br />

<p>    5.    43044   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Synthesis, Structure-Activity Relationships, and Mechanism of Drug Resistance of d- and l-beta-3&#39;-Fluoro-2&#39;,3&#39;-unsaturated-4&#39;-thionucleosides as Anti-HIV Agents</p>

<p>           Zhu, W, Chong, Y, Choo, H, Mathews, J, Schinazi, RF, and Chu, CK</p>

<p>           J Med Chem 2004. 47(7): 1631-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027854&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027854&amp;dopt=abstract</a> </p><br />

<p>    6.    43045   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Expression of HIV-1 integrase in CEM cells inhibits HIV-1 replication</p>

<p>           Van Griensven, J, Zhan, X, Van MaeLe B, Pluymers, W, Michiels, M, De Clercq, E, Cherepanov, P, and Debyser, Z</p>

<p>           J Gene Med 2004. 6(3): 268-77</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15026988&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15026988&amp;dopt=abstract</a> </p><br />

<p>    7.    43046   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           6-Aryl-2,4-dioxo-5-hexenoic acids, novel integrase inhibitors active against HIV-1 multiplication in cell-based assays</p>

<p>           Costi, R, Santo, RD, Artico, M, Roux, A, Ragno, R, Massa, S, Tramontano, E, La ColLa, M, Loddo, R, Marongiu, ME, Pani, A, and ColLa, PL</p>

<p>           Bioorg Med Chem Lett 2004. 14(7): 1745-1749</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15026063&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15026063&amp;dopt=abstract</a> </p><br />

<p>    8.    43047   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           A structure-activity study of the inhibition of HIV-1 Tat-dependent trans-activation by mixmer 2&#39;-O-methyl oligoribonucleotides containing locked nucleic acid (LNA), alpha-L-LNA, or 2&#39;-thio-LNA residues</p>

<p>           Arzumanov, A, Stetsenko, DA, Malakhov, AD, Reichelt, S, Sorensen, MD, Babu, BR, Wengel, J, and Gait, MJ</p>

<p>           Oligonucleotides 2003. 13(6): 435-53</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025911&amp;dopt=abstract</a> </p><br />

<p>    9.    43048   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Synthesis of Novel, Multivalent Glycodendrimers as Ligands for HIV-1 gp120</p>

<p>           Kensinger, RD, Yowler, BC, Benesi, AJ, and Schengrund, CL</p>

<p>           Bioconjug Chem 2004. 15(2): 349-358</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025531&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025531&amp;dopt=abstract</a> </p><br />

<p>  10.    43049   HIV-LS-292; EMBASE-HIV-3/24/2004</p>

<p class="memofmt1-2">           High Resolution Crystal Structures of HIV-1 Protease with a Potent Non-peptide Inhibitor (UIC-94017) Active against Multi-drug-resistant Clinical Strains</p>

<p>           Tie, Yunfeng, Boross, Peter I, Wang, Yuan-Fang, Gaddis, Laquasha, Hussain, Azhar K, Leshchenko, Sofiya, Ghosh, Arun K, Louis, John M, Harrison, Robert W, and Weber, Irene T</p>

<p>           Journal of Molecular Biology 2004.  In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WK7-4BXDKHH-1/2/b81ad1ce83769b800e8a6141f00ad826">http://www.sciencedirect.com/science/article/B6WK7-4BXDKHH-1/2/b81ad1ce83769b800e8a6141f00ad826</a> </p><br />

<p>  11.    43050   HIV-LS-292; EMBASE-HIV-3/24/2004</p>

<p class="memofmt1-2">           Interaction of amiloride and one of its derivatives with Vpu from HIV-1: a molecular dynamics simulation</p>

<p>           Lemaitre, V, Ali, R, Kim, CG, Watts, A, and Fischer, WB</p>

<p>           FEBS Letters 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T36-4BXDMJ5-2/2/17f37ff7632c975dc418d4c9a1b3a6bb">http://www.sciencedirect.com/science/article/B6T36-4BXDMJ5-2/2/17f37ff7632c975dc418d4c9a1b3a6bb</a> </p><br />

<p>  12.    43051   HIV-LS-292; EMBASE-HIV-3/24/2004</p>

<p class="memofmt1-2">           De novo design and synthesis of HIV-1 integrase inhibitors</p>

<p>           Makhija, Mahindra T, Kasliwal, Rajesh T, Kulkarni, Vithal M, and Neamati, Nouri</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BX77WS-3/2/e133f0ac2672cf29d8eb3762017b894d">http://www.sciencedirect.com/science/article/B6TF8-4BX77WS-3/2/e133f0ac2672cf29d8eb3762017b894d</a> </p><br />

<p>  13.    43052   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Drug-Resistant Variants That Evolve During Nonsuppressive Therapy Persist in HIV-1-Infected Peripheral Blood Mononuclear Cells After Long-Term Highly Active Antiretroviral Therapy</p>

<p>           VerhofsteDe C, Noe, A, Demecheleer, E, De Cabooter, N, Van WanzeeLe F, Van Der Gucht, B, Vogelaers, D, and Plum, J</p>

<p>           J Acquir Immune Defic Syndr 2004. 35(5): 473-483</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15021312&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15021312&amp;dopt=abstract</a> </p><br />

<p>  14.    43053   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Actin filaments play an essential role for transport of nascent HIV-1 proteins in host cells</p>

<p>           Sasaki, H, Ozaki, H, Karaki, H, and Nonomura, Y</p>

<p>           Biochem Biophys Res Commun 2004. 316(2):  588-93</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15020258&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15020258&amp;dopt=abstract</a> </p><br />

<p>  15.    43054   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           A new method for analysis of AZT-triphosphate and nucleotide-triphosphates</p>

<p>           Van Kampen, JJ, Fraaij, PL, Hira, V, Van Rossum, AM, Hartwig, NG, De Groot, R, and LuiDer TM</p>

<p>           Biochem Biophys Res Commun 2004. 315(1):  151-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15013439&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15013439&amp;dopt=abstract</a> </p><br />

<p>  16.    43055   HIV-LS-292; EMBASE-HIV-3/24/2004</p>

<p class="memofmt1-2">           Crystal Structures of HIV-1 Reverse Transcriptases Mutated at Codons 100, 106 and 108 and Mechanisms of Resistance to Non-nucleoside Inhibitors</p>

<p>           Ren, J, Nichols, CE, Chamberlain, PP, Weaver, KL, Short, SA, and Stammers, DK</p>

<p>           Journal of Molecular Biology 2004.  336(3): 569-578</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WK7-4BD5GRJ-9/2/f2c45436e2125a03885043f73de59d05">http://www.sciencedirect.com/science/article/B6WK7-4BD5GRJ-9/2/f2c45436e2125a03885043f73de59d05</a> </p><br />

<p>  17.    43056   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           A possible improvement for structure-based drug design illustrated by the discovery of a Tat HIV-1 inhibitor</p>

<p>           Montembault, M, Vo-Thanh, G, Deyine, A, Fargeas, V, Villieras, M, Adjou, A, Dubreuil, D, Esquieu, D, Gregoire, C, Opi, S, Peloponese, JM, Campbell, G, Watkins, J, De Mareuil, J, Aubertin, AM, Bailly, C, Loret, E , and Lebreton, J</p>

<p>           Bioorg Med Chem Lett 2004. 14(6): 1543-1546</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006399&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006399&amp;dopt=abstract</a> </p><br />

<p>  18.    43057   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           HIV-1 integrase pharmacophore model derived from diverse classes of inhibitors</p>

<p>           Mustata, GI, Brigo, A, and Briggs, JM</p>

<p>           Bioorg Med Chem Lett 2004. 14(6): 1447-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006380&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006380&amp;dopt=abstract</a> </p><br />

<p>  19.    43058   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Crucial amides for dimerization inhibitors of HIV-1 protease</p>

<p>           Bowman, MJ and Chmielewski, J</p>

<p>           Bioorg Med Chem Lett 2004. 14(6): 1395-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006369&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006369&amp;dopt=abstract</a> </p><br />

<p>  20.    43059   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Actinohivin, a novel anti-human immunodeficiency virus protein from an actinomycete, inhibits viral entry to cells by binding high-mannose type sugar chains of gp120</p>

<p>           Chiba, H, Inokoshi, J, Nakashima, H, Omura, S, and Tanaka, H</p>

<p>           Biochem Biophys Res Commun 2004. 316(1):  203-10</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15003531&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15003531&amp;dopt=abstract</a> </p><br />

<p>  21.    43060   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Bispecific short hairpin siRNA constructs targeted to CD4, CXCR4, and CCR5 confer HIV-1 resistance</p>

<p>           Anderson, J,  Banerjea, A, and Akkina, R</p>

<p>           Oligonucleotides 2003. 13(5): 303-312</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000821&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000821&amp;dopt=abstract</a> </p><br />

<p>  22.    43061   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           The reverse transcriptase (RT) mutation V118I is associated with virologic failure on abacavir-based antiretroviral treatment (ART) in HIV-1 infection</p>

<p>           Saberg, P, Koppel, K, Bratt, G, Fredriksson, EL, Hejdeman, B, Sitbon, G, and Sandstrom, E</p>

<p>           Scand J Infect Dis 2004. 36(1): 40-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000558&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000558&amp;dopt=abstract</a> </p><br />

<p>  23.    43062   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Cytotoxic and anti-HIV principles from the rhizomes of Begonia nantoensis</p>

<p>           Wu, PL, Lin, FW, Wu, TS, Kuoh, CS, Lee, KH, and Lee, SJ</p>

<p>           Chem Pharm Bull (Tokyo) 2004. 52(3): 345-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14993759&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14993759&amp;dopt=abstract</a> </p><br />

<p>  24.    43063   HIV-LS-292; PUBMED-HIV-3/24/2004</p>

<p class="memofmt1-2">           Rapid screening for HIV-1 protease inhibitor leads through X-ray diffraction</p>

<p>           Pillai, B, Kannan, KK, Bhat, SV, and Hosur, MV</p>

<p>           Acta Crystallogr D Biol Crystallogr 2004. 60(Pt 3): 594-596</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14993705&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14993705&amp;dopt=abstract</a> </p><br />

<p>  25.    43064   HIV-LS-292; EMBASE-HIV-3/24/2004</p>

<p class="memofmt1-2">           HIV-1 nef decreases CXCR4 expression on CD4+ T-lymphocytes*1</p>

<p>           Kamchaisatian, W, Haraguchi, S, Good, RA, Day, NK, Emmanuel, P, Sleasman, JW, and Tangsinmankong, N</p>

<p>           Journal of Allergy and Clinical Immunology 2004. 113(2, Supplement 1): S124</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WH4-4BRJ5RW-G5/2/2fe2f1e3fd82090ee94135b5b0d00e83">http://www.sciencedirect.com/science/article/B6WH4-4BRJ5RW-G5/2/2fe2f1e3fd82090ee94135b5b0d00e83</a> </p><br />

<p>  26.    43065   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           HIV viral resistance to antiretroviral drugs: Foreword</p>

<p>           Ippolito, G</p>

<p>           SCAND J INFECT DIS 2003. 35: 4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189255700001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189255700001</a> </p><br />

<p>  27.    43066   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           Synthesis and antiviral activity of betulonic acid amides and conjugates with amino acids</p>

<p>           Flekhter, OB, Boreko, EI, Nigmatullina, LR, Tret&#39;yakova, EV, Pavlova, NI, Baltina, LA, Nikolaeva, SN, Savinova, OV, Eremin, VF, Galin, FZ, and Tolstikov, GA</p>

<p>           RUSS J BIOORG CHEM+ 2004. 30(1): 80-88</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189279900012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189279900012</a> </p><br />

<p>  28.    43067   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           Design and synthesis of alpha Gal-conjugated peptide T20 as novel antiviral agent for HIV-immunotargeting</p>

<p>           Naicker, KP, Li, HG, Heredia, A, Song, HJ, and Wang, LX</p>

<p>           ORG BIOMOL CHEM 2004. 2(5): 660-664</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189235900007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189235900007</a> </p><br />

<p>  29.    43068   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           The distribution of the HIV protease inhibitor, ritonavir, to the brain, cerebrospinal fluid, and choroid plexuses of the guinea pig</p>

<p>           Anthonypillai, C, Sanderson, RN, Gibbs, JE, and Thomas, SA</p>

<p>           J PHARMACOL EXP THER 2004. 308(3):  912-920</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189212300015">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189212300015</a> </p><br />

<p>  30.    43069   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           Identification of a novel CD4i human monoclonal antibody Fab that neutralizes HIV-1 primary isolates from different clades</p>

<p>           Zhang, MY, Shu, Y, Sidorov, I, and Dimitrov, DS</p>

<p>           ANTIVIR RES 2004. 61(3): 161-164</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189131000003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189131000003</a> </p><br />

<p>  31.    43070   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           Characterization of a naphthalene derivative inhibitor of retroviral integrases</p>

<p>           Daniel, R, Myers, CB, Kulkosky, J, Taganov, K, Greger, JG, Merkel, G, Weber, IT, Harrison, RW, and Skalka, AM</p>

<p>           AIDS RES HUM RETROV 2004. 20(2): 135-144</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189260200002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189260200002</a> </p><br />

<p>  32.    43071   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           A potent anti-HIV immunotoxin blocks spreading infection by primary HIV type 1 isolates in multiple cell types</p>

<p>           Lueders, KK,  De Rosa, SC, Valentin, A , Pavlakis, GN, Roederer, M, and Hamer, DH</p>

<p>           AIDS RES HUM RETROV 2004. 20(2): 145-150</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189260200003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189260200003</a> </p><br />

<p>  33.    43072   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           A novel HIV-CCR5 receptor vaccine strategy in the control of mucosal SIV/HIV infection</p>

<p>           Bogers, WMJM, Bergmeier, LA, Ma, J, Oostermeijer, H, Wang, YF, Kelly, CG, ten, Haaft P, Singh, M, Heeney, JL, and Lehner, T</p>

<p>           AIDS 2004. 18(1): 25-36</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189101600003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189101600003</a> </p><br />

<p>  34.    43073   HIV-LS-292; WOS-HIV-3/14/2004</p>

<p class="memofmt1-2">           Decline in HIV infectivity following the introduction of highly active antiretroviral therapy</p>

<p>           Porco, TC, Martin, JN, Page-Shafer, KA, Cheng, A, Charlebois, E, Grant, RM, and Osmond, DH</p>

<p>           AIDS 2004. 18(1): 81-88</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189101600010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189101600010</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
