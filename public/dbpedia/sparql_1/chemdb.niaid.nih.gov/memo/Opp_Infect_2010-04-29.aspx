

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-04-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NS/SUrH63nEaweq6uyoJiqMHPMBrJvgAitN+UYYX2JwuAu+JrQ7Q0udaXxaGr3n5V9M3Ho3yQafYNKaC/bCTiUSAxuw1Jm00kHCv/rGeXrjoIk3W5AQ2q2c17Lk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="678BB4E8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">April 16- April 29, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20420370">3&#39;-Bromo Analogues of Pyrimidine Nucleosides as a New Class of Potent Inhibitors of Mycobacterium tuberculosis.</a> Shakya, N., N.C. Srivastav, N. Desroches, B. Agrawal, D.Y. Kunimoto, and R. Kumar. J Med Chem; <b>[Epub ahead of print]</b> PMID[20420370].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0416-042910.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">2. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276521000008&amp;SID=1FNJh4bejdGDcdG8egg&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis, in vitro antitubercular activity and 3D-QSAR study of 1,4-dihydropyridines.</a> Manvar, A.T., R.R.S. Pissurlenkar, V.R. Virsodia, K.D. Upadhyay, D.R. Manvar, A.K. Mishra, H.D. Acharya, A.R. Parecha, C.D. Dholakia, A.K. Shah, and E.C. Coutinho. Molecular Diversity. 14(2): p. 285-305; ISI[000276521000008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0416-042910.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276363900009&amp;SID=1FNJh4bejdGDcdG8egg&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Analysis of the response of Candida albicans cells to Silver(I).</a> Rowan, R., M. McCann, and K. Kavanagh. Medical Mycology. 48(3): p. 498-505; ISI[000276363900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0416-042910.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276566900011&amp;SID=1FNJh4bejdGDcdG8egg&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">The Molecular Structure of Ornithine Acetyltransferase from Mycobacterium tuberculosis Bound to Ornithine, a Competitive Inhibitor.</a> Sankaranarayanan, R., M.M. Cherney, C. Garen, G. Garen, C.Y. Niu, M. Yuan, and M.N.G. James. Journal of Molecular Biology. 397(4): p. 979-990; ISI[000276566900011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0416-042910.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276258700037&amp;SID=1FNJh4bejdGDcdG8egg&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">New 3-methylquinoxaline-2-carboxamide 1,4-di-N-oxide derivatives as anti-Mycobacterium tuberculosis agents.</a> Ancizu, S., E. Moreno, B. Solano, R. Villar, A. Burguete, E. Torres, S. Perez-Silanes, I. Aldana, and A. Monge. Bioorganic &amp; Medicinal Chemistry. 18(7): p. 2713-2719; ISI[000276258700037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0416-042910.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276323100002&amp;SID=1FNJh4bejdGDcdG8egg&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Antagonistic activity of a novel antibiotic against Mycobacterium tuberculosis.</a> Chen, X.X. and Y. Jun. African Journal of Microbiology Research. 4(5): p. 323-327; ISI[000276323100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0416-042910.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276291500046&amp;SID=1FNJh4bejdGDcdG8egg&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Substituted hydrazinecarbothioamide as potent antitubercular agents: Synthesis and quantitative structure-activity relationship (QSAR).</a> Singh, S., P.K. Mandal, N. Singh, A.K. Misra, V. Chaturvedi, S. Sinha, and A.K. Saxena. Bioorganic &amp; Medicinal Chemistry Letters. 20(8): p. 2597-2600; ISI[000276291500046].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0416-042910.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
