

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-08-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8N8t44OwFd//PY8G2nrKlkJSRxqnS+KQWl/hhQPmwvGrDRFHKJfFCr/t/rsP18Nv4AhskeRlXfSwTYtSWnH6pKhRuH+gpt/w8bTCr17qOVVS0FgFzjE1IF6xMyI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="78A528D0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 2 - August 15, 2013</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23931586">NS5A Inhibitor, Daclatasvir , for the Treatment of Chronic Hepatitis C Virus Infection<i>.</i></a> Herbst, D.A. and K.R. Reddy. Expert Opinion on Investigational Drugs, 2013. <b>[Epub ahead of print]</b>; PMID[23931586].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23582447">Novel Imino Sugar Alpha-Glucosidase Inhibitors as Antiviral Compounds<i>.</i></a> Howe, J.D., N. Smith, M.J. Lee, N. Ardes-Guisot, B. Vauzeilles, J. Desire, A. Baron, Y. Bleriot, M. Sollogoub, D.S. Alonzi, and T.D. Butters. Bioorganic &amp; Medicinal Chemistry, 2013. 21(16): p. 4831-4838; PMID[23582447].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23943612">A Conformational Mimetic Approach for the Synthesis of Carbocyclic Nucleosides as anti-HCV Leads<i>.</i></a> Kasula, M., T. Balaraju, M. Toyama, A. Thiyagarajan, C. Bal, M. Baba, and A. Sharon. ChemMedChem, 2013. <b>[Epub ahead of print]</b>; PMID[23943612].</p>

    <p class="plaintext"> <b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23919347">Development of a Practical, Asymmetric Synthesis of the Hepatitis C Virus Protease Inhibitor MK-5172<i>.</i></a> Kuethe, J., Y.L. Zhong, N. Yasuda, G. Beutner, K. Linn, M. Kim, B. Marcune, S.D. Dreher, G. Humphrey, and T. Pei. Organic Letters, 2013. <b>[Epub ahead of print]</b>; PMID[23919347].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23849880">Cyclophilin Inhibitors as Antiviral Agents<i>.</i></a> Peel, M. and A. Scribner. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4485-4492; PMID[23849880].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23913364">Highly Efficient Infectious Cell Culture of Three HCV Genotype 2b Strains and Sensitivity to Lead Protease, NS5A, and Polymerase Inhibitors<i>.</i></a> Ramirez, S., Y.P. Li, S.B. Jensen, J. Pedersen, J.M. Gottwein, and J. Bukh. Hepatology, 2013. <b>[Epub ahead of print]</b>; PMID[23913364].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856047">Tetrazole and Triazole as Bioisosteres of Carboxylic Acid: Discovery of Diketo Tetrazoles and Diketo Triazoles as anti-HCV Agents<i>.</i></a> Song, W.H., M.M. Liu, D.W. Zhong, Y.L. Zhu, M. Bosscher, L. Zhou, D.Y. Ye, and Z.H. Yuan. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4528-4531; PMID[23856047].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">8<a href="http://www.ncbi.nlm.nih.gov/pubmed/23939896">. In Vitro Characterization of GSK5852, a Novel HCV Polymerase Inhibitor<i>.</i></a> Voitenleitner, C., R. Crosby, J. Walker, K. Remlinger, J. Vamathevan, A. Wang, S. You, J. Johnson, 3rd, E. Woldu, S. Van Horn, J. Horton, K. Creech, J.B. Shotwell, Z. Hong, and R. Hamatake. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23939896].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23939899">Preclinical Characterization of the Novel Hepatitis C Virus NS3 Protease Inhibitor GS-9451<i>.</i></a> Yang, H., M. Robinson, A.C. Corsa, B. Peng, G. Cheng, Y. Tian, Y. Wang, R. Pakdaman, M. Shen, X. Qi, H. Mo, C. Tay, S. Krawczyk, X.C. Sheng, C.U. Kim, C. Yang, and W.E.t. Delaney. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23939899].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">10<a href="http://www.ncbi.nlm.nih.gov/pubmed/23933116">. Ribonucleotide Reductase Inhibitors Hydroxyurea, Didox, and Trimidox Inhibit Human Cytomegalovirus Replication in Vitro and Synergize with Ganciclovir<i>.</i></a> Bhave, S., H. Elford, and M.A. McVoy. Antiviral Research, 2013. <b>[Epub ahead of print]</b>; PMID[23933116].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">11<a href="http://www.ncbi.nlm.nih.gov/pubmed/23924316">. Broad-spectrum Antiviral Activity of Chebulagic Acid and Punicalagin against Viruses That Use Glycosaminoglycans for Entry<i>.</i></a> Lin, L.T., T.Y. Chen, S.C. Lin, C.Y. Chung, T.C. Lin, G.H. Wang, R. Anderson, C.C. Lin, and C.D. Richardson. BMC Microbiology, 2013. 13: p. 187; PMID[23924316].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0802-081513.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321432300009">Allosteric N-Acetamide-indole-6-carboxylic acid Thumb Pocket 1 Inhibitors of Hepatitis C Virus NS5B Polymerase - Acylsulfonamides and Acylsulfamides as Carboxylic acid Replacements<i>.</i></a> Beaulieu, P.L., R. Coulombe, J. Gillard, C. Brochu, J.M. Duan, M. Garneau, E. Jolicoeur, P. Kuhn, M.A. Poupart, J. Rancourt, T.A. Stammers, B. Thavonekham, and G. Kukolj. Canadian Journal of Chemistry, 2013. 91(1): p. 66-81; ISI[000321432300009].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321230300046">3-Biphenylimidazo[1,2-a]pyridines or [1,2-b]pyridazines and Analogues, Novel Flaviviridae Inhibitors<i>.</i></a> Enguehard-Gueiffier, C., S. Musiu, N. Henry, J.B. Veron, S. Mavel, J. Neyts, P. Leyssen, J. Paeshuyse, and A. Gueiffier. European Journal of Medicinal Chemistry, 2013. 64: p. 448-463; ISI[000321230300046].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321447400009">New Direct-active Antiviral Agents for Hepatitis C Virus Infection in Previous Treatment Failure<i>.</i></a> Hoepelman, A.I.M. International Journal of Antimicrobial Agents, 2013. 42: p. S3-S4; ISI[000321447400009].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321883800006">Identification of a Series of Compounds with Potent Antiviral Activity for the Treatment of Enterovirus Infections<i>.</i></a> MacLeod, A.M., D.R. Mitchell, N.J. Palmer, H.V. de Poel, K. Conrath, M. Andrews, P. Leyssen, and J. Neyts. ACS Medicinal Chemistry Letters, 2013. 4(7): p. 20-24; ISI[000321883800006].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321337600016">Very Low-density Lipoprotein/Lipo-viro Particles Reverse Lipoprotein Lipase-mediated Inhibition of Hepatitis C Virus Infection Via Apolipoprotein C-III<i>.</i></a> Sun, H.Y., C.C. Lin, J.C. Lee, S.W. Wang, P.N. Cheng, I.C. Wu, T.T. Chang, M.D. Lai, D.B. Shieh, and K.C. Young. Gut, 2013. 62(8): p. 1193-1203; ISI[000321337600016].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0802-081513.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321230300017">Elucidation of the Pharmacophore of Echinocystic acid, a New Lead for Blocking HCV Entry<i>.</i></a> Wang, H., Q. Wang, S.L. Xiaoa, F. Yu, M. Ye, Y.X. Zheng, C.K. Zhao, D.A. Sun, L.H. Zhang, and D.M. Zhou. European Journal of Medicinal Chemistry, 2013. 64: p. 160-168; ISI[000321230300017].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0802-081513.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
