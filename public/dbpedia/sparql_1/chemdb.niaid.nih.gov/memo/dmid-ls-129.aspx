

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-129.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QLp2L2X/6Gxl+oKokc53sUv/6tinHPu4GggvXy04mWnYoeeRtwev2GXAaVh64IHMaNzjjApIQXD6Y21a889fxP47+tgEUOZHlXMPGTErRSaV1T19eJtxV1JoSY8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F16CCC37" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-129-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59108   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Benzo[2,3]azepino[4,5-b]indol-6-ones as kinase or phosphatase inhibitors, their preparation, pharmaceutical compositions, and use against infectious diseases</p>

    <p>          Klebl, Bert, Neumann, Lars, Hafenbradl, Doris, Greff, Zoltan, Keri, Gyoergy, and Oerfi, Laszlo </p>

    <p>          PATENT:  WO <b>2006089874</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 67pp.</p>

    <p>          ASSIGNEE:  (Gpc Biotech AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     59109   DMID-LS-129; PUBMED-DMID-10/9/2006 </p>

    <p class="memofmt1-2">          Hepatitis C virus RNA replication is regulated by FKBP8 and Hsp90</p>

    <p>          Okamoto, T, Nishimura, Y, Ichimura, T, Suzuki, K, Miyamura, T, Suzuki, T, Moriishi, K, and Matsuura, Y</p>

    <p>          EMBO J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17024179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17024179&amp;dopt=abstract</a> </p><br />

    <p>3.     59110   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Polysaccharide-oligoamine conjugates as anti-amyloid and anti-viral agents</p>

    <p>          Domb, Abraham J, Katz, Ehud, Yudovin-Farber, Ira, and Taraboulos, Albert</p>

    <p>          PATENT:  WO <b>2006090365</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 44pp.</p>

    <p>          ASSIGNEE:  (Yissum Research Development Company, Israel</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     59111   DMID-LS-129; PUBMED-DMID-10/9/2006 </p>

    <p class="memofmt1-2">          A second, non-canonical RNA-dependent RNA polymerase in SARS Coronavirus</p>

    <p>          Imbert, I, Guillemot, JC, Bourhis, JM, Bussetta, C, Coutard, B, Egloff, MP, Ferron, F, Gorbalenya, AE, and Canard, B</p>

    <p>          EMBO J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17024178&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17024178&amp;dopt=abstract</a> </p><br />

    <p>5.     59112   DMID-LS-129; PUBMED-DMID-10/9/2006 </p>

    <p class="memofmt1-2">          Development and validation of a capillary electrophoresis method for the characterization of herpes simplex virus type 1 (HSV-1) thymidine kinase substrates and inhibitors</p>

    <p>          Iqbal, J, Scapozza, L, Folkers, G, and Muller, CE</p>

    <p>          J Chromatogr B Analyt Technol Biomed Life Sci <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17023224&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17023224&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59113   DMID-LS-129; PUBMED-DMID-10/9/2006 </p>

    <p class="memofmt1-2">          Antiviral treatment of hepatitis C</p>

    <p>          Toniutto, P, Fabris, C, and Pirisi, M</p>

    <p>          Expert Opin Pharmacother <b>2006</b>.  7(15): 2025-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020430&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020430&amp;dopt=abstract</a> </p><br />

    <p>7.     59114   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Sulfur oligonucleotides having antiviral activities</p>

    <p>          Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>          PATENT:  WO <b>2006096995</b>  ISSUE DATE:  20060921</p>

    <p>          APPLICATION: 2006  PP: 51pp.</p>

    <p>          ASSIGNEE:  (Replicor Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     59115   DMID-LS-129; PUBMED-DMID-10/9/2006 </p>

    <p><b>          Modular alpha-Helical Mimetics with Antiviral Activity against Respiratory Syncitial Virus</b> </p>

    <p>          Shepherd, NE, Hoang, HN, Desai, VS, Letouze, E, Young, PR, and Fairlie, DP</p>

    <p>          J Am Chem Soc <b>2006</b>.  128(40): 13284-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17017810&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17017810&amp;dopt=abstract</a> </p><br />

    <p>9.     59116   DMID-LS-129; PUBMED-DMID-10/9/2006 </p>

    <p class="memofmt1-2">          In Vitro Antiviral Activity of 1,2,3,4,6-Penta-O-galloyl-beta-D-glucose against Hepatitis B Virus</p>

    <p>          Lee, SJ, Lee, HK, Jung, MK, and Mar, W</p>

    <p>          Biol Pharm Bull <b>2006</b>.  29(10): 2131-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015965&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015965&amp;dopt=abstract</a> </p><br />

    <p>10.   59117   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Glycan microarray technologies: tools to survey host specificity of influenza viruses</p>

    <p>          Stevens, J, Blixt, O, Paulson, JC, and Wilson, IA</p>

    <p>          Nat Rev Microbiol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17013397&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17013397&amp;dopt=abstract</a> </p><br />

    <p>11.   59118   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Validation of biocides against duck hepatitis B virus as a surrogate virus for human hepatitis B virus</p>

    <p>          Sauerbrei, A, Schacke, M, Gluck, B, Egerer, R, and Wutzler, P</p>

    <p>          J Hosp Infect <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17011665&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17011665&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   59119   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Hepatitis C Virus RNA-Dependent RNA Polymerase: Study on the Inhibition Mechanism by Pyrogallol Derivatives</p>

    <p>          Kozlov, MV, Polyakov, KM, Ivanov, AV, Filippova, SE, Kuzyakin, AO, Tunitskaya, VL, and Kochetkov, SN</p>

    <p>          Biochemistry (Mosc) <b>2006</b>.  71(9): 1021-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17009957&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17009957&amp;dopt=abstract</a> </p><br />

    <p>13.   59120   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Novel methods to treat and prevent human papillomavirus infection</p>

    <p>          Kendrick, JE, Huh, WK, and Alvarez, RD</p>

    <p>          Expert Rev Anti Infect Ther <b>2006</b>.  4(4): 593-600</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17009939&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17009939&amp;dopt=abstract</a> </p><br />

    <p>14.   59121   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Clevudine: a potent inhibitor of hepatitis B virus in vitro and in vivo</p>

    <p>          Korba, BE, Furman, PA, and Otto, MJ</p>

    <p>          Expert Rev Anti Infect Ther <b>2006</b>.  4(4): 549-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17009935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17009935&amp;dopt=abstract</a> </p><br />

    <p>15.   59122   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Crimean-Congo Hemorrhagic Fever</p>

    <p>          Gunes Turabi</p>

    <p>          Mikrobiyoloji bulteni <b>2006</b>.  40(3): 279-87.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   59123   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Biological interactions between herpesviruses and cyclooxygenase enzymes</p>

    <p>          Reynolds, AE and Enquist, LW</p>

    <p>          Rev Med Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17006962&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17006962&amp;dopt=abstract</a> </p><br />

    <p>17.   59124   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          BAY 41-4109 has multiple effects on Hepatitis B virus capsid assembly</p>

    <p>          Stray, SJ and Zlotnick, A</p>

    <p>          J Mol Recognit  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17006877&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17006877&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   59125   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Maribavir antagonizes the antiviral action of ganciclovir on human cytomegalovirus</p>

    <p>          Chou, S and Marousek, GI</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(10): 3470-3472</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005835&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005835&amp;dopt=abstract</a> </p><br />

    <p>19.   59126   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Ribavirin antagonizes the in vitro anti-hepatitis C virus activity of 2&#39;-C-methylcytidine, the active component of valopicitabine</p>

    <p>          Coelmont, L, Paeshuyse, J, Windisch, MP, De, Clercq E, Bartenschlager, R, and Neyts, J</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(10): 3444-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005827&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005827&amp;dopt=abstract</a> </p><br />

    <p>20.   59127   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Tyro3 family-mediated cell entry of ebola and marburg viruses</p>

    <p>          Shimojima, M, Takada, A, Ebihara, H, Neumann, G, Fujioka, K, Irimura, T, Jones, S, Feldmann, H, and Kawaoka, Y</p>

    <p>          J Virol <b>2006</b>.  80(20): 10109-16</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005688&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005688&amp;dopt=abstract</a> </p><br />

    <p>21.   59128   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Identification of two amino acid residues on Ebola virus glycoprotein 1 critical for cell entry</p>

    <p>          Mpanju, Onesmo M, Towner, Jonathan S, Dover, Jason E, Nichol, Stuart T, and Wilson, Carolyn A</p>

    <p>          Virus Research  <b>2006</b>.  121(2): 205-214</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   59129   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Treatments for Flaviviridae virus infection</p>

    <p>          Malcolm, Bruce A, Palermo, Robert, Tong, Xiao, Feld, Boris, and Le, Hung</p>

    <p>          PATENT:  US <b>2006198824</b>  ISSUE DATE:  20060907</p>

    <p>          APPLICATION: 2006-37328  PP: 37pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.   59130   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Discovery of (1R,5S)-N-[3-Amino-1-(cyclobutylmethyl)-2,3-dioxopropyl]- 3-[2(S)-[[[(1,1-dimethylethyl)amino]carbonyl]amino]-3,3-dimethyl-1-oxobuty l]- 6,6-dimethyl-3-azabicyclo[3.1.0]hexan-2(S)-carboxamide (SCH 503034), a Selective, Potent, Orally Bioavailable Hepatitis C Virus NS3 Protease Inhibitor: A Potential Therapeutic Agent for the Treatment of Hepatitis C Infection</p>

    <p>          Venkatraman, S, Bogen, SL, Arasappan, A, Bennett, F, Chen, K, Jao, E, Liu, YT, Lovey, R, Hendrata, S, Huang, Y, Pan, W, Parekh, T, Pinto, P, Popov, V, Pike, R, Ruan, S, Santhanam, B, Vibulbhan, B, Wu, W, Yang, W, Kong, J, Liang, X, Wong, J, Liu, R, Butkiewicz, N, Chase, R, Hart, A, Agrawal, S, Ingravallo, P, Pichardo, J, Kong, R, Baroudy, B, Malcolm, B, Guo, Z, Prongay, A, Madison, V, Broske, L, Cui, X, Cheng, KC, Hsieh, Y, Brisson, JM, Prelusky, D, Korfmacher, W, White, R, Bogdanowich-Knipp, S, Pavlovsky, A, Bradley, P, Saksena, AK, Ganguly, A, Piwinski, J, Girijavallabhan, V, and Njoroge, FG</p>

    <p>          J Med Chem <b>2006</b>.  49(20): 6074-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17004721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17004721&amp;dopt=abstract</a> </p><br />

    <p>24.   59131   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Virtual screening for finding natural inhibitor against cathepsin-L for SARS therapy</p>

    <p>          Wang, SQ, Du, QS, Zhao, K, Li, AX, Wei, DQ, and Chou, KC</p>

    <p>          Amino Acids <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16998715&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16998715&amp;dopt=abstract</a> </p><br />

    <p>25.   59132   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of b-polypeptides that inhibit cytomegalovirus infection</p>

    <p>          Compton, Teresa, Gellman, Samuel H, English, Emily P, and Chumanov, Robert S</p>

    <p>          PATENT:  WO <b>2006099170</b>  ISSUE DATE:  20060921</p>

    <p>          APPLICATION: 2006  PP: 69pp.</p>

    <p>          ASSIGNEE:  (Wisconsin Alumni Research Foundation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   59133   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of bisheterocyclic compounds as antiviral agents</p>

    <p>          Nan, Fajun, Zuo, Jianping, Wang, Wenlong, Wang, Guifeng, Chen, Haijun, and He, Peilan</p>

    <p>          PATENT:  WO <b>2006097030</b>  ISSUE DATE:  20060921</p>

    <p>          APPLICATION: 2006  PP: 71pp.</p>

    <p>          ASSIGNEE:  (Shanghai Institute of Materia Medica , Chinese Academy of Sciences Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   59134   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          In vitro comparative experiments of aciclovir injectables, Acirovec injection and Zovirax for I.V. infusion, on anti-herpes virus activity</p>

    <p>          Ogura, Takeharu, Kataoka, Hirofumi, Ohtsubo, Yoshikazu, Ueda, Haruyasu, and Okamura, Haruki</p>

    <p>          Igaku to Yakugaku <b>2006</b>.  55(6): 893-900</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>28.   59135   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Viral protein epitopes and antiviral agents for diagnosis, prognosis and treatment of antiviral agent-resistant HIV-1 and hepatitis B virus infection</p>

    <p>          Catanzaro, Andrew, Yarchoan, Robert, Berzofsky, Jay A, Okazaki, Takahiro, Snyder, James T II, and Broder, Samuel</p>

    <p>          PATENT:  WO <b>2006091798</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 103pp.</p>

    <p>          ASSIGNEE:  (The Government of the United States of America, As Represented by the Secretary of the Department of Health and Human Services USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   59136   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of imidazol-4-ylureas as antiviral agents</p>

    <p>          Zimmermann, Holger, Brueckner, David, Henninger, Kerstin, Rosentreter, Ulrich, Hendrix, Martin, Keldenich, Joerg, Lang, Dieter, Radtke, Martin, Paulsen, Daniela, and Kern, Armin</p>

    <p>          PATENT:  WO <b>2006089664</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 62pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   59137   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          ANTI-INFLUENZA VIRAL PRODRUG OSELTAMIVIR IS ACTIVATED BY CARBOXYLESTERASE HCE1 AND THE ACTIVATION IS INHIBITED BY ANTI-PLATELET AGENT CLOPIDOGREL</p>

    <p>          Shi, D, Yang, J, Yang, D, Lecluyse, EL, Black, C, You, L, Akhlaghi, F, and Yan, B</p>

    <p>          J Pharmacol Exp Ther <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16966469&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16966469&amp;dopt=abstract</a> </p><br />

    <p>31.   59138   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          A Luciferase-Based Budding Assay for Ebola Virus</p>

    <p>          Mccarthy, S., Licata, J., and Harty, R.</p>

    <p>          Journal of Virological Methods <b>2006</b>.  137(1): 115-119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240531000016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240531000016</a> </p><br />

    <p>32.   59139   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Aryl furano pyrimidines: The most potent and selective anti-VZV agents reported to date</p>

    <p>          McGuigan, Christopher and Balzarini, Jan</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 149-153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   59140   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Structure and Antiviral Activity of the Galactofucan Sulfates Extracted from Undaria Pinnatifida (Phaeophyta)</p>

    <p>          Hemmingson, JA, Falshaw, R, Furneaux, RH, and Thompson, K</p>

    <p>          Journal of Applied Phycology <b>2006</b>.  18(2): 185-193</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>34.   59141   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          The Synthesis and Antiviral Properties of Acyclic Nucleoside Analogues With a Phosphonomethoxy Fragment in the Side Chain</p>

    <p>          Khandazhinskaya, A., Yasko, M., and Shirokova, E.</p>

    <p>          Current Medicinal Chemistry <b>2006</b>.  13(24): 2953-2980</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240453700008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240453700008</a> </p><br />

    <p>35.   59142   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Improved Replicon Cellular Activity of Non-Nucleoside Allosteric Inhibitors of Hcvns5b Polymerase: From Benzimidazole to Indole Scaffolds</p>

    <p>          Beaulieu, P. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(19): 4987-4993</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615400001</a> </p><br />

    <p>36.   59143   DMID-LS-129; PUBMED-DMID-10/9/2006</p>

    <p class="memofmt1-2">          The crystal structure of the Venezuelan equine encephalitis alphavirus nsP2 protease</p>

    <p>          Russo, AT, White, MA, and Watowich, SJ</p>

    <p>          Structure <b>2006</b>.  14(9): 1449-58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16962975&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16962975&amp;dopt=abstract</a> </p><br />

    <p>37.   59144   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          The synergistic effect of IFN-a and IFN-g against HSV-2 replication in vero cells is not interfered by the plant antiviral 1-cinnamoyl-3, 11-dihydroxymeliacarpin</p>

    <p>          Petrera, Erina and Coto, Celia E</p>

    <p>          Virology Journal <b>2006</b>.  3: No pp. given</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   59145   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Design and Synthesis of Dipeptidyl Glutaminyl Fluoromethylketones as Potent Sars-Cov Inhibitors</p>

    <p>          Zhang, H. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2006</b>.  231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906604">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906604</a> </p><br />

    <p>39.   59146   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          In vitro inhibitory effects of recombinant consensus interferon a on herpes simplex type 1 and type 2 viruses</p>

    <p>          Xu, Bing, Wang, Shu-qin, Li, Yu-huan, and Tao, Pei-zhen</p>

    <p>          Redai Yixue Zazhi <b>2006</b>.  6(4): 397-399</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   59147   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Identification of Pyrazolo[3,4-D]Pyrimidines as Inhibitors of a Hepatitis C Virus Replicon System</p>

    <p>          Barnes, M. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2006</b>.  231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906605">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906605</a> </p><br />
    <br clear="all">

    <p>41.   59148   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of nucleoside analogues with 6-membered carbon ring structure as antiviral agents</p>

    <p>          Lou, Hongxiang, Zhan, Tianrong, Ma, Yudao, and Fan, Peihong</p>

    <p>          PATENT:  CN <b>1803819</b>  ISSUE DATE: 20060719</p>

    <p>          APPLICATION: 1004-2184  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (Shandong University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   59149   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Rational Design of Measles Virus Entry Inhibitors and Mechanisms of Viral Resistance</p>

    <p>          Prussia, A. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2006</b>.  231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906606">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906606</a> </p><br />

    <p>43.   59150   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          A New Triterpene and Anti-Hepatitis B Virus Active Compounds From Alisma Orientalis</p>

    <p>          Jiang, Z. <i>et al.</i></p>

    <p>          Planta Medica <b>2006</b>.  72(10): 951-954</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240478500019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240478500019</a> </p><br />

    <p>44.   59151   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of 4-(1H-Indol-3-yl)-pyrimidin-2-ylamine Derivatives as Protein Kinase Inhibitors and Their Use in Therapy</p>

    <p>          Fischer, Peter Martin, Wang, Shudong, Meades, Christopher Keith, Andrews, Martin James Inglis, Gibson, Darren, and Duncan, Kenneth</p>

    <p>          PATENT:  WO <b>2006075152</b>  ISSUE DATE:  20060720</p>

    <p>          APPLICATION: 2006  PP: 88 pp.</p>

    <p>          ASSIGNEE:  (Cyclacel Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>45.   59152   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Efficacy of Antiviral Compounds in Human Herpesvirus-6-Infected Glial Cells</p>

    <p>          Akhyani, N. <i>et al.</i></p>

    <p>          Journal of Neurovirology <b>2006</b>.  12(4): 284-293</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240435300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240435300005</a> </p><br />

    <p>46.   59153   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Protein Kinase Ck2: a Potential Target in the Development of Novel Therapeutics Against West Nile Virus</p>

    <p>          Ramanathan, M. <i>et al.</i></p>

    <p>          Journal of Neurovirology <b>2006</b>.  12: 69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238758300163">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238758300163</a> </p><br />

    <p>47.   59154   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Neuraminidase Inhibitors Prevent Influenza Infection in Patients After Contact With an Influenza Index Case</p>

    <p>          Behrens, T. and Ahrens, W.</p>

    <p>          European Journal of Epidemiology <b>2006</b>.  21: 108-109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239245500355">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239245500355</a> </p><br />

    <p>48.   59155   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Three-Dimensional Culture Models for Human Viral Diseases and Antiviral Drug Development</p>

    <p>          Andrei, G.</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 96-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200004</a> </p><br />

    <p>49.   59156   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Influenza and the pandemic threat</p>

    <p>          Lee, V J, Fernandez, G G, Chen M I, Lye, D, and Leo, Y S</p>

    <p>          Singapore medical journal <b>2006</b>.  47(6): 463-470</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>50.   59157   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Ribonucleotide Reductase Inhibitors as Anti-Herpes Agents</p>

    <p>          Wnuk, S. and Robins, M.</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 122-126</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200007</a> </p><br />

    <p>51.   59158   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Tricyclic Nucleoside Analogues as Antiherpes Agents</p>

    <p>          Golankiewicz, B. and Ostrowski, T.</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 134-140</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200009</a> </p><br />

    <p>52.   59159   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Papillomavirus and Treatment</p>

    <p>          Snoeck, R.</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 181-191</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200015</a> </p><br />

    <p>53.   59160   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Antiviral drug discovery targeting to viral proteases</p>

    <p>          Hsu John T A, Wang Hsiang-Ching, Chen Guang-Wu, and Shih Shin-Ru</p>

    <p>          Current pharmaceutical design <b>2006</b>.  12(11): 1301-14.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>54.   59161   DMID-LS-129; WOS-DMID-10/9/2006</p>

    <p class="memofmt1-2">          New Developments in the Treatment of Chronic Hepatitis B</p>

    <p>          Hadziyannis, S.</p>

    <p>          Expert Opinion on Biological Therapy <b>2006</b>.  6(9): 913-921</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240336300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240336300006</a> </p><br />

    <p>55.   59162   DMID-LS-129; SCIFINDER-DMID-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of peptides as coronavirus inhibitors</p>

    <p>          Kania, Robert Steven, Mitchell, Lennert Joseph Jr, and Nieman, James A</p>

    <p>          PATENT:  WO <b>2006061714</b>  ISSUE DATE:  20060615</p>

    <p>          APPLICATION: 2005  PP: 43 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
