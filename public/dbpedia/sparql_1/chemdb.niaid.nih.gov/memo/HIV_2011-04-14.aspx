

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-04-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dCgq782zhQX0xSUE2pnEnf+f6woNwCw2g8ijV5q8R3K7xuMTtEHlU38sYs8YnQe0bOVGdq0OEy4aWkj8G5lfW7ToMqGfLEXowMSkVzZPN2ol1et1CMKfYJ0CqXk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3B1CEECC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 01, 2011 - April 14, 2011</h1>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="memofmt2-h1"> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21382714">Synthesis and Biological Evaluation of Fatty Acyl Ester Derivatives of 2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-dideoxythymidine.</a> Agarwal, H.K., K. Loethan, D. Mandal, G.F. Doncel, and K. Parang. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(7): p. 1917-1921; PMID[21382714].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21295891">Synthesis of 9-Substituted Derivatives of Berberine as Anti-HIV Agents.</a> Bodiwala, H.S., S. Sabde, D. Mitra, K.K. Bhutani, and I.P. Singh. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1045-1049; PMID[21295891].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21245449">In Vitro Characterization of Gs-8374, a Novel Phosphonate-Containing Inhibitor of HIV-1 Protease with a Favorable Resistance Profile.</a> Callebaut, C., K. Stray, L. Tsai, M. Williams, Z.Y. Yang, C. Cannizzaro, S.A. Leavitt, X. Liu, K. Wang, B.P. Murray, A. Mulato, M. Hatada, T. Priskich, N. Parkin, S. Swaminathan, W. Lee, G.X. He, L. Xu, and T. Cihlar. Antimicrobial Agents and Chemotherapy, 2011. 55(4): p. 1366-1376; PMID[21245449].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21454634">Hormonally Active Vitamin D3 (1{Alpha},25-Dihydroxycholecalciferol) Triggers Autophagy in Human Macrophages That Inhibits HIV-1 Infection.</a> Campbell, G.R. and S.A. Spector. Journal of Biological Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21454634].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21462994">The Absolute Configuration of Anti-HIV-1 Agent (-)-Concentricolide : Total Synthesis of (+)-(R)-Concentricolide.</a> Chang, C.W. and R.J. Chein. The Journal of Organic Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21462994].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21441484">Advances of Research on Anti-HIV Agents from Traditional Chinese Herbs.</a> Chu, Y. and H. Liu. Advances in Dental Research, 2011. 23(1): p. 67-75; PMID[21441484].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21468934">Down-Regulation of HIV-1 Infection by Inhibition of the Mapk Signaling Pathway.</a> Gong, J., X.H. Shen, C. Chen, H. Qiu, and R.G. Yang. Virologica Sinica, 2011. 26(2): p. 114-122; PMID[21468934].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21483687">Activation of Latent Hiv Using Drug-Loaded Nanoparticles.</a> Kovochich, M., M.D. Marsden, and J.A. Zack. PLoS One, 2011. 6(4): p. e18270; PMID[21483687].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21474836">Imaging-Based Assay for Identification and Characterization of Inhibitors of Cxcr4-Tropic HIV-1 Envelope-Dependent Cell-Cell Fusion.</a> Kramer, S., P. Buontempo, S. Agrawal, and R. Ralston. Journal of Biomolecular Screening, 2011.  <b>[Epub ahead of print]</b>; PMID[21474836].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21339030">Rhodium(II) Acetate-Catalyzed Stereoselective Synthesis, SAR and Anti-HIV Activity of Novel Oxindoles Bearing Cyclopropane Ring.</a> Kumari, G., Nutan, M. Modi, S.K. Gupta, and R.K. Singh. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1181-1188; PMID[21339030].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21393163">Phenotypic Characterization of Drug Resistance-Associated Mutations in HIV-1 Rt Connection and Rnase H Domains and Their Correlation with Thymidine Analogue Mutations.</a> Lengruber, R.B., K.A. Delviks-Frankenberry, G.N. Nikolenko, J. Baumann, A.F. Santos, V.K. Pathak, and M.A. Soares. Journal of Antimicrobial Chemotherapy, 2011. 66(4): p. 702-708; PMID[21393163].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21456073">Synthesis and Anti-HIV-1 Activity of New Fluoro-Hept Analogues: An Investigation on Fluoro Versus Hydroxy Substituents.</a> Loksha, Y.M., E.B. Pedersen, R. Loddo, and P. La Colla. Archiv der Pharmazie (Weinheim), 2011.  <b>[Epub ahead of print]</b>; PMID[21456073].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21245437">Vaginal Microbicide Gel for Delivery of Iqp-0528, a Pyrimidinedione Analog with a Dual Mechanism of Action against HIV-1.</a> Mahalingam, A., A.P. Simmons, S.R. Ugaonkar, K.M. Watson, C.S. Dezzutti, L.C. Rohan, R.W. Buckheit, Jr., and P.F. Kiser. Antimicrobial Agents and Chemotherapy, 2011. 55(4): p. 1650-1660; PMID[21245437].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21282419">Impact of the N348i Mutation in HIV-1 Reverse Transcriptase on Nonnucleoside Reverse Transcriptase Inhibitor Resistance in Non-Subtype B HIV-1.</a> McCormick, A.L., C.M. Parry, A. Crombe, R.L. Goodall, R.K. Gupta, P. Kaleebu, C. Kityo, M. Chirara, G.J. Towers, and D. Pillay. Antimicrobial Agents and Chemotherapy, 2011. 55(4): p. 1806-1809; PMID[21282419].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21453487">HIV Integrase Variability and Genetic Barrier in Antiretroviral Naive and Experienced Patients.</a> Piralla, A., S. Paolucci, R. Gulminetti, G. Comolli, and F. Baldanti. Virology Journal, 2011. 8(1): p. 149; PMID[21453487].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21460211">High Mannose-Binding Lectin with Preference for the Cluster of {Alpha}1-2 Mannose from the Green Alga Boodlea Coacta Is a Potent Entry Inhibitor of HIV-1 and Influenza Viruses.</a> Sato, Y., M. Hirayama, K. Morimoto, N. Yamamoto, S. Okuyama, and K. Hori. Journal of Biological Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21460211].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21282453">Human Immunodeficiency Virus Type 1 Resistance or Cross-Resistance to Nonnucleoside Reverse Transcriptase Inhibitors Currently under Development as Microbicides.</a> Selhorst, P., A.C. Vazquez, K. Terrazas-Aranda, J. Michiels, K. Vereecken, L. Heyndrickx, J. Weber, M.E. Quinones-Mateu, K.K. Arien, and G. Vanham. Antimicrobial Agents and Chemotherapy, 2011. 55(4): p. 1403-1413; PMID[21282453].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21478177">Glu659leu Substitution of Recombinant Hiv Fusion Inhibitor C52l Induces Soluble Expression in Escherichia Coli with Equivalent Anti-HIV Potency.</a> Shang, S., S. Tan, K. Li, J. Wu, H. Lin, S. Liu, and Y. Deng. Protein Engineering, Design and Selection, 2011.  <b>[Epub ahead of print]</b>; PMID[21478177].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21459966">Identification and Characterization of Incb9471, an Allosteric Non-Competitive Small Molecule Antagonist of Ccr5 with Potent Inhibitory Activity against Monocyte Migration and HIV-1 Infection.</a> Shin, N., K. Solomon, N. Zhou, K.H. Wang, V. Garlapati, B. Thomas, Y. Li, M. Covington, F. Baribaud, S. Erickson-Viitanen, P. Czerniak, N. Contel, P. Liu, T. Burn, G. Hollis, S. Yeleswaram, K. Vaddi, C.B. Xue, B. Metcalf, S. Friedman, P. Scherle, and R. Newton. Journal of Pharmacology and Experimental TherapeuticsTher, 2011.  <b>[Epub ahead of print]</b>; PMID[21459966].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21289114">Single-Nucleotide Changes in the HIV Rev-Response Element Mediate Resistance to Compounds That Inhibit Rev Function.</a> Shuck-Lee, D., H. Chang, E.A. Sloan, M.L. Hammarskjold, and D. Rekosh. The Journal of Virology, 2011. 85(8): p. 3940-3949; PMID[21289114].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21345545">Viral Surface Glycoproteins, Gp120 and Gp41, as Potential Drug Targets against HIV-1: Brief Overview One Quarter of a Century Past the Approval of Zidovudine, the First Anti-Retroviral Drug.</a> Teixeira, C., J.R. Gomes, P. Gomes, and F. Maurel. European Journal of Medicinal Chemistry, 2011. 46(4): p. 979-992; PMID[21345545].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21466163">The Role of Conserved Glu Residue on Cyclotide Stability and Activity: A Structural and Functional Study of Kalata B12, a Naturally Occurring Glu to Asp Mutant.</a> Wang, C.K., R.J. Clark, P. Harvey, K.J. Rosengren, M. Cemazar, and D.J. Craik. Biochemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21466163].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21375388">Analyses of Anti-Hiv Type 1 Activity of a Small Ccr5 Peptide Antagonist.</a> Wang, F.Y., S.Y. He, Z.J. Zhang, L.F. He, X.W. Chen, and T. Teng. AIDS Res Hum Retroviruses, 2011.  <b>[Epub ahead of print]</b>; PMID[21375388].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21457080">Novel Hiv-1 Non-Nucleoside Reverse Transcriptase Inhibitors: A Patent Review (2005 - 2010).</a> Zhan, P. and X. Liu. Expert Opinion on Therapeutic Patents, 2011.  <b>[Epub ahead of print]</b>; PMID[21457080].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21468674">Isolation and Characterization of a Kunitz-Type Trypsin Inhibitor with Antiproliferative Activity from Gymnocladus Chinensis (Yunnan Bean) Seeds</a> Zhu, M.J., G.Q. Zhang, H.X. Wang, and T.B. Ng. The Protein Journal, 2011.  <b>[Epub ahead of print]</b>; PMID[21468674].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0401-041410.</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287727400162">A New De Novo Approach for Optimizing Peptides That Inhibit Hiv-1 Entry.</a> Fung, H.K., C.A. Floudas, M.S. Taylor, and R.F. Siliciano.17th European Symposium on Computer Aided Process Engineering, 2007.  ISI[000287727400162].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288370900005">Synthesis and Antiviral Activity of Some (3,4-Diaryl-3h-Thiazol-2-Ylidene)Pyrimidin-2-Yl Amine Derivatives.</a> Turan-Zitouni, G., A. Ozdemir, and Z.A. Kaplancikli. Phosphorus Sulfur and Silicon and the Related Elements, 2011. 186(2): p. 233-239; ISI[000288370900005].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288398900003">Synthesis, Cytostatic and Anti-Viral Activity Evaluation of the Novel Acyclic Nucleoside Analogues Containing a Sterically Constrained (Z)-4-Amino-2-Butenyl Moiety.</a> Wittine, K., K. Benci, S.K. Pavelic, K. Pavelic, S. Bratulic, K. Hock, J. Balzarini, and M. Mintas. Medicinal Chemistry Research, 2011. 20(3): p. 280-286; ISI[000288398900003].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288398900008">Screening and Evaluation of Thiourea Derivatives for Their Hiv Capsid and Human Cyclophilin a Inhibitory Activity.</a> Tan, Z.W., J.B. Li, R.F. Pang, S.S. He, M.Z. He, S.X. Tang, I. Hewlett, and M. Yang. Medicinal Chemistry Research, 2011. 20(3): p. 314-320; ISI[000288398900008].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288457500010">Bioactive Triterpenoids from Kadsura Heteroclita.</a> Xu, L.J., Z.G. Peng, H.S. Chen, J. Wang, and P.G. Xiao. Chemistry &amp; Biodiversity, 2010. 7(9): p. 2289-2295; ISI[000288457500010].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288465200003">Various Biological Activities of Pyridazinone Ring Derivatives.</a> Banerjee, P.S. Asian Journal of Chemistry, 2011. 23(5): p. 1905-1910; ISI[000288465200003].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">32.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288465200086">Isolation and Characterization of Sesquiterpenoids from Daphne Acutiloba Rehd.</a> He, S.Q., Z. Li, Y.W. Ou, L. Wang, G.Y. Yang, and Q.F. Hu. Asian Journal of Chemistry, 2011. 23(5): p. 2225-2226; ISI[000288465200086].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288517400025">Synthesis and Anti-Hiv Activity of Beta-Cyclodextrin-C6-Sulfate/3-Azido-3 &#39;-Deoxythymidine Inclusion Complex.</a> Silion, M., A. Dascalu, B.C. Simionescu, M. Pinteala, and C. Ungurenasu. Journal of Polymer Science Part a-Polymer Chemistry, 2011. 49(7): p. 1730-1733; ISI[000288517400025].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288564600002">Phytochemicals and Biological Activities of Dipsacus Species.</a> Zhao, Y.M. and Y.P. Shi. Chemistry &amp; Biodiversity, 2011. 8(3): p. 414-430; ISI[000288564600002].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288568000004">Strategies Towards Improving the Pharmacokinetic Profile of Epsilon-Substituted Lysinol-Derived Hiv Protease Inhibitors.</a> Rajapakse, H.A., A.M. Walji, K.P. Moore, H. Zhu, A.W. Mitra, A.R. Gregro, E. Tinney, C. Burlein, S. Touch, B.L. Paton, S.S. Carroll, D.J. DiStefano, M.T. Lai, J.A. Grobler, R.I. Sanchez, T.M. Williams, J.P. Vacca, and P.G. Nantermet. ChemMedChem, 2011. 6(2): p. 253-257; ISI[000288568000004].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">36.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288640200010">Blocking Semen-Mediated Enhancement of Hiv Infection by Amyloid-Binding Small Molecules.</a> Kirchhoff, F. and J. Munch. Future Virology, 2011. 6(2): p. 183-186; ISI[000288640200010].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288792400001">Synthesis and Antiviral Activity of N-9- 3-Fluoro-2-(Phosphonomethoxy)Propyl Analogues Derived from N-6-Substituted Adenines and 2,6-Diaminopurines.</a>  Baszczynski, O., P. Jansa, M. Dracinsky, B. Klepetarova, A. Holy, I. Votruba, E. de Clercq, J. Balzarini, and Z. Janeba. Bioorganic &amp; Medicinal Chemistry, 2011. 19(7): p. 2114-2124; ISI[000288792400001].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>

    <p> </p>

    <p class="NoSpacing">38.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288855100018">Transition-Metal Complexes Based on a Sulfonate-Containing N-Donor Ligand and Their Use as Hiv Antiviral Agents.</a> Garcia-Gallego, S., M.J. Serramia, E. Arnaiz, L. Diaz, M.A. Munoz-Fernandez, P. Gomez-Sal, M.F. Ottaviani, R. Gomez, and F.J. de la Mata. European Journal of Inorganic Chemistry, 2011(10): p. 1657-1665; ISI[000288855100018].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0401-041410.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
