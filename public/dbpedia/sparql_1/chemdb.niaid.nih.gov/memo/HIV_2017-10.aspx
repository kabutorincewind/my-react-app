

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CH6n+hMhRhpY1WjFIf2kolWAMk635qiAJIFYNx2wEnNtvALW+EQ3d9OYbLWTNbQeFG7IDTRY3yyYwhmddG7nJ7/bANNpimujqgZNI/9AKmhG7c7q1yLR2SOaKH8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E5819E2A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: October, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29039736">A Novel Central Nervous System-penetrating Protease Inhibitor Overcomes Human Immunodeficiency Virus 1 Resistance with Unprecedented aM to pM Potency.</a> Aoki, M., H. Hayashi, K.V. Rao, D. Das, N. Higashi-Kuwata, H. Bulut, H. Aoki-Ogata, Y. Takamatsu, R.S. Yedidi, D.A. Davis, S.I. Hattori, N. Nishida, K. Hasegawa, N. Takamune, P.R. Nyalapatla, H.L. Osswald, H. Jono, H. Saito, R. Yarchoan, S. Misumi, A.K. Ghosh, and H. Mitsuya. eLife, 2017. 6: e28020. PMID[29039736]. PMCID[PMC5644950].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28802233">1H-Tetrazol-5-amine and 1,3-Thiazolidin-4-one Derivatives Containing 3-(Trifluoromethyl)phenyl Scaffold: Synthesis, Cytotoxic and anti-HIV Studies.</a>Bielenica, A., D. Szulczyk, W. Olejarz, S. Madeddu, G. Giliberti, I.B. Materek, A.E. Koziol, and M. Struga. Biomedecine &amp; Pharmacotherapie, 2017. 94: p. 804-812. PMID[28802233].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28797705">Effects of HIV-1 gp41-derived Virucidal Peptides on Virus-like Lipid Membranes.</a> Carravilla, P., A. Cruz, I. Martin-Ugarte, I.R. Oar-Arteta, J. Torralba, B. Apellaniz, J. Perez-Gil, J. Requejo-Isidro, N. Huarte, and J.L. Nieva. Biophysical Journal, 2017. 113(6): p. 1301-1310. PMID[28797705]. PMCID[PMC5607204].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28864040">Sundarbanxylogranins A-E, Five New Limonoids from the Sundarban Mangrove, Xylocarpus granatum<i>.</i></a> Dai, Y.G., J. Wu, K.P. Padmakumar, and L. Shen. Fitoterapia, 2017. 122: p. 85-89. PMID[28864040].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28951095">1-Hydroxypyrido[2,3-d]pyrimidin-2(1H)-ones as Novel Selective HIV Integrase Inhibitors Obtained via Privileged Substructure-based Compound Libraries.</a> 95. Gao, P., L. Zhang, L. Sun, T. Huang, J. Tan, J. Zhang, Z. Zhou, T. Zhao, L. Menendez-Arias, C. Pannecouque, E. Clercq, P. Zhan, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2017. 25(20): p. 5779-5789. PMID[28951095].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28434781">Design of Novel HIV-1 Protease Inhibitors Incorporating Isophthalamide-derived P2-P3 Ligands: Synthesis, Biological Evaluation and X-ray Structural Studies of Inhibitor-HIV-1 Protease Complex.</a> Ghosh, A.K., M. Brindisi, P.R. Nyalapatla, J. Takayama, J.R. Ella-Menye, S. Yashchuk, J. Agniswamy, Y.F. Wang, M. Aoki, M. Amano, I.T. Weber, and H. Mitsuya. Bioorganic &amp; Medicinal Chemistry, 2017. 25(19): p. 5114-5127. PMID[28434781]. PMCID[PMC5617771].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28768869">Protective Efficacy of Broadly Neutralizing Antibodies with Incomplete Neutralization Activity against Simian-Human Immunodeficiency Virus in Rhesus Monkeys.</a> Julg, B., D. Sok, S.D. Schmidt, P. Abbink, R.M. Newman, T. Broge, C. Linde, J. Nkolola, K. Le, D. Su, J. Torabi, M. Pack, A. Pegu, T.M. Allen, J.R. Mascola, D.R. Burton, and D.H. Barouch. Journal of Virology, 2017. 91(20): e01187-01117. PMID[28768869]. PMCID[PMC5625479].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29027985">Triterpenoids from Ocimum labiatum Activates Latent HIV-1 Expression in Vitro: Potential for Use in Adjuvant Therapy.</a>Kapewangolo, P., J.J. Omolo, P. Fonteh, M. Kandawa-Schulz, and D. Meyer. Molecules, 2017. 22(10): E1703. PMID[29027985].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29045830">In Vivo Suppression of HIV Rebound by Didehydro-cortistatin a, a &quot;Block-and-lock&quot; Strategy for HIV-1 Treatment.</a> Kessing, C.F., C.C. Nixon, C. Li, P. Tsai, H. Takata, G. Mousseau, P.T. Ho, J.B. Honeycutt, M. Fallahi, L. Trautmann, J.V. Garcia, and S.T. Valente. Cell Reports, 2017. 21(3): p. 600-611. PMID[29045830].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28864671">Expression of an RNA Glycosidase Inhibits HIV-1 Transactivation of Transcription.</a> Kutky, M. and K.A. Hudak. The Biochemical Journal, 2017. 474(20): p. 3471-3483. PMID[28864671].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28878089">HLA-B*14:02-restricted Env-specific CD8+ T-Cell Activity Has Highly Potent Antiviral Efficacy Associated with Immune Control of HIV Infection.</a> Leitman, E.M., C.B. Willberg, M.H. Tsai, H. Chen, S. Buus, F. Chen, L. Riddell, D. Haas, J. Fellay, J.J. Goedert, A. Piechocka-Trocha, B.D. Walker, J. Martin, S. Deeks, S.M. Wolinsky, J. Martinson, M. Martin, Y. Qi, A. Saez-Cirion, O.O. Yang, P.C. Matthews, M. Carrington, and P.J.R. Goulder. Journal of Virology, 2017. 91(22): e00544-00517. PMID[28878089]. PMCID[PMC5660483].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000412066700079">Inhibition of HIV-1 Infection in Humanized Mice and Metabolic Stability of Protein Phosphatase-1-targeting Small Molecule 1E7-03.</a> Lin, X.H., N. Kumari, C. DeMarino, Y.S. Kont, T. Ammosova, A. Kulkarni, M. Jerebtsova, G. Vazquez-Meves, A. Ivanov, D. Kovalskyy, A. Uren, F. Kashanchi, and S. Nekhai. Oncotarget, 2017. 8(44): p. 76749-76769. ISI[000412066700079].
    <br />
    <b>[WOS]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28978468">Host microRNAs-221 and -222 Inhibit HIV-1 Entry in Macrophages by Targeting the CD4 Viral Receptor.</a> Lodge, R., J.A. Ferreira Barbosa, F. Lombard-Vadnais, J.C. Gilmore, A. Deshiere, A. Gosselin, T.R. Wiche Salinas, M.G. Bego, C. Power, J.P. Routy, P. Ancuta, M.J. Tremblay, and E.A. Cohen. Cell Reports, 2017. 21(1): p. 141-153. PMID[28978468].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000412416600037">Synthesis, Crystal Structure, anti-HIV, and Antiproliferative Activity of New Pyrazolylthiazole Derivatives.</a> Madni, M., S. Hameed, M.N. Ahmed, M.N. Tahir, N.A. Al-Masoudi, and C. Pannecouque. Medicinal Chemistry Research, 2017. 26(10): p. 2653-2665. ISI[000412416600037].
    <br />
    <b>[WOS]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28930676">Localized Phosphorylation of a Kinesin-1 Adaptor by a Capsid-associated Kinase Regulates HIV-1 Motility and Uncoating.</a> Malikov, V. and M.H. Naghavi. Cell Reports, 2017. 20(12): p. 2792-2799. PMID[28930676].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28627965">Role of Angiopoietin-2, Endoglin, and Placental Growth Factor in HIV-associated Preeclampsia.</a> Mbhele, N., J. Moodley, and T. Naicker. Hypertension in Pregnancy, 2017. 36(3): p. 240-246. PMID[28627965].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28934476">Crystal Structure of an Engineered, HIV-specific Recombinase for Removal of Integrated Proviral DNA.</a> Meinke, G., J. Karpinski, F. Buchholz, and A. Bohm. Nucleic Acids Research, 2017. 45(16): p. 9726-9740. PMID[28934476].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28928403">Dimerization Regulates Both Deaminase-dependent and Deaminase-independent HIV-1 Restriction by APOBEC3G.</a> Morse, M., R. Huo, Y.Q. Feng, I. Rouzina, L. Chelico, and M.C. Williams. Nature Communications, 2017. 8: p. 597. PMID[28928403]. PMCID[PMC5605669].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000411789300155">The Potential of Moringa oleifera Lam. To Manage HIV-1 Infections and Its Positive Pharmaco-synergy with Antiretroviral Therapies.</a> Ndhlala, A.R., K. Cele, R. Mulaudzi, P.C.P. du, S. Venter, P.W. Mashela, and H.A. Abdelgadir. Planta Medica, 2016. 82. ISI[000411789300155].
    <br />
    <b>[WOS]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28925702">Environmentally Friendly Procedure Based on Supercritical Fluid Chromatography and Tandem Mass Spectrometry Molecular Networking for the Discovery of Potent Antiviral Compounds from Euphorbia semiperfoliata.</a> Nothias, L.F., S. Boutet-Mercey, X. Cachet, E. De La Torre, L. Laboureur, J.F. Gallard, P. Retailleau, A. Brunelle, P.C. Dorrestein, J. Costa, L.M. Bedoya, F. Roussi, P. Leyssen, J. Alcami, J. Paolini, M. Litaudon, and D. Touboul. Journal of Natural Products, 2017. 80(10): p. 2620-2629. PMID[28925702].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29025433">The Role of the Glycosyl Moiety of Myricetin Derivatives in anti-HIV-1 Activity in Vitro.</a> Ortega, J.T., A.I. Suarez, M.L. Serrano, J. Baptista, F.H. Pujol, and H.R. Rangel. AIDS Research and Therapy, 2017. 14: p. 57. PMID[29025433]. PMCID[PMC5639754].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000411789300112">Antiviral Screening of Microbial Natural Products.</a> Overacker, R.D. and S. Loesgen. Planta Medica, 2016. 82. ISI[000411789300112].
    <br />
    <b>[WOS]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">23<a href="http://www.ncbi.nlm.nih.gov/pubmed/29068373">. A Search for Dual Action HIV-1 Reverse Transcriptase, Bacterial RNA Polymerase Inhibitors.</a> Paneth, A., T. Fraczek, A. Grzegorczyk, D. Janowska, A. Malm, and P. Paneth. Molecules, 2017. 22(11): E1808. PMID[29068373].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29016131">Metabolites from the Plant Endophytic Fungus Aspergillus sp. CPCC 400735 and their anti-HIV Activities.</a> Pang, X., J.Y. Zhao, X.M. Fang, T. Zhang, D.W. Zhang, H.Y. Liu, J. Su, S. Cen, and L.Y. Yu. Journal of Natural Products, 2017. 80(10): p. 2595-2601. PMID[29016131].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28770939">Chemical Optimization of Macrocyclic HIV-1 Inactivators for Improving Potency and Increasing the Structural Diversity at the Triazole Ring.</a> Rashad, A.A., K. Acharya, A. Haftl, R. Aneja, A. Dick, A.P. Holmes, and I. Chaiken. Organic &amp; Biomolecular Chemistry, 2017. 15(37): p. 7770-7782. PMID[28770939]. PMCID[PMC5614861].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28747499">Effects of Inner Nuclear Membrane Proteins SUN1/UNC-84A and SUN2/UNC-84B on the Early Steps of HIV-1 Infection.</a> Schaller, T., L. Bulli, D. Pollpeter, G. Betancor, J. Kutzner, L. Apolonia, N. Herold, R. Burk, and M.H. Malim. Journal of Virology, 2017. 91(19): e00463-00417. PMID[28747499]. PMCID[PMC5599759].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28938888">Unique Binding Modes for the Broad Neutralizing Activity of Single-chain Variable Fragments (SCFV) Targeting CD4-induced Epitopes.</a> Tanaka, K., T. Kuwata, M. Alam, G. Kaplan, S. Takahama, K.P.R. Valdez, A. Roitburd-Berman, J.M. Gershoni, and S. Matsushita. Retrovirology, 2017. 14: p. 44. PMID[28938888]. PMCID[PMC5610415].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28797967">Multipurpose Tenofovir disoproxil fumarate Electrospun Fibers for the Prevention of HIV-1 and HSV-2 Infections in Vitro.</a> Tyo, K.M., H.R. Vuong, D.A. Malik, L.B. Sims, H. Alatassi, J. Duan, W.H. Watson, and J.M. Steinbach-Rankins. International Journal of Pharmaceutics, 2017. 531(1): p. 118-133. PMID[28797967].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000411789300606">Anti-HIV Natural Products (28): Preparation of Conjugate for 3-O-Acyl betulin Derivative and AZT as anti-HIV Agents.</a> Wada, S., N. Tanaka, C.H. Chen, S.L. Morris-Natschke, K.H. Lee, and Y. Kashiwada. Planta Medica, 2016. 82. ISI[000411789300606].
    <br />
    <b>[WOS]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">30. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28917838">Human SMOOTHENED Inhibits Human Immunodeficiency Virus Type 1 Infection.</a> Yoshida, T., A. Hamano, A. Ueda, H. Takeuchi, and S. Yamaoka. Biochemical and Biophysical Research Communications, 2017. 493(1): p. 132-138. PMID[28917838].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29077062">Krishnolides A-D: New 2-Ketokhayanolides from the Krishna Mangrove, Xylocarpus moluccensis.</a> Zhang, Q., T. Satyanandamurty, L. Shen, and J. Wu. Marine Drugs, 2017. 15(11): E333. PMID[29077062].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28637181">Crosstalk between Histone Modifications Indicates that Inhibition of Arginine methyltransferase CARM1 Activity Reverses HIV Latency.</a> Zhang, Z., B.C. Nikolai, L.A. Gates, S.Y. Jung, E.B. Siwak, B. He, A.P. Rice, B.W. O&#39;Malley, and Q. Feng. Nucleic Acids Research, 2017. 45(16): p. 9348-9360. PMID[28637181].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28145126">Bioactive Steroids and Sorbicillinoids Isolated from the Endophytic Fungus Trichoderma sp. Xy24.</a> Zhao, J.L., M. Zhang, J.M. Liu, Z. Tan, R.D. Chen, K.B. Xie, and J.G. Dai. Journal of Asian Natural Products Research, 2017. 19(10): p. 1028-1035. PMID[28145126].
    <br />
    <b>[PubMed]</b>. HIV_10_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">This month, no relevant HIV patents were identified.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
