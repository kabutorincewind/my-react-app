

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-06-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MB8uUaSnqoQzCZ52+kITUwSeA7/lfxjH0LcwlCUiajMAupQkM6gNTL5t9fB38E1tHY1/tTKg25iHnZfpG67BXSQF3HY69G1h6WcocvcmYQE8fZajEei2g8gL5NI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="432ADA35" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">June 11 - June 24, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20562278">Production of a New Thiopeptide Antibiotic, TP-1161, by a Marine-derived Nocardiopsis species.</a> Engelhardt, K., K.F. Degnes, M. Kemmler, H. Bredholt, E. Fjaervik, G. Klinkenberg, H. Sletta, T.E. Ellingsen, and S.B. Zotchev. Appl Environ Microbiol. [Epub ahead of print]; PMID[20562278].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20557461">In vitro susceptibility of Aspergillus spp. to dithiocarbamate organoruthenium compounds.</a> Nogueira, L.J., M.A. de Resende, S.R. Oliveira, M.H. de Araujo, T.F. Magalhaes, M.B. de Oliveira, C.V. Martins, M.T. Lopes, E.S.A.C. Araujo, and C.L. Donnici. Mycoses. [Epub ahead of print]; PMID[20557461].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20562904">A novel polyamide SL-A92 as a potential fungal resistance blocker: synthesis and bioactivities in Candida albicans.</a> Zhu, S.L., Z.H. Jiang, P.H. Gao, Y. Qiu, L. Wang, Y.Y. Jiang, and D.Z. Zhang. Acta Pharmacol Sin. [Epub ahead of print]; PMID[20562904].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20559419">The thioamides methimazole and thiourea inhibit growth of M. avium Subspecies paratuberculosis in culture.</a> Greenstein, R.J., L. Su, and S.T. Brown. PLoS One. 5(6): p. e11099; PMID[20559419].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20537903">A new class of potential anti-tuberculosis agents: Synthesis and preliminary evaluation of novel acrylic acid ethyl ester derivatives.</a> Kabir, M.S., O.A. Namjoshi, R. Verma, R. Polanowski, S.M. Krueger, D. Sherman, M.A. Rott, W.R. Schwan, A. Monte, and J.M. Cook. Bioorg Med Chem. 18(12): p. 4178-4186; PMID[20537903].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20558127">Fellutamide B is a potent inhibitor of the Mycobacterium tuberculosis proteasome.</a> Lin, G., D. Li, T. Chidawanyika, C. Nathan, and H. Li. Arch Biochem Biophys. <b>[Epub ahead of print]</b>; PMID[20558127].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20556653">Antimycobacterial activity in vitro of pigments isolated from Antarctic bacteria.</a> Mojib, N., R. Philpott, J.P. Huang, M. Niederweis, and A.K. Bej. Antonie Van Leeuwenhoek. <b>[Epub ahead of print]</b>; PMID[20556653].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20483615">Trichoderins, novel aminolipopeptides from a marine sponge-derived Trichoderma sp., are active against dormant mycobacteria.</a> Pruksakorn, P., M. Arai, N. Kotoku, C. Vilcheze, A.D. Baughn, P. Moodley, W.R. Jacobs, Jr., and M. Kobayashi. Bioorg Med Chem Lett. 20(12): p. 3658-63; PMID[20483615].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20540651">Mannosylated gelatin nanoparticles bearing isoniazid for effective management of tuberculosis.</a> Saraogi, G.K., B. Sharma, B. Joshi, P. Gupta, U.D. Gupta, N.K. Jain, and G.P. Agrawal. J Drug Target. <b>[Epub ahead of print]</b>; PMID[20540651].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20569083">5-Nitro-2,6-dioxohexahydro-4-pyrimidinecarboxamides: synthesis, in vitro antimycobacterial activity, cytotoxicity, and isocitrate lyase inhibition studies.</a> Sriram, D., P. Yogeeswari, P. Senthilkumar, G. Naidu, and P. Bhat. J Enzyme Inhib Med Chem. <b>[Epub ahead of print]</b>; PMID[20569083].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0611-062410.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278131700005">Synthesis, nematicidal and antimicrobial activity of [(3-methyl-7-5-[2-(aryl)-4-oxo-1,3-thiazolan-3-yl]-1,3,4-thiadiazol-2-yl benzo[b]furan-5-yl)methyl]benzo[b]furan-7-yl-1,3,4-thiadiazol-2-yl)-2-(aryl)-1,3-thiazolan-4-one.</a> Reddy, C.S., D.C. Rao, V. Yakub, and A. Nagaraj. Chemical &amp; Pharmaceutical Bulletin. 58(6): p. 805-810; ISI[000278131700005].</p>

    <p><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278112400006">Biological Activity of Chemical Constituents from Clausena harmandiana.</a> Thongthoom, T., U. Songsiang, C. Phaosiri, and C. Yenjai. Archives of Pharmacal Research. 33(5): p. 675-680; ISI[000278112400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278078300007">Exploring 3-arylquinoxaline-2-carbonitrile 1,4-di-N-oxides Activities Against Neglected Diseases with QSAR.</a> Vicente, E., P.R. Duchowicz, E.D. Ortiz, A. Monge, and E.A. Castro. Chemical Biology &amp; Drug Design. 76(1): p. 59-69; ISI[000278078300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278131700009">6 &#39;-Hydroxy-3 &#39;-methoxy-mitorubrin, a New Potentiator of Antifungal Miconazole Activity, Produced by Penicillium radicum FKI-3765-2.</a> Yamazaki, H., S. Omura, and H. Tomoda. Chemical &amp; Pharmaceutical Bulletin. 58(6): p. 829-832; ISI[000278131700009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277981100010">Farnesol misplaces tip-localized Rho proteins and inhibits cell wall integrity signalling in Aspergillus fumigatus.</a>Dichtl, K., F. Ebel, F. Dirr, F.H. Routier, J. Heesemann, and J. Wagener. Molecular Microbiology. 76(5): p. 1191-1204; ISI[000277981100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278067200002">Nystatin-Intralipid Preparation: Characterization and In Vitro Activity Against Yeasts and Molds.</a>Semis, R., I. Polacheck, and E. Segal. Mycopathologia. 169(5): p. 333-341; ISI[000278067200002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278353600007">Rotenone Enhances the Antifungal Properties of Staurosporine.</a> Castro, A., C. Lemos, A. Falcao, A.S. Fernandes, N.L. Glass, and A. Videira. Eukaryotic Cell. 9(6): p. 906-914; PMID[ISI:000278353600007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278322500020">Antimycobacterial and Cytotoxicity Activities of Free and Liposome-Encapsulated 3-(4&#39;-Bromo[1,1&#39;-biphenyl-4-yl)-3-(4-bromo-phenyl)-n,n-dimethyl-2-propen-1-amine.</a> de Souza, A.O., C.L. Silva, N. Duran, and M.H. Andrade-Santana. Quimica Nova. 33(4): p. 871-874; PMID[ISI:000278322500020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271775107171">MEDI 224-Synthesis of pyrimido[4,5-b]indoles as selective inhibitors of Toxoplasma gondii dihydrofolate reductase.</a> Gangjee, A., L. Wang, and R.L. Kisliuk. Abstracts of Papers of the American Chemical Society, 2008. 235; PMID[ISI:000271775107171].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0611-062410.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
