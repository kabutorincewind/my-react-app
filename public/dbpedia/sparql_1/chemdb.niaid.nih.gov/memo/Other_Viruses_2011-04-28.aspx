

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-04-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8GNvont8fLeAQHZLx5UCrO4tcmD2rtp1eMEZojHaluHTfUGCe4BjF0pxk4yoCT504K2ja3kEDhnrfcrh484jsk9deq31IKxXRaM9NkDWhpj8iR37O+71tiE2+0g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="50C5D673" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">April 15 - April 28, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Human cytomegalovirus</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21493074">Synthesis, Metabolic Stability and Antiviral Evaluation of Various Alkoxyalkyl Esters of Cidofovir and 9-(S)-[3-Hydroxy-2-(Phosphonomethoxy)Propyl]Adenine.</a> Ruiz, J., J.R. Beadle, R. Mark Buller, J. Schreiwer, M.N. Prichard, K.A. Keith, K.C. Lewis, and K.Y. Hostetler. Bioorganic &amp; Medicinal Chemistry, 2011. 19(9): p. 2950-2958; PMID[21493074].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0415-042811.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Epstein-Barr virus</p> 

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21490247">Inhibition of the Epstein-Barr Virus Lytic Cycle by Protoapigenone</a>. Tung, C.P., F.R. Chang, Y.C. Wu, D.W. Chuang, A. Hunyadi, and S.T. Liu. The Journal of General Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21490247].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0415-042811.</p>  

    <p class="memofmt2-2">Hepatitis A virus</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21498749">Grape Seed Extract for the Control of Human Enteric Viruses</a>. Su, X. and D.H. D&#39;Souza. Applied and Environmental Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21498749].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0415-042811.</p>  

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21520195">Resistance Mutations Define Specific Antiviral Effects for Inhibitors of the Hepatitis C Virus (Hcv) P7 Ion Channel</a>. Foster, T.L., M. Verow, A.L. Wozniak, M.J. Bentham, J. Thompson, E. Atkins, S.A. Weinman, C. Fishwick, R. Foster, M. Harris, and S. Griffin. Hepatology (Baltimore, Md.), 2011. <b>[Epub ahead of print]</b>; PMID[21520195].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21507645">The Synthesis and Evaluation of a Novel Class of (E)-3-(1-Cyclohexyl-1h-Pyrazol-3-Yl)-2-Methylacrylic Acid Hepatitis C Virus Polymerase Ns5b Inhibitors</a>. Martin, S.W., P. Glunz, B.R. Beno, C. Bergstrom, J.L. Romine, E. Scott Priestley, M. Newman, M. Gao, S. Roberts, K. Rigat, R. Fridell, D. Qiu, G. Knobloh, and Y.K. Wang. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21507645].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21506603">Hcv-Ns3/4a Protease Inhibitory Iridoid Glucosides and Dimeric Foliamenthoic Acid Derivatives from Anarrhinum Orientale</a>. Salah El Dine, R., A.R. Abdel Monem, A.M. El-Halawany, M. Hattori, and E. Abdel-Sattar. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21506603].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0415-042811.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21502629">Selection, Optimisation and Pharmacokinetic Properties of a Novel, Potent Antiviral Locked Nucleic Acid (Lna-) Based Antisense Oligomer (Aso) Targeting Hepatitis C Virus (Hcv) Internal Ribosome Entry Site (Ires)</a>. Laxton, C., K. Brady, S. Moschos, P. Turnpenny, J. Rawal, D.C. Pryde, B. Sidders, R. Corbau, C. Pickford, and E.J. Murray. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21502629].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0415-042811.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775602482">Rational Design of a Novel Family of Non-Peptidic Cyclophylin Inhibitors That Potently Inhibit Hcv Replication by Fragment-Based Drug Design</a>. Ahmed-Belkacem, A., L. Colliandre, Y. Bessin, P. Barthe, W. Bourguet, D. Douguet, N. Ahnou, J.M. Pawlotsky, and J.F. Guichou. Hepatology, 2010. 52(4): p. 1206A-1207A; ISI[000288775602482].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775601372">Novel Small-Molecule Hcv-Entry Inhibitors Block Virus Spread and Promote Virus Clearance in Vitro</a>. de Muys, J.M., G. Coburn, A.O. Han, J.D. Murga, D. Fisch, D. Paul, S. Moorji, K. Provoncha, D. Qian, P.J. Maddon, and W.C. Olson. Hepatology, 2010. 52(4): p. 828A-829A; ISI[000288775601372].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775601122">Vitamin D Metabolites Inhibit Replication of the Hepatitis C Virus</a>. Gutierrez, J.A., K.A. Jones, R.L. Fitzgerald, J. Allina, A.D. Branch, D.L. Trump, R.T. Schooley, and D.L. Wyles. Hepatology, 2010. 52(4): p. 704A-704A; ISI[000288775601122].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775601324">Chloroquine and Related Compounds Are Inhibitors of Hepatitis C Virus Rna-by Inhibiting Autophagy Proteolysis</a>. Halfon, P., J.P. Nallet, S.J. Petit, M. Bouzid, F. Joly, C.J. Camus, P. Benech, J. Dubuisson, J. Courcambeck, and C. Wychowski. Hepatology, 2010. 52(4): p. 807A-807A; ISI[000288775601324]. <b>[WOS]</b>. <b>OV_0415-042811</b>.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775602492">Metabolite Characterization of Inx-189, a Potent Hcv Inhibitor, in Fresh Human Primary Hepatocytes and Human Liver and Kidney Cell Lines</a>. Hall, A., N. Raja, S.D. Chamberlain, E. Gorovits, G.W. Henson, J.T. Hutchins, J. Muhammad, J. Patti, J. Vernachio, J. Wang, K. Madela, M. Aljarah, S.L. Jones, and C. McGuigan. Hepatology, 2010. 52(4): p. 1211A-1211A; ISI[000288775602492].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775602477">Ach-2684: Hcv Ns3 Protease Inhibitor with Potent Activity against Multiple Genotypes and Known Resistant Variants</a>. Huang, M.J., S. Podos, D. Patel, G.W. Yang, J.L. Fabrycki, Y.S. Zhao, C. Marlor, P. Kapoor, X.Z. Wang, A. Hashimoto, V. Gadhachanda, G. Pais, D.W. Chen, A. Agarwal, M. Deshpande, K.L. Stauber, and A. Phadke. Hepatology, 2010. 52(4): p. 1204A-1204A; ISI[000288775602477].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775602501">Nonclinical Profile and Phase I Results in Healthy Volunteers for the Novel and Potent Hcv Ns5a Inhibitor Gs-5885</a>. Link, J.O., R. Bannister, L.D. Beilke, G.F. Cheng, M. Cornpropst, A. Corsa, E. Dowdy, H.Y. Guo, D. Kato, T. Kirschberg, H.T. Liu, M. Mitchell, M. Matles, E. Mogalian, E. Mondou, C. Ohmstede, B. Peng, R.W. Scott, J.W. Findlay, G.E. Chittick, F. Wang, J. Alianti, J.Y. Sun, J. Taylor, Y. Tian, L.H. Xu, C. Yang, G.J. Yuen, K. Wang, and E.J. Eisenberg. Hepatology, 2010. 52(4): p. 1215A-1216A; ISI[000288775602501].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289048400002">Synthesis and Biological Evaluation of Unsaturated Keto and Exomethylene D-Arabinopyranonucleoside Analogs: Novel 5-Fluorouracil Analogs That Target Thymidylate Synthase</a>. Tzioumaki, N., S. Manta, E. Tsoukala, J.V. Voorde, S. Liekens, D. Komiotis, and J. Balzarini. European Journal of Medicinal Chemistry, 2011. 46(4): p. 993-1005; ISI[000289048400002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288775602498">In Vitro Combination Studies of Ach-1625 (Hcv Ns3 Protease Inhibitor) and Ach-2928 (Hcv Ns5a Inhibitor) in Presence and Absence of Ribavirin</a>. Zhao, Y.S., G.W. Yang, J.L. Fabrycki, D. Patel, J. Wiles, X.Z. Wang, A. Phadke, M. Deshpande, and M.J. Huang. Hepatology. 52: p. 1214A-1214A; ISI[000288775602498].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0415-042811.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
