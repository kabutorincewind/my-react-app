

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-02-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="FbGTHA2U+CzkAjExyseJgGpa74jiiSzPTTe75SDgoGJfaK0JrjbEXKj0fLVJUgpzf7/O91fhSGRj/ynkAbbA8Ylq04j+TG40A2Kph0uqb3h8zk7kSXSrsVUFG64=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2F718CBC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 20 - February 2, 2012</h1>

    <p class="memofmt2-2">Pubmed citations
    <br />
    <br /></p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22118474">Mechanism of HIV Antiretroviral Drugs Progress Toward Drug Resistance.</a> Ammaranond, P. and S. Sanguansittianan, Fundamental &amp; Clinical Pharmacology, 2012. 26(1): p. 146-161; PMID[22118474].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22083480">Characterization of the Inhibition Mechanism of HIV-1 Nucleocapsid Protein Chaperone Activities by Methylated Oligoribonucleotides</a>. Avilov, S.V., C. Boudier, M. Gottikh, J.L. Darlix, and Y. Mely, Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 1010-1018; PMID[22083480].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22104209">ADAR1 is a Novel Multi Targeted anti-HIV-1 Cellular Protein.</a> Biswas, N., T. Wang, M. Ding, A. Tumne, Y. Chen, Q. Wang, and P. Gupta, Virology, 2012. 422(2): p. 265-277; PMID[22104209].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22271897">The HIV-1 Protease Inhibitor Nelfinavir Impairs Proteasome Activity and Inhibits the Multiple Myeloma Cells Proliferation in Vitro and in Vivo.</a> Bono, C., L. Karlin, S. Harel, E. Mouly, S. Labaume, L. Galicier, S. Apcher, H. Sauvageon, J.P. Fermand, J.C. Bories, and B. Arnulf, Haematologica, 2012. <b>[Epub ahead of print]</b>; PMID[22271897].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22293827">Baseline Susceptibility of Primary HIV-2 to Entry Inhibitors.</a>. Borrego, P., R. Calado, J.M. Marcelino, I. Bartolo, C. Rocha, P. Cavaco-Silva, M. Doroana, F. Antunes, F. Maltez, U. Caixas, H. Barroso, and N. Taveira, Antiviral Therapy, 2012. <b>[Epub ahead of print]</b>; PMID[22293827].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22284360">A Synthetic Heparan Sulfate-mimetic Peptide Conjugated to a Mini CD4 Displays Very High anti-HIV-1 Activity Independently of Coreceptor Usage.</a> Connell, B.J., F. Baleux, Y.M. Coic, P. Clayette, D. Bonnaffe, and H. Lortat-Jacob, Chemistry &amp; Biology, 2012. 19(1): p. 131-139; PMID[22284360].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22276994">Cyclic Peptide Inhibitors of HIV-1 Capsid-human Lysyl-tRNA Synthetase Interaction.</a> Dewan, V., T. Liu, K.M. Chen, Z. Qian, M. Niu, L. Kleiman, K. Mahasenan, C. Li, H. Matsuo, D. Pei, and K. Musier-Forsyth, ACS Chemical Biology, 2012. <b>[Epub ahead of print]</b>; PMID[22276994].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22292009">The GB Virus C (GBV-C) NS3 Serine Protease Inhibits HIV-1 Replication in a CD4+ T Lymphocyte Cell Line without Decreasing HIV Receptor Expression.</a> George, S.L., D. Varmaz, J.E. Tavis, and A. Chowdhury, Plos One, 2012. 7(1): p. e30653; PMID[22292009].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22291339">Inhibition of HIV-1 Protease: The Rigidity Perspective.</a> Heal, J.W., J.E. Jimenez-Roldan, S.A. Wells, R.B. Freedman, and R.A. Romer, Bioinformatics, 2012. 28(3): p. 350-357; PMID[22291339].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22244940">Synthesis and Biological Evaluation of Deoxy-hematoxylin Derivatives as a Novel Class of anti-HIV-1 Agents.</a> Ishii, H., H. Koyama, K. Hagiwara, T. Miura, G. Xue, Y. Hashimoto, G. Kitahara, Y. Aida, and M. Suzuki, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(3): p. 1469-1474; PMID[22244940].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22280246">Origin of Decrease in Potency of Darunavir and Two Related Antiviral Inhibitors against HIV2 Compared to HIV1 Protease.</a> Kar, P. and V. Knecht, The Journal of Physical Chemistry B, 2012. <b>[Epub ahead of print]</b>; PMID[22280246].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21982718">Pentacycloundecane Derived Hydroxy acid Peptides: A New Class of Irreversible Non-scissile Ether Bridged Type Isoster as Potential HIV-1 Wild Type C-SA Protease Inhibitors.</a> Karpoormath, R., Y. Sayed, P. Govender, T. Govender, H.G. Kruger, M.E. Soliman, and G.E. Maguire, Bioorganic Chemistry, 2012. 40(1): p. 19-29; PMID[21982718].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22268494">Strategies for The Design of HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors: Lessons from The Development of Seven Representative Paradigms</a>. Li, D., P. Zhan, E. De Clercq, and X. Liu, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22268494].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22276657">Effect of the HIV Nucleoside Reverse Transcriptase Inhibitor Zidovudine on the Growth and Differentiation of Primary Gingival Epithelium.</a> Mitchell, D., M. Israr, S. Alam, J. Kishel, D. Dinello, and C. Meyers, HIV Medicine, 2012. <b>[Epub ahead of print]</b>; PMID[22276657].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22226641">Testing of Viscous anti-HIV Microbicides Using Lactobacillus.</a> Moncla, B.J., K. Pryke, L.C. Rohan, and H. Yang, Journal of Microbiological Methods, 2012. 88(2): p. 292-296; PMID[22226641].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22290950">Anti-Human Immunodeficiency Virus Type 1 Activity of Novel 6-Substituted 1-benzyl-3-(3,5-dimethylbenzyl)uracil derivatives.</a> Ordonez, P., T. Hamasaki, Y. Isono, N. Sakakibara, M. Ikejiri, T. Maruyama, and M. Baba, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22290950].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22286884">Development and Performance of Conventional HIV-1 Phenotyping (Antivirogram(R)) and Genotype-based Calculated Phenotyping Assay (virco(R)TYPE HIV-1) on Protease and Reverse Transcriptase Genes to Evaluate Drug Resistance.</a> Pattery, T., Y. Verlinden, H. De Wolf, D. Nauwelaers, K. Van Baelen, M. Van Houtte, P. Mc Kenna, and J. Villacian, Intervirology, 2012. 55(2): p. 138-146; PMID[22286884].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22283809">Development of Novel CXCR4-based Therapeutics.</a> Peled, A., O. Wald, and J. Burger, Expert Opinion on Investigational Drugs, 2012. <b>[Epub ahead of print]</b>; PMID[22283809].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22288567">Drug Interaction Profile for GSK2248761, a Next-generation Non-nucleoside Reverse Transcriptase Inhibitor.</a> Piscitelli, S., J. Kim, E. Gould, Y. Lou, S. White, M. de Serres, M. Johnson, X.J. Zhou, K. Pietropaolo, and D. Mayers, British Journal of Clinical Pharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22288567].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22269860">Cytosine Deoxyribonucleoside anti-HIV Analogues: A Small Chemical Substitution Allows Relevant Activities.</a> Scaglione, F. and L. Berrino, International Journal of Antimicrobial Agents, 2012. <b>[Epub ahead of print]</b>; PMID[22269860].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22123692">In Vitro Resistance Profile of the Candidate HIV-1 Microbicide Drug Dapivirine.</a> Schader, S.M., M. Oliveira, R.I. Ibanescu, D. Moisi, S.P. Colby-Germinario, and M.A. Wainberg, Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 751-756; PMID[22123692].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22122263">Characterization of an HIV-Targeted Transcriptional Gene-silencing RNA in Primary Cells.</a> Turner, A.M., A.M. Ackley, M.A. Matrone, and K.V. Morris, Human Gene Therapy, 2012. <b>[Epub ahead of print]</b>; PMID[22122263].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22172974">A Novel High-throughput Cellular Screening Assay for the Discovery of HIV-1 Integrase Inhibitors.</a> Van Loock, M., G. Meersseman, K. Van Acker, C. Van Den Eynde, D. Jochmans, B. Van Schoubroeck, G. Dams, L. Heyndrickx, and R.F. Clayton, Journal of Virological Methods, 2012. 179(2): p. 396-401; PMID[22172974].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22283377">Design, Synthesis and Biological Evaluation of 1-[(2-Benzyloxyl/alkoxyl) methyl]-5-halo-6-aryluracils as Potent HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors with Improved Drug Resistance Profile.</a> Wang, X., J. Zhang, Y. Huang, R. Wang, L. Zhang, K. Qiao, L. Li, C. Liu, Y. Ouyang, W. Xu, Z. Zhang, Y. Shao, S. Jiang, L. Ma, and J. Liu, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22283377].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22226654">Synthesis and Antiviral Activities of Novel Gossypol Derivatives.</a> Yang, J., F. Zhang, J. Li, G. Chen, S. Wu, W. Ouyang, W. Pan, R. Yu, and P. Tien, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(3): p. 1415-1420; PMID[22226654].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">26. <a href="file:///C:/Users/AppData/Local/Temp/:/www.ncbi.nlm.nih.gov/pubmed/22228771">Broad Antiviral Activity and Crystal Structure of HIV-1 Fusion Inhibitor Sifuvirtide.</a> Yao, X., H. Chong, C. Zhang, S. Waltersperger, M. Wang, S. Cui, and Y. He, Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22228771].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22226544">Facile Synthesis of Acacetin-7-O-beta-D-galactopyranoside.</a> Zacharia, J.T. and M. Hayashi, Carbohydrate Research, 2012. 348: p. 91-94; PMID[22226544].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22107736">Bicyclic Hydroxy-1H-pyrrolopyridine-trione Containing HIV-1 Integrase Inhibitors.</a> Zhao, X.Z., K. Maddali, M. Metifiot, S.J. Smith, B.C. Vu, C. Marchand, S.H. Hughes, Y. Pommier, and T.R. Burke, Jr., Chemical Biology &amp; Drug Design, 2012. 79(2): p. 157-165; PMID[22107736].
    <br />
    <b>[Pubmed]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298149500011">Evaluation of the Performance of the Abbott ARCHITECT HIV Ag/Ab Combo Assay.</a> Chavez, P., L. Wesolowski, P. Patel, K. Delaney, and S.M. Owen, Journal of Clinical Virology, 2011. 52: p. S51-S55; ISI[000298149500011].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298414200023">Iron Chelators of the di-2-Pyridylketone thiosemicarbazone and 2-Benzoylpyridine thiosemicarbazone Series Inhibit HIV-1 Transcription: Identification of Novel Cellular Targets-Iron, Cyclin-Dependent Kinase (CDK) 2, and CDK9.</a> Debebe, Z., T. Ammosova, D. Breuer, D.B. Lovejoy, D.S. Kalinowski, K. Kumar, M. Jerebtsova, P. Ray, F. Kashanchi, V.R. Gordeuk, D.R. Richardson, and S. Nekhai, Molecular Pharmacology, 2011. 80(6): p. 1190-1190; ISI[000298414200023].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">31. 1<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298498000004">,3,4-Oxadiazole: A Privileged Structure in Antiviral Agents.</a> Li, Z., P. Zhan, and X. Liu, Mini-Reviews in Medicinal Chemistry, 2011. 11(13): p. 1130-1142; ISI[000298498000004].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298405100075">Introducing Catastrophe-QSAR. Application on Modeling Molecular Mechanisms of Pyridinone Derivative-Type HIV Non-nucleoside Reverse Transcriptase Inhibitors.</a> Putz, M.V., M. Lazea, A.M. Putz, and C. Duda-Seiman, International Journal of Molecular Sciences, 2011. 12(12): p. 9533-9569; ISI[000298405100075].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298163400033">TRAF6 and IRF7 Control HIV Replication in Macrophages.</a> Sirois, M., L. Robitaille, R. Allary, M. Shah, C.H. Woelk, J. Estaquier, and J. Corbeil, Plos One, 2011. 6(11); ISI[000298163400033].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298380200007">An Ethnobotanical Survey of Medicinal Plants of Laos Toward the Discovery of Bioactive Compounds as Potential Candidates for Pharmaceutical Development.</a> Soejarto, D.D., C. Gyllenhaal, M.R. Kadushin, B. Southavong, K. Sydara, S. Bouamanivong, M. Xaiveu, H.J. Zhang, S.G. Franzblau, G.T. Tan, J.M. Pezzuto, M.C. Riley, B.G. Elkington, and D.P. Waller, Pharmaceutical Biology, 2012. 50(1): p. 42-60; ISI[000298380200007].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298630600013">Transcriptional Gene Silencing of HIV-1 Through Promoter Targeted RNA is Highly Specific.</a> Suzuki, K., T. Ishida, M. Yamagishi, C. Ahlenstiel, S. Swaminathan, K. Marks, D. Murray, E.M. McCartney, M.R. Beard, M. Alexander, D.F.J. Purcell, D.A. Cooper, T. Watanabe, and A.D. Kelleher, RNA Biology, 2011. 8(6): p. 1035-1046; ISI[000298630600013].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298347700019">Single-Chain Fv-Based anti-HIV Proteins: Potential and Limitations.</a> West, A.P., R.P. Galimidi, P.N.P. Gnanapragasam, and P.J. Bjorkman, Journal of Virology, 2012. 86(1): p. 195-202; ISI[000298347700019].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298645500055">HIV-1 Glycoprotein 41 Ectodomain Induces Activation of the CD74 Protein-mediated Extracellular Signal-regulated Kinase/Mitogen-activated Protein Kinase Pathway to Enhance Viral Infection.</a> Zhou, C., L. Lu, S.Y. Tan, S.B. Jiang, and Y.H. Chen, Journal of Biological Chemistry, 2011. 286(52): p. 44869-44877; ISI[000298645500055].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p class="MsoNoSpacing"></p>

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298345500006">Current Progress in the Development of RNAi-based Therapeutics for HIV-1.</a> Zhou, J. and J.J. Rossi, Gene Therapy, 2011. 18(12): p. 1134-1138; ISI[000298345500006].
    <br />
    <b>[WOS]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">SciFinder citations</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120105&amp;CC=WO&amp;NR=2012003498A1&amp;KC=A1">2-(Quinolin-6-yl)acetic acid Derivatives as HIV Antiviral Agents and Their Preparation and Use for the Treatment of AIDS.</a> Babaoglu, K., K. Bjornson, H. Guo, R.L. Halcomb, J.O. Link, H. Liu, M.L. Mitchell, J. Sun, R.W. Vivian, and L. Xu. 2012: 351pp.</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120105&amp;CC=WO&amp;NR=2012003497A1&amp;KC=A1">2-(Naphth-2-yl)acetic acid Derivatives as HIV Antiviral Agents and Their Preparation and Use for the Treatment of AIDS.</a> Babaoglu, K., K. Bjornson, H. Guo, R.L. Halcomb, J.O. Link, R. McFadden, M.L. Mitchell, P. Roethle, J.D. Trenkle, R.W. Vivian, and L. Xu. 2012: 2011-US42880 2012003497: 444pp..</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120105&amp;CC=WO&amp;NR=2012000459A1&amp;KC=A1">Anti-Hiv Infection Polypeptides, Composition, and Use.</a> Liu, K., S. Jiang, W. Shi, Q. Jia, Y. Bai, S. Feng, L. Cai, C. Wang, S. Zhang, and X. Jiang. 2012: 2011-CN76812 2012000459: 31pp.</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120118&amp;CC=CN&amp;NR=102321102A&amp;KC=A">N2, N4-Bis-Substituted-2h,4h-pyrrolo[1,2-B][1,2,4,6]thiophene-triazine-1,1,3-trione Derivatives, Their Preparation Method and Application as HIV-1 Non-nucleoside Inhibitors to Prepare anti-HIV Drugs.</a> Liu, X., W. Chen, and P. Zhan. 2012: 2011-10202340 102321102: 20pp.</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120104&amp;CC=EP&amp;NR=2402357A1&amp;KC=A1">Analogs of 2&#39;,3&#39;-Dideoxy-3&#39;-fluorothymidine 5&#39;-monophosphate for Treatment of HIV Infection.</a> Miazga, A., P. Ziemkowski, T. Kulikowski, F. Hamy, S. Louvel, and T. Klimkait. 2012: 2010-172852 2402357: 10pp.</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">44. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120118&amp;CC=CN&amp;NR=102321009A&amp;KC=A">Aroylhydrazone Derivatives, Their Preparation and Application in Preparing anti-HIV-1 Drugs.</a> Xu, H. and Z. Che. 2012: 2011-10192240 102321009: 24pp.</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">45. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120104&amp;CC=EP&amp;NR=2402356A2&amp;KC=A2">Modified 2&#39;,3&#39;-Dideoxynucleosides for Treatment Infections Caused by HIV.</a> Ziemkowski, P., A. Miazga, T. Kulikowski, T. Klimkait, S. Louvel, F. Hamy, A. Lipniacki, and A. Piasek. 2012: 2010-172854</p>

    <p class="plaintext">2402356: 8pp.</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>

    <p> </p>

    <p class="plaintext">46. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120104&amp;CC=EP&amp;NR=2402358A2&amp;KC=A2">2&#39;,3&#39;-Didehydro-3&#39;-deoxy-4&#39;-ethynylthymidine Derivatives for Treatment Infections Caused by Human Immunodeficiency Virus (HIV) Multidrug Resistant Strains, Method of Their Synthesis and Pharmaceutically Acceptable Forms of Drugs.</a> Ziemkowski, P.a., A. Miazga, T. Kulikowski, S. Louvel, T. Klimkait, and V. Vidal. 2012: 2010-172853 2402358: 7pp.</p>

    <p class="plaintext"><b>[SciFinder]</b>. HIV_0120-020212.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
