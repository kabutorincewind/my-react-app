

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-118.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="a/6lQ5rMu6oSmGw2BkoQi2s50iyz/SP2wmfqsztkh4koE/pXEVec2QoZQixV/eeRaKdJQHhRYJ3D3/fX+aPCsGyiSSIWpVSjqQyCOQYUdQ6/NCU4JahXvhRon1E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1EEF6E63" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-118-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58412   DMID-LS-118; EMBASE-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Role of interferons in the control of Lassa virus replication in human dendritic cells and macrophages</p>

    <p>          Baize, Sylvain, Pannetier, Delphine, Faure, Caroline, Marianneau, Philippe, Marendat, Ingrid, Georges-Courbot, Marie-Claude, and Deubel, Vincent</p>

    <p>          Microbes and Infection <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4JHMGYF-1/2/aeeed9fc293f44f42406ad1f84789916">http://www.sciencedirect.com/science/article/B6VPN-4JHMGYF-1/2/aeeed9fc293f44f42406ad1f84789916</a> </p><br />

    <p>2.     58413   DMID-LS-118; EMBASE-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent inhibitors of the hepatitis C virus NS3 protease: Use of a novel P2 cyclopentane-derived template</p>

    <p>          Johansson, Per-Ola, Back, Marcus, Kvarnstrom, Ingemar, Jansson, Katarina, Vrang, Lotta, Hamelink, Elizabeth, Hallberg, Anders, Rosenquist, Asa, and Samuelsson, Bertil</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4JW121D-1/2/e26a4b6816f0ada6863c59421f76d74e">http://www.sciencedirect.com/science/article/B6TF8-4JW121D-1/2/e26a4b6816f0ada6863c59421f76d74e</a> </p><br />

    <p>3.     58414   DMID-LS-118; EMBASE-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatotoxicity and antiretroviral therapy with protease inhibitors: A review</p>

    <p>          Bruno, R, Sacchi, P, Maiocchi, L, Patruno, S, and Filice, G</p>

    <p>          Digestive and Liver Disease <b>2006</b>.  In Press, Corrected Proof</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7582-4JVTCHP-1/2/f4d2c8f8963d38294988049d95dc109d">http://www.sciencedirect.com/science/article/B7582-4JVTCHP-1/2/f4d2c8f8963d38294988049d95dc109d</a> </p><br />

    <p>4.     58415   DMID-LS-118; EMBASE-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of HCV NS5B polymerase: Synthesis and structure-activity relationships of N-alkyl-4-hydroxyquinolon-3-yl-benzothiadiazine sulfamides</p>

    <p>          Chris Krueger, A, Madigan, Darold L, Jiang, Wen W, Kati, Warren M, Liu, Dachun, Liu, Yaya, Maring, Clarence J, Masse, Sherie, McDaniel, Keith F, and Middleton, Tim</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4JVTBNM-2/2/446d9240af93ca90a805f3193899bca3">http://www.sciencedirect.com/science/article/B6TF9-4JVTBNM-2/2/446d9240af93ca90a805f3193899bca3</a> </p><br />

    <p>5.     58416   DMID-LS-118; EMBASE-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The history of N-methanocarbathymidine: The investigation of a conformational concept leads to the discovery of a potent and selective nucleoside antiviral agent</p>

    <p>          Marquez, Victor E, Hughes, Stephen H, Sei, Shizuko, and Agbaria, Riad</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JWMSCF-2/2/65321d4f7edf62c7af40ca37582d302e">http://www.sciencedirect.com/science/article/B6T2H-4JWMSCF-2/2/65321d4f7edf62c7af40ca37582d302e</a> </p><br />
    <br clear="all">

    <p>6.     58417   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ebola and Marburg viruses: the humans strike back</p>

    <p>          Alazard-Dany, N, Terrangle, MO, and Volchkov, V</p>

    <p>          M S-MEDECINE SCIENCES: M S-Med. Sci <b>2006</b>.  22(4): 405-410, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236850900017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236850900017</a> </p><br />

    <p>7.     58418   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The use of the hydrodynamic HBV animal model to study HBV biology and anti-viral therapy</p>

    <p>          Ketzinel-Gilad, M, Zauberman, A, Nussbaum, O, Shoshany, Y, Ben-Moshe, O, Pappo, O, Felig, Y, Ilan, E, Wald, H, Dagan, S, and Galun, E</p>

    <p>          HEPATOLOGY RESEARCH: Hepatol. Res <b>2006</b>.  34(4): 228-237, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236803000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236803000004</a> </p><br />

    <p>8.     58419   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Extended effects of human papillomavirus 16 E6-specific short hairpin RNA on cervical carcinoma cells</p>

    <p>          Bai, L, Wei, L, Wang, J, Li, X, and He, P</p>

    <p>          INTERNATIONAL JOURNAL OF GYNECOLOGICAL CANCER: Int. J. Gynecol. Cancer <b>2006</b>.  16(2): 718-729, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236943200041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236943200041</a> </p><br />

    <p>9.     58420   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of HPV 16 E6 oncogene expression by RNA interference in vitro and in vivo</p>

    <p>          Niu, XY, Peng, ZL, Duan, WQ, Wang, H, and Wang, P</p>

    <p>          INTERNATIONAL JOURNAL OF GYNECOLOGICAL CANCER: Int. J. Gynecol. Cancer <b>2006</b>.  16(2): 743-751, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236943200044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236943200044</a> </p><br />

    <p>10.   58421   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Esculatin-4-carboxylic acid ethyl ester: A novel class of marine-derived anti-SARS-coronavirus 3CL protease inhibitors</p>

    <p>          Jean, F, Hamill, P, Chow, P, Raj, M, Kao, R, and Andersen, R</p>

    <p>          FASEB JOURNAL: Faseb J <b>2006</b>.  20(4): A51-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236206500244">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236206500244</a> </p><br />

    <p>11.   58422   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of IL-6 knockout on susceptibility to HSV-1 respiratory infection and intrinsic macrophage anti-viral resistance in mice</p>

    <p>          Murphy, EA, Davis, JM, Carmichael, MD, Ghaffar, A, and Mayer, EP</p>

    <p>          FASEB JOURNAL: Faseb J <b>2006</b>.  20(4): A213-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236206501444">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236206501444</a> </p><br />
    <br clear="all">

    <p>12.   58423   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel open-chain analogues of neplanocin A</p>

    <p>          Hong, JH, Kim, SY, Oh, CH, Yoo, KH, and Cho, JH</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS: Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(3): 341-350, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236794200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236794200011</a> </p><br />

    <p>13.   58424   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The CPSF30 binding site on the NS1A protein of influenza A virus is a potential antiviral target</p>

    <p>          Twu, KY, Noah, DL, Rao, P, Kuo, RL, and Krug, RM</p>

    <p>          JOURNAL OF VIROLOGY: J. Virol <b>2006</b>.  80(8): 3957-3965, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236685600027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236685600027</a> </p><br />

    <p>14.   58425   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A novel fingerprint map for detecting SARS-CoV</p>

    <p>          Gao, L, Ding, YS, Dai, H, Shao, SH, Huang, ZD, and Chou, KC</p>

    <p>          JOURNAL OF PHARMACEUTICAL AND BIOMEDICAL ANALYSIS: J. Pharm. Biomed. Anal <b> 2006</b>.  41(1): 246-250, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236655100034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236655100034</a> </p><br />

    <p>15.   58426   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Induction of productive human papillomavirus type 11 life cycle in epithelial cells grown in organotypic raft cultures</p>

    <p>          Fang, L, Meyers, C, Budgeon, LR, and Howett, MK</p>

    <p>          VIROLOGY: Virology <b>2006</b>.  347(1): 28-35, 8</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236531600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236531600003</a> </p><br />

    <p>16.   58427   DMID-LS-118; WOS-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent SARS inhibitors identified</p>

    <p>          Anon</p>

    <p>          CHEMICAL &amp; ENGINEERING NEWS: Chem. Eng. News <b>2006</b>.  84(14): 35-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236607100047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236607100047</a> </p><br />

    <p>17.   58428   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of feline (FIPV) and human (SARS) coronavirus by semisynthetic derivatives of glycopeptide antibiotics</p>

    <p>          Balzarini, J,  Keyaerts, E, Vijgen, L, Egberink, H, De Clercq, E, Van, Ranst M, Printsevskaya, SS, Olsufyeva, EN, Solovieva, SE, and Preobrazhenskaya, MN</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675038&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675038&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   58429   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian influenza fact sheet (April 2006)</p>

    <p>          Anon</p>

    <p>          Wkly Epidemiol Rec <b>2006</b>.  81(14): 129-136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16673509&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16673509&amp;dopt=abstract</a> </p><br />

    <p>19.   58430   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Influenza: current threat from avian influenza</p>

    <p>          Stephenson, I and Democratis, J</p>

    <p>          Br Med Bull <b>2006</b>.  75-76: 63-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16651383&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16651383&amp;dopt=abstract</a> </p><br />

    <p>20.   58431   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of HCV NS5B polymerase: Synthesis and structure-activity relationships of N-1-benzyl and N-1-[3-methylbutyl]-4-hydroxy-1,8-naphthyridon-3-yl benzothiadiazine analogs containing substituents on the aromatic ring</p>

    <p>          Rockway, TW, Zhang, R, Liu, D, Betebenner, DA, McDaniel, KF, Pratt, JK, Beno, D, Montgomery, D, Jiang, WW, Masse, S, Kati, WM, Middleton, T, Molla, A, Maring, CJ, and Kempf, DJ</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650984&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650984&amp;dopt=abstract</a> </p><br />

    <p>21.   58432   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral properties of new arylsulfone derivatives with activity against human betaherpesviruses</p>

    <p>          Naesens, L, Stephens, CE, Andrei, G, Loregian, A, De, Bolle L, Snoeck, R, Sowell, JW, and De, Clercq E</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650489&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650489&amp;dopt=abstract</a> </p><br />

    <p>22.   58433   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Insights from modeling the 3D structure of H5N1 influenza virus neuraminidase and its binding interactions with ligands</p>

    <p>          Wei, DQ, Du, QS, Sun, H, and Chou, KC</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  344(3): 1048-1055</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16647045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16647045&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   58434   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          VX-950, a Novel Hepatitis C Virus (HCV) NS3-4A Protease Inhibitor, Exhibits Potent Antiviral Activities in HCV Replicon Cells</p>

    <p>          Lin, K, Perni, RB, Kwong, AD, and Lin, C</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(5): 1813-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641454&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641454&amp;dopt=abstract</a> </p><br />

    <p>24.   58435   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro comparison of antiviral drugs against feline herpesvirus 1</p>

    <p>          van der Meulen, K, Garre, B, Croubels, S, and Nauwynck, H</p>

    <p>          BMC Vet Res <b>2006</b>.  2(1): 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640781&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640781&amp;dopt=abstract</a> </p><br />

    <p>25.   58436   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of Hydroxyferroquine Derivatives with Antimalarial and Antiviral Activities</p>

    <p>          Biot, C, Daher, W, Chavain, N, Fandeur, T, Khalife, J, Dive, D, and De, Clercq E</p>

    <p>          J Med Chem <b>2006</b>.  49(9): 2845-2849</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640347&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640347&amp;dopt=abstract</a> </p><br />

    <p>26.   58437   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of SCH446211 (SCH6): A New Ketoamide Inhibitor of the HCV NS3 Serine Protease and HCV Subgenomic RNA Replication</p>

    <p>          Bogen, SL, Arasappan, A, Bennett, F, Chen, K, Jao, E, Liu, YT, Lovey, RG, Venkatraman, S, Pan, W, Parekh, T, Pike, RE, Ruan, S, Liu, R, Baroudy, B, Agrawal, S, Chase, R, Ingravallo, P, Pichardo, J, Prongay, A, Brisson, JM, Hsieh, TY, Cheng, KC, Kemp, SJ, Levy, OE, Lim-Wilby, M, Tamura, SY, Saksena, AK, Girijavallabhan, V, and Njoroge, FG</p>

    <p>          J Med Chem <b>2006</b>.  49(9): 2750-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640336&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640336&amp;dopt=abstract</a> </p><br />

    <p>27.   58438   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          N-Chlorotaurine Is an Effective Antiviral Agent against Adenovirus In Vitro and in the Ad5/NZW Rabbit Ocular Model</p>

    <p>          Romanowski, EG, Yates, KA, Teuchner, B, Nagl, M, Irschick, EU, and Gordon, YJ</p>

    <p>          Invest Ophthalmol Vis Sci <b>2006</b>.  47(5): 2021-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16639011&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16639011&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   58439   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          N-Acetylphenylisoserinates of Lactarius Sesquiterpenoid Alcohols - Cytotoxic, Antiviral, Antiproliferative and Immunotropic Activities in vitro</p>

    <p>          Krawczyk, E, Kniotek, M, Nowaczyk, M, Dzieciatkowski, T, Przybylski, M, Majewska, A, and Luczak, M</p>

    <p>          Planta Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16636965&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16636965&amp;dopt=abstract</a> </p><br />

    <p>29.   58440   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          1-Cinnamoyl-3,11-dihydroxymeliacarpin is a natural bioactive compound with antiviral and nuclear factor-kappaB modulating properties</p>

    <p>          Barquero, AA, Michelini, FM, and Alche, LE</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  344(3): 955-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16631615&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16631615&amp;dopt=abstract</a> </p><br />

    <p>30.   58441   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oligonucleotides as antivirals: Dream or realistic perspective?</p>

    <p>          Van Aerschot, A</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621039&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621039&amp;dopt=abstract</a> </p><br />

    <p>31.   58442   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Enhancement of the infectivity of SARS-CoV in BALB/c mice by IMP dehydrogenase inhibitors, including ribavirin</p>

    <p>          Barnard, DL, Day, CW, Bailey, K, Heiner, M, Montgomery, R, Lauridsen, L, Winslow, S, Hoopes, J, Li, JK, Lee, J, Carson, DA, Cottam, HB, and Sidwell, RW</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621037&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621037&amp;dopt=abstract</a> </p><br />

    <p>32.   58443   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Influenza: the next pandemic?: A review</p>

    <p>          Adungo, FO, Adungo, NI, Bedno, S, and Yingst, SL</p>

    <p>          East Afr Med J  <b>2005</b>.  82(9): 477-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16619723&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16619723&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>33.   58444   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Molecular Virology of Hepatitis C Virus (HCV): 2006 Update</p>

    <p>          Brass, V, Moradpour, D, and Blum, HE</p>

    <p>          Int J Med Sci <b>2006</b>.  3(2): 29-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16614739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16614739&amp;dopt=abstract</a> </p><br />

    <p>34.   58445   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Current status of anti-picornavirus therapies</p>

    <p>          Barnard, DL</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(11): 1379-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611122&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611122&amp;dopt=abstract</a> </p><br />

    <p>35.   58446   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent advances in antiviral agents: antiviral drug discovery for hepatitis viruses</p>

    <p>          Tanikawa, K</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(11): 1371-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611121&amp;dopt=abstract</a> </p><br />

    <p>36.   58447   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antisense treatments for biothreat agents</p>

    <p>          Warfield, KL, Panchal, RG, Aman, MJ, and Bavari, S</p>

    <p>          Curr Opin Mol Ther <b>2006</b>.  8(2): 93-103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610760&amp;dopt=abstract</a> </p><br />

    <p>37.   58448   DMID-LS-118; PUBMED-DMID-5/8/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Analysis of synthetic peptides from heptad-repeat domains of herpes simplex virus type 1 glycoproteins H and B</p>

    <p>          Galdiero, S, Vitiello, M, D&#39;Isanto, M, Falanga, A, Collins, C, Raieta, K, Pedone, C, Browne, H, and Galdiero, M</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 5): 1085-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603508&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603508&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
