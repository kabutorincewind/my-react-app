

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-07-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gtcEMEB5FCDJlBi/tRItIB69pR4aPvwVnoOgVfIpR6f09DlzSt0Z09SmRQ4cl+UbKa7cOAk6UdWLZGc2z1ldz9fZ3Pm/EwrRYqzc3+uLe9pRCQ1KBPrx9ZwU9O0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EED3B9BA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">June 25 - July 8, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20590148">Antifungal and Cytotoxic Activity of Withanolides from Acnistus arborescens.</a> Roumy, V., M. Biabiany, T. Hennebelle, E.M. Aliouat, M. Pottier, H. Joseph, S. Joha, B. Quesnel, R. Alkhatib, S. Sahpaz, and F. Bailleul. J Nat Prod. <b>[Epub ahead of print]</b>; PMID[20590148].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20504981">In vitro activity of anidulafungin and other agents against esophageal candidiasis-associated isolates from a phase 3 clinical trial.</a> Pfaller, M.A., R. Hollis, B.P. Goldstein, S. Messer, D. Diekema, and T. Henkel. J Clin Microbiol. 48(7): p. 2613-4; PMID[20504981].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20603106">Novel short antibacterial and antifungal peptides with low cytotoxicity: efficacy and action mechanisms.</a> Qi, X., C. Zhou, P. Li, W. Xu, Y. Cao, H. Ling, W.N. Chen, C.M. Li, R. Xu, M. Lamrani, Y. Mu, S. Leong, M.W. Chang, and M.B. Chan-Park. Biochem Biophys Res Commun. <b>[Epub ahead of print]</b>; PMID[20603106].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19837144">In vitro antifungal activity of phenylheptatriyne from Bidens cernua L. against yeasts.</a> Rybalchenko, N.P., V.A. Prykhodko, S.S. Nagorna, N.N. Volynets, A.N. Ostapchuk, V.V. Klochko, T.V. Rybalchenko, and L.V. Avdeeva. Fitoterapia. 81(5): p. 336-8; PMID[19837144].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20603144">Anticandidal Action of Fungal Chitosan against Candida albicans.</a> Tayel, A.A., S. Moussa, W.F. El-Tras, D. Knittel, K. Opwis, and E. Schollmeyer. Int J Biol Macromol. <b>[Epub ahead of print]</b>; PMID[20603144].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20582812">Evaluation of in vitro activity of carvacrol against Candida albicans strains.</a> Vardar-Unlu, G., A. Yagmuroglu, and M. Unlu. Nat Prod Res. 24(12): p. 1189-93; PMID[20582812].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20583864">Bis dihydropyrimidine: synthesis and antimycobacterial activity.</a> Ali, M.A., E. Manogaran, J. Govindasamy, V. Sellappan, and S. Pandian. J Enzyme Inhib Med Chem. <b>[Epub ahead of print]</b>; PMID[20583864].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20607650">Antitubercular Resorcinol Analogs and Benzenoid C-Glucoside from the Roots of Ardisia cornudentata.</a> Chang, C.P., H.S. Chang, C.F. Peng, S.J. Lee, and I.S. Chen. Planta Med. <b>[Epub ahead of print]</b>; PMID[20607650].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20586565">New drugs and regimens for treatment of TB.</a> Leibert, E. and W.N. Rom. Expert Rev Anti Infect Ther. 8(7): p. 801-13; PMID[20586565].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20226572">A note to the biological activity of benzoxazine derivatives containing the thioxo group.</a> Waisser, K., E. Petrlikova, M. Perina, V. Klimesova, J. Kunes, K. Palat, Jr., J. Kaustova, H.M. Dahse, and U. Mollmann. Eur J Med Chem. 45(7): p. 2719-25; PMID[20226572].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0625-070810.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278870300008">Comparison of the activities of the lantibiotics nisin and lacticin 3147 against clinically significant mycobacteria.</a> Carroll, J., L.A. Draper, P.M. O&#39;Connor, A. Coffey, C. Hill, R.P. Ross, P.D. Cotter, and J. O&#39;Mahony. International Journal of Antimicrobial Agents. 36(2): p. 132-136; ISI[000278870300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278805500017">Triazole-Linked Inhibitors of Inosine Monophosphate Dehydrogenase from Human and Mycobacterium tuberculosis.</a> Chen, L.Q., D.J. Wilson, Y.L. Xu, C.C. Aldrich, K. Felczak, Y.Y. Sham, and K.W. Pankiewicz. Journal of Medicinal Chemistry. 53(12): p. 4768-4778; ISI[000278805500017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278760700002">Antitubercular Potential of Plants: A Brief Account of Some Important Molecules.</a> Negi, A.S., J.K. Kumar, S. Luqman, D. Saikia, and S.P.S. Khanuja. Medicinal Research Reviews. 30(4): p. 603-645; ISI[000278760700002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278891700003">Synthesis and antimicrobial activity of [N-1-(N-substituted-arylidene-hydrazino)-acetyl]-2-methyl-imidazoles and [N-1-(4-substituted-aryl-3-chloro-2-oxo-1-azetidinyl-amino)-acetyl]-2-me thyl-imidazoles.</a> Srivastava, S.K., R. Dua, and S.D. Srivastava. Proceedings of the National Academy of Sciences India Section a-Physical Sciences. 80A(I Web of Knowledge Search Alert): p. 117-121; ISI[000278891700003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278626400069">Synthesis and in vitro evaluation of substituted pyrimido[5,4-d]pyrimidines as a novel class of Antimycobacterium tuberculosis agents.</a> Bacelar, A.H., M.A. Carvalho, and M.F. Proenca. European Journal of Medicinal Chemistry. 45(7): p. 3234-3239; ISI[000278626400069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278638500013">Novel 2-Hydrazino-pyrimidin-4(3H)-One Derivatives as Potential Dihydrofolate Reductase Inhibitors.</a> Degani, M.S., S. Bag, R. Bairwa, N.R. Tawari, and S.F. Queener. Journal of Heterocyclic Chemistry. 47(3): p. 558-563; ISI[000278638500013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0625-070810.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278638500035">Preparation and Properties In Vitro and In Vivo of Antitubercular Pyrroles.</a> Hearn, M.J., M.F. Chen, M.S. Terrot, E.R. Webster, and M.H. Cynamon. Journal of Heterocyclic Chemistry. 47(3): p. 707-712; ISI[000278638500035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0625-070810.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
