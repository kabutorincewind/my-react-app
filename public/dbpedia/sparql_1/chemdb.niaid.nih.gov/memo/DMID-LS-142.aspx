

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-142.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Hb2jD614HSg1JIdloMwgz2L3R67TVsY1fuCZqkpg6q9VCFmdPo6tlwSGh+0NBwRpYEEOF3IsGiMgtsrO4m+1Z9LjbWeixm/dweq8cd4AOb0tjjOS+k0MGTDdv7I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="15FB077B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-142-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60595   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Rift Valley fever virus lacking NSm proteins retains high virulence in vivo and may provide a model of human delayed onset neurologic disease</p>

    <p>          Bird, Brian H, Albarino, Cesar G, and Nichol, Stuart T</p>

    <p>          Virology <b>2007</b>.  In Press, Corrected Proof: 1392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NF2H7B-1/2/eadc1a497211323383cc8b9ac1eb5eec">http://www.sciencedirect.com/science/article/B6WXR-4NF2H7B-1/2/eadc1a497211323383cc8b9ac1eb5eec</a> </p><br />

    <p>2.     60596   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Antiviral and antimicrobial profiles of selected isoquinoline alkaloids from Fumaria and Corydalis species</p>

    <p>          Orhana, I, Ozcelik, B, Karaoglu, T, and Sener, B</p>

    <p>          Z Naturforsch [C] <b>2007</b>.  62(1-2): 19-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425100&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425100&amp;dopt=abstract</a> </p><br />

    <p>3.     60597   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Activity of T-1106 in a hamster model of yellow fever virus infection</p>

    <p>          Julander, JG, Furuta, Y, Schafer, K, and Sidwell, RW</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420215&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420215&amp;dopt=abstract</a> </p><br />

    <p>4.     60598   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Inhibitory activity of three classes of acyclic nucleoside phosphonates against murine polyomavirus and primate SV40 strains</p>

    <p>          Lebeau, I, Andrei, G, Krecmerova, M, De, Clercq E, Holy, A, and Snoeck, R</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420214&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420214&amp;dopt=abstract</a> </p><br />

    <p>5.     60599   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Concise synthesis of dideoxy-epigallocatechin gallate (DO-EGCG) and evaluation of its anti-influenza virus activity</p>

    <p>          Furuta, T, Hirooka, Y, Abe, A, Sugata, Y, Ueda, M, Murakami, K, Suzuki, T, Tanaka, K, and Kan, T</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420124&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60600   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          SCH 503034, a Novel Hepatitis C Virus Protease Inhibitor, Plus Pegylated Interferon alpha-2b for Genotype 1 Nonresponders</p>

    <p>          Sarrazin, C, Rouzier, R, Wagner, F, Forestier, N, Larrey, D, Gupta, SK, Hussain, M, Shah, A, Cutler, D, Zhang, J, and Zeuzem, S</p>

    <p>          Gastroenterology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17408662&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17408662&amp;dopt=abstract</a> </p><br />

    <p>7.     60601   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Drug Targets and Molecular Mechanisms of Drug Resistance in Chronic Hepatitis B</p>

    <p>          Ghany, M and Liang, TJ</p>

    <p>          Gastroenterology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17408658&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17408658&amp;dopt=abstract</a> </p><br />

    <p>8.     60602   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Emergence of influenza B viruses with reduced sensitivity to neuraminidase inhibitors</p>

    <p>          Hatakeyama, S, Sugaya, N, Ito, M, Yamazaki, M, Ichikawa, M, Kimura, K, Kiso, M, Shimizu, H, Kawakami, C, Koike, K, Mitamura, K, and Kawaoka, Y</p>

    <p>          JAMA <b>2007</b>.  297(13): 1435-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17405969&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17405969&amp;dopt=abstract</a> </p><br />

    <p>9.     60603   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Strong and Selective Inhibitors of HBV-Replication Between Novel N4-Hydroxy-and 5-Methyl-{beta}-L-deoxycytidine Analogues</p>

    <p>          Matthes, E, Funk, A, Krahn, I, Gaertner, K, von, Janta-Lipinski M, Lin, L, Will, H, and Sirma, H</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17404006&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17404006&amp;dopt=abstract</a> </p><br />

    <p>10.   60604   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Identification of Anthranilic Acid Derivatives as a Novel Class of Allosteric Inhibitors of Hepatitis C NS5B Polymerase</p>

    <p>          Nittoli, T, Curran, K, Insaf, S, Digrandi, M, Orlowski, M, Chopra, R, Agarwal, A, Howe, AY, Prashad, A, Floyd, MB, Johnson, B, Sutherland, A, Wheless, K, Feld, B, O&#39;connell, J, Mansour, TS, and Bloom, J</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17402724&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17402724&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60605   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Evaluating the 3C-like protease activity of SARS-Coronavirus: Recommendations for standardized assays for drug discovery</p>

    <p>          Grum-Tokars, V, Ratia, K, Begaye, A, Baker, SC, and Mesecar, AD</p>

    <p>          Virus Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17397958&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17397958&amp;dopt=abstract</a> </p><br />

    <p>12.   60606   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Evaluation of a cyclophilin inhibitor in hepatitis C virus-infected chimeric mice in vivo</p>

    <p>          Inoue, K, Umehara, T, Ruegg, UT, Yasui, F, Watanabe, T, Yasuda, H, Dumont, JM, Scalfaro, P, Yoshiba, M, and Kohara, M</p>

    <p>          Hepatology <b>2007</b>.  45(4): 921-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17393519&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17393519&amp;dopt=abstract</a> </p><br />

    <p>13.   60607   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Structure--antiadenoviral activity of nitrogen containing macroheterocycles and their analogues</p>

    <p>          Dyachenko, NS, Nosach, LN, Povnitsa, OY, Kuz&#39;min, VE, Artemenko, AG, Lozitskaya, RN, Basok, SS, Alexeeva, IV, Zhovnovataya, VL, and Vanden, Eynde JJ</p>

    <p>          Mikrobiol Z <b>2006</b>.  68(5): 69-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17388122&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17388122&amp;dopt=abstract</a> </p><br />

    <p>14.   60608   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Mutagenic effect of ribavirin on hepatitis C nonstructural 5B quasispecies in vitro and during antiviral therapy</p>

    <p>          Hofmann, WP, Polta, A, Herrmann, E, Mihm, U, Kronenberger, B, Sonntag, T, Lohmann, V, Schonberger, B, Zeuzem, S, and Sarrazin, C</p>

    <p>          Gastroenterology <b>2007</b>.  132(3): 921-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17383421&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17383421&amp;dopt=abstract</a> </p><br />

    <p>15.   60609   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Design, Synthesis, and Evaluation of Inhibitors for Severe Acute Respiratory Syndrome 3C-Like Protease Based on Phthalhydrazide Ketones or Heteroaromatic Esters</p>

    <p>          Zhang, J, Pettersson, HI, Huitema, C, Niu, C, Yin, J, James, MN, Eltis, LD, and Vederas, JC</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17381079&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17381079&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60610   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Antiviral activity and RNA polymerase degradation following Hsp90 inhibition in a range of negative strand viruses</p>

    <p>          Connor, John H, McKenzie, Margie O, Parks, Griffith D, and Lyles, Douglas S</p>

    <p>          Virology <b>2007</b>.  In Press, Corrected Proof: 1460</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4MX4VN1-6/2/ada6578eb584c6033b10d304b7f2e497">http://www.sciencedirect.com/science/article/B6WXR-4MX4VN1-6/2/ada6578eb584c6033b10d304b7f2e497</a> </p><br />

    <p>17.   60611   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Nuclear and Nucleolar Targeting of Influenza A Virus NS1A Protein: Striking Differences between Different Virus Subtypes</p>

    <p>          Melen, K, Kinnunen, L, Fagerlund, R, Ikonen, N, Twu, KY, Krug, RM, and Julkunen, I</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17376915&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17376915&amp;dopt=abstract</a> </p><br />

    <p>18.   60612   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Real-time quantitative PCR for assessment of antiviral drug effects against Epstein-Barr virus replication and EBV late mRNA expression</p>

    <p>          Ballout, M, Germi, R, Fafi-Kremer, S, Guimet, J, Bargues, G, Seigneurin, JM, and Morand, P</p>

    <p>          J Virol Methods <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17368820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17368820&amp;dopt=abstract</a> </p><br />

    <p>19.   60613   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Intramuscular Administration of Neuraminidase Inhibitor Peramivir Promotes Survival Against Lethal H5N1 Influenza Infection in Mice</p>

    <p>          Boltz, David A, Ilyushina, Natalia A, Arnold, CShane, Babu, YSudhakar, Webster, Robert G, and Govorkova, Elena A</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-G/2/2a9195e0871f3f6f31d87736fca489e4">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-G/2/2a9195e0871f3f6f31d87736fca489e4</a> </p><br />

    <p>20.   60614   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Phosphorodiamidate Morpholino Oligomer--Mediated Inhibition of Influenza A Virus in Mice</p>

    <p>          Voss, Thomas, Warfield, Kelly, Brocato, Rebecca, Barbercheck, Joseph, Kaplin, Bryan, Stein, David, Bavari, Sina, and Iversen, Patrick</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1R/2/182cd52cf678c22f7e8c475babffffb8">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1R/2/182cd52cf678c22f7e8c475babffffb8</a> </p><br />
    <br clear="all">

    <p>21.   60615   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Inhibition of rhinovirus replication in vitro and in vivo by Acid-buffered saline</p>

    <p>          Gern, JE, Mosser, AG, Swenson, CA, Rennie, PJ, England, RJ, Shaffer, J, and Mizoguchi, H</p>

    <p>          J Infect Dis <b>2007</b>.  195(8): 1137-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17357049&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17357049&amp;dopt=abstract</a> </p><br />

    <p>22.   60616   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Development and Validation of a High Throughput Screen for Inhibitors of Respiratory Syncytial Virus</p>

    <p>          Heil, Marintha, McDowell, Michael, Jonsson, Colleen, and Severson, William</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A59-A57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2S/2/d8093a48524e291e7ecdb8c306108686">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2S/2/d8093a48524e291e7ecdb8c306108686</a> </p><br />

    <p>23.   60617   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          High Throughput Screening of a 100,000 Compound Library for Inhibitors of Influenza A Virus (H3N2)</p>

    <p>          Severson, William, McDowell, Michael, Rasumussen, Lynn, Sosa, Mindy, Ananthan, Subramaniam, Noah, James, White, Lucile, and Jonsson, Colleen</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A62-A57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-31/2/ab841b1c1f7cb4f3630c8c10eeb0a2f5">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-31/2/ab841b1c1f7cb4f3630c8c10eeb0a2f5</a> </p><br />

    <p>24.   60618   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Combinations of Thiovir and Neuraminidase Inhibitors Exert Synergistic Antiviral Activity on Human, Equine and Avian Influenza In Vitro</p>

    <p>          Waninger, Shani, Ramos, Silvestre, and Robbins, Joan</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A82-A57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4S/2/b96f685d64e72c47b84d8ce9b18c0016">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4S/2/b96f685d64e72c47b84d8ce9b18c0016</a> </p><br />

    <p>25.   60619   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Identification of inhibitors using a cell-based assay for monitoring Golgi-resident protease activity</p>

    <p>          Coppola, Julia M, Hamilton, Christin A, Bhojani, Mahaveer S, Larsen, Martha J, Ross, Brian D, and Rehemtulla, Alnawaz</p>

    <p>          Analytical Biochemistry <b>2007</b>.  364(1): 19-29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W9V-4MV756W-3/2/78873a681fdeccdb495d45baa8b11bfd">http://www.sciencedirect.com/science/article/B6W9V-4MV756W-3/2/78873a681fdeccdb495d45baa8b11bfd</a> </p><br />

    <p>26.   60620   DMID-LS-142; PUBMED-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Inactivation of avian influenza viruses by chemical agents and physical conditions: a review</p>

    <p>          De Benedictis, P, Beato, MS, and Capua, I</p>

    <p>          Zoonoses Public Health <b>2007</b>.  54(2): 51-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348909&amp;dopt=abstract</a> </p><br />

    <p>27.   60621   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Quantitative structure activity relationship studies on thiourea analogues as influenza virus neuraminidase inhibitors</p>

    <p>          Nair, Pramod C and Sobhia, MElizabeth</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript:  2891</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4NFR510-3/2/89ce851ff790380ffaf1af795d23a04a">http://www.sciencedirect.com/science/article/B6VKY-4NFR510-3/2/89ce851ff790380ffaf1af795d23a04a</a> </p><br />

    <p>28.   60622   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Neuraminidase inhibitor susceptibility of porcine H3N2 influenza A viruses isolated in Germany between 1982 and 1999</p>

    <p>          Bauer, Katja, Schrader, Christina, Suess, Jochen, Wutzler, Peter, and Schmidtke, Michaela</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 2891</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4NDDNJH-1/2/8846d4bf1fe48791dbb327212fbc8880">http://www.sciencedirect.com/science/article/B6T2H-4NDDNJH-1/2/8846d4bf1fe48791dbb327212fbc8880</a> </p><br />

    <p>29.   60623   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Antipyretic effect of Mao-to, a Japanese herbal medicine, for treatment of type A influenza infection in children</p>

    <p>          Kubo, Tomohiro and Nishimura, Hidekazu</p>

    <p>          Phytomedicine <b>2007</b>.  14(2-3): 96-101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7GVW-4MG6P5T-1/2/ac66fcc8f201d02f6385176ca1cf7b28">http://www.sciencedirect.com/science/article/B7GVW-4MG6P5T-1/2/ac66fcc8f201d02f6385176ca1cf7b28</a> </p><br />

    <p>30.   60624   DMID-LS-142; WOS-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Characterisation of the role of zinc in the hepatitis C virus NS2/3 auto-cleavage and NS3 protease activities</p>

    <p>          Tedbury, PR and Harris, M</p>

    <p>          JOURNAL OF MOLECULAR BIOLOGY: J. Mol. Biol <b>2007</b>.  366(5): 1652-1660, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244621100024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244621100024</a> </p><br />

    <p>31.   60625   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Activity of 1-(S)-[3-Hydroxy-2-(Phosphonomethoxy)Propyl]-5-Azacytosine and its Ester Prodrugs</p>

    <p>          Krecmerova, Marcela, Holy, Antonin, Piskala, Alois, Andrei, Graciela, Snoeck, Robert, Naesens, Lieve, Neyts, Johan, Balzarini, Jan, and De Clercq, Erik</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-K/2/1658147152b92e77e6fdee739bd1a61b">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-K/2/1658147152b92e77e6fdee739bd1a61b</a> </p><br />
    <br clear="all">

    <p>32.   60626   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          In Vivo Antiviral Activity of 1-(S)-[3-Hydroxy-2-(Phosphonomethoxy)Propyl]-5-Azacytosine and its Cyclic Form</p>

    <p>          Andrei, Graciela, Krecmerova, Marcela, Holy, Antonin, Naesens, Lieve, Neyts, Johan, Balzarini, Jan, De Clercq, Erik, and Snoeck, Robert</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A33-A34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-M/2/9ee56b4a5138655b64efddb997253a7a">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-M/2/9ee56b4a5138655b64efddb997253a7a</a> </p><br />

    <p>33.   60627   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Successful Treatment in the Monkeypox and Variola Primate Models of Smallpox by the Oral Drug ST-246</p>

    <p>          Huggins, John, Goff, Arthur, Eric, Mucker, Twenhafel, Nancy, Chapman, Jennifer, Tate, Mallory, Jordan, Rob, Bolken, Tove&#39;, and Hruby, Dennis</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A35-A34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-R/2/b63dc12f36dc67f3165326ee036c652e">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-R/2/b63dc12f36dc67f3165326ee036c652e</a> </p><br />

    <p>34.   60628   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Design and Characterization of R1626, A Prodrug of the HCV Replication Inhibitor R1479 (4&#39;-Azidocytidine) With Enhanced Oral Bioavailability</p>

    <p>          Klumpp, Klaus, Smith, David, Brandl, Michael, Alfredson, Tom, Sarma, Keshab, Smith, Mark, Najera, Isabel, Jiang, Wen-Rong, Le Pogam, Sophie, Leveque, Vincent, Ma, Han, Tu, Yaping, Chan, Rebecca, Chen, Chiao-Wen, Wu, Xiaoyang, Birudaraj, Raj, Swallow, Steven, Martin, Joseph A, Cammack, Nick, Berns, Heather, Fettner, Scott, Ipe, David, Mannino, Marie, O&#39;Mara, Edward, Washington, Carla, Roberts, Stuart, Cooksley, Graham, Dore, Greg, Shaw, David, Blue, Jr David R, Zahm, Friederike, and Hill, George</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A35-A34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-S/2/806a3b242735d88ff6bee4cf01420822">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-S/2/806a3b242735d88ff6bee4cf01420822</a> </p><br />

    <p>35.   60629   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Potent HCV NS5B Polymerase Inhibitors Derived From 5-Hydroxy-3(2H)-Pyridazinones: Part 1: Exploration of Pyridazinone 4-Substituent Variation</p>

    <p>          Zhou, Y, Li, L-S, Webber, S, Dragovich, P, Murphy, D, Tran, C, Ruebsam, F, Shah, A, Tsan, M, Showalter, R, Brooks, J, Okamoto, E, Nolan, T, Norris, DA, and Kirkovsky, L</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A38-A37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-10/2/d870de81cf68babe8a2d49d0b4207279">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-10/2/d870de81cf68babe8a2d49d0b4207279</a> </p><br />

    <p>36.   60630   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          The Cyclophilin Inhibitor Debio-025 is a Potent Inhibitor of Hepatitis C Virus Replication in vitro With a Unique Resistance Profile</p>

    <p>          Coelmont, Lotte, Paeshuyse, Jan, Kaptein, Suzanne, Vliegen, Inge, Kaul, Artur, De Clercq, Erik, Rosenwirth, Brigitte, Scalfaro, Pietro, Crabbe, Raf, Bartenschlager, Ralf, Dumont, Jean-Maurice, and Neyts, Johan</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A39-A37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-12/2/e05a7a229d37d7913a9bc283112c646f">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-12/2/e05a7a229d37d7913a9bc283112c646f</a> </p><br />

    <p>37.   60631   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p><b>          Nitazoxanide is an Effective Antiviral Agent Against Both HBV and HCV replication in vitro</b> </p>

    <p>          Korba, Brent, Abigail, Muller, Marc, Ayers, and Rossignol, Jean-Francois</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A40-A37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-14/2/c3b7e26c219ffe1f9acfeed6dccbefe1">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-14/2/c3b7e26c219ffe1f9acfeed6dccbefe1</a> </p><br />

    <p>38.   60632   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Antiviral Activity of (-)-Carbocyclic Cytosine [(-)-Carbodine] Against Venezuelan Equine Encephalitis Virus (VEEV) in a Mouse Model</p>

    <p>          Julander, Justin, Chu, Chung, Rao, Jagadeeshwar, Shafer, Kristiina, and Morrey, John</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A44-A45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1H/2/c73176e7b484b1cdfb1d1111fd4352e5">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1H/2/c73176e7b484b1cdfb1d1111fd4352e5</a> </p><br />

    <p>39.   60633   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Iso-methyl-alkoxyalkyl (S)-HPMPA Esters</p>

    <p>          Beadle, James R, Hostetler, Karl Y, Buller, RMark, Schriewer, Jill, Aldern, Kathy A, Prichard, Mark N, Keith, Kathy, and Kern, Earl R</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A48-A45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1V/2/988e938abd81d0da5b7223ea950f1317">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1V/2/988e938abd81d0da5b7223ea950f1317</a> </p><br />

    <p>40.   60634   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          NIM811, A Cyclophilin Inhibitor, and NM107, An HCV Polymerase Inhibitor, Synergistically Inhibits HCV Replication and Suppresses the Emergence of Resistance In Vitro</p>

    <p>          Boerner, Joanna, Ma, Sue, Compton, Teresa, and Lin, Kai</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A57-A52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2M/2/511d99a82b8fb3cc4382bd93d5f7e118">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2M/2/511d99a82b8fb3cc4382bd93d5f7e118</a> </p><br />

    <p>41.   60635   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Identification and Characterization of a Novel, Potent HCV Helicase Inhibitor</p>

    <p>          Peng, Junzhong, Huang, Chunsheng, Murray, Michael G, and Huang, Zhuhui</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A67-A63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3H/2/0ce8f2d755c01b332f5cdab0c4e7f463">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3H/2/0ce8f2d755c01b332f5cdab0c4e7f463</a> </p><br />

    <p>42.   60636   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Discovery of Cage Antiviral Agents</p>

    <p>          Klimochkin, Yuri, Boreko, Eugene, Shiryaev, Andrey, Moiseev, Igor, Golovin, Eugene, and Leonova, Marina</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A73-A63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-40/2/3e622ff8f7ae907d309eea8c52d5903f">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-40/2/3e622ff8f7ae907d309eea8c52d5903f</a> </p><br />
    <br clear="all">

    <p>43.   60637   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Chloroquine a Novel and Versatile Anti viral Agent with Nine Prong Modes of Anti viral Actions and Postive Approach in Radical Cure of Viral Hepatitis Varieties B and C Both Acute and Chronic Forms</p>

    <p>          Chandramohan, M, Vivekanandan, SC, Sivakumar, D, and Selvam, P</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A74-A75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-44/2/4b9e39918eb1b1c5b8f882f52f3d8b93">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-44/2/4b9e39918eb1b1c5b8f882f52f3d8b93</a> </p><br />

    <p>44.   60638   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Design, Synthesis, Antiviral Activity and Cytotoxicity of Novel Sulphonamide Derivatives</p>

    <p>          Selvam, P, Smee, DF, Gowen, BB, Day, CW, Barnard, DL, and Morrey, JD</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A81-A75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4P/2/7a0c13576746605c3d2a22fcd4ac4432">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4P/2/7a0c13576746605c3d2a22fcd4ac4432</a> </p><br />

    <p>45.   60639   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Potent Inhibition of Viral Entry and Replication of SARS-CoV by siRNAs Targeting the Genes Encoding the Cellular ACE2 Receptor or the Viral Nucleocapsid Protein</p>

    <p>          Yan, Xin, Shen, Hua, Feng, Yan, Wang, Jun, Lou, Shiwen, Wang, Liping, Wong, Gillian, Yang, Zhaoxiong, Jiang, Hongjian, Wu, Xinqi, Hu, Dan, Guan, Yi, Smaill, Fiona, and Zhang, Chengsheng</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A30-A31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-C/2/a18391a8a0beba40a8bcec4878ea755d">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-C/2/a18391a8a0beba40a8bcec4878ea755d</a> </p><br />

    <p>46.   60640   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          The Inhibitory Effects of Medicinal Herbs on SARS-CoV Entry In Vitro</p>

    <p>          Zhuang, Min, Jiang, Hong, Xiao, Peng, Suzuki, Yasuhiro, and Hattori, Toshio</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A82-A83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4W/2/e2eef9389d5e45d19611d83bc9fde1d2">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4W/2/e2eef9389d5e45d19611d83bc9fde1d2</a> </p><br />

    <p>47.   60641   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          Plant lectins are potent inhibitors of coronaviruses by interfering with two targets in the viral replication cycle</p>

    <p>          Keyaerts, Els, Vijgen, Leen, Pannecouque, Christophe, Van Damme, Els, Peumans, Willy, Egberink, Herman, Balzarini, Jan, and Van Ranst, Marc</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Corrected Proof: 133</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4NCMCSH-1/2/458753ea55114653bffdd7dcc4bad748">http://www.sciencedirect.com/science/article/B6T2H-4NCMCSH-1/2/458753ea55114653bffdd7dcc4bad748</a> </p><br />

    <p>48.   60642   DMID-LS-142; EMBASE-DMID-4/11/2007</p>

    <p class="memofmt1-2">          SARS epidemiology--From descriptive to mechanistic analyses</p>

    <p>          Zhao, Guo-Ping </p>

    <p>          Virus Research  <b>2007</b>.  In Press, Corrected Proof: 207</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4N6FNPW-1/2/8c9884ccd4d0f3da1a14db2a3127963e">http://www.sciencedirect.com/science/article/B6T32-4N6FNPW-1/2/8c9884ccd4d0f3da1a14db2a3127963e</a> </p><br />

    <p>49.   60643   DMID-LS-142; WOS-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Real-time PCR determination of human herpesvirus 6 antiviral drug susceptibility</p>

    <p>          Isegawa, Y, Takemoto, M, Yamanishi, K, Ohshima, A, and Sugimoto, N</p>

    <p>          JOURNAL OF VIROLOGICAL METHODS: J. Virol. Methods <b>2007</b> .  140(1-2): 25-31, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244671400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244671400004</a> </p><br />

    <p>50.   60644   DMID-LS-142; WOS-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 5 &#39;-triazole nucleosides</p>

    <p>          Lee, L, Chang, KH, Valiyev, F, Liu, HJ, and Li, WS</p>

    <p>          JOURNAL OF THE CHINESE CHEMICAL SOCIETY: J. Chin. Chem. Soc <b>2006</b>.  53(6): 1547-1555, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244623400042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244623400042</a> </p><br />

    <p>51.   60645   DMID-LS-142; WOS-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Inhibition of respiratory syncytial virus (RSV) replication in cell culture by small interfering RNA (siRNA)</p>

    <p>          Khaitov, MR, Akimov, VS, Faizuloev, EB, Nikonova, AA, Alexeev, LP, Zverev, VV, and DuBuske, LM </p>

    <p>          JOURNAL OF ALLERGY AND CLINICAL IMMUNOLOGY: J. Allergy Clin. Immunol <b>2007</b>.  119(1): S233-S234, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243642201300">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243642201300</a> </p><br />

    <p>52.   60646   DMID-LS-142; WOS-DMID-4/12/2007</p>

    <p class="memofmt1-2">          Peptide-conjugated phosphorodiamidate morpholino oligomers inhibit alphavirus replication and prevent lethal encephalitis in VEEV-infected mice</p>

    <p>          Paessler, S, Ni, HL, Yun, NE, Stein, D, and Rijnbrand, C</p>

    <p>          AMERICAN JOURNAL OF TROPICAL MEDICINE AND HYGIENE: Am. J. Trop. Med. Hyg <b>2006</b>.  75(5): 184-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343901074">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343901074</a> </p><br />

    <p>53.   60647   DMID-LS-142; WOS-DMID-4/12/2007</p>

    <p class="memofmt1-2">          New and emerging infectious diseases</p>

    <p>          Spicuzza, L, Spicuzza, A, La Rosa, M, Polosa, R, and Di Maria, G</p>

    <p>          ALLERGY AND ASTHMA PROCEEDINGS: Allergy Asthma Proc <b>2007</b>.  28(1): 28-34, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244263000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244263000007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
