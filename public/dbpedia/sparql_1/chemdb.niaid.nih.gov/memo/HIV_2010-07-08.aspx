

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-07-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ZRJ/6FcwPtliI540SwXXEtSvMJrqvczXEM0YPoCDRrDwz9Rckxeb7+5WhSjtMm0YyOC+dgXkTvDsJaMstDSXvdczWVIQERnuzKGDXHSEdugRhzeKAieLLImyKbw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="58374126" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  June 25 - July 8, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20600334">Quinoline-based compounds as modulators of HIV transcription through NF-kappaB and Sp1 inhibition.</a>  Bedoya LM, Abad MJ, Calonge E, Saavedra LA, Gutiérrez CM, Kouznetsov VV, Alcami J, Bermejo P.  Antiviral Res. 2010 Jun 23. [Epub ahead of print]PMID: 20600334</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20599774">Inhibition of HIV-1 by Non-Nucleoside Reverse Transcriptase Inhibitors via an Induced Fit Mechanism - Importance of Slow Dissociation and Relaxation Rates for Antiviral Efficacy.</a>  Elinder M, Selhorst P, Vanham G, Oberg B, Vrang L, Danielson UH.  Biochem Pharmacol. 2010 Jul 2. [Epub ahead of print]PMID: 20599774</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20592080">Identification of a gp41 Core-binding Molecule with Homologous Sequence of Human TNNI3K Like Protein as a Novel Human Immunodeficiency Viruses Type 1 Entry Inhibitor.</a>  Zhu Y, Lu L, Xu L, Yang H, Jiang S, Chen YH.  J Virol. 2010 Jun 30. [Epub ahead of print]PMID: 20592080</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20586421">Peptide HIV-1 Integrase Inhibitors from HIV-1 Gene Products.</a>  Suzuki S, Urano E, Hashimoto C, Tsutsumi H, Nakahara T, Tanaka T, Nakanishi Y, Maddali K, Han Y, Hamatake M, Miyauchi K, Pommier Y, Beutler JA, Sugiura W, Fuji H, Hoshino T, Itotani K, Nomura W, Narumi T, Yamamoto N, Komano JA, Tamamura H.  J Med Chem. 2010 Jun 29. [Epub ahead of print]PMID: 20586421</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20582467">A synthetic globotriaosylceramide analogue inhibits HIV-1 infection in vitro by two mechanisms.</a>  Harrison AL, Olsson ML, Jones RB, Ramkumar S, Sakac D, Binnington B, Henry S, Lingwood CA, Branch DR.  Glycoconj J. 2010 Jun 26. [Epub ahead of print]PMID: 20582467</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20572021">Topology of the disulfide bonds in the antiviral lectin scytovirin.</a>  Moulaei T, Stuchlik O, Reed M, Yuan W, Pohl J, Lu W, Haugh-Krumpe L, O&#39;Keefe BR, Wlodawer A.  Protein Sci. 2010 Jun 23. [Epub ahead of print]PMID: 20572021</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20427524">A low-molecular-weight entry inhibitor of both CCR5- and CXCR4-tropic strains of human immunodeficiency virus type 1 targets a novel site on gp41.</a>  Murray EJ, Leaman DP, Pawa N, Perkins H, Pickford C, Perros M, Zwick MB, Butler SL.  J Virol. 2010 Jul;84(14):7288-99. Epub 2010 Apr 28.PMID: 20427524</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20421122">Primary mutations selected in vitro with raltegravir confer large fold changes in susceptibility to first-generation integrase inhibitors, but minor fold changes to inhibitors with second-generation resistance profiles.</a>  Goethals O, Vos A, Van Ginderen M, Geluykens P, Smits V, Schols D, Hertogs K, Clayton R.  Virology. 2010 Jul 5;402(2):338-46. Epub 2010 Apr 24.PMID: 20421122</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20406823">Oxazolomycin biosynthesis in Streptomyces albus JA3453 featuring an &quot;acyltransferase-less&quot; type I polyketide synthase that incorporates two distinct extender units.</a>  Zhao C, Coughlin JM, Ju J, Zhu D, Wendt-Pienkowski E, Zhou X, Wang Z, Shen B, Deng Z.  J Biol Chem. 2010 Jun 25;285(26):20097-108. Epub 2010 Apr 20.PMID: 20406823</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20398971">Novel antiviral benzofuran-transition metal complexes.</a>  Galal SA, Abd El-All AS, Hegab KH, Magd-El-Din AA, Youssef NS, El-Diwani HI.  Eur J Med Chem. 2010 Jul;45(7):3035-46. Epub 2010 Mar 27.PMID: 20398971</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20357085">Viral entry inhibitors targeted to the membrane site of action.</a>  Porotto M, Yokoyama CC, Palermo LM, Mungall B, Aljofan M, Cortese R, Pessi A, Moscona A.  J Virol. 2010 Jul;84(13):6760-8. Epub 2010 Mar 31.PMID: 20357085</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20006684">A bioactive isoprenylated xanthone and other constituents of Garcinia edulis.</a>  Magadula JJ.  Fitoterapia. 2010 Jul;81(5):420-3. Epub 2009 Dec 16.PMID: 20006684</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19962287">A dimeric high-molecular-weight chymotrypsin inhibitor with antitumor and HIV-1 reverse transcriptase inhibitory activities from seeds of Acacia confusa.</a>  Lam SK, Ng TB.  Phytomedicine. 2010 Jul;17(8-9):621-5. Epub 2009 Dec 3.PMID: 19962287</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="rprtbody">14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278701100011">Isolation and characterization of a French bean hemagglutinin with antitumor, antifungal, and anti-HIV-1 reverse transcriptase activities and an exceptionally high yield.</a>  Lam SK, Ng TB. Phytomedicine. 2010 May;17(6):457-62. Epub 2009 Sep 8.</p>

    <p class="rprtbody">15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278713200030">Synthesis, Biological Properties and Anti-HIV-1 Activity of New Pyrimidine P1,P2-Dinucleotides.</a>  Miazga A, Ziemkowski P, Siwecka MA, Lipniacki A, Piasek A, Kulikowski T. Nucleosides Nucleotides Nucleic Acids. 2010 Jun;29(4-6):438-44.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278476600008">GERANYL DERIVATIVES OF ISOQUINOLINE ALKALOIDS SHOW INCREASED BIOLOGICAL ACTIVITIES.</a> Nishiyama, Y; Iwasa, K; Okada, S; Takeuchi, S; Moriyasu, M; Kamigauchi, M; Koyama, J; Takeuchi, A; Tokuda, H; Kim, HS; Wataya, Y; Takeda, K; Liu, YN; Wu, PC; Bastow, KF; Akiyama, T; Lee, KH.  HETEROCYCLES, 2010; 81(5): 1193-1229.</p>

    <p class="rprtbody">17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278626400016">Efficient synthesis and identification of novel propane-1,3-diamino bridged CCR5 antagonists with variation on the basic center carrier.</a>  Fan X, Zhang HS, Chen L, Long YQ. Eur J Med Chem. 2010 Jul;45(7):2827-40. Epub 2010 Mar 7.</p>

    <p class="rprtbody">18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278480900005">Pharmacophore and structure-activity relationships of integrase inhibition within a dual inhibitor scaffold of HIV reverse transcriptase and integrase.</a>  Wang Z, Tang J, Salomon CE, Dreis CD, Vince R. Bioorg Med Chem. 2010 Jun 15;18(12):4202-11. Epub 2010 May 7.</p>

    <p>19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278480900020">Anti-AIDS agents 82: Synthesis of seco-(3 &#39; R,4 &#39; R)-3 &#39;,4 &#39;-di-O-(S)-camphanoyl-(+)-cis-khellactone (DCK) derivatives as novel anti-HIV agents.</a>  Tang, JA; Qian, KD; Zhang, BN; Chen, Y; Xia, P; Yu, DL; Xia, Y; Yang, ZY; Chen, CH; Morris-Natschke, SL; Lee, KH.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(12): 4363-4373.</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278626800006">Polyketide-peroxides from a Species of Jamaican Plakortis (Porifera: Demospongiae).</a>  Mohammed, R; Peng, JN; Kelly, M; Yousaf, M; Winn, E; Odde, S; Bie, Z; Xie, AH; Doerksen, RJ; Hamann, MT.  AUSTRALIAN JOURNAL OF CHEMISTRY, 2010; 63(6): 877-885.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
