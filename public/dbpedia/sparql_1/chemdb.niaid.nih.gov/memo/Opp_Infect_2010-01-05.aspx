

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-01-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CJBy2xMv5GQxFJWG1gdNFbz1zMPeRRQH6gh6Gqv36kRIA69ZUoKCfxUOgAyOOxOkgFoBlXkunbsJoo/lxs02ibM/tFJATBL/iuTv5wJT2VE7mjxpsLkck8rFT0o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BBD9A607" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">December 23, 2009 - January 5, 2010</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-1">Opportunistic protozoa COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="NoSpacing"> </p>

    <p class="NoSpacing">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20047919?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">Evaluation of new thiazolide/thiadiazolide derivatives reveals nitro group-independent efficacy against <i>Cryptosporidium parvum</i> in vitro development.</a>  Gargala G, Le Goff L, Ballet JJ, Favennec L, Stachulski AV, Rossignol JF.  Antimicrob Agents Chemother. 2010 Jan 4. [Epub ahead of print]PMID: 20047919 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-1">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-1"> </p>

    <p class="NoSpacing">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20044131?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">The efficacy of self-assembled cationic antimicrobial peptide nanoparticles against<i>Cryptococcus neoformans</i> for the treatment of meningitis.</a>  Wang H, Xu K, Liu L, Tan JP, Chen Y, Li Y, Fan W, Wei Z, Sheng J, Yang YY, Li L. Biomaterials. 2009 Dec 29. [Epub ahead of print]PMID: 20044131 [PubMed - as supplied by publisher]</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="NoSpacing">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20047918?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">The Antibacterial Activity of N-Pentylpantothenamide is Due to Inhibition of CoA Synthesis.</a>  Thomas J, Cronan JE.  Antimicrob Agents Chemother. 2010 Jan 4. [Epub ahead of print]PMID: 20047918 [PubMed - as supplied by publisher]<a href="http://www.ncbi.nlm.nih.gov/sites/entrez?db=pubmed&amp;cmd=link&amp;linkname=pubmed_pubmed&amp;uid=20047918&amp;ordinalpos=7">Related articles</a></p> 

    <p class="NoSpacing">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20047831?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=8">Synthesis and antibacterial activity of benzyl-[3-(benzylamino-methyl)-cyclohexylmethyl]-amine derivatives.</a>  Kumar D, Joshi S, Rohilla RK, Roy N, Rawat DS.  Bioorg Med Chem Lett. 2009 Dec 24. [Epub ahead of print]PMID: 20047831 [PubMed - as supplied by publisher]</p>

    <p class="NoSpacing"> </p>

    <p class="NoSpacing">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20036039?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=17">Synthesis, in vitro antibacterial and antifungal evaluations of new alpha-hydroxyphosphonate and new alpha-acetoxyphosphonate derivatives of tetrazolo [1, 5-a] quinoline.</a>  Kategaonkar AH, Pokalwar RU, Sonar SS, Gawali VU, Shingate BB, Shingare MS.  Eur J Med Chem. 2009 Dec 23. [Epub ahead of print]PMID: 20036039 [PubMed - as supplied by publisher]</p> 

    <p>6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20030828?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=108">A rhodanine agent active against non-replicating intracellular <i>Mycobacterium avium</i> subspecies paratuberculosis.</a>  Bull, T.J., R. Linedale, J. Hinds, and J. Hermon-Taylor Gut Pathog, 2009. 1(1): p. 25; PMID[20030828]</p>

    <p> </p>

    <p>7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20045640?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=31">Synthesis, antimalarial and antitubercular activity of acetylenic chalcones.</a> Hans, R.H., E.M. Guantai, C. Lategan, P.J. Smith, B. Wan, S.G. Franzblau, J. Gut, P.J. Rosenthal, and K. Chibale.  Bioorg Med Chem Lett, 2009; PMID[20045640]</p>

    <p> </p>

    <p>8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20030344?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=113">Structure and Total Synthesis of Fungal Calpinactam, A New Antimycobacterial Agent.</a> Koyama, N., S. Kojima, T. Fukuda, T. Nagamitsu, T. Yasuhara, S. Omura, and H. Tomoda.  Org Lett, 2009; PMID[20030344]</p>

    <p> </p>

    <p>9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20038272?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=75">Preclinical pharmacokinetics of KBF611, a new antituberculosis agent in mice and rabbits, and comparison with thiacetazone.</a>  Shahab, F.M., F. Kobarfard, B. Shafaghi, and S. Dadashzadeh.   Xenobiotica, 2009; PMID[20038272]</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
