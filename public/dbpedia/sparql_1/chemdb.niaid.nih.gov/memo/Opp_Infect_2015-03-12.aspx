

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-03-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="YmJpKuEe9HK8W0rQLX0jQT1nDReoYu3P3PErdgJyUxLeml3UALmI/IJ9XVybCtFxyk9dpUQFWBGPUsbkisndP++e1nFCpGh5hyEAqWyyi9q3wvpVTvU8OhVBREQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="953E78BE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: February 27 - March 12, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25673986">Design and Formulation of a Topical Hydrogel Integrating Lemongrass-loaded Nanosponges with an Enhanced Antifungal Effect: In Vitro/in Vivo Evaluation.</a> Aldawsari, H.M., S.M. Badr-Eldin, G.S. Labib, and A.H. El-Kamel. International Journal of Nanomedicine, 2015. 10: p. 893-902. PMID[25673986].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25541751">Broad Spectrum Antibacterial and Antifungal Polymeric Paint Materials: Synthesis, Structure-activity Relationship, and Membrane-active Mode of Action.</a> Hoque, J., P. Akkapeddi, V. Yadav, G.B. Manjunath, D.S. Uppu, M.M. Konai, V. Yarlagadda, K. Sanyal, and J. Haldar. ACS Applied Materials &amp; Interfaces, 2015. 7(3): p. 1804-1815. PMID[25541751].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25512406">Pharmacodynamic Target Evaluation of a Novel Oral Glucan Synthase Inhibitor, SCY-078 (MK-3118), Using an in Vivo Murine Invasive Candidiasis Model.</a> Lepak, A.J., K. Marchillo, and D.R. Andes. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1265-1272. PMID[25512406].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25722982">The Influence of Tea Tree Oil (Melaleuca alternifolia) on Fluconazole Activity against Fluconazole-resistant Candida albicans Strains.</a> Mertas, A., A. Garbusinska, E. Szliszka, A. Jureczko, M. Kowalska, and W. Krol. BioMed Research International, 2015. 590470:  9pp. PMID[25722982].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25638570">Synthesis of Novel Thiazole-based 8,9-Dihydro-7H-pyrimido[4,5-b][1,4]diazepines as Potential Antitumor and Antifungal Agents.</a> Ramirez, J., L. Svetaz, J. Quiroga, R. Abonia, M. Raimondi, S. Zacchino, and B. Insuasty. European Journal of Medicinal Chemistry, 2015. 92: p. 866-875. PMID[25638570].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25629386">6. Antitumor and Antimicrobial Potential of Bromoditerpenes Isolated from the Red Alga, Sphaerococcus coronopifolius.</a> Rodrigues, D., C. Alves, A. Horta, S. Pinteus, J. Silva, G. Culioli, O.P. Thomas, and R. Pedrosa. Marine Drugs, 2015. 13(2): p. 713-726. PMID[25629386].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25641692">Susceptibility of Candida albicans to New Synthetic Sulfone Derivatives.</a> Staniszewska, M., M. Bondaryk, and Z. Ochal. Archiv der Pharmazie, 2015. 348(2): p. 132-143. PMID[25641692].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25652819">Total and Semi-syntheses of Antimicrobial Thuggacin Derivatives.</a> Franke, J., M. Bock, R. Dehn, J. Fohrer, S.B. Mhaske, A. Migliorini, A.A. Kanakis, R. Jansen, J. Herrmann, R. Muller, and A. Kirschning. Chemistry, 2015. 21(11): p. 4272-4284. PMID[25652819].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25734014">Design and Synthesis of Novel Hybrid Molecules against Malaria.</a> Lodige, M. and L. Hiersch. International Journal of Medicinal Chemistry, 2015. 458319:  23pp. PMID[25734014].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25685309">Evaluation of Aromatic 6-Substituted Thienopyrimidines as Scaffolds against Parasites that Cause Trypanosomiasis, Leishmaniasis, and Malaria.</a> Woodring, J.L., G. Patel, J. Erath, R. Behera, P.J. Lee, S.E. Leed, A. Rodriguez, R.J. Sciotti, K. Mensa-Wilmot, and M.P. Pollastri. MedChemComm, 2015. 6(2): p. 339-346. PMID[25685309].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0227-031215.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348689500010">In Vitro Study on Antifungal Activity of Achillea wilhelmsii Flower Essential Oil against Twenty Strains of Candida albicans.</a> Amjad, L., K. Mousavidehmourdi, and Z. Rezvani. Chiang Mai Journal of Science, 2014. 41(5.1): p. 1058-1064. ISI[000348689500010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348345700023">Antitumor and Antifungal Activities of Organic Extracts of Seacucumber Holothuria atra from the Southeast Coast of India.</a> Dhinakaran, D.I. and A.P. Lipton. Journal of Ocean University of China, 2015. 14(1): p. 185-189. ISI[000348345700023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348493200012">Antifungal Efficiency of Miconazole and Econazole and the Interaction with Transport Protein: A Comparative Study.</a> Hu, Z.Q., J. Zhang, and X.R. Cheng. Pharmaceutical Biology, 2015. 53(2): p. 251-261. ISI[000348493200012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349534800012">Shape-based Virtual Screening, Docking, and Molecular Dynamics Simulations to Identify Mtb-ASADH Inhibitors.</a> Kumar, R., P. Garg, and P.V. Bharatam. Journal of Biomolecular Structure &amp; Dynamics, 2015. 33(5): p. 1082-1093. ISI[000349534800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349290800012">The Antibacterial and Antifungal Activity of Essential Oils Extracted from Guatemalan Medicinal Plants.</a> Miller, A.B., R.G. Cates, M. Lawrence, J.A.F. Soria, L.V. Espinoza, J.V. Martinez, and D.A. Arbizu. Pharmaceutical Biology, 2015. 53(4): p. 548-554. ISI[000349290800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348987700040">Novel Hybrid-pyrrole Derivatives: Their Synthesis, Antitubercular Evaluation and Docking Studies.</a> Saha, R., M.M. Alam, and M. Akhter. RSC Advances, 2015. 5(17): p. 12807-12820. ISI[000348987700040].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349331700005">Design, Synthesis and Biological Evaluation of Some New Coumarin Derivatives as Potential Antimicrobial Agents.</a> Singh, L.K., Priyanka, V. Singh, and D. Katiyar. Medicinal Chemistry, 2015. 11(2): p. 128-134. ISI[000349331700005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348610000074">The Novel Arylamidine T-2307 Maintains in Vitro and in Vivo Activity against Echinocandin-resistant Candida albicans.</a> Wiederhold, N.P., L.K. Najvar, A.W. Fothergill, R. Bocanegra, M. Olivo, D.I. McCarthy, W.R. Kirkpatrick, Y. Fukuda, J. Mitsuyama, and T.F. Patterson. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1341-1343. ISI[000348610000074].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348574100041">Novel Synthesis of Some Imidazolyl-, Benzoxazinyl-, and Quinazolinyl-2,4-dioxothiazolidine Derivatives.</a> Youssef, A.M., A.K. El-Ziaty, W.S.I. Abou-Elmagd, and S.K. Ramadan. Journal of Heterocyclic Chemistry, 2015. 52(1): p. 278-283. ISI[000348574100041].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0227-031215.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
