

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-77.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yprf/Afe/RPUHTieh5RTO5R7QAmn/JbsPHRMrla65UrwpdXAo4tt9ng8mD2JayHenNUPy6uuhxnG4UecPzl9IO+vjDtFufA9PV+pS5tuhta6GM0NT3MS2rPV0No=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="18CF4095" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-77-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6455   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inhibition of Epstein-Barr Virus (EBV) Reactivation by Short Interfering RNAs Targeting p38 Mitogen-Activated Protein Kinase or c-myc in EBV-Positive Epithelial Cells</p>

    <p>          Gao, X, Wang, H, and Sairenji, T</p>

    <p>          J Virol <b>2004</b>.  78(21): 11798-806</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479821&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479821&amp;dopt=abstract</a> </p><br />

    <p> </p>

    <p>2.         6456   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Human Sars-Coronavirus Rna-Dependent Rna Polymerase: Activity Determinants and Nucleoside Analogue Inhibitors</p>

    <p>          Azzi, A. and Lin, S.</p>

    <p>          Proteins-Structure Function and Bioinformatics <b>2004</b>.  57(1 ): 12-14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>3.         6457   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Antiviral activities of medicinal herbs traditionally used in southern mainland China</p>

    <p>          Li, Y, Ooi, LS, Wang, H, But, PP, and Ooi, VE</p>

    <p>          Phytother Res <b>2004</b>.  18(9): 718-722</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15478204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15478204&amp;dopt=abstract</a> </p><br />

    <p>4.         6458   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          High Resolution Crystal Structure of Human Rab9 Gtpase - a Novel Antiviral Drug Target</p>

    <p>          Chen, L. <i>et al.</i></p>

    <p>          Journal of Biological Chemistry <b> 2004</b>.  279(38): 40204-40208</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>5.         6459   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Interferon-beta and interferon-gamma synergistically inhibit the replication of severe acute respiratory syndrome-associated coronavirus (SARS-CoV)</p>

    <p>          Sainz, B Jr, Mossel, EC, Peters, CJ, and Garry, RF</p>

    <p>          Virology <b>2004</b>.  329(1): 11-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476870&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476870&amp;dopt=abstract</a> </p><br />

    <p> </p>

    <p>6.         6460   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Influence of Mutations in the Hepatitis B Virus Genome on Virus Replication and Drug Resistance - Implications for Novel Antiviral Strategies</p>

    <p>          Tacke, F., Manns, M., and Trautwein, C.</p>

    <p>          Current Medicinal Chemistry <b>2004</b>.  11(20): 2667-2677</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.         6461   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inhibition of herpesvirus-6B RNA replication by short interference RNAs</p>

    <p>          Yoon, JS, Kim, SH, Shin, MC, Hong, SK, Jung, YT, Khang, IG, Shin, WS, Kim, CC, and Paik, SY</p>

    <p>          J Biochem Mol Biol <b>2004</b>.  37(3): 383-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469723&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469723&amp;dopt=abstract</a> </p><br />

    <p>8.         6462   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Peginterferon Alfa-2b Plus Ribavirin Compared With Interferon Alfa-2b Plus Ribavirin for Treatment of Hiv/Hcv Co-Infected Patients</p>

    <p>          Laguno, M. <i>et al.</i></p>

    <p>          Aids <b>2004</b>.  18(13): F27-F36</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>9.         6463   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Pegylated interferon alpha-2a and -2b in combination with ribavirin in the treatment of chronic hepatitis C: a systematic review and economic evaluation</p>

    <p>          Shepherd, J, Brodin, H, Cave, C, Waugh, N, Price, A, and Gabbay, J</p>

    <p>          Health Technol Assess <b>2004</b>.  8(39): 1-140</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15461877&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15461877&amp;dopt=abstract</a> </p><br />

    <p>10.       6464   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inhibition of hepatitis viral replication by siRNA</p>

    <p>          Wu, J and Nandamuri, KM</p>

    <p>          Expert Opin Biol Ther <b>2004</b>.  4(10): 1649-59</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15461576&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15461576&amp;dopt=abstract</a> </p><br />

    <p>11.       6465   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Standard Treatment of Chronic Hepatitis B</p>

    <p>          Manns, M. <i>et al.</i></p>

    <p>          Zeitschrift Fur Gastroenterologie <b>2004</b>.  42(8): 687-691</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>12.       6466   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Structure-activity relationship of heterobase-modified 2&#39;-C-methyl ribonucleosides as inhibitors of hepatitis C virus RNA replication</p>

    <p>          Eldrup, AB, Prhavc, M, Brooks, J, Bhat, B, Prakash, TP, Song, Q, Bera, S, Bhat, N, Dande, P, Cook, PD, Bennett, CF, Carroll, SS, Ball, RG, Bosserman, M, Burlein, C, Colwell, LF, Fay, JF, Flores, OA, Getty, K, LaFemina, RL, Leone, J, MacCoss, M, McMasters, DR, Tomassini, JE, Von, Langen D, Wolanski, B, and Olsen, DB</p>

    <p>          J Med Chem <b>2004</b>.  47(21): 5284-97</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456273&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456273&amp;dopt=abstract</a> </p><br />

    <p>13.       6467   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Synthesis of benzodithiol-2-yl-substituted nucleoside derivatives as lead compounds having anti-bovine viral diarrhea virus activity</p>

    <p>          Seio, K, Sasaki, T, Yanagida, K, Baba, M, and Sekine, M</p>

    <p>          J Med Chem <b>2004</b>.  47(21): 5265-75</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456271&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456271&amp;dopt=abstract</a> </p><br />

    <p>14.       6468   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Stereoselective synthesis of sugar-modified enyne analogues of adenosine and uridine. Interaction with S-adenosyl-L-homocysteine hydrolase and antiviral and cytotoxic effects</p>

    <p>          Wnuk, SF, Lewandowska, E, Sacasa, PR, Crain, LN, Zhang, J, Borchardt, RT, and De, Clercq E</p>

    <p>          J Med Chem <b>2004</b>.  47(21): 5251-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456269&amp;dopt=abstract</a> </p><br />

    <p>15.       6469   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Small molecules blocking the entry of severe acute respiratory syndrome coronavirus into host cells</p>

    <p>          Yi, L, Li, Z, Yuan, K, Qu, X, Chen, J, Wang, G, Zhang, H, Luo, H, Zhu, L, Jiang, P, Chen, L, Shen, Y, Luo, M, Zuo, G, Hu, J, Duan, D, Nie, Y, Shi, X, Wang, W, Han, Y, Li, T, Liu, Y, Ding, M, Deng, H, and Xu, X</p>

    <p>          J Virol <b>2004</b>.  78(20): 11334-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452254&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452254&amp;dopt=abstract</a> </p><br />

    <p>16.       6470   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          In vitro evaluation of cyanovirin-N antiviral activity, by use of lentiviral vectors pseudotyped with filovirus envelope glycoproteins</p>

    <p>          Barrientos, Laura G, Lasala, Fatima, Otero, Joaquin R, Sanchez, Anthony, and Delgado, Rafael</p>

    <p>          Journal of Infectious Diseases <b>2004</b>.  189(8): 1440-1443</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>17.       6471   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Structural and virological studies of the stages of virus replication that are affected by antirhinovirus compounds</p>

    <p>          Zhang, Y, Simpson, AA, Ledford, RM, Bator, CM, Chakravarty, S, Skochko, GA, Demenczuk, TM, Watanyar, A, Pevear, DC, and Rossmann, MG</p>

    <p>          J Virol <b>2004</b>.  78(20): 11061-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452226&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452226&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.       6472   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Influenza B virus BM2 protein is a crucial component for incorporation of viral ribonucleoprotein complex into virions during virus assembly</p>

    <p>          Imai, M, Watanabe, S, Ninomiya, A, Obuchi, M, and Odagiri, T</p>

    <p>          J Virol <b>2004</b>.  78(20): 11007-15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452221&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452221&amp;dopt=abstract</a> </p><br />

    <p>19.       6473   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Induction of interferon-stimulated gene expression and antiviral responses require protein deacetylase activity</p>

    <p>          Chang, Hao-Ming, Paulson, Matthew, Holko, Michelle, Rice, Charles M, Williams, Bryan RG, Marie, Isabelle, and Levy, David E</p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2004</b>.  101(26): 9578-9583</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>20.       6474   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Application of real-time PCR for testing antiviral compounds against Lassa virus, SARS coronavirus and Ebola virus in vitro</p>

    <p>          Gunther, S, Asper, M, Roser, C, Luna, LK, Drosten, C, Becker-Ziaja, B, Borowski, P, Chen, HM, and Hosmane, RS</p>

    <p>          Antiviral Res <b>2004</b>.  63(3): 209-15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451189&amp;dopt=abstract</a> </p><br />

    <p>21.       6475   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inhibition of cytomegalovirus infection by lactoferrin in vitro and in vivo</p>

    <p>          Beljaars, L, Van Der Strate, BW, Bakker, HI, Reker-Smit, C,  Van Loenen-Weemaes, AM, Wiegmans, FC, Harmsen, MC, Molema, G, and Meijer, DK</p>

    <p>          Antiviral Res <b>2004</b>.  63(3): 197-208</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451188&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451188&amp;dopt=abstract</a> </p><br />

    <p>22.       6476   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          In vitro generation and characterisation of an influenza B variant with reduced sensitivity to neuraminidase inhibitors</p>

    <p>          Cheam, AL, Barr, IG, Hampson, AW, Mosse, J, and Hurt, AC</p>

    <p>          Antiviral Res <b>2004</b>.  63(3): 177-81</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451185&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451185&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.       6477   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Full Resistance of Herpes Simplex Virus Type 1-Infected Primary Human Cells to Alpha Interferon Requires Both the Us11 and Gamma(1)34.5 Gene Products</p>

    <p>          Mulvey, M., Camarena, V., and Mohr, I.</p>

    <p>          Journal of Virology <b>2004</b>.  78(18): 10193-10196</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       6478   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Lactoferrin inhibits human papillomavirus binding and uptake in vitro</p>

    <p>          Drobni, P, Naslund, J, and Evander, M</p>

    <p>          Antiviral Res <b>2004</b>.  64(1): 63-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451180&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451180&amp;dopt=abstract</a> </p><br />

    <p>25.       6479   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Mycophenolic acid inhibits avian reovirus replication</p>

    <p>          Robertson, CM, Hermann, LL, and Coombs, KM</p>

    <p>          Antiviral Res <b>2004</b>.  64(1): 55-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451179&amp;dopt=abstract</a> </p><br />

    <p>26.       6480   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inactivation of adenovirus types 5 and 6 by Virkon((R)) S</p>

    <p>          McCormick, L and Maheshwari, G</p>

    <p>          Antiviral Res <b>2004</b>.  64(1): 27-33</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451176&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451176&amp;dopt=abstract</a> </p><br />

    <p>27.       6481   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Rhinovirus 3C protease precursors 3CD and 3CD&#39; localize to the nuclei of infected cells</p>

    <p>          Amineva, SP, Aminev, AG, Palmenberg, AC, and Gern, JE</p>

    <p>          J Gen Virol <b>2004</b>.  85(Pt 10): 2969-79</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15448360&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15448360&amp;dopt=abstract</a> </p><br />

    <p>28.       6482   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          5-(1-Substituted) Alkyl Pyrimidine Nucleosides as Antiviral (Herpes) Agents</p>

    <p>          Kumar, R.</p>

    <p>          Current Medicinal Chemistry <b>2004</b>.  11(20): 2749-2766</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>29.       6483   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inhibition of infectious human herpesvirus 8 production by gamma interferon and alpha interferon in BCBL-1 cells</p>

    <p>          Pozharskaya, VP, Weakland, LL, and Offermann, MK</p>

    <p>          J Gen Virol <b>2004</b>.  85(Pt 10): 2779-87</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15448338&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15448338&amp;dopt=abstract</a> </p><br />

    <p>30.       6484   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Novel Antiviral Fucoidan From Sporophyll of Undaria Pinnatifida (Mekabu)</p>

    <p>          Lee, J. <i>et al.</i></p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2004</b>.  52(9): 1091-1094</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       6485   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Antiviral furanose-type bicyclic carbohydrates</p>

    <p>          Sas, Benedikt, Van Hemel, Johan, Vandenkerckhove, Jan, Peys, Eric, Van Der Eycken, Johan, and Van Hoof, Steven</p>

    <p>          PATENT:  WO <b>2004080410</b>  ISSUE DATE:  20040923</p>

    <p>          APPLICATION: 2004  PP: 28 pp.</p>

    <p>          ASSIGNEE:  (Kemin Pharma Europe B.V.B.A., USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>32.       6486   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Antiviral properties and cytotoxic activity of platinum(II) complexes with 1,10-phenanthrolines and acyclovir or penciclovir</p>

    <p>          Margiotta, Nicola, Bergamo, Alberta, Sava, Gianni, Padovano, Giacomo, De Clercq, Erik, and Natile, Giovanni</p>

    <p>          Journal of Inorganic Biochemistry <b>2004</b>.  98(8): 1385-1390</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>33.       6487   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Delta-9 tetrahydrocannabinol (THC) inhibits lytic replication of gamma oncogenic herpesviruses in vitro</p>

    <p>          Medveczky, MM, Sherwood, TA, Klein, TW, Friedman, H, and Medveczky, PG</p>

    <p>          BMC Med <b>2004</b>.  2(1): 34</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369590&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369590&amp;dopt=abstract</a> </p><br />

    <p>34.       6488   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inhibitors of 3C cysteine proteinases from Picornaviridae</p>

    <p>          Lall, Manjinder S, Jain, Rajendra P, and Vederas, John C</p>

    <p>          Current Topics in Medicinal Chemistry (Sharjah, United Arab Emirates) <b>2004</b>.  4(12): 1239-1253</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>35.       6489   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Inhibition of Sars by Peptides of the Coronavirus Spike Protein</p>

    <p>          Suriawinata, A.</p>

    <p>          Laboratory Investigation <b>2004</b>.  84(9): 1083</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>36.       6490   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          In vitro activity of expanded-spectrum pyridazinyl oxime ethers related to pirodavir: Novel capsid-binding inhibitors with potent antipicornavirus activity</p>

    <p>          Barnard, DL, Hubbard, VD, Smee, DF, Sidwell, RW, Watson, KGW, Tucker, SPT, and Reece, PAR</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(5): 1766-1772</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>37.       6491   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Influenza: Emergence and Control</p>

    <p>          Lipatov, A. <i>et al.</i></p>

    <p>          Journal of Virology <b>2004</b>.  78(17): 8951-8959</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>38.       6492   DMID-LS-77; WOS-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Antiviral Properties and Cytotoxic Activity of Platinum(Ii) Complexes With 1,10-Phenanthrolines and Acyclovir or Penciclovir</p>

    <p>          Nicola, M. <i>et al.</i></p>

    <p>          Journal of Inorganic Biochemistry <b>2004</b>.  98(8): 1385-1390</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>39.       6493   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Oligonucleotide chip for detection of Lamivudine-resistant hepatitis B virus</p>

    <p>          Jang, H, Cho, M, Heo, J, Kim, H, Jun, H, Shin, W, Cho, B, Park, H, and Kim, C</p>

    <p>          J Clin Microbiol <b>2004</b>.  42(9): 4181-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365009&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365009&amp;dopt=abstract</a> </p><br />

    <p>40.       6494   DMID-LS-77; PUBMED-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Photoinactivation of Hepatitis A Virus by Synthetic Porphyrins</p>

    <p>          Casteel, MJ, Jayaraj, K, Gold, A, Ball, LM, and Sobsey, MD</p>

    <p>          Photochem Photobiol <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15362943&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15362943&amp;dopt=abstract</a> </p><br />

    <p>41.       6495   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Severe acute respiratory syndrome (SARS) and SARS coronavirus</p>

    <p>          Taguchi, Fumihiro and Tashiro, Masato</p>

    <p>          Kagaku to Seibutsu <b>2004</b>.  42(8): 546-552</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>42.       6496   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Susceptibility to SARS coronavirus S protein-driven infection correlates with expression of angiotensin converting enzyme 2 and infection can be blocked by soluble receptor</p>

    <p>          Hofmann, Heike, Geier, Martina, Marzi, Andrea, Krumbiegel, Mandy, Peipp, Matthias, Fey, Georg H, Gramberg, Thomas, and Poehlmann, Stefan</p>

    <p>          Biochemical and Biophysical Research Communications <b>2004</b>.  319(4): 1216-1221</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>43.       6497   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          The impact of HIV antiviral therapy on human papillomavirus (HPV) infections and HPV-related diseases</p>

    <p>          Heard, Isabelle, Palefsky, Joel M, and Kazatchkine, Michel D</p>

    <p>          Antiviral Therapy <b>2004</b>.  9(1): 13-22</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>44.       6498   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Infectious diseases. Part 4. Infections due to human immunodeficiency virus (HIV) and hepatitis-C virus (HCV)</p>

    <p>          Stock, Ingo</p>

    <p>          PZ Prisma <b>2004</b>.  11(3): 188-198</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>45.       6499   DMID-LS-77; SCIFINDER-DMID-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of triazolyl nucleoside derivatives as Hepatitis C antiviral agents</p>

    <p>          Hendricks, Robert Than, Humphreys, Eric Roy, Martin, Joseph Armstrong, Prince, Anthony, and Sarma, Keshab</p>

    <p>          PATENT:  WO <b>2004052905</b>  ISSUE DATE:  20040624</p>

    <p>          APPLICATION: 2003  PP: 40 pp.</p>

    <p>          ASSIGNEE:  (F. Hoffmann-La Roche Ag, Switz.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
