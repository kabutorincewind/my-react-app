

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-07-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ly8RnQ2+Iv5GSuXSb9ACfMOBCTFR7k+CxV6+1vGpjQwBShyxRakZ7doxICX0jZea0JdFAunozGbyIfld5k/kRut/UkJm0lErSeXuKxdvC4etTnkD6B1U/bqVD0M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="398FD466" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  July 6 - July 19, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22721712">Synthesis and Evaluation of Novel 3a,9a-Dihydro-1-ethoxycarbonyl-1-cyclopenteno[5,4-B]benzopyran-4-ones as Antifungal Agents.</a> Goel, R., V. Sharma, A. Budhiraja, and M.P. Ishar. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4665-4667. PMID[22721712].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22800680">Antidiabetic, Anti-oxidant and Antimicrobial Activities of Fadogia ancylantha Extracts from Malawi.</a> Nyirenda, K.K., J.D. Saka, D. Naidoo, V.J. Maharaj, and C.J. Muller. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[22800680].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22704888">Total Synthesis and Antifungal Activity of (2S,3R)-2-Aminododecan-3-ol.</a> Vijai Kumar Reddy, T., B.L. Prabhavathi Devi, R.B. Prasad, M. Poornima, and C. Ganesh Kumar. Bioorganic &amp; Medicinal Chemistry letters, 2012. 22(14): p. 4678-4680. PMID[22704888].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22733761">Structural Basis of Inhibition of Mycobacterium Tuberculosis DprE1 by Benzothiazinone Inhibitors.</a> Batt, S.M., T. Jabeen, V. Bhowruth, L. Quill, P.A. Lund, L. Eggeling, L.J. Alderwick, K. Futterer, and G.S. Besra. Proceedings of the National Academy of Sciences of the United States of America, 2012. 109(28): p. 11354-11359. PMID[22733761].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22704656">Analogs</a> <a href="http://www.ncbi.nlm.nih.gov/pubmed/22773252">of N&#39;-Hydroxy-N-(4H,5H-naphtho[1,2-D]thiazol-2-yl)methanimidamide Inhibit Mycobacterium tuberculosis Methionine Aminopeptidases.</a> Bhat, S., O. Olaleye, K.J. Meyer, W. Shi, Y. Zhang, and J.O. Liu. Bioorganic &amp; Medicinal Chemistry, 2012. 20(14): p. 4507-4513. PMID[22704656]. [<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22773252">Tuberculosis: Finding a New Potential Antimycobacterium Derivative in a Aldehyde-arylhydrazone-oxoquinoline Series.</a> da, C.S.F., H.C. Castro, M.C. Lourenco, P.A. Abreu, P.N. Batalha, A.C. Cunha, G.S. Carvalho, C.R. Rodrigues, C.A. Medeiros, S.D. Souza, V.F. Ferreira, and M.C. de Souza. Current Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[22773252].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22789014">Putative Mycobacterial Efflux Inhibitors from the Seeds of Aframomum melegueta.</a> Groblacher, B., V. Maier, O. Kunert, and F. Bucar. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>. PMID[22789014].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22726933">Synthesis and in Vitro Antitubercular Activity of 4-Aryl/Alkylsulfonylmethylcoumarins as Inhibitors of Mycobacterium Tuberculosis.</a> Jeyachandran, M., P. Ramesh, D. Sriram, P. Senthilkumar, and P. Yogeeswari. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4807-4809. PMID[22726933]. [<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22708897">Design, Synthesis, and Biological and Structural Evaluations of Novel HIV-1 Protease Inhibitors to Combat Drug Resistance.</a> Parai, M.K., D.J. Huggins, H. Cao, M.N. Nalam, A. Ali, C.A. Schiffer, B. Tidor, and T.M. Rana. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22708897]. [<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22778419">Eradication of Bacterial Persisters with Antibiotic-generated Hydroxyl Radicals.</a> Schmidt Grant, S., B.B. Kaufmann, N.S. Chand, N. Haseley, and D.T. Hung. Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>. PMID[22778419].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22800603">Effect of Vitamin D(3) on Chemokine Expression in Pulmonary Tuberculosis.</a> Selvaraj, P., M. Harishankar, B. Singh, V.V. Banurekha, and M.S. Jawahar. Cytokine, 2012. <b>[Epub ahead of print]</b>. PMID[22800603].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22777190">Synthesis and Antibacterial Evaluation of a New Series of N-Alkyl-2-alkynyl/(E)-alkenyl-4-(1H)-quinolones.</a> Wube, A., J.D. Guzman, A. Hufner, C. Hochfellner, M. Blunder, R. Bauer, S. Gibbons, S. Bhakta, and F. Bucar. Molecules (Basel, Switzerland), 2012. 17(7): p. 8217-8240. PMID[22777190].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22645149">Novel Anti-inflammatory Activity of Epoxyazadiradione against Macrophage Migration Inhibitory Factor: Inhibition of Tautomerase and Proinflammatory Activities of Macrophage Migration Inhibitory Factor.</a> Alam, A., S. Haldar, H.V. Thulasiram, R. Kumar, M. Goyal, M.S. Iqbal, C. Pal, S. Dey, S. Bindu, S. Sarkar, U. Pal, N.C. Maiti, and U. Bandyopadhyay. The Journal of Biological Chemistry, 2012. 287(29): p. 24844-24861. PMID[22645149].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22781441">New Trifluoromethyl triazolopyrimidines as anti-Plasmodium falciparum Agents.</a> Boechat, N., L.C. Pinheiro, T.S. Silva, A.C. Aguiar, A.S. Carvalho, M.M. Bastos, C.C. Costa, S. Pinheiro, A.C. Pinto, J.S. Mendonca, K.D. Dutra, A.L. Valverde, O.A. Santos-Filho, I.P. Ceravolo, and A.U. Krettli. Molecules (Basel, Switzerland), 2012. 17(7): p. 8285-8302. PMID[22781441].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22783984">Optimization of 4-Aminoquinoline/Clotrimazole-based Hybrid Antimalarials: Further Structure-Activity Relationships, in Vivo Studies, and Preliminary Toxicity Profiling.</a> Gemma, S., C. Camodeca, S. Sanna Coccone, B.P. Joshi, M. Bernetti, V. Moretti, S. Brogi, M.C. Bonache, L. Savini, D. Taramelli, N. Basilico, S. Parapini, M. Rottmann, R. Brun, S. Lamponi, S. Caccia, G. Guiso, R. Summer, R. Martin, S. Saponara, B. Gorelli, E. Novellino, G. Campiani, and S. Butini. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22783984].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22609155">Antiparasitic Compounds from Cornus florida L. With Activities against Plasmodium falciparum and Leishmania tarentolae.</a> Graziose, R., P. Rojas-Silva, T. Rathinasabapathy, C. Dekock, M.H. Grace, A. Poulev, M. Ann Lila, P. Smith, and I. Raskin. Journal of Ethnopharmacology, 2012. 142(2): p. 456-461. PMID[22609155].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22727670">Synthesis and Antimalarial Activity of Calothrixins A and B, and Their N-Alkyl Derivatives.</a> Matsumoto, K., T. Choshi, M. Hourai, Y. Zamami, K. Sasaki, T. Abe, M. Ishikura, N. Hatae, T. Iwamura, S. Tohyama, J. Nobuhiro, and S. Hibino. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4762-4764. PMID[22727670].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22727641">Synthesis and Structure-Activity Relationship of Disubstituted Benzamides as a Novel Class of Antimalarial Agents.</a> Mitachi, K., Y.G. Salinas, M. Connelly, N. Jensen, T. Ling, and F. Rivas. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4536-4539. PMID[22727641].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22796506">Antimalarial Alkaloids from a Bhutanese Traditional Medicinal Plant Corydalis dubia.</a> Wangchuk, P., P.A. Keller, S.G. Pyne, A.C. Willis, and S. Kamchonwongpaisan. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[22796506].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p>
    
    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22782478">Effects of the Chinese Medicine Matrine on Experimental C. parvum Infection in Balb/C Mice and MDBK Cells.</a> Chen, F. and K. Huang. Parasitology Research, 2012. <b>[Epub ahead of print]</b>. PMID[22782478].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22781137">In Vitro anti-Toxoplasma Gondii Activity of Root Extract/Fractions of Eurycoma longifolia Jack.</a> Kavitha, N., R. Noordin, K.L. Chan, and S. Sreenivasan. BMC Complementary and Alternative Medicine, 2012. 12(1): p. 91. PMID[22781137].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22789504">Enhancement of Protective Immune Responses Induced by Toxoplasma gondii Dense Granule Antigen 7 (GRA7) against Toxoplasmosis in Mice Using a Prime-boost Vaccination Strategy.</a> Min, J., D. Qu, C. Li, X. Song, Q. Zhao, X.A. Li, Y. Yang, Q. Liu, S. He, and H. Zhou. Vaccine, 2012. <b>[Epub ahead of print]</b>. PMID[22789504].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0706-071912.</p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0000305295200011">Cyclohexadepsipeptides from the Filamentous Fungus Acremonium sp BCC 2629</a>. Bunyapaiboonsri, T., P. Vongvilai, P. Auncharoen, and M. Isaka. Helvetica Chimica Acta, 2012. 95(6): p. 963-972. ISI[000305295200011].</p>

    <p class="plaintext">[<b>WOS</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305295000017">Structure-Activity Relationships of Pyrrole hydrazones as New Anti-tuberculosis Agents.</a> Lessigiarska, I., I. Pajeva, P. Prodanova, M. Georgieva, and A. Bijev. Medicinal Chemistry, 2012. 8(3): p. 462-473. ISI[000305295000017].</p>

    <p class="plaintext">[<b>WOS</b>]. OI_0706-071912.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305503500005">Total Synthesis and Biological Evaluation of Transvalencin Z.</a> Nelson, K.M., C.E. Salomon, and C.C. Aldrich. Journal of Natural Products, 2012. 75(6): p. 1037-1043. ISI[000305503500005].</p>

    <p class="plaintext">[<b>WOS</b>]. OI_0706-071912.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
