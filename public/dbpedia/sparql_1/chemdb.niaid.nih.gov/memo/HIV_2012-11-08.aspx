

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-11-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OjBHdEDQCMdaQuz6ZbS1jjHL9wqdA/pYPODXZ5YC//MoBBfRHSSWBfyoWTY5jJCZS8fe6xO4GJJiDp6zk0jpN8BtA1YkEye5GOQtju5OX75HHHC95DQBrvlyjQY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CD51B797" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: October 26 - November 8, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23000900">Codon-usage-based Inhibition of HIV Protein Synthesis by Human Schlafen 11.</a> Li, M., E. Kao, X. Gao, H. Sandig, K. Limmer, M. Pavon-Eternod, T.E. Jones, S. Landry, T. Pan, M.D. Weitzman, and M. David. Nature, 2012. 491(7422): p. 125-128. PMID[23000900].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23128354">Inhibition of HIV-1 Replication by RNA with a microRNA-like Function.</a> Kato, K., T. Senoki, and H.</p>

    <p class="plaintext">Takaku. International Journal of Molecular Medicine, 2012. <b>[Epub ahead of print]</b> PMID[23128354].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">3. <a href="file:///C:/AppData/Local/Temp/Caught%20in%20Translation:%20Innate%20Restriction%20of%20HIV%20mRNA%20Translation%20by%20a%20Schlafen%20Family%20Protein">Caught in Translation: Innate Restriction of HIV mRNA Translation by a Schlafen Family Protein.</a></p>

    <p class="plaintext">Jakobsen, M.R., T.H. Mogensen, and S.R. Paludan. Cell Research, 2012. <b>[Epub ahead of print];</b></p>

    <p class="plaintext">PMID[23128674].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23111169">Co-expression of HIV-1 Virus-like Particles and Granulocyte-macrophage Colony Stimulating Factor</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23111169">by Geo-D03 DNA Vaccine.</a> Hellerstein, M., Y. Xu, T. Marino, S. Lu, H. Yi, E.R. Wright, and H.L.</p>

    <p class="plaintext">Robinson. Human Vaccines &amp; Immunotherapeutics, 2012. 8(11): p. PMID[23111169].</p>

    <p class="plaintext"><b>[PubMed].</b>  HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23109186">Partial Protection against Multiple RT-SHIV162p3 Vaginal Challenge of Rhesus macaques by a</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23109186">Silicone Elastomer Vaginal Ring Releasing the NNTRI MC1220.</a>Fetherston, S.M., L. Geer, R.S. Veazey,</p>

    <p class="plaintext">L.Goldman, D.J. Murphy, T.J. Ketas, P.J. Klasse, S. Blois, P. La Colla, J.P. Moore, and R.K. Malcolm.</p>

    <p class="plaintext">The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print];</b> PMID[23109186].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23128501">New Anthraquinone Derivatives as Inhibitors of the HIV-1 Reverse Transcriptase-associated</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23128501">Ribonuclease H Function.</a> Esposito, F., A. Corona, L. Zinzula, T. Kharlamova, and E.</p>

    <p class="plaintext">Tramontano. Chemotherapy, 2012. 58(4): p. 299-307. PMID[23128501].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23010273">Anti-HIV-1 Activity of Resveratrol Derivatives and Synergistic Inhibition of HIV-1 by the</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23010273">Combination of Resveratrol and Decitabine.</a>Clouser, C.L., J. Chauhan, M.A. Bess, J.L. Oploo, D. Zhou,</p>

    <p class="plaintext">S. Dimick-Gray, L.M. Mansky, and S.E. Patterson. Bioorganic &amp; Medicinal Chemistry Letters, 2012.</p>

    <p class="plaintext">22(21): p. 6642-6646. PMID[23010273].</p>

    <p class="plaintext">[PubMed]. HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22933279">Enhanced Recognition and Neutralization of HIV-1 by Antibody-derived CCR5-Mimetic Peptide</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/22933279">Variants.</a> Chiang, J.J., M.R. Gardner, B.D. Quinlan, T. Dorfman, H. Choe, and M. Farzan. Journal of</p>

    <p class="plaintext">Virology, 2012. 86(22): p. 12417-12421. PMID[22933279].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23121263">DNA Polymerase Beta Gap-filling Translesion DNA Synthesis.</a> Chary, P., W.A. Beard, S.H. Wilson,</p>

    <p class="plaintext">and R.S. Lloyd. Chemical Research in Toxicology, 2012. <b>[Epub ahead of print];</b> PMID[23121263].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22728110">A New Fluorometric Assay for the Study of DNA-Binding and 3&#39;-Processing Activities of Retroviral</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/22728110">Integrases and Its Use for Screening of HIV-1 Integrase Inhibitors.</a> Anisenko, A., J. Agapkina, T.</p>

    <p class="plaintext">Zatsepin, D. Yanvarev, and M. Gottikh. Biochimie, 2012. 94(11): p. 2382-2390. PMID[22728110].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23121465">A Molecular Dynamics Investigation on a Series of HIV Protease Inhibitors: Assessing the</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23121465">Performance of MM-PBSA and MM-GBSA Approaches.</a>Srivastava, H.K. and G.N. Sastry. Journal of</p>

    <p class="plaintext">Chemical Information and Modeling, 2012. <b>[Epub ahead of print];</b> PMID[23121465].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23127567">Nerium oleander Derived Cardiac Glycoside Oleandrin Is a Novel Inhibitor of HIV Infectivity.</a></p>

    <p class="plaintext">Singh, S., S. Shenoy, P.N. Nehete, P. Yang, B. Nehete, D. Fontenot, G. Yang, R.A. Newman, and K.J.</p>

    <p class="plaintext">Sastry. Fitoterapia, 2012. <b>[Epub ahead of print];</b> PMID[23127567].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23129652">Most Rhesus macaques Infected with the CCR5-Tropic SHIVAD8 Generate Cross-reactive</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23129652">Antibodies That Neutralize Multiple HIV-1 Strains.</a>Shingai, M., O.K. Donau, S.D. Schmidt, R. Gautam,</p>

    <p class="plaintext">R.J. Plishka, A. Buckler-White, R. Sadjadpour, W.R. Lee, C.C. Labranche, D.C. Montefiori, J.R.</p>

    <p class="plaintext">Mascola, Y. Nishimura, and M.A. Martin. Proceedings of the National Academy of Sciences of the</p>

    <p class="plaintext">United States of America, 2012. <b>[Epub ahead of print];</b> PMID[23129652].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23093045">Design of Highly Potent HIV Fusion Inhibitors Based on Artificial Peptide Sequences.</a> Shi, W., L.</p>

    <p class="plaintext">Cai, L. Lu, C. Wang, K. Wang, L. Xu, S. Zhang, H. Han, X. Jiang, B. Zheng, S. Jiang, and K. Liu.</p>

    <p class="plaintext">Chemical Communications, 2012. 48(94): p. 11579-11581. PMID[23093045].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22811299">Pharmacodynamic and Antiretroviral Activities of Combination Nanoformulated Antiretrovirals in</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/22811299">HIV-1-Infected Human Peripheral Blood Lymphocyte-reconstituted Mice.</a> Roy, U., J. McMillan, Y.</p>

    <p class="plaintext">Alnouti, N. Gautum, N. Smith, S. Balkundi, P. Dash, S. Gorantla, A. Martinez-Skinner, J. Meza, G.</p>

    <p class="plaintext">Kanmogne, S. Swindells, S.M. Cohen, R.L. Mosley, L. Poluektova, and H.E. Gendelman. The Journal of</p>

    <p class="plaintext">Infectious Diseases, 2012. 206(10): p. 1577-1588. PMID[22811299].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23110726">Identification of Novel CDK9 and Cyclin T1-Associated Protein Complexes (CCAPS) Whose siRNA</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23110726">Depletion Enhances HIV-1 Tat Function.</a>Ramakrishnan, R., H. Liu, H. Donahue, A. Malovannaya, J.</p>

    <p class="plaintext">Qin, and A.P. Rice. Retrovirology, 2012. 9(1): p. 90. PMID[23110726].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23119100">Slit2n/Robo1 Inhibit HIV-GO120-Induced Migration and Podosome Formation in Immature</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23119100">Dendritic Cells by Sequestering LSP1 and WASP.</a>Prasad, A., P.M. Kuzontkoski, A. Shrivastava, W.</p>

    <p class="plaintext">Zhu, D.Y. Li, and J.E. Groopman. PLoS One, 2012. 7(10): p. e48854. PMID[23119100].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22964245">Safety and Pharmacokinetics of Intravaginal Rings Delivering Tenofovir in Pig-Tailed Macaques.</a></p>

    <p class="plaintext">Moss, J.A., A.M. Malone, T.J. Smith, I. Butkyavichene, C. Cortez, J. Gilman, S. Kennedy, E. Kopin, C.</p>

    <p class="plaintext">Nguyen, P. Sinha, R.M. Hendry, P. Guenthner, A. Holder, A. Martin, J. McNicholl, J. Mitchell, C.P. Pau,</p>

    <p class="plaintext">P. Srinivasan, J.M. Smith, and M.M. Baum. Antimicrobial Agents and Chemotherapy, 2012. 56(11): p.</p>

    <p class="plaintext">5952-5960. PMID[22964245].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23022280">Structure-activity Relationship Study of Pyrimido[1,2-C][1,3]benzothiazin-6-imine Derivatives for</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23022280">Potent anti-HIV Agents.</a> Mizuhara, T., S. Oishi, H. Ohno, K. Shimura, M. Matsuoka, and N. Fujii.</p>

    <p class="plaintext">Bioorganic &amp; Medicinal Chemistry, 2012. 20(21): p. 6434-6441. PMID[23022280].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23111849">Pharmacokinetics and Efficacy of a Vaginally Administered Maraviroc Gel in Rhesus macaques..</a></p>

    <p class="plaintext">Malcolm, R.K., C.J. Forbes, L. Geer, R.S. Veazey, L. Goldman, P. Johan Klasse, and J.P. Moore. The</p>

    <p class="plaintext">Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>: p. PMID[23111849].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1026-110812.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308897500007">Aurone Constituents from the Flowers of Rosa rugosa and Their Biological Activities.</a> Gao, X.M.,</p>

    <p class="plaintext">L.Y. Yang, L.D. Shu, Y.Q. Shen, Y.J. Zhang, and Q.F. Hu. Heterocycles, 2012. 85(8): p. 1925-1931.</p>

    <p class="plaintext">ISI[000308897500007].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308977000017">Human Milk Oligosaccharide Concentration and Risk of Postnatal Transmission of HIV through</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308977000017">Breastfeeding.</a> Bode, L., L. Kuhn, H.Y. Kim, L. Hsiao, C. Nissan, M. Sinkala, C. Kankasa, M. Mwiya,</p>

    <p class="plaintext">D.M. Thea, and G.M. Aldrovandi. American Journal of Clinical Nutrition, 2012. 96(4): p. 831-839.</p>

    <p class="plaintext">ISI[000308977000017].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308959800011.">A Large-scale Association Study for Nanoparticle C60 Uncovers Mechanisms of Nanotoxicity</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308959800011.">Disrupting the Native Conformations of DNA/RNA.</a>Xu, X., X. Wang, Y. Li, Y.H. Wang, and L. Yang.</p>

    <p class="plaintext">Nucleic Acids Research, 2012. 40(16): p. 7622-7632. ISI[000308959800011].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307908600061">Compared to Subcutaneous Tenofovir, Oral Tenofovir Disoproxyl Fumarate Administration</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307908600061">Preferentially Concentrates the Drug into Gut-associated Lymphoid Cells in Simian Immunodeficiency</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307908600061">Virus-Infected Macaques.</a> Van Rompay, K.K.A., D. Babusis, Z. Abbott, Y.Z. Geng, K. Jayashankar, J.A.</p>

    <p class="plaintext">Johnson, J. Lipscomb, W. Heneine, K. Abel, and A.S. Ray. Antimicrobial Agents and Chemotherapy,</p>

    <p class="plaintext">2012. 56(9): p. 4980-4984. ISI[000307908600061].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308054900008">Bioactive Phenolic Compounds from the Egyptian Red Sea Seagrass Thalassodendron ciliatum.</a></p>

    <p class="plaintext">Hamdy, A.H.A., W.S.A. Mettwally, M. Abou El Fotouh, B. Rodriguez, A.I. El-Dewany, S.A.A. El-</p>

    <p class="plaintext">Toumy, and A.A. Hussein. Zeitschrift Fur Naturforschung Section C-a Journal of Biosciences, 2012.</p>

    <p class="plaintext">67(5-6): p. 291-296. ISI[000308054900008].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1026-110812.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120927&amp;CC=WO&amp;NR=2012126833A1&amp;KC=A1">New Conjugated Molecules Comprising a Peptide Derived from the CD4 Receptor Coupled to a Polyanionic Polypeptide for the Treatment of AIDS.</a> Baleux, F., H. Lortat-Jacob, and D. Bonnaffe. Patent. 2012. 2012-EP54674 2012126833: 57pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121017&amp;CC=EP&amp;NR=2511273A1&amp;KC=A1">Preparation of Phenylacetic acid Derivatives as Inhibitors of Viral Replication.</a> Chasset, S., F. Chevreuil, B. Ledoussal, and F. Le Strat. Patent. 2012. 2011-305458 2511273: 139pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121011&amp;CC=WO&amp;NR=2012137181A1&amp;KC=A1">Cyclic Compounds as Inhibitors of Viral Replication, Their Process of Preparation and Their Therapeutic Use.</a> Chasset, S., F. Chevreuil, B. Ledoussal, F. Le Strat, and R. Benarous. Patent. 2012. 2012-IB51722 2012137181: 271pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121004&amp;CC=WO&amp;NR=2012135385A1&amp;KC=A1">Pegylated Peptides Binding Envelope Glycoproteins as Inhibitors of Retroviral Entry.</a> Francis, J.N., J.S. Redman, and M.S. Kay. Patent. 2012. 2012-US31015 2012135385: 74pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120919&amp;CC=CN&amp;NR=102675280A&amp;KC=A">Preparation of Indole Derivatives as HIV Reverse Transcriptase Inhibitors.</a> Jiang, B., C. Zhang, J. Li, D. Zhuang, and S. Chen. Patent. 2012. 2011-10056506 102675280: 16pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121004&amp;CC=WO&amp;NR=2012130915A1&amp;KC=A1">Spiroalkene carboxamide Derivatives and Their Use as Chemokine Receptor Modulators and Their Preparation.</a> Jorand-Lebrun, C., D. Swinnen, P. Gerber, and S. Kulkarni. Patent. 2012. 2012-EP55575 2012130915: 212pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120919&amp;CC=CN&amp;NR=102675305A&amp;KC=A">Imidazopyridines as CXCR4 Antagonists and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of Diseases.</a> Long, Y., B. Cao, X. Xie, and W. Wei. Patent. 2012. 2011-10056076 102675305: 54pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120919&amp;CC=CN&amp;NR=102675212A&amp;KC=A">Preparation of 2-(1H-Benzimidazol-2-ylthio)-N-phenyl-acetamide Derivatives as anti-HIV Agents.</a> Wang, Y., W. Yan, Q. Guo, and Y. He. Patent. 2012. 2012-10141458 102675212: 14pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1026-110812.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
