

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-365.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WHS5euXroyWdFthuVhb1KDkC7iaCzyFPHh2K2V/s0xhN2gjBsjO/wHVJPd68od2tvnd3AgZmDY7qiFd2Puj95GQmAb0BTAOMETVB4HvMDDk8QACu+62lmv+d7tc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="99E5158D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-365-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59874   HIV-LS-365; PUBMED-HIV-1/8/2007</p>

    <p class="memofmt1-2">          Synthesis of new derivatives of 2,3-dihydro-7-benzo[b]furanol with potential pharmacological activity</p>

    <p>          Kossakowski, J and Ostrowska, K</p>

    <p>          Acta Pol Pharm  <b>2006</b>.  63(4): 271-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17203863&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17203863&amp;dopt=abstract</a> </p><br />

    <p>2.     59875   HIV-LS-365; PUBMED-HIV-1/8/2007</p>

    <p class="memofmt1-2">          Synthesis of amino derivatives of 10-(diphenylmethylene)-4-azatricyclo[5.2.1.0(2,6)]dec-8-ene-3,5-dione as potential psychotropic and/or anti HIV agents</p>

    <p>          Kossakowski, J, Wojciechowska, A, and Koziol, AE</p>

    <p>          Acta Pol Pharm  <b>2006</b>.  63(4): 261-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17203861&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17203861&amp;dopt=abstract</a> </p><br />

    <p>3.     59876   HIV-LS-365; PUBMED-HIV-1/8/2007</p>

    <p class="memofmt1-2">          A potent and orally active HIV-1 integrase inhibitor</p>

    <p>          Egbertson, MS, Moritz, HM, Melamed, JY, Han, W, Perlow, DS, Kuo, MS, Embrey, M, Vacca, JP, Zrada, MM, Cortes, AR, Wallace, A, Leonard, Y, Hazuda, DJ, Miller, MD, Felock, PJ, Stillmock, KA, Witmer, MV, Schleif, W, Gabryelski, LJ, Moyer, G, Ellis, JD, Jin, L, Xu, W, Braun, MP, Kassahun, K, Tsou, NN, and Young, SD</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17194584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17194584&amp;dopt=abstract</a> </p><br />

    <p>4.     59877   HIV-LS-365; PUBMED-HIV-1/8/2007</p>

    <p class="memofmt1-2">          Rubrisandrins A and B, Lignans and Related Anti-HIV Compounds from Schisandra rubriflora</p>

    <p>          Chen, M, Kilgore, N, Lee, KH, and Chen, DF</p>

    <p>          J Nat Prod <b>2006</b>.  69(12): 1697-1701</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17190445&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17190445&amp;dopt=abstract</a> </p><br />

    <p>5.     59878   HIV-LS-365; PUBMED-HIV-1/8/2007</p>

    <p class="memofmt1-2">          Synthesis and HIV-1 integrase inhibitory activity of spiroundecane(ene) derivatives</p>

    <p>          Shults, EE, Semenova, EA, Johnson, AA, Bondarenko, SP, Bagryanskaya, IY, Gatilov, YV, Tolstikov, GA, and Pommier, Y</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17189685&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17189685&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59879   HIV-LS-365; SCIFINDER-HIV-1/2/2007</p>

    <p class="memofmt1-2">          Preparation of diketo acids with nucleobase scaffolds as anti-HIV replication inhibitors targeted at HIV integrase</p>

    <p>          Nair, Vasu, Chi, Guochen, and Uchil, Vinod R</p>

    <p>          PATENT:  US <b>2006172973</b>  ISSUE DATE:  20060803</p>

    <p>          APPLICATION: 2005-47229  PP: 33pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     59880   HIV-LS-365; SCIFINDER-HIV-1/2/2007</p>

    <p class="memofmt1-2">          Anti-viral compositions comprising heterocyclic substituted phenyl furans and related compounds</p>

    <p>          Jiang, Shibo, Debnath, Asim Kumar, and Lu, Hong</p>

    <p>          PATENT:  US <b>2006287319</b>  ISSUE DATE:  20061221</p>

    <p>          APPLICATION: 2006-55223  PP: 23pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     59881   HIV-LS-365; PUBMED-HIV-1/8/2007</p>

    <p class="memofmt1-2">          HIV-1 RT Nonnucleoside Inhibitors and Their Interaction with RT for Antiviral Drug Development</p>

    <p>          Zhou, Z, Lin, X, and Madura, JD</p>

    <p>          Infect Disord Drug Targets <b>2006</b>.  6(4): 391-413</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168804&amp;dopt=abstract</a> </p><br />

    <p>9.     59882   HIV-LS-365; PUBMED-HIV-1/8/2007</p>

    <p class="memofmt1-2">          Immune modulation and reconstitution of HIV-1-specific responses: novel approaches and strategies</p>

    <p>          Burton, CT, Mela, CM, Rosignoli, G, Westrop, SJ, Gotch, FM, and Imami, N</p>

    <p>          Curr Med Chem <b>2006</b>.  13(26): 3203-3211</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168707&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168707&amp;dopt=abstract</a> </p><br />

    <p>10.   59883   HIV-LS-365; SCIFINDER-HIV-1/2/2007</p>

    <p class="memofmt1-2">          Biopolymer-binding hexanucleotides for use as antivirals</p>

    <p>          Grohmann, Dina, Laufer, Sandra, Mescalchin, Alessandra, Sczakiel, Georg, Restle, Tobias, and Wuensche, Winfried</p>

    <p>          PATENT:  DE <b>102005022290</b>  ISSUE DATE: 20061116</p>

    <p>          APPLICATION: 2005  PP: 10pp.</p>

    <p>          ASSIGNEE:  (Universitaetsklinikum Schleswig-Holstein, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   59884   HIV-LS-365; SCIFINDER-HIV-1/2/2007</p>

    <p class="memofmt1-2">          Preparation of tricyclic naphthyridine derivatives, as HIV integrase inhibitors</p>

    <p>          Vacca, Joseph P, Wai, John S, Payne, Linda S, Isaacs, Richard CA, Han, Wei, Egbertson, Melissa, and Pracitto, Richard</p>

    <p>          PATENT:  WO <b>2006121831</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2006  PP: 122pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   59885   HIV-LS-365; SCIFINDER-HIV-1/2/2007</p>

    <p class="memofmt1-2">          Synthesis of 1-(alkoxymethyl)-5-benzyl-6-methyluracil as potential nonnucleoside HIV-1 RT inhibitors</p>

    <p>          Chen, Yanli, Guo, Ying, Yang, Hua, Wang, Xiaowei, and Liu, Junyi</p>

    <p>          Synthetic Communications <b>2006</b>.  36(19): 2913-2920</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   59886   HIV-LS-365; SCIFINDER-HIV-1/2/2007</p>

    <p class="memofmt1-2">          Preparation of pyrazolobenzoxazinylphenylmethanones as HIV viral replication inhibitors</p>

    <p>          Tahri, Abdellah, Hu, Lili, Surleraux, Dominique Louis Nestor Ghislain, and Wigerinck, Piet Tom Bert Paul</p>

    <p>          PATENT:  WO <b>2006108828</b>  ISSUE DATE:  20061019</p>

    <p>          APPLICATION: 2006  PP: 47pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   59887   HIV-LS-365; WOS-HIV-12/31/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV-1 activity of novel MKC-442 analogues containing alkenyl chains or reactive functionalities in the 6-benzyl group</p>

    <p>          Aly, YL, Pedersen, EB, La Colla, P, and Loddo, R</p>

    <p>          MONATSHEFTE FUR CHEMIE <b>2006</b>.  137(12): 1557-1570, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242660600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242660600008</a> </p><br />

    <p>15.   59888   HIV-LS-365; SCIFINDER-HIV-1/2/2007</p>

    <p class="memofmt1-2">          Anti-HIV composition comprising arsenic acid, meta-arsenite, and pharmaceutically acceptable salts</p>

    <p>          Yang, Yong Jin and Lee, Sang Bong</p>

    <p>          PATENT:  WO <b>2006104292</b>  ISSUE DATE:  20061005</p>

    <p>          APPLICATION: 2005  PP: 13pp.</p>

    <p>          ASSIGNEE:  (Komipharm International Co., Ltd. S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
