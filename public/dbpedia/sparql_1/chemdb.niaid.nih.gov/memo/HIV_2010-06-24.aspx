

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-06-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="94ne8fVT2DvOGwmfZjdKJAnVuLt0ZkXRj1i2T33p0KWFEpwBDvGc82xdPup7yAgKYJihgRw7/Idk4rYdf5njbpux3dVrK8S7JP8wDjEzAq1blD5IPaCEIt0hZ+s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="854CC36B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  June 11 - June 24, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20565056">Binding Characteristics of Small Molecules that Mimic Nucleocapsid Protein-induced Maturation of Stem-loop-1 of HIV-1 RNA.</a> Chung J, Ulyanov NB, Guilbert C, Mujeeb A, James TL. Biochemistry. 2010 Jun 21. [Epub ahead of print]PMID: 20565056</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20540707">Cosalane and Its Analogues: A Unique Class of Anti-HIV Agents.</a> Zhan P, Li Z, Liu X. Mini Rev Med Chem. 2010 Jun 14. [Epub ahead of print]PMID: 20540707</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20546571">Differential activity of candidate microbicides against early steps of HIV-1 infection upon complement virus opsonization.</a>Jenabian MA, Saidi H, Charpentier C, Bouhlal H, Schols D, Balzarini J, Bell TW, Vanham G, Belec L. AIDS Res Ther. 2010 Jun 14;7(1):16. [Epub ahead of print]PMID: 20546571</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20547183">Distribution of lignin-carbohydrate complex in plant kingdom and its functionality as alternative medicine.</a>Sakagami H, Kushida T, Oizumi T, Nakashima H, Makino T. Pharmacol Ther. 2010 Jun 11. [Epub ahead of print]PMID: 20547183</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20547794">Structure-Activity Analysis of Vinylogous Urea Inhibitors of Human Immunodeficiency Virus-Encoded Ribonuclease H.</a> Chung S, Wendeler M, Rausch JW, Beilhartz G, Gotte M, O&#39;Keefe BR, Bermingham A, Beutler JA, Liu S, Zhuang X, Le Grice SF. Antimicrob Agents Chemother. 2010 Jun 14. [Epub ahead of print]PMID: 20547794</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20527972">Diarylaniline Derivatives as a Distinct Class of HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Qin B, Jiang X, Lu H, Tian X, Barbault F, Huang L, Qian K, Chen CH, Huang R, Jiang S, Lee KH, Xie L. J Med Chem. 2010 Jun 9. [Epub ahead of print]PMID: 20527972</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20371284">Phytochemical and biological investigations of Elaeodendron schlechteranum.</a> Maregesi SM, Hermans N, Dhooghe L, Cimanga K, Ferreira D, Pannecouque C, Vanden Berghe DA, Cos P, Maes L, Vlietinck AJ, Apers S, Pieters L. J Ethnopharmacol. 2010 Jun 16;129(3):319-26. Epub 2010 Apr 3.PMID: 20371284</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278198000011">Synthesis of a small library of non-symmetric cyclic sulfamide HIV-1 protease inhibitors.</a>  Ax, A; Joshi, AA; Orrling, KM; Vrang, L; Samuelsson, B; Hallberg, A; Karlen, A; Larhed, M.  TETRAHEDRON, 2010; 66(23): 4049-4056.</p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278414800008">PhDD: A new pharmacophore-based de novo design method of drug-like molecules combined with assessment of synthetic accessibility.</a>  Huang, Q; Li, LL; Yang, SY. JOURNAL OF MOLECULAR GRAPHICS &amp; MODELLING, 2010; 28(8): 775-787.</p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271775102897">CHED 1307-Kinetic analysis of inhibitors of reverse transcriptases from HIV-1, MMLV, and AMV.</a>  Lin, T; Doughty, MB.  ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY, 2008; 235: 5190A.</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277967500019">Mannose-rich glycosylation patterns on HIV-1 subtype C gp120 and sensitivity to the lectins, Griffithsin, Cyanovirin-N and Scytovirin.</a>  Alexandre, KB; Gray, ES; Lambson, BE; Moore, PL; Choge, IA; Mlisana, K; Karim, SSA; McMahon, J; O&#39;Keefe, B; Chikwamba, R; Morris, L.  VIROLOGY, 2010; 402(1): 187-196.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278028100007">HIV-1 Integrase and Virus and Cell DNAs: Complex Formation and Perturbation by Inhibitors of Integration.</a>  Hobaika, Z; Zargarian, L; Maroun, RG; Mauffret, O; Burke, TR; Fermandjian, S. NEUROCHEMICAL RESEARCH, 2010; 35(6): 888-893.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278105400009">Chemistry and Structure-Activity Relationship of the Styrylquinoline-Type HIV Integrase Inhibitors.</a>  Mouscadet, JF; Desmaele, D.  MOLECULES, 2010; 15(5): 3048-3078.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275161600019">NOVEL THIAZOLYL THIAZOLYDIN-4-ONE DERIVATIVES AS HIV-1 REVERSE TRANSCRIPTASE INHIBITORS. POTENTIAL NOVEL DRUGS FOR THE TREATMENT OF AIDS.</a>  Athina, G; Eleni, P; Sofiko, S; Phaedra, E.  MEDICINAL CHEMISTRY RESEARCH, 2010; 19(Supp 1): S17.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277982600017">Small Molecular Compounds Inhibit HIV-1 Replication through Specifically Stabilizing APOBEC3G.</a>  Cen, S; Peng, ZG; Li, XY; Li, ZR; Ma, J; Wang, YM; Fan, B; You, XF; Wang, YP; Liu, F; Shao, RG; Zhao, LX; Yu, LY; Jiang, JD. JOURNAL OF BIOLOGICAL CHEMISTRY, 2010; 285(22): 16546-16552.</p>

    <p> </p>

    <p> </p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
