

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-333.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zHgPxhyP6LLaCzBvTE2YqJ5Z/ggaPikPvRtRb6PGf8OLGqvWCsIbjTwJf3MUfaOtyEmiT2ZchjicSeFrkgYgv7gIN5J8DoM6lKKjbdXOF+FQuxfFl+AkigfVRf8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE705536" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH OI-LS-333-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48008   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Screening of compounds for antimicrosporidial activity in vitro</p>

    <p>          Didier, Elizabeth S, Maddry, Joseph A, Kwong, Cecil D, Green, Linda C, Snowden, Karen F, and Shadduck, John A</p>

    <p>          Folia Parasitologica (Prague) <b>1998</b>.  45(2): 129-139</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     48009   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some 3,5-disubstituted-tetrahydro-2H-1,3,5-thiadiazine-2-thione derivatives</p>

    <p>          Moty, Samia GAbdel</p>

    <p>          Bulletin of Pharmaceutical Sciences, Assiut University <b>2005</b>.  28(1): 9-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     48010   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Synthesis of some thiazolidine derivatives of 1,4-benzoquinone as potential antimicrobial agents</p>

    <p>          Chaaban, I, Bekhit, AA, and Aboulmagd, Elsayed</p>

    <p>          Bulletin of Pharmaceutical Sciences, Assiut University <b>2005</b>.  28(1): 1-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     48011   OI-LS-333; PUBMED-OI-10/17/2005</p>

    <p class="memofmt1-2">          Identification of a new antitubercular drug candidate, SQ109, from a combinatorial library of 1,2-ethylenediamines</p>

    <p>          Protopopova, M, Hanrahan, C, Nikonenko, B, Samala, R, Chen, P, Gearhart, J, Einck, L, and Nacy, CA</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16172107&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16172107&amp;dopt=abstract</a> </p><br />

    <p>5.     48012   OI-LS-333; PUBMED-OI-10/17/2005</p>

    <p class="memofmt1-2">          Binding-site characterization and resistance to a class of non-nucleoside inhibitors of the HCV NS5B polymerase</p>

    <p>          Kukolj, G, McGibbon, GA, McKercher, G, Marquis, M, Lefebvre, S, Thauvette, L, Gauthier, J, Goulet, S, Poupart, MA, and Beaulieu, PL</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188890&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188890&amp;dopt=abstract</a> </p><br />

    <p>6.     48013   OI-LS-333; PUBMED-OI-10/17/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of novel (l)-alpha-amino acid methyl ester, heteroalkyl, and aryl substituted 1,4-naphthoquinone derivatives as antifungal and antibacterial agents</p>

    <p>          Tandon, VK, Yadav, DB, Singh, RV, Chaturvedi, AK, and Shukla, PK</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16202590&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16202590&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     48014   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of some new 2-(2-(p-chlorophenyl) benzimidazol-1-yl methyl)-5-substituted amino-[1,3,4]-thiadiazoles</p>

    <p>          Ayhan Kilcigil, Guelguen, Kus, Canan, Altanlar, Nurten, and Oezbey, Sueheyla</p>

    <p>          Turkish Journal of Chemistry <b>2005</b>.  29(2): 153-162</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     48015   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          New triorganotin(IV) derivatives of dipeptides as anti-inflammatory-antimicrobial agents</p>

    <p>          Nath, Mala, Pokharia, Sandeep, Eng, George, Song, Xueqing, and Kumar, Ashok</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(3): 289-298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48016   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          a-helical domain is essential for antimicrobial activity of high mobility group nucleosomal binding domain 2 (HMGN2)</p>

    <p>          Feng, Yun, Huang, Ning, Wu, Qi, Bao, Lang, and Wang, Bo-yao</p>

    <p>          Acta Pharmacologica Sinica <b>2005</b>.  26(9): 1087-1092</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48017   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Synthesis of analogs of amathamide A and their preliminary antimicrobial activity</p>

    <p>          Ramirez-Osuna, Moises, Chavez, Daniel, Hernandez, Lourdes, Molins, Elias, Somanathan, Ratnasamy, and Aguirre, Gerardo</p>

    <p>          Molecules <b>2005</b>.  10(1): 295-301</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   48018   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Preparation of pyrazines as protein kinase, especially pUL-97 kinase, inhibitors for treatment of infectious diseases, particularly human cytomegaloviral infections</p>

    <p>          Eikhoff, Jan Eike, Ashton, Mark Richard, Courtney, Stephen Martin, Yarnold, Christopher John, Varrone, Maurizio, Loke, Pui Leng, Herget, Thomas, Schwab, Wilfried, and Hafenbradl, Doris</p>

    <p>          PATENT:  WO <b>2005058876</b>  ISSUE DATE:  20050630</p>

    <p>          APPLICATION: 2004  PP: 139 pp.</p>

    <p>          ASSIGNEE:  (Axxima Pharmaceuticals A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   48019   OI-LS-333; WOS-OI-10/09/2005</p>

    <p class="memofmt1-2">          A cyclic PNA-based compound targeting domain IV of HCVIRES RNA inhibits in vitro IRES-dependent translation</p>

    <p>          Caldarelli, SA, Mehiri, M, Di, Giorgio A, Martin, A, Hantz, O, Zoulim, F, Terreux, R, Condom, R, and Patino, N</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(20): 5700-5709, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231933200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231933200002</a> </p><br />

    <p>13.   48020   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Synthesis of N4-substituted derivatives of (S)-1-[3-hydroxy-2-(phosphonomethoxy)propyl]cytosine (HPMPC, cidofovir, vistide)</p>

    <p>          Chalupova, Sarka and Holy, Antonin</p>

    <p>          Collection Symposium Series <b>2005</b>.  7(Chemistry of Nucleic Acid Components): 387-388</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>14.   48021   OI-LS-333; SIFINDER-OI-10/11/2005</p>

    <p class="memofmt1-2">          Preparation of phosphonated isoxazolidinyl nucleoside analogs with antiviral activity</p>

    <p>          Chiacchio, Ugo, Corsaro, Antonino, Iannazzo, Daniela, Macchi, Beatrice, Mastino, Antonio, Piperno, Anna, Rescifina, Antonio, Romeo, Giovanni, and Romeo, Roberto</p>

    <p>          PATENT:  WO <b>2005082896</b>  ISSUE DATE:  20050909</p>

    <p>          APPLICATION: 2005  PP: 29 pp.</p>

    <p>          ASSIGNEE:  (Universita&#39; Degli Studi di Catania, Italy and Universita&#39; Degli Studi di Messina)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   48022   OI-LS-333; WOS-OI-10/16/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity of a new series of fluorine-containing tertiary alcohols</p>

    <p>          da Costa, MRG, Curto, MJM, Furtado, OR, Feio, SS, and Roseiro, JC</p>

    <p>          RUSSIAN JOURNAL OF BIOORGANIC CHEMISTRY <b>2005</b>.  31(4): 398-400, 3</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231921800013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231921800013</a> </p><br />

    <p>16.   48023   OI-LS-333; WOS-OI-10/16/2005</p>

    <p class="memofmt1-2">          Direct and synergistic inhibition of hepatitis C virus replication by cyclosporina and mycophenolic acid</p>

    <p>          van der Laan, LJW, Henry, SD, Bartenschlager, R, Tilanus, HW, and Metselaar, HJ</p>

    <p>          LIVER TRANSPLANTATION <b>2005</b>.  11(7): C2-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230158500030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230158500030</a> </p><br />

    <p>17.   48024   OI-LS-333; WOS-OI-10/16/2005</p>

    <p class="memofmt1-2">          Profiles in practice series: Stewerds of drug discovery - Developing and maintaining the future drug candidates</p>

    <p>          Balogh, MR</p>

    <p>          LC GC EUROPE <b>2005</b>.  18(9): 466-469, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232075400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232075400003</a> </p><br />

    <p>18.   48025   OI-LS-333; WOS-OI-10/16/2005</p>

    <p class="memofmt1-2">          Correlation between isoniazid resistance and superoxide reactivity in Mycobacterium tuberculosis KatG</p>

    <p>          Ghiladi, RA, Medzihradszky, KF, Rusnak, FM, and de, Montellano PR</p>

    <p>          JOURNAL OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  127(38): 13428-13442, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232170800077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232170800077</a> </p><br />

    <p>19.   48026   OI-LS-333; WOS-OI-10/16/2005</p>

    <p class="memofmt1-2">          Antimycobacterial agents from selected Mexican medicinal plants</p>

    <p>          Rivero-Cruz, I, Acevedo, L, Guerrero, JA, Martinez, S, Bye, R, Pereda-Miranda, R, Franzblau, S, Timmermann, BN, and Mata, R</p>

    <p>          JOURNAL OF PHARMACY AND PHARMACOLOGY <b>2005</b>.  57(9): 1117-1126, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232172300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232172300007</a> </p><br />
    <br clear="all">

    <p>20.   48027   OI-LS-333; WOS-OI-10/16/2005</p>

    <p class="memofmt1-2">          Design, synthesis, and biochemical evaluation of 1,5,6,7-tetrahydro-6,7-dioxo-9-D-ribitylaminolumazines bearing alkyl phosphate substituents as inhibitors of lumazine synthase and riboflavin synthase</p>

    <p>          Cushman, M, Jin, GY, Illarionov, B, Fischer, M, Ladenstein, R, and Bacher, A</p>

    <p>          JOURNAL OF ORGANIC CHEMISTRY <b>2005</b>.  70(20): 8162-8170, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232166800045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232166800045</a> </p><br />

    <p>21.   48028   OI-LS-333; WOS-OI-10/16/2005</p>

    <p class="memofmt1-2">          Crystal structure and functional analysis of lipoamide dehydrogenase from Mycobacterium tuberculosis</p>

    <p>          Rajashankar, KR, Bryk, R, Kniewel, R, Buglino, JA, Nathan, CF, and Lima, CD</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(40): 33977-33983, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232229700035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232229700035</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
