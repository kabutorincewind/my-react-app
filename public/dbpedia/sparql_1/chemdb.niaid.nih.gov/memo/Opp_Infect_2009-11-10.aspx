

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-11-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hW+OnHXznuy3v1PcRuOsTtt7VQuaHVAghIcjdZVY4mi1Yutou7DYMeogyS47JQFhrND4uEP5yCRVZKw/ouIQp8j+oiOeunZrs0hopIxAu5SN5Ys+pR0ejVaZ/VE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C5C25A4B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">October 28, 2009 -November 10, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19893994?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">Anti-parasitic action and elimination of intracellular Toxoplasma gondii in the presence of novel thiosemicarbazone and its 4-thiazolidinone derivatives.</a></p>

    <p class="NoSpacing">Carvalho CS, de Melo EJ.</p>

    <p class="NoSpacing">Braz J Med Biol Res. 2009 Nov 6. pii: S0100-879X2009005000038. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19893994</p>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19884369?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=12">In-vitro and in-vivo activity of 1-hydroxy-2-alkyl-4(1H)quinolone derivatives against Toxoplasma gondii.</a></p>

    <p class="NoSpacing">Bajohr LL, Ma L, Platte C, Liesenfeld O, Tietze LF, Groß U, Bohne W.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Nov 2. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19884369</p>

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19874136?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=19">Synthesis and biological evaluation of biguanide and dihydrotriazine derivatives as potential inhibitors of dihydrofolate reductase of opportunistic microorganisms.</a></p>

    <p class="NoSpacing">Bag S, Tawari NR, Queener SF, Degani MS.</p>

    <p class="NoSpacing">J Enzyme Inhib Med Chem. 2009 Oct 29. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19874136</p>

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19501055?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=44">Drug discovery targeting epigenetic codes: the great potential of UHRF1, which links DNA methylation and histone modifications, as a drug target in cancers and toxoplasmosis.</a></p>

    <p class="NoSpacing">Unoki M, Brunet J, Mousli M.</p>

    <p class="NoSpacing">Biochem Pharmacol. 2009 Nov 15;78(10):1279-88. Epub 2009 Jun 6.</p>

    <p class="NoSpacing">PMID: 19501055</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19899102?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">Design, Synthesis, and Antifungal Activity of Novel Conformationally Restricted Triazole Derivatives.</a></p>

    <p class="NoSpacing">Wang W, Sheng C, Che X, Ji H, Miao Z, Yao J, Zhang W.</p>

    <p class="NoSpacing">Arch Pharm (Weinheim). 2009 Nov 6. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19899102</p>

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19882690?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=33">Synthesis, cytotoxicity by bioluminescence inhibition, antibacterial and antifungal activity of ([1,2,4]Triazolo[1,5-c]quinazolin-2-ylthio)carboxylic acid amides.</a></p>

    <p class="NoSpacing">Antipenko LN, Karpenko AV, Kovalenko SI, Katsev AM, Komarovska-Porokhnyavets EZ, Novikov VP.</p>

    <p class="NoSpacing">Arch Pharm (Weinheim). 2009 Nov;342(11):651-62.</p>

    <p class="NoSpacing">PMID: 19882690</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19879756?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=42">Synthesis, stereochemistry and antimicrobial studies of novel oxime ethers of aza/diazabicycles.</a></p>

    <p class="NoSpacing">Parthiban P, Aridoss G, Rathika P, Ramkumar V, Kabilan S.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Oct 29. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19879756</p> 

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19878645?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=46">Antifungal effect with apoptotic mechanism(s) of Styraxjaponoside C.</a></p>

    <p class="NoSpacing">Park C, Woo ER, Lee DG.</p>

    <p class="NoSpacing">Biochem Biophys Res Commun. 2009 Oct 28. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19878645</p> 

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19875509?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=53">In vitro interactions between primycin and different statins in their effects against some clinically important fungi.</a></p>

    <p class="NoSpacing">Nyilasi I, Kocsube S, Pesti M, Lukacs G, Papp T, Vagvolgyi C.</p>

    <p class="NoSpacing">J Med Microbiol. 2009 Oct 29. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19875509</p> 

    <p class="ListParagraph">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19879765?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=26">Antiprotozoal, anticancer and antimicrobial activities of dihydroartemisinin acetal dimers and monomers.</a></p>

    <p class="NoSpacing">Slade D, Galal AM, Gul W, Radwan MM, Ahmed SA, Khan SI, Tekwani BL, Jacob MR, Ross SA, Elsohly MA.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Dec 1;17(23):7949-57. Epub 2009 Oct 30.</p>

    <p class="NoSpacing">PMID: 19879765 [PubMed - in process]</p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p class="ListParagraph">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19022511?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Panning for chemical gold: marine bacteria as a source of new therapeutics.</a></p>

    <p class="NoSpacing">Williams PG.</p>

    <p class="NoSpacing">Trends Biotechnol. 2008 Nov 18. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19022511 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19005871?ordinalpos=10&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Efavirenz Mannich bases: Synthesis, anti-HIV and antitubercular activities.</a></p>

    <p class="NoSpacing">Sriram D, Banerjee D, Yogeeswari P.</p>

    <p class="NoSpacing">J Enzyme Inhib Med Chem. 2008 Nov 13:1. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19005871 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">13. Geng, CA, et al</p>

    <p class="NoSpacing">Swerilactones C and D, anti-HBV New Lactones from a Traditional Chinese Herb: Swertia mileensis</p>

    <p class="NoSpacing">ORGANIC LETTERS   2009 (Nov.) 11: 4838 - 4841</p> 

    <p class="ListParagraph">14. Ozcelik, B, et al</p>

    <p class="NoSpacing">Antiviral and antimicrobial activities of three sesquiterpene lactones from Centaurea solstitialis L. ssp solstitialis</p>

    <p class="NoSpacing">MICROBIOLOGICAL RESEARCH   2009 164: 545 - 552</p> 

    <p class="ListParagraph">15. Yenjai, C, et al.</p>

    <p class="NoSpacing">Structural Modification of 5,7-Dimethoxyflavone from Kaempferia parviflora and Biological Activities</p>

    <p class="NoSpacing">ARCHIVES OF PHARMACAL RESEARCH  2009 (Sept) 32: 1179 - 1184 PD</p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
