

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-07-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="U3Q/ry8ujYfOt7APnKaVSaTfYTS95kadALX8lDIL415JroTejJYxnedwOC0sojJVu1QXthWgkUCT8M69A3MojuU1NBgzr62Vb8POcWhe1boPmUJV8c6PmvKwi/U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="199E1163" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: July 17 - July 30, 2015</h1>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26092447">Design, Synthesis and Characterization of Dual Inhibitors against New Targets FaGg4 and HtdX of Mycobacterium tuberculosis.</a> Banerjee, D.R., R. Biswas, A.K. Das, and A. Basak. European Journal of Medicinal Chemistry, 2015. 100: p. 223-234. PMID[26092447].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0717-073015.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26112067">Recent Progress in the Drug Development of Coumarin Derivatives as Potent Antituberculosis Agents.</a> Keri, R.S., B.S. Sasidhar, B.M. Nagaraja, and M.A. Santos. European Journal of Medicinal Chemistry, 2015. 100: p. 257-269. PMID[26112067].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0717-073015.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26071857">Discovery of Antitubercular 2,4-Diphenyl-1H-imidazoles from Chemical Library Repositioning and Rational Design.</a> Pieroni, M., B. Wan, V. Zuliani, S.G. Franzblau, G. Costantino, and M. Rivara. European Journal of Medicinal Chemistry, 2015. 100: p. 44-49. PMID[26071857].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0717-073015.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26110337">Synthesis and Pharmacokinetic Evaluation of Siderophore Biosynthesis Inhibitors for Mycobacterium tuberculosis.</a> Nelson, K.M., K. Viswanathan, S. Dawadi, B.P. Duckworth, H.I. Boshoff, C.E. Barry, 3rd, and C.C. Aldrich. Journal of Medicinal Chemistry, 2015. 58(14): p. 5459-5475. PMID[26110337].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0717-073015.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26147490">Cytotoxic Homoisoflavones from the Bulbs of Bellevalia eigii.</a> Alali, F., T. El-Elimat, H. Albataineh, Q. Al-Balas, M. Al-Gharaibeh, J.O. Falkinham, 3rd, W.L. Chen, S.M. Swanson, and N.H. Oberlies. Journal of Natural Products, 2015. 78(7): p. 1708-1715. PMID[26147490].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0717-073015.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000356863700003">Furan Type Lignans with Antimycobacterial Activity.</a> Baquero, E., W. Quinones, S. Franzblau, F. Torres, R. Archbold, and F. Echeverri. Boletin Latinoamericano y del Caribe de Plantas Medicinales y Aromaticas, 2015. 14(3): p. 171-178. ISI[000356863700003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0717-073015.</p><br /> 

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000356820900015">Optimization and Evaluation of 5-Styryl-oxathiazol-2-one Mycobacterium tuberculosis Proteasome Inhibitors as Potential Antitubercular Agents.</a> Russo, F., J. Gising, L. Akerbladh, A.K. Roos, A. Naworyta, S.L. Mowbray, A. Sokolowski, I. Henderson, T. Alling, M.A. Bailey, M. Files, T. Parish, A. Karlen, and M. Larhed. ChemistryOpen, 2015. 4(3): p. 342-362. ISI[000356820900015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0717-073015.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000357139000021">Structure-Activity Relationships of Neplanocin A Analogues as S-Adenosylhomocysteine Hydrolase Inhibitors and their Antiviral and Antitumor Activities.</a> Chandra, G., Y.W. Moon, Y. Lee, J.Y. Jang, J. Song, A. Nayak, K. Oh, V.A. Mulamoottil, P.K. Sahu, G. Kim, T.S. Chang, M. Noh, S.K. Lee, S. Choi, and L.S. Jeong. Journal of Medicinal Chemistry, 2015. 58(12): p. 5108-5120. ISI[000357139000021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0717-073015.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000356544800068">Evolution of a Thienopyrimidine Antitubercular Relying on Medicinal Chemistry and Metabolomics Insights.</a> Li, S.G., C. Vilcheze, S. Chakraborty, X. Wang, H. Kim, M. Anisetti, S. Ekins, K.Y. Rhee, W.R. Jacobs, and J.S. Freundlich. Tetrahedron Letters, 2015. 56(23): p. 3246-3250. ISI[000356544800068].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0717-073015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
