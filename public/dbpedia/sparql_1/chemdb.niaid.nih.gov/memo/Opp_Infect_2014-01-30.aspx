

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-01-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kJ+GC4WKW03xQETP+NgN6OYZncd+PxTzd+v742VsACCc6lPbAE4NmbYiKbkBJGZh5kfLuTlaPEkAgyJIVZxB6SFIJ9kGk+xs2C/N76tLKEqvj1J2q//wN61zcVE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="96392DC8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: January 17 - January 30, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24460988">Antimicrobial Activity of Fluorescent AG Nanoparticles.</a> Bera, R.K., S.M. Mandal, and C. Retna Raj. Letters in Applied Microbiology, 2014. <b>[Epub ahead of print]</b>. PMID[24460988].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24444026">Chemical Composition, and in Vitro Antibacterial and Antifungal Activity of an Alkaloid Extract from Crinum angustum Steud.</a> Iannello, C., J. Bastida, F. Bonvicini, F. Antognoni, G.A. Gentilomi, and F. Poli. Natural Product Research, 2014. <b>[Epub ahead of print]</b>. PMID[24444026]. <b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24445343">Design and Synthesis of Some New 1,3,4-Thiadiazines with Coumarin Moieties and Their Antioxidative and Antifungal Activity.</a> Cacic, M., V. Pavic, M. Molnar, B. Sarkanj, and E. Has-Schon. Molecules, 2014. 19(1): p. 1163-1177. PMID[24445343].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24150496">Dual Role of Acidic Diacetate Sophorolipid as Biostabilizer for ZNO Nanoparticle Synthesis and Biofunctionalizing Agent against Salmonella enterica and Candida albicans.</a> Basak, G., D. Das, and N. Das. Journal of Microbiology and Biotechnology, 2014. 24(1): p. 87-96. PMID[24150496].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24460654">A Lectin from Spatholobus parviflorus Inhibits Aspergillus flavus alpha-Amylase: Enzyme Kinetics and Thermodynamic Studies.</a> Ignatius, T., A. Joseph, D.K. Vijayan, A. Augustine, H. Madathilkovilakath, and S. Chittalakkottu. Chemical Biology and Drug Design, 2014. <b>[Epub ahead of print]</b>. PMID[24460654].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24442594">Purification and Characterization of Alkaline Chitinase from Paenibacillus pasadenensis NCIM 5434.</a> Loni, P.P., J.U. Patil, S.S. Phugare, and S.S. Bajekal. Journal of Basic Microbiology, 2014. <b>[Epub ahead of print]</b>. PMID[24442594].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24470309">Synthesis and Biological Activity of Piperazine Derivatives of Phenothiazine.</a> Amani, A.M. Drug Research, 2014. <b>[Epub ahead of print]</b>. PMID[24470309].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24436013">In Vitro Effect of Amphotericin B on Candida albicans, Candida glabrata and Candida parapsilosis Biofilm Formation.</a> Prazynska, M. and E. Gospodarek. Mycopathologia, 2014. <b>[Epub ahead of print]</b>. PMID[24436013].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24465737">In Vitro and in Vivo Activity of a Novel Antifungal Small Molecule against Candida infections.</a> Wong, S.S., R.Y. Kao, K.Y. Yuen, Y. Wang, D. Yang, L.P. Samaranayake, and C.J. Seneviratne. Plos One, 2014. 9(1): p. e85836. PMID[24465737].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24388809">Alkylamino Derivatives of Pyrazinamide: Synthesis and Antimycobacterial Evaluation.</a> Servusova, B., P. Paterova, J. Mandikova, V. Kubicek, R. Kucera, J. Kunes, M. Dolezal, and J. Zitko. Bioorganic and Medicinal Chemistry Letters, 2014. 24(2): p. 450-453. PMID[24388809].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24449778">The Anti-tuberculosis Antibiotic Capreomycin Inhibits Protein Synthesis through Disrupting Interaction between Ribosomal Proteins L12 and L10.</a> Lin, Y., Y. Li, N. Zhu, Y. Han, W. Jiang, Y. Wang, S. Si, and J. Jiang. Antimicrobial Agents and Chemotherapy, 2014. <b>[Epub ahead of print]</b>. PMID[24449778].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24360562">The Effect of Chain Length and Unsaturation on MTB DXR Inhibition and Antitubercular Killing Activity of FR900098 Analogs.</a> Jackson, E.R., G. San Jose, R.C. Brothers, E.K. Edelstein, Z. Sheldon, A. Haymond, C. Johny, H.I. Boshoff, R.D. Couch, and C.S. Dowd. Bioorganic and Medicinal Chemistry Letters, 2014. 24(2): p. 649-653. PMID[24360562].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24443810">A New Cinnamoylglycoflavonoid, Antimycobacterial and Antioxidant Constituents from Heritiera littoralis Leaf Extracts.</a> Christopher, R., S.S. Nyandoro, M. Chacha, and C.B. de Koning. Natural Product Research, 2014. <b>[Epub ahead of print]</b>. PMID[24443810].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24369840">Salicylanilide diethyl phosphates: Synthesis, Antimicrobial Activity and Cytotoxicity.</a> Vinsova, J., J. Kozic, M. Kratky, J. Stolarikova, J. Mandikova, F. Trejtnar, and V. Buchta. Bioorganic and Medicinal Chemistry, 2014. 22(2): p. 728-737. PMID[24369840].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24464186">Spectinamides: A New Class of Semisynthetic Antituberculosis Agents That Overcome Native Drug Efflux.</a> Lee, R.E., J.G. Hurdle, J. Liu, D.F. Bruhn, T. Matt, M.S. Scherman, P.K. Vaddady, Z. Zheng, J. Qi, R. Akbergenov, S. Das, D.B. Madhura, C. Rathi, A. Trivedi, C. Villellas, R.B. Lee, Rakesh, S.L. Waidyarachchi, D. Sun, M.R. McNeil, J.A. Ainsa, H.I. Boshoff, M. Gonzalez-Juarrero, B. Meibohm, E.C. Bottger, and A.J. Lenaerts. Nature Medicine, 2014. <b>[Epub ahead of print]</b>. PMID[24464186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24446205">Synthesis and Antitubercular Activity of Novel 3,5-Diaryl-4,5-dihydro-1H-pyrazole Derivatives.</a> Alegaon, S.G., K.R. Alagawadi, and D.H. Dadwe. Drug Research, 2014. <b>[Epub ahead of print]</b>. PMID[24446205].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24373723">Synthesis and Biological Evaluation of 2-Aminothiazole Derivatives as Antimycobacterial and Antiplasmodial Agents.</a> Mjambili, F., M. Njoroge, K. Naran, C. De Kock, P.J. Smith, V. Mizrahi, D. Warner, and K. Chibale. Bioorganic and Medicinal Chemistry Letters, 2014. 24(2): p. 560-564. PMID[24373723].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24424275">The Synthesis and Biological Evaluation of Mycobacterial P-Hydroxybenzoic acid Derivatives (P-HBADs).</a> Bourke, J., C.F. Brereton, S.V. Gordon, E.C. Lavelle, and E.M. Scanlan. Organic and Biomolecular Chemistry, 2014. 12(7): p. 1114-1123. PMID[24424275].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24447252">In Vitro Activity of Rifampicin alone and in Combination with Imipenem against Multidrug-resistant Acinetobacter baumannii Harboring the BLA Resistance Gene.</a> Majewski, P., P. Wieczorek, D. Ojdana, P.T. Sacha, A. Wieczorek, and E.A. Tryniszewska. Scandinavian Journal of Infectious Diseases, 2014. <b>[Epub ahead of print]</b>. PMID[24447252].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24446664">2,4-Diamino-thienopyrimidines as Orally Active Antimalarial Agents.</a> Gonzalez Cabrera, D., C. Le Manach, F. Douelle, Y. Younis, T.S. Feng, T. Paquet, A.T. Nchinda, L.J. Street, D. Taylor, C. de Kock, L. Wiesner, S. Duffy, K.L. White, K.M. Zabiulla, Y. Sambandan, S. Bashyam, D. Waterson, M.J. Witty, S.A. Charman, V.M. Avery, S. Wittlin, and K. Chibale. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24446664].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24440906">Antiplasmodial and Cytotoxic Dibenzofurans from Preussia Sp. Harboured in Enantia chlorantha Oliv.</a> Talontsi, F.M., M. Lamshoft, C. Douanla-Meli, S.F. Kouam, and M. Spiteller. Fitoterapia, 2014. <b>[Epub ahead of print]</b>. PMID[24440906].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24444074">Design and Synthesis of a New Class of 4-Aminoquinolinyl- and 9-Anilinoacridinyl Schiff Base Hydrazones as Potent Antimalarial Agents.</a> Sharma, M., K. Chauhan, R.K. Srivastava, S.V. Singh, K. Srivastava, J.K. Saxena, S.K. Puri, and P.M. Chauhan. Chemical Biology and Drug Design, 2014. <b>[Epub ahead of print]</b>. PMID[24444074].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24468190">Dietary Flavonoids Fisetin and Myricetin: Dual Inhibitors of Plasmodium falciparum Falcipain-2 and Plasmepsin II.</a> Jin, H., Z. Xu, K. Cui, T. Zhang, W. Lu, and J. Huang. Fitoterapia, 2014. <b>[Epub ahead of print]</b>. PMID[24468190].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24350818">Myristicyclins A and B: Antimalarial Procyanidins from Horsfieldia spicata from Papua New Guinea.</a> Lu, Z., R.M. Van Wagoner, C.D. Pond, A.R. Pole, J.B. Jensen, D. Blankenship, B.T. Grimberg, R. Kiapranis, T.K. Matainaho, L.R. Barrows, and C.M. Ireland. Organic Letters, 2014. 16(2): p. 346-349. PMID[24350818].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24354322">Quinoline-pyrimidine Hybrids: Synthesis, Antiplasmodial Activity, SAR, and Mode of Action Studies.</a> Singh, K., H. Kaur, P. Smith, C. de Kock, K. Chibale, and J. Balzarini. Journal of Medicinal Chemistry, 2014. 57(2): p. 435-448. PMID[24354322].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24464049">Recent Progress Regarding the Bioactivities, Biosynthesis and Synthesis of Naturally Occurring Resorcinolic Macrolides.</a> Xu, J., C.S. Jiang, Z.L. Zhang, W.Q. Ma, and Y.W. Guo. Acta Pharmacologica Sinica, 2014. <b>[Epub ahead of print]</b>. PMID[24464049].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0117-013014.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328886000006">2-Hydroxy-substituted cinnamic acids and Acetanilides Are Selective Growth Inhibitors of Mycobacterium tuberculosis.</a> Guzman, J.D., P.N. Mortazavi, T. Munshi, D. Evangelopoulos, T.D. McHugh, S. Gibbons, J. Malkinson, and S. Bhakta. MedChemComm, 2014. 5(1): p. 47-50. ISI[000328886000006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329114200052">A Novel Indigoid anti-Tuberculosis Agent.</a> Klein, L.L., V. Petukhova, B.J. Wan, Y.H. Wang, B.D. Santasiero, D.C. Lankin, G.F. Pauli, and S.G. Franzblau. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 268-270. ISI[000329114200052].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329078100010">Antibacterial Activity of and Resistance to Small Molecule Inhibitors of the CLPP Peptidase.</a> Compton, C.L., K.R. Schmitz, R.T. Sauer, and J.K. Sello. ACS Chemical Biology, 2013. 8(12): p. 2669-2677. ISI[000329078100010].</p>
    
    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329118100009">Antimicrobial Activity of Invertebrate-pathogenic Fungi in the Genera Akanthomyces and Gibellula.</a> Kuephadungphan, W., S. Phongpaichit, J.J. Luangsa-ard, and V. Rukachaisirikul. Mycoscience, 2014. 55(2): p. 127-133. ISI[000329118100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328869500039">Antimicrobial and Cytotoxic Secondary Metabolites from Tropical Leaf Endophytes: Isolation of Antibacterial Agent Pyrrocidine C from Lewia infectoria SNB-GTC2402.</a> Casella, T.M., V. Eparvier, H. Mandavid, A. Bendelac, G. Odonne, L. Dayan, C. Duplais, L.S. Espindola, and D. Stien. Phytochemistry, 2013. 96: p. 370-377. ISI[000328869500039].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328723300015">Antimicrobial Effect of Sodium Houttuyfonate on Staphylococcus epidermidis and Candida albicans Biofilms.</a> Shao, J., H.J. Cheng, D.Q. Wu, C.Z. Wang, L.L. Zhu, Z.X. Sun, Q.J. Duan, W.F. Huang, and J.L. Huang. Journal of Traditional Chinese Medicine, 2013. 33(6): p. 798-803. ISI[000328723300015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328588200034">Antioxidant, Antiproliferative and Antimicrobial Activities of the Volatile Oil from the Wild Pepper Piper Capense Used in Cameroon as a Culinary Spice.</a> Woguem, V., F. Maggi, H.P.D. Fogang, L.A. Tapondjou, H.M. Womeni, L. Quassinti, M. Bramucci, L.A. Vitali, D. Petrelli, G. Lupidi, F. Papa, S. Vittori, and L. Barboni. Natural Product Communications, 2013. 8(12): p. 1791-1796. ISI[000328588200034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329100800012">Biological Activity of Lipids and Photosynthetic Pigments of Sargassum pallidum C-Agardh.</a> Gerasimenko, N.I., E.A. Martyyas, S.V. Logvinov, and N.G. Busarova. Applied Biochemistry and Microbiology, 2014. 50(1): p. 73-81. ISI[000329100800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328529400021">Bisubstrate Inhibitors of Biotin Protein Ligase in Mycobacterium tuberculosis Resistant to Cyclonucleoside Formation.</a> Shi, C., D. Tiwari, D.J. Wilson, C.L. Seiler, D. Schnappinger, and C.C. Aldrich. ACS Medicinal Chemistry Letters, 2013. 4(12): p. 1213-1217. ISI[000328529400021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328588200033">Essential Oil Characterization of Two Azorean Cryptomeria japonica Populations and Their Biological Evaluations.</a> Moiteiro, C., T. Esteves, L. Ramalho, R. Rojas, S. Alvarez, S. Zacchino, and H. Braganca. Natural Product Communications, 2013. 8(12): p. 1785-1790. ISI[000328588200033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328863300010">Essential Oil Compositions and Bioactivities of Thymus revolutus and Cyclotrichium origanifolium.</a> Gokturk, R.S., O. Sagdic, G. Ozkan, O. Unal, A. Aksoy, S. Albayrak, M. Arici, and M.Z. Durak. Journal of Essential Oil Bearing Plants, 2013. 16(6): p. 795-805. ISI[000328863300010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328132000007">Evaluation of Antimicrobial Activity of Some Newly Synthesized 4-Thiazolidinones.</a> Saleh, N.A.K., H. El-abd Saltani, F.A. Al-Issa, and A.S.G. Melad. Journal of the Chinese Chemical Society, 2013. 60(10): p. 1234-1240. ISI[000328132000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329276700010">Identification of Novel Gyrase B Inhibitors as Potential anti-TB Drugs: Homology Modelling, Hybrid Virtual Screening and Molecular Dynamics Simulations.</a> Maharaj, Y. and M.E.S. Soliman. Chemical Biology &amp; Drug Design, 2013. 82(2): p. 205-215. ISI[000329276700010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329063500013">IQG-607 Abrogates the Synthesis of Mycolic acids and Displays Intracellular Activity against Mycobacterium tuberculosis in Infected Macrophages.</a> Rodrigues, V.S., A.A. dos Santos, A.D. Villela, J.M. Belardinelli, H.R. Morbidoni, L.A. Basso, M.M. Campos, and D.S. Santos. International Journal of Antimicrobial Agents, 2014. 43(1): p. 82-85. ISI[000329063500013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328810500027">Molecular Cloning and Functional Characterization of an endo-beta-1,3-Glucanase from Streptomyces matensis ATCC 23935.</a> Woo, J.B., H.N. Kang, E.J. Woo, and S.B. Lee. Food Chemistry, 2014. 148: p. 184-187. ISI[000328810500027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329114200044">Rational Design, Synthesis and Antitubercular Evaluation of Novel 2-(Trifluoromethyl)phenothiazine-[1,2,3]triazole Hybrids.</a><a name="_GoBack"></a> Addla, D., A. Jallapally, D. Gurram, P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 233-236. ISI[000329114200044].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328819900012">Reactive Oxygen Species-inducing Antifungal Agents and Their Activity against Fungal Biofilms.</a> Delattin, N., B.P.A. Cammue, and K. Thevissen. Future Medicinal Chemistry, 2014. 6(1): p. 77-90. ISI[000328819900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />
    
    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329068400038">Synthesis of 3-(3-Aryl-pyrrolidin-1-yl)-5-aryl-1,2,4-triazines That Have Antibacterial Activity and Also Inhibit Inorganic Pyrophosphatase.</a> Lv, W., B. Banerjee, K.L. Molland, M.N. Seleem, A. Ghafoor, M.I. Hamed, B.J. Wan, S.G. Franzblau, A.D. Mesecar, and M. Cushman. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 406-418. ISI[000329068400038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328631900006">Synthesis and Antifungal Activity of Arylthiocyanates.</a> Kokorekin, V.A., A.O. Terent&#39;ev, G.V. Ramenskaya, N.E. Grammatikova, G.M. Rodionova, and A.I. Ilovaiskii. Pharmaceutical Chemistry Journal, 2013. 47(8): p. 422-425. ISI[000328631900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329090500015">Synthesis and Antimicrobial Activity of Novel 2- 4-(1H-Benzimidazol-1-yl)phenyl-1H-benzimidazoles.</a> Alp, M., A.H. Goker, and N. Altanlar. Turkish Journal of Chemistry, 2014. 38(1): p. 152-156. ISI[000329090500015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329114200029">Synthesis and Antimycobacterial Activity of Novel Camphane-based Agents.</a> Stavrakov, G., I. Philipova, V. Valcheva, and G. Momekov. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 165-167. ISI[000329114200029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329114200035">Synthesis and Evaluation of Novel Azoles as Potent Antifungal Agents.</a> Li, L.J., H. Ding, B.G. Wang, S.C. Yu, Y. Zou, X.Y. Chai, and Q.Y. Wu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 192-194. ISI[000329114200035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br /> 
    
    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328621500009">Total Synthesis of Fellutamide B and Deoxy-fellutamides B, C, and D.</a> Giltrap, A.M., K.M. Cergol, A. Pang, W.J. Britton, and R.J. Payne. Marine Drugs, 2013. 11(7): p. 2382-2397. ISI[000328621500009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328700300012">Variation of the Chemical Composition and Antimicrobial Activity of the Essential Oils of Natural Populations of Tunisian Daucus carota L. (Apiaceae).</a> Rokbeni, N., Y. M&#39;Rabet, S. Dziri, H. Chaabane, M. Jemli, X. Fernandez, and A. Boulila. Chemistry &amp; Biodiversity, 2013. 10(12): p. 2278-2290. ISI[000328700300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0117-013014.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
