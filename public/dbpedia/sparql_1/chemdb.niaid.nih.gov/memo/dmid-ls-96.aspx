

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-96.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="D/UGeWY4+JzzbLbpZg32njdjLB3bFjDbwz/Ouzi3e4ezEVLalHuBEW5HxzJ7Og5aCrB8k5WA/5m4b7j+YkqA8rriPUPfSYOjPv5kd8jBzrgpnHJntaoimUNr0LM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B1DF3BA8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-96-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57696   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Castanospermine, a Potent Inhibitor of Dengue Virus Infection In Vitro and In Vivo</p>

    <p>          Whitby, K, Pierson, TC, Geiss, B, Lane, K, Engle, M, Zhou, Y, Doms, RW, and Diamond, MS</p>

    <p>          J Virol <b>2005</b>.  79(14): 8698-8706</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994763&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994763&amp;dopt=abstract</a> </p><br />

    <p>2.     57697   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral cyclic d,l-alpha-peptides: Targeting a general biochemical pathway in virus infections</p>

    <p>          Horne, WS, Wiethoff, CM, Cui, C, Wilcoxen, KM, Amorin, M, Ghadiri, MR, and Nemerow, GR</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993611&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993611&amp;dopt=abstract</a> </p><br />

    <p>3.     57698   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Bicyclic nucleoside inhibitors of Varicella-Zoster virus: The effect of branching in the p-alkylphenyl side chain</p>

    <p>          Luoni, G, McGuigan, C, Andrei, G, Snoeck, R, De, Clercq E, and Balzarini, J</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993062&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993062&amp;dopt=abstract</a> </p><br />

    <p>4.     57699   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Molecular basis for the antiviral and anticancer activities of unnatural L-beta-nucleosides</p>

    <p>          Spadari, S, Maga, G, Verri, A, and Focher, F</p>

    <p>          Expert Opin Investig Drugs <b>1998</b>.  7(8): 1285-1300</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15992031&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15992031&amp;dopt=abstract</a> </p><br />

    <p>5.     57700   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of antivirals against influenza</p>

    <p>          Cianci, C and Krystal, M</p>

    <p>          Expert Opin Investig Drugs <b>1998</b>.  7(2): 149-165</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15991949&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15991949&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     57701   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sialidase as a target for inhibitors of influenza virus replication</p>

    <p>          Bethell, RC and Smith, PW</p>

    <p>          Expert Opin Investig Drugs <b>1997</b>.  6(10): 1501-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15989515&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15989515&amp;dopt=abstract</a> </p><br />

    <p>7.     57702   DMID-LS-96; WOS-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Drug Targets for Hiv and Hepatitis C Virus Coinfection</p>

    <p>          Tedaldi, E.</p>

    <p>          Clinical Infectious Diseases <b>2005</b>.  41: S101-S104</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229530700018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229530700018</a> </p><br />

    <p>8.     57703   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Differentially expressed cellular genes following HBV: potential targets of anti-HBV drugs?</p>

    <p>          Yang, J, Bo, XC, Yao, J, Yang, NM, and Wang, SQ</p>

    <p>          J Viral Hepat <b>2005</b>.  12(4): 357-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15985005&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15985005&amp;dopt=abstract</a> </p><br />

    <p>9.     57704   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Investigation of glycine alpha-ketoamide HCV NS3 protease inhibitors: Effect of carboxylic acid isosteres</p>

    <p>          Han, W, Jiang, X, Hu, Z, Wasserman, ZR, and Decicco, CP</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15982872&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15982872&amp;dopt=abstract</a> </p><br />

    <p>10.   57705   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Disabling poxvirus pathogenesis by inhibition of Abl-family tyrosine kinases</p>

    <p>          Reeves, PM, Bommarius, B, Lebeis, S, McNulty, S, Christensen, J, Swimm, A, Chahroudi, A, Chavan, R, Feinberg, MB, Veach, D, Bornmann, W, Sherman, M, and Kalman, D</p>

    <p>          Nat Med <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980865&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   57706   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of oral administration of tenofovir disoproxil fumarate in woodchucks with chronic woodchuck hepatitis virus infection</p>

    <p>          Menne, S, Cote, PJ, Korba, BE, Butler, SD, George, AL, Tochkov, IA, Delaney, WE 4th, Xiong, S, Gerin, JL, and Tennant, BC</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(7): 2720-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980342&amp;dopt=abstract</a> </p><br />

    <p>12.   57707   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacology and Pharmacokinetics of the Antiviral Agent {beta}-D-2&#39;,3&#39;-Dideoxy-3&#39;-Oxa-5-Fluorocytidine in Cells and Rhesus Monkeys</p>

    <p>          Hernandez-Santiago, BI, Chen, H, Asif, G, Beltran, T, Mao, S, Hurwitz, SJ, Grier, J, McClure, HM, Chu, CK, Liotta, DC, and Schinazi, RF</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(7): 2589-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980324&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980324&amp;dopt=abstract</a> </p><br />

    <p>13.   57708   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structural analysis of inhibition mechanisms of Aurintricarboxylic Acid on SARS-CoV polymerase and other proteins</p>

    <p>          Yap, Y, Zhang, X, Andonov, A, and He, R</p>

    <p>          Comput Biol Chem <b>2005</b>.  29(3): 212-219</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15979041&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15979041&amp;dopt=abstract</a> </p><br />

    <p>14.   57709   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of Potent Anilide Inhibitors against the Severe Acute Respiratory Syndrome 3CL Protease</p>

    <p>          Shie, JJ, Fang, JM, Kuo, CJ, Kuo, TH, Liang, PH, Huang, HJ, Yang, WB, Lin, CH, Chen, JL, Wu, YT, and Wong, CH</p>

    <p>          J Med Chem <b>2005</b>.  48(13): 4469-4473</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15974598&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15974598&amp;dopt=abstract</a> </p><br />

    <p>15.   57710   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cytotoxic, antiviral (in-vitro and in-vivo), immunomodulatory activity and influence on mitotic divisions of three taxol derivatives: 10-deacetyl-baccatin III, methyl (N-benzoyl-(2&#39;R,3&#39;S)-3&#39;-phenylisoserinate) and N-benzoyl-(2&#39;R,3&#39;S)-3&#39;-phenylisoserine</p>

    <p>          Krawczyk, E, Luczak, M, Kniotek, M, and Nowaczyk, M</p>

    <p>          J Pharm Pharmacol <b>2005</b>.  57(6): 791-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15969936&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15969936&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   57711   DMID-LS-96; EMBASE-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral drug discovery and development: Where chemistry meets with biomedicine</p>

    <p>          De Clercq, Erik</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GGWDGJ-1/2/bec7169eeb5a1b260eb965011505a75c">http://www.sciencedirect.com/science/article/B6T2H-4GGWDGJ-1/2/bec7169eeb5a1b260eb965011505a75c</a> </p><br />

    <p>17.   57712   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus translation and subgenomic replication by siRNAs directed against highly conserved HCV sequence and cellular HCV cofactors</p>

    <p>          Korf, M, Jarczak, D, Beger, C, Manns, MP, and Kruger, M</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15964661&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15964661&amp;dopt=abstract</a> </p><br />

    <p>18.   57713   DMID-LS-96; WOS-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Prevalence of Avian Influenza Virus Infections in Poultry and Wild Birds</p>

    <p>          Smietanka, K. <i> et al.</i></p>

    <p>          Medycyna Weterynaryjna <b>2005</b>.  61(6): 676-679</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229570800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229570800016</a> </p><br />

    <p>19.   57714   DMID-LS-96; WOS-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Advances in Antiviral Therapy</p>

    <p>          Wu, J. <i>et al.</i></p>

    <p>          Dermatologic Clinics <b>2005</b>.  23(2): 313-+</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229644300011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229644300011</a> </p><br />

    <p>20.   57715   DMID-LS-96; WOS-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Poxviral Cross-Class Proteinase Inhibitors Reduce Plaque Growth and Alter Arterial Apoptotic Responses and Cellular Apoptosis</p>

    <p>          Liu, L. <i>et al.</i></p>

    <p>          Circulation <b>2004</b>.  110(17): 185</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224783501040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224783501040</a> </p><br />

    <p>21.   57716   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human infection by avian influenza A H5N1</p>

    <p>          Yuen, KY and Wong, SS</p>

    <p>          Hong Kong Med J <b>2005</b>.  11(3): 189-99</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15951584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15951584&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   57717   DMID-LS-96; WOS-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Murine Interferon Lambdas (Type Iii Interferons) Exhibit Potent Antiviral Activity in Vivo in a Poxvirus Infection Model</p>

    <p>          Bartlett, N. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2005</b>.  86: 1589-1596</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229575500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229575500002</a> </p><br />

    <p>23.   57718   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mismatched hemagglutinin and neuraminidase specificities in recent human H3N2 influenza viruses</p>

    <p>          Gulati, U, Wu, W, Gulati, S, Kumari, K, Waner, JL, and Air, GM</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15950996&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15950996&amp;dopt=abstract</a> </p><br />

    <p>24.   57719   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Screening of drugs by FRET analysis identifies inhibitors of SARS-CoV 3CL protease</p>

    <p>          Liu, YC, Huang, V, Chao, TC, Hsiao, CD, Lin, A, Chang, MF, and Chow, LP</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  333(1): 194-199</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15950190&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15950190&amp;dopt=abstract</a> </p><br />

    <p>25.   57720   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Complete Replication of Hepatitis C Virus in Cell Culture</p>

    <p>          Lindenbach, BD, Evans, MJ, Syder, AJ, Wolk, B, Tellinghuisen, TL, Liu, CC, Maruyama, T, Hynes, RO, Burton, DR, McKeating, JA, and Rice, CM</p>

    <p>          Science <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15947137&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15947137&amp;dopt=abstract</a> </p><br />

    <p>26.   57721   DMID-LS-96; PUBMED-DMID-7/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure-based characterization and optimization of novel hydrophobic binding interactions in a series of pyrrolidine influenza neuraminidase inhibitors</p>

    <p>          Maring, CJ, Stoll, VS, Zhao, C, Sun, M, Krueger, AC, Stewart, KD, Madigan, DL, Kati, WM, Xu, Y, Carrick, RJ, Montgomery, DA, Kempf-Grote, A, Marsh, KC, Molla, A, Steffy, KR, Sham, HL, Laver, WG, Gu, YG, Kempf, DJ, and Kohlbrenner, WE</p>

    <p>          J Med Chem <b>2005</b>.  48(12): 3980-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15943472&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15943472&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
