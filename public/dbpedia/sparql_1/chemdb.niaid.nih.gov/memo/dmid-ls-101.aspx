

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-101.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4IKqE66ZM6j8q+XlXgEvHP5XstN0i5Mo4BhPklOO4lbsJ5aGUbkAHvuEXSDl3L6qvC9BiB2ZywOIGXaDw+p9bvjzxt9JySHj3RXt9hBfXclrlfQHoF0SGTkmO+4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="52C9E83E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-101-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57828   DMID-LS-101; WOS-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Celgosivir - Alpha-Glucosidase Inhibitor Anti-Hepatitis C Virus Drug</p>

    <p>          Sorbera, L., Castaner, J., and Garcia-Capdevila, L.</p>

    <p>          Drugs of the Future <b>2005</b>.  30(6): 545-552</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231178900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231178900001</a> </p><br />

    <p>2.     57829   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Method for discovery and development of broad-spectrum antiviral drugs</p>

    <p>          Buscher, Benjamin A, Dyall, Julie, Jockel-Balsarotti, Jennifer I, O&#39;Guin, Andrew K, Olivo, Paul D, Roth, Robert M, and Zhou, Yi</p>

    <p>          PATENT:  US <b>2005164167</b>  ISSUE DATE:  20050728</p>

    <p>          APPLICATION: 2004-45330  PP: 36 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     57830   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The therapeutic potential of NS3 protease inhibitors in HCV infection</p>

    <p>          Goudreau, N and Llinas-Brunet, M</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(9): 1129-44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16144497&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16144497&amp;dopt=abstract</a> </p><br />

    <p>4.     57831   DMID-LS-101; WOS-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis C Virus Translation and Subgenomic Replication by Sirnas Directed Against Highly Conserved Hcv Sequence and Cellular Hcv Cofactors</p>

    <p>          Korf, M. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2005</b>.  43(2): 225-234</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230803900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230803900006</a> </p><br />

    <p>5.     57832   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Suppression of hepatitis C virus replication by cyclosporin a is mediated by blockade of cyclophilins</p>

    <p>          Nakagawa, M, Sakamoto, N, Tanabe, Y, Koyama, T, Itsui, Y, Takeda, Y, Chen, CH, Kakinuma, S, Oooka, S, Maekawa, S, Enomoto, N, and Watanabe, M</p>

    <p>          Gastroenterology <b>2005</b>.  129(3): 1031-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16143140&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16143140&amp;dopt=abstract</a> </p><br />

    <p>6.     57833   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activities of some 4,4&#39;- and 2,2&#39;-dihydroxytriphenylmethanes</p>

    <p>          Mibu, N, Yokomizo, K, Uyeda, M, and Sumoto, K</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2005</b>.  53(9): 1171-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16141590&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16141590&amp;dopt=abstract</a> </p><br />

    <p>7.     57834   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and anti-viral activity of a series of sesquiterpene lactones and analogues in the subgenomic HCV replicon system</p>

    <p>          Hwang, DR, Wu, YS, Chang, CW, Lien, TW, Chen, WC, Tan, UK, Hsu, JT, and Hsieh, HP</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140536&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140536&amp;dopt=abstract</a> </p><br />

    <p>8.     57835   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity and mode of action of caffeoylquinic acids from Schefflera heptaphylla (L.) Frodin</p>

    <p>          Li, Y, But, PP, and Ooi, VE</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140400&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140400&amp;dopt=abstract</a> </p><br />

    <p>9.     57836   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Viral polymerase inhibitors</p>

    <p>          Tsantrizos, Youla S, Chabot, Catherine, Beaulieu, Pierre, Brochu, Christian, Poirier, Martin, Stammers, Timothy A, Thavonekham, Bounkham, and Rancourt, Jean</p>

    <p>          PATENT:  WO <b>2005080388</b>  ISSUE DATE:  20050901</p>

    <p>          APPLICATION: 2005  PP: 177 pp.</p>

    <p>          ASSIGNEE:  (Boehringer Ingelheim International G.m.b.H., Germany and Boehringer Ingelheim Pharma G.m.b.H. &amp; Co. K.-G.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   57837   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cytostatic 6-arylpurine nucleosides. 6. SAR in anti-HCV and cytostatic activity of extended series of 6-hetarylpurine ribonucleosides</p>

    <p>          Hocek, M, Naus, P, Pohl, R, Votruba, I, Furman, PA, Tharnish, PM, and Otto, MJ</p>

    <p>          J Med Chem <b>2005</b>.  48(18): 5869-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16134952&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16134952&amp;dopt=abstract</a> </p><br />

    <p>11.   57838   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          4-Oxo-4,7-dihydrothieno[2,3-b]pyridines as non-nucleoside inhibitors of human cytomegalovirus and related herpesvirus polymerases</p>

    <p>          Schnute, ME, Cudahy, MM, Brideau, RJ, Homa, FL, Hopkins, TA, Knechtel, ML, Oien, NL, Pitts, TW, Poorman, RA, Wathen, MW, and Wieber, JL</p>

    <p>          J Med Chem <b>2005</b>.  48(18): 5794-804</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16134946&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16134946&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   57839   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro antiviral activity of three enantiomeric sesquiterpene lactones from Senecio species against hepatitis B virus</p>

    <p>          Li, H, Zhou, C, Zhou, L, Chen, Z, Yang, L, Bai, H, Wu, X, Peng, H, and Zhao, Y</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(4): 277-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130525&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130525&amp;dopt=abstract</a> </p><br />

    <p>13.   57840   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and anti-rhinovirus properties of fluoro-substituted flavonoids</p>

    <p>          Conti, C, Mastromarino, P, Goldoni, P, Portalone, G, and Desideri, N</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(4): 267-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130524&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130524&amp;dopt=abstract</a> </p><br />

    <p>14.   57841   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          HCV antiviral resistance: The impact of in vitro studies on the development of antiviral agents targeting the viral NS5B polymerase</p>

    <p>          Tomei, L, Altamura, S, Paonessa, G, De Francesco, R, and Migliaccio, G</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(4): 225-245</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130521&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130521&amp;dopt=abstract</a> </p><br />

    <p>15.   57842   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design of Wide-Spectrum Inhibitors Targeting Coronavirus Main Proteases</p>

    <p>          Yang, H, Xie, W, Xue, X, Yang, K, Ma, J, Liang, W, Zhao, Q, Zhou, Z, Pei, D, Ziebuhr, J, Hilgenfeld, R, Yuen, KY, Wong, L, Gao, G, Chen, S, Chen, Z, Ma, D, Bartlam, M, and Rao, Z</p>

    <p>          PLoS Biol <b>2005</b>.  3(10): e324</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16128623&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16128623&amp;dopt=abstract</a> </p><br />

    <p>16.   57843   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Conquering influenza: Recent advances in anti-influenza drug discovery</p>

    <p>          Luo, G, Cianci, C, Harte, W, and Krystal, M</p>

    <p>          IDrugs <b>1999</b>.  2(7): 671-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127638&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127638&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   57844   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Viral proteases and phosphorodiesterase inhibitors</p>

    <p>          Agathangelou, P</p>

    <p>          IDrugs <b>1999</b>.  2(6): 523-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127607&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127607&amp;dopt=abstract</a> </p><br />

    <p>18.   57845   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design of a small-molecule entry inhibitor with activity against primary measles virus strains</p>

    <p>          Plemper, RK, Doyle, J, Sun, A, Prussia, A, Cheng, LT, Rota, PA, Liotta, DC, Snyder, JP, and Compans, RW</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(9): 3755-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127050&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127050&amp;dopt=abstract</a> </p><br />

    <p>19.   57846   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p><b>          </b>Antiviral drugs: Triphosphates of nucleoside analogues active as antiviral drugs</p>

    <p>          De Clercq, Erik</p>

    <p>          Nucleoside Triphosphates and Their Analogs <b>2005.</b> 329-341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   57847   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structural analysis and antiviral activity of a sulfated galactan from the red seaweed Schizymenia binderi (Gigartinales, Rhodophyta)</p>

    <p>          Matsuhiro, B, Conte, AF, Damonte, EB, Kolender, AA, Matulewicz, MC, Mejias, EG, Pujol, CA, and Zuniga, EA</p>

    <p>          Carbohydr Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16125685&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16125685&amp;dopt=abstract</a> </p><br />

    <p>21.   57848   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New antivirals for herpesviruses</p>

    <p>          Eizuru, Yoshito</p>

    <p>          Uirusu <b>2005</b>.  55 (1): 95-104</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   57849   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of artemisinin/artesunate as inhibitors of hepatitis B virus production in an &quot;in vitro&quot; replicative system</p>

    <p>          Romero, MR, Efferth, T, Serrano, MA, Castano, B, Macias, RI, Briz, O, and Marin, JJ</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16122816&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16122816&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   57850   DMID-LS-101; WOS-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Study of Novel Peptide Inhibitors Against Sars-Cov Entry</p>

    <p>          Yan, Z., Tripet, B., and Hodges, R.</p>

    <p>          Biopolymers <b>2005</b>.  80(4): 499-500</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229901200077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229901200077</a> </p><br />

    <p>24.   57851   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Therapy and prophylaxis of Ebola virus infections</p>

    <p>          Feldmann, H, Jones, SM, Schnittler, HJ , and Geisbert, T</p>

    <p>          Curr Opin Investig Drugs <b>2005</b>.  6(8): 823-830</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16121689&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16121689&amp;dopt=abstract</a> </p><br />

    <p>25.   57852   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral efficacy of VP14637 against respiratory syncytial virus in vitro and in cotton rats following delivery by small droplet aerosol</p>

    <p>          Wyde, PR, Laquerre, S, Chetty, SN, Gilbert, BE, Nitz, TJ, and Pevear, DC</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112208&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112208&amp;dopt=abstract</a> </p><br />

    <p>26.   57853   DMID-LS-101; WOS-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Protein Kinases C Prevents Murine Cytomegalovirus Replication</p>

    <p>          Kucic, N., Mahmutefendic, H., and Lucin, P.</p>

    <p>          Journal of General Virology <b>2005</b>.  86: 2153-2161</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300004</a> </p><br />

    <p>27.   57854   DMID-LS-101; PUBMED-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of the hepatitis B virus replication in vitro by an oligodeoxynucleotide containing cytidine-guanosine motifs</p>

    <p>          Li, N, Fan, XG, Chen, ZH, Zhu, C, Liu, HB, and Huang, Y</p>

    <p>          Immunol Lett <b>2005</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16105691&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16105691&amp;dopt=abstract</a> </p><br />

    <p>28.   57855   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of glutathione derivatives as antiviral agents</p>

    <p>          Benatti, Umberto, Brandi, Giorgio, Garaci, Enrico, Magnani, Mauro, Millo, Enrico, Palamara, Anna Teresa, and Rossi, Luigia</p>

    <p>          PATENT:  WO <b>2005063795</b>  ISSUE DATE:  20050714</p>

    <p>          APPLICATION: 2004  PP: 26 pp.</p>

    <p>          ASSIGNEE:  (Italy)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>29.   57856   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phospholipids for the treatment of infection by togaviruses, herpes viruses and coronaviruses</p>

    <p>          Fleming, Ronald A, Hes, Jan V, Huang, Yunsheng, Read, Russ H, Morris-Natschke, Susan L, Ishaq, Khalid S, Kucera, Louis S, and Furman, Phillip A</p>

    <p>          PATENT:  US <b>2005187192</b>  ISSUE DATE:  20050825</p>

    <p>          APPLICATION: 2004-63031  PP: 36 pp.</p>

    <p>          ASSIGNEE:  (Kucera Pharmaceutical Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   57857   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of cisplatin derivatives of methotrexate against vaccinia virus, varicella zoster virus (VZV), reovirus ST3, and herpes simplex virus (HSV-1)</p>

    <p>          Roner, Michael R, Carraher, Charles E Jr, and Dhanji, Salima</p>

    <p>          PMSE Preprints  <b>2005</b>.  93: 410-413</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   57858   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-SARS-coronavirus monoclonal antibodies, and diagnostic, therapeutic and vaccine preparation uses</p>

    <p>          Berry, Jody, Jones, Steven, Yuan, Xin Yong, Gubbins, Mike, Andonov, Anton, Weingarti, Hana, Drebot, Mike, and Plummer, Frank</p>

    <p>          PATENT:  WO <b>2005054469</b>  ISSUE DATE:  20050616</p>

    <p>          APPLICATION: 2004  PP: 75 pp.</p>

    <p>          ASSIGNEE:  (Her Majesty the Queen in Right of Canada as Represented by the Minister of Health, Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   57859   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mechanisms of imiquimod indirect antiviral activity</p>

    <p>          Hober, D, Ajram, L, Chehadeh, W, Lazrek, M, Goffard, A, Dewilde, A, and Wattre, P</p>

    <p>          Annales de Biologie Clinique <b>2005</b>.  63(2): 155-163</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   57860   DMID-LS-101; SCIFINDER-DMID-9/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polyamide compositions and therapeutic methods for treatment of human papilloma virus</p>

    <p>          Fisher, Chris, Bashkin, James K, Crowley, Kathleen S, Sverdrup, Francis M, Garner-Hamrick, Peggy A, and Phillion, Dennis P</p>

    <p>          PATENT:  WO <b>2005033282</b>  ISSUE DATE:  20050414</p>

    <p>          APPLICATION: 2004  PP: 53 pp.</p>

    <p>          ASSIGNEE:  (Pharmacia &amp; Upjohn Company LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
