

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-359.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="rqhfuGDsv3WE8rHt1vO1KyPY4QrxXqSuRGZNv+fj00VjaTEOncZ91c0PhU7yANZJPQAZW/UzsS9irkvMPAoA6m34focbz4q4bUNNPI8gKQIrgzM57PE0VWceAh8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="26D461B8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH- OI-LS-359-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59191   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Lysophosphatidic acid enhances antimycobacterial activity both in vitro and ex vivo</p>

    <p>          Garg, SK, Valente, E, Greco, E, Santucci, MB, De Spirito, M, Papi, M, Bocchino, M, Saltini, C, and Fraziano, M</p>

    <p>          Clinical Immunology (San Diego, CA, United States) <b>2006</b>.  121(1): 23-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     59192   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Novel 1,2,4-triazole-3-mercaptoacetic acid derivatives as potential anti-mycobacterial and antimicrobial agents</p>

    <p>          El-Koussi, Nawal A and Abdel-Rahman, Hamdy M</p>

    <p>          Bulletin of Pharmaceutical Sciences, Assiut University <b>2006</b>.  29(1): 127-136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     59193   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Pyrrolidine Carboxamides as a Novel Class of Inhibitors of Enoyl Acyl Carrier Protein Reductase from Mycobacterium tuberculosis</p>

    <p>          He, X, Alian, A, Stroud, R, and Ortiz, de Montellano PR</p>

    <p>          J Med Chem <b>2006</b>.  49(21): 6308-6323</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17034137&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17034137&amp;dopt=abstract</a> </p><br />

    <p>4.     59194   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Molecular and therapeutic advances in the management of chronic hepatitis B and C</p>

    <p>          McCarron, B, Main, J, and Thomas, HC</p>

    <p>          Curr Opin Infect Dis <b>1998</b>.  11(5): 555-64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17033422&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17033422&amp;dopt=abstract</a> </p><br />

    <p>5.     59195   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Glutamate racemase from Mycobacterium tuberculosis inhibits DNA gyrase by affecting its DNA-binding</p>

    <p>          Sengupta, S, Shah, M, and Nagaraja, V</p>

    <p>          Nucleic Acids Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020913&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020913&amp;dopt=abstract</a> </p><br />

    <p>6.     59196   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Design, Synthesis, and Biological Evaluation of beta-Ketosulfonamide Adenylation Inhibitors as Potential Antitubercular Agents</p>

    <p>          Vannada, J, Bennett, EM, Wilson, DJ, Boshoff, HI, Barry, CE 3rd, and Aldrich, CC</p>

    <p>          Org Lett <b>2006</b>.  8(21): 4707-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020283&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020283&amp;dopt=abstract</a> </p><br />

    <p>7.     59197   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          New insights into DHFR interactions: Analysis of Pneumocystis carinii and mouse DHFR complexes with NADPH and two highly potent 5-(omega-carboxy(alkyloxy) trimethoprim derivatives reveals conformational correlations with activity and novel parallel ring stacking interactions</p>

    <p>          Cody, V, Pace, J, Chisum, K, and Rosowsky, A</p>

    <p>          Proteins <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17019704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17019704&amp;dopt=abstract</a> </p><br />

    <p>8.     59198   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          In Vitro and in Vivo Activities of HQQ-3, a New Triazole Antifungal Agent</p>

    <p>          Zhao, JX, Cao, YY, Quan, H, Liu, CM, He, QQ, Wu, QY, Gao, PH, Cao, YB, Liu, WX, and Jiang, YY</p>

    <p>          Biol Pharm Bull <b>2006</b>.  29(10): 2031-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015946&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015946&amp;dopt=abstract</a> </p><br />

    <p>9.     59199   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Screening for anti-infective properties of several medicinal plants of the Mauritians flora</p>

    <p>          Rangasamy, O, Raoelison, G, Rakotoniriana, FE, Cheuk, K, Urverg-Ratsimamanga, S, Quetin-Leclercq, J, Gurib-Fakim, A, and Subratty, AH</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17011733&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17011733&amp;dopt=abstract</a> </p><br />

    <p>10.   59200   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Novel non-classical C9-methyl-5-substituted-2,4-diaminopyrrolo[2,3-d]pyrimidines as potential inhibitors of dihydrofolate reductase and as anti-opportunistic agents</p>

    <p>          Gangjee, A, Yang, J, and Queener, SF</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17010625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17010625&amp;dopt=abstract</a> </p><br />

    <p>11.   59201   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Screening for Toxoplasma gondii-regulated transcriptional responses in lipopolysaccharide-activated macrophages</p>

    <p>          Lee, Chiang W, Bennouna, Soumaya, and Denkers, Eric Y</p>

    <p>          Infection and Immunity <b>2006</b>.  74(3): 1916-1923</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   59202   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of novel 2-methyl-3-(1&#39;3&#39;4&#39;-thiadiazoyl)-4-(3H) quinazolinones</p>

    <p>          Jatav, Varsha, Jain, SK, Kashaw, SK, and Mishra, P</p>

    <p>          Indian Journal of Pharmaceutical Sciences <b>2006</b>.  68(3): 360-363</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   59203   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Azoles: mode of antifungal action and resistance development. Effect of miconazole on endogenous reactive oxygen species production in Candida albicans</p>

    <p>          Francois, Isabelle EJA, Cammue, Bruno PA, Borgers, Marcel, Ausma, Jannie, Dispersyn, Gerrit D, and Thevissen, Karin</p>

    <p>          Anti-Infective Agents in Medicinal Chemistry <b>2006</b>.  5(1): 3-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   59204   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Tensidols, new potentiators of antifungal miconazole activity, produced by Aspergillus niger FKI-2342</p>

    <p>          Fukuda, Takashi, Hasegawa, Yoko, Hagimori, Keiichi, Yamaguchi, Yuichi, Masuma, Rokuro, Tomoda, Hiroshi, and Omura, Satoshi</p>

    <p>          Journal of Antibiotics <b>2006</b>.  59(8): 480-485</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   59205   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of a novel series of 2-(4-chloro-3-methylphenoxy)acetyl amino acids and peptides</p>

    <p>          Dahiya, Rajiv, Pathak, Davender, and Bhatt, Sunita</p>

    <p>          Journal of Saudi Chemical Society <b>2006</b>.  10(1): 165-176</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   59206   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Antifungal Activity from 14-Helical b-Peptides</p>

    <p>          Karlsson, Amy J, Pomerantz, William C, Weisblum, Bernard, Gellman, Samuel H, and Palecek, Sean P</p>

    <p>          Journal of the American Chemical Society <b>2006</b>.  128(39): 12630-12631</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   59207   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial and anticancer activities of 2-substituted acetamido-4-ethoxycarbonylmethyl-1,3-thiazoles</p>

    <p>          Altintas, H, Ates, O, Gursoy, A, Birteksoz, S, and Otuk, G</p>

    <p>          Journal of Faculty of Pharmacy of Istanbul University <b>2006</b>.  38(1): 57-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   59208   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Unify QSAR approach to antimicrobials. Part 1: Predicting antifungal activity against different species</p>

    <p>          Gonzalez-Diaz, Humberto, Prado-Prado, Francisco J, Santana, Lourdes, and Uriarte, Eugenio</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(17): 5973-5980</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   59209   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Synthesis and Antifungal Activity of a Novel Series of Alkyldimethylamine Cyanoboranes and Their Derivatives</p>

    <p>          Takrouri, Khuloud, Oren, Gal, Polacheck, Itzhack, Sionov, Edward, Shalom, Eli, Katzhendler, Jehoshua, and Srebnik, Morris</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(16): 4879-4885</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>20.   59210   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Transcriptional autoregulation by Mycobacterium tuberculosis PhoP involves recognition of novel direct repeat sequences in the regulatory region of the promoter</p>

    <p>          Gupta, S, Sinha, A, and Sarkar, D</p>

    <p>          FEBS Lett <b>2006</b>.  580(22): 5328-38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16979633&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16979633&amp;dopt=abstract</a> </p><br />

    <p>21.   59211   OI-LS-359; PUBMED-OI-10/16/2006</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus gene expression by small interfering RNAs using a tri-cistronic full-length viral replicon and a transient mouse model</p>

    <p>          Kim, M, Shin, D, Kim, SI, and Park, M</p>

    <p>          Virus Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16979254&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16979254&amp;dopt=abstract</a> </p><br />

    <p>22.   59212   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          In vitro antibacterial, antifungal and cytotoxic activity of some isonicotinoylhydrazide Schiff&#39;s bases and their cobalt(II), copper(II), nickel(II) and zinc(II) complexes</p>

    <p>          Chohan, Zahid H, Arif, M, Shafiq, Zahid, Yaqub, Muhammad, and Supuran, Claudiu T</p>

    <p>          Journal of Enzyme Inhibition and Medicinal Chemistry <b>2006</b>.  21(1): 95-103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   59213   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Studies on synthesis and evaluation of quantitative structure-activity relationship of 10-methyl-6-oxo-5-arylazo-6,7-dihydro-5H-[1,3]azaphospholo[1,5-d][1,4]benzodiazepin-2-phospha-3-ethoxycarbonyl-1-phosphorus dichlorides</p>

    <p>          Kumar, Ashok, Sharma, Pratibha, Gurram, VK, and Rane, Nilesh</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(9): 2484-2491</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   59214   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Crystalline forms of (1R,2S)-N-[(1,1-dimethylethoxy)carbonyl]-3-methyl-l-valyl-(4r)-4-[(6-methoxy-1-isoquinolinyl)oxy]-l-prolyl-1-amino-n-(cyclopropylsulfonyl)-2-ethenyl-cyclopropanecarboxamide, monopotassium salt and uses for anti-HCV therapy</p>

    <p>          Sausker, Justin B and Scola, Paul Michael</p>

    <p>          PATENT:  US <b>2006199773</b>  ISSUE DATE:  20060907</p>

    <p>          APPLICATION: 39  PP: 16pp., Cont.-in-part of U.S. Ser. No. 295,914.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   59215   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of 1,1-dioxido-4H-1,2,4-thiadiazines as hepatitis C polymerase inhibitors and anti-infective agents</p>

    <p>          Klein, Larry, Huang, Peggy-pei-Yu, Randolph, John T, Hutchinson, Douglas K, Yeung, Ming C, and Flentge, Charles A</p>

    <p>          PATENT:  WO <b>2006093801</b>  ISSUE DATE:  20060908</p>

    <p>          APPLICATION: 2006  PP: 172pp.</p>

    <p>          ASSIGNEE:  (Abbott Laboratories, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   59216   OI-LS-359; WOS-OI-10/8/2006</p>

    <p class="memofmt1-2">          rpoB gene mutations and molecular characterization of rifampin-resistant Mycobacterium tuberculosis isolates from Shandong Province, China</p>

    <p>          Ma, X, Wang, HY, Deng, YF, Liu, ZM, Xu, Y, Pan, X, Musser, JM, and Graviss, EA</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2006</b>.  44(9): 3409-3412, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240708000060">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240708000060</a> </p><br />

    <p>27.   59217   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Preparation and HCV anti-viral activity of bicyclo-[3.1.0]-hexane prodrugs</p>

    <p>          Jacobson, Kenneth Alan, Mackman, Richard L, and Joshi, Bhalchandra V</p>

    <p>          PATENT:  WO <b>2006091905</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 87pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   59218   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of 2-thienylacetylenes as antiviral agents</p>

    <p>          Wunberg, Tobias, Baumeister, Judith, Gottschling, Dirk, Henninger, Kerstin, Koletzki, Diana, Pernerstorfer, Josef, Urban, Andreas, Birkmann, Alexander, Harrenga, Axel, and Lobell, Mario</p>

    <p>          PATENT:  DE <b>102004061746</b>  ISSUE DATE: 20060706</p>

    <p>          APPLICATION: 2004  PP: 26 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   59219   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Serial propagation of the microsporidian Enterocytozoon bieneusi of human origin in immunocompromised rodents</p>

    <p>          Feng, Xiaochuan, Akiyoshi, Donna E, Sheoran, Abhineet, Singh, Inderpal, Hanawalt, Joel, Zhang, Quanshun, Widmer, Giovanni, and Tzipori, Saul</p>

    <p>          Infection and Immunity <b>2006</b>.  74(8): 4424-4429</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   59220   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Diagnosis and manifestation of encephalitozoonosis in mice after experimental infection with different species and application of dexamethasone</p>

    <p>          Herich, R, Levkutova, M, Kokincakova, T, Reiterova, K, Hipikova, V, and Levkut, M</p>

    <p>          Journal of veterinary medicine. A, Physiology, pathology, clinical medicine <b>2006</b>.  53(7): 340-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   59221   OI-LS-359; WOS-OI-10/8/2006</p>

    <p class="memofmt1-2">          Search of antitubercular activities in tetrahydroacridines: Synthesis and biological evaluation</p>

    <p>          Tripathi, RP, Verma, SS, Pandey, J, Agarwal, KC, Chaturvedi, V, Manju, YK, Srivastva, AK, Gaikwad, A, and Sinha, S</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(19): 5144-5147, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615400032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615400032</a> </p><br />

    <p>32.   59222   OI-LS-359; SCIFINDER-OI-10/9/2006</p>

    <p class="memofmt1-2">          Functional characterization of a putative aquaporin from Encephalitozoon cuniculi, a microsporidia pathogenic to humans</p>

    <p>          Ghosh, Kaya, Cappiello, Clint D, McBride, Sean M, Occi, James L, Cali, Ann, Takvorian, Peter M, McDonald, Thomas V, and Weiss, Louis M</p>

    <p>          International Journal for Parasitology <b>2006</b>.  36(1): 57-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   59223   OI-LS-359; WOS-OI-10/8/2006</p>

    <p class="memofmt1-2">          Nonclassical 5-Alkyl-6-substitutedarylthio-pyrrolo[2,3-d]pyrimidines as potent and selective Toxoplasma gondii dihydrofolate reductase inhibitors</p>

    <p>          Gangjee, A, Jain, HD, Kisliuk, RL, and Queener, SF</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2006</b>.  231: 1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906563">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238125906563</a> </p><br />

    <p>34.   59224   OI-LS-359; WOS-OI-10/16/2006</p>

    <p class="memofmt1-2">          Adenosine kinase from Cryptosporidium parvum</p>

    <p>          Galazka, J, Striepen, B, and Ullman, B</p>

    <p>          MOLECULAR AND BIOCHEMICAL PARASITOLOGY <b>2006</b>.  149(2): 223-230, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240776600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240776600012</a> </p><br />

    <p>35.   59225   OI-LS-359; WOS-OI-10/16/2006</p>

    <p class="memofmt1-2">          Bird flu, influenza and 1918: The case for mutant Avian tuberculosis</p>

    <p>          Broxmeyer, L</p>

    <p>          MEDICAL HYPOTHESES <b>2006</b>.  67(5): 1006-1015, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240743600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240743600002</a> </p><br />

    <p>36.   59226   OI-LS-359; WOS-OI-10/16/2006</p>

    <p class="memofmt1-2">          Conformational analysis of R207910, a new drug candidate for the treatment of tuberculosis, by a combined NMR and molecular modeling approach</p>

    <p>          Gaurrand, S, Desjardins, S, Meyer, C, Bonnet, P, Argoullon, JM, Oulyadi, H, and Guillemont, J</p>

    <p>          CHEMICAL BIOLOGY &amp; DRUG DESIGN <b>2006</b>.  68(2): 77-84, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240773400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240773400001</a> </p><br />

    <p>37.   59227   OI-LS-359; WOS-OI-10/16/2006</p>

    <p class="memofmt1-2">          Discovery of synthetic penaeidin activity against antibiotic-resistant fungi</p>

    <p>          Cuthbertson, BJ, Bullesbach, EE, and Gross, PS</p>

    <p>          CHEMICAL BIOLOGY &amp; DRUG DESIGN <b>2006</b>.  68(2): 120-127, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240773400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240773400006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
