

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-93.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yYCDQpCmaG5j54NPF17bxwIQ8o20Ey3FQHRryMW2cTJITUj+zowqKtzuphl+1WKP+0NIoEu1d9aAEOmYWUP+gHbq8c5UqFXoFbgph6+k5Mvvj6mqc10Ci04aANA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8832DF4F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-93-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56432   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The 4&#39;,4&#39;-difluoro analog of 5&#39;-noraristeromycin: A new structural prototype for possible antiviral drug development toward orthopoxvirus and cytomegalovirus</p>

    <p>          Roy, A, Schneller, SW, Keith, KA, Hartline, CB, and Kern, ER</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908221&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908221&amp;dopt=abstract</a> </p><br />

    <p>2.     56433   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Biological activities of isatin and its derivatives</p>

    <p>          Pandeya, SN, Smitha, S, Jyoti, M, and Sridhar, SK</p>

    <p>          Acta Pharm <b>2005</b>.  55(1): 27-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15907222&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15907222&amp;dopt=abstract</a> </p><br />

    <p>3.     56434   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Potential Anti-Hbv Effect of Amantadine in Combination With Ursodeoxycholic Acid and Biphenyl Dimethyl Dicarboxylate in Hepg2 2.2.15 Cells</p>

    <p>          Joo, S. and Lee, D.</p>

    <p>          Archives of Pharmacal Research <b>2005</b>.  28(4): 451-457</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228809700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228809700012</a> </p><br />

    <p>4.     56435   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-phosphatidylserine antibodies and antibody-antiviral agent conjugates for treating cancer and viral infection</p>

    <p>          Thorpe, Philip E, Soares, MMelina, and He, Jin</p>

    <p>          PATENT:  US <b>2005025761</b>  ISSUE DATE: 20050203</p>

    <p>          APPLICATION: 2003-52276  PP: 180 pp., Cont.-in-part of U.S. Ser. No. 621,269.</p>

    <p>          ASSIGNEE:  (Board of Regents, the University of Texas System USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     56436   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent and specific inhibition of SARS-CoV antigen expression by RNA interference</p>

    <p>          Tao, P, Zhang, J, Tang, N, Zhang, BQ, He, TC, and Huang, AL</p>

    <p>          Chin Med J (Engl) <b>2005</b>.  118(9): 714-719</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15899131&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15899131&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     56437   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and evaluation of isatin derivatives as effective SARS coronavirus 3CL protease inhibitors</p>

    <p>          Chen, LR, Wang, YC, Lin, YW, Chou, SY, Chen, SF, Liu, LT, Wu, YT, Kuo, CJ, Chen, TS, and Juang, SH</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15896959&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15896959&amp;dopt=abstract</a> </p><br />

    <p>7.     56438   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus core protein expression in immortalized human hepatocytes induces cytochrome c-independent increase in Apaf-1 and caspase-9 activation for cell death</p>

    <p>          Meyer, K, Basu, A, Saito, K, Ray, RB, and Ray, R</p>

    <p>          Virology <b>2005</b>.  336(2): 198-207</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892961&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892961&amp;dopt=abstract</a> </p><br />

    <p>8.     56439   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development in lipid drugs</p>

    <p>          Raulin, J</p>

    <p>          Mini Rev Med Chem <b>2005</b>.  5(5): 489-498</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892690&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892690&amp;dopt=abstract</a> </p><br />

    <p>9.     56440   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Method of preventing virus: cell fusion by inhibiting the function of the fusion initiation region in rna viruses having class i membrane fusogenic envelope proteins</p>

    <p>          Garry, Robert F and Russell, Wilson</p>

    <p>          PATENT:  WO <b>2005044992</b>  ISSUE DATE: 20050519</p>

    <p>          APPLICATION: 2004  PP: 63 pp.</p>

    <p>          ASSIGNEE:  (The Administrators of the Tulane Educational Fund, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   56441   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and in Vitro Anti-Hbv and Anti-Hcv Activities of Ring-Expanded (&quot;Fat&quot;) Nucleobases and Nucleosides Containing the Imidazo[4,5-E][1,3]Diazepine-4,8-Dione Ring System</p>

    <p>          Zhang, P., Korba, B., and Hosmane, R.</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A56-A57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900065">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900065</a> </p><br />

    <p>11.   56442   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral evaluation of plants from Brazilian Atlantic Tropical Forest</p>

    <p>          Andrighetti-Frohner, CR, Sincero, TC, da Silva, AC, Savi, LA, Gaido, CM, Bettega, JM, Mancini, M, de Almeida, MT, Barbosa, RA , Farias, MR, Barardi, CR, and Simoes, CM</p>

    <p>          Fitoterapia <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15890472&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15890472&amp;dopt=abstract</a> </p><br />

    <p>12.   56443   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of nucleosides for treating viral infections caused by a Flaviviridae family virus</p>

    <p>          Roberts, Christopher D and Keicher, Jesse D</p>

    <p>          PATENT:  US <b>2005090463</b>  ISSUE DATE: 20050428</p>

    <p>          APPLICATION: 2004-9343  PP: 37 pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   56444   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Geldanamycin, a potent and specific inhibitor of Hsp90, inhibits gene expression and replication of human cytomegalovirus</p>

    <p>          Basha, W, Kitagawa, R, Uhara, M, Imazu, H, Uechi, K, and Tanaka, J</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(2): 135-146</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15889536&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15889536&amp;dopt=abstract</a> </p><br />

    <p>14.   56445   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Methods and compositions comprising diamines as antiinfective and antitubercular therapeutics, and preparation thereof</p>

    <p>          Bogatcheva, Elena, Protopopova, Marina, and Nikonenko, Boris</p>

    <p>          PATENT:  WO <b>2005034857</b>  ISSUE DATE: 20050421</p>

    <p>          APPLICATION: 2004  PP: 104 pp.</p>

    <p>          ASSIGNEE:  (Sequella, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   56446   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, synthesis, and antipicornavirus activity of 1-[5-(4-arylphenoxy)alkyl]-3-pyridin-4-ylimidazolidin-2-one derivatives</p>

    <p>          Chang, CS, Lin, YT, Shih, SR, Lee, CC, Lee, YC, Tai, CL, Tseng, SN, and Chern, JH</p>

    <p>          J Med Chem <b>2005</b>.  48(10): 3522-3535</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15887961&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15887961&amp;dopt=abstract</a> </p><br />

    <p>16.   56447   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory Action of Sulfated Polysaccharides on Dengue Virus Infection of Human Cells</p>

    <p>          Talarico, L. and Damonte, E.</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900139">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900139</a> </p><br />
    <br clear="all">

    <p>17.   56448   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and structure-activity-relationships of 1H-imidazo[4,5-c]quinolines that induce interferon production</p>

    <p>          Gerster, JF, Lindstrom, KJ, Miller, RL , Tomai, MA, Birmachu, W, Bomersine, SN, Gibson, SJ, Imbertson, LM, Jacobson, JR, Knafla, RT, Maye, PV, Nikolaides, N, Oneyemi, FY, Parkhurst, GJ, Pecore, SE, Reiter, MJ, Scribner, LS, Testerman, TL, Thompson, NJ, Wagner, TL, Weeks, CE, Andre, JD, Lagain, D, Bastard, Y, and Lupu, M</p>

    <p>          J Med Chem <b>2005</b>.  48(10): 3481-3491</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15887957&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15887957&amp;dopt=abstract</a> </p><br />

    <p>18.   56449   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of natural compounds with antiviral activities against SARS-associated coronavirus</p>

    <p>          Li, SY, Chen, C, Zhang, HQ, Guo, HY, Wang, H, Wang, L, Zhang, X, Hua, SN, Yu, J, Xiao, PG, Li, RS, and Tan, X</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885816&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885816&amp;dopt=abstract</a> </p><br />

    <p>19.   56450   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of antiviral treatment with entecavir on age- and dose-related outcomes of duck hepatitis B virus infection</p>

    <p>          Foster, Wendy K, Miller, Darren S, Scougall, Catherine A, Kotlarski, Ieva, Colonno, Richard J, and Jilbert, Allison R</p>

    <p>          Journal of Virology <b>2005</b>.  79(9): 5819-5832</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   56451   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An anti-herpes simplex virus-type 1 agent from Xylaria mellisii (BCC 1005)</p>

    <p>          Pittayakhajonwut, Pattama, Suvannakad, Rapheephat, Thienhirun, Surang, Prabpai, Samran, Kongsaeree, Palangpon, and Tanticharoen, Morakot</p>

    <p>          Tetrahedron Letters <b>2005</b>.  46(8): 1341-1344</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   56452   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Activity of stabilized short interfering RNA in a mouse model of hepatitis B virus replication</p>

    <p>          Morrissey, DV, Blanchard, K, Shaw, L, Jensen, K, Lockridge, JA, Dickinson, B, McSwiggen, JA, Vargeese, C, Bowman, K, Shaffer, CS, Polisky, BA, and Zinnen, S</p>

    <p>          Hepatology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15880588&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15880588&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   56453   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p><b>          Structural features and molecular evolution of bowman-birk protease inhibitors and their potential application</b> </p>

    <p>          Qi, RF, Song, ZW, and Chi, CW</p>

    <p>          Acta Biochim Biophys Sin (Shanghai) <b>2005</b>.  37(5): 283-292</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15880256&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15880256&amp;dopt=abstract</a> </p><br />

    <p>23.   56454   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Duration of Infectivity and Rna of Venezuelan Equine Encephalitis, West Nile, and Yellow Fever Viruses Dried on Filter Paper and Maintained at Room Temperature</p>

    <p>          Guzman, H. <i>et al.</i></p>

    <p>          American Journal of Tropical Medicine and Hygiene <b>2005</b> .  72(4): 474-477</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228303400022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228303400022</a> </p><br />

    <p>24.   56455   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rhodanine compositions for use as antiviral agents</p>

    <p>          Rajinder, Singh, Usha, Ramesh, Clough, Jeffrey, Issakani, Sarkiz D, and Look, Gary Charles</p>

    <p>          PATENT:  WO <b>2005041951</b>  ISSUE DATE: 20050512</p>

    <p>          APPLICATION: 2004  PP: 82 pp.</p>

    <p>          ASSIGNEE:  (Rigel Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   56456   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, Synthesis and Antiviral Activity of Novel 4,5-Disubstituted 7-(b-D-Ribofuranosyl)pyrrolo[2,3-d][1,2,3]triazines and the Novel 3-Amino-5-methyl-1-(b-D-ribofuranosyl)- and 3-Amino-5-methyl-1-(2-deoxy-b-D-ribofuranosyl)-1,5-dihydro-1,4,5,6,7,8-hexaazaacenaphthylene as Analogues of Triciribine</p>

    <p>          Migawa, Michael T, Drach, John C, and Townsend, Leroy B</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(11): 3840-3851</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   56457   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Method for the treatment of herpes with cetyl myristate and cetyl palmitate</p>

    <p>          Meakin, Timothy David, Heatley, Craig Leonard, and Cadwallader, Dianne</p>

    <p>          PATENT:  NZ <b>524311</b>  ISSUE DATE: 20050225</p>

    <p>          APPLICATION: 2001  PP: 10 pp.</p>

    <p>          ASSIGNEE:  (Meracol Corporation Limited, N. Z.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   56458   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of naturally occurring amino acid variations that affect the ability of the measles virus C protein to regulate genome replication and transcription</p>

    <p>          Bankamp, B, Wilson, J, Bellini, WJ, and Rota, PA</p>

    <p>          Virology <b>2005</b>.  336(1): 120-129</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15866077&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15866077&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   56459   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Purinyl Homo-Carbonucleoside Derivatives of 2-Benzylcyclo-Penta[C]Pyrazol</p>

    <p>          Garcia, M. <i>et al.</i></p>

    <p>          Synthesis-Stuttgart <b>2005</b>.(6): 925-932</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228616300008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228616300008</a> </p><br />

    <p>29.   56460   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Small Interfering Rna Inhibits Sars-Cov Nucleocapsid Gene Expression in Cultured Cells and Mouse Muscles</p>

    <p>          Zhao, P. <i>et al.</i></p>

    <p>          Febs Letters <b>2005</b>.  579(11): 2404-2410</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228756400027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228756400027</a> </p><br />

    <p>30.   56461   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of screening substrates for the directed evolution of sialic acid aldolase: towards tailored enzymes for the preparation of influenza A sialidase inhibitor analogues</p>

    <p>          Woodhall, T, Williams, G, Berry, A, and Nelson, A</p>

    <p>          Org Biomol Chem <b>2005</b>.  3(9): 1795-1800</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15858666&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15858666&amp;dopt=abstract</a> </p><br />

    <p>31.   56462   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Amantadine inhibits hepatitis A virus internal ribosomal entry site-mediated translation in human hepatoma cells</p>

    <p>          Kanda, T, Yokosuka, O, Imazeki, F, Fujiwara, K, Nagao, K, and Saisho, H</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  331(2): 621-629</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850805&amp;dopt=abstract</a> </p><br />

    <p>32.   56463   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SOCS-1 inhibits expression of the antiviral proteins 2&#39;,5&#39;-OAS and MxA induced by the novel interferon-lambdas IL-28A and IL-29</p>

    <p>          Brand, S, Zitzmann, K, Dambacher, J, Beigel, F, Olszak, T, Vlotides, G, Eichhorst, ST, Goke, B, Diepolder, H, and Auernhammer, CJ</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  331(2): 543-548</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850793&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850793&amp;dopt=abstract</a> </p><br />

    <p>33.   56464   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification and use of antiviral compounds that inhibit interaction of host cell proteins and viral proteins required for viral replication</p>

    <p>          Palese, Peter and O&#39;Neill, Robert</p>

    <p>          PATENT:  US <b>6890710</b>  ISSUE DATE: 20050510</p>

    <p>          APPLICATION: 95-51778  PP: 66 pp., Cont.-in-part of U.S. Ser. No. 246,583.</p>

    <p>          ASSIGNEE:  (Mount Sinai School of Medicine of New York University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   56465   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pathogenic significance of alpha-N-acetylgalactosaminidase activity found in the hemagglutinin of influenza virus</p>

    <p>          Yamamoto, N and Urade, M</p>

    <p>          Microbes Infect <b>2005</b>.  7(4): 674-681</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15848273&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15848273&amp;dopt=abstract</a> </p><br />

    <p>35.   56466   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Study on the resistance of severe acute respiratory syndrome-associated coronavirus</p>

    <p>          Wang, XW, Li, JS, Jin, M, Zhen, B, Kong, QX, Song, N,  Xiao, WJ, Yin, J, Wei, W, Wang, GJ, Si, BY, Guo, BZ, Liu, C, Ou, GR, Wang, MN, Fang, TY, Chao, FH, and Li, JW</p>

    <p>          J Virol Methods <b>2005</b>.  126(1-2): 171-177</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15847934&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15847934&amp;dopt=abstract</a> </p><br />

    <p>36.   56467   DMID-LS-93; PUBMED-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian Influenza</p>

    <p>          Zeitlin, GA and Maslow, MJ</p>

    <p>          Curr Infect Dis Rep <b>2005</b>.  7(3): 193-199</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15847721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15847721&amp;dopt=abstract</a> </p><br />

    <p>37.   56468   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-influenza A viral drug - amantadine</p>

    <p>          Miyamoto, Daisei and Suzuki, Yasuo</p>

    <p>          Sogo Rinsho <b>2005</b>.  54(2): 352-357</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   56469   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Natural Inhibitor of Influenza a-Pr8 Extracted From Cinnamon</p>

    <p>          Barak, I. and Ovadia, M.</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900085">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900085</a> </p><br />

    <p>39.   56470   DMID-LS-93; WOS-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Smallpox Antivirals: in Vitro Assay for Vaccinia Virus 17l Enzymatic Cleavage of Core Protein Precursors</p>

    <p>          Byrd, C. and Hruby, D.</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900120">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900120</a> </p><br />
    <br clear="all">

    <p>40.   56471   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of phenolic polymers and cyclo-Saligenyl-pronucleotides towards SARS-related Coronavirus</p>

    <p>          Schmidtke, Michaela, Meier, Chris, Schacke, Michael, Helbig, Bjoern, Makarov, Vadim, Rabenau, Holger F, Cinatl, Jindrich Jr, and Wutzler, Peter</p>

    <p>          Chemotherapie Journal <b>2005</b>.  14(1): 16-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   56472   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-coronavirus agent</p>

    <p>          Sugiura, Koji and Ono, Yasuharu</p>

    <p>          PATENT:  WO <b>2005037296</b>  ISSUE DATE: 20050428</p>

    <p>          APPLICATION: 2003  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (Toagosei Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   56473   DMID-LS-93; SCIFINDER-DMID-5/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of novel cycloalkyl[b] condensed indoles for treating human papillomaviruses</p>

    <p>          Boggs, Sharon Davis, Catalano, John G, Gudmundsson, Kristjan S, D&#39;Aurora Richardson, Leah, and Sebahar, Paul Richard</p>

    <p>          PATENT:  WO <b>2005023245</b>  ISSUE DATE: 20050317</p>

    <p>          APPLICATION: 2004  PP: 89 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
