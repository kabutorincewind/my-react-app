

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-83.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="m/lci1BpZxrHcvWOTPg3Cq1c+1Fc1tO4V7u/GRfM/rF4yqtjTHavj/ywKG/TdjB8VLJpWcyJ4sAyQF1Z6Qc2L5DYdMEIuG5hIaBm+NQaYsthLL6vLsi3ocarW8A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AE566649" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-83-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56003   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Adaptation of West Nile virus replicons to cells in culture and use of replicon-bearing cells to probe antiviral action</p>

    <p>          Rossi, Shannan L, Zhao, Qizu, O&#39;Donnell, Vivian K, and Mason, Peter W</p>

    <p>          Virology <b>2005</b>.  331(2): 457-470</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     56004   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evolution and Variation of the SARS-CoV Genome</p>

    <p>          Hu, J, Wang, J, Xu, J, Li, W, Han, Y, Li, Y, Ji, J, Ye, J, Xu, Z, Zhang, Z, Wei, W, Li, S, Wang, J, Wang, J, Yu, J, and Yang, H</p>

    <p>          Genomics Proteomics Bioinformatics <b>2003</b>.  1(3): 216-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629034&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629034&amp;dopt=abstract</a> </p><br />

    <p>3.     56005   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          FK778, A Synthetic Malononitrilamide</p>

    <p>          Fitzsimmons, WE and First, MR</p>

    <p>          Yonsei Med J <b>2004</b>.  45(6): 1132-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15627308&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15627308&amp;dopt=abstract</a> </p><br />

    <p>4.     56006   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Suppression of hepatitis C virus replicon by TGF-b</p>

    <p>          Murata, Takayuki, Ohshima, Takayuki, Yamaji, Masashi, Hosaka, Masahiro, Miyanari, Yusuke, Hijikata, Makoto, and Shimotohno, Kunitada</p>

    <p>          Virology <b>2005</b>.  331(2): 407-417</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     56007   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory properties of nucleic acid-binding ligands on protein synthesis</p>

    <p>          Malina, A, Khan, S, Carlson, CB, Svitkin, Y, Harvey, I, Sonenberg, N, Beal, PA, and Pelletier, J</p>

    <p>          FEBS Lett <b>2005</b>.  579(1): 79-89</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15620694&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15620694&amp;dopt=abstract</a> </p><br />

    <p>6.     56008   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A strategy for obtaining near full-length HCV cDNA clones (assemblicons) by assembly PCR</p>

    <p>          Sheehy, P, Scallan, M, Kenny-Walsh, E, Shanahan, F, and Fanning, LJ</p>

    <p>          Journal of Virological Methods <b>2005</b>.  123(2): 115-124</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     56009   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Albuferon (Tm) - a Novel Therapeutic Agent for Hepatitis C: Results of a Phase 1/2 Study in Treatment Experienced Subjects With Chronic Hepatitis C</p>

    <p>          Balan, V. <i>et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(4): 280A-281A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100267">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100267</a> </p><br />

    <p>8.     56010   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Expression of Hcv Protease in the Liver of Mice Results in Liver Injury Which Can Be Inhibited by Vx-950, a Vertex Hcv Protease Inhibitor</p>

    <p>          Kalkeri, G. <i>et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(4): 281A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100268">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100268</a> </p><br />

    <p>9.     56011   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of respiratory viruses by nasally administered siRNA</p>

    <p>          Bitko, V, Musiyenko, A, Shulyayeva, O, and Barik, S</p>

    <p>          Nat Med <b>2004</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15619632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15619632&amp;dopt=abstract</a> </p><br />

    <p>10.   56012   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of respiratory syncytial virus infection with intranasal siRNA nanoparticles targeting the viral NS1 gene</p>

    <p>          Zhang, W, Yang, H, Kong, X, Mohapatra, S, Juan-Vergara, HS, Hellermann, G, Behera, S, Singam, R, Lockey, RF, and Mohapatra, SS</p>

    <p>          Nat Med <b>2004</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15619625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15619625&amp;dopt=abstract</a> </p><br />

    <p>11.   56013   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Anti-Viral Effects of Interferon and Ribavirin at Their Clinical Concentrations Against Hepatitis C Virus.</p>

    <p>          Kato, T. <i>et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(4): 321A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100366">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100366</a> </p><br />

    <p>12.   56014   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Impact of mycophenolate mofetil versus azathioprine on early recurrence of hepatitis C after liver transplantation</p>

    <p>          Kornberg, A, Kuepper, B, Tannapfel, A, Hommann, M, and Scheele, J</p>

    <p>          International Immunopharmacology <b>2005</b>.  5(1): 107-115</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   56015   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          (Z)- and (E)-[2-Fluoro-2-(hydroxymethyl)cyclopropylidene]methylpurines and -pyrimidines, a New Class of Methylenecyclopropane Analogues of Nucleosides: Synthesis and Antiviral Activity(1)</p>

    <p>          Zhou, S, Kern, ER, Gullen, E, Cheng, YC, Drach, JC, Matsumi, S, Mitsuya, H, and Zemlicka, J</p>

    <p>          J Med Chem <b>2004</b>.  47(27): 6964-6972</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615545&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615545&amp;dopt=abstract</a> </p><br />

    <p>14.   56016   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Phase Ii, Randomized Trial Evaluating the Safety, Pharmacokinetics and Antiviral Activity of Clevudine for 12 Weeks in Patients With Chronic Hepatitis B</p>

    <p>          Marcellin, P. <i> et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(4): 652A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101131">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101131</a> </p><br />

    <p>15.   56017   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p><b>          Non-Nucleoside Benzimidazole-Based Allosteric Inhibitors of the Hepatitis C Virus NS5B Polymerase: Inhibition of Subgenomic Hepatitis C Virus RNA Replicons in Huh-7 Cells</b> </p>

    <p>          Beaulieu, PL, Bousquet, Y, Gauthier, J, Gillard, J, Marquis, M, McKercher, G, Pellerin, C, Valois, S, and Kukolj, G</p>

    <p>          J Med Chem <b>2004</b>.  47(27): 6884-92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615537&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615537&amp;dopt=abstract</a> </p><br />

    <p>16.   56018   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          N-1-Aza-4-Hydroxyquinolone Benzothiadiazines: Inhibitors of Hcv Genotype 1 Ns5b Rna-Dependent Rna Polymerase Exhibiting Replicon Potency and Favorable Rat Pharmacokinetics.</p>

    <p>          Pratt, J. <i>et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(4): 697A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101227">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101227</a> </p><br />

    <p>17.   56019   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Final Phase I/Ii Trial Results for Nm283, a New Polymerase Inhibitor for Hepatitis C: Antiviral Efficacy and Tolerance in Patients With Hcv-1 Infection, Including Previous Interferon Failures</p>

    <p>          Afdhal, N. <i>et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(4): 726A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101292">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101292</a> </p><br />

    <p>18.   56020   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sensitivity of Hepatitis C Virus Rna to the Antiviral Enzyme Ribonuclease L Is Determined by a Subset of Efficient Cleavage Sites</p>

    <p>          Han, J. <i>et al.</i></p>

    <p>          Journal of Interferon and Cytokine Research <b>2004</b>.  24(11): 664-676</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225268900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225268900005</a> </p><br />
    <br clear="all">

    <p>19.   56021   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hcv Infective Virions Can Be Carried by Human Platelets</p>

    <p>          Pugliese, A. <i>et al.</i></p>

    <p>          Cell Biochemistry and Function <b>2004</b>.  22(6): 353-358</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225253500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225253500002</a> </p><br />

    <p>20.   56022   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The pneumonia virus of mice infection model for severe respiratory syncytial virus infection: identifying novel targets for therapeutic intervention</p>

    <p>          Rosenberg, Helene F, Bonville, Cynthia A, Easton, Andrew J, and Domachowske, Joseph B</p>

    <p>          Pharmacology &amp; Therapeutics <b>2005</b>.  105(1): 1-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   56023   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Receptor modulation in viral replication: HIV, HSV, HHV-8 and HPV: same goal, different techniques to interfere with MHC-I antigen presentation</p>

    <p>          Piguet, V</p>

    <p>          Curr Top Microbiol Immunol <b>2005</b>.  285: 199-217</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15609505&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15609505&amp;dopt=abstract</a> </p><br />

    <p>22.   56024   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The management of chronic viral hepatitis: A Canadian consensus conference 2004</p>

    <p>          Sherman, M, Bain, V, Villeneuve, JP, Myers, RP, Cooper, C, Martin, S, and Lowe, C</p>

    <p>          Can J Gastroenterol <b>2004</b>.  18(12): 715-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15605136&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15605136&amp;dopt=abstract</a> </p><br />

    <p>23.   56025   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity against Coxsackie virus B3 of some novel benzimidazole derivatives</p>

    <p>          Cheng, Jun, Xie, Jiangtao, and Luo, Xianjin</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(2): 267-269</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   56026   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Studies on Acyclovir-Dextran Conjugate: Synthesis and Pharmacokinetics</p>

    <p>          Tu, J., Zhong, S., and Li, P.</p>

    <p>          Drug Development and Industrial Pharmacy <b>2004</b>.  30(9): 959-965</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224989700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224989700006</a> </p><br />

    <p>25.   56027   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Modulation of drug metabolism and antiviral therapies</p>

    <p>          Lauterburg, Bernhard H <b>2005</b>.: 233-251</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>26.   56028   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Implication of proprotein convertases in the processing and spread of severe acute respiratory syndrome coronavirus</p>

    <p>          Bergeron, E, Vincent, MJ, Wickham, L, Hamelin, J, Basak, A, Nichol, ST, Chretien, M, and Seidah, NG</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  326(3): 554-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15596135&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15596135&amp;dopt=abstract</a> </p><br />

    <p>27.   56029   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of an Efficient and Scalable Process of a Respiratory Syncytial Virus Inhibitor</p>

    <p>          Provencal, D. <i> et al.</i></p>

    <p>          Organic Process Research &amp; Development <b>2004</b>.  8(6): 903-908</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225340900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225340900013</a> </p><br />

    <p>28.   56030   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mode of selection and experimental evolution of antiviral drugs resistance in vesicular stomatitis virus</p>

    <p>          Cuevas, Jose M, Sanjuan, Rafael, Moya, Andres, and Elena, Santiago F</p>

    <p>          Infection, Genetics and Evolution <b>2005</b>.  5(1): 55-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   56031   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Therapeutic effects of dengue 2 virus capsid protein and staphylococcal nuclease fusion protein on dengue-infected cell cultures</p>

    <p>          Qin, CF, Qin, E, Yu, M, Chen, SP, Jiang, T, Deng, YQ, Duan, HY, and Zhao, H</p>

    <p>          Arch Virol <b>2004</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592886&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592886&amp;dopt=abstract</a> </p><br />

    <p>30.   56032   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ribavirin and interferon-b synergistically inhibit SARS-associated coronavirus replication in animal and human cell lines</p>

    <p>          Morgenstern, Birgit, Michaelis, Martin, Baer, Patrick C, Doerr, Hans W, and Cinatl, Jindrich</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  326(4): 905-908</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   56033   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacokinetics and Pharmacodynamics of Pegylated Interferon Alfa-2a or Alfa-2b With Ribavirin in Treatment Naive Patients With Genotype 1 Chronic Hepatitis C</p>

    <p>          Di Bisceglie, A. <i>et al.</i></p>

    <p>          Hepatology <b>2004</b>.  40(4): 734A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101307">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101307</a> </p><br />

    <p>32.   56034   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Quaternary Structure of the Severe Acute Respiratory Syndrome (Sars) Coronavirus Main Protease</p>

    <p>          Chou, C. <i>et al.</i></p>

    <p>          Biochemistry <b>2004</b>.  43(47): 14958-14970</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225353300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225353300010</a> </p><br />

    <p>33.   56035   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Picornavirus IRES: structure function relationship</p>

    <p>          Martinez-Salas, E and Fernandez-Miragall, O</p>

    <p>          Curr Pharm Des  <b>2004</b>.  10(30): 3757-67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579069&amp;dopt=abstract</a> </p><br />

    <p>34.   56036   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of phenylacetamides and phenyl ureas as inhibitors of papilloma virus</p>

    <p>          Tsantrizos, Youla S, Faucher, Anne-Marie, Rancourt, Jean, and White, Peter</p>

    <p>          PATENT:  WO <b>2004108673</b>  ISSUE DATE:  20041216</p>

    <p>          APPLICATION: 2004  PP: 80 pp.</p>

    <p>          ASSIGNEE:  (Boehringer Ingelheim International G.m.b.H., Germany and Boehringer Ingelheim Pharma G.m.b.H. &amp; Co. K.-G.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   56037   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Herpes simplex virus type 1 amplicons and their hybrid virus partners, EBV, AAV, and retrovirus</p>

    <p>          Oehmig, A, Fraefel, C, Breakefield, XO, and Ackermann, M</p>

    <p>          Curr Gene Ther  <b>2004</b>.  4(4): 385-408</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578989&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578989&amp;dopt=abstract</a> </p><br />

    <p>36.   56038   DMID-LS-83; PUBMED-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent advances in rhinovirus therapeutics</p>

    <p>          Charles, CH, Yelmene, M, and Luo, GX</p>

    <p>          Curr Drug Targets Infect Disord <b>2004</b>.  4(4): 331-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578974&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578974&amp;dopt=abstract</a> </p><br />

    <p>37.   56039   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of heterocyclic peptides as hepatitis C inhibitors</p>

    <p>          Llinas-Brunet, Montse</p>

    <p>          PATENT:  US <b>2004229818</b>  ISSUE DATE:  20041118</p>

    <p>          APPLICATION: 2004-5555  PP: 42 pp.</p>

    <p>          ASSIGNEE:  (Boehringer Ingelheim International Gmbh, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   56040   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Tri-peptide hepatitis c serine protease inhibitors</p>

    <p>          Miao, Zhenwei, Sun, Ying, Nakajima, Suanne, Tang, Datong, Wang, Zhe, and Or, Yat Sun</p>

    <p>          PATENT:  WO <b>2004113365</b>  ISSUE DATE:  20041229</p>

    <p>          APPLICATION: 2004  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (Enanta Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>39.   56041   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis c virus inhibitors</p>

    <p>          Lim, Jae-Hong, Yoon, Joo-Yong, Song, Jeong-Uk, Sung, Lee-Taek, Choi, Sung-Pil, Song, Ho-Young, Kim, Jong-Yup, Kim, Yong-Zu, Cho, Young-Gyu, Kim, Chang-Myung, Kim, Won-Sup, Kang, Seung-Wan, and Park, Ji-Hyun</p>

    <p>          PATENT:  WO <b>2004111013</b>  ISSUE DATE:  20041223</p>

    <p>          APPLICATION: 2004  PP: 147 pp.</p>

    <p>          ASSIGNEE:  (LG Life Sciences Ltd., S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   56042   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pyridine n-oxides as antiviral agents</p>

    <p>          Colarusso, Stefania and Narjes, Frank</p>

    <p>          PATENT:  WO <b>2004110442</b>  ISSUE DATE:  20041223</p>

    <p>          APPLICATION: 2004  PP: 36 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   56043   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of [(morpholino)ethylamino]pyridine derivatives as antiviral agents</p>

    <p>          Kim, Jong-Woo, Lee, Sang-Wook, Lee, Geun-Hyung, Han, Jae-Jin, Park, Sang-Jin, Park, Eul-Yong, and Shin, Joong-Chul</p>

    <p>          PATENT:  WO <b>2004108719</b>  ISSUE DATE:  20041216</p>

    <p>          APPLICATION: 2004  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (B &amp; C Biopharm Co., Ltd. S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   56044   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of substituted imidazole derivatives as antiviral agents</p>

    <p>          Roberts, Christopher Don, Shi, Dong-Fang, and Griffith, Ronald Conrad</p>

    <p>          PATENT:  WO <b>2004108687</b>  ISSUE DATE:  20041216</p>

    <p>          APPLICATION: 2004  PP: 54 pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   56045   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Conventional Protein Kinase C Inhibition Prevents Alpha Interferon-Mediated Hepatitis C Virus Replicon Clearance by Impairing Stat Activation</p>

    <p>          Fimia, G. <i>et al.</i></p>

    <p>          Journal of Virology <b>2004</b>.  78(23): 12809-12816</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500009</a> </p><br />

    <p>44.   56046   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-viral activity of cationic peptides of the cathelicidin family including LL-37, homologs and variants thereof, and diagnosing atopic dermatitis by detecting LL-37 or its gene</p>

    <p>          Gallo, Richard L, Leung, Donald YM, and Jones, James F</p>

    <p>          PATENT:  WO <b>2004098536</b>  ISSUE DATE:  20041118</p>

    <p>          APPLICATION: 2004  PP: 55 pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA and National Jewish Medical and Research Center)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>45.   56047   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Induction of gene mutations by 5-(2-chloroethyl)-2&#39;-deoxyuridine (CEDU), an antiviral pyrimidine nucleoside analogue</p>

    <p>          Suter, Willi, Plappert-Helbig, Ulla, Glowienke, Susanne, Poetter-Locher, Franziska, Staedtler, Frank, Racine, Robert, and Martus, Hans-Joerg</p>

    <p>          Mutation Research <b>2004</b>.  568(2): 195-209</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   56048   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Tyrosine Dephosphorylation of Stat3 in Sars Coronavirus-Infected Vero E6 Cells</p>

    <p>          Mizutani, T. <i>et al.</i></p>

    <p>          Febs Letters <b>2004</b>.  577(1-2): 187-192</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225128600032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225128600032</a> </p><br />

    <p>47.   56049   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of certain 6-(arylthio)uracils and related derivatives as potential antiviral agents</p>

    <p>          El-Emam, Ali A, Massoud, Mohamed AM, El-Bendary, Eman R, and El-Sayed, Magda A</p>

    <p>          Bulletin of the Korean Chemical Society <b>2004</b>.  25(7): 991-996</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>48.   56050   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of thienopyridine-ethanolamine derivatives as antiviral agents</p>

    <p>          Schnute, Mark E, Cudahy, Michele M, Eggen, Marijean, Anderson, David J, Judge, Thomas M, Kim, Euibong J, and Collier, Sarah A</p>

    <p>          PATENT:  WO <b>2004106345</b>  ISSUE DATE:  20041209</p>

    <p>          APPLICATION: 2003  PP: 95 pp.</p>

    <p>          ASSIGNEE:  (Pharmacia &amp; Upjohn Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>49.   56051   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of cytidine nucleoside analogs as antiviral agents</p>

    <p>          Girardet, Jean-Luc, Koh, Yung-Hyo, An, Haoyun, and Hong, Zhi</p>

    <p>          PATENT:  WO <b>2004080466</b>  ISSUE DATE:  20040923</p>

    <p>          APPLICATION: 2003  PP: 59 pp.</p>

    <p>          ASSIGNEE:  (Ribapharm Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>50.   56052   DMID-LS-83; WOS-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent and Selective Inhibition of Sars Coronavirus Replication by Aurintricarboxylic Acid (Vol 320, Pg 1199, 2004)</p>

    <p>          Adonov, A. <i>et al.</i></p>

    <p>          Biochemical and Biophysical Research Communications <b>2004</b>.  324(3): 1152-1153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224807800030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224807800030</a> </p><br />

    <p>51.   56053   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-influenza virus compounds comprising biflavonoid-sialic acid glycoside</p>

    <p>          Yamada, Haruki, Nagai, Takayuki, and Takahashi, Kunio</p>

    <p>          PATENT:  WO <b>2004076471</b>  ISSUE DATE:  20040910</p>

    <p>          APPLICATION: 2003  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (The Kitasato Institute, Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>52.   56054   DMID-LS-83; SCIFINDER-DMID-1/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cromoglycic acid and its salts as antiviral agents for prevention and treatment of influenza</p>

    <p>          Hiroi, Jun, Tsujii, Eisaku, Suzuki, Yasuo, Hidari, Ippachi, and Miyatake, Akihiko</p>

    <p>          PATENT:  JP <b>2004352712</b>  ISSUE DATE:  20041216</p>

    <p>          APPLICATION: 2004-6194  PP: 11 pp.</p>

    <p>          ASSIGNEE:  (Fujisawa Pharmaceutical Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
