

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-323.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5/tjSz9DvUv8B8TC0KaBnBNVkaOGHzZ2UQ8r9EUVW+P0XAe77sDYVKwe7ZIo0XFXU3Zu/2H7CF7QaaLG48eKwbViCJj5AoQlQogzcwRHN8Qco6sfcRsLBHSyT5Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8298EDA3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-323-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45323   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          TMC114, a Novel Human Immunodeficiency Virus Type 1 Protease Inhibitor Active against Protease Inhibitor-Resistant Viruses, Including a Broad Range of Clinical Isolates</p>

    <p>          De Meyer, S, Azijn, H, Surleraux, D, Jochmans, D, Tahri, A, Pauwels, R, Wigerinck, P, and de Bethune, MP</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(6): 2314-2321</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917527&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917527&amp;dopt=abstract</a> </p><br />

    <p>2.     45324   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Structure-Based Design, Parallel Synthesis, Structure-Activity Relationship, and Molecular Modeling Studies of Thiocarbamates, New Potent Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitor Isosteres of Phenethylthiazolylthiourea Derivatives</p>

    <p>          Ranise, A, Spallarossa, A, Cesarini, S , Bondavalli, F, Schenone, S, Bruno, O, Menozzi, G, Fossa, P, Mosti, L, La Colla, M, Sanna, G, Murreddu, M, Collu, G, Busonera, B, Marongiu, ME, Pani, A, La Colla, P, and Loddo, R</p>

    <p>          J Med Chem <b>2005</b>.  48(11): 3858-3873</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15916438&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15916438&amp;dopt=abstract</a> </p><br />

    <p>3.     45325   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Synthesis, Antiviral Activity, and Mechanism of Drug Resistance of d- and l-2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-dideoxy-2&#39;-fluorocarbocyclic Nucleosides</p>

    <p>          Wang, J, Jin, Y, Rapp, KL, Bennett, M, Schinazi, RF, and Chu, CK</p>

    <p>          J Med Chem <b>2005</b>.  48(11): 3736-3748</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15916425&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15916425&amp;dopt=abstract</a> </p><br />

    <p>4.     45326   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Specific inhibition of HIV-1 replication by short hairpin RNAs targeting human cyclin T1 without inducing apoptosis</p>

    <p>          Li, Z, Xiong, Y, Peng, Y, Pan, J, Chen, Y, Wu, X, Hussain, S, Tien, P, and Guo, D</p>

    <p>          FEBS Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15913611&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15913611&amp;dopt=abstract</a> </p><br />

    <p>5.     45327   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Rational design of highly potent HIV-1 fusion inhibitory proteins: Implication for developing antiviral therapeutics</p>

    <p>          Ni, L, Gao, GF, and Tien, P</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15913557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15913557&amp;dopt=abstract</a> </p><br />

    <p>6.     45328   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          2H-Pyrrolo[3,4-b] [1,5]benzothiazepine derivatives as potential inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Di Santo, R and Costi, R</p>

    <p>          Farmaco <b>2005</b> .  60(5): 385-392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15910811&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15910811&amp;dopt=abstract</a> </p><br />

    <p>7.     45329   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Analogues of the styrylquinoline and styrylquinazoline HIV-1 integrase inhibitors: design and synthetic problems</p>

    <p>          Polanski, J, Niedbala, H, Musiol, R, Tabak, D, Podeszwa, B, Gieleciak, R, Bak, A, Palka, A, and Magdziarz, T</p>

    <p>          Acta Pol Pharm  <b>2004</b>.  61 Suppl: 3-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15909921&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15909921&amp;dopt=abstract</a> </p><br />

    <p>8.     45330   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Screening of selected wood-damaging fungi for the HIV-1 reverse transcriptase inhibitors</p>

    <p>          Mlinaric, A, Kac, J, and Pohleven, F</p>

    <p>          Acta Pharm <b>2005</b>.  55(1): 69-79</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15907225&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15907225&amp;dopt=abstract</a> </p><br />

    <p>9.     45331   HIV-LS-323; SCIFINDER-HIV-5/24/2005</p>

    <p class="memofmt1-2">          Structure-Activity Relationship Studies of a Series of Antiviral and Antibacterial Aglycon Derivatives of the Glycopeptide Antibiotics Vancomycin, Eremomycin, and Dechloroeremomycin</p>

    <p>          Printsevskaya, Svetlana S, Solovieva, Svetlana E, Olsufyeva, Eugenia N, Mirchink, Elena P, Isakova, Elena B, De Clercq, Erik, Balzarini, Jan, and Preobrazhenskaya, Maria N</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(11): 3885-3890</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   45332   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by RNA targeted against the LTR region</p>

    <p>          Puerta-Fernandez, E, Jesus, AB, Romero-Lopez, C, Tapia, N, Martinez, MA, and Berzal-Herranz, A </p>

    <p>          AIDS <b>2005</b>.  19 (9): 863-870</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15905666&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15905666&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   45333   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          In vitro anti-HIV potency of stampidine alone and in combination with standard anti-HIV drugs</p>

    <p>          Uckun, FM, Qazi, S, and Venkatachalam, TK</p>

    <p>          Arzneimittelforschung <b>2005</b>.  55(4): 223-231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15901046&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15901046&amp;dopt=abstract</a> </p><br />

    <p>12.   45334   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Beta-lactam compounds as apparently uncompetitive inhibitors of HIV-1 protease</p>

    <p>          Sperka, T, Pitlik, J, Bagossi, P, and Tozser, J</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893929&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15893929&amp;dopt=abstract</a> </p><br />

    <p>13.   45335   HIV-LS-323; SCIFINDER-HIV-5/24/2005</p>

    <p class="memofmt1-2">          Studies on the Mechanism of Inactivation of the HIV-1 Nucleocapsid Protein NCp7 with 2-Mercaptobenzamide Thioesters</p>

    <p>          Jenkins, Lisa MMiller, Byrd, JCalvin, Hara, Toshiaki, Srivastava, Pratibha, Mazur, Sharlyn J, Stahl, Stephen J, Inman, John K, Appella, Ettore, Omichinski, James G, and Legault, Pascale</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(8): 2847-2858</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   45336   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Synthesis of 5&#39;-triphosphate mimics (P3Ms) of 3&#39;-azido-3&#39;,5&#39;-dideoxythymidine and 3&#39;,5&#39;-dideoxy-5&#39;-difluoromethylenethymidine as HIV-1 reverse transcriptase inhibitors</p>

    <p>          Rajwanshi, VK, Prhavc, M, Fagan, P , Brooks, JL, Hurd, T, Cook, PD, and Wang, G</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(3): 179-189</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892257&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15892257&amp;dopt=abstract</a> </p><br />

    <p>15.   45337   HIV-LS-323; SCIFINDER-HIV-5/24/2005</p>

    <p class="memofmt1-2">          Arylsulphones: a promising class of non-nucleoside antiviral agents</p>

    <p>          Garuti, Laura, Roberti, Marinella, Pizzirani, Daniela, and Poggi, Gabriella</p>

    <p>          Current Medicinal Chemistry: Anti-Infective Agents <b>2005</b>.  4(2): 167-183</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   45338   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          KP-1212/1461, a nucleoside designed for the treatment of HIV by viral mutagenesis</p>

    <p>          Harris, KS, Brabant, W, Styrchak, S, Gall, A, and Daifuku, R</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15890415&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15890415&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   45339   HIV-LS-323; SCIFINDER-HIV-5/24/2005</p>

    <p class="memofmt1-2">          Preparation of pyrrolidinecarboxamides and related carboxamides as HIV protease inhibitors for pharmaceutical use against AIDS</p>

    <p>          Alegria, Larry Andrew, Dress, Klaus Ruprecht, Huang, Buwen, Kumpf, Robert Arnold, Lewis, Kathleen Kingsley, Matthews, Jean Joo, Sakata, Sylvie Kim, and Wallace, Michael Brennan</p>

    <p>          PATENT:  WO <b>2005026114</b>  ISSUE DATE:  20050324</p>

    <p>          APPLICATION: 2004  PP: 159 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   45340   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Inhibition of cellular HIV-1 protease activity by LYSYL-tRNA synthetase</p>

    <p>          Guo, F, Gabor, J, Cen, S, Hu, K, Mouland, A, and Kleiman, L</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888436&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888436&amp;dopt=abstract</a> </p><br />

    <p>19.   45341   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Effects of HIV Q151M-associated multi-drug resistance mutations on the activities of (-)-beta-d-1&#39;,3&#39;-dioxolan guanine</p>

    <p>          Feng, JY, Myrick, F, Selmi, B, Deval, J, Canard, B, and Borroto-Esoda, K</p>

    <p>          Antiviral Res <b>2005</b>.  66(2-3): 153-158</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885814&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885814&amp;dopt=abstract</a> </p><br />

    <p>20.   45342   HIV-LS-323; PUBMED-HIV-5/31/2005</p>

    <p class="memofmt1-2">          Chemoenzymatic Synthesis of HIV-1 gp41 Glycopeptides: Effects of Glycosylation on the Anti-HIV Activity and alpha-Helix Bundle-Forming Ability of Peptide C34</p>

    <p>          Wang, LX, Song, H, Liu, S, Lu, H, Jiang, S, Ni, J, and Li, H</p>

    <p>          Chembiochem <b>2005</b>.  6(6): 1068-1074</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15883971&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15883971&amp;dopt=abstract</a> </p><br />

    <p>21.   45343   HIV-LS-323; SCIFINDER-HIV-5/24/2005</p>

    <p class="memofmt1-2">          Targeted delivery of antiviral compounds through hemoglobin bioconjugates</p>

    <p>          Adamson, JGordon, Bell, David, Ng, Nancy, and Biessels, Pieter</p>

    <p>          PATENT:  US <b>2005059576</b>  ISSUE DATE:  20050317</p>

    <p>          APPLICATION: 2004-60165  PP: 32 pp., Cont.-in-part of U.S. Ser. No. 231,062.</p>

    <p>          ASSIGNEE:  (Can.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   45344   HIV-LS-323; SCIFINDER-HIV-5/24/2005</p>

    <p class="memofmt1-2">          Antiviral activity and molecular mechanism of an orally active respiratory syncytial virus fusion inhibitor</p>

    <p>          Cianci, Christopher, Meanwell, Nicholas, and Krystal, Mark</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2005</b>.  55(3): 289-292</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   45345   HIV-LS-323; WOS-HIV-5/22/2005</p>

    <p class="memofmt1-2">          Mitogen-activated protein kinases regulate LSF occupancy at the human immunodeficiency virus type 1 promoter</p>

    <p>          Ylisastigui, L, Kaur, R, Johnson, H, Volker, J, He, GC, Hansen, U, and Margolis, D</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(10): 5952-5962, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228814400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228814400008</a> </p><br />

    <p>24.   45346   HIV-LS-323; WOS-HIV-5/22/2005</p>

    <p class="memofmt1-2">          Identification of novel low molecular weight CXCR4 antagonists by structural tuning of cyclic tetrapeptide scaffolds</p>

    <p>          Tamamura, H, Araki, T, Ueda, S, Wang, ZX, Oishi, S, Esaka, A, Trent, JO, Nakashima, H, Yamamoto, N, Peiper, SC, Otaka, A, and Fujii, N</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(9): 3280-3289, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228871700025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228871700025</a> </p><br />

    <p>25.   45347   HIV-LS-323; WOS-HIV-5/22/2005</p>

    <p class="memofmt1-2">          Computational strategies in discovering novel non-nucleoside inhibitors of HIV-1 RT</p>

    <p>          Barreca, ML, Rao, A, De Luca, L, Zappala, L, Monforte, AM, Maga, G, Pannecouque, C, Balzarini, J, De Clercq, E, Chimirri, A, and Monforte, P</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(9): 3433-3437, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228871700041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228871700041</a> </p><br />

    <p>26.   45348   HIV-LS-323; WOS-HIV-5/30/2005</p>

    <p class="memofmt1-2">          Convenient route to both enantiomers of a highly functionalized trans-disubstituted cyclopentene. Synthesis of the carbocyclic core of the nucleoside BCA</p>

    <p>          Banerjee, S, Ghosh, S, Sinha, S, and Ghosh, S</p>

    <p>          JOURNAL OF ORGANIC CHEMISTRY <b>2005</b>.  70(10): 4199-4202, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228983300061">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228983300061</a> </p><br />

    <p>27.   45349   HIV-LS-323; WOS-HIV-5/30/2005</p>

    <p class="memofmt1-2">          Ubiquitination of APOBEC3G by an HIV-1 Vif-Cullin5-Elongin B-Elongin C complex is essential for Vif function</p>

    <p>          Kobayashi, M, Takaori-Kondo, A, Miyauchi, Y, Iwai, K, and Uchiyama, T</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(19): 18573-18578, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228932300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228932300005</a> </p><br />

    <p>28.   45350   HIV-LS-323; WOS-HIV-5/30/2005</p>

    <p class="memofmt1-2">          Combination of candidate microbicides cellulose acetate 1,2-benzenedicarboxylate and UC781 has synergistic and complementary effects against human immunodeficiency virus type 1 infection</p>

    <p>          Liu, SW, Lu, H, Neurath, AR, and Jiang, SB</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(5): 1830-1836, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228982600024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228982600024</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
