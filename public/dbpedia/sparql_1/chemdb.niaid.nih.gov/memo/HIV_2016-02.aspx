

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zEVCJk/X1oN9xFjT9km9svu+tO1uuAxd/BfQpWp/5A3Xa6nPZd/NPPCGzXNxgIJbv+R4/5+7AagLx6S/o3C67gsJ9Pot2YIvNemlZxrSXJaWnTGiZWqyeWcb9pU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1E4C268D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: February, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25889234">Long Lasting Control of Viral Rebound with a New Drug ABX464 Targeting Rev - Mediated Viral RNA Biogenesis.</a> Campos, N., R. Myburgh, A. Garcel, A. Vautrin, L. Lapasset, E.S. Nadal, F. Mahuteau-Betzer, R. Najman, P. Fornarelli, K. Tantale, E. Basyuk, M. Seveno, J.P. Venables, B. Pau, E. Bertrand, M.A. Wainberg, R.F. Speck, D. Scherrer, and J. Tazi. Retrovirology, 2015. 12( 30): 15pp. PMID[25889234]. PMCID[PMC4422473].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26717022">Design, Synthesis and In-vitro Evaluation of Novel Tetrahydroquinoline Carbamates as HIV-1 RT Inhibitor and their Antifungal Activity.</a> Chander, S., P. Ashok, Y.T. Zheng, P. Wang, K.S. Raja, A. Taneja, and S. Murugesan. Bioorganic Chemistry, 2016. 64: p. 66-73. PMID[26717022].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26347922">Design, Synthesis, and Biological Evaluation of Novel 2-(Pyridin-3-yloxy)acetamide Derivatives as Potential anti-HIV-1 Agents.</a> Huang, B., X. Li, P. Zhan, E. De Clercq, D. Daelemans, C. Pannecouque, and X. Liu. Chemical Biology &amp; Drug Design, 2016. 87(2): p. 283-289. PMID[26347922].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26559996">Novel Diarylpyrimidines and Diaryltriazines as Potent HIV-1 NNRTIs with Dramatically Improved Solubility: A Patent Evaluation of US20140378443A1.</a> Huang, B., D. Kang, J. Yang, P. Zhan, and X. Liu. Expert Opinion on Therapeutic Patents, 2016. 26(2): p. 281-289. PMID[26559996].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26765351">Marine Pyridoacridine Alkaloids: Biosynthesis and Biological Activities.</a> Ibrahim, S.R.M. and G.A. Mohamed. Chemistry &amp; Biodiversity, 2016. 13(1): p. 37-47. PMID[26765351].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26858771">The Antiviral Compound BIT225 Inhibits HIV-1 Replication in Myeloid Dendritic Cells.</a> Khoury, G., G. Ewart, C. Luscombe, M. Miller, and J. Wilkinson. AIDS Research and Therapy, 2016. 13(7): 5pp. PMID[26858771].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26212217">Molecular Modeling, Synthesis, and anti-HIV Activity of Novel Isoindolinedione Analogues as Potent Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Kumari, G. and R.K. Singh. Chemical Biology &amp; Drug Design, 2016. 87(2): p. 200-212. PMID[26212217].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25613133">Didehydro-Cortistatin A Inhibits HIV-1 Tat Mediated Neuroinflammation and Prevents Potentiation of Cocaine Reward in Tat Transgenic Mice.</a> Mediouni, S., J. Jablonski, J.J. Paris, M.A. Clementz, S. Thenin-Houssier, J.P. McLaughlin, and S.T. Valente. Current HIV research, 2015. 13(1): p. 64-79. PMID[25613133]. PMCID[PMC4416414].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_02_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26706175">A Minimally Cytotoxic CD4 Mimic as an HIV Entry Inhibitor.</a> Mizuguchi, T., S. Harada, T. Miura, N. Ohashi, T. Narumi, H. Mori, Y. Irahara, Y. Yamada, W. Nomura, S. Matsushita, K. Yoshimura, and H. Tamamura. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(2): p. 397-400. PMID[26706175].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26152583">The Tat Inhibitor Didehydro-Cortistatin A Prevents HIV-1 Reactivation from Latency.</a> Mousseau, G., C.F. Kessing, R. Fromentin, L. Trautmann, N. Chomont, and S.T. Valente. MBio, 2015. 6(4): p. e00465. PMID[26152583].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">11<a href="http://www.ncbi.nlm.nih.gov/pubmed/26813581">. Exploring the Role of the alpha-Carboxyphosphonate Moiety in the HIV-RT Activity of alpha-Carboxy nucleoside phosphonates.</a> Mullins, N.D., N.M. Maguire, A. Ford, K. Das, E. Arnold, J. Balzarini, and A.R. Maguire. Organic &amp; Biomolecular Chemistry, 2016. 14(8): p. 2454-2465. PMID[26813581]. PMCID[PMC475589].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26861355">Alkaloids from the Sponge Stylissa carteri Present Prospective Scaffolds for the Inhibition of Human Immunodeficiency Virus 1 (HIV-1).</a> O&#39;Rourke, A., S. Kremb, T.M. Bader, M. Helfer, P. Schmitt-Kopplin, W.H. Gerwick, R. Brack-Werner, and C.R. Voolstra. Marine Drugs, 2016. 14(2): 10pp. PMID[26861355].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26754878">A New Vinyl Selenone-based Domino Approach to Spirocyclopropyl oxindoles Endowed with anti-HIV RT Activity.</a> Palomba, M., L. Rossi, L. Sancineto, E. Tramontano, A. Corona, L. Bagnoli, C. Santi, C. Pannecouque, O. Tabarrini, and F. Marini. Organic &amp; Biomolecular Chemistry, 2016. 14(6): p. 2015-2024. PMID[26754878].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26875938">Dendronized Anionic Gold Nanoparticles: Synthesis, Characterization, and Antiviral Activity.</a> Pena-Gonzalez, C.E., P. Garcia-Broncano, M.F. Ottaviani, M. Cangiotti, A. Fattori, M. Hierro-Oliva, M.L. Gonzalez-Martin, J. Perez-Serrano, R. Gomez, M.A. Munoz-Fernandez, J. Sanchez-Nieves, and F.J. de la Mata. Chemistry, 2016. 22(9): p. 2987-2999. PMID[26875938].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26709958">Correction to Discovery of 2-Pyridinone aminals: A Prodrug Strategy to Advance a Second Generation of HIV-1 Integrase Strand Transfer Inhibitors.</a> Raheem, I.T., A.M. Walji, D. Klein, J.M. Sanders, D.A. Powell, P. Abeywickrema, G. Barbe, A. Bennet, K. Childers, M. Christensen, S.D. Clas, D. Dubost, M. Embrey, J. Grobler, M.J. Hafey, T.J. Hartingh, D.J. Hazuda, J.T. Kuethe, J.M. Dunn, M.D. Miller, K.P. Moore, A. Nolting, N. Pajkovic, S. Patel, Z.H. Peng, V. Rada, P. Rearden, J.D. Schreier, J. Sisko, T.G. Steele, J.F. Truchon, J. Wai, M. Xu, and P.J. Coleman. Journal of Medicinal Chemistry, 2016. 59(1): p. 486-486. PMID[26709958].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000368959100021">Alkaloids from Croton echinocarpus Baill.: Anti-HIV Potential.</a> Ravanelli, N., K.P. Santos, L.B. Motta, J.H.G. Lago, and C.M. Furlan. South African Journal of Botany, 2016. 102: p. 153-156. ISI[000368959100021].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26833261">Potent in Vitro Antiviral Activity of Cistus incanus Extract against HIV and Filoviruses Targets Viral Envelope Proteins.</a> Rebensburg, S., M. Helfer, M. Schneider, H. Koppensteiner, J. Eberle, M. Schindler, L. Gurtler, and R. Brack-Werner. Scientific Reports, 2016. 6(20394): 15pp. PMID[26833261]. PMCID[PMC4735868].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000369005200004">Structural Analogs of Umifenovir 2*. The Synthesis and AntiHIV Activity Study of New Regioisomeric (trans-2-Phenylcyclopropyl)-1H-indole Derivatives.</a> Schols, D., E.A. Ruchko, S.N. Lavrenov, V.V. Kachala, M.B. Nawrozkij, and A.S. Babushkin. Chemistry of Heterocyclic Compounds, 2015. 51(11-12): p. 978-983. ISI[000369005200004].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26880034">Rilpivirine Analogs Potently Inhibit Drug-Resistant HIV-1 Mutants.</a> Smith, S.J., G.T. Pauly, A. Akram, K. Melody, G. Rai, D.J. Maloney, Z. Ambrose, C.J. Thomas, J.T. Schneider, and S.H. Hughes. Retrovirology, 2016. 13(11): 13pp. PMID[26880034]. PMCID[PMC4754833].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_02_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26826732">Pyridoxine hydroxamic acids as Novel HIV-Integrase Inhibitors.</a> Stranix, B.R., J.J. Wu, G. Milot, F. Beaulieu, J.E. Bouchard, K. Gouveia, A. Forte, S. Garde, Z. Wang, J.F. Mouscadet, O. Delelis, and Y. Xiao. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(4): p. 1233-1236. PMID[26826732]. <b>[PubMed].</b> HIV_02_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26747394">Identification of a Small Molecule HIV-1 Inhibitor that Targets the Capsid Hexamer.</a> Xu, J.P., J.D. Branson, R. Lawrence, and S. Cocklin. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(3): p. 824-828. PMID[26747394]. PMCID[PMC4728034].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_02_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26802545">Design, Synthesis and anti-HIV Evaluation of Novel Diarylpyridine Derivatives Targeting the Entrance Channel of NNRTI Binding Pocket.</a> Yang, J., W. Chen, D. Kang, X. Lu, X. Li, Z. Liu, B. Huang, D. Daelemans, C. Pannecouque, E. De Clercq, P. Zhan, and X. Liu. European Journal of Medicinal Chemistry, 2016. 109: p. 294-304. PMID[26802545].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_02_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000368699400008">8-O-4&#39;-Neolignans from the Stem Bark of Illicium difengpi and their anti-HIV-1 Activities.</a> Yang, Y.C., Y. Qin, Y.L. Meng, C.F. Xia, X.M. Gao, and Q.F. Hu. Chemistry of Natural Compounds, 2016. 52(1): p. 43-47. ISI[000368699400008].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_02_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160204&amp;CC=WO&amp;NR=2016018921A1&amp;KC=A1">Targeted Deubiquitinating Enzyme Fusion Proteins Disrupting Human Immunodeficiency Virus Maturation and Budding.</a> Bouamr, F. and P. Sette. Patent. 2016. 2015-US42492 2016018921: 95pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160203&amp;CC=CN&amp;NR=105287497A&amp;KC=A">Application of Stellera chamaejasme Toxin Diterpene in Preparation of anti-AIDS Agent.</a> Chen, D., M. Yan, Y. Lu, and G. Li. Patent. 2016. 2014-10309142 105287497: 8pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160203&amp;CC=CN&amp;NR=105294688A&amp;KC=A">Preparation of Pyrrolo[2,3-b]pyridine-2-Carboxamide Compounds as HIV-1 Integrase Inhibitors.</a> Hu, L., L. Ju, Z. Mao, Z. Li, and C. Zeng. Patent. 2016. 2015-10873837 105294688: 22pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160128&amp;CC=WO&amp;NR=2016012930A1&amp;KC=A1">Isoindolinone Derivatives Useful as Antiviral Agents and Their Preparation.</a> Johns, B.A., E.J. Velthuisen, and J.G. Weatherhead. Patent. 2016. 2015-IB55489 2016012930: 26pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160218&amp;CC=WO&amp;NR=2016025681A1&amp;KC=A1">Preparation of 1H-Indene Compounds as CD4-Mimetics Inhibiting HIV-1 Entry for the Treatment of HIV Infection.</a> Smith, A.B., III, J. Sodroski, N. Madani, and B. Melillo. Patent. 2016. 2015-US44998 2016025681: 92pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2016.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160225&amp;CC=WO&amp;NR=2016027879A1&amp;KC=A1">Preparation of Polycyclic Pyridone Derivatives with HIV Integrase Inhibitory Effect, and AIDS Treatment Using Them.</a> Yoshinaga, T. and K. Nodu. Patent. 2016. 2015-JP73487 2016027879: 179pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
