

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-146.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mLlrwiQ/PnhSCCVMiMv/zQGV7Ls4dudCbqti0pBTAXVG691SCrzv2ylzfLfXA/sPRx2/g3Pq2WP0kN3jtkcLS6erxFI6/oHqIAkoRiPLeZywhKlCM/h508AxxuI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="59519BED" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH   DMID-LS-146-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60982   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Ebola virus infection of human PBMCs causes massive death of macrophages, CD4 and CD8 T cell sub-populations in vitro</p>

    <p>          Gupta, Manisha, Spiropoulou, Christina, and Rollin, Pierre E</p>

    <p>          Virology <b>2007</b>.  364(1): 45-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NBXVCG-1/2/27780d73727c985f5dae7175e8d3c486">http://www.sciencedirect.com/science/article/B6WXR-4NBXVCG-1/2/27780d73727c985f5dae7175e8d3c486</a> </p><br />

    <p>2.     60983   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          A review of studies on animal reservoirs of the SARS coronavirus</p>

    <p>          Shi, Zhengli and Hu, Zhihong</p>

    <p>          Virus Research  <b>2007</b>.  In Press, Corrected Proof: 148</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4NJP3FY-3/2/98d23784c5bf10ce0a978d6d45a7f21e">http://www.sciencedirect.com/science/article/B6T32-4NJP3FY-3/2/98d23784c5bf10ce0a978d6d45a7f21e</a> </p><br />

    <p>3.     60984   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Structure and Function of Flavivirus Ns5 Methyltransferase</p>

    <p>          Zhou, Y. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(8): 3891-3903</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900022</a> </p><br />

    <p>4.     60985   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Sulfated Homologues of Heparin Inhibit Hepatitis C Virus Entry Into Mammalian Cells</p>

    <p>          Basu, A. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(8): 3933-3941</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900026</a> </p><br />

    <p>5.     60986   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Protective efficacy of neutralizing antibodies against Ebola virus infection</p>

    <p>          Takada, Ayato, Ebihara, Hideki, Jones, Steven, Feldmann, Heinz, and Kawaoka, Yoshihiro</p>

    <p>          Vaccine <b>2007</b>.  25(6): 993-999</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4M33SMC-2/2/0918e2e73cf4ce8b72f1c82627a676f0">http://www.sciencedirect.com/science/article/B6TD4-4M33SMC-2/2/0918e2e73cf4ce8b72f1c82627a676f0</a> </p><br />

    <p>6.     60987   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          29 Antiviral drugs; Side Effects of Drugs Annual: Side Effects of Drugs Annual 29 - A worldwide yearly survey of new data and trends in adverse drug reactions</p>

    <p>          Fux, Christoph, Evison, John, Schlegel, Matthias, Thurnheer, Christine, and Furrer, Hansjakob</p>

    <p>          Side Effects of Drugs Annual <b>2007</b>.: 300-314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B8CWJ-4NTJJ14-19/2/069e972f7a3320681e65f2b640536cdd">http://www.sciencedirect.com/science/article/B8CWJ-4NTJJ14-19/2/069e972f7a3320681e65f2b640536cdd</a> </p><br />
    <br clear="all">

    <p>7.     60988   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          A rapid DNA hybridization assay for the evaluation of antiviral compounds against Epstein-Barr virus</p>

    <p>          Prichard, Mark N, Daily, Shannon L, Jefferson, Geraldine M, Perry, Amie L, and Kern, Earl R</p>

    <p>          Journal of Virological Methods <b>2007</b>.  In Press, Corrected Proof: 3207</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T96-4NVK1NX-1/2/3d8ce40c7e5ebfdb767e07b5516abe6f">http://www.sciencedirect.com/science/article/B6T96-4NVK1NX-1/2/3d8ce40c7e5ebfdb767e07b5516abe6f</a> </p><br />

    <p>8.     60989   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Electrophile-promoted addition of hydroxymethylphosphonate to 4&#39;,5&#39;-didehydronucleosides: a way to novel isosteric analogues of 5&#39;-nucleotides</p>

    <p>          Tocik, Zdenek, Dvorakova, Ivana, Liboska, Radek, Budesinsky, Milos, Masojidkova, Milena, and Rosenberg, Ivan</p>

    <p>          Tetrahedron <b>2007</b>.  63(21): 4516-4534</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4N7S52H-3/2/d6a3563fe54dd8ef23855c7aa95a33b7">http://www.sciencedirect.com/science/article/B6THR-4N7S52H-3/2/d6a3563fe54dd8ef23855c7aa95a33b7</a> </p><br />

    <p>9.     60990   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Unsaturated fluoro-ketopyranosyl nucleosides: Synthesis and biological evaluation of 3-fluoro-4-keto-[beta]-d-glucopyranosyl derivatives of N4-benzoyl cytosine and N6-benzoyl adenine</p>

    <p>          Manta, Stella, Agelis, George, Botic, Tanja, Cencic, Avrelija, and Komiotis, Dimitri</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 1998</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4NK4G05-1/2/7ab6aa2061e696e74a19dda739fe74d9">http://www.sciencedirect.com/science/article/B6VKY-4NK4G05-1/2/7ab6aa2061e696e74a19dda739fe74d9</a> </p><br />

    <p>10.   60991   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Hbv Pathogenesis in Animal Models: Recent Advances on the Role of Platelets</p>

    <p>          Iannacone, M. <i> et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46(4): 719-726</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245794900027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245794900027</a> </p><br />

    <p>11.   60992   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Identification of 1-isopropylsulfonyl-2-amine benzimidazoles as a new class of inhibitors of hepatitis B virus</p>

    <p>          Li, Yun-Fei, Wang, Gui-Feng, Luo, Yu, Huang, Wei-Gang, Tang, Wei, Feng, Chun-Lan, Shi, Li-Ping, Ren, Yu-Dan, Zuo, Jian-Ping, and Lu, Wei</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 1278</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4NCSGG4-1/2/f3a970576470dcd21ceca50e52559c4b">http://www.sciencedirect.com/science/article/B6VKY-4NCSGG4-1/2/f3a970576470dcd21ceca50e52559c4b</a> </p><br />
    <br clear="all">

    <p>12.   60993   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Peptides coupled to the surface of a kind of liposome protect infection of influenza viruses</p>

    <p>          Nagata, Tomoya, Toyota, Toshiaki, Ishigaki, Hirohito, Ichihashi, Toru, Kajino, Kiichi, Kashima, Yoshitaka, Itoh, Yasushi, Mori, Masahito, Oda, Hiroshi, Yamamura, Hiroyuki, Taneichi, Maiko, Uchida, Tetsuya, and Ogasawara, Kazumasa</p>

    <p>          Vaccine <b>2007</b>.  25(26): 4914-4921</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4NJ25C7-C/2/b731c480422b8633947ba7f2ed09aeb3">http://www.sciencedirect.com/science/article/B6TD4-4NJ25C7-C/2/b731c480422b8633947ba7f2ed09aeb3</a> </p><br />

    <p>13.   60994   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Screening of Some Novel Pyridazine and Triazolopyridazine Nucleosides</p>

    <p>          Rashad, A. <i>et al.</i></p>

    <p>          Heteroatom Chemistry <b>2007</b>.  18(3): 274-282</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245716800013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245716800013</a> </p><br />

    <p>14.   60995   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Development of Motavizumab, an Ultra-potent Antibody for the Prevention of Respiratory Syncytial Virus Infection in the Upper and Lower Respiratory Tract</p>

    <p>          Wu, Herren, Pfarr, David S, Johnson, Syd, Brewah, Yambasu A, Woods, Robert M, Patel, Nita K, White, Wendy I, Young, James F, and Kiener, Peter A</p>

    <p>          Journal of Molecular Biology <b>2007</b>.  368(3): 652-665</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4N3GHPT-2/2/c5ed202f1f8f6dc8acaf8769e02c6717">http://www.sciencedirect.com/science/article/B6WK7-4N3GHPT-2/2/c5ed202f1f8f6dc8acaf8769e02c6717</a> </p><br />

    <p>15.   60996   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          The use of hepatitis C virus NS3/4A and secreted alkaline phosphatase to quantitate cell-cell membrane fusion mediated by severe acute respiratory syndrome coronavirus S protein and the receptor angiotensin-converting enzyme 2</p>

    <p>          Chou, Chih-Fong, Shen, Shuo, Mahadevappa, Geetha, Lim, Seng Gee, Hong, Wanjin, and Tan, Yee-Joo</p>

    <p>          Analytical Biochemistry <b>2007</b>.  In Press, Uncorrected Proof: 665</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W9V-4NKJ0J7-4/2/fd12e55e46794164acc1a13e2c46702e">http://www.sciencedirect.com/science/article/B6W9V-4NKJ0J7-4/2/fd12e55e46794164acc1a13e2c46702e</a> </p><br />

    <p>16.   60997   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Efficacy of Delayed Therapy Using Combinations of ST-246 with CMX-001 Against Systemic Cowpox Virus Infections in Mice</p>

    <p>          Quenelle, Debra, Prichard, Mark, Keith, Kathy, Collins, Deborah, Jordan, Robert, Hruby, Dennis, Painter, George, Robertson, Alice, and Kern, Earl</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A70-A45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3S/2/40baeb605b241a590428dd6f12d1ec6c">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3S/2/40baeb605b241a590428dd6f12d1ec6c</a> </p><br />

    <p>17.   60998   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Sequential Treatment With Lamivudine and Alpha-Interferon in Chronic Hepatitis B: a Pilot Study</p>

    <p>          Niro, G. <i>et al.</i></p>

    <p>          Gastroenterology <b>2007</b>.  132(4): A200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927601478">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927601478</a> </p><br />

    <p>18.   60999   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          In vivo imaging of cidofovir treatment of cowpox virus infection</p>

    <p>          Goff, Arthur, Twenhafel, Nancy, Garrison, Aura, Mucker, Eric, Lawler, James, and Paragas, Jason</p>

    <p>          Virus Research  <b>2007</b>.  In Press, Corrected Proof: 232</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4NT24NP-3/2/51269405b0af56d6d091d430763597a1">http://www.sciencedirect.com/science/article/B6T32-4NT24NP-3/2/51269405b0af56d6d091d430763597a1</a> </p><br />

    <p>19.   61000   DMID-LS-146; EMBASE-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Optimization and determination of the absolute configuration of a series of potent inhibitors of human papillomavirus type-11 E1-E2 protein-protein interaction: A combined medicinal chemistry, NMR and computational chemistry approach</p>

    <p>          Goudreau, Nathalie, Cameron, Dale R, Deziel, Robert, Hache, Bruno, Jakalian, Araz, Malenfant, Eric, Naud, Julie, Ogilvie, William W, O&#39;Meara, Jeff, White, Peter W, and Yoakim, Christiane</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(7): 2690-2700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4MWPSGJ-2/2/8d6a1a481459cdab8fbf83d110afa321">http://www.sciencedirect.com/science/article/B6TF8-4MWPSGJ-2/2/8d6a1a481459cdab8fbf83d110afa321</a> </p><br />

    <p>20.   61001   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Favorable Cross-Resistance Profile of Hcv-796 and Sch-503034 and Enhanced Anti-Replicon Activity Mediated by Combination Treatment</p>

    <p>          Ralston, R. <i>et al.</i></p>

    <p>          Gastroenterology <b>2007</b>.  132(4): A738</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606128">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606128</a> </p><br />

    <p>21.   61002   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Pharmacodynamic Analysis of the Antiviral Activity of the Non-Nucleoside Polymerase Inhibitor, Hcv-796, in Combination With Pegylated Interferon Alfa-2b in Treatment-Naive Patients With Chronic Hcv</p>

    <p>          Maller, E. <i>et al.</i></p>

    <p>          Gastroenterology <b>2007</b>.  132(4): A740</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606136">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606136</a> </p><br />

    <p>22.   61003   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          A Randomized Trial of Telbivudine Vs Adefovir for Hbeag-Positive Chronic Hepatitis B: Efficacy Through Week 76, Predictors of Response and Effects of Switching to Telbivudine</p>

    <p>          Bzowej, N. <i>et al.</i></p>

    <p>          Gastroenterology <b>2007</b>.  132(4): A764</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606251">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606251</a> </p><br />

    <p>23.   61004   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Final Results From a Phase Iia Pharmacokinetic Study of Valopicitabine (Nm283) and Peg-Ifn Alpha-2b in Patients With Genotype 1 Chronic Hepatitis C</p>

    <p>          Rodriguez-Torres, M. <i>et al.</i></p>

    <p>          Gastroenterology <b>2007</b>.  132(4): A778</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606313">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606313</a> </p><br />
    <br clear="all">

    <p>24.   61005   DMID-LS-146; WOS-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Characterization of Hcv Ns3/4a Protease Inhibition by Itmn-191 Reveals Picomolar Potency and Slow Dissociation: Implications for the Use of Itmn-191 in Chronic Hcv Treatment</p>

    <p>          Rajagopalan, P. <i>et al.</i></p>

    <p>          Gastroenterology <b>2007</b>.  132(4): A782</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606331">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606331</a> </p><br />

    <p>25.   61006   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Novel C-6 Fluorinated Acyclic Side Chain Pyrimidine Derivatives: Synthesis, (1)H and (13)C NMR Conformational Studies, and Antiviral and Cytostatic Evaluations</p>

    <p>          Prekupec, S, Makuc, D, Plavec, J, Suman, L, Kralj, M, Pavelic, K, Balzarini, J, Clercq, ED, Mintas, M, and Raic-Malic, S</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17539622&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17539622&amp;dopt=abstract</a> </p><br />

    <p>26.   61007   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Monitoring the anti-viral effect of interferon-alpha on individual cells</p>

    <p>          Kim, CS, Jung, JH, Wakita, T, Yoon, SK, and Jang, SK</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17537862&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17537862&amp;dopt=abstract</a> </p><br />

    <p>27.   61008   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Small-molecule activators of RNase L with broad-spectrum antiviral activity</p>

    <p>          Thakur, CS, Jha, BK, Dong, B, Das, Gupta J, Silverman, KM, Mao, H, Sawai, H, Nakamura, AO, Banerjee, AK, Gudkov, A, and Silverman, RH</p>

    <p>          Proc Natl Acad Sci U S A <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17535916&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17535916&amp;dopt=abstract</a> </p><br />

    <p>28.   61009   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Prophylactic and therapeutic efficacy of human monoclonal antibodies against H5N1 influenza</p>

    <p>          Simmons, CP, Bernasconi, NL, Suguitan, AL, Mills, K, Ward, JM, Chau, NV, Hien, TT, Sallusto, F, Ha, do Q, Farrar, J, de, Jong MD, Lanzavecchia, A, and Subbarao, K</p>

    <p>          PLoS Med <b>2007</b>.  4(5): e178</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17535101&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17535101&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>29.   61010   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          NS5A protein of HCV enhances HBV replication and resistance to interferon response</p>

    <p>          Pan, Y, Wei, W, Kang, L, Wang, Z, Fang, J, Zhu, Y, and Wu, J</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17532300&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17532300&amp;dopt=abstract</a> </p><br />

    <p>30.   61011   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Drug resistance in antiviral treatment for infections with hepatitis B and C viruses</p>

    <p>          Yotsuyanagi, H and Koike, K</p>

    <p>          J Gastroenterol <b>2007</b>.  42(5): 329-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17530355&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17530355&amp;dopt=abstract</a> </p><br />

    <p>31.   61012   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Synthesis of neutral and cationic tripyridylporphyrin-d-galactose conjugates and the photoinactivation of HSV-1</p>

    <p>          Tome, JP, Silva, EM, Pereira, AM, Alonso, CM, Faustino, MA, Neves, MG, Tome, AC, Cavaleiro, JA, Tavares, SA, Duarte, RR, Caeiro, MF, and Valdeira, ML</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17524654&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17524654&amp;dopt=abstract</a> </p><br />

    <p>32.   61013   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Novel linear polymers bearing thiosialosides as pendant-type epitopes for influenza neuraminidase inhibitors</p>

    <p>          Matsuoka, K, Takita, C, Koyama, T, Miyamoto, D, Yingsakmongkon, S, Hidari, KI, Jampangern, W, Suzuki, T, Suzuki, Y, Hatano, K, and Terunuma, D</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17524642&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17524642&amp;dopt=abstract</a> </p><br />

    <p>33.   61014   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Inhibition of influenza viral neuraminidase activity by collectins</p>

    <p>          Tecle, T, White, MR, Crouch, EC, and Hartshorn, KL</p>

    <p>          Arch Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17514488&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17514488&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   61015   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Progress for dengue virus diseases</p>

    <p>          Melino, S and Paci, M</p>

    <p>          FEBS J <b>2007</b>.  274(12): 2986-3002</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17509079&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17509079&amp;dopt=abstract</a> </p><br />

    <p>35.   61016   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          1,2-Mannobioside Mimic: Synthesis, DC-SIGN Interaction by NMR and Docking, and Antiviral Activity</p>

    <p>          Reina, JJ, Sattin, S, Invernizzi, D, Mari, S, Martinez-Prats, L, Tabarani, G, Fieschi, F, Delgado, R, Nieto, PM, Rojo, J, and Bernardi, A</p>

    <p>          ChemMedChem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17508368&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17508368&amp;dopt=abstract</a> </p><br />

    <p>36.   61017   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Replication of hepatitis C virus</p>

    <p>          Moradpour, D, Penin, F, and Rice, CM</p>

    <p>          Nat Rev Microbiol <b>2007</b>.  5(6): 453-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17487147&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17487147&amp;dopt=abstract</a> </p><br />

    <p>37.   61018   DMID-LS-146; PUBMED-DMID-6/4/2007</p>

    <p class="memofmt1-2">          Development of potent inhibitors of the coxsackievirus 3C protease</p>

    <p>          Lee, ES, Lee, WG, Yun, SH, Rho, SH, Im, I, Yang, ST, Sellamuthu, S, Lee, YJ, Kwon, SJ, Park, OK, Jeon, ES, Park, WJ, and Kim, YC</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.  358(1): 7-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17485072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17485072&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
