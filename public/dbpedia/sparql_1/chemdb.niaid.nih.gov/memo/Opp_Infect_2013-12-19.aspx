

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-12-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="m3taGpaekBUyVTFi1rZabzUWCSRjbYh2YRN+y15Q3tJ5JB/xz3bp1fuguOqjkAwqlvAU5IqMpSsaZzfQv+Vo05BAYOIxDZ8hSW6Z0lSwY3QNNfLtgPXKh7yX/Ys=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="24AE946A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: December 6 - December 19, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24316318">Anti-Candida Activity of Spent Culture Filtrate of Lactobacillus plantarum Strain LR/14.</a> Sharma, A. and S. Srivastava. Journal of Medical Mycology, 2013. <b>[Epub ahead of print]</b>. PMID[24316318]. <b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24314266">Antifungal Activity of Cinnamic acid Derivatives Involves Inhibition of Benzoate 4-hydroxylase (CYP53).</a> Korosec, B., M. Sova, S. Turk, N. Krasevec, M. Novak, L. Lah, J. Stojan, B. Podobnik, S. Berne, N. Zupanec, M. Bunc, S. Gobec, and R. Komel. Journal of Applied Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[24314266].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24316259">Artemisinins: Pharmacological Actions Beyond anti-Malarial.</a> Ho, W.E., H.Y. Peh, T.K. Chan, and W.S. Wong. Pharmacology &amp; Therapeutics, 2013. <b>[Epub ahead of print]</b>. PMID[24316259].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24306184">Efficacy of the Combination of Voriconazole and Caspofungin in Experimental Pulmonary Aspergillosis by Different Aspergillus Species.</a> Zhang, M., X. Su, W.K. Sun, F. Chen, X.Y. Xu, and Y. Shi. Mycopathologia, 2013. <b>[Epub ahead of print]</b>. PMID[24306184].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24323472">Enfumafungin Derivative MK-3118 Shows Increased in Vitro Potency against Clinical Echinocandin Resistant Candida spp. and Aspergillus spp. Isolates.</a> Jimenez-Ortigosa, C., P. Paderu, M.R. Motyl, and D.S. Perlin. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24323472].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24314136">In Vitro Antifungal Activity of Farnesyltransferase Inhibitors against Clinical Isolates of Aspergillus and Candida.</a> Qiao, J., P. Gao, X. Jiang, and H. Fang. Annals of Clinical Microbiology and Antimicrobials, 2013. 12(1): p. 1. PMID[24314136].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24338208">Purification and Applications of a Lectin from the Mushroom Gymnopilus spectabilis.</a> Albores, S., P. Mora, M.J. Bustamante, M.P. Cerdeiras, and L. Franco Fraguas. Applied Biochemistry and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[24338208]<a name="_GoBack"></a>.</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24206766">Anti-Tubercular Agents. Part 8: Synthesis, Antibacterial and Antitubercular Activity of 5-Nitrofuran Based 1,2,3-Triazoles.</a> Kamal, A., S.M. Hussaini, S. Faazil, Y. Poornachandra, G. Narender Reddy, C.G. Kumar, V.S. Rajput, C. Rani, R. Sharma, I.A. Khan, and N. Jagadeesh Babu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6842-6846. PMID[24206766].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24318724">Antitubercular and Antibacterial Activity of Quinonoid Natural Products against Multi-drug Resistant Clinical Isolates.</a> Dey, D., R. Ray, and B. Hazra. Phytotherapy Research, 2013. <b>[Epub ahead of print]</b>. PMID[24318724].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24200931">Design and Synthesis of Novel Antimicrobials with Activity against Gram-positive Bacteria and Mycobacterial Species, Including M. tuberculosis.</a> Tiruveedhula, V.V., C.M. Witzigmann, R. Verma, M.S. Kabir, M. Rott, W.R. Schwan, S. Medina-Bielski, M. Lane, W. Close, R.L. Polanowski, D. Sherman, A. Monte, J.R. Deschamps, and J.M. Cook. Bioorganic &amp; Medicinal Chemistry, 2013. 21(24): p. 7830-7840. PMID[24200931].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24169232">Design, Synthesis and Evaluation of 6-(4-((Substituted-1H-1,2,3-triazol-4-yl)methyl)piperazin-1-yl)phenanthridine Analogues as Antimycobacterial Agents.</a> Nagesh, H.N., K.M. Naidu, D.H. Rao, J.P. Sridevi, D. Sriram, P. Yogeeswari, and K.V. Chandra Sekhar. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6805-6810. PMID[24169232].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24339412">Enhancement of Cationic Antimicrobial Materials Via Cholesterol Incorporation.</a> Coady, D.J., Z.Y. Ong, P.S. Lee, S. Venkataraman, W. Chin, A.C. Engler, Y.Y. Yang, and J.L. Hedrick. Advanced Healthcare Materials, 2013. <b>[Epub ahead of print]</b>. PMID[24339412].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24325645">A Noncompetitive Inhibitor for M. tuberculosis&#39;s Class IIa Fructose 1,6-bisphosphate aldolase.</a> Capodagli, G.C., W.G. Sedhom, M. Jackson, K.A. Ahrendt, and S.D. Pegan. Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24325645].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24333643">Salicylanilide Pyrazinoates Inhibit in Vitro Multidrug-resistant Mycobacterium tuberculosis Strains, Atypical Mycobacteria and Isocitrate Lyase.</a> Kratky, M., J. Vinsova, E. Novotna, and J. Stolarikova. European Journal of Pharmaceutical Sciences, 2013. <b>[Epub ahead of print]</b>. PMID[24333643].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24341638">7-Chloroquinoline-Isatin Conjugates: Antimalarial, Antitubercular and Cytotoxic Evaluation.</a> Raj, R., C. Biot, S.C. Kremer, L. Kremer, Y. Guerardel, J. Gut, P.J. Rosenthal, D. Forge, and V. Kumar. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[24341638].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24324180">Antibodies to a Single, Conserved Epitope in Anopheles APN1 Inhibit Universal Transmission of Falciparum and Vivax Malaria.</a> Armistead, J.S., I. Morlais, D.K. Mathias, J.G. Jardim, J. Joy, A. Fridman, A.C. Finnefrock, A. Bagchi, M. Plebanski, D.G. Scorpio, T.S. Churcher, N.A. Borg, J. Sattabongkot, and R.R. Dinglasan. Infection and Immunity, 2013. <b>[Epub ahead of print]</b>. PMID[24324180].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24318747">In Vitro Antiplasmodial Activity of Some Medicinal Plants of Burkina Faso.</a> Ouattara, L.P., S. Sanon, V. Mahiou-Leddet, A. Gansane, B. Baghdikian, A. Traore, I. Nebie, A.S. Traore, N. Azas, E. Ollivier, and S.B. Sirima. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[24318747].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24320229">The Mechanism by Which the Phenothiazine thioridazine Contributes to Cure Problematic Drug-resistant Forms of Pulmonary Tuberculosis: Recent Patents for &quot;New Use&quot;.</a> Amaral, L., A. Martins, G. Spengler, A. Hunyadi, and J. Molnar. Recent Patents on Anti-infective Drug Discovery, 2013. <b>[Epub ahead of print]</b>. PMID[24320229].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24120516">A New Protoberberine Alkaloid from Meconopsis simplicifolia (D. Don) Walpers with Potent Antimalarial Activity against a Multidrug Resistant Plasmodium falciparum Strain.</a> Wangchuk, P., P.A. Keller, S.G. Pyne, W. Lie, A.C. Willis, R. Rattanajak, and S. Kamchonwongpaisan. Journal of Ethnopharmacology, 2013. 150(3): p. 7. PMID[24120516].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24330395">The Potential of anti-Malarial Compounds Derived from African Medicinal Plants, Part I: A Pharmacological Evaluation of Alkaloids and Terpenoids.</a> Amoa Onguene, P., F. Ntie-Kang, L.L. Lifongo, J.C. Ndom, W. Sippl, and L.M. Mbaze. Malaria Journal, 2013. 12(1): p. 1. PMID[24330395].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23707188">Recent Pharmacological Developments in beta-Carboline Alkaloid &quot;Harmaline&quot;.</a> Khan, F.A., A. Maalik, Z. Iqbal, and I. Malik. European Journal of Pharmacology, 2013. 721(1-3): p. 4. PMID[23707188].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24341604">A SYBR Green 1-Based in Vitro Test of Susceptibility of Ghanaian Plasmodium falciparum Clinical Isolates to a Panel of anti-Malarial Drugs.</a> Quashie, N.B., N.O. Duah, B. Abuaku, L. Quaye, R. Ayanful-Torgby, G.A. Akwoviah, M. Kweku, J.D. Johnson, N.W. Lucchi, V. Udhayakumar, C. Duplessis, K.C. Kronmann, and K.A. Koram. Malaria Journal, 2013. 12(1): p. 1. PMID[24341604].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24183979">DNA Immunization with Eukaryotic Initiation Factor-2alpha of Toxoplasma gondii Induces Protective Immunity against Acute and Chronic Toxoplasmosis in Mice.</a> Chen, J., S.Y. Huang, D.H. Zhou, Z.Y. Li, E. Petersen, H.Q. Song, and X.Q. Zhu. Vaccine, 2013. 31(52): p. 6225-6231. PMID[24183979].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1206-121913.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326713200009">Antifungal Activity and Mode of Action of Carvacrol against Candida albicans Strains.</a> Lima, I.O., F.D. Pereira, W.A. de Oliveira, E.D. Lima, E.A. Menezes, F.A. Cunha, and M. Diniz. Journal of Essential Oil Research, 2013. 25(2): p. 138-142. ISI[000326713200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326859300017">Antimicrobial Activity of Extracts and Fractions from Aerial Parts of Selected Plants (Garcinia achachairu, Macrosiphonia velame, Rubus niveus and Pilea microphylla) against Some Pathogenic Microorganisms.</a> Melim, C., K. Guimaraes, Z. Martin-Quintal, A.D. Alves, D.T.D. Martins, F. Delle Monache, V. Cechinel, A.B. Cruz, and R. Niero. Natural Product Communications, 2013. 8(11): p. 1567-1569. ISI[000326859300017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327123000021">Antimicrobial Activity of Nanocellulose Conjugated with Allicin and Lysozyme.</a> Jebali, A., S. Hekmatimoghaddam, A. Behzadi, I. Rezapor, B.H. Mohammadi, T. Jasemizad, S.A. Yasini, M. Javadzadeh, A. Amiri, M. Soltani, Z. Rezaei, N. Sedighi, M. Seyfi, M. Rezaei, and M. Sayadi. Cellulose, 2013. 20(6): p. 2897-2907. ISI[000327123000021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326482600081">Antimicrobial Activity of Saudi Mint on Some Pathogenic Microbes.</a> Selim, S., S. Hassan, and B. El Sabty. Journal of Pure and Applied Microbiology, 2013. 7(3): p. 2155-2160. ISI[000326482600081].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />
    
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326482600113">Antimicrobial Activity of Trypsin-Chymotrypsin Inhibitor from the Seeds of Mucuna pruriens.</a> Chandrashekharaiah, K.S. Journal of Pure and Applied Microbiology, 2013. 7(3): p. 2405-2410. ISI[000326482600113].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326911800008">Basic Esters of meta-/para-Alkoxyphenylcarbamic acid Containing 4-(2-Methyl-/2-fluorophenyl)piperazin-1-yl Moiety and Their Antimicrobial Activity.</a> Malik, I., E. Sedlarova, M. Bukovsky, J. Csollei, and L. Sichrovska. Fresenius Environmental Bulletin, 2013. 22(9A): p. 2689-2694. ISI[000326911800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />
    
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326662900023">Candida albicans Biofilm Inhibition by Synergistic Action of Terpenes and Fluconazole.</a> Pemmaraju, S.C., P.A. Pruthi, R. Prasad, and V. Pruthi. Indian Journal of Experimental Biology, 2013. 51(11): p. 1032-1037. ISI[000326662900023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326903600024">Chemical Composition and Biological Activities of Hypericum pamphylicum.</a> Ozkan, E.E., N. Ozsoy, G. Ozhan, B.O. Celik, and A. Mat. Industrial Crops and Products, 2013. 50: p. 182-189. ISI[000326903600024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325950600133">Design, Synthesis, and Preliminary Biological Evaluation of Novel Ketone Derivatives of Shikimic acid.</a> Jiang, M., B. Xiong, Y.M. Shen, and C.H. Yang. RSC Advances, 2013. 3(43): p. 20599-20605. ISI[000325950600133].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326259900006">Development of Mycobacterium tuberculosis Whole Cell Screening Hits as Potential Antituberculosis Agents.</a> Cooper, C.B. Journal of Medicinal Chemistry, 2013. 56(20): p. 7755-7760. ISI[000326259900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326939800005">Didehydroroflamycoin Pentaene Macrolide Family from Streptomyces durmitorensis MS405(T): Production Optimization and Antimicrobial Activity.</a> Stankovic, N., L. Senerovic, Z. Bojic-Trbojevic, I. Vuckovic, L. Vicovac, B. Vasiljevic, and J. Nikodinovic-Runic. Journal of Applied Microbiology, 2013. 115(6): p. 1297-1306. ISI[000326939800005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p>  

    <br />
    
    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327141300021">Effects of Dextran Sulfates on the Acute Infection and Growth Stages of Toxoplasma gondii.</a> Ishiwa, A., K. Kobayashi, H. Takemae, T. Sugi, H. Gong, F. Recuenco, F. Murakoshi, A. Inomata, T. Horimoto, and K. Kato. Parasitology Research, 2013. 112(12): p. 4169-4176. ISI[000327141300021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326734500077">Gymnemic acids Inhibit Hyphal Growth and Virulence in Candida albicans.</a> Vediyappan, G., V. Dumontet, F. Pelissier, and C. d&#39;Enfert. Plos One, 2013. 8(9): p. e74189. ISI[000326734500077].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />
    
    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326554800015">Imidazolium Azolates. Antifungal Activity and the Ability to Use in Papermaking.</a> Kozirog, A., A. Wysocka-Robak, K. Przybysz, A. Michalczyk, and F. Walkiewicz. Przemysl Chemiczny, 2013. 92(9): p. 1618-1620. ISI[000326554800015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />    

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326842900012">Phenothiazine Is a Potent Inhibitor of Prostaglandin E2 Production by Candida albicans Biofilms.</a> Ells, R., G. Kemp, J. Albertyn, J.L.F. Kock, and C.H. Pohl. FEMS Yeast Research, 2013. 13(8): p. 849-855. ISI[000326842900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />    

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326747700014">Structure Determinants of Indolin-2-on-3-spirothiazolidinones as MptpB Inhibitors: An in Silico Study.</a> Yang, Y.F., J.H. Wang, Y. Li, W. Xiao, Z.Z. Wang, J.X. Zhang, W.M. Gao, S.W. Zhang, and L. Yang. Soft Matter, 2013. 9(46): p. 11054-11077. ISI[000326747700014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />
    
    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326745800011">Synthesis and Antifungal Activity of New Hydrazide Derivatives.</a> Turan-Zitouni, G., M.D. Altintop, A. Ozdemir, F. Demirci, U. Abu Mohsen, and Z.A. Kaplancikli. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. 28(6): p. 1211-1216. ISI[000326745800011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />
    
    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326829200001">Synthesis and Biological Evaluation of 2-Hydroxy-3-[(2-aryloxyethyl)amino]propyl 4-[(alkoxycarbonyl)amino]benzoates.</a> Tengler, J., I. Kapustikova, M. Pesko, R. Govender, S. Keltosova, P. Mokry, P. Kollar, J. O&#39;Mahony, A. Coffey, K. Kral&#39;ova, and J. Jampilek. Scientific World Journal, 2013. <b>[Epub ahead of print]</b>. ISI[000326829200001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />
    
    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326688100009">Synthesis and Biological Evaluation of Some Novel Urea and Thiourea Derivatives of Isoxazolo[4,5-d]pyridazine and Structurally Related Thiazolo[4,5-d]pyridazine as Antimicrobial Agents.</a> Faidallah, H.M., S.A.F. Rostom, S.A. Basaif, M.S.T. Makki, and K.A. Khan. Archives of Pharmacal Research, 2013. 36(11): p. 1354-1368. ISI[000326688100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />    

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327091200007">Synthesis and Biological Properties of alpha-Thymidine 5&#39;-aryl phosphonates.</a> Ivanov, M.A., I.L. Karpenko, L.N. Chernousova, S.N. Andreevskaya, T.G. Smirnova, and L.A. Alexandrova. Russian Journal of Bioorganic Chemistry, 2013. 39(6): p. 639-648. ISI[000327091200007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />    

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327317600003">Synthesis, Characterization, and Antimicrobial Properties of Copper Nanoparticles.</a> Usman, M.S., M.E. El Zowalaty, K. Shameli, N. Zainuddin, M. Salama, and N.A. Ibrahim. International Journal of Nanomedicine, 2013. 8: p. 4467-4478. ISI[000327317600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 

    <br />    

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327111100048">Thiazolopyridine ureas as Novel Antitubercular Agents Acting through Inhibition of DNA Gyrase B.</a> Kale, M.G., A. Raichurkar, P.S. Hameed, D. Waterson, D. McKinney, M.R. Manjunatha, U. Kranthi, K. Koushik, L.K. Jena, V. Shinde, S. Rudrapatna, S. Barde, V. Humnabadkar, P. Madhavapeddi, H. Basavarajappa, A. Ghosh, V.K. Ramya, S. Guptha, S. Sharma, P. Vachaspati, K.N.M. Kumar, J. Giridhar, J. Reddy, V. Panduga, S. Ganguly, V. Ahuja, S. Gaonkar, C.N.N. Kumar, D. Ogg, J.A. Tucker, P.A. Boriack-Sjodin, S.M. de Sousa, V.K. Sambandamurthy, and S.R. Ghorpade. Journal of Medicinal Chemistry, 2013. 56(21): p. 8834-8848. ISI[000327111100048].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />
    
    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327175200019">Two Rare-class Tricyclic Diterpenes with Antitubercular Activity from the Caribbean Sponge Svenzea flava. Application of Vibrational Circular Dichroism Spectroscopy for Determining Absolute Configuration.</a> Aviles, E., A.D. Rodriguez, and J. Vicente. Journal of Organic Chemistry, 2013. 78(22): p. 11294-11301. ISI[000327175200019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p> 
    
    <br />    

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326933700010">Ultrastructural, Morphological, and Antifungal Properties of Micro and Nanoparticles of Chitosan Crosslinked with Sodium Tripolyphosphate.</a> Cota-Arriola, O., M.O. Cortez-Rocha, J.M. Ezquerra-Brauer, J. Lizardi-Mendoza, A. Burgos-Hernandez, R.M. Robles-Sanchez, and M. Plascencia-Jatomea. Journal of Polymers and the Environment, 2013. 21(4): p. 971-980. ISI[000326933700010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1206-121913.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
