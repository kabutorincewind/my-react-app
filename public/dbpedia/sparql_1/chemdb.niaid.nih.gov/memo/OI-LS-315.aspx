

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-315.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uCcUbSiLt1buS8RR7IeWdIFHYdx+VJ+8qG3GC3SKg4BDCOEhD8fnw+VpS1kHyQRVp/DYlADiSftjA5FDwj0znf+bC23jRhb10C7W0QYVhxVsvvfPay1o0RZf+rI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ECFCC739" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-315-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44827   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p><b>          Hepatitis C virus RNA replication is regulated by host geranylgeranylation and fatty acids</b> </p>

    <p>          Kapadia, SB and Chisari, FV</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15699349&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15699349&amp;dopt=abstract</a> </p><br />

    <p>2.         44828   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Synthesis and potent antimicrobial activity of some novel 2-phenyl or methyl-4H-1-benzopyran-4-ones carrying amidinobenzimidazoles</p>

    <p>          Goker, H, Boykin, DW, and Yildiz, S</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(5): 1707-1714</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698788&amp;dopt=abstract</a> </p><br />

    <p>3.         44829   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-viral activity of a series of d- and l-2&#39;-deoxy-2&#39;-fluororibonucleosides in the subgenomic HCV replicon system</p>

    <p>          Shi, J, Du, J, Ma, T, Pankiewicz, KW, Patterson, SE, Tharnish, PM, McBrayer, TR, Stuyver, LJ, Otto, MJ, Chu, CK, Schinazi, RF, and Watanabe, KA</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(5): 1641-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698782&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698782&amp;dopt=abstract</a> </p><br />

    <p>4.         44830   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Synthesis and potent antimicrobial activity of some novel methyl or ethyl 1H-benzimidazole-5-carboxylates derivatives carrying amide or amidine groups</p>

    <p>          Ozden, S, Atabey, D, Yildiz, S, and Goker, H</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(5): 1587-1597</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698776&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698776&amp;dopt=abstract</a> </p><br />

    <p>5.         44831   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Synthesis of 4-hydroxy-1-methylindole and benzo[b]thiophen-4-ol based unnatural flavonoids as new class of antimicrobial agents</p>

    <p>          Yadav, PP, Gupta, P, Chaturvedi, AK, Shukla, PK, and Maurya, R</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(5): 1497-505</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698765&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698765&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44832   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          The crystal structure of Rv1347c, a putative antibiotic resistance protein from mycobacterium tuberculosis, reveals a GCN5-related fold and suggests an alternative function in siderophore biosynthesis</p>

    <p>          Card, GL, Peterson, NA, Smith, CA, Rupp, B, Schick, BM, and Baker, EN</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695811&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695811&amp;dopt=abstract</a> </p><br />

    <p>7.         44833   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Antifungal activities of R-135853, a sordarin derivative, in experimental candidiasis in mice</p>

    <p>          Kamai, Yasuki, Kakuta, Masayo, Shibayama, Takahiro, Fukuoka, Takashi, and Kuwahara, Shogo</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(1): 52-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.         44834   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Drug Resistance and Fitness in Mycobacterium tuberculosis Infection</p>

    <p>          Bottger, EC, Pletschette, M, and Andersson, D</p>

    <p>          J Infect Dis <b>2005</b>.  191(5): 823-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15688309&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15688309&amp;dopt=abstract</a> </p><br />

    <p>9.         44835   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Studies on synthesis and evaluation of quantitative structure-activity relationship of 5-[(3&#39;-chloro-4&#39;,4&#39;-disubstituted-2-oxoazetidinyl)(N-nitro)amino]-6-hydrox y-3-alkyl/aryl[1,3]azaphospholo[1,5-a]pyridin-1-yl-phosphorus dichlorides</p>

    <p>          Sharma, P, Kumar, A, Sharma, S, and Rane, N</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(4): 937-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15686890&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15686890&amp;dopt=abstract</a> </p><br />

    <p>10.       44836   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Effect of sub-inhibitory concentrations of amphotericin B on the yeast surface and phagocytic killing activity</p>

    <p>          Bahmed, Karim, Bonaly, Roger, Benallaoua, Said, and Coulon, Joel</p>

    <p>          Process Biochemistry (Oxford, United Kingdom) <b>2005</b>.  40(2): 759-765</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.       44837   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          The 1.9 A Crystal Structure of Alanine Racemase from Mycobacterium tuberculosis Contains a Conserved Entryway into the Active Site(,)</p>

    <p>          Lemagueres, P, Im, H, Ebalunode, J, Strych, U, Benedik, MJ, Briggs, JM, Kohn, H, and Krause, KL</p>

    <p>          Biochemistry <b>2005</b>.  44(5): 1471-1481</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15683232&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15683232&amp;dopt=abstract</a> </p><br />

    <p>12.       44838   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Pharmacodynamics and pharmacokinetics of SQ109, a new diamine-based antitubercular drug</p>

    <p>          Jia, Lee, Tomaszewski, Joseph E, Hanrahan, Colleen, Coward, Lori, Noker, Patricia, Gorman, Gregory, Nikonenko, Boris, and Protopopova, Marina</p>

    <p>          British Journal of Pharmacology <b>2005</b>.  144(1): 80-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.       44839   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Pseudo-peptides derived from isomannide as potential inhibitors of serine proteases</p>

    <p>          Muri, EM, Gomes, M Jr, Albuquerque, MG, da, Cunha EF, de, Alencastro RB, Williamson, JS, and Antunes, OA</p>

    <p>          Amino Acids <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662562&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662562&amp;dopt=abstract</a> </p><br />

    <p>14.       44840   OI-LS-315; PUBMED-OI-2/9/2005</p>

    <p class="memofmt1-2">          Pathway to synthesis and processing of mycolic acids in Mycobacterium tuberculosis</p>

    <p>          Takayama, K, Wang, C, and Besra, GS</p>

    <p>          Clin Microbiol Rev <b>2005</b>.  18(1): 81-101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653820&amp;dopt=abstract</a> </p><br />

    <p>15.       44841   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Novel HCV shorter form core+1 proteins and polynucleotides for prophylaxis and diagnosis of HCV infections and for screening of anti-HCV agents</p>

    <p>          Mavromara, Penelope and Niki, Vassilaki</p>

    <p>          PATENT:  EP <b>1493749</b>  ISSUE DATE: 20050105</p>

    <p>          APPLICATION: 2003-29532  PP: 80 pp.</p>

    <p>          ASSIGNEE:  (Institut Pasteur, Fr. and Institut Pasteur Hellenique)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.       44842   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Amino substituted derivatives of 5&#39;-amino-5&#39;-deoxy-5&#39;-noraristeromycin</p>

    <p>          Yang, Minmin and Schneller, Stewart W</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(3): 877-882</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.       44843   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          The road to new antiviral therapies</p>

    <p>          Jerome, Keith R</p>

    <p>          Clinical and Applied Immunology Reviews <b>2005</b>.  5(1): 65-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.       44844   OI-LS-315; WOS-OI-1/30/2005</p>

    <p class="memofmt1-2">          Prophylactic role of liposomized chloroquine against murine cryptococcosis less susceptible to fluconazole</p>

    <p>          Khan, MA, Jabeen, R, and Mohammad, O</p>

    <p>          PHARMACEUTICAL RESEARCH <b>2004</b>.  21(12): 2207-2212, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226031500009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226031500009</a> </p><br />

    <p>19.       44845   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Luciferase reporter gene-based cytotoxicity assays for high throughput dual screening of specificity and cytotoxicity of antiviral agents and antibiotics</p>

    <p>          Blair, Wade Stanton, Isaacson, Jason Steven, Cao, Joan Qun, and Patick, Amy Karen</p>

    <p>          PATENT:  WO <b>2004048613</b>  ISSUE DATE:  20040610</p>

    <p>          APPLICATION: 2003  PP: 56 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.       44846   OI-LS-315; WOS-OI-1/30/2005</p>

    <p class="memofmt1-2">          Synthesis and microbiological evaluation of novel [N-acetyl-2,6-diarylpiperidin-4-yl]-5-spiro-4-ace yl-2-(acetylamino)-Delta(2)-1,3,4-thiadiazolines</p>

    <p>          Balasubramanian, S, Ramalingan, C, Aridoss, G, Parthiban, P, and Kabilan, S</p>

    <p>          MEDICINAL CHEMISTRY RESEARCH <b>2004</b>.  13(5): 297-311, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226106600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226106600004</a> </p><br />

    <p>21.       44847   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Steroidal glucocorticoid receptor antagonist antiviral compositions and methods of use</p>

    <p>          Kim, Jong Joseph</p>

    <p>          PATENT:  WO <b>2004112720</b>  ISSUE DATE:  20041229</p>

    <p>          APPLICATION: 2004  PP: 36 pp.</p>

    <p>          ASSIGNEE:  (Viral Genomix, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.       44848   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Inhibitors of serine proteases, particularly hcv ns3-ns4a protease</p>

    <p>          Britt, Shawn D, Cottrell, Kevin M, Perni, Robert B, and Pitlik, Janos</p>

    <p>          PATENT:  WO <b>2005007681</b>  ISSUE DATE:  20050127</p>

    <p>          APPLICATION: 2004</p>

    <p>          ASSIGNEE:  (Vertex Pharmaceuticals Incorporated, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.       44849   OI-LS-315; SCIFINDER-OI-2/1/2005</p>

    <p class="memofmt1-2">          Substituted arylthiourea derivatives useful as inhibitors of viral replication</p>

    <p>          Phadke, Avinash, Quinn, Jesse, Ohkanda, Junko, Thurkauf, Andrew, Shen, Yiping, Liu, Cuixian, Chen, Dawei, and Li, Shouming</p>

    <p>          PATENT:  WO <b>2005007601</b>  ISSUE DATE:  20050127</p>

    <p>          APPLICATION: 2004</p>

    <p>          ASSIGNEE:  (Achillion Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.       44850   OI-LS-315; WOS-OI-1/30/2005</p>

    <p class="memofmt1-2">          Bovine viral diarrhoea virus is internalized by clathrin-dependent receptor-mediated endocytosis</p>

    <p>          Grummer, B, Grotha, S, and Greiser-Wilke, I</p>

    <p>          JOURNAL OF VETERINARY MEDICINE SERIES B-INFECTIOUS DISEASES AND VETERINARY PUBLIC HEALTH <b>2004</b>.  51(10): 427-432, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226093500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226093500002</a> </p><br />
    <br clear="all">

    <p>25.       44851   OI-LS-315; WOS-OI-1/30/2005</p>

    <p class="memofmt1-2">          Peroxiredoxin-linked detoxification of hydroperoxides in Toxoplasma gondii</p>

    <p>          Akerman, SE and Muller, S</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(1): 564-570, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226025100067">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226025100067</a> </p><br />

    <p>26.       44852   OI-LS-315; WOS-OI-1/30/2005</p>

    <p class="memofmt1-2">          Composition and antimicrobial activity of Achillea clavennae and Achillea holosericea essential oils</p>

    <p>          Stojanovic, G, Asakawa, Y, Palic, R, and Radulovic, N</p>

    <p>          FLAVOUR AND FRAGRANCE JOURNAL <b>2005</b>.  20(1): 86-88, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226040300020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226040300020</a> </p><br />

    <p>27.       44853   OI-LS-315; WOS-OI-1/30/2005</p>

    <p class="memofmt1-2">          Inhibitory properties of nucleic acid-binding ligands on protein synthesis</p>

    <p>          Malina, A, Khan, S, Carlson, CB, Svitkin, Y, Harvey, I, Sonenberg, N, Beal, PA, and Pelletier, J</p>

    <p>          FEBS LETTERS <b>2005</b>.  579(1): 79-89, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226170100013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226170100013</a> </p><br />

    <p>28.       44854   OI-LS-315; WOS-OI-1/30/2005</p>

    <p class="memofmt1-2">          A new model for utilising chemical diversity from natural sources</p>

    <p>          Buss, AD and Butler, MS</p>

    <p>          DRUG DEVELOPMENT RESEARCH <b>2004</b>.  62(4): 362-370, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226158500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226158500007</a> </p><br />

    <p>29.       44855   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Mutational dynamics in Mycobacterium tuberculosis: implications for the evolution of drug resistance</p>

    <p>          Boshoff, HIM, Barry, CE, and Mizrahi, V</p>

    <p>          SOUTH AFRICAN JOURNAL OF SCIENCE <b>2004</b>.  100(9-10): 471-474, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226240500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226240500018</a> </p><br />

    <p>30.       44856   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          S-nitroso proteome of Mycobacterium tuberculosis: Enzymes of intermediary metabolism and antioxidant defense</p>

    <p>          Rhee, KY, Erdjument-Bromage, H, Tempst, P, and Nathan, CF</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2005</b>.  102(2): 467-472, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226315700038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226315700038</a> </p><br />

    <p>31.       44857   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          The reductase steps of the type II fatty acid synthase as antimicrobial targets</p>

    <p>          Zhang, YM, Lu, YJ, and Rock, CO</p>

    <p>          LIPIDS <b>2004</b>.  39 (11): 1055-1060, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226341600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226341600003</a> </p><br />
    <br clear="all">

    <p>32.       44858   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Control of hepatitis C: A medicinal chemistry perspective</p>

    <p>          Gordon, CP and Keller, PA</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(1): 1-20, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226212900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226212900001</a> </p><br />

    <p>33.       44859   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Researchers identify better hepatitis C treatment for people with HIV</p>

    <p>          Anon</p>

    <p>          JOURNAL OF INVESTIGATIVE MEDICINE <b>2004</b>.  52(7): 431-432, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226188200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226188200003</a> </p><br />

    <p>34.       44860   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Biologically active polymers: Modification and anti-microbial activity of chitosan derivatives</p>

    <p>          Kenawy, ER, Abdel-Hay, FI, Abou, El-Magd A, and Mahmoud, Y</p>

    <p>          JOURNAL OF BIOACTIVE AND COMPATIBLE POLYMERS <b>2005</b>.  20(1): 95-111, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226242200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226242200006</a> </p><br />

    <p>35.       44861   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          FR227244, a novel antifungal antibiotic from Myrothecium cinctum no. 002 - II. Biological properties and mode of action</p>

    <p>          Kobayashi, M, Sato, I, Abe, F, Nitta, K, Hashimoto, M, Fujie, A, Hino, M, and Hori, Y</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2004</b>.  57(12): 788-796, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226243400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226243400003</a> </p><br />

    <p>36.       44862   OI-LS-315; WOS-OI-2/6/2005</p>

    <p><b>          Antifungal and anti-aflatoxinogenic activity of the brown algae Cystoseira tamarisscifolia</b> </p>

    <p>          Zinedine, A, Elakhdari, S, Faid, M, and Benlemlih, M</p>

    <p>          JOURNAL DE MYCOLOGIE MEDICALE <b>2004</b>.  14(4): 201-205, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226239700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226239700005</a> </p><br />

    <p>37.       44863   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Rapid structural characterization of the arabinogalactan and lipoarabinomannan in live mycobacterial cells using 2D and 3D HR-MAS NMR: structural changes in the arabinan due to ethambutol treatment and gene mutation are observed</p>

    <p>          Lee, REB, Li, W, Chatterjee, D, and Lee, RE</p>

    <p>          GLYCOBIOLOGY <b>2005</b>.  15(2): 139-151, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226199900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226199900005</a> </p><br />

    <p>38.       44864   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          CP-MLR directed QSAR studies on the antimycobacterial activity of functionalized alkenols - topological descriptors in modeling the activity</p>

    <p>          Gupta, MK, Sagar, R, Shaw, AK, and Prabhakar, YS</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(2): 343-351, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226343800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226343800006</a> </p><br />
    <br clear="all">

    <p>39.       44865   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Antiviral and antimicrobial activities of new nitrobutane derivatives</p>

    <p>          Gokce, M, Ozcelik, B, Bakir, G, Karaoglu, T, Bercin, E, and Noyanalpan, N</p>

    <p>          ARZNEIMITTEL-FORSCHUNG-DRUG RESEARCH <b>2004</b>.  54(12): 891-897, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226221400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226221400010</a> </p><br />

    <p>40.       44866   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Identification and optimization of an antimicrobial peptide from the ant venom toxin pilosulin</p>

    <p>          Zelezetsky, I, Pag, U, Antcheva, N, Sahl, HG, and Tossi, A</p>

    <p>          ARCHIVES OF BIOCHEMISTRY AND BIOPHYSICS <b>2005</b>.  434(2): 358-364, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226316300017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226316300017</a> </p><br />

    <p>41.       44867   OI-LS-315; WOS-OI-2/6/2005</p>

    <p class="memofmt1-2">          Application of molecular genetic methods in macrolide, lincosamide and streptogramin resistance diagnostics and in detection of drug-resistant Mycobacterium tuberculosis</p>

    <p>          Jalava, J and Marttila, H</p>

    <p>          APMIS <b>2004</b>.  112(11-12): 838-855, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226305700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226305700009</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
