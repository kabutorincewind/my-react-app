

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-06-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="rSG0W+rpkchh9cB6qiosNZi9RnMHxj/iw2iVVWt2AGWgOB/G5sVFT71yaGxcJwJgnvOVNEqC9BwIXdpEMg8lHa2n/LbUk0mzaaprTSa90Riw2KPOGjeppagC7ns=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8DC602A4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: June 6 - June 19, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24914898">Structural Crystalline Characterization of Sakuranetin - An Antimicrobial Flavanone from Twigs of Baccharis retusa (Asteraceae).</a> dos S. Grecco, S., A.C. Dorigueto, I.M. Landre, M.G. Soares, K. Martho, R. Lima, R.C. Pascon, M.A. Vallim, T.M. Capello, P. Romoff, P. Sartorelli, and J.H. Lago. Molecules, 2014. 19(6): p. 7528-7542. PMID[24914898].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24926289">Potential of Essential Oils for Protection of Grains Contaminated by Aflatoxin Produced by Aspergillus flavus.</a> Esper, R.H., E. Goncalez, M.O. Marques, R.C. Felicio, and J.D. Felicio. Frontiers in Microbiology, 2014. 5(269): p. 5. PMID[24926289].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24901768">Coriandrum sativum L. (Coriander) Essential Oil: Antifungal Activity and Mode of Action on Candida spp., and Molecular Targets Affected in Human Whole-genome Expression.</a> de Almeida Freires, I., R.M. Murata, V.F. Furletti, A. Sartoratto, S.M. Alencar, G.M. Figueira, J.A. de Oliveira Rodrigues, M.C. Duarte, and P.L. Rosalen. Plos One, 2014. 9(6): p. e99086. PMID[24901768].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24779556">Activity, Reduced Toxicity, and Scale-up Synthesis of Amphotericin B-conjugated Polysaccharide.</a> Ickowicz, D.E., S. Farber, E. Sionov, S. Kagan, A. Hoffman, I. Polacheck, and A.J. Domb. Biomacromolecules, 2014. 15(6): p. 2079-2089. PMID[24779556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24897299">Silver Sucrose Octasulfate (IASOS) as a Valid Active Ingredient into a Novel Vaginal Gel against Human Vaginal Pathogens: In Vitro Antimicrobial Activity Assessment.</a> Marianelli, C., P. Petrucci, M.C. Comelli, and G. Calderini. Plos One, 2014. 9(6): p. e97791. PMID[24897299].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24794769">Synthesis of Amino acid Appended Indoles: Appreciable Anti-fungal Activity and Inhibition of Ergosterol Biosynthesis as Their Probable Mode of Action.</a> Pooja, P. Prasher, P. Singh, K. Pawar, K.S. Vikramdeo, N. Mondal, and S.S. Komath. European Journal of Medicinal Chemistry, 2014. 80: p. 325-339. PMID[24794769].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24769347">Design, Synthesis, and Evaluation of Novel Fluoroquinolone-Flavonoid Hybrids as Potent Antibiotics against Drug-resistant Microorganisms.</a> Xiao, Z.P., X.D. Wang, P.F. Wang, Y. Zhou, J.W. Zhang, L. Zhang, J. Zhou, S.S. Zhou, H. Ouyang, X.Y. Lin, M. Mustapa, A. Reyinbaike, and H.L. Zhu. European Journal of Medicinal Chemistry, 2014. 80: p. 92-100. PMID[24769347].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24916461">Targeting Mycobacterium Tuberculosis Nucleoid-associated Protein Hu with Structure-based Inhibitors.</a> Bhowmick, T., S. Ghosh, K. Dixit, V. Ganesan, U.A. Ramagopal, D. Dey, S.P. Sarma, S. Ramakumar, and V. Nagaraja. Nature Communications, 2014. 5: p. 4124. PMID[24916461].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24769346">Synthesis, Cytotoxic Study and Docking Based Multidrug Resistance Modulator Potential Analysis of 2-(9-Oxoacridin-10(9H)-yl)-N-phenyl acetamides.</a> Kumar, R., M. Kaur, M.S. Bahia, and O. Silakari. European Journal of Medicinal Chemistry, 2014. 80: p. 83-91. PMID[24769346].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24835149">Dithiolopyrrolones: Biosynthesis, Synthesis, and Activity of a Unique Class of Disulfide-containing Antibiotics.</a> Li, B., W.J. Wever, C.T. Walsh, and A.A. Bowers. Natural Product Reports, 2014. 31(7): p. 905-923. PMID[24835149].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24768632">Antitubercular Constituents from Premna odorata Blanco.</a> Lirio, S.B., A.P. Macabeo, E.M. Paragas, M. Knorn, P. Kohls, S.G. Franzblau, Y. Wang, and M.A. Aguinaldo. Journal of Ethnopharmacology, 2014. 154(2): p. 471-474. PMID[24768632].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24818517">Discovery of Pyrazolopyridones as a Novel Class of Noncovalent DprE1 Inhibitor with Potent Anti-mycobacterial Activity.</a> Panda, M., S. Ramachandran, V. Ramachandran, P.S. Shirude, V. Humnabadkar, K. Nagalapur, S. Sharma, P. Kaur, S. Guptha, A. Narayan, J. Mahadevaswamy, A. Ambady, N. Hegde, S.S. Rudrapatna, V.P. Hosagrahara, V.K. Sambandamurthy, and A. Raichurkar. Journal of Medicinal Chemistry, 2014. 57(11): p. 4761-4771. PMID[24818517].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24786226">Identification of an Atg8-Atg3 Protein-protein Interaction Inhibitor from the Medicines for Malaria Venture Malaria Box Active in Blood and Liver Stage Plasmodium falciparum Parasites.</a> Hain, A.U., D. Bartee, N.G. Sanders, A.S. Miller, D.J. Sullivan, J. Levitskaya, C.F. Meyers, and J. Bosch. Journal of Medicinal Chemistry, 2014. 57(11): p. 4521-4531. PMID[24786226].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">14.<a href="http://www.ncbi.nlm.nih.gov/pubmed/24813731">Total Synthesis and Antiplasmodial Activity of Pohlianin C and Analogues.</a> Lawer, A., J. Tai, K.A. Jolliffe, S. Fletcher, V.M. Avery, and L. Hunter. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(12): p. 2645-2647. PMID[24813731].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24737313">Small Molecule Screen for Candidate Antimalarials Targeting Plasmodium Kinesin-5.</a> Liu, L., J. Richard, S. Kim, and E.J. Wojcik. The Journal of Biological Chemistry, 2014. 289(23): p. 16601-16614. PMID[24737313].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24813729">Antiplasmodial Activity of Synthetic Ellipticine Derivatives and an Isolated Analog.</a> Montoia, A., E.S.L.F. Rocha, Z.E. Torres, D.S. Costa, M.C. Henrique, E.S. Lima, M.C. Vasconcellos, R.C. Souza, M.R. Costa, A. Grafov, I. Grafova, M.N. Eberlin, W.P. Tadei, R.C. Amorim, and A.M. Pohlit. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(12): p. 2631-2634. PMID[24813729].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24824551">Tetraoxane-pyrimidine Nitrile Hybrids as Dual Stage Antimalarials.</a> Oliveira, R., R.C. Guedes, P. Meireles, I.S. Albuquerque, L.M. Goncalves, E. Pires, M.R. Bronze, J. Gut, P.J. Rosenthal, M. Prudencio, R. Moreira, P.M. O&#39;Neill, and F. Lopes. Journal of Medicinal Chemistry, 2014. 57(11): p. 4916-4923. PMID[24824551].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0606-061914.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335645900001">Synthesis, Antimicrobial, and Anti-inflammatory Activity, of Novel S-substituted and N-substituted 5-(1-Adamantyl)-1,2,4-triazole-3-thiols.</a> Al-Abdullah, E.S., H.H. Asiri, S. Lahsasni, E.E. Habib, T.M. Ibrahim, and A.A. El-Emam. Drug Design, Development and Therapy, 2014. 8: p. 505-518. ISI[000335645900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336116200005">Dill (Anethum graveolens L.) Seed Essential Oil Induces Candida albicans Apoptosis in a Metacaspase-dependent Manner.</a> Chen, Y.X., H. Zeng, J. Tian, X.Q. Ban, B.X. Ma, and Y.W. Wang. Fungal Biology, 2014. 118(4): p. 394-401. ISI[000336116200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336007900002">Analysis of Validamycin as a Potential Antifungal Compound against Candida albicans.</a> Guirao-Abad, J.P., R. Sanchez-Fresneda, E. Valentin, M. Martinez-Esparza, and J.C. Arguelles. International Microbiology, 2013. 16(4): p. 217-225. ISI[000336007900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335090600094">Susceptibility Testing of VT-1129, a Novel Fungal Cyp51 Inhibitor, against Cryptococcus neoformans and Cryptococcus gattii.</a> Lockhart, S.R., N. Iqbal, C.B. Bolden, N.T. Grossman, E.A. Ottinger, E.P. Garvey, S.R. Brand, W.J. Hoekstra, W.R. Moore, and R.J. Schotzinger. Mycoses, 2014. 57: p. 43-43. ISI[000335090600094].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335518300025">Discovery of the First Potent and Selective Mycobacterium tuberculosis Zmp1 Inhibitor.</a> Mori, M., F. Moraca, D. Deodato, D.M. Ferraris, P. Selchow, P. Sander, M. Rizzi, and M. Botta. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(11): p. 2508-2511. ISI[000335518300025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336152800001">Phyllanthus wightianus Mull. Arg.: A Potential Source for Natural Antimicrobial Agents.</a> Natarajan, D., R. Srinivasan, and M.S. Shivakumar. Biomed Research International, 2014. 135082: p. 9. ISI[000336152800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335879300028">Design and Syntheses of Anti-tuberculosis Agents Inspired by BTZ043 Using a Scaffold Simplification Strategy.</a> Tiwari, R., U. Mollmann, S. Cho, S.G. Franzblau, P.A. Miller, and M.J. Miller. ACS Medicinal Chemistry Letters, 2014. 5(5): p. 587-591. ISI[000335879300028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335837800038">Synthesis and Evaluation of Some Substituted Heterocyclic Fluconazole Analogues as Antifungal Agents.</a> Wang, S.D., L. Zhang, Y.S. Jin, J.H. Tang, H. Su, S.C. Yu, and H.X. Ren. Asian Journal of Chemistry, 2014. 26(8): p. 2362-2364. ISI[000335837800038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335879300013">Novel Carboline Derivatives as Potent Antifungal Lead Compounds: Design, Synthesis, and Biological Evaluation.</a> Wang, S.Z., Y. Wang, W. Liu, N. Liu, Y.Q. Zhang, G.Q. Dong, Y. Liu, Z.G. Li, X.M. He, Z.Y. Miao, J.Z. Yao, J. Li, W.N. Zhang, and C.Q. Sheng. ACS Medicinal Chemistry Letters, 2014. 5(5): p. 506-511. ISI[000335879300013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0606-061914.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
