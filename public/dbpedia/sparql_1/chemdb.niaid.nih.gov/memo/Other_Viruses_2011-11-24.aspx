

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-11-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Fu86KS8uguxiuo3+7T2Pa+UzSApnhC8vrfhaJgCIr6e0DAVJKrXEmEqUtpl1tO4txPY/tEKzfvVInptIoS7UBc082mx5tNsMmAqdLCdQuAIbg5Yj1O8iaRGW8Lc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="03D9402E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">November 11 - November 24, 2011</h1>

    <p class="memofmt2-2">Hepatitis B Virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22085635">Synthesis, Stability and Pharmacological Evaluation of a Novel Codrug Consisting of Lamivudine and Ursolic acid.</a> Zhong, Y., Z. Dai, Y. Xu, Y. Teng, and B. Wu, European journal of pharmaceutical sciences : Official Journal of the European Federation for Pharmaceutical Sciences, 2011. <b>[Epub ahead of print]</b>; PMID[22085635].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22100256">The Design, Synthesis and Biological Evaluations of C-6 or C-7 Substituted 2-Hydroxyisoquinoline-1,3-diones as Inhibitors of Hepatitis C Virus.</a> Chen, Y.L., J. Tang, M.J. Kesler, Y.Y. Sham, R. Vince, R.J. Geraghty, and Z. Wang, Bioorganic &amp; Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22100256].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22085716">Potent and Selective Inhibition of Hepatitis C Virus Replication by Novel Phenanthridinone Derivatives.</a> Salim, M.T., H. Aoyama, K. Sugita, K. Watashi, T. Wakita, T. Hamasaki, M. Okamoto, Y. Urata, Y. Hashimoto, and M. Baba, Biochemical and Biophysical Research Communications, 2011. <b>[Epub ahead of print]</b>; PMID[22085716].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22083468">PD 404,182 is a Virucidal Small Molecule that Disrupts Hepatitis C Virus and Human Immunodeficiency Virus.</a> Chamoun, A.M., K. Chockalingam, M. Bobardt, R. Simeon, J. Chang, P. Gallay, and Z. Chen. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22083468].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22076990">Inhibition of Hepatitis C Virus Replication by Monascus Pigment Derivatives that Interfere with Viral RNA Polymerase Activity and the Mevalonate Biosynthesis Pathway.</a> Sun, J.M., S.J. Kim, G.W. Kim, J.K. Rhee, N.D. Kim, H. Jung, J. Jeun, S.H. Lee, S.H. Han, C.S. Shin, and J.W. Oh, The Journal of Antimicrobial Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22076990].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22098176">Multiple Effects of Honokiol on the Life Cycle of Hepatitis C Virus.</a> Lan, K.H., Y.W. Wang, W.P. Lee, K.L. Lan, S.H. Tseng, L.R. Hung, S.H. Yen, H.C. Lin, and S.D. Lee, Liver International: Official Journal of the International Association for the Study of the Liver, 2011. <b>[Epub ahead of print]</b>; PMID[22098176].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22106211">In Vitro Evaluation of the Activities of the Novel anti-Cytomegalovirus Compound AIC246 (letermovir) against Herpesviruses and Other Human Pathogenic Viruses.</a> Marschall, M., T. Stamminger, A. Urban, S. Wildum, H. Ruebsamen-Schaeff, H. Zimmermann, and P. Lischka, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22106211].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes Simplex Virus 2</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22085753">Structures and anti-HSV-2 Activities of Neutral Polysaccharides from an Edible Plant, Basella rubra L.</a> Dong, C.X., K. Hayashi, Y. Mizukoshi, J.B. Lee, and T. Hayashi, International Journal of Biological Macromolecules, 2011. <b>[Epub ahead of print]</b>; PMID[22085753].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1111-112411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296318300014">Development of New Sulfur-containing Conjugated Compounds as anti-HCV Agents.</a> Hwu, J.R., S.Y. Lin, S.C. Tsay, R. Singha, B.K. Pal, P. Leyssen, and J. Neyts, Phosphorus Sulfur and Silicon and the Related Elements, 2011. 186(5): p. 1144-1152; ISI[000296318300014].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296423700043">Discovery of Highly Potent Small Molecule Hepatitis C Virus Entry Inhibitors.</a> Mittapalli, G.K., A. Jackson, F. Zhao, H. Lee, S. Chow, J. McKelvy, F. Wong-Staal, and J.E. Macdonald, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(22): p. 6852-6855; ISI[000296423700043].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296115600056">(-)-Epigallocatechin gallate Inhibits Hepatitis C Virus (HCV) Viral Protein NS5B.</a> Roh, C. and S.K. Jo, Talanta, 2011. 85(5): p. 2639-2642; ISI[000296115600056].
    <br />
    <b>[WOS]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295858700040">Target Specific Hyaluronic acid-Interferon Alpha Conjugate for the Treatment of Hepatitis C Virus Infection.</a> Yang, J.A., K. Park, H. Jung, H. Kim, S.W. Hong, S.K. Yoon, and S.K. Hahn, Biomaterials, 2011. 32(33): p. 8722-8729; ISI[000295858700040].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296423700030">Synthesis of Purine Modified 2 &#39;-C-Methyl nucleosides as Potential anti-HCV Agents.</a> Zhang, H.W., L.H. Zhou, S.J. Coats, T.R. McBrayer, P.M. Tharnish, L. Bondada, M. Detorio, S.A. Amichai, M.D. Johns, T. Whitaker, and R.F. Schinazi, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(22): p. 6788-6792; ISI[000296423700030].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1111-112411.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296045300073">Complement L-Ficolin Binds to Surface Glycans of HCV and Reduces the Viral Infectivity, and Functions as an Antiviral Opsonin.</a> Zhao, Y.N., Y.D. Zhou, K. Yang, and X.L. Zhang, Glycobiology, 2011. 21(11): p. 1475-1475; ISI[000296045300073].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1111-112411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
