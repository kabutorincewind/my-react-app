

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-07-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9oAwjR3lsl8VQM3a/ViwkMnUGn2eD09Spckt4RL35j8AMx7uBlDJFB9c616+egqjEbx5rFjKnc37NQcp9Hm3W1zTjlWdOTMGWSt/OSAhjfZg+OfAmnH9FajBhLM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="623985F0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: July 18 - July 31, 2014</h1>

    <h2>Herpes Simplex Virus I</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25033084">An Attenuated Herpes Simplex Virus Type 1 (HSV1) Encoding the HIV-1 Tat Protein Protects Mice from a Deadly Mucosal HSV1 Challenge.</a> Sicurella, M., F. Nicoli, E. Gallerani, I. Volpi, E. Berto, V. Finessi, F. Destro, R. Manservigi, A. Cafaro, B. Ensoli, A. Caputo, R. Gavioli, and P.C. Marconi. PLoS One, 2014. 9(7): p. e100844. PMID[25033084].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0718-073114.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337917900016">Turmeric Curcumin Inhibits Entry of All Hepatitis C Virus Genotypes into Human Liver Cells.</a> Anggakusuma, C.C. Colpitts, L.M. Schang, H. Rachmawati, A. Frentzen, S. Pfaender, P. Behrendt, R.J.P. Brown, D. Bankwitz, J. Steinmann, M. Ott, P. Meuleman, C.M. Rice, A. Ploss, T. Pietschmann, and E. Steinmann. Gut, 2014. 63(7): p. 1137-1149. ISI[000337917900016].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338410100021">Syntheses of Nucleosides with 2 &#39;-Spirolactam and 2 &#39;-Spiropyrrolidine Moieties as Potential Inhibitors of Hepatitis C Virus NS5B Polymerase.</a> Dang, Q., Z.B. Zhang, B.Y. Tang, Y. Song, L. Wu, T.Q. Chen, S. Bogen, V. Girijavallabhan, D.B. Olsen, and P.T. Meinke. Tetrahedron Letters, 2014. 55(28): p. 3813-3816. ISI[000338410100021].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338412500009">An Interferon-beta Promoter Reporter Assay for High Throughput Identification of Compounds against Multiple RNA Viruses.</a> Guo, F., X.S. Zhao, T. Gill, Y. Zhou, M. Campagna, L.J. Wang, F. Liu, P.H. Zhang, L. DiPaolo, Y.M. Du, X.D. Xu, D. Jiang, L. Wei, A. Cuconati, T.M. Block, J.T. Guo, and J.H. Chang. Antiviral Research, 2014. 107: p. 56-65. ISI[000338412500009].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338109800028">In vitro Inhibition of the Bovine Viral Diarrhoea Virus by the Essential Oil of Ocimum basilicum (Basil) and Monoterpenes.</a> Kubica, T.F., S.H. Alves, R. Weiblen, and L.T. Lovato. Brazilian Journal of Microbiology, 2014. 45(1): p. 209-214. ISI[000338109800028].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337871100005">Anti-Hepatitis B Virus Norbisabolane Sesquiterpenoids from Phyllanthus acidus and the Establishment of Their Absolute Configurations Using Theoretical Calculations.</a> Lv, J.J., S. Yu, Y.F. Wang, D. Wang, H.T. Zhu, R.R. Cheng, C.R. Yang, M. Xu, and Y.J. Zhang. Journal of Organic Chemistry, 2014. 79(12): p. 5432-5447. ISI[000337871100005].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338598200028">Protective Effect of Curcumin against Cytomegalovirus Infection in BALB/c Mice.</a> Lv, Y.L., N. Lei, D. Wang, Z.L. An, G.R. Li, F.F. Han, H. Liu, and L.H. Liu. Environmental Toxicology and Pharmacology, 2014. 37(3): p. 1140-1147. ISI[000338598200028].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337871100024">Rigid 2 &#39;,4 &#39;-Difluororibonucleosides: Synthesis, Conformational Analysis, and Incorporation into Nascent RNA by HCV Polymerase.</a> Martinez-Montero, S., G.F. Deleavey, A. Kulkarni, N. Martin-Pintado, P. Lindovska, M. Thomson, C. Gonzalez, M. Gotte, and M.J. Damha. Journal of Organic Chemistry, 2014. 79(12): p. 5627-5635. ISI[000337871100024].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337870900007">Synthesis of a Nucleoside Phosphoramidate Prodrug Inhibitor of HCV NS5B Polymerase: Phenylboronate as a Transient Protecting Group.</a> Mayes, B.A., J. Arumugasamy, E. Baloglu, D. Bauer, A. Becker, N. Chaudhuri, G.M. Latham, J. Li, S. Mathieu, F.P. McGarry, E. Rosinovsky, A. Stewart, C. Trochet, J.Y. Wang, and A. Moussa. Organic Process Research &amp; Development, 2014. 18(6): p. 717-724. ISI[000337870900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337899300008">In Silico Screening for Potent Inhibitors against the NS3/4A Protease of Hepatitis C Virus.</a> Meeprasert, A., T. Rungrotmongkol, M.S. Li, and S. Hannongbua. Current Pharmaceutical Design, 2014. 20(21): p. 3465-3477. ISI[000337899300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0718-073114.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
