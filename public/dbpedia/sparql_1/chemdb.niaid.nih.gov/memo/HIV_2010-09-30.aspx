

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-09-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Rd+U+sgxQEYYImgcKywEpwtlLCqdLGCurGGRKciwXyk9Ngirpx0znBgU7tu3+D2R2o4MfkF7+v3/vv6Xke0kRBy158xRL6TAkKLi9t3R27bgJXbb22uAJ1KQKwE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AD517486" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  September 17 - September 30, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20728367">Anti-AIDS agents 79. Design, synthesis, molecular modeling and structure-activity relationships of novel dicamphanoyl 2&#39;,2&#39;-dimethyldihydropyranochromone (DCP) analogs as potent anti-HIV agents.</a> Zhou T, Shi Q, Chen CH, Zhu H, Huang L, Ho P, Lee KH. Bioorganic &amp; Medicinal Chemistry. 2010; 18(18): 6678-6689; PMID[20728367].</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20876377">EASY-HIT: HIV full-replication technology for broad discovery of multiple classes of HIV inhibitors.</a>  Kremb S, Helfer M, Heller W, Hoffmann D, Wolff H, Kleinschmidt A, Cepok S, Hemmer B, Durner J, Brack-Werner R.  Antimicrobial Agents and Chemotherapy. 2010 Sep 27. <b>[Epub ahead of print]</b>; PMID[20876377]</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20875373">Antiretroviral activity of two polyisoprenylated acylphloroglucinols, 7-epi-nemorosone and plukenetione A, isolated from Caribbean propolis.</a>  Díaz-Carballo D, Ueberla K, Kleff V, Ergun S, Malak S, Freistuehler M, Somogyi S, Kücherer C, Bardenheuer W, Strumberg D.  International Journal of Clinical Pharmacology and Therapeutics. 2010; 48(10): 670-677; PMID[20875373]</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20869872">Potent and selective HIV-1 ribonuclease H inhibitors based on a 1-hydroxy-1,8-naphthyridin-2(1H)-one scaffold.</a>Williams PD, Staas DD, Venkatraman S, Loughran HM, Ruzek RD, Booth TM, Lyle TA, Wai JS, Vacca JP, Feuston BP, Ecto LT, Flynn JA, Distefano DJ, Hazuda DJ, Bahnck CM, Himmelberger AL, Dornadula G, Hrin RC, Stillmock KA, Witmer MV, Miller MD, Grobler JA.  Bioorganic &amp; Medicinal Chemistry Letters. 2010. <b>[Epub ahead of print]</b>; PMID[20869872]</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20854256">Recent Advances in the Development of Small-molecule CCR5 Inhibitors for HIV.</a>  Liu T, Weng Z, Dong X, Hu Y.  Mini Reviews in Medicinal Chemistry. 2010. <b>[Epub ahead of print]</b>; PMID[20854256]</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20850978">Small molecules targeting the interaction between HIV-1 integrase and LEDGF/p75 cofactor.</a>  De Luca L, Ferro S, Gitto R, Barreca ML, Agnello S, Christ F, Debyser Z, Chimirri A.  Bioorganic &amp; Medicinal Chemistry. 2010. <b>[Epub ahead of print]</b>; PMID[20850978]</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20832304">Validated predictive QSAR modeling of N-aryl-oxazolidinone-5-carboxamides for anti-HIV protease activity.</a>  Halder AK, Jha T.  Bioorganic &amp; Medicinal Chemistry Letters. 2010; 20(20): 6082-6087; PMID[20832304]</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20829038">Exploration of piperidine-4-yl-aminopyrimidines as HIV-1 reverse transcriptase inhibitors. N-Phenyl derivatives with broad potency against resistant mutant viruses.</a>  Tang G, Kertesz DJ, Yang M, Lin X, Wang Z, Li W, Qiu Z, Chen J, Mei J, Chen L, Mirzadegan T, Harris SF, Villaseñor AG, Fretland J, Fitch WL, Hang JQ, Heilek G, Klumpp K.  Bioorganic &amp; Medicinal Chemistry Letters. 2010; 20(20): 6020-6023; PMID[20829038]</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20727748">Design of a series of bicyclic HIV-1 integrase inhibitors. Part 1: selection of the scaffold.</a>  Jones ED, Vandegraaff N, Le G, Choi N, Issa W, Macfarlane K, Thienthong N, Winfield LJ, Coates JA, Lu L, Li X, Feng X, Yu C, Rhodes DI, Deadman JJ.  Bioorganic &amp; Medicinal Chemistry Letters. 2010; 20(19): 5913-5917; PMID[20727748]</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20660667">Lersivirine, a nonnucleoside reverse transcriptase inhibitor with activity against drug-resistant human immunodeficiency virus type 1.</a>  Corbau R, Mori J, Phillips C, Fishburn L, Martin A, Mowbray C, Panton W, Smith-Burchnell C, Thornberry A, Ringrose H, Knöchel T, Irving S, Westby M, Wood A, Perros M.  Antimicrobial Agents and Chemotherapy. 2010 ; 54(10):4451-4463; PMID[20660667]</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20625151">Phosphorothioate 2&#39; deoxyribose oligomers as microbicides that inhibit human immunodeficiency virus type 1 (HIV-1) infection and block Toll-like receptor 7 (TLR7) and TLR9 triggering by HIV-1.</a>  Fraietta JA, Mueller YM, Do DH, Holmes VM, Howett MK, Lewis MG, Boesteanu AC, Alkan SS, Katsikis PD.  Antimicrobial Agents and Chemotherapy. 2010; (10): 4064-4073; PMID[20625151]</p>

    <p class="title"> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="memofmt2-2"> </p>

    <p class="ListParagraph">12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281667200005">Synthesis, Biological Evaluation and Molecular Modeling Studies of N-aryl-2-arylthioacetamides as Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors</a>.  Zhu, XH;  Qin, Y; Yan, H;  Song, XQ;  Zhong, RR.  Chemical Biology &amp; Drug Design, 2010; 76(4): 330-339.</p>

    <p class="ListParagraph">13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281700100012">Antiviral Activity Enhancement through the SATE Prodrug of a 2 &#39;-Modified 5 &#39;-Norcarbocyclic Adenine Analogue.</a>  Li, H; Kim, SW; Hong, JH.  Bulletin of the Korean Chemical Society, 2010; 31(8): 2180-2184.</p>

    <p class="ListParagraph">14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281735600067">Design of a series of bicyclic HIV-1 integrase inhibitors. Part 2:Azoles: Effective metal chelators.</a> Le, GA;  Vandegraaff, N;  Rhodes, DI;  Jones, ED; Coates, JAV;  Thienthong, N;  Winfield, LJ;  Lu, L;  Li, XM;  Yu, CJ;  Feng, X;  Deadman, JJ.  Bioorganic &amp; Medicinal Chemistry Letters, 2010; 20(19):  5909-5912.</p>

    <p class="ListParagraph">15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281568600062">Synthesis of substituted thieno[2,3-d]pyrimidine-2,4-dithiones and their S-glycoside analogues as potential antiviral and antibacterial agents</a>.  Hafez, HN;  Hussein, HAR;  El-Gazzar, ARBA.  European Journal of Medicinal Chemistry, 2010; 45(9):  4026-4034.</p>

    <p class="ListParagraph">16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281698800009">HIV-1 Gag virus-like particles inhibit HIV-1 replication in dendritic cells through up-regulation of APOBEC3 expression.</a> Chang, MO;  Suzuki, T;  Suzuki, H;  Watanabe, M;  Takakua, H.  European Journal of Medical Research, 2010; 18(5): 325-325.</p>

    <p class="ListParagraph">17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281568600054">QSAR study of carboxylic acid derivatives as HIV-1 Integrase inhibitors</a>.  Cheng, ZJ;  Zhang, YT;  Fu, WZ.   European Journal of Medicinal Chemistry, 2010; 45(9):  3970-3980.</p>

    <p class="ListParagraph">18.   T<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281568600071">ricyclononene carboxamide derivatives as novel anti-HIV-1 agents.</a>   Dong, MX; Zhang, JA; Peng, XQ; Lu, H;  Yun, LH; Jiang, SB;  Dai, QY. European Journal of Medicinal Chemistry, 2010; 45(9): 4096-4103.</p>

    <p class="ListParagraph">19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281698800030">Design, synthesis and anti-HIV-1 evaluation of novel substituted uracil derivatives as potent NNRTIs</a>.  Ordonez, P  Hamasaki, T;  Isono, Y;  Ikejiri, M;  Sakakibara, N;  Maruyama, T;  Baba, M.  European Journal of Medical Research, 2010; 18(5): 333-333.</p>

    <p class="ListParagraph">20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281568600032">Synthesis, structure-activity relationship and antiviral activity of 3 &#39;-N,N-dimethylamino-2 &#39;,3 &#39;-dideoxythymidine and its prodrugs</a>.   Singh, RK; Yadav, D; Rai, D; Kumari, G; Pannecouque, C; De Clercq, E. European Journal of Medicinal Chemistry, 2010; 45(9):  3787-3793.</p>

    <p class="ListParagraph">21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281524700020">Peptidic HIV integrase inhibitors derived from HIV gene products:  Structure-activity relationship studies.</a>  Suzuki, S;  Maddali, K;  Hashimoto, C;  Urano, E;  Ohashi, N;  Tanaka, T;  Ozaki, T;  Arai, H;  Tsutsumi, H;  Narumi, T;  Nomura, W;  Yamamoto, N;  Pommier, Y;  Komano, JA;  Tamamura, H. Bioorganic &amp; Medicinal Chemistry, 2010; 18(18):  6771-6775.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
