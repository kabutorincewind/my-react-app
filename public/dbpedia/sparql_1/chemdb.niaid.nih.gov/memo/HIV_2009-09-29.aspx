

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-09-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TKgNfknWrgrq+/wXTMFQAh4taloL4SDXztSZusWy/aZP5sqWHSpsp7RrzQGVGuQ7+DbiE3SDnJFIT5ftLYdDOjP+B9ngoMJkJV9DW6X9SxikBqmwSKwsm/BEX+Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4D060DFB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  September 16 - September 29, 2009</h1> 

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19786602?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">ADS-J1 inhibits HIV-1 entry by interacting with the gp41 pocket region and blocking the fusion-active gp41 core formation.</a>Wang H, Qi Z, Guo A, Mao Q, Lu H, An X, Xia C, Li X, Debnath AK, Wu S, Liu S, Jiang S. Antimicrob Agents Chemother. 2009 Sep 28. [Epub ahead of print] PMID: 19786602</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19780144?ordinalpos=16&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Utilization of in vitro Caco-2 permeability and liver microsomal half-life screens in discovering BMS-488043, a novel HIV-1 attachment inhibitor with improved pharmacokinetic properties.</a> Yang Z, Zadjura LM, Marino AM, D&#39;Arienzo CJ, Malinowski J, Gesenberg C, Lin PF, Colonno RJ, Wang T, Kadow JF, Meanwell NA, Hansel SB. J Pharm Sci. 2009 Sep 24. [Epub ahead of print] PMID: 19780144</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19769332?ordinalpos=41&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibitors of Human Immunodeficiency Virus Type 1 (HIV-1) Attachment. 5. An Evolution from Indole to Azaindoles Leading to the Discovery of 1-(4-Benzoylpiperazin-1-yl)-2-(4,7-dimethoxy-1H-pyrrolo[2,3-c]pyridin-3-yl)ethane-1,2-dione (BMS-488043), a Drug Candidate That Demonstrates Antiviral Activity in HIV-1-Infected Subjects (infinity).</a> Wang T, Yin Z, Zhang Z, Bender JA, Yang Z, Johnson G, Yang Z, Zadjura LM, D&#39;Arienzo CJ, Digiugno Parker D, Gesenberg C, Yamanaka GA, Gong YF, Ho HT, Fang H, Zhou N, McAuliffe BV, Eggers BJ, Fan L, Nowicka-Sans B, Dicker IB, Gao Q, Colonno RJ, Lin PF, Meanwell NA, Kadow JF. J Med Chem. 2009 Sep 21. [Epub ahead of print] PMID: 19769332</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>4.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269703900010">Quantitative Structure-Activity Relationship Studies on 2-Amino-6-arylsulfonylbenzonitriles as Human Immunodeficiency Viruses Type 1 Reverse Transcriptase Inhibitors Using Descriptors Obtained from Substituents and Whole Molecular Structures.</a>  Hemmateenejad, B; Sabet, R; Fassihi, A.  CHEMICAL BIOLOGY &amp; DRUG DESIGN, 2009; 74(4): 405-415.</p>

    <p>5.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269747700001">Optimal drug combinations and minimal hitting sets.</a>Vazquez, A. BMC SYSTEMS BIOLOGY, 2009; 3: 4930U. </p>

    <p>6.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269760900026">Spiropiperidine CCR5 antagonists.</a> Rotstein, DM; Gabriel, SD; Makra, F; Filonova, L; Gleason, S; Brotherton-Pleiss, C; Setti, LQ; Trejo-Martin, A; Lee, EK; Sankuratri, S; Ji, CH; deRosier, A; Dioszegi, M; Heilek, G; Jekle, A; Berry, P; Weller, P; Mau, CI.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(18): 5401-5406.</p>

    <p>7.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269710700001">Dipyridodiazepinone derivatives; synthesis and anti HIV-1 activity.</a>  Khunnawutmanotham, N; Chimnoi, N; Thitithanyanont, A; Saparpakorn, P; Choowongkomon, K; Pungpo, P; Hannongbua, S; Techasakul, S. BEILSTEIN JOURNAL OF ORGANIC CHEMISTRY, 2009; 5: 493CK</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
