

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-10-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X1gnbbS01GiYbhcPAvTSrBXwYA8O/+B1pq2DYwNSbGD9G6apnT51HBAP/MHxNANLk2TxLfEp4u013VJ6CGmPcH+4T+BbBwkQdrHvfUqSQhsrdgQK92B5+lh2aAg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="325EE1DC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  September 30 - October 13, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19807678?ordinalpos=20&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">PRO 140 - A Novel CCR5 Co-Receptor Inhibitor.</a>  Khatib N, Das S.  Recent Pat Antiinfect Drug Discov. 2009 Oct 7. [Epub ahead of print]  PMID: 19807678</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19807674?ordinalpos=21&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Purification and Characterization of a Laccase with Inhibitory Activity toward HIV-1 Reverse Transcriptase and Tumor Cells from an Edible Mushroom (Pleurotus cornucopiae).</a>  Wong JH, Ng TB, Jiang Y, Liu F, Cho S, Sze W, Zhang KY.  Protein Pept Lett. 2009 Oct 7. [Epub ahead of print]  PMID: 19807674</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19807124?ordinalpos=23&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Chemical Library Screens Targeting an HIV-1 Accessory Factor/Host Cell Kinase Complex Identify Novel Anti-retroviral Compounds.</a>  Emert-Sedlak L, Kodama T, Lerner EC, Dai W, Foster C, Day B, Lazo JS, Smithgall TE.  ACS Chem Biol. 2009 Oct 6. [Epub ahead of print]  PMID: 19807124</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19805571?ordinalpos=25&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A New Small Molecule Inhibitor Class Targeting HIV-1 Virion Maturation.</a>  Blair WS, Cao J, Fok-Seang J, Griffin P, Isaacson J, Jackson RL, Murray E, Patick AK, Peng Q, Perros M, Pickford C, Wu H, Butler SL.  Antimicrob Agents Chemother. 2009 Oct 5. [Epub ahead of print]  PMID: 19805571</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19779642?ordinalpos=49&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Plant-derived triterpenoids and analogues as antitumor and anti-HIV agents.</a>  Kuo RY, Qian K, Morris-Natschke SL, Lee KH.  Nat Prod Rep. 2009 Oct;26(10):1321-1344. Epub 2009 Aug 13.  PMID: 19779642</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19734912?ordinalpos=60&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A synthetic CD4-heparan sulfate glycoconjugate inhibits CCR5 and CXCR4 HIV-1 attachment and entry.</a>  Baleux F, Loureiro-Morais L, Hersant Y, Clayette P, Arenzana-Seisdedos F, Bonnaffé D, Lortat-Jacob H.  Nat Chem Biol. 2009 Oct;5(10):743-8. Epub 2009 Sep 6.  PMID: 19734912</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19717303?ordinalpos=63&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pyrazole NNRTIs 3: optimisation of physicochemical properties.</a>  Mowbray CE, Corbau R, Hawes M, Jones LH, Mills JE, Perros M, Selby MD, Stupple PA, Webster R, Wood A.  Bioorg Med Chem Lett. 2009 Oct 1;19(19):5603-6. Epub 2009 Aug 14.  PMID: 19717303</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19709880?ordinalpos=66&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pyrazole NNRTIs 1: design and initial optimisation of a novel template.</a>  Mowbray CE, Burt C, Corbau R, Perros M, Tran I, Stupple PA, Webster R, Wood A.  Bioorg Med Chem Lett. 2009 Oct 1;19(19):5599-602. Epub 2009 Aug 13.  PMID: 19709880</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19640982?ordinalpos=90&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Small molecule targets Env for endoplasmic reticulum-associated protein degradation and inhibits human immunodeficiency virus type 1 propagation.</a>  Jejcic A, Daniels R, Goobar-Larsson L, Hebert DN, Vahlne A.  J Virol. 2009 Oct;83(19):10075-84. Epub 2009 Jul 29.  PMID: 19640982</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19470798?ordinalpos=136&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, expression, and characterization of a multivalent, combination HIV microbicide.</a>  Sexton A, Harman S, Shattock RJ, Ma JK.  FASEB J. 2009 Oct;23(10):3590-600. Epub 2009 May 26.  PMID: 19470798</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="ListParagraph">11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270201900006">Synthesis and Anti-Human Immunodeficiency Virus Type 1 Activity of (E)-N-Phenylstyryl-N-alkylacetamide Derivatives.</a>  Cheng, P; Chen, JJ; Huang, N; Wang, RR; Zheng, YT; Liang, YZ.  MOLECULES, 2009; 14(9): 3176-3186.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270106700039">Structure-based design of substituted biphenyl ethylene ethers as ligands binding in the hydrophobic pocket of gp41 and blocking the helical bundle formation.</a>  Liu, B; Joseph, RW; Dorsey, BD; Schiksnis, RA; Northrop, K; Bukhtiyarova, M; Springman, EB.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(19): 5693-5697.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270107000034">Synthesis and biological evaluation of new heterocyclic quinolinones as anti-parasite and anti-HIV drug candidates.</a>  Darque, A; Dumetre, A; Hutter, S; Casano, G; Robin, M; Pannecouque, C; Azas, N.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(20): 5962-5964.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269831900020">Phosphodiester Substrates for Incorporation of Nucleotides in DNA Using HIV-1 Reverse Transcriptase.</a>  Giraut, A; Dyubankova, N; Song, XP; Herdewijn, P.  CHEMBIOCHEM, 2009; 2246-2252.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
