

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-01-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="T6mDpAIpOoDlM3z968ixTemg1cjvhKJhaze2/EIrGvTHMXR+/t0MUH33l9Q9uCs+uBdP0eNZGfojdlpacZG3oPp1rBHie4J98Qc5HvZBThonxO/psRcNWqJ7byg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0CD00E1B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">January 6 - January 19, 2012</h1>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22243686">Trivalent Ultrashort Lipopeptides are Potent Ph Dependent Antifungal Agents.</a> Arnusch, C.J., H.B. Albada, M. van Vaardegem, R.M. Liskamp, H.G. Sahl, Y. Shadkchan, N. Osherov, and Y. Shai. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22243686].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22246961">Susceptibilities of Candida albicans Mouth Isolates to Antifungal Agents, Essentials Oils and Mouth Rinses.</a> Carvalhinho, S., A.M. Costa, A.C. Coelho, E. Martins, and A. Sampaio. Mycopathologia, 2012. <b>[Epub ahead of print]</b>; PMID[22246961].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22232293">Effect of Ph on in Vitro Susceptibility of Candida glabrata and Candida albicans to Eleven Antifungal Agents - Implications for Clinical Use.</a> Danby, C.S., D. Boikov, R. Rautemaa, and J.D. Sobel. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22232293].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22225420">Chemical Compositions of the Essential Oils of Aerial Parts of Chamaemelum Mixtum (L.) Alloni.</a> Darriet, F., M. Bendahou, J. Costa, and A. Muselli. Journal of Agricultural and Food Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22225420].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22248145">Cranberry Proanthocyanidins Inhibit the Adherence Properties of Candida albicans and Cytokine Secretion by Oral Epithelial Cells.</a> Feldman, M., S. Tanabe, A. Howell, and D. Grenier. Bmc Complementary and Alternative Medicine, 2012. 12(1): p. 6; PMID[22248145].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238015">Novel Copper-Based Therapeutic Agent for Anti-Inflammatory: Synthesis, Characterization, and Biochemical Activities of Copper(Ii) Complexes of Hydroxyflavone Schiff Bases.</a> Joseph, J. and K. Nagashri. Applied Biochemistry and Biotechnology, 2012. <b>[Epub ahead of print]</b>; PMID[22238015].<b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22237925">Chemical Composition and Antifungal Activity of Essential Oils and Supercritical Co(2) Extracts of Apium nodiflorum (L.) lag.</a> Maxia, A., D. Falconieri, A. Piras, S. Porcedda, B. Marongiu, M.A. Frau, M.J. Goncalves, C. Cabral, C. Cavaleiro, and L. Salgueiro. Mycopathologia, 2012. <b>[Epub ahead of print]</b>; PMID[22237925].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22075455">In Vitro Antimicrobial Synergism within Plant Extract Combinations from Three South African Medicinal Bulbs.</a> Ncube, B., J.F. Finnie, and J. Van Staden. Journal of Ethnopharmacology, 2012. 139(1): p. 81-89; PMID[22075455].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22249992">Antimicrobial Activity of Peptides Derived from Human Ss-Amyloid Precursor Protein.</a> Papareddy, P., M. Morgelin, B. Walse, A. Schmidtchen, and M. Malmsten. Journal of Peptide Science: An Official Publication of the European Peptide Society, 2012. <b>[Epub ahead of print]</b>; PMID[22249992].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22246169">Calix[4]arene Based 1,3,4-Oxadiazole and Thiadiazole Derivatives: Design, Synthesis, and Biological Evaluation.</a> Patel, M.B., N.R. Modi, J.P. Raval, and S.K. Menon. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22246169].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22233267">Species and Susceptibility Distribution of 1062 Clinical Yeast Isolates to Azoles, Echinocandins, Flucytosine and Amphotericin B from a Multi-Centre Study.</a> Schmalreck, A.F., B. Willinger, G. Haase, G. Blum, C. Lass-Florl, W. Fegeler, and K. Becker. Mycoses, 2012. <b>[Epub ahead of print]</b>; PMID[22233267].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22197387">Discovery of Novel Antitubercular 1,5-Dimethyl-2-phenyl-4-([5-(arylamino)-1,3,4-oxadiazol-2-Yl]methylamino)-1,2-dihydro-3H-pyrazol-3-one Analogues.</a> Ahsan, M.J., J.G. Samy, C.B. Jain, K.R. Dutt, H. Khalilullah, and M.S. Nomani. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(2): p. 969-972; PMID[22197387].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22098589">Ethionamide Boosters. 2. Combining Bioisosteric Replacement and Structure-based Drug Design to Solve Pharmacokinetic Issues in a Series of Potent 1,2,4-Oxadiazole EthR Inhibitors.</a> Flipo, M., M. Desroses, N. Lecat-Guillet, B. Villemagne, N. Blondiaux, F. Leroux, C. Piveteau, V. Mathys, M.P. Flament, J. Siepmann, V. Villeret, A. Wohlkonig, R. Wintjens, S.H. Soror, T. Christophe, H.K. Jeon, C. Locht, P. Brodin, B. Deprez, A.R. Baulard, and N. Willand. Journal of Medicinal Chemistry, 2012. 55(1): p. 68-83; PMID[22098589].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22233564">Investigating the Spectrum of Biological Activity of Substituted Quinoline-2-carboxamides and Their Isosteres.</a> Gonec, T., P. Bobal, J. Sujan, M. Pesko, J. Guo, K. Kralova, L. Pavlacka, L. Vesely, E. Kreckova, J. Kos, A. Coffey, P. Kollar, A. Imramovsky, L. Placek, and J. Jampilek. Molecules (Basel, Switzerland), 2012. 17(1): p. 613-644; PMID[22233564].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22197138">In Vitro and in Silico Studies of Polycondensed Diazine Systems as Anti-parasitic Agents.</a> Almerico, A.M., M. Tutone, A. Guarcello, and A. Lauria. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(2): p. 1000-4; PMID[22197138].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22178188">Synthesis of 9-Phosphonoalkyl and 9-Phosphonoalkoxyalkyl Purines: Evaluation of Their Ability to Act as Inhibitors of Plasmodium falciparum, Plasmodium vivax and Human Hypoxanthine-Guanine-(Xanthine) Phosphoribosyltransferases.</a> Cesnek, M., D. Hockova, A. Holy, M. Dracinsky, O. Baszczynski, J. Jersey, D.T. Keough, and L.W. Guddat. Bioorganic &amp; Medicinal Chemistry, 2012. 20(2): p. 1076-1089; PMID[22178188].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22249579">Ruthenium(Ii) arene Complexes with Chelating Chloroquine Analogue Ligands: Synthesis, Characterization and in Vitro Antimalarial Activity.</a> Glans, L., A. Ehnbom, C. de Kock, A. Martinez, J. Estrada, P.J. Smith, M. Haukka, R.A. Sanchez-Delgado, and E. Nordlander. Dalton Transactions (Cambridge, England : 2003), 2012. <b>[Epub ahead of print]</b>; PMID[22249579].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22192590">Design and Synthesis of Small Molecular Dual Inhibitor of Falcipain-2 and Dihydrofolate Reductase as Antimalarial Agent.</a> Huang, H., W. Lu, X. Li, X. Cong, H. Ma, X. Liu, Y. Zhang, P. Che, R. Ma, H. Li, X. Shen, H. Jiang, J. Huang, and J. Zhu. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(2): p. 958-962; PMID[22192590].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22101085">Ethnobotanical Study of Antimalarial Plants in Shinile District, Somali Region, Ethiopia, and in Vivo Evaluation of Selected Ones against Plasmodium berghei.</a> Mesfin, A., M. Giday, A. Animut, and T. Teklehaymanot. Journal of Ethnopharmacology, 2012. 139(1): p. 221-227; PMID[22101085].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22142474">New Antimalarial Indolone-N-oxides, Generating Radical Species, Destabilize the Host Cell Membrane at Early Stages of Plasmodium falciparum Growth: Role of Band 3 Tyrosine Phosphorylation.</a> Pantaleo, A., E. Ferru, R. Vono, G. Giribaldi, O. Lobina, F. Nepveu, H. Ibrahim, J.P. Nallet, F. Carta, F. Mannu, P. Pippia, E. Campanella, P.S. Low, and F. Turrini. Free Radical Biology &amp; Medicine, 2012. 52(2): p. 527-536; PMID[22142474].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22204908">New Benzimidazole Derivatives as Antiplasmodial Agents and Plasmepsin Inhibitors: Synthesis and Analysis of Structure-Activity Relationships.</a> Saify, Z.S., M.K. Azim, W. Ahmad, M. Nisa, D.E. Goldberg, S.A. Hussain, S. Akhtar, A. Akram, A. Arayne, A. Oksman, and I.A. Khan. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(2): p. 1282-1286; PMID[22204908].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22248242">Antiplasmodial Activity of Steroidal Chalcones: Evaluation of Their Effect on Hemozoin Synthesis and the New Permeation Pathway of Plasmodium falciparum-Infected Erythrocyte Membrane.</a> Sisodia, B.S., A.S. Negi, M.P. Darokar, U.N. Dwivedi, and S.P. Khanuja. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22248242].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22196513">Synthesis, Biological Evaluation and Mechanistic Studies of Totarol amino alcohol Derivatives as Potential Antimalarial Agents.</a> Tacon, C., E.M. Guantai, P.J. Smith, and K. Chibale. Bioorganic &amp; Medicinal Chemistry, 2012. 20(2): p. 893-902; PMID[22196513].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22182577">Febrifugine Analogue Compounds: Synthesis and Antimalarial Evaluation.</a> Zhu, S., G. Chandrashekar, L. Meng, K. Robinson, and D. Chatterji. Bioorganic &amp; Medicinal Chemistry, 2012. 20(2): p. 927-932; PMID[22182577].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0106-011912.</p>

    <p class="memofmt2-2"> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298411200051">Chemical Constituents of the Methanolic Extract of Leaves of Leiothrix spiralis ruhland and Their Antimicrobial Activity.</a> Araujo, M.G.D., F. Hilario, L.G. Nogueira, W. Vilegas, L.C. dos Santos, and T.M. Bauab. Molecules, 2011. 16(12): p. 10479-10490; ISI[000298411200051].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298411200040">Synthesis, Characterization, Anti-inflammatory and in Vitro Antimicrobial Activity of Some Novel Alkyl/Aryl substituted tertiary alcohols.</a> Baseer, M., F.L. Ansari, Z. Ashraf, and R. SaeedulHaq. Molecules, 2011. 16(12): p. 10337-10346; ISI[000298411200040].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298411200063">Microwave-assisted Synthesis of New N(1),N(4)-Substituted thiosemicarbazones.</a> dos Reis, C.M., D.S. Pereira, R.D. Paiva, L.F. Kneipp, and A. Echevarria. Molecules, 2011. 16(12): p. 10668-10684; ISI[000298411200063].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297960100008">Oxathiazole-2-one Derivative of Bortezomib: Synthesis, Stability and Proteasome Inhibition Activity.</a> Gryder, B.E., W. Guerrant, C.H. Chen, and A.K. Oyelere. Medchemcomm, 2011. 2(11): p. 1083-1086; ISI[000297960100008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297457400013">In Vitro Antibacterial and Antifungal Activity of Iraqi propolis.</a> Hendi, N.K.K., H.S. Naher, and A.H. Al-Charrakh. Journal of Medicinal Plants Research, 2011. 5(20): p. 5058-5066; ISI[000297457400013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298174900006">Candidacidal Action of Cf66i, an Antifungal Compound Produced by Burkholderia cepacia.</a> Li, X., H.Y. Yu, and C.S. Quan. Tropical Journal of Pharmaceutical Research, 2011. 10(5): p. 577-584; ISI[000298174900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298216000012">Biological Properties of the Cotinus coggygria Methanol Extract.</a> Matic, S., S. Stanic, S. Solujic, T. Milosevic, and N. Niciforovic. Periodicum Biologorum, 2011. 113(1): p. 87-92; ISI[000298216000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298385000020">Evaluation of the Antifungal Activity of the Iranian Thyme Essential Oils on the Postharvest Pathogens of Strawberry Fruits.</a> Nabigol, A. and H. Morshedi. African Journal of Biotechnology, 2011. 10(48): p. 9864-9869; ISI[000298385000020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297458400007">Antimicrobial Potential of the Selected Plant Species against Some Infectious Microbes Used.</a> Naz, R., A. Bano, H. Yasmin, Samiullah, and U. Farooq. Journal of Medicinal Plants Research, 2011. 5(21): p. 5247-5253; ISI[000297458400007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297457400018">Antifungal Activity of the Essential Oil of Iranian Medicinal Plants.</a> Pirbalouti, A.G., B. Hamedi, R. Abdizadeh, and F. Malekpoor. Journal of Medicinal Plants Research, 2011. 5(20): p. 5089-5093; ISI[000297457400018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297398800028">Synthesis and in-Vitro Activity of Some New Class of Thiazolidinone and Their Arylidene Derivatives.</a> Seelam, N. and S.P. Shrivastava. Bulletin of the Korean Chemical Society, 2011. 32(11): p. 3996-4000; ISI[000297398800028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297457400031">Nutraceutical Potential and Bioassay of Apium graveolens l. Grown in Khyber Pakhtunkhwa-Pakistan.</a> Shad, A.A., H.U. Shah, J. Bakht, M.I. Choudhary, and J. Ullah. Journal of Medicinal Plants Research, 2011. 5(20): p. 5160-5166; ISI[000297457400031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298385000022">Chemical Composition, Antimicrobial Activity, Proximate Analysis and Mineral Content of the Seed of Detarium senegalense Jf Gmelin.</a> Sowemimo, A.A., C. Pendota, B. Okoh, T. Omotosho, N. Idika, A.A. Adekunle, and A.J. Afolayan. African Journal of Biotechnology, 2011. 10(48): p. 9875-9879; ISI[000298385000022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297562200005">Toxicity Effect of Cr (Vi) and Zn (Ii) on Growth of Filamentous Fungi Aspergillus niger Isolated from Industrial Effluent.</a> Vale, M.D., K.D. Abreu, S.T. Gouveia, R.C. Leitao, and S.T. Santaella. Engenharia Sanitaria E Ambiental, 2011. 16(3): p. 237-244; ISI[000297562200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297860100033">Synthesis and Inhibitory Activity of Thymidine Analogues Targeting Mycobacterium tuberculosis Thymidine monophosphate kinase.</a> Van Poecke, S., H. Munier-Lehmann, O. Helynck, M. Froeyen, and S. Van Calenbergh. Bioorganic &amp; Medicinal Chemistry, 2011. 19(24): p. 7603-7611; ISI[000297860100033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298461100006">Direct in Vivo Interaction of the Antibiotic Primycin with the Plasma Membrane of Candida albicans: An EPR Study.</a> Virag, E., J. Belagyi, Z. Gazdag, C. Vagvolgyi, and M. Pesti. Biochimica Et Biophysica Acta-Biomembranes, 2012. 1818(1): p. 42-48; ISI[000298461100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297960100005">Discovery of Highly Potent Antifungal Triazoles by Structure-based Lead Fusion.</a> Wang, W.Y., S.Z. Wang, G.Q. Dong, Y. Liu, Z.Z. Guo, Z.Y. Miao, J.Z. Yao, W.N. Zhang, and C.Q. Sheng. Medchemcomm, 2011. 2(11): p. 1066-1072; ISI[000297960100005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297389000027">Preparation, Characterization and Antimicrobial Activity of 6-Amino-6-deoxychitosan.</a> Yang, J.H., J. Cai, Y. Hu, D.L. Li, and Y.M. Du. Carbohydrate Polymers, 2012. 87(1): p. 202-209; ISI[000297389000027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298366600052">Bisbibenzyls, a New Type of Antifungal Agent, Inhibit Morphogenesis Switch and Biofilm Formation through Upregulation of DPP3 in Candida albicans.</a> Zhang, L., W.Q. Chang, B. Sun, M. Groh, A. Speicher, and H.X. Lou. Plos One, 2011. 6(12); ISI[000298366600052].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011912.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
