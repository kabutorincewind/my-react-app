

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-345.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vLlwk5i3yb7xJnpdhwKCSipyC5LcXJU4rQayABphchkulaUEjzCUsqJGde3OKv/MXDrXq5m/5wQv7FYX7OFdQcg7XW/C4+fj4c/VW3gk+NFLDZZPcJ49FPJ7CxY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F70EA65C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-345-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48674   OI-LS-345; PUBMED-OI-4/3/2006</p>

    <p class="memofmt1-2">          Rapid Microbiologic and Pharmacologic Evaluation of Experimental Compounds against Mycobacterium tuberculosis</p>

    <p>          Gruppo, V, Johnson, CM, Marietta, KS, Scherman, H, Zink, EE, Crick, DC, Adams, LB, Orme, IM, and Lenaerts, AJ</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1245-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569835&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569835&amp;dopt=abstract</a> </p><br />

    <p>2.     48675   OI-LS-345; PUBMED-OI-4/3/2006</p>

    <p class="memofmt1-2">          Activity and Mechanism of Action of N-Methanocarbathymidine against Herpesvirus and Orthopoxvirus Infections</p>

    <p>          Prichard, MN, Keith, KA, Quenelle, DC, and Kern, ER</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1336-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569849&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569849&amp;dopt=abstract</a> </p><br />

    <p>3.     48676   OI-LS-345; SCIFINDER-OI-3/27/2006</p>

    <p class="memofmt1-2">          Bioactive natural and semisynthetic latrunculins</p>

    <p>          El Sayed, Khalid A, Youssef, Diaa TA, and Marchetti, Dario</p>

    <p>          Journal of Natural Products <b>2006</b>.  69(2): 219-223</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     48677   OI-LS-345; SCIFINDER-OI-3/27/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro microbiological evaluation of imidazo(4,5-b)pyridinylethoxypiperidones</p>

    <p>          Aridoss, G, Balasubramanian, S, Parthiban, P, and Kabilan, S</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  41(2): 268-275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48678   OI-LS-345; PUBMED-OI-4/3/2006</p>

    <p class="memofmt1-2">          Protection against an aerogenic Mycobacterium tuberculosis infection in BCG-immunized and DNA-vaccinated mice is associated with early type I cytokine responses</p>

    <p>          Goter-Robinson, C, Derrick, SC, Yang, AL, Jeon, BY, and Morris, SL</p>

    <p>          Vaccine <b>2006</b>.  24(17): 3522-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16519971&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16519971&amp;dopt=abstract</a> </p><br />

    <p>6.     48679   OI-LS-345; SCIFINDER-OI-3/27/2006</p>

    <p class="memofmt1-2">          Efficient synthesis and anti-protozoal evaluation of 2,2&#39;-bichalcophene analogues of furamidine</p>

    <p>          Ismail, Mohamed A, Paliakov, Ekaterina M, Boykin, David W, Stephens, Chad E, Werbovetz, Karl A, and Brun, Reto</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-360</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     48680   OI-LS-345; SCIFINDER-OI-3/27/2006</p>

    <p class="memofmt1-2">          Patent developments in antimycobacterial small-molecule therapeutics</p>

    <p>          van Daele, Ineke and van Calenbergh, Serge</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2005</b>.  15(2): 131-140</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     48681   OI-LS-345; SCIFINDER-OI-3/27/2006</p>

    <p class="memofmt1-2">          Effects of chitin, chitosan, and oligochitosan on the antimicrobial activity of clarithromycin in combination with rifampicin against Mycobacterium avium complex within mouse peritoneal macrophages</p>

    <p>          Sato, Katsumasa, Sano, Chiaki, Shimizu, Toshiaki, and Tomioka, Haruaki</p>

    <p>          Nippon Kagaku Ryoho Gakkai Zasshi <b>2006</b>.  54(1): 39-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48682   OI-LS-345; WOS-OI-3/26/2006</p>

    <p class="memofmt1-2">          Sterilising action of pyrazinamide in models of dormant and rifampicin-tolerant Mycobacterium tuberculosis</p>

    <p>          Hu, Y, Coates, AR, and Mitchison, DA</p>

    <p>          INTERNATIONAL JOURNAL OF TUBERCULOSIS AND LUNG DISEASE <b>2006</b>.  10(3): 317-322, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235941600016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235941600016</a> </p><br />

    <p>10.   48683   OI-LS-345; WOS-OI-3/26/2006</p>

    <p class="memofmt1-2">          HEP DART 2005 - Frontiers in drug development for viral hepatitis</p>

    <p>          Feitelson, MA</p>

    <p>          IDRUGS <b>2006</b>.  9(3): 165-167, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236005600005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236005600005</a> </p><br />

    <p>11.   48684   OI-LS-345; WOS-OI-3/26/2006</p>

    <p class="memofmt1-2">          Composition and antituberculosis activity of the volatile oil of Heliotropium indicum Linn. growing in Phitsanulok, Thailand</p>

    <p>          Machan, T, Korth, J, Liawruangrath, B, Liawruangrath, S, and Pyne, SG</p>

    <p>          FLAVOUR AND FRAGRANCE JOURNAL <b>2006</b>.  21(2): 265-267, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235913100020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235913100020</a> </p><br />

    <p>12.   48685   OI-LS-345; WOS-OI-3/26/2006</p>

    <p class="memofmt1-2">          Synthesis and biological activity of flavane derivatives</p>

    <p>          He, L, Kong, EJ, Liu, YM, An, Y, Zhang, WS, Shi, DH, and Tan, RX</p>

    <p>          CHINESE JOURNAL OF CHEMISTRY <b>2006</b>.  24(3): 401-408, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235990900022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235990900022</a> </p><br />

    <p>13.   48686   OI-LS-345; WOS-OI-3/26/2006</p>

    <p class="memofmt1-2">          Targeted approaches in natural product lead discovery</p>

    <p>          Potterat, O</p>

    <p>          CHIMIA <b>2006</b>.  60 (1-2): 19-22, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235891500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235891500004</a> </p><br />
    <br clear="all">

    <p>14.   48687   OI-LS-345; WOS-OI-3/26/2006</p>

    <p class="memofmt1-2">          Larvicidal, antimycobacterial and antifungal compounds from the bark of the Peruvian plant Swartzia polyphylla DC</p>

    <p>          Rojas, R, Bustamante, B, Ventosilla, P, Fernadez, I, Caviedes, L, Gilman, RH, Lock, O, and Hammond, GB</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2006</b>.  54(2): 278-279, 2</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235852700031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235852700031</a> </p><br />

    <p>15.   48688   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          NIAID researchers make key discovery in development of TB drug candidate</p>

    <p>          Anon</p>

    <p>          JOURNAL OF INVESTIGATIVE MEDICINE <b>2006</b>.  54(2): 53-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235999400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235999400006</a> </p><br />

    <p>16.   48689   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          Oxovanadium(IV) complexes of hydrazides: Potential antifungal agents</p>

    <p>          Maqsood, ZT, Khan, KM, Ashiq, U, Jamal, RA, Chohan, ZH, Mahroof-Tahir, M, and Supuran, CT</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2006</b>.  21(1): 37-42, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236146500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236146500005</a> </p><br />

    <p>17.   48690   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of benzothiazole derivatives of pyrimidines, acrylonitriles, and coumarins</p>

    <p>          Youssef, AM, Mohamed, HM, Czezowski, C, Ata, A, and Abd-El-Aziz, AS</p>

    <p>          HETEROCYCLES <b>2006</b>.  68(2): 347-355, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236020600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236020600012</a> </p><br />

    <p>18.   48691   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          Synthesis, antibacterial and antifungal activity of some new pyridazinone metal complexes</p>

    <p>          Sonmez, M, Berber, I, and Akbas, E</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(1): 101-105, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236088800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236088800011</a> </p><br />

    <p>19.   48692   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          Potential applications of siRNA in hepatitis C virus therapy</p>

    <p>          Smolic, R, Volarevic, M, Wu, CH, and Wu, GY</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS <b>2006</b>.  7(2): 142-146, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800006</a> </p><br />

    <p>20.   48693   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          SCV-07 - SciClone pharmaceuticals/verta</p>

    <p>          Aspinall, RJ and Pockros, PJ</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS <b>2006</b>.  7(2): 180-185, 6</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800011</a> </p><br />
    <br clear="all">

    <p>21.   48694   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          Micafungin: A new echinocandin</p>

    <p>          Chandrasekar, PH and Sobel, JD</p>

    <p>          CLINICAL INFECTIOUS DISEASES <b>2006</b>.  42(8): 1171-1178, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236101800019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236101800019</a> </p><br />

    <p>22.   48695   OI-LS-345; WOS-OI-4/3/2006</p>

    <p class="memofmt1-2">          A target on the move: Innate and adaptive immune escape strategies of hepatitis C virus</p>

    <p>          Thimme, R, Lohmann, V, and Weber, F</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  69(3): 129-141, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236084800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236084800001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
