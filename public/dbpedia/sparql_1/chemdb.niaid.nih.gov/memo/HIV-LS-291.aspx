

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-291.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8jvQl9tSD8cxSmZCwET172exFpqLosc6SzL8CVlhVFGah47v1o+rxSheU37aL0THsFAdKsP0TPSisqnAK2gnANhcU5TXw9nkjsdqdgrHHq3xpgHAZThzYeQ0n/A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="885E3B3A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-291-MEMO</b> </p>

<p>    1.    42958   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           HIV fitness and resistance as covariates associated with the appearance of mutations under antiretroviral treatment</p>

<p>           Perno, CF, Cenci, A, Piro, C, D&#39;Arrigo, R, Marcon, L, Ceccherini-Silberstein, F, Ascoli, Marchetti F, Calio, R, and Antinori, A</p>

<p>           Scand J Infect Dis Suppl 2003. 35 Suppl 106: 37-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000581&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000581&amp;dopt=abstract</a> </p><br />

<p>    2.    42959   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Detection of Drug-Resistant Minority Variants of HIV-1 During Virologic Failure of Indinavir, Lamivudine, and Zidovudine</p>

<p>           Dykes, C, Najjar, J, Bosch, RJ, Wantman, M, Furtado, M, Hart, S, Hammer, SM, and Demeter, LM</p>

<p>           J Infect Dis 2004. 189(6): 1091-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14999613&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14999613&amp;dopt=abstract</a> </p><br />

<p>    3.    42960   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Enfuvirtide, a new fusion inhibitor for therapy of human immunodeficiency virus infection</p>

<p>           Hardy, H and Skolnik, PR</p>

<p>           Pharmacotherapy 2004. 24(2): 198-211</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998221&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998221&amp;dopt=abstract</a> </p><br />

<p>    4.    42961   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Crystal Structures of a Multidrug-Resistant Human Immunodeficiency Virus Type 1 Protease Reveal an Expanded Active-Site Cavity</p>

<p>           Logsdon, BC, Vickrey, JF, Martin, P, Proteasa, G, Koepke, JI, Terlecky, SR, Wawrzak, Z, Winters, MA, Merigan, TC, and Kovari, LC</p>

<p>           J Virol 2004. 78(6): 3123-3132</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14990731&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14990731&amp;dopt=abstract</a> </p><br />

<p>    5.    42962   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p class="memofmt1-2">           Rational Design of Inhibitors of HIV-1 TAR RNA through the Stabilisation of Electrostatic \&quot;Hot Spots\&quot;</p>

<p>           Davis, Ben, Afshar, Mohammad, Varani, Gabriele, Murchie, Alastair IH, Karn, Jonathan, Lentzen, Georg, Drysdale, Martin, Bower, Justin, Potter, Andrew J, Starkey, Ian D, Swarbrick, Terry, and Aboul-ela, Fareed</p>

<p>           Journal of Molecular Biology 2004.  336(2): 343-356</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    6.    42963   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Anti-HIV-1 activity of pyrryl aryl sulfone (PAS) derivatives: synthesis and SAR studies of novel esters and amides at the position 2 of the pyrrole nucleus</p>

<p>           Silvestri, R, Artico, M, La Regina, G, De Martino, G, La ColLa M, Loddo, R, and La ColLa P</p>

<p>           Farmaco 2004. 59(3): 201-10</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987983&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987983&amp;dopt=abstract</a> </p><br />

<p>    7.    42964   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Plant Substances as Anti-HIV Agents Selected According to Their Putative Mechanism of Action</p>

<p>           Cos, P, Maes, L, Vanden, Berghe D, Hermans, N, Pieters, L, and Vlietinck, A</p>

<p>           J Nat Prod 2004. 67(2): 284-93</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987070&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987070&amp;dopt=abstract</a> </p><br />

<p>    8.    42965   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Anti-AIDS Agents. Part 57; Actein, an anti-HIV principle from the rhizome of Cimicifuga racemosa (black cohosh), and the anti-HIV activity of related saponins</p>

<p>           Sakurai, N, Wu, JH, Sashida, Y, Mimaki, Y, Nikaido, T, Koike, K, Itokawa, H, and Lee, KH</p>

<p>           Bioorg Med Chem Lett 2004. 14(5): 1329-32</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980692&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980692&amp;dopt=abstract</a> </p><br />

<p>    9.    42966   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Synthesis of 5&#39;-C-methyl-1&#39;,3&#39;-dioxolan-4&#39;-yl nucleosides</p>

<p>           Du, J, Patterson, S, Shi, J, Chun, BK, Stuyver, LJ, and Watanabe, KA</p>

<p>           Bioorg Med Chem Lett 2004. 14(5): 1243-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980674&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980674&amp;dopt=abstract</a> </p><br />

<p>  10.    42967   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Design and synthesis of photoactivatable aryl diketo acid-containing HIV-1 integrase inhibitors as potential affinity probes</p>

<p>           Zhang, X, Marchand, C, Pommier, Y, and Burke, TR Jr</p>

<p>           Bioorg Med Chem Lett 2004. 14(5): 1205-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980666&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980666&amp;dopt=abstract</a> </p><br />

<p>  11.    42968   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p><b>           Anti-HIV agent</b> ((Fuso Pharmaceutical Industries, Ltd. Japan)</p>

<p>           Wakamiya, Nobutaka, Ohtani, Katsuki, Sakamoto, Takashi, Keshi, Hiroyuki, and Kishi, Yuichiro</p>

<p>           PATENT: WO 2004002511 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 44 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    42969   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p><b>           Antiviral regimens with once daily oral zidovudine for HIV infections</b>( (Glaxo Group Limited, UK)</p>

<p>           Keller, Amy Lee and Paes, Dominic Joseph Vincent</p>

<p>           PATENT: WO 2004002498 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 20 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  13.    42970   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p class="memofmt1-2">           Inhibition of HIV-1 Replication by Cell-penetrating Peptides Binding Rev</p>

<p>           Roisin, Armelle, Robin, Jean-Philippe, Dereuddre-Bosquet, Nathalie, Vitte, Anne-Laure, Dormont, Dominique, Clayette, Pascal, and Jalinot, Pierre</p>

<p>           Journal of Biological Chemistry 2004. 279(10): 9208-9214</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  14.    42971   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Enhanced gene silencing of HIV-1 specific siRNA using microRNA designed hairpins</p>

<p>           Boden, D, Pusch, O, Silbermann, R, Lee, F, Tucker, L, and Ramratnam, B</p>

<p>           Nucleic Acids Res 2004. 32(3): 1154-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14966264&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14966264&amp;dopt=abstract</a> </p><br />

<p>  15.    42972   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           A single amino acid determinant governs the species-specific sensitivity of APOBEC3G to Vif action</p>

<p>           Mangeat, B, Turelli, P, Liao, S, and Trono, D</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14966139&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14966139&amp;dopt=abstract</a> </p><br />

<p>  16.    42973   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Antiviral sulfonamide derivatives</p>

<p>           Supuran, CT, Innocenti, A, Mastrolorenzo, A, and Scozzafava, A</p>

<p>           Mini Rev Med Chem 2004. 4(2): 189-200</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14965291&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14965291&amp;dopt=abstract</a> </p><br />

<p>  17.    42974   HIV-LS-291; PUBMED-HIV-3/9/2004</p>

<p class="memofmt1-2">           Artificial neural networks: non-linear QSAR studies of HEPT derivatives as HIV-1 reverse transcriptase inhibitors</p>

<p>           Douali, L, Villemin, D, Zyad, A, and Cherqaoui, D</p>

<p>           Mol Divers 2004. 8(1): 1-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14964783&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14964783&amp;dopt=abstract</a> </p><br />

<p>  18.    42975   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p><b>           Prepn of 5&#39;-choline phosphate 3&#39;-azido 3&#39;-deoxythymidine as anti-AIDS agents</b>((Zakrytoe Aktsionernoe Obshcestvo &#39;Proizvodstvenno -Kommercheskaya Assotsiatsiya Azt&#39;, Russia)</p>

<p>           Shipitsyn, Alexander Valer&#39;evich, Yasko, Maxim Vladimirovich, Shirokova, Elena Anatol&#39;evna, Koukhanova, Marina Konstantinovna, Pronayeva, Tatiana Rudolfovna, Feduk, Nina Vladimirovna, Pokrovsky, Andrey Georgievich, Gosselin, Gilles, and Perigaud, Christian</p>

<p>           PATENT: WO 2004011480 A1;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2003; PP: 13 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  19.    42976   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p><b>           Preparation of indolyl-, azaindolyl-, and related heterocyclic ureido and thioureido piperazines for treatment of HIV and AIDS</b>((Bristol-Myers Squibb Company, USA)</p>

<p>           Regueiro-Ren, Alicia, Xue, Qiufen May, Kadow, John F, and Taylor, Malcolm</p>

<p>           PATENT: WO 2004011425 A2;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2003; PP: 107 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  20.    42977   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p><b>           Substituted heterocyclic compounds as modulators of the CCR5 receptor, and therapeutic use</b>((Smithkline Beecham Corporation, USA)</p>

<p>           Bondinell, William E and Neeb, Michael J</p>

<p>           PATENT: WO 2004010942 A2;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2003; PP: 50 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    42978   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           The first generation of candidate HIV-1 vaccines can induce antibodies able to neutralize primary isolates in assays with extended incubation phases</p>

<p>           Donners, H, Vermoesen, T, Willems, B, Davis, D, and Van der, Groen G</p>

<p>           VACCINE 2003. 22(1): 104-111</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188791100015">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188791100015</a> </p><br />

<p>  22.    42979   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           Cytotoxic and anti-HIV-1 constituents from leaves and twigs of Gardenia tubifera</p>

<p>           Reutrakul, V, Krachangchaeng, C, Tuchinda, P, Pohmakotr, M, Jaipetch, T, Yoosook, C, Kasisit, J, Sophasan, S, Sujarit, K, and Santisuk, T</p>

<p>           TETRAHEDRON 2004. 60(7): 1517-1523</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188785600009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188785600009</a> </p><br />

<p>  23.    42980   HIV-LS-291; SCIFINDER-HIV-3/2/2004</p>

<p><b>           Preparation of aromatic amines bearing tetraazacyclotetradecanyl group as nontoxic CCR5 antagonists and HIV antiviral agents</b>((Kureha Chemical Industry Co., Ltd. Japan)</p>

<p>           Suzuki, Shigeru, Takemura, Yoshiyuki, Sakauchi, Kenji, Hirose, Kunitaka, and Taninaka, Mikio</p>

<p>           PATENT: JP 2004043324 A2;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2002-3331; PP: 12 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    42981   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           The effect of human immunodeficiency virus-1 protease inhibitors on the toxicity of a variety of cells</p>

<p>           Germinario, RJ and Colby-Germinario, SP</p>

<p>           IN VITRO CELL DEV-AN 2003. 39(7): 275-279</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188859600005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188859600005</a> </p><br />

<p>  25.    42982   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           Capravirine - Anti-HIV agent - Reverse transcriptase inhibitor</p>

<p>           Sorbera, LA, Castaner, J, and Bayes, M</p>

<p>           DRUG FUTURE 2003. 28(12): 1149-1158</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188806500001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188806500001</a> </p><br />

<p>  26.    42983   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           Specific inhibition of HIV-1 coreceptor activity by synthetic peptides corresponding to the predicted extracellular loops of CCR5</p>

<p>           Agrawal, L, VanHorn-Ali, Z, Berger, EA, and Alkhatib, G</p>

<p>           BLOOD 2004. 103(4): 1211-1217</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188828200014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188828200014</a> </p><br />

<p>  27.    42984   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           CXCR4 engagement is required for HIV-1-induced L-selectin shedding</p>

<p>           Wang, JF, Marschner, S, and Finkel, TH</p>

<p>           BLOOD 2004. 103(4): 1218-1221</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188828200015">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188828200015</a> </p><br />

<p>  28.    42985   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           In vitro activity of stampidine against primary clinical human immunodeficiency virus isolates</p>

<p>           Uckun, FM, Pendergrass, S, Qazi, S, and Venkatachalam, TK</p>

<p>           ARZNEIMITTEL-FORSCH 2004. 54(1): 69-77</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188832700011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188832700011</a> </p><br />

<p>  29.    42986   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           Protease mutations in HIV-1 non-B strains infecting drug-naive villagers of Cameroon</p>

<p>           Konings, FAJ, Zhong, P, Agwara, M, Agyingi, L, Zekeng, L, Achkar, JM, Ewane, L, Saa, D, Ze, EA, Kinge, T, and Nyambi, PN</p>

<p>           AIDS RES HUM RETROV 2004. 20(1): 105-109</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188861700012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188861700012</a> </p><br />

<p>  30.    42987   HIV-LS-291; WOS-HIV-2/29/2004</p>

<p class="memofmt1-2">           The CCR5 and CXCR4 coreceptors - Central to understanding the transmission and pathogenesis of human immunodeficiency virus type 1 infection</p>

<p>           Moore, JP, Kitchen, SG, Pugach, P, and Zack, JA</p>

<p>           AIDS RES HUM RETROV 2004. 20(1): 111-126</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188861700013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188861700013</a> </p><br />

<p>  31.    42988   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Role for CCR5 Delta 32 protein in resistance to R5, R5X4, and X4 human immunodeficiency virus type 1 in primary CD4(+) cells</p>

<p>           Agrawal, L, Lu, XH, Qingwen, J, VanHorn-Ali, Z, Nicolescu, IV, McDermott, DH, Murphy, PM, and Alkhatib, G</p>

<p>           J VIROL 2004. 78(5): 2277-2287</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189019300012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189019300012</a> </p><br />

<p>  32.    42989   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Structure-based drug design targeting an inactive RNA conformation: Exploiting the flexibility of HIV-1 TAR RNA</p>

<p>           Murchie, AIH, Davis, B, Isel, C, Afshar, M, Drysdale, MJ, Bower, J, Potter, AJ, Starkey, ID, Swarbrick, TM, Mirza, S, Prescott, CD, Vaglio, P, Aboul-eLa F, and Karn, J</p>

<p>           J MOL BIOL 2004. 336(3): 625-638</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188948500006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188948500006</a> </p><br />

<p>  33.    42990   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Vif overcomes the innate antiviral activity of APOBEC3G by promoting its degradation in the ubiquitin-proteasome pathway</p>

<p>           Mehle, A, Strack, B, Ancuta, P, Zhang, CS, McPike, M, and Gabuzda, D</p>

<p>           J BIOL CHEM 2004. 279(9): 7792-7798</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189103300053">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189103300053</a> </p><br />

<p>  34.    42991   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Inhibition of intra- and extra-cellular Tat function and HIV expression by pertussis toxin B-oligomer</p>

<p>           Rizzi, C, Alfano, M, Bugatti, A, Camozzi, M, Poli, G, and Rusnati, M</p>

<p>           EUR J IMMUNOL  2004. 34(2): 530-536</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188980400026">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188980400026</a> </p><br />

<p>  35.    42992   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Synthesis of novel 5 &#39;-hydrogenphosphonothioate derivatives of AZT, d4T and ddI</p>

<p>           Jin, Y, Sun, M, Fu, H, and Zhao, YF</p>

<p>           CHEM LETT 2004. 33(2): 116-117</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189038400017">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189038400017</a> </p><br />

<p>  36.    42993   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Adverse effects of antiretroviral therapy for HIV infection</p>

<p>           Montessori, V, Press, N, Harris, M, Akagi, L, and Montaner, JSG</p>

<p>           CAN MED ASSOC J 2004. 170(2): 229-238</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189082300030">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189082300030</a> </p><br />

<p>  37.    42994   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Risk of failure in patients with 215 HIV-1 revertants starting their first thymidine analog-containing highly active antiretroviral therapy</p>

<p>           Violin, M, Cozzi-Lepri, A, Velleca, R, Vincenti, A, D&#39;Elia, S, Chiodo, F, Ghinelli, F, Bertoli, A, Monforte, AD, Perno, CF, Moroni, M, and Balotta, C</p>

<p>           AIDS 2004. 18(2): 227-235</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188968100012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188968100012</a> </p><br />

<p>  38.    42995   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Novel 5,10-dihydrobenzo[b][1,8]napthyridine N-oxides as non-nucleoside reverse transcriptase inhibitors of HIV-1 with high potency against clinically relevant mutants variants.</p>

<p>           Johnson, BL, Tarby, CM, Srivastava, A, Bakthavatchalam, R, Lin, QY, Cocuzza, AJ, Bilder, DM, Bacheler, LT, Diamond, S, Jeffrey, S, Klabe, RM, Cordova, BC, Garber, S, Logue, K, Erickson-Viitanen, SK, Trainor, GL, Anderson, PS, and Rodgers, JD</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U24</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500129">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500129</a> </p><br />

<p>  39.    42996   HIV-LS-291; WOS-HIV-3/7/2004</p>

<p class="memofmt1-2">           Design, antiviral and pharmacokinetic profiles of potent 1,3,4-trisubstituted pyrrolidine CCR5 receptor antagonists for the treatment of HIV-1 infection.</p>

<p>           Kim, D, Wang, LP, Hale, JJ, Lynch, C, Budhu, RJ, MacCoss, M, Mills, SG, Malkowitz, L, Gould, SL, DeMartino, JA, Springer, MS, Hazuda, D, Kessler, J, Danzeisen, R, CarelLa A, Holmes, K, Lineberger, J, Schleif, WA, and Emini, EA</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U30</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500162">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500162</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
