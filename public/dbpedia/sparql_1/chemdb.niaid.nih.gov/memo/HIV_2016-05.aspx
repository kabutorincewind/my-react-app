

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X3shAfA0ZBAmvyxlkdual+Sl3adpJxf0AJwhfVpHYa9si4cT3F0U8mWauHYCmm+x0f41hryVKmR3KObFXhISPDcIPsktBRp8i3sddMBRbFObIsD0RiN+P5q9CsY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8F7371A0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: May, 2016</h1>

    <h2>Literature Citations</h2>
    
    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27118832">Human DDX3 Protein Is a Valuable Target to Develop Broad Spectrum Antiviral Agents.</a> Brai, A., R. Fazi, C. Tintori, C. Zamperini, F. Bugli, M. Sanguinetti, E. Stigliano, J. Este, R. Badia, S. Franco, M.A. Martinez, J.P. Martinez, A. Meyerhans, F. Saladini, M. Zazzi, A. Garbelli, G. Maga, and M. Botta. Proceedings of the National Academy of Sciences of the United States of America, 2016. 113(19): p. 5388-5393. PMID[27118832]. PMCID[PMC4868442].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000373862600001">Pharmacology and Synthesis of Daurichromenic acid as a potent anti-HIV Agent.</a> Bukhari, S.M., I. Ali, A. Zaidi, N. Iqbal, T. Noor, R. Mehmood, M.S. Chishti, B. Niaz, U. Rashid, and M. Atif. Acta Poloniae Pharmaceutica, 2015. 72(6): p. 1059-1071. ISI[000373862600001].
    <br />
    <b>[WOS]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26864337">Anti-HIV-1 Integrase Compounds from Dioscorea bulbifera and Molecular Docking Study.</a> Chaniad, P., C. Wattanapiromsakul, S. Pianwanit, and S. Tewtrakul. Pharmaceutical Biology, 2016. 54(6): p. 1077-1085. PMID[26864337].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26919736">Development of Potent and Long-acting HIV-1 Fusion Inhibitors.</a> Chong, H., X. Wu, Y. Su, and Y. He. AIDS, 2016. 30(8): p. 1187-1196. PMID[26919736].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27094954">Design, Synthesis, and Biological Evaluations of Hydroxypyridonecarboxylic acids as Inhibitors of HIV Reverse Transcriptase Associated RNase H</a>. Kankanala, J., K.A. Kirby, F. Liu, L. Miller, E. Nagy, D.J. Wilson, M.A. Parniak, S.G. Sarafianos, and Z. Wang. Journal of Medicinal Chemistry, 2016. 59(10): p. 5051-5062. PMID[27094954]. PMCID[PMC4882222].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000374009600002">The First Synthesis and Anti-retroviral Activity of 5,5-Difluoro-3-hydroxy-apiosyl Nucleoside Cyclomonophosphonic acid Analogs.</a> Kim, S. and J.H. Hong. Bulletin of the Korean Chemical Society, 2016. 37(4): p. 425-433. ISI[000374009600002].
    <br />
    <b>[WOS]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26490422">Expedient Screening for HIV-1 Protease Inhibitors Using a Simplified Immunochromatographic Assay.</a> Kitidee, K., W. Khamaikawin, W. Thongkum, Y. Tawon, T.R. Cressey, R. Jevprasesphant, W. Kasinrerk, and C. Tayapiwatana. Journal of Chromatography. B, Analytical Technologies in the Biomedical and Life Sciences, 2016. 1021: p. 153-158. PMID[26490422].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000374900000008">Substituted Indoles as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors: A Patent Evaluation (WO2015044928).</a> Li, X., P. Gao, P. Zhan, and X.Y. Liu. Expert Opinion on Therapeutic Patents, 2016. 26(5): p. 629-635. PMID[26742549].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26996241">Synthesis and anti-HIV-1 Evaluation of Some Novel MC-1220 Analogs as Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Loksha, Y.M., E.B. Pedersen, R. Loddo, and P. La Colla. Archiv der Pharmazie, 2016. 349(5): p. 363-372. PMID[26996241].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000374813300009">HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors: Incorporation of Benzylphosphonate Moiety for Solubility Improvement.</a> Matyugina, E.S., V.T. Valuev-Elliston, A.O. Chizhov, S.N. Kochetkov, and A.L. Khandazhinskaya. Mendeleev Communications, 2016. 26(2): p. 114-116. ISI[000374813300009].
    <br />
    <b>[WOS]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26994843">Design, Synthesis and Evaluation of Novel HIV-1 NNRTIs with Dual Structural Conformations Targeting the Entrance Channel of the NNRTI Binding Pocket.</a> Meng, Q., X.W. Chen, D.W. Kang, B.S. Huang, W.X. Li, P. Zhan, D. Daelemans, E. De Clercq, C. Pannecouque, and X.Y. Liu. European Journal of Medicinal Chemistry, 2016. 115: p. 53-62. PMID[26994843].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26891461">Small-molecule CD4 Mimics Containing Mono-cyclohexyl Moieties as HIV Entry Inhibitors.</a> Ohashi, N., S. Harada, T. Mizuguchi, Y. Irahara, Y. Yamada, M. Kotani, W. Nomura, S. Matsushita, K. Yoshimura, and H. Tamamura. ChemMedChem, 2016. 11(8): p. 940-946. PMID[26891461].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26896646">Bioavailable Inhibitors of HIV-1 RNA Biogenesis Identified through a Rev-based Screen.</a> Prado, S., M. Beltran, M. Coiras, L.M. Bedoya, J. Alcami, and J. Gallego. Biochemical Pharmacology, 2016. 107: p. 14-28. PMID[26896646].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27117260">Synergistic Reduction of HIV-1 Infectivity by 5-Azacytidine and Inhibitors of Ribonucleotide Reductase.</a> Rawson, J.M.O., M.E. Roth, J.S. Xie, M.B. Daly, C.L. Clouser, S.R. Landman, C.S. Reilly, L. Bonnac, B. Kim, S.E. Patterson, and L.M. Mansky. Bioorganic &amp; Medicinal Chemistry, 2016. 24(11): p. 2410-2422. PMID[27117260].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26340566">Drug Interaction Profile of the HIV Integrase Inhibitor Cabotegravir: Assessment from in Vitro Studies and a Clinical Investigation with Midazolam.</a> Reese, M.J., G.D. Bowers, J.E. Humphreys, E.P. Gould, S.L. Ford, L.O. Webster, and J.W. Polli. Xenobiotica, 2016. 46(5): p. 445-456. PMID[26340566].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000375264200577">Dimerization Inhibition of HIV-1 RNA by 2&#39;-Deoxy-2&#39;-fluoro-beta-D-arabinose Nucleic acid (2&#39;-FANA) Modified Antisense Oligonucleotides Results in Potential Inhibition of Viral Expression.</a> Takahashi, M., H.T. Li, V. Aishwarya, M.J. Damha, and J.J. Rossi. Molecular Therapy, 2016. 24: p. S232-S233. ISI[000375264200577].
    <br />
    <b>[WOS]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26974376">Discovery of Non-peptide Small Molecular CXCR4 Antagonists as anti-HIV Agents: Recent Advances and Future Opportunities.</a> Zhang, H., D. Kang, B. Huang, N. Liu, F. Zhao, P. Zhan, and X. Liu. European Journal of Medicinal Chemistry, 2016. 114: p. 65-78. PMID[26974376].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27023513">Purification and Characterization of a White Laccase with Pronounced Dye Decolorizing Ability and HIV-1 Reverse Transcriptase Inhibitory Activity from Lepista nuda.</a> Zhu, M.J., G.Q. Zhang, L. Meng, H.X. Wang, K.X. Gao, and T. Ng. Molecules, 2016. 21(4):E415. PMID[27023513].
    <br />
    <b>[PubMed]</b>. HIV_05_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160428&amp;CC=WO&amp;NR=2016062854A1&amp;KC=A1">Conjugated Molecules Comprising a Peptide Derived from the CD4 Receptor Coupled to an Anionic Polypeptide for the Treatment of AIDS.</a> Baleux, F., H. Lortat-Jacob, D. Bonnaffe, and Y.-M. Coiec. Patent. 2016. 2015-EP74598 2016062854: 53pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160421&amp;CC=WO&amp;NR=2016059647A2&amp;KC=A2">Preparation of Triaminotriazine picolinonitrile Derivatives for Treatment of Hyperproliferative Disease and HIV-1 Infection.</a> Mane, D.V., S.N. Gavade, S.S. Kulkarni, and R.S. Paranjape. Patent. 2016. 2015-IN386 2016059647: 34pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160506&amp;CC=WO&amp;NR=2016067090A1&amp;KC=A1">Alkaloids from Sponge, Scaffolds for the Inhibition of Human Immunodeficiency Virus (HIV) Infection.</a> O&#39;Rourke, A., S. Kremb, and C.R. Voolstra. Patent. 2016. 2015-IB2126 2016067090: 50pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">22. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160428&amp;CC=WO&amp;NR=2016063251A1&amp;KC=A1">Histone Deacetylase Inhibitor Reactivation of Latent Human Immunodeficiency Virus.</a> Pantaleo, G. and M. Perreau. Patent. 2016. 2015-IB58168 2016063251: 40pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160505&amp;CC=US&amp;NR=2016122366A1&amp;KC=A1">Boronic acid Inhibitors of HIV Protease.</a> Raines, R.T., I. Windsor, M. Palte, and J. Lukesh. Patent. 2016. 2015-14927390 20160122366: 54pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160519&amp;CC=WO&amp;NR=2016077569A1&amp;KC=A1">Preparation of C17-Aryl Substituted Betulinic acid Analogs.</a> Sin, N., Y. Chen, S.-Y. Sit, J. Chen, A. Regueiro-Ren, and N.A. Meanwell. Patent. 2016. 2015-US60353 2016077569: 79pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_05_2016.</p><br />

    <p class="plaintext"> </p>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160519&amp;CC=WO&amp;NR=2016077572A1&amp;KC=A1">Preparation of Extended Betulinic acid Analogs as HIV Maturation Inhibitors.</a> Sit, S.-Y., J. Chen, Y. Chen, J. Swidorski, A. Regueiro-Ren, and N.A. Meanwell. Patent. 2016. 2015-US60360 2016077572: 109pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_05_2016.    </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
