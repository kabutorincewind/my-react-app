

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2015-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uNngIEQnZENZyUf+blPdLBN79loL5ifycnphMoz0yabDM5IcsMhoqvKn+JgXYL1Zw81TAQtkCsiJmemctVSmVpvV7rnu5HyJLSxPei4fctRzY4NeYrRg6l4dJ+0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E54CD91" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: November, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/6494223">Antitubercular and CNS Activities of Some 2-Aryl-3-[N-(2/3/4-benzimidazol-2-yl)phenyl]iminomethylenyl Indoles.</a> Agarwal, R., C. Agarwal, and P. Kumar. Pharmacological Research Communications, 1984. 16(8): p. 831-844. PMID[6494223].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26482569">Synthesis and Evaluation of C-5 Modified 2&#39;-Deoxyuridine Monophosphates as Inhibitors of M. tuberculosis Thymidylate Synthase.</a> Alexandrova, L.A., V.O. Chekhov, E.R. Shmalenyuk, S.N. Kochetkov, R.A. El-Asrar, and P. Herdewijn. Bioorganic &amp; Medicinal Chemistry, 2015. 23(22): p. 7131-7137. PMID[26482569].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26421992">Synthesis and Preliminary Biological Evaluation of a Small Library of Hybrid Compounds Based on Ugi Isocyanide Multicomponent Reactions with a Marine Natural Product Scaffold.</a> Aviles, E., J. Prudhomme, K.G. Le Roch, S.G. Franzblau, K. Chandrasena, A.M. Mayer, and A.D. Rodriguez. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(22): p. 5339-5343. PMID[26421992].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24266862">SAR Studies on Trisubstituted Benzimidazoles as Inhibitors of Mtb FtsZ for the Development of Novel Antitubercular Agents.</a> Awasthi, D., K. Kumar, S.E. Knudson, R.A. Slayden, and I. Ojima. Journal of Medicinal Chemistry, 2013. 56(23): p. 9756-9770. PMID[24266862]. PMCID[PMC3933301].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">5. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26530386">Preclinical Evaluations to Identify Optimal Linezolid Regimens for Tuberculosis Therapy.</a></span> Brown, A.N., G.L. Drusano, J.R. Adams, J.L. Rodriquez, K. Jambunathan, D.L. Baluya, D.L. Brown, A. Kwara, J.C. Mirsalis, R. Hafner, and A. Louie. MBio, 2015. 6(6). PMID[26530386]. PMCID[PMC4631805].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/404634">Derivatives of 4(7)-Methyl- and 5,6-Dimethyl-benzimidazolyl-2-mercaptoacetic acids.</a> Czarnocka-Janowicz, A., J. Sawlewicz, W. Gilewski, and M. Janowiec. Polish Journal of Pharmacology &amp; Pharmacy, 1977. 29(1): p. 69-73. PMID[404634].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">7. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/9241831">Synthesis and Antimycobacterial Activity of Some New 3-Heterocyclic Substituted Chromones.</a></span> Gasparova, R., M. Lacova, H.M. el-Shaaer, and Z. Odlerova. Il Farmaco, 1997. 52(4): p. 251-253. PMID[9241831].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2517303">In Vitro Tuberculostatic Activities of Some 2-Benzylbenzimidazole and 2-Phenoxymethylbenzimidazole Derivatives.</a> Gumus, F., T.G. Altuntas, N. Saygun, T. Ozden, and S. Ozden. Journal de Pharmacie de Belgique, 1989. 44(6): p. 398-3402. PMID[2517303].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">9. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/19150154">SAR Study of Clubbed [1,2,4]-Triazolyl with Fluorobenzimidazoles as Antimicrobial and Antituberculosis Agents.</a></span> Jadhav, G.R., M.U. Shaikh, R.P. Kale, M.R. Shiradkar, and C.H. Gill. European Journal of Medicinal Chemistry, 2009. 44(7): p. 2930-2935. PMID[19150154].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">10. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/24381946">Antituberculosis: Synthesis and Antimycobacterial Activity of Novel Benzimidazole Derivatives.</a></span> Keng Yoon, Y., M. Ashraf Ali, T.S. Choon, R. Ismail, A. Chee Wei, R. Suresh Kumar, H. Osman, and F. Beevi. BioMed Research International, 2013. 926309: 6pp. PMID[24381946]. PMCID[PMC3870127].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">11. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26245639">Cell Division Inhibitors with Efficacy Equivalent to Isoniazid in the Acute Murine Mycobacterium tuberculosis Infection Model.</a></span> Knudson, S.E., D. Awasthi, K. Kumar, A. Carreau, L. Goullieux, S. Lagrange, H. Vermet, I. Ojima, and R.A. Slayden. Journal of Antimicrobial Chemotherapy, 2015. 70(11): p. 3070-3073. PMID[26245639]. PMCID[PMC4613742].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24746463">In Vitro-in Vivo Activity Relationship of Substituted Benzimidazole Cell Division Inhibitors with Activity against Mycobacterium tuberculosis.</a> Knudson, S.E., K. Kumar, D. Awasthi, I. Ojima, and R.A. Slayden. Tuberculosis, 2014. 94(3): p. 271-276. PMID[24746463]. PMCID[PMC4068151].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">13. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26617968">Design and Synthesis of a Focused Library of Diamino Triazines as Potential DHFR Inhibitors.</a></span> Lele, A.C., A. Raju, M.P. Khambete, M.K. Ray, M.G. Rajan, M.A. Arkile, N.J. Jadhav, D. Sarkar, and M.S. Degani. ACS Medicinal Chemistry Letters, 2015. 6(11): p. 1140-1144. PMID[26617968]. PMCID[PMC4645240].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">14. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26473275">Dibenz[b,f]oxepin and Antimycobacterial Chalcone Constituents of Empetrum nigrum.</a></span> Li, H., S. Jean, D. Webster, G.A. Robichaud, L.A. Calhoun, J.A. Johnson, and C.A. Gray. Journal of Natural Products, 2015. 78(11): p. 2837-2840. PMID[26473275].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">15. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26421981">In Vitro Activity against Mycobacterium tuberculosis of Levofloxacin, Moxifloxacin and UB-8902 in Combination with Clofazimine and Pretomanid.</a></span> Lopez-Gavin, A., G. Tudo, A. Vergara, J.C. Hurtado, and J. Gonzalez-Martin. International Journal of Antimicrobial Agents, 2015. 46(5): p. 582-585. PMID[26421981].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">16. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26037320">Syntheses and Evaluation of Substituted Aromatic Hydroxamates and Hydroxamic acids That Target Mycobacterium tuberculosis.</a></span> Majewski, M.W., S. Cho, P.A. Miller, S.G. Franzblau, and M.J. Miller. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(21): p. 4933-4936. PMID[26037320]. PMCID[PMC4607592].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/827741">Reactions of Cyanomethylbenzimidazoles. Part III. Reaction of Cyanomethylbenzimidazoles with Isocyanates and Isothiocyanates.</a> Milczarska, B., J. Sawlewicz, and W. Manowska. Polish Journal of Pharmacology &amp; Pharmacy, 1976. 28(5): p. 521-528. PMID[827741].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/7312722">Reactions of Cyanomethylbenzimidazoles. Part IV. Derivatives of 2-[alpha-Cyano-beta-aryl-(dialkyl)-vinyl]-benzimidazole and N-Arylthioamides of Benzimidazolyl-2-alpha-cyanoacetic acid and Their Tuberculostatic Activity.</a> Milczarska, B., K. Wrzesniowska, and M. Janowiec. Polish Journal of Pharmacology &amp; Pharmacy, 1981. 33(2): p. 217-221. PMID[7312722].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">19. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26303795">Susceptibility of Mycobacterium abscessus to Antimycobacterial Drugs in Preclinical Models.</a></span> Obregon-Henao, A., K.A. Arnett, M. Henao-Tamayo, L. Massoudi, E. Creissen, K. Andries, A.J. Lenaerts, and D.J. Ordway. Antimicrobial Agents and Chemotherapy, 2015. 59(11): p. 6904-6912. PMID[26303795]. PMCID[PMC4604395].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">20. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26526729">Novel Derivatives of Nitro-substituted Salicylic acids: Synthesis, Antimicrobial Activity and Cytotoxicity.</a></span> Paraskevopoulos, G., M. Kratky, J. Mandikova, F. Trejtnar, J. Stolarikova, P. Pavek, G. Besra, and J. Vinsova. Bioorganic &amp; Medicinal Chemistry, 2015. 23(22): p. 7292-7301. PMID[26526729].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">21. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25944535">Activity of Lipophilic and Hydrophilic Drugs against Dormant and Replicating Mycobacterium tuberculosis.</a></span> Piccaro, G., G. Poce, M. Biava, F. Giannoni, and L. Fattorini. The Journal of Antibiotics, 2015. 68(11): p. 711-714. PMID[25944535].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">22. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/21297296">Effect of Substitution on the Antimycobacterial Activity of 2-(Substituted Benzyl)sulfanyl Benzimidazoles, Benzoxazoles, and Benzothiazoles--A Quantitative Structure-Activity Relationship Study.</a></span> Pytela, O. and V. Klimesova. Chemical and Pharmaceutical Bulletin, 2011. 59(2): p. 179-184. PMID[21297296].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">23. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26352292">Peptides from the Scorpion Vaejovis punctatus with Broad Antimicrobial Activity.</a></span> Ramirez-Carreto, S., J.M. Jimenez-Vargas, B. Rivas-Santiago, G. Corzo, L.D. Possani, B. Becerril, and E. Ortiz. Peptides, 2015. 73: p. 51-59. PMID[26352292].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">24. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/24625232">Synthesis and Biological Evaluation of Novel N&#39; (4-Aryloxybenzylidene)-1H-benzimidazole-2 carbohydrazide Derivatives as Anti-tubercular Agents.</a></span> Siddiki, A.A., V.K. Bairwa, and V.N. Telvekar. Combinatorial Chemistry &amp; High Throughput Screening, 2014. 17(7): p. 630-638. PMID[24625232].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">25. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/26522089">Aminopyrazolo[1,5-a]pyrimidines as Potential Inhibitors of Mycobacterium tuberculosis: Structure Activity Relationships and ADME Characterization.</a></span> Soares de Melo, C., T.S. Feng, R. van der Westhuyzen, R.K. Gessner, L.J. Street, G.L. Morgans, D.F. Warner, A. Moosa, K. Naran, N. Lawrence, H.I. Boshoff, C.E. Barry, 3rd, C.J. Harris, R. Gordon, and K. Chibale. Bioorganic &amp; Medicinal Chemistry, 2015. 23(22): p. 7240-7250. PMID[26522089].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25151735">Design and Synthesis of Novel Benzimidazole Derivatives as Anti-tuberculosis Agents.</a> Zhang, H.Y., B. Wang, L. Sheng, D. Li, D.F. Zhang, Z.Y. Lin, Y. Lu, Y. Li, and H.H. Huang. Yao Xue Xue Bao, 2014. 49(5): p. 644-651. PMID[25151735].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">27. <span class="memofmt2-1"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25971324">Evaluation of the Efficacy of Novel Oxazolidinone Analogues against Nontuberculous Mycobacteria in Vitro.</a></span> Zhao, W., Y. Jiang, P. Bao, Y. Li, L. Tang, Y. Zhou, and Y. Zhao. Japanese Journal of Infectious Diseases, 2015. 68(6): p. 520-522. PMID[25971324].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_11_2015.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26378507">Synthesis, Antibacterial and Antitubercular Activities of Some 5H-Thiazolo[3,2-a]pyrimidin-5-ones and Sulfonic acid Derivatives.</a> Cai, D., Z.H. Zhang, Y. Chen, X.J. Yan, L.J. Zou, Y.X. Wang, and X.Q. Liu. Molecules, 2015. 20(9): p. 16419-16434. PMID[26378507].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26471125">The Identification of Novel Mycobacterium tuberculosis DHFR Inhibitors and the Investigation of Their Binding Preferences by Using Molecular Modelling.</a> Hong, W., Y. Wang, Z. Chang, Y.H. Yang, J. Pu, T. Sun, S. Kaur, J.C. Sacchettini, H.M. Jung, W.L. Wong, L.F. Yap, Y.F. Ngeow, I.C. Paterson, and H. Wang. Scientific Reports, 2015. 5(15328): 4pp. PMID[26471125]. PMCID[PMC4607890].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26214585">Virtual Screening for UDP-galactopyranose Mutase Ligands Identifies a New Class of Antimycobacterial Agents.</a> Kincaid, V.A., N. London, K. Wangkanont, D.A. Wesener, S.A. Marcus, A. Heroux, L. Nedyalkova, A.M. Talaat, K.T. Forest, B.K. Shoichet, and L.L. Kiessling. ACS Chemical Biology, 2015. 10(10): p. 2209-2218. PMID[26214585].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2598762">Mode of Action of Clofazimine and Combination Therapy with Benzothiazinones against Mycobacterium tuberculosis.</a> Lechartier, B. and S.T. Cole. Antimicrobial Agents and Chemotherapy, 2015. 59(8): p. 4457-4463. PMID[2598762]. PMCID[PMC4505229].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000363713800003">Antimycobacterial Natural Products from Endophytes of the Medicinal Plant Aralia nadicaulis.</a> Li, H.X., B. Doucet, A.J. Flewelling, S. Jean, D. Webster, G.A. Robichaud, J.A. Johnson, and C.A. Gray. Natural Product Communications, 2015. 10(10): p. 1641-1642. ISI[000363713800003].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25987616">The 8-Pyrrole-benzothiazinones Are Noncovalent Inhibitors of DprE1 from Mycobacterium tuberculosis.</a> Makarov, V., J. Neres, R.C. Hartkoorn, O.B. Ryabova, E. Kazakova, M. Sarkan, S. Huszar, J. Piton, G.S. Kolly, A. Vocat, T.M. Conroy, K. Mikusova, and S.T. Cole. Antimicrobial Agents and Chemotherapy, 2015. 59(8): p. 4446-4452. PMID[25987616]. PMCID[PMC4505229].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26483967">5-Arylaminouracil Derivatives as Potential Dual-action Agents.</a> Matyugina, E.S., M.S. Novikov, D.A. Babkov, V.T. Valuev-Elliston, C. Vanpouille, S. Zicari, A. Corona, E. Tramontano, L.B. Margolis, A.L. Khandazhinskaya, and S.N. Kochetkov. Acta Naturae, 2015. 7(3): p. 113-115. PMID[26483967]. PMCID[PMC4610172].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25240097">Design, Synthesis and Antimycobacterial Activity of Various 3-(4-(Substitutedsulfonyl)piperazin-1-yl)benzo[d]isoxazole Derivatives.</a> Naidu, K.M., A. Suresh, J. Subbalakshmi, D. Sriram, P. Yogeeswari, P. Raghavaiah, and K. Sekhar. European Journal of Medicinal Chemistry, 2014. 87: p. 71-78. PMID[25240097].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000364158200021">Antimicrobial and Antimycobacterial Activities of Aliphatic Amines Derived from Vanillin.</a> Patterson, A.E., A.J. Flewelling, T.N. Clark, S.J. Geier, C.M. Vogels, J.D. Masuda, C.A. Gray, and S.A. Westcott. Canadian Journal of Chemistry, 2015. 93(11): p. 1305-1311. ISI[000364158200021].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26478333">Synthesis and Characterization of the Antitubercular Phenazine lapazine and Development of PLGA and PCL Nanoparticles for Its Entrapment.</a> Silveira, N., M.M. Longuinho, S.G. Leitao, R.S.F. Silva, M.C. Lourenco, P.E.A. Silva, M. Pinto, L.G. Abracado, and P.V. Finotelli. Materials Science &amp; Engineering C-Materials for Biological Applications, 2016. 58: p. 458-466. PMID[26478333].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000363706400003">5(4H)-Oxazolones as Novel Antitubercular Agents: Synthesis, Characterisation and Structure Activity Study.</a> Suhasini, K.P., P.K. Chintakindi, K. Chaguruswamy, and Y.L.N. Murthy. Journal of the Chinese Chemical Society, 2015. 62(10): p. 855-860. ISI[000363706400003].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26062396">Rational Drug Design, Synthesis and Biological Evaluation of Dihydrofolate Reductase Inhibitors as Antituberculosis Agents.</a> Tawari, N.R., S. Bag, A. Raju, A.C. Lele, R. Bairwa, M.K. Ray, M.G.R. Rajan, L.U. Nawale, D. Sarkar, and M.S. Degani. Future Medicinal Chemistry, 2015. 7(8): p. 979-988. PMID[26062396].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26227776">Radiolabelling and Positron Emission Tomography of PT70, a Time-dependent Inhibitor of InhA, the Mycobacterium tuberculosis Enoyl-ACP Reductase.</a> Wang, H., L. Liu, Y. Lu, P. Pan, J.M. Hooker, J.S. Fowler, and P.J. Tonge. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(21): p. 4782-4786. PMID[26227776]. PMCID[PMC4607644].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000363249100002">Pyrazolo[3,4-d]pyrimidines as the Inhibitors of Mycobacterial Beta-oxidation Trifunctional Enzyme.</a> Yadava, U., B.K. Shukla, and M. Roychoudhury. Medicinal Chemistry Research, 2015. 24(12): p. 4002-4015. ISI[000363249100002].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_11_2015.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151126&amp;CC=WO&amp;NR=2015179500A1&amp;KC=A1">Lipid-modulating Agents as Adjunctive Therapy for Tuberculosis.</a> Karakousis, P., C. Skerry, R. Pine, M.L. Gennaro, and N. Dutta. Patent. 2015. 2015-US31746 2015179500: 21pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_11_2015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
