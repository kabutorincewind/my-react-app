

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-01-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EPu6JwakAhSzVwZsxRPwNX3cx6h+tnHUtTbFTRbLUBVbvGubhetK5AFhuA+h1nQUQ07/ephbR5poXMqY2GUwzYCblfraarzk6aUM7IGcCn5j8iidIyH3J3Pwutk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5474A1BD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: January 4 - January 17, 2013</h1>

    <h2>HCMV</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23320521">Novel Antiviral Activity of L-Dideoxy Bicyclic Nucleoside Analogues Versus Vaccinia and Measles Viruses in Vitro.</a> McGuigan, C., K. Hinsinger, L. Farleigh, R.N. Pathirana, and J.J. Bugert. Journal of Medicinal Chemistry, 2013.  <b>[Epub ahead of print]</b>; PMID[23320521].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0104-011712.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23237841">2&#39;-Fluoro-6&#39;-methylene-carbocyclic adenosine phosphoramidate (FMCAP) Prodrug: In Vitro anti-HBV Activity against the Lamivudine-Entecavir Resistant Triple Mutant and Its Mechanism of Action.</a> Rawal, R.K., U.S. Singh, S.N. Chavre, J. Wang, M. Sugiyama, W. Hung, R. Govindarajan, B. Korba, Y. Tanaka, and C.K. Chu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(2): p. 503-506; PMID[23237841].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0104-011712.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23293300">In Vitro Assessment of Drug-drug Interaction Potential of Boceprevir Associated with Drug Metabolizing Enzymes and Transporters.</a> Chu, X., X. Cai, D. Cui, C. Tang, A. Ghosal, G.H. Chan, M.D. Green, Y. Kuo, Y. Liang, C.M. Maciolek, J. Palamanda, R. Evers, and T. Prueksaritanont. Drug Metabolism and Disposition: the Biological Fate of Chemicals, 2013.  <b>[Epub ahead of print]</b>; PMID[23293300].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23294107">Ligand Design, Synthesis and Biological anti-HCV Evaluations for Genotypes 1B and 4A of Certain 4-(3- &amp; 4-[3-(3,5-Dibromo-4-hydroxyphenyl)-propylamino]phenyl) butyric acids and 3-(3,5-Dibromo-4-hydroxyphenyl)-propylamino-acetamidobenzoic acid Esters.</a> Ismail, M.A., K.A. Abouzid, N.S. Mohamed, and E.M. Dokla. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013.  <b>[Epub ahead of print]</b>; PMID[23294107].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23140245">Direct-acting Antiviral Agents for Hepatitis C Virus Infection.</a> Kiser, J.J. and C. Flexner. Annual Review of Pharmacology and Toxicology, 2013. 53: p. 427-449; PMID[23140245].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23311976">Evaluation of Coumarin and Neoflavone Derivatives as HCV NS5B Polymerase Inhibitors.</a> Nichols, D.B., R.A. Leao, A. Basu, M. Chudayeu, F.d.M.P. de, T.T. Talele, P.R. Costa, and N. Kaushik-Basu. Chemical Biology &amp; Drug Design, 2013.  <b>[Epub ahead of print]</b>; PMID[23311976].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0104-011712.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955601213">Identification of a New Mechanism of Non-Nucleoside Inhibition of HCV RNA-dependent RNA Polymerase by the Flavonoid Quercetagetin.</a> Ahmed-Belkacem, A., R. Brillet, A. Florimond, N. Ahnou, C. Pallier, and J.M. Pawlotsky. Hepatology, 2012. 56: p. 298A-299A; ISI[000310955601213].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603470">TVB-2640, a Novel anti-HCV Agent, Safely Causes Sustained Host-target Inhibition in Vivo.</a> Evanchik, M., H.Y. Cai, Q.S. Feng, L. Hu, R. Johnson, G. Kemble, Y. Kosaka, J. Lai, J.D. Oslob, M. Sivaraja, S. Tep, H.B. Yan, C.A. Zaharia, and R. McDowell. Hepatology, 2012. 56: p. 1066A-1067A; ISI[000310955603470].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603487">HDAC Inhibitors Are Potent Suppressors of HCV-induced Hepatocarcinogenesis by Upregulating MICA, a GWAS-Identified HCC Susceptibility Gene.</a> Goto, K., R. Muroyama, N. Kowatari, R. Nakagawa, W.W. Li, and N. Kato. Hepatology, 2012. 56: p. 1075A-1075A; ISI[000310955603487].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312267700062">Enantioselective Synthesis of Derivatives and Structure-Activity Relationship Study in the Development of NA255 as a Novel Host-targeting anti-HCV Agent.</a> Kawasaki, K., M. Masubuchi, T. Hayase, S. Komiyama, F. Watanabe, H. Fukuda, T. Murata, Y. Matsubara, K. Koyama, H. Shindoh, H. Sakamoto, K. Okamato, A. Ohta, A. Katsume, M. Aoki, Y. Aoki, N. Shimma, M. Sudoh, and T. Tsukuda. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 336-339; ISI[000312267700062].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603475">Antiviral Activity and Resistance Profiles for ABT-267, a Novel HCV NS5A Inhibitor, in Vitro and During 3-Day Monotherapy in HCV Genotype-1 (GT1)-infected Treatment-naive Subjects.</a> Krishnan, P., J. Beyer, G. Koev, T. Reisch, R. Mondal, D. Liu, J. Pratt, D.A. DeGoey, R. Wagner, C. Maring, W. Kati, A. Molla, A.L. Campbell, B. Bernstein, L. Williams, C. Collins, and T. Pilot-Matias. Hepatology, 2012. 56: p. 1069A-1069A; ISI[000310955603475].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955602465">Antiviral Effetcts and Action Mechanisms of Novel N-(Morpholine-4-carbonyloxy) Amidine Compounds against Hepatitis C Virus.</a> Kusano-Kitazume, A., N. Sakamoto, Y. Okuno, K. Mori, M. Nakagawa, S. Kakinuma, S. Nitta, M. Murakawa, S. Azuma, Y. Nishimura-Sakurai, A. Matsumoto, M. Hagiwara, Y. Asahina, and M. Watanabe. Hepatology, 2012. 56: p. 716A-716A; ISI[000310955602465].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603468">PPI-668, a Potent New Pan-genotypic HCV NS5A Inhibitor: Phase 1 Efficacy and Safety.</a> Lalezari, J.P., G.C. Farrell, P.S. Shah, C. Schwab, D. Walsh, P. Vig, N.A. Brown, E. Ruby, S. Halfon, R. Colonno, L.P. Li, B. Johnston, W. Wargin, and E.J. Gane. Hepatology, 2012. 56: p. 1065A-1066A; ISI[000310955603468].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603492">Synthesis of Novel Amino-substituted N-Aryl benzamides as HA3G Stabilizer and Their Hepatitis C Virus Inhibitory Activities.</a> Li, Y.P., Z.G. Peng, L.H. Hao, Z.Y. Wu, J.D. Jiang, and Z.R. Li. Hepatology, 2012. 56: p. 1077A-1077A; ISI[000310955603492].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955601079">Sustained Virologic Response in Chronic HCV Genotype (GT) 1-Infected Null Responders with Combination of Daclatasvir (DCV; NS5A Inhibitor) and Asunaprevir (ASV; NS3 Inhibitor) with or without Peginterferon Alfa-2A/Ribavirin (PEG/RBV).</a> Lok, A.S., D.F. Gardiner, C. Hezode, E. Lawitz, M. Bourliere, G.T. Everson, P. Marcellin, M. Rodriguez-Torres, S. Pol, L. Serfaty, T. Eley, S.P. Huang, M. Wind-Rotolo, F. McPhee, D.M. Grasela, and C. Pasquinelli. Hepatology, 2012. 56: p. 230A-231A; ISI[000310955601079].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603477">Miravirsen, a Novel anti-HCV Therapeutic Targeting the Host Cell Factor miR-122, Demonstrates Broad Antiviral Activity against Drug-resistant HCV Variants.</a> Patick, A., K. Zeh, S. Ottosen, and M.R. Hodges. Hepatology, 2012. 56: p. 1070A-1070A; ISI[000310955603477].</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603493">Preclinical Evaluation of Asp5286, a Novel Cyclophilin Inhibitor with Potent anti-HCV Activity.</a> Sawada, M., E. Tsujii, Y. Morishita, M. Uchida, K. Katsumata, K. Maki, T. Makino, S. Yoshimura, T. Yamanaka, G. Lund, N.M. Kneteman, and D.J.L. Tyrrell. Hepatology, 2012. 56: p. 1078A-1078A; ISI[000310955603493].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000310955602460">Inhibition of Hepatitis C Virus Replication by Acyclic Retinoid.</a> Shimakami, T., M. Honda, T. Shirasaki, S. Lemon, and S. Kaneko. Hepatology, 2012. 56: p. 713A-714A; ISI[000310955602460].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312269500092">Cell Penetrable Humanized-VH/VHH That Inhibit RNA Dependent RNA Polymerase (NS5B) of HCV.</a> Thueng-in, K., J. Thanongsaksrikul, P. Srimanote, K. Bangphoomi, O. Poungpair, S. Maneewatch, K. Choowongkomon, and W. Chaicumpa. Plos One, 2012. 7(11): p. e49254; ISI[000312269500092].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955603497">Discovery of AT26893, a Novel Allosteric Inhibitor of the Regulation of HCV NS3/4A Protein Function.</a> Wilsher, N.E., M.G. Carr, J. Coyle, L. Fazal, B. Graham, C. Hamlett, S. Hiscock, H. Jhoti, A. Millemaggi, C. Murray, D. Norton, M. Reader, D. Rees, S.J. Rich, S. Saalau-Bethell, M. Sanders, N. Thompson, M. Vinkovic, H.M. Willems, and A. Woodhead. Hepatology, 2012. 56: p. 1079A-1080A; ISI[000310955603497].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310955601232">Interferon (IFN)-free Combination Treatment with the HCV NS3/4A Protease Inhibitor BI 201335 and the Non-nucleoside NS5B Inhibitor BI 207127 +/- Ribavirin (R): Final Results of Sound-C2 and Predictors of Response.</a> Zeuzem, S., V. Soriano, T. Asselah, J.P. Bronowicki, A.W. Lohse, B. Mullhaupt, M. Schuchmann, M. Bourliere, M. Buti, S.K. Roberts, E.J. Gane, J.O. Stern, R. Vinisko, I. Herichova, W.O. Boecher, and F.J. Mensa. Hepatology, 2012. 56: p. 308A-309A; ISI[000310955601232].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0104-011712.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
