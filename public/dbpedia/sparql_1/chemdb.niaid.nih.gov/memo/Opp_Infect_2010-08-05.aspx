

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-08-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+q+fp/WwNnEkcUkfhQYhCS5tDJI3bacL3ixNCJ5dlzz1FpXTFMVW/A1Bbg+hnNFHdQzV+FvPjOj8GnMqDqhjvW/t9XRQWByBbRVOqAChk6PGC0ZOTIpimheQKvU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7E768A6C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">July 23 - August 4, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20235747">Synthesis and antileishmanial and antimicrobial activities of some 2,3-disubstituted 3H-quinazolin-4-ones.</a> Arfan, M., R. Khan, M.A. Khan, S. Anjum, M.I. Choudhary, and M. Ahmad. J Enzyme Inhib Med Chem. 25(4): p. 451-8; PMID[20235747].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20677177">A comparative study of activity and apparent inhibition of fungal beta-glucosidases.</a> Bohlin, C., S.N. Olsen, M.D. Morant, S. Patkar, K. Borch, and P. Westh. Biotechnol Bioeng. <b>[Epub ahead of print]</b>; PMID[20677177].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20541177">Antimicrobial activity of n-6, n-7 and n-9 fatty acids and their esters for oral microorganisms.</a> Huang, C.B., B. George, and J.L. Ebersole. Arch Oral Biol. 55(8): p. 555-60; PMID[20541177].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20656059">Antimicrobial peptides with therapeutic potential from skin secretions of the Marsabit clawed frog Xenopus borealis (Pipidae).</a> Mechkarska, M., E. Ahmed, L. Coquet, J. Leprince, T. Jouenne, H. Vaudry, J.D. King, and J.M. Conlon. Comp Biochem Physiol C Toxicol Pharmacol. <b>[Epub ahead of print]</b>; PMID[20656059].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20667003">Antifungal activity of tri- and tetra-thioureido amino derivatives against different Candida species.</a> Oliveira, S.R., L.J. Nogueira, C.L. Donnici, T.F. Magalhaes, C.V. Martins, C.A. Montanari, and M.A. Resende. Mycoses. <b>[Epub ahead of print]</b>; PMID[20667003].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20235748">Synthesis of 1-(4-methoxybenzyl)-3-cyclopropyl-1H-pyrazol-5-amine derivatives as antimicrobial agents.</a> Raju, H., T.S. Nagamani, S. Chandrappa, H. Ananda, K. Vinaya, N.R. Thimmegowda, S.M. Byregowda, and K.S. Rangappa. J Enzyme Inhib Med Chem. 25(4): p. 537-43; PMID[20235748].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20335618">In vitro efficacy of antifungal treatment using riboflavin/UV-A (365 nm) combination and amphotericin B.</a> Sauer, A., V. Letscher-Bru, C. Speeg-Schatz, D. Touboul, J. Colin, E. Candolfi, and T. Bourcier. Invest Ophthalmol Vis Sci. 51(8): p. 3950-3; PMID[20335618].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20528949">Synergistic anticandidal activity of pure polyphenol curcumin I in combination with azoles and polyenes generates reactive oxygen species leading to apoptosis.</a> Sharma, M., R. Manoharlal, A.S. Negi, and R. Prasad. FEMS Yeast Res. 10(5): p. 570-8; PMID[20528949].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20235749">Template synthesis, spectroscopic, antibacterial, and antifungal studies of trivalent transition metal ion macrocyclic complexes.</a> Singh, D.P., K. Kumar, C. Sharma, and K.R. Aneja. J Enzyme Inhib Med Chem. 25(4): p. 544-50; PMID[20235749].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20362606">A defensin antimicrobial peptide from the venoms of Nasonia vitripennis.</a> Ye, J., H. Zhao, H. Wang, J. Bian, and R. Zheng. Toxicon. 56(1): p. 101-6; PMID[20362606].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20673174">Antimycobacterial activity of Indigofera suffruticosa with activation potential of the innate immune system.</a> de, A.C.C.B., M.B. Quilles, D.C. Maia, F.C. Lopes, R. Santos, F.R. Pavan, C.Q. Fujimura Leite, T.R. Calvo, W. Vilegas, and I.Z. Carlos. Pharm Biol. 48(8): p. 878-82; PMID[20673174].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20674375">Design, synthesis, and in vitro antiprotozoal, antimycobacterial activities of N-{2-[(7-chloroquinolin-4-yl)amino]ethyl}ureas.</a> Nava-Zuazo, C., S. Estrada-Soto, J. Guerrero-Alvarez, I. Leon-Rivera, G.M. Molina-Salinas, S. Said-Fernandez, M.J. Chan-Bacab, R. Cedillo-Rivera, R. Moo-Puc, G. Miron-Lopez, and G. Navarrete-Vazquez. Bioorg Med Chem. <b>[Epub ahead of print]</b>; PMID[20674375].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20615698">5-Nitro-2-furoic acid hydrazones: design, synthesis and in vitro antimycobacterial evaluation against log and starved phase cultures.</a> Sriram, D., P. Yogeeswari, D.R. Vyas, P. Senthilkumar, P. Bhat, and M. Srividya. Bioorg Med Chem Lett. 20(15): p. 4313-6; PMID[20615698].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0723-080410.</p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279505600001">Biocidal and catalytic efficiency of ruthenium(III) complexes with tridentate Schiff base ligands.</a> Arunachalam, S., N.P. Priya, K. Boopathi, C. Jayabalakrishnan, and V. Chinnusamy. Applied Organometallic Chemistry. 24(7): p. 491-498; ISI[000279505600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279739100007">Synthesis and biological evaluation of acyclic nucleotide analogues with a furo[2,3-d]pyrimidin-2(3H)-one base.</a> Janeba, Z., A. Holy, R. Pohl, R. Snoeck, G. Andrei, E. De Clercq, and J. Balzarini. Canadian Journal of Chemistry. 88(7): p. 628-638; ISI[000279739100007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279521800014">New quinoline derivatives: Synthesis and investigation of antibacterial and antituberculosis properties.</a> Eswaran, S., A.V. Adhikari, I.H. Chowdhury, N.K. Pal, and K.D. Thomas. European Journal of Medicinal Chemistry. 45(8): p. 3374-3383; ISI[000279521800014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279734100009">Antimicrobial peptide MUC7 12-mer activates the calcium/calcineurin pathway in Candida albicans.</a> Lis, M., T.T. Liu, K.S. Barker, P.D. Rogers, and L.A. Bobek. Fems Yeast Research. 10(5): p. 579-586; ISI[000279734100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279521800010">Discovery of new 1,3,5-triazine scaffolds with potent activity against Mycobacterium tuberculosis H37Rv.</a> Sunduru, N., L. Gupta, V. Chaturvedi, R. Dwivedi, S. Sinha, and P.M.S. Chauhan. European Journal of Medicinal Chemistry. 45(8): p. 3335-3345; ISI[000279521800010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279975500026">An Inorganic Complex that Inhibits Mycobacterium tuberculosis Enoyl Reductase as a Prototype of a New Class of Chemotherapeutic Agents to Treat Tuberculosis.</a> Basso, L.A., C.Z. Schneider, A. dos Santos, A.A. dos Santos, M.M. Campos, A.A. Souto, and D.S. Santos. Journal of the Brazilian Chemical Society. 21(7): p. 1384-1389; ISI[000279975500026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279939100012">Synthesis and Biological Evaluation of some new Imidazo[1,2-a]pyridines.</a> Cesur, Z., N. Cesur, S. Birteksoz, and G. Otuk. Acta Chimica Slovenica. 57(2): p. 355-362; ISI[000279939100012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279893600003">Synthesis and antimicrobial activities of 2-(5-mercapto)-1,3-oxadiazol-2-ylmethyl-1,2,4-triazol-3-one derivatives.</a> Demirbas, A., D. Sahin, N. Demirbas, S.A. Karaoglu, and H. Bektas. Turkish Journal of Chemistry. p. 347-358; ISI[000279893600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279958200075">Synthesis of Poly-N-(thiazol-2-yl)methacrylamide: Characterization, Complexation, and Bioactivity.</a> Elassar, A.Z.A., A.H. Al Sughayer, and F. Al Sagheer. Journal of Applied Polymer Science. 117(6): p. 3679-3686; ISI[000279958200075].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279698600010">Synthesis and antimicrobial activity of tetrazolo[1,5-a]quinoline-4-carbonitrile derivatives.</a> Kategaonkar, A.H., V.B. Labade, P.V. Shinde, B.B. Shingate, and M.S. Shingare. Monatshefte Fur Chemie. 141(7): p. 787-791; ISI[000279698600010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">24.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279866300074">Synthesis and evaluation of novel antifungal agents-quinoline and pyridine amide derivatives.</a> Nakamoto, K., I. Tsukada, K. Tanaka, M. Matsukura, T. Haneda, S. Inoue, N. Murai, S. Abe, N. Ueda, M. Miyazaki, N. Watanabe, M. Asada, K. Yoshimatsu, and K. Hata. Bioorganic &amp; Medicinal Chemistry Letters. 20(15): p. 4624-4626; ISI[000279866300074].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>

    <p> </p>

    <p class="plaintext">25.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279911700027">Copper(I) iodide complexes containing new aliphatic aminophosphine ligands and diimines-luminescent properties and antibacterial activity.</a> Starosta, R., M. Florek, J. Krol, M. Puchalska, and A. Kochel. New Journal of Chemistry. 34(7): p. 1441-1449; ISI[000279911700027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0723-080410.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
