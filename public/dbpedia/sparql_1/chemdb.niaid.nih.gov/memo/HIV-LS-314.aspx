

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-314.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Te/Bqr0prqA8/DY8xeJnWlB3cALcTN+3sBOUXMq/HIw3sV/s1e6NvyocPpxeySE7UaefFwf/TOQdPnfLoHm1zLwyo3BwO/5hyQz0jAxHIMJRcvMIZZvWKtbRtos=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9B747277" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-314-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44718   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          APOBEC3G cytidine deaminase inhibits retrotransposition of endogenous retroviruses</p>

    <p>          Esnault, C, Heidmann, O, Delebecque, F, Dewannieux, M, Ribet, D, Hance, AJ, Heidmann, T, and Schwartz, O</p>

    <p>          Nature <b>2005</b>.  433(7024): 430-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15674295&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15674295&amp;dopt=abstract</a> </p><br />

    <p>2.         44719   HIV-LS-314; EMBASE-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Mechanism of action and resistant profile of anti-HIV-1 coumarin derivatives</p>

    <p>          Huang, Li, Yuan, Xiong, Yu, Donglei, Lee, KH, and Ho Chen, Chin</p>

    <p>          Virology <b>2005</b>.  332(2): 623-628</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4F6CRCJ-1/2/2c015eb74777657331cb5e52d90ada5a">http://www.sciencedirect.com/science/article/B6WXR-4F6CRCJ-1/2/2c015eb74777657331cb5e52d90ada5a</a> </p><br />

    <p>3.         44720   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Studies of non-nucleoside HIV-1 reverse transcriptase inhibitors. Part 2: Synthesis and structure-activity relationships of 2-cyano and 2-hydroxy thiazolidenebenzenesulfonamide derivatives</p>

    <p>          Masuda, N, Yamamoto, O, Fujii, M, Ohgami, T, Fujiyasu, J, Kontani, T, Moritomo, A, Orita, M, Kurihara, H, Koga, H, Kageyama, S, Ohta, M, Inoue, H, Hatta, T, Shintani, M, Suzuki, H, Sudo, K, Shimizu, Y, Kodama, E, Matsuoka, M, Fujiwara, M, Yokota, T, Shigeta, S, and Baba, M</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(4): 949-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15670903&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15670903&amp;dopt=abstract</a> </p><br />

    <p>4.         44721   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Synthesis of 5-alkynylated d4T analogues as potential HIV-1 reverse transcriptase inhibitors</p>

    <p>          Ciurea, A, Fossey, C, Gavriliu, D, Delbederi, Z, Sugeac, E, Laduree, D, Schmidt, S, Laumond, G, and Aubertin, AM</p>

    <p>          J Enzyme Inhib Med Chem <b>2004</b>.  19(6): 511-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662955&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662955&amp;dopt=abstract</a> </p><br />

    <p>5.         44722   HIV-LS-314; EMBASE-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Lysine derivatives as potent HIV protease inhibitors. Discovery, synthesis and structure-activity relationship studies</p>

    <p>          Bouzide, Abderrahim, Sauve, Gilles, and Yelle, Jocelyn</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FB93SS-1/2/6c700e44d6a64f93cb4bf75d0d329402">http://www.sciencedirect.com/science/article/B6TF9-4FB93SS-1/2/6c700e44d6a64f93cb4bf75d0d329402</a> </p><br />
    <br clear="all">

    <p>6.         44723   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of some [Nucleoside Reverse Transcriptase Inhibitor]-C5&#39;-linker-[Integrase Inhibitor] heterodimers as inhibitors of HIV replication</p>

    <p>          Sugeac, E, Fossey, C, Laduree, D, Schmidt, S, Laumond, G, and Aubertin, AM</p>

    <p>          J Enzyme Inhib Med Chem <b>2004</b>.  19(6): 497-509</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662954&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662954&amp;dopt=abstract</a> </p><br />

    <p>7.         44724   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Role of Acetylases and Deacetylase Inhibitors in IRF-1-Mediated HIV-1 Long Terminal Repeat Transcription</p>

    <p>          Marsili, G, Remoli, AL, Sgarbanti, M, and Battistini, A</p>

    <p>          Ann N Y Acad Sci <b>2004</b>.  1030: 636-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15659847&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15659847&amp;dopt=abstract</a> </p><br />

    <p>8.         44725   HIV-LS-314; EMBASE-HIV-1/28/2005</p>

    <p class="memofmt1-2">          6-[1-(4-Fluorophenyl)methyl-1H-pyrrol-2-yl)]-2,4-dioxo-5-hexenoic acid ethyl ester a novel diketo acid derivative which selectively inhibits the HIV-1 viral replication in cell culture and the ribonuclease H activity in vitro</p>

    <p>          Tramontano, Enzo, Esposito, Francesca, Badas, Roberta, Di Santo, Roberto, Costi, Roberta, and La Colla, Paolo</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4F94G7X-1/2/a385fc8368c08705369b5491f149ce85">http://www.sciencedirect.com/science/article/B6T2H-4F94G7X-1/2/a385fc8368c08705369b5491f149ce85</a> </p><br />

    <p>9.         44726   HIV-LS-314; EMBASE-HIV-1/28/2005</p>

    <p class="memofmt1-2">          4&#39;C-Ethynyl-thymidine acts as a chain terminator during DNA-synthesis catalyzed by HIV-1 reverse transcriptase</p>

    <p>          Summerer, Daniel and Marx, Andreas</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4F92406-C/2/c086261d47d3984526b83dd839b67846">http://www.sciencedirect.com/science/article/B6TF9-4F92406-C/2/c086261d47d3984526b83dd839b67846</a> </p><br />

    <p>10.       44727   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p><b>          Cyclic sulfamide HIV-1 protease inhibitors, with sidechains spanning from P2/P2&#39; to P1/P1&#39;</b> </p>

    <p>          Ax, A, Schaal, W, Vrang, L, Samuelsson, B, Hallberg, A, and Karlen, A</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(3): 755-64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653343&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653343&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44728   HIV-LS-314; EMBASE-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Efavirenz enhances the proteolytic processing of an HIV-1 pol polyprotein precursor and reverse transcriptase homodimer formation</p>

    <p>          Tachedjian, Gilda, Moore, Katie L, Goff, Stephen P, and Sluis-Cremer, Nicolas</p>

    <p>          FEBS Letters <b>2005</b>.  579(2): 379-384</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4F19SSK-1/2/31eb0ef07b340c1e0b3771e0008e0851">http://www.sciencedirect.com/science/article/B6T36-4F19SSK-1/2/31eb0ef07b340c1e0b3771e0008e0851</a> </p><br />

    <p>12.       44729   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Combination of inhibitors of lymphocyte activation (hydroxyurea, trimidox, and didox) and reverse transcriptase (didanosine) suppresses development of murine retrovirus-induced lymphoproliferative disease</p>

    <p>          Mayhew, CN, Sumpter, R, Inayat, M, Cibull, M, Phillips, JD, Elford, HL, and Gallicchio, VS</p>

    <p>          Antiviral Res <b>2005</b>.  65(1): 13-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15652967&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15652967&amp;dopt=abstract</a> </p><br />

    <p>13.       44730   HIV-LS-314; EMBASE-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Anti-gene peptide nucleic acid targeted to proviral HIV-1 DNA inhibits in vitro HIV-1 replication</p>

    <p>          Pesce, Caterina D, Bolacchi, Francesca, Bongiovanni, Barbara, Cisotta, Federica, Capozzi, Marcella, Diviacco, Silvia, Quadrifoglio, Franco, Mango, Ruggiero, Novelli, Giuseppe, and Mossa, Giuseppe</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4F6K73J-1/2/77c5132a2b2915009b9012269c26e1bc">http://www.sciencedirect.com/science/article/B6T2H-4F6K73J-1/2/77c5132a2b2915009b9012269c26e1bc</a> </p><br />

    <p>14.       44731   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Pyridine N-oxide derivatives: unusual anti-HIV compounds with multiple mechanisms of antiviral action</p>

    <p>          Balzarini, J,  Stevens, M, De Clercq, E , Schols, D, and Pannecouque, C</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650002&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650002&amp;dopt=abstract</a> </p><br />

    <p>15.       44732   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          The pharmacokinetics of HIV protease inhibitor combinations</p>

    <p>          Boffito, M, Maitland, D, Samarasinghe, Y, and Pozniak, A</p>

    <p>          Curr Opin Infect Dis <b>2005</b>.  18(1): 1-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15647693&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15647693&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.       44733   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          P-glycoprotein expression affects the intracellular concentration and antiviral activity of the protease inhibitor saquinavir in a T cell line</p>

    <p>          Maffeo, A, Bellomi, F, Solimeo, I, Bambacioni, F, Scagnolari, C, De, Pisa F, Dupuis, ML, Cianfriglia, M, Antonelli, G, and Turriziani, O</p>

    <p>          New Microbiol <b>2004</b>.  27(2 Suppl 1): 119-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646074&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646074&amp;dopt=abstract</a> </p><br />

    <p>17.       44734   HIV-LS-314; EMBASE-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Betulinic acid derivatives as HIV-1 antivirals</p>

    <p>          Aiken, Christopher and Chen, Chin Ho</p>

    <p>          Trends in Molecular Medicine <b>2005</b>.  11(1): 31-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7J-4DVBDT6-1/2/c47175c0e86aae2544567da4e240b8d2">http://www.sciencedirect.com/science/article/B6W7J-4DVBDT6-1/2/c47175c0e86aae2544567da4e240b8d2</a> </p><br />

    <p>18.       44735   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Atazanavir: A novel once-daily protease inhibitor</p>

    <p>          Piliero, PJ</p>

    <p>          Drugs Today (Barc) <b>2004</b>.  40(11): 901-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15645003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15645003&amp;dopt=abstract</a> </p><br />

    <p>19.       44736   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Docking and 3-D QSAR studies on indolyl aryl sulfones. Binding mode exploration at the HIV-1 reverse transcriptase non-nucleoside binding site and design of highly active N-(2-hydroxyethyl)carboxamide and N-(2-hydroxyethyl)carbohydrazide derivatives</p>

    <p>          Ragno, R, Artico, M, De Martino, G, La Regina, G, Coluccia, A, Di Pasquali, A, and Silvestri, R</p>

    <p>          J Med Chem <b>2005</b>.  48(1): 213-223</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634015&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634015&amp;dopt=abstract</a> </p><br />

    <p>20.       44737   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Beta-diketo acid pharmacophore hypothesis. 1. Discovery of a novel class of HIV-1 integrase inhibitors</p>

    <p>          Dayam, R, Sanchez, T, Clement, O, Shoemaker, R, Sei, S, and Neamati, N</p>

    <p>          J Med Chem <b>2005</b>.  48(1): 111-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634005&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634005&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.       44738   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Identification of cellular deoxyhypusine synthase as a novel target for antiretroviral therapy</p>

    <p>          Hauber, I, Bevec, D, Heukeshoven, J, Kratzer, F, Horn, F, Choidas, A, Harrer, T, and Hauber, J </p>

    <p>          J Clin Invest <b>2005</b>.  115(1): 76-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15630446&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15630446&amp;dopt=abstract</a> </p><br />

    <p>22.       44739   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          A PNA-transportan conjugate targeted to the TAR region of the HIV-1 genome exhibits both antiviral and virucidal properties</p>

    <p>          Chaubey, B, Tripathi, S, Ganguly, S, Harris, D, Casale, RA, and Pandey, VN</p>

    <p>          Virology <b>2005</b>.  331(2): 418-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629784&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629784&amp;dopt=abstract</a> </p><br />

    <p>23.       44740   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Agrocybin, an antifungal peptide from the edible mushroom Agrocybe cylindracea</p>

    <p>          Ngai, PH, Zhao, Z, and Ng, TB</p>

    <p>          Peptides <b>2005</b>.  26(2): 291-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629530&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629530&amp;dopt=abstract</a> </p><br />

    <p>24.       44741   HIV-LS-314; PUBMED-HIV-1/28/2005</p>

    <p class="memofmt1-2">          Evaluation of the activity of HIV-1 integrase over-expressed in eukaryotic cells</p>

    <p>          Van Maele, B,  Van Eylen, L, Pluymers, W, and Debyser, Z</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  327(1): 261-267</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629457&amp;dopt=abstract</a> </p><br />

    <p>25.       44742   HIV-LS-314; WOS-HIV-1/16/2005</p>

    <p class="memofmt1-2">          Use and cost of antiretrovirals in France 1995-2000 - An analysis based on the medical dossier on human immunodeficiency (release 2) database</p>

    <p>          Flori, YA and  le Vaillant, M</p>

    <p>          PHARMACOECONOMICS <b>2004</b>.  22(16): 1061-1070, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225577300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225577300003</a> </p><br />

    <p>26.       44743   HIV-LS-314; WOS-HIV-1/16/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of novel 1 &#39;-branched and spironucleoside analogues</p>

    <p>          Chatgillaloglu, C, Ferreri, C, Gimisis, T, Roberti, M, Balzarini, J, and De, Clercq AE</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2004</b>.  23(10): 1565-1581, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225549600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225549600001</a> </p><br />
    <br clear="all">

    <p>27.       44744   HIV-LS-314; WOS-HIV-1/16/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of D4T analogues with a spacer arm between glucidic and base moieties</p>

    <p>          Roy, V, Zerrouki, R, Krausz, P, Schmidt, S, and Aubertin, AM</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2004</b>.  23(10): 1625-1637, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225549600005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225549600005</a> </p><br />

    <p>28.       44745   HIV-LS-314; WOS-HIV-1/16/2005</p>

    <p class="memofmt1-2">          Evaluation of the sensitivity and specificity of six HIV combined p24 antigen and antibody assays</p>

    <p>          Ly, TD, Laperche, S, Brennan, C, Vallari, A, Ebel, A, Hunt, J, Martin, L, Daghfal, D, Schochetman, G, and Devare, S</p>

    <p>          JOURNAL OF VIROLOGICAL METHODS <b>2004</b>.  122(2): 185-194, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225632900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225632900008</a> </p><br />

    <p>29.       44746   HIV-LS-314; WOS-HIV-1/16/2005</p>

    <p class="memofmt1-2">          A novel peptide that inhibits HIV-1 entry</p>

    <p>          Yu, Y, Huang, XX, Wang, Q, Yang, YL, Tian, P, and Zhang, WT</p>

    <p>          CHINESE SCIENCE BULLETIN <b>2004</b>.  49(19): 2110-2112, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225725000019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225725000019</a> </p><br />

    <p>30.       44747   HIV-LS-314; WOS-HIV-1/16/2005</p>

    <p class="memofmt1-2">          New CCR5 variants associated with reduced HIV coreceptor function in southeast Asia</p>

    <p>          Capoulade-Metay, C, Ma, LY, Truong, LX, Dudoit, Y, Versmisse, P, Nguyen, NV, Nguyen, M, Scott-Algara, D, Barre-Sinoussi, F, Debre, P, Bismuth, G, Pancino, G, and Theodorou, I </p>

    <p>          AIDS <b>2004</b>.  18 (17): 2243-2252, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225656900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225656900003</a> </p><br />

    <p>31.       44748   HIV-LS-314; WOS-HIV-1/23/2005</p>

    <p class="memofmt1-2">          The cytoplasmic tail slows the folding of human immunodeficiency virus type 1 Env from a late prebundle configuration into the six-helix bundle</p>

    <p>          Abrahamyan, LG, Mkrtchyan, SR, Binley, J, Lu, M, Melikyan, GB, and Cohen, FS</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(1): 106-115, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225904700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225904700010</a> </p><br />

    <p>32.       44749   HIV-LS-314; WOS-HIV-1/23/2005</p>

    <p class="memofmt1-2">          Cyclophilin interactions with incoming human immunodeficiency virus type 1 capsids with opposing effects on infectivity in human cells</p>

    <p>          Hatziioannou, T, Perez-Caballero, D, Cowan, S, and Bieniasz, PD</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(1): 176-183, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225904700017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225904700017</a> </p><br />

    <p>33.       44750   HIV-LS-314; WOS-HIV-1/23/2005</p>

    <p class="memofmt1-2">          Anti-HIV activities of organic and aqueous extracts of Sutherlandia frutescens and Lobostemon trigonus</p>

    <p>          Harnett, SM, Oosthuizen, V, and de Venter, MV</p>

    <p>          JOURNAL OF ETHNOPHARMACOLOGY <b>2005</b>.  96(1-2): 113-119, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225869600014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225869600014</a> </p><br />

    <p>34.       44751   HIV-LS-314; WOS-HIV-1/23/2005</p>

    <p class="memofmt1-2">          Synthesis of new potential HIV-1 integrase inhibitors</p>

    <p>          Ferro, S, Rao, A, Zappala, M, Chimirri, A, Barreca, ML, Witvrouw, M, Debyser, Z, and Monforte, P</p>

    <p>          HETEROCYCLES <b>2004</b>.  63(12): 2727-2734, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225941400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225941400004</a> </p><br />

    <p>35.       44752   HIV-LS-314; WOS-HIV-1/23/2005</p>

    <p class="memofmt1-2">          Locked nucleic acid containing antisense oligonucleotides enhance inhibition of HIV-1 genome dimerization and inhibit virus replication</p>

    <p>          Elmen, J, Zhang, HY, Zuber, B, Ljungberg, K, Wahren, B, Wahlestedt, C, and Liang, ZC</p>

    <p>          FEBS LETTERS <b>2004</b>.  578(3): 285-290, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225939900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225939900016</a> </p><br />

    <p>36.       44753   HIV-LS-314; WOS-HIV-1/23/2005</p>

    <p class="memofmt1-2">          Monoubiquitinated histone H1B is required for antiviral protection in CD4(+)T cells resistant to HIV-1</p>

    <p>          Lesner, A, Kartvelishvili, A, Lesniak, J, Nikolov, D, Kartvelishvili, M, Trillo-Pazos, G, Zablotna, E, and Simm, M</p>

    <p>          BIOCHEMISTRY <b>2004</b>.  43(51): 16203-16211, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225937700022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225937700022</a> </p><br />

    <p>37.       44754   HIV-LS-314; WOS-HIV-1/23/2005</p>

    <p class="memofmt1-2">          Understanding the integrase inhibitory activity of azido containing HIV-1 integrase inhibitors</p>

    <p>          Karki, RG and Nicklaus, MC</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  227: U7-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655700018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655700018</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
