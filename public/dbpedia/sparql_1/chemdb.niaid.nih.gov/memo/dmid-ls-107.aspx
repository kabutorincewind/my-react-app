

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-107.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8vPFqhirIwAAVQYx/vuVCgTCPwfCV/DdHOYqDQqFkmUqL/9y4JHZcRvDMa0RC24mnh2VRUiuIC7R0456Hu1cRSJN7B5Q/SAK0XSctSXz0N96Q7wL5USX9klMWFQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B1980173" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-107-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58030   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-viral pharmaceutical compositions</p>

    <p>          Chen, Andrew Xian</p>

    <p>          PATENT:  WO <b>2005074947</b>  ISSUE DATE:  20050818</p>

    <p>          APPLICATION: 2004  PP: 46 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     58031   DMID-LS-107; PUBMED-DMID-12/6/2005; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-hepatitis B virus activities of some ethyl 5-hydroxy-1H-indole-3-carboxylates</p>

    <p>          Zhao, C, Zhao, Y, Chai, H, and Gong, P</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16326106&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16326106&amp;dopt=abstract</a> </p><br />

    <p>3.     58032   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-influenza virus activity of peramivir in mice with single intramuscular injection</p>

    <p>          Bantia, S, Arnold, CS, Parker, CD, Upshaw, R, and Chand, P</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16325932&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16325932&amp;dopt=abstract</a> </p><br />

    <p>4.     58033   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Emtricitabine, a new antiretroviral agent with activity against HIV and hepatitis B virus</p>

    <p>          Saag, MS</p>

    <p>          Clin Infect Dis <b>2006</b>.  42(1): 126-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16323102&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16323102&amp;dopt=abstract</a> </p><br />

    <p>5.     58034   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Pharmacodynamic Evaluation of Antiviral Medicinal Plants Using a Vector-Based Assay Technique</p>

    <p>          Esimone, C. <i>et al.</i></p>

    <p>          Journal of Applied Microbiology <b>2005</b>.  99(6): 1346-1355</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233311600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233311600008</a> </p><br />
    <br clear="all">

    <p>6.     58035   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and anti-HSV-1 activity of quinolonic acyclovir analogues</p>

    <p>          Lucero, BD, Gomes, CR, Frugulhetti, IC, Faro, LV, Alvarenga, L, de, Souza MC, de, Souza TM, and Ferreira, VF</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16321530&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16321530&amp;dopt=abstract</a> </p><br />

    <p>7.     58036   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Apobec3b Is the Predominantly Expressed Apobec3 Editing Enzyme in Human Hepatocytes and a Strong Inhibitor of Hepatitis B Virus Replication</p>

    <p>          Bonvin, M., Achermann, F., and Greeve, J.</p>

    <p>          Hepatology <b>2005</b>.  42(4): 233A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300092">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300092</a> </p><br />

    <p>8.     58037   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-Viral Activity of Sch 503034, a Hcv Protease Inhibitor, Administered as Monotherapy in Hepatitis C Genotype-1 (Hcv-1) Patients Refractory to Pegylated Interferon (Peg-Ifn-Alpha)</p>

    <p>          Zeuzem, S. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 233A-234A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300093">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300093</a> </p><br />

    <p>9.     58038   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The novel nucleoside analog R1479, 4&#39;-azidocytidine, is a potent inhibitor of NS5B dependent RNA synthesis and HCV replication in cell culture</p>

    <p>          Klumpp, K, Leveque, V, Le Pogam, S, Ma, H, Jiang, WR, Kang, H,  Granycome, C, Singer, M, Laxton, C, Hang, JQ, Sarma, K, Smith, DB, Heindl, D, Hobbs, CJ, Merrett, JH, Symons, J, Cammack, N, Martin, JA, Devos, R, and Najera, I</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16316989&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16316989&amp;dopt=abstract</a> </p><br />

    <p>10.   58039   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Final Results of a Phase 1b, Multiple-Dose Study of Vx-950, a Hepatitis C Virus Protease Inhibitor</p>

    <p>          Reesink, H. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 234A-235A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300095">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300095</a> </p><br />

    <p>11.   58040   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of coxsackie B virus infection by soluble forms of its receptors: Binding affinities, altered particle formation, and competition with cellular receptors</p>

    <p>          Goodfellow, Ian G, Evans, David J, Blom, Anna M, Kerrigan, Dave, Miners, JScott, Morgan, BPaul, and Spiller, OBrad</p>

    <p>          Journal of Virology <b>2005</b>.  79(18): 12016-12024</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   58041   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Entry Inhibitor Gns 037 Exhibits Potent Anti-Hcv Activity in Vitro</p>

    <p>          Halfon, P. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 236A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300097">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480300097</a> </p><br />

    <p>13.   58042   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and characterization of human respiratory syncytial virus entry inhibitors</p>

    <p>          Ni, L, Zhao, L, Qian, Y, Zhu, J, Jin, Z, Chen, YW, Tien, P, and Gao, GF</p>

    <p>          Antivir Ther <b>2005</b>.  10(7): 833-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16312179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16312179&amp;dopt=abstract</a> </p><br />

    <p>14.   58043   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-virals for Influenza Virus Infection</p>

    <p>          Sugaya, N</p>

    <p>          Uirusu <b>2005</b>.  55 (1): 111-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16308537&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16308537&amp;dopt=abstract</a> </p><br />

    <p>15.   58044   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sch 503034, a Mechanism-Based Inhibitor of Hepatitis C Virus (Hcv) Ns3 Protease Suppresses Polyprotein Maturation and Enhances the Antiviral Activity of Interferona-211 (Inf)</p>

    <p>          Malcolm, B. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 535A-536A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301405">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301405</a> </p><br />

    <p>16.   58045   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SARS: Understanding the Virus and Development of Rational Therapy</p>

    <p>          Stadler, K and Rappuoli, R</p>

    <p>          Curr Mol Med <b>2005</b>.  5(7): 677-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16305493&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16305493&amp;dopt=abstract</a> </p><br />

    <p>17.   58046   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hcv Replication Is Inhibited by Cyclosporin a, Mycophenolic Acid and Ifn-Alpha in a Synergistic Fashion and With Distinct Antiviral Kinetics</p>

    <p>          Henry, S. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 536A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301406">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301406</a> </p><br />
    <br clear="all">

    <p>18.   58047   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent Antiviral Activity of North-Methanocarbathymidine against Kaposi&#39;s Sarcoma-Associated Herpesvirus</p>

    <p>          Zhu, W, Burnette, A, Dorjsuren, D, Roberts, PE, Huleihel, M, Shoemaker, RH, Marquez, VE, Agbaria, R, and Sei, S</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(12): 4965-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16304159&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16304159&amp;dopt=abstract</a> </p><br />

    <p>19.   58048   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Itmn a and B, Novel Inhibitors of the Hcvns3/4 Protease Retain Their Potency Against Vx-950 and Biln-2016 Resistant Ns3/4 Protease Mutants and an Ifn-a-2a Resistant Hcv Replicon</p>

    <p>          Seiwert, S. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 537A-538A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301410">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301410</a> </p><br />

    <p>20.   58049   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Biphenylsulfonacetic Acid inhibitors of the human papillomavirus type 6 e1 helicase inhibit ATP hydrolysis by an allosteric mechanism involving tyrosine 486</p>

    <p>          White, PW, Faucher, AM, Massariol, MJ, Welchner, E, Rancourt, J, Cartier, M, and Archambault, J</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(12): 4834-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16304143&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16304143&amp;dopt=abstract</a> </p><br />

    <p>21.   58050   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vivo Inhibition of Hepadnavirus Infection by Acylated Pre-S Peptides in the Urokinase-Type Plasminogen Activator (Upa) Mouse Model</p>

    <p>          Petersen, J. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 584A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302045</a> </p><br />

    <p>22.   58051   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Novel Nucleoside Analog R1479 Is a Potent Inhibitor of Hcv Polymerase and Hcv Replication in Cell Culture</p>

    <p>          Klumpp, K. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 656A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302228">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302228</a> </p><br />

    <p>23.   58052   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus replication by various RNAi constructs and their pharmacodynamic properties</p>

    <p>          Peng, J, Zhao, Y, Mai, J, Pang, WK, Wei, X, Zhang, P, and Xu, Y</p>

    <p>          J Gen Virol <b>2005</b>.  86(Pt 12): 3227-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16298967&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16298967&amp;dopt=abstract</a> </p><br />

    <p>24.   58053   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hcv Resistance to Antiviral Agents</p>

    <p>          Migliaccio, G. </p>

    <p>          Antiviral Therapy <b>2005</b>.  10: 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232470800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232470800003</a> </p><br />

    <p>25.   58054   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of engystol((r)): an in vitro analysis</p>

    <p>          Oberbaum, M, Glatthaar-Saalmuller, B, Stolt, P, and Weiser, M</p>

    <p>          J Altern Complement Med <b>2005</b>.  11(5): 855-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16296918&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16296918&amp;dopt=abstract</a> </p><br />

    <p>26.   58055   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure-activity relationships of dengue antiviral polycyclic quinones</p>

    <p>          Laurent, D, Baumann, F, Benoit, AG, Mortelecqe, A, Nitatpattana, N, Desvignes, I, Debitus, C, Laille, M, Gonzalez, JP, and Chungue, E</p>

    <p>          Southeast Asian J Trop Med Public Health <b>2005</b>.  36(4): 901-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16295543&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16295543&amp;dopt=abstract</a> </p><br />

    <p>27.   58056   DMID-LS-107; PUBMED-DMID-12/6/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Treating HCV with ribavirin analogues and ribavirin-like molecules</p>

    <p>          Gish, RG</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16293677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16293677&amp;dopt=abstract</a> </p><br />

    <p>28.   58057   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Homo-oligomerization of Marburgvirus VP35 is essential for its function in replication and transcription</p>

    <p>          Moeller, Peggy, Pariente, Nonia, Klenk, Hans-Dieter, and Becker, Stephan</p>

    <p>          Journal of Virology <b>2005</b>.  79(23): 14876-14886</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   58058   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of peptidomimetic compounds as HCV NS3 serine protease inhibitors</p>

    <p>          Rosenquist, Asa, Thorstensson, Fredrik, Johansson, Per-Ola, Kvarnstroem, Ingemar, Ayesa, Susana, Classon, Bjoern, Rakos, Lazlo, and Samuelsson, Bertil</p>

    <p>          PATENT:  WO <b>2005073216</b>  ISSUE DATE:  20050811</p>

    <p>          APPLICATION: 2005  PP: 179 pp.</p>

    <p>          ASSIGNEE:  (Medivir AB, Swed.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>30.   58059   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel antiviral helioxanthin analogs</p>

    <p>          Yeo, Hosup, Austin, David J, Li, Ling, and Cheng, Yung-Chi</p>

    <p>          PATENT:  WO <b>2005107742</b>  ISSUE DATE:  20051117</p>

    <p>          APPLICATION: 2005  PP: 90 pp.</p>

    <p>          ASSIGNEE:  (Yale University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   58060   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of morpholinylanilino quinazoline derivatives for use as antiviral agents</p>

    <p>          Spencer, Keith, Dennison, Helena, Matthews, Neil, Barnes, Michael, and Chana, Surinder</p>

    <p>          PATENT:  WO <b>2005105761</b>  ISSUE DATE:  20051110</p>

    <p>          APPLICATION: 2005  PP: 55 pp.</p>

    <p>          ASSIGNEE:  (Arrow Therapeutics Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   58061   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Immunophenotyping Profile of Cpg 10101, a New Tlr9 Agonist Antiviral for Hepatitis C</p>

    <p>          Mchutchinson, J. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 539A-540A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301414">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301414</a> </p><br />

    <p>33.   58062   DMID-LS-107; WOS-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Novel 4 &#39;alpha-Phenyl and 5 &#39;alpha-Methyl Branched Carbocyclic Nucleosides</p>

    <p>          Oh, C. and Hong, J.</p>

    <p>          Bulletin of the Korean Chemical Society <b>2005</b>.  26(10): 1520-1524</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233211900012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233211900012</a> </p><br />

    <p>34.   58063   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cytomegalovirus disintegrin-like peptides</p>

    <p>          Compton, Teresa and Feire, Adam Lloyd</p>

    <p>          PATENT:  US <b>20050260199</b>  ISSUE DATE: 20051124</p>

    <p>          APPLICATION: 2005-62065  PP: 45 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   58064   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of purine derivatives as antiviral agents</p>

    <p>          Wang, Xuechao, Liu, Zhongrong, Li, Bogang, Wu, Fengwei, Zhong, Chaobin, He, Min, and Huang, Yu </p>

    <p>          PATENT:  CN <b>1566118</b>  ISSUE DATE: 20050119</p>

    <p>          APPLICATION: 2003-4232  PP: 15 pp.</p>

    <p>          ASSIGNEE:  (Chengdu Diao Pharmaceutical Group Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   58065   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-herpes virus type 1 activity of oleanane-type triterpenoids</p>

    <p>          Ikeda, Tsuyoshi, Yokomizo, Kazumi, Okawa, Masafumi, Tsuchihashi, Ryota, Kinjo, Junei, Nohara, Toshihiro, and Uyeda, Masaru</p>

    <p>          Biological &amp; Pharmaceutical Bulletin <b>2005</b>.  28(9): 1779-1781</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>37.   58066   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral composition comprising p-menthane-3,8-diol</p>

    <p>          Clarke, Paul Douglas</p>

    <p>          PATENT:  WO <b>2005087209</b>  ISSUE DATE:  20050922</p>

    <p>          APPLICATION: 2005  PP: 37 pp.</p>

    <p>          ASSIGNEE:  (UK)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   58067   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Characterization of Viral Polypeptide Inhibitors Targeting Newcastle Disease Virus Fusion</p>

    <p>          Zhu, Jieqing, Jiang, Xiuli, Liu, Yueyong, Tien, Po, and Gao, George F</p>

    <p>          Journal of Molecular Biology <b>2005</b>.  354(3): 601-613</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   58068   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Orally active lipid esters of antiviral nucleoside phosphonates</p>

    <p>          Hostetler, Karl Y and Beadle, James R</p>

    <p>          Collection Symposium Series <b>2005</b>.  7(Chemistry of Nucleic Acid Components): 95-103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   58069   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The anti-SARS-CoV effects of remedial horse anti-SARS-CoV immunoglobulins</p>

    <p>          Zhao, Guangyu, Ji, Xiaoguang, Shi, Xinfu, Liu, Jianyuan, Li, Yan, Zhang, Shumin, Wang, Junzhi, Zhang, Liangyan, Zhang, Songle, and Wang, Xiliang</p>

    <p>          Zhonghua Weishengwuxue He Mianyixue Zazhi <b>2005</b>.  25(2): 121-123</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   58070   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-SARS coronavirus agents containing flavonols or grape extract and their use as foods, beverages, pharmaceuticals, and cosmetics for prophylactic and therapeutic treatment</p>

    <p>          Tokutake, Masakazu, Katayama, Hiroshi, Yoshinaga, Yoshiyuki, and Yamamoto, Naoki</p>

    <p>          PATENT:  JP <b>2005314316</b>  ISSUE DATE:  20051110</p>

    <p>          APPLICATION: 2004-4390  PP: 12 pp.</p>

    <p>          ASSIGNEE:  (Kikkoman Corp., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   58071   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sequences of SARS coronavirus nucleocapsid protein epitopes and uses in diagnosis and antiviral therapy</p>

    <p>          Kelvin, David, Persad, Desmond, Cameron, Cheryl, Bray, Kurtis R, Lofaro, Lori R, Johnson, Camille, Sekaly, Rafick-Pierre, Younes, Souheil-Antoine, and Chong, Pele</p>

    <p>          PATENT:  WO <b>2005103259</b>  ISSUE DATE:  20051103</p>

    <p>          APPLICATION: 2005  PP: 314 pp.</p>

    <p>          ASSIGNEE:  (University Health Network, Can., Beckman Coulter, Inc., Universite De Montreal, and National Health Research Institutes)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   58072   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polypeptide drug for inhibiting SARS coronavirus from infecting host cell</p>

    <p>          Deng, Hongkui, Ding, Mingxiao, Yuan, Kehu, Qing, Tingting, Chen, Jian, Xiong, Zikai, Nie, Yuchun, Wang, Zai, Yi, Ling, Jiang, Pengfei, and Ren, Lichen</p>

    <p>          PATENT:  CN <b>1566142</b>  ISSUE DATE: 20050119</p>

    <p>          APPLICATION: 2003-11965  PP: 19 pp.</p>

    <p>          ASSIGNEE:  (Peking University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>44.   58073   DMID-LS-107; SCIFINDER-DMID-12/5/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polyoma virus infection of renal allografts: relationships of the distribution of viral infection, tubulointerstitial inflammation, and fibrosis suggesting viral interstitial nephritis in untreated disease</p>

    <p>          Meehan, Shane M, Kadambi, Pradeep V, Manaligod, Jose R, Williams, James W, Josephson, Michelle A, and Javaid, Basit</p>

    <p>          Human Pathology <b>2005</b>.  36(12): 1256-1264</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
