

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-350.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fJL1pvduSGBcNv8Vtie3J/nu2C7SHbWKDkjmWss9ASenNB//WzFKj+kDfu93M01svJqfE6byR7NbUY3FR8FpHF0uTXNRJldh0yMqAOk2wfhRL1j/FM2cttVtvQM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5B18AC03" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-350-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49013   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Broad substrate stereospecificity of the mycobacterium tuberculosis 7-keto-8-aminopelargonic acid synthase: spectroscopic and kinetic studies</p>

    <p>          Bhor, VM, Dev, S, Vasanthakumar, GR, Kumar, P, Sinha, S, and Surolia, A</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16769720&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16769720&amp;dopt=abstract</a> </p><br />

    <p>2.     49014   OI-LS-350; EMBASE-OI-6/12/2006</p>

    <p class="memofmt1-2">          Antimycobacterial activity and possible mode of action of newly isolated neodiospyrin and other naphthoquinones from Euclea natalensis</p>

    <p>          van der Kooy, F, Meyer, JJM, and Lall, N</p>

    <p>          South African Journal of Botany <b>2006</b>.  72(3): 349-352</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7XN9-4JRKWVV-1/2/1d8f335e3fcf18ec04022a2e95d28e13">http://www.sciencedirect.com/science/article/B7XN9-4JRKWVV-1/2/1d8f335e3fcf18ec04022a2e95d28e13</a> </p><br />

    <p>3.     49015   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Broad Specificity of Mammalian Adenylyl Cyclase for Interaction with 2&#39;,3&#39;-Substituted Purine- and Pyrimidine Nucleotide Inhibitors</p>

    <p>          Mou, TC, Gille, A, Suryanarayana, S, Richter, M, Seifert, R, and Sprang, SR</p>

    <p>          Mol Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16766715&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16766715&amp;dopt=abstract</a> </p><br />

    <p>4.     49016   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Antiviral drugs for cytomegalovirus diseases</p>

    <p>          Biron, KK</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16765457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16765457&amp;dopt=abstract</a> </p><br />

    <p>5.     49017   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Two New Cassane-Type Diterpenes from Calliandra californica with Antituberculosis and Cytotoxic Activities</p>

    <p>          Encarnacion-Dimayuga, R, Agundez-Espinoza, J, Garcia, A, Delgado, G, Molina-Salinas, GM, and Said-Fernandez, S</p>

    <p>          Planta Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16755469&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16755469&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     49018   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Synergistic interactions of SQ109, a new ethylene diamine, with front-line antitubercular drugs in vitro</p>

    <p>          Chen, P, Gearhart, J, Protopopova, M, Einck, L, and Nacy, CA</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16751637&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16751637&amp;dopt=abstract</a> </p><br />

    <p>7.     49019   OI-LS-350; EMBASE-OI-6/12/2006</p>

    <p class="memofmt1-2">          Antimycobacterial flavonoids from Derris indica</p>

    <p>          Koysomboon, Sorwaporn, van Altena, Ian, Kato, Shigeru, and Chantrapromma, Kan</p>

    <p>          Phytochemistry  <b>2006</b>.  67(10): 1034-1040</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TH7-4K1X5RB-1/2/c7e67a414fd51c5dd6c6a8c1148947c9">http://www.sciencedirect.com/science/article/B6TH7-4K1X5RB-1/2/c7e67a414fd51c5dd6c6a8c1148947c9</a> </p><br />

    <p>8.     49020   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          In vitro bactericidal activity of oxazolidinone, RBx 8700 against Mycobacterium tuberculosis and Mycobacterium avium complex</p>

    <p>          Rao, M, Sood, R, Malhotra, S, Fatma, T, Upadhyay, DJ, and Rattan, A</p>

    <p>          J Chemother <b>2006</b>.  18(2): 144-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16736882&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16736882&amp;dopt=abstract</a> </p><br />

    <p>9.     49021   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Selection and characterization of replicon variants dually resistant to thumb- and palm-binding nonnucleoside polymerase inhibitors of the hepatitis C virus</p>

    <p>          Le Pogam, S, Kang, H, Harris, SF, Leveque, V, Giannetti, AM, Ali, S, Jiang, WR, Rajyaguru, S, Tavares, G, Oshiro, C, Hendricks, T, Klumpp, K, Symons, J, Browner, MF, Cammack, N, and Najera, I</p>

    <p>          J Virol <b>2006</b> .  80(12): 6146-6154</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16731953&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16731953&amp;dopt=abstract</a> </p><br />

    <p>10.   49022   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          P2-P4 Macrocyclic inhibitors of hepatitis C virus NS3-4A serine protease</p>

    <p>          Arasappan, A, Njoroge, FG, Chen, KX, Venkatraman, S, Parekh, TN, Gu, H, Pichardo, J, Butkiewicz, N, Prongay, A, Madison, V, and Girijavallabhan, V</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16730985&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16730985&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   49023   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Anti-HCV therapies in chimeric scid-Alb/uPA mice parallel outcomes in human clinical application</p>

    <p>          Kneteman, NM, Weiner, AJ, O&#39;connell, J, Collett, M, Gao, T, Aukerman, L, Kovelsky, R, Ni, ZJ, Hashash, A, Kline, J, Hsi, B, Schiller, D, Douglas, D, Tyrrell, DL, and Mercer, DF</p>

    <p>          Hepatology <b>2006</b>.  43(6): 1346-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16729319&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16729319&amp;dopt=abstract</a> </p><br />

    <p>12.   49024   OI-LS-350; PUBMED-OI-6/14/2006</p>

    <p class="memofmt1-2">          Aegicerin, the first oleanane triterpene with wide-ranging antimycobacterial activity, isolated from Clavija procera</p>

    <p>          Rojas, R, Caviedes, L, Aponte, JC, Vaisberg, AJ, Lewis, WH, Lamas, G, Sarasara, C, Gilman, RH, and Hammond, GB</p>

    <p>          J Nat Prod <b>2006</b>.  69(5): 845-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724857&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724857&amp;dopt=abstract</a> </p><br />

    <p>13.   49025   OI-LS-350; EMBASE-OI-6/12/2006</p>

    <p class="memofmt1-2">          Identification of thieno[3,2-b]pyrroles as allosteric inhibitors of hepatitis C virus NS5B polymerase</p>

    <p>          Ontoria, Jesus M, Martin Hernando, Jose I, Malancona, Savina, Attenni, Barbara, Stansfield, Ian, Conte, Immacolata, Ercolani, Caterina, Habermann, Jorg, Ponzi, Simona, and Di Filippo, Marcello</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4K128YD-6/2/65f85b1396b7a9ab3f1fd6ad4471cd60">http://www.sciencedirect.com/science/article/B6TF9-4K128YD-6/2/65f85b1396b7a9ab3f1fd6ad4471cd60</a> </p><br />

    <p>14.   49026   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Complex formation between hepatitis C virus NS2 and NS3 proteins</p>

    <p>          Kiiver, K, Merits, A, Ustav, M, and Zusinaite, E</p>

    <p>          VIRUS RESEARCH  <b>2006</b>.  117(2): 264-272, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237668100009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237668100009</a> </p><br />

    <p>15.   49027   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Synthesis and biological assessment of hemiacetal spiro derivatives towards development of efficient chemotherapeutic agent</p>

    <p>          Ogamino, T, Ohnishi, S, Ishikawa, Y, Sugai, T, Obata, R, and Nishiyama, S</p>

    <p>          SCIENCE AND TECHNOLOGY OF ADVANCED MATERIALS <b>2006</b>.  7(2): 175-183, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237644300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237644300009</a> </p><br />

    <p>16.   49028   OI-LS-350; EMBASE-OI-6/12/2006</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          White, P. W., Llinas-Brunet, M., and Bos, M.</p>

    <p>          &lt;10 Journal Title&gt; <b>2006</b>.: pp. 65-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7CV8-4JXVCPR-5/2/e793426a86a11ab166e2cf3bf03e899e">http://www.sciencedirect.com/science/article/B7CV8-4JXVCPR-5/2/e793426a86a11ab166e2cf3bf03e899e</a> </p><br />
    <br clear="all">

    <p>17.   49029   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Construction and characterization of infectious intragenotypic and intergenotypic hepatitis C virus chimeras</p>

    <p>          Pietschmann, T, Kaul, A, Koutsoudakis, G, Shavinskaya, A, Kallis, S, Steinmann, E, Abid, K, Negro, F, Dreux, M, Cosset, FL, and Bartenschlager, R</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2006</b>.  103(19): 7408-7413, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237589000039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237589000039</a> </p><br />

    <p>18.   49030   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Expression, purification, and characterization of a galactofuranosyltransferase involved in Mycobacterium tuberculosis arabinogalactan biosynthesis</p>

    <p>          Rose, NL, Completo, GC, Lin, SJ, McNeil, M, Palcic, MM, and Lowary, TL</p>

    <p>          JOURNAL OF THE AMERICAN CHEMICAL SOCIETY <b>2006</b>.  128(20): 6721-6729, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237590500047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237590500047</a> </p><br />

    <p>19.   49031   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Emergence of Mycobacterium tuberculosis with extensive resistance to second-line drugs - Worldwide, 2000-2004 (Reprinted from MMWR, vol 55, pg 301-305, 2006)</p>

    <p>          Wright, A, Bai, G, Barrera, L, Boulahbal, F, Martin-Casabona, N, Gilpin, C, Drobniewsji, F, Havelkova, M, Lepe, R, Lumb, R, Metchock, B, Portaels, F, Rodrigues, M, Rusch-Gerdes, S, Van, Deun A, Vincent, V, Leimane, V, Riekstina, V, Skenders, G, Holtz, T, Pratt, R, Laserson, K, Wells, C, Cegielski, P, and Shah, NS</p>

    <p>          JAMA-JOURNAL OF THE AMERICAN MEDICAL ASSOCIATION <b>2006</b>.  295(20): 2349-2351, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237734400011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237734400011</a> </p><br />

    <p>20.   49032   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          QSAR study of antimycobacterial activity of quaternary ammonium salts of piperidinylethyl esters of alkoxysubstituted phenylcarbamic acids</p>

    <p>          Waisser, K, Dolezal, R, Palat, K, Cizmarik, J, and Kaustova, J</p>

    <p>          FOLIA MICROBIOLOGICA <b>2006</b>.  51(1): 21-24, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237519900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237519900002</a> </p><br />

    <p>21.   49033   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          New antimycobacterial 2,3-dihydro-1-alkylindole-2-thiones</p>

    <p>          Waisser, K, Heinisch, L, Slosarek, M, and Janota, J</p>

    <p>          FOLIA MICROBIOLOGICA <b>2006</b>.  51(1): 25-26, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237519900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237519900003</a> </p><br />

    <p>22.   49034   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Antiviral drug discovery targeting to viral proteases</p>

    <p>          Hsu, JTA, Wang, HC, Chen, GW, and Shih, SR</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2006</b>.  12(11): 1301-1314, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237432600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237432600002</a> </p><br />
    <br clear="all">

    <p>23.   49035   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Foldamer-based inhibitors of cytomegalovirus entry</p>

    <p>          English, E, Roy, R, Gellman, S, and Compton, T</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  70(1): A32-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200018</a> </p><br />

    <p>24.   49036   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Application of stable hepatitis C virus (HCV)-secreting human hepatoma cell lines for antiviral drug discovery</p>

    <p>          Luo, GX, Cai, ZH, Zhang, C, Chang, KS, and Jiang, JY</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  70(1): A35-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200026</a> </p><br />

    <p>25.   49037   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Interference of hepatitis c virus replication by combination of protein kinase C-like 2 inhibitors and interferon-alpha</p>

    <p>          Kim, SJ, Yun, J, and Oh, JW</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  70(1): A56-A57, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200082">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200082</a> </p><br />

    <p>26.   49038   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Inhibition of murine cytomegalovirus by second generation ribonucleotide reductase inhibitors didox and trimidox</p>

    <p>          Inayat, M, Cooper, S, Smee, D, Gallicchio, V, Garvy, B, Elford, H, and Oakley, O</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  70(1): A68-A69, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200112">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200112</a> </p><br />

    <p>27.   49039   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Effect of oral treatment with (S)-HPMPA, HDP-(S)HPMPA, or ODE-(S)-HPMPA on replication of human cytomegalovirus (HCMV) or murine cytomegalovirus (MCMV) in animal models</p>

    <p>          Quenelle, D, Collins, D, Pettway, L, Hartline, C, Beadle, J, Wan, W, Hostetler, K, and Kern, E </p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  70(1): A70-A71, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200117">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200117</a> </p><br />

    <p>28.   49040   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          A nucleoside inhibitor of hepatitis C virus replication efficiently inhibits the replication of flaviviruses in vitro</p>

    <p>          Leyssen, P, De Lamballerie, X, De Clercq, E, and Neyts, J</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  70(1): A80-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200142">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200142</a> </p><br />

    <p>29.   49041   OI-LS-350; WOS-OI-6/4/2006</p>

    <p class="memofmt1-2">          Lydiamycins A-D: Cyclodepsipetides with antimycobacterial properties</p>

    <p>          Huang, X, Roemer, E, Sattler, I, Moellmann, U, Christner, A, and Grabley, S</p>

    <p>          ANGEWANDTE CHEMIE-INTERNATIONAL EDITION <b>2006</b>.  45(19): 3067-3072, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237533400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237533400006</a> </p><br />

    <p>30.   49042   OI-LS-350; WOS-OI-6/11/2006</p>

    <p class="memofmt1-2">          Lessons from experimental Mycobacterium tuberculosis infections</p>

    <p>          Flynn, JL</p>

    <p>          MICROBES AND INFECTION <b>2006</b>.  8(4): 1179-1188, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237754100027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237754100027</a> </p><br />

    <p>31.   49043   OI-LS-350; WOS-OI-6/11/2006</p>

    <p class="memofmt1-2">          Mycophenolic acid inhibits hepatitis C virus replication independent of guanosine depletion and acts in synergy with interferon-A</p>

    <p>          Henry, SD, Metsclaar, HJ, Kok, A, Haagmans, BL, Tilanus, HW, and van, der Laan LJW</p>

    <p>          LIVER TRANSPLANTATION <b>2006</b>.  12(5): C109-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237037100471">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237037100471</a> </p><br />

    <p>32.   49044   OI-LS-350; WOS-OI-6/11/2006</p>

    <p class="memofmt1-2">          Characterization of the early steps of hepatitis C virus infection by using luciferase reporter viruses</p>

    <p>          Koutsoudakis, G, Kaul, A, Steinmann, E, Kallis, S, Lohmann, V, Pietschmann, T, and Bartenschlager, R</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(11): 5308-5320, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237753400021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237753400021</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
