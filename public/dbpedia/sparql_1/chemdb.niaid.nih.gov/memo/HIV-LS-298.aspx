

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-298.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5bKG4NSyF8vHwHY+yFptWt+947KBFnXkFqTZ3nbSit7vsrH2IuvW18MzaWmLK83XfvchjcsVUH2YnutFGgVe2sboeeRM77j5nxgnpAkuDxetX+2LWLR9FgV1FDU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2C4F7C49" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-298-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43529   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Non-Peptidic HIV Protease Inhibitors</p>

<p>           Chrusciel, RA and Strohbach, JW</p>

<p>           Curr Top Med Chem 2004. 4(10): 1097-114</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193141&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193141&amp;dopt=abstract</a> </p><br />

<p>     2.    43530   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Peptidomimetic Inhibitors of HIV Protease</p>

<p>           Randolph, JT and DeGoey, DA</p>

<p>           Curr Top Med Chem 2004. 4(10): 1079-95</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193140&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193140&amp;dopt=abstract</a> </p><br />

<p>     3.    43531   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           HIV-1 Integrase Inhibitors: A Decade of Research and Two Drugs in Clinical Trial</p>

<p>           Johnson, AA, Marchand, C, and Pommier, Y</p>

<p>           Curr Top Med Chem 2004. 4(10): 1059-77</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193139&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15193139&amp;dopt=abstract</a> </p><br />

<p>     4.    43532   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Synthesis, Structure-Activity Relationships, and Drug Resistance of beta-d-3&#39;-Fluoro-2&#39;,3&#39;-Unsaturated Nucleosides as Anti-HIV Agents</p>

<p>           Zhou, W, Gumina, G, Chong, Y, Wang, J, Schinazi, RF, and Chu, CK</p>

<p>           J Med Chem 2004. 47(13): 3399-408</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15189036&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15189036&amp;dopt=abstract</a> </p><br />

<p>     5.    43533   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           HIV-chemotherapy and -prophylaxis: new drugs, leads and approaches</p>

<p>           De Clercq, Erik</p>

<p>           The International Journal of Biochemistry &amp; Cell Biology 2004. 36(9): 1800-1822</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCH-4CBDBKJ-1/2/1255b5662c1cb233be6baacbadc8220d">http://www.sciencedirect.com/science/article/B6TCH-4CBDBKJ-1/2/1255b5662c1cb233be6baacbadc8220d</a> </p><br />

<p>     6.    43534   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Synthesis, antiviral, and anti-HIV-1 integrase activities of 3-aroyl-1,1-dioxo-1,4,2-benzodithiazines</p>

<p>           Brzozowski, Z, Saczewski, F, Sanchez, T, Kuo, CL, Gdaniec, M, and Neamati, N</p>

<p>           Bioorg Med Chem 2004. 12(13): 3663-72</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15186851&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15186851&amp;dopt=abstract</a> </p><br />

<p>     7.    43535   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Detection of HIV-1 antiretroviral resistance from patients with persistently low but detectable viraemia</p>

<p>           Mackie, Nicola, Dustan, Simon, McClure, Myra O, Weber, Jonathan N, and Clarke, John R</p>

<p>           Journal of Virological Methods 2004.  119(2): 73-78</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T96-4C76GN5-2/2/f048012db030a1f2adef5984b4777052">http://www.sciencedirect.com/science/article/B6T96-4C76GN5-2/2/f048012db030a1f2adef5984b4777052</a> </p><br />

<p>     8.    43536   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Sensitivity of HIV type 1 subtype C isolates to the entry inhibitor T-20</p>

<p>           Cilliers, T, Patience, T, Pillay, C, Papathanasopoulos, M, and Morris, L</p>

<p>           AIDS Res Hum Retroviruses 2004. 20(5): 477-82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15186521&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15186521&amp;dopt=abstract</a> </p><br />

<p>     9.    43537   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           The Stability of the Intact Envelope Glycoproteins is a Major Determinant of Sensitivity of HIV/SIV to Peptidic Fusion Inhibitors</p>

<p>           Gallo, Stephen A, Sackett, Kelly, Rawat, Satinder S, Shai, Yechiel, and Blumenthal, Robert</p>

<p>           Journal of Molecular Biology 2004.  340(1): 9-14</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WK7-4CDG0BR-1/2/91f90c9ad8c7fbad207b50afcc4d5ce5">http://www.sciencedirect.com/science/article/B6WK7-4CDG0BR-1/2/91f90c9ad8c7fbad207b50afcc4d5ce5</a> </p><br />

<p>   10.    43538   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Design and synthesis of novel dihydroxyindole-2-carboxylic acids as HIV-1 integrase inhibitors</p>

<p>           Sechi, M, Angotzi, G, Dallocchio, R, Dessi, A, Carta, F, Sannia, L, Mariani, A, Fiori, S, Sanchez, T, Movsessian, L, Plasencia, C, and Neamati, N</p>

<p>           Antivir Chem Chemother 2004. 15(2): 67-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185725&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185725&amp;dopt=abstract</a> </p><br />

<p>   11.    43539   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Obligatory involvement of CD26/dipeptidyl peptidase IV in the activation of the antiretroviral tripeptide glycylprolylglycinamide (GPG-NH(2))</p>

<p>           Balzarini, J, Andersson, E, Schols, D, Proost, P, Van, Damme J, Svennerholm, B, Horal, P, and Vahlne, A</p>

<p>           Int J Biochem Cell Biol 2004. 36(9): 1848-59</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15183349&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15183349&amp;dopt=abstract</a> </p><br />

<p>   12.    43540   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           New Carbohydrate Specificity and HIV-1 Fusion Blocking Activity of the Cyanobacterial Protein MVL: NMR, ITC and Sedimentation Equilibrium Studies</p>

<p>           Bewley, Carole A, Cai, Mengli, Ray, Satyajit, Ghirlando, Rodolfo, Yamaguchi, Masato, and Muramoto, Koji</p>

<p>           Journal of Molecular Biology 2004.  339(4): 901-914</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WK7-4C8DCM4-1/2/0968718ad542716afd92f3b43fdc5014">http://www.sciencedirect.com/science/article/B6WK7-4C8DCM4-1/2/0968718ad542716afd92f3b43fdc5014</a> </p><br />

<p>   13.    43541   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Effects of HIV-1 entry inhibitors in combination</p>

<p>           Tremblay, C</p>

<p>           Curr Pharm Des 2004. 10(15): 1861-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180545&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180545&amp;dopt=abstract</a> </p><br />

<p>  14.     43542   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           HIV-resistance to viral entry inhibitors</p>

<p>           Menendez-Arias, L and Este, JA</p>

<p>           Curr Pharm Des 2004. 10(15): 1845-60</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180544&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180544&amp;dopt=abstract</a> </p><br />

<p>   15.    43543   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Intracellular tracking of protamine/antisense oligonucleotide nanoparticles and their inhibitory effect on HIV-1 transactivation</p>

<p>           Dinauer, Norbert, Lochmann, Dirk, Demirhan, Ilhan, Bouazzaoui, Abdellatif, Zimmer, Andreas, Chandra, Angelika, Kreuter, Jorg, and von Briesen, Hagen</p>

<p>           Journal of Controlled Release 2004.  96(3): 497-507</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T3D-4C52NVY-1/2/a017bf4da34785aee8e22fedab94ab58">http://www.sciencedirect.com/science/article/B6T3D-4C52NVY-1/2/a017bf4da34785aee8e22fedab94ab58</a> </p><br />

<p>   16.    43544   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           A novel class of HIV-1 inhibitors that targets the viral envelope and inhibits CD4 receptor binding</p>

<p>           Wang, HG, Williams, RE, and Lin, PF</p>

<p>           Curr Pharm Des 2004. 10(15): 1785-93</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180540&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180540&amp;dopt=abstract</a> </p><br />

<p>   17.    43545   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Antiviral Properties of Quinolone-based Drugs</p>

<p>           Richter, S, Parolin, C, Palumbo, M, and Palu, G</p>

<p>           Curr Drug Targets Infect Disord 2004. 4(2): 111-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180459&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180459&amp;dopt=abstract</a> </p><br />

<p>   18.    43546   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           A novel laccase with fair thermostability from the edible wild mushroom (Albatrella dispansus)</p>

<p>           Wang, HX and Ng, TB</p>

<p>           Biochem Biophys Res Commun 2004. 319(2):  381-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178417&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178417&amp;dopt=abstract</a> </p><br />

<p>   19.    43547   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Polyamidoamine dendrimers inhibit binding of Tat peptide to TAR RNA</p>

<p>           Zhao, Hong, Li, Jinru, Xi, Fu, and Jiang, Long</p>

<p>           FEBS Letters 2004. 563(1-3): 241-245</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T36-4BYP29F-G/2/d20f69a56f94363eb417067c01dc3bed">http://www.sciencedirect.com/science/article/B6T36-4BYP29F-G/2/d20f69a56f94363eb417067c01dc3bed</a> </p><br />

<p>   20.    43548   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Establishment of a novel CCR5 and CXCR4 expressing CD4+ cell line which is highly sensitive to HIV and suitable for high-throughput evaluation of CCR5 and CXCR4 antagonists</p>

<p>           Princen, K, Hatse, S, Vermeire, K, De, Clercq E, and Schols, D</p>

<p>           Retrovirology  2004. 1(1): 2</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15169555&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15169555&amp;dopt=abstract</a> </p><br />

<p>   21.    43549   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Crucial amides for dimerization inhibitors of HIV-1 protease</p>

<p>           Bowman, Michael J and Chmielewski, Jean</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(6): 1395-1398</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BT8F6G-3/2/dfcc94eb5e99448350e1fc36c4e136a7">http://www.sciencedirect.com/science/article/B6TF9-4BT8F6G-3/2/dfcc94eb5e99448350e1fc36c4e136a7</a> </p><br />

<p>   22.    43550   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           A new ribonuclease from the black oyster mushroom Pleurotus ostreatus</p>

<p>           Wang, HX and Ng, TB</p>

<p>           Peptides 2004. 25(4): 685-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15165725&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15165725&amp;dopt=abstract</a> </p><br />

<p>   23.    43551   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Binding of hybrid molecules containing pyrrolo [2,1-c][1,4]benzodiazepine (PBD) and oligopyrrole carriers to the human immunodeficiency type 1 virus TAR-RNA</p>

<p>           Mischiati, Carlo, Finotti, Alessia, Sereni, Alessia, Boschetti, Sindi, Baraldi, Pier Giovanni, Romagnoli, Romeo, Feriotto, Giordana, Jeang, Kuan-Teh, Bianchi, Nicoletta, Borgatti, Monica, and Gambari, Roberto</p>

<p>           Biochemical Pharmacology 2004. 67(3): 401-410</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T4P-4BRCNCP-3/2/f9cf7272f21dd4b21f3558568e53d075">http://www.sciencedirect.com/science/article/B6T4P-4BRCNCP-3/2/f9cf7272f21dd4b21f3558568e53d075</a> </p><br />

<p>   24.    43552   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Structure-based design, synthesis, and antimicrobial activity of purine derived SAH/MTA nucleosidase inhibitors</p>

<p>           Tedder, ME, Nie, Z, Margosiak, S, Chu, S, Feher, VA, Almassy, R, Appelt, K, and Yager, KM</p>

<p>           Bioorg Med Chem Lett 2004. 14(12):  3165-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149667&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149667&amp;dopt=abstract</a> </p><br />

<p>   25.    43553   HIV-LS-298; PUBMED-HIV-6/16/2004</p>

<p class="memofmt1-3">           Design, synthesis, and evaluation of 5&#39;-S-aminoethyl-N(6)- azidobenzyl-5&#39;-thioadenosine biotin conjugate: a bifunctional photoaffinity probe for the es nucleoside transporter</p>

<p>           Addo, JK and Buolamwini, JK</p>

<p>           Bioconjug Chem 2004. 15(3): 536-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149181&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149181&amp;dopt=abstract</a> </p><br />

<p>   26.    43554   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Inhibition of HIV-1 TAR RNA-Tat peptide complexation using poly(acrylic acid)</p>

<p>           Zhao, Hong, Li, Jinru, and Jiang, Long</p>

<p>           Biochemical and Biophysical Research Communications 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4CK7STX-5/2/9b474cec37aa7b4b99253f52f5eef670">http://www.sciencedirect.com/science/article/B6WBK-4CK7STX-5/2/9b474cec37aa7b4b99253f52f5eef670</a> </p><br />

<p>  27.     43555   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Syntheses and SAR studies of 4-(heteroarylpiperdin-1-yl-methyl)-pyrrolidin-1-yl-acetic acid antagonists of the human CCR5 chemokine receptor</p>

<p>           Shankaran, K, Donnelly, Karla L, Shah, Shrenik K, Guthikonda, Ravindra N, MacCoss, Malcolm, Mills, Sander G, Gould, Sandra L, Malkowitz, Lorraine, Siciliano, Salvatore J, and Springer, Martin S</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(13): 3419-3424</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CDHFTG-6/2/31c9d689640beb297ec738dd1c752c70">http://www.sciencedirect.com/science/article/B6TF9-4CDHFTG-6/2/31c9d689640beb297ec738dd1c752c70</a> </p><br />

<p>   28.    43556   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Syntheses and biological evaluation of 5-(piperidin-1-yl)-3-phenyl-pentylsulfones as CCR5 antagonists</p>

<p>           Shankaran, K, Donnelly, Karla L, Shah, Shrenik K, Caldwell, Charles G, Chen, Ping, Finke, Paul E, Oates, Bryan, MacCoss, Malcolm, Mills, Sander G, and DeMartino, Julie A</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(13): 3589-3593</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CF5BWW-2/2/d0f7240a135e5f5e1c19dcae1c722417">http://www.sciencedirect.com/science/article/B6TF9-4CF5BWW-2/2/d0f7240a135e5f5e1c19dcae1c722417</a> </p><br />

<p>   29.    43557   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Antiviral activity against human immunodeficiency virus-1 in vitro by myristoylated-peptide from Heliothis virescens</p>

<p>           Ourth, Donald D</p>

<p>           Biochemical and Biophysical Research Communications 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4CKNK3F-1/2/272867c18539a322bf4ac7eea6731441">http://www.sciencedirect.com/science/article/B6WBK-4CKNK3F-1/2/272867c18539a322bf4ac7eea6731441</a> </p><br />

<p>   30.    43558   HIV-LS-298; EMBASE-HIV-6/16/2004</p>

<p class="memofmt1-3">           Mechanisms of HIV-1 escape from immune responses and antiretroviral drugs</p>

<p>           Bailey, Justin, Blankson, Joel N, Wind-Rotolo, Megan, and Siliciano, Robert F</p>

<p>           Current Opinion in Immunology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VS1-4CMJ0D5-1/2/84735d93c617d30c21aa62a34dde1652">http://www.sciencedirect.com/science/article/B6VS1-4CMJ0D5-1/2/84735d93c617d30c21aa62a34dde1652</a> </p><br />

<p>   31.    43559   HIV-LS-298; WOS-HIV-6/6/2004</p>

<p class="memofmt1-3">           Mycobacterium tuberculosis - Induced CXCR4 and chemokine expression leads to preferential X4 HIV-1 replication in human macrophages</p>

<p>           Hoshino, Y, Tse, DB, Rochford, G, Prabhakar, S, Hoshino, S, Chitkara, N, Kuwabara, K, Ching, E, Raju, B, Gold, JA, Borkowsky, W, Rom, WN, Pine, R, and Weiden, M</p>

<p>           J IMMUNOL 2004. 172(10): 6251-6258</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221276900058">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221276900058</a> </p><br />

<p>   32.    43560   HIV-LS-298; WOS-HIV-6/6/2004</p>

<p class="memofmt1-3">           Linker-modified quinoline derivatives targeting HIV-1 integrase: synthesis and biological activity</p>

<p>           Bernard, C, Zouhiri, F, Normand-Bayle, M, Danet, M, Desmaele, D, Leh, H, Mouscadet, JF, Mbemba, G, Thomas, CM, Bonnenfant, S, Le, Bret M, and d&#39;Angelo, J</p>

<p>           BIOORG MED CHEM LETT 2004. 14(10):  2473-2476</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221316000017">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221316000017</a> </p><br />

<p>   33.    43561   HIV-LS-298; WOS-HIV-6/6/2004</p>

<p class="memofmt1-3">           Novel lopinavir analogues incorporating heterocyclic replacements of six-member cyclic urea - synthesis and structure-activity relationships</p>

<p>           Sham, HL, Betebenner, DA, Rosenbrook, W, Herrin, T, Saldivar, A, Vasavanonda, S, Plattner, JJ, and Norbeck, DW</p>

<p>           BIOORG MED CHEM LETT 2004. 14(10):  2643-2645</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221316000051">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221316000051</a> </p><br />

<p>  34.     43562   HIV-LS-298; WOS-HIV-6/6/2004</p>

<p class="memofmt1-3">           Synergistic anti-HIV activity of zinc-finger inhibitory molecules used in combination with a variety of other anti-HIV agents</p>

<p>           Buckheit, RW, Russell, J, Hartman, TL, Imnan, JK, Schito, M, Goel, A, Appella, E, and Turpin, JA</p>

<p>           ANTIVIR RES 2004. 62(2): A27-A28</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800004</a> </p><br />

<p>   35.    43563   HIV-LS-298; WOS-HIV-6/6/2004</p>

<p class="memofmt1-3">           Discovery of a novel class of small-molecule HIV entry inhibitors that target the gp120-binding domain of CD4</p>

<p>           Yang, Q, Roberts, P, Zhu, W, Stephen, A, Adelsberger, J, Currens, M, Feng, Y, Fisher, R, Rein, A, Shoemaker, R, and Sei, S</p>

<p>           ANTIVIR RES 2004. 62(2): A28</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800005</a> </p><br />

<p>   36.    43564   HIV-LS-298; WOS-HIV-6/6/2004</p>

<p class="memofmt1-3">           Novel tyrosine derived HIV protease inhibitors possessing potent anti-HIV activity and remarkable inverse antiviral resistance profiles</p>

<p>           Miller, JF, Brieger, M, Furfine, ES, Hazen, RJ, Kaldor, I, and Spaltenstien, A</p>

<p>           ANTIVIR RES 2004. 62(2): A41</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800035">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800035</a> </p><br />

<p>   37.    43565   HIV-LS-298; WOS-HIV-6/13/2004</p>

<p class="memofmt1-3">           Thrombin activates envelope glycoproteins of HIV type 1 and enhances fusion</p>

<p>           Ling, H, Xiao, P, Usami, O, and Hattori, T</p>

<p>           MICROBES INFECT 2004. 6(5): 414-420</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221494200001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221494200001</a> </p><br />

<p>   38.    43566   HIV-LS-298; WOS-HIV-6/13/2004</p>

<p class="memofmt1-3">           Specific reactions between purified HIV-1 particles and CD4(+) cell membrane fragments in a cell-free system of virus fusion or entry</p>

<p>           Harada, T, Tatsumi, M, Takahashi, H, Sata, T, Kurata, T, and Kojima, A</p>

<p>           MICROBES INFECT 2004. 6(5): 421-428</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221494200002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221494200002</a> </p><br />

<p>   39.    43567   HIV-LS-298; WOS-HIV-6/13/2004</p>

<p class="memofmt1-3">           Interaction of cycloSal-pronucleotides with cholinesterases from different origins. A structure-activity relationship</p>

<p>           Meier, C, Ducho, C, Gorbig, U, Esnouf, R, and Balzarini, J</p>

<p>           J MED CHEM 2004. 47(11): 2839-2852</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221456700015">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221456700015</a> </p><br />

<p>   40.    43568   HIV-LS-298; WOS-HIV-6/13/2004</p>

<p class="memofmt1-3">           The antiviral activity of the milk protein lactoferrin against the human immunodeficiency virus type 1</p>

<p>           Berkhout, B, Floris, R, Recio, I, and Visser, S</p>

<p>           BIOMETALS 2004. 17(3): 291-294</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221392300017">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221392300017</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
