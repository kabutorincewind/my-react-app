

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-10-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1cDmQ7I+GyHVusFxc4txRYoEKbbpqLPIYqCnAdfacN66AJALuOnVf3hj4Jf09yp1l/+mh8CA6awF2tnkq0hvTitEr4P6VwM/zdTBZUos6AZQhGr6G7DbwL41Xt0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7E0A6595" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: September 26 - October 9, 2014</h1>

    <h2>Hepatitis A Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25008939">New Small-molecule Inhibitors Effectively Blocking Picornavirus Replication.</a> Ford Siltz, L.A., E.G. Viktorova, B. Zhang, D. Kouiavskaia, E. Dragunsky, K. Chumakov, L. Isaacs, and G.A. Belov. Journal of Virology, 2014. 88(19): p. 11091-11107. PMID[25008939].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25155914">The Enantiomers of the 1&#39;,6&#39;-Isomer of Neplanocin A: Synthesis and Antiviral Properties.</a> Ye, W. and S.W. Schneller. Bioorganic &amp; Medicinal Chemistry, 2014. 22(19): p. 5315-5319. PMID[25155914].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="https://mail.google.com/mail/u/0/#inboxhttp://www.ncbi.nlm.nih.gov/pubmed/?term=24986787">Mangosteen Xanthones Suppress Hepatitis C Virus Genome Replication.</a> Choi, M., Y.M. Kim, S. Lee, Y.W. Chin, and C. Lee. Virus Genes, 2014. 49(2): p. 208-222. PMID[24986787].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25096404">GS-9669: A Novel Non-nucleoside Inhibitor of Viral Polymerase for the Treatment of Hepatitis C Virus Infection.</a> Gentile, I., A.R. Buonomo, E. Zappulo, N. Coppola, and G. Borgia. Expert Review of Anti-infective Therapy, 2014. 12(10): p. 1179-1186. PMID[25096404].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25242482">Synthetic Fermentation of Bioactive Non-ribosomal Peptides without Organisms, Enzymes or Reagents.</a> Huang, Y.L. and J.W. Bode. Nature Chemistry, 2014. 6(10): p. 877-884. PMID[25242482].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25081106">Chemical Constituents of Abies nukiangensis.</a> Li, Y.L., Y.X. Gao, H.Z. Jin, L. Shan, X.S. Liang, X.K. Xu, X.W. Yang, N. Wang, A. Steinmetz, Z. Chen, and W.D. Zhang. Phytochemistry, 2014. 106: p. 116-123. PMID[25081106].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25151487">Fluoxetine a Novel anti-Hepatitis C Virus Agent via ROS-, JNK-, and PPARbeta/gamma-dependent Pathways.</a> Young, K.C., C.H. Bai, H.C. Su, P.J. Tsai, C.Y. Pu, C.S. Liao, Y.M. Lin, H.W. Lai, L.W. Chong, Y.S. Tsai, and C.W. Tsao. Antiviral Research, 2014. 110: p. 158-167. PMID[25151487].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25092704">Agmatine-containing Poly(amidoamine)s as a Novel Class of Antiviral Macromolecules: Structural Properties and in Vitro Evaluation of Infectivity Inhibition.</a> Donalisio, M., E. Ranucci, V. Cagno, A. Civra, A. Manfredi, R. Cavalli, P. Ferruti, and D. Lembo. Antimicrobial Agents and Chemotherapy, 2014. 58(10): p. 6315-6319. PMID[25092704].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=24986086">Rapamycin Does Not Inhibit Human Cytomegalovirus Reactivation from Dendritic Cells in Vitro.</a> Glover, T.E., V.G. Kew, and M.B. Reeves. The Journal of General Virology, 2014. 95(Pt 10): p. 2260-2266. PMID[24986086].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p>

    <h2>Herpes Simplex Virus I</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=24817544">Attachment and Penetration of Acyclovir-resistant Herpes Simplex Virus Are Inhibited by Melissa officinalis Extract.</a> Astani, A., M. Heidary Navid, and P. Schnitzler. Phytotherapy Research, 2014. 28(10): p. 1547-1552. PMID[24817544].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25169112">Evidence That the Major Hemolymph Protein of the Pacific Oyster, Crassostrea gigas, has Antiviral Activity against Herpesviruses.</a> Green, T.J., N. Robinson, T. Chataway, K. Benkendorff, W. O&#39;Connor, and P. Speck. Antiviral Research, 2014. 110: p. 168-174. PMID[25169112].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25172789">Pentacyclic Triterpenes in Birch Bark Extract Inhibit Early Step of Herpes Simplex Virus Type 1 Replication.</a> Heidary Navid, M., M.N. Laszczyk-Lauer, J. Reichling, and P. Schnitzler. Phytomedicine, 2014. 21(11): p. 1273-1280. PMID[25172789].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=25111906">Ellagitannins as Synergists of ACV on the Replication of ACV-resistant Strains of HSV 1 and 2.</a> Vilhelmova-Ilieva, N., R. Jacquet, S. Quideau, and A.S. Galabov. Antiviral Research, 2014. 110: p. 104-114. PMID[25111906].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0926-100914.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341062400002">Synthesis, Molecular Modeling, and Biological Evaluation of Novel Benzimidazole Derivatives as Inhibitors of Hepatitis C Virus RNA Replication.</a> El Diwani, H.I., H.T. Abdel-Mohsen, I. Salama, F.A.F. Ragab, M.M. Ramla, S.A. Galal, M.M. Abdalla, A. Abdel-Wahab, and M.A. El Demellawy. Chemical &amp; Pharmaceutical Bulletin, 2014. 62(9): p. 856-866. ISI[000341062400002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">15<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341717400014">. Identification of Novel Human Kinases That Suppress Hepatitis C Virus Infection.</a> Lee, A., S. Liu, and T. Wang. Journal of Viral Hepatitis, 2014. 21(10): p. 716-726. ISI[000341717400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0926-100914.</p><br /> 

    <p class="plaintext">16. <a href="https://mail.google.com/mail/u/0/#inboxhttp://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341588100068">HIV Integrase Inhibitors Block Replication of Alpha-, Beta-, and Gammaherpesviruses.</a> Yan, Z.P., K.F. Bryant, S.M. Gregory, M. Angelova, D.H. Dreyfus, X.Z. Zhao, D.M. Coen, T.R. Burke, and D.M. Knipe. mBio, 2014. 5(4): p. e01318-14 . ISI[000341588100068].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0926-100914.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
