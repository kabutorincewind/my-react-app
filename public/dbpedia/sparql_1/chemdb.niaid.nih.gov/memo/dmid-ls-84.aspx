

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-84.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wUl4TVsP8DPAFHCMR5vC1GdksYRd8Bb3tIshARp01SIdFkxyDgzmPM77CoY6qOTvebcod7zTBGM+mOLNaDvbqpH8oJ4ML9D1Pywc53uLVNWqetgUhVV9RB5pVsM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6C841DE7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-84-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56055   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Update on Human Herpesvirus 6 Biology, Clinical Features, and Therapy</p>

    <p>          De Bolle, L, Naesens, L, and De Clercq, E</p>

    <p>          Clin Microbiol Rev <b>2005</b>.  18(1): 217-245</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653828&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653828&amp;dopt=abstract</a> </p><br />

    <p>2.     56056   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          3-NITROTYROSINE ATTENUATES RESPIRATORY SYNCYTIAL VIRUS INFECTION IN HUMAN BRONCHIAL EPITHELIAL CELL LINE</p>

    <p>          Huang, YC, Li, Z, Brighton, LE, Carson, JL, Becker, S, and Soukup, JM</p>

    <p>          Am J Physiol Lung Cell Mol Physiol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653711&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653711&amp;dopt=abstract</a> </p><br />

    <p>3.     56057   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Amino substituted derivatives of 5&#39;-amino-5&#39;-deoxy-5&#39;-noraristeromycin</p>

    <p>          Yang, M and Schneller, SW</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(3): 877-882</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653353&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15653353&amp;dopt=abstract</a> </p><br />

    <p>4.     56058   DMID-LS-84; EMBASE-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Efficacy of a 24-week course of PEG-interferon [alpha]-2b monotherapy in patients with acute hepatitis C after failure of spontaneous clearance</p>

    <p>          Santantonio, Teresa, Fasano, Massimo, Sinisi, Emanuele, Guastadisegni, Angela, Casalino, Caterina, Mazzola, Michele, Francavilla, Ruggiero, and Pastore, Giuseppe</p>

    <p>          Journal of Hepatology <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4DWGPYJ-1/2/a718fe963fe0287bd356e8aa282e8134">http://www.sciencedirect.com/science/article/B6W7C-4DWGPYJ-1/2/a718fe963fe0287bd356e8aa282e8134</a> </p><br />

    <p>5.     56059   DMID-LS-84; EMBASE-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and anti-viral activity of a series of D- and L-2&#39;-deoxy-2&#39;-fluororibonucleosides in the subgenomic HCV replicon system</p>

    <p>          Shi, Junxing, Du, Jinfa, Ma, Tianwei, Pankiewicz, Krzysztof W, Patterson, Steven E, Tharnish, Phillip M, McBrayer, Tamara R, Stuyver, Lieven J, Otto, Michael J, and Chu, Chung K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4F7B3YX-2/2/910ab35de4553066eb91451c41c0889e">http://www.sciencedirect.com/science/article/B6TF8-4F7B3YX-2/2/910ab35de4553066eb91451c41c0889e</a> </p><br />
    <br clear="all">

    <p>6.     56060   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virological effects of ISIS 14803, an antisense oligonucleotide inhibitor of hepatitis C virus (HCV) internal ribosome entry site (IRES), on HCV IRES in chronic hepatitis C patients and examination of the potential role of primary and secondary HCV resistance in the outcome of treatment</p>

    <p>          Soler, M, McHutchison, JG, Kwoh, TJ, Dorr, FA, and Pawlotsky, JM</p>

    <p>          Antivir Ther <b>2004</b>.  9(6): 953-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15651754&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15651754&amp;dopt=abstract</a> </p><br />

    <p>7.     56061   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nitric oxide inhibits the replication cycle of severe acute respiratory syndrome coronavirus</p>

    <p>          Akerstrom, S, Mousavi-Jazi, M, Klingstrom, J, Leijon, M, Lundkvist, A, and Mirazimi, A</p>

    <p>          J Virol <b>2005</b>.  79(3): 1966-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650225&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650225&amp;dopt=abstract</a> </p><br />

    <p>8.     56062   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Predominant Mechanism by Which Ribavirin Exerts Its Antiviral Activity In Vitro against Flaviviruses and Paramyxoviruses Is Mediated by Inhibition of IMP Dehydrogenase</p>

    <p>          Leyssen, P, Balzarini, J, De Clercq, E , and Neyts, J</p>

    <p>          J Virol <b>2005</b> .  79(3): 1943-1947</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650220&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650220&amp;dopt=abstract</a> </p><br />

    <p>9.     56063   DMID-LS-84; EMBASE-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification and characterization of nonsubstrate based inhibitors of the essential dengue and West Nile virus proteases</p>

    <p>          Ganesh, Vannakambadi K, Muller, Nik, Judge, Ken, Luan, Chi-Hao, Padmanabhan, Radhakrishnan, and Murthy, Krishna HM</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(1): 257-264</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4DS7SYD-1/2/27a8d340e4f5deb54e79c13bae5036d3">http://www.sciencedirect.com/science/article/B6TF8-4DS7SYD-1/2/27a8d340e4f5deb54e79c13bae5036d3</a> </p><br />

    <p>10.   56064   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          West nile virus inhibits the signal transduction pathway of alpha interferon</p>

    <p>          Guo, JT, Hayashi, J, and Seeger, C</p>

    <p>          J Virol <b>2005</b>.  79(3): 1343-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650160&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650160&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   56065   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Compounds From Traditional Chinese Medicines Galla Chinese as Inhibitors of Hcvns3 Protease</p>

    <p>          Duan, DL, Li, ZQ, Luo, HP, Zhang, W, Chen, LR, and Xu, XJ</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(24): 6041-6044</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225522700019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225522700019</a> </p><br />

    <p>12.   56066   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Bicyclic nucleoside inhibitors of varicella-zoster virus modified on the sugar moiety: 3&#39; and 5&#39; derivatives</p>

    <p>          Luoni, GM, McGuigan, C, Andrei, G, Snoeck, R, De Clercq, E, and Balzarini, J</p>

    <p>          Antivir Chem Chemother <b>2004</b>.  15(6): 333-341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646647&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646647&amp;dopt=abstract</a> </p><br />

    <p>13.   56067   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Non-nucleoside structures retain full anti-HCMV potency of the dideoxy furanopyrimidine family</p>

    <p>          Bidet, O, McGuigan, C, Snoeck, R, Andrei, G, De, Clercq E, and Balzarini, J</p>

    <p>          Antivir Chem Chemother <b>2004</b>.  15(6): 329-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646646&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646646&amp;dopt=abstract</a> </p><br />

    <p>14.   56068   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inactivation of Dengue Virus by Methylene Blue/Narrow Bandwidth Light System</p>

    <p>          Huang, Q, Fu, WL, Chen, B, Huang, JF, Zhang, X, and Xue, Q</p>

    <p>          Journal of Photochemistry and Photobiology B-Biology <b>2004</b>.  77(1-3): 39-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225342700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225342700004</a> </p><br />

    <p>15.   56069   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          West Nile virus: recent experience with the model virus approach</p>

    <p>          Kreil, TR</p>

    <p>          Dev Biol (Basel) <b>2004</b>.  118: 101-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15645678&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15645678&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   56070   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p><b>          Longer Treatment Duration With Peginterferon Alfa-2a (40kd) (Pegasys (R)) and Ribavirin (Copegus (R)) in Naive Patients With Chronic Hepatitis C and Detectable Hcv Rna by Week 4 of Therapy: Final Results of the Randomized, Multicenter Teravic-4 Study</b> </p>

    <p>          Sanchez-Tapias, JM, Diago, M, Escartin, P, Enriquez, J, Moreno, R, Romero-Gomez, M, Barcena, R, Crespo, J, Andrade, R, Perez, R, Suarez, MJ, Planas, R, Sola, R, Garcia-Bengoechea, M, Garcia-Samaniego, J, Berenguer, J, Berenguer, J, Montoro, M, Del Olmo, J, Del Olmo, J, Lopez, G, Tome, S, Casanovas, T, Castellanos, G, and Castro, A</p>

    <p>          Hepatology <b>2004</b>.  40(4): 218A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100128">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100128</a> </p><br />

    <p>        </p>

    <p>17.   56071   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacokinetic-Pharmacodynamic Relationships of Merimepodib and Ribavirin in Pegylated Interferon-Alfa/Ribavirin/Merimepodib Treated Genotype-1 Hcv Patients Non-Responsive to Previous Therapy With Interferon-Alfa/Ribavirin</p>

    <p>          Zha, JH, Garg, V, Mcnair, L, Marcellin, P, Alam, J, Purdy, S, and Ette, E</p>

    <p>          Hepatology <b>2004</b>.  40(4): 250A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100195">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100195</a> </p><br />

    <p>18.   56072   DMID-LS-84; EMBASE-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Prophylaxis of acute respiratory virus infections using nucleic acid-based drugs</p>

    <p>          Wong, Jonathan P, Nagata, Les P, Christopher, Mary E, Salazar, Andres M, and Dale, Roderic MK</p>

    <p>          Vaccine <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4F82P0S-4/2/13885a8ded7f7ace4bf8f2de7353652f">http://www.sciencedirect.com/science/article/B6TD4-4F82P0S-4/2/13885a8ded7f7ace4bf8f2de7353652f</a> </p><br />

    <p>19.   56073   DMID-LS-84; EMBASE-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          5&#39;-Noraristeromycin derivatives isomeric to aristeromycin and 2&#39;-deoxyaristeromycin</p>

    <p>          Yin, Xue-qiang and Schneller, Stewart W</p>

    <p>          Tetrahedron <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4F7H5FY-2/2/b999e03e49bc777e947a4905c841d853">http://www.sciencedirect.com/science/article/B6THR-4F7H5FY-2/2/b999e03e49bc777e947a4905c841d853</a> </p><br />

    <p>20.   56074   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of chinonin against herpes simplex virus</p>

    <p>          Jiang, J, Li, S, Li, M, and Xiang, J</p>

    <p>          J Huazhong Univ Sci Technolog Med Sci <b>2004</b>.  24(5): 521-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15641710&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15641710&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   56075   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Randomized, Double-Blind Study Comparing Adefovir Dipivoxil (Adv) Plus Emtricitabine (Ftc) Combination Therapy Versus Adv Alone in Hbeag(+) Chronic Hepatitis B: Efficacy and Mechanisms of Treatment Response</p>

    <p>          Lau, G, Cooksley, H, Ribeiro, RM, Powers, KA, Bowden, S, Mommeja-Marin, H, Mondou, E, Lewin, S, Rousseau, F, Perelson, AS, Locarnini, S, and Naoumov, NV</p>

    <p>          Hepatology <b>2004</b>.  40(4): 272A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100247">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100247</a> </p><br />

    <p>22.   56076   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Highly Specific Carbohydrate-Binding Protein Cyanovirin-N: Structure, Anti-HIV/Ebola Activity and Possibilities for Therapy</p>

    <p>          Barrientos, LG and Gronenborn, AM</p>

    <p>          Mini Rev Med Chem <b>2005</b>.  5(1): 21-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15638789&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15638789&amp;dopt=abstract</a> </p><br />

    <p>23.   56077   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-HBV effect of liposome-encapsulated matrine in vitro and in vivo</p>

    <p>          Li, CQ, Zhu, YT, Zhang, FX, Fu, LC, Li, XH, Cheng, Y, and Li, XY</p>

    <p>          World J Gastroenterol <b>2005</b>.  11(3): 426-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15637760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15637760&amp;dopt=abstract</a> </p><br />

    <p>24.   56078   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Iron inactivates the RNA polymerase NS5B and suppresses subgenomic replication of hepatitis C virus</p>

    <p>          Fillebeen, C, Rivas-Estilla, AM, Bisaillon, M, Ponka, P, Muckenthaler, M, Hentze, MW, Koromilas, AE, and Pantopoulos, K</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15637067&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15637067&amp;dopt=abstract</a> </p><br />

    <p>25.   56079   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of the volatile oils of Melissa officinalis L. against Herpes simplex virus type-2</p>

    <p>          Allahverdiyev, A, Duran, N, Ozguven, M, and Koltas, S</p>

    <p>          Phytomedicine <b>2004</b>.  11(7-8): 657-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15636181&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15636181&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>26.   56080   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Pilot Trial of Thymalfasin (Thymosin Alpha-1) in Combination With Peginterferon Alpha-2a (Peg-Ifn2a) and Ribavirin in Hcv Non-Responders: 48-Week (End of Therapy) Results</p>

    <p>          Poo, JL, Sanchez-Avila, F, Kershenobich, D, Samper, XG, Gongora, J, Sandoval, MG, Oest, JA, Martins, EB, and Uribe, M</p>

    <p>          Hepatology <b>2004</b>.  40(4): 336A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100398">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100398</a> </p><br />

    <p>27.   56081   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Molecular Mechanism Susceptibility Profile of Biln-2061 to Various Hepatitis C Virus Genotypes and Ns3-Ns4a Protease Inhibitor Mutation: D168a and D168v.</p>

    <p>          Courcambeck, J, Perbost, R, Chabaud, P, Pepe, G, Bouzidi, M, Mabrouk, K, Sabatier, JM, and Halfon, P</p>

    <p>          Hepatology <b>2004</b>.  40(4): 385A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100515">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100515</a> </p><br />

    <p>28.   56082   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Modelling of anti-HSV activity of lactoferricin analogues using amino acid descriptors</p>

    <p>          Jenssen, H, Gutteberg, TJ, and Lejon, T</p>

    <p>          J Pept Sci <b>2004</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15635641&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15635641&amp;dopt=abstract</a> </p><br />

    <p>29.   56083   DMID-LS-84; EMBASE-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and SAR of novel 4,5-diarylimidazolines as potent P2X7 receptor antagonists</p>

    <p>          Merriman, Gregory H, Ma, Liang, Shum, Patrick, McGarry, Daniel, Volz, Frank, Sabol, Jeffrey S, Gross, Alexandre, Zhao, Zhicheng, Rampe, David, and Wang, Lin</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(2): 435-438</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DTBSVY-1/2/d4a21cdf4113b8a4e0d66458a5c6b4f3">http://www.sciencedirect.com/science/article/B6TF9-4DTBSVY-1/2/d4a21cdf4113b8a4e0d66458a5c6b4f3</a> </p><br />

    <p>30.   56084   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Novel Animal Model to Assess Hcv Ns3 Center Dot 4a Protease Inhibitors Validated Using Vx-950 and Biln 2061</p>

    <p>          Kalkeri, G, Almquist, S, Perni, R, and Kwong, A</p>

    <p>          Hepatology <b>2004</b>.  40(4): 402A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100548">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100548</a> </p><br />
    <br clear="all">

    <p>31.   56085   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nucleotides and Pronucleotides of 2,2-Bis(hydroxymethyl)methylenecyclopropane Analogues of Purine Nucleosides: Synthesis and Antiviral Activity</p>

    <p>          Yan, Z, Kern, ER, Gullen, E, Cheng, YC, Drach, JC, and Zemlicka, J</p>

    <p>          J Med Chem <b>2005</b>.  48(1): 91-99</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634003&amp;dopt=abstract</a> </p><br />

    <p>32.   56086   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Resistance Mutations Against Vx-950 and Biln 2061, Two Hcv Protease Inhibitor Clinical Candidates: Single-Resistance, Cross-Resistance, and Fitness</p>

    <p>          Lin, C, Rao, BG, Luong, YP, Fulghum, JR, Brennan, DL, Wei, YY, Frantz, JD, Lippke, J, Hsiao, HM, Ma, S, Lin, K, Maxwell, JP, Cottrell, KM, Gates, CA, Perni, RB, and Kwong, AD</p>

    <p>          Hepatology <b>2004</b>.  40(4): 404A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100554">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100554</a> </p><br />

    <p>33.   56087   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          High-Throughput Screening of Anti-Hcv Drug Candidates Using an Elisa-Based Hcv Replicon Assay</p>

    <p>          Tan, H, Hong, J, Seiwert, S, and Blatt, LM</p>

    <p>          Hepatology <b>2004</b>.  40(4): 407A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100560">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100560</a> </p><br />

    <p>34.   56088   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Safety, Tolerance, Pharmacokinetics and Pharmacodynamics of Remofovir, a Livertargeting Prodrug of Pmea in Hbv Patients Following Daily Dosing for 28 Days.</p>

    <p>          Lin, CC, Chao, YC, Lai, MY, Chang, TT, Chuang, WL, Yang, SS, Braeckman, R, and Chen, DS</p>

    <p>          Hepatology <b>2004</b>.  40(4): 658A-659A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101143">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101143</a> </p><br />

    <p>35.   56089   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Molecular advances in severe acute respiratory syndrome-associated coronavirus (SARS-CoV)</p>

    <p>          Chow, KY, Hon, CC, Hui, RK, Wong, RT, Yip, CW, Zeng, F, and Leung, FC</p>

    <p>          Genomics Proteomics Bioinformatics <b>2003</b>.  1(4): 247-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629054&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629054&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>36.   56090   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phosphoramidate protides of carbocyclic 2&#39;,3&#39;-dideoxy-2&#39;,3&#39;-didehydro-7-deazaadenosine with potent activity against HIV and HBV</p>

    <p>          Gudmundsson, KS, Wang, Z, Daluge, SM, Johnson, LC, Hazen, R, Condreay, LD, and McGuigan, C</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2004</b>.  23(12): 1929-37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15628749&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15628749&amp;dopt=abstract</a> </p><br />

    <p>37.   56091   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Selection and Characterization of Hepatitis C Virus Replicons Resistant to Potent Ns5b Polymerase Inhibitors.</p>

    <p>          Lu, LJ, Mo, HM, Pilot-Matias, T, Dekhtyar, T, Ng, T, Pithawalla, R, Masse, S, Pratt, J, Donner, P, Maring, C, and Molla, A</p>

    <p>          Hepatology <b>2004</b>.  40(4): 694A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101219">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101219</a> </p><br />

    <p>38.   56092   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A novel ex vivo model system for evaluation of conditionally replicative adenoviruses therapeutic efficacy and toxicity</p>

    <p>          Kirby, TO, Rivera, A, Rein, D, Wang, M, Ulasov, I, Breidenbach, M, Kataram, M, Contreras, JL, Krumdieck, C, Yamamoto, M, Rots, MG, Haisma, HJ, Alvarez, RD, Mahasreshti, PJ, and Curiel, DT</p>

    <p>          Clin Cancer Res <b>2004</b>.  10(24): 8697-703</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15623655&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15623655&amp;dopt=abstract</a> </p><br />

    <p>39.   56093   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Activity of New 4-(Phenylamino)Thieno[2,3-B]Pyridine Derivatives</p>

    <p>          Bernardino, AMR, Pinheiro, LCS, Ferreira, VF, Azevedo, AR, Carneiro, JWD, Souza, TML, and Frugulhetti Icpp</p>

    <p>          Heterocyclic Communications <b>2004</b>.  10(6): 407-410</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225696100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225696100007</a> </p><br />

    <p>40.   56094   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Isolation and Characterization of Herpes Simplex Virus Type 1 Resistant to Arninothiazolylphenyl-Based Inhibitors of the Viral Helicase-Primase</p>

    <p>          Liuzzi, M, Kibler, P, Bousquet, C, Harji, F, Bolger, G, Garneau, M, Lapeyre, N, Mccollum, RS, Faucher, AM, Simoneau, B, and Cordingley, MG</p>

    <p>          Antiviral Research <b>2004</b>.  64(3): 161-170</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225626000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225626000002</a> </p><br />
    <br clear="all">

    <p>41.   56095   DMID-LS-84; PUBMED-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of human papillomavirus type 16 E7 phosphorylation by the S100 MRP-8/14 protein complex</p>

    <p>          Tugizov, S, Berline, J, Herrera, R, Penaranda, ME, Nakagawa, M, and Palefsky, J</p>

    <p>          J Virol <b>2005</b>.  79(2): 1099-112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613338&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613338&amp;dopt=abstract</a> </p><br />

    <p>42.   56096   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral and Antiplasmodial Spirodihydrobenzofuran Terpenes From the Fungus Stachybotrys Nephrospora</p>

    <p>          Sawadjoon, S,  Kittakoop, P, Isaka, M, Kirtikara, K, Madla, S, and Thebtaranonth, Y</p>

    <p>          Planta Medica <b>2004</b>.  70(11): 1085-1087</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225506000014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225506000014</a> </p><br />

    <p>43.   56097   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Parainfluenza Virus Type 3 and Newcastle Disease Virus Hemagglutinin-Neuraminidase Receptor Binding: Effect of Receptor Avidity and Steric Hindrance at the Inhibitor Binding Sites</p>

    <p>          Porotto, M, Murrell, M, Greengard, O, Lawrence, MC, Mckimm-Breschkin, JL, and Moscona, A</p>

    <p>          Journal of Virology <b>2004</b>.  78(24): 13911-13919</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409900050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409900050</a> </p><br />

    <p>44.   56098   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cooperation Between Different Forms of the Human Papillomavirus Type 1 E4 Protein to Block Cell Cycle Progression and Cellular Dna Synthesis</p>

    <p>          Knight, GL, Grainger, JR, Gallimore, PH, and Roberts, S</p>

    <p>          Journal of Virology <b>2004</b>.  78(24): 13920-13933</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409900051">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409900051</a> </p><br />

    <p>45.   56099   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Genetic Screen for Monitoring Severe Acute Respiratory Syndrome Coronavirus 3c-Like Protease</p>

    <p>          Parera, M, Clotet, B, and Martinez, MA</p>

    <p>          Journal of Virology <b>2004</b>.  78(24): 14057-14061</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409900066">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409900066</a> </p><br />

    <p>46.   56100   DMID-LS-84; WOS-DMID-1/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Amantadine Has Direct Inhibitory Effects Against Hepatitis a Internal Ribosomal Entry Site-Mediated Translation.</p>

    <p>          Kanda, T, Yokosuka, O, Imazeki, F, Fujiwara, K, Saisho, H, and Nagao, K</p>

    <p>          Hepatology <b>2004</b>.  40(4): 286A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>        <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100280">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100280</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
