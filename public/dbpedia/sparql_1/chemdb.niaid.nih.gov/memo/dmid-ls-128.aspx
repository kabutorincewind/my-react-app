

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-128.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="F11ZCSNoF9xm95v7YMkgOKg8oU5DPuQ3t0UOTJxK8xk+506cZeSz72mKkDnoRRhCWXdu6IT4DwQs2EKJODqKFoCbTdSCKfYsRZwk/hRwPlLFBhNJdN6REh1xNxM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C23109E9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-128-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59022   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Synthesis of thiazolone-based sulfonamides as inhibitors of HCV NS5B polymerase</p>

    <p>          Ding, Y, Smith, KL, Varaprasad, CV, Chang, E, Alexander, J, and Yao, N</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16990004&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16990004&amp;dopt=abstract</a> </p><br />

    <p>2.     59023   DMID-LS-128; EMBASE-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Crimean-Congo haemorrhagic fever</p>

    <p>          Ergonul, Onder </p>

    <p>          The Lancet Infectious Diseases <b>2006</b>.  6(4): 203-214</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W8X-4JHV0JG-N/2/186ef898b85db7a0f7622240aafd79c4">http://www.sciencedirect.com/science/article/B6W8X-4JHV0JG-N/2/186ef898b85db7a0f7622240aafd79c4</a> </p><br />

    <p>3.     59024   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Mechanistic study of HCV polymerase inhibitors at individual steps of the polymerization reaction</p>

    <p>          Liu, Y, Jiang, WW, Pratt, J, Rockway, T, Harris, K, Vasavanonda, S, Tripathi, R, Pithawalla, R, and Kati, WM</p>

    <p>          Biochemistry <b>2006</b>.  45(38): 11312-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16981691&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16981691&amp;dopt=abstract</a> </p><br />

    <p>4.     59025   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of a new series of substituted acyl(thio)urea and thiadiazolo [2,3-a] pyrimidine derivatives as potent inhibitors of influenza virus neuraminidase</p>

    <p>          Sun, C, Zhang, X, Huang, H, and Zhou, P</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16979342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16979342&amp;dopt=abstract</a> </p><br />

    <p>5.     59026   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Antiviral properties of immunosuppressant drugs in primary and latent bk virus infection in kidney cells</p>

    <p>          Anon</p>

    <p>          Transplantation <b>2006</b>.  82(1 Suppl 2): 683</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16976077&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16976077&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59027   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          An Antiviral Peptide Inhibitor That Is Active against Picornavirus 2A Proteinases but Not Cellular Caspases</p>

    <p>          Deszcz, L, Cencic, R, Sousa, C, Kuechler, E, and Skern, T</p>

    <p>          J Virol <b>2006</b>.  80(19): 9619-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16973565&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16973565&amp;dopt=abstract</a> </p><br />

    <p>7.     59028   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          The NS1 Gene Contributes to the Virulence of H5N1 Avian Influenza Viruses</p>

    <p>          Li, Z, Jiang, Y, Jiao, P, Wang, A, Zhao, F, Tian, G, Wang, X, Yu, K, Bu, Z, and Chen, H</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16971424&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16971424&amp;dopt=abstract</a> </p><br />

    <p>8.     59029   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Expressional screening of interferon-stimulated genes for antiviral activity against hepatitis C virus replication</p>

    <p>          Itsui, Y, Sakamoto, N, Kurosaki, M, Kanazawa, N, Tanabe, Y, Koyama, T, Takeda, Y, Nakagawa, M, Kakinuma, S, Sekine, Y, Maekawa, S, Enomoto, N, and Watanabe, M</p>

    <p>          J Viral Hepat <b>2006</b>.  13(10): 690-700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16970601&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16970601&amp;dopt=abstract</a> </p><br />

    <p>9.     59030   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          SARS: Systematic Review of Treatment Effects</p>

    <p>          Stockman, LJ, Bellamy, R, and Garner, P</p>

    <p>          PLoS Med <b>2006</b>.  3(9)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16968120&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16968120&amp;dopt=abstract</a> </p><br />

    <p>10.   59031   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Inhibition of Multiple Subtypes of Influenza A Virus in Cell Cultures with Morpholino Oligomers</p>

    <p>          Ge, Q, Pastey, M, Kobasa, D, Puthavathana, P, Lupfer, C, Bestwick, RK, Iversen, PL, Chen, J, and Stein, DA</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16966399&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16966399&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59032   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Antiviral action of nitric oxide on dengue virus type 2 replication</p>

    <p>          Takhampunya, R, Padmanabhan, R, and Ubol, S</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 10): 3003-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16963759&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16963759&amp;dopt=abstract</a> </p><br />

    <p>12.   59033   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Maribavir sensitivity of cytomegalovirus isolates resistant to ganciclovir, cidofovir or foscarnet</p>

    <p>          Drew, WL, Miner, RC, Marousek, GI, and Chou, S</p>

    <p>          J Clin Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16962820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16962820&amp;dopt=abstract</a> </p><br />

    <p>13.   59034   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Drug evaluation: LB-80380--a novel phosphonate nucleoside for the potential treatment of HBV infection</p>

    <p>          Papatheodoridis, GV and Manolakopoulos, S</p>

    <p>          Curr Opin Mol Ther <b>2006</b>.  8(4): 352-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955699&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955699&amp;dopt=abstract</a> </p><br />

    <p>14.   59035   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Drug evaluation: Albuferon-alpha--an antiviral interferon-alpha/albumin fusion protein</p>

    <p>          Chemmanur, AT and Wu, GY</p>

    <p>          Curr Opin Investig Drugs <b>2006</b>.  7(8): 750-758</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955687&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955687&amp;dopt=abstract</a> </p><br />

    <p>15.   59036   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Interactions between respiratory syncytial virus and the host cell: opportunities for antivirus strategies?</p>

    <p>          Sugrue, RJ</p>

    <p>          Expert Rev Mol Med <b>2006</b>.  8(21): 1-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16953942&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16953942&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   59037   DMID-LS-128; EMBASE-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Design and synthesis of 3,4-dihydro-1H-[1]-benzothieno[2,3-c]pyran and 3,4-dihydro-1H-pyrano[3,4-b]benzofuran derivatives as non-nucleoside inhibitors of HCV NS5B RNA dependent RNA polymerase</p>

    <p>          Gopalsamy, Ariamala, Aplasca, Alexis, Ciszewski, Gregory, Park, Kaapjoo, Ellingboe, John W, Orlowski, Mark, Feld, Boris, and Howe, Anita YM</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(2): 457-460</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HH81CX-1/2/9fbdeb8788a8fc87d215afa48f711641">http://www.sciencedirect.com/science/article/B6TF9-4HH81CX-1/2/9fbdeb8788a8fc87d215afa48f711641</a> </p><br />

    <p>17.   59038   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Development of Models and Detection Methods for Different Forms of Cytomegalovirus for the Evaluation of Viral Inactivation Agents</p>

    <p>          Jayarama, V. <i>et al.</i></p>

    <p>          Transfusion <b>2006</b>.  46(9): 1580-1588</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240147000020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240147000020</a> </p><br />

    <p>18.   59039   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Synthesis and Reactions of Some New Pyrrolylfuro[2,3-D]Pyrimidines and Pyrrolopyrazinofuropyrimidines</p>

    <p>          Abdel-Mohsen, S., Abdel-Hafez, S., and Geies, A.</p>

    <p>          Journal of the Chinese Chemical Society <b>2006</b>.  53(4): 909-914</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240278300021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240278300021</a> </p><br />

    <p>19.   59040   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          An Episulfide Cation (Thiiranium Ring) Trapped in the Active Site of Hav 3c Proteinase Inactivated by Peptide-Based Ketone Inhibitors</p>

    <p>          Yin, J. <i>et al.</i></p>

    <p>          Journal of Molecular Biology <b>2006</b>.  361(4): 673-686</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240233700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240233700006</a> </p><br />

    <p>20.   59041   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Potential Therapies for Coronaviruses</p>

    <p>          Savarino, A. <i>et al.</i></p>

    <p>          Expert Opinion on Therapeutic Patents <b>2006</b>.  16(9): 1269-1288</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240372300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240372300006</a> </p><br />

    <p>21.   59042   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Focus on new antiviral drugs for AIDS and smallpox treatments</p>

    <p>          Borchardt, JK</p>

    <p>          Drug News Perspect <b>2006</b>.  19(5): 307-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16941053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16941053&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   59043   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Lactoferrin Inhibits Human Bk Polyomavirus Adsorption to Vero Cells</p>

    <p>          Degener, A. <i>et al.</i></p>

    <p>          Biochemistry and Cell Biology-Biochimie Et Biologie Cellulaire <b>2006</b>.  84(3): 389</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240052000027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240052000027</a> </p><br />

    <p>23.   59044   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Avian Influenza: Preparing for a Pandemic</p>

    <p>          Juckett, G.</p>

    <p>          American Family Physician <b>2006</b>.  74(5): 783-790</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240324000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240324000010</a> </p><br />

    <p>24.   59045   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis B Virus Dna Replicative Intermediate Forms by Recombinant Interferon-Gamma</p>

    <p>          Parvez, M. <i>et al.</i></p>

    <p>          World Journal of Gastroenterology <b>2006</b>.  12(19): 3006-3014</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239997000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239997000005</a> </p><br />

    <p>25.   59046   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          The Structure of H5n1 Avian Influenza Neuraminidase Suggests New Opportunities for Drug Design</p>

    <p>          Russell, R. <i>et al.</i></p>

    <p>          Nature <b>2006</b>.  443(7107): 45-49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240313900035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240313900035</a> </p><br />

    <p>26.   59047   DMID-LS-128; PUBMED-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Respiratory syncytial virus: disease, development and treatment</p>

    <p>          Banning, M</p>

    <p>          Br J Nurs <b>2006</b>.  15(14): 751-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16936612&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16936612&amp;dopt=abstract</a> </p><br />

    <p>27.   59048   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Avian Influenza in Humans - First Annual Conference - Latest Advances on Prevention, Therapies and Protective Measures - 29-30 June 2006, Paris, France</p>

    <p>          Kotwal, G.</p>

    <p>          Idrugs <b>2006</b>.  9(9): 625-626</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240048700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240048700010</a> </p><br />

    <p>28.   59049   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Hepatitis C Virus Ns3-4a Protease Inhibitors: Countering Viral Subversion in Vitro and Showing Promise in the Clinic</p>

    <p>          Thomson, J. and Perni, R.</p>

    <p>          Current Opinion in Drug Discovery &amp; Development <b>2006</b>.  9(5 ): 606-617</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240047900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240047900008</a> </p><br />

    <p>29.   59050   DMID-LS-128; EMBASE-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxynucleosides having variations at either or both of the 2&#39;- and 3&#39;-positions</p>

    <p>          Len, Christophe and Mackenzie, Grahame</p>

    <p>          Tetrahedron <b>2006</b>.  62(39): 9085-9107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4KJTNC5-B/2/9abf885667e19396e85fbc49674b13f9">http://www.sciencedirect.com/science/article/B6THR-4KJTNC5-B/2/9abf885667e19396e85fbc49674b13f9</a> </p><br />

    <p>30.   59051   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Quinolizidine Alkaloids With Anti-Hbv Activity From Sophora Tonkinensis</p>

    <p>          Ding, P. <i>et al.</i></p>

    <p>          Planta Medica <b>2006</b>.  72(9): 854-856</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239926000015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239926000015</a> </p><br />

    <p>31.   59052   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          A Novel and Simple Method for Construction of Recombinant Adenoviruses</p>

    <p>          Tan, R. <i>et al.</i></p>

    <p>          Nucleic Acids Research <b>2006</b>.  34(12)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239908000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239908000007</a> </p><br />

    <p>32.   59053   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Structural Basis for Specificity in the Poxvirus Topoisomerase</p>

    <p>          Perry, K. <i>et al.</i></p>

    <p>          Molecular Cell  <b>2006</b>.  23(3): 343-354</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239805700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239805700006</a> </p><br />

    <p>33.   59054   DMID-LS-128; WOS-DMID-9/25/2006</p>

    <p class="memofmt1-2">          Nonpeptide Inhibitors of Measles Virus Entry</p>

    <p>          Sun, A. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(17): 5080-5092</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239818200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239818200006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
