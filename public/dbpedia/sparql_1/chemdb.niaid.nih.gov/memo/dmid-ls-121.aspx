

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-121.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="26GS1Q4TWLdDS15uDwyqIpUzvrZN9iT3cX9DvqGKdj3KE73U2YZmvc8oh48umkTVB/pAx+0YeM39rMpwul/iC7/RlvzLKSEoet9X9l2TOCQpHn7rsno4DYzc+ig=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="45997DEE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-121-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58536   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-virus activity of recombinant adenoviral vector HBV-TRL</p>

    <p>          Gong, Weidong, Yi, Jun, Zhao, Ya, Liu, Jun, Ding, Jin, and Xue, Caifang</p>

    <p>          Disi Junyi Daxue Xuebao <b>2005</b>.  26(15): 1345-1348</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     58537   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure-guided design of a novel class of benzyl-sulphonate inhibitors for influenza virus neuraminidase</p>

    <p>          Platis, D, Smith, BJ, Huyton, T, and Labrou, NE</p>

    <p>          Biochem J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16776653&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16776653&amp;dopt=abstract</a> </p><br />

    <p>3.     58538   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro antiviral activity of marine sponges collected off brazilian coast</p>

    <p>          Cordeiro da Silva, Alexandre, Kratz, Jadel Muller, Farias, Fabiane Maria, Henriques, Amelia Terezinha, dos Santos, Josivete, Leonel, Rosa Maria, Lerner, Clea, Mothes, Beatriz, Barardi, Celia Regina Monte, and Simoes, Claudia Maria Oliveira</p>

    <p>          Biological &amp; Pharmaceutical Bulletin <b>2006</b>.  29(1): 135-140</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     58539   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Lactoferrin inhibits early steps of human BK polyomavirus infection</p>

    <p>          Longhi, G, Pietropaolo, V, Mischitelli, M, Longhi, C, Conte, MP, Marchetti, M, Tinari, A, Valenti, P, Degener, AM, Seganti, L, and Superti, F</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16774792&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16774792&amp;dopt=abstract</a> </p><br />

    <p>5.     58540   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The synergistic effect of IFN-alpha and IFN-gamma against HSV-2 replication in Vero cells is not interfered by the plant antiviral 1-cinnamoyl-3, 11-dihydroxymeliacarpin</p>

    <p>          Petrera, E and Coto, CE</p>

    <p>          Virol J <b>2006</b>.  3(1): 45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16772029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16772029&amp;dopt=abstract</a> </p><br />

    <p>6.     58541   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-viral treatment methods using phosphatidylethanolamine-binding peptide derivatives</p>

    <p>          Thorpe, Philip E, Soares, MMelina, and He, Jin</p>

    <p>          PATENT:  US <b>2004214764</b>  ISSUE DATE:  20041028</p>

    <p>          APPLICATION: 2003-52293  PP: 181 pp., Cont.-in-part of U.S. Ser. No. 621,269.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     58542   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Exotic emerging viral diseases: Progress and challenges</p>

    <p>          Geisbert, Thomas W and Jahrling, Peter B</p>

    <p>          Nature Medicine (New York, NY, United States) <b>2004</b>.  10(12, Suppl.): S110-S121</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     58543   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Influenza virus proton channels</p>

    <p>          Pinto, LH and Lamb, RA</p>

    <p>          Photochem Photobiol Sci <b>2006</b>.  5(6): 629-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16761092&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16761092&amp;dopt=abstract</a> </p><br />

    <p>9.     58544   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of dengue virus replication by mycophenolic acid and ribavirin</p>

    <p>          Takhampunya, R, Ubol, S, Houng, HS, Cameron, CE, and Padmanabhan, R</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 7): 1947-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16760396&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16760396&amp;dopt=abstract</a> </p><br />

    <p>10.   58545   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Is there an ideal animal model for SARS?</p>

    <p>          Subbarao, K and Roberts, A</p>

    <p>          Trends Microbiol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759866&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759866&amp;dopt=abstract</a> </p><br />

    <p>11.   58546   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Assembling a smallpox biodefense by interrogating 5-substituted pyrimidine nucleoside chemical space</p>

    <p>          Fan, X, Zhang, X, Zhou, L, Keith, KA, Kern, ER, and Torrence, PF</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759713&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759713&amp;dopt=abstract</a> </p><br />

    <p>12.   58547   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of Various Pyrimidines Possessing the 1-[(2-Hydroxy-1-(hydroxymethyl)ethoxy)methyl] Moiety, Able To Mimic Natural 2&#39;-Deoxyribose, on Wild-type and Mutant Hepatitis B Virus Replication</p>

    <p>          Kumar, R, Semaine, W, Johar, M, Tyrrell, DL, and Agrawal, B</p>

    <p>          J Med Chem <b>2006</b>.  49(12): 3693-3700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759112&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759112&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.   58548   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of a Novel Family of SARS-CoV Protease Inhibitors by Virtual Screening and 3D-QSAR Studies</p>

    <p>          Tsai, KC, Chen, SY, Liang, PH, Lu, IL, Mahindroo, N, Hsieh, HP, Chao, YS, Liu, L, Liu, D, Lien, W, Lin, TH, and Wu, SY</p>

    <p>          J Med Chem <b>2006</b>.  49(12): 3485-3495</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759091&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759091&amp;dopt=abstract</a> </p><br />

    <p>14.   58549   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Isatin Compounds as Noncovalent SARS Coronavirus 3C-like Protease Inhibitors</p>

    <p>          Zhou, L, Liu, Y, Zhang, W, Wei, P, Huang, C, Pei, J, Yuan, Y, and Lai, L</p>

    <p>          J Med Chem <b>2006</b>.  49(12): 3440-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759084&amp;dopt=abstract</a> </p><br />

    <p>15.   58550   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An Efficient RNA Aptamer against Human Influenza B Virus Hemagglutinin</p>

    <p>          Gopinath, SC,  Sakamaki, Y, Kawasaki, K , and Kumar, PK</p>

    <p>          J Biochem (Tokyo) <b>2006</b>.  139(5): 837-846</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16751591&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16751591&amp;dopt=abstract</a> </p><br />

    <p>16.   58551   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Bacterial Reporter System for the Evaluation of Antisense Oligodeoxynucleotides Directed against Human Papillomavirus Type 16 (HPV-16)</p>

    <p>          Guapillo, MR,  Marquez, MA, Benitez-Hess, ML, and Alvarez-Salas, LM</p>

    <p>          Arch Med Res <b>2006</b>.  37(5): 584-592</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16740427&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16740427&amp;dopt=abstract</a> </p><br />

    <p>17.   58552   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery and development of compounds with anti-West Nile Virus activity</p>

    <p>          Goodell, John R, Shi, Pei-Yong, and Ferguson, David M</p>

    <p>          37th Great Lakes Regional Meeting of the American Chemical Society Abstracts <b>2006</b>.: GLRM-354</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   58553   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of nucleoside analogs for treating Hepatitis C and other Flaviviridae family viral infections</p>

    <p>          Keicher, Jesse D, Roberts, Christopher D, and Dyatkina, Natalia B</p>

    <p>          PATENT:  US <b>2006111311</b>  ISSUE DATE:  20060525</p>

    <p>          APPLICATION: 2005-18840  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   58554   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C replicon RNA synthesis by b-D-2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methylcytidine: a specific inhibitor of hepatitis C virus replication</p>

    <p>          Stuyver, Lieven J, McBrayer, Tamara R, Tharnish, Phillip M, Clark, Jeremy, Hollecker, Laurent, Lostia, Stefania, Nachman, Tammy, Grier, Jason, Bennett, Matthew A, Xie, Meng-Yu, Schinazi, Raymond F, Morrey, John D, Julander, Justin L, Furman, Phillip A, and Otto, Michael J</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(2): 79-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   58555   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oral administration of exopolysaccharide from Aphanothece halophytica (Chroococcales) significantly inhibits influenza virus (H1N1)-induced pneumonia in mice</p>

    <p>          Zheng, W, Chen, C, Cheng, Q, Wang, Y, and Chu, C</p>

    <p>          Int Immunopharmacol <b>2006</b>.  6(7): 1093-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16714212&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16714212&amp;dopt=abstract</a> </p><br />

    <p>21.   58556   DMID-LS-121; PUBMED-DMID-6/19/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cell line dependency for antiviral activity and in vivo efficacy of N-methanocarbathymidine against orthopoxvirus infections in mice</p>

    <p>          Smee, DF, Wandersee, MK, Bailey, KW, Wong, MH, Chu, CK, Gadthula, S, and Sidwell, RW</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16712967&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16712967&amp;dopt=abstract</a> </p><br />

    <p>22.   58557   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mycophenolic Acid Inhibits Hepatitis C Virus Replication Independent of Guanosine Depletion and Acts in Synergy With Interferon-a</p>

    <p>          Henry, S. <i>et al.</i></p>

    <p>          Liver Transplantation <b>2006</b>.  12(5): C109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237037100471">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237037100471</a> </p><br />

    <p>23.   58558   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A guided tour through the antiviral drug field</p>

    <p>          De Clercq, E</p>

    <p>          Future Virology <b>2006</b>.  1(1): 19-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   58559   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of alkoxyalkyl esters of phosphonopropoxymethyl-guanine and phosphonopropoxymethyl-diaminopurine</p>

    <p>          Ruiz, Jacqueline C, Aldern, Kathy A, Beadle, James R, Hartline, Caroll B, Kern, Earl R, and Hostetler, Karl Y</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(2): 89-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>25.   58560   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory Activity of Cyclohexenyl Chalcone Derivatives and Flavonoids of Fingerroot, Boesenbergia Rotunda (L.), Towards Dengue-2 Virus Ns3 Protease</p>

    <p>          Kiat, T. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(12): 3337-3340</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237763500054">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237763500054</a> </p><br />

    <p>26.   58561   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of One Peptide Which Inhibited Infectivity of Avian Infectious Bronchitis Virus in Vitro</p>

    <p>          Peng, B. <i>et al.</i></p>

    <p>          Science in China Series C-Life Sciences <b>2006</b>.  49(2): 158-163</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237495000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237495000008</a> </p><br />

    <p>27.   58562   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, Reactions, and Antiviral Activity of 1-(1h-Pyrazolo[3,4-B]Pyridin-5-Yl)Ethanone and Pyrido[2 &#39;,3 &#39;: 3,4]Pyrazolo[5,1-C][1,2,4]Triazine Derivatives</p>

    <p>          Attaby, F. <i>et al.</i></p>

    <p>          Phosphorus Sulfur and Silicon and the Related Elements <b>2006</b>.  181(5): 1087-1102</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237605200012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237605200012</a> </p><br />

    <p>28.   58563   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Compositions comprising pyranocoumarin enriched Prickly ash preparations and uses thereof as antiviral agents</p>

    <p>          Bafi-Yeboa, Nana Fredua A, Baker, John, Arnason, John T, and Hudson, Jim A</p>

    <p>          PATENT:  WO <b>2006026853</b>  ISSUE DATE:  20060316</p>

    <p>          APPLICATION: 2005  PP: 104 pp.</p>

    <p>          ASSIGNEE:  (Bioniche Life Sciences Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   58564   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiherpes activity of 5-alkynyl derivatives of 2,-deoxyuridine</p>

    <p>          Andronova, VL, Pchelintseva, AA, Ustinov, AV, Petrunina, AL, Korshun, VA, Skorobogatyi, MV, and Galegov, GA</p>

    <p>          Voprosy Virusologii <b>2006</b>.  51(1): 34-38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   58565   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Application of Stable Hepatitis C Virus (Hcv)-Secreting Human Hepatoma Cell Lines for Antiviral Drug Discovery</p>

    <p>          Luo, G. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200026</a> </p><br />

    <p>31.   58566   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Strategies Against Nipah and Ebola Virus: Exploring Gene Silencing Mechanisms to Identify Potential Antiviral Targets</p>

    <p>          Enterlein, S. <i> et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200034</a> </p><br />

    <p>32.   58567   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recombinant human super-compound interferon with decreased side effects and increased antiviral and antitumor activity for treating viral infection and cancer</p>

    <p>          Wei, Guangwen</p>

    <p>          PATENT:  US <b>2006035327</b>  ISSUE DATE:  20060216</p>

    <p>          APPLICATION: 2005-12277  PP: 72 pp., Cont.-in-part of U.S. Ser. No. 927,975.</p>

    <p>          ASSIGNEE:  (Peop. Rep. China)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   58568   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Multiple Influenza a Subtypes in Cell Culture With Antisense Phosphorodiamidate Morpholino Oligomers</p>

    <p>          Iversen, P. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200062">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200062</a> </p><br />

    <p>34.   58569   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Influenza Virus a and B Production by Rna Interference</p>

    <p>          Saitoh, H., Miyano-Kurosaki, N., and Takaku, H.</p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200065">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200065</a> </p><br />

    <p>35.   58570   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Antiviral Activity of S11, a Natural Herb Extract, Against Influenza Virus Infections in Mice</p>

    <p>          Kwon, J. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200067">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200067</a> </p><br />

    <p>36.   58571   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mammalian genes showing altered levels of expression in reovirus infection and their use in development of therapeutics preventing viral replication</p>

    <p>          Rubin, Donald H</p>

    <p>          PATENT:  WO <b>2006047673</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 143 pp.</p>

    <p>          ASSIGNEE:  (Vanderbilt University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   58572   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cage Compounds as Inhibitors of Arenaviruses Reproduction</p>

    <p>          Klimochkin, Y. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(1): A80-A81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200143">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200143</a> </p><br />

    <p>38.   58573   DMID-LS-121; WOS-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification and Analysis of Fitness of Resistance Mutations Against the Hcv Protease Inhibitor Sch 503034</p>

    <p>          Tong, X. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(2): 28-38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237444300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237444300004</a> </p><br />

    <p>39.   58574   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          T-cell antigen determinants of SARS coronavirus and their applications as therapeutic agents</p>

    <p>          Liu, Gang, Cheng, Guifang, Li, Li, and Yang, Hongzhen</p>

    <p>          PATENT:  CN <b>1746181</b>  ISSUE DATE: 20060315</p>

    <p>          APPLICATION: 1007-4566  PP: 17 pp.</p>

    <p>          ASSIGNEE:  (Institute of Materia Medica, Chinese Academy of Medical Sciences Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   58575   DMID-LS-121; SCIFINDER-DMID-6/20/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation peptides for inhibition of SARS virus infection</p>

    <p>          Fujii, Nobutaka, Otaka, Akira, Yamamoto, Naoki, and Yamamoto, Norio</p>

    <p>          PATENT:  WO <b>2006025536</b>  ISSUE DATE:  20060309</p>

    <p>          APPLICATION: 2005  PP: 47 pp.</p>

    <p>          ASSIGNEE:  (Kyoto University, Japan and Tokyo Medical and Dental University)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
