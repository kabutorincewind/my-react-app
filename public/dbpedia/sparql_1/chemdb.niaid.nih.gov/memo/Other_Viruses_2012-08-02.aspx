

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-08-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KdtNKSnGMg0D+FZ+1alg2mp2TzYIa7JUSAIY7THNhvHQqZLCwnWL0QZk7+kcv60NWcpqznv7PUhSh1IEv9Vbs6hPhgt8s6GkS9wmxJi26TM/NIOMDw9TXlVVabI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7038BE46" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: July 20 - August 2, 2012</h1>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22735264">Small Molecule and Peptide-Mediated Inhibition of Epstein-Barr Virus Nuclear Antigen 1 Dimerization</a><i>.</i> Kim, S.Y., K.A. Song, E. Kieff, and M.S. Kang. Biochemical and Biophysical Research Communications, 2012. 424(2): p. 251-256; PMID[22735264].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22644022">Identification of Disubstituted Sulfonamide Compounds as Specific Inhibitors of Hepatitis B Virus Covalently Closed Circular DNA Formation</a><i>.</i> Cai, D., C. Mills, W. Yu, R. Yan, C.E. Aldrich, J.R. Saputelli, W.S. Mason, X. Xu, J.T. Guo, T.M. Block, A. Cuconati, and H. Guo. Antimicrobial Agents and Chemotherapy, 2012. 56(8): p. 4277-4288; PMID[22644022].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22835135">Mulberrofuran G and Isomulberrofuran G from Morus Alba L.: Anti-Hepatitis B Virus Activity and Mass Spectrometric Fragmentation</a><i>.</i> Geng, C., Y.B. Ma, X.M. Zhang, S.Y. Yao, D.Q. Xue, P.R. Zhang, and J.J. Chen. Journal of Agricultural and Food Chemistry, 2012; PMID[22835135].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22826629">Effect of Interferon-gamma and Tumor Necrosis Factor-alpha on Hepatitis B Virus Following Lamivudine Treatment</a><i>.</i> Shi, H., L. Lu, N.P. Zhang, S.C. Zhang, and X.Z. Shen. World Journal of Gastroenterology, 2012. 18(27): p. 3617-3622; PMID[22826629].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22452737">Lamivudine Plus Adefovir vs. Entecavir in HBeAg-Positive Hepatitis B with Sequential Treatment Failure of Lamivudine and Adefovir</a><i>.</i> Son, C.Y., H.J. Ryu, J.M. Lee, S.H. Ahn, Y. Kim do, M.H. Lee, K.H. Han, C.Y. Chon, and J.Y. Park. Liver International, 2012. 32(7): p. 1179-1185; PMID[22452737].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22687441">Design, Synthesis, and Molecular Hybrids of Caudatin and Cinnamic Acids as Novel Anti-Hepatitis B Virus Agents</a><i>.</i> Wang, L.J., C.A. Geng, Y.B. Ma, J. Luo, X.Y. Huang, H. Chen, N.J. Zhou, X.M. Zhang, and J.J. Chen. European Journal of Medicinal Chemistry, 2012. 54: p. 352-365; PMID[22687441]. <b>[PubMed]</b>. OV_0720-080212.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22391672">LET-7B Is a Novel Regulator of Hepatitis C Virus Replication</a><i>.</i> Cheng, J.C., Y.J. Yeh, C.P. Tseng, S.D. Hsu, Y.L. Chang, N. Sakamoto, and H.D. Huang. Cellular and Molecular Life Sciences, 2012. 69(15): p. 2621-2633; PMID[22391672].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22770556">Synthesis and Antiviral Properties of Novel 7-Heterocyclic Substituted 7-Deaza-adenine Nucleoside Inhibitors of Hepatitis C NS5B Polymerase</a><i>.</i> Di Francesco, M.E., S. Avolio, M. Pompei, S. Pesci, E. Monteagudo, V. Pucci, C. Giuliano, F. Fiore, M. Rowley, and V. Summa. Bioorganic Medicinal Chemistry, 2012. 20(15): p. 4801-4811; PMID[22770556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22825118">Anti-HCV Activity and Toxicity of Pi4KIIIbeta Inhibitors</a><i>.</i> Lamarche, M.J., J. Borawski, A. Bose, C. Capacci-Daniel, R. Colvin, M. Dennehy, J. Ding, M. Dobler, J. Drumm, L.A. Gaither, J. Gao, X. Jiang, K. Lin, U. McKeever, X. Puyang, P. Raman, S. Thohan, R. Tommasi, K. Wagner, X. Xiong, T. Zabawa, S. Zhu, and B. Wiedmann. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print].</b> PMID[22825118].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22580131">Discovery of Substituted N-Phenylbenzenesulphonamides as a Novel Class of Non-Nucleoside Hepatitis C Virus Polymerase Inhibitors</a><i>.</i> May, M.M., D. Brohm, A. Harrenga, T. Marquardt, B. Riedl, J. Kreuter, H. Zimmermann, H. Ruebsamen-Schaeff, and A. Urban. Antiviral Research, 2012. 95(2): p. 182-191; PMID[22580131].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22784640">Discovery of ITX 4520: A Highly Potent Orally Bioavailable Hepatitis C Virus Entry Inhibitor</a><i>.</i> Mittapalli, G.K., F. Zhao, A. Jackson, H. Gao, H. Lee, S. Chow, M.P. Kaur, N. Nguyen, R. Zamboni, J. McKelvy, F. Wong-Staal, and J.E. Macdonald. Bioorganic and Medicinal Chemistry Letters, 2012. 22(15): p. 4955-4961; PMID[22784640].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22842003">Anti-Retroviral Drugs Do Not Facilitate Hepatitis C Virus (HCV) Infection in Vitro</a><i>.</i> Sandmann, L., M. Wilson, D. Back, H. Wedemeyer, M.P. Manns, E. Steinmann, T. Pietschmann, T.V. Hahn, and S. Ciesek. Antiviral Research, 2012. <b>[Epub ahead of print].</b>  PMID[22842003].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615282">MK-5172, a Selective Inhibitor of Hepatitis C Virus NS3/4A Protease with Broad Activity Across Genotypes and Resistant Variants</a><i>.</i> Summa, V., S.W. Ludmerer, J.A. McCauley, C. Fandozzi, C. Burlein, G. Claudio, P.J. Coleman, J.M. Dimuzio, M. Ferrara, M. Di Filippo, A.T. Gates, D.J. Graham, S. Harper, D.J. Hazuda, C. McHale, E. Monteagudo, V. Pucci, M. Rowley, M.T. Rudd, A. Soriano, M.W. Stahlhut, J.P. Vacca, D.B. Olsen, N.J. Liverton, and S.S. Carroll. Antimicrobial Agents and Chemotherapy, 2012. 56(8): p. 4161-4167; PMID[22615282].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22543048">Tegobuvir (GS-9190) Potency Against HCV Chimeric Replicons Derived from Consensus NS5B Sequences from Genotypes 2b, 3a, 4a, 5a, and 6a</a><i>.</i> Wong, K.A., S. Xu, R. Martin, M.D. Miller, and H. Mo. Virology, 2012. 429(1): p. 57-62; PMID[22543048].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22580497">In Vitro and in Vivo Evaluation of a Novel anti-Herpetic Flavonoid, 4&#39;-Phenylflavone, and its Synergistic Actions with Acyclovir</a><i>.</i> Hayashi, K., M. Iinuma, K. Sasaki, and T. Hayashi. Archives of Virology, 2012. 157(8): p. 1489-1498; PMID[22580497].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22758929">Synthesis and Biological Properties of C-2 Triazolylinosine Derivatives</a><i>.</i> Lakshman, M.K., A. Kumar, R. Balachandran, B.W. Day, G. Andrei, R. Snoeck, and J. Balzarini. Journal of Organic Chemistry, 2012. 77(14): p. 5870-83; PMID[22758929].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22779691">Human Mannose-Binding Lectin Inhibits Human Cytomegalovirus Infection in Human Embryonic Pulmonary Fibroblast</a><i>.</i> Wu, W., Y. Chen, H. Qiao, R. Tao, W. Gu, and S. Shang. APMIS, 2012. 120(8): p. 675-682; PMID[22779691].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0720-080212.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">18. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=1&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">Isoflavone Agonists of IRF-3 Dependent Signaling Have Antiviral Activity against RNA Viruses<i>.</i></a> Bedard, K.M., M.L. Wang, S.C. Proll, Y.M. Loo, M.G. Katze, M. Gale, and S.P. Iadonato. Journal of Virology, 2012. 86(13): p. 7334-7344; ISI[000305501600031].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=3&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">(-)-Epigallocatechin-3-gallate Inhibits the Replication Cycle of Hepatitis C Virus</a><i>.</i> Chen, C., H. Qiu, J. Gong, Q. Liu, H. Xiao, X.W. Chen, B.L. Sun, and R.G. Yang. Archives of Virology, 2012. 157(7): p. 1301-1312; ISI[000305795300009].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=4&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">UNC93b1 Mediates Innate Inflammation and Antiviral Defense in the Liver During Acute Murine Cytomegalovirus Infection</a><i>.</i> Crane, M.J., P.J. Gaddi, and T.P. Salazar-Mather. PLoS One, 2012. <b>7</b>(6); ISI[000305583300109].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=5&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">Artemisinin-Derived Dimer Diphenyl Phosphate is an Irreversible Inhibitor of Human Cytomegalovirus Replication</a><i>.</i> He, R., K. Park, H.Y. Cai, A. Kapoor, M. Forman, B. Mott, G.H. Posner, and R. Arav-Boger. Antimicrobial Agents and Chemotherapy, 2012. 56(7): p. 3508-3515; ISI[000305673000005]</p>

    <p class="plaintext"><b>[WOS].</b> OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=6&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">The Cyclophilin Inhibitor Scy-635 Disrupts Hepatitis C Virus NS5A-Cyclophilin A Complexes</a><i>.</i> Hopkins, S., M. Bobardt, U. Chatterji, J.A. Garcia-Rivera, P. Lim, and P.A. Gallay. Antimicrobial Agents and Chemotherapy, 2012. 56(7): p. 3888-3897; ISI[000305673000056].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=7&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">Hepatitis C Virus Attachment Mediated by Apolipoprotein E Binding to Cell Surface Heparan Sulfate</a><i>.</i> Jiang, J.Y., W. Cun, X.F. Wu, Q. Shi, H.L. Tang, and G.X. Luo. Journal of Virology, 2012. 86(13): p. 7256-7267; ISI[000305501600024].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=8&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">A Daphnane Diterpenoid Isolated from Wikstroemia Polyantha Induces an Inflammatory Response and Modulates Mirna Activity</a><i>.</i> Khong, A., R. Forestieri, D.E. Williams, B.O. Patrick, A. Olmstead, V. Svinti, E. Schaeffer, F. Jean, M. Roberge, R.J. Andersen, and E. Jan. PLoS One, 2012. <b>7</b>(6); ISI[000305808700039].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=9&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">Hepatoprotective and Anti-Hepatitis C Viral Activity of Platycodon Grandiflorum Extract on Carbon Tetrachloride-Induced Acute Hepatic Injury in Mice</a><i>.</i> Kim, T.W., J.H. Lim, I.B. Song, S.J. Park, J.W. Yang, J.C. Shin, J.W. Suh, H.Y. Son, E.S. Cho, M.S. Kim, S.W. Lee, J.W. Kim, and H.I. Yun. Journal of Nutritional Science and Vitaminology, 2012. 58(3): p. 187-194; ISI[000306025300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=10&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">Screening and Mechanism of Trapping Ligand Antagonist Peptide for Chemokine Receptor US28 of Human Cytomegalovirus</a><i>.</i> Liu, H.G., H.X. Sun, L. Li, X.M. Mo, X.Y. Li, and G. Zhang. Tropical Journal of Pharmaceutical Research, 2012. 11(2): p. 193-200; ISI[000305738800004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=12&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">Probing of Exosites Leads to Novel Inhibitor Scaffolds of HCV NS3/4A Proteinase</a><i>.</i> Shiryaev, S.A., A.V. Cheltsov, and A.Y. Strongin. PLoS One, 2012. 7(7); PMID[WOS:000305966500040].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=13&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">Simple and Accurate Approaches to Predict the Activity of Benzothiadiazine Derivatives as HCV Inhibitors</a><i>.</i> Su, L., L. Li, Y.W. Li, X.Y. Zhang, X.Y. Huang, and H.L. Zhai. Medicinal Chemistry Research, 2012. 21(8): p. 2079-2096; PMID[WOS:000305559900061].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=14&amp;SID=1Cc7ElJFb9okkfglfDM&amp;page=1&amp;doc=1">5-Acetyl-2-Arylbenzimidazoles as Antiviral Agents. Part 4</a><i>.</i> Vitale, G., P. Corona, M. Loriga, A. Carta, G. Paglietti, G. Giliberti, G. Sanna, P. Farci, M.E. Marongiu, and P. La Colla. European Journal of Medicinal Chemistry, 2012. 53: p. 83-97; PMID[WOS:000305863100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0720-080212.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
