

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-06-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5izm8/1HoVKsOhGFcSoNT5k5vmxW131ET6sdddRmm9uJenNiCya8YYJzl3Bs7P3DnpNGhu6P6lVWJGtWX6qpOK+cLz9uaTobGeipasgjvKkllrqCQnZnQxdmFwM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="392B1B04" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">May 27 - June 9, 2011</h1>

    
    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21641218">Synthesis, Transport and Antiviral Activity of Ala-Ser and Val-Ser Prodrugs of Cidofovir</a>. Peterson, L.W., J.S. Kim, P. Kijek, S. Mitchell, J. Hilfinger, J. Breitenbach, K. Borysko, J.C. Drach, B.A. Kashemirov, and C.E. McKenna. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21641218].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="memofmt2-2">Epstein-Barr virus</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21620532">Conjugates of 3alpha-Methoxyserrat-14-En-21beta-Ol (Pj-1) and 3beta-Methoxyserrat-14-En-21beta-Ol (Pj-2) as Cancer Chemopreventive Agents</a>. Tanaka, R., H. Tsujii, T. Yamada, T. Kajimoto, H. Tokuda, T. Arai, N. Suzuki, J. Hasegawa, Y. Hamashima, and M. Node. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21620532].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21605886">Arsenic Mediated Disruption of Promyelocytic Leukemia Protein Nuclear Bodies Induces Ganciclovir Susceptibility in Epstein-Barr Positive Epithelial Cells</a>. Sides, M.D., G.J. Block, B. Shan, K.C. Esteves, Z. Lin, E.K. Flemington, and J.A. Lasky. Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21605886].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21645219">Anti-Ulcer Agent Teprenone Inhibits Hepatitis C Virus Replication: Potential Treatment for Hepatitis C</a>. Ikeda, M., Y. Kawai, K. Mori, M. Yano, K. Abe, G. Nishimura, H. Dansako, Y. Ariumi, T. Wakita, K. Yamamoto, and N. Kato. Liver international : official journal of the International Association for the Study of the Liver, 2011. 31(6): p. 873-882; PMID[21645219].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21621997">Synthesis and Antiviral Activity of Cyclopropyl-Spirocarbocyclic Adenosine, (4r,5s,6r,7r)-4-(6-Amino-9h-Purin-9-Yl)-7-(Hydroxymethyl)Spiro[2.4]Heptane-5,6-Di Ol against Hepatitis C Virus</a>. Gadthula, S., R.K. Rawal, A. Sharon, D. Wu, B. Korba, and C.K. Chu. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21621997].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes Simplex virus</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21623543">Synthesis, Crystal Structure, and in Vitro Biological Evaluation of C-6 Pyrimidine Derivatives: New Lead Structures for Monitoring Gene Expression in Vivo</a>. Martic, M., L. Pernot, Y. Westermaier, R. Perozzo, T.G. Kraljevic, S. Kristafor, S. Raic-Malic, L. Scapozza, and S. Ametamey. Nucleosides, nucleotides &amp; nucleic acids, 2011. 30(4): p. 293-315; PMID[21623543].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21621625">Antiviral Activity of Lactobacillus Brevis Towards Herpes Simplex Virus Type 2: Role of Cell Wall Associated Components</a>. Mastromarino, P., F. Cacciotti, A. Masci, and L. Mosca. Anaerobe, 2011. <b>[Epub ahead of print]</b>; PMID[21621625].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21607799">Efficacy of Plant Products against Herpetic Infections</a>. Schnitzler, P. and J. Reichling. HNO, 2011. <b>[Epub ahead of print]</b>; PMID[21607799].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290446300001">Lysosomotropic Agents as HCV Entry Inhibitors</a>. Ashfaq, U.A., T. Javed, S. Rehman, Z. Nawaz, and S. Riazuddin. Virology Journal, 2011. 8; ISI[000290446300001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290836400025">A Reporter System for Epstein-Barr Virus (EBV) Lytic Replication: Anti-EBVActivity of the Broad Anti-Herpesviral Drug Artesunate</a>. Auerochs, S., K. Korn, and M. Marschall. Journal of Virological Methods, 2011. 173(2): p. 334-339; ISI[000290836400025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290708300007">The Discovery and Structure-Activity Relationships of Pyrano 3,4-B Indole-Based Inhibitors of Hepatitis C Virus Ns5b Polymerase</a>. Jackson, R.W., M.G. LaPorte, T. Herbertz, T.L. Draper, J.A. Gaboury, S.R. Rippin, R. Patel, S.K. Chunduru, C.A. Benetatos, D.C. Young, C.J. Burns, and S.M. Condon. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3227-3231; ISI[000290708300007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290708300028">Discovery of Novel HCV Polymerase Inhibitors Using Pharmacophore-Based Virtual Screening</a>. Kim, N.D., H. Chun, S.J. Park, J.W. Yang, J.W. Kim, and S.K. Ahn. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3329-3334; ISI[000290708300028].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290708300002">2-Arylmethylaminomethyl-5,6-Dihydroxychromone Derivatives with Selective Anti-HCV Activity</a>. Park, H.R., K.S. Park, and Y. Chong. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3202-3205; ISI[000290708300002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290587700005">2,3-Dihydro-1,2-Diphenyl-Substituted 4h-Pyridinone Derivatives as New Anti Flaviviridae Inhibitors</a>. Peduto, A., A. Massa, A. Di Mola, P. de Caprariis, P. La Colla, R. Loddo, S. Altamura, G. Maga, and R. Filosa. Chemical Biology &amp; Drug Design, 2011. 77(6): p. 441-449; ISI[000290587700005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290024200027">Syntheses and Initial Evaluation of a Series of Indolo-Fused Heterocyclic Inhibitors of the Polymerase Enzyme (Ns5b) of the Hepatitis C Virus</a>. Zheng, X.F., T.W. Hudyma, S.W. Martin, C. Bergstrom, M. Ding, F. He, J. Romine, M.A. Poss, J.F. Kadow, C.H. Chang, J. Wan, M.R. Witmer, P. Morin, D.M. Camac, S. Sheriff, B.R. Beno, K.L. Rigat, Y.K. Wang, R. Fridell, J. Lemm, D. Qiu, M.P. Liu, S. Voss, L. Pelosi, S.B. Roberts, M. Gao, J. Knipe, and R.G. Gentles. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2925-2929; ISI[000290024200027].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0527-060911.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
