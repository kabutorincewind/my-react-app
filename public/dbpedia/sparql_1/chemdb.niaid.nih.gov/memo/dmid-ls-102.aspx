

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-102.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ClsQvkjcKY4qCW2I3AqEWHH/FFEpMuwDbV1kGwngyEy/J7G7PxiwCwVaCV9Wa1UALhCycIXDKbOizuKpnRmN1lRpTPZrheeDBE1vEsoKZ9KF8RApLISrgCJ2QuI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B7EE9604" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-102-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57861   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral mode of action of a synthetic brassinosteroid against Junin virus replication</p>

    <p>          Castilla, V, Larzabal, M, Sgalippa, NA , Wachsman, MB, and Coto, CE</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4H279XX-1/2/f10496d405cd800401a5d5ece9e21852">http://www.sciencedirect.com/science/article/B6T2H-4H279XX-1/2/f10496d405cd800401a5d5ece9e21852</a> </p><br />

    <p>2.     57862   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          996. Development of an siRNA Based Therapy for Ebola Virus Infection</p>

    <p>          Geisbert, Thomas W, Hensley, Lisa E, Curtis, Kristopher M, Geisbert, Joan B, Daddario, Kathleen M, Kagan, Elliott, Lee, Amy CH, Palmer, Lorne, Jeffs, Lloyd, and MacLachlan, Ian</p>

    <p>          Molecular Therapy <b>2005</b>.  11(Supplement 1): 385</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WNJ-4H23F0X-17K/2/0d3b0099415ef78be361de29307d45e5">http://www.sciencedirect.com/science/article/B6WNJ-4H23F0X-17K/2/0d3b0099415ef78be361de29307d45e5</a> </p><br />

    <p>3.     57863   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C virus inhibits intracellular interferon alpha expression in human hepatic cell lines</p>

    <p>          Zhang, T, Lin, RT, Li, Y, Douglas, SD, Maxcey, C, Ho, C, Lai, JP, Wang, YJ, Wan, Q, and Ho, WZ </p>

    <p>          Hepatology <b>2005</b>.  42(4): 819-827</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16175599&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16175599&amp;dopt=abstract</a> </p><br />

    <p>4.     57864   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human papillomavirus infection: biology, epidemiology, and prevention</p>

    <p>          Scheurer, ME, Tortolero-Luna, G, and Adler-Storthz, K</p>

    <p>          Int J Gynecol Cancer <b>2005</b>.  15(5): 727-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16174218&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16174218&amp;dopt=abstract</a> </p><br />

    <p>5.     57865   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nucleotide analogs as novel anti-hepatitis B virus agents</p>

    <p>          Iyer, Radhakrishnan P, Padmanabhan, Seetharamaiyer, Zhang, Guangrong, Morrey, John D, and Korba, Brent E</p>

    <p>          Current Opinion in Pharmacology <b>2005</b>.  5(5): 520-528</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7F-4GTW8WK-4/2/247c0ac48e6a523ec05ca67407261137">http://www.sciencedirect.com/science/article/B6W7F-4GTW8WK-4/2/247c0ac48e6a523ec05ca67407261137</a> </p><br />
    <br clear="all">

    <p>6.     57866   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activities of extracts and selected pure constituents of Ocimum basilicum</p>

    <p>          Chiang, LC, Ng, LT, Cheng, PW, Chiang, W, and Lin, CC</p>

    <p>          Clin Exp Pharmacol Physiol <b>2005</b>.  32(10): 811-6</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16173941&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16173941&amp;dopt=abstract</a> </p><br />

    <p>7.     57867   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-hepatitis B virus activities of some ethyl 6-bromo-5-hydroxy-1H-indole-3-carboxylates</p>

    <p>          Chai, Huifang, Zhao, Yanfang, Zhao, Chunshen, and Gong, Ping</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H5MYG7-6/2/91c83959a2d834e46a3780ba19f25e12">http://www.sciencedirect.com/science/article/B6TF8-4H5MYG7-6/2/91c83959a2d834e46a3780ba19f25e12</a> </p><br />

    <p>8.     57868   DMID-LS-102; WOS-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of Small-Molecule Inhibitors of Hcvns3-4a Protease as Potential Therapeutic Agents Against Hcv Infection</p>

    <p>          Chen, S. and Tan, S.</p>

    <p>          Current Medicinal Chemistry <b>2005</b>.  12(20): 2317-2342</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231533800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231533800002</a> </p><br />

    <p>9.     57869   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peginterferon Alfa-2b and ribavirin for 12 versus 24 weeks in HCV infection</p>

    <p>          Borg, BB and Hoofnagle, JH</p>

    <p>          N Engl J Med <b>2005</b>.  353(11): 1182-3; author reply 1182-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16162894&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16162894&amp;dopt=abstract</a> </p><br />

    <p>10.   57870   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of hemagglutinin glycosylation on influenza virus susceptibility to neuraminidase inhibitors</p>

    <p>          Mishin, VP, Novikov, D, Hayden, FG, and Gubareva, LV</p>

    <p>          J Virol <b>2005</b>.  79(19): 12416-24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16160169&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16160169&amp;dopt=abstract</a> </p><br />

    <p>11.   57871   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of cationic [beta]-vinyl substituted meso-tetraphenylporphyrins and their in vitro activity against herpes simplex virus type 1</p>

    <p>          Silva, Eduarda MP, Giuntini, Francesca, Faustino, Maria AF, Tome, Joao PC, Neves, Maria GPMS, Tome, Augusto C, Silva, Artur MS, Santana-Marques, Maria G, Ferrer-Correia, Antonio J, and Cavaleiro, Jose AS</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(14): 3333-3337</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4GCX06R-8/2/89b9d050d4e49d03f6f0f78f45d491de">http://www.sciencedirect.com/science/article/B6TF9-4GCX06R-8/2/89b9d050d4e49d03f6f0f78f45d491de</a> </p><br />

    <p>12.   57872   DMID-LS-102; LR-14909-LWC; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Emerging drugs for chronic hepatitis C</p>

    <p>          Bhopale, Girish Mahadeorao and Nanda, Rabindra Kumar</p>

    <p>          Hepatology Research <b>2005</b>.  32(3): 146-153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T74-4GMGX86-1/2/9cccdf859d03bb68d8df03bd9f59facf">http://www.sciencedirect.com/science/article/B6T74-4GMGX86-1/2/9cccdf859d03bb68d8df03bd9f59facf</a> </p><br />

    <p>13.   57873   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Specific anti-viral effects of RNA interference on replication and expression of hepatitis B virus in mice</p>

    <p>          Wu, Y, Huang, AL, Tang, N, Zhang, BQ, and Lu, NF</p>

    <p>          Chin Med J (Engl) <b>2005</b>.  118(16): 1351-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16157029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16157029&amp;dopt=abstract</a> </p><br />

    <p>14.   57874   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Screening and identification of linear B-cell epitopes and entry-blocking peptide of severe acute respiratory syndrome (SARS)-associated coronavirus using synthetic overlapping peptide library</p>

    <p>          Hu, H, Li, L, Kao, RY, Kou, B, Wang, Z, Zhang, L, Zhang, H, Hao, Z, Tsui, WH, Ni, A, Cui, L, Fan, B, Guo, F, Rao, S, Jiang, C, Li, Q, Sun, M, He, W, and Liu, G</p>

    <p>          J Comb Chem <b>2005</b>.  7(5): 648-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16153058&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16153058&amp;dopt=abstract</a> </p><br />

    <p>15.   57875   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Novel 6&#39;(alpha)-Methyl-Branched Carbovir Analogues</p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.  338(9): 399-404</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16143957&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16143957&amp;dopt=abstract</a> </p><br />

    <p>16.   57876   DMID-LS-102; WOS-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Enhancement of Internal Ribosome Entry Site-Mediated Translation and Replication of Hepatitis C Virus by Pd98059</p>

    <p>          Murata, T., Hijikata, M., and Shimotohno, K.</p>

    <p>          Virology <b>2005</b>.  340(1): 105-115</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231631400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231631400010</a> </p><br />
    <br clear="all">

    <p>17.   57877   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of chemical and antiviral properties of essential oils from South American plants</p>

    <p>          Duschatzky, CB, Possetto, ML, Talarico, LB, Garcia, CC, Michis, F, Almeida, NV, de, Lampasona MP, Schuff, C, and Damonte, EB</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(4): 247-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130522&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130522&amp;dopt=abstract</a> </p><br />

    <p>18.   57878   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent highlights in the development of new antiviral drugs</p>

    <p>          De Clercq, Erik</p>

    <p>          Current Opinion in Microbiology <b>2005</b>.  8(5): 552-560</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VS2-4GY8B1D-1/2/c2d72d32f9151c9480fecf7f5192d604">http://www.sciencedirect.com/science/article/B6VS2-4GY8B1D-1/2/c2d72d32f9151c9480fecf7f5192d604</a> </p><br />

    <p>19.   57879   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of novel bisheterocycle-containing compounds as potential anti-influenza virus agents</p>

    <p>          Wang, Wen-Long, Yao, De-Yong, Gu, Min, Fan, Min-Zhi, Li, Jing-Ya, Xing, Ya-Cheng, and Nan, Fa-Jun</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4H5MYGM-9/2/74491a2b565090f14560b274aee85221">http://www.sciencedirect.com/science/article/B6TF9-4H5MYGM-9/2/74491a2b565090f14560b274aee85221</a> </p><br />

    <p>20.   57880   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Incidence of adamantane resistance among influenza A (H3N2) viruses isolated worldwide from 1994 to 2005: a cause for concern</p>

    <p>          Bright, Rick A, Medina, Marie-jo, Xu, Xiyan, Perez-Oronoz, Gilda, Wallis, Teresa R, Davis, Xiaohong M, Povinelli, Laura, Cox, Nancy J, and Klimov, Alexander I</p>

    <p>          The Lancet <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1B-4H5DYDM-1/2/ec835414f5e509bf7f33c6c29c5474bc">http://www.sciencedirect.com/science/article/B6T1B-4H5DYDM-1/2/ec835414f5e509bf7f33c6c29c5474bc</a> </p><br />

    <p>21.   57881   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparative activities of lipid esters of cidofovir and cyclic cidofovir against replication of herpesviruses in vitro</p>

    <p>          Williams-Aziz, SL, Hartline, CB, Harden, EA, Daily, SL, Prichard, MN, Kushner, NL, Beadle, JR, Wan, WB, Hostetler, KY, and Kern, ER</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(9): 3724-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127046&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127046&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   57882   DMID-LS-102; PUBMED-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent highlights in the development of new antiviral drugs</p>

    <p>          De Clercq, E</p>

    <p>          Curr Opin Microbiol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16125443&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16125443&amp;dopt=abstract</a> </p><br />

    <p>23.   57883   DMID-LS-102; WOS-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ion Transport Blockers Inhibit Human Rhinovirus 2 Release</p>

    <p>          Gazina, E. <i>et al.</i></p>

    <p>          Antiviral Research <b>2005</b>.  67(2): 98-106</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231485000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231485000005</a> </p><br />

    <p>24.   57884   DMID-LS-102; EMBASE-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-SARS coronavirus 3C-like protease effects of Isatis indigotica root and plant-derived phenolic compounds</p>

    <p>          Lin, Cheng-Wen, Tsai, Fuu-Jen, Tsai, Chang-Hai, Lai, Chien-Chen, Wan, Lei, Ho, Tin-Yun, Hsieh, Chang-Chi, and Chao, Pei-Dawn Lee</p>

    <p>          Antiviral Research <b>2005</b>.  68(1): 36-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-2/2/3c64da6b6a2e76ac0f696035ca750d31">http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-2/2/3c64da6b6a2e76ac0f696035ca750d31</a> </p><br />

    <p>25.   57885   DMID-LS-102; WOS-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Direct and Synergistic Inhibition of Hepatitis C Virus Replication by Cyclosporin a and Mycophenolic Acid.</p>

    <p>          Van Der Laan, L. <i>et al.</i></p>

    <p>          American Journal of Transplantation <b>2005</b>.  5: 421</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229231601456">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229231601456</a> </p><br />

    <p>26.   57886   DMID-LS-102; WOS-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Impact of 2-Bromo-5,6-Dichloro-1-Beta-D-Ribofuranosyl Benzimidazole Riboside and Inhibitors of Dna, Rna, and Protein Synthesis on Human Cytomegalovirus Genome Maturation</p>

    <p>          Mcvoy, M. and Nixon, D.</p>

    <p>          Journal of Virology <b>2005</b>.  79(17): 11115-11127</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231303900026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231303900026</a> </p><br />

    <p>27.   57887   DMID-LS-102; WOS-DMID-9/26/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure Exploration and Function Prediction of Sars Coronavirus E Protein</p>

    <p>          Shao, C. <i>et al.</i></p>

    <p>          Chemical Journal of Chinese Universities-Chinese <b>2005</b>.  26(8): 1512-1516</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231321400031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231321400031</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
