

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-132.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dowcCc7t1Fn6qgJZLTIKN34IafT3yeRvxUZBUodCeetxplmm0M7cDvKQbNmIPOYPMZWgKeBpRBF/lxlXlRKqriX/kA+Lca/7KuINzmMqTdC5qALTB3NXKaBdbPU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8BD548BD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-132-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59435   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p class="memofmt1-2">          New treatment strategies against hepatitis C viral infection</p>

    <p>          Bilodeau, M and Lamarre, D</p>

    <p>          Can J Gastroenterol <b>2006</b>.  20(11): 735-739</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17111056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17111056&amp;dopt=abstract</a> </p><br />

    <p>2.     59436   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p class="memofmt1-2">          Anti-influenza virus activity of biflavonoids</p>

    <p>          Miki, K, Nagai, T, Suzuki, K, Tsujimura, R, Koyama, K, Kinoshita, K, Furuhata, K, Yamada, H, and Takahashi, K</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17110111&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17110111&amp;dopt=abstract</a> </p><br />

    <p>3.     59437   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p class="memofmt1-2">          Antiviral treatment of hepatitis C: present status and future prospects</p>

    <p>          Koike, K</p>

    <p>          J Infect Chemother <b>2006</b>.  12(5): 227-232</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109084&amp;dopt=abstract</a> </p><br />

    <p>4.     59438   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p class="memofmt1-2">          SARS Coronavirus Protein 6 Accelerates Murine Coronavirus Infections</p>

    <p>          Tangudu, C, Olivares, H, Netland, J, Perlman, S, and Gallagher, T</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108045&amp;dopt=abstract</a> </p><br />

    <p>5.     59439   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p class="memofmt1-2">          The Old World and New World alphaviruses use different virus-specific proteins for induction of the transcriptional shutoff</p>

    <p>          Garmashova, N, Gorchakov, R, Volkova, E, Paessler, S, Frolova, E, and Frolov, I</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108023&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108023&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59440   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p><b>          Synthesis of novel potent hepatitis C virus NS3 protease inhibitors: Discovery of 4-hydroxy-cyclopent-2-ene-1,2-dicarboxylic acid as a N-acyl-l-hydroxyproline bioisostere</b> </p>

    <p>          Thorstensson, F, Wangsell, F, Kvarnstrom, I, Vrang, L, Hamelink, E, Jansson, K, Hallberg, A, Rosenquist, S, and Samuelsson, B</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17107807&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17107807&amp;dopt=abstract</a> </p><br />

    <p>7.     59441   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Mycophenolic Acid Inhibits Hepatitis C Virus Replication and Acts in Synergy With Cyclosporin A and Interferon-[alpha]</p>

    <p>          Henry, Scot D, Metselaar, Herold J, Lonsdale, Richard CB, Kok, Alice, Haagmans, Bart L, Tilanus, Hugo W, and van der Laan, Luc JW</p>

    <p>          Gastroenterology <b>2006</b>.  131(5): 1452-1462</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4KN3S9S-4/2/a3c674d3285c139333fdfa0b63248f8d">http://www.sciencedirect.com/science/article/B6WFX-4KN3S9S-4/2/a3c674d3285c139333fdfa0b63248f8d</a> </p><br />

    <p>8.     59442   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p class="memofmt1-2">          Targets of emerging therapies for viral hepatitis B and C</p>

    <p>          Yerly, D, Di, Giammarino L, Bihl, F, and Cerny, A</p>

    <p>          Expert Opin Ther Targets <b>2006</b>.  10(6): 833-850</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17105371&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17105371&amp;dopt=abstract</a> </p><br />

    <p>9.     59443   DMID-LS-132; PUBMED-WOS-11/20/2006 </p>

    <p class="memofmt1-2">          Mechanism of activation of {beta}-D-2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methylcytidine and inhibition of hepatitis C virus NS5B RNA polymerase</p>

    <p>          Murakami, E, Bao, H, Ramesh, M, McBrayer, TR, Whitaker, T, Micolochick, Steuer HM, Schinazi, RF, Stuyver, LJ, Obikhod, A, Otto, MJ, and Furman, PA</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101674&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101674&amp;dopt=abstract</a> </p><br />

    <p>10.   59444   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Efficacy and safety of peg-IFN alfa-2a with ribavirin for the treatment of HCV/HIV coinfected patients who failed previous IFN based therapy</p>

    <p>          Rodriguez-Torres, Maribel, Rodriguez-Orengo, Jose F, Rios-Bedoya, Carlos F, Fernandez-Carbia, Alberto, Gonzalez-Lassalle, Elsa, Salgado-Mercado, Rosa, and Marxuach-Cuetara, Acisclo M</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4M63RY9-1/2/8988b288ef37f8b84b2678b9a22c1cfe">http://www.sciencedirect.com/science/article/B6VJV-4M63RY9-1/2/8988b288ef37f8b84b2678b9a22c1cfe</a> </p><br />
    <br clear="all">

    <p>11.   59445   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Ribavirin and mycophenolic acid markedly potentiate the anti-hepatitis B virus activity of entecavir</p>

    <p>          Ying, C, Colonno, R, De, Clercq E, and Neyts, J</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17098296&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17098296&amp;dopt=abstract</a> </p><br />

    <p>12.   59446   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Derivation of a novel SARS-coronavirus replicon cell line and its application for anti-SARS drug screening</p>

    <p>          Ge, F, Luo, Y, Liew, PX, and Hung, E</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17098272&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17098272&amp;dopt=abstract</a> </p><br />

    <p>13.   59447   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Rapid Decline of Viral RNA in Hepatitis C Patients Treated With VX-950: A Phase Ib, Placebo-Controlled, Randomized Study</p>

    <p>          Reesink, Hendrik W, Zeuzem, Stefan, Weegink, Christine J, Forestier, Nicole, van Vliet, Andre, van de Wetering de Rooij, Jeroen, McNair, Lindsay, Purdy, Susan, Kauffman, Robert, Alam, John, and Jansen, Peter LM</p>

    <p>          Gastroenterology <b>2006</b>.  131(4): 997-1002</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4M69264-R/2/5b19161da34dac9cfbd146cc09511a55">http://www.sciencedirect.com/science/article/B6WFX-4M69264-R/2/5b19161da34dac9cfbd146cc09511a55</a> </p><br />

    <p>14.   59448   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Discovering Severe Acute Respiratory Syndrome Coronavirus 3CL Protease Inhibitors: Virtual Screening, Surface Plasmon Resonance, and Fluorescence Resonance Energy Transfer Assays</p>

    <p>          Chen, L, Chen, S, Gui, C, Shen, J, Shen, X, and Jiang, H</p>

    <p>          J Biomol Screen <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17092912&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17092912&amp;dopt=abstract</a> </p><br />

    <p>15.   59449   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Olefin ring-closing metathesis as a powerful tool in drug discovery and development - potent macrocyclic inhibitors of the hepatitis C virus NS3 protease</p>

    <p>          Tsantrizos, Youla S, Ferland, Jean-Marie, McClory, Andrew, Poirier, Martin, Farina, Vittorio, Yee, Nathan K, Wang, Xiao-jun, Haddad, Nizar, Wei, Xudong, Xu, Jinghua, and Zhang, Li</p>

    <p>          Journal of Organometallic Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGW-4KXNT5H-P/2/9b16cc346341b719e78d94074c8d509d">http://www.sciencedirect.com/science/article/B6TGW-4KXNT5H-P/2/9b16cc346341b719e78d94074c8d509d</a> </p><br />
    <br clear="all">

    <p>16.   59450   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Influenza virus resistance to neuraminidase inhibitors</p>

    <p>          Mendel, DB and Sidwell, RW</p>

    <p>          Drug Resist Updat <b>1998</b>.  1(3): 184-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17092804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17092804&amp;dopt=abstract</a> </p><br />

    <p>17.   59451   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Antiviral Effect of Aqueous Extracts from Species of the Lamiaceae Family against Herpes simplex Virus Type 1 and Type 2 in vitro</p>

    <p>          Nolkemper, S, Reichling, J, Stintzing, FC, Carle, R, and Schnitzler, P</p>

    <p>          Planta Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17091431&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17091431&amp;dopt=abstract</a> </p><br />

    <p>18.   59452   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Interferons alpha and lambda Inhibit Hepatitis C Virus Replication With Distinct Signal Transduction and Gene Regulation Kinetics</p>

    <p>          Marcello, T, Grakoui, A, Barba-Spaeth, G, Machlin, ES, Kotenko, SV, Macdonald, MR, and Rice, CM</p>

    <p>          Gastroenterology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17087946&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17087946&amp;dopt=abstract</a> </p><br />

    <p>19.   59453   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Molecular virology of hepatitis B virus and the development of antiviral drug resistance</p>

    <p>          Locarnini, S and Omata, M</p>

    <p>          Liver Int <b>2006</b>.  26 Suppl 2: 11-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17087765&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17087765&amp;dopt=abstract</a> </p><br />

    <p>20.   59454   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Peginterferon alfa-2b Therapy in Acute Hepatitis C: Impact of Onset of Therapy on Sustained Virologic Response</p>

    <p>          Malnick, Stephen DH and Basevitch, Alon</p>

    <p>          Gastroenterology <b>2006</b>.  131(2): 683</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4KJKXVY-29/2/9d3540a20d41177f42e0aae43d2ec2a1">http://www.sciencedirect.com/science/article/B6WFX-4KJKXVY-29/2/9d3540a20d41177f42e0aae43d2ec2a1</a> </p><br />
    <br clear="all">

    <p>21.   59455   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Structure and antiviral activity of sulfated fucans from Stoechospermum marginatum</p>

    <p>          Adhikari, U, Mateu, CG, Chattopadhyay, K, Pujol, CA, Damonte, EB, and Ray, B</p>

    <p>          Phytochemistry  <b>2006</b>.  67(22): 2474-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17067880&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17067880&amp;dopt=abstract</a> </p><br />

    <p>22.   59456   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Cleavage preference distinguishes the two-component NS2B-NS3 serine proteinases of Dengue and West Nile viruses</p>

    <p>          Shiryaev, SA, Kozlov, IA, Ratnikov, BI, Smith, JW, Lebl, M, and Strongin, AY</p>

    <p>          Biochem J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17067286&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17067286&amp;dopt=abstract</a> </p><br />

    <p>23.   59457   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Hcvns2 Protein Inhibits Cell Proliferation and Induces Cell Cycle Arrest in the S-Phase in Mammalian Cells Through Down-Regulation of Cyclin a Expression</p>

    <p>          Yang, X. <i>et al.</i></p>

    <p>          Virus Research  <b>2006</b>.  121(2): 134-143</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241644400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241644400004</a> </p><br />

    <p>24.   59458   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Integerrimides A and B, cyclic heptapeptides from the latex of Jatropha integerrima</p>

    <p>          Mongkolvisut, W, Sutthivaiyakit, S, Leutbecher, H, Mika, S, Klaiber, I, Moller, W, Rosner, H, Beifuss, U, and Conrad, J</p>

    <p>          J Nat Prod <b>2006</b>.  69(10): 1435-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17067157&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17067157&amp;dopt=abstract</a> </p><br />

    <p>25.   59459   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          3&#39;-Carbon-substituted pyrimidine nucleosides having a 2&#39;,3&#39;-dideoxy and 2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxy structure: synthesis and antiviral evaluation</p>

    <p>          Kumamoto, H, Onuma, S, Tanaka, H, Dutschman, GE, and Cheng, YC</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(4): 225-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17066900&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17066900&amp;dopt=abstract</a> </p><br />

    <p>26.   59460   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Respiratory Syncytial Virus Disease Mechanisms Implicated by Human, Animal Model, and in Vitro Data Facilitate Vaccine Strategies and New Therapeutics</p>

    <p>          Moore, M. and Peebles, R.</p>

    <p>          Pharmacology &amp; Therapeutics <b>2006</b>.  112(2): 405-424</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241567500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241567500003</a> </p><br />

    <p>27.   59461   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          The combination of type I interferon and ribavirin has an inhibitory effect on mouse hepatitis virus infection</p>

    <p>          Sone, S, Izawa, A, Narumi, H, Kajita, A, Tanabe, J, and Taguchi, F</p>

    <p>          Hepatol Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064956&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064956&amp;dopt=abstract</a> </p><br />

    <p>28.   59462   DMID-LS-132; PUBMED-WOS-11/20/2006</p>

    <p class="memofmt1-2">          Drug resistance among influenza A viruses isolated in Italy from 2000 to 2005: are the emergence of Adamantane-resistant viruses cause of concern?</p>

    <p>          Ansaldi, F, Valle, L, Amicizia, D, Banfi, F, Pastorino, B, Sticchi, L, Icardi, G, Gasparini, R, and Crovari, P</p>

    <p>          J Prev Med Hyg  <b>2006</b>.  47(1): 1-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17061402&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17061402&amp;dopt=abstract</a> </p><br />

    <p>29.   59463   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p><b>          Downregulation of Protein Disulfide Isomerase Inhibits Infection by the Mouse Polyomavirus</b> </p>

    <p>          Gilbert, J. <i>et al.</i></p>

    <p>          Journal of Virology <b>2006</b>.  80(21): 10868-10870</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241606100057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241606100057</a> </p><br />

    <p>30.   59464   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of alkoxyalkyl-phosphate conjugates of cidofovir and adefovir</p>

    <p>          Ruiz, Jacqueline C, Beadle, James R, Aldern, Kathy A, Keith, Kathy A, Hartline, Caroll B, Kern, Earl R, and Hostetler, Karl Y</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4M33SJN-2/2/c1f5858a76bebf00eb95feb6631caff8">http://www.sciencedirect.com/science/article/B6T2H-4M33SJN-2/2/c1f5858a76bebf00eb95feb6631caff8</a> </p><br />

    <p>31.   59465   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Three Dimensional Model of Severe Acute Respiratory Syndrome Coronavirus Helicase Atpase Catalytic Domain and Molecular Design of Severe Acute Respiratory Syndrome Coronavirus Helicase Inhibitors</p>

    <p>          Hoffmann, M. <i>et al.</i></p>

    <p>          Journal of Computer-Aided Molecular Design <b>2006</b>.  20(5): 305-319</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241554400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241554400004</a> </p><br />

    <p>32.   59466   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Recommendations and Potential Future Options in the Treatment of Hepatitis B</p>

    <p>          Yuen, M. and Lai, C.</p>

    <p>          Expert Opinion on Pharmacotherapy <b>2006</b>.  7(16): 2225-2231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241643900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241643900004</a> </p><br />
    <br clear="all">

    <p>33.   59467   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Hepatitis B and C Virus Antiviral Resistance</p>

    <p>          Saez-Lopez, A. and Aguero-Balbin, J.</p>

    <p>          Enfermedades Infecciosas Y Microbiologia Clinica <b>2006</b>.  24(9): 576-584</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241648300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241648300009</a> </p><br />

    <p>34.   59468   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          In Vitro Antiviral Activities of Chinese Medicinal Herbs Against Duck Hepatitis B Virus</p>

    <p>          Leung, K. <i>et al.</i></p>

    <p>          Phytotherapy Research <b>2006</b>.  20(10): 911-914</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241474900017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241474900017</a> </p><br />

    <p>35.   59469   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          HBV drug resistance: Mechanisms, detection and interpretation</p>

    <p>          Shaw, Tim, Bartholomeusz, Angeline, and Locarnini, Stephen</p>

    <p>          Journal of Hepatology <b>2006</b>.  44(3): 593-606</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4J2TRRT-2/2/e1421e342429f8c08badf7780af7998a">http://www.sciencedirect.com/science/article/B6W7C-4J2TRRT-2/2/e1421e342429f8c08badf7780af7998a</a> </p><br />

    <p>36.   59470   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Validation of Coronavirus E Proteins Ion Channels as Targets for Antiviral Drugs</p>

    <p>          Wilson, L, Gage, P, and Ewart, G</p>

    <p>          Advances in Experimental Medicine and Biology <b>2006</b>.  581: 573-578</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241010800104">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241010800104</a> </p><br />

    <p>37.   59471   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Identification and Evaluation of Coronavirus Replicase Inhibitors Using a Replicon Cell Line</p>

    <p>          Scandella, E,  Eriksson, KK, Hertzig, T , Drosten, C, Chen, LL, Gui, CS, Luo, XM, Shen, JH, Shen, X,  Siddell, SG, Ludewig, B, Jiang, HL, Gunther, S, and Thiel, V</p>

    <p>          Advances in Experimental Medicine and Biology <b>2006</b>.  581: 609-613</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241010800111">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241010800111</a> </p><br />

    <p>38.   59472   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Characterization of drug-resistant recombinant influenza A/H1N1 viruses selected in vitro with peramivir and zanamivir</p>

    <p>          Baz, Mariana, Abed, Yacine, and Boivin, Guy</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4MCW7M3-1/2/dfc3538d5ae8463a29347d70e89f6469">http://www.sciencedirect.com/science/article/B6T2H-4MCW7M3-1/2/dfc3538d5ae8463a29347d70e89f6469</a> </p><br />

    <p>39.   59473   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Susceptibility of highly pathogenic A(H5N1) avian influenza viruses to the neuraminidase inhibitors and adamantanes</p>

    <p>          Hurt, AC, Selleck, P, Komadina, N, Shaw, R, Brown, L, and Barr, IG</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4M9S95W-2/2/eef432f91bfef2c3208e14af88a2c14c">http://www.sciencedirect.com/science/article/B6T2H-4M9S95W-2/2/eef432f91bfef2c3208e14af88a2c14c</a> </p><br />

    <p>40.   59474   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Bicyclol for Chronic Hepatitis B</p>

    <p>          Wu, T. <i>et al.</i></p>

    <p>          Cochrane Database of Systematic Reviews <b>2006</b>.(4)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241386000037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241386000037</a> </p><br />

    <p>41.   59475   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Increased adamantane resistance in influenza A(H3) viruses in Australia and neighbouring countries in 2005</p>

    <p>          Barr, IG, Hurt, AC, Iannello, P, Tomasov, C, Deed, N, and Komadina, N</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4KRY1R2-1/2/7ac96d5473a2380c4049ab40d50d32e7">http://www.sciencedirect.com/science/article/B6T2H-4KRY1R2-1/2/7ac96d5473a2380c4049ab40d50d32e7</a> </p><br />

    <p>42.   59476   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Influenza virus inhibits RNA polymerase II elongation</p>

    <p>          Chan, Annie Y, Vreede, Frank T, Smith, Matt, Engelhardt, Othmar G, and Fodor, Ervin</p>

    <p>          Virology <b>2006</b>.  351(1): 210-217</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4JS1MRH-3/2/f2b2f7fa7ec073e27469ff3a357b122d">http://www.sciencedirect.com/science/article/B6WXR-4JS1MRH-3/2/f2b2f7fa7ec073e27469ff3a357b122d</a> </p><br />

    <p>43.   59477   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Avian Influenza: Exploring All the Avenues</p>

    <p>          Treanor, J.</p>

    <p>          Annals of Internal Medicine <b>2006</b>.  145(8): 631-632</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241315700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241315700011</a> </p><br />

    <p>44.   59478   DMID-LS-132; WOS-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Fatal Outcome of Human Influenza a (H5n1) Is Associated With High Viral Load and Hypercytokinemia</p>

    <p>          De Jong, M. <i>et al.</i></p>

    <p>          Nature Medicine <b>2006</b>.  12(10): 1203-1207</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241102200039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241102200039</a> </p><br />

    <p>45.   59479   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Discovery of adamantane ethers as inhibitors of 11[beta]-HSD-1: Synthesis and biological evaluation</p>

    <p>          Patel, Jyoti R, Shuai, Qi, Dinges, Jurgen, Winn, Marty, Pliushchev, Marina, Fung, Steven, Monzon, Katina, Chiou, William, Wang, Jiahong, and Pan, Liping</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4M6XMFM-5/2/fbf36f38fa72da4f7771810b292d3a41">http://www.sciencedirect.com/science/article/B6TF9-4M6XMFM-5/2/fbf36f38fa72da4f7771810b292d3a41</a> </p><br />
    <br clear="all">

    <p>46.   59480   DMID-LS-132; EMBASE-DMID-11/20/2006</p>

    <p class="memofmt1-2">          Novel 5,7-disubstituted 6-amino-5H-pyrrolo[3,2-b]pyrazine-2,3-dicarbonitriles, the[no-break space]promising protein kinase inhibitors with antiproliferative activity</p>

    <p>          Dubinina, GG, Platonov, MO, Golovach, SM, Borysko, PO, Tolmachov, AO, and Volovenko, YM</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  41(6): 727-737</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4JW11VH-2/2/d7f15fd023ee140fcfd04d0d676e847a">http://www.sciencedirect.com/science/article/B6VKY-4JW11VH-2/2/d7f15fd023ee140fcfd04d0d676e847a</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
