

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-08-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7qFxRk1VlRVB0ULzI6NpYl/2oHpnmJwO5Uotq3dewGKxADKU/SD/0NRGwA5vOhrn1ATS1Zgc4rYVdgyLJEgYb/prZ992vtxWoZXpYYFnoAwp9eSRoV7xd5w14fc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="06B9B7A4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  July 21 - August 3, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="ListParagraph">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20677318">The Active Core in a Triazole Peptide Dual-Site Antagonist of HIV-1 gp120.</a> Umashankara M, McFadden K, Zentner I, Schön A, Rajagopal S, Tuzer F, Kuriakose SA, Contarino M, Lalonde J, Freire E, Chaiken I. ChemMedChem. 2010 Aug 2. [Epub ahead of print]PMID: 20677318</p>

    <p class="ListParagraph">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20668078">P Body-Associated Protein Mov10 Inhibits HIV-1 Replication at Multiple Stages.</a> Burdick R, Smith JL, Chaipan C, Friew Y, Chen J, Venkatachari NJ, Delviks-Frankenberry KA, Hu WS, Pathak VK. J Virol. 2010 Jul 28</p>

    <p class="ListParagraph">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20661812">Synthesis and anti-HIV evaluation of new acyclic phosphonate nucleotide analogues and their bis(SATE) derivatives.</a> Li H, Hong JH. Nucleosides Nucleotides Nucleic Acids. 2010 Aug;29(8):581-90.PMID: 20661812</p>

    <p class="ListParagraph">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20660667">Lersivirine: a Non-Nucleoside Reverse Transcriptase Inhibitor with Activity against Drug-Resistant Human Immunodeficiency Virus-1.</a> Corbau R, Mori J, Phillips C, Fishburn L, Martin A, Mowbray C, Panton W, Smith Burchnell C, Thornberry A, Ringrose H, Knöchel T, Irving S, Westby M, Wood A, Perros M. Antimicrob Agents Chemother. 2010 Jul 26. [Epub ahead of print]PMID: 20660667</p>

    <p class="ListParagraph">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20660316">Hydrocarbon double-stapling remedies the proteolytic instability of a lengthy peptide therapeutic.</a> Bird GH, Madani N, Perry AF, Princiotto AM, Supko JG, He X, Gavathiotis E, Sodroski JG, Walensky LD. Proc Natl Acad Sci U S A. 2010 Jul 21. [Epub ahead of print]PMID: 20660316</p>

    <p class="ListParagraph">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20652190">Design of novel RNA ligands that bind stem-bulge HIV-1 TAR RNA.</a> Duca M, Malnuit V, Barbault F, Benhida R. Chem Commun (Camb). 2010 Jul 23. [Epub ahead of print]PMID: 20652190</p>

    <p class="ListParagraph">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20624681">Baculiferins A-O, O-sulfated pyrrole alkaloids with anti-HIV-1 activity, from the Chinese marine sponge Iotrochota baculifera.</a> Fan G, Li Z, Shen S, Zeng Y, Yang Y, Xu M, Bruhn T, Bruhn H, Morschhäuser J, Bringmann G, Lin W. Bioorg Med Chem. 2010 Aug 1;18(15):5466-74. Epub 2010 Jun 23.PMID: 20624681</p>

    <p class="ListParagraph">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20619663">Rational conversion of noncontinuous active region in proteins into a small orally bioavailable macrocyclic drug-like molecule: the HIV-1 CD4:gp120 paradigm.</a> Hurevich M, Swed A, Joubran S, Cohen S, Freeman NS, Britan-Rosich E, Briant-Longuet L, Bardy M, Devaux C, Kotler M, Hoffman A, Gilon C. Bioorg Med Chem. 2010 Aug 1;18(15):5754-61. Epub 2010 Apr 21.PMID: 20619663</p>

    <p class="ListParagraph">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20609585">Biaryl ethers as potent allosteric inhibitors of reverse transcriptase and its key mutant viruses: aryl substituted pyrazole as a surrogate for the pyrazolopyridine motif.</a> Su DS, Lim JJ, Tinney E, Tucker TJ, Saggar S, Sisko JT, Wan BL, Young MB, Anderson KD, Rudd D, Munshi V, Bahnck C, Felock PJ, Lu M, Lai MT, Touch S, Moyer G, Distefano DJ, Flynn JA, Liang Y, Sanchez R, Perlow-Poehnelt R, Miller M, Vacca JP, Williams TM, Anthony NJ. Bioorg Med Chem Lett. 2010 Aug 1;20(15):4328-32. Epub 2010 Jun 17.PMID: 20609585</p>

    <p class="ListParagraph">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20596607">Histone deacetylase inhibitor Scriptaid reactivates latent HIV-1 promoter by inducing histone modification in in vitro latency cell lines.</a> Ying H, Zhang Y, Lin S, Han Y, Zhu HZ. Int J Mol Med. 2010 Aug;26(2):265-72.PMID: 20596607</p>

    <p class="ListParagraph">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20572811">Design, biologic evaluation, and SAR of novel pseudo-peptide incorporating benzheterocycles as HIV-1 protease inhibitors.</a> He M, Zhang H, Yao X, Eckart M, Zuo E, Yang M. Chem Biol Drug Des. 2010 Aug;76(2):174-80. Epub 2010 Jun 21.PMID: 20572811</p>

    <p class="ListParagraph">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20565056">Binding Characteristics of Small Molecules That Mimic Nucleocapsid Protein-Induced Maturation of Stem-Loop 1 of HIV-1 RNA.</a> Chung J, Ulyanov NB, Guilbert C, Mujeeb A, James TL. Biochemistry. 2010 Aug 3;49(30):6341-51.PMID: 20565056</p>

    <p class="ListParagraph">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20533501">Novel Monocyclam Derivatives as HIV Entry Inhibitors: Design, Synthesis, Anti-HIV Evaluation, and Their Interaction with the CXCR4 Co-receptor.</a> Pettersson S, Pérez-Nueno VI, Mena MP, Clotet B, Esté JA, Borrell JI, Teixidó J. ChemMedChem. 2010 Aug 2;5(8):1272-81.PMID: 20533501</p>

    <p class="ListParagraph">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20517160">Potent in vitro inactivation of both free and cell-associated CCR5- and CXCR4-tropic HIV-1 by common commercial soap bars from South Africa.</a> Jenabian MA, Auvert B, Saïdi H, Lissouba P, Matta M, Bélec L. J Acquir Immune Defic Syndr. 2010 Aug 1;54(4):340-2.PMID: 20517160</p>

    <p class="ListParagraph">15.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20498311">Actinohivin, a broadly neutralizing prokaryotic lectin, inhibits HIV-1 infection by specifically targeting high-mannose-type glycans on the gp120 envelope.</a> Hoorelbeke B, Huskens D, Férir G, François KO, Takahashi A, Van Laethem K, Schols D, Tanaka H, Balzarini J. Antimicrob Agents Chemother. 2010 Aug;54(8):3287-301. Epub 2010 May 24.PMID: 20498311</p>

    <p class="ListParagraph">16.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20491589">Inhibition of human immunodeficiency virus type 1 by lactic acid bacteria from human breastmilk.</a> Martín V, Maldonado A, Fernández L, Rodríguez JM, Connor RI. Breastfeed Med. 2010 Aug;5:153-8.PMID: 20491589</p>

    <p class="ListParagraph">17.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20438763">Characterization of HIV-1 resistance to a fusion inhibitor, N36, derived from the gp41 amino-terminal heptad repeat.</a> Izumi K, Nakamura S, Nakano H, Shimura K, Sakagami Y, Oishi S, Uchiyama S, Ohkubo T, Kobayashi Y, Fujii N, Matsuoka M, Kodama EN. Antiviral Res. 2010 Aug;87(2):179-86. Epub 2010 May 8.PMID: 20438763</p>

    <p class="ListParagraph">18.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20353818">Intrathecal injection of an alpha seven nicotinic acetylcholine receptor agonist attenuates gp120-induced mechanical allodynia and spinal pro-inflammatory cytokine profiles in rats.</a> Loram LC, Harrison JA, Chao L, Taylor FR, Reddy A, Travis CL, Giffard R, Al-Abed Y, Tracey K, Maier SF, Watkins LR. Brain Behav Immun. 2010 Aug;24(6):959-67. Epub 2010 Mar 28.PMID: 20353818</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>19.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279806300001">Immunotoxin Complementation of HAART to Deplete Persisting HIV-Infected Cell Reservoirs.</a>  Berger, EA; Pastan, I.  PLOS PATHOGENS, 2010; 6(6).</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279656500011">A dimeric high-molecular-weight chymotrypsin inhibitor with antitumor and HIV-1 reverse transcriptase inhibitory activities from seeds of Acacia confusa</a>.  Lam, SK; Ng, TB.  PHYTOMEDICINE, 2010; 17(8-9): 621-625.</p>

    <p>21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279787200026">Peptide HIV-1 Integrase Inhibitors from HIV-1 Gene Products</a>.  Suzuki, S; Urano, E; Hashimoto, C; Tsutsumi, H; Nakahara, T; Tanaka, T; Nakanishi, Y; Maddali, K; Han, Y; Hamatake, M; Miyauchi, K; Pommie, Y; Beutler, JA; Sugiura, W; Fuji, H; Hoshino, T; Itotani, K; Nomura, W; Narumi, T; Yamamoto, N; Komano, JA; Tamamura, H.  JOURNAL OF MEDICINAL CHEMISTRY,2010;  53(14): 5356-5360.</p>

    <p>22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279935000001">Schizolysin, a hemolysin from the split gill mushroom Schizophyllum</a>.  Han, CH; Zhang, GQ; Wang, HX; Ng, TB.  FEMS MICROBIOLOGY LETTERS,2010; 309(2): 115-121.</p>

    <p>23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279799600009">Design, Biologic Evaluation, and SAR of Novel Pseudo-peptide Incorporating Benzheterocycles as HIV-1 Protease Inhibitors.</a>  AU He, MZ; Zhang, H; Yao, XJ; Eckart, M; Zuo, E; Yang, M.  CHEMICAL BIOLOGY &amp; DRUG DESIGN, 2010; 76(2): 174-180.</p>

    <p>24.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279866300006">Biaryl ethers as potent allosteric inhibitors of reverse transcriptase and its key mutant viruses: Aryl substituted pyrazole as a surrogate for the pyrazolopyridine motif.</a>  Su, DS; Lim, JJ; Tinney, E; Tucker, TJ; Saggar, S; Sisko, JT; Wan, BL; Young, MB; Anderson, KD; Rudd, D; Munshi, V; Bahnck, C; Felock, PJ; Lu, M; Lai, MT; Touch, S; Moyer, G; DiStefano, DJ; Flynn, JA; Liang, YX; Sanchez, R; Perlow-Poehnelt, R; Miller, M; Vacca, JP; Williams, TM; Anthony, NJ.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(15):4328-4332.</p>

    <p>25.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279866300025">Synthesis and evaluation of beta-carboline derivatives as inhibitors of human immunodeficiency virus.</a> Brahmbhatt, KG; Ahmed, N; Sabde, S; Mitra, D; Singh, IP; Bhutani, KK.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS.  20(15):  4416-4419.</p>

    <p>26.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279866300028">Active site binding modes of dimeric phloroglucinols for HIV-1 reverse transcriptase, protease and integrase.</a>  Gupta, P; Kumar, R; Garg, P; Singh, IP.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(15):4427-4431.</p>

    <p>27.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279866300029">Synthesis of highly functionalized 2,4-diaminoquinazolines as anticancer and anti-HIV agents.</a>  Yan, SJ; Zheng, H; Huang, C; Yan, YY; Lin, J.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(15): 4432-4435.</p>

    <p>28.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279704800010">A Novel Ribonuclease with Antiproliferative Activity from Fresh Fruiting Bodies of the Edible Mushroom Lyophyllum shimeiji</a>.  Zhang, RY; Zhang, GQ; Hu, DD; Wang, HX; Ng, TB.  BIOCHEMICAL GENETICS, 2010; 48(7-8): 658-668.</p>

    <p>29.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279844900077">A New Daphne Diterpenoids from Daphne acutiloba Rehd.</a>  Xu, YR; Li, YK; Cao, JL; Zhang, X; Yang, GY; Hu, QF.  ASIAN JOURNAL OF CHEMISTRY, 2010; 22(8): 6371-6374.</p>

    <p>30.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279844900095">Arylnaphthalene Lignans from Daphne acutiloba Rehd</a>.  Cao, JL; Xue, JJ; He, SQ; Yang, GY; Hu, QF.  ASIAN JOURNAL OF CHEMISTRY, 2010; 22(8): 6509-6512.</p>

    <p>31.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279963300068">An Albumin-Conjugated Peptide Exhibits Potent Anti-HIV Activity and Long In Vivo Half-Life.</a>  AU Xie, D; Yao, C; Wang, L; Stoddart, CA; Ptak, RG; Min, WJ; Xu, JH; Xiao, JH; Huang, MX; Chen, B; Liu, B; Li, XL; Jiang, H.  ANTIMICROBIAL AGENTS AND CHEMOTHERAPY, 2010; 54(8): 3535-3535.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
