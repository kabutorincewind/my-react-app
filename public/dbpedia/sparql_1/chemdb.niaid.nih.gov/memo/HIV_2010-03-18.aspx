

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-03-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ouASImZiiEJeqbuH0phjl9d1TsdWPJDKxecfdDs29hGvlUZwjM3jwGe89TF8SHQjDQDZhC0AuzzfKEllhf/AEjuykka1FnJpxXDRbCs0KpDnMWQ5DaBIfMfWQNA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CF57772A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 4 - March 18, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20231780">The design, synthesis and antiviral evaluation of a series of 5-trimethylsilyl-1-beta-D-(arabinofuranosyl)uracil phosphoramidate ProTides.</a> Mehellou Y, Balzarini J, McGuigan C. Antivir Chem Chemother. 2010 Mar 9;20(4):153-60.PMID: 20231780</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20231400">Persistent interactions between the biguanide-based compound NB325 and CXCR4 result in prolonged inhibition of human immunodeficiency virus type 1 infection.</a> Thakkar N, Pirrone V, Passic S, Keogan S, Zhu W, Kholodovych V, Welsh W, Rando R, Labib M, Wigdahl B, Krebs FC. Antimicrob Agents Chemother. 2010 Mar 15. [Epub ahead of print]PMID: 20231400</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>3.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274709300030">Design, synthesis, and biological evaluation of novel substituted [12,3]triazolo[4,5-d]pyrimidines as HIV-1 Tat-TAR interaction inhibitors.</a>  Yu, F; Pang, RF; Yuan, DK; He, MZ; Zhang, CL; Chen, SG; Yang, M.  PURE AND APPLIED CHEMISTRY, 2010; 82(1): 339-347.</p>

    <p>4.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274599500081">A broad-spectrum antiviral targeting entry of enveloped viruses.</a>  Wolf, MC; Freiberg, AN; Zhang, TH; Akyol-Ataman, Z; Grock, A; Hong, PW; Li, JR; Watson, NF; Fang, AQ; Aguilar, HC; Porotto, M; Honko, AN; Damoiseaux, R; Miller, JP; Woodson, SE; Chantasirivisal, S; Fontanes, V; Negrete, OA; Krogstad, P; Dasgupta, A; Moscona, A; Hensley, LE; Whelan, SP; Faull, KF; Holbrook, MR; Jung, ME; Lee, B.  PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA, 2010; 107(7): 3157-3162.</p>

    <p>5.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274714600005">Design, synthesis, and evaluation of indole compounds as novel inhibitors targeting Gp41.</a>  Zhou, GY; Wu, D; Hermel, E; Balogh, E; Gochin, M.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(5): 1500-1503.</p>

    <p>6.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274714600027">N1-Alkyl pyrimidinediones as non-nucleoside inhibitors of HIV-1 reverse transcriptase.</a>  Mitchell, ML; Son, JC; Guo, HY; Im, YA; Cho, EJ; Wang, JH; Hayes, J; Wang, M; Paul, A; Lansdon, EB; Chen, JM; Graupe, D; Rhodes, G; He, GX; Geleziunas, R; Xu, LH; Kim, CU.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(5):1589-1592.</p>

    <p>7.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274748900013">Debio-025 inhibits HIV-1 by interfering with an early event in the replication cycle.</a>  Daelemans, D; Dumont, JM; Rosenwirth, B; De Clercq, E; Pannecouque, C. ANTIVIRAL RESEARCH, 2010; 85(2): 418-421.</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274733300056">Identification of Novel Human Immunodeficiency Virus Type 1-Inhibitory Peptides Based on the Antimicrobial Peptide Database.</a>  Wang, GS; Watson, KM; Peterkofsky, A; Buckheit, RW.  ANTIMICROBIAL AGENTS AND CHEMOTHERAPY, 2010; 54(3): 1343-1346.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
