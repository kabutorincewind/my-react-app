

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2015-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dlpCIH2s9grqDG7MOfjppR+nNhIqS4Afn0G4d1O2j3YF5z2SpQxN3qtfK8K4bzM4PhLN1s7RNVspbKHJaow7v2gX0WF49YaOm+lp3Aqp9+OzvvxlIvho/51rT4c=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="26280557" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: December, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26631439">Pyrazole Clubbed Triazolo[1,5-a]pyrimidine Hybrids as an Anti-tubercular Agents: Synthesis, in Vitro Screening and Molecular Docking Study.</a> Bhatt, J.D., C.J. Chudasama, and K.D. Patel. Bioorganic &amp; Medicinal Chemistry, 2015. 23(24): p. 7711-7716. PMID[26631439].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26628019">Investigation of Local Anesthetic and Antimycobacterial Activity of Ottonia martiana Miq. (Piperaceae).</a> Cunico, M.M., H.A. Trebien, F.C. Galetti, O.G. Miguel, M.D. Miguel, C.G. Auer, C.L. Silva, and A.O. Souza. Anais da Academia Brasileira de Ciencias, 2015. 87(4): p. 1991-2000. PMID[26628019].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26299907">Discovery of Novel Lysine Varepsilon-aminotransferase Inhibitors: An Intriguing Potential Target for Latent Tuberculosis.</a> Devi, P.B., J.P. Sridevi, S.S. Kakan, S. Saxena, V.U. Jeankumar, V. Soni, H.S. Anantaraju, P. Yogeeswari, and D. Sriram. Tuberculosis, 2015. 95(6): p. 786-794. PMID[26299907].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26713110">Cefsulodin Inspired Potent and Selective Inhibitors of mPTPB a Virulent Phosphatase from Mycobacterium tuberculosis.</a> He, R., Z.H. Yu, R.Y. Zhang, L. Wu, A.M. Gunawan, and Z.Y. Zhang. ACS Medicinal Chemistry Letters, 2015. 6(12): p. 1231-1235. PMID[26713110]. PMCID[PMC4677373].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26712622">Evidences for Anti-mycobacterium Activities of Lipids and Surfactants.</a> Hussain, A. and S.K. Singh. World Journal of Microbiology and  Biotechnology, 2016. 32(1): p. 7. PMID[26712622].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26643218">Discovery of Benzothiazoles as Antimycobacterial Agents: Synthesis, Structure-Activity Relationships and Binding Studies with Mycobacterium tuberculosis Decaprenylphosphoryl-beta-D-ribose 2&#39;-oxidase.</a> Landge, S., A.B. Mullick, K. Nagalapur, J. Neres, V. Subbulakshmi, K. Murugan, A. Ghosh, C. Sadler, M.D. Fellows, V. Humnabadkar, J. Mahadevaswamy, P. Vachaspati, S. Sharma, P. Kaur, M. Mallya, S. Rudrapatna, D. Awasthy, V.K. Sambandamurthy, F. Pojer, S.T. Cole, T.S. Balganesh, B.G. Ugarkar, V. Balasubramanian, B.S. Bandodkar, M. Panda, and V. Ramachandran. Bioorganic &amp; Medicinal Chemistry, 2015. 23(24): p. 7694-7710. PMID[26643218].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26438491">Roflumilast, a Type 4 Phosphodiesterase Inhibitor, Shows Promising Adjunctive, Host-directed Therapeutic Activity in a Mouse Model of Tuberculosis.</a> Maiga, M.C., B.A. Ahidjo, M. Maiga, and W.R. Bishai. Antimicrobial Agents and Chemotherapy, 2015. 59(12): p. 7888-7890. PMID[26438491]. PMCID[PMC4649180].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26520663">New INH-Pyrazole Analogs: Design, Synthesis and Evaluation of Antitubercular and Antibacterial Activity.</a> Nayak, N., J. Ramprasad, and U. Dalimba. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(23): p. 5540-5545. PMID[26520663].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26544629">Structure Activity Relationships of 4-Hydroxy-2-pyridones: A Novel Class of Antituberculosis Agents.</a> Ng, P.S., U.H. Manjunatha, S.P. Rao, L.R. Camacho, N.L. Ma, M. Herve, C.G. Noble, A. Goh, S. Peukert, T.T. Diagana, P.W. Smith, and R.R. Kondreddi. European Journal of Medicinal Chemistry, 2015. 106: p. 144-156. PMID[26544629].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">10. <a href="../../../../../../../../elauer/Documents/AIDS/OI%20Memo/.%20http:/www.ncbi.nlm.nih.gov/pubmed/26410242">Identification of Chebulinic acid as Potent Natural Inhibitor of M. tuberculosis DNA Gyrase and Molecular Insights into its Binding Mode of Action.</a> Patel, K., C. Tyagi, S. Goyal, S. Jamal, D. Wahi, R. Jain, N. Bharadvaja, and A. Grover. Computational Biology and Chemistry, 2015. 59 p. 37-47. PMID[26410242].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26438493">Esters of Pyrazinoic acid Are Active against Pyrazinamide-resistant Strains of Mycobacterium tuberculosis and Other Naturally Resistant Mycobacteria in Vitro and Ex Vivo within Macrophages.</a> Pires, D., E. Valente, M.F. Simoes, N. Carmo, B. Testa, L. Constantino, and E. Anes. Antimicrobial Agents and Chemotherapy, 2015. 59(12): p. 7693-7699. PMID[26438493]. PMCID[PMC4649235].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26551248">Pyrrolo[3,4-c]pyridine-1,3(2H)-diones: A Novel Antimycobacterial Class Targeting Mycobacterial Respiration.</a> van der Westhuyzen, R., S. Winks, C.R. Wilson, G.A. Boyle, R.K. Gessner, C. Soares de Melo, D. Taylor, C. de Kock, M. Njoroge, C. Brunschwig, N. Lawrence, S.P. Rao, F. Sirgel, P. van Helden, R. Seldon, A. Moosa, D.F. Warner, L. Arista, U.H. Manjunatha, P.W. Smith, L.J. Street, and K. Chibale. Journal of Medicinal Chemistry, 2015. 58(23): p. 9371-9381. PMID[26551248].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26395151">Determination of in Vitro Synergy between Linezolid and Other Antimicrobial Agents against Mycobacterium tuberculosis Isolates.</a> Zou, L., M. Liu, Y. Wang, J. Lu, and Y. Pang. Tuberculosis, 2015. 95(6): p. 839-842. PMID[26395151].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_12_2015.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365355400016">Antibacterial Activity of Newly Synthesized 5-Hydrazono-triazoloquinazolines.</a> Al-Salahi, R., E.D. Rabab, and M. Mohamed. Latin American Journal of Pharmacy, 2015. 34(8): p. 1594-1600. ISI[000365355400016].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365979900015">Synthesis and Evaluation of Thieno[2,3-d]pyrimidin-4(3H)-ones as Potential Antitubercular Agents.</a> Borate, H.B., R.A. Annadate, S.S. Vagh, M.M. Pisal, S.B. Deokate, M.A. Arkile, N.J. Jadhav, L.U. Nawale, and D. Sarkar. MedChemComm, 2015. 6(12): p. 2209-2215. ISI [000365979900015].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26522089">Aminopyrazolo[1,5-a]pyrimidines as Potential Inhibitors of Mycobacterium tuberculosis: Structure Activity Relationships and ADME Characterization.</a> Candice, S.D., T.S. Feng, R. van der Westhuyzen, R.K. Gessner, L.J. Street, G.L. Morgans, D.F. Warner, A. Moosa, K. Naran, N. Lawrence, H.I.M. Boshoff, C.E. Barry, C.J. Harris, R. Gordon, and K. Chibale. Bioorganic &amp; Medicinal Chemistry, 2015. 23(22): p. 7240-7250. PMID[26522089].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365363100002">Pyrazolyl Pd(II) Complexes Containing Triphenylphosphine: Synthesis and Antimycobacterial Activity.</a> da Silva, C., L.B. Ribeiro, C.C. Furuno, G.A. da Cunha, R.F.F. de Souza, A.V.G. Netto, A.E. Mauro, R.C.G. Frem, J.A. Fernandes, F.A.A. Paz, L.B. Marino, F.R. Pavan, and C.Q.F. Leite. Polyhedron, 2015. 100: p. 10-16. ISI[000365363100002].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26603226">Chemical Characterization and Evaluation of Antibacterial, Antifungal, Antimycobacterial, and Cytotoxic Activities of Talinum paniculatum.</a> Dos Reis, L.F.C., C.D. Cerdeira, B.F. De Paula, J.J. da Silva, L.F.L. Coelho, M.A. Silva, V.B.B. Marques, J.K. Chavasco, and G. Alves-Da-Silva. Revista do Instituto de Medicina Tropical de Sao Paulo, 2015. 57(5): p. 397-405. PMID[26603226]. PMCID[PMC4660448].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365366500015">Antimalarial and Antimycobacterial Agents from Streptomyces sp BCC27095.</a> Intaraudom, C., N. Bunbamrung, A. Dramae, K. Danwisetkanjana, P. Rachtawee, and P. Pittayakhajonwut. Tetrahedron Letters, 2015. 56(49): p. 6875-6877. ISI[000365366500015].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365220100002">SAR Studies on 1,2,4-Triazolo[3,4-b]1,3,4 thiadiazoles as Inhibitors of Mtb Shikimate Dehydrogenase for the Development of Novel Antitubercular Agents.</a> Li, Z.Q., Y.S. Liu, X.G. Bai, Q. Deng, J.X. Wang, G.N. Zhang, C.L. Xiao, Y.N. Mei, and Y.C. Wang. RSC Advances, 2015. 5(118): p. 97089-97101. ISI[000365220100002].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365911500001">Synthesis and Biological Evaluation of New Hydrazone Derivatives of Quinoline and Their Cu(II) and Zn(II) Complexes against Mycobacterium tuberculosis.</a> Mandewale, M.C., B. Thorat, D. Shelke, and R. Yamgar. Bioinorganic Chemistry and Applications, 2015. 153015: 14pp. ISI[000365911500001].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365979900013">Antimycobacterial Activity of Rhodamine 3,4-HPO Iron Chelators against Mycobacterium avium: Analysis of the Contribution of Functional Groups and of Chelator&#39;s Combination with Ethambutol.</a> Moniz, T., D. Silva, T. Silva, M.S. Gomes, and M. Rangel. MedChemComm, 2015. 6(12): p. 2194-2203. ISI[000365979900013].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26520841">Design of New Phenothiazine-Thiadiazole Hybrids via Molecular Hybridization Approach for the Development of Potent Antitubercular Agents.</a> Ramprasad, J., N. Nayak, and U. Dalimba. European Journal of Medicinal Chemistry, 2015. 106: p. 75-84. PMID[26520841].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000365048200007">Synthesis of an Indoloquinoxaline Derivative as Potential Inhibitor of INHA Enzyme of Mycobacterium tuberculosis.</a> Zanzoul, A., A. Chollet, E. Piedra-Arroni, J.L. Stigliani, V. Bernardes-Genisson, E. Essassi, and G. Pratviel. Letters in Organic Chemistry, 2015. 12(10): p. 727-733. ISI[000365048200007].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_12_2015.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151203&amp;CC=US&amp;NR=2015342984A1&amp;KC=A1">Antimicrobial Cationic Polyamines.</a> Cheng, W., X. Ding, J.M. Garcia, J.L. Hedrick, C. Yang, and Y.Y. Yang. Patent. 2015. 2014-14288782 20150342984: 61pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151203&amp;CC=WO&amp;NR=2015181837A2&amp;KC=A2">Novel Compounds as Antitubercular Agents.</a> Dugar, S., D. Mahajan, S.K. Rai, K. Rao, and V. Singh. Patent. 2015. 2015-IN226 2015181837: 80pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151203&amp;CC=US&amp;NR=2015344501A1&amp;KC=A1">Inhibitors of Udp-galactopyranose Mutase.</a> Kiessling, L.L., V.A. Kincaid, N. London, and B.K. Shoichet. Patent. 2015. 2015-14728914 20150344501: 50pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151210&amp;CC=US&amp;NR=2015353572A1&amp;KC=A1">Preparation of 1,3-Benzothiazinone, Sulfoxide, and Sulfone Compounds with Electrophilic Substituent as Antibacterial and Anti-tuberculosis Agents.</a> Miller, M.J. and R. Tiwari. Patent. 2015. 2015-14734657 20150353572: 23pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151210&amp;CC=US&amp;NR=2015353571A1&amp;KC=A1">Preparation of 1,3-Benzothiazinone Sulfoxide and Sulfone Compounds as Anti-tuberculosis Agents.</a> Miller, M.J. and R. Tiwari. Patent. 2015. 2015-14734095 20150353571: 24pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151203&amp;CC=WO&amp;NR=2015181799A1&amp;KC=A1">Preparation of Pyrimidine isoxazole amide Compounds for Treating Tuberculosis.</a> Ratan Kale, R., P. Madhavapeddi, S. Raghunath Ghorpade, E. Vithalrao Bellale, M. Ganpat Kale, A. Raichurkar, S. Landge, S. Douglas Cowen, J. Anthony Read, P. Iyer, and S. Narayanan. Patent. 2015. 2015-IB54076 2015181799: 35pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2015.</p>

    <br />

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150819&amp;CC=MX&amp;NR=2014001998A&amp;KC=A">Use of Synthetic Peptides as Antibiotics against Mycobacterium tuberculosis and Further Pathogen Bacteria.</a> Rodriguez Solis, A.J., E.C. Villegas Villarreal, and G.A. Corzo Burguete. Patent. 2015. 2014-1998 2014001998: 27pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_12_2015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
