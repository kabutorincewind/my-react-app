

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-07-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+KxHjYJl+pPztcMVJOmhSFMuw3u68nVvFKDxa5wbBu1KB3ACXKHRPJtWEmRYymq4nmSaIdQPC8vQDYv1ySgdDMHb5JODXM09/dNdm0jiGfn/tdnqPN0JNEHQuxk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EAFCF0E2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: July 5 - July 18, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23857507">Telbivudine and Adefovir Combination Therapy for Patients with Chronic Lamivudine-resistant Hepatitis B Virus Infections</a><i>.</i> Lin, M.T., Y.P. Chou, T.H. Hu, H.C. Yu, Y.C. Hsu, M.C. Tsai, P.L. Tseng, K.C. Chang, Y.H. Yen, and K.W. Chiu. Archives of Virology, 2013. <b>[Epub ahead of print]</b>; PMID[23857507].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23853601">Inhibition of Hepatitis B Virus Replication by the Host Zinc Finger Antiviral Protein<i>.</i></a> Mao, R., H. Nie, D. Cai, J. Zhang, H. Liu, R. Yan, A. Cuconati, T.M. Block, J.T. Guo, and H. Guo. Plos Pathogens, 2013. 9(7): p. e1003494; PMID[23853601].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23685181">Novel Artemisinin Derivatives with Potential Usefulness against Liver/Colon Cancer and Viral Hepatitis<i>.</i></a> Blazquez, A.G., M. Fernandez-Dolon, L. Sanchez-Vicente, A.D. Maestre, A.B. Gomez-San Miguel, M. Alvarez, M.A. Serrano, H. Jansen, T. Efferth, J.J. Marin, and M.R. Romero. Bioorganic &amp; Medicinal Chemistry, 2013. 21(14): p. 4432-4441; PMID[23685181].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23852269">Bastos, J.C.S.; Kohn, L.K.; Fantinatti-Garboggini, F.; Padilla, M.A.; Flores, E.F.; da Silva, B.P.; de Menezes, C.B.; and Arns, C.W. Antiviral Activity of Bacillus Sp. Isolated from the Marine Sponge Petromica Citrina against Bovine Viral Diarrhea Virus, a Surrogate Model of the Hepatitis C Virus.</a> Viruses 2013, 5, 1219-1230<i>.</i> Bastos, J.C., L.K. Kohn, F. Fantinatti-Garboggini, M.A. Padilla, E.F. Flores, B.P. da Silva, C.B. de Menezes, and C.W. Arns. Viruses, 2013. 5(7): p. 1682-1683; PMID[23852269].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23768906">Structure-based Design of Novel HCV NS5B Thumb Pocket 2 Allosteric Inhibitors with Submicromolar gt1 Replicon Potency: Discovery of a Quinazolinone Chemotype<i>.</i></a> Beaulieu, P.L., R. Coulombe, J. Duan, G. Fazal, C. Godbout, O. Hucke, A. Jakalian, M.A. Joly, O. Lepage, M. Llinas-Brunet, J. Naud, M. Poirier, N. Rioux, B. Thavonekham, G. Kukolj, and T.A. Stammers. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(14): p. 4132-4140; PMID[23768906].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23735741">Synthesis and Optimization of a Novel Series of HCV NS3 Protease Inhibitors: 4-Arylproline Analogs<i>.</i></a> Bilodeau, F., M.D. Bailey, P.K. Bhardwaj, J. Bordeleau, P. Forgione, M. Garneau, E. Ghiro, V. Gorys, T. Halmos, E.S. Jolicoeur, M. Leblanc, C.T. Lemke, J. Naud, J. O&#39;Meara, P.W. White, and M. Llinas-Brunet. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(14): p. 4267-4271; PMID[23735741].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23824794">High-throughput Profiling of IFNalpha and IL-28B Regulated Micrornas and Identification of let-7s with anti-HCV Activity by Targeting IGF2BP1<i>.</i></a> Cheng, M., Y. Si, Y. Niu, X. Liu, X. Li, J. Zhao, Q. Jin, and W. Yang. Journal of Virology, 2013. <b>[Epub ahead of print]</b>; PMID[23824794].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23848533">A Novel Promising Therapeutic Option against Hepatitis C Virus: An Oral Nucleotide NS5B Polymerase Inhibitor Sofosbuvir<i>.</i></a> Gentile, I., F. Borgia, A.R. Buonomo, G. Castaldo, and G. Borgia. Current Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[23848533].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23836176">Restoration of the Activated Rig-I Pathway in HCV Replicon Cells by HCV Protease, Polymerase and NS5A Inhibitors in Vitro at Clinically Relevant Concentrations<i>.</i></a> Kalkeri, G., C. Lin, J. Gopilan, K. Sloan, R. Rijnbrand, and A.D. Kwong. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23836176].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23845899">Fetal Bovine Serum Inhibits Hepatitis C Virus Attachment to Host Cells<i>.</i></a> Qin, Z.L., H.P. Ju, Y. Liu, T.T. Gao, W.B. Wang, L. Aurelian, P. Zhao, and Z.T. Qi. Journal of Virological Methods, 2013. <b>[Epub ahead of print]</b>; PMID[23845899].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23703611">Primuline Derivatives That Mimic RNA to Stimulate Hepatitis C Virus NS3 Helicase-Catalyzed ATP Hydrolysis<i>.</i></a> Sweeney, N.L., W.R. Shadrick, S. Mukherjee, K. Li, K.J. Frankowski, F.J. Schoenen, and D.N. Frick. The Journal of Biological Chemistry, 2013. 288(27): p. 19949-19957; PMID[23703611].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856778">A Peptide Inhibitor of Cytomegalovirus Infection from Human Hemofiltrate<i>.</i></a> Borst, E.M., L. Standker, K. Wagner, T.F. Schulz, W.G. Forssmann, and M. Messerle. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23856778].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23743443">Synthesis and anti-HCMV Activity of 1-[omega-(Phenoxy)alkyl]uracil Derivatives and Analogues Thereof<i>.</i></a> Novikov, M.S., D.A. Babkov, M.P. Paramonova, A.L. Khandazhinskaya, A.A. Ozerov, A.O. Chizhov, G. Andrei, R. Snoeck, J. Balzarini, and K.L. Seley-Radtke. Bioorganic &amp; Medicinal Chemistry, 2013. 21(14): p. 4151-4157; PMID[23743443].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23843639">Hsp90 Inhibitor 17-DMAG Decreases Expression of Conserved Herpesvirus Protein Kinases and Reduces Virus Production in Epstein-Barr Virus-infected Cells<i>.</i></a> Sun, X., J.A. Bristol, S. Iwahori, S.R. Hagemeier, Q. Meng, E.A. Barlow, J.D. Fingeroth, V.L. Tarakanova, R.F. Kalejta, and S.C. Kenney. Journal of Virology, 2013. <b>[Epub ahead of print]</b>; PMID[23843639].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23841834">PI3Kdelta Inhibition Augments the Efficacy of Rapamycin in Suppressing Proliferation of Epstein-Barr Virus (EBV)+ B Cell Lymphomas<i>.</i></a> Furukawa, S., L. Wei, S.M. Krams, C.O. Esquivel, and O.M. Martinez. American Journal of Transplantation, 2013. <b>[Epub ahead of print]</b>; PMID[23841834].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23727195">6&#39;-Methyl-5&#39;-homoaristeromycin: A Structural Variation of the Anti-orthopox Virus Candidate 5&#39;-Homoaristeromycin<i>.</i></a> Yang, M., W. Ye, and S.W. Schneller. Bioorganic &amp; Medicinal Chemistry, 2013. 21(14): p. 4374-4377; PMID[23727195].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0705-071813.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319795500017">Different Molecular Mechanisms of Inhibition of Bovine Viral Diarrhea Virus and Hepatitis C Virus RNA-dependent RNA Polymerases by a Novel Benzimidazole<i>.</i></a> Asthana, S., S. Shukla, A.V. Vargiu, M. Ceccarelli, P. Ruggerone, G. Paglietti, M.E. Marongiu, S. Blois, G. Giliberti, and P. La Colla. Biochemistry, 2013. 52(21): p. 3752-3764; ISI[000319795500017].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319711900023">Rapid and Convenient Assays to Assess Potential Inhibitory Activity on in Vitro Hepatitis a Replication<i>.</i></a> Debing, Y., G.G. Kaplan, J. Neyts, and D. Jochmans. Antiviral Research, 2013. 98(2): p. 325-331; ISI[000319711900023].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319330200125">A Novel Monoclonal anti-CD81 Antibody Produced by Genetic Immunization Efficiently Inhibits Hepatitis C Virus Cell-Cell Transmission<i>.</i></a> Fofana, I., F. Xiao, C. Thumann, M. Turek, L. Zona, R.G. Tawar, F. Grunert, J. Thompson, M.B. Zeisel, and T.F. Baumert. Plos One, 2013. 8(5): p. e64221; ISI[000319330200125].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">20<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319508600064">. Inhibition of Calmodulin-dependent Kinase Kinase Blocks Human Cytomegalovirus-induced Glycolytic Activation and Severely Attenuates Production of Viral Progeny (Vol 85, Pg 705, 2011)<i>.</i></a> McArdle, J., X.L. Schafer, and J. Munger. Journal of Virology, 2013. 87(12): p. 7197-7197; ISI[000319508600064].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320511400016">Effect of Baicalein on the Expression of VIP in Extravillous Cytotrophoblasts Infected with Human Cytomegalovirus in Vitro<i>.</i></a> Qiao, Y., J.G. Fang, J. Xiao, T. Liu, J. Liu, Y.L. Zhang, and S.H. Chen. Journal of Huazhong University of Science and Technology-Medical Sciences, 2013. 33(3): p. 406-411; ISI[000320511400016].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319739200002">All-oral HCV Therapies near Approval<i>.</i></a> Tse, M.T. Nature Reviews Drug Discovery, 2013. 12(6): p. 409-U17; ISI[000319739200002].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0705-071813.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319508600032">A Herpes Simplex Virus Scaffold Peptide That Binds the Portal Vertex Inhibits Early Steps in Viral Replication<i>.</i></a> Yang, K., E. Wills, and J.D. Baines. Journal of Virology, 2013. 87(12): p. 6876-6887; ISI[000319508600032].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0705-071813.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
