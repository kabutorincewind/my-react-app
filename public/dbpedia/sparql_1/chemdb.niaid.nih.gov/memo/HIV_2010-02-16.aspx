

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-02-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EosNcMUrZNCjMs5iQoKDvZM0C6lmCCdnBdrpmM8nAK/O9bpuTPdByeOfTbOGQRLpDWUHejmNKFOaZTRagHaXI6uvB3Oc+0tMewgx684+HZSm52Cv3R5dOSgKdNI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9DA029B8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 3 - February 16, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20127392?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">HIV therapeutic possibilities of gold compounds.</a>  Fonteh PN, Keter FK, Meyer D.  Biometals. 2010 Feb 3. [Epub ahead of print]PMID: 20127392</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20146529?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Chemical Constituents from the Leaves and Stems of Schisandra rubriflora.</a>  Xiao WL, Yang SY, Yang LM, Yang GY, Wang RR, Zhang HB, Zhao W, Pu JX, Lu Y, Zheng YT, Sun HD.  J Nat Prod. 2010 Feb 10. [Epub ahead of print]PMID: 20146529</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20147291?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">Anti-HIV activity of defective cyanovirin-N mutants is restored by dimerization.</a>  Matei E, Zheng A, Furey W, Rose J, Aiken C, Gronenborn AM.  J Biol Chem. 2010 Feb 10. [Epub ahead of print]PMID: 20147291</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20153375?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">Assessment of the Susceptibility of Mutant HIV-1 to Antiviral Agents.</a>  Wang YJ, McKenna PM, Hrin R, Felock P, Lu M, Jones KG, Coburn CA, Grobler JA, Hazuda DJ, Miller MD, Lai MT.  J Virol Methods. 2010 Feb 10. [Epub ahead of print]PMID: 20153375</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>5.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273745100017">Synthesis and biological evaluation of selective CXCR4 antagonists containing alkene dipeptide isosteres.</a>  Narumi, T; Hayashi, R; Tomita, K; Kobayashi, K; Tanahara, N; Ohno, H; Naito, T; Kodama, E; Matsuoka, M; Oishi, S; Fujii, N.  ORGANIC &amp; BIOMOLECULAR CHEMISTRY, 2010; 8(3): 616-621. </p>

    <p>6.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273751200023">SYNTHESIS OF DIBENZYLBUTANEDIOL LIGNANS AND THEIR ANTI-HIV, ANTI-HSV, ANTI-TUMOR ACTIVITIES.</a>  Xia, YM; Bi, WH; Zhang, YY.  JOURNAL OF THE CHILEAN CHEMICAL SOCIETY, 2009; 54(4): 428-431.</p>

    <p>7.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273843900021">A New Class of Dual-Targeted Antivirals: Monophosphorylated Acyclovir Prodrug Derivatives Suppress Both Human Immunodeficiency Virus Type 1 and Herpes Simplex Virus Type 2.</a>  Vanpouille, C; Lisco, A; Derudas, M; Saba, E; Grivel, JC; Brichacek, B; Scrimieri, F; Schinazi, R; Schols, D; McGuigan, C; Balzarini, J; Margolis, L.  JOURNAL OF INFECTIOUS DISEASES, 2010; 201(4): 635-643.</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273783800102">Synthesis and biological evaluation of novel allophenylnorstatine-based HIV-1 protease inhibitors incorporating high affinity P2-ligands.</a>  Ghosh, AK; Gemma, S; Simoni, E; Baldridge, A; Walters, DE; Ide, K; Tojo, Y; Koh, Y; Amano, M; Mitsuya, H.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(3): 1241-1246.</p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274000000014">Anti-HIV Actions of 7- and 10-Substituted Camptothecins.</a>  Li, YY; Liu, YQ; Yang, LM; Wang, RR; Pang, W; Chen, SW; Tian, X; Zheng, YT.  MOLECULES, 2010; 15(1): 149-149.</p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274015900013">Multivalent Benzoboroxole Functionalized Polymers as gp120 Glycan Targeted Microbicide Entry Inhibitors.</a>  Jay, JI; Lai, BE; Myszka, DG; Mahalingam, A; Langheinrich, K; Katz, DF; Kiser, PF.  MOLECULAR PHARMACEUTICS, 2010; 7(1): 116-129.</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273976500015">Synthesis and Antiretroviral Evaluation of Derivatives of Zidovudine.</a>  Raviolo, MA; Trinchero-Hernandez, JS; Turk, G; Brinon, MC.  JOURNAL OF THE BRAZILIAN CHEMICAL SOCIETY, 2009; 20(10): 1870-U152.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
