

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-335.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uI4e0i9cqqCL2SzNBfg/DrdP6PJGToYmWCWEbx3cOI26+2PM1xiwrx6vbmmRp6+6LjOuNVUDHW55GKcTilRn6kKGy+KOmM5j62hz0uzCJRxq3idhEF8B433JEtQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7EE211B6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-335-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48084   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Effect of Cell Cycle Arrest on the Activity of Nucleoside Analogues against Human Immunodeficiency Virus Type 1</p>

    <p>          Wurtzer, S, Compain, S, Benech, H, Hance, AJ, and Clavel, F</p>

    <p>          J Virol <b>2005</b>.  79(23): 14815-14821</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16282481&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16282481&amp;dopt=abstract</a> </p><br />

    <p>2.     48085   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Preparation of carbocyclic cyclopropyl nucleoside phosphonate derivatives useful in the treatment of HIV infections</p>

    <p>          Averett, Devron R</p>

    <p>          PATENT:  WO <b>2005079812</b>  ISSUE DATE:  20050901</p>

    <p>          APPLICATION: 2005  PP: 97 pp.</p>

    <p>          ASSIGNEE:  (Anadys Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     48086   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Synthesis and Biological Activity of Novel Acyclic Versions of NeplanocinA</p>

    <p>          Wu, Y and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16281309&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16281309&amp;dopt=abstract</a> </p><br />

    <p>4.     48087   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Research on anti-HIV activity of polytungstate</p>

    <p>          Li, Li and Lu, Xiaoming</p>

    <p>          Huaxue Tongbao  <b>2005</b>.  68(6): 438-443</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48088   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Specific Targeting Highly Conserved Residues in the HIV-1 Reverse Transcriptase Primer Grip Region. Design, Synthesis, and Biological Evaluation of Novel, Potent, and Broad Spectrum NNRTIs with Antiviral Activity</p>

    <p>          Fattorusso, C, Gemma, S, Butini, S, Huleatt, P, Catalanotti, B, Persico, M, De, Angelis M, Fiorini, I, Nacci, V, Ramunno, A, Rodriquez, M, Greco, G, Novellino, E, Bergamini, A, Marini, S, Coletta, M, Maga, G, Spadari, S, and Campiani, G</p>

    <p>          J Med Chem <b>2005</b>.  48(23): 7153-7165</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16279773&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16279773&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48089   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Selective elimination of HIV-1-infected cells by Env-directed, HIV-1-based virus-like particles</p>

    <p>          Peretti, S, Schiavoni, I, Pugliese, K, and Federico, M</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16271741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16271741&amp;dopt=abstract</a> </p><br />

    <p>7.     48090   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          New trends in synthesis of pyrazole nucleosides as new antimetabolites</p>

    <p>          Elgemeie, GH, Zaghary, WA, Amin, KM, and Nasr, TM</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(8): 1227-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16270665&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16270665&amp;dopt=abstract</a> </p><br />

    <p>8.     48091   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Use of human b defensin or its inducing agents for treating HIV-1 assocd. with chemokine receptor CXCR4</p>

    <p>          Weinberg, Aaron</p>

    <p>          PATENT:  US <b>2005075292</b>  ISSUE DATE:  20050407</p>

    <p>          APPLICATION: 2004-39857  PP: 47 pp., Cont.-in-part of U.S. Ser. No. 737,288.</p>

    <p>          ASSIGNEE:  (Case Western Reserve University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48092   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Case history: Acyclic nucleoside phosphonates: a key class of antiviral drugs</p>

    <p>          De Clercq, E and Holy, A</p>

    <p>          Nat Rev Drug Discov <b>2005</b>.  4(11): 928-940</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16264436&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16264436&amp;dopt=abstract</a> </p><br />

    <p>10.   48093   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Preparation of 2-aminobenzimidazoles for the treatment of HIV/AIDS</p>

    <p>          Schohe-Loop, Rudolf, Paessens, Arnold, Bauser, Marcus, Koebberling, Johannes, Dittmer, Frank, Henninger, Kerstin, Lang, Dieter, and Paulsen, Daniela</p>

    <p>          PATENT:  WO <b>2005019186</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2004  PP: 59 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   48094   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Computer-aided design of non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Jorgensen, WL, Ruiz-Caro, J, Tirado-Rives, J, Basavapathruni, A, Anderson, KS, and Hamilton, AD</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263277&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263277&amp;dopt=abstract</a> </p><br />

    <p>12.   48095   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Preparation of ethynyl 4&#39;-C-substituted-2-halo-adenosine derivatives as antiviral agents</p>

    <p>          Kohgo, Satoru, Mitsuya, Hiroaki, Ohrui, Hiroshi, Kodama, Eiichi, and Matsuoka, Masao</p>

    <p>          PATENT:  CA <b>2502109</b>  ISSUE DATE: 20050924</p>

    <p>          APPLICATION: 2005  PP: 71 pp.</p>

    <p>          ASSIGNEE:  (Yamasa Corporation, Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   48096   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Phosphonate nucleosides useful as active ingredients in pharmaceutical compositions for the treatment of viral infections, and intermediates for their production</p>

    <p>          Herdewijn, Piet, Pannecouque, Christophe, Wu, Tongfei, and De Clercq, Erik</p>

    <p>          PATENT:  WO <b>2005085268</b>  ISSUE DATE:  20050915</p>

    <p>          APPLICATION: 2005  PP: 134 pp.</p>

    <p>          ASSIGNEE:  (K. U. Leuven Research &amp; Development, Belg.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48097   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Indirubin-3&#39;-monoxime, a derivative of a Chinese antileukemia medicine, inhibits P-TEFb function and HIV-1 replication</p>

    <p>          Heredia, Alonso, Davis, Charles, Bamba, Douty, Le, Nhut, Gwarzo, Muhammad Y, Sadowska, Mariola, Gallo, Robert C, and Redfield, Robert R</p>

    <p>          AIDS (London, United Kingdom) <b>2005</b>.  19(18): 2087-2095</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   48098   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Broad spectrum antiinfective potential of xanthohumol from hop (Humulus lupulus L.) in comparison with activities of other hop constituents and xanthohumol metabolites</p>

    <p>          Gerhaeuser, Clarissa</p>

    <p>          Molecular Nutrition &amp; Food Research <b>2005</b>.  49(9): 827-831</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   48099   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          HIV-1 Vif: HIV&#39;s weapon against the cellular defense factor APOBEC3G</p>

    <p>          Kremer, Melanie and Schnierle, Barbara S</p>

    <p>          Current HIV Research <b>2005</b>.  3(4): 339-344</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   48100   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Pharmacophore-Based Design of HIV-1 Integrase Strand-Transfer Inhibitors</p>

    <p>          Barreca, ML, Ferro, S, Rao, A, De Luca, L, Zappala, M, Monforte, AM, Debyser, Z, Witvrouw, M, and Chimirri, A</p>

    <p>          J Med Chem <b>2005</b>.  48(22): 7084-7088</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250669&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250669&amp;dopt=abstract</a> </p><br />

    <p>18.   48101   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of 1-{[2-(phenoxy)ethoxy]methyl}uracil derivatives</p>

    <p>          Novikov, MS, Ozerov, AA, Orlova, YuA, and Buckheit, RW</p>

    <p>          Chemistry of Heterocyclic Compounds (New York, NY, United States) <b>2005</b>.  41(5): 625-629</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>19.   48102   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;-C-methyl-beta-D-ribofuranosylimidazo [4,5-d]-pyridazine derivatives (2-aza-3-deazapurine nucleoside analogues)</p>

    <p>          Leroy, F, Dukhan, D, Durka, M, Chaves, D, Bragnier, N, Storer, R, and Gosselin, G</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 667-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248010&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248010&amp;dopt=abstract</a> </p><br />

    <p>20.   48103   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Homo and heterodimers of ddI, d4T and AZT: influence of (5&#39;-5&#39;) thiolcabonate-carbamate linkage on anti-HIV activity</p>

    <p>          Taourirte, M, Lazrek, HB, Rochdi, A, Vasseur, JJ, and Engels, JW</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 523-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247983&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247983&amp;dopt=abstract</a> </p><br />

    <p>21.   48104   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Borano-nucleotides: new analogues to circumvent HIV-1 RT-mediated nucleoside drug-resistance</p>

    <p>          Alvarez, K, Deval, J, Selmi, B, Barral, K, Boretto, J, Guerreiro, C, Mulard, L, Sarfati, R, and Canard, B</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 419-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247962&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247962&amp;dopt=abstract</a> </p><br />

    <p>22.   48105   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Antiviral activity of steric-block oligonucleotides targeting the HIV-1 trans-activation response and packaging signal stem-loop RNAs</p>

    <p>          Brown, D, Arzumanov, AA, Turner, JJ, Stetsenko, DA, Lever, AM, and Gait, MJ</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 393-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247957&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247957&amp;dopt=abstract</a> </p><br />

    <p>23.   48106   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Tipranavir (Aptivus) for HIV</p>

    <p>          Anon</p>

    <p>          Med Lett Drugs Ther <b>2005</b>.  47(1219): 83-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247341&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247341&amp;dopt=abstract</a> </p><br />

    <p>24.   48107   HIV-LS-335; SCIFINDER-HIV-11/15/2005</p>

    <p class="memofmt1-2">          Cytotoxicity and antiviral activity of a lignan extracted from Larrea divaricata</p>

    <p>          Konigheim, BS, Goleniowski, ME, and Contigiani, MS</p>

    <p>          Drug Design Reviews--Online <b>2005</b>.  2(1): 81-83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   48108   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          A short synthetic route to biologically active (+/-)-daurichromenic acid as highly potent anti-HIV agent</p>

    <p>          Rok, Lee Y and Wang, X</p>

    <p>          Org Biomol Chem <b>2005</b>.  3(21): 3955-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16240013&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16240013&amp;dopt=abstract</a> </p><br />

    <p>26.   48109   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 group M and O isolates by fusion inhibitors</p>

    <p>          Chinnadurai, R, Munch, J, Dittmar, MT, and Kirchhoff, F</p>

    <p>          AIDS <b>2005</b>.  19 (16): 1919-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16227804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16227804&amp;dopt=abstract</a> </p><br />

    <p>27.   48110   HIV-LS-335; PUBMED-HIV-11/14/2005</p>

    <p class="memofmt1-2">          Simultaneous determination of HIV protease inhibitors amprenavir, atazanavir, indinavir, lopinavir, nelfinavir, ritonavir and saquinavir in human plasma by high-performance liquid chromatography-tandem mass spectrometry</p>

    <p>          Dickinson, L, Robinson, L, Tjia, J, Khoo, S, and Back, D</p>

    <p>          J Chromatogr B Analyt Technol Biomed Life Sci <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16226495&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16226495&amp;dopt=abstract</a> </p><br />

    <p>28.   48111   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Statins could be used to control replication of some viruses, including HIV-1</p>

    <p>          Gilbert, C, Bergeron, M, Methot, S, Giguere, JF, and Tremblay, MJ</p>

    <p>          VIRAL IMMUNOLOGY <b>2005</b>.  18(3): 474-489, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232733600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232733600009</a> </p><br />

    <p>29.   48112   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          One-pot synthesis of aryl phosphoramidate derivatives of AZT/d4T as anti-HIV prodrugs</p>

    <p>          Jiang, P, Guo, J, Fu, H, Jiang, YY, and Zhao, YF</p>

    <p>          SYNLETT <b>2005</b>.(16): 2537-2539, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232736100035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232736100035</a> </p><br />

    <p>30.   48113   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Synthesis of 8-chloro-5,5-dioxo[1,2,4]triazolo[4,3-b] [1,4,2]benzodithiazine derivatives with potential anticancer and anti-HIV-1 activities</p>

    <p>          Brzozowski, Z and Saczewski, F</p>

    <p>          POLISH JOURNAL OF CHEMISTRY <b>2005</b>.  79(10): 1659-1666, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232589600011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232589600011</a> </p><br />
    <br clear="all">

    <p>31.   48114   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          New acyclonucleosides: Synthesis and anti-HIV activity</p>

    <p>          Hadj-Bouazza, A, Zerrouki, R, Krausz, P, Laumond, G, Aubertin, AM, and Champavier, Y</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(8): 1249-1263, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900008</a> </p><br />

    <p>32.   48115   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p><b>          Synthesis of novel acyclonucleosides analogs of pyridothienopyrimidine as antiviral agents</b> </p>

    <p>          El-Essawy, FA</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(8): 1265-1276, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900009</a> </p><br />

    <p>33.   48116   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Biologically active bisbenzylisoquinoline alkaloids from the root bark of Epinetrum villosum</p>

    <p>          Otshudi, AL, Apers, S, Pieters, L, Claeys, M, Pannecouque, C, De Clercq, E, Van Zeebroeck, A, Lauwers, S, Frederich, M, and Foriers, A</p>

    <p>          JOURNAL OF ETHNOPHARMACOLOGY <b>2005</b>.  102(1): 89-94, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232709800012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232709800012</a> </p><br />

    <p>34.   48117   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Isolation, synthesis and biological activity of grifolic acid derivatives from the inedible mushroom Albatrellus dispansus</p>

    <p>          Hashimoto, T, Quang, DN, Nukada, M, and Asakawa, Y</p>

    <p>          HETEROCYCLES <b>2005</b>.  65(10): 2431-2439, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232582400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232582400010</a> </p><br />

    <p>35.   48118   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Clinical pharmacokinetics and summary of efficacy and tolerability of atazanavir</p>

    <p>          Le Tiec, C, Barrail, A, Goujard, U, and Taburet, AM</p>

    <p>          CLINICAL PHARMACOKINETICS <b>2005</b>.  44(10): 1035-1050, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232788400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232788400003</a> </p><br />

    <p>36.   48119   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of trisubstituted (3 &#39; R, 4 &#39; R)-3 &#39;,4 &#39;-di-O-(S)-camphanoyl-(+)-cis-khellactone (DCK) analogs</p>

    <p>          Zhao, CH, Zhou, T, Xie, L, Li, JY, Bao, ZY, Lou, ZW, and Lee, KH</p>

    <p>          CHINESE CHEMICAL LETTERS <b>2005</b>.  16(10): 1297-1300, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232638500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232638500006</a> </p><br />

    <p>37.   48120   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          HIV cohort collaborations: proposal for harmonization of data exchange</p>

    <p>          Kjaer, J and Ledergerber, B</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): 631-633, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600020</a> </p><br />
    <br clear="all">

    <p>38.   48121   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Novel small-molecule compounds which inhibit strand transfer activity of HIV-1 integrase</p>

    <p>          Yan, H, Chiba, T, Kitamura, Y, Nishizawa, M, Fujino, M, Yamamoto, N, and Sugiura, W</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U23-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600026</a> </p><br />

    <p>39.   48122   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          In vitro development of resistance against styrylquinolines of HIV-1 by emergence of integrase mutations</p>

    <p>          Bonnenfant, S, Zouhiri, F, Cheret, A, and Leh, H</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U24-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600027</a> </p><br />

    <p>40.   48123   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          In vitro selection and characterization of resistance to the new HIV protease inhibitor GW640385</p>

    <p>          Yates, P, Hazen, R, St, Clair M, Boone, L, and Elston, R</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U28-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600036</a> </p><br />

    <p>41.   48124   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Antiviral activity and resistance profile of AG-001859, a novel HIV-1 protease inhibitor with potent activity against protease inhibitor-resistant strains of HIV</p>

    <p>          Hammond, J, Jackson, L, Graham, J, Knowles, S, Digits, J, Tatlock, J, Jewell, T, Canan-Koch, S, and Patick, AK</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U29-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600037</a> </p><br />

    <p>42.   48125   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Highly potent HIV protease inhibitors with broad activity against MDR strains</p>

    <p>          Gulnik, SV, Afonina, E, Yu, B, Eissenstat, M, Guerassina, T, Silva, AM, and Erickson, JW</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U29-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600038</a> </p><br />

    <p>43.   48126   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Mutations in HIV-1 RNase H domain confer high-level resistance to nucleoside reverse transcriptase inhibitors and provide novel insights into the mechanism of nucleotide excision-mediated drug resistance</p>

    <p>          Nikolenko, GN, Palmer, S, Maldarelli, F, Mellors, JW, Coffin, JM, and Pathak, VK</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U35-U36, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600044</a> </p><br />

    <p>44.   48127   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Dinucleoside polyphosphates are novel inhibitors of HIV-1 reverse transcriptase with increased potency against enzymes containing AZT-resistance mutations</p>

    <p>          Meyer, P, Smith, A, Matsuura, S, and Scott, W</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U41-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600055</a> </p><br />

    <p>45.   48128   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Susceptibility to saquinavir and atazanavir in highly protease inhibitor (PI) resistant HIV-1 is caused by lopinavir-induced drug resistance mutation L76V</p>

    <p>          Mueller, SM, Daeumer, M, Kaiser, R, Walter, H, Colonno, R, and Korn, K</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U44-U45, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600062">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600062</a> </p><br />

    <p>46.   48129   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          No evidence for stavudine resistance due to nevirapine-selected mutation Y181C in HIV-1 reverse transcriptase in a large genotype/phenotype database</p>

    <p>          Korn, K, Schmidt, B, and Walter, H</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U47-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600067">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600067</a> </p><br />

    <p>47.   48130   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          A novel method based on recombinant viruses expressing green (EGFP) or red (DsRED) fluorescent proteins to study replicative fitness evolution of nelfinavir-resistant HIV-1 harbouring D30N and L90M mutations in the protease gene</p>

    <p>          Weber, J, Weberova, J, Kiser, P, Kazanjian, P, and Quinones-Mateu, M</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U55-U56, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600077</a> </p><br />

    <p>48.   48131   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Replicative fitness of HIV-1 strains with reduced susceptibility to protease-, reverse transcriptase and entry (enfuvirtide)-inhibitors</p>

    <p>          Chakraborty, B, Weber, J, and Quinones-Mateu, ME</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U59-U60, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600085">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600085</a> </p><br />

    <p>49.   48132   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Previously unclassified mutations at positions experienced treatment failure associated with antiretroviral drug resistance</p>

    <p>          Rhee, SY, Hurley, L, Zolopa, A, Fessel, WJ, Nguyen, DP, Slome, S, Smith, S, Klein, D, Horberg, M, Flamm, J, Follansbee, S, and Shafer, RW</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(4): U109-U110, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600164">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615600164</a> </p><br />

    <p>50.   48133   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Interaction of nucleoside inhibitors of HIV-1 reverse transcriptase with the concentrative nucleoside transporter-1 (SLC28A1)</p>

    <p>          Cono-Soldodo, P, Larrayoz, IM, Molina-Arcas, M, Casado, FJ, Martinez-Picado, J, Lostao, MP, and Pastor-Anglada, M</p>

    <p>          ANTIVIRAL THERAPY <b>2004</b>.  9(6): 993-1002, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231616000016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231616000016</a> </p><br />
    <br clear="all">

    <p>51.   48134   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Impact of newly available drugs on clinical progression in patients with virological failure after exposure to three classes of antiretrovirals</p>

    <p>          Costagliola, D, Potard, V, Duvivier, C, Pradier, C, Dupont, C, Salmon, D, and Duval, X</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): 563-573, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100012</a> </p><br />

    <p>52.   48135   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Relation between the antiretroviral activity of didanosine (ddI) and the number of reverse tran-scriptase (RT) mutations: dINAM study</p>

    <p>          Blanco, JL, Biglia, A, Arnedo, M, de Lazzari, E, Mallolas, J, Martinez, E, Lonca, M, Laguno, M, Larrousse, M, Leon, A, Milinkovic, A, Garcia, F, Miro, JM, Arnaiz, J, Pumarola, T, and Gatell, JM</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S26-1</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100040</a> </p><br />

    <p>53.   48136   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Divalent metal ion chelating small molecules as novel HIV integrase inhibitors</p>

    <p>          Jegede, O, Weber, J, Rajagopalan, R, Wawro, WJ, Babu, JS, and Quinones-Mateu, ME</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S83-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100092">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100092</a> </p><br />

    <p>54.   48137   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          An HIV-1 RNase H inhibitor synergizes with three different classes of RT polymerase inhibitors in an in vitro reverse transcription assay</p>

    <p>          Miller, MD, Feuston, B, Munshi, V, Getty, K, Krueger, J, Hazuda, DJ, Parniak, MA, Lewis, D, Grobler, JA, and Shaw-Reid, CA</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S90-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100096">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100096</a> </p><br />

    <p>55.   48138   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Substrate dependent inhibition or activation of HIV RNase H activity by non-nucleoside reverse transcriptase inhibitors (NNRTIs)</p>

    <p>          Hang, JO, Yang, Y, Li, Y, Tsing, S, Barnett, J, Cammack, N, and Klumpp, K</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S97-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100103">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100103</a> </p><br />

    <p>56.   48139   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Role of HIV-1 protease residues 71 and 89 in protease inhibitor resistance of subtype G viruses</p>

    <p>          Gonzalez, LMF, Van Laethem, K, Abecasis, AB, Soares, EAJM, Deforche, K, Vandamme, AM, Camacho, R, and Soares, MA</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S112-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100118">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100118</a> </p><br />
    <br clear="all">

    <p>57.   48140   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Selection of resistance following first-line antiretroviral regimens among HIV-1 subtypes</p>

    <p>          Kantor, R, DeLong, A, Shafer, RW, Carvalho, AP, Wynhoven, B, Cane, P, Sirivichayakul, S, Soares, MA, Snoeck, J, Rudich, H, Rodrigues, R, Holguin, A, Ariyoshi, K, Bouzas, MB, Cahn, P, Sugiura, W, Soriano, V, Brigido, LF, Grossman, Z, Morris, L, Vandamme, AM, Tanuri, A, Phanuphak, P, Weber, J, Pillay, D, Harrigan, PR, Camacho, R, Schapiro, JM, Hogan, J, and Katzenstein, DA</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S146-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100149">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100149</a> </p><br />

    <p>58.   48141   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Reversion of drug resistant HIV protease mutants is prohibited by a reduction in replication capacity of the intermediate variants</p>

    <p>          van Maarseveen, NM, Wensing, AMJ, de Jong, D, Taconis, M, Boucher, CAB, and Nijhuis, M</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S161-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100162">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100162</a> </p><br />

    <p>59.   48142   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          In vitro microbicide activity of the nonnucleoside reverse transcriptase inhibitor (NNRTI) UC781 against NNRTI-resistant HIV-1</p>

    <p>          Hossain, MM and Parniak, MA</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(4): S170-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100171">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231963100171</a> </p><br />

    <p>60.   48143   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          Selection and characterization of HIV-1 showing reduced susceptibility to the non-peptidic protease inhibitor tipranavir</p>

    <p>          Doyon, L, Tremblay, S, Bourgon, L, Wardrop, E, and Cordingley, MG</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  68(1): 27-35, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232672600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232672600004</a> </p><br />

    <p>61.   48144   HIV-LS-335; WOS-HIV-11/6/2005</p>

    <p class="memofmt1-2">          First-line antiretroviral therapy in Africa - How evidence-based are our recommendations?</p>

    <p>          Colebunders, R, Kamya, MR, Laurence, J, Kambugu, A, Byakwaga, H, Mwebaze, PS, Muganga, AM, Katwere, M, and Katabira, E</p>

    <p>          AIDS REVIEWS <b>2005</b>.  7(3): 148-154, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232700900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232700900003</a> </p><br />

    <p>62.   48145   HIV-LS-335; WOS-HIV-11/13/2005</p>

    <p class="memofmt1-2">          Ester derivatives of nucleoside inhibitors of reverse transcriptase: 2. Molecular systems for the combined therapy with 3 &#39;-azido-3 &#39;-deoxythymidine and 2 &#39;,3 &#39;-didehydro-3 &#39;-deoxythymidine</p>

    <p>          Berezovskaya, YV and Chudinov, MV</p>

    <p>          RUSSIAN JOURNAL OF BIOORGANIC CHEMISTRY <b>2005</b>.  31(5): 405-418, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232932200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232932200001</a> </p><br />
    <br clear="all">

    <p>63.   48146   HIV-LS-335; WOS-HIV-11/13/2005</p>

    <p class="memofmt1-2">          Synthesis, conformation and antiviral activity of nucleoside analogues with the (2-hydroxy-1-phenylethoxy) methyl glycone - a family of nucleoside analogues related to d4T and aciclovir</p>

    <p>          Ewing, DF, Glacon, V, Len, C, and Mackenzie, G</p>

    <p>          NEW JOURNAL OF CHEMISTRY <b>2005</b>.  29(11): 1461-1468, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232846000016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232846000016</a> </p><br />

    <p>64.   48147   HIV-LS-335; WOS-HIV-11/13/2005</p>

    <p class="memofmt1-2">          Study of potential HIV-1 inhibition. Glutaric dialdehyde adducts</p>

    <p>          Jankowski, CK, Martel, J, Fermandjian, S, and Maroun, RG</p>

    <p>          JOURNAL OF MOLECULAR STRUCTURE-THEOCHEM <b>2005</b>.  731(1-3): 83-87, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232847400011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232847400011</a> </p><br />

    <p>65.   48148   HIV-LS-335; WOS-HIV-11/13/2005</p>

    <p class="memofmt1-2">          Hydroxyethylene sulfones as a new scaffold to address aspartic proteases: Design, synthesis, and structural characterization</p>

    <p>          Specker, E, Bottcher, J, Heine, A, Sotriffer, CA, Lilie, H, Schoop, A, Muller, G, Griebenow, N, and Klebe, G</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(21): 6607-6619, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232781600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232781600009</a> </p><br />

    <p>66.   48149   HIV-LS-335; WOS-HIV-11/13/2005</p>

    <p class="memofmt1-2">          Novel approaches in designing anti-HIV microbicides and anti-HIV agents - Editorial</p>

    <p>          Parang, K</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2005</b>.  11(29): 1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232839600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232839600001</a> </p><br />

    <p>67.   48150   HIV-LS-335; WOS-HIV-11/13/2005</p>

    <p class="memofmt1-2">          Indolyl aryl Sulfones (IASs): Development of highly potent NNRTIs active against wt-HIV-1 and clinically relevant drug resistant mutants</p>

    <p>          Silvestri, R and Artico, M</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2005</b>.  11(29): 3779-3806, 28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232839600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232839600006</a> </p><br />

    <p>68.   48151   HIV-LS-335; WOS-HIV-11/13/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of novel anomeric branched carbocyclic nucleosides</p>

    <p>          Kim, A and Hong, JH</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2005</b>.  28(10): 1105-1110, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232930100001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232930100001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
