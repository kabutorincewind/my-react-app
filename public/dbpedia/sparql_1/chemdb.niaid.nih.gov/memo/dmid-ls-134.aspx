

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-134.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="G/ouVngNvmw51rq0DEojR1daoNDVB2JmjRJQUHD9y35Y8yYQoEqhkrgnxLOmTNOv98X9GWnGAflWXmlpxzYRh0NiRobmbHBdJ5K1hqE01e6+3ewCSb3wMZBbHZs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="82CE84FA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-134-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59710   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Antiviral activity obtained from aqueous extracts of the Chilean soapbark tree (Quillaja saponaria Molina)</p>

    <p>          Roner, MR, Sprayberry, J, Spinks, M, and Dhanji, S</p>

    <p>          J Gen Virol <b>2007</b>.  88(Pt 1): 275-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17170461&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17170461&amp;dopt=abstract</a> </p><br />

    <p>2.     59711   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Respiratory syncytial virus fusion inhibitors. Part 4: Optimization for oral bioavailability</p>

    <p>          Yu, KL, Sin, N, Civiello, RL, Wang, XA, Combrink, KD, Gulgeze, HB, Venables, BL, Wright, JJ, Dalterio, RA, Zadjura, L, Marino, A, Dando, S, D&#39;Arienzo, C, Kadow, KF, Cianci, CW, Li, Z, Clarke, J, Genovesi, EV, Medina, I, Lamb, L, Colonno, RJ, Yang, Z, Krystal, M, and Meanwell, NA</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17169560&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17169560&amp;dopt=abstract</a> </p><br />

    <p>3.     59712   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Dengue Virus RNA Polymerase NS5: A Potential Therapeutic Target?</p>

    <p>          Rawlinson, SM, Pryor, MJ, Wright, PJ, and Jans, DA</p>

    <p>          Curr Drug Targets <b>2006</b>.  7(12): 1623-38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168837&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168837&amp;dopt=abstract</a> </p><br />

    <p>4.     59713   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          An Overall Picture of SARS Coronavirus (SARS-CoV) Genome-Encoded Major Proteins: Structures, Functions and Drug Development</p>

    <p>          Chen, S, Luo, H, Chen, L, Chen, J, Shen, J, Zhu, W, Chen, K, Shen, X, and Jiang, H</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(35): 4539-4553</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168760&amp;dopt=abstract</a> </p><br />

    <p>5.     59714   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          SARS-CoV: A Scenario of Modern Drug Design</p>

    <p>          Huang, X</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(35): 4537-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168759&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168759&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59715   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Enantioselective synthesis and antiviral activity of purine and pyrimidine cyclopentenyl C-nucleosides</p>

    <p>          Rao, Jagadeeshwar R, Schinazi, Raymond F, and Chu, Chung K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(2): 839-846</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M6935M-1/2/009d1ee3cfa74deb7a221b2b197f0a33">http://www.sciencedirect.com/science/article/B6TF8-4M6935M-1/2/009d1ee3cfa74deb7a221b2b197f0a33</a> </p><br />

    <p>7.     59716   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          Quaternary structure, substrate selectivity and inhibitor design for SARS 3C-like proteinase</p>

    <p>          Lai, L, Han, X, Chen, H, Wei, P, Huang, C, Liu, S, Fan, K, Zhou, L, Liu, Z, Pei, J, and Liu, Y </p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(35): 4555-64</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168761&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168761&amp;dopt=abstract</a> </p><br />

    <p>8.     59717   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          A mouse cell-adapted NS4B mutation attenuates West Nile virus RNA synthesis</p>

    <p>          Puig-Basagoiti, Francesc, Tilgner, Mark, Bennett, Corey J, Zhou, Yangsheng, Munoz-Jordan, Jorge L, Garcia-Sastre, Adolfo, Bernard, Kristen A, and Shi, Pei-Yong</p>

    <p>          Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4MKV2J5-1/2/75a50f1d20964728af0bc3a291102daf">http://www.sciencedirect.com/science/article/B6WXR-4MKV2J5-1/2/75a50f1d20964728af0bc3a291102daf</a> </p><br />

    <p>9.     59718   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          The mechanism by which influenza A virus nucleoprotein forms oligomers and binds RNA</p>

    <p>          Ye, Q, Krug, RM, and Tao, YJ</p>

    <p>          Nature <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17151603&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17151603&amp;dopt=abstract</a> </p><br />

    <p>10.   59719   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          N-terminal and C-terminal cytosine deaminase domain of APOBEC3G inhibit hepatitis B virus replication</p>

    <p>          Lei, YC, Tian, YJ, Ding, HH, Wang, BJ, Yang, Y, Hao, YH, Zhao, XP, Lu, MJ, Gong, FL, and Yang, DL</p>

    <p>          World J Gastroenterol <b>2006</b>.  12(46): 7488-96</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17167839&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17167839&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59720   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Antiviral Peptides Targeting the West Nile Virus Envelope Protein</p>

    <p>          Bai, F, Town, T, Pradhan, D, Cox, J, Ashish, Ledizet, M, Anderson, JF, Flavell, RA, Krueger, JK, Koski, RA, and Fikrig, E</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17151121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17151121&amp;dopt=abstract</a> </p><br />

    <p>12.   59721   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Some Sugar Arylglycinoylhydrazones and Their Oxadiazoline Derivatives</p>

    <p>          Abdel-Aal, MT, El-Sayed, WA, and El-Ashry, ES</p>

    <p>          Arch Pharm (Weinheim) <b>2006</b>.  339(12): 656-663</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17149795&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17149795&amp;dopt=abstract</a> </p><br />

    <p>13.   59722   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Hepatitis C virus genotype 1b chimeric replicon containing genotype 3 NS5A domain</p>

    <p>          Lanford, Robert E, Guerra, Bernadette, and Lee, Helen</p>

    <p>          Virology <b>2006</b>.  355(2): 192-202</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4KPFKFC-3/2/aaa5b55b7e787210993b31978bf20d5e">http://www.sciencedirect.com/science/article/B6WXR-4KPFKFC-3/2/aaa5b55b7e787210993b31978bf20d5e</a> </p><br />

    <p>14.   59723   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Some Pyrrolo[2,3-d]pyrimidines</p>

    <p>          Rashad, AE, Mohamed, MS, Zaki, ME, and Fatahala, SS</p>

    <p>          Arch Pharm (Weinheim) <b>2006</b>.  339(12): 664-669</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17149793&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17149793&amp;dopt=abstract</a> </p><br />

    <p>15.   59724   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          Short, discontinuous exposure to butyrate effectively sensitizes latently EBV-infected lymphoma cells to nucleoside analogue antiviral agents</p>

    <p>          Ghosh, SK, Forman, LW, Akinsheye, I, Perrine, SP, and Faller, DV</p>

    <p>          Blood Cells Mol Dis <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17161633&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17161633&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   59725   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          Anti-influenza virus agents: Synthesis and mode of action</p>

    <p>          Lagoja, IM and De Clercq, E</p>

    <p>          Med Res Rev <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17160999&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17160999&amp;dopt=abstract</a> </p><br />

    <p>17.   59726   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Inhibition of SARS-CoV Gene Expression by Adenovirus-Delivered Small Hairpin RNA</p>

    <p>          Zhang, X, Wu, K, Yue, X, Zhu, Y, and Wu, J</p>

    <p>          Intervirology <b>2006</b>.  50(2): 63-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139181&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139181&amp;dopt=abstract</a> </p><br />

    <p>18.   59727   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          Application of artificial zinc-finger proteins to inhibition of DNA replication of human papillomavirus</p>

    <p>          Mino, T, Mori, T, Matsumoto, N, Mineta, Y, Okamoto, T, Aoyama, Y, and Sera, T</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2006</b>.(50): 313-4</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150943&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150943&amp;dopt=abstract</a> </p><br />

    <p>19.   59728   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          Anti-SARS-CoV activity of nucleoside analogs having 6-chloropurine as a nucleobase</p>

    <p>          Ikejiri, M, Saijo, M, Morikawa, S, Fukushi, S, Mizutani, T, Kurane, I, and Maruyama, T</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2006</b>.(50): 113-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150843&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150843&amp;dopt=abstract</a> </p><br />

    <p>20.   59729   DMID-LS-134; WOS-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Use of Neuraminidase Inhibitors to Combat Pandemic Influenza</p>

    <p>          Democratis, J., Pareek, M., and Stephenson, I.</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  58(5): 911-915</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242342900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242342900001</a> </p><br />

    <p>21.   59730   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Efficacy of Delayed Treatment of ST-246 Given Orally Against Systemic Orthopoxvirus Infections in Mice</p>

    <p>          Quenelle, DC, Buller, RM, Parker, S, Keith, KA, Hruby, DE, Jordan, R, and Kern, ER</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116683&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116683&amp;dopt=abstract</a> </p><br />

    <p>22.   59731   DMID-LS-134; PUBMED-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Influence of an additional 2-amino substituent of the 1-aminoethyl pharmacophore group on the potency of rimantadine against influenza virus A</p>

    <p>          Tataridis, D, Fytas, G, Kolocouris, A, Fytas, C, Kolocouris, N, Foscolos, GB, Padalko, E, Neyts, J, and De, Clercq E</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113287&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113287&amp;dopt=abstract</a> </p><br />

    <p>23.   59732   DMID-LS-134; WOS-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Conformational Maturation of the Nucleoprotein Synthesized in Influenza C Virus-Infected Cells</p>

    <p>          Sugawara, K. <i>et al.</i></p>

    <p>          Virus Research  <b>2006</b>.  122(1-2): 45-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242227300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242227300006</a> </p><br />

    <p>24.   59733   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          Structural and mechanistic insights into hepatitis C viral translation initiation</p>

    <p>          Fraser, CS and Doudna, JA</p>

    <p>          Nat Rev Microbiol <b>2007</b>.  5(1): 29-38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17128284&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17128284&amp;dopt=abstract</a> </p><br />

    <p>25.   59734   DMID-LS-134; WOS-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Strategies for Developing Vaccines Against H5n1 Influenza a Viruses</p>

    <p>          Horimoto, T. and Kawaoka, Y.</p>

    <p>          Trends in Molecular Medicine <b>2006</b>.  12(11): 506-514</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242065300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242065300002</a> </p><br />

    <p>26.   59735   DMID-LS-134; PUBMED-DMID-12/19/2006</p>

    <p class="memofmt1-2">          Kinetics, inhibition and oligomerization of Epstein-Barr virus protease</p>

    <p>          Buisson, M, Rivail, L, Hernandez, JF, Jamin, M, Martinez, J, Ruigrok, RW, and Burmeister, WP</p>

    <p>          FEBS Lett <b>2006</b>.  580(28-29): 6570-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17118362&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17118362&amp;dopt=abstract</a> </p><br />

    <p>27.   59736   DMID-LS-134; WOS-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Anti-Influenza Prodrug Oseltamivir Is Activated by Carboxylesterase Human Carboxylesterase 1, and the Activation Is Inhibited by Antiplatelet Agent Clopidogrel</p>

    <p>          Shi, D. <i>et al.</i></p>

    <p>          Journal of Pharmacology and Experimental Therapeutics <b>2006</b>.  319(3): 1477-1484</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242048500053">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242048500053</a> </p><br />
    <br clear="all">

    <p>28.   59737   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          The helicase primase inhibitor, BAY 57-1293 shows potent therapeutic antiviral activity superior to famciclovir in BALB/c mice infected with herpes simplex virus type 1</p>

    <p>          Biswas, Subhajit, Jennens, Lyn, and Field, Hugh J</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4MJBPBY-1/2/add9bf1e16ee9f07a1f7915c74da6e19">http://www.sciencedirect.com/science/article/B6T2H-4MJBPBY-1/2/add9bf1e16ee9f07a1f7915c74da6e19</a> </p><br />

    <p>29.   59738   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Synthesis of the 5-phosphono-pent-2-en-1-yl nucleosides: A new class of antiviral acyclic nucleoside phosphonates</p>

    <p>          Choo, Hyunah, Beadle, James R, Chong, Youhoon, Trahan, Julissa, and Hostetler, Karl Y</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4MG0647-1/2/29611aeed1f4914b915b37697bb1b28f">http://www.sciencedirect.com/science/article/B6TF8-4MG0647-1/2/29611aeed1f4914b915b37697bb1b28f</a> </p><br />

    <p>30.   59739   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Synthesis, in vitro and in silico assessment of organometallic Rhenium(I) and Technetium(I) thymidine complexes</p>

    <p>          Stichelberger, M, Desbouis, D, Spiwok, V, Scapozza, L, Schubiger, PA, and Schibli, R</p>

    <p>          Journal of Organometallic Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGW-4M3H7KG-1/2/476467877ca680efc3741f4cc41d1f28">http://www.sciencedirect.com/science/article/B6TGW-4M3H7KG-1/2/476467877ca680efc3741f4cc41d1f28</a> </p><br />

    <p>31.   59740   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-hepatitis B virus activities of some ethyl 5-hydroxy-1H-indole-3-carboxylates</p>

    <p>          Zhao, Chunshen, Zhao, Yanfang, Chai, Huifang, and Gong, Ping</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(8): 2552-2558</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4HPD3M1-3/2/7ec75ce2f15d3dcb04a766c066b96d49">http://www.sciencedirect.com/science/article/B6TF8-4HPD3M1-3/2/7ec75ce2f15d3dcb04a766c066b96d49</a> </p><br />

    <p>32.   59741   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Combination Therapy with Cyclophosphamide and the Oncolytic Herpes Virus Mutant rRp450, Expressing a Cyclophosphamide Prodrug, Is Clinically Safe by Intratumoral, Intravenous, and Intracerebral Administration in an HSV-1 Susceptible Mouse Model</p>

    <p>          Currier, Mark A, Mahller, Yonatan Y, Stroup, Greg, Kambara, Hirokazu, Chiocca, EA, and Cripe, Timothy P</p>

    <p>          Molecular Therapy <b>2006</b>.  13(Supplement 1): S253</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WNJ-4M10KTK-TH/2/95cde33765b29313699a5ca4f8e68422">http://www.sciencedirect.com/science/article/B6WNJ-4M10KTK-TH/2/95cde33765b29313699a5ca4f8e68422</a> </p><br />
    <br clear="all">

    <p>33.   59742   DMID-LS-134; EMBASE-DMID-12/18/2006</p>

    <p class="memofmt1-2">          Venezuelan equine encephalitis virus complex-specific monoclonal antibody provides broad protection, in murine models, against airborne challenge with viruses from serogroups I, II and III</p>

    <p>          Phillpotts, RJ </p>

    <p>          Virus Research  <b>2006</b>.  120(1-2): 107-112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4JRVFMW-1/2/1b6cbfa6f99d1e36578f02dc0bf05755">http://www.sciencedirect.com/science/article/B6T32-4JRVFMW-1/2/1b6cbfa6f99d1e36578f02dc0bf05755</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
