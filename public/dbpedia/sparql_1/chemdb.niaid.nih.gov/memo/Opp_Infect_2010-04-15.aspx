

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-04-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JdPGSASQ1B7kw7Kt0kL8JvRSn7rsBhdI4srfar99e9luU2zzg8P0+W3Ew9LsnTXYC+79bo2XnZKvo5y5aoej4nUZnyBWq2isWAVrJTEHvbyF1/Gtp4JYMj+cYaI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="237CE524" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">April 1- April 15, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20373807">Thiazole, Oxadiazole, and Carboxamide Derivatives of Artemisinin are Highly Selective and Potent Inhibitors of Toxoplasma gondii.</a> Hencken, C.P., L. Jones-Brando, C. Bordon, R. Stohler, B.T. Mott, R. Yolken, G.H. Posner, and L.E. Woodard. J Med Chem. <b>[Epub ahead of print]</b>; PMID[20373807].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20045696">Toxoplasma gondii: fluconazole and itraconazole activity against toxoplasmosis in a murine model.</a> Martins-Duarte, E.S., L. Lemgruber, W. de Souza, and R.C. Vommaro. Exp Parasitol. 124(4): p. 466-9; PMID[20045696].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p> 

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p> </p>

    <p>3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20335035">Antimicrobial selaginellin derivatives from Selaginella pulvinata.</a> Cao, Y., J.J. Chen, N.H. Tan, L. Oberer, T. Wagner, Y.P. Wu, G.Z. Zeng, H. Yan, and Q. Wang. Bioorg Med Chem Lett. 20(8): p. 2456-60; PMID[20335035].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20299219">Carbonic anhydrase inhibitors. The beta-carbonic anhydrases from the fungal pathogens Cryptococcus neoformans and Candida albicans are strongly inhibited by substituted-phenyl-1H-indole-5-sulfonamides.</a> Guzel, O., A. Maresca, R.A. Hall, A. Scozzafava, A. Mastrolorenzo, F.A. Muhlschlegel, and C.T. Supuran. Bioorg Med Chem Lett. 20(8): p. 2508-11; PMID[20299219].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20356752">Trypanoside, anti-tuberculosis, leishmanicidal, and cytotoxic activities of tetrahydrobenzothienopyrimidines.</a> Aponte, J.C., A.J. Vaisberg, D. Castillo, G. Gonzalez, Y. Estevez, J. Arevalo, M. Quiliano, M. Zimic, M. Verastegui, E. Malaga, R.H. Gilman, J.M. Bustamante, R.L. Tarleton, Y. Wang, S.G. Franzblau, G.F. Pauli, M. Sauvain, and G.B. Hammond. Bioorg Med Chem. 18(8): p. 2880-6; PMID[20356752].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20368403">In Vitro Activities of DC-159a, a New-generation Fluoroquinolone, against Mycobacterium species.</a> Disratthakit, A. and N. Doi. Antimicrob Agents Chemother. <b>[Epub ahead of print]</b>; PMID[20368403].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20386700">A Mycobacterium tuberculosis sigma factor network responds to cell-envelope damage by the promising anti-mycobacterial thioridazine.</a> Dutta, N.K., S. Mehra, and D. Kaushal. PLoS One. 5(4): p. e10069; PMID[20386700].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20364296">Fluorescent 3-hydroxy-4-pyridinone hexadentate iron chelators: intracellular distribution and the relevance to antimycobacterial properties.</a> Nunes, A., M. Podinovskaia, A. Leite, P. Gameiro, T. Zhou, Y. Ma, X. Kong, U.E. Schaible, R.C. Hider, and M. Rangel. J Biol Inorg Chem. <b>[Epub ahead of print]</b>; PMID[20364296].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20384293">Secondary Metabolites from the Roots of Litsea hypophaea and Their Antitubercular Activity.</a> Pan, P.C., M.J. Cheng, C.F. Peng, H.Y. Huang, J.J. Chen, and I.S. Chen. J Nat Prod. <b>[Epub ahead of print]</b>; PMID[20384293].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20385864">In vitro interactions between new anti-tubercular drug candidates SQ109 and TMC207.</a> Reddy, V.M., L. Einck, K. Andries, and C.A. Nacy. Antimicrob Agents Chemother. <b>[Epub ahead of print]</b>; PMID[20385864].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0402-041510.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p class="plaintext">11. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000275662700006&amp;SID=3Dbm69Kp1MneP7i2I6a&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Dry Powder PA-824 Aerosols for Treatment of Tuberculosis in Guinea Pigs.</a> Garcia-Contreras, L., J.C. Sung, P. Muttil, D. Padilla, M. Telko, J.L. VerBerkmoes, K.J. Elbert, A.J. Hickey, and D.A. Edwards. Antimicrobial Agents and Chemotherapy. 54(4): p. 1436-1442; ISI[000275662700006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0402-041510.</p>

    <p> </p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
