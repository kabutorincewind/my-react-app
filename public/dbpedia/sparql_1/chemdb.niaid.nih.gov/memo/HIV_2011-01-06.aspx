

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-01-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="rwsQ9G4BCsCFUk+4p7elGI9aTa3pYJcVGWdu8am6KNdJfytIchgJOxSZjwE9zE/AdtR0UMxha16hz7m/BinAK7IH2bH1CiCGJM6ftNaZb1PIHSxPDWaT4pBmHg8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F573DF32" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  December 22 - January 6, 2011</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21093117">Novel isatinyl thiosemicarbazones derivatives as potential molecule to combat HIV-TB co-infection.</a> Banerjee, D., P. Yogeeswari, P. Bhat, A. Thomas, M. Srividya, and D. Sriram. European Journal of Medicinal Chemistry, 2011. 46(1): p. 106-21; PMID[21093117].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21175357">Efficacy of Tat-conjugated ritonavir-loaded nanoparticles in reducing HIV-1 replication in monocyte-derived macrophages and cytocompatibility with macrophages and human neurons.</a> Borgmann, K., K.S. Rao, V. Labhasetwar, and A. Ghorpade. AIDS Research and Human Retroviruses, 2010. <b>[Epub ahead of print]</b>;  PMID[21175357].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21186803">HIV Protease-Mediated Activation of Sterically Capped Proteasome Inhibitors and Substrates.</a> Buckley, D.L., T.W. Corson, N. Aberle, and C.M. Crews. Journal of the American Chemistry Society, 2010. <b>[Epub ahead of print]</b>;  PMID[21186803].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21095125">Synthesis and biological activity of novel 5&#39;-arylamino-nucleosides by microwave-assisted one-pot tandem Staudinger/aza-Wittig/reduction.</a> Chen, H., J. Zhao, Y. Li, F. Shen, X. Li, Q. Yin, Z. Qin, X. Yan, Y. Wang, P. Zhang, and J. Zhang. Bioorganic and Medical Chemistry Letters, 2011. 21(1): p. 574-6; PMID[21095125].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21168336">Virtual screening based identification of novel small-molecule inhibitors targeted to the HIV-1 capsid.</a> Curreli, F., H. Zhang, X. Zhang, I. Pyatkin, Z. Victor, A. Altieri, and A.K. Debnath. Bioorganic and Medicinal Chemistry, 2011. 19(1): p. 77-90; PMID[21168336].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20956357">Iron chelators of the Di-2-pyridylketone thiosemicarbazone and 2-benzoylpyridine thiosemicarbazone series inhibit HIV-1 transcription: identification of novel cellular targets--iron, cyclin-dependent kinase (CDK) 2, and CDK9.</a> Debebe, Z., T. Ammosova, D. Breuer, D.B. Lovejoy, D.S. Kalinowski, P.K. Karla, K. Kumar, M. Jerebtsova, P. Ray, F. Kashanchi, V.R. Gordeuk, D.R. Richardson, and S. Nekhai. Molecular Pharmacology, 2011. 79(1): p. 185-96; PMID[20956357]</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21084190">Triterpene derivatives that inhibit human immunodeficiency virus type 1 replication.</a> Dorr, C.R., S. Yemets, O. Kolomitsyna, P. Krasutsky, and L.M. Mansky. Bioorganic and Medicinal Chemistry Letters, 2011. 21(1): p. 542-5; PMID[21084190].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21087861">Discovery of a 1,5-dihydrobenzo[b][1,4]diazepine-2,4-dione series of inhibitors of HIV-1 capsid assembly.</a> Fader, L.D., R. Bethell, P. Bonneau, M. Bos, Y. Bousquet, M.G. Cordingley, R. Coulombe, P. Deroy, A.M. Faucher, A. Gagnon, N. Goudreau, C. Grand-Maitre, I. Guse, O. Hucke, S.H. Kawai, J.E. Lacoste, S. Landry, C.T. Lemke, E. Malenfant, S. Mason, S. Morin, J. O&#39;Meara, B. Simoneau, S. Titolo, and C. Yoakim. Bioorganic and Medicinal Chemistry Letters, 2011. 21(1): p. 398-404; PMID[21087861].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20955169">Isolation of a New Trypsin Inhibitor from the Faba bean (Vicia faba cv. Giza 843) with Potential Medicinal Applications.</a> Fang, E.F., A.A. Hassanien, J.H. Wong, C.S. Bah, S.S. Soliman, and T.B. Ng. Protein and Peptide Letters, 2011. 18(1): p. 64-72; PMID[20955169].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21194227">Design and Synthesis of Potent HIV-1 Protease Inhibitors Incorporating Hexahydrofuropyranol-Derived High Affinity P(2) Ligands: Structure-Activity Studies and Biological Evaluation.</a> Ghosh, A.K., B.D. Chapsal, A. Baldridge, M.P. Steffey, D.E. Walters, Y. Koh, M. Amano, and H. Mitsuya. Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>;  PMID[21194227].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21190369">Design, Synthesis, and Biological Activity of Novel 5-((Arylfuran/1H-pyrrol-2-yl)methylene)-2-thioxo-3-(3-(trifluoromethyl)phenyl)thi azolidin-4-ones as HIV-1 Fusion Inhibitors Targeting gp41.</a> Jiang, S., S.R. Tala, H. Lu, N.E. Abo-Dya, I. Avan, K. Gyanda, L. Lu, A.R. Katritzky, and A.K. Debnath. Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>;  PMID[21190369].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20956603">Potent Strategy To Inhibit HIV-1 by Binding both gp120 and gp41.</a> Kagiampakis, I., A. Gharibi, M.K. Mankowski, B.A. Snyder, R.G. Ptak, K. Alatas, and P.J. Liwang. Antimicrobial Agents and Chemotherapy, 2011. 55(1): p. 264-75; PMID[20956603]</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21143001">Potential of polymeric nanoparticles in AIDS treatment and prevention.</a> Khalil, N.M., E. Carraro, L.F. Cotica, and R.M. Mainardes. Expert Opinion on Drug Delivery, 2011. 8(1): p. 95-112; PMID[21143001].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21169023">Design, synthesis and biological evaluation of small molecule inhibitors of CD4-gp120 binding based on virtual screening.</a> Lalonde, J.M., M.A. Elban, J.R. Courter, A. Sugawara, T. Soeta, N. Madani, A.M. Princiotto, Y.D. Kwon, P.D. Kwong, A. Schon, E. Freire, J. Sodroski, and A.B. Smith, 3rd. Bioorganic and Medicinal Chemistry, 2011. 19(1): p. 91-101; PMID[21169023].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20890754">Lectins: production and practical applications.</a> Lam, S.K. and T.B. Ng. Applied Microbiology and Biotechnology, 2011. 89(1): p. 45-55; PMID[20890754].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20553419">Expression and characterization of antimicrobial peptides Retrocyclin-101 and Protegrin-1 in chloroplasts to control viral and bacterial infections.</a> Lee, S.B., B. Li, S. Jin, and H. Daniell. Plant Biotechnology J, 2011. 9(1): p. 100-15; PMID[20553419].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21099673">HIV-1 trans-activator protein dysregulates IFN-gamma signaling and contributes to the suppression of autophagy induction.</a> Li, J.C., K.Y. Au, J.W. Fang, H.C. Yim, K.H. Chow, P.L. Ho, and A.S. Lau. Aids, 2011. 25(1): p. 15-25; PMID[21099673].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21175150">Schinarisanlactone A, a New Bisnortriterpenoid from Schisandra arisanensis.</a> Lin, Y.C., I.W. Lo, S.Y. Chen, P.H. Lin, C.T. Chien, S.Y. Chang, and Y.C. Shen. Organic Letters, 2010. <b>[Epub ahead of print]</b>;  PMID[21175150].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21199933">Molecular engineering of RANTES peptide mimetics with potent anti-HIV-1 activity.</a> Lusso, P., L. Vangelista, R. Cimbro, M. Secchi, F. Sironi, R. Longhi, M. Faiella, O. Maglio, and V. Pavone. FASEB Journal, 2011; PMID[21199933].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20969902">Involvement of inhibitory factors in the inefficient entry of HIV-1 into the human CD4 positive HUT78 cell line.</a> Maeda, Y., K. Yusa, Y. Nakano, and S. Harada. Virus Research, 2011. 155(1): p. 368-71; PMID[20969902].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21198428">A RhoA-Derived Peptide Inhibits Human Immunodeficiency Virus-1 Entry in vitro.</a> Maselko, M., C. Ward, and M. Pastey. Current HIV Research, 2011. <b>[Epub ahead of print]</b>; PMID[21198428].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21195767">Structural characterization and anti-HIV-1 activities of arginine/glutamate-rich polypeptide Luffin P1 from the seeds of sponge gourd (Luffa cylindrical).</a> Ng, Y.M., Y. Yang, K.H. Sze, X. Zhang, Y.T. Zheng, and P.C. Shaw. Journal of Structural Biology, 2010. <b>[Epub ahead of print]</b>;  PMID[21195767].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20980522">RNA aptamers directed to human immunodeficiency virus type 1 Gag polyprotein bind to the matrix and nucleocapsid domains and inhibit virus production.</a> Ramalingam, D., S. Duclair, S.A. Datta, A. Ellington, A. Rein, and V.R. Prasad. Journal of Virology, 2011. 85(1): p. 305-14; PMID[20980522].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937333">Complete inactivation of HIV-1 using photo-labeled non-nucleoside reverse transcriptase inhibitors.</a> Rios, A., J. Quesada, D. Anderson, A. Goldstein, T. Fossum, S. Colby-Germinario, and M.A. Wainberg. Virus Research, 2011. 155(1): p. 189-94; PMID[20937333].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21190666">Allosteric Suppression of HIV-1 Reverse Transcriptase Structural Dynamics upon Inhibitor Binding.</a> Seckler, J.M., M.D. Barkley, and P.L. Wintrode. Biophysical Journal, 2011. 100(1): p. 144-53; PMID[21190666].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20962083">Small-molecule inhibition of human immunodeficiency virus type 1 infection by virus capsid destabilization.</a> Shi, J., J. Zhou, V.B. Shah, C. Aiken, and K. Whitby. Journal of Virology, 2011. 85(1): p. 542-9; PMID[20962083].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21109432">Synthesis and SAR of novel CXCR4 antagonists that are potent inhibitors of T tropic (X4) HIV-1 replication.</a> Skerlj, R., G. Bridger, E. McEachern, C. Harwig, C. Smith, T. Wilson, D. Veale, H. Yee, J. Crawford, K. Skupinska, R. Wauthy, W. Yang, Y. Zhu, D. Bogucki, M. Di Fluri, J. Langille, D. Huskens, E. De Clercq, and D. Schols. Bioorgznic and Medicinal Chemistry Letters, 2011. 21(1): p. 262-6; PMID[21109432].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21175156">Pyrrolinone-Based Peptidomimetics. &quot;Let the Enzyme or Receptor be the Judge&quot;.</a> Smith, A.B., A.K. Charnley, and R. Hirschmann. Accounts of Chemical Research, 2010. <b>[Epub ahead of print]</b>;  PMID[21175156].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21193846">Targeting cell entry of enveloped viruses as an antiviral strategy.</a> Teissier, E., F. Penin, and E.I. Pecheur. Molecules, 2010. 16(1): p. 221-50; PMID[21193846].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21159508">Rapid access to new bioconjugates of betulonic acid via click chemistry.</a> Vasilevsky, S.F., A.I. Govdi, I.V. Sorokina, T.G. Tolstikova, D.S. Baev, G.A. Tolstikov, V.I. Mamatuyk, and I.V. Alabugin. Bioorganic and Medicinal Chemistry Letters, 2011. 21(1): p. 62-5; PMID[21159508].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937786">Novel Compounds Containing Multiple Guanide Groups That Bind the HIV Coreceptor CXCR4.</a> Wilkinson, R.A., S.H. Pincus, J.B. Shepard, S.K. Walton, E.P. Bergin, M. Labib, and M. Teintze. Antimicrob Agents Chemotherapy, 2011. 55(1): p. 255-63; PMID[20937786].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21123066">Efficient synthesis and biological evaluation of epiceanothic acid and related compounds.</a> Zhang, P., L. Xu, K. Qian, J. Liu, L. Zhang, K.H. Lee, and H. Sun. Bioorganic and Medicinal Chemistry Letters, 2011. 21(1): p. 338-41; PMID[21123066].</p>

    <p class="plaintext"><b>[Pubmed]</b>.HIV_1222-010611.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2"> ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284665400013">Selective Serotonin Reuptake Inhibitor Suppression of HIV Infectivity and Replication.</a> Benton, T., K. Lynch, B. Dube, D.R. Gettes, N.B. Tustin, J.P. Lai, D.S. Metzger, J. Blume, S.D. Douglas, and D.L. Evans. Psychosomatic Medicine, 2010. 72(9): p. 925-932; ISI[000284665400013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284533700020">Arylation of 2-Furyl 4-Fluorophenyl Ketone: An Extension of Heck Chemistry towards Novel Integrase Inhibitors.</a> Franchi, L., M. Rinaldi, G. Vignaroli, A. Innitzer, M. Radi, and M. Botta. Synthesis-Stuttgart, 2010(22): p. 3927-3933; ISI[000284533700020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284794100083">5-Hydroxychromone, An Alternative Scaffold for Anti-HCV 1,3-Diketo Acid (DKA).</a> Lee, C., K.S. Park, H.R. Park, J.C. Park, B. Lee, D.E. Kim, and Y. Chong. Bulletin of the Korean Chemical Society, 2010. 31(11): p. 3471-3474; ISI[000284794100083].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282312600034">Targeting the PI3K/Akt Cell Survival Pathway to Induce Cell Death of HIV-1 Infected Macrophages with Alkylphospholipid Compounds.</a> Lucas, A., Y. Kim, O. Rivera-Pabon, S. Chae, D.H. Kim, and B. Kim. PLoS ONE, 2010. 5(9); ISI[000282312600034].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284794100053">Synthesis and Anti-HIV Activity of Novel 4 &#39;-Ethyl-5 &#39;-norcarbocyclic Adenosine Phosphonic Acid Analogues.</a> Yoo, J.C., H. Li, W. Lee, and J.H. Hong, . Bulletin of the Korean Chemical Society, 2010. 31(11): p. 3348-3352; ISI[000284794100053].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284967200003">7,8-Secolignans from Schisandra wilsoniana and Their Anti-HIV-1 Activities.</a> Zhang, X.J., G.Y. Yang, R.R. Wang, J.X. Pu, H.D. Sun, W.L. Xiao, and Y.T. Zheng, . Chemistry &amp; Biodiversity, 2010. 7(11): p. 2692-2701; ISI[000284967200003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284876100016">Synthesis and Anti-HIV Activity of Triterpene 3-O-Galactopyranosides, Analogs of Glycyrrhizic Acid.</a> Baltina, L.A., R.M. Kondratenko, O.A. Plyasunova, S.A. Nepogodiev, and R.A. Field. Chemistry of Natural Compounds, 2010. 46(4): p. 576-582; ISI[000284876100016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284459800004">Synthesis and Antiviral Activity of 18 Alpha-Glycyrrhizic Acid and Its Esters.</a> Baltina, L.A., O.V. Stolyarova, R.M. Kondratenko, O.A. Plyasunova, and A.G. Pokrovskii. Pharmaceutical Chemistry Journal, 2010. 44(6): p. 299-302; ISI[000284459800004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284559100038">Sesquiterpene Coumarins from Ferula gumosa.</a> Iranshahi, M., M. Masullo, A. Asili, A. Hamedzadeh, B. Jahanbin, M. Festa, A. Capasso, and S. Piacente. Journal of Natural Products, 2010. 73(11): p. 1958-1962; ISI[000284559100038].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284941300065">Ribonucleoside Triphosphates as Substrate of Human Immunodeficiency Virus Type 1 Reverse Transcriptase in Human Macrophages.</a> Kennedy, E.M., C. Gavegnano, L. Nguyen, R. Slater, A. Lucas, E. Fromentin, R.F. Schinazi, and B. Kim. Journal of Biological Chemistry, 2010. 285(50): p. 39380-39391; ISI[000284941300065].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283953600015">Synthesis of SATE Prodrug of 6 &#39;-Fluoro-6 &#39;-methyl-5 &#39;-noradenosine Nucleoside Phosphonic Acid as a New Class of Anti-HIV Agent.</a> Li, H., J.C. Yoo, Y.C. Baik, W. Lee, and J.H. Hong. Bulletin of the Korean Chemical Society, 2010. 31(9): p. 2514-2518; ISI[000283953600015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284598600016">Microwave-assisted synthesis of a novel class of imidazolylthiazolidin-4-ones and evaluation of its biological activities.</a> Modha, S.G., V.P. Mehta, D. Ermolat&#39;ev, J. Balzarini, K. Van Hecke, L. Van Meervelt, and E. Van der Eycken. Molecular Diversity, 2010. 14(4): p. 767-776; ISI[000284598600016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283953600007">Synthesis and Antiviral Evaluation of 1 &#39;-Branched-5 &#39;-Norcarbocyclic Adenosine Phosphonic Acid Analogues.</a> Oh, C.H., K.H. Yoo, and J.H. Hong. Bulletin of the Korean Chemical Society, 2010. 31(9): p. 2473-2478; ISI[000283953600007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">46. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284889700010">Synthesis of Novel 4 &#39;-Cyclopropyl-5 &#39;-norcarbocyclic Adenosine Phosphonic Acid Analogues.</a> Shen, G.H. and J.H. Hong. Nucleosides Nucleotides &amp; Nucleic Acids, 2010. 29(11-12): p. 905-919; ISI[000284889700010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">47. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284941300059">Gyrase B Inhibitor Impairs HIV-1 Replication by Targeting Hsp90 and the Capsid Protein.</a> Vozzolo, L., B. Loh, P.J. Gane, M. Tribak, L.H. Zhou, I. Anderson, E. Nyakatura, R.G. Jenner, D. Selwood, and A. Fassati. Journal of Biological Chemistry, 2010. 285(50): p. 39314-39328; ISI[000284941300059].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1222-010611.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
