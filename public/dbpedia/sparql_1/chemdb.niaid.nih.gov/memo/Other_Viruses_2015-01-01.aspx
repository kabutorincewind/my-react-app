

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-01-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JTuCZBEc1lLLBjbJV1BW0c+wSaHsgnvbTXaN1cWKI/8ueYnNYc60G1wAs3O9ouLsmxusTWat877CdoJeDEhadmyxUrV1DXQnThWibs6N8B5ZWwGUqIzVxWxBVf8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="30694D0E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: December 19 - January 1, 2015</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25313217">Mass Balance, Metabolite Profile, and in Vitro-in Vivo Comparison of Clearance Pathways of Deleobuvir, a Hepatitis C Virus Polymerase Inhibitor.</a> Chen, L.Z., J.P. Sabo, E. Philip, L. Rowland, Y. Mao, B. Latli, D. Ramsden, D.A. Mandarino, and R.S. Sane. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 25-37. PMID[25313217].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24606010">Antiviral Activity of Virocidal Peptide Derived from NS5A Against Two Different HCV Genotypes: An in Vitro Study.</a> El-Shenawy, R., A. Tabll, N.G. Bader El Din, Y. El Abd, M. Mashaly, C.A. Abdel Malak, R. Dawood, and M. El-Awady. Journal of Immunoassay &amp; Immunochemistry, 2015. 36(1): p. 63-79. PMID[24606010].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25446333">Inhibitory Effects of Pycnogenol(R) on Hepatitis C Virus Replication.</a> Ezzikouri, S., T. Nishimura, M. Kohara, S. Benjelloun, Y. Kino, K. Inoue, A. Matsumori, and K. Tsukiyama-Kohara. Antiviral Research, 2015. 113: p. 93-102. PMID[25446333].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25385103">In Vitro Antiviral Activity and Preclinical and Clinical Resistance Profile of Miravirsen, A Novel Anti-hepatitis C Virus Therapeutic Targeting the Human Factor miR-122.</a> Ottosen, S., T.B. Parsley, L. Yang, K. Zeh, L.J. van Doorn, E. van der Veer, A.K. Raney, M.R. Hodges, and A.K. Patick. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 599-608. PMID[25385103].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24680700">Exploiting ChEMBL Database to Identify Indole Analogs as HCV Replication Inhibitors.</a> Vrontaki, E., G. Melagraki, T. Mavromoustakos, and A. Afantitis. Methods, 2015. 71: p. 4-13. PMID[24680700].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25449363">Computational Study on the Drug Resistance Mechanism of HCV NS5B RNA-dependent RNA Polymerase Mutants V494I, V494A, M426A, and M423T to Filibuvir.</a> Wang, H., C. Guo, B.Z. Chen, and M. Ji. Antiviral Research, 2015. 113: p. 79-92. PMID[25449363].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25451544">Macromolecular (Pro)drugs with Concurrent Direct Activity against the Hepatitis C Virus and Inflammation.</a> Wohl, B.M., A.A. Smith, B.E. Jensen, and A.N. Zelikin. Journal of Controlled Release 2014. 196: p. 197-207. PMID[25451544].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p>

    <h2>Feline Immunodeficiency Virus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25521480">Selective Interaction of Heparin with the Variable Region 3 Within Surface Glycoprotein of Laboratory-adapted Feline Immunodeficiency Virus.</a> Hu, Q.Y., E. Fink, C.K. Grant, and J.H. Elder. PLoS One, 2014. 9(12): p. e115252. PMID[25521480].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25479789">Ex Vivo Model of Congenital Cytomegalovirus Infection and New Combination Therapies.</a> Morere, L., D. Andouard, F. Labrousse, F. Saade, C.A. Calliste, S. Cotin, Y. Aubard, W.D. Rawlinson, F. Esclaire, S. Hantz, M.C. Ploy, and S. Alain. Placenta, 2015. 36(1): p. 41-47. PMID[25479789].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24976258">Statins Demonstrate a Broad Anti-cytomegalovirus Activity in Vitro in Ganciclovir-susceptible and Resistant Strains.</a> Ponroy, N., A. Taveira, N.J. Mueller, and A.L. Millard. Journal of Medical Virology, 2015. 87(1): p. 141-153. PMID[24976258].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25446405">Development of a High-content Screen for the Identification of Inhibitors Directed against the Early Steps of the Cytomegalovirus Infectious Cycle.</a> T, J.G., T. Cohen, V. Redmann, Z. Lau, D. Felsenfeld, and D. Tortorella. Antiviral Research, 2015. 113: p. 49-61. PMID[25446405].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25385102">Antiviral Activity of a Single-domain Antibody Immunotoxin Binding to Glycoprotein D of Herpes Simplex Virus 2.</a> Geoghegan, E.M., H. Zhang, P.J. Desai, A. Biragyn, and R.B. Markham. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 527-535. PMID[25385102].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25447452">Fungal Metabolite Myriocin Promotes Human Herpes Simplex Virus-2 Infection.</a> Wang, J., X. Guo, Z. Yang, R.X. Tan, X. Chen, and E. Li. Life Sciences, 2015. 120: p. 31-38. PMID[25447452].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1219-010115.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="file:///C:/Downloads/GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345477200007">Synthesis of Unnatural 3&#39;-Phospha-2&#39;-deoxyfuranose Nucleoside Analogues.</a> Dayde, B., C. Pierra, G. Gosselin, D. Surleraux, A.T. Ilagouma, J.N. Volle, D. Virieux, and J.L. Pirat. Tetrahedron Letters, 2014. 55(46): p. 6328-6330. ISI[000345477200007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">15. <a href="file:///C:/Downloads/GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345540800012">Synthesis and anti-Hepatitis B Virus Activities of Heterocyclic Substituted Matijin-su Derivatives.</a> Liang, G.P., Z.X. Hu, Q.C. Liu, Z.M. Huang, J.X. Zhang, G.Y. Liang, and B.X. Xu. Chemical Journal of Chinese Universities, 2014. 35(11): p. 2353-2359. ISI[000345540800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">16. <a href="file:///C:/Downloads/GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344774700156">A Therapeutic anti-Hepatitis C Virus shmiRNA Integrated into the miR-122 Genomic Locus Mediates a Potent Anti-viral Response.</a> Senis, E., S. Mockenhaupt, D. Rupp, R. Bartenschlager, and D. Grimm. Human Gene Therapy, 2014. 25(11): p. A51-A52. ISI[000344774700156].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1219-010115.</p><br /> 

    <p class="plaintext">17. <a href="file:///C:/Downloads/GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345734400019">UFLC/MS-IT-TOF Guided Isolation of anti-HBV Active Chlorogenic acid Analogues from Artemisia capillaris as a Traditional Chinese Herb for the Treatment of Hepatitis.</a> Zhao, Y., C.A. Geng, Y.B. Ma, X.Y. Huang, H. Chen, T.W. Cao, K. He, H. Wang, X.M. Zhang, and J.J. Chen. Journal of Ethnopharmacology, 2014. 156: p. 147-154. ISI[000345734400019].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1219-010115.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
