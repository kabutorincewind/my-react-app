

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-332.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gPjMMiYU0doTrGeJKeEXtUsDfAI5N4PIST9Q+OXhuDH3ucY3B9dpJMbREDOCBeOBeZyTf/t8W9+FJkElZu8/Zp55/N74mUaEE121AJog6xrmzmmf1D7r3cGiA0k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C4169883" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-332-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47928   HIV-LS-332; EMBASE-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Neamine dimers targeting the HIV-1 TAR RNA</p>

    <p>          Riguet, Emmanuel, Desire, Jerome, Boden, Oliver, Ludwig, Verena, Gobel, Michael, Bailly, Christian, and Decout, Jean-Luc</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(21): 4651-4655</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4H2FXW1-D/2/30aa16de3d12f9ebcc627988ceecb442">http://www.sciencedirect.com/science/article/B6TF9-4H2FXW1-D/2/30aa16de3d12f9ebcc627988ceecb442</a> </p><br />

    <p>2.     47929   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Antiviral Activity of GW678248, a Novel Benzophenone Nonnucleoside Reverse Transcriptase Inhibitor</p>

    <p>          Ferris, RG, Hazen, RJ, Roberts, GB, St, Clair MH, Chan, JH, Romines, KR, Freeman, GA, Tidwell, JH, Schaller, LT, Cowan, JR, Short, SA, Weaver, KL, Selleseth, DW, Moniri, KR, and Boone, LR</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(10): 4046-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189079&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189079&amp;dopt=abstract</a> </p><br />

    <p>3.     47930   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Evaluation of the anti-HIV activity of statins</p>

    <p>          Moncunill, G, Negredo, E, Bosch, L, Vilarrasa, J, Witvrouw, M, Llano, A, Clotet, B, and Este, JA</p>

    <p>          AIDS <b>2005</b>.  19 (15): 1697-1700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184043&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184043&amp;dopt=abstract</a> </p><br />

    <p>4.     47931   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Synthesis and HIV-1 integrase inhibitory activities of caffeic acid dimers derived from Salvia officinalis</p>

    <p>          Bailly, F, Queffelec, C, Mbemba, G, Mouscadet, JF, and Cotelle, P</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16183277&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16183277&amp;dopt=abstract</a> </p><br />

    <p>5.     47932   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Anti-HIV-1 constituents from Clausena excavata: Part II. carbazoles and a pyranocoumarin</p>

    <p>          Kongkathip, B, Kongkathip, N, Sunthitikawinsakul, A, Napaswat, C, and Yoosook, C</p>

    <p>          Phytother Res <b>2005</b>.  19(8): 728-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16177980&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16177980&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47933   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          HIV entry inhibitors: a new generation of antiretroviral drugs</p>

    <p>          Krambovitis, E, Porichis, F, and Spandidos, DA</p>

    <p>          Acta Pharmacol Sin <b>2005</b>.  26(10): 1165-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16174430&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16174430&amp;dopt=abstract</a> </p><br />

    <p>7.     47934   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          The anti-HIV activity of ADS-J1 targets the HIV-1 gp120</p>

    <p>          Armand-Ugon, M, Clotet-Codina, I, Tintori, C, Manetti, F, Clotet, B, Botta, M, and Este, JA</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16168454&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16168454&amp;dopt=abstract</a> </p><br />

    <p>8.     47935   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Mechanisms of HIV-1 Inhibition by the Lipid Mediator N-Arachidonoyldopamine</p>

    <p>          Sancho, R, de la Vega, L, Macho, A, Appendino, G, Di Marzo, V, and Munoz, E</p>

    <p>          J Immunol <b>2005</b>.  175(6): 3990-3999</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16148147&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16148147&amp;dopt=abstract</a> </p><br />

    <p>9.     47936   HIV-LS-332; PUBMED-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Histone Deacetylase 6 Regulates Human Immunodeficiency Virus Type 1 Infection</p>

    <p>          Valenzuela-Fernandez, A, Alvarez, S, Gordon-Alonso, M, Barrero, M, Ursa, A, Cabrero, JR, Fernandez, G, Naranjo-Suarez, S, Yanez-Mo, M, Serrador, JM, Munoz-Fernandez, MA, and Sanchez-Madrid, F</p>

    <p>          Mol Biol Cell <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16148047&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16148047&amp;dopt=abstract</a> </p><br />

    <p>10.   47937   HIV-LS-332; WOS-HIV-9/25/2005</p>

    <p class="memofmt1-2">          N-phenyl-N&#39;-(2,2,6,6-tetramethyl-piperidin-4-yl)-oxalam des as a new class of HIV-1 entry inhibitors that prevent gp120 binding to CD4</p>

    <p>          Zhao, Q, Ma, LY, Jiang, SB, Lu, H, Liu, SW, He, YX, Strick, N, Neamati, N, and Debnath, AK</p>

    <p>          VIROLOGY <b>2005</b>.  339(2): 213-225, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231631900007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231631900007</a> </p><br />

    <p>11.   47938   HIV-LS-332; WOS-HIV-9/25/2005</p>

    <p class="memofmt1-2">          Human APOBEC3B is a potent inhibitor of HIV-1 infectivity and is resistant to HIV-1 Vif</p>

    <p>          Doehle, BP, Schafer, A, and Cullen, BR</p>

    <p>          VIROLOGY <b>2005</b>.  339(2): 281-288, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231631900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231631900013</a> </p><br />
    <br clear="all">

    <p>12.   47939   HIV-LS-332; EMBASE-HIV-10/3/2005</p>

    <p class="memofmt1-2">          A series of 5-(5,6)-dihydrouracil substituted 8-hydroxy-[1,6]naphthyridine-7-carboxylic acid 4-fluorobenzylamide inhibitors of HIV-1 integrase and viral replication in cells</p>

    <p>          Embrey, Mark W, Wai, John S, Funk, Timothy W, Homnick, Carl F, Perlow, Debbie S, Young, Steven D, Vacca, Joseph P, Hazuda, Daria J, Felock, Peter J, and Stillmock, Kara A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(20): 4550-4554</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4GWC0RK-2/2/165144fa8e34d14f6a0e8403e0fd0948">http://www.sciencedirect.com/science/article/B6TF9-4GWC0RK-2/2/165144fa8e34d14f6a0e8403e0fd0948</a> </p><br />

    <p>13.   47940   HIV-LS-332; EMBASE-HIV-10/3/2005</p>

    <p class="memofmt1-2">          Screen for underlying HIV before antiviral treatment for HBV</p>

    <p>          Wong, Ka Hing, Lee, Shui Shan, and Chan, Kenny Chi Wai</p>

    <p>          Journal of Infection <b>2005</b>.  51(2): 172-173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WJT-4G9R1DV-7/2/2bd4b8c368e250ef171be8cb6141f14c">http://www.sciencedirect.com/science/article/B6WJT-4G9R1DV-7/2/2bd4b8c368e250ef171be8cb6141f14c</a> </p><br />

    <p>14.   47941   HIV-LS-332; WOS-HIV-9/25/2005</p>

    <p class="memofmt1-2">          Antiviral 6-amino-quinolones: Molecular basis for potency and selectivity</p>

    <p>          Richter, SN, Gatto, B, Tabarrini, O, Fravolini, A, and Palumbo, M</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(19): 4247-4251, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231663400018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231663400018</a> </p><br />

    <p>15.   47942   HIV-LS-332; WOS-HIV-10/2/2005</p>

    <p class="memofmt1-2">          Mycophenolic acid inhibits syncytium formation accompanied by reduction of gp120 expression</p>

    <p>          Ui, H, Asanuma, S, Chiba, H, Takahashi, A, Yamaguchi, Y, Masuma, R, Omura, S, and Tanaka, H</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2005</b>.  58(8): 514-518, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231787100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231787100004</a> </p><br />

    <p>16.   47943   HIV-LS-332; WOS-HIV-10/2/2005</p>

    <p class="memofmt1-2">          The nonnucleoside reverse transcriptase inhibitors efavirenz and nevirapine in the treatment of HIV</p>

    <p>          Sheran, M</p>

    <p>          HIV CLINICAL TRIALS <b>2005</b>.  6(3): 158-168, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231815500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231815500004</a> </p><br />

    <p>17.   47944   HIV-LS-332; WOS-HIV-10/2/2005</p>

    <p class="memofmt1-2">          Synthesis and biological activity of N-alkyl/aryi-N &#39;-(4-arylthiazol-2-yl)-N &#39;-glycosyl guanidines</p>

    <p>          Shi, HF and Cao, LH</p>

    <p>          CHINESE JOURNAL OF ORGANIC CHEMISTRY <b>2005</b>.  25(9): 1066-1070, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231741300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231741300010</a> </p><br />

    <p>18.   47945   HIV-LS-332; WOS-HIV-10/2/2005</p>

    <p class="memofmt1-2">          Conformational changes in HIV-1 gp41 in the course of HIV-1 envelope glycoprotein-mediated fusion and inactivation</p>

    <p>          Dimitrov, AS, Louis, JM, Bewley, CA, Clore, GM, and Blumenthal, R</p>

    <p>          BIOCHEMISTRY <b>2005</b>.  44(37): 12471-12479, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231942200022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231942200022</a> </p><br />

    <p>19.   47946   HIV-LS-332; WOS-HIV-10/2/2005</p>

    <p class="memofmt1-2">          Sensitivity of primary R5 HIV-1 to inhibition by RANTES correlates with sensitivity to small-molecule R5 inhibitors</p>

    <p>          Koning, FA, Koevoets, C, van, der Vorst TJK, and Schuitemaker, H</p>

    <p>          ANTIVIRAL THERAPY <b>2005</b>.  10(2): 231-237, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231962300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231962300005</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
