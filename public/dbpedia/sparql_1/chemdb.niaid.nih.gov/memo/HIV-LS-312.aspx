

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-312.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gm0CDGe5sOXXuK0coWdhN5luDEyPhsDVr0cM+DIBVMX7zwh2jb7gDwA64qDDo7h5ecYqBEuTPABdus+8/hptUwLWCJbcsJ3tZ3xR6jCuk1QJNREhIfjSRvuJEm0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EEE42874" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-312-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44552   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p><b>          Pharmacokinetic Interactions between Cyclosporine and Protease Inhibitors in HIV+ Subjects</b> </p>

    <p>          Frassetto, L, Thai, T, Aggarwal, AM, Bucher, P, Jacobsen, W, Christians, U, Benet, LZ, and Floren, LC</p>

    <p>          Drug Metab Pharmacokinet <b>2004</b>.  18(2): 114-120</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15618725&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15618725&amp;dopt=abstract</a> </p><br />

    <p>2.         44553   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Inhibition of pathogenic SHIV replication in macaques treated with antisense DNA of interleukin-4</p>

    <p>          Dhillon, NK, Sui, Y, Potula, R, Dhillon, S, Adany, I, Li, Z, Villinger, F, Pinson, D, Narayan, O, and Buch, S</p>

    <p>          Blood <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15618469&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15618469&amp;dopt=abstract</a> </p><br />

    <p>3.         44554   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Single-Dose Safety and Pharmacokinetics of Amprenavir (141W94), a Human Immunodeficiency Virus Type 1 (HIV-1) Protease Inhibitor, in HIV-Infected Children</p>

    <p>          Yogev, R, Kovacs, A, Chadwick, EG, Homans, JD, Lou, Y, and Symonds, WT</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(1): 336-41</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616313&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616313&amp;dopt=abstract</a> </p><br />

    <p>4.         44555   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Glycine-amide is an active metabolite of the antiretroviral tripeptide glycyl-prolyl-glycine-amide</p>

    <p>          Andersson, E, Horal, P, Jejcic, A, Hoglund, S, Balzarini, J, Vahlne, A, and Svennerholm, B</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(1): 40-4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616273&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616273&amp;dopt=abstract</a> </p><br />

    <p>5.         44556   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Multiple and Multivalent Interactions of Novel Anti-AIDS Drug Candidates, Sulfated Polymannuronate (SPMG)-derived Oligosaccharides, with gp120, and their Anti-HIV Activities</p>

    <p>          Liu, H, Geng, M, Xin, X, Li, F, Zhang, Z, Li, J, Guan, H, and Ding, J</p>

    <p>          Glycobiology <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616125&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15616125&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44557   HIV-LS-312; EMBASE-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Human neutrophil [alpha]-defensin 4 inhibits HIV-1 infection in vitro</p>

    <p>          Wu, Zhibin, Cocchi, Fiorenza, Gentles, David, Ericksen, Bryan, Lubkowski, Jacek, DeVico, Anthony, Lehrer, Robert I, and Lu, Wuyuan</p>

    <p>          FEBS Letters <b>2005</b>.  579(1): 162-166</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4DXT40V-1/2/9c39170020b79a91bdd5cf072ab1edc0">http://www.sciencedirect.com/science/article/B6T36-4DXT40V-1/2/9c39170020b79a91bdd5cf072ab1edc0</a> </p><br />

    <p>7.         44558   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Synthesis of AZT 5&#39;-Triphosphate Mimics and Their Inhibitory Effects on HIV-1 Reverse Transcriptase</p>

    <p>          Wang, G, Boyle, N, Chen, F, Rajappan, V, Fagan, P, Brooks, JL, Hurd, T, Leeds, JM, Rajwanshi, VK, Jin, Y, Prhavc, M, Bruice, TW, and Cook, PD</p>

    <p>          J Med Chem <b>2004</b>.  47(27): 6902-13</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615539&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615539&amp;dopt=abstract</a> </p><br />

    <p>8.         44559   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Isolation and characterization of griffithsin, a novel HIV-inactivating protein from the red alga griffithsia sp</p>

    <p>          Mori, T, O&#39;keefe, BR, Sowder, RC 2nd, Bringans, S, Gardella, R, Berg, S, Cochran, P, Turpin, JA, Buckheit, RW Jr, McMahon, JB, and Boyd, MR</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613479&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613479&amp;dopt=abstract</a> </p><br />

    <p>9.         44560   HIV-LS-312; EMBASE-HIV-12/27/2004</p>

    <p class="memofmt1-2">          HIV-1 interaction with human mannose receptor (hMR) induces production of matrix metalloproteinase 2 (MMP-2) through hMR-mediated intracellular signaling in astrocytes</p>

    <p>          Lopez-Herrera, Albeiro, Liu, Ying, Rugeles, Maria T, and He, Johnny J</p>

    <p>          Biochimica et Biophysica Acta (BBA) - Molecular Basis of Disease <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1Y-4F2B670-1/2/9f676c620ee13dbe9a00917a5733627f">http://www.sciencedirect.com/science/article/B6T1Y-4F2B670-1/2/9f676c620ee13dbe9a00917a5733627f</a> </p><br />

    <p>10.       44561   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Mutations Conferring Resistance to Human Immunodeficiency Virus Type 1 Fusion Inhibitors Are Restricted by gp41 and Rev-Responsive Element Functions</p>

    <p>          Nameki, D, Kodama, E, Ikeuchi, M, Mabuchi, N, Otaka, A, Tamamura, H, Ohno, M, Fujii, N, and Matsuoka, M</p>

    <p>          J Virol <b>2005</b>.  79(2): 764-70</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613304&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613304&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44562   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          HIV-1 Vif can directly inhibit APOBEC3G-mediated cytidine deamination by using a single amino acid interaction and without protein degradation</p>

    <p>          Santa-Marta, M, Aires, da Silva F, Fonseca, A, and Goncalves, J</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15611076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15611076&amp;dopt=abstract</a> </p><br />

    <p>12.       44563   HIV-LS-312; EMBASE-HIV-12/27/2004</p>

    <p class="memofmt1-2">          A PNA-transportan conjugate targeted to the TAR region of the HIV-1 genome exhibits both antiviral and virucidal properties</p>

    <p>          Chaubey, Binay, Tripathi, Snehlata, Ganguly, Sabyasachi, Harris, Dylan, Casale, Ralph A, and Pandey, Virendra N</p>

    <p>          Virology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4DTKMY9-1/2/744b08f95c9dc7941f696a3ee90e62c5">http://www.sciencedirect.com/science/article/B6WXR-4DTKMY9-1/2/744b08f95c9dc7941f696a3ee90e62c5</a> </p><br />

    <p>13.       44564   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Orally active CCR5 antagonists as anti-HIV-1 agents. Part 3: Synthesis and biological activities of 1-benzazepine derivatives containing a sulfoxide moiety</p>

    <p>          Seto, M, Miyamoto, N, Aikawa, K, Aramaki, Y, Kanzaki, N, Iizawa, Y, Baba, M, and Shiraishi, M</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(2): 363-86</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15598559&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15598559&amp;dopt=abstract</a> </p><br />

    <p>14.       44565   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Predicting anti-HIV activity of 2,3-diaryl-1,3-thiazolidin-4-ones: computational approach using reformed eccentric connectivity index</p>

    <p>          Kumar, V, Sardana, S, and Madan, AK</p>

    <p>          J Mol Model (Online) <b>2004</b>.  10(5-6): 399-407</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15597209&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15597209&amp;dopt=abstract</a> </p><br />

    <p>15.       44566   HIV-LS-312; EMBASE-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Prevention of HIV-1 infection by platinum triazines</p>

    <p>          Vzorov, AN, Bhattacharyya, D, Marzilli, LG, and Compans, RW</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4D39WT8-1/2/248d6cca677f7590f135358c2398252b">http://www.sciencedirect.com/science/article/B6T2H-4D39WT8-1/2/248d6cca677f7590f135358c2398252b</a> </p><br />
    <br clear="all">

    <p>16.       44567   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Anti-HIV activities of organic and aqueous extracts of Sutherlandia frutescens and Lobostemon trigonus</p>

    <p>          Harnett, SM, Oosthuizen, V, and van de Venter, M</p>

    <p>          J Ethnopharmacol <b>2005</b>.  96(1-2): 113-119</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588658&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588658&amp;dopt=abstract</a> </p><br />

    <p>17.       44568   HIV-LS-312; EMBASE-HIV-12/27/2004</p>

    <p class="memofmt1-2">          A route for preparing new neamine derivatives targeting HIV-1 TAR RNA</p>

    <p>          Riguet, Emmanuel, Desire, Jerome, Bailly, Christian, and Decout, Jean-Luc</p>

    <p>          Tetrahedron <b>2004</b>.  60(37): 8053-8064</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4CYGY9G-3/2/2cfd6583bd21d5b385a57257c4add999">http://www.sciencedirect.com/science/article/B6THR-4CYGY9G-3/2/2cfd6583bd21d5b385a57257c4add999</a> </p><br />

    <p>18.       44569   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          The HIV-1 nucleocapsid zinc finger protein as a target of antiretroviral therapy</p>

    <p>          Musah, RA</p>

    <p>          Curr Top Med Chem <b>2004</b>.  4(15): 1605-22</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579099&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579099&amp;dopt=abstract</a> </p><br />

    <p>19.       44570   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Anti-HIV drug development--an overview</p>

    <p>          Pereira, CF and Paridaen, JT</p>

    <p>          Curr Pharm Des  <b>2004</b>.  10(32): 4005-37</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579085&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579085&amp;dopt=abstract</a> </p><br />

    <p>20.       44571   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Blocking HIV-1 Vif restores a natural mechanism of intracellular antiviral defense</p>

    <p>          Bovolenta, C</p>

    <p>          Curr Drug Targets Immune Endocr Metabol Disord <b>2004</b>.  4(4): 257-63</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578976&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578976&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.       44572   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          The HIV-1 nucleoside reverse transcriptase inhibitors stavudine and zidovudine alter adipocyte functions in vitro</p>

    <p>          Caron, M, Auclair, M, Lagathu, C, Lombes, A, Walker, UA, Kornprobst, M, and Capeau, J</p>

    <p>          AIDS <b>2004</b>.  18 (16): 2127-36</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577645&amp;dopt=abstract</a> </p><br />

    <p>22.       44573   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Predicted genotypic resistance to the novel entry inhibitor, BMS-378806, among HIV-1 isolates of subtypes A to G</p>

    <p>          Moore, PL, Cilliers, T, and Morris, L</p>

    <p>          AIDS <b>2004</b>.  18 (17): 2327-30</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577547&amp;dopt=abstract</a> </p><br />

    <p>23.       44574   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Phenotypic Susceptibility to Nonnucleoside Inhibitors of Virion-Associated Reverse Transcriptase From Different HIV Types and Groups</p>

    <p>          Tuaillon, E, Gueudin, M, Lemee, V, Gueit, I, Roques, P, Corrigan, GE, Plantier, JC, Simon, F, and Braun, J</p>

    <p>          J Acquir Immune Defic Syndr <b>2004</b>.  37(5): 1543-1549</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577405&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577405&amp;dopt=abstract</a> </p><br />

    <p>24.       44575   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Diversity-oriented solid-phase synthesis and biological evaluation of oligonucleotide hairpins as HIV-1 RT RNase H inhibitors</p>

    <p>          Hannoush, RN, Min, KL, and Damha, MJ</p>

    <p>          Nucleic Acids Res <b>2004</b>.  32(21): 6164-75</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570067&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570067&amp;dopt=abstract</a> </p><br />

    <p>25.       44576   HIV-LS-312; PUBMED-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Monoclonal antibodies to p24-core protein of HIV-1 mediate ADCC and inhibit virus spread in vitro</p>

    <p>          Grunow, R, Franke, L, Hinkula, J, Wahren, B, Fenyo, EM, Jondal, M, and von, Baehr R</p>

    <p>          Clin Diagn Virol <b>1995</b>.  3(3): 221-31</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566804&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>26.       44577   HIV-LS-312; EMBASE-HIV-12/27/2004</p>

    <p class="memofmt1-2">          Enfuvirtide: A fusion inhibitor for the treatment of HIV infection</p>

    <p>          Fung, Horatio B and Guo, Yi</p>

    <p>          Clinical Therapeutics <b>2004</b>.  26(3): 352-378</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VRS-4C6XJFJ-2/2/85399abb2ea26733a6f26c8875f6d797">http://www.sciencedirect.com/science/article/B6VRS-4C6XJFJ-2/2/85399abb2ea26733a6f26c8875f6d797</a> </p><br />

    <p>27.       44578   HIV-LS-312; WOS-HIV-12/19/2004</p>

    <p class="memofmt1-2">          6-[2-phosphonomethoxy)alkoxy]-2,4-diaminopyrimidines: A new class of acyclic pyrimidine nucleoside phosphonates with antiviral activity</p>

    <p>          Balzarini, J, Pannecouque, C, Naesens, L, Andrei, G, Snoeck, R, De, Clercq E, Hockova, D, and Holy, A</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2004</b>.  23(8-9): 1321-1327, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225113700048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225113700048</a> </p><br />

    <p>28.       44579   HIV-LS-312; WOS-HIV-12/19/2004</p>

    <p class="memofmt1-2">          Identification of cellular cofactors for human immunodeficiency virus replication via a ribozyme-based genomics approach</p>

    <p>          Waninger, S, Kuhen, K, Hu, XY, Chatterton, JE, Wong-Staal, F, and Tang, HL</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(23): 12829-12837, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500011</a> </p><br />

    <p>29.       44580   HIV-LS-312; WOS-HIV-12/19/2004</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus replication by a dual CCR5/CXCR4 antagonist</p>

    <p>          Princen, K, Hatse, S, Vermeire, K, Aquaro, S, De, Clercq E, Gerlach, LO, Rosenkilde, M, Schwartz, TW, Skerlj, R, Bridger, G, and Schols, D</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(23): 12996-13006, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500027</a> </p><br />

    <p>30.       44581   HIV-LS-312; WOS-HIV-12/19/2004</p>

    <p class="memofmt1-2">          Lentiviral vectors interfering with virus-induced CD4 down-modulation potently block human immunodeficiency virus type 1 replication in primary lymphocytes</p>

    <p>          Pham, HM, Arganaraz, ER, Groschel, B, Trono, D, and Lama, J</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(23): 13072-13081, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225087500035</a> </p><br />

    <p>31.       44582   HIV-LS-312; WOS-HIV-12/19/2004</p>

    <p class="memofmt1-2">          Polyarginine inhibits gp160 processing by furin and suppresses productive human immunodeficiency virus type 1 infection</p>

    <p>          Kibler, KV, Miyazato, A, Yedavalli, VSRK, Dayton, AI, Jacobs, BL, Dapolito, G, Kim, SJ, and Jeang, KT</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2004</b>.  279(47): 49055-49063, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225098100069">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225098100069</a> </p><br />
    <br clear="all">

    <p>32.       44583   HIV-LS-312; WOS-HIV-12/19/2004</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 5R- and 5S-methyl substituted D- and L-configuration 1,3-dioxolane nucleoside analogs</p>

    <p>          Bera, S, Malik, L, Bhat, B, Carroll, SS, Hrin, R, MacCoss, M, McMasters, DR, Miller, MD, Moyer, G, Olsen, DB, Schleif, WA, Tomassini, JE, and Eldrup, AB</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(23): 6237-6247, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225050600019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225050600019</a> </p><br />

    <p>33.       44584   HIV-LS-312; WOS-HIV-12/19/2004</p>

    <p class="memofmt1-2">          N-substituted pyrrole derivatives as novel human immunodeficiency virus type 1 entry inhibitors that interfere with the gp41 six-helix bundle formation and block virus fusion</p>

    <p>          Jiang, SB, Lu, H, Liu, SW, Zhao, Q, He, YX, and Debnath, AK</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(11): 4349-4359, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225017900041</a> </p><br />

    <p>34.       44585   HIV-LS-312; WOS-HIV-12/26/2004</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of some new fused heterobicyclic derivatives containing 1,2,4-triazolo/1,2,4-triazinopyridinone moieties</p>

    <p>          Abdel-Monem, WR</p>

    <p>          CHEMICAL PAPERS-CHEMICKE ZVESTI <b>2004</b>.  58(4): 276-285, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225247700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225247700010</a> </p><br />

    <p>35.       44586   HIV-LS-312; WOS-HIV-12/26/2004</p>

    <p class="memofmt1-2">          High resolution X-ray structure and potent anti-HIV activity of recombinant dianthin antiviral protein</p>

    <p>          Kurinov, IV, Rajamohan, F, and Uckun, FM</p>

    <p>          ARZNEIMITTEL-FORSCHUNG-DRUG RESEARCH <b>2004</b>.  54(10): 692-702, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225231400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225231400010</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
