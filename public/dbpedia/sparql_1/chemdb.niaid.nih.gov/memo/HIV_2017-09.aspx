

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="T5W2S266GJinLWen5znn1FzkhadqdJiIJRPL1YyShCbd60NGGvWDtISCC0ShE04kyZGnYZ5IolS9DbSJki2L3eG2q349pj6AV6/ANjECD3jMkIasLipfWo7TR4o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4D73522B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: September, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28743447">Dual-targeted anti-TB/anti-HIV Heterodimers.</a> Alexandrova, L., S. Zicari, E. Matyugina, A. Khandazhinskaya, T. Smirnova, S. Andreevskaya, L. Chernousova, C. Vanpouille, S. Kochetkov, and L. Margolis. Antiviral Research, 2017. 145: p. 175-183. PMID[28743447]. PMCID[PMC5576140].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28947797">GRL-09510, a Unique P2-crown-retrahydrofuranylurethane-containing HIV-1 Protease Inhibitor, Maintains its Favorable Antiviral Activity against Highly-Drug-Resistant HIV-1 Variants in Vitro.</a> Amano, M., P. Miguel Salcedo-Gomez, R.S. Yedidi, N.S. Delino, H. Nakata, K. Venkateswara Rao, A.K. Ghosh, and H. Mitsuya. Scientific Reports, 2017. 7(12235): 12pp. PMID[28947797].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408427200024">Synthesis of Some Substituted 5H-Furo[3,2-g]chromene and Benzofuran sulfonate Derivatives as Potent anti-HIV Agents.</a> Amr, A.E., M.M. Abdalla, S.A. Essaouy, M.M.H. Areef, M.H. Elgamal, T.A. Nassear, A.E. Haschich, and M.A. Al-Omar. Russian Journal of General Chemistry, 2017. 87(7): p. 1591-1600. ISI[000408427200024].
    <br />
    <b>[WOS]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28554091">Design and Synthesis of Potent HIV-1 Protease Inhibitors with (S)-Tetrahydrofuran-tertiary Amine-acetamide as P2-ligand: Structure-activity Studies and Biological Evaluation.</a> Bai, X., Z. Yang, M. Zhu, B. Dong, L. Zhou, G. Zhang, J. Wang, and Y. Wang. European Journal of Medicinal Chemistry, 2017. 137: p. 30-44. PMID[28554091].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28794022">Potent in vivo NK Cell-mediated Elimination of HIV-1-infected Cells Mobilized by a gp120-bispecific and Hexavalent Broadly Neutralizing Fusion Protein.</a> Bardhi, A., Y. Wu, W. Chen, W. Li, Z. Zhu, J.H. Zheng, H. Wong, E. Jeng, J. Jones, C. Ochsenbauer, J.C. Kappes, D.S. Dimitrov, T. Ying, and H. Goldstein. Journal of Virology, 2017. 91(20): e00937-17. PMID[28794022].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">6. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28893287">Natural Killer (NK) Cell Receptor-HLA Ligand Genotype Combinations Associated with Protection from HIV Infection: Investigation of How Protective Genotypes Influence Anti HIV NK Cell Functions.</a> Bernard, N.F. AIDS Research and Therapy, 2017. 14(1): p. 38. PMID[28893287]. PMCID[PMC5594513].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28956865">Effects of Amprenavir on HIV-1 Maturation, Production and Infectivity Following Drug Withdrawal in Chronically-infected Monocytes/macrophages.</a> Borrajo, A., A. Ranazzi, M. Pollicita, R. Bruno, A. Modesti, C. Alteri, C.F. Perno, V. Svicher, and S. Aquaro. Viruses, 2017. 9(10): e277. PMID[28956865].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28827354">Covalent Inhibitors for Eradication of Drug-resistant HIV-1 Reverse Transcriptase: From Design to Protein Crystallography.</a> Chan, A.H., W.G. Lee, K.A. Spasov, J.A. Cisneros, S.N. Kudalkar, Z.O. Petrova, A.B. Buckingham, K.S. Anderson, and W.L. Jorgensen. Proceedings of the National Academy of Sciences of the United States of America, 2017. 114(36): p. 9725-9730. PMID[28827354]. PMCID[PMC5594698].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28885587">Anti-HIV Activities and Mechanism of 12-O-Tricosanoylphorbol-20-acetate, a Novel Phorbol Ester from Ostodes katharinae.</a> Chen, H., R. Zhang, R.H. Luo, L.M. Yang, R.R. Wang, X.J. Hao, and Y.T. Zheng. Molecules, 2017. 22(9): e1498. PMID[28885587].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18056282">Design and Profiling of GS-9148, a Novel Nucleotide Analog Active against Nucleoside-resistant Variants of Human Immunodeficiency Virus Type 1, and its Orally Bioavailable Phosphonoamidate Prodrug, GS-9131.</a> Cihlar, T., A.S. Ray, C.G. Boojamra, L. Zhang, H. Hui, G. Laflamme, J.E. Vela, D. Grant, J. Chen, F. Myrick, K.L. White, Y. Gao, K.Y. Lin, J.L. Douglas, N.T. Parkin, A. Carey, R. Pakdaman, and R.L. Mackman. Antimicrobial Agents and Chemotherapy, 2008. 52(2): p. 655-665. PMID[18056282]. PMCID[PMC2224772].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000407987700011">HIV-1 Infection Inhibition by Neem (Azadirachta indica A. Juss.) Leaf Extracts and Azadirachtin.</a> David, P.E., S.G.L. Benjamin, E. Delia, N.H.M. Patricia, and C.V.M.M. del. Indian Journal of Traditional Knowledge, 2017. 16(3): p. 437-441. ISI[000407987700011].
    <br />
    <b>[WOS]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">12. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28294572">N-Hydroxy-substituted 2-Aryl acetamide Analogs: A Novel Class of HIV-1 Integrase Inhibitors.</a> Debnath, U., P. Kumar, A. Agarwal, A. Kesharwani, S.K. Gupta, and S.B. Katti. Chemical Biology &amp; Drug Design, 2017. 90(4): p. 527-534. PMID[28294572].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28829913">Virtual Screening of Acyclovir Derivatives as Potential Antiviral Agents: Design, Synthesis, and Biological Evaluation of New Acyclic Nucleoside Protides.</a> Derudas, M., C. Vanpouille, D. Carta, S. Zicari, G. Andrei, R. Snoeck, A. Brancale, L. Margolis, J. Balzarini, and C. McGuigan. Journal of Medicinal Chemistry, 2017. 60(18): p. 7876-7896. PMID[28829913].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28577966">Efficacy of Carbosilane dendrimers with an Antiretroviral Combination against HIV-1 in the Presence of Semen-derived Enhancer of Viral Infection.</a> Garcia-Broncano, P., R. Cena-Diez, F.J. de la Mata, R. Gomez, S. Resino, and M.A. Munoz-Fernandez. European Journal of Pharmacology, 2017. 811: p. 155-163. PMID[28577966].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28891315">Development of Pyridine Dicoumarols as Potent Anti HIV-1 Leads, Targeting HIV-1 Associated TopoisomeraseII&#946; Kinase.</a> Kammari, K., K. Devaraya, A. Bommakanti, and A.K. Kondapi. Future Medicinal Chemistry, 2017. 9(14): p. 1597-1609. PMID[28891315].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28760905">A 2-Hydroxyisoquinoline-1,3-dione Active-site RNase H Inhibitor Binds in Multiple Modes to HIV-1 Reverse Transcriptase.</a> Kirby, K.A., N.A. Myshakina, M.T. Christen, Y.L. Chen, H.A. Schmidt, A.D. Huber, Z. Xi, S. Kim, R.K. Rao, S.T. Kramer, Q. Yang, K. Singh, M.A. Parniak, Z. Wang, R. Ishima, and S.G. Sarafianos. Antimicrobial Agents and Chemotherapy, 2017. 61(10): e01351-17. PMID[28760905]. PMCID[PMC5610509].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/15855512">Selective Intracellular Activation of a Novel Prodrug of the Human Immunodeficiency Virus Reverse Transcriptase Inhibitor Tenofovir Leads to Preferential Distribution and Accumulation in Lymphatic Tissue.</a> Lee, W.A., G.X. He, E. Eisenberg, T. Cihlar, S. Swaminathan, A. Mulato, and K.C. Cundy. Antimicrobial Agents and Chemotherapy, 2005. 49(5): p. 1898-1906. PMID[15855512]. PMCID[PMC1087627].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">18. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28831040">One-domain CD4 Fused to Human Anti-CD16 Antibody Domain Mediates Effective Killing of HIV-1-infected Cells.</a> Li, W., Y.L. Wu, D.S. Kong, H.J. Yang, Y.P. Wang, J.P. Shao, Y. Feng, W.Z. Chen, L.Y. Ma, T.L. Ying, and D.S. Dimitrov. Scientific Reports, 2017. 7(9130): 12pp. PMID[28831040]. PMCID[PMC5567353].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28877726">The Effect of Bovine BST2A1 on the Release and Cell-to-Cell Transmission of Retroviruses.</a> Liang, Z., Y. Zhang, J. Song, H. Zhang, S. Zhang, Y. Li, J. Tan, and W. Qiao. Virology Journal, 2017. 14(1): p. 173. PMID[28877726]. PMCID[PMC5588738].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">20. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28900421">Efficacy Analysis of Combinatorial siRNAs against HIV Derived from One Double Hairpin RNA Precursor.</a> Liu, C., Z.P. Liang, and X.H. Kong. Frontiers in Microbiology, 2017. 8: p. 1651. PMID[28900421]. PMCID[PMC5581867].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">21. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27428649">Development of anti-HIV Peptides Based on a Viral Capsid Protein.</a> Mizuguchi, T., N. Ohashi, D. Matsumoto, C. Hashimoto, W. Nomura, N. Yamamoto, T. Murakami, and H. Tamamura. Biopolymers, 2017. 108(1): e22920. PMID[27428649].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28754362">Design, Synthesis and Biological Evaluation of Indole Derivatives as Vif Inhibitors.</a> Pu, C.L., R.H. Luo, M.Q. Zhang, X.Y. Hou, G.Y. Yan, J. Luo, Y.T. Zheng, and R. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(17): p. 4150-4155. PMID[28754362].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408695500005">Synthesis and Evaluation of Isoflavones as Potential Anti-inflammatory Inhibitors.</a> Ramaite, I.D.I., M.D. Maluleke, and S.S. Mnyakeni-Moleele. Arabian Journal for Science and Engineering, 2017. 42(10): p. 4263-4271. ISI[000408695500005].
    <br />
    <b>[WOS]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28873410">N-terminally Truncated POM121C Inhibits HIV-1 Replication.</a> Saito, H., H. Takeuchi, T. Masuda, T. Noda, and S. Yamaoka. Plos One, 2017. 12(9): p. e0182434. PMID[28873410]. PMCID[PMC5584925].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28809479">Natural Seminal Amyloids as Targets for Development of Synthetic Inhibitors of HIV Transmission.</a> Sheik, D.A., S. Dewhurst, and J. Yang. Accounts of Chemical Research, 2017. 50(9): p. 2159-2166. PMID[28809479].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408773101033">Antiretroviral Activity of the Conjugates 3&#39;-Azido-3&#39;deoxythymidine and Derivatives of 1,3-Diacylglycerides.</a> Siniavin, A., S. Grinkina, Y. Zhernov, N. Shastina, and M. Khaitov. Allergy, 2017. 72: p. 164. ISI[000408773101033].
    <br />
    <b>[WOS]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">27. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28803740">Vorinostat Renders the Replication-competent Latent Reservoir of Human Immunodeficiency Virus (HIV) Vulnerable to Clearance by CD8 T Cells.</a> Sung, J.A., K. Sholtis, J. Kirchherr, J.D. Kuruc, C.L. Gay, J.L. Nordstrom, C.M. Bollard, N.M. Archin, and D.M. Margolis. EBioMedicine, 2017. 23: p. 52-58. PMID[28803740]. PMCID[PMC5605299].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28827668">A Clue to Unprecedented Strategy to HIV Eradication: &quot;Lock-in and Apoptosis.&quot;</a>Tateishi, H., K. Monde, K. Anraku, R. Koga, Y. Hayashi, H.I. Ciftci, H. DeMirci, T. Higashi, K. Motoyama, H. Arima, M. Otsuka, and M. Fujita. Scientific Reports, 2017. 7(8957): 8pp. PMID[28827668]. PMCID[PMC5567282].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28837332">Discovery of a Novel HIV-1 Integrase/P75 Interacting Inhibitor by Docking Screening, Biochemical Assay, and in Vitro Studies.</a> Wang, Y., H.Q. Lin, P. Wang, J.S. Hu, T.M. Ip, L.M. Yang, Y.T. Zheng, and D. Chi-Cheong Wan. Journal of Chemical Information and Modeling, 2017. 57(9): p. 2336-2343. PMID[28837332].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28878233">A Chalcone Derivative Reactivates Latent HIV-1 Transcription through Activating P-TEFb and Promoting Tat-SEC Interaction on Viral Promoter.</a> Wu, J., M.T. Ao, R. Shao, H.R. Wang, D. Yu, M.J. Fang, X. Gao, Z. Wu, Q. Zhou, and Y.H. Xue. Scientific Reports, 2017. 7(10657): 12pp. PMID[28878233]. PMCID[PMC5587564].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19805909">Small-molecule Screening Using a Human Primary Cell Model of HIV Latency Identifies Compounds that Reverse Latency without Cellular Activation.</a> Yang, H.C., S. Xing, L. Shan, K. O&#39;Connell, J. Dinoso, A. Shen, Y. Zhou, C.K. Shrum, Y. Han, J.O. Liu, H. Zhang, J.B. Margolick, and R.F. Siliciano. The Journal of Clinical Investigation, 2009. 119(11): p. 3473-3486. PMID[19805909]. PMCID[PMC2769176].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25367908">Antiviral Characteristics of GSK1265744, an HIV Integrase Inhibitor Dosed Orally or by Long-acting Injection.</a>Yoshinaga, T., M. Kobayashi, T. Seki, S. Miki, C. Wakasa-Morimoto, A. Suyama-Kagitani, S. Kawauchi-Miki, T. Taishi, T. Kawasuji, B.A. Johns, M.R. Underwood, E.P. Garvey, A. Sato, and T. Fujiwara. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 397-406. PMID[25367908]. PMCID[PMC4291378].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28737946">Structure-guided Optimization of HIV Integrase Strand Transfer Inhibitors.</a> Zhao, X.Z., S.J. Smith, D.P. Maskell, M. Metifiot, V.E. Pye, K. Fesen, C. Marchand, Y. Pommier, P. Cherepanov, S.H. Hughes, and T.R. Burke, Jr. Journal of Medicinal Chemistry, 2017. 60(17): p. 7315-7332. PMID[28737946]. PMCID[PMC5601359].
    <br />
    <b>[PubMed]</b>. HIV_09_2017.</p><br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408773101034">Humic Substance-based Antivirals: Antiretroviral Activity, Mechanisms of Action, and Impact on Mucosal Immunity.</a> Zhernov, Y.V., E.V. Karamov, I.V. Perminova, M.R. Khaitov, and R.M. Khaitov. Allergy, 2017. 72: p. 164-165. ISI[000408773101034].
    <br />
    <b>[WOS]</b>. HIV_09_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170908&amp;CC=WO&amp;NR=2017149518A1&amp;KC=A1">Preparation of C-3 Novel Triterpenes with C-17 Amine Derivatives as HIV Inhibitors.</a> Bandi, P.R., R.R. Kura, D.K. Gazula Levi, P.R. Adulla, V. Mukkera, and S. Neela. Patent. 2017. 2017-IB51272 2017149518: 77pp.
    <br />
    <b>[Patent]</b>. HIV_09_2017.</p>

    <br />

    <p class="plaintext">36. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170914&amp;CC=WO&amp;NR=2017156262A1&amp;KC=A1">Preparation of Acyclic Nucleotide Analogs as Antiviral Agents.</a>Beigelman, L., G. Wang, and M. Zhong. Patent. 2017. 2017-US21559 2017156262: 249pp.
    <br />
    <b>[Patent]</b>. HIV_09_2017.</p>

    <br />

    <p class="plaintext">37. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170908&amp;CC=WO&amp;NR=2017149511A1&amp;KC=A1">Preparation of 2-Oxo-2H-pyrrol-1(5H)-carboxamide Derivatives as anti-HIV Agents and Process for the Production Thereof.</a> Della Ca, N., B. Gabriele, B. Macchi, A. Mastino, S.V. Giofre, R. Romeo, M. Costa, M. Queirolo, and R. Mancuso. Patent. 2017. 2017-IB51261 2017149511: 37pp.
    <br />
    <b>[Patent]</b>. HIV_09_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
