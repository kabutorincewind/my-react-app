

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-351.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0//JS/jT8aM5r3Vbl0x5uJAWQnb3qjv7n+YBNLh9dZ7TCwMRb7ArLV4hengD2ShDIB34gQSA8BoYBA7rdusMZ+xSBpK2pSyIfOaZk4k0XdJ4arPXBdhMabzQaNw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6BA345D9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-351-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49067   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Comparison of a conventional antimicrobial susceptibility assay to an oligonucleotide chip system for detection of drug resistance in Mycobacterium tuberculosis isolates</p>

    <p>          Park, Heekyung, Song, Eun Ju, Song, Eun Sil, Lee, Eun Yup, Kim, Cheol Min, Jeong, Seok Hoon, Shin, Jeong Hwan, Jeong, Joseph, Kim, Sunjoo, Park, Young Kil, Bai, Gill-Han, and Chang, Chulhun L</p>

    <p>          Journal of Clinical Microbiology <b>2006</b>.  44(5): 1619-1624</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     49068   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Oxazolidinone derivatives as antimicrobials, and their preparation, pharmaceutical composition and use for treatment of microbial infections</p>

    <p>          Das, Biswajit, Yadav, Ajay Singh, Ahmed, Shahadat, Gujrati, Arti, Ghosh, Soma, and Rattan, Ashok</p>

    <p>          PATENT:  WO <b>2006051408</b>  ISSUE DATE:  20060518</p>

    <p>          APPLICATION: 2005  PP: 59 pp.</p>

    <p>          ASSIGNEE:  (Ranbaxy Laboratories Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     49069   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Targets and Tools: Recent Advances in the Development of Anti-HCV Nucleic Acids</p>

    <p>          Romero-Lopez, C, Sanchez-Luque, FJ, and Berzal-Herranz, A</p>

    <p>          Infect Disord Drug Targets <b>2006</b>.  6(2): 121-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789875&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789875&amp;dopt=abstract</a> </p><br />

    <p>4.     49070   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Antimicrobial activity studies on some piperidine and pyrrolidine substituted halogenobenzene derivatives</p>

    <p>          Arslan, S, Logoglu, E, and Oktemer, A</p>

    <p>          J Enzyme Inhib Med Chem <b>2006</b>.  21(2): 211-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789435&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789435&amp;dopt=abstract</a> </p><br />

    <p>5.     49071   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Allosteric inhibition of the hepatitis C virus NS5B RNA dependent RNA polymerase</p>

    <p>          Koch, U and Narjes, F</p>

    <p>          Infect Disord Drug Targets <b>2006</b>.  6(1): 31-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787302&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787302&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     49072   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of phenoxy acetic acid derivatives as anti-mycobacterial agents</p>

    <p>          Yar, MS, Siddiqui, AA, and Ali, MA</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16784842&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16784842&amp;dopt=abstract</a> </p><br />

    <p>7.     49073   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Synthesis, antibacterial and antitubercular activities of some 7-+AFs-4-(5-amino-+AFs-1,3,4+AF0-thiadiazole-2-sulfonyl)-piperazin-1-yl+AF0- fluoroquinolonic derivatives</p>

    <p>          Talath, S and Gadad, AK</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781799&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781799&amp;dopt=abstract</a> </p><br />

    <p>8.     49074   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Early and extended early bactericidal activity of levofloxacin, gatifloxacin and moxifloxacin in pulmonary tuberculosis</p>

    <p>          Johnson, JL, Hadad, DJ, Boom, WH, Daley, CL, Peloquin, CA, Eisenach, KD, Jankus, DD, Debanne, SM, Charlebois, ED, Maciel, E, Palaci, M, and Dietze, R</p>

    <p>          Int J Tuberc Lung Dis <b>2006</b>.  10(6): 605-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16776446&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16776446&amp;dopt=abstract</a> </p><br />

    <p>9.     49075   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          In vitro activity of linezolid against Mycobacterium tuberculosis complex, including multidrug-resistant Mycobacterium bovis isolates</p>

    <p>          Tato, M, de, la Pedrosa EG, Canton, R, Gomez-Garcia, I, Fortun, J, Martin-Davila, P, Baquero, F, and Gomez-Mampaso, E</p>

    <p>          Int J Antimicrob Agents <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16774814&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16774814&amp;dopt=abstract</a> </p><br />

    <p>10.   49076   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activities of certain trifluoromethyl-aminoquinoline derivatives</p>

    <p>          Mital, Alka, Negi, Villendra Singh, and Ramachandran, Uma</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2006</b>.(10): 220-227</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>11.   49077   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Characterization of the phosphorylation sites of Mycobacterium tuberculosis serine/threonine protein kinases, PknA, PknD, PknE, and PknH by mass spectrometry</p>

    <p>          Molle, V, Zanella-Cleon, I, Robin, JP, Mallejac, S, Cozzone, AJ, and Becchi, M</p>

    <p>          Proteomics <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16739134&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16739134&amp;dopt=abstract</a> </p><br />

    <p>12.   49078   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Identification of cell cycle regulators in Mycobacterium tuberculosis by inhibition of septum formation and global transcriptional analysis</p>

    <p>          Slayden, RA, Knudson, DL, and Belisle, JT</p>

    <p>          Microbiology <b>2006</b>.  152(Pt 6): 1789-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16735741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16735741&amp;dopt=abstract</a> </p><br />

    <p>13.   49079   OI-LS-351; PUBMED-OI-6/26/2006</p>

    <p class="memofmt1-2">          Selection and characterization of replicon variants dually resistant to thumb- and palm-binding nonnucleoside polymerase inhibitors of the hepatitis C virus</p>

    <p>          Le Pogam, S, Kang, H, Harris, SF, Leveque, V, Giannetti, AM, Ali, S, Jiang, WR, Rajyaguru, S, Tavares, G, Oshiro, C, Hendricks, T, Klumpp, K, Symons, J, Browner, MF, Cammack, N, and Najera, I</p>

    <p>          J Virol <b>2006</b> .  80(12): 6146-6154</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16731953&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16731953&amp;dopt=abstract</a> </p><br />

    <p>14.   49080   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          EmbR, a regulatory protein with ATPase activity, is a substrate of multiple serine/threonine kinases and phosphatase in Mycobacterium tuberculosis</p>

    <p>          Sharma, K, Gupta, M, Krupa, A, Srinivasan, N, and Singh, Y</p>

    <p>          FEBS JOURNAL: FEBS J <b>2006</b>.  273(12): 2711-2721, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237954700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237954700012</a> </p><br />

    <p>15.   49081   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          Toxoplasma gondii: In vitro and in vivo activities of the hydroxynaphthoquinone 2-hydroxy-3-(1 &#39;-propen-3-phenyl)-1, 4-naphthoquinone alone or combined with sulfadiazine</p>

    <p>          Ferreira, RA, Oliveira, AB, Ribeiro, MFB, Tafuri, WL, and Vitor, RWA</p>

    <p>          EXPERIMENTAL PARASITOLOGY: Exp. Parasitol <b>2006</b>.  113(2): 125-129, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237993600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237993600008</a> </p><br />

    <p>16.   49082   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          Structure-activity relationship for nucleoside analogs as inhibitors or substrates of adenosine kinase from Mycobacterium tuberculosis I. Modifications to the adenine moiety</p>

    <p>          Long, MC and Parker, WB</p>

    <p>          BIOCHEMICAL PHARMACOLOGY: Biochem. Pharmacol <b>2006</b>.  71(12): 1671-1682, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238056200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238056200002</a> </p><br />

    <p>17.   49083   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Bisbenzamidines for the treatment of Pneumocystis pneumonia or other infection</p>

    <p>          Walzer, Peter D, Cushion, Melanie T, Mayence, Annie, Huang, Tien Liang, and Vanden Eynde, Jean Jacques</p>

    <p>          PATENT:  WO <b>2006021833</b>  ISSUE DATE:  20060302</p>

    <p>          APPLICATION: 2004  PP: 68 pp.</p>

    <p>          ASSIGNEE:  (University of Cincinnati, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   49084   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Nonclassical 5-Alkyl-6-substituted arylthio-pyrrolo[2,3-d]pyrimidines as potent and selective Toxoplasma gondii dihydrofolate reductase inhibitors</p>

    <p>          Gangjee, Aleem, Jain, Hiteshkumar D, Kisliuk, Roy L, and Queener, Sherry F</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-348</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   49085   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          The effect of lectins on Cryptosporidium parvum oocyst in vitro attachment to host cells</p>

    <p>          Stein, Barry, Stover, Larry, Gillem, Ashley, Winters, Katherine, Lee, John H, and Chauret, Christian</p>

    <p>          Journal of Parasitology <b>2006</b>.  92(1): 1-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   49086   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          NIM811: An HCV replication inhibitor of novel mechanism, exhibits potent antiviral activities alone or in combination with a non-nucleoside HCV polymerase inhibitor</p>

    <p>          Boerner, JE, Ma, S, Yip, CLT, Cooreman, MP, Compton, T, and Lin, K</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  70(1): A34-A35, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200024</a> </p><br />

    <p>21.   49087   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          R1479 is a highly selective inhibitor of NS5B-dependent HCV replication and does not inhibit human DNA and RNA polymerases</p>

    <p>          Ma, H, Le Pogam, S, Leveque, V, Li, WX, Jiang, WR, Najera, I, Symons, J, Cammack, N, and Klumpp, K</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  70(1): A35-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200025</a> </p><br />

    <p>22.   49088   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          Design and synthesis of novel anthranilic acid analogs as HCV polymerase inhibitors</p>

    <p>          Curran, K, Nittoli, T, Insaf, S, Prashad, A, Floyd, B, Orlowski, M, Howe, A, Chropra, R, Agrawal, A, and Bloom, J</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  70(1): A53-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200073">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200073</a> </p><br />

    <p>23.   49089   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of novel potential HCV helicase inhibitors</p>

    <p>          Brancale, A, Vlachakis, D, Barbera, MC, Silvestri, R, Berry, C, and Neyts, J</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  70(1): A53-A54, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200074">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200074</a> </p><br />
    <br clear="all">

    <p>24.   49090   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          Identification of novel HCV inhibitors utilizing virtual screening</p>

    <p>          Cameron, DR</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  70(1): A54-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200075">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200075</a> </p><br />

    <p>25.   49091   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          The design of hepatitis C protease inhibitors and the discovery of VX-950</p>

    <p>          Perni, RB</p>

    <p>          Abstracts, 37th Great Lakes Regional Meeting of the American Chemical Society <b>2006</b>.  37: GLRM-028</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   49092   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          HCVNS5B normucleoside inhibitors specifically block synthesis of single-stranded viral RNA catalyzed by HCV replication complexes in vitro</p>

    <p>          Yang, WG, Sun, YN, Phadke, A, Deshpande, M, and Huang, MJ</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  70(1): A54-A55, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200077</a> </p><br />

    <p>27.   49093   OI-LS-351; WOS-OI-6/26/2006</p>

    <p class="memofmt1-2">          Potent inhibition of both HIV and HCV in vitro by a ring-expanded (&quot;fat&quot;) nucleoside: Part II. Mechanistic studies of anti-HCV activity</p>

    <p>          Zhang, N, Zhang, P, Korba, B, Oldach, D, Lagrange, S, Cova, L, Baier, A, Borowski, P, Fattom, A, Winston, S, Fahim, R, and Hosmane, RS</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  70(1): A55-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200078">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200078</a> </p><br />

    <p>28.   49094   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Preparation of nucleoside analogs for treating Hepatitis C and other Flaviviridae family viral infections</p>

    <p>          Keicher, Jesse D, Roberts, Christopher D, and Dyatkina, Natalia B</p>

    <p>          PATENT:  US <b>2006111311</b>  ISSUE DATE:  20060525</p>

    <p>          APPLICATION: 2005-18840  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   49095   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C replicon RNA synthesis by b-D-2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methylcytidine: a specific inhibitor of hepatitis C virus replication</p>

    <p>          Stuyver, Lieven J, McBrayer, Tamara R, Tharnish, Phillip M, Clark, Jeremy, Hollecker, Laurent, Lostia, Stefania, Nachman, Tammy, Grier, Jason, Bennett, Matthew A, Xie, Meng-Yu, Schinazi, Raymond F, Morrey, John D, Julander, Justin L, Furman, Phillip A, and Otto, Michael J</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(2): 79-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   49096   OI-LS-351; WOS-OI-6/18/2006</p>

    <p class="memofmt1-2">          Inhibition of viruses by RNA interference</p>

    <p>          Stram, Y and Kuzntzova, L</p>

    <p>          VIRUS GENES <b>2006</b>.  32(3): 299-306, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237866400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237866400009</a> </p><br />

    <p>31.   49097   OI-LS-351; WOS-OI-6/18/2006</p>

    <p class="memofmt1-2">          The effect of the administration of human gamma globulins in a model of BCG infection in mice</p>

    <p>          Olivares, N, Leon, A, Lopez, Y, Puig, A, Cadiz, A, Falero, G, Martinez, M, Sarmiento, ME, Farinas, M, Infante, JF, Sierra, G, Solis, RL, and Acosta, A</p>

    <p>          TUBERCULOSIS <b>2006</b>.  86(3-4): 268-272, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237868900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237868900016</a> </p><br />

    <p>32.   49098   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Preparation of pyrazoles and [1,2,4]triazoles as antiviral agents</p>

    <p>          Shipps, Gerald W Jr, Curran, Patrick J, Annis, DAllen, Nash, Huw M, Cooper, Alan B, Zhu, Hugh Y, Wang, James J-S, Desai, Jagdish A, and Girijavallabhan, Viyyoor Moopil</p>

    <p>          PATENT:  WO <b>2006050034</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 104 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   49099   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Discovery and development of VX-950, a novel, covalent, and reversible inhibitor of hepatitis C virus NS3.bul.4A serine protease</p>

    <p>          Lin, C, Kwong, AD, and Perni, RB</p>

    <p>          Infectious Disorders: Drug Targets <b>2006</b>.  6(1): 3-16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   49100   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of alkoxyalkyl esters of phosphonopropoxymethyl-guanine and phosphonopropoxymethyl-diaminopurine</p>

    <p>          Ruiz, Jacqueline C, Aldern, Kathy A, Beadle, James R, Hartline, Caroll B, Kern, Earl R, and Hostetler, Karl Y</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(2): 89-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   49101   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Use of diindolylmethane-related indoles and growth factor receptor inhibitors for the treatment of human cytomegalovirus-associated disease</p>

    <p>          Zeligs, Michael A</p>

    <p>          PATENT:  WO <b>2006047716</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 71 pp.</p>

    <p>          ASSIGNEE:  (Bioresponse LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   49102   OI-LS-351; SCIFINDER-OI-6/20/2006</p>

    <p class="memofmt1-2">          Preparation of nucleoside-lipid conjugates as antiviral and antitumor agents</p>

    <p>          Ahmad, Moghis U, Ali, Shoukath M, Khan, Abdul R, and Ahmad, Imran</p>

    <p>          PATENT:  WO <b>2006029081</b>  ISSUE DATE:  20060316</p>

    <p>          APPLICATION: 2005  PP: 72 pp.</p>

    <p>          ASSIGNEE:  (Neopharm, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   49103   OI-LS-351; WOS-OI-6/18/2006</p>

    <p class="memofmt1-2">          Biological activity of Tat (47-58) peptide on human pathogenic fungi</p>

    <p>          Jung, HJ, Park, Y, Hahm, KS, and Lee, DG</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2006</b>.  345(1): 222-228, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237877700032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237877700032</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
