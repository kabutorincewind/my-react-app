

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BtxSaF25LvW8BTbyTpN7RGi4ZQnkpPIbnaNnorMz9kU246ga/oa8gDmFdZLRzgJ5SDw9/W4eKwrYcP0xxYUL7MHoPvMLyZJd/Nc+bqObUXGRCB5tVTp9nvzwMo0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BA7DEC86" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: March, 2017</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28280303">In Silico Discovery and in Vitro Activity of Inhibitors against Mycobacterium tuberculosis 7,8-Diaminopelargonic acid Synthase (Mtb BioA).</a> Billones, J.B., M.C. Carrillo, V.G. Organo, J.B. Sy, N.A. Clavio, S.J. Macalino, I.A. Emnacen, A.P. Lee, P.K. Ko, and G.P. Concepcion. Drug Design, Development and Therapy, 2017. 11: p. 563-574. PMID[28280303]. PMCID[PMC5338852].<b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28287416">Novel Pharmacological Activity of Artesunate and Artemisinin: Their Potential as Anti-tubercular Agents.</a> Choi, W.H. Journal of Clinical Medicine, 2017. 6(3): E30. PMID[28287416]. PMCID[PMC5372999].<b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28162983">Activity of Moxifloxacin and Linezolid against Mycobacterium tuberculosis in Combination with Potentiator Drugs Verapamil, Timcodar, Colistin and SQ109.</a> de Knegt, G.J., A. van der Meijden, C.P. de Vogel, R.E. Aarnoutse, and J.E. de Steenwinkel. International Journal of Antimicrobial Agents, 2017. 49(3): p. 302-307. PMID[28162983]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000394058900014">Synthesis and Antitubercular Activity of New Thiazolidinones with Pyrazinyl and Thiazolyl Scaffolds.</a> Dhumal, S.T., A.R. Deshmukh, L.D. Khillare, M. Arkile, D. Sarkar, and R.A. Mane. Journal of Heterocyclic Chemistry, 2017. 54(1): p. 125-130. ISI[000394058900014]. <b><br />
    [WOS]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28335544.">Combination Effect of Antituberculosis Drugs and Ethanolic Extract of Selected Medicinal Plants against Multi-drug Resistant Mycobacterium tuberculosis Isolates.</a> Fauziyah, P.N., E.Y. Sukandar, and D.K. Ayuningtyas. Scientia Pharmaceutica, 2017. 85(1): E14. PMID[28335544]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28142116">A Novel Benzothiazinethione Analogue SKLB-TB1001 Displays Potent Antimycobacterial Activities in a Series of Murine Models.</a> Gao, C., T.H. Ye, C.T. Peng, Y.J. Shi, X.Y. You, L. Xiong, K. Ran, L.D. Zhang, X.X. Zeng, N.Y. Wang, L.T. Yu, and Y.Q. Wei. Biomedicine &amp; Pharmacotherapy, 2017. 88: p. 603-609. PMID[28142116]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000394058900092">Synthesis and Characterization of New 1-(4-Methylpiperazin-1-yl)-thioureas as Potential Antitubercular Agents.</a> Hearn, M.J., T. Wang, and M.H. Cynamon. Journal of Heterocyclic Chemistry, 2017. 54(1): p. 720-727. ISI[000394058900092]. <b><br />
    [WOS]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28340368">Synthesis, Molecular Docking, Antimycobacterial and Antimicrobial Evaluation of New Pyrrolo[3,2-C]pyridine Mannich Bases.</a> Jose, G., T.H. Suresha Kumara, H.B. Sowmya, D. Sriram, T.N. Guru Row, A.A. Hosamani, S.S. More, B. Janardhan, B.G. Harish, S. Telkar, and Y.S. Ravikumar. European Journal of Medicinal Chemistry, 2017. 131: p. 275-288. PMID[28340368]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000396376900006">Synthesis and Molecular Modeling Studies of Novel Pyrrole Analogs as Antimycobacterial Agents.</a> Joshi, S.D., U.A. More, K. Pansuriya, T.M. Aminabhavi, and A.K. Gadad. Journal of Saudi Chemical Society, 2017. 21(1): p. 42-57. ISI[000396376900006]. <b><br />
    [WOS]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28274226">Evaluation of the Antimycobacterial Activity of Crude Extracts and Solvent Fractions of Selected Ethiopian Medicinal Plants.</a> Kahaliw, W., A. Aseffa, M. Abebe, M. Teferi, and E. Engidawork. BMC Complementary and Alternative Medicine, 2017. 17(143): 9pp. PMID[28274226]. PMCID[PMC5343536].<b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28235402">Anti-mycobacteria Potential and Synergistic Effects of Combined Crude Extracts of Selected Medicinal Plants Used by Bapedi Traditional Healers to Treat Tuberculosis Related Symptoms in Limpopo Province, South Africa.</a> Komape, N.P.M., V.P. Bagla, P. Kabongo-Kayoka, and P. Masoko. BMC Complementary and Alternative Medicine, 2017. 17(128): 13pp. PMID[28235402]. PMCID[PMC5324313].<b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28106753">Naphthalimides Selectively Inhibit the Activity of Bacterial, Replicative DNA Ligases and Display Bactericidal Effects against Tubercle Bacilli.</a> Korycka-Machala, M., M. Nowosielski, A. Kuron, S. Rykowski, A. Olejniczak, M. Hoffmann, and J. Dziadek. Molecules, 2017. 22(1): E154. PMID[28106753]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28350331">Novel Sulfamethoxazole Ureas and Oxalamide as Potential Antimycobacterial Agents.</a> Kratky, M., J. Stolarikova, and J. Vinsova. Molecules, 2017. 22(4): E535. PMID[28350331]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28196707">Antimicrobial Activity of Rhodanine-3-acetic acid Derivatives.</a> Kratky, M., J. Vinsova, and J. Stolarikova. Bioorganic &amp; Medicinal Chemistry, 2017. 25(6): p. 1839-1845. PMID[28196707]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28131606">Antimicrobial Susceptibility and MIC Distribution of 41 Drugs against Clinical Isolates from China and Reference Strains of Nontuberculous Mycobacteria.</a> Li, G., H. Pang, Q. Guo, M. Huang, Y. Tan, C. Li, J. Wei, Y. Xia, Y. Jiang, X. Zhao, H. Liu, L.L. Zhao, Z. Liu, D. Xu, and K. Wan. International Journal of Antimicrobial Agents, 2017. 49(3): p. 364-374. PMID[28131606]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000394058900022">Design, Synthesis, and Biological Evaluation of New 8-Trifluoromethylquinoline Containing Pyrazole-3-carboxamide Derivatives.</a> Nayak, N., J. Ramprasad, and U. Dalimba. Journal of Heterocyclic Chemistry, 2017. 54(1): p. 171-182. ISI[000394058900022]. <b><br />
    [WOS]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28247991">The Discovery of 2-Aminobenzimidazoles That Sensitize Mycobacterium smegmatis and M. tuberculosis to beta-Lactam Antibiotics in a Pattern Distinct from beta-Lactamase Inhibitors.</a> Nguyen, T.V., M.S. Blackledge, E.A. Lindsey, B.M. Minrovic, D.F. Ackart, A.B. Jeon, A. Obregon-Henao, R.J. Melander, R.J. Basaraba, and C. Melander. Angewandte Chemie, 2017. 56(14): p. 3940-3944. PMID[28247991]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000393477900019">Antimicrobial Activity of Lichen Bryoria capillaris and Its Compound Barbatolic acid.</a> Sariozlu, N.Y., M.Y. Cankilic, M. Candan, and T. Tay. Biomedical Research, 2016. 27: p. S419-S423. ISI[000393477900019]. <b><br />
    [WOS]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28335571">3-Substituted N-benzylpyrazine-2-carboxamide Derivatives: Synthesis, Antimycobacterial and Antibacterial Evaluation.</a> Semelkova, L., O. Jandourek, K. Konecna, P. Paterova, L. Navratilova, F. Trejtnar, V. Kubicek, J. Kunes, M. Dolezal, and J. Zitko. Molecules, 2017. 22(3): E495. PMID[28335571]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28273423">Thermal and Photoinduced Copper-promoted C-Se Bond Formation: Synthesis of 2-Alkyl-1,2-benzisoselenazol-3(2H)-ones and Evaluation against Mycobacterium tuberculosis.</a> Thanna, S., C.M. Goins, S.E. Knudson, R.A. Slayden, D.R. Ronning, and S.J. Sucheck. The Journal of Organic Chemistry, 2017. 82(7): p. 3844-3854. PMID[28273423]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28183185">Rational Design of Selective and Bioactive Inhibitors of the Mycobacterium tuberculosis Proteasome.</a> Totaro, K.A., D. Barthelme, P.T. Simpson, X.J. Jiang, G. Lin, C.F. Nathan, R.T. Sauer, and J.K. Sello. ACS Infectious Diseases, 2017. 3(2): p. 176-181. PMID[28183185]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28248311">Sansanmycin Natural Product Analogues as Potent and Selective Anti-mycobacterials That Inhibit Lipid I Biosynthesis.</a> Tran, A.T., E.E. Watson, V. Pujari, T. Conroy, L.J. Dowman, A.M. Giltrap, A. Pang, W.R. Wong, R.G. Linington, S. Mahapatra, J. Saunders, S.A. Charman, N.P. West, T.D.H. Bugg, J. Tod, C.G. Dowson, D.I. Roper, D.C. Crick, W.J. Britton, and R.J. Payne. Nature Communications, 2017. 8(14414): 9pp. PMID[28248311]. PMCID[PMC5337940].<b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000393929300001">Isatin Hybrids and Their Anti-tuberculosis Activity.</a> Xu, Z., S. Zhang, C. Gao, J. Fan, F. Zhao, Z.S. Lv, and L.S. Feng. Chinese Chemical Letters, 2017. 28(2): p. 159-167. ISI[000393929300001]. <b><br />
    [WOS]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">24.        <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000393744400037">Design, Synthesis and Antitubercular Evaluation of Benzothiazinones Containing an Oximido or Amino Nitrogen Heterocycle Moiety.</a> Zhang, R., K. Lv, B. Wang, L.H. Li, B. Wang, M.L. Liu, H.Y. Guo, A.P. Wang, and Y. Lu. RSC Advances, 2017. 7(3): p. 1480-1483. ISI[000393744400037]. <b><br />
    [WOS]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28282140">Synthesis of Transition-state Inhibitors of Chorismate Utilizing Enzymes from Bromobenzene cis-1,2-dihydrodiol.</a> Zhang, X.K., F. Liu, W.D. Fiers, W.M. Sun, J. Guo, Z. Liu, and C.C. Aldrich. The Journal of Organic Chemistry, 2017. 82(7): p. 3432-3440. PMID[28282140]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28188830">In Vitro Activity of Clarithromycin in Combination with Other Antimicrobial Agents against Mycobacterium abscessus and Mycobacterium massiliense.</a> Zhang, Z., J. Lu, M. Liu, Y. Wang, Y. Zhao, and Y. Pang. International Journal of Antimicrobial Agents, 2017. 49(3): p. 383-386. PMID[28188830]. <b><br />
    [PubMed]</b>. TB_03_2017.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27992879">Inhibitors of Mycobacterium tuberculosis DosRST Signaling and Persistence.</a> Zheng, H.Q., C.J. Colvin, B.K. Johnson, P.D. Kirchhoff, M. Wilson, K. Jorgensen-Muga, S.D. Larsen, and R.B. Abramovitch. Nature Chemical Biology, 2017. 13(2): p. 218-225. PMID[27992879]. <b><br />
    [PubMed]</b>. TB_03_2017.</p>

    <br />

    <h2><a name="_Toc479676105">Patent Citations</a></h2>

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170323&amp;CC=WO&amp;NR=2017049321A1&amp;KC=A1">Benzyl Amine-containing Heterocyclic Compounds and Compositions Useful against Mycobacterial Infection.</a> Miller, M.J. and G. Moraski. Patent. 2017. 2016-US52558 2017049321: 133pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_03_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
