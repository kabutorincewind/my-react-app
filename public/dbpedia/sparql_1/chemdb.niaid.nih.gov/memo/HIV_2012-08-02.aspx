

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-08-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="a4+QSUnPM+d4UkBfVkRJYGXHFnTLc8ogqd9g1XzF+rFKce4bY7PMG4Ft4oYsO5/lKDZKRxO4mFRl2SsW6TC7Y8ciORvLIk+68QhLIZEekLudj2fObjhEjFiGoNU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="30B85367" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 20 - August 2, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22664973">Panel of Prototypical Recombinant Infectious Molecular Clones Resistant to Nevirapine, Efavirenz, Etravirine, and Rilpivirine</a>. Balamane, M., Varghese, V., Melikian, G. L., Fessel, W. J., D.A. Katzenstein and R.W. Shafer, Antimicrobial Agents and Chemotherapy, 2012. 56(8): p. 4522-4524; PMID[22664973].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22844444">A Novel Class of anti-HIV Agents with Multiple Copies of Enfuvirtide Enhances Inhibition of Viral Replication and Cellular Transmission <i>in vitro</i></a>. Chang, Chien-Hsing, Hinkula, J., Loo, M., Falkeborn, T., Li, R., Cardillo, T.M., Rossi, E.A., D.M. Goldenberg and B. Wahren, PLoS One, 2012. 7(7): p. e41235; PMID[22844444].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22664975">Small-Molecule Inhibitors of the LEDGF/p75 Binding Site of Integrase Block HIV Replication and Modulate Integrase Multimerization</a>. Christ, F., Shaw, S., Demeulemeester, J., Desimmie, B.A., Marchand, A., Butler, S., Smets, W., Chaltin, P., Westby, M., Z. Debyser and C. Pickford, Antimicrobial Agents and Chemotherapy, 2012. 56(8): p. 4365-4374; PMID[22664975].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22779681">Critical Role in CXCR4 Signaling and Internalization of the Polypeptide Main Chain in the Amino Terminus of SDF-1alpha Probed by Novel N-Methylated Synthetically and Modularly Modified Chemokine Analogues</a>. Dong, C.Z., Tian, S., Choi, Won-Tak, Kumar, S., Liu, D., Xu, Y., Han, X., Z. Huang and J. An, Biochemistry, 2012. [<b>Epub ahead of Print</b>]; PMID[22779681].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22749283">Enamino-oxindole HIV Protease Inhibitors</a>. Eissenstat, M., Guerassina, T., Gulnik, S., Afonina, E., Silva, Abelardo M., Ludtke, D., Yokoe, H., B. Yu and J. Erickson, Bioorganic and Medicinal Chemistry Letters, 2012. 22(15): p. 5078-5083; PMID[22749283].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22763201">Oxoquinoline Acyclonucleoside Phosphonate Analogues as a New Class of Specific Inhibitors of Human Immunodeficiency Virus Type 1</a>. Faro, L.V., de Almeida, J.M., Cirne-Santos, C.C., Giongo, V.A., Castello-Branco, L.R., Oliveira, Ingrid de B., Barbosa, J.E., Cunha, A.C., Ferreira, V.F., M.C. de Souza and I.C. Paixao, Bioorganic and Medicinal Chemistry Letters, 2012. 22(15): p. 5055-5058; PMID[22763201].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22765892">Benzoxazole and Benzothiazole Amides as Novel Pharmacokinetic Enhancers of HIV Protease Inhibitors</a>. Jonckers, T.H., Rouan, M.-C., Hache, G., Schepens, W., Hallenberger, S., J. Baumeister and J. C. Sasaki, Bioorganic and Medicinal Chemistry Letters, 2012. 22(15): p. 4998-5002; PMID[22765892].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22847190">Ginkgolic Acid Inhibits HIV Protease Activity and HIV Infection <i>in vitro</i></a>. Lu, J-M, Yan, S., Jamaluddin, S., Weakley, S.M., Liang, Z., Siwak, E.B., Q. Yao and C. Chen, Medical Science Monitor, 2012. 18(8): p. BR293-298; PMID[22847190].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22845329">Thiazoline Peptides and a Tris-Phenethyl Urea from Didemnum molle with anti-HIV Activity</a>. Lu, Z., Harper, M.K., Pond, C.D., Barrows, L.R., C. M. Ireland and R. M. Van Wagoner, Journal of Natural Products, 2012. [<b>Epub ahead of print</b>]; PMID[22845329].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22829408">Stereoselective Synthesis of D- and L-Carbocyclic Nucleosides by Enzymatically Catalyzed Kinetic Resolution</a>. Mahler, M., Reichardt, B., Hartjen, P., J. van Lunzen and C. Meier, Chemistry, 2012. [<b>Epub ahead of print</b>]; PMID[22829408].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22834818">Highly Organized Self-Assembled Dendriplexes Based on Poly(Propylene Imine) Glycodendrimer and anti-HIV Oligodeoxynucleotides</a>. Maly, J., Pedziwiatr-Werbicka, E., Maly, M.,Semeradtova, A., Appelhans, D., Danani, A., Zaborski, M., B. Klajnert and M. Bryszewska, Current Medicinal Chemistry, 2012. [<b>Epub ahead of print</b>]; PMID[22834818].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22848626">Silibinin Inhibits HIV-1 Infection by Reducing Cellular Activation and Proliferation</a>. McClure, J., Lovelace, E.S., Elahi, S., Maurice, N.J., Wagoner, J., Dragavon, J., Mittler, J.E., Kraft, Z., Stamatatos, L., Horton, H., De Rosa, S.C., R.W. Coombs and S.J. Polyak, PLoS One, 2012. 7(7): p. e41832. [<b>Epub ahead of print</b>]; PMID[22848626].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22829059">Concise Synthesis and anti-HIV Activity of Pyrimido[1,2-C][1,3]benzothiazin-6-imines and Related Tricyclic Heterocycles</a>. Mizuhara, T., Oishi, S., Ohno, H., Shimura, K., M. Matsuoka and N. Fujii, Organic and Biomolecular Chemistry, 2012. [<b>Epub ahead of print</b>]; PMID[22829059].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22696642">Antiretroviral Agents Effectively Block HIV Replication after Cell-to-Cell Transfer</a>. Permanyer, M., Ballana, E., Ruiz, A., Badia, R., Riveira-Munoz, E., Gonzalo, E., B. Clotet and J. A. Este, Journal of Virology, 2012. 86(16): p.8773-8780; PMID[22696642].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22652226">Diverse Combinatorial Design, Synthesis And <i>in vitro</i> Evaluation of New HEPT Analogues as Potential Non-Nucleoside HIV-1 Reverse Transcription Inhibitors</a>. Puig-de-la-Bellacasa, R., Gimenez, L., Pettersson, S., Pascual, R., Gonzalo, E., Este, J.A., Clotet, B., J. I. Borrell and J. Teixido, European Journal of Medicinal Chemistry, 2012. 54: p. 159-174; PMID[22652226].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22820401">HIV RNA Dimerisation Interference by Antisense Oligonucleotides Targeted to the 5&#39; UTR Structural Elements</a>. Reyes-Darias, J.A., F. J. Sanchez-Luque and A. Berzal-Herranz, Virus Research, 2012. [<b>Epub ahead of print</b>]; PMID[22820401].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22615295">HIV gp120 H375 is Unique to HIV-1 Subtype CRF01_AE and Confers Strong Resistance to the Entry Inhibitor BMS-599793, a Candidate Microbicide Drug</a>. Schader, S.M., Colby-Germinario, S.P., Quashie, P.K., Oliveira, M., Ibanescu, R.-I., Moisi, D., T. Mesplede and M. A. Wainberg, Antimicrobial Agents and Chemotherapy, 2012. 56(8): p. 4257-4267; PMID[22615295].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22832125">Topical Gel Formulation of Broadly Neutralizing anti-HIV-1 Monoclonal Antibody VRC01 Confers Protection Against HIV-1 Vaginal Challenge in a Humanized Mouse Model</a>. Veselinovic, M., Preston Neff, C., L. R. Mulder and R. Akkina, Virology, 2012. [<b>Epub ahead of print</b>]; PMID[22832125].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22608761">Design, Synthesis and Antiviral Activity of Novel Pyridazines</a>. Wang, Z., Wang, M., Yao, X., Li, Y., Tan, J., Wang, L., Qiao, W., Geng, Y., Y. Liu and Q. Wang, European Journal of Medicinal Chemistry, 2012. 54: p. 33-41; PMID[22608761].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22827601">The Role of Individual Carbohydrate-Binding Sites in the Function of the Potent anti-HIV Lectin Griffithsin</a>. Xue, J., Gao, Y., Hoorelbeke, B., Kagiampakis, I., Zhao, B., Demeler, B., J. Balzarini and P. J. Liwang, Molecular Pharmaceutics, 2012. [<b>Epub ahead of print</b>]; PMID[22827601].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">21. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=2&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Small Molecules Targeted to a Non-Catalytic &quot;RVxf&quot; Binding Site of Protein Phosphatase-1 Inhibit HIV-1</a>. T. Ammosova, T.,  Platonov, M.,  Yedavalli, V.R.K., Obukhov, Y., Gordeuk, V.R. , Jeang, K.T., D.Kovalskyy and S. Nekhai, PLoS One, 2012. 7(6): p. e39481. [<b>Epub ahead of print</b>]; ISI[000305892100052].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=1&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Molecular Modeling Study of HIV-1 gp120 Attachment Inhibitor</a>. C. G. Gadhe, C.G.,  G. Kothandan, G., T. Madhavan and S. J. Cho, Medicinal Chemistry Research, 2012. 21(8): p. 1892-1904; ISI[000305559900041].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=3&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Novel Cyclopropyl-Indole Derivatives as HIV Non-Nucleoside Reverse Transcriptase Inhibitors</a>. Hassam, M., A. E. Basson, A.E., Liotta, D.C., Morris, L., W.A.L. van Otterlo and S.C. Pelly, ACS Medicinal Chemistry Letters, 2012. 3(6): p. 470-475; ISI[000305303400009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=4&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Synthesis, Antimycobacterial, Antiviral, Antimicrobial Activities, and QSAR Studies of Isonicotinic Acid-1-(Substituted Phenyl)-Ethylidene/Cycloheptylidene Hydrazides</a>. Judge, V.,  Narasimhan, B., Ahuja, M., Sriram, D., Yogeeswari, P., De Clercq, E., C. Pannecouque and J. Balzarini, Medicinal Chemistry Research, 2012. 21(8): p. 1935-1952; ISI[000305559900046].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=5&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Cytotoxicity and anti-HIV Evaluations of Some New Synthesized Quinazoline and Thioxopyrimidine Derivatives Using 4-(Thiophen-2-yl)-3,4,5,6-tetrahydrobenzo(h)quinazoline-2(1H)-thione as Synthon</a>. Mohamed, Y.A., A. E. Amr, A.E., Mohamed, S.F., Abdalla, M.M., M.A. Al-Omar and S.H. Shfik, Journal of Chemical Sciences, 2012. 124(3): p. 693-702; ISI[000305595200016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=6&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">SAR and Lead Optimization of an HIV-1 Vif-APOBEC3G Axis Inhibitor</a>. Mohammed, I., Parai, M.K., Jiang, X.P., Sharova, N., Singh, G., M. Stevenson and T. M. Rana, ACS Medicinal Chemistry Letters, 2012. 3(6): p.465-469; ISI[000305303400008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=7&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Design, Synthesis and Antiviral Activity of Novel Quinazolinones</a>. Wan, Z.W., Wang, M.X., Yao, X., Li, Y., Tan, J., Wang, L.Z., Qiao, W.T., Geng, Y.Q., Y.X. Liu and Q.M. Wang, European Journal of Medicinal Chemistry, 2012. 53: p. 275-282; ISI[000305863100029].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=8&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Arylazolyl(azinyl)thioacetanilide. Part 9: Synthesis and Biological Investigation of Thiazolylthioacetamides Derivatives as a Novel Class of Potential Antiviral Agents</a>. Zhan, P., Wang, L., Liu, H., Chen, X.W., Li, X., Jiang, X., Zhang, Q.Q., Liu, X.Y., Pannecouque, C., Naesens, L., De Clercq, E., A. L. Liu and G. H. Du, Archives of Pharmacal Research, 2012. 35(6): p. 975-986; ISI[000305841200005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://apps.webofknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=9&amp;SID=3CgoOfeD@M376p4MkP6&amp;page=1&amp;doc=1">Potent anti-HIV Activities and Mechanisms of Action of a Pine Cone Extract from <i>Pinus yunnanensis</i></a>. Zhang, X., Yang, L.M., Liu, G.M., Liu, Y.J., Zheng, C.B., Lv, Y.J., H.Z. Li and Y.T. Zheng, Molecules, 2012. 17(6): p. 6916-6929; ISI[000305800400050].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Latent HIV Reactivation</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22837004">Administration of Vorinostat Disrupts HIV-1 Latency In Patients on Antiretroviral Therapy.</a> Archin, N.M., Liberty, A.L., Kashuba, A.D., Choudhary, S.K., Kuruc, J.D., Crooks, A.M. , Parker, D.C. , Anderson, E.M., Kearney, M.F., Strain, M.C., Richman, D.D., Hudgens, M.G., Bosch, R.J., Coffin, J.M., Eron, J.J., D.J. Hazuda and D.M. Margolis, Nature, 2012 487(7408): p.  482-485; PMID[22837004].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22322557">Reactivation of Latent HIV-1 by a Wide Variety of Butyric Acid-Producing Bacteria</a>. Imai, K., Yamada, K., Tamura, M., K. Ochiai and T. Okamoto, Cellular and Molecular Life Sciences, 2012. 69(15): p. 2583-2592; PMID[22322557].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0720-080212.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
