

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-06-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1SEmRXRBurtNKHyq48YADCDBpb+ZCok0uqUvSaGIloUz3ibEJtS6e5/R1wTEz9KxCUlEiWLMyMkjfEnyYix1GqIes4w9PJilfO5RkBKNbQUcz+l7V8+eoyuV7rs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="81E8C531" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">June 10 - June 23, 2011</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21644248">Two Antimicrobial and Nematicidal Peptides Derived from Sequences Encoded Picea sitchensis.</a> Liu, R., L. Mu, H. Liu, L. Wei, T. Yan, M. Chen, K. Zhang, J. Li, D. You, and R. Lai. Journal of Peptide Science, 2011. <b>[Epub ahead of print]</b>; PMID[21644248].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21691785.">Peptide Derived from Anti-idiotypic Single-chain Antibody is a Potent Antifungal Agent Compared to its Parent Fungicide HM-1 Killer Toxin Peptide</a>. Kabir, M.E., N. Karim, S. Krishnaswamy, D. Selvakumar, M. Miyamoto, Y. Furuichi, and T. Komiyama. Applied Microbiology and Biotechnology, 2011. <b>[Epub ahead of print]</b>; PMID[21691785].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21690286">Synergistic Effect of Antituberculosis Drugs and Azoles In vitro against Histoplasma capsulatum var. capsulatum.</a> Cordeiro, R.D., F.J. Marques, R.S. Brilhante, K.R. Silva, C.I. Mourao, E.P. Caetano, M.A. Fechine, J.F. Ribeiro, D.D. Castelo-Branco, R.A. Lima, J.R. Mesquita, A.J. Monteiro, F.A. Rocha, M.F. Rocha, and J.J. Sidrim. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21690286].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21669279">Interesting Anticandidal Effects of Anisic Aldehydes on Growth and Proton-pumping-ATPase-targeted Activity.</a> Shreaz, S., R. Bhatia, N. Khan, S. Imran Ahmad, S. Muralidhar, S.F. Basir, N. Manzoor, and L.A. Khan. Microbial Pathogenesis, 2011. <b>[Epub ahead of print]</b>; PMID[21669279].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21654581">Synthesis and Antimicrobial Activity of Some Novel 5-Alkyl-6-substituted Uracils and Related Derivatives</a>. Al-Turkistani, A.A., O.A. Al-Deeb, N.R. El-Brollosy, and A.A. El-Emam. Molecules, 2011. 16(6): p. 4764-4774; PMID[21654581]. <b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21691438">Advent of Imidazo[1,2-a]pyridine-3-carboxamides with Potent Multi- and Extended Drug Resistant Antituberculosis Activity</a>. Moraski, G.C., L.D. Markley, P.A. Hipskind, H. Boshoff, S. Cho, S.G. Franzblau, and M.J. Miller. ACS Medicinal Chemistry Letters, 2011. 2(6): p. 466-470; PMID[21691438].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21678907">Design, Synthesis and X-ray Crystallographic Studies of alpha-Aryl Substituted Fosmidomycin Analogues as Inhibitors of Mycobacterium tuberculosis 1-Deoxy-D-xylulose-5-phosphate Reductoisomerase</a>. Andaloussi, M., L.M. Henriksson, A. Wieckowska, M. Lindh, C. Bjorkelid, A.M. Larsson, S. Suresh, H. Iyer, B.R. Srinivasa, T. Bergfors, T. Unge, S.L. Mowbray, M.L. Larhed, A.T. Jones, and A.B. Karlen. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21678907]. <b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21657202">Synthesis and Evaluation of 5-Substituted 2&#39;-deoxyuridine Monophosphate Analogues As Inhibitors of Flavin-Dependent Thymidylate Synthase in Mycobacterium tuberculosis</a>. Kogler, M., B. Vanderhoydonck, S. De Jonghe, J. Rozenski, K. Van Belle, J. Herman, T. Louat, A. Parchina, C. Sibley, E. Lescrinier, and P. Herdewijn. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21657202].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21656601">Antimycobacterial and Synergistic Effects of 18beta-Glycyrrhetinic Acid or Glycyrrhetinic acid-30-piperazine in Combination with Isoniazid, Rifampicin or Streptomycin against Mycobacterium bovis</a>. Zhou, X., L. Zhao, X. Liu, X. Li, F. Jia, Y. Zhang, and Y. Wang. Phytotherapy Research, 2011. <b>[Epub ahead of print]</b>; PMID[21656601].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21563281">The Natural Product Cyclomarin Kills Mycobacterium Tuberculosis by Targeting the ClpC1 Subunit of the Caseinolytic Protease.</a> Schmitt, E.K., M. Riwanto, V. Sambandamurthy, S. Roggo, C. Miault, C. Zwingelstein, P. Krastel, C. Noble, D. Beer, S.P. Rao, M. Au, P. Niyomrattanakit, V. Lim, J. Zheng, D. Jeffery, K. Pethe, and L.R. Camacho. Angewandte Chemie (International Edition in English), 2011. 50(26): p. 5889-5891; PMID[21563281].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21536127">Transformation of Cinnamic acid from Trans- to Cis-form Raises a Notable Bactericidal and Synergistic Activity against Multiple-drug Resistant Mycobacterium tuberculosis</a>. Chen, Y.L., S.T. Huang, F.M. Sun, Y.L. Chiang, C.J. Chiang, C.M. Tsai, and C.J. Weng. European Journal of Pharmaceutical Sciences, 2011. 43(3): p. 188-194; PMID[21536127].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p class="memofmt2-h1"> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21682873">Hypericum lanceolatum (Hypericaceae) as a Potential Source of New Anti-malarial agents: a Bioassay-guided Fractionation of the Stem Bark.</a> Zofou, D., T.K. Kowa, H.K. Wabo, M.N. Ngemenya, P. Tane, and V.P. Titanji. Malaria Journal, 2011. 10(1): p. 167; PMID[21682873].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21570837">2,4-Diarylthiazole Antiprion Compounds as a Novel Structural Class of Antimalarial Leads.</a> Thompson, M.J., J.C. Louth, S.M. Little, B. Chen, and I. Coldham. Bioorganic and Medicinal Chemistry Letters, 2011. 21(12): p. 3644-3647; PMID[21570837].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0610-062311.</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290847800015">Cryptic Aspergillus nidulans Antimicrobials</a> Giles, S.S., A.A. Soukup, C. Lauer, M. Shaaban, A. Lin, B.R. Oakley, C.C.C. Wang, and N.P. Keller. Applied and Environmental Microbiology, 2011. 77(11): p. 3669-3675; ISI[000290847800015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291117400025">Inhibitory Effect of Cyclic Trihydroxamate Siderophore, Desferrioxamine E, on the Biofilm Formation of Mycobacterium Species</a>. Ishida, S., M. Arai, H. Niikawa, and M. Kobayashi. Biological &amp; Pharmaceutical Bulletin, 2011. 34(6): p. 917-920; ISI[000291117400025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291044100007">Lactam nonanic acid, a New Substance from Cleome viscosa with Allelopathic and Antimicrobial Properties</a> Jana, A. and S.M. Biswas. Journal of Biosciences 2011. 36(1): p. 27-35; ISI[000291044100007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290955400018">Antimycobacterial Activity of Novel 1,2,4-Oxadiazole-pyranopyridine/chromene Hybrids Generated by Chemoselective 1,3-Dipolar Cycloadditions of Nitrile oxides.</a> Kumar, R.R., S. Perumal, J.C. Menendez, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2011. 19(11): p. 3444-3450; ISI[000290955400018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291121500020">New Triterpenes from Barringtonia asiatica.</a>Ragasa, C.Y., D.L. Espineli, and C.C. Shen. Chemical &amp; Pharmaceutical Bulletin, 2011. 59(6): p. 778-782; ISI[000291121500020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291078400007">Synthesis of Oxadiazoles and Pyrazolones as a Antimycobacterial and Antimicrobial agents.</a> Thaker, K.M., R.M. Ghetiya, S.D. Tala, B.L. Dodiya, K.A. Joshi, K.L. Dubal, and H.S. Joshi. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2011. 50(5): p. 738-744; ISI[000291078400007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291145900037">Novel Fungicidal Benzylsulfanyl-phenylguanidines.</a> Thevissen, K., K. Pellens, K. De Brucker, I. Francois, K.K. Chow, E.M.K. Meert, W. Meert, G. Van Minnebruggen, M. Borgers, V. Vroome, J. Levin, D. De Vos, L. Maes, P. Cos, and B.P.A. Cammue. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(12): p. 3686-3692; ISI[000291145900037].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291145900039">New 1,4-di-N-Oxide-quinoxaline-2-ylmethylene isonicotinic acid Hydrazide Derivatives as Anti-mycobacterium Tuberculosis Agents.</a> Torres, E., E. Moreno, S. Ancizu, C. Barea, S. Galiano, I. Aldana, A. Monge, and S. Perez-Silanes. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(12): p. 3699-3703; ISI[000291145900039].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0610-062311.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
