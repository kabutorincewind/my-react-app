

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-05-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hqNZxqsy/QheOSTMaDKkUG5VQNlDRgn8H2XFBbxb8tNWNQcEv1GpNsuMvNBJsUUWZ6732FEvBWGef38CYEj8w/tiEw/FzTDWzxqRVIveuD6N0AvMsdml1zB1X6U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8F5C59A6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: May 11 - May 24, 2012</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22618567">Frequent Emergence of N348I in HIV-1 Subtype C Reverse Transcriptase with Failure of Initial Therapy Reduces Susceptibility to Reverse Transcriptase Inhibitors.</a> Brehm, J.H., D.L. Koontz, C.L. Wallis, K.A. Shutt, I. Sanne, R. Wood, J.A. McIntyre, W.S. Stevens, N. Sluis-Cremer, and J.W. Mellors, Clinical Infectious Disease, 2012. <b>[Epub ahead of print]</b>; PMID[22618567].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22587465">Identification of HIV-1 Inhibitors Targeting the Nucleocapsid Protein.</a> Breuer, S., M.W. Chang, J. Yuan, and B.E. Torbett, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22587465].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22581908">Reformulated Tenofovir Gel for Use as a Dual Compartment Microbicide.</a> Dezzutti, C.S., L.C. Rohan, L. Wang, K. Uranker, C. Shetler, M. Cost, J.D. Lynam, and D. Friend, Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22581908].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22607556">Combinations of Griffithsin with Other Carbohydrate-binding Agents (CBAs) Demonstrate Superior Activity against HIV-1, HIV-2 and Selected CBA-resistant HIV-1 Strains.</a> Ferir, G., D. Huskens, K.E. Palmer, D.M. Boudreaux, M.M. Swanson, D.M. Markovitz, J. Balzarini, and D. Schols, AIDS Research and Human Retroviruses, 2012. <b>[Epub ahead of print]</b>; PMID[22607556].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22607096">Correction to Design, Synthesis, and X-ray Crystallographic Analysis of a Novel Class of HIV-1 Protease Inhibitors.</a> Ganguly, A.K., S.S. Alluri, D. Caroccia, D. Biswas, C.H. Wang, E. Kang, Y. Zhang, A.T. McPhail, S.S. Carroll, C. Burlein, V. Munshi, P. Orth, and C. Strickland, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22607096].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22592582">Retrocyclin RC-101 Blocks HIV-1 Transmission across Cervical Mucosa in an Organ Culture.</a> Gupta, P., D. Ratner, M. Ding, B. Patterson, L.C. Rohan, T.A. Reinhart, V. Ayyavoo, X. Huang, D.L. Patton, B. Ramratnam, and A.M. Cole, Journal of Acquired Immune Deficiency Syndrome, 2012. <b>[Epub ahead of print]</b>; PMID[22592582].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22584270">A High-throughput Assay for HIV-1 Integrase 3&#39;-processing Activity Using Time-resolved Fluorescence.</a> Han, Y.S., P. Quashie, T. Mesplede, H. Xu, K. Mekhssian, C. Fenwick, and M.A. Wainberg, Journal of Virological Methods, 2012. <b>[Epub ahead of print]</b>; PMID[22584270].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22507205">A Comparative Study of Backbone Versus Side Chain Peptide Cyclization: Application for HIV-1 Integrase Inhibitors.</a> Hayouka, Z., A. Levin, M. Hurevich, D.E. Shalev, A. Loyter, C. Gilon, and A. Friedler, Bioorganic &amp; Medicinal Chemistry, 2012. 20(10): p. 3317-3322; PMID[22507205].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22447925">CCR5 Antagonist TD-0680 Uses a Novel Mechanism for Enhanced Potency against HIV-1 Entry, Cell-mediated Infection, and a Resistant Variant.</a> Kang, Y., Z. Wu, T.C. Lau, X. Lu, L. Liu, A.K. Cheung, Z. Tan, J. Ng, J. Liang, H. Wang, S. Li, B. Zheng, B. Li, L. Chen, and Z. Chen, Journal of Biological Chemistry, 2012. 287(20): p. 16499-16509; PMID[22447925].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22574920">Energetics of Mutation-induced Changes in Potency of Lersivirine against HIV-1 Reverse Transcriptase.</a> Kar, P. and V. Knecht, Journal of Physical Chemistry B, 2012. <b>[Epub ahead of print]</b>; PMID[22574920].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615275">Maraviroc and Other HIV-1 Entry Inhibitors Exhibit a Class-specific Redistribution Effect That Results in Increased Extracellular Viral Load.</a> Kramer, V.G., S.M. Schader, M. Oliveira, S. Colby-Germinario, D.A. Donahue, D.N. Singhroy, R. Tressler, R.D. Sloan, and M.A. Wainberg, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22615275].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615996">Efficacy, Stability, and Biosafety of Sifuvirtide Gel as a Microbicide Candidate against HIV-1.</a> Li, L., Y. Ben, S. Yuan, S. Jiang, J. Xu, and X. Zhang, PLoS One, 2012. 7(5): p. e37381; PMID[22615996].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22592973">Synthesis and Biological Evaluation of Andrographolide Derivatives as Potent anti-HIV Agents.</a> Tang, C., Y. Liu, B. Wang, G. Gu, L. Yang, Y. Zheng, H. Qian, and W. Huang, Archiv der Pharmazie (Weinheim), 2012. <b>[Epub ahead of print]</b>; PMID[22592973].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22584351">IFN-lambda Inhibits HIV-1 Integration and Post-transcriptional Events in Vitro, but There Is Only Limited in Vivo Repression of Viral Production.</a> Tian, R.R., H.X. Guo, J.F. Wei, C.K. Yang, S.H. He, and J.H. Wang, Antiviral Research, 2012. <b>[Epub ahead of print]</b>; PMID[22584351].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22609684">Benzophenone Glycosides and Epicatechin Derivatives from Malania oleifera.</a> Wu, X.D., J.T. Cheng, J. He, X.J. Zhang, L.B. Dong, X. Gong, L.D. Song, Y.T. Zheng, L.Y. Peng, and Q.S. Zhao, Fitoterapia, 2012. <b>[Epub ahead of print]</b>; PMID[22609684].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22608061">Characterization of a Peptide Domain within the GB Virus C Envelope Glycoprotein (E2) That Inhibits HIV Replication.</a> Xiang, J., J.H. McLinden, T.M. Kaufman, E.L. Mohr, N. Bhattarai, Q. Chang, and J.T. Stapleton, Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22608061].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22469467">Crystal Structures of Multidrug-resistant HIV-1 Protease in Complex with Two Potent Anti-malarial Compounds.</a> Yedidi, R.S., Z. Liu, Y. Wang, J.S. Brunzelle, I.A. Kovari, P.M. Woster, L.C. Kovari, and D. Gupta, Biochemical and Biophysical Research Communications, 2012. 421(3): p. 413-417; PMID[22469467].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22480519">EGCG Inhibits Tat-induced LTR Transactivation: Role of Nrf2, AKT, AMPK Signaling Pathway.</a> Zhang, H.S., T.C. Wu, W.W. Sang, and Z. Ruan, Life Sciences, 2012. 90(19-20): p. 747-754; PMID[22480519].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22607384">Preclinical Development of TLR Ligands as Drugs for the Treatment of Chronic Viral Infections.</a> Zhang, X., A. Kraft, R. Broering, J.F. Schlaak, U. Dittmer, and M. Lu, Expert Opinion on Drug Discovery, 2012. <b>[Epub ahead of print]</b>; PMID[22607384].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302777800035">A Role of Template Cleavage in Reduced Excision of Chain-terminating Nucleotides by Human Immunodeficiency Virus Type 1 Reverse Transcriptase Containing the M184V Mutation.</a> Acosta-Hoyos, A.J., S.E. Matsuura, P.R. Meyer, and W.A. Scott, Journal of Virology, 2012. 86(9): p. 5122-5133; ISI[000302777800035].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302993200008">Synthesis and anti-HIV Activities of Suramin Conjugates of 3 &#39;-Fluoro-2 &#39;,3 &#39;-dideoxythymidine and 3 &#39;-Azido-2 &#39;,3 &#39;-dideoxythymidine.</a> Agarwal, H.K., G.F. Doncel, and K. Parang, Medicinal Chemistry, 2012. 8(2): p. 193-197; ISI[000302993200008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303154300012">Electronic Structure and Spectroscopic Properties of anti-HIV Active Aminophenols.</a> Bazyl, O.K., V.Y. Artyukhov, G.V. Maier, G.B. Tolstorozhev, T.F. Raichenok, I.V. Skornyakov, O.I. Shadyro, V.L. Sorokin, and G.A. Ksendzova, Optics and Spectroscopy, 2012. 112(2): p. 223-232; ISI[000303154300012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302777800013">Single-domain Antibody-SH3 Fusions for Efficient Neutralization of HIV-1 Nef Functions.</a> Bouchet, J., C. Herate, C.A. Guenzei, C. Verollet, A. Jarviluoma, J. Mazzolini, S. Rafie, P. Chames, D. Baty, K. Saksela, F. Niedergang, I. Maridonneau-Parini, and S. Benichou, Journal of Virology, 2012. 86(9): p. 4856-4867; ISI[000302777800013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302977300019">Cyclic Peptide Inhibitors of HIV-1 Capsid-human Lysyl-tRNA Synthetase Interaction.</a> Dewan, V., T. Liu, K.M. Chen, Z.Q. Qian, Y. Xiao, L. Kleiman, K.V. Mahasenan, C.L. Li, H. Matsuo, D.H. Pei, and K. Musier-Forsyth, ACS Chemical Biology, 2012. 7(4): p. 761-769; ISI[000302977300019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303284400024">Identification of HIV-1 Reverse Transcriptase Dual Inhibitors by a Combined Shape-, 2D-fingerprint- and Pharmacophore-based Virtual Screening Approach.</a> Distinto, S., F. Esposito, J. Kirchmair, M.C. Cardia, M. Gaspari, E. Maccioni, S. Alcaro, P. Markt, G. Wolber, L. Zinzula, and E. Tramontano, European Journal of Medicinal Chemistry, 2012. 50: p. 216-229; ISI[000303284400024].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302964300049">Design, Synthesis, and Biological Activity of Novel 1,4-Disubstituted piperidine/piperazine Derivatives as CCR5 Antagonist-based HIV-1 Entry Inhibitors.</a> Dong, M.X., L. Lu, H.T. Li, X.H. Wang, H. Lu, S.B. Jiang, and Q.Y. Dai, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(9): p. 3284-3286; ISI[000302964300049].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302848800012">Lignan Derivatives from the Leaves Nicotiana tabacum and Their Activities.</a> Gao, X.M., X.S. Li, X.Z. Yang, H.X. Mu, Y.K. Chen, G.Y. Yang, and Q.F. Hu, Heterocycles, 2012. 85(1): p. 147-153; ISI[000302848800012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302978900071">Determination of anti-HIV Active Constituents of Helichrysum species by Means of Metabolomics.</a> Heyman, H.M., V. Maharaj, C. Kenyon, and J.J.M. Meyer, South African Journal of Botany, 2012. 79: p. 189-189; ISI[000302978900071]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302768200078">Footprint-based Identification of Viral Entry Inhibitors Targeting HIVgp41.</a> Holden, P.M., H. Kaur, R. Goyal, M. Gochin, and R.C. Rizzo, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 3011-3016; ISI[000302768200078].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302951100084">The Combined anti-HIV-1 Activity of Emtricitabine and Tenofovir with the Integrase Inhibitors Elvitegravir or Raltegravir Show High Levels of Synergy in Vitro.</a> Kulkarni, R., R. Hluhanich, D. McColl, M.E. Abram, M.D. Miller, and K. White, Hiv Medicine, 2012. 13: p. 29-29; ISI[000302951100084].</p>

    <p class="plaintext"><b>WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301797800025">Sometimes It Takes Two to Tango: Contributions of Dimerization to Functions of Human Alpha-defensin HNP1 Peptide.</a> Pazgier, M., G. Wei, B. Ericksen, G. Jung, Z.B. Wu, E. de Leeuw, W.R. Yuan, H. Szmacinski, W.Y. Lu, J. Lubkowski, and R.I. Lehrer, Journal of Biological Chemistry, 2012. 287(12): p. 8944-8953; ISI[000301797800025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302982000031">Concise Synthesis of 2-Benzazepine Derivatives and Their Biological Activity.</a> So, M., T. Kotake, K. Matsuura, M. Inui, and A. Kamimura, Journal of Organic Chemistry, 2012. 77(8): p. 4017-4028; ISI[000302982000031].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303012900004">Recent Advances in the Research of 2,3-Diaryl-1,3-thiazolidin-4-one Derivatives as Potent HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Tian, Y., P. Zhan, D. Rai, J.Y. Zhang, E. De Clercq, and X.Y. Liu, Current Medicinal Chemistry, 2012. 19(13): p. 2026-2037; ISI[000303012900004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303246900001">Exploring Marine Cyanobacteria for Lead Compounds of Pharmaceutical Importance.</a> Uzair, B., S. Tabassum, M. Rasheed, and S.F. Rehman, Scientific World Journal, 2012: p. 1-10; ISI[000303246900001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302990200004">Practical Synthesis of a Benzophenone-based NNRT Inhibitor of HIV-1.</a> Wang, X.J., L. Zhang, X.F. Sun, H. Lee, D. Krishnamurthy, J.A. O&#39;Meara, S. Landry, C. Yoakim, B. Simoneau, N.K. Yee, and C.H. Senanayake, Organic Process Research &amp; Development, 2012. 16(4): p. 561-566; ISI[000302990200004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303284400039">Hydroxyl May Not Be Indispensable for Raltegravir: Design, Synthesis and SAR Studies of Raltegravir Derivatives as HIV-1 Inhibitors.</a> Wang, Z.W., M.X. Wang, X. Yao, Y. Li, W.T. Qiao, Y.Q. Geng, Y.X. Liu, and Q.M. Wang, European Journal of Medicinal Chemistry, 2012. 50: p. 361-369; ISI[000303284400039]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302768200045">Discovery of HIV Fusion Inhibitors Targeting gp41 Using a Comprehensive Alpha-helix Mimetic Library.</a> Whitby, L.R., K.E. Boyle, L.F. Cai, X.Q. Yu, M. Gochin, and D.L. Boger, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2861-2865; ISI[000302768200045].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302843000010">Flavonoids and Antioxidative Enzymes in Temperature-challenged Roots of Scutellaria baicalensis Georgi.</a> Yuan, Y., L.F. Shuai, S.Q. Chen, L.Q. Huang, S.S. Qin, and Z.C. Yang, Zeitschrift fur Naturforschung Section C-A Journal of Biosciences, 2012. 67(1-2): p. 77-85; ISI[000302843000010]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0511-052412.</p>

    <h2>Animal Studies</h2>

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22586094">Macaque Studies of Vaccine and Microbicide Combinations for Preventing HIV-1 Sexual Transmission.</a> Barouch, D.H., P.J. Klasse, J. Dufour, R.S. Veazey, and J.P. Moore, Proceedings of the National Academy of Sciences of the United States of America, 2012; PMID[22586094].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22610784">Pharmacokinetic Profiling of Efavirenz-Emtricitabine-Tenofovir Fixed Dose Combination in Pregnant and Non-pregnant Rats.</a> Nirogi, R., G. Bhyrapuneni, V. Kandikere, N. Muddana, R. Saralaya, P. Komarneni, K. Mudigonda, and K. Mukkanti, Biopharmaceutics and Drug Disposition, 2012; PMID[22610784].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0511-052412.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
