

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-08-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nlnR+LqsPJ8529oXYUx1jeyF/L8LD0bYk9Yq82iPq2P2sr4q1eFE/A9OqRh2X9K2UDBuQ6o5iNTHejs9jZmxfbPDa/9XX478SgUGcJIhJRUg+H9OUkQ7V2rnN9k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="51104F78" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses  Citation List:   </h1>

    <h1 class="memofmt2-h1">August 5, 2009 - August 18, 2009</h1>

    <p class="memofmt2-2">HEPATITIS C VIRUS</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19672916?ordinalpos=17&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Optimization of Thienopyrrole-Based Finger-Loop Inhibitors of the Hepatitis C Virus NS5B Polymerase.</a></p>

    <p class="NoSpacing">Martin Hernando JI, Ontoria JM, Malancona S, Attenni B, Fiore F, Bonelli F, Koch U, Di Marco S, Colarusso S, Ponzi S, Gennari N, Vignetti SE, Rico Ferreira MD, Habermann J, Rowley M, Narjes F.</p>

    <p class="NoSpacing">ChemMedChem. 2009 Aug 11. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19672916 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19596195?ordinalpos=46&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Potent aza-peptide derived inhibitors of HCV NS3 protease.</a></p>

    <p class="NoSpacing">Venkatraman S, Wu W, Shih NY, George Njoroge F.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Aug 15;19(16):4760-3. Epub 2009 Jun 17.</p>

    <p class="NoSpacing">PMID: 19596195 [PubMed - in process]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19501043?ordinalpos=58&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Increased inhibitory ability of conjugated RNA aptamers against the HCV IRES.</a></p>

    <p class="NoSpacing">Kikuchi K, Umehara T, Nishikawa F, Fukuda K, Hasegawa T, Nishikawa S.</p>

    <p class="NoSpacing">Biochem Biophys Res Commun. 2009 Aug 14;386(1):118-23. Epub 2009 Jun 6.</p>

    <p class="NoSpacing">PMID: 19501043 [PubMed - indexed for MEDLINE]</p>

    <p class="memofmt2-2">HERPES VIRUS</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19666102?ordinalpos=21&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antiviral screening of forty-two Egyptian medicinal plants.</a></p>

    <p class="NoSpacing"> Soltan MM, Zaki AK.</p>

    <p class="NoSpacing">J Ethnopharmacol. 2009 Aug 7. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19666102 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17948170?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of HCV replicon cell growth by 2-arylbenzofuran derivatives isolated from Mori Cortex Radicis.</a></p>

    <p class="NoSpacing">Lee HY, Yum JH, Rho YK, Oh SJ, Choi HS, Chang HB, Choi DH, Leem MJ, Choi EJ, Ryu JM, Hwang SB.</p>

    <p class="NoSpacing">Planta Med. 2007 Nov;73(14):1481-5. Epub 2007 Oct 18.</p>

    <p class="NoSpacing">PMID: 17948170 [PubMed - indexed for MEDLINE]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17291045?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anti-HCV bioactivity of pseudoguaianolides from Parthenium hispitum.</a></p>

    <p class="NoSpacing">Hu JF, Patel R, Li B, Garo E, Hough GW, Goering MG, Yoo HD, O&#39;neil-Johnson M, Eldridge GR.</p>

    <p class="NoSpacing">J Nat Prod. 2007 Apr;70(4):604-7. Epub 2007 Feb 10.</p>

    <p class="NoSpacing">PMID: 17291045 [PubMed - indexed for MEDLINE]</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
