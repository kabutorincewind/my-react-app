

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-293.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cdPkCOfvktLtn8rRQlLe++Vtzvcd9jp7hdFtgpBw5vJ7r9zFVqhdLtRUp0xRd47fEe6melEBuPwNB/v208djKG60c+tEACBs+RdU4GUxJch81RnkY5U+lLBc13A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DC45DAC5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-293-MEMO</b> </p>

<p>    1.    43164   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Kinetic and Chemical Mechanism of Mycobacterium tuberculosis 1-Deoxy-d-xylulose-5-phosphate Isomeroreductase</p>

<p>           Argyrou, A and Blanchard, JS</p>

<p>           Biochemistry 2004. 43(14): 4375-84</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15065882&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15065882&amp;dopt=abstract</a> </p><br />

<p>    2.    43165   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Drug susceptibility testing of Mycobacterium tuberculosis by the broth microdilution method with 7H9 broth</p>

<p>           Coban, AY, Birinci, A, Ekinci, B, and Durupinar, B</p>

<p>           Mem Inst Oswaldo Cruz 2004. 99(1): 111-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057358&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057358&amp;dopt=abstract</a> </p><br />

<p>    3.    43166   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Preparation of 2,4,5-trisubstituted imidazoles and their use as antibacterial and/or antifungal agents</b> ((Lorus Therapeutics Inc., Can.)</p>

<p>           Huesca, Mario, Al-qawasmeh, Raed, Young, Aiping H, and Lee, Yoon</p>

<p>           PATENT: WO 2004016086 A2;  ISSUE DATE: 20040226</p>

<p>           APPLICATION: 2003; PP: 84 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    4.    43167   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Combined action of micafungin, a new echinocandin, and human phagocytes for antifungal activity against Aspergillus fumigatus</p>

<p>           Choi, JH, Brummer, E, and Stevens, DA</p>

<p>           Microbes Infect 2004. 6(4): 383-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15050966&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15050966&amp;dopt=abstract</a> </p><br />

<p>    5.    43168   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Effect of rpoB mutations conferring rifampin resistance on fitness of Mycobacterium tuberculosis</p>

<p>           Mariam, DH, Mengistu, Y, Hoffner, SE, and Andersson, DI</p>

<p>           Antimicrob Agents Chemother 2004.  48(4): 1289-94</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047531&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047531&amp;dopt=abstract</a> </p><br />

<p>    6.    43169   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis DNA gyrase: interaction with quinolones and correlation with antimycobacterial drug activity</p>

<p>           Aubry, A, Pan, XS, Fisher, LM, Jarlier, V, and Cambau, E</p>

<p>           Antimicrob Agents Chemother 2004. 48(4): 1281-1288</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047530&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047530&amp;dopt=abstract</a> </p><br />

<p>    7.    43170   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           A comparative study of the post-antifungal effect (PAFE) of amphotericin B, triazoles and echinocandins on Aspergillus fumigatus and Candida albicans</p>

<p>           Manavathu, Elias K, Ramesh, Mayur S, Baskaran, Inthumathi, Ganesan, Latha T, and Chandrasekar, Pranatharthi H</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(2): 386-389</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    8.    43171   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Isoprenoid biosynthesis as a novel target for antibacterial and antiparasitic drugs</p>

<p>           Rohmer, M, Grosdemange-Billiard, C, Seemann, M, and Tritsch, D</p>

<p>           Curr Opin Investig Drugs 2004. 5(2): 154-62</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043389&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043389&amp;dopt=abstract</a> </p><br />

<p>    9.    43172   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Ribozymes as antiviral agents</p>

<p>           Bartolome, J, Castillo, I, and Carreno, V</p>

<p>           Minerva Med 2004. 95(1): 11-24</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15041923&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15041923&amp;dopt=abstract</a> </p><br />

<p>  10.    43173   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Hepatitis C virus NS5A protein interacts with 2&#39;,5&#39;-oligoadenylate synthetase and inhibits antiviral activity of IFN in an IFN sensitivity-determining region-independent manner</p>

<p>           Taguchi, T, Nagano-Fujii, M, Akutsu, M, Kadoya, H, Ohgimoto, S, Ishido, S, and Hotta, H</p>

<p>           J Gen Virol 2004. 85(Pt 4): 959-69</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15039538&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15039538&amp;dopt=abstract</a> </p><br />

<p>  11.    43174   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Linear cationic peptides with antibacterial and/or antifungal properties</b>((Diatos, Fr.)</p>

<p>           Avrameas, Eustrate and Arranz, Valerie</p>

<p>           PATENT: FR 2841902 A1;  ISSUE DATE: 20040109</p>

<p>           APPLICATION: 2002-8565; PP: 33 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    43175   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis lipomannan induces apoptosis and interleukin-12 production in macrophages</p>

<p>           Dao, DN, Kremer, L, Guerardel, Y, Molano, A, Jacobs, WR Jr, Porcelli, SA, and Briken, V</p>

<p>           Infect Immun 2004. 72(4): 2067-74</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15039328&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15039328&amp;dopt=abstract</a> </p><br />

<p>  13.    43176   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Synthesis of some new 6-methylimidazo[2,1-b]thiazole-5-carbohydrazide derivatives and their antimicrobial activities</p>

<p>           Ur, F, Cesur, N, Birteksoz, S, and Otuk, G</p>

<p>           Arzneimittelforschung 2004. 54(2): 125-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038463&amp;dopt=abstract</a> </p><br />

<p>  14.    43177   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Structure and biological functions of fungal cerebrosides</p>

<p>           Barreto-Bergter, Eliana, Pinto, Marcia R, and Rodrigues, Marcio L</p>

<p>           Anais da Academia Brasileira de Ciencias 2004. 76(1): 67-84</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    43178   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Preparation of azolylethanol antifungals and their intermediates</b>((Eisai Co., Ltd. Japan)</p>

<p>           Naito, Toshihiko, Hata, Katsura, Nagasaka, Yumiko, Tsuruoka, Akihiko, Tsukada, Itaru, Yanagisawa, Manabu, Toyosawa, Toshio, and Nara, Kazumasa</p>

<p>           PATENT: EP 1394162 A1;  ISSUE DATE: 20040303</p>

<p>           APPLICATION: 95; PP: 149 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    43179   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Preparation of azolylethanol antifungals and protected hydroxypropionate active ester intermediates</b> ((Eisai Co., Ltd. Japan)</p>

<p>           Naito, Toshihiko, Hata, Katsura, Nagasaka, Yumiko, Tsuruoka, Akihiko, Tsukada, Itaru, Yanagisawa, Manabu, Toyosawa, Toshio, and Nara, Kazumasa</p>

<p>           PATENT: EP 1394142 A1;  ISSUE DATE: 20040303</p>

<p>           APPLICATION: 95; PP: 138 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    43180   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Inhibition of hepatitis B virus replication by APOBEC3G</p>

<p>           Turelli, P, Mangeat, B, Jost, S, Vianin, S, and Trono, D</p>

<p>           Science 2004. 303(5665): 1829</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15031497&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15031497&amp;dopt=abstract</a> </p><br />

<p>  18.    43181   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           In vitro antifungal activity of miconazole studies alone and in combination against clinical isolates of yeasts</p>

<p>           Sunada, Atsuko, Nishi, Isao, Toyokawa, Masahiro, Horikawa, Masayuki, Ueda, Akiko, and Asari, Seishi</p>

<p>           Nippon Kagaku Ryoho Gakkai Zasshi 2004. 52(1): 23-30</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  19.    43182   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Synthesis and biological activity of 4-oxothiazolidines and their 5-arylidenes</p>

<p>           Srivastava, SK, Yadav, R, and Srivastava, SD</p>

<p>           Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry 2004. 43B(2): 399-405</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  20.    43183   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Synthesis and antimicrobial activity of some halo derivatives of thiophene</p>

<p>           Bardia, Rahul and Rao, JT</p>

<p>           Asian Journal of Chemistry 2004. 16(1): 511-512</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    43184   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Inhibitors of Civ1 kinase belonging to 6-aminoaromatic-2-cyclohexyldiamino purine series as potent anti-fungal compounds</p>

<p>           Bordon-Pallier, F, Jullian, N, Ferrari, P, Girard, AM, Bocquel, MT, Biton, J, Bouquin, N, and Haesslein, JL</p>

<p>           Biochim Biophys Acta 2004. 1697(1-2): 211-23</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15023362&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15023362&amp;dopt=abstract</a> </p><br />

<p>  22.    43185   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Evaluation of novel bisbenzamidines as potential drug candidates for Pneumocystis carinii pneumonia</p>

<p>           Cushion, Melanie T, Walzer, Peter D, Vanden Eynde, Jean J, Mayence, Annie, and Huang, Tien L</p>

<p>           CONFERENCE: Abstracts of Papers, 227th ACS National Meeting, Anaheim, CA, United States, March 28-April 1, 2004 2004: MEDI-068</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    43186   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Organic synthesis of a multisubstrate analogue inhibitor of the bifunctional dihydrofolate reductase-thymidylate synthase enzyme in Toxoplasma gondii</p>

<p>           Lawlis, Shauna M and Selassie, Cynthia R</p>

<p>           CONFERENCE: Abstracts of Papers, 227th ACS National Meeting, Anaheim, CA, United States, March 28-April 1, 2004 2004: CHED-498</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    43187   OI-LS-293; PUBMED-OI-4/7/2003</p>

<p class="memofmt1-2">           Toxoplasma gondii: the model apicomplexan</p>

<p>           Kim, K and Weiss, LM</p>

<p>           Int J Parasitol 2004. 34(3): 423-32</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15003501&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15003501&amp;dopt=abstract</a> </p><br />

<p>  25.    43188   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           Efficacy of alpha-cyclodextrin against experimental cryptosporidiosis in neonatal goats</p>

<p>           Castro-Hermida, JA, Pors, I, Otero-Espinar, F, Luzardo-Alvarez, A, Ares-Mazas, E, and Chartier, C</p>

<p>           VET PARASITOL  2004. 120(1-2): 35-41</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220128200004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220128200004</a> </p><br />

<p>  26.    43189   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Preparation of phenyl oxazolidinone derivatives as antimicrobials</b>((Ranbaxy Laboratories Limited, India)</p>

<p>           Mehta, Anita, Rudra, Sonali, Raja Rao, Ajjarapu Venkata Subrahmanya, Yadav, Ajay Singh, and Rattan, Ashok</p>

<p>           PATENT: WO 2004014392 A1;  ISSUE DATE: 20040219</p>

<p>           APPLICATION: 2002; PP: 62 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  27.    43190   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Ethylene diamines as anti tubercular drugs: compositions and methods</b>((USA))</p>

<p>           Protopopova, Marina Nikolaevna, Lee, Richard Edward, Slayden, Richard Allan, Barry, Clifton E, Bogatcheva, Elena, and Einck, Leo</p>

<p>           PATENT: US 2004019117 A1;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003-47930; PP: 158 pp., Cont.-in-part of U.S. Ser. No. 147,587.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  28.    43191   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           Gene transfer in the evolution of parasite nucleotide biosynthesis</p>

<p>           Striepen, B, Pruijssers, AJP, Huang, JL, Li, C, Gubbels, MJ, Umejiego, NN, Hedstrom, L, and Kissinger, JC</p>

<p>           P NATL ACAD SCI USA 2004. 101(9):  3154-3159</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220065300090">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220065300090</a> </p><br />

<p>  29.    43192   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           Selective inhibition of anthrax edema factor by adefovir, a drug for chronic hepatitis B virus infection</p>

<p>           Shen, YQ, Zhukovskaya, NL, Zimmer, MI, Soelaiman, S, Bergson, P, Wang, CR, Gibbs, CS, and Tang, WJ</p>

<p>           P NATL ACAD SCI USA 2004. 101(9):  3242-3247</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220065300105">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220065300105</a> </p><br />

<p>  30.    43193   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Glycopeptidolipid synthesis in mycobacteria</p>

<p>           Billman-Jacobe, H</p>

<p>           Current Science 2004. 86(1): 111-114</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    43194   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           Nucleoside analogues exerting antiviral activity through a non-nucleoside mechanism</p>

<p>           De Clercq, E</p>

<p>           NUCLEOS NUCLEOT NUCL 2004. 23(1-2): 457-470</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100036">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100036</a> </p><br />

<p>  32.    43195   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Preparation of quinoline derivatives and their use as mycobacterial inhibitors</b>((Janssen Pharmaceutica N.V., Belg.)</p>

<p>           Guillemont, Jerome Emile Georges, Van Gestel, Jozef Frans Elisabetha, Venet, Marc Gaston, Poignet, Herve Jean Joseph, Decrane, Laurence Francoise Bernadette, and Vernier, Daniel FJ</p>

<p>           PATENT: WO 2004011436 A1;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2003; PP: 61 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.    43196   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           Antimycobacterial naphthopyrones from Senna obliqua</p>

<p>           Graham, JG, Zhang, HJ, Pendland, SL, Santarsiero, BD, Mesecar, AD, Cabieses, F, and Farnsworth, NR</p>

<p>           J NAT PROD 2004. 67(2): 225-227</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220023300019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220023300019</a> </p><br />

<p>  34.    43197   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           New steroidal dimers with antifungal and antiproliferative activity</p>

<p>           Salunke, DB, Hazra, BG, Pore, VS, Bhat, MK, Nahar, PB, and Deshpande, MV</p>

<p>           J MED CHEM 2004. 47(6): 1591-1594</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220038700027">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220038700027</a> </p><br />

<p>  35.    43198   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           2,4-dinitroimidazole: Microwave assisted synthesis and use in synthesis of 2,3-dihydro-6-nitroimidazo[2,1-b]oxazole analogues with antimycobacterial activity</p>

<p>           Bhaumik, K and Akamanchi, KG</p>

<p>           J HETEROCYCLIC CHEM 2004. 41(1): 51-55</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220077500008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220077500008</a> </p><br />

<p>  36.    43199   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Glycopeptide antibiotic derivatives, their preparation and their use as antiviral agents</b>((K.U. Leuven Research and Development, Belg.)</p>

<p>           Balzarini, Jan, Preobrazhenskaya, Maria, and De Clercq, Erik</p>

<p>           PATENT: WO 2004019970 A2;  ISSUE DATE: 20040311</p>

<p>           APPLICATION: 2003; PP: 91 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  37.    43200   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Role of indoleamine-2,3-dioxygenase in alpha/beta and gamma interferon-mediated antiviral effects against herpes simplex virus infections</p>

<p>           Adams, O, Besken, K, Oberdoerfer, C, MacKenzie, CR, Takikawa, O, and Daeubener, W</p>

<p>           Journal of Virology 2004. 78(5): 2632-2636</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  38.    43201   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Surveillance network for herpes simplex virus resistance to antiviral drugs: 3-year follow-up</p>

<p>           Danve-Szatanek, C, Aymard, M, Thouvenot, D, Morfin, F, Agius, G, Bertin, I, Billaudel, S, Chanzy, B, Coste-Burel, M, Finkielsztejn, L, Fleury, H, Hadou, T, Henquell, C, Lafeuille, H, Lafon, ME, Le Faou, A, Legrand, MC, Maille, L, Mengelle, C, Morand, P, Morinet, F, Nicand, E, Omar, S, Picard, B, Pozzetto, B, Puel, J, Raoult, D, Scieux, C, Segondy, M, Seigneurin, JM, Teyssou, R, and Zandotti, C</p>

<p>           Journal of Clinical Microbiology 2004. 42(1): 242-249</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  39.    43202   OI-LS-293; WOS-OI-3/28/2004</p>

<p class="memofmt1-2">           Serpin mechanism of hepatitis C virus nonstructural 3 (NS3) protease inhibition - Induced fit as a mechanism for narrow specificity</p>

<p>           Richer, MJ, Juliano, L, Hashimoto, C, and Jean, F</p>

<p>           J BIOL CHEM 2004. 279(11): 10222-10227</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220050400069">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220050400069</a> </p><br />

<p>  40.    43203   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           Design, synthesis and evaluation of novel indole-guanidino compounds as Potent Antifungal Agents</p>

<p>           Shi, Dong-Fang, Botyanszki, Janos, Roberts, Chris, Gezginci, Mikail, Sattarzadeh, Sherwin, Valdez, Corey, Kongpachith, Ana, Hancock, Christina, Fung, Kevin, Vigil, Mike, Pham, Konnie, Singh, Gaurav, Imai, Jackie, Clemons, Karl, Stevens, David, Schmitz, Uli, Velligan, Mark, Lou, Lillian, and Griffith, Ronald</p>

<p>           CONFERENCE: Abstracts of Papers, 227th ACS National Meeting, Anaheim, CA, United States, March 28-April 1, 2004 2004: MEDI-084</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  41.    43204   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p class="memofmt1-2">           In vitro evaluation of double and triple combinations of antifungal drugs against Aspergillus fumigatus and Aspergillus terreus</p>

<p>           Dannaoui, Eric, Lortholary, Olivier, and Dromer, Francoise</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(3): 970-978</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  42.    43205   OI-LS-293; SCIFINDER-OI-3/30/2004</p>

<p><b>           Preparation of azole derivatives as antifungal agents</b>((Ranbaxy Laboratories Limited, India)</p>

<p>           Salman, Mohammad, Sattigeri, Jitendra Anant, Katoch, Rita, Sethi, Sachin, and Rattan, Ashok</p>

<p>           PATENT: WO 2004018485 A1;  ISSUE DATE: 20040304</p>

<p>           APPLICATION: 2002; PP: 72 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  43.    43206   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Antimicrobial properties of the frog skin peptide, ranatuerin-1 and its [Lys-8]-substituted analog</p>

<p>           Sonnevend, A, Knoop, FC, Patel, M, Pal, T, Soto, AM, and Conlon, JM</p>

<p>           PEPTIDES 2004. 25(1): 29-36</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220267900005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220267900005</a> </p><br />

<p>  44.    43207   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Merging the potential of microbial genetics with biological and chemical diversity: an even brighter future for marine natural product drug discovery</p>

<p>           Salomon, CE, Magarvey, NA, and Sherman, DH</p>

<p>           NAT PROD REP 2004. 21(1): 105-121</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220253700007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220253700007</a> </p><br />

<p>  45.    43208   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Antimicrobial susceptibility determined by the E test, Lowenstein-Jensen proportion, and DNA Sequencing methods among Mycobacterium tuberculosis isolates - Discrepancies, preliminary results</p>

<p>           Freixo, MIM, Caldas, PCS, Said, A, Martins, F, Brito, RC, Fonseca, LD, and Saad, MHF</p>

<p>           MEM I OSWALDO CRUZ 2004. 99(1): 107-110</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220161500019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220161500019</a> </p><br />

<p>  46.    43209   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Use of dapsone in the treatment of Pneumocystis carinii pneumonia in a foal</p>

<p>           Clark-Price, SC, Cox, JH, Bartoe, JT, and Davis, EG</p>

<p>           JAVMA-J AM VET MED A 2004. 224(3):  407-+</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220139400024">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220139400024</a> </p><br />

<p>  47.    43210   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Prodrugs as therapeutics</p>

<p>           Stella, VJ</p>

<p>           EXPERT OPIN THER PAT 2004. 14(3): 277-280</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220193600001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220193600001</a> </p><br />

<p>  48.    43211   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Essential eukaryotic core</p>

<p>           Strobel, GL and Arnold, J</p>

<p>           EVOLUTION 2004. 58(2): 441-446</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220194200022">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220194200022</a> </p><br />

<p>  49.    43212   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Antimycobacterial compounds. New pyrrole derivatives of BM212</p>

<p>           Biava, M, Porretta, GC, Deidda, D, Pompei, R, Tafi, A, and Manetti, F</p>

<p>           BIOORGAN MED CHEM 2004. 12(6): 1453-1458</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220237800020">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220237800020</a> </p><br />

<p>  50.    43213   OI-LS-293; WOS-OI-4/4/2004</p>

<p class="memofmt1-2">           Biosynthesis of mycobacterial phosphatidylinositol mannosides</p>

<p>           Morita, YS, Patterson, JH, Billman-Jacobe, H, and McConville, MJ</p>

<p>           BIOCHEM J 2004. 378: 589-597</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220189700031">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220189700031</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
