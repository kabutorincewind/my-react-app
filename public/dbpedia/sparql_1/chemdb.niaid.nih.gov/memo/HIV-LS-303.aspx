

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-303.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="41FrUdz0eqfGvl6zYo3nVkGquFzGk/TdeeIulm+snKlZVJ2dDvVVimxea58qZKvuxJv4cmHr8IIPbDPrUX+gNhgyDWGHBr46RO2580ovJnwmYIEJ9ZsvhAcf3LQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ED66FB53" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-303-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>     1.    43914   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p class="memofmt1-2">           Synthesis of certain 6-(arylthio)uracils and related derivatives as potential antiviral agents</p>

    <p>           El-Emam, Ali A, Massoud, Mohamed AM, El-Bendary, Eman R, and El-Sayed, Magda A</p>

    <p>           Bulletin of the Korean Chemical Society 2004. 25(7): 991-996</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>     2.    43915   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Resistance to HIV Protease Inhibitors: Mechanisms and Clinical Consequences</p>

    <p>           De Mendoza, C and Soriano, V</p>

    <p>           Curr Drug Metab 2004. 5(4): 321-328</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320704&amp;dopt=abstract</a> </p><br />

    <p>     3.    43916   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Structural Determinants and Molecular Mechanisms for the Resistance of HIV-1 RT to Nucleoside Analogues</p>

    <p>           Deval, J, Courcambeck, J, Selmi, B, Boretto, J, and Canard, B</p>

    <p>           Curr Drug Metab 2004. 5(4): 305-16</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320702&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320702&amp;dopt=abstract</a> </p><br />

    <p>     4.    43917   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           HIV-1 Reverse Transcriptase Inhibitors: Current Issues and Future Perspectives</p>

    <p>           Locatelli, GA, Cancio, R, Spadari, S, and Maga, G</p>

    <p>           Curr Drug Metab 2004. 5(4): 283-90</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320700&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320700&amp;dopt=abstract</a> </p><br />

    <p>     5.    43918   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p class="memofmt1-2">           Antiviral Drugs: Third strike against HIV enzymes</p>

    <p>           Kirkpatrick, Peter</p>

    <p>           Nature Reviews Drug Discovery 2004.  3(8): 645</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>     6.    43919   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Statins Inhibit HIV-1 Infection by Down-regulating Rho Activity</p>

    <p>           Del Real, G,  Jimenez-Baranda, S, Mira, E, Lacalle, RA, Lucas, P, Gomez-Mouton, C, Alegret, M, Pena, JM, Rodriguez-Zapata, M, Alvarez-Mon, M, Martinez-A, C, and Manes, S</p>

    <p>           J Exp Med 2004. 200(4): 541-547</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15314078&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15314078&amp;dopt=abstract</a> </p><br />

    <p>     7.    43920   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p><b>           Preparation of tetrahydro-4H-pyrido[1,2-a]pyrimidines and related compounds as HIV integrase inhibitors</b> ((Istituto Di Ricerche Di Biologia Molecolare P. Angeletti Spa, Italy)</p>

    <p>           Crescenzi, Benedetta, Kinzel, Olaf, Muraglia, Ester, Orvieto, Federica, Pescatore, Giovanna, Rowley, Michael, and Summa, Vincenzo</p>

    <p>           PATENT: WO 2004058756 A1;  ISSUE DATE: 20040715</p>

    <p>           APPLICATION: 2003; PP: 113 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>     8.    43921   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Synthesis and application of methyleneoxy pseudodipeptide building blocks in biologically active peptides</p>

    <p>           Hlavacek, J, Marik, J, Konvalinka, J, Bennettova, B, and Tykva, R</p>

    <p>           Amino Acids 2004. 27(1): 19-27</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15309568&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15309568&amp;dopt=abstract</a> </p><br />

    <p>     9.    43922   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Inhibition of HIV-1 fusion with small interfering RNAs targeting the chemokine coreceptor CXCR4</p>

    <p>           Zhou, N, Fang, J, Mukhtar, M, Acheampong, E, and Pomerantz, RJ</p>

    <p>           Gene Ther 2004</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15306840&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15306840&amp;dopt=abstract</a> </p><br />

    <p>   10.    43923   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p><b>           Preparation of pyrrolidine and azetidine compounds as CCR5 antagonists</b> ((Smithkline Beecham Corporation, USA)</p>

    <p>           Yang, Hanbiao, Kazmierski, Wieslaw Mieczyslaw, and Aquino, Christopher Joseph</p>

    <p>           PATENT: WO 2004055016 A1;  ISSUE DATE: 20040701</p>

    <p>           APPLICATION: 2003; PP: 130 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   11.    43924   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p><b>           Preparation of indane compounds and analogs as CCR5 antagonists</b> ((Smithkline Beecham Corporation, USA)</p>

    <p>           Youngman, Michael, Kazmierski, Wieslaw Mieczyslaw, Yang, Hanbiao, and Aquino, Christopher Joseph</p>

    <p>           PATENT: WO 2004055012 A1;  ISSUE DATE: 20040701</p>

    <p>           APPLICATION: 2003; PP: 129 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   12.    43925   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           PHI-443: A Novel Noncontraceptive Broad-Spectrum Anti- Human Immunodeficiency Virus Microbicide</p>

    <p>           D&#39;Cruz, OJ, Samuel, P, and Uckun, FM</p>

    <p>           Biol Reprod 2004</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15306558&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15306558&amp;dopt=abstract</a> </p><br />

    <p>   13.    43926   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Small-Molecule Dimerization Inhibitors of Wild-Type and Mutant HIV Protease: A Focused Library Approach</p>

    <p>           Shultz, MD, Ham, YW, Lee, SG, Davis, DA, Brown, C, and Chmielewski, J</p>

    <p>           J Am Chem Soc  2004. 126(32): 9886-7</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15303839&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15303839&amp;dopt=abstract</a> </p><br />

    <p>        </p>
    <br clear="all">

    <p>  14.     43927   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Inhibition of human immunodeficiency virus type 1 replication by Z-100, an immunomodulator extracted from human-type tubercle bacilli, in macrophages</p>

    <p>           Emori, Y, Ikeda, T, Ohashi, T, Masuda, T, Kurimoto, T, Takei, M, and Kannagi, M</p>

    <p>           J Gen Virol 2004. 85(Pt 9): 2603-13</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302954&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302954&amp;dopt=abstract</a> </p><br />

    <p>   15.    43928   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           l-Chicoric acid inhibits human immunodeficiency virus type 1 integration in vivo and is a noncompetitive but reversible inhibitor of HIV-1 integrase in vitro</p>

    <p>           Reinke, RA, Lee, DJ, McDougall, BR, King, PJ, Victoria, J, Mao, Y, Lei, X, Reinecke, MG, and Robinson, WE Jr</p>

    <p>           Virology 2004. 326(2): 203-19</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302207&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302207&amp;dopt=abstract</a> </p><br />

    <p>   16.    43929   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           2-(2,6-Dihalophenyl)-3-(pyrimidin-2-yl)-1,3-thiazolidin-4-ones as non-nucleoside HIV-1 reverse transcriptase inhibitors</p>

    <p>           Rao, A, Balzarini, J, Carbone, A, Chimirri, A, De, Clercq E, Monforte, AM, Monforte, P, Pannecouque, C, and Zappala, M</p>

    <p>           Antiviral Res  2004. 63(2): 79-84</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302136&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302136&amp;dopt=abstract</a> </p><br />

    <p>   17.    43930   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Protecting from R5-tropic HIV: individual and combined effectiveness of a hammerhead ribozyme and a single-chain Fv antibody that targets CCR5</p>

    <p>           Cordelier, P, Kulkowsky, JW, Ko, C, Matskevitch, AA, McKee, HJ, Rossi, JJ, Bouhamdan, M, Pomerantz, RJ, Kari, G, and Strayer, DS</p>

    <p>           Gene Ther 2004</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15295615&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15295615&amp;dopt=abstract</a> </p><br />

    <p>   18.    43931   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p><b>           Preparation of 5,11-dihydro-8-(2-hydroxyethyl)-6H-dipyrido[3,2-b:2&#39;,3&#39;-e][1,4]diazepin-6-one derivatives as non-nucleoside reverse transcriptase inhibitors</b> ((Boehringer Ingelheim (Canada) Ltd., Can.)</p>

    <p>           Yoakim, Christiane, Malenfant, Eric, Thavonekham, Bounkham, Ogilvie, William, and Deziel, Robert</p>

    <p>           PATENT: US 2004106791 A1;  ISSUE DATE: 20040603</p>

    <p>           APPLICATION: 2003-7496; PP: 27 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   19.    43932   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Lamellarins, from A to Z: a family of anticancer marine pyrrole alkaloids</p>

    <p>           Bailly, C</p>

    <p>           Curr Med Chem Anti-Canc Agents 2004.  4(4): 363-78</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15281908&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15281908&amp;dopt=abstract</a> </p><br />

    <p>   20.    43933   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p><b>           Quinolones with anti-HIV activity, and preparation thereof</b> ((IRM LLC, Bermuda)</p>

    <p>           He, Yun, Ellis, David Archer, Anaclerio, Beth Marie, Kuhen, Kelli L, Wu, Baogen, and Jiang, Tao</p>

    <p>           PATENT: WO 2004037853 A2;  ISSUE DATE: 20040506</p>

    <p>           APPLICATION: 2003; PP: 71 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   21.    43934   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p class="memofmt1-2">           Structures of HIV-1 RT-DNA complexes before and after incorporation of the anti-AIDS drug tenofovir</p>

    <p>           Tuske, Steve, Sarafianos, Stefan G, Clark, Arthur D, Ding, Jianping, Naeger, Lisa K, White, Kirsten L, Miller, Michael D, Gibbs, Craig S, Boyer, Paul L, Clark, Patrick, Wang, Gang, Gaffney, Barbara L, Jones, Roger A, Jerina, Donald M, Hughes, Stephen H, and Arnold, Eddy</p>

    <p>           Nature Structural &amp; Molecular Biology 2004. 11(5): 469-474</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   22.    43935   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p><b>           Use of soluble monovalent oligosaccharides as inhibitors of HIV-1 fusion and replication</b>((USA))</p>

    <p>           Leach, James L, Garber, Stacey Ann, Prieto, Pedro A, Alarcon, Balbino, Rueda, Ricardo, and Vazquez, Enrique</p>

    <p>           PATENT: US 2004077590 A1;  ISSUE DATE: 20040422</p>

    <p>           APPLICATION: 2002-15115; PP: 13 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   23.    43936   HIV-LS-303; PUBMED-HIV-8/24/2004</p>

    <p class="memofmt1-2">           Drug Targeting of HIV-1 RNA.DNA Hybrid Structures: Thermodynamics of Recognition and Impact on Reverse Transcriptase-Mediated Ribonuclease H Activity and Viral Replication</p>

    <p>           Li, TK, Barbieri, CM, Lin, HC, Rabson, AB, Yang, G, Fan, Y, Gaffney, BL, Jones, RA, and Pilch, DS</p>

    <p>           Biochemistry 2004. 43(30): 9732-9742</p>

    <p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15274628&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15274628&amp;dopt=abstract</a> </p><br />

    <p>   24.    43937   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p class="memofmt1-2">           Short-term safety and antiretroviral activity of T-1249, a second-generation fusion inhibitor of HIV</p>

    <p>           Eron, Joseph J, Gulick, Roy M, Bartlett, John A, Merigan, Thomas, Arduino, Roberto, Kilby, JMichael, Yangco, Bienvenido, Diers, Adriann, Drobnes, Claude, DeMasi, Ralph, Greenberg, Michael, Melby, Thomas, Raskino, Claire, Rusnak, Pam, Zhang, Ying, Spence, Rebecca, and Miralles, GDiego</p>

    <p>           Journal of Infectious Diseases 2004.  189(6): 1075-1083</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   25.    43938   HIV-LS-303; SCIFINDER-HIV-8/17/2004</p>

    <p><b>           Antiviral random sequence oligonucleotides lacking complementarity to target genomes and their therapeutic uses</b> ((Replicor, Inc. Can.)</p>

    <p>           Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>           PATENT: WO 2004024919 A1;  ISSUE DATE: 20040325</p>

    <p>           APPLICATION: 2003; PP: 161 pp.</p>

    <p>&nbsp;           <!--HYPERLNK:--> </p>

    <p>   26.    43939   HIV-LS-303; WOS-HIV-8/15/2004</p>

    <p class="memofmt1-2">           The HIV protease inhibitor ritonavir blocks osteoclastogenesis and function by impairing RANKL-induced signaling</p>

    <p>           Wang, MWH, Wei, S, Faccio, R, Takeshita, S, Tebas, P, Powderly, WG, Teitelbaum, SL, and Ross, FP</p>

    <p>           J CLIN INVEST  2004. 114(2): 206-213</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222667600011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222667600011</a> </p><br />

    <p>   27.    43940   HIV-LS-303; WOS-HIV-8/15/2004</p>

    <p class="memofmt1-2">           Exploring binding mode for styrylquinoline HIV-1 integrase inhibitors using comparative molecular field analysis and docking studies</p>

    <p>           Ma, XH, Zhang, XY, Tan, JJ, Chen, WZ, and Wang, CX</p>

    <p>           ACTA PHARMACOL SIN 2004. 25(7): 950-958</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222574700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222574700016</a> </p><br />

    <p>   28.    43941   HIV-LS-303; WOS-HIV-8/23/2004</p>

    <p class="memofmt1-2">           Improved preparation and structural investigation of 4-aryl-4-oxo-2-hydroxy-2-butenoic acids and methyl esters</p>

    <p>           Maurin, C, Bailly, F, and Cotelle, P</p>

    <p>           TETRAHEDRON 2004. 60(31): 6479-6486</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863600007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863600007</a> </p><br />

    <p>   29.    43942   HIV-LS-303; WOS-HIV-8/23/2004</p>

    <p class="memofmt1-2">           In-water reactivity of nucleosides and nucleotides: one-step preparation and biological evaluation of novel ferrocenyl-derivatives</p>

    <p>           de Champdore, M, Di Fabio, G, Messere, A, Montesarchio, D, Piccialli, G, Loddo, R, La Colla, M, and La Colla, P</p>

    <p>           TETRAHEDRON 2004. 60(31): 6555-6563</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863600015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863600015</a> </p><br />

    <p>   30.    43943   HIV-LS-303; WOS-HIV-8/23/2004</p>

    <p class="memofmt1-2">           Synthesis of peptidomimetics based on iminosugar and beta-D-glucopyranoside scaffolds and inhibiton of HIV-protease</p>

    <p>           Chery, F, Cronin, L, O&#39;Brien, JL, and Murphy, PV</p>

    <p>           TETRAHEDRON 2004. 60(31): 6597-6608</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863600021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222863600021</a> </p><br />

    <p>   31.    43944   HIV-LS-303; WOS-HIV-8/23/2004</p>

    <p class="memofmt1-2">           Actinchinin, a novel antifungal protein from the gold kiwi fruit</p>

    <p>           Xia, LX and Ng, TB</p>

    <p>           PEPTIDES 2004. 25(7): 1093-1098</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222855100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222855100004</a> </p><br />

    <p>   32.    43945   HIV-LS-303; WOS-HIV-8/23/2004</p>

    <p class="memofmt1-2">           Expression of HIV-1 accessory protein Vif is controlled uniquely to be low and optimal by proteasome degradation</p>

    <p>           Fujita, M, Akari, H, Sakurai, A, Yoshida, A, Chiba, T, Tanaka, K, Strebel, K, and Adachi, A</p>

    <p>           MICROBES INFECT 2004. 6(9): 791-798</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222860200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222860200001</a> </p><br />

    <p>   33.    43946   HIV-LS-303; WOS-HIV-8/23/2004</p>

    <p class="memofmt1-2">           Lysine sulfonamides as novel HIV-protease inhibitors: N epsilon-disubstituted ureas</p>

    <p>           Stranix, BR, Sauve, G, Bouzide, A, Cote, A, Sevigny, G, Yelle, J, and Perron, V</p>

    <p>           BIOORG MED CHEM LETT 2004. 14(15):  3971-3974</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222792800025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222792800025</a> </p><br />

    <p>   34.    43947   HIV-LS-303; WOS-HIV-8/23/2004</p>

    <p class="memofmt1-2">           Rigid backbone moiety of KNI-272, a highly selective HIV protease inhibitor: methanol, acetone and dimethylsulfoxide solvated forms of 3-[3-benzyl-2-hydroxy-9-(isoquinoli -5-yloxy)-6-methyl-sulfanylmethyl-5 8-dioxo-4,7-diazanonanoyl]-N-tert-b tyl-1,3-thiazolidine-4-carboxamide</p>

    <p>           Doi, M, Kimura, T, Ishida, T, and Kiso, Y</p>

    <p>           ACTA CRYSTALLOGR B 2004. 60: 433-437</p>

    <p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222725100009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222725100009</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
