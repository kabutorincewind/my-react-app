

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-344.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="B6Hvun8H2kC8svhbYnp+/ix9Y9+Z4XnXgpwx1HYc9I+YIbM5YCP2UJpXpeC+e1fNViAyHHzEBMUHOvFzkkOh5gGdi2wARH3xGvBPtx3fIB0TgG5EkKO//IHRB3U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="802F02F6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-344 MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48623   OI-LS-344; PUBMED-OI-3/20/2006</p>

    <p class="memofmt1-2">          A Mechanism of Virulence: Virulent Mycobacterium tuberculosis Strain H37Rv, but Not Attenuated H37Ra, Causes Significant Mitochondrial Inner Membrane Disruption in Macrophages Leading to Necrosis</p>

    <p>          Chen, M, Gan, H, and Remold, HG</p>

    <p>          J Immunol <b>2006</b>.  176(6): 3707-3716</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16517739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16517739&amp;dopt=abstract</a> </p><br />

    <p>2.     48624   OI-LS-344; PUBMED-OI-3/20/2006</p>

    <p class="memofmt1-2">          Preclinical profile of VX-950, a potent, selective, and orally bioavailable inhibitor of hepatitis C virus NS3-4A serine protease</p>

    <p>          Perni, RB, Almquist, SJ, Byrn, RA, Chandorkar, G, Chaturvedi, PR, Courtney, LF, Decker, CJ, Dinehart, K, Gates, CA, Harbeson, SL, Heiser, A, Kalkeri, G, Kolaczkowski, E, Lin, K, Luong, YP, Rao, BG, Taylor, WP, Thomson, JA, Tung, RD, Wei, Y, Kwong, AD, and Lin, C</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(3): 899-909</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495249&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16495249&amp;dopt=abstract</a> </p><br />

    <p>3.     48625   OI-LS-344; PUBMED-OI-3/20/2006</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Alkoxyalkyl Derivatives of 9-(S)-(3-Hydroxy-2-phosphonomethoxypropyl)adenine against Cytomegalovirus and Orthopoxviruses</p>

    <p>          Beadle, JR, Wan, WB, Ciesla, SL, Keith, KA, Hartline, C, Kern, ER, and Hostetler, KY</p>

    <p>          J Med Chem <b>2006</b>.  49(6): 2010-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539388&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16539388&amp;dopt=abstract</a> </p><br />

    <p>4.     48626   OI-LS-344; EMBASE-OI-3/20/2006</p>

    <p class="memofmt1-2">          Structure-activity relationship for nucleoside analogs as inhibitors and substrates of adenosine kinase from Mycobacterium tuberculosis. I. Modifications to the adenine moiety</p>

    <p>          Long, Mary C and Parker, William B</p>

    <p>          Biochemical Pharmacology <b>2006</b>.  In Press, Accepted Manuscript</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4P-4JFH6SW-3/2/822219f9a7c508206209c160b5286c06">http://www.sciencedirect.com/science/article/B6T4P-4JFH6SW-3/2/822219f9a7c508206209c160b5286c06</a> </p><br />

    <p>5.     48627   OI-LS-344; EMBASE-OI-3/20/2006</p>

    <p class="memofmt1-2">          Antiviral and antituberculous activity of Helichrysum melanacme constituents</p>

    <p>          Lall, N, Hussein, AA, and Meyer, JJM</p>

    <p>          Fitoterapia <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VSC-4JF97B2-1/2/de53525aafbb02316a052d4fbf3f89f9">http://www.sciencedirect.com/science/article/B6VSC-4JF97B2-1/2/de53525aafbb02316a052d4fbf3f89f9</a> </p><br />
    <br clear="all">

    <p>6.     48628   OI-LS-344; PUBMED-OI-3/20/2006</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial, and anti-inflammatory activities of novel 2-[3-(1-adamantyl)-4-substituted-5-thioxo-1,2,4-triazolin-1-yl] acetic acids, 2-[3-(1-adamantyl)-4-substituted-5-thioxo-1,2,4-triazolin-1-yl]propionic acids and related derivatives</p>

    <p>          Al-Deeb, OA, Al-Omar, MA, El-Brollosy, NR, Habib, EE, Ibrahim, TM, and El-Emam, AA</p>

    <p>          Arzneimittelforschung <b>2006</b>.  56(1): 40-47</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16478004&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16478004&amp;dopt=abstract</a> </p><br />

    <p>7.     48629   OI-LS-344; PUBMED-OI-3/20/2006</p>

    <p class="memofmt1-2">          Present status and future prospects of chemotherapeutics for intractable infections due to Mycobacterium avium complex</p>

    <p>          Tomioka, H</p>

    <p>          Curr Drug Discov Technol <b>2004</b>.  1(4): 255-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16472242&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16472242&amp;dopt=abstract</a> </p><br />

    <p>8.     48630   OI-LS-344; EMBASE-OI-3/20/2006</p>

    <p class="memofmt1-2">          Pyranocoumarin, a novel anti-TB pharmacophore: Synthesis and biological evaluation against Mycobacterium tuberculosis</p>

    <p>          Xu, Ze-Qi, Pupek, Krzysztof, Suling, William J, Enache, Livia, and Flavin, Michael T</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4JCSK0M-2/2/86fbdefe94dcdb1e7253fea5416b270c">http://www.sciencedirect.com/science/article/B6TF8-4JCSK0M-2/2/86fbdefe94dcdb1e7253fea5416b270c</a> </p><br />

    <p>9.     48631   OI-LS-344; PUBMED-OI-3/20/2006</p>

    <p class="memofmt1-2">          Influenza pandemics: past, present and future</p>

    <p>          Hsieh, YC, Wu, TZ, Liu, DP, Shao, PL, Chang, LY, Lu, CY, Lee, CY, Huang, FY, and Huang, LM</p>

    <p>          J Formos Med Assoc <b>2006</b>.  105(1): 1-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16440064&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16440064&amp;dopt=abstract</a> </p><br />

    <p>10.   48632   OI-LS-344; PUBMED-OI-3/20/2006</p>

    <p class="memofmt1-2">          Viral hepatitis treatment and beyond: a review of CROI 2005</p>

    <p>          Wilson, L and Gebo, K</p>

    <p>          Hopkins HIV Rep <b>2005</b>.  17(3): 10-1, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16419312&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16419312&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48633   OI-LS-344; EMBASE-OI-3/20/2006</p>

    <p class="memofmt1-2">          Ethnopharmacological evaluation of the informant consensus model on anti-tuberculosis claims among the Manus</p>

    <p>          Case, Ryan J, Franzblau, Scott G, Wang, Yuehong, Cho, Sang Hyun, Soejarto, DDoel, and Pauli, Guido F</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4J2TSK8-2/2/fbbf2c6729ee7280bf41ea0001823d0a">http://www.sciencedirect.com/science/article/B6T8D-4J2TSK8-2/2/fbbf2c6729ee7280bf41ea0001823d0a</a> </p><br />

    <p>12.   48634   OI-LS-344; EMBASE-OI-3/20/2006</p>

    <p class="memofmt1-2">          Novel Inhibitors of Hepatitis C Virus RNA-dependent RNA Polymerases</p>

    <p>          Lee, Gary, Piper, Derek E, Wang, Zhulun, Anzola, John, Powers, Jay, Walker, Nigel, and Li, Yang</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  357(4): 1051-1057</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4J554RY-2/2/affee7a6de984abb455f78ee2f1d17d6">http://www.sciencedirect.com/science/article/B6WK7-4J554RY-2/2/affee7a6de984abb455f78ee2f1d17d6</a> </p><br />

    <p>13.   48635   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of some 4-or 6-chloroisatin derivatives</p>

    <p>          Pandeya, SN, Raja, AS, and Nath, G</p>

    <p>          INDIAN JOURNAL OF CHEMISTRY SECTION B-ORGANIC CHEMISTRY INCLUDING MEDICINAL CHEMISTRY <b>2006</b>.  45 (2): 494-499, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235555800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235555800011</a> </p><br />

    <p>14.   48636   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          The robe of antiviral agents in the management of viral hepatitis</p>

    <p>          Mutimer, D</p>

    <p>          CLINICAL MEDICINE <b>2006</b>.  6(1): 29-34, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235642500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235642500008</a> </p><br />

    <p>15.   48637   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Schiff base conjugates of p-aminosalicylic acid as antimycobacterial agents</p>

    <p>          Patole, J, Shingnapurkar, D, Padhye, S, and Ratledge, C</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(6): 1514-1517, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235492800012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235492800012</a> </p><br />

    <p>16.   48638   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Depeptidization efforts on P-3-P &#39;(2) alpha-ketoamide inhibitors of HCVNS3-4A serine protease: Effect on HCV replicon activity</p>

    <p>          Bogen, SL, Ruan, S, Liu, R, Agrawal, S, Pichardo, J, Prongay, A, Baroudy, B, Saksena, AK, Girijavallabhan, V, and Njoroge, FG</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(6): 1621-1627, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235492800034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235492800034</a> </p><br />
    <br clear="all">

    <p>17.   48639   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Crystal structure of the pantothenate synthetase from Mycobacterium tuberculosis snapshots of the enzyme in action</p>

    <p>          Wang, SS and Eisenberg, D</p>

    <p>          BIOCHEMISTRY <b>2006</b>.  45(6): 1554-1561, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235631400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235631400003</a> </p><br />

    <p>18.   48640   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Genomics applied to drug candidate prioritization and for identification of novel therapeutically useful properties of existing drugs</p>

    <p>          Jarnagin, K, Roter, A, Tolley, A, Lee, M, and Natsoulis, CG</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U156-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600269">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600269</a> </p><br />

    <p>19.   48641   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Development of new tuberculosis drug candidates</p>

    <p>          Laughon, BE</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U164-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600310">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600310</a> </p><br />

    <p>20.   48642   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Novel inhibitors of InhA, the enoyl reductase from mycobacterium tuberculosis</p>

    <p>          Tonge, PJ, Sullivan, TJ, Novichenok, P, Truglio, JJ, Kisker, C, Johnson, F, and Slayden, RA</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U164-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600314">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600314</a> </p><br />

    <p>21.   48643   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Cell wall of mycobacterium tuberculosis and drug discovery</p>

    <p>          Brennan, PJ</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U164-U165, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600315">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600315</a> </p><br />

    <p>22.   48644   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Synthesis and design of 2,4-diamino-6-substituted-pyrido[3,2-D]pyrimidines as dihydrofolate reductase inhibitors from opportunistic pathogens</p>

    <p>          Gangjee, A, Ye, ZQ, Kisliuk, RL, and Queener, SF</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U172-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600352">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600352</a> </p><br />

    <p>23.   48645   OI-LS-344; WOS-OI-3/12/2006</p>

    <p class="memofmt1-2">          Developing novel inhibitors of the enoyl reductase from mycobacterium tuberculosis (INHA) : SAR studies of triclosan congeners</p>

    <p>          Sullivan, TJ, Novichenok, P, Truglio, JJ, Kisker, C, Johnson, F, Slayden, RA, and Tonge, PJ</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U176-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600376">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600376</a> </p><br />
    <br clear="all">

    <p>24.   48646   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          Insights into mechanisms of induction and ligands recognition in the transcriptional repressor EthR from Mycobacterium tuberculosis</p>

    <p>          Frenois, F, Baulard, AR, and Villeret, V</p>

    <p>          TUBERCULOSIS <b>2006</b>.  86(2): 110-114, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235740800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235740800002</a> </p><br />

    <p>25.   48647   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          Development of a toxicogenomics in vitro assay for the efficient characterization of compounds</p>

    <p>          Yang, Y, Abel, SJ, Ciurlionis, R, and Waring, JF</p>

    <p>          PHARMACOGENOMICS <b>2006</b>.  7(2): 177-186, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235752500010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235752500010</a> </p><br />

    <p>26.   48648   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          Academia given a helping hand in drug development</p>

    <p>          Branca, MA</p>

    <p>          NATURE REVIEWS DRUG DISCOVERY <b>2006</b>.  5(3): 177-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235836300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235836300003</a> </p><br />

    <p>27.   48649   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          Computer-assisted analysis of the interactions of macrocyclic inhibitors with wild type and mutant D168A hepatitis C virus NS3 serine protease</p>

    <p>          da Cunha, EFF, Ramalho, TC, Taft, CA, and de Alencastro, RB</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2006</b>.  3(1): 17-28, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235724000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235724000004</a> </p><br />

    <p>28.   48650   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          On the horizon: Promising investigational antiretroviral agents</p>

    <p>          McNicholl, IR and McNicholl, JJ</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2006</b>.  12(9): 1091-1103, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235758400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235758400007</a> </p><br />

    <p>29.   48651   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          New methods for medicinal chemistry - Universal gene cloning and expression systems for production of marine bioactive metabolites</p>

    <p>          Dunlap, WC, Jaspars, M, Hranueli, D, Battershill, CN, Peric-Concha, N, Zucko, J, Wright, SH, and Long, PF</p>

    <p>          CURRENT MEDICINAL CHEMISTRY <b>2006</b>.  13(6): 697-710, 14</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235756900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235756900008</a> </p><br />

    <p>30.   48652   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          Effectiveness of fluoroquinolones against Mycobacterium abscessus in vivo</p>

    <p>          Caballero, AR, Marquart, ME, O&#39;Callaghan, RJ, Thibodeaux, BA, Johnston, KH, and Dajcs, JJ</p>

    <p>          CURRENT EYE RESEARCH <b>2006</b>.  31(1): 23-29, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234819700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234819700003</a> </p><br />
    <br clear="all">

    <p>31.   48653   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          rmlB and rmlC genes are essential fair growth with mycobacteria</p>

    <p>          Li, W, Xin, Y, McNeil, MR, and Ma, YF</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2006</b>.  342(1): 170-178, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235793800023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235793800023</a> </p><br />

    <p>32.   48654   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          SCH 503034, a mechanism-based inhibitor of hepatitis C virus NS3 protease, suppresses polyprotein maturation and enhances the antiviral activity of alpha interferon in replicon cells</p>

    <p>          Malcolm, BA, Liu, R, Lahser, F, Agrawal, S, Belanger, B, Butkiewicz, N, Chase, R, Gheyas, F, Hart, A, Hesk, D, Ingravallo, P, Jiang, C, Kong, R, Lu, J, Pichardo, J, Prongay, A, Skelton, A, Tong, X, Venkatraman, S, Xia, E, Girijavallabhan, V, and Njoroge, FG</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(3): 1013-1020, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300026</a> </p><br />

    <p>33.   48655   OI-LS-344; WOS-OI-3/19/2006</p>

    <p class="memofmt1-2">          Molecular characterization of isoniazid resistance in Mycobacterium tuberculosis: Identification of a novel mutation in inhA</p>

    <p>          Leung, ETY, Ho, PL, Yuen, KY, Woo, WL, Lam, TH, Kao, RY, Seto, WH, and Yam, WC</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(3): 1075-1078, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300034</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
