

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-07-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lSsBLN1BLYYI+HOmMgclS8y94+thjLM48sYjwvWELgQ80FmbNrtbR7kAhzcES723m4HN/fJtRPme9q8m3RI1KDSMYTBvruXu6RKVrD9XyZAAjX2FSSIPo4kGlCM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="10667D91" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">June 25 - July 8, 2010</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>
    <br>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20593456">Small molecular compounds that inhibit hepatitis C virus replication through destabilizing heat shock cognate 70 messenger RNA.</a></p>

    <p class="NoSpacing">Peng ZG, Fan B, Du NN, Wang YP, Gao LM, Li YH, Li YH, Liu F, You XF, Han YX, Zhao ZY, Cen S, Li JR, Song DQ, Jiang JD.</p>

    <p class="NoSpacing">Hepatology. 2010 May 18. [Epub ahead of print]PMID: 20593456 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20586716">Studies of Benzothiadiazine Derivatives as Hepatitis C Virus NS5B Polymerase Inhibitors Using 3D-QSAR, Molecular Docking and Molecular Dynamics.</a></p>

    <p class="NoSpacing">Wang X, Yang W, Xu X, Zhang H, Li Y, Wang Y.</p>

    <p class="NoSpacing">Curr Med Chem. 2010 Jun 29. [Epub ahead of print]PMID: 20586716 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20585111">Resistance Analysis of the HCV NS5A Inhibitor, BMS-790052, in the In Vitro Replicon System.</a></p>

    <p class="NoSpacing">Fridell RA, Qiu D, Wang C, Valera L, Gao M.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2010 Jun 28. [Epub ahead of print]PMID: 20585111 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20580554">Synthesis and anti-HCV activity of 3&#39;,4&#39;-oxetane nucleosides.</a></p>

    <p class="NoSpacing">Chang W, Du J, Rachakonda S, Ross BS, Convers-Reignier S, Yau WT, Pons JF, Murakami E, Bao H, Steuer HM, Furman PA, Otto MJ, Sofia MJ.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2010 Jun 8. [Epub ahead of print]PMID: 20580554 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20579888">Studies on the anti-hepatitis C virus activity of newly synthesized tropolone derivatives: Identification of NS3 helicase inhibitors that specifically inhibit subgenomic HCV replication.</a></p>

    <p class="NoSpacing">Najda-Bernatowicz A, Krawczyk M, Stankiewicz-Drogo&#324; A, Bretner M, Boguszewska-Chachulska AM.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2010 Jun 2. [Epub ahead of print]PMID: 20579888 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="memofmt2-2">HSV-1 Virus</p>
    <br>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20603154">Discovery of a novel TLR2 signaling inhibitor with anti-viral activity.</a></p>

    <p class="NoSpacing">Zhou S, Cerny AM, Bowen G, Chan M, Kurt-Jones EA, Finberg RW.</p>

    <p class="NoSpacing">Antiviral Res. 2010 Jul 2. [Epub ahead of print]PMID: 20603154 [PubMed - as supplied by publisher]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
