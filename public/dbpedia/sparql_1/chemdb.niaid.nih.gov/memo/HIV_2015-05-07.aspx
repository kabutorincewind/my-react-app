

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-05-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X2v1O3giS3DWC3G3SO240Bn8pk/v2xxpsuvd1NoLc7ZeGw+u3GP9uM+Ep/h4C3iBDYzVLHo7UjjSYMdzr84nrnUqx3FeqYAXw9CLgdZ8fxJ2vphac6L6vsLHEmU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5F869341" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: April 24 - May 7, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25838144">Synthesis and Biological Evaluation of Novel HIV-1 Protease Inhibitors Using Tertiary Amine as P2-Ligands.</a> Yang, Z.H., X.G. Bai, L. Zhou, J.X. Wang, H.T. Liu, and Y.C. Wang. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(9): p. 1880-1883. PMID[25838144].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25847660">Bis(Benzoyloxybenzyl)-dippro Nucleoside Diphosphates of anti-HIV Active Nucleoside Analogues.</a> Weinschenk, L., T. Gollnest, D. Schols, J. Balzarini, and C. Meier. ChemMedChem, 2015. 10(5): p. 891-900. PMID[25847660].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25676492">Anti-HIV-1 Integrase Activity and Molecular Docking Study of Compounds from Caesalpinia sappan L.</a> Tewtrakul, S., P. Chaniad, S. Pianwanit, C. Karalai, C. Ponglimanont, and O. Yodsaoue. Phytotherapy Research, 2015. 29(5): p. 724-729. PMID[25676492].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25766862">Enzymatic Synthesis of Acyclic Nucleoside Thiophosphonate Diphosphates: Effect of the alpha-Phosphorus Configuration on HIV-1 RT Activity.</a> Priet, S., L. Roux, M. Saez-Ayala, F. Ferron, B. Canard, and K. Alvarez. Antiviral Research, 2015. 117: p. 122-131. PMID[25766862].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25847768">Development of Benzimidazole Derivatives to Inhibit HIV-1 Replication through Protecting APOBEC3G Protein.</a> Pan, T., X. He, B. Chen, H. Chen, G. Geng, H. Luo, H. Zhang, and C. Bai. European Journal of Medicinal Chemistry, 2015. 95: p. 500-513. PMID[25847768].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25915507">HBAPH-25, an in-Silico Designed Peptide, Inhibits HIV-1 Entry by Blocking gp120 Binding to CD4 Receptor.</a> Bashir, T., M. Patgaonkar, C.S. Kumar, A. Pasi, and K.V. Reddy. Plos One, 2015. 10(4): p. e0124839. PMID[25915507].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25691652">A Novel Tricyclic Ligand-containing Nonpeptidic HIV-1 Protease Inhibitor, GRL-0739, Effectively Inhibits the Replication of Multidrug-resistant HIV-1 Variants and Has a Desirable Central Nervous System Penetration Property in Vitro.</a> Amano, M., Y. Tojo, P.M. Salcedo-Gomez, G.L. Parham, P.R. Nyalapatla, D. Das, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2015. 59(5): p. 2625-2635. PMID[25691652].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0424-050715.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000353000300019">Procyanidin Trimer C1 Derived from Theobroma cacao Reactivates Latent Human Immunodeficiency Virus Type 1 Provirus.</a> Hon, T., J. Barnor, T.N. Huu, O. Morinaga, A. Hamano, J. Ndzinu, A. Frimpong, K. Minta-Asare, M. Amoa-Bosompem, J. Brandful, J. Odoom, J. Bonney, I. Tuffour, B.A. Owusu, M. Ofosuhene, P. Atchoglo, M. Sakyiamah, R. Adegle, R. Appiah-Opong, W. Ampofo, K. Koram, A. Nyarko, L. Okine, D. Edoh, A. Appiah, T. Uto, Y. Yoshinaka, S. Uota, Y. Shoyama, and S. Yamaoka. Biochemical and Biophysical Research Communications, 2015. 459(2): p. 288-293. ISI[000353000300019].
    <br />
    <b>[WOS]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000352219600012">A Mutant Tat Protein Inhibits HIV-1 Reverse Transcription by Targeting the Reverse Transcription Complex.</a> Lin, M.H., A. Apolloni, V. Cutillas, H. Sivakumaran, S. Martin, D.S. Li, T. Wei, R. Wang, H.P. Jin, K. Spann, and D. Harrich. Journal of Virology, 2015. 89(9): p. 4827-4836. ISI[000352219600012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0424-050715.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">10. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150409&amp;CC=WO&amp;NR=2015051230A1&amp;KC=A1">Novel Compositions Useful for Inhibiting HIV-1 Infection and Methods Using Same.</a> Cocklin, S. Patent. 2015. 2014-US58998 2015051230: 113pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">11. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150326&amp;CC=WO&amp;NR=2015039348A1&amp;KC=A1">Preparation of Tetracyclic Fused Pyrimidinone Derivatives Useful as HIV Integrase Inhibitors.</a> Neelamkavil, S.F., A. Walji, C.N.D. Marco, P. Coleman, J. Wai, I.T. Raheem, L. Hu, and X. Peng. Patent. 2015. 2013-CN83998 2015039348: 57pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">12. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150402&amp;CC=WO&amp;NR=2015044928A1&amp;KC=A1">Trisubstituted Indoles and Their Use as Non-Nucleoside Reverse Transcriptase Inhibitors for Treatment of HIV Infection or AIDS.</a> Pelly, S.C., W.A.L. Van Otterlo, R. Muller, and A. Basson. Patent. 2015. 2014-IB64955 2015044928: 31pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0424-050715.</p>

    <br />

    <p class="plaintext">13. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150402&amp;CC=WO&amp;NR=2015048363A1&amp;KC=A1">Preparation of Quinolizine Derivatives Useful as HIV Integrase Inhibitors in Treatment and Prophylaxis of HIV Infection.</a> Yu, T., Y. Zhang, S.T. Waddell, A. Stamford, J.S. Wai, P.J. Coleman, J.M. Sanders, and R. Ferguson. Patent. 2015. 2014-US57572 2015048363: 147pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0424-050715.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
