

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-304.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kpPjXyoX+lRjltDGXdtsrY4K/XDNA5H9P1R5HhjB0AnNZTnkDH7oSxphYuFN0L97E9msDqLsGVedHiQfDOxT41G+yYuSn+x4pxXB106g7brwtyA5Lxv3ainnKew=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DEDC2249" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH -OI-LS-304-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44011   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Rapid detection of ethambutol-resistant Mycobacterium tuberculosis strains by PCR-RFLP targeting embB codons 306 and 497 and iniA codon 501 mutations</p>

    <p>          Ahmad, S, Mokaddas, E, and Jaber, A-A</p>

    <p>          Molecular and Cellular Probes <b>2004</b>.  18(5): 299-306</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WNC-4CHRGDY-1/2/0fc1a263987e61e7c08ada24452448da">http://www.sciencedirect.com/science/article/B6WNC-4CHRGDY-1/2/0fc1a263987e61e7c08ada24452448da</a> </p><br />

    <p>2.         44012   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          A synthetic peptide as a novel anticryptococcal agent</p>

    <p>          Cenci, E, Bistoni, F, Mencacci, A, Perito, S, Magliani, W, Conti, S, Polonelli, L, and Vecchiarelli, A</p>

    <p>          Cell Microbiol  <b>2004</b>.  6(10): 953-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15339270&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15339270&amp;dopt=abstract</a> </p><br />

    <p>3.         44013   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Rapid amplification for the detection of Mycobacterium tuberculosis using a non-contact heating method in a silicon microreactor based thermal cycler</p>

    <p>          Ke, C, Berney, H, Mathewson, A, Sheehan, M, and M</p>

    <p>          Sensors and Actuators B: Chemical <b>2004</b>.  102(2): 308-314</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THH-4CHJ82K-N/2/f942581633bfb647a8622227cb2e1545">http://www.sciencedirect.com/science/article/B6THH-4CHJ82K-N/2/f942581633bfb647a8622227cb2e1545</a> </p><br />

    <p>4.         44014   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Synthesis and anti-tubercular activity of a series of 2-sulfonamido/trifluoromethyl-6-substituted imidazo[2,1-b]-1,3,4-thiadiazole derivatives</p>

    <p>          Gadad, Andanappa K, Noolvi, Malleshappa N, and Karpoormath, Rajshekhar V</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4D7JYBX-2/2/47c1c5253dd12460355710679934f208">http://www.sciencedirect.com/science/article/B6TF8-4D7JYBX-2/2/47c1c5253dd12460355710679934f208</a> </p><br />

    <p>5.         44015   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Molecular cloning, expression, purification, and characterization of fructose 1,6-bisphosphate aldolase from Mycobacterium tuberculosis--a novel Class II A tetramer</p>

    <p>          Ramsaywak, Peggy C, Labbe, Genevieve, Siemann, Stefan, Dmitrienko, Gary I, Guillemette, J, and Guy</p>

    <p>          Protein Expression and Purification <b>2004</b>.  37(1): 220-228</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WPJ-4CS4M68-3/2/23f55f3cb20f83b89a276ebf565cc3ab">http://www.sciencedirect.com/science/article/B6WPJ-4CS4M68-3/2/23f55f3cb20f83b89a276ebf565cc3ab</a> </p><br />
    <br clear="all">

    <p>6.         44016   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          The Novel Azole R126638 Is a Selective Inhibitor of Ergosterol Synthesis in Candida albicans, Trichophyton spp., and Microsporum canis</p>

    <p>          Vanden, Bossche H, Ausma, J, Bohets, H, Vermuyten, K, Willemsens, G, Marichal, P, Meerpoel, L, Odds, F, and Borgers, M</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(9): 3272-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15328084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15328084&amp;dopt=abstract</a> </p><br />

    <p>7.         44017   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Discovery of Cercosporamide, a known antifungal natural product, as a selective Pkc1 kinase inhibitor through high-throughput screening</p>

    <p>          Sussman, A, Huss, K, Chio, LC, Heidler, S, Shaw, M, Ma, D, Zhu, G, Campbell, RM, Park, TS, Kulanthaivel, P, Scott, JE, Carpenter, JW, Strege, MA, Belvo, MD, Swartling, JR, Fischl, A, Yeh, WK, Shih, C, and Ye, XS</p>

    <p>          Eukaryot Cell <b>2004</b>.  3(4): 932-43</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302826&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302826&amp;dopt=abstract</a> </p><br />

    <p>8.         44018   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Activity of capuramycin analogues against Mycobacterium tuberculosis, Mycobacterium avium and Mycobacterium intracellulare in vitro and in vivo</p>

    <p>          Koga, T, Fukuoka, T, Doi, N, Harasaki, T, Inoue, H, Hotoda, H, Kakuta, M, Muramatsu, Y, Yamamura, N, Hoshi, M, and Hirota, T</p>

    <p>          J Antimicrob Chemother <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15347635&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15347635&amp;dopt=abstract</a> </p><br />

    <p>9.         44019   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Crystallographic studies of shikimate binding and induced conformational changes in Mycobacterium tuberculosis shikimate kinase</p>

    <p>          Dhaliwal, Balvinder, Nichols, Charles E, Ren, Jingshan, Lockyer, Michael, Charles, Ian, Hawkins, Alastair R, and Stammers, David K</p>

    <p>          FEBS Letters <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4D34FFN-2/2/8e96ecc2272df8d3494b309d998add96">http://www.sciencedirect.com/science/article/B6T36-4D34FFN-2/2/8e96ecc2272df8d3494b309d998add96</a> </p><br />

    <p>10.       44020   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Structure of Mycobacterium tuberculosis FtsZ Reveals Unexpected, G Protein-like Conformational Switches</p>

    <p>          Leung, AK, Lucile, White E, Ross, LJ, Reynolds, RC, DeVito, JA, and Borhani, DW</p>

    <p>          J Mol Biol <b>2004</b>.  342(3): 953-70</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15342249&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15342249&amp;dopt=abstract</a> </p><br />

    <p>11.       44021   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Characterisation of rpsL, rrs and embB mutations associated with streptomycin and ethambutol resistance in Mycobacterium tuberculosis</p>

    <p>          Tracevska, Tatjana, Jansone, Inta, Nodieva, Anda, Marga, Olgerts, Skenders, Girts, and Baumanis, Viesturs</p>

    <p>          Research in Microbiology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VN3-4D2FRWY-4/2/f6f0d98837b39c6c5186ebf141bbffac">http://www.sciencedirect.com/science/article/B6VN3-4D2FRWY-4/2/f6f0d98837b39c6c5186ebf141bbffac</a> </p><br />

    <p>12.       44022   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Evaluation of moxifloxacin activity in vitro against Mycobacterium tuberculosis, including resistant and multidrug-resistant strains</p>

    <p>          Tortoli, E, Dionisio, D, and Fabbri, C</p>

    <p>          J Chemother <b>2004</b>.  16(4): 334-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332706&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332706&amp;dopt=abstract</a> </p><br />

    <p>13.       44023   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Genes Required for Intrinsic Multidrug Resistance in Mycobacterium avium</p>

    <p>          Philalay, JS, Palermo, CO, Hauge, KA, Rustad, TR, and Cangelosi, GA</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(9): 3412-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15328105&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15328105&amp;dopt=abstract</a> </p><br />

    <p>14.       44024   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of new inhibitors of UDP-Galf transferase-a key enzyme in M. tuberculosis cell wall biosynthesis</p>

    <p>          Cren, S, Gurcha, SS, Blake, AJ, Besra, GS, and Thomas, NR</p>

    <p>          Org Biomol Chem <b>2004</b>.  2(17): 2418-20</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15326520&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15326520&amp;dopt=abstract</a> </p><br />

    <p>15.       44025   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Genomics meets transgenics in search of the elusive Cryptosporidium drug target</p>

    <p>          Striepen, Boris and Kissinger, Jessica C</p>

    <p>          Trends in Parasitology <b>2004</b>.  20(8): 355-358</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7G-4CN9JNB-2/2/0d6e5f80b3adef7cf64fc989b86d48c8">http://www.sciencedirect.com/science/article/B6W7G-4CN9JNB-2/2/0d6e5f80b3adef7cf64fc989b86d48c8</a> </p><br />

    <p>16.       44026   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          TB tools to tell the tale-molecular genetic methods for mycobacterial research</p>

    <p>          Machowski, Edith E, Dawes, Stephanie, and Mizrahi, Valerie</p>

    <p>          The International Journal of Biochemistry &amp; Cell Biology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TCH-4D0NJ4T-4/2/3549d09bc882ab7ee24eed1e643f9a51">http://www.sciencedirect.com/science/article/B6TCH-4D0NJ4T-4/2/3549d09bc882ab7ee24eed1e643f9a51</a> </p><br />

    <p>17.       44027   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Impact of drug resistance on fitness of Mycobacterium tuberculosis strains of the W-Beijing genotype</p>

    <p>          Toungoussova, Olga S, Caugant, Dominique A, Sandven, Per, Mariandyshev, Andrey O, and Bjune, Gunnar</p>

    <p>          FEMS Immunology and Medical Microbiology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2T-4CMHMJK-1/2/4249074549e4080ce29310aebc0444cb">http://www.sciencedirect.com/science/article/B6T2T-4CMHMJK-1/2/4249074549e4080ce29310aebc0444cb</a> </p><br />

    <p>18.       44028   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Past, present, and future hepatitis C treatments</p>

    <p>          Foster, GR</p>

    <p>          Semin Liver Dis <b>2004</b>.  24 Suppl 2: 97-104</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15346252&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15346252&amp;dopt=abstract</a> </p><br />

    <p>19.       44029   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Role of amantadine and other adjuvant therapies in the treatment of hepatitis C</p>

    <p>          Brillanti, S</p>

    <p>          Semin Liver Dis <b>2004</b>.  24 Suppl 2: 89-95</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15346251&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15346251&amp;dopt=abstract</a> </p><br />

    <p>20.       44030   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Progress and development of small molecule HCV antivirals</p>

    <p>          Ni, ZJ and Wagman, AS</p>

    <p>          Curr Opin Drug Discov Devel <b>2004</b>.  7(4): 446-59</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15338954&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15338954&amp;dopt=abstract</a> </p><br />

    <p>21.       44031   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Hepatitis C treatment update</p>

    <p>          Pearlman, BL</p>

    <p>          Am J Med <b>2004</b>.  117(5): 344-52</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15336584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15336584&amp;dopt=abstract</a> </p><br />

    <p>22.       44032   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          SAR and pharmacokinetic studies on phenethylamide inhibitors of the hepatitis C virus NS3/NS4A serine protease</p>

    <p>          Malancona, Savina, Colarusso, Stefania, Ontoria, Jesus M, Marchetti, Antonella, Poma, Marco, Stansfield, Ian, Laufer, Ralph, Di Marco, Annalise, Taliani, Marina, and Verdirame, Maria</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(17): 4575-4579</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4CWRKT5-2/2/7aaae23d1ad83685a8cc6a8f067b6f46">http://www.sciencedirect.com/science/article/B6TF9-4CWRKT5-2/2/7aaae23d1ad83685a8cc6a8f067b6f46</a> </p><br />

    <p>23.       44033   OI-LS-304; EMBASE-OI-9/7/2004</p>

    <p class="memofmt1-2">          Active site inhibitors of HCV NS5B polymerase. The development and pharmacophore of 2-thienyl-5,6-dihydroxypyrimidine-4-carboxylic acid</p>

    <p>          Stansfield, Ian, Avolio, Salvatore, Colarusso, Stefania, Gennari, Nadia, Narjes, Frank, Pacini, Barbara, Ponzi, Simona, and Harper, Steven</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4D4PPK3-B/2/30c2fdf1ef46655ce06232aa65fa4b10">http://www.sciencedirect.com/science/article/B6TF9-4D4PPK3-B/2/30c2fdf1ef46655ce06232aa65fa4b10</a> </p><br />

    <p>24.       44034   OI-LS-304; PUBMED-OI-9/7/2004</p>

    <p class="memofmt1-2">          Safety and Antiretroviral Effectiveness of Concomitant Use of Rifampicin and Efavirenz for Antiretroviral-Naive Patients in India Who Are Coinfected With Tuberculosis and HIV-1</p>

    <p>          Patel, A, Patel, K, Patel, J, Shah, N, Patel, B, and Rani, S</p>

    <p>          J Acquir Immune Defic Syndr <b>2004</b>.  37(1): 1166-1169</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15319677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15319677&amp;dopt=abstract</a> </p><br />

    <p>25.       44035   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Comparison of alamar blue and MTT assays for high through-put screening</p>

    <p>          Hamid, R, Rotshteyn, Y, Rabadi, L, Parikh, R, and Bullock, P</p>

    <p>          TOXICOL IN VITRO <b>2004</b>.  18(5): 703-710</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223022500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223022500018</a> </p><br />

    <p>26.       44036   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Isolation of specific against NS3 helicase and high-affinity RNA aptamers domain of hepatitis C virus</p>

    <p>          Hwang, B, Cho, JS, Yeo, HJ, Kim, JH, Chung, KM, Han, K, Jang, SK, and Lee, SW</p>

    <p>          RNA <b>2004</b>.  10 (8): 1277-1290</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222952300012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222952300012</a> </p><br />

    <p>27.       44037   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Drug repositioning: Identifying and developing new uses for existing drugs</p>

    <p>          Ashburn, TT and Thor, KB</p>

    <p>          NAT REV DRUG DISCOV <b>2004</b>.  3(8): 673-683</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223005000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223005000018</a> </p><br />

    <p>28.       44038   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Current progress in the fatty acid metabolism in Cryptosporidium parvum</p>

    <p>          Zhu, G</p>

    <p>          J EUKARYOT MICROBIOL <b>2004</b>.  51(4): 381-388</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222957300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222957300001</a> </p><br />
    <br clear="all">

    <p>29.       44039   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Antifungal activity of a spirostanol tomatoside obtained from Lycopersicon esculentum mill against various yeast species</p>

    <p>          Baissac, Y, Benoit-Vical, F, Lavaud, C, and Imbert, C</p>

    <p>          J MYCOL MED <b>2004</b>.  14(2): 59-63</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222921300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222921300001</a> </p><br />

    <p>30.       44040   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis diverts alpha interferon-induced monocyte differentiation from dendritic cells into immunoprivileged macrophage-like host cells</p>

    <p>          Mariotti, S, Teloni, R, Iona, E, Fattorini, L, Romagnoli, G, Gagliardi, MC, Orefici, G, and Nisini, R</p>

    <p>          INFECT IMMUN <b>2004</b>.  72(8): 4385-4392</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222932600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222932600008</a> </p><br />

    <p>31.       44041   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Synthesis of novel substituted tetrazoles having antifungal activity</p>

    <p>          Upadhayaya, RS, Jain, S, Sinha, N, Kishore, N, Chandra, R, and Arora, SK</p>

    <p>          EUR J MED CHEM  <b>2004</b>.  39(7): 579-592</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222926700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222926700003</a> </p><br />

    <p>32.       44042   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Susceptibility of Mycobacterium avium sbsp paratuberculosis to monensin sodium or tilmicosin phosphate in vitro and resulting infectivity in a murine model</p>

    <p>          Brumbaugh, GW, Simpson, RB, Edwards, JF, Anders, DR, and Thomson, TD</p>

    <p>          CAN J VET RES <b>2004</b>.  68(3): 175-181</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223031500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223031500003</a> </p><br />

    <p>33.       44043   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Identification of [(naphthalene-1-carbonyl)-amino]-acetic acid derivatives as nonnucleoside inhibitors of HCVNS5B RNA dependent RNA polymerase</p>

    <p>          Gopalsamy, A, Lim, K, Ellingboe, JW, Krishnamurthy, G, Orlowski, M, Feld, B, van, Zeijl M, and Howe, AYM</p>

    <p>          BIOORG MED CHEM LETT <b>2004</b>.  14(16): 4221-4224</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222923300020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222923300020</a> </p><br />

    <p>34.       44044   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activity of 6-chloro (Unsubstituted)-2-methoxy-9-substituted acridine derivatives</p>

    <p>          Aly, EI and Abadi, AH</p>

    <p>          ARCH PHARM RES  <b>2004</b>.  27(7): 713-719</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223008600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223008600003</a> </p><br />
    <br clear="all">

    <p>35.       44045   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Nitric oxide generated from isoniazid activation by KatG: Source of nitric oxide and activity against Mycobacterium tuberculosis</p>

    <p>          Timmins, GS, Master, S, Rusnak, F, and Deretic, V</p>

    <p>          ANTIMICROB AGENTS CH <b>2004</b>.  48(8): 3006-3009</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300032</a> </p><br />

    <p>36.       44046   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          In vitro activities of ravuconazole and four other antifungal agents against fluconazole-resistant or -susceptible clinical yeast isolates</p>

    <p>          Cuenca-Estrella, M, Gomez-Lopez, A, Mellado, E, Garcia-Effron, G, and Rodriguez-Tudela, JL</p>

    <p>          ANTIMICROB AGENTS CH <b>2004</b>.  48(8): 3107-3111</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300046">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300046</a> </p><br />

    <p>37.       44047   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Rv2686c-rv2687c-rv2688c, an ABC fluoroquinolone efflux pump in Mycobacterium tuberculosis</p>

    <p>          Pasca, MR, Guglierame, P, Arcesi, F, Bellinzoni, M, De, Rossi E, and Riccardi, G</p>

    <p>          ANTIMICROB AGENTS CH <b>2004</b>.  48(8): 3175-3178</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300063">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222998300063</a> </p><br />

    <p>38.       44048   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Molecular identification of mycobacteria and detection of antibiotic resistance</p>

    <p>          Cattoir, V</p>

    <p>          ANN BIOL CLIN-PARIS <b>2004</b>.  62(4): 405-413</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222894200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222894200003</a> </p><br />

    <p>39.       44049   OI-LS-304; WOS-OI-9/2/2004</p>

    <p class="memofmt1-2">          Identification of the 5-methylthiopentosyl substituent in mycobacterium tuberculosis lipoarabinomannan</p>

    <p>          Turnbull, WB, Shimizu, KH, Chatterjee, D, Homans, SW, and Treumann, A</p>

    <p>          ANGEW CHEM INT EDIT <b>2004</b>.  43(30): 3918-3922</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223047200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223047200006</a> </p><br />

    <p>40.       44050   OI-LS-304; WOS-OI-9/5/2004</p>

    <p class="memofmt1-2">          Protection against the early acute phase of Cryptosporidium parvum infection conferred by interleukin-4-induced expression of T helper 1 cytokines</p>

    <p>          McDonald, SAC, O&#39;Grady, JE, Bajaj-Elliott, M, Notley, CA, Alexander, J, Brombacher, F, and McDonald, V</p>

    <p>          J INFECT DIS <b>2004</b>.  190(5): 1019-1025</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223114800022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223114800022</a> </p><br />

    <p>41.       44051   OI-LS-304; WOS-OI-9/5/2004</p>

    <p class="memofmt1-2">          Combined effect of liposomal and conventional amphotericin B in a mouse model of systemic infection with Candida albicans</p>

    <p>          Kretschmar, M, Bertsch, T, and Nichterlein, T</p>

    <p>          J CHEMOTHERAPY  <b>2004</b>.  16(3): 255-258</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223085700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223085700006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
