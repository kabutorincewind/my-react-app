

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-04-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BOT/7old5DWTpuFikRBm+cDh0S92XNSPjO74aQ68Aw9AaGI6QoSAY/W7FAlbM9XWfXnS2m4/PdCgzOcaFkxsnpKKpIaN9f0EI6GURwdmWRJwrZjRU/O9ZuuIVDI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="263FD57E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 30 - April 12, 2012</h1>

    <h2>Hepatitis B virus</h2>

    <p class="plaintext">1. <a href="yhttp://www.ncbi.nlm.nih.gov/pubmed/22472044">Synthesis, Structure-Activity Relationships and Biological Evaluation of Caudatin Derivatives as Novel Anti-hepatitis B Virus Agents</a><i>.</i> Wang, L.J., C.A. Geng, Y.B. Ma, X.Y. Huang, J. Luo, H. Chen, R.H. Guo, X.M. Zhang, and J.J. Chen, Bioorganic &amp; Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22472044].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22487892">25-Hydroxyvitamin D(3) Suppresses Hepatitis C Virus Production.</a> Matsumura, T., T. Kato, N. Sugiyama, M. Tasaka-Fujita, A. Murayama, T. Masaki, T. Wakita, and M. Imawari, Hepatology (Baltimore, Md.), 2012. <b>[Epub ahead of print]</b>; PMID[22487892].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22473861">Structure-based Macrocyclization Yields Hepatitis C Virus NS5B Inhibitors with Improved Binding Affinities and Pharmacokinetic Properties.</a> Cummings, M.D., T.I. Lin, L. Hu, A. Tahri, D. McGowan, K. Amssoms, S. Last, B. Devogelaere, M.C. Rouan, L. Vijgen, J.M. Berke, P. Dehertogh, E. Fransen, E. Cleiren, L. van der Helm, G. Fanning, K. Van Emelen, O. Nyanguile, K. Simmen, P. Raboisson, and S. Vendeville, Angewandte Chemie (International ed. in English), 2012. <b>[Epub ahead of print]</b>; PMID[22473861].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22472692">5-Benzothiazole Substituted Pyrimidine Derivatives as HCV Replication (Replicase) Inhibitors.</a> Arasappan, A., F. Bennett, V. Girijavallabhan, Y. Huang, R. Huelgas, C. Alvarez, L. Chen, S. Gavalas, S.H. Kim, A. Kosinski, P. Pinto, R. Rizvi, R. Rossman, B. Shankar, L. Tong, F. Velazquez, S. Venkatraman, V.A. Verma, J. Kozlowski, N.Y. Shih, J.J. Piwinski, M. Maccoss, C.D. Kwong, J.L. Clark, A.T. Fowler, F. Geng, H.S. Kezar, 3rd, A. Roychowdhury, R.C. Reynolds, J.A. Maddry, S. Ananthan, J.A. Secrist, 3rd, C. Li, R. Chase, S. Curry, H.C. Huang, X. Tong, and F.G. Njoroge, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22472692].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22472694">Discovery of 4&#39;-Azido-2&#39;-deoxy-2&#39;-C-methyl cytidine and Prodrugs thereof: A Potent Inhibitor of Hepatitis C Virus Replication.</a> Nilsson, M., G. Kalayanov, A. Winqvist, P. Pinho, C. Sund, X.X. Zhou, H. Wahling, A.K. Belfrage, M. Pelcman, T. Agback, K. Benckestock, K. Wikstrom, M. Boothee, A. Lindqvist, C. Rydegard, T.H. Jonckers, K. Vandyck, P. Raboisson, T.I. Lin, S. Lachau-Durand, H. de Kock, D.B. Smith, J.A. Martin, K. Klumpp, K. Simmen, L. Vrang, Y. Terelius, B. Samuelsson, S. Rosenquist, and N.G. Johansson, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22472694].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22471376">Discovery of Novel Urea-based Hepatitis C Protease Inhibitors with High Potency against Protease-inhibitor-resistant Mutants.</a> Kazmierski, W.M., R. Hamatake, M. Duan, L.L. Wright, G.K. Smith, R.L. Jarvest, J.J. Ji, J.P. Cooper, M.D. Tallant, R.M. Crosby, K. Creech, A. Wang, X. Li, S. Zhang, Y.K. Zhang, Y. Liu, C.Z. Ding, Y. Zhou, J.J. Plattner, S.J. Baker, W. Bu, and L. Liu, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22471376].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22465429">A Plant-derived Flavonoid Inhibits Entry of All HCV Genotypes into Human Hepatocytes.</a> Haid, S., A. Novodomska, J. Gentzsch, C. Grethe, S. Geuenich, D. Bankwitz, P. Chhatwal, B. Jannack, T. Hennebelle, F. Bailleul, O.T. Keppler, M. Ponisch, R. Bartenschlager, C. Hernandez, M. Lemasson, A. Rosenberg, F. Wong-Staal, E. Davioud-Charvet, and T. Pietschmann, Gastroenterology, 2012. <b>[Epub ahead of print]</b>; PMID[22465429].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22462058">Antiviral Activity of Inonotus obliquus Fungus Extract towards Infection Caused by Hepatitis C Virus in Cell Cultures.</a> Shibnev, V.A., D.V. Mishin, T.M. Garaev, N.P. Finogenova, A.G. Botikov, and P.G. Deryabin, Bulletin of Experimental Biology and Medicine, 2011. 151(5): p. 612-4; PMID[22462058].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22458448">Synthesis of the HCV Protease Inhibitor Vaniprevir (MK-7009) Using Ring-closing Metathesis Strategy.</a> Kong, J., C.Y. Chen, J. Balsells-Padros, Y. Cao, R.F. Dunn, S.J. Dolman, J. Janey, H. Li, and M.J. Zacuto, The Journal of Organic Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22458448].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22457281">A New Class of Synthetic Peptide Inhibitors Block Attachment and Entry of Human Pathogenic Viruses.</a> Krepstakies, M., J. Lucifora, C.H. Nagel, M.B. Zeisel, B. Holstermann, H. Hohenberg, I. Kowalski, T. Gutsmann, T.F. Baumert, K. Brandenburg, J. Hauber, and U. Protzer, The Journal of Infectious Diseases, 2012. <b>[Epub ahead of print]</b>; PMID[22457281].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22474501">Efficacy of Bidens pilosa Extract against Herpes Simplex Virus Infection in Vitro and in Vivo.</a> Nakama, S., K. Tamaki, C. Ishikawa, M. Tadano, and N. Mori, Evidence-Based Complementary and Alternative Medicine : eCAM, 2012. 2012: p. 413453; PMID[22474501].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <h2>HSV-2</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22491462">A 3-O Sulfated Heparan Sulfate Binding Peptide Preferentially Targets Herpes Simplex Virus Type-2 Infected Cells.</a> Ali, M.M., G.A. Karasneh, M.J. Jarding, V. Tiwari, and D. Shukla, Journal of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22491462].
    <br />
    <b>[PubMed]</b>. OV_0330-041212.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301170000026">A Novel Class of Highly Potent Irreversible Hepatitis C Virus NS5B Polymerase Inhibitors.</a> Chen, K.X., C.A. Lesburg, B. Vibulbhan, W.Y. Yang, T.Y. Chan, S. Venkatraman, F. Velazquez, Q.B. Zeng, F. Bennett, G.N. Anilkumar, J. Duca, Y.H. Jiang, P. Pinto, L. Wang, Y.H. Huang, O. Selyutin, S. Gavalas, H.Y. Pu, S. Agrawal, B. Feld, H.C. Huang, C. Li, K.C. Cheng, N.Y. Shih, J.A. Kozlowski, S.B. Rosenblum, and F.G. Njoroge, Journal of Medicinal Chemistry, 2012. 55(5): p. 2089-2101; ISI[000301170000026].
    <br />
    <b>[WOS]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299816100009">Conformationally Locked Nucleoside Analogues Based on the Bridgehead Substituted 7-Oxonorbornane and Their Antiviral Properties.</a> Dejmek, M., H. Hrebabecky, M. Dracinsky, J. Neyts, P. Leyssen, H. Mertlikova-Kaiserova, and R. Nencka, Collection of Czechoslovak Chemical Communications, 2011. 76(12): p. 1549-1566; ISI[000299816100009].
    <br />
    <b>[WOS]</b>. OV_0330-041212.  </p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301379300029">Integrated Structure-based Activity Prediction Model of Benzothiadiazines on Various Genotypes of HCV NS5b Polymerase (1a, 1b and 4) and Its Application in the Discovery of New Derivatives.</a> Ismail, M.A.H., D.A.A. El Ella, K.A.M. Abouzid, and A.H. Mahmoud, Bioorganic and Medicinal Chemistry, 2012. 20(7): p. 2455-2478; ISI[000301379300029].
    <br />
    <b>[WOS]</b>. OV_0330-041212.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299816100002">7-Halogenated 7-Deazapurine 2 &#39;-C-Methylribonucleosides.</a> Seela, F., S. Budow, K.Y. Xu, X.H. Peng, and H. Eickmeier, Collection of Czechoslovak Chemical Communications, 2011. 76(12): p. 1413-1431; ISI[000299816100002].
    <br />
    <b>[WOS]</b>. OV_0330-041212.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
