

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-04-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xfAL3RmfjbVcJfQHkQSKry2YQl50IduQ+7sjzzFPpdljDavUhCgsK/fweoG+g9Aozkqg7ynXxOQWBMsms5NNUl53Asrrf00PVptgzZVfHhsW5vZXBqb3v+LevAM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="12CE8B21" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 30 - April 12, 2012</h1>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22449052">Discovery of Selective Menaquinone Biosynthesis Inhibitors against Mycobacterium tuberculosis.</a> Debnath, J., S. Siricilla, B. Wan, D.C. Crick, A.J. Lenaerts, S.G. Franzblau, and M. Kurosu. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22449052].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/224815373">Antioxidant, Antitubercular and Cytotoxic Activities of Piper Imperiale.</a> Diaz, L.E., D.R. Munoz, R.E. Prieto, S.A. Cuervo, D.L. Gonzalez, J.D. Guzman, and S. Bhakta. Molecules (Basel, Switzerland), 2012. 17(4): p. 4142-4157; PMID[22481537].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22452568">Structure-based Design of Novel Benzoxazinorifamycins with Potent Binding Affinity to Wild-type and Rifampin-resistant Mutant Mycobacterium tuberculosis RNA Polymerases.</a> Gill, S.K., H. Xu, P.D. Kirchhoff, T. Cierpicki, A.J. Turbiak, B. Wan, N. Zhang, K.W. Peng, S.G. Franzblau, G.A. Garcia, and H.D. Showalter. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22452568]. <b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21953875">Antimicrobial Antioxidant Daucane Sesquiterpenes from Ferula hermonis Boiss.</a> Ibraheim, Z.Z., W.M. Abdel-Mageed, H. Dai, H. Guo, L. Zhang, and M. Jaspars. Phytotherapy Research : PTR, 2012. 26(4): p. 579-586; PMID[21953875].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22283814">Inhibition of 1-Deoxy-D-xylulose-5-phosphate reductoisomerase (dxr): A Review of the Synthesis and Biological Evaluation of Recent Inhibitors.</a> Jackson, E.R. and C.S. Dowd. Current Topics in Medicinal Chemistry, 2012. 12(7): p. 706-728; PMID[22283814].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22468745">Design, Synthesis and in Vivo/in Vitro Screening of Novel Chlorokojic Acid Derivatives.</a> Karakaya, G., M.D. Aytemir, B. Ozcelik, and U. Calis. Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22468745].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22473422">Synthesis and in Vitro Anti-tubercular Evaluation of 1,2,3-Triazole Tethered beta-Lactam-Ferrocene and beta-Lactam-Ferrocenylchalcone Chimeric Scaffolds.</a> Kumar, K., P. Singh, L. Kremer, Y. Guerardel, C. Biot, and V. Kumar. Dalton Transactions (Cambridge, England : 2003), 2012. <b>[Epub ahead of print]</b>; PMID[22473422].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22283815">Derivatives of 3-Isoxazolecarboxylic acid Esters - a Potent and Selective Compound Class against Replicating and Nonreplicating Mycobacterium tuberculosis.</a> Lilienkampf, A., M. Pieroni, S.G. Franzblau, W.R. Bishai, and A.P. Kozikowski. Current Topics in Medicinal Chemistry, 2012. 12(7): p. 729-734; PMID[22283815].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22326144">Synthesis and Evaluation of Novel Monosubstituted Sulfonylurea Derivatives as Antituberculosis Agents.</a> Pan, L., Y. Jiang, Z. Liu, X.H. Liu, G. Wang, Z.M. Li, and D. Wang. European Journal of Medicinal Chemistry, 2012. 50: p. 18-26; PMID[22326144].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22477069">Combining Cheminformatics Methods and Pathway Analysis to Identify Molecules with Whole-cell Activity against Mycobacterium tuberculosis.</a> Sarker, M., C. Talcott, P. Madrid, S. Chopra, B.A. Bunin, G. Lamichhane, J.S. Freundlich, and S. Ekins. Pharmaceutical Research, 2012. <b>[Epub ahead of print]</b>; PMID[22477069].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22283813">Drug Design and Identification of Potent Leads against Mycobacterium tuberculosis Thymidine Monophosphate Kinase.</a> Van Calenbergh, S., S. Pochet, and H. Munier-Lehmann. Current Topics in Medicinal Chemistry, 2012. 12(7): p. 694-705; PMID[22283813].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p>
    <p> </p>
    
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22252809">In Vitro and in Vivo Antimalarial Activities of T-2307, a Novel Arylamidine.</a> Kimura, A., H. Nishikawa, N. Nomura, J. Mitsuyama, S. Fukumoto, N. Inoue, and S. Kawazu. Antimicrobial Agents and Chemotherapy, 2012. 56(4): p. 2191-2193; PMID[22252809].</p>

    <p class="plaintext"> <b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22477194">Synthesis and Evaluation of Peptidyl Alpha,Beta-unsaturated Carbonyl Derivatives as Anti-malarial Calpain Inhibitors.</a> Mallik, S.K., Y. Li da, M. Cui, H.O. Song, H. Park, and H.S. Kim. Archives of Pharmacal Research, 2012. 35(3): p. 469-479; PMID[22477194].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22476602">In Vitro Activity of Extracts and Isolated Polyphenols from West African Medicinal Plants against Plasmodium falciparum.</a> Ndjonka, D., B. Bergmann, C. Agyare, F.M. Zimbres, K. Luersen, A. Hensel, C. Wrenger, and E. Liebau. Parasitology Research, 2012. <b>[Epub ahead of print]</b>; PMID[22476602].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p>
    <p> </p>
    
    <h2>ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21922239">Antileishmanial, Antitrypanosomal, and Cytotoxic Screening of Ethnopharmacologically Selected Peruvian Plants.</a> Gonzalez-Coloma, A., M. Reina, C. Saenz, R. Lacret, L. Ruiz-Mesia, V.J. Aran, J. Sanz, and R.A. Martinez-Diaz. Parasitology Research, 2012. 110(4): p. 1381-1392; PMID[21922239].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22336386">Synthesis and Anti-Leishmanial Activity of 5-(5-Nitrofuran-2-Yl)-1,3,4-Thiadiazol-2-Amines Containing N-[(1-Benzyl-1h-1,2,3-Triazol-4-Yl)Methyl] Moieties.</a> Tahghighi, A., S. Razmi, M. Mahdavi, P. Foroumadi, S.K. Ardestani, S. Emami, F. Kobarfard, S. Dastmalchi, A. Shafiee, and A. Foroumadi. European Journal of Medicinal Chemistry, 2012. 50: p. 124-128; PMID[22336386].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0330-041212.</p>
    <p> </p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299687700003">Antimycobacterial Activity of Ferutinin Alone and in Combination with Antitubercular Drugs against a Rapidly Growing Surrogate of Mycobacterium tuberculosis.</a> Abourashed, E.A., A.M. Galal, and A.M. Shibl. Natural Product Research, 2011. 25(12): p. 1142-1149; ISI[000299687700003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301169800002">Efficient One-pot Preparation of Novel Fused Chromeno 2,3-D pyrimidine and Pyrano 2,3-D pyrimidine Derivatives.</a> Aly, H.M. and M.M. Kamal. European Journal of Medicinal Chemistry, 2012. 47: p. 18-23; ISI[000301169800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301286000013">The Additive and Synergistic Antimicrobial Effects of Select Frankincense and Myrrh Oils - a Combination from the Pharaonic Pharmacopoeia.</a> De Rapper, S., S.F. Van Vuuren, G.P.P. Kamatou, A.M. Viljoen, and E. Dagne. Letters in Applied Microbiology, 2012. 54(4): p. 352-358; ISI[000301286000013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301016800032">In Vitro Evaluation of Antimicrobial and Antioxidant Activities of Some Tunisian Vegetables.</a> Edziri, H., S. Ammar, L. Souad, M.A. Mahjoub, M. Mastouri, M. Aouni, Z. Mighri, and L. Verschaeve. South African Journal of Botany, 2012. 78: p. 252-256; ISI[000301016800032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297573000027">Antimicrobial Potential of Gemmo-modified Extracts of Terminalia arjuna and Euphorbia tirucalli.</a> Jahan, N., R. Khalil Ur, S. Ali, and I.A. Bhatti. International Journal of Agriculture and Biology, 2011. 13(6): p. 1001-1005; ISI[000297573000027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301480100002">Isonicotinic acid hydrazide Derivatives: Synthesis, Antimycobacterial, Antiviral, Antimicrobial Activity and QSAR Studies.</a> Judge, V., B. Narasimhan, M. Ahuja, D. Sriram, and P. Yogeeswari. Letters in Drug Design &amp; Discovery, 2011. 8(9): p. 792-810; ISI[000301480100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299297400006">Synthesis, Spectral Characterization, in-Vitro Antibacterial and Antifungal Activities of Novel (2E)-Ethyl-2-(2-(2, 4-dinitrophenyl) hydrazono)-4-(naphthalen-2-yl)-6-arylcyclohex-3-enecarboxylates.</a> Kanagarajan, V., J. Thanusu, and M. Gopalakrishnan. Iranian Journal of Pharmaceutical Research, 2011. 10(4): p. 711-725; ISI[000299297400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301379300003">Generation and Exploration of New Classes of Antitubercular Agents: The Optimization of Oxazolines, Oxazoles, Thiazolines, Thiazoles to Imidazo 1,2-a Pyridines and Isomeric 5,6-Fused Scaffolds.</a> Moraski, G.C., L.D. Markley, M. Chang, S. Cho, S.G. Franzblau, C.H. Hwang, H. Boshoff, and M.J. Miller. Bioorganic &amp; Medicinal Chemistry, 2012. 20(7): p. 2214-2220; ISI[000301379300003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298811900023">Synthesis and Antimycobacterial Activity of Various 1-(8-Quinolinyloxy)-3-piperazinyl(piperidinyl)-5-(4-cyano-3-trifluoromet hylphenylamino)-S-triazines.</a> Patel, R.V., P. Kumari, D.P. Rajani, and K.H. Chikhalia. Acta Chimica Slovenica, 2011. 58(4): p. 802-810; ISI[000298811900023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p>  
    <p> </p>
    
    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299465800011">Synthesis, Characterization, DNA Cleavage, and in-Vitro Antimicrobial Studies of Co(II), Ni(II), and Cu(II) Complexes with Schiff Bases of Coumarin Derivatives.</a> Patil, S.A., V.H. Naik, A.D. Kulkarni, S.N. Unki, and P.S. Badami. Journal of Coordination Chemistry, 2011. 64(15): p. 2688-2697; ISI[000299465800011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300404900032">Camphorsulfonic acid Catalysed Facile Tandem Double Friedlander Annulation Protocol for the Synthesis of Phenoxy Linked Bisquinoline Derivatives and Discovery of Antitubercular Agents.</a> Paul, N., M. Murugavel, S. Muthusubramanian, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(4): p. 1643-1648; ISI[000300404900032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300430600004">Evaluation of Antifungal Activity in Essential Oil of the Syzygium aromaticum (L.) by Extraction, Purification and Analysis of Its Main Component Eugenol.</a> Rana, I.S., A.S. Rana, and R.C. Rajak. Brazilian Journal of Microbiology, 2011. 42(4): p. 1269-1277; ISI[000300430600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301370700001">Cytotoxic and Antifungal Activities of 5-Hydroxyramulosin, a Compound Produced by an Endophytic Fungus Isolated from Cinnamomum mollisimum.</a> Santiago, C., C. Fitchett, M.H.G. Munro, J. Jalil, and J. Santhanam. Evidence-Based Complementary and Alternative Medicine, 2012. <b>[Epub ahead of print]</b>; ISI[000301370700001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298692700001">Metal-based Biologically Active Compounds: Synthesis, Spectral, and Antimicrobial Studies of Cobalt, Nickel, Copper, and Zinc Complexes of Triazole-derived Schiff Bases.</a> Singh, K., Y. Kumar, P. Puri, C. Sharma, and K.R. Aneja. Bioinorganic Chemistry and Applications, 2011. <b>[Epub ahead of print]</b>; ISI[000298692700001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301185800010">Isoniazid Metal Complex Reactivity and Insights for a Novel Anti-tuberculosis Drug Design.</a> Sousa, E.H.S., L.A. Basso, D.S. Santos, I.C.N. Diogenes, E. Longhinotti, L.G.D. Lopes, and I.D. Moreira. Journal of Biological Inorganic Chemistry, 2012. 17(2): p. 275-283; ISI[000301185800010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">32<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301454400067">. The Mechanism of Antifungal Action of Essential Oil from Dill (Anethum graveolens L.) on Aspergillus flavus.</a> Tian, J., X.Q. Ban, H. Zeng, J.S. He, Y.X. Chen, and Y.W. Wang. Plos One, 2012. 7(1); ISI[000301454400067].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300430600044">Phytochemical Screening and Antimicrobial Activity of Roots of Murraya koenigii (Linn.) Spreng. (Rutaceae).</a> Vats, M., H. Singh, and S. Sardana. Brazilian Journal of Microbiology, 2011. 42(4): p. 1569-1573; ISI[000300430600044].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p> 
    <p> </p>
    
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301169800026">Synthesis, Antimicrobial, Antimycobacterial and Structure-Activity Relationship of Substituted Pyrazolo-, Isoxazolo-, Pyrimido- and Mercaptopyrimidocyclohepta B Indoles.</a> Yamuna, E., R.A. Kumar, M. Zeller, and K.J.R. Prasad. European Journal of Medicinal Chemistry, 2012. 47: p. 228-238; ISI[000301169800026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0330-041212.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
