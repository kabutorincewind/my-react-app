

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pnbR3NkAPvCXWZajsnZXj+/oGnYFj8B1H8nbb5HupGodJ4zwUseQU0dBHOHSiaP+OtuwYUaT5GUzZAqi18ne6tiECTat5rpiQ28u3qJ0yVjOlRNOupSnihhQgzw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9FEAAFBA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800"><i>Mycobacterium</i> Citations List: August, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28749330">In Vitro Activity of Bedaquiline against Rapidly Growing Nontuberculous Mycobacteria.</a> Aguilar-Ayala, D.A., M. Cnockaert, E. Andre, K. Andries, Y.M.J.A. Gonzalez, P. Vandamme, J.C. Palomino, and A. Martin. Journal of Medicinal Microbiology, 2017. 66(8): p. 1140-1143. PMID[28749330].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">2. <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5524660">Novel Drug Targets for Mycobacterium tuberculosis: 2-Heterostyrylbenzimidazoles as Inhibitors of Cell Wall Protein Synthesis.</a> Anguru, M.R., A.K. Taduri, R.D. Bhoomireddy, M. Jojula, and S.K. Gunda. Chemistry Central Journal, 2017. 11(68): 11pp. PMCID[PMC5524660].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28749666">Substituted N-Phenyl-5-(2-(phenylamino)thiazol-4-yl)isoxazole-3-carboxamides are Valuable Antitubercular Candidates That Evade Innate Efflux Machinery.</a> Azzali, E., D. Machado, A. Kaushik, F. Vacondio, S. Flisi, C.S. Cabassi, G. Lamichhane, M. Viveiros, G. Costantino, and M. Pieroni. Journal of Medicinal Chemistry, 2017. 60(16): p. 7108-7122. PMID[28749666].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28764749">Evaluation of Antimycobacterial, Leishmanicidal and Antibacterial Activity of Three Medicinal Orchids of Arunachal Pradesh, India.</a> Bhatnagar, M., N. Sarkar, N. Gandharv, O. Apang, S. Singh, and S. Ghosal. BMC Complementary and Alternative Medicine, 2017. 17(1): p. 379. PMID[28764749]. PMCID[PMC5540558].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000406175000002">Synthesis and Biological Evaluation of Pyrimidinyl Sulphonamide Derivatives as Promising Class of Antitubercular Agents.</a> Bhuva, N.H., P.K. Talpara, P.M. Singala, V.K. Gothaliya, and V.H. Shah. Journal of Saudi Chemical Society, 2017. 21(5): p. 517-527. ISI[000406175000002].
    <br />
    <b>[WOS]</b>. TB_08_2017.</p><br />

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405467600054">Antimicrobial Activity of Illudalane and Alliacane Sesquiterpenes from the Mushroom Gloeostereum incarnatum BCC41461.</a> Bunbamrung, N., C. Intaraudom, A. Dramae, N. Boonyuen, S. Veeranondha, P. Rachtawee, and P. Pittayakhajonwut. Phytochemistry Letters, 2017. 20: p. 274-281.
    <br />
    ISI[000405467600054].
    <br />
    <b>[WOS]</b>. TB_08_2017.</p><br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405935700010">Synthesis, Antibacterial, Antifungal, Antimycobacterial Activity Evaluation of Novel 1,2,4-Triazole Derivatives Bearing 4-Aminophenyl Moiety.</a> Cavusoglu, B.K., L. Yurttas, M.Y. Cankilic, and Z.A. Kaplancikli. Letters in Drug Design &amp; Discovery, 2017. 14(8): p. 938-948. ISI[000405935700010].
    <br />
    <b>[WOS]</b>. TB_08_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28682613">Synthesis of a 3-Amino-2,3-dihydropyrid-4-one and Related Heterocyclic Analogues as Mechanism-based Inhibitors of BioA, a Pyridoxal Phosphate-dependent Enzyme.</a> Eiden, C.G. and C.C. Aldrich. Journal of Organic Chemistry, 2017. 82(15): p. 7806-7819. PMID[28682613]. PMCID[PMC5590672].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28607021">Selective Killing of Dormant Mycobacterium tuberculosis by Marine Natural Products.</a> Felix, C.R., R. Gupta, S. Geden, J. Roberts, P. Winder, S.A. Pomponi, M.C. Diaz, J.K. Reed, A.E. Wright, and K.H. Rohde. Antimicrobial Agents and Chemotherapy, 2017. 61(8): e00743-17. PMID[28607021]. PMCID[PMC5527660].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28699251">Antituberculotic Activity of Actinobacteria Isolated from the Rare Habitats.</a> Hussain, A., M.A. Rather, A.M. Shah, Z.S. Bhat, A. Shah, Z. Ahmad, and Q.P. Hassan. Letters in Applied Microbiology, 2017. 65(3): p. 256-264. PMID[28699251].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28527405">Synthesis and Structure-Activity Relationships of Novel Fused Ring Analogues of Q203 as Antitubercular Agents.</a> Kang, S., Y.M. Kim, H. Jeon, S. Park, M.J. Seo, S. Lee, D. Park, J. Nam, S. Lee, K. Nam, S. Kim, and J. Kim. European Journal of Medicinal Chemistry, 2017. 136: p. 420-427. PMID[28527405].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28575382">In Vitro and in Vivo Activity of Biapenem against Drug-susceptible and Rifampicin-resistant Mycobacterium tuberculosis.</a> Kaushik, A., N.C. Ammerman, R. Tasneen, E. Story-Roller, K.E. Dooley, S.E. Dorman, E.L. Nuermberger, and G. Lamichhane. Journal of Antimicrobial Chemotherapy, 2017. 72(8): p. 2320-2325. PMID[28575382].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28674049">Activity of LCB01-0371, a Novel Oxazolidinone, against Mycobacterium abscessus.</a> Kim, T.S., J.H. Choe, Y.J. Kim, C.S. Yang, H.J. Kwon, J. Jeong, G. Kim, D.E. Park, E.K. Jo, Y.L. Cho, and J. Jang. Antimicrobial Agents and Chemotherapy, 2017. 61(9): e02752-16. PMID[28674049]. PMCID[PMC5571369].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28552479">Biopharmaceutic Parameters, Pharmacokinetics, Transport and CYP-Mediated Drug Interactions of IIIM-017: A Novel Nitroimidazooxazole Analogue with Anti-tuberculosis Activity.</a> Kour, G., P.P. Singh, A. Bhagat, and Z. Ahmed. European Journal of Pharmaceutical Sciences, 2017. 106: p. 71-78. PMID[28552479].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28789944">Prioritization of Natural Compounds against Mycobacterium tuberculosis 3-Dehydroquinate dehydratase: A Combined in-Silico and in-Vitro Study.</a> Lone, M.Y., M. Athar, V.K. Gupta, and P.C. Jha. Biochemical and Biophysical Research Communications, 2017. 491(4): p. 1105-1111. PMID[28789944].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28068839">Antituberculosis Compounds from a Deep-sea-derived Fungus Aspergillus Sp. SCSIO Ind09F01.</a> Luo, X., X. Zhou, X. Lin, X. Qin, T. Zhang, J. Wang, Z. Tu, B. Yang, S. Liao, Y. Tian, X. Pang, K. Kaliyaperumal, J.L. Li, H. Tao, and Y. Liu. Natural Product Research, 2017. 31(16): p. 1958-1962. PMID[28068839].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28855504">Biosynthesis of Ilamycins Featuring Unusual Building Blocks and Engineered Production of Enhanced Anti-tuberculosis Agents.</a> Ma, J., H. Huang, Y. Xie, Z. Liu, J. Zhao, C. Zhang, Y. Jia, Y. Zhang, H. Zhang, T. Zhang, and J. Ju. Natural Communications, 2017. 8(1): p. 391. PMID[28855504]. PMCID[PMC5577134].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28601525">Anti-mycobacterial Alkaloids, Cyclic 3-Alkyl pyridinium Dimers, from the Indonesian Marine Sponge Haliclona sp.</a> Maarisit, W., D.B. Abdjul, H. Yamazaki, H. Kato, H. Rotinsulu, D.S. Wewengkang, D.A. Sumilat, M.M. Kapojos, K. Ukai, and M. Namikoshi. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(15): p. 3503-3506. PMID[28601525].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27585570">Microwave-assisted Synthesis of 2-Styrylquinoline-4-carboxylic acids as Antitubercular Agents.</a> Muscia, G.C., S.E. Asis, and G.Y. Buldain. Medicinal Chemistry, 2017. 13(5): p. 448-452. PMID[27585570].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28635184">Design, Synthesis, and Biological Evaluation of Isothiosemicarbazones with Antimycobacterial Activity.</a> Novotna, E., K. Waisser, J. Kunes, K. Palat, L. Skalova, B. Szotakova, V. Buchta, J. Stolarikova, V. Ulmann, M. Pavova, J. Weber, J. Komrskova, P. Haskova, I. Vokral, and V. Wsol. Archiv der Pharmazie, 2017. 350(8): e1700020. PMID[28635184].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000406602000013">Design, Synthesis and QSAR Studies of 2-Amino benzo[d]thiazolyl Substituted Pyrazol-5-ones: Novel Class of Promising Antibacterial Agents.</a> Palkar, M.B., A. Patil, G.A. Hampannavar, M.S. Shaikh, H.M. Patel, A.M. Kanhed, M.R. Yadav, and R.V. Karpoormath. Medicinal Chemistry Research, 2017. 26(9): p. 1969-1987. ISI[000406602000013].
    <br />
    <b>[WOS]</b>. TB_08_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28674058">A Novel 6-Benzyl ether benzoxaborole Is Active against Mycobacterium tuberculosis in Vitro.</a> Patel, N., T. O&#39;Malley, Y.K. Zhang, Y. Xia, B. Sunde, L. Flint, A. Korkegian, T.R. Ioerger, J. Sacchettini, M.R.K. Alley, and T. Parish. Antimicrobial Agents and Chemotherapy, 2017. 61(9): e01205-17. PMID[28674058]. PMCID[PMC5571309].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000407471900042">Ring Functionalization and Molecular Hybridization of Quinolinyl pyrazole: Design, Synthesis and Antimycobacterial Activity.</a> Rachakonda, V., S.S. Kotapalli, R. Ummanni, and M. Alla. ChemistrySelect, 2017. 2(22): p. 6529-6534. ISI[000407471900042].<b><br />
    [WOS]</b>. TB_08_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28317754">First Report on Isolation of 2,3,4-Trihydroxy-5-methylacetophenone from Palmyra Palm (Borassus flabellifer Linn.) Syrup, Its Antioxidant and Antimicrobial Properties.</a> Reshma, M.V., J. Jacob, V.L. Syamnath, V.P. Habeeba, B.S. Dileep Kumar, and R.S. Lankalapalli. Food Chemistry, 2017. 228: p. 491-496. PMID[28317754].<b><br />
    [PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000394058900051">Synthesis of Novel Triazole-incorporated Isatin Derivatives as Antifungal, Antitubercular, and Antioxidant Agents and Molecular Docking Study.</a> Shaikh, M.H., D.D. Subhedar, F.A.K. Khan, J.N. Sangshetti, L. Nawale, M. Arkile, D. Sarkar, and B.B. Shingate. Journal of Heterocyclic Chemistry, 2017. 54(1): p. 413-421. ISI[000394058900051].<b><br />
    [WOS]</b>. TB_08_2017.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28185538">Synthesis and Biological Evaluation of Hybrid 1,5-and 2,5-Disubstituted indoles as Potentially New Antitubercular Agents.</a> Soares, A., M.S. Estevao, M.M.B. Marques, V. Kovalishyn, D. Latino, J. Aires-de-Sousa, J. Ramos, M. Viveiros, and F. Martins. Medicinal Chemistry, 2017. 13(5): p. 439-447. PMID[28185538].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28537707">The Tyrosine Kinase Inhibitor Gefitinib Restricts Mycobacterium tuberculosis Growth through Increased Lysosomal Biogenesis and Modulation of Cytokine Signaling.</a> Sogi, K.M., K.A. Lien, J.R. Johnson, N.J. Krogan, and S.A. Stanley. ACS Infectious Diseases, 2017. 3(8): p. 564-574. PMID[28537707].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28083914">Synthesis and Biological Evaluation of 1,2,4-Triazole-3-thione and 1,3,4-Oxadiazole-2-thione as Antimycobacterial Agents.</a> Sonawane, A.D., N.D. Rode, L. Nawale, R.R. Joshi, R.A. Joshi, A.P. Likhite, and D. Sarkar. Chemical Biology &amp; Drug Design, 2017. 90(2): p. 200-209. PMID[28083914].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28586174">2,6-Difluorobenzamide Inhibitors of Bacterial Cell Division Protein FtsZ: Design, Synthesis, and Structure-Activity Relationships.</a> Straniero, V., C. Zanotto, L. Straniero, A. Casiraghi, S. Duga, A. Radaelli, C. De Giuli Morghen, and E. Valoti. ChemMedChem, 2017. 12(16): p. 1303-1318. PMID[28586174].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28713884">Antimicrobial Activity of Organometallic Isonicotinyl and Pyrazinyl Ferrocenyl-derived Complexes.</a> Stringer, T., R. Seldon, N. Liu, D.F. Warner, C. Tam, L.W. Cheng, K.M. Land, P.J. Smith, K. Chibale, and G.S. Smith. Dalton Transactions, 2017. 46(30): p. 9875-9885. PMID[28713884].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28587823">Identification and Synthesis of Novel Inhibitors of Mycobacterium ATP Synthase.</a> Surase, Y.B., K. Samby, S.R. Amale, R. Sood, K.P. Purnapatre, P.K. Pareek, B. Das, K. Nanda, S. Kumar, and A.K. Verma. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(15): p. 3454-3459. PMID[28587823].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28778369">Discovery of New Leads against Mycobacterium tuberculosis Using Scaffold Hopping and Shape Based Similarity.</a> Wavhale, R.D., E.A.F. Martis, P.K. Ambre, B.J. Wan, S.G. Franzblau, K.R. Iyer, K. Raikuvar, K. Macegoniuk, L. Berlicki, S.R. Nandan, and E.C. Coutinho. Bioorganic &amp; Medicinal Chemistry, 2017. 25(17): p. 4835-4844. PMID[28778369].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28576632">The Synthesis and Evaluation of Triazolopyrimidines as Anti-tubercular Agents.</a> Zuniga, E.S., A. Korkegian, S. Mullen, E.J. Hembre, P.L. Ornstein, G. Cortez, K. Biswas, N. Kumar, J. Cramer, T. Masquelin, P.A. Hipskind, J. Odingo, and T. Parish. Bioorganic &amp; Medicinal Chemistry, 2017. 25(15): p. 3922-3946. PMID[28576632].
    <br />
    <b>[PubMed]</b>. TB_08_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170817&amp;CC=WO&amp;NR=2017137743A1&amp;KC=A1">Preparation of Disubstituted 2-Oxo-octahydropyrrolo[3,4-E][1,3]oxazines as Antibacterial Compds.</a> Cooper, I. and A. Lyons. Patent. 2017. 2017-GB50317 2017137743: 102pp.
    <br />
    <b>[Patent]</b>. TB_08_2017.</p>

    <br />

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170817&amp;CC=WO&amp;NR=2017137744A1&amp;KC=A1">Preparation of 2-Oxo-4,4,5,5,6,6,7,7-octahydrobenzoxazole Derivatives, and Their Use as Antibacterial Compounds.</a> Cooper, I., A. Lyons, D. Orr, J. Kirkham, and K. Blades. Patent. 2017. 2017-GB50318 2017137744: 267pp.
    <br />
    <b>[Patent]</b>. TB_08_2017.</p>

    <br />

    <p class="plaintext">36. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170824&amp;CC=WO&amp;NR=2017143112A2&amp;KC=A2">An Oxazolidinone for Treatment of Infections with Mycobacterium tuberculosis.</a>Lamichhane, G. and J.T. Ippoliti. Patent. 2017. 2017-US18248 2017143112: 33pp.
    <br />
    <b>[Patent]</b>. TB_08_2017.</p>

    <br />

    <p class="plaintext">37. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170803&amp;CC=WO&amp;NR=2017132321A1&amp;KC=A1">Preparation of Novel Inhibitors of Bacterial Growth Targeting L,D-Transpeptidases.</a>Lamichhane, G., C.A. Townsend, E. Lloyd, A. Kaushik, P. Kumar, J. Freundlich, S. Li, S. Ekins, and N. Parrish. Patent. 2017. 2017-US15046 2017132321: 86pp.
    <br />
    <b>[Patent]</b>. TB_08_2017.</p>

    <br />

    <p class="plaintext">38. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170727&amp;CC=WO&amp;NR=2017125944A1&amp;KC=A1">Broad Spectrum Antibacterial Activity of Novel Bisbenzimidazoles Targeting Topoisomerases and the Synergistic Composition of Bisbenzimidazole with Efflux Pump Inhibitors against Pathogenic Bacteria.</a> Tandon, V. and D. Sinha. Patent. 2017. 2017-IN13 2017125944: 31pp.
    <br />
    <b>[Patent]</b>. TB_08_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
