

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-62.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OH55pes2JVa7xDrwhTC+tEJ8kRO5Ix9wkb67Igthdg5aviesGH4SKGEBhaDpWwHDP6uqi9gbCUJxNHYOh9/CbpXiyCrvhk3xbn+aauvCWqJ8zDqw5vDa1VgqdPs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C470E42B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - DMID-LS-62-MEMO</b> </p>

<p>    1.    5746   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Highly attenuated smallpox vaccine protects mice with and without immune deficiencies against pathogenic vaccinia virus challenge</p>

<p>           Wyatt, LS, Earl, PL, Eller, LA, and Moss, B</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15016914&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15016914&amp;dopt=abstract</a> </p><br />

<p>    2.    5747   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           [Some research clues on Chinese herbal medicine for SARS prevention and treatment]</p>

<p>           Xiao, PG, Wang, YY, and Chen, HS</p>

<p>           Zhongguo Zhong Yao Za Zhi 2003. 28(6): 481-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15015319&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15015319&amp;dopt=abstract</a> </p><br />

<p>    3.    5748   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Design and synthesis of DNA-intercalating 9-fluoren-[beta]-O-glycosides as potential IFN-inducers, and antiviral and cytostatic agents*1</p>

<p>           Alcaro, S, Arena, A, Neri, S, Ottana, R, Ortuso, F, Pavone, B, and Vigorita, MG</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(7): 1781-1791</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BT8CH6-1/2/eddbae1b3a16c49ffae5df89c372aa0f">http://www.sciencedirect.com/science/article/B6TF8-4BT8CH6-1/2/eddbae1b3a16c49ffae5df89c372aa0f</a> </p><br />

<p>    4.    5749   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           [Isolation and elucidation of chemical constituents with antiviral action from yinqiaosan on influenza virus]</p>

<p>           Shi, Y, Shi, RB, Liu, B, Lu, YR, and Du, LJ</p>

<p>           Zhongguo Zhong Yao Za Zhi 2003. 28(1): 43-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15015266&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15015266&amp;dopt=abstract</a> </p><br />

<p>    5.    5750   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Effects of pharmacological cyclin-dependent kinase inhibitors on viral transcription and replication</p>

<p>           Schang, Luis M</p>

<p>           Biochimica et Biophysica Acta (BBA) - Proteins &amp; Proteomics 2004. 1697(1-2): 197-209</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B73DJ-4B5R93M-4/2/cb66b65a8841fe76aef5d782a611b2fd">http://www.sciencedirect.com/science/article/B73DJ-4B5R93M-4/2/cb66b65a8841fe76aef5d782a611b2fd</a> </p><br />

<p>    6.    5751   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Exploring the binding mechanism of the main proteinase in SARS-associated coronavirus and its implication to anti-SARS drug design</p>

<p>           Zhang, Xue Wu and Yap, Yee Leng</p>

<p>           Bioorganic &amp; Medicinal Chemistry  2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BWW86H-7/2/66794c05e6f1e049fbe6ef054325f4f9">http://www.sciencedirect.com/science/article/B6TF8-4BWW86H-7/2/66794c05e6f1e049fbe6ef054325f4f9</a> </p><br />

<p>    7.    5752   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Synthesis of 5&#39;-C-methyl-1&#39;,3&#39;-dioxolan-4&#39;-yl nucleosides</p>

<p>           Du, Jinfa, Patterson, Steven, Shi, Junxing, Chun, Byoung-Kwon, Stuyver, Lieven J, and Watanabe, Kyoichi A</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(5): 1243-1245</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BRJ576-19/2/5141ccfe25d22fe7d90d3716c0ef924a">http://www.sciencedirect.com/science/article/B6TF9-4BRJ576-19/2/5141ccfe25d22fe7d90d3716c0ef924a</a> </p><br />

<p>    8.    5753   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Non-nucleoside inhibitors of the hepatitis C virus NS5B polymerase: discovery of benzimidazole 5-carboxylic amide derivatives with low-nanomolar potency</p>

<p>           Beaulieu, PL, Bos, M, Bousquet, Y, DeRoy, P, Fazal, G, Gauthier, J, Gillard, J, Goulet, S, McKercher, G, Poupart, MA, Valois, S, and Kukolj, G</p>

<p>           Bioorg Med Chem Lett 2004. 14(4): 967-71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15013003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15013003&amp;dopt=abstract</a> </p><br />

<p>    9.    5754   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Controlling influenza by inhibiting the virus&#39;s neuraminidase</p>

<p>           Garman, E and Laver, G</p>

<p>           Curr Drug Targets 2004. 5(2): 119-36</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15011946&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15011946&amp;dopt=abstract</a> </p><br />

<p>  10.    5755   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Synthesis and Antiviral Activity of Betulonic Acid Amides and Conjugates With Amino Acids</b></p>

<p>           Flekhter, OB, Boreko, EI, Nigmatullina, LR, Tret&#39;yakova, EV, Pavlova, NI, Baltina, LA, Nikolaeva, SN, Savinova, OV, Eremin, VF, Galin, FZ, and Tolstikov, GA</p>

<p>           Russian Journal of Bioorganic Chemistry 2004. 30(1): 80-88</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  11.    5756   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Highly potent and long-acting trimeric and tetrameric inhibitors of influenza virus neuraminidase</p>

<p>           Watson, KG, Cameron, R, Fenton, RJ, Gower, D, Hamilton, S, Jin, B, Krippner, GY, Luttick, A, McConnell, D, MacDonald, SJ, Mason, AM, Nguyen, V, Tucker, SP, and Wu, WY</p>

<p>           Bioorg Med Chem Lett 2004. 14(6): 1589-92</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006410&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15006410&amp;dopt=abstract</a> </p><br />

<p>  12.    5757   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Management of herpes simplex virus infections</p>

<p>           Jones, Cheryl A and Isaacs, David</p>

<p>           Current Paediatrics 2004. 14(2): 131-136</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WDB-4BT1J21-7/2/71c03310a7add4a8d3929e454f56dd04">http://www.sciencedirect.com/science/article/B6WDB-4BT1J21-7/2/71c03310a7add4a8d3929e454f56dd04</a> </p><br />

<p>  13.    5758   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Changes in Hepatitis C Viral Response After Initiation of Highly Active Antiretroviral Therapy and Control of HIV Viremia in Chronically Co-infected Individuals</p>

<p>           Kottilil, S, Jagannatha, S, Lu, A, Liu, S, McLaughlin, M, Metcalf, JA, Dewar, R, Campbell, C, Koratich, C, Maldarelli, F, Masur, H, and Polis, MA</p>

<p>           HIV Clin Trials 2004. 5(1): 25-32</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15002084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15002084&amp;dopt=abstract</a> </p><br />

<p>  14.    5759   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Synergistic combination of n-acetylcysteine and ribavirin to protect from lethal influenza viral infection in a mouse model</p>

<p>           Ghezzi, P and Ungheri, D</p>

<p>           Int J Immunopathol Pharmacol 2004.  17(1): 99-102</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000873&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15000873&amp;dopt=abstract</a> </p><br />

<p>  15.    5760   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Early Hcv Dynamics on Peg-Interferon and Ribavirin in Hiv/Hcv Co-Infection: Indications for the Investigation of New Treatment Approaches</b></p>

<p>           Ballesteros, AL, Franco, S, Fuster, D, Planas, R, Martinez, MA, Acosta, L, Sirera, G, Salas, A, Tor, J, Rey-Joly, C, Clotet, B, and Tural, C</p>

<p>           Aids 2004. 18(1): 59-66</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  16.    5761   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Synthesis, characterization and antiviral activity against influenza virus of a series of novel manganese-substituted rare earth borotungstates heteropolyoxometalates</p>

<p>           Liu, Jie, Mei, Wen-Jie, Xu, An-Wu, Tan, Cai-Ping, Shi, Shuo, and Ji, Liang-Nian</p>

<p>           Antiviral Research 2004. 62(1): 65-71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4BHT75X-1/2/0a715d9b50fd862776732d55495eb83f">http://www.sciencedirect.com/science/article/B6T2H-4BHT75X-1/2/0a715d9b50fd862776732d55495eb83f</a> </p><br />

<p>  17.    5762   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Interfering with capsid formation: A practicable antiviral strategy against hepatitis B virus?</p>

<p>           Kann, M</p>

<p>           Hepatology 2004. 39(3): 838-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14999705&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14999705&amp;dopt=abstract</a> </p><br />

<p>  18.    5763   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Neuraminidase Inhibitors. A New Weapon Against Influenza Virus Infections in Childhood?</b></p>

<p>           Larcher, C</p>

<p>           Monatsschrift Kinderheilkunde 2004.  152(1): 74-76</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  19.    5764   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Putranjivain A from Euphorbia jolkini inhibits both virus entry and late stage replication of herpes simplex virus type 2 in vitro</p>

<p>           Cheng, HY, Lin, TC, Yang, CM, Wang, KC, Lin, LT, and Lin, CC</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998984&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998984&amp;dopt=abstract</a> </p><br />

<p>  20.    5765   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Improved antiviral activity in vitro of ribavirin against measles virus after complexation with cyclodextrins</p>

<p>           Grancher, Nicolas, Venard, Veronique, Kedzierewicz, Francine, Ammerlaan, Wim, Finance, Chantal, Muller, Claude P, and Faou, Alain Le</p>

<p>           Antiviral Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4BTY94M-1/2/ba7a86e9d28b6499815b14768c7ab4cc">http://www.sciencedirect.com/science/article/B6T2H-4BTY94M-1/2/ba7a86e9d28b6499815b14768c7ab4cc</a> </p><br />

<p>  21.    5766   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Peginterferon-alpha2a and ribavirin combination therapy in chronic hepatitis C: a randomized study of treatment duration and ribavirin dose</p>

<p>           Hadziyannis, SJ, Sette, H Jr, Morgan, TR, Balan, V, Diago, M, Marcellin, P, Ramadori, G, Bodenheimer, H Jr, Bernstein, D, Rizzetto, M, Zeuzem, S, Pockros, PJ, Lin, A, and Ackrill, AM</p>

<p>           Ann Intern Med 2004. 140(5): 346-55</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14996676&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14996676&amp;dopt=abstract</a> </p><br />

<p>  22.    5767   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Design and performance testing of quantitative real time PCR assays for influenza A and B viral load measurement</p>

<p>           Ward, CL, Dempsey, MH, Ring, CJA, Kempson, RE, Zhang, L, Gor, D, Snowden, BW, and Tisdale, M</p>

<p>           Journal of Clinical Virology 2004.  29(3): 179-188</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-492W0P8-1/2/7575686b4ad97fc4190e40940c372112">http://www.sciencedirect.com/science/article/B6VJV-492W0P8-1/2/7575686b4ad97fc4190e40940c372112</a> </p><br />

<p>  23.    5768   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Herpes simplex virus</p>

<p>           Waggoner-Fountain, LA and Grossman, LB</p>

<p>           Pediatr Rev 2004. 25(3): 86-93</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14993516&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14993516&amp;dopt=abstract</a> </p><br />

<p>  24.    5769   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Targeting TFIIH to inhibit host cell transcription by Rift Valley Fever Virus</p>

<p>           Dasgupta, A</p>

<p>           Mol Cell 2004. 13(4): 456-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14992716&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14992716&amp;dopt=abstract</a> </p><br />

<p>  25.    5770   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Inhibitors of the Hepatitis C Virus Ns5b Rna-Dependent Rna Polymerase.</b></p>

<p>           Beaulieu, PL, Austel, V, Bos, M, Bousquet, Y, Fazal, G, Gauthier, J, Gillard, J, Goulet, S, Laplante, S, Poupart, MA, Lamarre, D, Lefebvre, S, Mckercher, G, Marquis, M, Pause, A, Pellerin, C, and Kukolj, G</p>

<p>           Abstracts of Papers of the American Chemical Society 2003. 226: 21-MEDI</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  26.    5771   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Solid Phase Parallel Synthesis of Alpha-Ketoamide Inhibitors of Ns3 Center Dot 4a Hcv Protease.</b></p>

<p>           Court, JJ, Cottrell, KC, Harbeson, SL, and Pitlik, J</p>

<p>           Abstracts of Papers of the American Chemical Society 2003. 226: 97-MEDI</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  27.    5772   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           The development of antimicrobials and vaccines against bacterial bioterrorism agents - where are we?</p>

<p>           Gilligan, Peter H</p>

<p>           Drug Discovery Today 2004. 9(5): 205-206</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T64-4BRJ5JJ-7/2/b20150ab26a0868fcdd0ecc6a64c0a29">http://www.sciencedirect.com/science/article/B6T64-4BRJ5JJ-7/2/b20150ab26a0868fcdd0ecc6a64c0a29</a> </p><br />

<p>  28.    5773   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Synthesis and Sar of 2-(4,5-Dihydroxy-6-Carboxy-Pyrimidinyl) Thiophenes as Inhibitors of the Hepatitis C Virus Ns5b Polymerase.</b></p>

<p>           Malancona, S, Attenni, B, Colarusso, S, Conte, I, Harper, S, Summa, V, Altamura, S, Koch, U, Matassa, VG, and Narjes, F</p>

<p>           Abstracts of Papers of the American Chemical Society 2003. 226: 123-MEDI</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  29.    5774   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Current concepts in SARS treatment</p>

<p>           Fujii, T, Nakamura, T, and Iwamoto, A</p>

<p>           J Infect Chemother 2004. 10(1): 1-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14991510&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14991510&amp;dopt=abstract</a> </p><br />

<p>  30.    5775   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Resistance to superinfection of Vero cells persistently infected with Junin virus</p>

<p>           Ellenberg, P, Edreira, M, and Scolaro, L</p>

<p>           Arch Virol 2004. 149(3): 507-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14991440&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14991440&amp;dopt=abstract</a> </p><br />

<p>  31.    5776   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Roscovitine, a cyclin-dependent kinase inhibitor, prevents replication of varicella-zoster virus</p>

<p>           Taylor, SL, Kinchington, PR, Brooks, A, and Moffat, JF</p>

<p>           J Virol 2004. 78(6): 2853-62</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14990704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14990704&amp;dopt=abstract</a> </p><br />

<p>  32.    5777   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Inhibition of respiratory syncytial virus replication by vector-derived small interfering RNAs against NS1 protein*1</p>

<p>           Zhang, W, Yang, H, Juan, HSan, Singam, R, Kong, X, Hellerman, G, Lockey, RF, Peeples, ME, and Mohapatra, SS</p>

<p>           Journal of Allergy and Clinical Immunology 2004. 113(2, Supplement 1): S330-S331</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WH4-4BRJ5RW-1HP/2/204edab898598b92c6bf0e879604ccc1">http://www.sciencedirect.com/science/article/B6WH4-4BRJ5RW-1HP/2/204edab898598b92c6bf0e879604ccc1</a> </p><br />

<p>  33.    5778   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Treatment of Interferon Non-Responsive Chronic Hepatitis C With Triple Therapy With Interferon, Ribavirin, and Amantidine Can Be Encouraging</b></p>

<p>           Khokhar, N</p>

<p>           Gut 2004. 53(3): 468-469</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  34.    5779   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Phase I/II trial of immunogenicity of a human papillomavirus (HPV) type 16 E7 protein-based vaccine in women with oncogenic HPV-positive cervical intraepithelial neoplasia</p>

<p>           Hallez, S, Simon, P, Maudoux, F, Doyen, J, Noel, JC, Beliard, A, CapelLe X, Buxant, F, Fayt, I, Lagrost, AC, Hubert, P, Gerday, C, Burny, A, Boniver, J, Foidart, JM, Delvenne, P, and Jacobs, N</p>

<p>           Cancer Immunol Immunother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14985860&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14985860&amp;dopt=abstract</a> </p><br />

<p>  35.    5780   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Respiratory syncytial virus infection: therapy and prevention</p>

<p>           Welliver, Robert C</p>

<p>           Paediatric Respiratory Reviews 2004.  5(Supplement 1): S127-S133</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WP5-4BRJ9SX-W/2/09e17812a83e90f2003348712ff19448">http://www.sciencedirect.com/science/article/B6WP5-4BRJ9SX-W/2/09e17812a83e90f2003348712ff19448</a> </p><br />

<p>  36.    5781   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Detection of BK virus and JC virus DNA in urine samples from immunocompromised (HIV-infected) and immunocompetent (HIV-non-infected) patients using polymerase chain reaction and microplate hybridisation</p>

<p>           Behzad-Behbahani, A, Klapper, PE, Vallely, PJ, Cleator, GM, and Khoo, SH</p>

<p>           Journal of Clinical Virology 2004.  29(4): 224-229</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-49569PK-1/2/b636d3c894f1b48d112df279e87ccdb9">http://www.sciencedirect.com/science/article/B6VJV-49569PK-1/2/b636d3c894f1b48d112df279e87ccdb9</a> </p><br />

<p>  37.    5782   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Interferon-inducible MyD88 protein inhibits hepatitis B virus replication</p>

<p>           Xiong, W, Wang, X, Liu, X, Xiang, L, Zheng, L, and Yuan, Z</p>

<p>           Virology 2004. 319(2): 306-14</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980490&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980490&amp;dopt=abstract</a> </p><br />

<p>  38.    5783   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p><b>           Potent nonclassical nucleoside antiviral drugs based on the N,N-diarylformamidine concept</b> </p>

<p>           Anastasi, C, Hantz, O, De Clercq, E, Pannecouque, C, Clayette, P, Dereuddre-Bosquet, N, Dormont, D, Gondois-Rey, F, Hirsch, I, and Kraus, JL</p>

<p>           J Med Chem 2004. 47(5): 1183-92</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971898&amp;dopt=abstract</a> </p><br />

<p>  39.    5784   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Phylogenomics and bioinformatics of SARS-CoV</p>

<p>           Lio, Pietro and Goldman, Nick</p>

<p>           Trends in Microbiology 2004. 12(3): 106-111</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TD0-4BMC9S2-4/2/56a40179b407a2b26c983a689167d3a7">http://www.sciencedirect.com/science/article/B6TD0-4BMC9S2-4/2/56a40179b407a2b26c983a689167d3a7</a> </p><br />

<p>  40.    5785   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           A randomized trial of a 4- vs 12-week daily interferon dose regimen combined with ribavirin in treatment of patients with chronic hepatitis C</p>

<p>           Sarin, SK, Goyal, A, Kumar, S, Guptan, RC, Hashmi, AZ, Sakhuja, P, and Malhotra, V</p>

<p>           Hepatobiliary Pancreat Dis Int 2004.  3(1): 42-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14969836&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14969836&amp;dopt=abstract</a> </p><br />

<p>  41.    5786   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Bovine Papillomavirus Replicative Helicase E1 Is a Target of the Ubiquitin Ligase Apc</b></p>

<p>           Mechali, F, Hsu, CY, Castro, A, Lorca, T, and Bonne-Andrea, C</p>

<p>           Journal of Virology 2004. 78(5): 2615-2619</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  42.    5787   DMID-LS-62; PUBMED-DMID-3/16/2004</p>

<p class="memofmt1-2">           Future antivirals-SMi conference: latest developments in HIV, hepatitis, herpes, influenza, RSV and SARS</p>

<p>           Harrison, R</p>

<p>           IDrugs 2004. 7 (1): 34-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968814&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968814&amp;dopt=abstract</a> </p><br />

<p>  43.    5788   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           The antiviral effect of interferon-beta against SARS-Coronavirus is not mediated by MxA protein</p>

<p>           Spiegel, Martin, Pichlmair, Andreas, Muhlberger, Elke, Haller, Otto, and Weber, Friedemann</p>

<p>           Journal of Clinical Virology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4BMC72D-1/2/5cbce8f5e2d3c0ec2ed56cc396d2152f">http://www.sciencedirect.com/science/article/B6VJV-4BMC72D-1/2/5cbce8f5e2d3c0ec2ed56cc396d2152f</a> </p><br />

<p>  44.    5789   DMID-LS-62; EMBASE-DMID-3/16/2004</p>

<p class="memofmt1-2">           Synthesis, anti-mycobacterial, anti-trichomonas and anti-candida in vitro activities of 2-substituted-6,7-difluoro-3-methylquinoxaline 1,4-dioxides</p>

<p>           Carta, Antonio, Loriga, Mario, Paglietti, Giuseppe, Mattana, Antonella, Fiori, Pier Luigi, Mollicotti, Paola, Sechi, Leonardo, and Zanetti, Stefania</p>

<p>           European Journal of Medicinal Chemistry 2004. 39(2): 195-203</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VKY-4BJ254G-1/2/adfea89dee1963eb2df080853d0c9b9e">http://www.sciencedirect.com/science/article/B6VKY-4BJ254G-1/2/adfea89dee1963eb2df080853d0c9b9e</a> </p><br />

<p>  45.    5790   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Synthesis and Anti-Hepatitis Virus Activity of Vertical Bar a, Vertical Bar a-Galactosyl Ceramides.</b></p>

<p>           Gao, KQ and Moriarty, RM</p>

<p>           Abstracts of Papers of the American Chemical Society 2003. 226: 24-MEDI</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  46.    5791   DMID-LS-62; WOS-DMID-3/16/2004</p>

<p>           <b>Interferon-Beta 1a and Sars Coronavirus Replication</b></p>

<p>           Hensley, LE, Fritz, EA, Jahrling, PB, Karp, CL, Huggins, JW, and Geisbert, TW</p>

<p>           Emerging Infectious Diseases 2004.  10(2): 317-319</p>           <!--HYPERLNK:-->
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
