

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-03-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="e+pmQMQlbVP0zOl35QHI3itvMLxgKJBfLvxtIiXLYwBUh4VUkZTXGK7aXlS0uF2JHpyWVqFagbxYdpRCwMNwe2uIWS6/xAX9Kl7VHF0hmEAcz6B++tSA8hKXnFc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DDD26374" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">March 18 - March 31, 2011</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21446701">Counterion-Induced Modulation in the Antimicrobial Activity and Biocompatibility of Amphiphilic Hydrogelators: Influence of in-Situ-Synthesized Ag-Nanoparticle on the Bactericidal Property.</a> Dutta, S., A. Shome, T. Kar, and P.K. Das. Langmuir: The ACS Journal of Surfaces and Colloids, 2011. <b>[Epub ahead of print]</b>; PMID[21446701].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21444708">Gentian Violet Exhibits Activity against Biofilms formed by Oral Candida isolates Obtained from HIV-infected Patients.</a> Traboulsi, R.S., P.K. Mukherjee, J. Chandra, R.A. Salata, R. Jurevic, and M.A. Ghannoum. Gentian Violet Exhibits Activity against Biofilms formed by Oral Candida isolates Obtained from HIV-infected Patients. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21444708].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21433056">Synthesis, Antibacterial and Antifungal Activities of Bifonazole Derivatives.</a> El Hage, S., B. Lajoie, C. Feuillolay, C. Roques, and G. Baziard. Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21433056].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21428374">Isolation and Structural Elucidation of Proline-Containing Cyclopentapeptides from an Endolichenic Xylaria sp.</a> Wu, W., H. Dai, L. Bao, B. Ren, J. Lu, Y. Luo, L. Guo, L. Zhang, and H. Liu. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21428374].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21417963">Synthesis and in vitro microbiological evaluation of novel diethyl 6,6&#39;-(1,4-phenylene)bis(4-aryl-2-oxo-cyclohex-3-enecarboxylates).</a> Kanagarajan, V., M.R. Ezhilarasi, and M. Gopalakrishnan. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21417963].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21409409">Evaluation of Antifungal Activity of Medicinal Plant Extracts Against Oral Candida albicans and Proteinases.</a> Hofling, J.F., R.C. Mardegan, P.C. Anibal, V.F. Furletti, and M.A. Foglio. Mycopathologia, 2011. <b>[Epub ahead of print]</b>; PMID[21409409].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.     </p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21441864">Chemical Composition and In Vitro Activity of Plant Extracts from Ferula communis and Dittrichia viscosa against Postharvest Fungi.</a> Mamoci, E., I. Cavoski, V. Simeone, D. Mondelli, L. Al-Bitar, and P. Caboni. Molecules, 2011. 16(3): p. 2609-2625; PMID[21441864].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21414146">Synthesis and in vitro antimycobacterial activity of N&#39;-benzylidene-2-oxo-2H-chromene-3-carbohydrazides derivatives.</a> Cardoso, S.H., M.B. Barreto, M.C. Lourenco, O.H.M.D. de, A.L. Candea, C.R. Kaiser, and M.V. de Souza. Chemical Biology and Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21414146].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21332115">Natural Product-Based Phenols as Novel Probes for Mycobacterial and Fungal Carbonic Anhydrases.</a> Davis, R.A., A. Hofmann, A. Osman, R.A. Hall, F.A. Muhlschlegel, D. Vullo, A. Innocenti, C.T. Supuran, and S.A. Poulsen. Journal of Medicinal Chemistry, 2011. 54(6): p. 1682-1692; PMID[21332115].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21417236">Ethionamide boosters: Synthesis, Biological Activity and Structure-Activity Relationships of a series of 1,2,4-oxadiazole EthR inhibitors.</a> Flipo, M., M. Desroses, N. Lecat-Guillet, B. Dirie, X. Carette, F. Leroux, C. Piveteau, F. Demirkaya, Z. Lens, P. Rucktooa, V. Villeret, T. Christophe, H.K. Jeon, C. Locht, P. Brodin, B.P. Deprez, A. Baulard, and N. Willand. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21417236].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21440336">2,3-Dideoxy hex-2-enopyranosid-4-uloses as promising new anti-tubercular agents: Design, synthesis, biological evaluation and SAR studies.</a> Saquib, M., I. Husain, S. Sharma, G. Yadav, V.K. Singh, S.K. Sharma, P. Shah, M.I. Siddiqi, B. Kumar, J. Lal, G.K. Jain, B.S. Srivastava, R. Srivastava, and A.K. Shaw. European Journal of Medicinal Chemistry, 2011.<b>[Epub ahead of print]</b>; PMID[21440336].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21422215">Aminoindoles: a Novel Scaffold with Potent Activity against Plasmodium falciparum.</a> Barker, R.H., Jr., S. Urgaonkar, R. Mazitschek, C. Celatka, R. Skerlj, J.F. Cortese, E. Tyndall, H. Liu, M. Cromwell, A.B. Sidhu, J.E. Guerrero-Bravo, K.N. Crespo-Llado, A.E. Serrano, J.W. Lin, C.J. Janse, S.M. Khan, M. Duraisingh, B.I. Coleman, I. Angulo-Barturen, M.B. Jimenez-Diaz, N. Magan, V. Gomez, S. Ferrer, M. Santos Martinez, S. Wittlin, P. Papastogiannidis, T. O&#39;Shea, J.D. Klinger, M. Bree, E. Lee, M. Levine, R.C. Wiegand, B. Munoz, D.F. Wirth, J. Clardy, I. Bathurst, and E. Sybertz. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21422215].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21426241">Screening for small molecule modulators of Hsp70 chaperone activity using protein aggregation suppression assays: inhibition of the plasmodial chaperone PfHsp70-1.</a> Cockburn, I.L., E.R. Pesce, J.M. Pryzborski, M.T. Davies-Coleman, P.G. Clark, R.A. Keyzers, L.L. Stephens, and G.L. Blatch. Biological Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21426241].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p class="NoSpacing">           </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21438586">Antiparasitic Compounds from Cupania cinerea with Activities against Plasmodium falciparum and Trypanosoma bruceirhodesiense.</a> Gachet, M.S., O. Kunert, M. Kaiser, R. Brun, M. Zehl, W. Keller, R.A. Munoz, R. Bauer, and W. Schuehly. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21438586].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21211554">Traditional herbal antimalarial therapy in Kilifi district, Kenya.</a> Gathirwa, J.W., G.M. Rukunga, P.G. Mwitari, N.M. Mwikwabe, C.W. Kimani, C.N. Muthaura, D.M. Kiboi, R.M. Nyangacha, and S.A. Omar. Journal of Ethnopharmacology, 2011. 134(2): p. 434-442; PMID[21211554].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21437649">Genetic analysis in mice identifies cysteamine as a novel partner for artemisinin in the treatment of malaria.</a> Min-Oo, G. and P. Gros. Mammalian Genome, 2011. <b>[Epub ahead of print]</b>; PMID[21437649].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21412695">In vitro Screening of Traditional South African Malaria Remedies against Trypanosoma brucei rhodesiense, Trypanosoma cruzi, Leishmania donovani, and Plasmodium falciparum.</a> Mokoka, T.A., S. Zimmermann, T. Julianti, Y. Hata, N. Moodley, M. Cal, M. Adams, M. Kaiser, R. Brun, N. Koorbanally, and M. Hamburger. Planta Medica, 2011. <b>[Epub ahead of print]</b>; PMID[21412695].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21428713">Antiplasmodial and cytotoxic activity of coumarin derivatives from dried roots of Angelica gigas Nakai in vitro.</a> Moon, H.I., J.H. Lee, Y.C. Lee, and K.S. Kim. Immunopharmacology and Immunotoxicology, 2011. <b>[Epub ahead of print]</b>; PMID[21428713].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21428453">Identification of Novel Malarial Cysteine Protease Inhibitors Using Structure-Based Virtual Screening of a Focused Cysteine Protease Inhibitor Library.</a> Shah, F., P. Mukherjee, J. Gut, J. Legac, P.J. Rosenthal, B.L. Tekwani, and M.A. Avery. Journal of Chemical Information and Modeling, 2011. <b>[Epub ahead of print]</b>; PMID[21428453].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21412700">Antiplasmodial and Antitrypanosomal Activity of Tanshinone-Type Diterpenoids from Salvia miltiorrhiza.</a> Slusarczyk, S., S. Zimmermann, M. Kaiser, A. Matkowski, M. Hamburger, and M. Adams. Planta Medica, 2011. <b>[Epub ahead of print]</b>; PMID[21412700].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21366301">Synthesis of new (-)-bestatin-based inhibitor libraries reveals a novel binding mode in the s1 pocket of the essential malaria m1 metalloaminopeptidase.</a> Velmourougane, G., M.B. Harbut, S. Dalal, S. McGowan, C.A. Oellig, N. Meinhardt, J.C. Whisstock, M. Klemba, and D.C. Greenbaum. Journal of Medicinal Chemistry, 2011. 54(6): p. 1655-1666; PMID[21366301].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0318-033111.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287927200014">Phytochemical characterization of the extracts of Aframomum danielli flower, leaf, stem and root.</a> Afolabi, M.O., G.O. Adegoke, and F.M. Mathooko. African Journal of Agricultural Research, 2011. 6(1): p. 97-101; ISI[000287927200014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288096600014">Molecular modeling and antimycobacterial studies of Mannich bases: 5-hydroxy-2-methyl-4H-pyran-4-ones.</a> Berk, B., D. Us, S. Oktem, Z.T. Kocagoz, B. Caglayan, I.A. Kurnaz, and D.D. Erol. Turkish Journal of Chemistry, 2011. 35(2): p. 317-330; ISI[000288096600014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288449500011">Solanum melongena: A potential source of antifungal agent. Das, J., J.P. Lahan, and R.B. Srivastava.</a> Indian Journal of Microbiology, 2010. 50: p. S62-S69; ISI[000288449500011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287279700007">Isolation and characterization of novel protein with anti-fungal and anti-inflammatory properties from Aloe vera leaf gel.</a> Das, S., B. Mishra, K. Gill, M.S. Ashraf, A.K. Singh, M. Sinha, S. Sharma, I. Xess, K. Dalal, T.P. Singh, and S. Dey. International Journal of Biological Macromolecules, 2011. 48(1): p. 38-43; ISI[000287279700007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287993800012">Antimicrobial and antioxidant activities of solvent extracts and the essential oil composition of Laurencia obtusa and Laurencia obtusa var. pyramidata.</a> Demirel, Z., F.F. Yilmaz-Koz, N.U. Karabay-Yavasoglu, G. Ozdemir, and A. Sukatar. Romanian Biotechnological Letters, 2011. 16(1): p. 5927-5936; ISI[000287993800012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287931700014">Antifungal and antibacterial effects of some acrocarpic mosses.</a> Elibol, B., T. Ezer, R. Kara, G.Y. Celik, and E. Colak. African Journal of Biotechnology, 2011. 10(6): p. 986-989; ISI[000287931700014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287637300021">Comazaphilones A-F, Azaphilone Derivatives from the Marine Sediment-Derived Fungus Penicillium commune QSD-17.</a> Gao, S.S., X.M. Li, Y. Zhang, C.S. Li, C.M. Cui, and B.G. Wang. Journal of Natural Products, 2011. 74(2): p. 256-261; ISI[000287637300021].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288270500003">Metal-containing polyurethanes from tetradentate Schiff bases: synthesis, characterization, and biocidal activities.</a> Hasnain, S., M. Zulfequar, and N. Nishat. Journal of Coordination Chemistry, 2011. 64(6): p. 952-964; ISI[000288270500003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286793200034">Chemical Composition and Antimicrobial Activity of the Essential Oil of the Leaves of Feronia elephantum (Rutaceae) from North West Karnataka.</a> Joshi, R.K., V.M. Badakar, S.D. Kholkute, and N. Khatib. Natural Product Communications, 2011. 6(1): p. 141-143; ISI[000286793200034].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287992800001">Synthesis and antimicrobial activity of some new quinazolin-4(3H)-one derivatives.</a> Kadi, A.A. Journal of Saudi Chemical Society, 2011. 15(2): p. 95-100; ISI[000287992800001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286972200056">Synthesis and Antimicrobial Activity of Some Aldehyde Derivatives of 3-Acetylchromen-2-one.</a> Lakshminarayanan, B., V. Rajamanickam, T. Subburaju, L.A.P. Rajkumar, and H. Revathi. E-Journal of Chemistry, 2010. 7: p. S400-S404; ISI[000286972200056].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287941100006">Antibacterial and Antifungal Activity of different Lichens Extracts and Lichen Acid.</a> Marijana, K. and R. Branislav. Research Journal of Biotechnology, 2011. 6(1): p. 23-26; ISI[000287941100006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288232000014">Synthesis, characterization and antimicrobial studies of Co-II, Ni-II, Cu-II and Zn-II complexes involving a bidentate Schiff base ligand.</a> Nair, M.S. and D. Arish. Journal of the Indian Chemical Society, 2011. 88(2): p. 265-270; ISI[000288232000014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288090700032">Biological Screening of Zizyphus Oxyphylla Edgew Stem.</a> Nisar, M., W.A. Kaleem, M. Qayum, I.K. Marwat, M. Zia-ul-Haq, I. Ali, and M.I. Choudhary. Pakistan Journal of Botany, 2011. 43(1): p. 311-317; ISI[000288090700032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288437300007">Synthesis and Antifungal Activity of 3-Aryl-4,4(5H)-dicarbonitrile-5-phenylisoxazolines.</a> Nosachev, S.B., O.V. Degtyarev, V.V. Zhizhikina, and A.G. Tyrkov. Pharmaceutical Chemistry Journal, 2010. 44(9): p. 493-494; ISI[000288437300007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287028500012">Badami. Spectroscopic, in vitro antibacterial, and antifungal studies of Co(II), Ni(II), and Cu(II) complexes with 4-chloro-3-coumarinaldehyde Schiff bases.</a> Patil, S.A., S.N. Unki, A.D. Kulkarni, V.H. Naik, U. Kamble, and P.S. Journal of Coordination Chemistry, 2011. 64(2): p. 323-336; ISI[000287028500012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287169400025">Synthesis and Antimicrobial Activity of Novel 2-(aryl)-3 5-(2-oxo-2H-3-chromenyl)-1,3-oxazol-2-yl -1,3-thiazolan-4-one s.</a> Reddy, C.S., M.V. Devi, G.R. Kumar, L.S. Rao, and A. Nagaraj. Journal of Heterocyclic Chemistry, 2011. 48(1): p. 176-182; ISI[000287169400025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287745400054">Enaminones as Building Blocks for the Synthesis of Substituted Pyrazoles with Antitumor and Antimicrobial Activities.</a> Riyadh, S.M. Molecules, 2011. 16(2): p. 1834-1853; ISI[000287745400054].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288232000019">Synthesis of some new alpha-oxo-N-phenyl-(4-fluoroaryl)-2-azetidinones by conventional and microwave methods and their spermicidal, antimicrobial activities.</a> Sharma, S., H. Madan, and A. Sharma. Journal of the Indian Chemical Society, 2011. 88(2): p. 283-288; ISI[000288232000019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286973000035">Synthesis, Spectroscopic Characterization and Preliminary Antimicrobial Studies of Mn(II) and Cu(II) Complexes of two Thiolates; S,S &#39;-(2,6-Diaminopyridine-3,5-diyl) Dibenzenecarbothioate (DBCT) and S-Benzyl Benzenecarbothioate (BBCT).</a> Ukoha, P.O., C.U. Alioke, N.L. Obasi, and K.F. Chah. E-Journal of Chemistry, 2011. 8(1): p. 231-239; ISI[000286973000035].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288096600015">Synthesis and in vitro antimycobacterial activities of novel 6-substituted-3(2H)-pyridazinone-2-acetyl-2-(substituted/nonsubstituted acetophenone) hydrazone.</a> Utku, S., M. Gokce, G. Aslan, G. Bayram, M. Ulger, G. Emekdas, and M.F. Sanhin. Turkish Journal of Chemistry, 2011. 35(2): p. 331-339; ISI[000288096600015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286965400062">Cytotoxicity decreasing effect and antimycobacterial activity of chitosan conjugated with antituberculotic drugs.</a> Vavrikova, E., J. Mandikova, F. Trejtnar, K. Horvati, S. Bosze, J. Stolarikova, and J. Vinsova. Carbohydrate Polymers, 2011. 83(4): p. 1901-1907; ISI[000286965400062].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288437700005">Synthesis and Antifungal Activity of Some Carbamate Containing Azaheterocycles.</a> Velikorodov, A.V., V.B. Kovalev, O.V. Degtyarev, and O.L. Titova. Pharmaceutical Chemistry Journal, 2011. 44(10): p. 536-539; ISI[000288437700005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>

    <p> </p>

    <p class="NoSpacing">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288306400005">Bioactive metabolites from Stenocarpella maydis, a stalk and ear rot pathogen of maize.</a> Wicklow, D.T., K.D. Rogers, P.F. Dowd, and J.B. Gloer. Fungal Biology, 2011. 115(2): p. 133-142; ISI[000288306400005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0318-033111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
