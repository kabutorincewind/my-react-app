

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-351.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="68oNShCI8NdriNwcHnC3SJXzBfonmG67+ROWhecyaHvD9kQx+1K7ZcKhLJMlIANUznref0dFKdT1cX2udB3yVCvs3qiYDrg0GfFqNjWTcEamHlt4pwCJ8yepj/o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9874DAD1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-351-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49045   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Novel Cyclopropyl Nucleosides, Phosphonate Nucleosides and Phosphonic Acid Nucleosides</p>

    <p>          Hyun, Oh C and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16795106&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16795106&amp;dopt=abstract</a> </p><br />

    <p>2.     49046   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          2&#39;-deoxy&#39;4&#39;-C-ethynyl-2-fluoroadenosine, a nucleoside reverse transcriptase inhibitor, is highly potent against all human immunodeficiency viruses type 1 and has low toxicity</p>

    <p>          Ohrui, H</p>

    <p>          Chem Rec <b>2006</b>.  6(3): 133-143</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16795005&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16795005&amp;dopt=abstract</a> </p><br />

    <p>3.     49047   HIV-LS-351; SCIFINDER-HIV-6/20/2006</p>

    <p class="memofmt1-2">          Anti-V3 humanized antibody KD-247 effectively suppresses ex vivo generation of human immunodeficiency virus type 1 and affords sterile protection of monkeys against a heterologous simian/human immunodeficiency virus infection</p>

    <p>          Eda, Yasuyuki, Murakami, Toshio, Ami, Yasushi, Nakasone, Tadashi, Takizawa, Mari, Someya, Kenji, Kaizu, Masahiko, Izumi, Yasuyuki, Yoshino, Naoto, Matsushita, Shuzo, Higuchi, Hirofumi, Matsui, Hajime, Shinohara, Katsuaki, Takeuchi, Hiroaki, Koyanagi, Yoshio, Yamamoto, Naoki, and Honda, Mitsuo</p>

    <p>          Journal of Virology <b>2006</b>.  80(11): 5563-5570</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     49048   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Aminopyrimidinimino isatin analogues: design and synthesis of novel non- nucleoside HIV-1 reverse transcriptase inhibitors with broad-spectrum anti-microbial properties</p>

    <p>          Sriram, D, Bal, TR, and Yogeeswari, P</p>

    <p>          Med Chem <b>2005</b>.  1(3): 277-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787323&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787323&amp;dopt=abstract</a> </p><br />

    <p>5.     49049   HIV-LS-351; SCIFINDER-HIV-6/20/2006</p>

    <p class="memofmt1-2">          Amino acid derivatives. Part I. Synthesis, antiviral and antitumor evaluation of new a-amino acid esters bearing coumarin side chain</p>

    <p>          Al-Masoudi, Najim A, Al-Masoudi, Iman A, Ali, Ibrahim AI, Al-Soud, Yaseen A, Saeed, Bahjat, and La Colla, Paolo</p>

    <p>          Acta Pharmaceutica (Zagreb, Croatia) <b>2006</b>.  56(2): 175-188</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     49050   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Novel Activities of Cyclophilin A and Cyclosporin A during HIV-1 Infection of Primary Lymphocytes and Macrophages</p>

    <p>          Saini, M and Potash, MJ</p>

    <p>          J Immunol <b>2006</b>.  177(1): 443-449</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16785541&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16785541&amp;dopt=abstract</a> </p><br />

    <p>7.     49051   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluations of novel quinolones as HIV-1 non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Ellis, D, Kuhen, KL, Anaclerio, B, Wu, B, Wolff, K, Yin, H, Bursulaya, B, Caldwell, J, Karanewsky, D, and He, Y</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16782337&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16782337&amp;dopt=abstract</a> </p><br />

    <p>8.     49052   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluations of sulfanyltriazoles as novel HIV-1 non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Wang, Z, Wu, B, Kuhen, KL, Bursulaya, B, Nguyen, TN, Nguyen, DG, and He, Y</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781149&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781149&amp;dopt=abstract</a> </p><br />

    <p>9.     49053   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          An approach to the stereo-controlled synthesis of polycyclic derivatives of l-4-thiazolidinecarboxylic acid active against HIV-1 integrase</p>

    <p>          Aiello, F, Brizzi, A, De, Grazia O, Garofalo, A, Grande, F, Sinicropi, MS, Dayam, R, and Neamati, N</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781021&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781021&amp;dopt=abstract</a> </p><br />

    <p>10.   49054   HIV-LS-351; SCIFINDER-HIV-6/20/2006</p>

    <p class="memofmt1-2">          Preparation of thiazole derivatives as inhibitors of HIV-1 capsid formation</p>

    <p>          Summers, Michael F, Agarwal, Atul, Chen, Xi, and Deshpande, Milind F</p>

    <p>          PATENT:  US <b>2006100232</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005-9293  PP: 38 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>11.   49055   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          An anti-CCR5 monoclonal antibody and small molecule CCR5 antagonists synergize by inhibiting different stages of human immunodeficiency virus type 1 entry</p>

    <p>          Safarian, D, Carnec, X, Tsamis, F, Kajumo, F, and Dragic, T</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16777164&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16777164&amp;dopt=abstract</a> </p><br />

    <p>12.   49056   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          A novel potent non-nucleoside reverse transcriptase inhibitor acylthiocarbamate derivative with extensive intramolecular pi-pi interactions</p>

    <p>          Mugnoli, A, Borassi, A, Spallarossa, A, and Cesarini, S</p>

    <p>          Acta Crystallogr C <b>2006</b>.  62(Pt 6): o315-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16763314&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16763314&amp;dopt=abstract</a> </p><br />

    <p>13.   49057   HIV-LS-351; SCIFINDER-HIV-6/20/2006</p>

    <p class="memofmt1-2">          Synthesis, antiviral and antibacterial activities of isatin Mannich bases</p>

    <p>          Sriram, Dharmarajan, Bal, Tanushree Ratan, and Yogeeswari, Perumal</p>

    <p>          Medicinal Chemistry Research <b>2006</b>.  14(4): 211-228</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   49058   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of purine derivatives incorporating metal chelating ligands as HIV integrase inhibitors</p>

    <p>          Li, X and Vince, R</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16753300&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16753300&amp;dopt=abstract</a> </p><br />

    <p>15.   49059   HIV-LS-351; SCIFINDER-HIV-6/20/2006</p>

    <p class="memofmt1-2">          Preparation of HIV inhibiting 5-heterocyclyl pyrimidines</p>

    <p>          Guillemont, Jerome Emile Georges, Heeres, Jan, and Lewi, Paulus Joannes</p>

    <p>          PATENT:  WO <b>2006035067</b>  ISSUE DATE:  20060406</p>

    <p>          APPLICATION: 2005  PP: 67 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   49060   HIV-LS-351; PUBMED-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Nucleotide analogues with immunobiological properties: 9-[2-Hydroxy-3-(phosphonomethoxy)propyl]-adenine (HPMPA), -2,6-diaminopurine (HPMPDAP), and their N(6)-substituted derivatives</p>

    <p>          Potmesil, P, Krecmerova, M, Kmonickova, E, Holy, A, and Zidek, Z</p>

    <p>          Eur J Pharmacol <b>2006</b>.  540(1-3):  191-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16733050&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16733050&amp;dopt=abstract</a> </p><br />

    <p>17.   49061   HIV-LS-351; WOS-HIV-6/26/2006</p>

    <p class="memofmt1-2">          5-hydroxy-6-quinaldic acid as a novel molecular scaffold for HIV-1 integrase inhibitors</p>

    <p>          Polanski, J, Niedbala, H, Musiol, R, Podeszwa, B, Tabak, D, Palka, A, Mencel, A, Finster, J, Mouscadet, JF, and Le Bret, M</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY: Lett. Drug Des. Discov <b>2006</b>.  3(3): 175-178, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238048100006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238048100006</a> </p><br />

    <p>18.   49062   HIV-LS-351; WOS-HIV-6/26/2006</p>

    <p class="memofmt1-2">          Stavudine-loaded mannosylated liposomes: in-vitro anti HIV-I activity, tissue distribution and pharmacokinetics</p>

    <p>          Garg, M, Asthana, A, Agashe, HB, Agrawal, GP, and Jain, NK</p>

    <p>          JOURNAL OF PHARMACY AND PHARMACOLOGY: J. Pharm. Pharmacol <b>2006</b>.  58(5): 605-616, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237768500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237768500005</a> </p><br />

    <p>19.   49063   HIV-LS-351; WOS-HIV-6/18/2006</p>

    <p class="memofmt1-2">          Synthesis of new 1H-1,2,4-triazolylcoumarins and their antitumor and anti-HIV activities</p>

    <p>          Al-Soud, YA, Al-Masoudi, IA, Saeed, B, Beifuss, U, and Al-Masoudi, NA</p>

    <p>          KHIMIYA GETEROTSIKLICHESKIKH SOEDINENII <b>2006</b>.(5): 669-676, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237909400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237909400004</a> </p><br />

    <p>20.   49064   HIV-LS-351; WOS-HIV-6/18/2006</p>

    <p class="memofmt1-2">          Identification of a new class of low molecular weight antagonists against the chemokine receptor CXCR4 having the dipicolylamine-zinc(II) complex structure</p>

    <p>          Tamamura, H, Ojida, A, Ogawa, T, Tsutsumi, H, Masuno, H, Nakashima, H, Yamamoto, N, Hamachi, I, and Fujii, N</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(11): 3412-3415, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237832400043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237832400043</a> </p><br />

    <p>21.   49065   HIV-LS-351; WOS-HIV-6/18/2006</p>

    <p class="memofmt1-2">          HIV-1 adapts to a retrocyclin with cationic amino acid substitutions that reduce fusion efficiency of grp41</p>

    <p>          Cole, AL, Yang, OO, Warren, AD, Waring, AJ, Lehrer, RI, and Cole, AM</p>

    <p>          JOURNAL OF IMMUNOLOGY <b>2006</b>.  176(11): 6900-6905, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237754200057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237754200057</a> </p><br />

    <p>22.   49066   HIV-LS-351; WOS-HIV-6/18/2006</p>

    <p class="memofmt1-2">          Efficient inhibition of HIV-1 replication in human immature monocyte-derived dendritic cells by purified anti-HIV-1 IgG without induction of maturation</p>

    <p>          Holl, V, Peressin, M, Schmidt, S, Decoville, T, Zolla-Pazner, S, Aubertin, AM, and Moog, C</p>

    <p>          BLOOD <b>2006</b>.  107(11): 4466-4474, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237877300043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237877300043</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
