

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-12-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cgFbGCsvhIbZvNXz3M+ax18gYdDm785SMtowleBPacwQ3jMs1QPXXwDkKRC5UmT1uh7LeBClEyUr2FYDu/kgqtGE48q1dLF+Q5q/ngJR9XZf1BGJZb4UNSqPez4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="10D90308" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: November 23 - December 6, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22820802">Characterization of Peripheral and Mucosal Immune Responses in Rhesus macaques on Long-term Tenofovir and Emtricitabine Combination Antiretroviral Therapy.</a> Jasny, E., S. Geer, I. Frank, P. Vagenas, M. Aravantinou, A.M. Salazar, J.D. Lifson, M. Piatak, Jr., A. Gettie, J.L. Blanchard, and M. Robbiani. Journal of Acquired Immune Deficiency Syndromes (1999), 2012. 61(4): p. 425-435. PMID[22820802].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23151033">Design of a Novel Cyclotide-based CXCR4 Antagonist with anti-Human Immunodeficiency Virus (HIV)-1 Activity.</a> Aboye, T.L., H. Ha, S. Majumder, F. Christ, Z. Debyser, A. Shekhtman, N. Neamati, and J.A. Camarero. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print];</b> PMID[23151033].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23098609">Discovery of Novel 2-(3-(2-Chlorophenyl)pyrazin-2-ylthio)-N-arylacetamides as Potent HIV-1 Inhibitors Using a Structure-based Bioisosterism Approach.</a> Zhan, P., W. Chen, Z. Li, X. Li, X. Chen, Y. Tian, C. Pannecouque, E.D. Clercq, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2012. 20(23): p. 6795-6802. PMID[23098609].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23065154">Inhibitory Effect of HIV-Specific Neutralizing IgA on Mucosal Transmission of HIV in Humanized Mice.</a> Hur, E.M., S.N. Patel, S. Shimizu, D.S. Rao, P.N. Gnanapragasam, D.S. An, L. Yang, and D. Baltimore. Blood, 2012. 120(23): p. 4571-4582. PMID[23065154].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23176059">Microwave-assisted Combinatorial Synthesis of 2-Alkyl- 2-(N-arylsulfonylindol-3-yl)-3-N-acyl-5-aryl-1,3,4-oxadiazolines as anti-HIV-1 Agents.</a> Che, Z.P., N. Huang, X. Yu, L.M. Yang, J.Q. Ran, X. Zhi, H. Xu, and Y.T. Zheng. Combinatorial Chemistry &amp; High Throughput Screening, 2012.</p>

    <p class="plaintext"><b>[Epub ahead of print];</b>PMID[23176059].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23129652">Most Rhesus macaques Infected with the CCR5-Tropic SHIVAD8 Generate Cross-reactive Antibodies That Neutralize Multiple HIV-1 Strains.</a> Shingai, M., O.K. Donau, S.D. Schmidt, R. Gautam, R.J. Plishka, A. Buckler-White, R. Sadjadpour, W.R. Lee, C.C. Labranche, D.C. Montefiori, J.R. Mascola, Y. Nishimura, and M.A. Martin. Proceedings of the National Academy of Sciences of the United States of America, 2012. 109(48): p. 19769-19774. PMID[23129652].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23187285">New Furanones from the Plant Endophytic Fungus Pestalotiopsis besseyi.</a> Liu, H., S. Liu, L. Guo, Y. Zhang, L. Cui, and G. Ding. Molecules, 2012. 17(12): p. 14015-14021. PMID[23187285].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23099090">POMA Analyses as New Efficient Bioinformatics&#39; Platform to Predict and Optimise Bioactivity of Synthesized 3a,4-Dihydro-3h-indeno[1,2-C]pyrazole-2-carboxamide/Carbothioamide Analogues.</a> Ahsan, M.J., J. Govindasamy, H. Khalilullah, G. Mohan, J.P. Stables, C. Pannecouque, and E.D. Clercq. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7029-7035. PMID[23099090].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23137340">Rational Design of Potent Non-nucleoside Inhibitors of HIV-1 Reverse Transcriptase.</a> Chong, P., P. Sebahar, M. Youngman, D. Garrido, H. Zhang, E.L. Stewart, R.T. Nolte, L. Wang, R.G. Ferris, M. Edelstein, K. Weaver, A. Mathis, and A. Peat. Journal of Medicinal Chemistry, 2012.</p>

    <p class="plaintext"><b>[Epub ahead of print];</b>PMID[23137340].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23180603">Small-molecule APOBEC3G DNA Cytosine Deaminase Inhibitors Based on a 4-Amino-1,2,4-triazole-3-thiol Scaffold.</a> Olson, M.E., M. Li, R.S. Harris, and D.A. Harki. ChemMedChem, 2012.</p>

    <p class="plaintext"><b>[Epub ahead of print];</b>PMID[23180603].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23099098">Structural Modifications of 5,6-dihydroxypyrimidines with anti-HIV Activity.</a> Guo, D.L., X.J. Zhang, R.R. Wang, Y. Zhou, Z. Li, J.Y. Xu, K.X. Chen, Y.T. Zheng, and H. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7114-7118. PMID[23099098].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23084898">Structure-based Bioisosterism Design, Synthesis and Biological Evaluation of Novel 1,2,4-Triazin-6-ylthioacetamides as Potent HIV-1 NNTRIs.</a> Zhan, P., X. Li, Z. Li, X. Chen, Y. Tian, W. Chen, X. Liu, C. Pannecouque, and E.D. Clercq. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7155-7162. PMID[23084898].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22958368">Synthesis and anti-HIV Properties of New Carbamate Prodrugs of AZT.</a> Solyev, P.N., A.V. Shipitsin, I.L. Karpenko, D.N. Nosik, L.B. Kalnina, S.N. Kochetkov, M.K. Kukhanova, and M.V. Jasko. Chemical Biology &amp; Drug Design, 2012. 80(6): p. 947-952. PMID[22958368].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23018557">Synthesis and Evaluation of Novel 4-Substituted styryl quinazolines as Potential Antimicrobial Agents.</a> Modh, R.P., A.C. Patel, D.H. Mahajan, C. Pannecouque, E. De Clercq, and K.H. Chikhalia. Archiv der Pharmazie, 2012. 345(12): p. 964-972. PMID[23018557].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23159806">Synthesis, Biological Evaluation and Molecular Modeling of 4,6-Diarylpyrimidines and Diarylbenzenes as Novel Non-nucleosides HIV-1 Reverse Transcriptase Inhibitors.</a> Ribone, S.R., V. Leen, M. Madrid, W. Dehaen, D. Daelemans, C. Pannecouque, and M.C. Brinon. European Journal of Medicinal Chemistry, 2012. 58: p. 485-492. PMID[23159806].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23084903">Synthesis, Evaluation and Structure-Activity Relationships of Triazine Dimers as Novel Antiviral Agents.</a> Venkatraj, M., K.K. Arien, J. Heeres, J. Joossens, J. Messagie, J. Michiels, P. Van der Veken, G. Vanham, P.J. Lewi, and K. Augustyns. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7174-7178. PMID[23084903].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/3489957">Synthesis, Evaluation of anti-HIV-1 and anti-HCV Activity of Novel 2&#39;,3&#39;-Dideoxy-2&#39;,2&#39;-difluoro-4&#39;-azanucleosides.</a> Martinez-Montero, S., S. Fernandez, Y.S. Sanghvi, E.A. Theodorakis, M.A. Detorio, T.R. McBrayer, T. Whitaker, R.F. Schinazi, V. Gotor, and M. Ferrero. Bioorganic &amp; Medicinal Chemistry, 2012. 20(23): p. 6885-6893. PMID[23085031].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23164656">Synthesis, Structure-Activity Relationships, and Docking Studies of N-Phenylarylformamide Derivatives (PAFAS) as Non-nucleoside HIV Reverse Transcriptase Inhibitors.</a> Ma, X.D., Q.Q. He, X. Zhang, S.Q. Yang, L.M. Yang, S.X. Gu, Y.T. Zheng, F.E. Chen, and H.F. Dai. European Journal of Medicinal Chemistry, 2012. 58: p. 504-512. PMID[23164656].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23188559">Two Retroviruses Packaged in One Cell Line Can Combined Inhibit the Replication of HIV-1 in TZM-Bl Cells</a>. Liang, Z., Z. Guo, X. Wang, X. Kong, and C. Liu. Virologica Sinica, 2012.</p>

    <p class="plaintext"><b>[Epub ahead of print];</b>PMID[23188559].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1123-120612.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309494100039">1-(2-Mercaptobenzenesulfonyl)-3-hydroxyguanidines - Novel Potent Antiproliferatives, Synthesis and in Vitro Biological Activity.</a> Brozewicz, K. and J. Slawinski. European Journal of Medicinal Chemistry, 2012. 55: p. 384-394. ISI[000309494100039].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309650400008">Anti HIV-1 Agents 6. Synthesis and anti-HIV-1 Activity of Indolyl Glyoxamides.</a> Wang, Y., N. Huang, X. Yu, L.M. Yang, X.Y. Zhi, Y.T. Zheng, and H. Xu. Medicinal Chemistry, 2012. 8(5): p. 831-833. ISI[000309650400008].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309472100293">Anti-HIV and Immune Modulating Activities of IND02</a>. Biedma, M.E., B. Connell, S. Schmidt, H. Lortat-Jacob, C. Moog, and E. Prakash. Retrovirology, 2012. 9(Suppl 2): p. 60. ISI[000309472100293].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308808700024">Arylazolyl(azinyl)Thioacetanilides. Part 10: Design, Synthesis and Biological Evaluation of Novel Substituted Imidazopyridinylthioacetanilides as Potent HIV-1 Inhibitors.</a> Li, X., P. Zhan, H. Liu, D.Y. Li, L. Wang, X.W. Chen, H.Q. Liu, C. Pannecouque, J. Balzarini, E. De Clercq, and X.Y. Liu. Bioorganic &amp; Medicinal Chemistry, 2012. 20(18): p. 5527-5536. ISI[000308808700024].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00031016320000">Assessing a Theoretical Risk of Dolutegravir-induced Developmental Immunotoxicity in Juvenile Rats.</a> Rhodes, M., S. Laffan, C. Genell, J. Gower, C. Maier, T. Fukushima, G. Nichols, and A.E. Bassiri. Toxicological Sciences, 2012. 130(1): p. 70-81. ISI[000310163200007].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310120700014">Carbamoyl Pyridone HIV-1 Integrase Inhibitors. 1. Molecular Design and Establishment of an Advanced Two-metal Binding Pharmacophore.</a> Kawasuji, T., B.A. Johns, H. Yoshida, T. Taishi, Y. Taoda, H. Murai, R. Kiyama, M. Fuji, T. Yoshinaga, T. Seki, M. Kobayashi, A. Sato, and T. Fujiwara. Journal of Medicinal Chemistry, 2012. 55(20): p. 8735-8744. ISI[000310120700014].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310356400045">Enhanced Recognition and Neutralization of HIV-1 by Antibody-derived CCR5-mimetic Peptide Variants.</a> Chiang, J.J., M.R. Gardner, T. Dorfman, H. Choe, and M. Farzan. Journal of Virology, 2012. 86(22): p. 12417-12421. ISI[000310356400045].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309472100275">Human Intestinal Beta Defensins Inhibit Viral Replication and are Diminished in Chronic Untreated HIV Infection.</a> Corleis, B., W.G. Gostic, J.A. Johnson, and D.S. Kwon. Retrovirology, 2012. 9(Suppl 2): p. 60. ISI[000309472100275].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308046400045">In vitro Evolution of an HIV Integrase Binding Protein from a Library of C-terminal Domain Gamma S-crystallin Variants.</a> Moody, I.S., S.C. Verde, C.M. Overstreet, W.E. Robinson, and G.A. Weiss. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(17): p. 5584-5589. ISI[000308046400045].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309777600001">A Laccase with Antiproliferative and HIV-I Reverse Transcriptase Inhibitory Activities from the Mycorrhizal Fungus Agaricus placomyces.</a> Sun, J., Q.J. Chen, Q.Q. Cao, Y.Y. Wu, L.J. Xu, M.J. Zhu, T.B. Ng, H.X. Wang, and G.Q. Zhang. Journal of Biomedicine and Biotechnology, 2012.</p>

    <p class="plaintext"><b>[Epub ahead of print];</b> ISI[000309777600001].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309654200059">The M-T Hook Structure Is Critical for Design of HIV-1 Fusion Inhibitors.</a> Chong, H.H., X. Yao, J.P. Sun, Z.L. Qiu, M. Zhang, S. Waltersperger, M.T. Wang, S. Cui, and Y.X. He. Journal of Biological Chemistry, 2012. 287(41): p. 34558-34568. ISI[000309654200059].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309629500011">New Aryl-1,3-thiazole-4-carbohydrazides, Their 1,3,4-oxadiazole-2-thione, 1,2,4-triazole, Isatin-3-ylidene and Carboxamide Derivatives. Synthesis and anti-HIV Activity.</a> Zia, M., T. Akhtar, S. Hameed, and N.A. Al-Masoudi. Zeitschrift Fur Naturforschung Section B, 2012. 67(7): p. 747-758. ISI[000309629500011].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309494100021">One Pot Efficient Diversity Oriented Synthesis of Polyfunctional Styryl Thiazolopyrimidines and Their Bio-evaluation as Antimalarial and anti-HIV Agents.</a> Fatima, S., A. Sharma, R. Saxena, R. Tripathi, S.K. Shukla, S.K. Pandey, and R.P. Tripathi. European Journal of Medicinal Chemistry, 2012. 55: p. 195-204. ISI[000309494100021].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310220000014">Pharmacodynamic and Antiretroviral Activities of Combination Nanoformulated Antiretrovirals in HIV-1-Infected Human Peripheral Blood Lymphocyte-reconstituted Mice.</a> Roy, U., J. McMillan, Y. Alnouti, N. Gautum, N. Smith, S. Balkundi, P. Dash, S. Gorantla, A. Martinez-Skinner, J. Meza, G. Kanmogne, S. Swindells, S.M. Cohen, R.L. Mosley, L. Poluektova, and H.E. Gendelman. Journal of Infectious Diseases, 2012. 206(10): p. 1577-1588. ISI[000310220000014].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309639500010">Purification and Characterization of anti-HIV-1 Protein from Canna indica L. Leaves.</a> Thepouyporn, A., C. Yoosook, W. Chuakul, K. Thirapanmethee, C. Napaswad, and C. Wiwat. Southeast Asian Journal of Tropical Medicine and Public Health, 2012. 43(5): p. 1153-1160. ISI[000309639500010].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310185900022">Structure-activity Relationship Study of Pyrimido 1,2-C 1,3 benzothiazin-6-imine Derivatives for Potent anti-HIV Agents.</a> Mizuhara, T., S. Oishi, H. Ohno, K. Shimura, M. Matsuoka, and N. Fujii. Bioorganic &amp; Medicinal Chemistry, 2012. 20(21): p. 6434-6441. ISI[000310185900022].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310470000012">Structures and Biological Activity of Cinnamoyl Derivatives of Coumarins and Dehydroacetic acid and Their Boron difluoride Complexes.</a> Tambov, K.V., I.V. Voevodina, A.V. Manaev, Y.A. Ivanenkov, N. Neamati, and V.F. Traven. Russian Chemical Bulletin, 2012. 61(1): p. 78-90. ISI[000310470000012].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308046400019">Synthesis and anti-HIV Activities of Symmetrical Dicarboxylate Esters of Dinucleoside Reverse Transcriptase Inhibitors.</a> Agarwal, H.K., K.W. Buckheit, R.W. Buckheit, and K. Parang. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(17): p. 5451-5454. ISI[000308046400019].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307980100005">Synthesis and anti-HIV Evaluation of New 2-Thioxoimidazolidin-4-ones and Their Arylidine (styryl) Derivatives.</a> Mahajan, D.H., K.H. Chikhalia, C. Pannecouque, and E. De Clercq. Pharmaceutical Chemistry Journal, 2012. 46(3): p. 165-170. ISI[000307980100005].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309650400001">Synthesis of 5-Methyl-1,3-oxathiolane-based Nucleoside Analogues as Potential Antiviral Agents.</a> Franchini, S., A. Tait, C. Sorbi, and L. Brasili. Medicinal Chemistry, 2012. 8(5): p. 769-778. ISI[000309650400001].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1123-120612.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121018&amp;CC=WO&amp;NR=2012140243A1&amp;KC=A1">Preparation of Phenylacetic Acid Derivatives as Inhibitors of Viral Replication.</a> Chasset, S., F. Chevreuil, B. Ledoussal, F. Le Strat, and R. Benarous. Patent. 2012. 2012-EP56851 2012140243: 252pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121025&amp;CC=US&amp;NR=2012270806A1&amp;KC=A1">Designer Cyclic Peptides-HIV Gp120 Antagonists and Their Applications.</a> Czyryca, P. Patent. 2012. 2012-452684 20120270806: 10pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121010&amp;CC=CN&amp;NR=102718696A&amp;KC=A">Method for Preparation of Baculiferin L and Its Analogue, Their Application for Treating AIDS.</a> Fan, A., W. Lin, Y. Jia, and M. Xu. Patent. 2012. 2011-10077956 102718696: 15pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121114&amp;CC=CN&amp;NR=102775375A&amp;KC=A">Chromone Compound, Preparation Method, Application, anti-AIDS Medicinal Composition and Medicinal Preparation Thereof.</a> Hu, Q., X. Gao, Y. Ye, Y. Shen, L. Su, and L. Yang. Patent. 2012. 2012-10291009 102775375: 12pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">44. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121010&amp;CC=CN&amp;NR=102718721A&amp;KC=A">Preparation of 4,4-Disubstituted-dihydro-2(1H)-quinazolinone or Thione-like Compounds as HIV Reverse Transcriptase Inhibitors for the Treatment of HIV.</a> Jiang, B., C. Zhang, J. Li, S. Teng, X. Du, D. Zhuang, L. Zhang, H. Jiang, X. Cao, W. Wang, and H. Chen. Patent. 2012. 2011-10078047 102718721: 41pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">45. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121108&amp;CC=WO&amp;NR=2012149906A1&amp;KC=A1">Modification of Nucleic Acids and Their Use as anti-HIV-1 Agents.</a> Liu, K., J. He, L. Xu, W. Chen, L. Cai, B. Zheng, and K. Wang. Patent. 2012. 2012-CN75063 2012149906: 61pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">46. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121018&amp;CC=WO&amp;NR=2012139519A1&amp;KC=A1">Preparation of Cyclopeptides for Treatment of HIV Infection.</a> Liu, K., Q. Jia, X. Jiang, S. Liu, L. Li, M. Cheng, L. Cai, B. Zheng, and K. Wang. Patent. 2012. 2012-CN73988 2012139519: 21pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">47. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121018&amp;CC=WO&amp;NR=2012142080A1&amp;KC=A1">Thioamide, Amidoxime and Amidrazone Derivatives as HIV Attachment Inhibitors.</a> Meanwell, N.A., T. Wang, Z. Zhang, L.G. Hamann, and J.F. Kadow. Patent. 2012. 2012-US33007 2012142080: 82pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">48. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121026&amp;CC=WO&amp;NR=2012145728A1&amp;KC=A1">Preparation of Benzothiazole Compounds as Antiviral Agents for the Treatment of HIV Infection.</a> Mitchell, M.L., P.A. Roethle, L. Xu, H. Yang, R. McFadden, and K. Babaoglu. Patent. 2012. 2012-US34593 2012145728: 207pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">49. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20121018&amp;CC=US&amp;NR=2012264820A1&amp;KC=A1">Synthetic Flavonoids and Pharmaceutical Compositions and Therapeutic Methods of Treatment of HIV Infection and Other Pathologies.</a> Redda, K., C.J. Mills, and N. Maleeva. Patent. 2012. 2007-968146 20120264820: 23pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1123-120612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
