

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-10-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xhiZVQU2LpGO8qnsHgF0KXaunFvVY2JXQELeG+M6LlF/WCg07nzAMpQ6ApjQQvxIY1zJsqbmDGf0FOJkUBLV1OKM4MpZ+6ocfpC7RADQRbHYS3anUdMvLXS900g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="38232370" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 14 - October 27, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19858258?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">An Albumin-conjugated Peptide Exhibits Potent Anti-HIV Activity and Long In Vivo Half-Life.</a>  Xie D, Yao C, Wang L, Min W, Xu J, Xiao J, Huang M, Chen B, Liu B, Li X, Jiang H.  Antimicrob Agents Chemother. 2009 Oct 26. [Epub ahead of print]PMID: 19858258</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19857522?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">Debio-025 inhibits HIV-1 by interfering with an early event in the replication cycle.</a>  Daelemans D, Dumont JM, Rosenwirth B, De Clercq E, Pannecouque C.  Antiviral Res. 2009 Oct 23. [Epub ahead of print]PMID: 19857522</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19856332?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=14">Synthesis of Novel Uracil Non-Nucleoside Derivatives as Potential Reverse Transcriptase Inhibitors of HIV-1.</a>  El-Brollosy NR, Al-Deeb OA, El-Emam AA, Pedersen EB, La Colla P, Collu G, Sanna G, Loddo R.  Arch Pharm (Weinheim). 2009 Oct 23. [Epub ahead of print]PMID: 19856332</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19837673?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=50">Mechanism of inhibition of HIV-1 reverse transcriptase by 4&#39;-ethynyl-2-fluoro-2&#39;-deoxyadenosine triphosphate, a translocation defective reverse transcriptase inhibitor.</a>  Michailidis E, Marchand B, Kodama EN, Singh K, Matsuoka M, Kirby KA, Ryan EM, Sawani AM, Nagy E, Ashida N, Mitsuya H, Parniak MA, Sarafianos SG.  J Biol Chem. 2009 Oct 16. [Epub ahead of print]PMID: 19837673</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19830655?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=59">Anti-HIV-1 and Anti-Inflammatory Lupanes from the Leaves, Twigs, and Resin of Garcinia hanburyi.</a> Reutrakul V, Anantachoke N, Pohmakotr M, Jaipetch T, Yoosook C, Kasisit J, Napaswa C, Panthong A, Santisuk T, Prabpai S, Kongsaeree P, Tuchinda P.  Planta Med. 2009 Oct 14. [Epub ahead of print]PMID: 19830655</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19775161?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=66">Crystallographic study of a novel subnanomolar inhibitor provides insight on the binding interactions of alkenyldiarylmethanes with human immunodeficiency virus-1 reverse transcriptase.</a>  Cullen MD, Ho WC, Bauman JD, Das K, Arnold E, Hartman TL, Watson KM, Buckheit RW, Pannecouque C, De Clercq E, Cushman M.  J Med Chem. 2009 Oct 22;52(20):6467-73.PMID: 19775161</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19748778?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=73">Pyrazole NNRTIs 4: selection of UK-453,061 (lersivirine) as a development candidate.</a>  Mowbray CE, Burt C, Corbau R, Gayton S, Hawes M, Perros M, Tran I, Price DA, Quinton FJ, Selby MD, Stupple PA, Webster R, Wood A.  Bioorg Med Chem Lett. 2009 Oct 15;19(20):5857-60. Epub 2009 Aug 27.PMID: 19748778</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="title">8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270561300021">A dynamic target-based pharmacophoric model mapping the CD4 binding site on HIV-1 gp120 to identify new inhibitors of gp120-CD4 protein-protein interactions.</a>  Caporuscio, F., A. Tafi, E. Gonzalez, F. Manetti, J.A. Este, and M. Botta,  Bioorganic &amp; Medicinal Chemistry Letters, 2009; 19(21):6087-6091.</p>

    <p class="title">9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270549400004">Synthesis, Antiinflammatory and HIV-1 Integrase Inhibitory Activities of 1,2-Bis[5-thiazolyl]ethane-1,2-dione Derivatives.</a>  Franklin, P.X., S. Yerande, H.M. Thakar, G.S. Inamdar, R.S. Giri, H. Padh, V. Sudarsanam, and K.K. Vasu. Indian Journal of Pharmaceutical Sciences, 2009;71(3): 259-263</p>

    <p class="title">10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270561300008">Evaluation of triazolamers as active site inhibitors of HIV-1 protease.</a>  Jochim, A.L., S.E. Miller, N.G. Angelo, and P.S. Arora BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(21):6023-6026;</p>

    <p class="title">11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270649500008">The capsid protein of human immunodeficiency virus: designing inhibitors of capsid assembly.</a> Neira, JL.   FEBS Journal, 2009; 276(21):6110-6117.</p>

    <p>&nbsp;</p>   
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
