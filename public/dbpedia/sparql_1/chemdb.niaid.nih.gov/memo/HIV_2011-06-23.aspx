

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-06-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="C/923yPPdt20WL0Z3K0Xv8Oc9f51OE5uWDLa6t8huMUnw6JITNBpMaev3tEZe3KDuWF70p8Iznu0D0n/tQzzyUqIEhwhtZMx6Jp2FrULXh/uGbyo+tOD+SBSLuo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="550ABFDF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  June 10, 2011- June 23, 2011</h1>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21669397">The TRIM Family Protein KAP1 Inhibits HIV-1 Integration.</a> Allouch, A., C. Di Primio, E. Alpi, M. Lusic, D. Arosio, M. Giacca, and A. Cereseto. Cell Host &amp; Microbe, 2011. 9(6): p. 484-495; PMID[21669397].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21670103">Activation of CB2 Cannabinoid Receptors Inhibits HIV-1 Envelope Glycoprotein gp120-Induced Synapse Loss.</a> Kim, H.J., A.H. Shin, and S.A. Thayer. Molecular Pharmacology, 2011. <b>[Epub ahead of print]</b>; PMID[21670103].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21666897">Synthesis and Antiviral Activity of Highly Water-soluble Polycarboxylic Derivatives of [70]Fullerene.</a> Kornev, A.B., A.S. Peregudov, V.M. Martynenko, J. Balzarini, B. Hoorelbeke, and P.A. Troshin. Chemical Communications (Cambridge, England), 2011. <b>[Epub ahead of print]</b>; PMID[21666897].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21688394">Approach toward the Design and Synthesis of Betulinic Acid-Polyphenol Conjugates as Inhibitors of the HIV-1 gp41 Fusion Core Formation.</a> Liu, Y., Z. Ke, K.Y. Wu, S. Liu, W.H. Chen, S. Jiang, and Z.H. Jiang, An Amphiphilic Conjugate ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21688394].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21669868">Protein Kinase C (PKC) Theta ({theta}) is a Specific Target for the Inhibition of the Human Immunodeficiency Virus Type 1 (HIV-1) Replication in CD4+ T Lymphocytes.</a> Lopez-Huertas, M.R., E. Mateos, G. Diaz-Gil, F. Gomez-Esquer, M. Sanchez Del Cojo, J. Alcami, and M. Coiras. The Journal of Biological Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21669868].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21690094">A Novel Chimeric Protein-based HIV-1 Fusion Inhibitor Targeting GP41 with High Potency and Stability.</a> Pan, C., L. Cai, H. Lu, L. Lu, and S. Jiang. The Journal of Biological Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21690094].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21688836">Synthesis of Aminoglycoside-3&#39;-Conjugates of 2&#39;-O-Methyl Oligoribonucleotides and Their Invasion to a 19F labelled HIV-1 TAR Model.</a> Virta, P. and A. Kiviniemi. Bioconjugate Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21688836].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21682887">Differential Effect of CLK SR Kinases on HIV-1 Gene Expression: Potential Novel Targets for Therapy.</a> Wong, R., A. Balachandran, A.Y. Mao, W. Dobson, S. Gray-Owen, and A. Cochrane. Retrovirology, 2011. 8(1): p. 47; PMID[21682887].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21690365">Evolution of Cyclic Peptide Protease Inhibitors.</a> Young, T.S., D.D. Young, I. Ahmad, J.M. Louis, S.J. Benkovic, and P.G. Schultz. Proceedings of the National Academy of Sciences of the United States of America, 2011. <b>[Epub ahead of print]</b>; PMID[21690365].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500023">Synthesis and Anti-HIV Activity of D-peptide Analogs as HIV Fusion Inhibitors.</a> Baron, A., C. Kreuz, G. Gosselin, F. Lamaty, J. Martinez, D. Surleraux, C. Pierra, and P. Clayette. Antiviral Research, 2011. 90(2): p. A28-A29; ISI[000291182500023].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500024">Activity of Novel Cyclophilin Inhibitors Based on the Polyketide Sanglifehrin A, against HIV.</a> Bobardt, M., S. Moss, U. Chatterji, M. Nur-E-Alam, T. Warneck, B. Wilkinson, P. Gallay, and M. Gregory. Antiviral Research, 2011. 90(2): p. A29-A29; ISI[000291182500024].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291140100002">Carbonylation of Functionalized Diamine Diols to Cyclic Ureas: Application to Derivatives of DMP 450.</a> Darko, A.K., F. Curran, C. Copin, and L. McElwee-White. Tetrahedron, 2011. 67(22): p. 3976-3983; ISI[000291140100002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291145900003">Inhibition of Multi-drug Resistant HIV-1 Reverse Transcriptase by Nucleoside beta-Triphosphates.</a> Dash, C., Y. Ahmadibeni, M. Hanley, J. Pandhare, M. Gotte, S. Le Grice, and K. Parang, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(12): p. 3519-3522; ISI[000291145900003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500032">Identification of HIV-1 Reverse Transcriptase Dual Inhibitors by a Combined Shape-, 2D-Fingerprint- and Pharmacophore-based Virtual Screening Approach.</a> Distinto, S., F. Esposito, J. Kirchmair, C. Cardia, E. Maccioni, S. Alcaro, L. Zinzula, and E. Tramontano. Antiviral Research, 2011. 90(2): p. A31-A31; ISI[000291182500032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291324400005">Use of Minocycline in Viral Infections.</a> Dutta, K. and A. Basu. Indian Journal of Medical Research, 2011. 133(5): p. 467-470; ISI[000291324400005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500035">Design and Synthesis of New Isatin Derivatives as HIV-1 Reverse Transcriptase Associated Ribonuclease H Inhibitors.</a>Esposito, F., R. Meleddu, M. Sanna, S. Distinto, A. Corona, V. Cannas, E. Tramontano, and M. Cardia. Antiviral Research, 2011. 90(2): p. A32-A32; ISI[000291182500035].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291145900046">Design and Synthesis of Potent HIV-1 Protease Inhibitors Incorporating Hydroxyprolinamides as Novel P2 Ligands.</a> Gao, B.L., C. Zhang, Y. Yin, L. Tang, and Z. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(12): p. 3730-3733; ISI[000291145900046].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500036.">In silico Screening of Compounds Targeting Human Cyclin T1 and In vitro Evaluation of their Anti-HIV-1 Activity.</a> Hamasaki, T., M. Okamoto, and M. Baba. Antiviral Research, 2011. 90(2): p. A33-A33; ISI[000291182500036].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500086">The Design and Synthesis of Pyrolle-Carbaldehydes as HIV-1 Integrase Strand-Transfer Inhibitors.</a> Hewer, R., T. Traut, B. Williams, and J. Coates. Antiviral Research, 2011. 90(2): p. A50-A50; ISI[000291182500086].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500037">Novel 2-Styryl-8-hydroxyquinolines (8SQs) Derivatives with Anti-HIV-1 Activity Targeting Viral Integrase and Protease.</a> Hinkov, A.V., K. Stanoeva, S. Raleva, V. Atanasov, P. Genova-Kalou, and R. Argirova, Antiviral Research, 2011. 90(2): p. A33-A33; ISI[000291182500037].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291118600059">Synthesis and Antiproliferative Evaluation of 23-Hydroxybetulinic acid Derivatives.</a> Lan, P., J. Wang, D. Zhang, C. Shu, H. Cao, P. Sun, X. Wu, W. Ye, and W. Chen. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2490-2502; ISI[000291118600059].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291084600055">Study of the Efficacy of Combination Therapy of SiRNAs in HepG2.2.15  Cells.</a> Li, G.Q., D. Yu, J. Lu, S. Chen, J. Zhao, and Y. Wang. Hepato-Gastroenterology, 2011. 58(106): p. 570-574; ISI[000291084600055].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291212900006">Structural Basis for Drug and Substrate Specificity Exhibited by FIV Encoding a Chimeric FIV/HIV Protease.</a> Lin, Y.C., A. Perryman, A. Olson, B. Torbett, J. Elder, and C. Stout. Acta Crystallographica Section D-Biological Crystallography, 2011. 67: p. 540-548; ISI[000291212900006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291171000042">Synthesis and Some Properties of 4 &#39;-Phenyl-5 &#39;-Norcarbocyclic Adenosine Phosphonic Acid Analogues.</a> Liu, L.J., E. Kim, and J. Hong. Bulletin of the Korean Chemical Society, 2011. 32(5): p. 1662-1668; ISI[000291171000042].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500148">A Novel Family of Multivalent Compounds able to Interact with GP120 Anti-HIV Evaluation and Binding Analysis With Surface Plasmon Resonance.</a> Lozano, V., L. Aguado, B. Hoorelbeke, M. Renders, M. Camarasa, A. San-Felix, J. Balzarini, and M. Perez-Perez. Antiviral Research, 2011. 90(2): p. A72-A72; ISI[000291182500148].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291310600035">Polarity Changes in the Transmembrane Domain Core of HIV-1 Vpu Inhibits Its Anti-Tetherin Activity.</a> Lv, M.Y., J. Wang, X. Wang, T. Zuo, Y. Zhu, W. Kong, and X. Yu. Plos One, 2011. 6(6); ISI[000291310600035].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291144100012">Effect of Methamphetamine on Expression of HIV Coreceptors and CC-chemokines by Dendritic Cells.</a> Nair, M.P.N. and Z. Saiyed. Life Sciences, 2011. 88(21-22): p. 987-994; ISI[000291144100012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500052">Long-term Inhibition of HIV-1 Replication in CD4(+) T Cells Transduced with a Retroviral Vector Conditionally Expressing the Escherichia coli Endoribonuclease MazF.</a> Okamoto, M., H. Chono, H. Tsuda, K. Inoue, J. Mineno, and M. Baba. Antiviral Research, 2011. 90(2): p. A38-A38; ISI[000291182500052].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500064">Synthesis, Anti-HIV and Cytotoxic Activity of Some Novel Isatine-Sulfisomidine Derivatives.</a> Selvam, P., M. Chandramohan, C. Pannecouque, and E. De Clercq. Antiviral Research, 2011. 90(2): p. A42-A42; ISI[000291182500064].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500065.">Studies of HIV Integrase Inhibitory Activity of Novel Isatine Derivatives.</a> Selvam, P., K. Maddali, C. Marchand, and Y. Pommier. Antiviral Research, 2011. 90(2): p. A43-A43; ISI[000291182500065].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291136700009">Simultaneous Determination of Antiretroviral Zidovudine, Lamivudine and Efavirenz by RP HPLC-DAD.</a> Soares, M., J. Soares-Sobrinho, K. da Silva, L. Rolim, L. Alves, and P. Rolim-Neto. Latin American Journal of Pharmacy, 2011. 30(2): p. 273-280; ISI[000291136700009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291261300027">Services for Prediction of Drug Susceptibility for HIV Proteases and Reverse Transcriptases at the HIV Drug Research Centre.</a> Spjuth, O., M. Eklund, M. Lapins, M. Junaid, and J. Wikberg, Bioinformatics, 2011. 27(12): p. 1719-1720; ISI[000291261300027].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500074">Polymer-coupled Systems for Blocking the Viral Fusion 1. Modeling in silico the in vitro HIV-1 Entry Inhibitors.</a> Tsvetkov, V., A. Veselovski, and A. Serbin. Antiviral Research, 2011. 90(2): p. A46-A46; ISI[000291182500074].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500163.">Synthesis and Antiviral Activity of 3-Methoxy-2-(phosphonomethoxy)propyl Nucleoside Esters Against HCV and HIV-1.</a> Valiaeva, N., J. Beadle, D. Wyles, R. Schooley, and K. Hostetler. Antiviral Research, 2011. 90(2): p. A77-A77; ISI[000291182500163].</p>

    <p class="NoSpacing">[WOS]. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291182500080">Novel Inhibitors of Nuclear Translocation of HIV-1 Integrase.</a> Wagstaff, K., S. Rawlinson, A. Hearps, and D. Jans. Antiviral Research, 2011. 90(2): p. A48-A48; ISI[000291182500080].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291107500009">Isolation and Characterization of a Novel Thermostable Lectin from the Wild Edible Mushroom Agaricus arvensis.</a> Zhao, J.K., Y. Zhao, S. Li, H. Wang, and T. Ng, Journal of Basic Microbiology, 2011. 51(3): p. 304-311; ISI[000291107500009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0610-062311.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
