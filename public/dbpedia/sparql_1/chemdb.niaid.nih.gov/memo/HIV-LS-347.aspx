

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-347.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="So9e168CfjC/wg+QKPPg4lDfP6YIFyPfmu76WZBvzkNc/rb20o5cfHteRpVNllCVNh4Kxsph1k0+TCdQ4fC3ttnZxao30H0hRhGrqi4IvquEvOgjKDiAXwijsxg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F76CFBC2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-347-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48763   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Synthesis and Structure-Activity Relationship Studies of CD4 Down-Modulating Cyclotriazadisulfonamide (CADA) Analogues</p>

    <p>          Bell, Thomas W, Anugu, Sreenivasa, Bailey, Patrick, Catalano, Vincent J, Dey, Kaka, Drew, Michael GB, Duffy, Noah H, Jin, Qi, Samala, Meinrado F, Sodoma, Andrej, Welch, William H, Schols, Dominique, and Vermeire, Kurt</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(4): 1291-1312</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     48764   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Potent anti-HIV activity of scytovirin domain 1 peptide</p>

    <p>          Xiong, C, O&#39;keefe, BR, Byrd, RA, and McMahon, JB</p>

    <p>          Peptides <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16647158&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16647158&amp;dopt=abstract</a> </p><br />

    <p>3.     48765   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Design of nevirapine derivatives insensitive to the K103N and Y181C HIV-1 reverse transcriptase mutantsdagger</p>

    <p>          Saparpakorn, P, Hannongbua, S, and Rognan, D</p>

    <p>          SAR QSAR Environ Res <b>2006</b>.  17(2): 183-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16644557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16644557&amp;dopt=abstract</a> </p><br />

    <p>4.     48766   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Lysine sulfonamides as novel HIV-protease inhibitors: Nepsilon-Acyl aromatic alpha-amino acids</p>

    <p>          Stranix, BR, Lavallee, JF, Sevigny, G, Yelle, J, Perron, V, Leberre, N, Herbart, D, and Wu, JJ </p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16644213&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16644213&amp;dopt=abstract</a> </p><br />

    <p>5.     48767   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Hiv reverse transcriptase inhibitors</p>

    <p>          Koch, Uwe, Kinzel, Olaf, Muraglia, Ester, and Summa, Vincenzo</p>

    <p>          PATENT:  WO <b>2006037468</b>  ISSUE DATE:  20060413</p>

    <p>          APPLICATION: 2005  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     48768   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Monomeric APOBEC3G Is Catalytically Active and Has Antiviral Activity</p>

    <p>          Opi, S, Takeuchi, H, Kao, S, Khan, MA, Miyagi, E, Goila-Gaur, R, Iwatani, Y, Levin, JG, and Strebel, K</p>

    <p>          J Virol <b>2006</b>.  80(10): 4673-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641260&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641260&amp;dopt=abstract</a> </p><br />

    <p>7.     48769   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Antiviral efficacy and genotypic resistance patterns of combination therapy with stavudine/tenofovir in highly active antiretroviral therapy experienced patients</p>

    <p>          Antinori, A, Trotta, MP, Nastao, P, Bini, T, Bonora, S, Castagnas, A, Zaccarelli, M, Quirino, T, Landonio, S, Merli, S, Tozzi, V, Di, Perri G, Andreoni, M, Perno, CF, and Carosi, G</p>

    <p>          Antivir Ther <b>2006</b>.  11(2): 233-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640104&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640104&amp;dopt=abstract</a> </p><br />

    <p>8.     48770   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of sulfated cholic acid molecular umbrellas as anti-HIV and anti-HSV agents</p>

    <p>          Regen, Steven and Herold, Betsy C</p>

    <p>          PATENT:  WO <b>2006034369</b>  ISSUE DATE:  20060330</p>

    <p>          APPLICATION: 2005  PP: 32 pp.</p>

    <p>          ASSIGNEE:  (Lehigh University, USA and Mount Sinai School of Medicine of New York University)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48771   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of quinolizinone compounds as HIV integrase inhibitors</p>

    <p>          Satoh, Motohide, Aramaki, Hisateru, Nakamura, Hiroshi, Inoue, Masafumi, Kawakami, Hiroshi, Shinkai, Hisashi, Matsuzaki, Yuji, and Yamataka, Kazunobu</p>

    <p>          PATENT:  WO <b>2006033422</b>  ISSUE DATE:  20060330</p>

    <p>          APPLICATION: 2005  PP: 211 pp.</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48772   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Adverse Effects of Antiretroviral Drugs on HIV-1-infected and -uninfected Human Monocyte-derived Macrophages</p>

    <p>          Azzam, R, Lal, L, Goh, SL, Kedzierska, K, Jaworowski, A, Naim, E, Cherry, CL, Wesselingh, SL, Mills, J, and Crowe, SM</p>

    <p>          J Acquir Immune Defic Syndr <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16639337&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16639337&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48773   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          A Mixed-Valent Ruthenium-Oxo Oxalato Cluster Na7[Ru4(m3-O)4(C2O4)6] with Potent Anti-HIV Activities</p>

    <p>          Wong, Ella Lai-Ming, Sun, Raymond Wai-Yin, Chung, Nancy P-Y, Lin, Chen-Lung Steve, Zhu, Nianyong, and Che, Chi-Ming</p>

    <p>          Journal of the American Chemical Society <b>2006</b>.  128(15): 4938-4939</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   48774   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of N-aryl pyrrolidinones as novel anti-HIV-1 agents. Part 1</p>

    <p>          Wu, B, Kuhen, K, Ngoc, Nguyen T, Ellis, D, Anaclerio, B, He, X, Yang, K, Karanewsky, D, Yin, H, Wolff, K, Bieza, K, Caldwell, J, and He, Y</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16632349&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16632349&amp;dopt=abstract</a> </p><br />

    <p>13.   48775   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of carbamoylpyridone derivative having HIV integrase inhibitory activity</p>

    <p>          Yoshida, Hiroshi</p>

    <p>          PATENT:  WO <b>2006030807</b>  ISSUE DATE:  20060323</p>

    <p>          APPLICATION: 2005  PP: 87 pp.</p>

    <p>          ASSIGNEE:  (Shionogi &amp; Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48776   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Rapid Synthesis and In Situ Screening of Potent HIV-1 Protease Dimerization Inhibitors</p>

    <p>          Lee, SG and Chmielewski, J</p>

    <p>          Chem Biol <b>2006</b>.  13(4): 421-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16632254&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16632254&amp;dopt=abstract</a> </p><br />

    <p>15.   48777   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Betulinol derivatives as anti-HIV agents</p>

    <p>          Saxena, Brij B and Rathnam, Premila</p>

    <p>          PATENT:  WO <b>2006031706</b>  ISSUE DATE:  20060323</p>

    <p>          APPLICATION: 2005  PP: 133 pp.</p>

    <p>          ASSIGNEE:  (Cornell Research Foundation, Inc USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   48778   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Fumagillin suppresses HIV-1 infection of macrophages through the inhibition of Vpr activity</p>

    <p>          Watanabe, N, Nishihara, Y, Yamaguchi, T, Koito, A, Miyoshi, H, Kakeya, H, and Osada, H</p>

    <p>          FEBS Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16631749&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16631749&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   48779   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          HIV entry inhibitors: mechanisms of action and resistance pathways</p>

    <p>          Briz, Veronica, Poveda, Eva, and Soriano, Vincent</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  57(4): 619-627</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   48780   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of alkoxyalkyl esters of acyclic purine and pyrimidine nucleoside phosphonates against HIV-1 in vitro</p>

    <p>          Valiaeva, N, Beadle, JR, Aldern, KA, Trahan, J, and Hostetler, KY</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16630664&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16630664&amp;dopt=abstract</a> </p><br />

    <p>19.   48781   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Studies on the binding affinity of aminoglycoside antibiotics to the HIV-1 Rev responsive element for designing potential antiviral agents</p>

    <p>          Kwon, Youngjoo </p>

    <p>          Journal of Microbiology and Biotechnology <b>2006</b>.  16(1): 109-117</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   48782   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Antiviral activity of the cholesterol-binding compound amphotericin B methyl ester (Ame)</p>

    <p>          Freed, Eric O</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: PHYS-552</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   48783   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Synthesis and RT inhibitory activity evaluation of new pyrimidine-based seco-nucleosides</p>

    <p>          Vargas, G, Escalona, IS, Salas, M, Gordillo, B, and Sierra, A</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(3): 243-57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16629118&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16629118&amp;dopt=abstract</a> </p><br />

    <p>22.   48784   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          The discovery of a novel orally available CCR5 antagonist, as a therapeutic for HIV-1 infection</p>

    <p>          Nogi, Rena Nishizawa, Nishiyama, Toshihiko, Hisaichi, Katsuya, Minamoto, Chiaki, Matsunaga, Naoki, Hirai, Keisuke, Habashita, Hiromu, Takaoka, Yoshikazu, Toda, Masaaki, Takahashi, Eiji, Imawaka, Haruo, Sagawa, Kenji, Shibayama, Shiro, Fukushima, Daikichi, Maeda, Kenji, and Mitsuya, Hiroaki</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-385</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   48785   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Mutagenesis of HIV-1 Vpr protein and its use for inhibition of the infection of HIV-1 to CD4+ T cells</p>

    <p>          Aida, Yokoa, Iijima, Sayuki, Kasahara, Yuko, and Kimata, Kiyonori</p>

    <p>          PATENT:  JP <b>2006067994</b>  ISSUE DATE:  20060316</p>

    <p>          APPLICATION: 2005-54161  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (The Institute of Physical &amp; Chemical Research (Riken), Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   48786   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of imidazo[1,2-a]pyridine derivatives as chemokine receptor ligands with anti-HIV activity</p>

    <p>          Gudmundsson, Kristjan and Boggs, Sharon Davis</p>

    <p>          PATENT:  WO <b>2006026703</b>  ISSUE DATE:  20060309</p>

    <p>          APPLICATION: 2005  PP: 184 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   48787   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Antiviral drugs. Anti-AIDS (HIV)</p>

    <p>          Tamamura, Hirokazu</p>

    <p>          BIO Clinica <b>2006</b>.  21(3): 224-230</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   48788   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          Synthesis and biological investigation of S-aryl-S-DABO derivatives as HIV-1 inhibitors</p>

    <p>          Mugnaini, C, Manetti, F, Este, JA, Clotet-Codina, I, Maga, G, Cancio, R, Botta, M, and Corelli, F</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621553&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621553&amp;dopt=abstract</a> </p><br />

    <p>27.   48789   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Complex anti-HIV compound</p>

    <p>          Timofeev, IV, Serbin, AV, Perminova, NG, Timofeev, DI, Plyasunova, OA, Neklyudov, VV, and Karpyshev, NN</p>

    <p>          PATENT:  RU <b>2270690</b>  ISSUE DATE: 20060227</p>

    <p>          APPLICATION: 2004-52368  PP: 9 pp.</p>

    <p>          ASSIGNEE:  (Gos. Nauchnyi Tsentr Virusol. Biotekhnol. \&quot;Vektor\&quot;, Russia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   48790   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          The HIV entry inhibitors revisited</p>

    <p>          Leonard, JT and Roy, K</p>

    <p>          Curr Med Chem <b>2006</b>.  13(8): 911-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611075&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611075&amp;dopt=abstract</a> </p><br />

    <p>29.   48791   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of 8-(imidazol-2-ylmethylamino)-5,6,7,8-tetrahydroquinolines that bind to chemokine receptors for use against HIV and other disorders</p>

    <p>          Gudmundsson, Kristjan, Miller, John Franklin, and Turner, Elizabeth Madalena</p>

    <p>          PATENT:  WO <b>2006020415</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005  PP: 272 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>30.   48792   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          A killer mimotope with therapeutic activity against AIDS-related opportunistic micro-organisms inhibits ex-vivo HIV-1 replication</p>

    <p>          Casoli, C, Pilotti, E, Perno, CF, Balestra, E, Polverini, E, Cassone, A, Conti, S, Magliani, W, and Polonelli, L</p>

    <p>          AIDS <b>2006</b>.  20 (7): 975-980</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603848&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603848&amp;dopt=abstract</a> </p><br />

    <p>31.   48793   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          In vitro preclinical testing of nonoxynol-9 as potential anti-human immunodeficiency virus microbicide: a retrospective analysis of results from five laboratories</p>

    <p>          Beer, Brigitte E, Doncel, Gustavo F, Krebs, Fred C, Shattock, Robin J, Fletcher, Patricia S, Buckheit, Robert W Jr, Watson, Karen, Dezzutti, Charlene S, Cummins, James E, Bromley, Ena, Richardson-Harman, Nicola, Pallansch, Luke A, Lackman-Smith, Carol, Osterling, Clay, Mankowski, Marie, Miller, Shendra R, Catalone, Bradley J, Welsh, Patricia A, Howett, Mary K, Wigdahl, Brian, Turpin, Jim A, and Reichelderfer, Patricia</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2006</b>.  50(2): 713-723</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   48794   HIV-LS-347; PUBMED-HIV-5/2/2006</p>

    <p class="memofmt1-2">          A Concise and Selective Synthesis of Novel 5-Aryloxyimidazole NNRTIs</p>

    <p>          Jones, LH, Dupont, T, Mowbray, CE, and Newman, SD</p>

    <p>          Org Lett <b>2006</b>.  8(8): 1725-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16597151&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16597151&amp;dopt=abstract</a> </p><br />

    <p>33.   48795   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Aryl nucleoside H-phosphonates. Synthesis, properties, and anti-HIV activity of aryl nucleoside 5&#39;-a-hydroxyphosphonates</p>

    <p>          Szymanska, Agnieszka, Szymczak, Marzena, Boryski, Jerzy, Stawinski, Jacek, Kraszewski, Adam, Collu, Gabriella, Sanna, Giseppina, Giliberti, Gabriele, Loddo, Roberta, and Colla, Paolo La</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(6): 1924-1934</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   48796   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          New Series of Tri-Substituted Triazoles as Potent Non-Nucleoside Inhibitors of the HIV Reverse Transcriptase</p>

    <p>          De La Rosa, Martha</p>

    <p>          40th Western Regional Meeting of the American Chemical Society, Anaheim, CA, United States, January 22-25 <b>2006</b>.: WRM-376</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   48797   HIV-LS-347; WOS-HIV-4/23/2006</p>

    <p class="memofmt1-2">          Microtiter plate based chemistry and in situ screening: a useful approach for rapid inhibitor discovery</p>

    <p>          Brik, A, Wu, CY, and Wong, CH</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2006</b>.  4(8): 1446-1457, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236612500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236612500002</a> </p><br />

    <p>36.   48798   HIV-LS-347; SCIFINDER-HIV-4/24/2006</p>

    <p class="memofmt1-2">          Simultaneous determination of 16 anti-HIV drugs in human plasma by high-performance liquid chromatography</p>

    <p>          Notari, Stefania, Bocedi, Alessio, Ippolito, Giuseppe, Narciso, Pasquale, Pucillo, Leopoldo Paolo, Tossini, Gianna, Donnorso, Raffaele Perrone, Gasparrini, Francesco, and Ascenzi, Paolo</p>

    <p>          Journal of Chromatography, B: Analytical Technologies in the Biomedical and Life Sciences <b>2006</b>.  831(1-2): 258-266</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   48799   HIV-LS-347; WOS-HIV-4/23/2006</p>

    <p class="memofmt1-2">          Screening of fungi from Chinese medical plants for anti-human immunodeficiency virus type 1 activity</p>

    <p>          Xiang, ZC, Jiang, Y, and Guo, SX</p>

    <p>          JOURNAL OF INTEGRATIVE PLANT BIOLOGY <b>2006</b>.  48(4): 488-490, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236549200018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236549200018</a> </p><br />

    <p>38.   48800   HIV-LS-347; WOS-HIV-4/23/2006</p>

    <p class="memofmt1-2">          Taking aim at a rapidly evolving target: designing drugs to inhibit drug-resistant HIV-1 reverse transcriptases</p>

    <p>          Arnold, E</p>

    <p>          FASEB JOURNAL <b>2006</b>.  20(5): A1306-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236326204232">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236326204232</a> </p><br />

    <p>39.   48801   HIV-LS-347; WOS-HIV-4/23/2006</p>

    <p class="memofmt1-2">          Are Blockers of gp120/CD4 interaction effective inhibitors of HIV-1 lmmunopathogenesis?</p>

    <p>          Herbeuval, JP and Shearer, GM</p>

    <p>          AIDS REVIEWS <b>2006</b>.  8(1): 3-8, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236529800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236529800001</a> </p><br />

    <p>40.   48802   HIV-LS-347; WOS-HIV-4/23/2006</p>

    <p class="memofmt1-2">          Interpreting resistance data for HIV-1 therapy management - Know the limitations</p>

    <p>          Van Laethem, K and Vandamme, AM</p>

    <p>          AIDS REVIEWS <b>2006</b>.  8(1): 37-43, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236529800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236529800005</a> </p><br />

    <p>41.   48803   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Involvement of a small GTP binding protein in HIV-1 release</p>

    <p>          Audoly, G, Popoff, MR, and Gluschankof, P</p>

    <p>          RETROVIROLOGY <b>2005</b>.  2: 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236827600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236827600001</a> </p><br />

    <p>42.   48804   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Molecular strategies to inhibit HIV-1 replication</p>

    <p>          Nielsen, MH, Pedersen, FS, and Kjems, J</p>

    <p>          RETROVIROLOGY <b>2005</b>.  2: 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236790600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236790600001</a> </p><br />
    <br clear="all">

    <p>43.   48805   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Inhibition of Tat activity by the HEXIM1 protein</p>

    <p>          Fraldi, A, Varrone, F, Napolitano, G, Michels, AA, Majello, B, Bensaude, O, and Lania, L</p>

    <p>          RETROVIROLOGY <b>2005</b>.  2: 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236826100001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236826100001</a> </p><br />

    <p>44.   48806   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Therapeutic targets for HIV-1 infection in the host proteome</p>

    <p>          Liang, WS, Maddukuri, A, Teslovich, TM, de, la Fuente C, Agbottah, E, Dadgar, S, Kehn, K, Hautaniemi, S, Pumfery, A, Stephan, DA, and Kashanchi, F</p>

    <p>          RETROVIROLOGY <b>2005</b>.  2: 23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236791600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236791600001</a> </p><br />

    <p>45.   48807   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          BioAfrica&#39;s HIV-1 Proteomics Resource: Combining protein data with bioinformatics tools</p>

    <p>          Doherty, RS, De Oliveira, T, Seebregts, C, Danaviah, S, Gordon, M, and Cassol, S</p>

    <p>          RETROVIROLOGY <b>2005</b>.  2: 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236791400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236791400001</a> </p><br />

    <p>46.   48808   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Contribution of the C-terminal tri-lysine regions of human immunodeficiency virus type 1 integrase for efficient reverse transcription and viral DNA nuclear import</p>

    <p>          Ao, ZJ, Fowke, KR, Cohen, EA, and Yao, XJ</p>

    <p>          RETROVIROLOGY <b>2005</b>.  2: 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236856800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236856800001</a> </p><br />

    <p>47.   48809   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          DBR1 siRNA inhibition of HIV-1 replication</p>

    <p>          Ye, Y, De Leon, J, Yokoyama, N, Naidu, Y, and Camerini, D</p>

    <p>          RETROVIROLOGY <b>2005</b>.  2: 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236856800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236856800002</a> </p><br />

    <p>48.   48810   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          HIV-1 integrase inhibitors: 2003-2004 update</p>

    <p>          Dayam, R, Detig, FX, and Neamati, N</p>

    <p>          MEDICINAL RESEARCH REVIEWS <b>2006</b>.  26(3): 271-309, 39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236838100001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236838100001</a> </p><br />

    <p>49.   48811   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of aminoglycosides as inhibitors for rev binding to rev responsive element</p>

    <p>          Adachi, H, Takahashi, Y, Kyo, M, Sato, T, and Nishimura, Y</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2006</b>.  3(2): 71-75, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236843000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236843000001</a> </p><br />
    <br clear="all">

    <p>50.   48812   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Trim-cyclophilin A fusion proteins can restrict human immunodeficiency virus type 1 infection at two distinct phases in the viral life cycle</p>

    <p>          Yap, MW, Dodding, MP, and Stoye, JP</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(8): 4061-4067, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236685600037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236685600037</a> </p><br />

    <p>51.   48813   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          The discovery of tropane-derived CCR5 receptor antagonists</p>

    <p>          Armour, DR, de Groot, MJ, Price, DA, Stammen, BLC, Wood, A, Perros, M, and Burt, C</p>

    <p>          CHEMICAL BIOLOGY &amp; DRUG DESIGN <b>2006</b>.  67(4): 305-308, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236798300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236798300006</a> </p><br />

    <p>52.   48814   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          A fast and robust F-19 NMR-based method for finding new HIV-1 protease inhibitors</p>

    <p>          Frutos, S, Tarrago, T, and Giralt, E</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(10): 2677-2681, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236839700022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236839700022</a> </p><br />

    <p>53.   48815   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Developmental safety profile of the anti-HIV agent stampidine in rabbits</p>

    <p>          D&#39;Cruz, OJ, Erbeck, D, and Uckun, FM</p>

    <p>          ARZNEIMITTEL-FORSCHUNG-DRUG RESEARCH <b>2006</b>.  56(2A): 159-166, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236812600005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236812600005</a> </p><br />

    <p>54.   48816   HIV-LS-347; WOS-HIV-4/30/2006</p>

    <p class="memofmt1-2">          Site-specific enzymatic activation of the anti-HIV agent stampidine</p>

    <p>          Venkatachalam, TK, Qazi, S, and Uckun, FM</p>

    <p>          ARZNEIMITTEL-FORSCHUNG-DRUG RESEARCH <b>2006</b>.  56(2A): 167-175, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236812600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236812600006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
