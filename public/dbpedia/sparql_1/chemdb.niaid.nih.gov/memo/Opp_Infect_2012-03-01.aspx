

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-03-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="SHTQR32AhoSFglOJ/F5BxlLpR3fgz+SAYUcy+yibpOmeDm7Tuc6Zh8rVAn3+6KmEFX4Bj8i6jkLaMoouH8XA90ti2Sh2ooRD4UwWJsPOTNfl9giP2KCVqa9MUew=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3FFBF04B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">February 17- March 1, 2012</h1>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22178895">Relationship between Aggregation Properties and Antimicrobial Activities of Alkylphosphocholines with Branched Alkyl Chains.</a> Lukac, M., M. Garajova, M. Mrva, M. Bukovsky, F. Ondriska, E. Mariassy, F. Devinsky, and I. Lacko. International Journal of Pharmaceutics, 2012. 423(2): p. 247-56; PMID[22178895].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0217-030112.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22339776">Antimicrobial Properties and Cytotoxicity of an Antimicrobial Monomer for Application in Prosthodontics.</a>Regis, R.R., M.P. Vecchia, A.C. Pizzolitto, M.A. Compagnoni, P.P. Souza, and R.F. Souza. Journal of Prosthodontics : Official Journal of the American College of Prosthodontists, 2012. <b>[Epub ahead of print]</b>; PMID[22339776].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0217-030112.</p>


    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22357321">Synthesis and Biological Evaluation of 2-(3-Fluoro-4-nitro phenoxy)-N-phenylacetamide Derivatives as Novel Potential Affordable Antitubercular Agents.</a> Ang, W., Y.N. Lin, T. Yang, J.Z. Yang, W.Y. Pi, Y.H. Yang, Y.F. Luo, Y. Deng, and Y.Q. Wei. Molecules (Basel, Switzerland), 2012. 17(2): p. 2248-2258; PMID[22357321].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0217-030112.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22342247">Synergistic Effect of Two Combinations of Antituberculous Drugs against Mycobacterium tuberculosis.</a> Rey-Jurado, E., G. Tudo, J.A. Martinez, and J. Gonzalez-Martin. Tuberculosis (Edinburgh, Scotland), 2012. <b>[Epub ahead of print]</b>; PMID[22342247].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0217-030112.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22359585">A High-throughput Screen Identifies a New Natural Product with Broad-Spectrum Antibacterial Activity.</a> Ymele-Leki, P., S. Cao, J. Sharp, K.G. Lambert, A.J. McAdam, R.N. Husson, G. Tamayo, J. Clardy, and P.I. Watnick. Plos One, 2012. 7(2): p. e31307; PMID[22359585].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0217-030112.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22352841">Synthesis, Biological Evaluation and Structure-Activity Relationships of N-Benzoyl-2-hydroxybenzamides as Agents Active against P. falciparum (K1 Strain), Trypanosomes, and Leishmania.</a> Stec, J., Q. Huang, M. Pieroni, M. Kaiser, A. Fomovska, E. Mui, W.H. Witola, S. Bettis, R. McLeod, R. Brun, and A.P. Kozikowski. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22352841].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0217-030112.</p>

    <p class="plaintext">  </p>

    <p class="memofmt2-3">ANTIPROTOZOAL COMPOUNDS</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22354304">Novel N-Benzoyl-2-hydroxybenzamide Disrupts Unique Parasite Secretory Pathway.</a> Fomovska, A., Q. Huang, K. El Bissati, E.J. Mui, W.H. Witola, G. Cheng, Y. Zhou, C. Sommerville, C.W. Roberts, S. Bettis, S.T. Prigge, G.A. Afanador, M.R. Hickman, P.J. Lee, S.E. Leed, J.M. Auschwitz, M. Pieroni, J. Stec, S.P. Muench, D.W. Rice, A.P. Kozikowski, and R. McLeod. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22354304].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0217-030112.</p>


    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299734600022">In Vitro Antimicrobial and Antioxidant Activities of Bark Extracts of Bauhinia purpurea.</a> Avinash, P., I.H. Attitalla, M. Ramgopal, S. Ch, and M. Balaji. African Journal of Biotechnology, 2011. 10(45): p. 9160-9164; ISI[000299734600022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0217-030112.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299637000004">Antimycobacterial Evaluation of Pyrazinoic Acid Reversible Derivatives.</a> Dolezal, M., D. Kesetovic, and J. Zitko. Current Pharmaceutical Design, 2011. 17(32): p. 3506-3514; ISI[000299637000004].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>

    <p> </p>
    
    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299637000004">Occidiofungin&#39;s Chemical Stability and in Vitro Potency against Candida Species.</a> Ellis, D., J. Gosai, C. Emrick, R. Heintz, L. Romans, D. Gordon, S.E. Lu, F. Austin, and L. Smith. Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 765-769; ISI[000299658900023].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
    
    <p> </p>

    <p class="plaintext">11.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0002996858900023">Chemical Composition and Antimicrobial Activities of Essential Oil of Matricaria songarica.</a> Jian-Yu, S., L. Zhu, and Y.J. Tian. International Journal of Agriculture and Biology, 2012. 14(1): p. 107-110; ISI[000299661000016].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
   
    <p> </p>

    <p class="plaintext">12.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299661000016">Antimycobacterial Activity of Salicylanilide Benzenesulfonates.</a> Kratky, M., J. Vinsova, N.G. Rodriguez, and J. Stolarikova. Molecules, 2012. 17(1): p. 492-503; ISI[000299535700033].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
    
    <p> </p>

    <p class="plaintext">13.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299535700033">Synthesis of 1,4-Disubstituted Mono and Bis-Triazolocarbo-acyclonucleoside Analogues of 9-(4-Hydroxybutyl)guanine by Cu(I)-Catalyzed Click Azide-Alkyne Cycloaddition.</a> Krim, J., M. Taourirte, and J.W. Engels. Molecules, 2012. 17(1): p. 179-190; ISI[000299535700011].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
   
    <p> </p>
    
    <p class="plaintext">14.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299535700011">Concise and Facile Synthesis of Novel Pyrano 2,3-D pyrimidine-7-one/thione Derivatives as in Vitro Antimicrobial and Antitubercular Agents.</a> Mistry, P.T., N.R. Kamdar, D.D. Haveliwala, and S.K. Patel. Letters in Drug Design &amp; Discovery, 2011. 8(10): p. 750-757; ISI[000299565900011].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
    
    <p> </p>

    <p class="plaintext">15.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299565900011">Dithiocarbamates Are Strong Inhibitors of the Beta-class Fungal Carbonic Anhydrases from Cryptococcus neoformans, Candida albicans and Candida glabrata.</a> Monti, S.M., A. Maresca, F. Viparelli, F. Carta, G. De Simone, F.A. Muhlschlegel, A. Scozzafava, and C.T. Supuran. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(2): p. 859-862; ISI[000299653500016].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
   
    <p> </p>
    
    <p class="plaintext">16.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299653500016">Synthesis and Antitubercular Activity of New L-Serinyl hydrazone Derivatives.</a> Pinheiro, A.C., C.R. Kaiser, T.C.M. Nogueira, S.A. Carvalho, E.F. da Silva, L.D. Feitosa, M. Henriques, A.L.P. Candea, M.C.S. Lourenco, and M.V.N. de Souza. Medicinal Chemistry, 2011. 7(6): p. 611-623; ISI[000299577500011].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
   
    <p> </p>
    
    <p class="plaintext">17.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299577500011">Aerosolized Gentamicin Reduces the Burden of Tuberculosis in a Murine Model.</a> Roy, C.J., S.K. Sivasubramani, N.K. Dutta, S. Mehra, N.A. Golden, S. Killeen, J.D. Talton, B.E. Hammoud, P.J. Didier, and D. Kaushal. Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 883-886; ISI[000299658900038].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>    
   
    <p> </p>
    
    <p class="plaintext">18.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299658900038">Comparison of the Effect of Non-antifungal and Antifungal Agents on Candida Isolates from the Gastrointestinal Tract.</a> Siavoshi, F., A. Tavakolian, A. Foroumadi, N.M. Hosseini, S. Massarrat, S. Pedramnia, and P. Saniee. Archives of Iranian medicine, 2012. 15(1): p. 27-31; ISI[000299648700007].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
   
    <p> </p>

    <p class="plaintext">19.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299648700007">Anti-tuberculosis Activity of Commonly Used Medicinal Plants of South India.</a> Sivakumar, A. and G. Jayaraman. Journal of Medicinal Plants Research, 2011. 5(31): p. 6881-6884; ISI[000299782000026].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>

    <p> </p>
    
    <p class="plaintext">20.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299782000026">Antimycobacterial Activities of 5-Alkyl (or Halo)-3 &#39;-substituted Pyrimidine Nucleoside Analogs.</a> Srivastav, N.C., N. Shakya, S. Bhavanam, A. Agrawal, C. Tse, N. Desroches, D.Y. Kunimoto, and R. Kumar. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(2): p. 1091-1094; ISI[000299653500065].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
   
    <p> </p>
    
    <p class="plaintext">21.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299653500065">Combining Molecular Dynamics and Docking Simulations of the Cytidine Deaminase from Mycobacterium tuberculosis H37Rv.</a> Timmers, L., R.G. Ducati, Z.A. Sanchez-Quitian, L.A. Basso, D.S. Santos, and W.F. de Azevedo. Journal of Molecular Modeling, 2012. 18(2): p. 467-479; ISI[000299769700005].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
   
    <p> </p>

    <p class="plaintext">22.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299769700005">Phytochemical Screening and in Vitro Antifungal Activities of Extracts of Leaves of Morinda morindoides (Morinda, Rubiaceae).</a> Toure, A., C. Bahi, K. Ouattara, J.A. Djama, and A. Coulibaly. Journal of Medicinal Plants Research, 2011. 5(31): p. 6780-6786; ISI[000299782000013].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>

    <p> </p>

    <p class="plaintext">23.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299782000013">Synthesis and Antimicrobial Activity of Schiff Base of Chitosan and Acylated Chitosan.</a> Wang, J.T., Z.R. Lian, H.D. Wang, X.X. Jin, and Y.J. Liu. Journal of Applied Polymer Science, 2012. 123(6): p. 3242-3247; ISI[000298086500007].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>

    <p> </p>

    <p class="plaintext">24.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298086500007">E1210, a New Broad-spectrum Antifungal, Suppresses Candida albicans Hyphal Growth through Inhibition of Glycosylphosphatidylinositol Biosynthesis.</a> Watanabe, N., M. Miyazaki, T. Horii, K. Sagane, K. Tsukahara, and K. Hata. Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 960-971; ISI[000299658900050].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
    
    <p> </p>

    <p class="plaintext">25.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299658900050">Design, Synthesis and Evaluation of Novel Molecules with a Diphenyl Ether Nucleus as Potential Antitubercular Agents.</a> Yang, Y.H., Z.L. Wang, J.Z. Yang, T. Yang, W.Y. Pi, W. Ang, Y.N. Lin, Y.Y. Liu, Z.C. Li, Y.F. Luo, and Y.Q. Wei. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(2): p. 954-957; ISI[000299653500036].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
    
    <p> </p>

    <p class="plaintext">26.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299653500036">Possible Inhibitory Molecular Mechanism of Farnesol on the Development of Fluconazole Resistance in Candida albicans Biofilm.</a> Yu, L.H., X. Wei, M. Ma, X.J. Chen, and S.B. Xu. Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 770-775; ISI[000299658900024].</p>

    <p class="plaintext">[WOS]. OI_0217-030112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
