

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-11-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6arSf5ZwI9H9/MtQfsN0bSI7vWsY/KymZgOol6I9qm+dLcv+iPwphgXmau5f/LICnNjo3+VOs/rpYBZ2qa8ZOqKibVHVvYdqKHOHZvuvH1YAYlOX2eoxLbAa5SQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3AE518EC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 29 - November 11, 2010</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20845360">Development of peptide and small-molecule HIV-1 fusion inhibitors that target gp41.</a> Cai, L. and S. Jiang. ChemMedChem, 2010. 5(11): p. 1813-24; PMID[20845360].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21036153">Identification of a novel Vpr-binding compound that inhibits HIV-1 multiplication in macrophages by chemical array.</a> Hagiwara, K., T. Murakami, G. Xue, Y. Shimizu, E. Takeda, Y. Hashimoto, K. Honda, Y. Kondoh, H. Osada, Y. Tsunetsugu-Yokota, and Y. Aida. Biochemical and Biophysical Research Communications, 2010. <b>[Epub ahead of print]</b>; PMID[21036153].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20936623">Synthesis, in vitro and in vivo release kinetics, and anti-HIV activity of a sustained-release prodrug (mPEG-AZT) of 3&#39;-azido-3&#39;-deoxythymidine (AZT, Zidovudine).</a> Li, W., Y. Chang, P. Zhan, N. Zhang, X. Liu, C. Pannecouque, and E. De Clercq. ChemMedChem, 2010. 5(11): p. 1893-8; PMID[20936623].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21062811">Enhanced anti-HIV-1 activity of G-quadruplexes comprising locked nucleic acids and intercalating nucleic acids.</a> Pedersen, E.B., J.T. Nielsen, C. Nielsen, and V.V. Filichev. Nucleic Acids Research, 2010. <b>[Epub ahead of print]</b>; PMID[21062811].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20981615">A cardiac glucoside with in vitro anti-HIV activity isolated from Elaeodendron croceum.</a>  Prinsloo, G., J.J. Meyer, A.A. Hussein, E. Munoz, and R. Sanchez. Natural Product Research, 2010. 24(18): p. 1743-6; PMID[20981615].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20980522">RNA Aptamers directed to human immunodeficiency virus type 1 Gag polyprotein bind to the matrix and nucleocapsid domains and inhibit virus production.</a> Ramalingam, D., S. Duclair, S.A. Datta, A. Ellington, A. Rein, and V.R. Prasad. Journal of Virology, 2010. <b>[Epub ahead of print]</b>; PMID[20980522].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20135632">HIV-1 integrase inhibitors: 2007-2008 update.</a> Ramkumar, K., E. Serrao, S. Odde, and N. Neamati. Medical Research Review, 2010. 30(6): p. 890-954; PMID[20135632].</p>

    <p class="NoSpacing"><b>[Pubmed</b>]. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20928882">Studies of anti-HIV transcription inhibitor quinolones: identification of potent N1-vinyl derivatives.</a> Tabarrini, O., S. Massari, D. Daelemans, F. Meschini, G. Manfroni, L. Bottega, B. Gatto, M. Palumbo, C.</p>

    <p class="NoSpacing"> Pannecouque, and V. Cecchetti. ChemMedChem, 2010. 5(11): p. 1880-92; PMID[20928882].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21049929">Design, Synthesis, and Evaluation of Diarylpyridines and Diarylanilines as Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Tian, X., B. Qin, Z. Wu, X. Wang, H. Lu, S.L. Morris-Natschke, C.H. Chen, S. Jiang, K.H. Lee, and L. Xie. Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[21049929].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21045256">Nano-NRTIs: efficient inhibitors of HIV type-1 in macrophages with a reduced mitochondrial toxicity.</a> Vinogradov, S.V., L.Y. Poluektova, E. Makarov, T. Gerson, and M.T. Senanayake,. Antiviral Chemistry and Chemotherapy, 2010. 21(1): p. 1-14; PMID[21045256].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20605950">Virus-cell and cell-cell fusion mediated by the HIV-1 envelope glycoprotein is inhibited by short gp41 N-terminal membrane-anchored peptides lacking the critical pocket domain.</a> Wexler-Cohen, Y., A. Ashkenazi, M. Viard, R. Blumenthal, and Y. Shai. FASEB Journal, 2010. 24(11): p. 4196-202; PMID[20605950].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20739003">Partial selective inhibition of HIV-1 reverse transcriptase and human DNA polymerases gamma and beta by thiated 3&#39;-fluorothymidine analogue 5&#39;-triphosphates.</a> Winska, P., A. Miazga, J. Poznanski, and T. Kulikowski. Antiviral Research, 2010. 88(2): p. 176-81; PMID[20739003].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21048705">Epithelial cell secretions from the human female reproductive tract inhibit sexually transmitted pathogens and Candida albicans but not Lactobacillus.</a> Wira, C.R., M. Ghosh, J.M. Smith, L. Shen, R.I. Connor, P. Sundstrom, G.M. Frechette, E.T. Hill, and J.V. Fahey. Mucosal Immunology, 2010. <b>[Epub ahead of print]</b>; PMID[21048705].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20810137">Daphnane diterpenoids isolated from Trigonostemon thyrsoideum as HIV-1 antivirals.</a> Zhang, L., R.H. Luo, F. Wang, Z.J. Dong, L.M. Yang, Y.T. Zheng, and J.K. Liu,. Phytochemistry, 2010. 71(16): p. 1879-83; PMID[20810137].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p> </p>

    <p class="listparagraph memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">15.  <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Peptidomimetic inhibitors targeting the CCR5-binding site on the human immunodeficiency virus type-1 gp120 glycoprotein complexed to CD4.</a> Seitz, M., P. Rusert, K. Moehle, A. Trkola, and J.A. Robinson. Chemical Communications, 2010. 46(41): p. 7754-7756; ISI[000282765600025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Polyamide Amino Acids trimers as TAR RNA ligands and anti-HIV agents.</a> Bonnard, V., L. Pascale, S. Azoulay, A. Di Giorgio, C. Rogez-Kreuz, K. Storck, P. Clayette, and N. Patino. Bioorganic &amp; Medicinal Chemistry, 2010. 18(21): p. 7432-7438; ISI[000282904200011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Lignans from Daphne feddei Levl. var and Their Cytotoxicity.</a> Wang, L., Z.Y. Chen, C.Q. Jiang, Z. Liao, G.Y. Yang, and Y.K. Chzn. Asian Journal of Chemistry, 2010. 22(10): p. 8162-8166; ISI[000282910600093].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Isolation and characterization of secondary metabolites from the mangrove plant Rhizophora mucronata.</a> Jagadish, K. and J.A. Camarero. Cyclotides, A Promising Molecular Scaffold for Peptide-Based Therapeutics. 2010 . 6. Joel, E.L. and V. Bhimba. Asian Pacific Journal of Tropical Medicine, 2010. 3(8): p. 602-604; ISI[000282994900003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Solid-phase synthesis and screening of N-acylated polyamine (NAPA) combinatorial libraries for protein binding.</a> Iera, J.A., L.M.M. Jenkins, H. Kajiyama, J.B. Kopp, and D.H. Appella,. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6500-6503; ISI[000283052900032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">20. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Discovery and SAR of a series of 4,6-diamino-1,3,5-triazin-2-ol as novel non-nucleoside reverse transcriptase inhibitors of HIV-1.</a> Liu, B., Y. Lee, J.M. Zou, H.M. Petrassi, R.W. Joseph, W. Chao, E.L. Michelotti, M. Bukhtiyarova, E.B. Springman, and B.D. Dorsey. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6592-6596; ISI[000283052900051].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Xiamycin, a pentacyclic indolosesquiterpene with selective anti-HIV activity from a bacterial mangrove endophyte.</a> Ding, L., J. Munich, H. Goerls, A. Maier, H.H. Fiebig, W.H. Lin, and C. Hertweck. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6685-6687; ISI[000283052900070].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Potent and selective HIV-1 ribonuclease H inhibitors based on a 1-hydroxy-1,8-naphthyridin-2(1H)-one scaffold.</a> Williams, P.D., D.D. Staas, S. Venkatraman, H.M. Loughran, R.D. Ruzek, T.M. Booth, T.A. Lyle, J.S. Wai, J.P. Vacca, B.P. Feuston, L.T. Ecto, J.A. Flynn, D.J. DiStefano, D.J. Hazuda, C.M. Bahnck, A.L. Himmelberger, G. Dornadula, R.C. Hrin, K.A. Stillmock, M.V. Witmer, M.D. Miller, and J.A. Grobler. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6754-6757; ISI[000283052900085].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Evaluation of amide replacements in CCR5 antagonists as a means to increase intrinsic permeability. Part 2: SAR optimization and pharmacokinetic profile of a homologous azacyle series.</a> Wanner, J., L.J. Chen, R.C. Lemoine, R. Kondru, A. Jekle, G. Heilek, A. Derosier, C.H. Ji, P.W. Berry, and D.M. Rotstein. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6802-6807; ISI[000283052900095].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Isolation of chemical constituents from Enicosanthum cupulare (King) Airy-Shaw.</a> Fujita, S., M. Ninomiya, M. Efdi, K. Ohguchi, Y. Nozawa, and M. Koketsu. Natural Product Research, 2010. 24(17): p. 1630-1636; ISI[000283062600006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Anthraquinones and Lignans from Cassia occidentalis.</a> Li, S.F., Y.T. Di, Y.H. Wang, C.J. Tan, X. Fang, Y. Zhang, Y.T. Zheng, L. Li, H.P. He, S.L. Li, and X.J. Hao. Helvetica Chimica Acta, 2010. 93(9): p. 1795-1802; ISI[000283273200015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="../../../../../../../../../Users/Chris/Desktop/Jon_new/AIDS/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Multiple Biological Complex of Alkaline Extract of the Leaves of Sasa senanensis Rehder.</a> Sakagami, H., L. Zhou, M. Kawano, M.M. Thet, S. Tanaka, M. Machino, S. Amano, R. Kuroshita, S. Watanabe, Q. Chu, Q.T. Wang, T. Kanamoto, S. Terakubo, H. Nakashima, K. Sekine, Y. Shirataki, C.H. Zhang, Y. Uesawa, K. Mohri, M. Ketajima, H. Oizumi, and T. Oizumi. In Vivo, 2010. 24(5): p. 735-743; ISI[000283400500019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1029-111210.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
