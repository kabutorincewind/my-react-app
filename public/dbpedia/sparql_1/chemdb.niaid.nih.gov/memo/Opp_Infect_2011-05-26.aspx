

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-05-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QebFq5eBU1AXlLdcmZxmKnKBo/WTWfzQf7OWvEQg0zrQ7XUL7A9BarIB6uyhR5XZLH4s0bpCCG1JuyvdKuVQnZeqgh/MbqnBR1PIOawBqMPJpp8JICNxp4wIYNc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C13665C5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">May 13 - May 26, 2011</h1>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21600954">Argentinean Andean Propolis Associated with the Medicinal Plant Larrea nitida Cav. (Zygophyllaceae). HPLC-MS and GC-MS Characterization and Antifungal Activity.</a> Aguero, M.B., L. Svetaz, M. Sanchez, L. Luna, B. Lima, M.L. Lopez, S. Zacchino, J. Palermo, D. Wunderlin, G.E. Feresin, and A. Tapia. Food and Chemical Toxicology, 2011. <b>[Epub ahead of print]</b>; PMID[21600954].</p>

    <p class="NoSpacing"> <b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21605196">In vitro Antifungal Activity of Phenothiazines and their Combination with Amphotericin B against Different Candida Species.</a> Galgoczy, L., A. Bacsi, M. Homa, M. Viragh, T. Papp, and C. Vagvolgyi. Mycoses, 2011. <b>[Epub ahead of print]</b>; PMID[21605196].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21596542">Terpenoids Inhibit Candida albicans Growth by Affecting Membrane Integrity and Arrest of Cell Cycle.</a> Zore, G.B., A.D. Thakre, S. Jadhav, and S.M. Karuppayil. Phytomedicine, 2011. <b>[Epub ahead of print]</b>; PMID[21596542].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21585708">Structure-based Design, Synthesis and Antifungal Activity of New Triazole Derivatives.</a> Sheng, C., X. Che, W. Wang, S. Wang, Y. Cao, J. Yao, Z. Miao, and W. Zhang. Chemical Biology &amp; Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21585708].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21489791">Synthesis and Antifungal Activity of Functionalized 2,3-Spirostane Isomers.</a> Upadhyay, S.K., C.C. Creech, K.L. Bowdy, E.D. Stevens, B.S. Jursic, and D.M. Neumann. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2826-31; PMID[21489791].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">6.<a href="http://www.ncbi.nlm.nih.gov/pubmed/21591840">Anticandidal Activity of Cassia fistula and its Effect on Ergosterol Biosynthesis.</a> Irshad, M., S. Shreaz, N. Manzoor, L.A. Khan, and M.M. Rizvi. Pharmaceutical Biology, 2011. <b>[Epub ahead of print]</b>; PMID[21591840].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">7.<a href="http://www.ncbi.nlm.nih.gov/pubmed/21489787">SAR Studies of Pyridazinone Derivatives as Novel Glucan Synthase Inhibitors.</a> Zhou, G., P.C. Ting, R. Aslanian, J. Cao, D.W. Kim, R. Kuang, J.F. Lee, J. Schwerdt, H. Wu, R. Jason Herr, A.J. Zych, J. Yang, S. Lam, S. Wainhaus, T.A. Black, P.M. McNicholas, Y. Xu, and S.S. Walker. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2890-3; PMID[21489787].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21599889">In vitro Antimicrobial Activity of Ten Medicinal Plants against Clinical Isolates of Oral Cancer Cases.</a> Panghal, M., V. Kaushal, and J.P. Yadav. Annals of Clinical Microbiology and Antimicrobials, 2011. 10(1): p. 21; PMID[21599889].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21598298">Thienopyrimidines as Novel Inhibitors of Mycobacterium tuberclosis: Synthesis and In-vitro Studies.</a>  Rashmi, P., L.V. Nargund, K. Hazra, and J.N. Chandra. Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21598298].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">10.<a href="http://www.ncbi.nlm.nih.gov/pubmed/21507641">Synthesis and In vitro Antitubercular Activity of Ferrocene-based Hydrazones.</a> Mahajan, A., L. Kremer, S. Louw, Y. Gueradel, K. Chibale, and C. Biot. bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2866-8; PMID[21507641].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21576426">Synergistic Drug Combinations for Tuberculosis Therapy Identified by a Novel High Throughput Screen.</a>  Ramon-Garcia, S., C. Ng, H. Anderson, J.D. Chao, X. Zheng, T. Pfeifer, Y. Av-Gay, M. Roberge, and C.J. Thompson. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21576426]. <b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21498077">A Green Expedient Synthesis of Pyridopyrimidine-2-thiones and Their Antitubercular Activity.</a>  Rajesh, S.M., R.S. Kumar, L.A. Libertsen, S. Perumal, P. Yogeeswari, and D. Sriram. Bioorganic and Medicinal Chemistry Letters, 2011. 21(10): p. 3012-6; PMID[21498077].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21507635">Synthesis, Structure-activity Relationship of Novel Substituted 4H-Chromen-1,2,3,4-tetrahydropyrimidine-5-carboxylates as Potential Anti-mycobacterial and Anticancer Agents.</a> China Raju, B., R. Nageswara Rao, P. Suman, P. Yogeeswari, D. Sriram, T.B. Shaik, and S.V. Kalivendi. Bioorganic &amp;  Medicinal Chemistry Letters, 2011. 21(10): p. 2855-9; PMID[21507635].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p> 

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21599958">Anti-malarial Effect of Gum A\rabic.</a> Ballal, A., D. Bobbala, S.M. Qadri, M. Foller, D. Kempe, O. Nasir, A. Saeed, and F. Lang. Malaria Journal, 2011. 10(1): p. 139; PMID[21599958].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21493068">Dibenzylideneacetone Analogues as Novel Plasmodium falciparum Inhibitors.</a> Aher, R.B., G. Wanare, N. Kawathekar, R.R. Kumar, N.K. Kaushik, D. Sahal, and V.S. Chauhan. Bioorganic &amp;  Medicinal Chemistry Letters, 2011. 21(10): p. 3034-6; PMID[21493068].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21591758">A New Class of Phenazines with Activity against a Chloroquine Resistant Plasmodium falciparum Strain and Antimicrobial Activity.</a> Hussain, H., S. Specht, S. Sarite, M. Saeftel, A. Hoerauf, B. Schulz, and K. Krohn. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21591758].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21489789">Antiplasmodial and Antitumor Activity of Dihydroartemisinin Analogs Derived via the Aza-Michael Addition Reaction.</a> Feng, T.S., E.M. Guantai, M.J. Nell, C.E. van Rensburg, H.C. Hoppe, and K. Chibale. Bioorganic &amp;  Medicinal Chemistry Letters, 2011. 21(10): p. 2882-6; PMID[21489789].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21397502">Synthesis of Benzologues of Nitazoxanide and Tizoxanide: A Comparative Study of their In vitro Broad-spectrum Antiprotozoal Activity.</a> Navarrete-Vazquez, G., F. Chavez-Silva, R. Argotte-Ramos, C. Rodriguez-Gutierrez Mdel, M.J. Chan-Bacab, R. Cedillo-Rivera, R. Moo-Puc, and E. Hernandez-Nunez. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 3168-71; PMID[21397502].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-3">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="listparagraph"> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289849900002">7-Hydroxy-coumarin Derivatives: Synthesis, Characterization and Preliminary Antimicrobial Activities.</a> Farshori, N.N., M.R. Banday, A. Ahmad, A.U. Khan, and A. Rauf. Medicinal Chemistry Research, 2011. 20(5): p. 535-541; ISI[000289849900002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289859700007">Chemical Characterization (GC/MS and NMR Fingerprinting) and Bioactivities of South-African Pelargonium capitatum (L.) L&#39; HER. (Geraniaceae) Essential Oil.</a> Guerrini, A., D. Rossi, G. Paganetto, M. Tognolini, M. Muzzoli, C. Romagnoli, F. Antognoni, S. Vertuani, A. Medici, A. Bruni, C. Useli, E. Tamburini, R. Bruni, and G. Sacchetti. Chemistry &amp; Biodiversity, 2011. 8(4): p. 624-642; ISI[000289859700007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289849900003">Synthesis, Characterisation and Antimicrobial Activities of Novel 7,9-Diphenyl-1,2,4-triaza-8-oxa-spiro 4.5 -decan-3-thiones.</a> Kabilan, S. and S. Umamatheswari. Medicinal Chemistry Research, 2011. 20(5): p. 542-548; ISI[000289849900003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290093900021">Comparative Evaluation of the Antimicrobial Activity of Citrullus colocynthis Immature Fruit and Seed Organic Extracts.</a> Marzouk, B., Z. Marzouk, M. Mastouri, N. Fenina, and M. Aouni. African Journal of Biotechnology, 2011. 10(11): p. 2130-2134; ISI[000290093900021].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290084700023">1,4-Di-N-oxide Quinoxaline-2-carboxamide: Cyclic Voltammetry and Relationship between Electrochemical Behavior, Structure and Anti-tuberculosis Activity.</a> Moreno, E., S. Perez-Silanes, S. Gouravaram, A. Macharam, S. Ancizu, E. Torres, I. Aldana, A. Monge, and P.W. Crawford. Electrochimica Acta, 2011. 56(9): p. 3270-3275; ISI[000290084700023].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290075500029">Syntheses and Biological Evaluation of New Triazole-spirochromone Conjugates as Inhibitors of Mycobacterium tuberculosis.</a> Muthukrishnan, M., M. Mujahid, P. Yogeeswari, and D. Sriram. Tetrahedron Letters, 2011. 52(18): p. 2387-2389; ISI[000290075500029].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290461100010">Antimicrobial Activity of Protease Inhibitor from Leaves of Coccinia grandis (L.) Voigt.</a> Satheesh, L.S. and K. Murugan. Indian Journal of Experimental Biology, 2011. 49(5): p. 366-374; ISI[000290461100010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289849900009">Design, Synthesis and In vitro Antibacterial and Antifungal Activities of some Novel Spiro Azetidine-2,3&#39;-indole -2,4(1&#39;H)-dione.</a> Shah, R.J., N.R. Modi, M.J. Patel, L.J. Patel, B.F. Chauhan, and M.M. Patel. Medicinal Chemistry Research, 2011. 20(5): p. 587-594; ISI[000289849900009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289952700016">Synthesis and Antimicrobial Studies of Novel 2,4-Diaryl-3-azabicyclo 3.3.1 nonan-9-one 4 &#39;-phenylthiosemicarbazones.</a> Umamatheswari, S. and S. Kabilan. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. 26(3): p. 430-439; ISI[000289952700016].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290227600013">Synthesis, Chemical Properties, and Antimicrobial Activity of 2-and 2,3-substituted (Tetrahydro-2,4-dioxopyrimidin-1(2H)-yl)phenoxy naphthalene-1,4-diones.</a> Voskiene, A., B. Sapijanskaite, V. Mickevicius, K. Kantminiene, M. Stasevych, O. Komarovska-Porokhnyavets, R. Musyanovych, and V. Novikov. Monatshefte Fur Chemie, 2011. 142(5): p. 529-537; ISI[000290227600013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0513-052611.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
