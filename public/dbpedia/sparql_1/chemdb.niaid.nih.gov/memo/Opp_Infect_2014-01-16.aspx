

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-01-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tj/bEg8BqlYr+GF5/PoHapyIv+kEoQmD0gpLpkGhFjqhjkFqwGVWe9eLgChwyMJ4la0qsf8BPsNYKedNvaGIgy+SIwes/6JXRZSYWwYpTAN84L9UEck3FkgVbmU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2B65F445" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: January 3 - January 16, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23770066">Amphotericin B Releasing Nanoparticle Topical Treatment of Candida Spp. in the Setting of a Burn Wound.</a>Sanchez, D.A., D. Schairer, C. Tuckman-Vernon, J. Chouake, A. Kutner, J. Makdisi, J.M. Friedman, J.D. Nosanchuk, and A.J. Friedman. Nanomedicine: Nanotechnology, Biology, and Medicine, 2014. 10(1): p. 269-277. PMID[23770066].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24078467">Anti-Candida albicans Biofilm Effect of Novel Heterocyclic Compounds.</a> Kagan, S., A. Jabbour, E. Sionov, A.A. Alquntar, D. Steinberg, M. Srebnik, R. Nir-Paz, A. Weiss, and I. Polacheck. The Journal of Antimicrobial Chemotherapy, 2014. 69(2): p. 416-427. PMID[24078467].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211773">Antifungal and Antiaflatoxigenic Properties of Cuminum cyminum (L.) Seed Essential Oil and Its Efficacy as a Preservative in Stored Commodities.</a> Kedia, A., B. Prakash, P.K. Mishra, and N.K. Dubey. International Journal of Food Microbiology, 2014. 168-169: p. 1-7. PMID[24211773].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211775">Antifungal Impact of Volatile Fractions of Peumus boldus and Lippia turbinata on Aspergillus Section Flavi and Residual Levels of These Oils in Irradiated Peanut.</a> Passone, M.A. and M. Etcheverry. International Journal of Food Microbiology, 2014. 168-169: p. 17-23. PMID[24211775].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24392738">Antimicrobial Activity of Traditional Medicinal Plants from Ankober District, North Shewa Zone, Amhara Region, Ethiopia.</a>Lulekal, E., J. Rondevaldova, E. Bernaskova, J. Cepkova, Z. Asfaw, E. Kelbessa, L. Kokoska, and P. Van Damme. Pharmaceutical Biology, 2014. <b>[Epub ahead of print]</b>. PMID[24392738].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211859">Antimicrobial Activity of Zinc Oxide Particles on Five Micro-Organisms of the Challenge Tests Related to Their Physicochemical Properties.</a> Pasquet, J., Y. Chevalier, E. Couval, D. Bouvier, G. Noizet, C. Morliere, and M.A. Bolzinger. International Journal of Pharmaceutics, 2014. 460(1-2): p. 92-100. PMID[24211859].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24389279">Arachidonic acid Affects Biofilm Formation and PGE2 Level in Candida albicans and Non-albicans Species in Presence of Subinhibitory Concentration of Fluconazole and Terbinafine.</a> Mishra, N.N., S. Ali, and P.K. Shukla. Brazilian Journal of Infectious Diseases, 2014. <b>[Epub ahead of print]</b>. PMID[24389279].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24262487">Biological Activities of Commercial Bee Pollens: Antimicrobial, Antimutagenic, Antioxidant and Anti-Inflammatory</a>. Pascoal, A., S. Rodrigues, A. Teixeira, X. Feas, and L.M. Estevinho. Food and Chemical Toxicology, 2014. 63: p. 233-239. PMID[24262487].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24114570">Cranberry-derived Proanthocyanidins Prevent Formation of Candida albicans Biofilms in Artificial Urine through Biofilm- and Adherence-specific Mechanisms.</a>Rane, H.S., S.M. Bernardo, A.B. Howell, and S.A. Lee. The Journal of Antimicrobial Chemotherapy, 2014. 69(2): p. 428-436. PMID[24114570]. <b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24331757">Discovery of the Rapanone and Suberonone Mixture as a Motif for Leishmanicidal and Antifungal Applications.</a>da Costa, R.C., D.B. Santana, R.M. Araujo, J.E. de Paula, P.C. do Nascimento, N.P. Lopes, R. Braz-Filho, and L.S. Espindola. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 135-140. PMID[24331757].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23975673">The Diversity and Antimicrobial Activity of Preussia Sp. Endophytes Isolated from Australian Dry Rainforests.</a>Mapperson, R.R., M. Kotiw, R.A. Davis, and J.D. Dearnaley. Current Microbiology, 2014. 68(1): p. 30-37. PMID[23975673].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">12. <a href="file:///C:/Daniel%20Sylvester/Documents/AIDS%20Memo%20Docs/OI%20Memo%20010314-011614/.%20http:/www.ncbi.nlm.nih.gov/pubmed/23381026">Effect of Ottoman Viper (Montivipera xanthina (Gray, 1849)) Venom on Various Cancer Cells and on Microorganisms.</a> Yalcin, H.T., M.O. Ozen, B. Gocmen, and A. Nalbantsoy. Cytotechnology, 2014. 66(1): p. 87-94. PMID[23381026].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211081">Effect of Stereochemistry, Chain Length and Sequence Pattern on Antimicrobial Properties of Short Synthetic beta-Sheet Forming Peptide Amphiphiles.</a> Ong, Z.Y., J. Cheng, Y. Huang, K. Xu, Z. Ji, W. Fan, and Y.Y. Yang. Biomaterials, 2014. 35(4): p. 1315-1325. PMID[24211081].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24156913">In Vitro Analysis of Flufenamic acid Activity against Candida albicans Biofilms.</a>Chavez-Dozal, A.A., M. Jahng, H.S. Rane, K. Asare, V.V. Kulkarny, S.M. Bernardo, and S.A. Lee. International Journal of Antimicrobial Agents, 2014. 43(1): p. 86-91. PMID[24156913].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24409413">Isolation and Antimicrobial and Antioxidant Evaluation of Bio-active Compounds from Eriobotrya japonica Stems.</a>Rashed, K.N. and M. Butnariu. Advanced Pharmaceutical Bulletin, 2014. 4(1): p. 75-81. PMID[24409413].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24001978">Metal Based Pharmacologically Active Agents: Synthesis, Structural Characterization, Molecular Modeling, CT-DNA Binding Studies and in Vitro Antimicrobial Screening of Iron(II) Bromosalicylidene Amino acid Chelates.</a> Abdel-Rahman, L.H., R.M. El-Khatib, L.A. Nassr, A.M. Abu-Dief, M. Ismael, and A.A. Seleem. Spectrochimica Acta. Part A, Molecular and Biomolecular Spectroscopy, 2014. 117: p. 366-378. PMID[24001978].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23821127">Microbicidal and Anti-inflammatory Effects of Actinomadura spadix (EHA-2) Active Metabolites from Himalayan Soils, India.</a> Islam, V.I., S. Saravanan, and S. Ignacimuthu. World Journal of Microbiology &amp; Biotechnology, 2014. 30(1): p. 9-18. PMID[23821127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24145546">Novel Antifungal Drug Discovery Based on Targeting Pathways Regulating the Fungus-conserved UPC2 Transcription Factor.</a> Gallo-Ebert, C., M. Donigan, I.L. Stroke, R.N. Swanson, M.T. Manners, J. Francisco, G. Toner, D. Gallagher, C.Y. Huang, S.E. Gygax, M. Webb, and J.T. Nickels, Jr. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 258-266. PMID[24145546].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23981416">Novel Mixed Ligand Complexes of Bioactive Schiff Base (E)-4-(Phenyl (phenylimino) methyl) benzene-1,3-diol and 2-Aminophenol/2-Aminobenzoic acid: Synthesis, Spectral Characterization, Antimicrobial and Nuclease Studies.</a> Subbaraj, P., A. Ramu, N. Raman, and J. Dharmaraja. Spectrochimica Acta. Part A, Molecular and Biomolecular Spectroscopy, 2014. 117: p. 65-71. PMID[23981416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23905682">Photodynamic Fungicidal Efficacy of Hypericin and Dimethyl Methylene Blue against Azole-resistant Candida albicans Strains.</a> Paz-Cristobal, M.P., D. Royo, A. Rezusta, E. Andres-Ciriano, M.C. Alejandre, J.F. Meis, M.J. Revillo, C. Aspiroz, S. Nonell, and Y. Gilaberte. Mycoses, 2014. 57(1): p. 35-42. PMID[23905682].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24269762">Production of a Defensin-like Antifungal Protein NFAP from Neosartorya fischeri in Pichia pastoris and its Antifungal Activity against Filamentous Fungal Isolates from Human Infections.</a> Viragh, M., D. Voros, Z. Kele, L. Kovacs, A. Fizil, G. Lakatos, G. Maroti, G. Batta, C. Vagvolgyi, and L. Galgoczy. Protein Expression and Purification, 2014. 94: p. 79-84. PMID[24269762].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24025671">Spectroscopic and Biological Activities Studies of Bivalent Transition Metal Complexes of Schiff Bases Derived from Condensation of 1,4-Phenylenediamine and Benzopyrone Derivatives.</a>Sherif, O.E. and N.S. Abdel-Kader. Spectrochimica Acta. Part A, Molecular and Biomolecular Spectroscopy, 2014. 117: p. 519-526. PMID[24025671].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24222512">Synthesis and Antimicrobial Activity of alpha-Aminoboronic-containing Peptidomimetics</a>. Gozhina, O.V., J.S. Svendsen, and T. Lejon. Journal of Peptide, 2014. 20(1): p. 20-24. PMID[24222512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24300736">Synthesis and Biological Evaluation of Novel N-Substituted 1H-dibenzo[a,c]carbazole Derivatives of Dehydroabietic acid as Potential Antimicrobial Agents.</a>Gu, W., C. Qiao, S.F. Wang, Y. Hao, and T.T. Miao. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 328-331. PMID[24300736]. <b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211430">Synthesis and Characterization of Some Novel Antimicrobial Thiosemicarbazone O-carboxymethyl chitosan Derivatives.</a> Mohamed, N.A., R.R. Mohamed, and R.S. Seoudi. International Journal of Biological Macromolecules, 2014. 63: p. 163-169. PMID[24211430].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24332489">Synthesis and Evaluation of Novel Azoles as Potent Antifungal Agents.</a> Li, L., H. Ding, B. Wang, S. Yu, Y. Zou, X. Chai, and Q. Wu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 192-194. PMID[24332489].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24218046">Synthesis, Characterization, and Antimicrobial Activity of Silver(I) and Copper(II) Complexes of Phosphate Derivatives of Pyridine and Benzimidazole.</a> Kalinowska-Lis, U., E.M. Szewczyk, L. Checinska, J.M. Wojciechowski, W.M. Wolf, and J. Ochocki. ChemMedChem, 2014. 9(1): p. 169-176. PMID[24218046].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p>  

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24397620">Synthesis, Cytostatic, Antimicrobial and anti-HCV Activity of 6-Substituted 7-(het)aryl-7-deazapurine ribonucleosides.</a> Naus, P., O. Caletkova, P. Konecny, P. Dzubak, K. Bogdanova, M. Kolar, J. Vrbkova, L. Slavetinska, E. Tloustova, P. Perlikova, M. Hajduch, and M. Hocek. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24397620].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0<a name="_GoBack"></a>103-011614.</p>  

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24406786">Antimalarial Evaluation of the Chemical Constituents of Hairy Root Culture of Bixa orellana L.</a> Zhai, B., J. Clark, T. Ling, M. Connelly, F. Medina-Bolivar, and F. Rivas. Molecules, 2013. 19(1): p. 756-766. PMID[24406786].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24268543">Antiprotozoal Activity of Dicationic 3,5-Diphenylisoxazoles, Their Prodrugs and Aza-analogues.</a> Patrick, D.A., S.A. Bakunov, S.M. Bakunova, T. Wenzler, R. Brun, and R.R. Tidwell. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 559-576. PMID[24268543].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24146207.">Assessment of in Vivo Antimalarial Activities of Some Selected Medicinal Plants from Turkey.</a> Ozbilgin, A., C. Durmuskahya, H. Kayalar, and I. Ostan. Parasitology Research, 2014. 113(1): p. 165-173. PMID[24146207].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24392715.">Balgacyclamides, Antiplasmodial Heterocyclic Peptides from Microcystis aeruguinosa EAWAG 251.</a> Portmann, C., S. Sieber, S. Wirthensohn, J.F. Blom, L. Da Silva, E. Baudat, M. Kaiser, R. Brun, and K. Gademann. Journal of Natural Products, 2014. <b>[Epub ahead of print]</b>. PMID[24392715].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24269775">Chemical Composition, Cytotoxicity and in Vitro Antitrypanosomal and Antiplasmodial Activity of the Essential Oils of Four Cymbopogon Species from Benin.</a> Kpoviessi, S., J. Bero, P. Agbani, F. Gbaguidi, B. Kpadonou-Kpoviessi, B. Sinsin, G. Accrombessi, M. Frederich, M. Moudachirou, and J. Quetin-Leclercq. Journal of Ethnopharmacology, 2014. 151(1): p. 652-659. PMID[24269775].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24403182">Development of the First Oral Bioprecursors of bis-Alkylguanidine Antimalarial Drugs.</a>Degardin, M., S. Wein, J.F. Duckert, M. Maynadier, A. Guy, T. Durand, R. Escale, H. Vial, and Y. Vo-Hoang. ChemMedChem, 2014. <b>[Epub ahead of print]</b>. PMID[24403182].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24145526">Evidence for Pyronaridine as a Highly Effective Partner Drug for Treatment of Artemisinin-resistant Malaria in a Rodent Model.</a>Henrich, P.P., C. O&#39;Brien, F.E. Saenz, S. Cremers, D.E. Kyle, and D.A. Fidock. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 183-195. PMID[24145526]. <b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24117456">Formulation and Evaluation of Pheroid Vesicles Containing Mefloquine for the Treatment of Malaria.</a> du Plessis, L.H., C. Helena, E. van Huysteen, L. Wiesner, and A.F. Kotze. The Journal of Pharmacy and Pharmacology, 2014. 66(1): p. 14-22. PMID[24117456].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24217693">A High-content Phenotypic Screen Reveals the Disruptive Potency of Quinacrine and 3&#39;,4&#39;-Dichlorobenzamil on the Digestive Vacuole of Plasmodium falciparum.</a>Lee, Y.Q., A.S. Goh, J.H. Ch&#39;ng, F.H. Nosten, P.R. Preiser, S. Pervaiz, S.K. Yadav, and K.S. Tan. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 550-558. PMID[24217693].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24318747">In Vitro Antiplasmodial Activity of Some Medicinal Plants of Burkina Faso.</a> Ouattara, L.P., S. Sanon, V. Mahiou-Leddet, A. Gansane, B. Baghdikian, A. Traore, I. Nebie, A.S. Traore, N. Azas, E. Ollivier, and S.B. Sirima. Parasitology Research, 2014. 113(1): p. 405-416. PMID[24318747].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23891618">Nanoparticle Formulations of Decoquinate Increase Antimalarial Efficacy against Liver Stage Plasmodium Infections in Mice.</a>Wang, H., Q. Li, S. Reyes, J. Zhang, Q. Zeng, P. Zhang, L. Xie, P.J. Lee, N. Roncal, V. Melendez, M. Hickman, and M.P. Kozar. Nanomedicine : Nanotechnology, Biology, and Medicine, 2014. 10(1): p. 57-65. PMID[23891618].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24165175">Potential Efficacy of Citicoline as Adjunct Therapy in Treatment of Cerebral Malaria.</a> El-Assaad, F., V. Combes, G.E. Grau, and R. Jambou. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 602-605. PMID[24165175].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0103-011614.</p><br />

    <p class="plaintext">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24369685">Quinine Dimers Are Potent Inhibitors of the Plasmodium falciparum Chloroquine Resistance Transporter and Are Active against Quinoline-Resistant P. falciparum.</a> Hrycyna, C.A., R.L. Summers, A.M. Lehane, M.M. Pires, H. Namanja, K. Bohn, J. Kuriakose, M. Ferdig, P.P. Henrich, D.A. Fidock, K. Kirk, J. Chmielewski, and R.E. Martin. ACS Chemical Biology, 2014. <b>[Epub ahead of print]</b>. PMID[24369685]. <b>[PubMed]</b>. OI_0103-011614.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327787700069">Anthranilic acid-based Thumb Pocket 2 HCV NS5B Polymerase Inhibitors with Sub-micromolar Potency in the Cell-based Replicon Assay</a>. Stammers, T.A., R. Coulombe, M. Duplessis, G. Fazal, A. Gagnon, M. Garneau, S. Goulet, A. Jakalian, S. LaPlante, J. Rancourt, B. Thavonekham, D. Wernic, G. Kukolj, and P.L. Beaulieu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6879-6885. ISI[000327787700069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327507700005">Anti-candida Activity and Chemical Composition of Cinnamomum zeylanicum Blume Essential Oil</a>. de Castro, R.D. and E.O. Lima. Brazilian Archives of Biology and Technology, 2013. 56(5): p. 749-755. ISI[000327507700005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327727900010">Antifungal Activity of a Library of Cyclitols and Related Compounds</a>. Bellomo, A., A. Bertucci, V. de la Sovera, G. Carrau, M. Raimondi, S. Zacchino, H.A. Stefani, and D. Gonzalez. Letters in Drug Design &amp; Discovery, 2014. 11(1): p. 67-75. ISI[000327727900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328179300030">Anti-Hepatitis C Virus RdRP Activity and Replication of Novel Anilinobenzothiazole Derivatives</a>. Peng, H.K., W.C. Chen, Y.T. Lin, C.K. Tseng, S.Y. Yang, C.C. Tzeng, J.C. Lee, and S.C. Yang. Antiviral Research, 2013. 100(1): p. 269-275. ISI[000328179300030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328260500015">Antimycobacterial Activity of Cyclic Dipeptides Isolated from Bacillus Sp N Strain Associated with Entomopathogenic Nematode</a>. Kumar, S.N. and C. Mohandas. Pharmaceutical Biology, 2014. 52(1): p. 91-96. ISI[000328260500015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327686900013">Antimycobacterial Efficacy of Silver Nanoparticles as Deposited on Porous Membrane Filters</a>. Islam, M.S., C. Larimer, A. Ojha, and I. Nettleship. Materials Science &amp; Engineering C-Materials for Biological Applications, 2013. 33(8): p. 4575-4581. ISI[000327686900013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327787700061">Anti-tubercular Agents. Part 8: Synthesis, Antibacterial and Antitubercular Activity of 5-Nitrofuran Based 1,2,3-Triazoles</a>. Kamal, A., S.M.A. Hussaini, S. Faazil, Y. Poornachandra, G.N. Reddy, C.G. Kumar, V.S. Rajput, C. Rani, R. Sharma, I.A. Khan, and N.J. Babu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6842-6846. ISI[000327787700061].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328234100011">Antiviral Activity of Methyl Helicterate Isolated from Helicteres angustifolia (Sterculiaceae) against Hepatitis B Virus</a>. Huang, Q.F., R.B. Huang, L. Wei, Y.X. Chen, S.J. Lv, C.H. Liang, X.R. Zhang, F.J. Yin, H.T. Li, L. Zhuo, and X. Lin. Antiviral Research, 2013. 100(2): p. 373-381. ISI[000328234100011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328324100017">50. Bacteriocin Activity against Various Pathogens Produced by Pediococcus pentosaceus VJ13 Isolated from Idly Batter</a>. Vidhyasagar, V. and K. Jeevaratnam. Biomedical Chromatography, 2013. 27(11): p. 1497-1502. ISI[000328324100017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br />  

    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328094600053">Block Copolymer Mixtures as Antimicrobial Hydrogels for Biofilm Eradication</a>. Lee, A.L.Z., V.W.L. Ng, W.X. Wang, J.L. Hedrick, and Y.Y. Yang. Biomaterials, 2013. 34(38): p. 10278-10286. ISI[000328094600053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328321900009">Chemical Composition and Antimicrobial Activity of the Essential Oil from the Edible Aromatic Plant Aristolochia delavayi</a>. Li, Z.J., G.S.S. Njateng, W.J. He, H.X. Zhang, J.L. Gu, S.N. Chen, and Z.Z. Du. Chemistry &amp; Biodiversity, 2013. 10(11): p. 2032-2041. ISI[000328321900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327697400017">Chemical Composition and Antimicrobial Activity of the Essential Oil of Stachys officinalis (L.) Trevis. (Lamiaceae)</a>. Lazarevic, J.S., A.S. Dordevic, D.V. Kitic, B.K. Zlatkovic, and G.S. Stojanovic. Chemistry &amp; Biodiversity, 2013. 10(7): p. 1335-1349. ISI[000327697400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327864000005">Clerodendrumic acid, a New Triterpenoid from Clerodendrum glabrum (Verbenaceae), and Antimicrobial Activities of Fractions and Constituents</a>. Masevhe, N.A., M.D. Awouafack, A.S. Ahmed, L.J. McGaw, and J.N. Eloff. Helvetica Chimica Acta, 2013. 96(9): p. 1693-1703. ISI[000327864000005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">55. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327766200029">Design and Synthesis of Novel Antimicrobials with Activity against Gram-positive Bacteria and Mycobacterial Species, Including M. tuberculosis</a>. Tiruveedhula, V., C.M. Witzigmann, R. Verma, M.S. Kabir, M. Rott, W.R. Schwan, S. Medina-Bielski, M. Lane, W. Close, R.L. Polanowski, D. Sherman, A. Monte, J.R. Deschamps, and J.M. Cook. Bioorganic &amp; Medicinal Chemistry, 2013. 21(24): p. 7830-7840. ISI[000327766200029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">56. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327787700053">Design, Synthesis and Evaluation of 6-(4-((Substituted-1H-1,2,3-triazol-4-yl)methyl)piperazin-1-yl) phenanthridine Analogues as Antimycobacterial Agents</a>. Nagesh, H.N., K.M. Naidu, D.H. Rao, J.P. Sridevi, D. Sriram, P. Yogeeswari, and K. Sekhar. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6805-6810. ISI[000327787700053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">57. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327787700011">Discovery of an Irreversible HCV NS5B Polymerase Inhibitor</a>. Zeng, Q.B., A.G. Nair, S.B. Rosenblum, H.C. Huang, C.A. Lesburg, Y.H. Jiang, O. Selyutin, T.Y. Chan, F. Bennett, K.X. Chen, S. Venkatraman, M. Sannigrahi, F. Velazquez, J.S. Duca, S. Gavalas, Y.H. Huang, H.Y. Pu, L. Wang, P. Pinto, B. Vibulbhan, S. Agrawal, E. Ferrari, C.K. Jiang, C. Li, D. Hesk, J. Gesell, S. Sorota, N.Y. Shih, F.G. Njoroge, and J.A. Kozlowski. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6585-6587. ISI[000327787700011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">58. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327308500086">Effect of Tetrandrine against Candida albicans Biofilms</a>. Zhao, L.X., D.D. Li, D.D. Hu, G.H. Hu, L. Yan, Y. Wang, and Y.Y. Jiang. Plos One, 2013. 8(11): p. e79671. ISI[000327308500086].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">59. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328065300006">Effective Concentration-based Serum Pharmacodynamics for Antifungal Azoles in a Murine Model of Disseminated Candida albicans Infection</a>. Maki, K. and S. Kaneko. European Journal of Drug Metabolism and Pharmacokinetics, 2013. 38(4): p. 261-268. ISI[000328065300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br />  

    <p class="plaintext">60. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328234100019">In Vitro Efficacy of Approved and Experimental Antivirals against Novel Genotype 3 Hepatitis C Virus Subgenomic Replicons</a>. Yu, M., A.C. Corsa, S.M. Xu, B. Peng, R.Y. Gong, Y.J. Lee, K.T. Chan, H.M. Mo, W. Delaney, and G.F. Cheng. Antiviral Research, 2013. 100(2): p. 439-445. ISI[000328234100019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">61. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327682400017">Inhibition Studies on Mycobacterium Tuberculosis N-Acetylglucosamine-1-phosphate uridyltransferase (GLMU)</a>. Tran, A.T., D.Y. Wen, N.P. West, E.N. Baker, W.J. Britton, and R.J. Payne. Organic &amp; Biomolecular Chemistry, 2013. 11(46): p. 8113-8126. ISI[000327682400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">62. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328372700001">The Inhibitory Effects of Curcuma longa L. Essential Oil and Curcumin on Aspergillus flavus Link Growth and Morphology</a>. Ferreira, F.D., S.A.G. Mossini, F.M.D. Ferreira, C.C. Arroteia, C.L. da Costa, C.V. Nakamura, and M. Machinski. Scientific World Journal, 2013. 343804: pp. 6. ISI[000328372700001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">63. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327567900019">Isolation and Identification of an Anti-Hepatitis B Virus Compound from Hydrocotyle sibthorpioides Lam</a>. Huang, Q.F., S.J. Zhang, R.B. Huang, L. Wei, Y.X. Chen, S.J. Lv, C.H. Liang, S.M. Tan, S. Liang, L. Zhuo, and X. Lin. Journal of Ethnopharmacology, 2013. 150(2): p. 568-575. ISI[000327567900019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">64. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328197100001">Nanodisk Containing Super Aggregated Amphotericin B: A High Therapeutic Index Antifungal Formulation with Enhanced Potency.</a> Burgess, B.L., Y.M. He, M.M. Baker, B. Luo, S.F. Carroll, T.M. Forte, and M.N. Oda. International Journal of Nanomedicine, 2013. 8: p. 4733-4742. ISI[000328197100001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">65. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327792200007">A New Plant-derived Antibacterial is an Inhibitor of Efflux Pumps in Staphylococcus aureus</a>. Shiu, W.K.P., J.P. Malkinson, M.M. Rahman, J. Curry, P. Stapleton, M. Gunaratnam, S. Neidle, S. Mushtaq, M. Warner, D.M. Livermore, D. Evangelopoulos, C. Basavannacharya, S. Bhakta, B.D. Schindler, S.M. Seo, D. Coleman, G.W. Kaatz, and S. Gibbons. International Journal of Antimicrobial Agents, 2013. 42(6): p. 513-518. ISI[000327792200007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">66. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328372400001">Phytochemical Profiles, Antioxidant and Antimicrobial Activities of Three Potentilla Species</a>. Wang, S.S., D.M. Wang, W.J. Pu, and D.W. Li. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 321. ISI[000328372400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">67. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327756900012">Screening of Fruit and Leaf Essential Oils of Litsea cubeba Pers. From North-east India - Chemical Composition and Antimicrobial Activity</a>. Saikia, A.K., D. Chetia, M. D&#39;Arrigo, A. Smeriglio, T. Strano, and G. Ruberto. Journal of Essential Oil Research, 2013. 25(4): p. 330-338. ISI[000327756900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">68. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327706100006">Synthesis and Antiviral Activity of New Substituted Methyl [2-(Arylmethylene-hydrazino)-4-oxo-thiazolidin-5-ylidene] acetates</a>. Saeed, A., N.A. Al-Masoudi, and M. Latif. Archiv der Pharmazie, 2013. 346(8): p. 618-625. ISI[000327706100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br />  

    <p class="plaintext">69. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000328509300012">Synthesis and Characterization of Antimicrobial Materials Derived from Natural Polymers in-Situ Copolymerization</a>. Bai, Y.X., C.P. Chang, Y. Bao, and Y.M. Song. Soft Materials, 2014. 12(1): p. 103-112. ISI[000328509300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">70. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327788800009">Synthesis of Tetradentate Schiff Base Derivatives of Transition Bimetallic Complexes as Antimicrobial Agents</a>. Yousaf, M., M. Pervaiz, M. Sagir, Z. Ashhar uz, M. Mushtaq, and M.Y. Naz. Journal of the Chinese Chemical Society, 2013. 60(9): p. 1150-1155.
    <br />
    ISI[000327788800009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br /> 

    <p class="plaintext">71. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000327939000057">Synthesis, Characterization and Antimicrobial Activity of Silver(I) Complexes of Hydroxymethyl Derivatives of Pyridine and Benzimidazole</a>. Kalinowska-Lis, U., A. Felczak, L. Checinska, K. Lisowska, and J. Ochocki. Journal of Organometallic Chemistry, 2014. 749: p. 394-399. ISI[000327939000057].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0103-011614.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
