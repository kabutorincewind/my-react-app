

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-343.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q48pQtK2dy523h1XqePrLkrPdbqJSh7SOTwTm3tjMcqmGTtC8uR1Z5jrEmKgyAisybviuEJWjiAVwF0AzWNB7F1SOiaglrZnUQywtYqnJ7z8NZCsFaVQr+PNVc8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7F395BF8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-343-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48565   OI-LS-343; PUBMED-OI-3/6/2006</p>

    <p class="memofmt1-2">          Pyranocoumarin, a novel anti-TB pharmacophore: Synthesis and biological evaluation against Mycobacterium tuberculosis</p>

    <p>          Xu, ZQ, Pupek, K, Suling, WJ, Enache, L, and Flavin, MT</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16513358&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16513358&amp;dopt=abstract</a> </p><br />

    <p>2.     48566   OI-LS-343; PUBMED-OI-3/6/2006</p>

    <p class="memofmt1-2">          Telbivudine: A Novel Nucleoside Analog for Chronic Hepatitis B (March)</p>

    <p>          Kim, JW, Park, SH, and Louie, SG</p>

    <p>          Ann Pharmacother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16507625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16507625&amp;dopt=abstract</a> </p><br />

    <p>3.     48567   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Effects of albendazole, fumagillin, and TNP-470 on microsporidial replication in vitro</p>

    <p>          Didier, Elizabeth S</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>1997</b>.  41(7): 1541-1546</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     48568   OI-LS-343; PUBMED-OI-3/6/2006</p>

    <p class="memofmt1-2">          The crystal structure of Rv0793, a hypothetical monooxygenase from M. tuberculosis</p>

    <p>          Lemieux, MJ, Ference, C, Cherney, MM, Wang, M, Garen, C, and James, MN</p>

    <p>          J Struct Funct Genomics <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16496224&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16496224&amp;dopt=abstract</a> </p><br />

    <p>5.     48569   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Water-soluble amphotericin B-polyvinylpyrrolidone complexes with maintained antifungal activity against Candida spp. and Aspergillus spp. and reduced haemolytic and cytotoxic effects</p>

    <p>          Charvalos, Ekatherina, Tzatzarakis, Manolis N, Van Bambeke, Francoise, Tulkens, Paul M, Tsatsakis, Aristidis M, Tzanakakis, George N, and Mingeot-Leclercq, Marie-Paule</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  57(2): 236-244</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     48570   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Preparation of arylamidine derivatives as antifungal agents</p>

    <p>          Hayashi, Kazuya, Kunitani, Kazuto, Uehara, Sayuri, and Morita, Teiichi</p>

    <p>          PATENT:  WO <b>2006003881</b>  ISSUE DATE:  20060112</p>

    <p>          APPLICATION: 2005  PP: 85 pp.</p>

    <p>          ASSIGNEE:  (Toyama Chemical Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     48571   OI-LS-343; PUBMED-OI-3/6/2006</p>

    <p class="memofmt1-2">          Potent Inhibition of Macrophage Responses to IFN-{gamma} by Live Virulent Mycobacterium tuberculosis Is Independent of Mature Mycobacterial Lipoproteins but Dependent on TLR2</p>

    <p>          Banaiee, N, Kincaid, EZ, Buchwald, U, Jacobs, WR Jr, and Ernst, JD</p>

    <p>          J Immunol <b>2006</b>.  176(5): 3019-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16493060&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16493060&amp;dopt=abstract</a> </p><br />

    <p>8.     48572   OI-LS-343; PUBMED-OI-3/6/2006</p>

    <p class="memofmt1-2">          Structure-based inhibitor design of AccD5, an essential acyl-CoA carboxylase carboxyltransferase domain of Mycobacterium tuberculosis</p>

    <p>          Lin, TW, Melgar, MM, Kurth, D, Swamidass, SJ, Purdon, J, Tseng, T, Gago, G, Baldi, P, Gramajo, H, and Tsai, SC</p>

    <p>          Proc Natl Acad Sci U S A <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16492739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16492739&amp;dopt=abstract</a> </p><br />

    <p>9.     48573   OI-LS-343; PUBMED-OI-3/6/2006</p>

    <p class="memofmt1-2">          Selective anti-cytomegalovirus compounds discovered by screening for inhibitors of subunit interactions of the viral polymerase</p>

    <p>          Loregian, A and Coen, DM</p>

    <p>          Chem Biol <b>2006</b>.  13(2): 191-200</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16492567&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16492567&amp;dopt=abstract</a> </p><br />

    <p>10.   48574   OI-LS-343; PUBMED-OI-3/6/2006</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some new 1-alkyl-2-alkylthio-1,2,4-triazolobenzimidazole derivatives</p>

    <p>          Mohamed, BG, Hussein, MA, Abdel-Alim, AA, and Hashem, M</p>

    <p>          Arch Pharm Res  <b>2006</b>.  29(1): 26-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16491839&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16491839&amp;dopt=abstract</a> </p><br />

    <p>11.   48575   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Antibacterial and antifungal ferrocene incorporated dithiothione and dithioketone compounds</p>

    <p>          Chohan, Zahid H</p>

    <p>          Applied Organometallic Chemistry <b>2006</b>.  20(2): 112-116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   48576   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Hydroxylated analogues of the orally active broad spectrum antifungal, Sch 51048, and the discovery of posaconazole (Sch 56592)</p>

    <p>          Bennett, Frank, Saksena, Anil K, Lovey, Raymond G, Liu, Yi-Tsung, Patel, Naginbhai M, Pinto, Patrick, Pike, Russel, Jao, Edwin, Girijavallabhan, Viyyoor M, Ganguly, Ashit K, Loebenberg, David, Wang, Haiyan, Cacciapuoti, Anthony, Moss, Eugene, Menzel, Fred, Hare, Roberta S, and Nomeir, Amin</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(1): 186-190</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   48577   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Aerosolised antibiotics: a critical appraisal of their use</p>

    <p>          Hagerman, Jennifer K, Hancock, Kim E, and Klepser, Michael E</p>

    <p>          Expert Opinion on Drug Delivery <b>2006</b>.  3(1): 71-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48578   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Oxazolidinone derivatives as antimicrobials</p>

    <p>          Das Biswajit,  Ahmed, Shahadat, Yadav, Ajay Singh, Ghosh, Soma, and Rattan, Ashok</p>

    <p>          PATENT:  WO <b>2006018682</b>  ISSUE DATE: 20060223</p>

    <p>          APPLICATION: 2005</p>

    <p>          ASSIGNEE:  (Ranbaxy Laboratories Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   48579   OI-LS-343; WOS-OI-2/26/2006</p>

    <p class="memofmt1-2">          Genomic analysis of the Mycobacterium tuberculosis complex: applications to laboratory diagnosis and genotyping</p>

    <p>          Driscoll, JR, Parsons, LA, Salfinger, M, and Taber, HW</p>

    <p>          REVIEWS IN MEDICAL MICROBIOLOGY <b>2005</b>.  16(2): 49-58, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235223400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235223400003</a> </p><br />

    <p>16.   48580   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Methods of screening for inhibitors of m. tuberculosis, assays for use therein, and methods and compositions for treating of preventing m. tuberculosis infection</p>

    <p>          Derbyshire, Keith M and Kowalski, Joseph C</p>

    <p>          PATENT:  WO <b>2006020898</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005</p>

    <p>          ASSIGNEE:  (Health Research, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   48581   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Characterization of the putative operon containing arylamine N-acetyltransferase (nat) in Mycobacterium bovis BCG</p>

    <p>          Anderton, Matthew C, Bhakta, Sanjib, Besra, Gurdyal S, Jeavons, Peter, Eltis, Lindsay D, and Sim, Edith</p>

    <p>          Molecular Microbiology <b>2006</b>.  59(1): 181-192</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   48582   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          High Affinity InhA Inhibitors with Activity against Drug-Resistant Strains of Mycobacterium tuberculosis</p>

    <p>          Sullivan, Todd J, Truglio, James J, Boyne, Melissa E, Novichenok, Polina, Zhang, Xujie, Stratton, Christopher F, Li, Huei-Jiun, Kaur, Tejinder, Amin, Amol, Johnson, Francis, Slayden, Richard A, Kisker, Caroline, and Tonge, Peter J</p>

    <p>          ACS Chemical Biology <b>2006</b>.  1(1): 43-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   48583   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Pyridinylamines and their preparations, pharmaceutical compositions and use for prophylaxis or treatment of various diseases</p>

    <p>          Eickhoff, Jan Eike, Hafenbradl, Doris, Schwab, Wilfried, Cotton, Matthew, Klebl, Bert Matthias, Zech, Birgit, Mueller, Stefan, Harris, John, Savic, Vladimir, Macritchie, Jackie, Sherborne, Brad, and Le, Joelle</p>

    <p>          PATENT:  WO <b>2006010637</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2005  PP: 177 pp.</p>

    <p>          ASSIGNEE:  (Gpc Biotech AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   48584   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Concurrent antiviral and immunosuppressive activities of leflunomide in vivo</p>

    <p>          Chong, AS, Zeng, H, Knight, DA, Shen, J, Meister, GT, Williams, JW, and Waldman, WJ</p>

    <p>          American Journal of Transplantation <b>2006</b>.  6(1): 69-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   48585   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Antiviral methods and compositions</p>

    <p>          Coen, Donald M and Loregian, Arianna</p>

    <p>          PATENT:  WO <b>2006019955</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005</p>

    <p>          ASSIGNEE:  (President and Fellows of Harvard College, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   48586   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Synthesis and screening of bicyclic carbohydrate-based compounds: A novel type of antivirals</p>

    <p>          Van Hoof, Steven, Ruttens, Bart, Hubrecht, Idzi, Smans, Gert, Blom, Petra, Sas, Benedikt, Van Hemel, Johan, Vandenkerckhove, Jan, and Van der Eycken, Johan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(6): 1495-1498</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   48587   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Agents with leukotriene B4-like antiviral (enveloped RNA) activities</p>

    <p>          Gosselin, Jean and Borgeat, Pierre</p>

    <p>          PATENT:  US <b>2006025479</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2004-11016  PP: 28 pp., Cont.-in-part of U.S. Ser. No. 683,882, abandoned.</p>

    <p>          ASSIGNEE:  (Can.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   48588   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Cyclic and Acyclic 2-Methyl-3-hydroxy-4-pyridinone Nucleoside Derivatives</p>

    <p>          Barral, Karine, Balzarini, Jan, Neyts, Johan, De Clercq, Erik, Hider, Robert C, and Camplo, Michel</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(1): 43-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   48589   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          N-Substituted-sulfamoylbenzoic acid derivatives, particularly 4-[N-[4-(4-morpholino)phenyl]sulfamoyl]benzoic acid derivatives, method for preparing them, and antiviral pharmaceutical compositions comprising them, for treatment of hepatitis C virus infection</p>

    <p>          Kim, Jong Woo, Lee, Sang Wook, Lee, Geun Hyung, Han, Jae Jin, Park, Sang Jin, Park, Eul Yong, and Shin, Joong Chul</p>

    <p>          PATENT:  WO <b>2006011719</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2005  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (B &amp; C Biopharm Co., Ltd. S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   48590   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          2-(2-Thienyl)-5,6-dihydroxy-4-carboxypyrimidines as Inhibitors of the Hepatitis C Virus NS5B Polymerase: Discovery, SAR, Modeling, and Mutagenesis</p>

    <p>          Koch, Uwe, Attenni, Barbara, Malancona, Savina, Colarusso, Stefania, Conte, Immacolata, Di Filippo, Marcello, Harper, Steven, Pacini, Barbara, Giomini, Claudia, Thomas, Steven, Incitti, Ilario, Tomei, Licia, De Francesco, Raffaele, Altamura, Sergio, Matassa, Victor G, and Narjes, Frank</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(5): 1693-1705</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   48591   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Preparation of peptide analogs as hepatitis C inhibitors</p>

    <p>          Bailey, Murray D, Bhardwaj, Punit, Ghiro, Elise, Goudreau, Nathalie, Halmos, Teddy, Llinas-Brunet, Montse, Poupart, Marc-Andre, and Rancourt, Jean</p>

    <p>          PATENT:  WO <b>2006007708</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 112 pp.</p>

    <p>          ASSIGNEE:  (Boehringer Engelheim International GmbH, Germany and Boehringer Ingelheim Pharma Gmbh &amp; Co KG)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   48592   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Fused imidazole and thiazole derivatives as antiviral agents, their preparation, pharmaceutical compositions, and use for inhibiting hepatitis C virus polymerase</p>

    <p>          Conte, Immacolata, Hernando, Jose Ignacio Martin, Malancona, Savina, Ontoria Ontoria, Jesus Maria, and Stansfield, Ian</p>

    <p>          PATENT:  WO <b>2006008556</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti SpA, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   48593   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Heterocyclic antiviral compounds</p>

    <p>          Blake, James F, Fell, Jay Bradford, Fischer, John P, Hendricks, Robert Than, Robinson, John E, Spencer, Stacey Renee, and Stengel, Peter J</p>

    <p>          PATENT:  US <b>2006040927</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005-12354  PP: 50 pp., which</p>

    <p>          ASSIGNEE:  (Roche Palo Alto LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   48594   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Anti-viral nucleosides</p>

    <p>          Martin, Joseph Armstrong, Sarma, Keshab, and Smith, David Bernard</p>

    <p>          PATENT:  US <b>2006040890</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005-11902  PP: 22 pp.</p>

    <p>          ASSIGNEE:  (Roche Palo Alto LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   48595   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Cyanoacetic acid hydrazones of 3-(and 4-)acetylpyridine and some derived ring systems as potential antitumor and anti-HCV agents</p>

    <p>          El-Hawash, Soad AM, Abdel Wahab, Abeer E, and El-Demellawy, Maha A</p>

    <p>          Archiv der Pharmazie (Weinheim, Germany) <b>2006</b>.  339(1): 14-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>32.   48596   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Preparation of alkyl-substituted 2-deoxy-2-fluoro-D-ribofuranosyl pyrimidine and purine nucleoside analogs via condensation of the lactone to nucleosides as potential antiviral agents</p>

    <p>          Wang, Peiyuan, Stec, Wojciech, Clark, Jeremy, Chun, Byoung-Kwon, Shi, Junxing, and Du, Jinfa</p>

    <p>          PATENT:  WO <b>2006012440</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2005  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (Pharmasset, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   48597   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Preparation of nucleoside aryl phosphoramidates for use as an inhibitor of hepatitis C virus NS5B polymerase, RNA-dependent RNA polymerase, RNA viral replication and treating RNA-dependent RNA viral infections</p>

    <p>          Maccoss, Malcolm and Olsen, David B</p>

    <p>          PATENT:  WO <b>2006012078</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2005  PP: 40 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   48598   OI-LS-343; SCIFINDER-OI-2/27/2006</p>

    <p class="memofmt1-2">          Exploration of acyl sulfonamides as carboxylic acid replacements in protease inhibitors of the hepatitis C virus full-length NS3</p>

    <p>          Roenn, Robert, Sabnis, Yogesh A, Gossas, Thomas, Aakerblom, Eva, Danielson, UHelena, Hallberg, Anders, and Johansson, Anja</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(2): 544-559</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   48599   OI-LS-343; WOS-OI-2/26/2006</p>

    <p class="memofmt1-2">          Fluoroquinolones: An important class of antibiotics against tuberculosis</p>

    <p>          De Souzaa, MVN, Vasconcelos, TRA, de Almeida, MV, and Cardoso, SH</p>

    <p>          CURRENT MEDICINAL CHEMISTRY <b>2006</b>.  13(4): 455-463, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235298200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235298200006</a> </p><br />

    <p>36.   48600   OI-LS-343; WOS-OI-2/26/2006</p>

    <p class="memofmt1-2">          Novel yeast cell-based assay to screen for inhibitors of human cytomegalovirus protease in a high-throughput format</p>

    <p>          Cottier, V, Barberis, A, and Luthi, U</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(2): 565-571, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235294200022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235294200022</a> </p><br />

    <p>37.   48601   OI-LS-343; WOS-OI-3/5/2006</p>

    <p class="memofmt1-2">          Structural characterization and antimicrobial activity of 2- 5-H/methyl-1H-benzimidazol-2-yl)-4-bromo/nitro-phenol ligands and their Fe(NO3)(3) complexes</p>

    <p>          Tavman, A, Agh-Atabay, NM, Neshat, A, Gucin, F, Dulger, B, and Haciu, D</p>

    <p>          TRANSITION METAL CHEMISTRY <b>2006</b>.  31(2): 194-200, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235447200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235447200009</a> </p><br />
    <br clear="all">

    <p>38.   48602   OI-LS-343; WOS-OI-3/5/2006</p>

    <p class="memofmt1-2">          Application and utilization of chemoinformatics tools in lead generation and optimization</p>

    <p>          Fotouhi, N, Gillespie, P, Goodnow, RA, So, SS, Han, Y, and Babiss, LE</p>

    <p>          COMBINATORIAL CHEMISTRY &amp; HIGH THROUGHPUT SCREENING <b>2006</b>.  9(2): 95-102, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235468800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235468800004</a> </p><br />

    <p>39.   48603   OI-LS-343; WOS-OI-3/5/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of (E)-3-(nitrophenyl)-1-(pyrazin-2-yl)prop-2-en-1-ones</p>

    <p>          Opletalova, V, Pour, M, Kunes, J, Buchta, V, Silva, L, Kralova, K, Chlupacava, M, Meltrova, D, Peterka, M, and Poslednikova, M</p>

    <p>          COLLECTION OF CZECHOSLOVAK CHEMICAL COMMUNICATIONS <b>2006</b>.  71(1): 44-58, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235347200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235347200004</a> </p><br />

    <p>40.   48604   OI-LS-343; WOS-OI-3/5/2006</p>

    <p class="memofmt1-2">          Understanding the RNA-specificity of HCV RdRp: Implications for anti-HCV drug discovery</p>

    <p>          Kim, J and Chong, YH</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY <b>2006</b>.  27(1): 59-64, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235405700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235405700011</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
