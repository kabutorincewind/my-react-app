

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-316.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="FLKNmZnhtRif6rFEgtzOogGwUkt4+op6yzfMxnz8sA7pOs0C8cIotl07o8wlE0nqwLbIxpIWIGlen44Dfz8N8VHC7p/ik9lq87BJsfgzSv7INGWfM01eQhftZGg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="66DC37E6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-316-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44868   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Dual role of alpha-defensin-1 in anti-HIV-1 innate immunity</p>

    <p>          Chang, TL, Vargas, Jr J, Delportillo, A, and Klotman, ME</p>

    <p>          J Clin Invest <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15719067&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15719067&amp;dopt=abstract</a> </p><br />

    <p>2.         44869   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Synthesis and Evaluation of Double-Prodrugs against HIV. Conjugation of D4T with 6-Benzyl-1-(ethoxymethyl)-5-isopropyluracil (MKC-442, Emivirine)-Type Reverse Transcriptase Inhibitors via the SATE Prodrug Approach</p>

    <p>          Petersen, L, Jorgensen, PT, Nielsen, C, Hansen, TH, Nielsen, J, and Pedersen, EB</p>

    <p>          J Med Chem <b>2005</b>.  48(4): 1211-1220</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715487&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715487&amp;dopt=abstract</a> </p><br />

    <p>3.         44870   HIV-LS-316; EMBASE-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Potent inhibition of HIV-1 entry by (s4dU)35</p>

    <p>          Horvath, Andras, Tokes, Szilvia, Hartman, Tracy, Watson, Karen, Turpin, Jim A, Buckheit, Jr Robert W, Sebestyen, Zsolt, Szollosi, Janos, and Benko, Ilona</p>

    <p>          Virology <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4FHKCW8-3/2/d75306fc914360aacf18b6bd5a2f324f">http://www.sciencedirect.com/science/article/B6WXR-4FHKCW8-3/2/d75306fc914360aacf18b6bd5a2f324f</a> </p><br />

    <p>4.         44871   HIV-LS-316; EMBASE-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Aptamers directed to HIV-1 reverse transcriptase display greater efficacy over small hairpin RNAs targeted to viral RNA in blocking HIV-1 replication</p>

    <p>          Joshi, Pheroze J, North, Thomas W, and Prasad, Vinayaka R</p>

    <p>          Molecular Therapy <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WNJ-4FGX7YC-1/2/776e2bdf0f34e29cadf0d829852ca97b">http://www.sciencedirect.com/science/article/B6WNJ-4FGX7YC-1/2/776e2bdf0f34e29cadf0d829852ca97b</a> </p><br />

    <p>5.         44872   HIV-LS-316; EMBASE-HIV-2/23/2005</p>

    <p class="memofmt1-2">          The tyrosine kinases Fyn and Hck favor the recruitment of tyrosine-phosphorylated APOBEC3G into vif-defective HIV-1 particles</p>

    <p>          Douaisi, Marc, Dussart, Sylvie, Courcoul, Marianne, Bessou, Gilles, Lerner, Edwina C, Decroly, Etienne, and Vigne, Robert</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4FH0BCM-F/2/746b6bb5795f5252215ad567c7f1dca9">http://www.sciencedirect.com/science/article/B6WBK-4FH0BCM-F/2/746b6bb5795f5252215ad567c7f1dca9</a> </p><br />
    <br clear="all">

    <p>6.         44873   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type-1 (HIV-1) glycoprotein-mediated cell-cell fusion by immunor (IM28)</p>

    <p>          Mavoungou, D, Poaty-Mavoungou, V, Akoume, MY, Ongali, B, and Mavoungou, E</p>

    <p>          Virol J <b>2005</b>.  2(1): 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15707492&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15707492&amp;dopt=abstract</a> </p><br />

    <p>7.         44874   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Pharmacokinetics and pharmacodynamics of drug interactions involving HIV-1 protease inhibitors</p>

    <p>          Jackson, A, Taylor, S, and Boffito, M</p>

    <p>          AIDS Rev <b>2004</b>.  6(4): 208-217</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15700619&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15700619&amp;dopt=abstract</a> </p><br />

    <p>8.         44875   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Crystal structures of HIV-1 Tat derived nonapeptides Tat(1-9) and Trp2-Tat(1-9) bound to the active site of dipeptidyl peptidase IV (CD26)</p>

    <p>          Weihofen, WA, Liu, J, Reutter, W, Saenger, W, and Fan, H</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695814&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695814&amp;dopt=abstract</a> </p><br />

    <p>9.         44876   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Evaluation of two commercially available, inexpensive alternative assays used for assessing viral load in a cohort of human immunodeficiency virus type 1 subtype C-infected patients from South Africa</p>

    <p>          Stevens, G, Rekhviashvili, N, Scott, LE, Gonin, R, and Stevens, W</p>

    <p>          J Clin Microbiol <b>2005</b>.  43(2): 857-861</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695692&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695692&amp;dopt=abstract</a> </p><br />

    <p>10.       44877   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Lipodystrophy associated with highly active anti-retroviral therapy for HIV infection: the adipocyte as a target of anti-retroviral-induced mitochondrial toxicity</p>

    <p>          Villarroya, F, Domingo, P, and Giralt, M</p>

    <p>          Trends Pharmacol Sci <b>2005</b>.  26(2): 88-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681026&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681026&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44878   HIV-LS-316; PUBMED-HIV-2/23/2005</p>

    <p class="memofmt1-2">          Simultaneous determination of the HIV nucleoside analogue reverse transcriptase inhibitors lamivudine, didanosine, stavudine, zidovudine and abacavir in human plasma by reversed phase high performance liquid chromatography</p>

    <p>          Verweij-van, Wissen CP, Aarnoutse, RE, and Burger, DM</p>

    <p>          J Chromatogr B Analyt Technol Biomed Life Sci <b>2005</b>.  816(1-2): 121-129</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664342&amp;dopt=abstract</a> </p><br />

    <p>12.       44879   HIV-LS-316; WOS-HIV-2/13/2005</p>

    <p class="memofmt1-2">          Antigen-specific beta-chemokine production and CD8(+) T-cell noncytotoxic antiviral activity in HIV-2-infected individuals</p>

    <p>          Ahmed, RKS, Norrgren, H, da Silva, Z, Blaxhult, A, Fredriksson, EL, Biberfeld, G, Andersson, S, and Thorstensson, R</p>

    <p>          SCANDINAVIAN JOURNAL OF IMMUNOLOGY <b>2005</b>.  61(1): 63-71, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226378200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226378200008</a> </p><br />

    <p>13.       44880   HIV-LS-316; WOS-HIV-2/13/2005</p>

    <p class="memofmt1-2">          Expression, purification, and characterization of recombinant cyanovirin-N for vaginal anti-HIV microbicide development</p>

    <p>          Colleluori, DM, Tien, D, Kang, FR, Pagliei, T, Kuss, R, McCormick, T, Watson, K, McFadden, K, Chaiken, I, Buckheit, RW, and Romano, JW</p>

    <p>          PROTEIN EXPRESSION AND PURIFICATION <b>2005</b>.  39(2): 229-236, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226437000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226437000013</a> </p><br />

    <p>14.       44881   HIV-LS-316; WOS-HIV-2/13/2005</p>

    <p class="memofmt1-2">          An interlocked dimeric parallel-stranded DNA quadruplex: A potent inhibitor of HIV-1 integrase</p>

    <p>          Phan, AT, Kuryavyi, V, Ma, JB, Faure, A, Andreola, ML, and Patel, DJ</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2005</b>.  102(3): 634-639, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226436000022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226436000022</a> </p><br />

    <p>15.       44882   HIV-LS-316; WOS-HIV-2/13/2005</p>

    <p class="memofmt1-2">          Heterologous expression, characterization and structural studies of a hydrophobic peptide from the HIV-1 p24 protein</p>

    <p>          Castilho, PV, Campana, PT, Garcia, AF, Beltramini, LM, and Araujo, APU</p>

    <p>          PEPTIDES <b>2005</b>.  26(2): 243-249, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226429700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226429700009</a> </p><br />

    <p>16.       44883   HIV-LS-316; WOS-HIV-2/13/2005</p>

    <p class="memofmt1-2">          Efavirenz enhances the proteolytic processing of an HIV-1 pol polyprotein precursor and reverse transcriptase homodimer formation</p>

    <p>          Tachedjian, G, Moore, KL, Goff, SP, and Sluis-Cremer, N</p>

    <p>          FEBS LETTERS <b>2005</b>.  579(2): 379-384, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226406000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226406000013</a> </p><br />
    <br clear="all">

    <p>17.       44884   HIV-LS-316; WOS-HIV-2/13/2005</p>

    <p class="memofmt1-2">          Capillary zone electrophoretic separation of four novel compounds active against human immunodeficiency virus type 1</p>

    <p>          Ding, L, Zhang, XX, Chang, WB, Lin, W, and Yang, M</p>

    <p>          CHROMATOGRAPHIA <b>2005</b>.  61(1-2): 85-87, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226506000014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226506000014</a> </p><br />

    <p>18.       44885   HIV-LS-316; WOS-HIV-2/13/2005</p>

    <p class="memofmt1-2">          QSAR studies of HIV-1 protease inhibitors</p>

    <p>          Garg, R</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  227: U908-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655603247">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655603247</a> </p><br />

    <p>19.       44886   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Inter-subunit disulfide bonds in soluble HIV-1 envelope glycoprotein trimers</p>

    <p>          Yuan, W, Craig, S, Yang, XZ, and Sodroski, J</p>

    <p>          VIROLOGY <b>2005</b>.  332(1): 369-383, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226688500035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226688500035</a> </p><br />

    <p>20.       44887   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Comparative analysis of HIV-1 Tat variants</p>

    <p>          Pantano, S and Carloni, P</p>

    <p>          PROTEINS-STRUCTURE FUNCTION AND BIOINFORMATICS <b>2005</b>.  58(3 ): 638-643, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226695900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226695900013</a> </p><br />

    <p>21.       44888   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Advances in laboratory testing for HIV</p>

    <p>          Dax, EM and Arnott, A</p>

    <p>          PATHOLOGY <b>2004</b>.  36(6): 551-560, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226482600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226482600004</a> </p><br />

    <p>22.       44889   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Phenolics with antiviral activity from Millettia erythrocalyx and Artocarpus lakoocha</p>

    <p>          Likhitwitayawuid, K, Sritularak, B, Benchanak, K, Lipipun, V, Mathew, J, and Schinazi, RF</p>

    <p>          NATURAL PRODUCT RESEARCH <b>2005</b>.  19(2): 177-182, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226668800013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226668800013</a> </p><br />

    <p>23.       44890   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          A new bis-andrographolide ether from Androgphis paniculata Nees and evaluation of anti-HIV activity</p>

    <p>          Reddy, VLN, Reddy, SM, Ravikanth, V, Krishnaiah, P, Goud, TV, Rao, TP, Ram, TS, Gonnade, RG, Bhadbhade, M, and Venkateswarlu, Y</p>

    <p>          NATURAL PRODUCT RESEARCH <b>2005</b>.  19(3): 223-230, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226669000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226669000004</a> </p><br />
    <br clear="all">

    <p>24.       44891   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Anti-CXCR4 monoclonal antibodies recognizing overlapping epitopes differ significantly in their ability to inhibit entry of human immunodeficiency virus type 1</p>

    <p>          Carnec, X, Quan, L, Olson, WC, Hazan, U, and Dragic, T</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(3): 1930-1933, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226634300059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226634300059</a> </p><br />

    <p>25.       44892   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Stereoselective synthesis of [L-Arg-L/D-3-(2-naphthyl) alanine]-type (E)-alkene dipeptide isosteres and its application to the synthesis and biological evaluation of pseudopeptide analogues of the CXCR4 antagonist FC131</p>

    <p>          Tamamura, H, Hiramatsu, K, Ueda, S, Wang, ZX, Kusano, S, Terakubo, S, Trent, JO, Peiper, SC, Yamamoto, N, Nakashima, H, Otaka, A, and Fujii, N</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(2): 380-391, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226591700008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226591700008</a> </p><br />

    <p>26.       44893   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of cis-substituted cyclohexenyl and cyclohexanyl-nucleosides</p>

    <p>          Barral, K, Courcambeck, J, Pepe, G, Balzarini, J, Neyts, J, De Clercq, E, and Camplo, M</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(2): 450-456, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226591700014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226591700014</a> </p><br />

    <p>27.       44894   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          HIV-1 Vpr inhibits the maturation and activation of macrophages and dendritic cells in vitro</p>

    <p>          Muthumani, K, Hwang, DS, Choo, AY, Mayilvahanan, S, Dayes, NS, Thieu, KP, and Weiner, DB</p>

    <p>          INTERNATIONAL IMMUNOLOGY <b>2005</b>.  17(2): 103-116, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226477300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226477300001</a> </p><br />

    <p>28.       44895   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of peptide mimics derived from first extracellular loop of CCR5 toward HIV-1</p>

    <p>          Ikeda, K, Ishii, Y, Konishi, K, Sato, M, and Tanaka, K</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2004</b>.  52(12): 1479-1482, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226646600018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226646600018</a> </p><br />

    <p>29.       44896   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Combinations of nucleoside/nucleotide analogues for HIV therapy</p>

    <p>          Barreiro, P, Garcia-Benayas, T, Rendon, A, Rodriguez-Novoa, S, and Soriano, V</p>

    <p>          AIDS REVIEWS <b>2004</b>.  6(4): 234-243, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226677500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226677500006</a> </p><br />

    <p>30.       44897   HIV-LS-316; WOS-HIV-2/20/2005</p>

    <p class="memofmt1-2">          Evolution of a novel class of HIV-1 integrase inhibitors</p>

    <p>          Gomez, RP</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U123-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800600">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800600</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
