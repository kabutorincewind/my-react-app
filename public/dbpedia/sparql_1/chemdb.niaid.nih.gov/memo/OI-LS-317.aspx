

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-317.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q3RlhpDOoTxi+WjDPwT9BW8x4oubQw4irsDR5k4EK5NgcBLI2ikWeD4MVBZJjz71wPG2Z5RZ7yXl4tmF2IIb0eFvVXe7Rwf2xjidvksTmgMmEIjiErdRc3g93dI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6B03780B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-317-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44965   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Cloning and characterization of aspartate-beta-semialdehyde dehydrogenase from Mycobacterium tuberculosis H37 Rv</p>

    <p>          Shafiani, S, Sharma, P, Vohra, RM, and Tewari, R</p>

    <p>          J Appl Microbiol <b>2005</b>.  98(4): 832-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752328&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752328&amp;dopt=abstract</a> </p><br />

    <p>2.         44966   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          The Mycobacterium tuberculosis iniA gene is essential for activity of an efflux pump that confers drug tolerance to both isoniazid and ethambutol</p>

    <p>          Colangeli, R, Helb, D, Sridharan, S, Sun, J, Varma-Basil, M, Hazbon, MH, Harbacheuski, R, Megjugorac, NJ, Jacobs, WR Jr, Holzenburg, A, Sacchettini, JC, and Alland, D</p>

    <p>          Mol Microbiol <b>2005</b>.  55(6): 1829-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752203&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752203&amp;dopt=abstract</a> </p><br />

    <p>3.         44967   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Crystal structures of the RNA dependent RNA polymerase genotype 2a of hepatitis C virus reveal two conformations and suggest mechanisms of inhibition by non-nucleoside inhibitors</p>

    <p>          Biswal, BK, Cherney, MM, Wang, M, Chan, L, Yannopoulos, CG, Bilimoria, D, Nicolas, O, Bedard, J, and James, MN</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15746101&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15746101&amp;dopt=abstract</a> </p><br />

    <p>4.         44968   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Toward a new therapy for tuberculosis</p>

    <p>          Rubin, EJ</p>

    <p>          N Engl J Med <b>2005</b>.  352(9): 933-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745987&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745987&amp;dopt=abstract</a> </p><br />

    <p>5.         44969   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Identification of small molecule inhibitors of the hepatitis C virus RNA-dependent RNA polymerase from a pyrrolidine combinatorial mixture</p>

    <p>          Burton, G, Ku, TW, Carr, TJ, Kiesow, T, Sarisky, RT, Lin-Goerke, J, Baker, A, Earnshaw, DL, Hofmann, GA, Keenan, RM, and Dhanak, D</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(6): 1553-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745795&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745795&amp;dopt=abstract</a> </p><br />

    <p>6.         44970   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          An overview of the antifungal properties of allicin and its breakdown products - the possibility of a safe and effective antifungal prophylactic</p>

    <p>          Davis, SR</p>

    <p>          Mycoses <b>2005</b>.  48(2): 95-100</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743425&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743425&amp;dopt=abstract</a> </p><br />

    <p>7.         44971   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Comparative evaluation of the nitrate reduction assay, the MTT test, and the resazurin microtitre assay for drug susceptibility testing of clinical isolates of Mycobacterium tuberculosis</p>

    <p>          Montoro, E, Lemus, D, Echemendia, M, Martin, A, Portaels, F, and Palomino, JC</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731200&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731200&amp;dopt=abstract</a> </p><br />

    <p>8.         44972   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Human cytomegalovirus resistance to antiviral drugs</p>

    <p>          Gilbert, C and Boivin, G</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 873-83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728878&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728878&amp;dopt=abstract</a> </p><br />

    <p>9.         44973   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Synthesis of 2,4-bis(alkylamino)pyrimidines and their use as antimicrobials</p>

    <p>          Marquais-Bienewald, Sophie, Hoelzl, Werner, Preuss, Andrea, and Mehlin, Andreas</p>

    <p>          PATENT:  WO <b>2005011758</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004  PP: 38 pp.</p>

    <p>          ASSIGNEE:  (Ciba Specialty Chemicals Holding Inc., Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.       44974   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Antifungal susceptibilities of clinical isolates of Candida species, Cryptococcus neoformans, and Aspergillus species from Taiwan: Surveillance of Multicenter Antimicrobial Resistance in Taiwan program data from 2003</p>

    <p>          Hsueh, Po-Ren, Lau, Yeu-Jun, Chuang, Yin-Ching, Wan, Jen-Hsien, Huang, Wen-Kuei, Shyr, Jainn-Ming, Yan, Jing-Jou, Yu, Kwok-Woon, Wu, Jiunn-Jong, Ko, Wen-Chien, Yang, Yi-Chueh, Liu, Yung-Ching, Teng, Lee-Jene, Liu, Cheng-Yi, and Luh, Kwen-Tay</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(2): 512-517</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.       44975   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Influence of indinavir on virulence and growth of Cryptococcus neoformans</p>

    <p>          Monari, Claudia, Pericolini, Eva, Bistoni, Giovanni, Cenci, Elio, Bistoni, Francesco, and Vecchiarelli, Anna</p>

    <p>          Journal of Infectious Diseases <b>2005</b>.  191(2): 307-311</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.       44976   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Methioninase, alone or in combination with other agents, for the treatment of Pneumocystis carinii infection</p>

    <p>          Tan, Yuying, Yang, Zhijian, Sun, Xinghua, Li, Shukuan, Han, Qinghong, and Xu, Mingxu</p>

    <p>          PATENT:  WO <b>2005000239</b>  ISSUE DATE:  20050106</p>

    <p>          APPLICATION: 2004  PP: 11 pp.</p>

    <p>          ASSIGNEE:  (Anticancer, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.       44977   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Sequences of a Mycobacterium tuberculosis Pknb protein kinase and a pstP2 phosphatase and use for screening bactericides</p>

    <p>          Alzari, Pedro, Boitel, Brigitte, Villarino, Andrea, Fernandez, Pablo, and Cole, Stewart</p>

    <p>          PATENT:  WO <b>2005007880</b>  ISSUE DATE:  20050127</p>

    <p>          APPLICATION: 2004  PP: 64 pp.</p>

    <p>          ASSIGNEE:  (Institut Pasteur, Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.       44978   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Siderocalin (Lcn 2) Also Binds Carboxymycobactins, Potentially Defending against Mycobacterial Infections through Iron Sequestration</p>

    <p>          Holmes, Margaret A, Paulsene, Wendy, Jide, Xu, Ratledge, Colin, and Strong, Roland K</p>

    <p>          Structure (Cambridge, MA, United States) <b>2005</b>.  13(1): 29-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.       44979   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Development and Preliminary Optimization of Indole-N-Acetamide Inhibitors of Hepatitis C Virus NS5B Polymerase</p>

    <p>          Harper, S, Pacini, B, Avolio, S, Di, Filippo M, Migliaccio, G, Laufer, R, De, Francesco R, Rowley, M, and Narjes, F</p>

    <p>          J Med Chem <b>2005</b>.  48(5): 1314-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743173&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743173&amp;dopt=abstract</a> </p><br />

    <p>16.       44980   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of some N,N&#39;-disubstituted isonicotinohydrazide derivatives</p>

    <p>          Sinha, Neelima, Jain, Sanjay, Tilekar, Ajay, Upadhayaya, Ram S, Kishore, Nawal, Sinha, Rakesh K, and Arora, Sudershan K</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2005</b>.(2): 9-19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.       44981   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Replicon cell culture system as a valuable tool in antiviral drug discovery against hepatitis C virus</p>

    <p>          Horscroft, N, Lai, VC, Cheney, W, Yao, N, Wu, JZ, Hong, Z, and Zhong, W</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(1): 1-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739617&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739617&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.       44982   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Elemental Analysis of Mycobacterium avium-, Mycobacterium tuberculosis-, and Mycobacterium smegmatis-Containing Phagosomes Indicates Pathogen-Induced Microenvironments within the Host Cell&#39;s Endosomal System</p>

    <p>          Wagner, Dirk, Maser, Joerg, Lai, Barry, Cai, Zhonghou, Barry, Clifton E III, Hoener zu Bentrup, Kerstin, Russell, David G, and Bermudez, Luiz E</p>

    <p>          Journal of Immunology <b>2005</b>.  174(3): 1491-1500</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.       44983   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Evaluation of a rapid differentiation test for the Mycobacterium tuberculosis complex by selective inhibition with rho-nitrobenzoic acid and thiophene-2-carboxylic acid hydrazide</p>

    <p>          Giampaglia, CM, Martins, MC, Inumaru, VT, Butuem, IV, and Telles, MA</p>

    <p>          Int J Tuberc Lung Dis <b>2005</b>.  9(2): 206-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15732742&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15732742&amp;dopt=abstract</a> </p><br />

    <p>20.       44984   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Use of fluorescence resonance energy transfer for rapid detection of isoniazid resistance in Mycobacterium tuberculosis clinical isolates</p>

    <p>          Saribas, Z, Yurdakul, P, Alp, A, and Gunalp, A</p>

    <p>          Int J Tuberc Lung Dis <b>2005</b>.  9(2): 181-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15732738&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15732738&amp;dopt=abstract</a> </p><br />

    <p>21.       44985   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Lambda interferon inhibits hepatitis B and C virus replication</p>

    <p>          Robek, MD, Boyd, BS, and Chisari, FV</p>

    <p>          J Virol <b>2005</b>.  79(6): 3851-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731279&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731279&amp;dopt=abstract</a> </p><br />

    <p>22.       44986   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Small-Molecule Inhibitors of the Budded-to-Hyphal-Form Transition in the Pathogenic Yeast Candida albicans</p>

    <p>          Toenjes, KA, Munsee, SM, Ibrahim, AS, Jeffrey, R, Edwards, JE Jr, and Johnson, DI</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 963-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728890&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728890&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.       44987   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Synthesis, X-ray Crystal Structure Study, and Cytostatic and Antiviral Evaluation of the Novel Cycloalkyl-N-aryl-hydroxamic Acids</p>

    <p>          Barbaric, Monika, Ursic, Stanko, Pilepic, Viktor, Zorc, Branka, Hergold-Brundic, Antonija, Nagl, Ante, Grdisa, Mira, Pavelic, Kresimir, Snoeck, Robert, Andrei, Graciela, Balzarini, Jan, De Clercq, Erik, and Mintas, Mladen</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(3): 884-887</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.       44988   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          NF-kB activation can mediate inhibition of human cytomegalovirus replication</p>

    <p>          Eickhoff, Jan Eike and Cotten, Matt</p>

    <p>          Journal of General Virology <b>2005</b>.  86(2): 285-295</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.       44989   OI-LS-317; PUBMED-OI-3/9/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus NS2 protein is phosphorylated by the protein kinase CK2 and targeted for degradation to the proteasome</p>

    <p>          Franck, N, Le Seyec, J, Guguen-Guillouzo, C, and Erdtmann, L</p>

    <p>          J Virol <b>2005</b> .  79(5): 2700-2708</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708989&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708989&amp;dopt=abstract</a> </p><br />

    <p>26.       44990   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Preparation of phenylalanine derivatives as anti-HCV agents</p>

    <p>          Sudoh, Masayuki, Tsukuda, Takuo, Masubuchi, Miyako, Kawasaki, Kenichi, Murata, Takeshi, Watanabe, Fumio, Fukuda, Hiroshi, Komiyama, Susumu, and Hayase, Tadakatsu</p>

    <p>          PATENT:  WO <b>2005005372</b>  ISSUE DATE:  20050120</p>

    <p>          APPLICATION: 2004  PP: 120 pp.</p>

    <p>          ASSIGNEE:  (Chugai Seiyaku Kabushiki Kaisha, Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.       44991   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Preparation of modified fluorinated (2&#39;R)-2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methyl nucleoside analogs as antiviral agents</p>

    <p>          Clark, Jeremy</p>

    <p>          PATENT:  WO <b>2005003147</b>  ISSUE DATE:  20050113</p>

    <p>          APPLICATION: 2004  PP: 228 pp.</p>

    <p>          ASSIGNEE:  (Pharmasset, Ltd. Barbados</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.       44992   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Molecular models of NS3 protease variants of the hepatitis C virus</p>

    <p>          da Silveira, Nelson JF, Arcuri, Helen A, Bonalumi, Carlos E, de Souza, Fatima P, Mello, Isabel MVGC, Rahal, Paula, Pinho, Joao RR, and de Azevedo, Walter F Jr</p>

    <p>          BMC Structural Biology <b>2005</b>.  5: No pp. given</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>29.       44993   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Colorimetric lactate dehydrogenase (LDH) assay for evaluation of antiviral activity against bovine viral diarrhoea virus (BVDV) in vitro</p>

    <p>          Baba, Chiaki, Yanagida, Koichiro, Kanzaki, Tamotsu, and Baba, M</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2005</b>.  16(1): 33-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.       44994   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          A preparation of bicyclic imidazole derivatives, useful for the treatment of viral infections mediated by flaviviridae family of viruses</p>

    <p>          Schmitz, Franz Ulrich, Roberts, Christopher Don, Griffith, Ronald Conrad, Botyanszki, Janos, Gezginci, Mikail Hakan, Gralapp, Joshua Michael, Shi, Dong Fang, and Liehr, Sebastian JR</p>

    <p>          PATENT:  WO <b>2005012288</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004  PP: 327 pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.       44995   OI-LS-317; SCIFINDER-OI-3/1/2005</p>

    <p class="memofmt1-2">          Glyco- and Peptidomimetics from Three-Component Joullie-Ugi Coupling Show Selective Antiviral Activity</p>

    <p>          Chapman, Timothy M, Davies, Ieuan G, Gu, Baohua, Block, Timothy M, Scopes, David IC, Hay, Philip A, Courtney, Stephen M, McNeill, Luke A, Schofield, Christopher J, and Davis, Benjamin G</p>

    <p>          Journal of the American Chemical Society <b>2005</b>.  127(2): 506-507</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.       44996   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Synthesis and ANTI-HCMV activity of novel 2 &#39;,3 &#39;,4 &#39;-trimethyl branched carbocyclic nucleosides</p>

    <p>          Kim, A and Hong, JH</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(1): 63-72, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226785500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226785500005</a> </p><br />

    <p>33.       44997   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Safe susceptibility testing of Mycobacterium tuberculosis by flow cytometry with the fluorescent nucleic acid stain SYTO 16</p>

    <p>          Pina-Vaz, C, Costa-de-Oliveira, S, and Rodrigues, AG</p>

    <p>          JOURNAL OF MEDICAL MICROBIOLOGY <b>2005</b>.  54(1): 77-81, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226677400013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226677400013</a> </p><br />

    <p>34.       44998   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Drug resistance in tuberculosis</p>

    <p>          Mitchison, DA</p>

    <p>          EUROPEAN RESPIRATORY JOURNAL <b>2005</b>.  25(2): 376-379, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226714800025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226714800025</a> </p><br />

    <p>35.       44999   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Targeting DHFR in parasitic protozoa</p>

    <p>          Anderson, AC</p>

    <p>          DRUG DISCOVERY TODAY <b>2005</b>.  10(2): 121-128, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226875800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226875800009</a> </p><br />
    <br clear="all">

    <p>36.       45000   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Single-nucleotide polymorphism-based differentiation and drug resistance detection in Mycobacterium tuberculosis from isolates or directly from sputum</p>

    <p>          Arnold, C, Westland, L, Mowat, G, Underwood, A, Magee, J, and Gharbia, S</p>

    <p>          CLINICAL MICROBIOLOGY AND INFECTION <b>2005</b>.  11(2): 122-130, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226641300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226641300007</a> </p><br />

    <p>37.       45001   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of noble 5-arylamino- and 6-arylthio-4,7-dioxobenzoselenazoles</p>

    <p>          Ryu, CK, Han, JY, Jung, OJ, Lee, SK, Lee, JY, and Jeong, SH</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(3): 679-682, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700037</a> </p><br />

    <p>38.       45002   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Synthesis of 9-(2-beta-C-methyl-beta-D-ribofuranosyl)-6-substituted purine derivatives as inhibitors of HCV RNA replication</p>

    <p>          Ding, YL, Girardet, JL, Hong, Z, Lai, VCH, An, HY, Koh, YH, Shaw, SZ, and Zhong, WD</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(3): 709-713, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700043</a> </p><br />

    <p>39.       45003   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Antimycobacterial compounds. Optimization of the BM 212 structure, the lead compound for a new pyrrole derivative class</p>

    <p>          Biava, M, Porretta, GC, Poce, G, Deidda, D, Pompel, R, Tafi, A, and Manetti, F</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(4): 1221-1230, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226967900030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226967900030</a> </p><br />

    <p>40.       45004   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          2,3-bis(5-alkyl-2-thiono-1,3,5-thiadiazin-3-yl) propionic acid: One-pot Domino Synthesis and antimicrobial activity</p>

    <p>          El Bialy, SAA, Abdelal, AM, Ei-Shorbagi, AN, and Kheira, SMM</p>

    <p>          ARCHIV DER PHARMAZIE <b>2005</b>.  338(1): 38-43, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226986400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226986400005</a> </p><br />

    <p>41.       45005   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Interaction between posaconazole and amphotericin B in concomitant treatment against Candida albicans in vivo</p>

    <p>          Cacciapuoti, A, Gurnani, M, Halpern, J, Norris, C, Patel, R, and Loebenberg, D</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(2): 638-642, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900025</a> </p><br />

    <p>42.       45006   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Comparison of the antiviral activities of alkoxyalkyl and alkyl esters of cidofovir against human and murine cytomegalovirus replication in vitro</p>

    <p>          Wan, WB, Beadle, JR, Hartline, C, Kern, ER, Ciesla, SL, Valiaeva, N, and Hostetler, KY</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(2): 656-662, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900028</a> </p><br />

    <p>43.       45007   OI-LS-317; WOS-OI-2/27/2005</p>

    <p class="memofmt1-2">          Altered NADH/NAD(+) ratio mediates coresistance to isoniazid and ethionamide in mycobacteria</p>

    <p>          Vilcheze, C, Weisbrod, TR, Chen, B, Kremer, L, Hazbon, MH, Wang, F, Alland, D, Sacchettini, JC, and Jacobs, WR</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(2): 708-720, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900035</a> </p><br />

    <p>44.       45008   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity of N-alkoxycarbonylmethyl-N-alkyl-piperidinium chlorides</p>

    <p>          Wozniak, E, Mozrzymas, A, Czarny, A, Kocieba, M, Rozycka-Roszak, B, Dega-Szafran, Z, Dulewicz, E, and Petryna, M</p>

    <p>          ZEITSCHRIFT FUR NATURFORSCHUNG C-A JOURNAL OF BIOSCIENCES <b>2004</b>.  59(11-12): 782-786, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227105100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227105100003</a> </p><br />

    <p>45.       45009   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Studies on the 4-benzoylpyridine-3-carboxamide entity as a fragment model of the Isoniazid-NAD adduct</p>

    <p>          Broussy, S, Bernardes-Genisson, V, Gornitzka, H, Bernadou, J, and Meunier, B</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2005</b>.  3(4): 666-669, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226876800020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226876800020</a> </p><br />

    <p>46.       45010   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Controlling gene expression in mycobacteria with anhydrotetracycline and Tet repressor</p>

    <p>          Ehrt, S, Guo, XZV, Hickey, CM, Ryou, M, Monteleone, M, Riley, LW, and Schnappinger, D</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2005</b>.  33(2): 11</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226941000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226941000011</a> </p><br />

    <p>47.       45011   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Tetracycline-inducible gene regulation in mycobacteria</p>

    <p>          Blokpoel, MCJ, Murphy, HN, O&#39;Toole, R, Wiles, S, Runn, ESC, Stewart, GR, Young, DB, and Robertson, BD</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2005</b>.  33(2): 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226941000012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226941000012</a> </p><br />

    <p>48.       45012   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Shortening the treatment of tuberculosis</p>

    <p>          Mitchison, DA</p>

    <p>          NATURE BIOTECHNOLOGY <b>2005</b>.  23(2): 187-188, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226797600018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226797600018</a> </p><br />

    <p>49.       45013   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Genetic analysis of the beta-lactamases of Mycobacterium tuberculosis and Mycobacterium smegmatis and susceptibility to beta-lactam antibiotics</p>

    <p>          Flores, AR, Parsons, LM, and Pavelka, MS</p>

    <p>          MICROBIOLOGY-SGM <b>2005</b>.  151: 521-532, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227031000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227031000018</a> </p><br />

    <p>50.       45014   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Acetophenones with selective antimycobacterial activity</p>

    <p>          Rajabi, L, Courreges, C, Montoya, J, Aguilera, RJ, and Primm, TP</p>

    <p>          LETTERS IN APPLIED MICROBIOLOGY <b>2005</b>.  40(3): 212-217, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227018000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227018000010</a> </p><br />

    <p>51.       45015   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Synthesis and evaluation of S-acyl-2-thioethyl esters of modified nucleoside 5&#39;-monophosphates as inhibitors of hepatitis C virus RNA replication</p>

    <p>          Prakash, TP, Prhavc, M, Eldrup, AB, Cook, PD, Carroll, SS, Olsen, DB, Stahlhut, MW, Tomassini, JE, MacCoss, M, Galloway, SM, Hilliard, C, and Bhat, B</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(4): 1199-1210, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227115500030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227115500030</a> </p><br />

    <p>52.       45016   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Drug resistance and fitness in Mycobacterium tuberculosis infection - Reply</p>

    <p>          Burgos, M, DeRiemer, K, Small, PM, Hopewell, PC, and Daley, CL</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2005</b>.  191(5): 824-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226862400035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226862400035</a> </p><br />

    <p>53.       45017   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Treatment of hepatitis C virus genotype 2 and 3 with pegylated interferon plus ribavirin - Reply</p>

    <p>          Zeuzem, S</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2005</b>.  42(2): 276-277, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226984400020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226984400020</a> </p><br />

    <p>54.       45018   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Molecular epidemiology of tuberculosis</p>

    <p>          Narayanan, S</p>

    <p>          INDIAN JOURNAL OF MEDICAL RESEARCH <b>2004</b>.  120(4): 233-247, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226986500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226986500004</a> </p><br />

    <p>55.       45019   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Tripeptide inhibitors of the hepatitis C virus serine protease</p>

    <p>          Ghiro, E, Bailey, M, Gorys, V, Goudreau, N, Halmos, T, Poirier, M, Rancourt, J, and Llinas-Brunet, M</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U932-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803966">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803966</a> </p><br />

    <p>56.       45020   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Nucleoside analogues as agents for the treatment of hepatitis B and C</p>

    <p>          Storer, R</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U937-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803997">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803997</a> </p><br />
    <br clear="all">

    <p>57.       45021   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          Methylenecyclopropane analogues of purine nucleosides as agents against drug-resistant cytomegalovirus</p>

    <p>          Zemlicka, J and Drach, JC</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U937-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803999">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803999</a> </p><br />

    <p>58.       45022   OI-LS-317; WOS-OI-3/6/2005</p>

    <p class="memofmt1-2">          CoMFA and CoMSIA analyses of Pneumocystis carinii, Toxoplasma gondii and rat liver dihydrofolate reducase (DHFR) inhibitors</p>

    <p>          Gangjee, A and Lin, X</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U946-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712804044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712804044</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
