

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-329.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="IXnMTGGE0LyHUkdWmoHRwsJRF6F6QFS86X6HcncseGcRdTYVuwRPnIApvhos0tu+p5kEqkToa2BQYxcI+rF0jFZQANnrGnLeXSsXWXJajOtmqvlEIouOpV6d3zA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9C29A11D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-329-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47765   HIV-LS-329; PUBMED-HIV-8/23/2005</p>

    <p class="memofmt1-2">          Novel 8-Substituted Dipyridodiazepinone Inhibitors with a Broad-Spectrum of Activity against HIV-1 Strains Resistant to Non-nucleoside Reverse Transcriptase Inhibitors</p>

    <p>          O&#39;meara, JA, Yoakim, C, Bonneau, PR, Bos, M, Cordingley, MG, Deziel, R, Doyon, L, Duan, J, Garneau, M, Guse, I, Landry, S, Malenfant, E, Naud, J, Ogilvie, WW, Thavonekham, B, and Simoneau, B</p>

    <p>          J Med Chem <b>2005</b>.  48(17): 5580-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107158&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107158&amp;dopt=abstract</a> </p><br />

    <p>2.     47766   HIV-LS-329; PUBMED-HIV-8/23/2005</p>

    <p class="memofmt1-2">          Effect of naturally-occurring gp41 HR1 variations on susceptibility of HIV-1 to fusion inhibitors</p>

    <p>          Chinnadurai, R, Munch, J, and Kirchhoff, F</p>

    <p>          AIDS <b>2005</b>.  19 (13): 1401-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16103771&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16103771&amp;dopt=abstract</a> </p><br />

    <p>3.     47767   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          HIV protease inhibitors with potential for once-daily dosing and reduced side effects</p>

    <p>          DeGoey, David A, Flosi, William J, Grampovnik, David J, Flentge, Charles A, Klein, Larry L, Yeung, Clinton M, Dekhtyar, Tatyana, Colletti, Lynn, Waring, Jeffrey F, Ciurlionis, Rita, Marsh, Kennan C, Schmidt, James M, Swanson, Sue J, Stoll, Vincent, Mamo, Mulugeta, Mo, Hongmei, Kati, Warren M, Molla, Akhteruzzaman, and Kempf, Dale J</p>

    <p>          Abstracts of Papers, 230th ACS National Meeting, Washington, DC, United States, Aug. 28-Sept. 1, 2005 <b>2005</b>.: MEDI-189</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     47768   HIV-LS-329; PUBMED-HIV-8/23/2005</p>

    <p class="memofmt1-2">          A non-natural dinucleotide containing an isomeric L-related deoxynucleoside: dinucleotide inhibitors of anti-HIV integrase activity</p>

    <p>          Newton, MG, Campana, CF, Chi, GC, Lee, D, Liu, ZJ, Nair, V, Phillips, J, Rose, JP, and Wang, BC</p>

    <p>          Acta Crystallogr C <b>2005</b>.  61(Pt 8): 518-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16082106&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16082106&amp;dopt=abstract</a> </p><br />

    <p>5.     47769   HIV-LS-329; PUBMED-HIV-8/23/2005</p>

    <p class="memofmt1-2">          HIV-1 protease inhibitor induces growth arrest and apoptosis of human prostate cancer LNCaP cells in vitro and in vivo in conjunction with blockade of androgen receptor STAT3 and AKT signaling</p>

    <p>          Yang, Y, Ikezoe, T, Takeuchi, T, Adachi, Y, Ohtsuki, Y, Takeuchi, S, Koeffler, HP, and Taguchi, H</p>

    <p>          Cancer Sci <b>2005</b>.  96(7): 425-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16053514&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16053514&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47770   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity from medicinal mushrooms</p>

    <p>          Stamets, Paul</p>

    <p>          PATENT:  WO <b>2005067955</b>  ISSUE DATE:  20050728</p>

    <p>          APPLICATION: 2005</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     47771   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Allophenylnorstatine-containing HIV-1 protease inhibitors: design, synthesis and structure-activity relationships for selected P2 ligands</p>

    <p>          Abdel-Rahman, Hamdy M, El-Koussi, Nawal A, Alkaramany, Gamal S, Youssef, Adel F, and Kiso, Yoshiaki</p>

    <p>          Bulletin of Pharmaceutical Sciences, Assiut University <b>2005</b>.  28(1): 95-103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     47772   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Preparation of amino acid hydrazide derivatives as HIV protease inhibitors</p>

    <p>          Randolph, John T, Chen, Hui-ju, Degoey, David A, Flentge, Charles A, Flosi, William J, Grampovnik, David J, Huang, Peggy P, Hutchinson, Douglas K, Kempf, Dale J, Klein, Larry L, and Yeung, Ming C</p>

    <p>          PATENT:  US <b>20050159469</b>  ISSUE DATE: 20050721</p>

    <p>          APPLICATION: 2004-10177  PP: 155 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     47773   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Preparation of non-nucleotide reverse transcriptase inhibitors</p>

    <p>          Sund, Christian, Roue, Nathalie, Lindstroem, Stefan, Antonov, Dmitry, Sahlberg, Christer, and Jansson, Katarina</p>

    <p>          PATENT:  WO <b>2005066131</b>  ISSUE DATE:  20050721</p>

    <p>          APPLICATION: 2004  PP: 118 pp.</p>

    <p>          ASSIGNEE:  (Medivir AB, Swed.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   47774   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Preparation of phosphonate analogs of HIV protease inhibitors and methods for identifying anti-HIV therapeutic compounds</p>

    <p>          Arimilli, Murty N, Becker, Mark M, Birkus, Gabriel, Bryant, Clifford, Chen, James M, Chen, Xiaowu, Cihlar, Tomas, Dastgah, Azar, Eisenberg, Eugene J, Fardis, Maria, Hatada, Marcos, He, Gong-Xin, Jin, Haolun, Kim, Choung U, Lee, William A, Lee, Christopher P, Lin, Kuei-Ying, Liu, Hongtao, Mackman, Richard L, McDermott, Martin J, Mitchell, Michael L, Nelson, Peter H, Pyun, Hyung-Jung, Rowe, Tanisha D, Sparacino, Mark, Swaminathan, Sundaramoorthi, Tario, James D, Wang, Jianying, Williams, Matthew A, Xu, Lianhong, Yang, Zheng-Yu, Yu, Richard H, Zhang, Jiancun, and Zhang, Lijun</p>

    <p>          PATENT:  WO <b>2005064008</b>  ISSUE DATE:  20050714</p>

    <p>          APPLICATION: 2004  PP: 1723 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   47775   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Preparation of amino acid derivatives as HIV protease inhibitors</p>

    <p>          Degoey, David A, Flentge, Charles A, Flosi, William J, Grampovnik, David J, Kempf, Dale J, Klein, Larry L, Yeung, Ming C, Randolph, John T, Wang, Xiu C, and Yu, Su</p>

    <p>          PATENT:  US <b>2005148623</b>  ISSUE DATE:  20050707</p>

    <p>          APPLICATION: 2004-8713  PP: 279 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   47776   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Virus isolates during acute and chronic human immunodeficiency virus type 1 infection show distinct patterns of sensitivity to entry inhibitors</p>

    <p>          Rusert, Peter, Kuster, Herbert, Joos, Beda, Misselwitz, Benjamin, Gujer, Cornelia, Leemann, Christine, Fischer, Marek, Stiegler, Gabriela, Katinger, Hermann, Olson, William C, Weber, Rainer, Aceto, Leonardo, Guenthard, Huldrych F, and Trkola, Alexandra</p>

    <p>          Journal of Virology <b>2005</b>.  79(13): 8454-8469</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   47777   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Antiviral protein OVT102 derived from cyanovirin with improved properties and methods therefor</p>

    <p>          Levine, Howard L and Kerns, William</p>

    <p>          PATENT:  WO <b>2005058229</b>  ISSUE DATE:  20050630</p>

    <p>          APPLICATION: 2004  PP: 32 pp.</p>

    <p>          ASSIGNEE:  (Omniviral Therapeutics LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   47778   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          HIV (human immunodeficiency virus) gp41 mimetibodies as improved anti-retroviral agents with longer plasma half-lives, and therapeutic uses thereof</p>

    <p>          Dillon, Susan B, Del Vecchio, Alfred Michael, Huang, Chichi, O&#39;Neil, Karyn T, and Sarisky, Robert T</p>

    <p>          PATENT:  US <b>2005136061</b>  ISSUE DATE:  20050623</p>

    <p>          APPLICATION: 2004-18102  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   47779   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Carbazole derivatives as integrase inhibitors and anti-retrovirus medicines</p>

    <p>          Chiba, Tomoko, Yan, Ka, and Sugiura, Yuzuru</p>

    <p>          PATENT:  JP <b>2005154347</b>  ISSUE DATE:  20050616</p>

    <p>          APPLICATION: 2003-2643  PP: 16 pp.</p>

    <p>          ASSIGNEE:  (Toyama Chemical Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   47780   HIV-LS-329; WOS-HIV-8/14/2005</p>

    <p class="memofmt1-2">          Nonnucleoside HIV-1 reverse transcriptase inhibitors, part 4. Synthesis and anti-HIV activity of N-1-beta-carbonyl-6-naphthylmethyl analogues of HEPT</p>

    <p>          He, YP, Kuang, YY, Chen, F, Wang, SX, Ji, L, De, Clercq E, Balzarini, J, and Pannecouque, C</p>

    <p>          MONATSHEFTE FUR CHEMIE <b>2005</b>.  136(7): 1233-1245, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230819000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230819000010</a> </p><br />

    <p>17.   47781   HIV-LS-329; WOS-HIV-8/14/2005</p>

    <p class="memofmt1-2">          Synthesis of N-1-(indanyloxymethyl) and N-1-(4-hydroxybut-2-enyloxymethyl) analogues of the HIV drugs Emivirine and GCA-186</p>

    <p>          El-Brollosy, NR, Nielsen, C, and Pedersen, EB</p>

    <p>          MONATSHEFTE FUR CHEMIE <b>2005</b>.  136(7): 1247-1254, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230819000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230819000011</a> </p><br />
    <br clear="all">

    <p>18.   47782   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Antiviral compositions for treatment of HIV infection</p>

    <p>          Wood, Allen Wayne, Currie, Robin, Mehta, Samir Chimanlal, Goodson, Gary Wayne, and Wells, Mickey Lee</p>

    <p>          PATENT:  US <b>2005113394</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004-14360  PP: 6 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   47783   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Anti-HIV Activity of (-)-(2R,4R)-1- (2-Hydroxymethyl-1,3-dioxolan-4-yl)thymine against Drug-Resistant HIV-1 Mutants and Studies of Its Molecular Mechanism</p>

    <p>          Chu, Chung K, Yadav, Vikas, Chong, Youhoon H, and Schinazi, Raymond F</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(12): 3949-3952</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   47784   HIV-LS-329; SCIFINDER-HIV-8/15/2005</p>

    <p class="memofmt1-2">          Structure-Based Design: Synthesis and Biological Evaluation of a Series of Novel Cycloamide-Derived HIV-1 Protease Inhibitors</p>

    <p>          Ghosh, Arun K, Swanson, Lisa M, Cho, Hanna, Leshchenko, Sofiya, Hussain, Khaja Azhar, Kay, Stephanie, Walters, DEric, Koh, Yasuhiro, and Mitsuya, Hiroaki</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(10): 3576-3585</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   47785   HIV-LS-329; WOS-HIV-8/21/2005</p>

    <p class="memofmt1-2">          Combinatorial selection, inhibition, and antiviral activity of DNA thioaptamers targeting the RNase H domain of HIV-1 reverse transcriptase</p>

    <p>          Somasunderam, A, Ferguson, MR, Rojo, DR, Thiviyanathan, V, Li, X, O&#39;Brien, WA, and Gorenstein, DG</p>

    <p>          BIOCHEMISTRY <b>2005</b>.  44(30): 10388-10395, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230879900044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230879900044</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
