

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-381.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="W9RYo4brNk/d9EJWo0lGkHxDiZ1MeVQjvb1j/AZWbRzVVJEEBx/h5TlZOdB0GlBC1Eqlb+j9aBu3BkcHJ1VgMrUgz0SsB15XIQhbh7xrZB8JEa1mOPk5cBFLVqQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="342C3071" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-381-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61500   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Thuggacins, macrolide antibiotics active against Mycobacterium tuberculosis: isolation from myxobacteria, structure elucidation, conformation analysis and biosynthesis</p>

    <p>          Steinmetz, Heinrich, Irschik, Herbert, Kunze, Brigitte, Reichenbach, Hans, Hoefle, Gerhard, and Jansen, Rolf</p>

    <p>          Chem.--Eur. J.  <b>2007</b>.  13(20): 5822-5832</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61501   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Enhanced killing of intracellular multidrug-resistant Mycobacterium tuberculosis by compounds that affect the activity of efflux pumps</p>

    <p>          Amaral, Leonard, Martins, Marta, and Viveiros, Miguel</p>

    <p>          J. Antimicrob. Chemother. <b>2007</b>.  59(6): 1237-1246</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61502   OI-LS-381; WOS-OI-8/10/2007</p>

    <p class="memofmt1-2">          Synthesis of Putative Chain Terminators of Mycobacterial Arabinan Biosynthesis</p>

    <p>          Smellie, I. <i>et al.</i></p>

    <p>          Organic &amp; Biomolecular Chemistry <b>2007</b>.  5(14): 2257-2266</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247768900023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247768900023</a> </p><br />

    <p>4.     61503   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Efficacy of clarithromycin on Mycobacterium avium which proliferates in bronchial epithelial cells</p>

    <p>          Yamazaki, Yoshitaka</p>

    <p>          Jpn. J. Antibiot. <b>2007</b>.  60(Suppl. A): 28-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61504   OI-LS-381; WOS-OI-8/10/2007</p>

    <p class="memofmt1-2">          New Anti-Tuberculosis Therapies</p>

    <p>          Portero, J. and Rubio, M.</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2007</b>.  17(6): 617-637</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247719900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247719900003</a> </p><br />

    <p>6.     61505   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          IL-10 underlies distinct susceptibility of BALB/c and C57BL/6 mice to Mycobacterium avium infection and influences efficacy of antibiotic therapy</p>

    <p>          Roque Susana, Nobrega Claudia, Appelberg Rui, and Correia-Neves Margarida</p>

    <p>          J Immunol <b>2007</b>.  178(12): 8028-35.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     61506   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Invasion and Egress by the obligate intracellular parasite Toxoplasma gondii: potential targets for the development of new antiparasitic drugs</p>

    <p>          Lavine, MD and Arrizabalaga, G</p>

    <p>          Curr. Pharm. Des. <b>2007</b>.  13(6): 641-651</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>8.     61507   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Coordination to copper(II) strongly enhances the in vitro antimicrobial activity of pyridine-derived N(4)-tolyl thiosemicarbazones</p>

    <p>          Mendes, Isolda C, Moreira, Juliana P, Mangrich, Antonio S, Balena, Solange P, Rodrigues, Bernardo L, and Beraldo, Heloisa</p>

    <p>          Polyhedron <b>2007</b>.  26(13): 3263-3270</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61508   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Antioxidant, antimalarial and antimicrobial activities of tannin-rich fractions, ellagitannins and phenolic acids from Punica granatum L</p>

    <p>          Reddy, Muntha K, Gupta, Sashi K, Jacob, Melissa R, Khan, Shabana I, and Ferreira, Daneel</p>

    <p>          Planta Med. <b>2007</b>.  73(5): 461-467</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61509   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Triple therapy of initial high-dose interferon with ribavirin and amantadine for patients with chronic hepatitis C</p>

    <p>          Uyama, Hirokazu, Nakamura, Hideji, Hayashi, Eijiro, Ogawa, Hiroyuki, Enomoto, Hirayuki, Yoshida, Kenya, Okuda, Yorihide, Yamamoto, Mitsunari, Hada, Toshikazu, and Hayashi, Norio</p>

    <p>          Hepatol. Res. <b>2007</b>.  37(5): 325-330</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61510   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Preparation of azacytosine acyclic nucleotide derivatives useful as antiviral agents</p>

    <p>          Holy, Antonin, Krecmerova, Marcela, Piskala, Alois, Andrei, Graciela, Snoeck, Robert, De Clercq, Erik, Neyts, Johan, and Naesens, Lieve</p>

    <p>          PATENT:  WO <b>2007065231</b>  ISSUE DATE:  20070614</p>

    <p>          APPLICATION: 2006  PP: 96pp.</p>

    <p>          ASSIGNEE:  (K.U. Leuven Research &amp; Development, Belg. and Institute of Organic Chemistry and Biochemistry of Academy of Sciences of the Czech Republic)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61511   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Mechanism of drug-resistance in human cytomegalovirus</p>

    <p>          Eizuru Yoshito </p>

    <p>          Nippon Rinsho <b>2007</b>.  65 Suppl 2 Pt. 1: 476-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   61512   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Antivirals for herpesviruses and cytomegalovirus</p>

    <p>          Minematsu Toshio</p>

    <p>          Nippon Rinsho <b>2007</b>.  65 Suppl 2 Pt. 1: 396-400.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   61513   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Structural characterization and antifungal studies of some mixed ligand schiff base complexes with 6-bromo-2-thio 3-phenyl quinazoline-4(3H) one thiosemicarbazone</p>

    <p>          Rai, BK, Choudhary, Pramod, Rana, Sweta, and Sahi, Poonam</p>

    <p>          Orient. J. Chem. <b>2007</b>.  23(1): 271-275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>15.   61514   OI-LS-381; WOS-OI-8/17/2007</p>

    <p class="memofmt1-2">          Therapeutic Efficacy of Posaconazole Against Isolates of Candida Albicans With Different Susceptibilities to Fluconazole in a Vaginal Model</p>

    <p>          Gonzalez, G. <i>et al.</i></p>

    <p>          Medical Mycology <b>2007</b>.  45(3): 221-224</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>          <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247374400004</span></p>

    <p>16.   61515   OI-LS-381; WOS-OI-8/17/2007</p>

    <p class="memofmt1-2">          Binding-Site Identification and Genotypic Profiling of Hepatitis C Virus Polymerase Inhibitors</p>

    <p>          Pauwels, F. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(13): 6909-6919</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247404900014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247404900014</a> </p><br />

    <p>17.   61516   OI-LS-381; SCIFINDER-OI-8/13/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some novel 2-(4-substituted-phenyl)-5- (naphthalen-1-yloxy-methyl) [1,3,4] oxadiazoles and 1-[2-(4-substituted phenyl)-5-(naphthaten-1- yloxy-methyl)-[1,3,4]oxadiazol-3-yl]-ethanones</p>

    <p>          Rajak, Harish, Kharya, Murlidhar, and Mishra, Pradeep</p>

    <p>          Int. J. Chem. Sci. <b>2007</b>.  5(1): 365-379</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   61517   OI-LS-381; WOS-OI-8/17/2007</p>

    <p class="memofmt1-2">          Synthesis of Methylene-Bridged Analogues of Biologically Active Pteridine Derivatives</p>

    <p>          Slusarczyk, M. <i>et al.</i></p>

    <p>          European Journal of Organic Chemistry <b>2007</b>.(18): 2987-2994</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247799200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247799200011</a> </p><br />

    <p>19.   61518   OI-LS-381; WOS-OI-8/17/2007</p>

    <p class="memofmt1-2">          Hts, Chemical Hybridization, and Drug Design Identify a Chemically Unique Antituberculosis Agent-Coupling Serendipity and Rational Approaches to Drug Discovery</p>

    <p>          Mao, J. <i>et al.</i></p>

    <p>          Chemmedchem <b>2007</b>.  2(6): 811-813</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247392900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247392900008</a> </p><br />

    <p>20.   61519   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Characterization of the metabolic activation of a HCV nucleoside inhibitor beta -D-2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methylcytidine (PSI-6130) and identification of a novel active 5&#39;-triphosphate species</p>

    <p>          Ma, H, Jiang, WR, Robledo, N, Leveque, V, Ali, S, Lara-Jaime, T, Masjedizadeh, M, Smith, DB, Cammack, N, Klumpp, K, and Symons, J</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17698842&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17698842&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   61520   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Synthesis and Enzyme Inhibitory Activity of the S-Nucleoside Analogue of the Ribitylaminopyrimidine Substrate of Lumazine Synthase and Product of Riboflavin Synthase</p>

    <p>          Talukdar, A, Illarionov, B, Bacher, A, Fischer, M, and Cushman, M</p>

    <p>          J Org Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696548&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696548&amp;dopt=abstract</a> </p><br />

    <p>22.   61521   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Inhibition of Mycobacterium tuberculosis, Mycobacterium bovis, and Mycobacterium avium by Novel Dideoxy Nucleosides</p>

    <p>          Rai, D, Johar, M, Srivastav, NC, Manning, T, Agrawal, B, Kunimoto, DY, and Kumar, R</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696514&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696514&amp;dopt=abstract</a> </p><br />

    <p>23.   61522   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Identification of Inhibitors for Mycobacterial Protein Tyrosine Phosphatase B (MptpB) by Biology-Oriented Synthesis (BIOS)</p>

    <p>          Correa, IR Jr, Noren-Muller, A, Ambrosi, HD, Jakupovic, S, Saxena, K, Schwalbe, H, Kaiser, M, and Waldmann, H</p>

    <p>          Chem Asian J <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17685373&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17685373&amp;dopt=abstract</a> </p><br />

    <p>24.   61523   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Design, Synthesis, and Pharmacological Evaluation of Mefloquine-Based Ligands as Novel Antituberculosis Agents</p>

    <p>          Mao, J, Wang, Y, Wan, B, Kozikowski, AP, and Franzblau, SG</p>

    <p>          ChemMedChem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17680579&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17680579&amp;dopt=abstract</a> </p><br />

    <p>25.   61524   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p><b>          Chemoenzymatic synthesis of feruloyl D: -arabinose as a potential anti-mycobacterial agent</b> </p>

    <p>          Vafiadi, C, Topakas, E, Alderwick, LJ, Besra, GS, and Christakopoulos, P</p>

    <p>          Biotechnol Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17676274&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17676274&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>26.   61525   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Morphine Inhibits Intrahepatic Interferon- alpha Expression and Enhances Complete Hepatitis C Virus Replication</p>

    <p>          Li, Y, Ye, L, Peng, JS, Wang, CQ, Luo, GX, Zhang, T, Wan, Q, and Ho, WZ</p>

    <p>          J Infect Dis <b>2007</b>.  196(5): 719-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17674315&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17674315&amp;dopt=abstract</a> </p><br />

    <p>27.   61526   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Antifungal potential of disulfiram</p>

    <p>          Khan, S, Singhal, S, Mathur, T, Upadhyay, DJ, and Rattan, A</p>

    <p>          Nippon Ishinkin Gakkai Zasshi <b>2007</b>.  48(3): 109-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17667894&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17667894&amp;dopt=abstract</a> </p><br />

    <p>28.   61527   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Identification and characterization of an aspartyl protease from Cryptococcus neoformans</p>

    <p>          Pinti, M, Orsi, CF, Gibellini, L, Esposito, R, Cossarizza, A, Blasi, E, Peppoloni, S, and Mussini, C</p>

    <p>          FEBS Lett <b>2007</b>.  581(20): 3882-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17651737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17651737&amp;dopt=abstract</a> </p><br />

    <p>29.   61528   OI-LS-381; PUBMED-OI-8/20/2007</p>

    <p class="memofmt1-2">          Antifungal activity of the amyrin derivatives and in vitro inhibition of Candida albicans adhesion to human epithelial cells</p>

    <p>          Johann, S, Soldi, C, Lyon, JP, Pizzolatti, MG, and Resende, MA</p>

    <p>          Lett Appl Microbiol <b>2007</b>.  45(2): 148-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17651210&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17651210&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
