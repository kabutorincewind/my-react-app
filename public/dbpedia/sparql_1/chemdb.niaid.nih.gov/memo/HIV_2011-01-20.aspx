

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-01-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nFe2EcNFsJONaavl3clesD/Iq2Npqlp+ZYjUBj8HfGQxZaUsmS+bbzFj/nXznLUO8unUc5cnqXdP2pAzaNUyv2gAs1XG4UlWevs5LSemJkPpJNp5LjK8gAAYz4w=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7B91D4BC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 7, 2011 - January 20, 2011</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21229520">Allosteric Inhibitor Development Targeting HIV-1 Integrase.</a> Al-Mawsawi, L.Q. and N. Neamati. ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21229520].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21245449">In Vitro Characterization of GS-8374, a Novel Phosphonate-Containing Inhibitor of HIV-1 Protease with Favorable Resistance Profile.</a> Callebaut, C., K. Stray, L. Tsai, M. Williams, Z.Y. Yang, C. Cannizzaro, S.A. Leavitt, X. Liu, K. Wang, B.P. Murray, A. Mulato, M. Hatada, T. Priskich, N. Parkin, S. Swaminathan, W. Lee, G.X. He, L. Xu, and T. Cihlar. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21245449].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21241212">Novel small molecules for the treatment of infections caused by Candida albicans: a patent review (2002 - 2011).</a> Calugi, C., A. Trabocchi, and A. Guarna. Expert Opinion on Therapeutic Patents, 2011. <b>[Epub ahead of print]</b>; PMID[21241212].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21238547">A new diarylheptanoid from the rhizomes of Zingiber mekongense.</a> Chareonkla, A., M. Pohmakotr, V. Reutrakul, C. Yoosook, J. Kasisit, C. Napaswad, and P. Tuchinda. Fitoterapia, 2011. <b>[Epub ahead of print]</b>; PMID[21238547].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21194939">Synthesis and biological evaluation of novel dihydro-aryl/alkylsulfanyl-cyclohexylmethyl-oxopyrimidines (S-DACOs) as high active anti-HIV agents.</a> He, Y.P., J. Long, S.S. Zhang, C. Li, C.C. Lai, C.S. Zhang, D.X. Li, D.H. Zhang, H. Wang, Q.Q. Cai, and Y.T. Zheng. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(2): p. 694-7; PMID[21194939].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21245443">GS-8374, a novel HIV protease inhibitor, does not alter glucose homeostasis in cultured adipocytes or in a healthy rodent model system.</a> Hruz, P.W., Q. Yan, L. Tsai, J. Koster, L. Xu, T. Cihlar, and C. Callebaut. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21245443].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20576416">Isolation of adenosine, iso-sinensetin and dimethylguanosine with antioxidant and HIV-1 protease inhibiting activities from fruiting bodies of Cordyceps militaris.</a> Jiang, Y., J.H. Wong, M. Fu, T.B. Ng, Z.K. Liu, C.R. Wang, N. Li, W.T. Qiao, T.Y. Wen, and F. Liu. Phytomedicine, 2011. 18(2-3): p. 189-93; PMID[20576416].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21245437">Vaginal microbicide gel for the delivery of IQP-0528, a pyrimidinedione analog with a dual mechanism of action against HIV-1.</a> Mahalingam, A., A.P. Simmons, S.R. Ugaonkar, K.M. Watson, C.S. Dezzutti, L.C. Rohan, R.W. Buckheit, Jr., and P.F. Kiser. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21245437].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21248316">An Aptamer-siRNA Chimera Suppresses HIV-1 Viral Loads and Protects from Helper CD4+ T Cell Decline in Humanized Mice.</a>  Neff, C.P., J. Zhou, L. Remling, J. Kuruvilla, J. Zhang, H. Li, D.D. Smith, P. Swiderski, J.J. Rossi, and R. Akkina. Science Translational Medicine, 2011. 3(66): p. 66ra6; PMID[21248316].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21245351">Organic Synthesis toward Small-Molecule Probes and Drugs Special Feature: Design, synthesis, and biological evaluation of a biyouyanagin compound library.</a> Nicolaou, K.C., S. Sanchini, D. Sarlah, G. Lu, T.R. Wu, D.K. Nomura, B.F. Cravatt, B. Cubitt, J.C. de la Torre, A.J. Hessell, and D.R. Burton. Proceedings of the National Academy of Sciences of the United States of America, 2011. <b>[Epub ahead of print]</b>; PMID[21245351].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21219864">High mannose-specific lectin (KAA-2) from the red alga Kappaphycus alvarezii potently inhibits influenza virus infection in a strain-independent manner.</a> Sato, Y., K. Morimoto, M. Hirayama, and K. Hori. Biochemical and Biophysical Research Communications, 2011. <b>[Epub ahead of print]</b>; PMID[21219864].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21219250">Small molecule HIV entry inhibitors: Part I. Chemokine receptor antagonists: 2004 - 2011.</a> Singh, I.P. and S.K. Chauthe. Expert Opinion on Therapeutic Patents, 2011. 21(2): p. 227-69; PMID[21219250].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21193314">Structural and biochemical study on the inhibitory activity of derivatives of 5-nitro-furan-2-carboxylic acid for RNase H function of HIV-1 reverse transcriptase.</a> Yanagita, H., E. Urano, K. Matsumoto, R. Ichikawa, Y. Takaesu, M. Ogata, T. Murakami, H. Wu, J. Chiba, J. Komano, and T. Hoshino. Bioorganic &amp; Medicinal Chemistry, 2011. 19(2): p. 816-25; PMID[21193314].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21103490">3-Phosphono-L-alanine as pyrophosphate mimic for DNA synthesis using HIV-1 reverse transcriptase.</a> Yang, S., M. Froeyen, E. Lescrinier, P. Marliere, and P. Herdewijn. Organic &amp; Biomolecular Chemistry, 2011. 9(1): p. 111-9; PMID[21103490].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0107-012110.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> ISI Web of Knowledge citations:</p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285065000005">Interaction of HIV-1 Reverse Transcriptase Ribonuclease H with an Acylhydrazone Inhibitor.</a>. L. Menon, T. Ilina, L.G. Miller, J. Ahn, M.A. Parniak, and R. Ishima Chemical Biology &amp; Drug Design, 2011. 77(1): p. 39-47; ISI[000285065000005].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0107-010710.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285173900006">Actinohivin: specific amino acid residues essential for anti-HIV activity.</a> Takahashi, A., J. Inokoshi, M. Tsunoda, K. Suzuki, A. Takenaka, T. Sekiguchi, S. Omura, and H. Tanaka. Journal of Antibiotics, 2011. 63(11): p. 661-665; ISI[000285173900006].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0107-010710.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285281800001">Medicinal uses of the mushroom Cordyceps militaris: Current state and prospects.</a> Vets, S., J. Kimpel, D. Von Laer, Z. Debyser, and R. Gijsbers. LEDGF/p75 as antiviral target for HIV gene therapy . 7. Das, S.K., M. Masuda, A. Sakurai, and M. Sakakibara. Fitoterapia, 2011. 81(8): p. 961-968; ISI[000285281800001].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0107-010710.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285267400002">Exploring Alkaloids as Inhibitors of Selected Enzymes.</a> Singh, R., Geetanjali, and V. Singh. Asian Journal of Chemistry, 2011. 23(2): p. 483-490; ISI[000285267400002].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0107-010710.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285267400117">Lignans from the Stem of Styrax japonica.</a> Wu, N., L. Wang, Y.K. Chzn, Z. Liao, G.Y. Yang, and Q.F. Hu. Asian Journal of Chemistry, 2011. 23(2): p. 931-932; ISI[000285267400117].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0107-010710.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
