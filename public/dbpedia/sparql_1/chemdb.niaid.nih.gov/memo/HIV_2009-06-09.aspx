

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-06-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3DUlCjhN8ibjAa7yeQfdkCT/YMFQT5oWv5a82TjpMx58SUScWutSLYBCTnNH664YrQa0VP88c5ltYuBVjvCLUIVPTrpkewLnPA6X2xb6Uoh7GL+4cxUbAX0/WoE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8029D048" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  May 27 - June 09, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19493996?ordinalpos=23&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Combinations of the first and next generation HIV fusion inhibitors exhibit highly potent synergistic effect against enfuvirtide-sensitive and resistant HIV-1 strains.</a>  Pan C, Cai L, Lu H, Qi Z, Jiang S.  J Virol. 2009 Jun 3. [Epub ahead of print]  PMID: 19493996</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19486542?ordinalpos=37&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">&quot;Shock and kill&quot; effects of class I-selective histone deacetylase inhibitors in combination with the glutathione synthesis inhibitor buthionine sulfoximine in cell line models for HIV-1 quiescence.</a>  Savarino A, Mai A, Norelli S, El Daker S, Valente S, Rotili D, Altucci L, Palamara AT, Garaci E.  Retrovirology. 2009 Jun 2;6(1):52. [Epub ahead of print]  PMID: 19486542</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19473017?ordinalpos=51&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design of HIV-1 Protease Inhibitors with Pyrrolidinones and Oxazolidinones as Novel P1&#39;-Ligands To Enhance Backbone-Binding Interactions with Protease: Synthesis, Biological Evaluation, and Protein-Ligand X-ray Studies (infinity).</a>  Ghosh AK, Leshchenko-Yashchuk S, Anderson DD, Baldridge A, Noetzel M, Miller HB, Tie Y, Wang YF, Koh Y, Weber IT, Mitsuya H.  J Med Chem. 2009 May 27. [Epub ahead of print]  PMID: 19473017</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19411176?ordinalpos=82&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, microwave-assisted synthesis and HIV-RT inhibitory activity of 2-(2,6-dihalophenyl)-3-(4,6-dimethyl-5-(un)substituted-pyrimidin-2-yl)thiazolidin-4-ones.</a>  Chen H, Bai J, Jiao L, Guo Z, Yin Q, Li X.  Bioorg Med Chem. 2009 Jun 1;17(11):3980-6. Epub 2009 Apr 18.  PMID: 19411176</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19289518?ordinalpos=126&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The octadecyloxyethyl ester of (S)-9-[3-hydroxy-2-(phosphonomethoxy) propyl]adenine is a potent and selective inhibitor of hepatitis C virus replication in genotype 1A, 1B, and 2A replicons.</a>  Wyles DL, Kaihara KA, Korba BE, Schooley RT, Beadle JR, Hostetler KY.  Antimicrob Agents Chemother. 2009 Jun;53(6):2660-2. Epub 2009 Mar 16.  PMID: 19289518</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19283692?ordinalpos=128&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">2-Phenylquinolones as inhibitors of the HIV-1 Tat-TAR interaction.</a>  Gatto B, Tabarrini O, Massari S, Giaretta G, Sabatini S, Del Vecchio C, Parolin C, Fravolini A, Palumbo M, Cecchetti V.  ChemMedChem. 2009 Jun;4(6):935-8.  PMID: 19283692</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p>7.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Development of a Novel Fusion Inhibitor against T-20-resistant HIV-1.</a>  Oishi, S; Ito, S; Nishikawa, H; Tanaka, M; Ohno, H; Otaka, A; Izumi, K; Kodama, E; Matsuoka, M; Fujii, N.  ADVANCES IN EXPERIMENTAL MEDICINE AND BIOLOGY, 2009; 611: 389-391.</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Anti-HIV dendrimeric peptides.</a>   Yu, QT; Li, L; Tam, JP. ADVANCES IN EXPERIMENTAL MEDICINE AND BIOLOGY, 2009; 611: 539-540. </p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Retrocyclin-2: a potent anti-HIV theta-defensin that forms a cyclic cystine ladder structural motif.</a>  Daly, NL; Chen, YK; Rosenren, KJ; Marx, UC; Phillips, ML; Waring, AJ; Wang, W; Lehrer, RI; Craik, DJ.  ADVANCES IN EXPERIMENTAL MEDICINE AND BIOLOGY, 2009; 611: 577-578. </p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Efficacy of dinitrochlorobenzene and 3 &#39;-azido-3 &#39;-deoxythymidine for treating acquired immunodeficiency disease.</a>  Urso, P; Goldberg, B; DeLancie, R; Margulis, SB.  LIFE SCIENCE JOURNAL-ACTA ZHENGZHOU UNIVERSITY OVERSEAS EDITION, 2009; 5(3): 41-49.</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Effectiveness of commercial inhibitors against subtype F HIV-1 protease.</a>  Krauchenco, S; Martins, NH; Sanches, M; Polikarpov, I.  JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY, 2009; 24(3): 638-645.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Screening for NNRTIs with Slow Dissociation and High Affinity for a Panel of HIV-1 RT Variants.</a>  Elinder, M; Nordstrom, H; Geitmann, M; Hamalainen, M; Vrang, L; Oberg, B; Danielson, UH. JOURNAL OF BIOMOLECULAR SCREENING, 2009; 14(4): 395-403. </p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Soluble CD4 and CD4-Mimetic Compounds Inhibt HIV-1 Infection by Induction of a Short-Lived Activated State.</a>  Haim, H; Si, ZH; Madani, N; Wang, LP; Courter, JR; Princiotto, A; Kassa, A; DeGrace, M; McGee-Estrada, K; Mefford, M; Gabuzda, D; Smith, AB; Sodroski, J.  PLOS PATHOGENS, 2009; 5(4): e1000360.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">4-Thio-uridylate (UD29) interferes with the function of protein -SH and inhibits HIV replication in vitro.</a>  Beck, Z; Kis, A; Berenyi, E; Kovacs, P; Fesus, L; Aradi, J.  PHARMACOLOGICAL REPORTS, 2009; 61(2): 343-347<b>.</b></p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">Discovery of Wild-Type and Y181C Mutant Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors Using Virtual Screening with Multiple Protein Structures.</a>  Nichols, SE; Domaoal, RA; Thakur, VV; Tirado-Rives, J; Anderson, KS; Jorgensen, WL.  JOURNAL OF CHEMICAL INFORMATION AND MODELING, 2009; 49(5): 1272-1279. </p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
