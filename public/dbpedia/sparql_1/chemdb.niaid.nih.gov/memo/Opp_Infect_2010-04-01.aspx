

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-04-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1ArZsQsnYOgn3MNdytwVFtB2gm30hebBtE/5iz77MXRDcWFaGARjs/8FvVdmPakY9GHeXMGmA08LVDy7Mvc4hv7KB74TE68jVdfNCRT7RC7GehjglhpOTVebbEI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DDE45B61" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">March 19 - April 1, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20099897">Characterization of plant-derived saponin natural products against Candida albicans.</a> Coleman, J.J., I. Okoli, G.P. Tegos, E.B. Holson, F.F. Wagner, M.R. Hamblin, and E. Mylonakis. ACS Chem Biol. 5(3): p. 321-32; PMID[20099897].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p> </p>

    <p>2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20106563">Synthesis and in vitro microbiological evaluation of an array of biolabile 2-morpholino-N-(4,6-diarylpyrimidin-2-yl)acetamides.</a> Kanagarajan, V., J. Thanusu, and M. Gopalakrishnan. Eur J Med Chem. 45(4): p. 1583-9; PMID[20106563].</p>

    <p class="pubmed"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20336497">Azinyl sulfides--CXVIII. Antimicrobial activity of novel 1-methyl-3-thio-4-aminoquinolinium salts.</a> Zieba, A., R.D. Wojtyczka, M. Kepa, and D. Idzik. Folia Microbiol (Praha). 55(1): p. 3-9; PMID[20336497].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20150181">Linezolid use for treatment of multidrug-resistant and extensively drug-resistant tuberculosis, New York City, 2000-06.</a> Anger, H.A., F. Dworkin, S. Sharma, S.S. Munsiff, D.M. Nilsen, and S.D. Ahuja. J Antimicrob Chemother. 65(4): p. 775-83; PMID[20150181].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20097303">Identification of a new hexadentate iron chelator capable of restricting the intramacrophagic growth of Mycobacterium avium.</a> Fernandes, S.S., A. Nunes, A.R. Gomes, B. de Castro, R.C. Hider, M. Rangel, R. Appelberg, and M.S. Gomes. Microbes Infect. 12(4): p. 287-94; PMID[20097303].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20149500">Synthesis and anti-tuberculosis activity of new hetero(Mn, Co, Ni)trinuclear iron(III) furoates.</a> Melnic, S., D. Prodius, H. Stoeckli-Evans, S. Shova, and C. Turta. Eur J Med Chem. 45(4): p. 1465-9; PMID[20149500].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20086151">Clinical isolates of Mycobacterium tuberculosis in four European hospitals are uniformly susceptible to benzothiazinones.</a> Pasca, M.R., G. Degiacomi, A.L. Ribeiro, F. Zara, P. De Mori, B. Heym, M. Mirrione, R. Brerra, L. Pagani, L. Pucillo, P. Troupioti, V. Makarov, S.T. Cole, and G. Riccardi. Antimicrob Agents Chemother. 54(4): p. 1616-8; PMID[20086151].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20353194">Structure and Anti-TB Activity of Trachylobanes from the Liverwort Jungermannia exsertifolia ssp. cordifolia.</a> Scher, J.M., A. Schinkovitz, J. Zapp, Y. Wang, S.G. Franzblau, H. Becker, D.C. Lankin, and G.F. Pauli. J Nat Prod. <b>[Epub ahead of print]</b>; PMID[20353194].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20346970">Therapeutic potential of Peptide deformylase inhibitors against experimental tuberculosis.</a> Sharma, A., G.K. Khuller, A.J. Kanwar, and S. Sharma. J Infect. <b>[Epub ahead of print]</b>; PMID[20346970].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0319-040110.</p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p class="plaintext">10. <a href="http://apps.isiknowledge.com/InboundService.do?Func=Frame&amp;product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000275404900030&amp;SID=3Api5bDdmcJIm5jH3DP&amp;Init=Yes&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Pharmacophoric model building for antitubercular activity of the individual Schiff bases of small combinatorial library.</a> Abdel-Aal, W.S., H.Y. Hassan, T. Aboul-Fadl, and A.F. Youssef. European Journal of Medicinal Chemistry. 45(3): p. 1098-1106; ISI[000275404900030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000275404900043&amp;SID=3Api5bDdmcJIm5jH3DP&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Identification of antibacterial and antifungal pharmacophore sites for potent bacteria and fungi inhibition: Indolenyl sulfonamide derivatives.</a> Chohan, Z.H., M.H. Youssoufi, A. Jarrahpour, and T. Ben Hadda. European Journal of Medicinal Chemistry. 45(3): p. 1189-1199; ISI[000275404900043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000275310600017&amp;SID=3Api5bDdmcJIm5jH3DP&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis and in vitro Antimicrobial Activity of New Ethyl 2-(Ethoxyphosphono)-1-cyano-2-(substituted tetrazolo[1,5-a]quinolin-4-yl)ethanoate Derivatives.</a> Kategaonkar, A.H., S.A. Sadaphal, K.F. Shelke, B.B. Shingate, and M.S. Shingare. Chinese Journal of Chemistry. 28(2): p. 243-249; ISI[000275310600017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000275221500051&amp;SID=3Api5bDdmcJIm5jH3DP&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis and chain-dependent antifungal activity of long-chain 2H-azirine-carboxylate esters related to dysidazirine.</a> Skepper, C.K., D.S. Dalisay, and T.F. Molinski. Bioorganic &amp; Medicinal Chemistry Letters. 20(6): p. 2029-2032; ISI[000275221500051].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0319-040110.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000275106400015&amp;SID=3Api5bDdmcJIm5jH3DP&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis of New 4-Methyl-2-(4-pyridyl)-1,2,3,4-tetrahydroquinolines as Potent Antifungal Compounds.</a> Mendez, L.Y.V., S.A. Zacchino, and V.V. Kouznetsov. JOURNAL OF THE BRAZILIAN CHEMICAL SOCIETY. 21(1): p. 105-U56; ISI[000275106400015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0319-040110.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
