

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-294.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ye2IwtvL9KlzGQ9x1v+X0UujOX2wt+UmvnQxnTSqoQ9peDiDDLBbA6xcEOX1PfPo/6FuRQZP7LLQIto1xU8lHfJwSP1Acb8C+alAdLB3BmMlQhokZf3Na1jSZ2c=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="16FCD9B3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-294-MEMO</b> </p>

<p>    1.    43254   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Association of serum interleukin-8 with virologic response to antiviral therapy in patients with chronic hepatitis C</p>

<p>           Mihm, U, Herrmann, E, Sarrazin, U, Von Wagner, M, Kronenberger, B, Zeuzem, S, and Sarrazin, C </p>

<p>           J Hepatol 2004. 40(5): 845-52</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15094234&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15094234&amp;dopt=abstract</a> </p><br />

<p>    2.    43255   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Quantitative analysis of phagolysosome fusion in intact cells: inhibition by mycobacterial lipoarabinomannan and rescue by an 1{alpha},25-dihydroxyvitamin D3-phosphoinositide 3-kinase pathway</p>

<p>           Hmama, Z, SendiDe K, Talal, A, Garcia, R, Dobos, K, and Reiner, NE</p>

<p>           J Cell Sci 2004. 117(Pt 10): 2131-2140</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090599&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090599&amp;dopt=abstract</a> </p><br />

<p>    3.    43256   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Pernicious anemia associated with interferon-alpha therapy and chronic hepatitis C infection</p>

<p>           Andres, E, Loukili, NH, Ben Abdelghani, M, and Noel, E</p>

<p>           J Clin Gastroenterol 2004. 38(4): 382</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15087703&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15087703&amp;dopt=abstract</a> </p><br />

<p>    4.    43257   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           The use of microarray analysis to determine the gene expression profiles of Mycobacterium tuberculosis in response to anti-bacterial compounds*1, *2</p>

<p>           Waddell, Simon J, Stabler, Richard A, Laing, Ken, Kremer, Laurent, Reynolds, Robert C, and Besra, Gurdyal S</p>

<p>           Tuberculosis 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WXK-4C6KSB5-4/2/b1bde265d366332120b07a73e70667dc">http://www.sciencedirect.com/science/article/B6WXK-4C6KSB5-4/2/b1bde265d366332120b07a73e70667dc</a> </p><br />

<p>    5.    43258   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Differential effects of the combination of caspofungin and terbinafine against Candida albicans, Candida dubliniensis and Candida kefyr</p>

<p>           Gil-Lamaignere, C and Muller, F-MC</p>

<p>           International Journal of Antimicrobial Agents 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T7H-4C606FR-H/2/60d7d784db2e989a40aa94b1036be1ac">http://www.sciencedirect.com/science/article/B6T7H-4C606FR-H/2/60d7d784db2e989a40aa94b1036be1ac</a> </p><br />

<p>    6.    43259   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Complete development of Cryptosporidium parvum in rabbit chondrocytes (VELI cells)</p>

<p>           Lacharme, Lizeth, Villar, Vega, Rojo-Vazquez, Francisco A, and Suarez, Susana</p>

<p>           Microbes and Infection 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VPN-4C5G6B6-2/2/d50cbd01428d4d682c10b1c65b1341f8">http://www.sciencedirect.com/science/article/B6VPN-4C5G6B6-2/2/d50cbd01428d4d682c10b1c65b1341f8</a> </p><br />

<p>    7.    43260   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Transfer factors as immunotherapy and supplement of chemotherapy in experimental pulmonary tuberculosis</p>

<p>           Fabre, RA, Perez, TM, Aguilar, LD, Rangel, MJ, Estrada-Garcia, I, Hernandez-Pando, R, and Estrada, Parra S</p>

<p>           Clin Exp Immunol 2004. 136(2): 215-23</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15086383&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15086383&amp;dopt=abstract</a> </p><br />

<p>    8.    43261   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Effect of antimetabolite immunosuppressants on Flaviviridae, including hepatitis C virus</p>

<p>           Stangl, JR, Carroll, KL, Illichmann, M, and Striker, R</p>

<p>           Transplantation 2004. 77(4): 562-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084936&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084936&amp;dopt=abstract</a> </p><br />

<p>    9.    43262   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Ring-substituted quinolines as potential anti-tuberculosis agents*1</p>

<p>           Vangapandu, Suryanarayana, Jain, Meenakshi, Jain, Rahul, Kaur, Sukhraj, and Pal Singh, Prati</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4C4DWVY-4/2/8aded61e3403642bedab87b63dadafe3">http://www.sciencedirect.com/science/article/B6TF8-4C4DWVY-4/2/8aded61e3403642bedab87b63dadafe3</a> </p><br />

<p>  10.    43263   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Structure-Activity Relationship of Purine Ribonucleosides for Inhibition of Hepatitis C Virus RNA-Dependent RNA Polymerase</p>

<p>           Eldrup, AB, Allerson, CR, Bennett, CF, Bera, S, Bhat, B, Bhat, N, Bosserman, MR, Brooks, J, Burlein, C, Carroll, SS, Cook, PD, Getty, KL, MacCoss, M, McMasters, DR, Olsen, DB, Prakash, TP, Prhavc, M, Song, Q, Tomassini, JE, and Xia, J</p>

<p>           J Med Chem 2004. 47(9): 2283-2295</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084127&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084127&amp;dopt=abstract</a> </p><br />

<p>  11.    43264   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Natural antimycobacterial metabolites: current status</p>

<p>           Okunade, Adewole L, Elvin-Lewis, Memory PF, and Lewis, Walter H</p>

<p>           Phytochemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TH7-4C47HH4-8/2/7fd5d9760b8bbf2da9684372c1bc65d2">http://www.sciencedirect.com/science/article/B6TH7-4C47HH4-8/2/7fd5d9760b8bbf2da9684372c1bc65d2</a> </p><br />

<p>  12.    43265   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Cofactor mimics as selective inhibitors of NAD-dependent inosine monophosphate dehydrogenase (IMPDH)--the major therapeutic target</p>

<p>           Pankiewicz, KW, Patterson, SE, Black, PL, Jayaram, HN, Risal, D, Goldstein, BM, Stuyver, LJ, and Schinazi, RF</p>

<p>           Curr Med Chem  2004. 11(7): 887-900</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15083807&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15083807&amp;dopt=abstract</a> </p><br />

<p>  13.    43266   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Synthesis and biological evaluation of oxindoles and benzimidazolinones derivatives</p>

<p>           Messaoudi, Samir, Sancelme, Martine, Polard-Housset, Valerie, Aboab, Bettina, Moreau, Pascale, and Prudhomme, Michelle</p>

<p>           European Journal of Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VKY-4C47S60-2/2/8ce00051163a90bb71852e3b388965b4">http://www.sciencedirect.com/science/article/B6VKY-4C47S60-2/2/8ce00051163a90bb71852e3b388965b4</a> </p><br />

<p>  14.    43267   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Heterocyclic compounds such as pyrrole, pyridines, pyrrolidine, piperidine, indole, imidazol and pyrazines</p>

<p>           Higashio, Yasuhiko and Shoji, Takayuki</p>

<p>           Applied Catalysis A: General 2004.  260(2): 251-259</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF5-4BY3XH8-1/2/948294668582a682e4716675cd5384f9">http://www.sciencedirect.com/science/article/B6TF5-4BY3XH8-1/2/948294668582a682e4716675cd5384f9</a> </p><br />

<p>  15.    43268   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           The vitro efficacy of beta-lactam and beta-lactamase inhibitors against multidrug resistant clinical strains of Mycobacterium tuberculosis</p>

<p>           Dincer, I, Ergin, A, and Kocagoz, T</p>

<p>           Int J Antimicrob Agents 2004. 23(4): 408-411</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081094&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081094&amp;dopt=abstract</a> </p><br />

<p>  16.    43269   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Antiviral drugs in current clinical use*1</p>

<p>           De Clercq, Erik</p>

<p>           Journal of Clinical Virology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4C40YN3-1/2/841b34d1a7c7a0e0f960fa18b5e0a969">http://www.sciencedirect.com/science/article/B6VJV-4C40YN3-1/2/841b34d1a7c7a0e0f960fa18b5e0a969</a> </p><br />

<p>  17.    43270   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Capped dipeptide phenethylamide inhibitors of the HCV NS3 protease</p>

<p>           Nizi, E, Koch, U, Ontoria, JM, Marchetti, A, Narjes, F, Malancona, S, Matassa, VG, and Gardelli, C</p>

<p>           Bioorg Med Chem Lett 2004. 14(9): 2151-2154</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15080998&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15080998&amp;dopt=abstract</a> </p><br />

<p>  18.    43271   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           PknH, a transmembrane Hank&#39;s type serine/threonine kinase from Mycobacterium tuberculosis is differentially expressed under stress conditions</p>

<p>           Sharma, Kirti, Chandra, Harish, Gupta, Pradeep K, Pathak, Monika, Narayan, Azeet, Meena, Laxman S, D&#39;Souza, Rochelle CJ, Chopra, Puneet, Ramachandran, S, and Singh, Yogendra </p>

<p>           FEMS Microbiology Letters 2004. 233(1):  107-113</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2W-4BSW4D7-2/2/00d7ce10de3e104ce3616a1604cbe478">http://www.sciencedirect.com/science/article/B6T2W-4BSW4D7-2/2/00d7ce10de3e104ce3616a1604cbe478</a> </p><br />

<p>  19.    43272   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Antifungal Susceptibilities of Cryptococcus neoformans</p>

<p>           Archibald, LK </p>

<p>           Emerg Infect Dis 2004. 10(1): 143-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15078612&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15078612&amp;dopt=abstract</a> </p><br />

<p>  20.    43273   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Tetracycline in combination with sodium dioctylsulfosuccinate show increased antimicrobial activity in resistant microorganisms</p>

<p>           Simonetti, G, Simonetti, N, and Villa, A</p>

<p>           J Chemother 2004. 16(1): 38-44</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15077997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15077997&amp;dopt=abstract</a> </p><br />

<p>  21.    43274   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Cloning and expression of the trehalose-phosphate phosphatase of Mycobacterium tuberculosis: comparison to the enzyme from Mycobacterium smegmatis</p>

<p>           Edavana, Vineetha Koroth, Pastuszak, Irena, Carroll, JD, Thampi, Prajitha, Abraham, Edathera C, and Elbein, Alan D</p>

<p>           Archives of Biochemistry and Biophysics 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WB5-4BYJWG0-3/2/4462eec4224174085114084dc6f07715">http://www.sciencedirect.com/science/article/B6WB5-4BYJWG0-3/2/4462eec4224174085114084dc6f07715</a> </p><br />

<p>  22.    43275   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of ATP-binding site directed potential inhibitors of nucleoside triphosphatases/helicases and polymerases of hepatitis C and other selected Flaviviridae viruses</p>

<p>           Bretner, M, Schalinski, S, Haag, A, Lang, M, Schmitz, H, Baier, A, Behrens, SE, Kulikowski, T, and Borowski, P</p>

<p>           Antivir Chem Chemother 2004. 15(1): 35-42</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074713&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074713&amp;dopt=abstract</a> </p><br />

<p>  23.    43276   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Multicenter evaluation of reverse line blot assay for detection of drug resistance in Mycobacterium tuberculosis clinical isolates</p>

<p>           Mokrousov, Igor, Bhanu, NVijaya, Suffys, Philip N, Kadival, Gururaj V, Yap, Sook-Fan, Cho, Sang-Nae, Jordaan, Annemarie M, Narvskaya, Olga, Singh, Urvashi B, and Gomes, Harrison M</p>

<p>           Journal of Microbiological Methods 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T30-4BY3X0W-1/2/2255c3ed94c1cd52e5befc5c9d19194e">http://www.sciencedirect.com/science/article/B6T30-4BY3X0W-1/2/2255c3ed94c1cd52e5befc5c9d19194e</a> </p><br />

<p>  24.    43277   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Hepatitis C Virus (HCV) Diversity in HIV-HCV-Coinfected Subjects Initiating Highly Active Antiretroviral Therapy</p>

<p>           Blackard, JT, Yang, Y, Bordoni, P, Sherman, KE, and Chung, RT</p>

<p>           J Infect Dis 2004. 189(8): 1472-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073685&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073685&amp;dopt=abstract</a> </p><br />

<p>  25.    43278   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           The MspA porin promotes growth and increases antibiotic susceptibility of both Mycobacterium bovis BCG and Mycobacterium tuberculosis</p>

<p>           Mailaender, C, Reiling, N, Engelhardt, H, Bossmann, S, Ehlers, S, and Niederweis, M</p>

<p>           Microbiology 2004. 150(Pt 4): 853-864</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073295&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073295&amp;dopt=abstract</a> </p><br />

<p>  26.    43279   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Selection of an Escherichia coli host that expresses mutant forms of Mycobacterium tuberculosis 2-trans enoyl-ACP(CoA) reductase and 3-ketoacyl-ACP(CoA) reductase enzymes</p>

<p>           Poletto, Simone S, da Fonseca, Isabel O, de Carvalho, Luiz PS, Basso, Luiz A, and Santos, Diogenes S</p>

<p>           Protein Expression and Purification 2004. 34(1): 118-125</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WPJ-4B8BMMK-1/2/80dceb9ba8359614bec494341b5db530">http://www.sciencedirect.com/science/article/B6WPJ-4B8BMMK-1/2/80dceb9ba8359614bec494341b5db530</a> </p><br />

<p>  27.    43280   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Free tuberculosis drugs from Novartis</p>

<p>           Ahmad, Khabir </p>

<p>           The Lancet Infectious Diseases 2004.  4(2): 66</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W8X-4BJVHRS-7/2/f209d141d04e1e93323b420e1b1e6811">http://www.sciencedirect.com/science/article/B6W8X-4BJVHRS-7/2/f209d141d04e1e93323b420e1b1e6811</a> </p><br />

<p>  28.    43281   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Treatment of chronic hepatitis C in HIV/HCV-coinfection with interferon alpha-2b+ full-course vs. 16-week delayed ribavirin</p>

<p>           Brau, N, Rodriguez-Torres, M, Prokupek, D, Bonacini, M, Giffen, CA, Smith, JJ, Frost, KR, and Kostman, JR</p>

<p>           Hepatology 2004. 39(4): 989-98</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057903&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057903&amp;dopt=abstract</a> </p><br />

<p>  29.    43282   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Peginterferon alfa-2a and ribavirin in patients with chronic hepatitis C who have failed prior treatment</p>

<p>           Shiffman, ML, Di Bisceglie, AM, Lindsay, KL, Morishima, C, Wright, EC, Everson, GT, Lok, AS, Morgan, TR, Bonkovsky, HL, Lee, WM, Dienstag, JL, Ghany, MG, Goodman, ZD, and Everhart, JE</p>

<p>           Gastroenterology 2004. 126(4): 1015-1023; discussion 947</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057741&amp;dopt=abstract</a> </p><br />

<p>  30.    43283   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           TB drug discovery: addressing issues of persistence and resistance</p>

<p>           Smith, Clare V, Sharma, Vivek, and Sacchettini, James C</p>

<p>           Tuberculosis 2004. 84(1-2): 45-55</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WXK-4B5B3Y1-6/2/7c77b2d0cdfa35dc1ca064765e6d2f05">http://www.sciencedirect.com/science/article/B6WXK-4B5B3Y1-6/2/7c77b2d0cdfa35dc1ca064765e6d2f05</a> </p><br />

<p>  31.    43284   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Inhibition of Clotrimazole-resistant Candida albicans by plants used in Iranian folkloric medicine</p>

<p>           Shahidi Bonjar, GH</p>

<p>           Fitoterapia 2004. 75(1): 74-76</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VSC-49WPSSX-5/2/d9211e67cba457638d9173c806247e62">http://www.sciencedirect.com/science/article/B6VSC-49WPSSX-5/2/d9211e67cba457638d9173c806247e62</a> </p><br />

<p>  32.    43285   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Caspofungin resistance in Candida albicans: correlating clinical outcome with laboratory susceptibility testing of three isogenic isolates serially obtained from a patient with progressive Candida esophagitis</p>

<p>           Hernandez, S, Lopez-Ribot, JL, Najvar, LK, McCarthy, DI, Bocanegra, R, and Graybill, JR</p>

<p>           Antimicrob Agents Chemother 2004.  48(4): 1382-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047549&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047549&amp;dopt=abstract</a> </p><br />

<p>  33.    43286   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Potent synergistic in vitro interaction between nonantimicrobial membrane-active compounds and itraconazole against clinical isolates of Aspergillus fumigatus resistant to itraconazole</p>

<p>           Afeltra, J, VitaLe RG, Mouton, JW, and Verweij, PE</p>

<p>           Antimicrob Agents Chemother 2004.  48(4): 1335-43</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047538&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047538&amp;dopt=abstract</a> </p><br />

<p>  34.    43287   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Interfering with hepatitis C virus RNA replication</p>

<p>           Randall, Glenn and Rice, Charles M</p>

<p>           Virus Research 2004. 102(1): 19-25</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T32-4BVP8BS-1/2/8147c77ae24eb54ce22b9c2daed7c0b5">http://www.sciencedirect.com/science/article/B6T32-4BVP8BS-1/2/8147c77ae24eb54ce22b9c2daed7c0b5</a> </p><br />

<p>  35.    43288   OI-LS-294; PUBMED-HIV-4/20/2004</p>

<p class="memofmt1-2">           Disposition of caspofungin, a novel antifungal agent, in mice, rats, rabbits, and monkeys</p>

<p>           Sandhu, P, Xu, X, Bondiskey, PJ, Balani, SK, Morris, ML, Tang, YS, Miller, AR, and Pearson, PG</p>

<p>           Antimicrob Agents Chemother 2004.  48(4): 1272-80</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047529&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047529&amp;dopt=abstract</a> </p><br />

<p>  36.    43289   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Anti-HCV activities of selective polyunsaturated fatty acids</p>

<p>           Leu, Guang-Zhou, Lin, Tiao-Yin, and Hsu, John TA</p>

<p>           Biochemical and Biophysical Research Communications 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4C5PTHC-F/2/ac81823e406d7053bc1190bca9d19107">http://www.sciencedirect.com/science/article/B6WBK-4C5PTHC-F/2/ac81823e406d7053bc1190bca9d19107</a> </p><br />

<p>  37.    43290   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           Structure-activity relationships for the selectivity of hepatitis C virus NS3 protease inhibitors</p>

<p>           Poliakov, Anton, Johansson, Anja, Akerblom, Eva, Oscarsson, Karin, Samuelsson, Bertil, Hallberg, Anders, and Danielson, UHelena</p>

<p>           Biochimica et Biophysica Acta (BBA) - General Subjects 2004. 1672(1): 51-59</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T1W-4BW9NM9-1/2/280855c3c0525972fa44aa7e6f94c2bd">http://www.sciencedirect.com/science/article/B6T1W-4BW9NM9-1/2/280855c3c0525972fa44aa7e6f94c2bd</a> </p><br />

<p>  38.    43291   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           63 In-vitro and in-vivo evaluation of HCV polymerase inhibitors as potential drug candidates for treatment of chronic hepatitis C infection</p>

<p>           Dagan, S, Ilan, E, Aviel, S, Nussbaum, O, Arazi, E, Ben-Moshe, O, Slama, D, Tzionas, I, Shoshany, Y, and Lee, SW</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 23</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-28/2/3bae4eb66319cce4e2d3c2c0f8c62136">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-28/2/3bae4eb66319cce4e2d3c2c0f8c62136</a> </p><br />

<p>  39.    43292   OI-LS-294; EMBASE-OI-4/21/2004</p>

<p class="memofmt1-2">           492 A phase II, placebo-controlled study of merimepodib (XV-497), in combination with pegylated interferon-alfa, and ribavirin in patients with chronic hepatitis C non-responsive to previous therapy with interferon-alfa and ribavirin</p>

<p>           Marcellin, P, Horsmans, Y, Nevens, F, Grange, JD, Bronowicki, JP, Vetter, D, Kauffman, R, Knox, S, McNair, L, Moseley, S, and Alam, J</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 145</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-KK/2/c80c36029583978229fa14ff8d13f6c6">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-KK/2/c80c36029583978229fa14ff8d13f6c6</a> </p><br />

<p>  40.    43293   OI-LS-294; WOS-OI-4/11/2004</p>

<p class="memofmt1-2">           Comparative cytotoxicity of N-substituted N &#39;-(4-imidazole-ethyl)thiourea in precision-cut rat liver slices</p>

<p>           Onderwater, RCA, Commandeur, JNM, and Vermeulen, NPE</p>

<p>           TOXICOLOGY 2004. 197(2): 81-91</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220322700001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220322700001</a> </p><br />

<p>  41.    43294   OI-LS-294; WOS-OI-4/11/2004</p>

<p class="memofmt1-2">           Anti-Candida and mode of action of two newly synthesized polymers: a modified poly (methylmethacrylate-co-vinylbenzoylchloride) and a modified linear poly (chloroethylvinylether-co-vinylbenzoylchloride) with special reference to Candida albicans and Candida tropicalis</p>

<p>           Mahmoud, YAG and Aly, MM</p>

<p>           MYCOPATHOLOGIA 2004. 157(2): 145-153</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220341600001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220341600001</a> </p><br />

<p>  42.    43295   OI-LS-294; WOS-OI-4/11/2004</p>

<p class="memofmt1-2">           Antimicrobial activities of selected Cyathus species</p>

<p>           Liu, YJ and Zhang, KQ</p>

<p>           MYCOPATHOLOGIA 2004. 157(2): 185-189</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220341600007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220341600007</a> </p><br />

<p>  43.    43296   OI-LS-294; WOS-OI-4/11/2004</p>

<p class="memofmt1-2">           Inhibitors of hepatitis C virus NS3 center dot 4A protease 2. Warhead SAR and optimization</p>

<p>           Perni, RB, Pitlik, J, Britt, SD, Court, JJ, Courtney, LF, Deininger, DD, Farmer, LJ, Gates, CA, Harbeson, SL, Levin, RB, Lin, C, Lin, K, Moon, YC, Luong, YP, O&#39;Malley, ET, Rao, BG, Thomson, JA, Tung, RD, Van, Drie JH, and Wei, YY</p>

<p>           BIOORG MED CHEM LETT 2004. 14(6): 1441-1446</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220334000016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220334000016</a> </p><br />

<p>  44.    43297   OI-LS-294; WOS-OI-4/11/2004</p>

<p class="memofmt1-2">           Structure determination of tetrahydroquinazoline antifolates in complex with human and Pneumocystis carinii dihydrofolate reductase: correlations between enzyme selectivity and stereochemistry</p>

<p>           Cody, V, Luft, JR, Pangborn, W, Gangjee, A, and Queener, SF</p>

<p>           ACTA CRYSTALLOGR D 2004. 60: 646-655</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220368300004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220368300004</a> </p><br />

<p>  45.    43298   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Survival of Mycobacterium avium in a model distribution system</p>

<p>           Norton, CD, LeChevallier, MW, and Falkinham, JO</p>

<p>           WATER RES 2004. 38(6): 1457-1466</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220462200010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220462200010</a> </p><br />

<p>  46.    43299   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Inhibition of hepatitis C virus IRES-mediated translation by small RNAs analogous to stem-loop structures of the 5 &#39;-untranslated region</p>

<p>           Ray, PS and Das, S</p>

<p>           NUCLEIC ACIDS RES 2004. 32(5): 1678-1687</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220487200013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220487200013</a> </p><br />

<p>  47.    43300   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Interplay between mycobacteria and host signalling pathways</p>

<p>           Koul, A, Herget, T, Klebl, B, and Ullrich, A</p>

<p>           NAT REV MICROBIOL 2004. 2(3): 189-202</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220431800011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220431800011</a> </p><br />

<p>  48.    43301   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Dinitroanilines bind alpha-tubulin to disrupt microtubules</p>

<p>           Morrissette, NS, Mitra, A, Sept, D, and Sibley, LD</p>

<p>           MOL BIOL CELL  2004. 15(4): 1960-1968</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220450800042">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220450800042</a> </p><br />

<p>  49.    43302   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Treatment of multidrug-resistant tuberculosis</p>

<p>           Colebunders, R, Apers, L, and Shamputa, IC</p>

<p>           LANCET 2004. 363(9416): 1240</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220748100040">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220748100040</a> </p><br />

<p>  50.    43303   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Evaluation of the fully automated BACTEC MGIT 960 system for testing susceptibility of Mycobacterium tuberculosis to pyrazinamide, streptomycin, isoniazid, rifampin, and ethambutol and comparison with the radiometric BACTEC 460TB method</p>

<p>           Scarparo, C, Ricordi, P, Ruggiero, G, and Piccoli, P</p>

<p>           J CLIN MICROBIOL 2004. 42(3): 1109-1114</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220376900026">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220376900026</a> </p><br />

<p>  51.    43304   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Antibiotics GE23077, novel inhibitors of bacterial RNA polymerase I. Taxonomy, isolation and characterization</p>

<p>           Ciciliato, I, Corti, E, Sarubbia, E, Stefanelli, S, Gastaldo, L, Montanini, N, Kurz, M, Losi, D, Marinelli, F, and Selva, E</p>

<p>           J ANTIBIOT 2004. 57(3): 210-217</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220465900006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220465900006</a> </p><br />

<p>  52.    43305   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of analogues of S-adenosyl-L-methionine, as inhibitors of the E-coli cyclopropane fatty acid synthase</p>

<p>           Guerard, C, Breard, M, Courtois, F, Drujon, T, and Ploux, O</p>

<p>           BIOORG MED CHEM LETT 2004. 14(7): 1661-1664</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220411200011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220411200011</a> </p><br />

<p>  53.    43306   OI-LS-294; WOS-OI-4/18/2004</p>

<p class="memofmt1-2">           Mannich reaction: an approach for the synthesis of water soluble mulundocandin analogues</p>

<p>           Lal, B, Gund, VG, Bhise, NB, and Gangopadhyay, AK</p>

<p>           BIOORGAN MED CHEM 2004. 12(7): 1751-1768</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220481200016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220481200016</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
