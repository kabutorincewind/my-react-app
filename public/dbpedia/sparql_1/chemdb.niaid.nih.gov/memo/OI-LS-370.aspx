

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-370.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ty50eFx2RDdKa2gdRYQkF+sjixQoHOTOVbyqLwE4iI4ybaaO4/1ILNqiOJHD5DcKG7OCCqUpjv6otOWL+ipukwKyuMX40oaF1dFfp/QpwQnO+VPFfv+4Z4Gv0wU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6D5836C1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-370-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60442   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          Evaluation of N-(phenylmethyl)-4-[5-(phenylmethyl)-4,5,6,7-tetrahydro-1 H - imidazo[4,5-c]pyridin-4-yl]benzamide inhibitors of Mycobacterium tuberculosis growth</p>

    <p>          Wall, Michael D, Oshin, Michael, Chung, Gavin AC, Parkhouse, Tony, Gore, Andrea, Herreros, Esperanza, Cox, Brian, Duncan, Ken, Evans, Brian, Everett, Martin, and Mendoza, Alfonso</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 734</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N5TN59-D/2/0c4039958ca7ea80fb15acb559ab8a28">http://www.sciencedirect.com/science/article/B6TF9-4N5TN59-D/2/0c4039958ca7ea80fb15acb559ab8a28</a> </p><br />

    <p>2.     60443   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Hepatitis C virus infection and hepatotoxicity during antituberculosis chemotherapy</p>

    <p>          Kwon, YS, Koh, WJ, Suh, GY, Chung, MP, Kim, H, and Kwon, OJ</p>

    <p>          Chest <b>2007</b>.  131(3): 803-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17356096&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17356096&amp;dopt=abstract</a> </p><br />

    <p>3.     60444   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Antiviral profiles of novel iminocyclitol compounds against bovine viral diarrhea virus, West Nile virus, dengue virus and hepatitis B virus</p>

    <p>          Gu, B, Mason, P, Wang, L, Norton, P, Bourne, N, Moriarty, R, Mehta, A, Despande, M, Shah, R, and Block, T</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(1): 49-59</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17354651&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17354651&amp;dopt=abstract</a> </p><br />

    <p>4.     60445   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          Synthesis, structural activity relationship and anti-tubercular activity of novel pyrazoline derivatives</p>

    <p>          Ali, Mohamed Ashraf, Shaharyar, Mohammad, and Siddiqui, Anees Ahamed</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(2): 268-275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4M04HTT-1/2/e60f9cd203b48031c2843c3b620dc484">http://www.sciencedirect.com/science/article/B6VKY-4M04HTT-1/2/e60f9cd203b48031c2843c3b620dc484</a> </p><br />

    <p>5.     60446   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          The Role of Homoserine Transacetylase as a New Target for Antifungal Agents</p>

    <p>          Nazi, I, Scott, A, Sham, A, Rossi, L, Williamson, PR, Kronstad, JW, and Wright, GD</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17353245&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17353245&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60447   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Enoyl reductases as targets for the development of anti-tubercular and anti-malarial agents</p>

    <p>          Oliveira, JS, Vasconcelos, IB, Moreira, IS, Santos, DS, and Basso, LA</p>

    <p>          Curr Drug Targets <b>2007</b>.  8(3): 399-411</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348833&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348833&amp;dopt=abstract</a> </p><br />

    <p>7.     60448   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Totarol Inhibits Bacterial Cytokinesis by Perturbing the Assembly Dynamics of FtsZ</p>

    <p>          Jaiswal, R, Beuria, TK, Mohan, R, Mahajan, SK, and Panda, D</p>

    <p>          Biochemistry <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348691&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348691&amp;dopt=abstract</a> </p><br />

    <p>8.     60449   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Antimycobacterial activity of bacteriocins and their complexes with liposomes</p>

    <p>          Sosunov, V, Mischenko, V, Eruslanov, B, Svetoch, E, Shakina, Y, Stern, N, Majorov, K, Sorokoumova, G, Selishcheva, A, and Apt, A</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17347179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17347179&amp;dopt=abstract</a> </p><br />

    <p>9.     60450   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          FtsZ: a novel target for tuberculosis drug discovery</p>

    <p>          Huang, Q, Tonge, PJ, Slayden, RA, Kirikae, T, and Ojima, I</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(5): 527-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346197&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346197&amp;dopt=abstract</a> </p><br />

    <p>10.   60451   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Screening for novel antituberculosis agents that are effective against multidrug resistant tuberculosis</p>

    <p>          Matsumoto, M, Hashizume, H, Tsubouchi, H, Sasaki, H, Itotani, M, Kuroda, H, Tomishige, T, Kawasaki, M, and Komatsu, M</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(5): 499-507</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346195&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346195&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60452   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Purine metabolism in Mycobacterium tuberculosis as a target for drug development</p>

    <p>          Parker, WB and Long, MC</p>

    <p>          Curr Pharm Des  <b>2007</b>.  13(6): 599-608</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346177&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346177&amp;dopt=abstract</a> </p><br />

    <p>12.   60453   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Galactose-derived phosphonate analogues as potential inhibitors of phosphatidylinositol biosynthesis in mycobacteria</p>

    <p>          Dinev, Z, Gannon, CT, Egan, C, Watt, JA, McConville, MJ, and Williams, SJ</p>

    <p>          Org Biomol Chem <b>2007</b>.  5(6): 952-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17340011&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17340011&amp;dopt=abstract</a> </p><br />

    <p>13.   60454   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Synthesis and in vitro antimicrobial activity of new 2-[p-substituted-benzyl]-5-[substituted-carbonylamino]benzoxazoles</p>

    <p>          Tekiner-Gulbas, B, Temiz-Arpaci, O, Yildiz, I, and Altanlar, N</p>

    <p>          Eur J Med Chem  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17337097&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17337097&amp;dopt=abstract</a> </p><br />

    <p>14.   60455   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Synthesis of novel triazole derivatives as inhibitors of cytochrome P450 14alpha-demethylase (CYP51)</p>

    <p>          Sun, QY, Xu, JM, Cao, YB, Zhang, WN, Wu, QY, Zhang, DZ, Zhang, J, Zhao, HQ, and Jiang, YY</p>

    <p>          Eur J Med Chem  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335940&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335940&amp;dopt=abstract</a> </p><br />

    <p>15.   60456   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Protein Flexibility and Species Specificity in Structure-Based Drug Discovery: Dihydrofolate Reductase as a Test System</p>

    <p>          Bowman, AL, Lerner, MG, and Carlson, HA</p>

    <p>          J Am Chem Soc <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335207&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335207&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60457   OI-LS-370; PUBMED-OI-3/19/2007</p>

    <p class="memofmt1-2">          Novel medicine to prevent fungal infections</p>

    <p>          Anon</p>

    <p>          FDA Consum <b>2006</b>.  40(6): 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17333551&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17333551&amp;dopt=abstract</a> </p><br />

    <p>17.   60458   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          QSAR analysis for heterocyclic antifungals</p>

    <p>          Duchowicz, Pablo R, Vitale, Martin G, Castro, Eduardo A, Fernandez, Michael, and Caballero, Julio</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4MWPSGJ-4/2/3910bc14a5ba6698b087efc2d44c5169">http://www.sciencedirect.com/science/article/B6TF8-4MWPSGJ-4/2/3910bc14a5ba6698b087efc2d44c5169</a> </p><br />

    <p>18.   60459   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          Design, synthesis of novel antifungal triazole derivatives with high activities against Aspergillus fumigatus</p>

    <p>          He, Qiu Qin, Liu, Chao Mei, Li, Ke, and Cao, Yong Bing</p>

    <p>          Chinese Chemical Letters <b>2007</b>.  In Press, Corrected Proof: 1607</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B8G3X-4N7Y8HM-N/2/47bfd87b0cd384c1468db435633ec554">http://www.sciencedirect.com/science/article/B8G3X-4N7Y8HM-N/2/47bfd87b0cd384c1468db435633ec554</a> </p><br />

    <p>19.   60460   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          Further Studies on Hepatitis C Virus NS5B RNA-Dependent RNA Polymerase Inhibitors toward Improved Replicon Cell Activities: Benzimidazole and Structurally Related Compounds Bearing 2-Morpholinophenyl Moiety</p>

    <p>          Hirashima, Shintaro, Oka, Takahiro, Ikegashira, Kazutaka, Noji, Satoru, Yamanaka, Hiroshi, Hara, Yoshinori, Goto, Hiroyuki, Mizojiri, Ryo, Niwa, Yasushi, Noguchi, Toru, Ando, Izuru, Ikeda, Satoru, and Hashimoto, Hiromasa</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N85B0V-2/2/614bd11b02165b3186373cb3d3db174a">http://www.sciencedirect.com/science/article/B6TF9-4N85B0V-2/2/614bd11b02165b3186373cb3d3db174a</a> </p><br />

    <p>20.   60461   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          Development of Hepatitis C Virus (HCV) Chimeric Replicons for Identifying Broad Spectrum NS3 Protease Inhibitors</p>

    <p>          Binder, Joseph, Tetangco, Selwyna, Wick, Megan, Maegley, Karen, Lingardo, Laura, Patick, Amy, and Smith, George</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-11/2/d4581f8106157e70c64539a99dd519cd">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-11/2/d4581f8106157e70c64539a99dd519cd</a> </p><br />
    <br clear="all">

    <p>21.   60462   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          Development of Intergenotypic Chimeric Replicons for Broad-spectrum Antiviral Activity Characterization of Hepatitis C Virus Polymerase Inhibitors</p>

    <p>          Herlihy, Koleen</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2T/2/dcfb274dc58c784759abb8d22c124238">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2T/2/dcfb274dc58c784759abb8d22c124238</a> </p><br />

    <p>22.   60463   OI-LS-370; EMBASE-OI-3/19/2007</p>

    <p class="memofmt1-2">          17-AAG, an Hsp90 Inhibitor, Suppress Hepatitis C Virus (HCV) Replication</p>

    <p>          Ujino, Saneyuki, Shimotohno, Kunitada, and Takaku, Hiroshi</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-33/2/4d001222c555b050d592b8cc0b7d6dde">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-33/2/4d001222c555b050d592b8cc0b7d6dde</a> </p><br />

    <p>23.   60464   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Synergistic fungicidal activities of amphotericin B and N-methyl-N &#39;&#39;-dodecylguanidine: A constituent of polyol macrolide antibiotic niphimycin</p>

    <p>          Ogita, A, Matsumoto, K, Fujita, K, Usuki, Y, Hatanaka, Y, and Tanaka, T</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2007</b>.  60(1): 27-35, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244134600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244134600004</a> </p><br />

    <p>24.   60465   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Intranasal IFN gamma and anti alpha-crystallin IgA inhibit Mycobacterium tuberculosis lung infection in mice</p>

    <p>          Reljic, R, Williams, A, Clark, S, Marsh, P, and Ivanyi, J</p>

    <p>          IMMUNOLOGY <b>2007</b>.  120: 43-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244080000166">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244080000166</a> </p><br />

    <p>25.   60466   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Inhibitory effects of oligodeoxynucleotides on hepatitis C viral translation</p>

    <p>          Li, XY and Wimmer, E</p>

    <p>          HEPATOLOGY <b>2006</b>.  44(4): 347A-348A, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300422">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300422</a> </p><br />

    <p>26.   60467   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Robust suppression of viral replication by a nucleoside polymerase inhibitor in chimpanzees infected with hepatitis C virus</p>

    <p>          Carroll, SS, Davies, ME, Handt, L, Koeplinger, K, Zhang, R, Ludmerer, S, MacCoss, M, Hazuda, D, and Olsen, D</p>

    <p>          HEPATOLOGY <b>2006</b>.  44(4): 535A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302029</a> </p><br />
    <br clear="all">

    <p>27.   60468   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Viral dynamics study with HCVNS3-4A protease inhibitor MP-424/VX-950 in human-chimeric liver mouse model for hepatitis C virus infection</p>

    <p>          Kamiya, N, Iwao, E, Hiraga, N, Imamura, M, Takahashi, S, and Chayama, K</p>

    <p>          HEPATOLOGY <b>2006</b>.  44(4): 535A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302030</a> </p><br />

    <p>28.   60469   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Glucosidase inhibitors induce HCV glycoproteins misfolding, impair viral assembly and release, and reduce the infectivity of residually secreted HCV particles</p>

    <p>          Chapel, C, Garcia, C, Bartosch, B, Roingeard, P, Zitzmann, N, Cosset, FL, Dubuisson, J, Dwek, R, Trepo, C, Zoulim, F, and Durantel, D</p>

    <p>          HEPATOLOGY <b>2006</b>.  44(4): 198A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300027</a> </p><br />

    <p>29.   60470   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Effect of an IMPDH inhibitor, merimepodib (MMPD), assessed alone and in combination with ribavirin, on HCV replication: Implications regarding ribavirin&#39;s mechanism of action</p>

    <p>          Hezode, C, Bouvier-Alias, M, Costentin, C, Medkour, F, Franc-Poole, E, Aalyson, M, Alam, J, Dhumeaux, D, and Pawlotsky, JM</p>

    <p>          HEPATOLOGY <b>2006</b>.  44(4): 615A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302237">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302237</a> </p><br />

    <p>30.   60471   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Inhibition of HCV RNA synthesis by the expression of RNA structural mimicry</p>

    <p>          Smolic, R, Smith, RM, Volarevic, M, Andorfer, JH, Wu, CH, and Wu, GY</p>

    <p>          HEPATOLOGY <b>2006</b>.  44(4): 696A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302452">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302452</a> </p><br />

    <p>31.   60472   OI-LS-370; WOS-OI-3/12/2007</p>

    <p class="memofmt1-2">          Interim results from a randomized, double-blind, placebo-controlled phase 1b study in subjects with chronic HCV after treatment with GI-5005, a yeast-based HCV immunotherapy targeting NS3 and core proteins</p>

    <p>          Everson, GT, Tong, MJ, Jacobson, IM, Jensen, DM, Haller, AA, Lauer, GM, Parker, J, Ferraro, J, Cruickshank, SE, Duke, RC, Apelian, D, Rodell, TC, and Schiff, ER</p>

    <p>          HEPATOLOGY <b>2006</b>.  44(4): 697A-698A, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302456">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302456</a> </p><br />

    <p>32.   60473   OI-LS-370; WOS-OI-3/18/2007</p>

    <p class="memofmt1-2">          Domain analysis of fatty acid synthase protein (NP_217040) from Mycobacterium tuberculosis H37Rv - A bioinformatics study</p>

    <p>          Ramesh, KV, Wagle, K, and Deshmukh, S</p>

    <p>          JOURNAL OF BIOMOLECULAR STRUCTURE &amp; DYNAMICS <b>2007</b>.  24(4): 393-412, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244281900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244281900008</a> </p><br />
    <br clear="all">

    <p>33.   60474   OI-LS-370; WOS-OI-3/18/2007</p>

    <p class="memofmt1-2">          Discovery of new drug candidates using structural chemoproteomics</p>

    <p>          Ro, S</p>

    <p>          DRUG METABOLISM REVIEWS <b>2006</b>.  38: 15-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242406900026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242406900026</a> </p><br />

    <p>34.   60475   OI-LS-370; WOS-OI-3/18/2007</p>

    <p class="memofmt1-2">          Development of drug resistance in Mycobacterium tuberculosis</p>

    <p>          Bergval, IL, Anthony, RM, Schuitema, AR, Oskam, L, and Klatser, PR</p>

    <p>          AMERICAN JOURNAL OF TROPICAL MEDICINE AND HYGIENE <b>2006</b> .  75(5): 118-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343900404">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343900404</a> </p><br />

    <p>35.   60476   OI-LS-370; WOS-OI-3/18/2007</p>

    <p class="memofmt1-2">          Synthesis and in vitro antifungal activity of novel tetralin compounds</p>

    <p>          Yao, B, Zhou, YJ, Zhu, J, Lu, JG, Jiang, YY, Chen, J, Cao, YB, Jiang, QF, and Zheng, CH</p>

    <p>          ACTA CHIMICA SINICA <b>2007</b>.  65(3): 257-264, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244262800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244262800014</a> </p><br />

    <p>36.   60477   OI-LS-370; WOS-OI-3/18/2007</p>

    <p class="memofmt1-2">          Towards TB control in Nigeria targeting delay in TB treatment care seeking</p>

    <p>          Ilika, AL and Obiora, OE</p>

    <p>          AMERICAN JOURNAL OF TROPICAL MEDICINE AND HYGIENE <b>2006</b> .  75(5): 25-26, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343900085">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343900085</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
