

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-108.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="63e6WVT8oFBFsh5CXw2aN7GiK0NGvN1cNF4T/ljRi2HJLiYzCFQP+FyHNBI4FuFJPIH5UOQ87X9ELnaJ/zaQ6AjxrX2+GaalPopQmOb1oNXRBQhj5/o2iWJJajg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1E24D439" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-108-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58074   DMID-LS-108; EMBASE-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification and characterization of potent small molecule inhibitor of hemorrhagic fever New World arenaviruses</p>

    <p>          Bolken, Tove&#39; C, Laquerre, Sylvie, Zhang, Yuanming, Bailey, Thomas R, Pevear, Daniel C, Kickner, Shirley S, Sperzel, Lindsey E, Jones, Kevin F, Warren, Travis K, and Amanda Lund, S</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4HNSNT0-2/2/9cd590378e628d56b7d8e8e876db8278">http://www.sciencedirect.com/science/article/B6T2H-4HNSNT0-2/2/9cd590378e628d56b7d8e8e876db8278</a> </p><br />

    <p>2.     58075   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral hepatitis and antiretroviral drug interactions</p>

    <p>          Perronne, C</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16360231&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16360231&amp;dopt=abstract</a> </p><br />

    <p>3.     58076   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Parallel synthesis of 5-cyano-6-aryl-2-thiouracil derivatives as inhibitors for hepatitis C viral NS5B RNA-dependent RNA polymerase</p>

    <p>          Ding, Y, Girardet, JL, Smith, KL, Larson, G, Prigaro, B, Wu, JZ, and Yao, N</p>

    <p>          Bioorg Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16360193&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16360193&amp;dopt=abstract</a> </p><br />

    <p>4.     58077   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral profile of Nyctanthes arbortristis L. against encephalitis causing viruses</p>

    <p>          Gupta, P, Bajpai, SK, Chandra, K, Singh, KL, and Tandon, JS</p>

    <p>          Indian J Exp Biol <b>2005</b>.  43(12): 1156-1160</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359127&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359127&amp;dopt=abstract</a> </p><br />

    <p>5.     58078   DMID-LS-108; EMBASE-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of 2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methyl purine nucleosides as inhibitors of hepatitis C virus RNA replication</p>

    <p>          Clark, Jeremy L, Mason, JChristian, Hollecker, Laurent, Stuyver, Lieven J, Tharnish, Phillip M, McBrayer, Tamara R, Otto, Michael J, Furman, Phillip A, Schinazi, Raymond F, and Watanabe, Kyoichi A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HVDJC5-3/2/822e3656322db8447fe04a70fc237dcf">http://www.sciencedirect.com/science/article/B6TF9-4HVDJC5-3/2/822e3656322db8447fe04a70fc237dcf</a> </p><br />
    <br clear="all">

    <p>6.     58079   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Acyclic Nucleoside Phosphonates: a Key Class of Antiviral Drugs</p>

    <p>          De Clercq, E. and Holy, A.</p>

    <p>          Nature Reviews Drug Discovery <b>2005</b>.  4(11): 928-940</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233304600019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233304600019</a> </p><br />

    <p>7.     58080   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel 4&#39;-hydroxymethyl branched thioapiosyl nucleosides</p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.  338(12): 577-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16353276&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16353276&amp;dopt=abstract</a> </p><br />

    <p>8.     58081   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Demonstration of the Efficacy of Small Molecule Hcv Inhibitors in the Kmt Mouse Model of Hepatitis C Virus (Hcv) Infection</p>

    <p>          Kneteman, N.</p>

    <p>          Hepatology <b>2005</b>.  42(4): 532A-533A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301398">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301398</a> </p><br />

    <p>9.     58082   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Novel, Highly Selective Inhibitor of Pestivirus Replication That Targets the Viral RNA-Dependent RNA Polymerase</p>

    <p>          Paeshuyse, J, Leyssen, P, Mabery, E, Boddeker, N, Vrancken, R, Froeyen, M, Ansari, IH, Dutartre, H, Rozenski, J, Gil, LH, Letellier, C, Lanford, R, Canard, B, Koenen, F, Kerkhofs, P, Donis, RO, Herdewijn, P, Watson, J, De, Clercq E, Puerstinger, G, and Neyts, J</p>

    <p>          J Virol <b>2006</b>.  80(1): 149-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16352539&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16352539&amp;dopt=abstract</a> </p><br />

    <p>10.   58083   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nim811 Exhibits Potent Anti-Hcv Activity in Vitro and Represents a Novel Approach for Viral Hepatitis C Therapy</p>

    <p>          Lin, K. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 536A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301407">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301407</a> </p><br />

    <p>11.   58084   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiherpetic activities of flavonoids against herpes simplex virus type 1 (HSV-1) and type 2 (HSV-2) in vitro</p>

    <p>          Lyu, SY, Rhim, JY, and Park, WB</p>

    <p>          Arch Pharm Res  <b>2005</b>.  28(11): 1293-301</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16350858&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16350858&amp;dopt=abstract</a> </p><br />

    <p>12.   58085   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral treatment is more effective than smallpox vaccination upon lethal monkeypox virus infection</p>

    <p>          Stittelaar, KJ, Neyts, J, Naesens, L, van, Amerongen G, van, Lavieren RF, Holy, A, De, Clercq E, Niesters, HG, Fries, E, Maas, C, Mulder, PG, van, der Zeijst BA, and Osterhaus, AD</p>

    <p>          Nature <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16341204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16341204&amp;dopt=abstract</a> </p><br />

    <p>13.   58086   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          High Selectivity and Low Cytotoxicity Associated With R1479, a Novel Inhibitor of Hcv Replication</p>

    <p>          Jiang, W. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 655A-656A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302227">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302227</a> </p><br />

    <p>14.   58087   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synergistic in vitro Interactions between (22S,23S)-3beta-Bromo-5alpha,22,23-Trihydroxystigmastan-6-one and Acyclovir or Foscarnet against Herpes simplex Virus Type 1</p>

    <p>          Talarico, LB, Castilla, V, Ramirez, JA, Galagovsky, LR, and Wachsman, MB</p>

    <p>          Chemotherapy <b>2005</b>.  52(1): 38-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16340198&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16340198&amp;dopt=abstract</a> </p><br />

    <p>15.   58088   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Coronavirus pathogenesis and the emerging pathogen severe acute respiratory syndrome coronavirus</p>

    <p>          Weiss, SR and Navas-Martin, S</p>

    <p>          Microbiol Mol Biol Rev <b>2005</b>.  69(4): 635-64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16339739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16339739&amp;dopt=abstract</a> </p><br />

    <p>16.   58089   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacokinetics of Vx-950, and Its Effect on Hepatitis C Viral Dynamics</p>

    <p>          Chu, H. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 694A-695A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302318">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302318</a> </p><br />

    <p>17.   58090   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SARS coronavirus, but not human coronavirus NL63, utilizes cathepsin L to infect ACE2-expressing cells</p>

    <p>          Huang, IC, Bosch, BJ, Li, F, Li, W, Lee, KH, Ghiran, S, Vasilieva, N, Dermody, TS, Harrison, SC, Dormitzer, PR, Farzan, M, Rottier, PJ, and Choe, H</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16339146&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16339146&amp;dopt=abstract</a> </p><br />

    <p>18.   58091   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virology of hepatitis B and C viruses and antiviral targets</p>

    <p>          Pawlotsky, JM</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16338022&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16338022&amp;dopt=abstract</a> </p><br />

    <p>19.   58092   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and biological activities of novel inhibitory peptides for SARS-CoV spike protein and angiotensin-converting enzyme 2 interaction</p>

    <p>          Ho, TY, Wu, SL, Chen, JC, Wei, YC, Cheng, SE, Chang, YH, Liu, HJ, and Hsiang, CY</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16337697&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16337697&amp;dopt=abstract</a> </p><br />

    <p>20.   58093   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent Advances in Discovery and Development of Promising Therapeutics Against Hepatitis C Virus Ns5b Rna-Dependent Rna Polymerase</p>

    <p>          Wu, J. <i>et al.</i></p>

    <p>          Mini-Reviews in Medicinal Chemistry <b>2005</b>.  5(12): 1103-1112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233099600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233099600006</a> </p><br />

    <p>21.   58094   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cottontail rabbit papillomavirus (CRPV) model system to test antiviral and immunotherapeutic strategies</p>

    <p>          Christensen, ND</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(6): 355-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16331841&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16331841&amp;dopt=abstract</a> </p><br />

    <p>22.   58095   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The N-terminal octapeptide acts as a dimerization inhibitor of SARS coronavirus 3C-like proteinase</p>

    <p>          Wei, P, Fan, K, Chen, H, Ma, L, Huang, C, Tan, L, Xi, D, Li, C, Liu, Y, Cao, A, and Lai, L</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  339(3): 865-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16329994&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16329994&amp;dopt=abstract</a> </p><br />

    <p>23.   58096   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design of Wide-Spectrum Inhibitors Targeting Coronavirus Main Proteases (Vol 3, Pg E428, 2005)</p>

    <p>          Yang, H. <i>et al.</i></p>

    <p>          Plos Biology <b>2005</b>.  3(11): 2044</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233609300024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233609300024</a> </p><br />
    <br clear="all">

    <p>24.   58097   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro anti-HCV activities of Saxifraga melanocentra and its related polyphenolic compounds</p>

    <p>          Zuo, GY, Li, ZQ, Chen, LR, and Xu, XJ</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(6): 393-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16329286&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16329286&amp;dopt=abstract</a> </p><br />

    <p>25.   58098   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Drugs for Prophylaxis and Treatment of Influenza</p>

    <p>          Anon</p>

    <p>          Medical Letter on Drugs and Therapeutics <b>2005</b>.  47(1222 ): 93-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233653400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233653400001</a> </p><br />

    <p>26.   58099   DMID-LS-108; PUBMED-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of the marine alga Symphyocladia latiuscula against herpes simplex virus (HSV-1) in vitro and its therapeutic efficacy against HSV-1 infection in mice</p>

    <p>          Park, HJ, Kurokawa, M, Shiraki, K, Nakamura, N, Choi, JS, and Hattori, M</p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(12): 2258-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16327161&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16327161&amp;dopt=abstract</a> </p><br />

    <p>27.   58100   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activities of Three Bangladeshi Medicinal Plant Extracts Against Herpes Simplex Viruses</p>

    <p>          Thompson, K., Ather, A., and Khan, M.</p>

    <p>          Minerva Biotecnologica <b>2005</b>.  17(3): 193-199</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233265800012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233265800012</a> </p><br />

    <p>28.   58101   DMID-LS-108; EMBASE-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          From genome to drug lead: Identification of a small-molecule inhibitor of the SARS virus</p>

    <p>          Dooley, Andrea J, Shindo, Nice, Taggart, Barbara, Park, Jewn-Giew, and Pang, Yuan-Ping</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HPD3M9-2/2/b4c9cc2ad8c79bb1e7e1a507dd243a81">http://www.sciencedirect.com/science/article/B6TF9-4HPD3M9-2/2/b4c9cc2ad8c79bb1e7e1a507dd243a81</a> </p><br />

    <p>29.   58102   DMID-LS-108; EMBASE-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and structure-activity relationships of novel poly(ADP-ribose) polymerase-1 inhibitors</p>

    <p>          Tao, Ming, Park, Chung Ho, Bihovsky, Ron, Wells, Gregory J, Husten, Jean, Ator, Mark A, and Hudkins, Robert L</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HK03DC-7/2/a3e90f3d2909238c36fe128ac0bdbc93">http://www.sciencedirect.com/science/article/B6TF9-4HK03DC-7/2/a3e90f3d2909238c36fe128ac0bdbc93</a> </p><br />
    <br clear="all">

    <p>30.   58103   DMID-LS-108; WOS-DMID-12/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of a Novel Small Molecule Hepatitis C Virus Replication Inhibitor That Targets Host Sphingolipid Biosynthesis</p>

    <p>          Sakamoto, H. <i>et al.</i></p>

    <p>          Hepatology <b>2005</b>.  42(4): 535A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301404">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301404</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
