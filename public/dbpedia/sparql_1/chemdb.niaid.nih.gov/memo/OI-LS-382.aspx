

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-382.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="70f2APFy7YKWBtP1tSWztJt9tBW6J6cfTkhfQtPmJY15gjTXY1gmsOuniUe83GkUc1potpOtPVtCCvQkDYDsFoHEvsU0ZOYXgpSRM0d69Aon1owBimpJoPd68RQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F8D3C94B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-382-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61572   OI-LS-382; WOS-OI-8/24/2007</p>

    <p class="memofmt1-2">          Cyclic Cidofovir (Chpmpc) Prevents Congenital Cytomegalovirus Infection in a Guinea Pig Model</p>

    <p>          Schleiss, M., Anderson, J., and Mcgregor, A.</p>

    <p>          Virology Journal <b>2006</b>.  3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247494800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247494800001</a> </p><br />

    <p>2.     61573   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Synthesis, biological activity, and SAR of antimycobacterial 2- and 8-substituted 6-(2-furyl)-9-(p-methoxybenzyl)purines</p>

    <p>          Braendvang, Morten and Gundersen, Lise-Lotte</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 6083</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-9/2/f095dfa0ccdb8c542d480c3ee36982cd">http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-9/2/f095dfa0ccdb8c542d480c3ee36982cd</a> </p><br />

    <p>3.     61574   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of a C5-biphenyl thiolactomycin library</p>

    <p>          Bhowruth, Veemal, Brown, Alistair K, Senior, Suzanne J, Snaith, John S, and Besra, Gurdyal S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  6083</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4PGGP17-7/2/e1dfd03bf88c505069feb80360a270f1">http://www.sciencedirect.com/science/article/B6TF9-4PGGP17-7/2/e1dfd03bf88c505069feb80360a270f1</a> </p><br />

    <p>4.     61575   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activity of lipophilic moxifloxacin and gatifloxacin derivatives</p>

    <p>          de Almeida, Mauro V, Saraiva, Mauricio F, de Souza, Marcus VN, da Costa, Cristiane F, Vicente, Felipe RC, and Lourenco, Maria CS</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  6083</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4PG86YW-4/2/601c128cf3768a5a1f3b4d469b5fb32a">http://www.sciencedirect.com/science/article/B6TF9-4PG86YW-4/2/601c128cf3768a5a1f3b4d469b5fb32a</a> </p><br />

    <p>5.     61576   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Synthesis and antituberculosis activity of new thiazolylhydrazone derivatives</p>

    <p>          Turan-Zitouni, Gulhan, Ozdemir, Ahmet, Asim Kaplancikli, Zafer, Benkli, Kadriye, Chevallet, Pierre, and Akalin, Gulsen</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4P6M7WK-4/2/a72cffbae32456e7a7fd6e1769de7de2">http://www.sciencedirect.com/science/article/B6VKY-4P6M7WK-4/2/a72cffbae32456e7a7fd6e1769de7de2</a> </p><br />

    <p>6.     61577   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Antimycobacterial activity of diphenylpyraline derivatives</p>

    <p>          Weis, Robert, Faist, Johanna, di Vora, Ulrike, Schweiger, Klaus, Brandner, Barbara, Kungl, Andreas J, and Seebacher, Werner</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4P59WXV-6/2/1fe389b72dc1cb518a6ed27a5cb8e68c">http://www.sciencedirect.com/science/article/B6VKY-4P59WXV-6/2/1fe389b72dc1cb518a6ed27a5cb8e68c</a> </p><br />

    <p>7.     61578   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Phagocytic activity of alveolar macrophages toward polystyrene latex microspheres and PLGA microspheres loaded with anti-tuberculosis agent</p>

    <p>          Hasegawa, Taizo, Hirota, Keiji, Tomoda, Keishiro, Ito, Fuminori, Inagawa, Hiroyuki, Kochi, Chie, Soma, Gen-Ichiro, Makino, Kimiko, and Terada, Hiroshi</p>

    <p>          Colloids and Surfaces B: Biointerfaces <b>2007</b>.  In Press, Corrected Proof: 173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TFS-4P192G2-2/2/9ad081a7d4bd756eac468cb42f581c75">http://www.sciencedirect.com/science/article/B6TFS-4P192G2-2/2/9ad081a7d4bd756eac468cb42f581c75</a> </p><br />

    <p>8.     61579   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Virulence of Mycobacterium avium in mice does not correlate with resistance to nitric oxide</p>

    <p>          Lousada, Susana, Florido, Manuela, and Appelberg, Rui</p>

    <p>          Microbial Pathogenesis <b>2007</b>.  In Press, Corrected Proof: 113</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WN6-4P40KJB-1/2/ebde061cd57f3f6b3261d186ba5582ba">http://www.sciencedirect.com/science/article/B6WN6-4P40KJB-1/2/ebde061cd57f3f6b3261d186ba5582ba</a> </p><br />

    <p>9.     61580   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Cryptosporidium parvum spermidine/spermine N1-acetyltransferase exhibits different characteristics from the host enzyme</p>

    <p>          Yarlett, Nigel, Wu, Gang, Waters, WRay, Harp, James A, Wannemuehler, Michael J, Morada, Mary, Athanasopoulos, Demos, Martinez, Martha P, Upton, Steve J, Marton, Laurence J, and Frydman, Benjamin J</p>

    <p>          Molecular and Biochemical Parasitology <b>2007</b>.  152(2): 170-180</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T29-4MSHXW6-2/2/976df31b16e0b97ca79537c210342edd">http://www.sciencedirect.com/science/article/B6T29-4MSHXW6-2/2/976df31b16e0b97ca79537c210342edd</a> </p><br />

    <p>10.   61581   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Synthesis of chlorogenic acid derivatives with promising antifungal activity</p>

    <p>          Ma, Chao-Mei, Kully, Maureen, Khan, Jehangir K, Hattori, Masao, and Daneshtalab, Mohsen</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 1233</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-F/2/395b904b5c2593ca239d289f004ce2ce">http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-F/2/395b904b5c2593ca239d289f004ce2ce</a> </p><br />

    <p>11.   61582   OI-LS-382; WOS-OI-8/24/2007</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antifungal Activities of Novel 1h-Triazole Derivatives Based on the Structure of the Active Site of Fungal Lanosterol 14 Alpha-Demethylase (Cyp51)</p>

    <p>          Zhao, Q. <i>et al.</i></p>

    <p>          Chemistry &amp; Biodiversity <b>2007</b>.  4(7): 1472-1479</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248370000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248370000005</a> </p><br />

    <p>12.   61583   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Nitazoxanide, tizoxanide and other thiazolides are potent inhibitors of hepatitis B virus and hepatitis C virus replication</p>

    <p>          Korba, Brent E, Mueller, Abigail B, Farrar, Kristine, Gaye, Karen, Mukerjee, Sampa, Ayers, Marc S, and Rossignol, Jean-Francois</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 5149</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4PK8K5F-2/2/207706b6ee0c8fac1bdfa00b56c78c09">http://www.sciencedirect.com/science/article/B6T2H-4PK8K5F-2/2/207706b6ee0c8fac1bdfa00b56c78c09</a> </p><br />

    <p>13.   61584   OI-LS-382; EMBASE-OI-9/4/2007</p>

    <p class="memofmt1-2">          Novel Potent Macrocyclic Inhibitors of the Hepatitis C Virus NS3 Protease. Use of Cyclopentane and Cyclopentene P2-Motifs</p>

    <p>          Back, Marcus, Johansson, Per-Ola, Wangsell, Fredrik, Thorstensson, Fredrik, Kvarnstrom, Ingemar, Alvarez, Susana Ayesa, Wahling, Horst, Pelcman, Mikael, Jansson, Katarina, Lindstrom, Stefan, Wallberg, Hans, Classon, Bjorn, Rydergard, Christina, Vrang, Lotta, Hamelink, Elizabeth, Hallberg, Anders, Rosenquist, Asa, and Samuelsson, Bertil</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript: 1367</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-6/2/6647437477a6490e810c6c29082b0c67">http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-6/2/6647437477a6490e810c6c29082b0c67</a> </p><br />

    <p>14.   61585   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          5&#39;-O-Masked 2&#39;-deoxyadenosine analogues as lead compounds for hepatitis C virus (HCV) therapeutic agents</p>

    <p>          Ikejiri, M, Ohshima, T, Kato, K, Toyama, M, Murata, T, Shimotohno, K, and Maruyama, T</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17766124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17766124&amp;dopt=abstract</a> </p><br />

    <p>15.   61586   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Design, synthesis and antitubercular activity of diarylmethylnaphthol derivatives</p>

    <p>          Das, SK, Panda, G, Chaturvedi, V, Manju, YS, Gaikwad, AK, and Sinha, S</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17764933&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17764933&amp;dopt=abstract</a> </p><br />

    <p>16.   61587   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Impact of NS5A Sequences of Hepatitis C Virus Genotype 1a on Early Viral Kinetics during Treatment with Peginterferon- alpha 2a Plus Ribavirin</p>

    <p>          Dal Pero, F, Tang, KH, Gerotto, M, Bortoletto, G, Paulon, E, Herrmann, E, Zeuzem, S, Alberti, A, and Naoumov, NV</p>

    <p>          J Infect Dis <b>2007</b>.  196(7): 998-1005</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17763320&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17763320&amp;dopt=abstract</a> </p><br />

    <p>17.   61588   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Identification of acetylated, tetrahalogenated benzimidazole D-ribonucleosides with enhanced activity against human cytomegalovirus</p>

    <p>          Hwang, JS, Kregler, O, Schilf, R, Bannert, N, Drach, JC, Townsend, LB, and Bogner, E</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17728228&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17728228&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   61589   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Concentration-dependent Mycobacterium tuberculosis killing and prevention of resistance by rifampin</p>

    <p>          Gumbo, T, Louie, A, Deziel, MR, Liu, W, Parsons, LM, Salfinger, M, and Drusano, GL</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724157&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724157&amp;dopt=abstract</a> </p><br />

    <p>19.   61590   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          The farnesyl diphosphate/geranylgeranyl diphosphate synthase of toxoplasma gondii is a bifunctional enzyme and a molecular target of bisphosphonates</p>

    <p>          Ling, Y, Li, ZH, Miranda, K, Oldfield, E, and Moreno, SN</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724033&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724033&amp;dopt=abstract</a> </p><br />

    <p>20.   61591   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of new chalcones containing piperazine or  2,5-dichlorothiophene moiety</p>

    <p>          Tomar, V, Bhattacharjee, G, Kamaluddin, and Ashok, Kumar</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17719779&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17719779&amp;dopt=abstract</a> </p><br />

    <p>21.   61592   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          In Vitro and In Vivo Activities of Novel Fluoroquinolones Alone and in Combination with Clarithromycin against Clinically Isolated Mycobacterium avium complex Strains in Japan</p>

    <p>          Kohno, Y, Ohno, H, Miyazaki, Y, Higashiyama, Y, Yanagihara, K, Hirakata, Y, Fukushima, K, and Kohno, S</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17709469&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17709469&amp;dopt=abstract</a> </p><br />

    <p>22.   61593   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Comparative aspects on the role of polypyrimidine tract-binding protein in internal initiation of hepatitis C virus and picornavirus RNAs</p>

    <p>          Nishimura, T, Saito, M, Takano, T, Nomoto, A, Kohara, M, and Tsukiyama-Kohara, K</p>

    <p>          Comp Immunol Microbiol Infect Dis <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17706779&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17706779&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   61594   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Adverse drug reactions associated with first-line anti-tuberculosis drug regimens</p>

    <p>          Marra, F, Marra, CA, Bruchet, N, Richardson, K, Moadebi, S, Elwood, RK, and Fitzgerald, JM</p>

    <p>          Int J Tuberc Lung Dis <b>2007</b>.  11(8): 868-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17705952&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17705952&amp;dopt=abstract</a> </p><br />

    <p>24.   61595   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          A New Series of 3-Alkyl Phosphate Derivatives of 4,5,6,7-Tetrahydro-1-d-ribityl-1H-pyrazolo[3,4-d]pyrimidinedione as Inhibitors of Lumazine Synthase: Design, Synthesis, and Evaluation</p>

    <p>          Zhang, Y, Jin, G, Illarionov, B, Bacher, A, Fischer, M, and Cushman, M</p>

    <p>          J Org Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17705537&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17705537&amp;dopt=abstract</a> </p><br />

    <p>25.   61596   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Anti-hepatitis C virus activity of tamoxifen reveals the functional association of estrogen receptor with viral RNA polymerase NS5B</p>

    <p>          Watashi, K, Inoue, D, Hijikata, M, Goto, K, Aly, HH, and Shimotohno, K</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17704057&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17704057&amp;dopt=abstract</a> </p><br />

    <p>26.   61597   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Additive effect of ethanol and HCV subgenomic replicon expression on COX-2 protein levels and activity</p>

    <p>          Trujillo-Murillo, K, Alvarez-Martinez, O, Garza-Rodriguez, L, Martinez-Rodriguez, H, Bosques-Padilla, F, Ramos-Jimenez, J, Barrera-Saldana, H, Rincon-Sanchez, AR, and Rivas-Estilla, AM</p>

    <p>          J Viral Hepat <b>2007</b>.  14(9): 608-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17697012&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17697012&amp;dopt=abstract</a> </p><br />

    <p>27.   61598   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Biological evaluations of fatty acid esters originated during storage of Prasaplai, a Thai traditional medicine</p>

    <p>          Tangyuenyongwatana, P and Gritsanapan, W</p>

    <p>          Nat Prod Res <b>2007</b>.  21(11): 990-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17691048&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17691048&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   61599   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Nuclear factors are involved in hepatitis C virus RNA replication</p>

    <p>          Isken, O, Baroth, M, Grassmann, CW, Weinlich, S, Ostareck, DH, Ostareck-Lederer, A, and Behrens, SE</p>

    <p>          RNA <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17684232&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17684232&amp;dopt=abstract</a> </p><br />

    <p>29.   61600   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          How pantothenol intervenes in Coenzyme-A biosynthesis of Mycobacterium tuberculosis</p>

    <p>          Kumar, P, Chhibber, M, and Surolia, A</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.  361(4): 903-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17679145&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17679145&amp;dopt=abstract</a> </p><br />

    <p>30.   61601   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Fumagillin: an anti-infective as a parent molecule for novel angiogenesis inhibitors</p>

    <p>          Lefkove, B, Govindarajan, B, and Arbiser, JL</p>

    <p>          Expert Rev Anti Infect Ther <b>2007</b>.  5(4): 573-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17678422&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17678422&amp;dopt=abstract</a> </p><br />

    <p>31.   61602   OI-LS-382; PUBMED-OI-9/4/2007</p>

    <p class="memofmt1-2">          Cutting Edge: Vitamin D-Mediated Human Antimicrobial Activity against Mycobacterium tuberculosis Is Dependent on the Induction of Cathelicidin</p>

    <p>          Liu, PT, Stenger, S, Tang, DH, and Modlin, RL</p>

    <p>          J Immunol <b>2007</b>.  179(4): 2060-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17675463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17675463&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
