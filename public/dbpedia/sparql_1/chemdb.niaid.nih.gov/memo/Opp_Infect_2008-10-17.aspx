

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2008-10-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QZ2LbiNacewLduzStbftQWIpogs0THOU5bh49SN4cnpAXrRkWAzqdYOSkdTY1iX5MzYYx9J2XsY0JXMUkGK7TK08SygKr4iN1E42pTGu543bqMw4cNFcT5Yxx5g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="701C8E84" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">October 1 - October 14, 2008</p>

    <p class="memofmt2-2">Opportunistic Protozoa (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18771252?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Gangjee A, Adair OO, Pagley M, Queener SF.</a></p>

    <p class="NoSpacing">N9-substituted 2,4-diaminoquinazolines: synthesis and biological evaluation of lipophilic inhibitors of pneumocystis carinii and toxoplasma gondii dihydrofolate reductase.</p>

    <p class="NoSpacing">J Med Chem. 2008 Oct 9;51(19):6195-200. Epub 2008 Sep 5.     </p>

    <p class="NoSpacing">PMID: 18771252 [PubMed - in process]            </p> 

    <p class="ListParagraph">2.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18834108?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Bolstad DB, Bolstad ES, Frey KM, Wright DL, Anderson AC.</a></p>

    <p class="NoSpacing">Structure-Based Approach to the Development of Potent and Selective Inhibitors of Dihydrofolate Reductase from Cryptosporidium.</p>

    <p class="NoSpacing">J Med Chem. 2008 Oct 4. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 18834108 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">3.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18837587?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Strobl JS, Seibert CW, Li Y, Nagarkatti R, Mitchell SM, Rosypal AC, Rathore D, Lindsay DS.</a></p>

    <p class="NoSpacing">Inhibition of Toxoplasma gondii and Plasmodium falciparum infections in vitro by NSC3852, a redox active anti-proliferative and tumor cell differentiation agent.</p>

    <p class="NoSpacing">J Parasitol. 2008 Oct 6:1. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 18837587 [PubMed - as supplied by publisher]                </p> 

    <p class="ListParagraph">4.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18771252?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Gangjee A, Adair OO, Pagley M, Queener SF.</a></p>

    <p class="NoSpacing">N9-substituted 2,4-diaminoquinazolines: synthesis and biological evaluation of lipophilic inhibitors of pneumocystis carinii and toxoplasma gondii dihydrofolate reductase.</p>

    <p class="NoSpacing">J Med Chem. 2008 Oct 9;51(19):6195-200. Epub 2008 Sep 5.</p>

    <p class="NoSpacing">PMID: 18771252 [PubMed - in process]            </p> 

    <p class="ListParagraph">5.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18619816?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Choi KM, Gang J, Yun J.</a></p>

    <p class="NoSpacing">Anti-Toxoplasma gondii RH strain activity of herbal extracts used in traditional medicine. </p>

    <p class="NoSpacing">Int J Antimicrob Agents. 2008 Oct;32(4):360-2. Epub 2008 Jul 10.</p>

    <p class="NoSpacing">PMID: 18619816 [PubMed - in process]            </p>

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">6.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18840542?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Rivers EC, Mancera RL.</a></p>

    <p class="NoSpacing">New anti-tuberculosis drugs in clinical trials with novel mechanisms of action.</p>

    <p class="NoSpacing">Drug Discov Today. 2008 Oct 4. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 18840542 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">7.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18778048?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Cho Y, Ioerger TR, Sacchettini JC.</a></p>

    <p class="NoSpacing">Discovery of novel nitrobenzothiazole inhibitors for Mycobacterium tuberculosis ATP phosphoribosyl transferase (HisG) through virtual screening.</p>

    <p class="NoSpacing">J Med Chem. 2008 Oct 9;51(19):5984-92. Epub 2008 Sep 9.</p>

    <p class="NoSpacing">PMID: 18778048 [PubMed - in process]            </p>   

    <p class="ListParagraph">8.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18855373?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Cosconati S, Hong JA, Novellino E, Carroll KS, Goodsell DS, Olson AJ.</a></p>

    <p class="NoSpacing">Structure-Based Virtual Screening and Biological Evaluation of Mycobacterium tuberculosis Adenosine 5&#39;-Phosphosulfate Reductase Inhibitors.</p>

    <p class="NoSpacing">J Med Chem. 2008 Oct 15. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 18855373 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">9.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18783951?ordinalpos=93&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Mao J, Eoh H, He R, Wang Y, Wan B, Franzblau SG, Crick DC, Kozikowski AP.</a></p>

    <p class="NoSpacing">Structure-activity relationships of compounds targeting mycobacterium tuberculosis 1-deoxy-D-xylulose 5-phosphate synthase.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2008 Oct 1;18(19):5320-3. Epub 2008 Aug 14.</p>

    <p class="NoSpacing">PMID: 18783951 [PubMed - in process]            </p> 

    <p class="ListParagraph">10.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18693235?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Hurdle JG, Lee RB, Budha NR, Carson EI, Qi J, Scherman MS, Cho SH, McNeil MR, Lenaerts AJ, Franzblau SG, Meibohm B, Lee RE.</a></p>

    <p class="NoSpacing">A microbiological assessment of novel nitrofuranylamides as anti-tuberculosis agents.</p>

    <p class="NoSpacing">J Antimicrob Chemother. 2008 Nov;62(5):1037-45. Epub 2008 Aug 7.                 </p>

    <p class="NoSpacing">PMID: 18693235 [PubMed - in process]            </p> 

    <p class="ListParagraph">11.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18587134?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Kurabachew M, Lu SH, Krastel P, Schmitt EK, Suresh BL, Goh A, Knox JE, Ma NL, Jiricek J, Beer D, Cynamon M, Petersen F, Dartois V, Keller T, Dick T, Sambandamurthy VK.</a></p>

    <p class="NoSpacing">Lipiarmycin targets RNA polymerase and has good activity against multidrug-resistant strains of Mycobacterium tuberculosis.</p>

    <p class="NoSpacing">J Antimicrob Chemother. 2008 Oct;62(4):713-9. Epub 2008 Jun 27.</p>

    <p class="NoSpacing">PMID: 18587134 [PubMed - in process]            </p> 

    <p class="ListParagraph">12.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18626672?ordinalpos=153&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Kumar A, Siddiqi MI.</a></p>

    <p class="NoSpacing">CoMFA based de novo design of pyrrolidine carboxamides as inhibitors of enoyl acyl carrier protein reductase from Mycobacterium tuberculosis.</p>

    <p class="NoSpacing">J Mol Model. 2008 Oct;14(10):923-35. Epub 2008 Jul 15.</p>

    <p class="NoSpacing">PMID: 18626672 [PubMed - in process]            </p>

    <h2>Antimicrobials, Antifungals (General)</h2>

    <p class="ListParagraph">13.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18827382?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pietka-Ottlik M, Wójtowicz-M&#322;ochowska H, Ko&#322;odziejczyk K, Piasecki E, M&#322;ochowski J.</a>&nbsp;</p>

    <p class="NoSpacing">New Organoselenium Compounds Active against Pathogenic Bacteria, Fungi and Viruses.</p>

    <p class="NoSpacing">Chem Pharm Bull (Tokyo). 2008 Oct;56(10):1423-7.</p>

    <p class="NoSpacing">PMID: 18827382 [PubMed - in process]            </p>  

    <p class="ListParagraph">14.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18788049?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Lamani DS, Venugopala Reddy KR, Bhojya Naik HS, Savyasachi A, Naik HR.</a></p>

    <p class="NoSpacing">Synthesis and DNA binding studies of novel heterocyclic substituted quinoline schiff bases: a potent antimicrobial agent.</p>

    <p class="NoSpacing">Nucleosides Nucleotides Nucleic Acids. 2008 Oct;27(10):1197-210.</p>

    <p class="NoSpacing">PMID: 18788049 [PubMed - in process]            </p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
