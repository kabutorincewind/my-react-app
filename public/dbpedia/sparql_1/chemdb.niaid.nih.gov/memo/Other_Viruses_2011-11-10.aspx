

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-11-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mDdXeToco6m3ChEFyNShwwjwtEzHl3Dz15NctfjsSq97uX6Q1YgaBAkgvaIkIZZCb/kWN9Cexjcldj/zgnmH3iPOu/d9yY+M/tr7TJ0hpEQeUVDW1fE9RkiNxVE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="01D0868B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">October 28 - November 10, 2011</h1>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22059983">Thiazolides as Novel Antiviral Agents: II. Inhibition of Hepatitis C Virus Replication.</a> Stachulski, A.V., C. Pidathala, E.C. Row, R. Sharma, N.G. Berry, A.S. Lawrenson, S.L. Moores, M. Iqbal, J. Bentley, S.A. Allman, G. Edwards, A. Helm, J. Hellier, B.E. Korba, J.E. Semple, and J.F. Rossignol, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22059983].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22060554">Synthesis and Biological Activity of Bimorpholine and Its Carbanucleoside.</a> Ausmees, K., A. Selyutina, K. Kutt, K. Lippur, T. Pehk, M. Lopp, E. Zusinaite, A. Merits, and T. Kanger, Nucleosides, nucleotides &amp; nucleic acids, 2011. 30(11): p. 897-907; PMID[22060554].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22052996">Inhibitory Effects of Native/Recombinant Full-Length Camel Lactoferrin and Its N/C Lobes on Hepatitis C Virus Infection of Huh7.5 Cells.</a> Liao, Y., E. El-Fakkarany, B. Lonnerdal, and E.M. Redwan, Journal of medical microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[22052996].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22052332">Insulin-induced mTOR Activity Exhibits anti-Hepatitis C Virus Activity.</a> Muraoka, T., T. Ichikawa, N. Taura, H. Miyaaki, S. Takeshita, M. Akiyama, S. Miuma, E. Ozawa, H. Isomoto, F. Takeshima, and K. Nakao, Molecular Medicine Reports, 2011. <b>[Epub ahead of print]</b>; PMID[22052332].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22047799">Quinoline Tricyclic Derivatives. Design, Synthesis and Evaluation of the Antiviral Activity of Three New Classes of RNA-dependent RNA Polymerase Inhibitors.</a> Carta, A., I. Briguglio, S. Piras, P. Corona, G. Boatto, M. Nieddu, P. Giunchedi, M.E. Marongiu, G. Giliberti, F. Iuliano, S. Blois, C. Ibba, B. Busonera, and P. La Colla, Bioorganic &amp; Medicinal Chemistry, 2011. 19(23): p. 7070-7084; PMID[22047799].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22045669">The Antiviral Protein Viperin Inhibits Hepatitis C Virus Replication via Interaction with Nonstructural Protein 5A.</a> Helbig, K.J., N.S. Eyre, E. Yip, S. Narayana, K. Li, G. Fiches, E.M. McCartney, R.K. Jangra, S.M. Lemon, and M.R. Beard, Hepatology (Baltimore, Md.), 2011. 54(5): p. 1506-1517; PMID[22045669].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22039920">Phosphorodiamidates as a Promising New Phosphate Pro-drug Motif for Antiviral Drug Discovery: Application to anti-HCV Agents.</a> McGuigan, C., K. Madela, M. Aljarah, C. Bourdin, M. Arrica, E. Barrett, S. Jones, A. Kolykhalov, B. Bleiman, D. Bryant, B. Ganguly, E. Gorovits, G. Henson, D. Hunley, J. Hutchins, J. Muhammad, A. Obikhod, J. Patti, R. Walters, J. Wang, J. Vernachio, C. Ramamurty, S. Battina, and S.D. Chamberlain, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22039920].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21993152">Novel Platinum(II) and Palladium(II) Complexes of Thiosemicarbazones Derived from 5-Substitutedthiophene-2-carboxaldehydes and Their Antiviral and Cytotoxic Activities.</a> Karakucuk-Iyidogan, A., D. Tasdemir, E.E. Oruc-Emre, and J. Balzarini, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5616-5624; PMID[21993152].</p>

    <p class="NoSpacing"><b>[PubMed].</b> OV_1028-111011.</p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p class="NoSpacing">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002386">Preclinical Characteristics of a Novel Series of Potent HCV N53/4a Protease Inhibitors with Robust Once Daily Dosing Potential.</a> Buckman, B.O., J. Nicholas, V. Serebryany, R. Rajagopalan, K. Kossen, D. Ruhrmund, S. Misialek, L. Hooi, L. Pan, L. Huang, C. Schaefer, S. Pietranico-Cole, K. Klumpp, and S. Seiwert, Hepatology, 2011. 54: p. 550A-551A; ISI[000295578002386].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578003162">Ribavirin and IFN-alpha each Inhibits HCV IRES Mediated Translation and Synergistically Inhibits HCV Replication in Cells with a Functional JAK-STAT Pathway.</a> Chandra, S., S. Hazari, P.K. Chandra, F. Gunduz, L.L. Bao, L.A. Balart, and S. Dash, Hepatology, 2011. 54: p. 798A-799A; ISI[000295578003162].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578004563">Metformin and Rapamycin Interact with Insulin Signalling Pathway and Inhibit HCV Replication in Vitro.</a> Del Campo, J.A., M. Garcia-Valdecasas, M. Cuaresma, A. Rojas, and M. Romero-Gomez, Hepatology, 2011. 54: p. 1347A-1347A; ISI[000295578004563].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296009200010">Antiviral Activity of Leaves Extracts of Marrubium alysson L.</a> Edziri, H., M. Mastouri, M.A. Mahjoub, S. Ammar, Z. Mighri, L. Gutmann, and M. Aouni, Journal of Medicinal Plants Research, 2011. 5(3): p. 360-363; ISI[000296009200010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002084">Synergy of Entry Inhibitors and Direct Acting Antivirals or Interferon-alfa Identifies Novel Antiviral Combinations for Hepatitis C Virus Infection.</a> Fofana, I., F. Xiao, C. Thumann, J. Lupberger, P. Leyssen, J.H. Neyts, F. Habersetzer, M. Doffoel, M.B. Zeisel, and T.F. Baumert, Hepatology, 2011. 54: p. 401A-401A; ISI[000295578002084].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002370">A Second Generation HCV NS3/4a Protease Inhibitor is Active against Common Resistance Associated Variants (RAVs) and Exhibits Cross-genotype Activity.</a> Graham, D., A. Acosta, Z.Y. Guo, J.A. Howe, R.A. Ogert, M.D. Miller, D. Hazuda, D.B. Olsen, S. Ludmerer, J.M. Strizki, and R.J. Barnard, MK-5172, Hepatology, 2011. 54: p. 543A-543A; ISI[000295578002370].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578003151">Robust Antiviral Activity of Lambda-interferon (IL-29) against HCV in Interferon Alpha Resistant HUH-7 Cells with a Defective JAK-STAT Signaling.</a> Hazari, S., S. Nayak, P.K. Chandra, D. Kaushal, L.A. Balart, and S. Dash, Hepatology, 2011. 54: p. 793A-793A; ISI[000295578003151].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578003153">Protoporphyrin, a Novel Endogenous HCV NS3-4a Protease Inhibitor, Displays Anti-viral Activity.</a> Hou, W.H., Q. Tian, L.W. Schrum, and H.L. Bonkovsky, ZINC Hepatology, 2011. 54: p. 794A-794A; ISI[000295578003153].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578004546">Antiviral Effect of a Novel Interferon-inducible Protein, IFI-27, against Hepatitis C Virus Replication.</a> Itsui, Y., N. Sakamoto, G. Suda, M. Nakagawa, S. Kakinuma, S. Azuma, T. Yauchi, and M. Watanabe, Hepatology, 2011. 54: p. 1338A-1338A; ISI[000295578004546].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002377">Nitazoxanide Exhibits Synergy with HCV Protease Inhibitors and Prevents the Emergence of Telaprevir-resistant HCV Mutants in Combination Treatments in Cell Culture.</a> Korba, B., K.E. Farrar, and C. Yon, Hepatology, 2011. 54: p. 546A-546A; ISI[000295578002377].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295915200001">Optimization of Pharmacokinetics through Manipulation of Physicochemical Properties in a Series of HCV Inhibitors.</a> Lazerwith, S.E., G. Bahador, E. Canales, G.F. Cheng, L. Chong, M.O. Clarke, E. Doerffler, E.J. Eisenberg, J. Hayes, B. Lu, Q. Liu, M. Matles, M. Mertzman, M.L. Mitchell, P. Morganelli, B.P. Murray, M. Robinson, R.G. Strickley, M. Tessler, N. Tirunagari, J.H. Wang, Y.J. Wang, J.R. Zhang, X.B. Zheng, W.D. Zhong, and W.J. Watkins, ACS Medicinal Chemistry Letters, 2011. 2(10): p. 715-719; ISI[000295915200001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002350">Human safety, Pharmacokinetics and Antiviral Activity of TMC647055, a Novel HCV Non-nucleoside Polymerase Inhibitor.</a> Leempoels, J., H.W. Reesink, S. Bourgeois, L. Vijgen, M.C. Rouan, G. Ispas, K. Marien, P. Van Remoortere, G.C. Fanning, K. Simmen, and R. Verloes, Hepatology, 2011. 54: p. 533A-533A; ISI[000295578002350].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002379">25-Hydroxyvitamin D Inhibits Hepatitis C Virus Replication and Production of the Infectious Viruses.</a> Matsumura, T., T. Kato, M. Tasaka-Fujita, A. Murayama, T. Masaki, T. Wakita, and M. Imawari, Hepatology, 2011. 54: p. 547A-547A; ISI[000295578002379].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295237400018">Synthesis and Antiviral Evaluation of alpha-L-2 &#39;-Deoxythreofuranosyl Nucleosides.</a> Toti, K.S., M. Derudas, C. McGuigan, J. Balzarini, and S. Van Calenbergh, European journal of medicinal chemistry, 2011. 46(9): p. 3704-3713; ISI[000295237400018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002081">Reduction of HCV Replication by HMG-CoA-Reductase Inhibitors via KLF2-dependent Induction of Heme Oxygenase 1.</a> Wustenberg, A., A.D. Keller, E. Lehmann, H. Sirma, R. Bartenschlager, V. Lohmann, C. Loscher, M. Dandri, G. Tiegs, and G. Sass, Hepatology, 2011. 54: p. 399A-400A; ISI[000295578002081].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002382">E2112 is a Novel Oxidosqualene cyclase Inhibitor Exhibiting anti-HCV Activity in Genotype 1b subgenome Replicon Cells.</a> Yanagimachi, M., M. Ino, M. Uesugi, Y. Sato, F. Bernier, K. Miyazaki, S.I. Agoulnik, J. Simon, K. Aoshima, and Y. Oda, Hepatology, 2011. 54: p. 549A-549A; ISI[000295578002382].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578002357">Novel Hepatitis C Virus NS5a Inhibitors with Improved Potency against Genotype-1a Replicons and Replicons Carrying Mutations Associated with Viral Resistance to 1st Generation NS5a Inhibitors.</a> Yang, G.W., Y.S. Zhao, D. Patel, J.L. Fabrycki, C. Marlor, J. Rivera, K. Stauber, J. Wiles, V. Gadhachanda, A. Hashimoto, D.W. Chen, Q.P. Wang, G. Pais, X.Z. Wang, M. Deshpande, A. Phadke, and M.J. Huang, Hepatology, 2011. 54: p. 537A-537A; ISI[000295578002357].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295578004574">In Vitro Selection of Resistance to GS-9451, a Novel and Potent Inhibitor of HCV NS3 Protease.</a> Yang, H.L., T.C. Appleby, X.W. Chen, and W.E. Delaney, Hepatology, 2011. 54: p. 1352A-1352A; ISI[000295578004574].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295758400010">Synthesis and Antiviral Activity of (E)-3-(4-or 6-Methylbenzo d thiazol-2-yl-amino)-2-cyano-3-(methylthio)acrylate Derivatives</a>. Zhang, H., X.Y. Li, D.Y. Hu, S. Yang, H.T. Fan, X. Wei, and B.A. Song, Chinese Journal of Organic Chemistry, 2011. 31(9): p. 1419-1424; ISI[000295758400010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1028-111011.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
