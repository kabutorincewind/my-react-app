

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-10-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yeSo10FByr1u6uKjMfZM+sCT7bdNnF8ZWV3vB6SD2+2prz731p1bIfdFUJPMuWeAb6sByRgJGlrCVaruTZWL3PBl63e6wB7kC5daPrKfNjR3kCNj80ImZcunOos=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="61742718" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: September 27 - October 24, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23877703">P2&#39; Benzene Carboxylic acid Moiety Is Associated with Decrease in Cellular Uptake: Evaluation of Novel Nonpeptidic HIV-1 Protease Inhibitors Containing P2 bis-Tetrahydrofuran Moiety.</a> Yedidi, R.S., K. Maeda, W.S. Fyvie, M. Steffey, D.A. Davis, I. Palmer, M. Aoki, J.D. Kaufman, S.J. Stahl, H. Garimella, D. Das, P.T. Wingfield, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4920-4927. PMID[23877703].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24136666">A Novel Laccase with Inhibitory Activity Towards HIV-I Reverse Transcriptase and Antiproliferative Effects on Tumor Cells from the Fermentation Broth of Mushroom Pleurotus cornucopiae.</a> Wu, X., C. Huang, Q. Chen, H. Wang, and J. Zhang. Biomedical Chromatography, 2013. <b>[Epub ahead of print]</b>. PMID[24136666].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24102161">Clicking 3&#39;-Azidothymidine into Novel Potent Inhibitors of Human Immunodeficiency Virus.</a> Sirivolu, V.R., S.K. Verneka, T. Ilina, N.S. Myshakina, M.A. Parniak, and Z. Wang. Journal of Medicinal Chemistry, 2013.<b>[Epub ahead of print]</b>. PMID[24102161].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23444043">Phenolic Constituents from Parakmeria yunnanensis and Their anti-HIV-1 Activity.</a>Shang, S.Z., H. Chen, C.Q. Liang, Z.H. Gao, X. Du, R.R. Wang, Y.M. Shi, Y.T. Zheng, W.L. Xiao, and H.D. Sun. Archives of Pharmacal Research, 2013. 36(10): p. 1223-1230. PMID[23444043].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23985689">Discovery of a Novel 5-Carbonyl-1H-imidazole-4-carboxamide Class of Inhibitors of the HIV-1 Integrase-LEDGF/P75 Interaction.</a> Serrao, E., Z.L. Xu, B. Debnath, F. Christ, Z. Debyser, Y.Q. Long, and N. Neamati. Bioorganic &amp; Medicinal Chemistry, 2013. 21(19): p. 5963-5972. PMID[23985689].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24135563">Synergistic Activity Profile of Carbosilane Dendrimer G2-STE16 in Combination with Other Dendrimers and Antiretrovirals as Topical anti-HIV-1 Microbicide.</a>Sepulveda-Crespo, D., R. Lorente, M. Leal, R. Gomez, F.J. De La Mata, J.L. Jimenez, and M.A. Munoz-Fernandez. Nanomedicine, 2013. <b>[Epub ahead of print]</b>. PMID[24135563].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24134734">Dynamic Micelles of Mannoside Glycolipids Are More Efficient Than Polymers for Inhibiting HIV-1 Trans-infection.</a> Schaeffer, E., L. Dehuyser, D. Sigwalt, V. Flacher, S. Bernacchi, O. Chaloin, J.S. Remy, C.G. Mueller, R. Baati, and A. Wagner. Bioconjugate Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24134734].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24080647">GRL-04810 and GRL-05010; Difluoride-containing Nonpeptidic HIV-1 Protease Inhibitors (PIs) That Inhibit the Replication of Multi-pi-resistant HIV-1 in Vitro and Possess Favorable Lipophilicity That May Allow Blood-brain Barrier Penetration.</a>Salcedo Gomez, P.M., M. Amano, S. Yashchuk, A. Mizuno, D. Das, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24080647].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24065278">Template-constrained Cyclic Sulfopeptide HIV-1 Entry Inhibitors.</a>Rudick, J.G., M.M. Laakso, A.C. Schloss, and W.F. Degrado. Organic &amp; Biomolecular Chemistry, 2013. 11(41): p. 7096-7100. PMID[24065278].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24121104">Evaluation of Chitosan Nanoformulations as Potent anti-HIV Therapeutic Systems.</a>Ramana, L.N., S. Sharma, S. Sethuraman, U. Ranga, and U.M. Krishnan. Biochimica et Biophysica Acta, 2013. <b>[Epub ahead of print]</b>. PMID[24121104].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24134904">Design, Synthesis and Biological Evaluation of 3-Benzyloxy-linked Pyrimidinylphenylamine Derivatives as Potent HIV-1 NNRTIs.</a> Rai, D., W. Chen, Y. Tian, X. Chen, P. Zhan, E. De Clercq, C. Pannecouque, J. Balzarini, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24134904].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24128277">Lersivirine - a New Drug for HIV Infection Therapy.</a> Platten, M. and G. Fatkenheuer. Expert Opinion on Investigational Drugs, 2013. <b>[Epub ahead of print]</b>. PMID[24128277].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24100441">Approaches to the Synthesis of a Novel, anti-HIV Active Integrase Inhibitor.</a>Okello, M., M. Nishonov, P. Singh, S. Mishra, N. Mangu, B. Seo, M. Gund, and V. Nair. Organic &amp; Biomolecular Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24100441].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24095857">The Antiviral Activities of ISG15.</a> Morales, D.J. and D.J. Lenschow. Journal of Molecular Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24095857].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24141089">F(ab&#39;) Fragment of a gp41 NHR-trimer-induced IgM Monoclonal Antibody Neutralizes HIV-1 Infection and Blocks Viral Fusion by Targeting the Conserved gp41 Pocket.</a>Lu, L., M. Wei, Y. Chen, W. Xiong, F. Yu, Z. Qi, S. Jiang, and C. Pan. Microbes and Infection, 2013. <b>[Epub ahead of print]</b>. PMID[24141089].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23769857">Anti-HIV-1 Activity and Structure-Activity-Relationship Study of a Fucosylated Glycosaminoglycan from an Echinoderm by Targeting the Conserved CD4 Induced Epitope.</a>Lian, W., M. Wu, N. Huang, N. Gao, C. Xiao, Z. Li, Z. Zhang, Y. Zheng, W. Peng, and J. Zhao. Biochimica et Biophysica Acta, 2013. 1830(10): p. 4681-4691. PMID[23769857].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23817385">Discovery of Novel Small-molecule HIV-1 Replication Inhibitors That Stabilize Capsid Complexes.</a> Lamorte, L., S. Titolo, C.T. Lemke, N. Goudreau, J.F. Mercier, E. Wardrop, V.B. Shah, U.K. von Schwedler, C. Langelier, S.S. Banik, C. Aiken, W.I. Sundquist, and S.W. Mason. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4622-4631. PMID[23817385].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24100493">Effects of 4&#39;- and 2-Substitutions on the Bioactivity of 4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine.</a> Kirby, K.A., E. Michailidis, T.L. Fetterly, M.A. Steinbach, K. Singh, B. Marchand, M.D. Leslie, A.N. Hagedorn, E.N. Kodama, V.E. Marquez, S.H. Hughes, H. Mitsuya, M.A. Parniak, and S.G. Sarafianos. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24100493].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24121441">MX2 Is an Interferon-induced Inhibitor of HIV-1 Infection.</a> Kane, M., S.S. Yadav, J. Bitzegeio, S.B. Kutluay, T. Zang, S.J. Wilson, J.W. Schoggins, C.M. Rice, M. Yamashita, T. Hatziioannou, and P.D. Bieniasz. Nature, 2013.<b>[Epub ahead of print]</b>. PMID[24121441].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23962762">The Depolymerized Fucosylated Chondroitin Sulfate from Sea Cucumber Potently Inhibits HIV Replication Via Interfering with Virus Entry.</a> Huang, N., M.Y. Wu, C.B. Zheng, L. Zhu, J.H. Zhao, and Y.T. Zheng. Carbohydrate Research, 2013. 380: p. 64-69. PMID[23962762].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24086129">Highly Significant Antiviral Activity of HIV-1 LTR-specific Tre-recombinase in Humanized Mice.</a> Hauber, I., H. Hofmann-Sieber, J. Chemnitz, D. Dubrau, J. Chusainow, R. Stucka, P. Hartjen, A. Schambach, P. Ziegler, K. Hackmann, E. Schrock, U. Schumacher, C. Lindner, A. Grundhoff, C. Baum, M.G. Manz, F. Buchholz, and J. Hauber. Plos Pathogens, 2013. 9(9): p. e1003587. PMID[24086129].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24083612">HIV-1 Inhibiting Capacity of Novel Forms of Presentation of Gb Virus C Peptide Domains is Enhanced by Coordination to Gold Compounds.</a> Haro, I. Current Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24083612].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24145401">Tenascin-C Is an Innate Broad-spectrum, HIV-1-Neutralizing Protein in Breast Milk.</a>Fouda, G.G., F.H. Jaeger, J.D. Amos, C. Ho, E.L. Kunz, K. Anasti, L.W. Stamper, B.E. Liebl, K.H. Barbas, T. Ohashi, M.A. Moseley, H.X. Liao, H.P. Erickson, S.M. Alam, and S.R. Permar. Proceedings of the National Academy of Sciences of the United States of America, 2013. <b>[Epub ahead of print]</b>. PMID[24145401].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23994868">New Scaffolds of Natural Origin as Integrase-LEDGF/P75 Interaction Inhibitors: Virtual Screening and Activity Assays.</a> De Luca, L., F. Morreale, F. Christ, Z. Debyser, S. Ferro, and R. Gitto. European Journal of Medicinal Chemistry, 2013. 68: p. 405-411. PMID[23994868].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22803661">Fragment Hopping Approach Directed at Design of HIV IN-LEDGF/P75 Interaction Inhibitors.</a>De Luca, L., S. Ferro, F. Morreale, F. Christ, Z. Debyser, A. Chimirri, and R. Gitto. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. 28(5): p. 1002-1009. PMID[22803661].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24124919">6-(1-Benzyl-1H-pyrrol-2-yl)-2,4-dioxo-5-hexenoic Acids as Dual Inhibitors of Recombinant HIV-1 Integrase and Ribonuclease H, Synthesized by a Parallel Synthesis Approach.</a> Costi, R., M. Metifiot, F. Esposito, G. Cuzzucoli Crucitti, L. Pescatori, A. Messore, L. Scipione, S. Tortorella, L. Zinzula, C. Marchand, Y. Pommier, E. Tramontano, and R. Di Santo. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24124919].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23856780">Chimeric Cyanovirin-MPER Recombinantly Engineered Proteins Cause Cell-free Virolysis of HIV-1.</a> Contarino, M., A.R. Bastian, R.V. Kalyana Sundaram, K. McFadden, C. Duffy, V. Gangupomu, M. Baker, C. Abrams, and I. Chaiken. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4743-4750. PMID[23856780].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24116111">Peptide-derivatized SB105-A10 Dendrimer Inhibits the Infectivity of R5 and X4 HIV-1 Strains in Primary PBMCs and Cervicovaginal Histocultures.</a>Bon, I., D. Lembo, M. Rusnati, A. Clo, S. Morini, A. Miserocchi, A. Bugatti, S. Grigolon, G. Musumeci, S. Landolfo, M.C. Re, and D. Gibellini. PloS One, 2013. 8(10): p. e76482. PMID[24116111].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1011-102413.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323800900009">Diverse Reactivity in Microwave-promoted Catalyst-free Coupling of Substituted Anilines with Ethyl Trifluoropyruvate and Biological Evaluation.</a> Zhang, C., D.M. Zhuang, J. Li, S.Y. Chen, X.L. Du, J.Y. Wang, J.Y. Li, B. Jiang, and J.H. Yao. Organic &amp; Biomolecular Chemistry, 2013. 11(34): p. 5621-5633. ISI[000323800900009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322855400073">Design, Synthesis, and Biological Evaluation of New 2&#39;-Deoxy-2&#39;-fluoro-4&#39;-triazole Cytidine Nucleosides as Potent Antiviral Agents.</a>Wu, J., W.Q. Yu, L.X. Fu, W. He, Y. Wang, B.S. Chai, C.J. Song, and J.B. Chang. European Journal of Medicinal Chemistry, 2013. 63: p. 739-745. ISI[000322855400073].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322855400021">Synthesis and Pharmacokinetic Profile of Highly Deuterated Brecanavir Analogs.</a> Velthuisen, E.J., T.M. Baughman, B.A. Johns, D.P. Temelkoff, and J.G. Weatherhead. European Journal of Medicinal Chemistry, 2013. 63: p. 202-212. ISI[000322855400021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322855400085">Ester Prodrugs of Acyclic Nucleoside Thiophosphonates Compared to Phosphonates: Synthesis, Antiviral Activity and Decomposition Study.</a>Roux, L., S. Priet, N. Payrot, C. Weck, M. Fournier, F. Zoulim, J. Balzarini, B. Canard, and K. Alvarez. European Journal of Medicinal Chemistry, 2013. 63: p. 869-881. ISI[000322855400085].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323815200066">Formation of a Unique Cluster of G-quadruplex Structures in the HIV-1 nef Coding Region: Implications for Antiviral Activity.</a>Perrone, R., M. Nadai, J.A. Poe, I. Frasson, M. Palumbo, G. Palu, T.E. Smithgall, and S.N. Richter. PloS One, 2013. 8(8): p. e73121. ISI[000323815200066].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323858100015">Sis and Pharmacological Activity of 1,8,11,11-Tetramethyl-4-azatricyclo 5.2.2.0(2,6) undec-8-ene-3,5-dione Derivatives.</a>Pakosinska-Parys, M., J. Kossakowski, B. Miroslaw, A.E. Koziol, and J. Stefanska. Acta Poloniae Pharmaceutica, 2013. 70(3): p. 505-515. ISI[000323858100015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323851302886">Structural Analysis of HIV-1 Inhibitor Drug Candidate BMS-378806: The Role of Benzyl Derivatives.</a> Murphy, M., E. Triplett, and S. Tacj. Abstracts of Papers of the American Chemical Society, 2013. 245. ISI[000323851302886].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324146200030">Identification of a Novel Sulfonamide Non-Nucleoside Reverse Transcriptase Inhibitor by a Phenotypic HIV-1 Full Replication Assay.</a>Kim, T.H., Y. Ko, T. Christophe, J. Cechetto, J. Kim, K.A. Kim, A.S. Boese, J.M. Garcia, D. Fenistein, M.K. Ju, J. Kim, S.J. Han, H.J. Kwon, V. Brondani, and P. Sommer. PloS One, 2013. 8(7): p. e68767. ISI[000324146200030].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323665600009">Synthesis, Biological Screening and ADME Prediction of Benzylindole Derivatives as Novel anti-HIV-1, Anti-fungal and Anti-bacterial Agents.</a>Kashid, A.M., P.N. Dube, P.G. Alkutkar, K.G. Bothara, S.N. Mokale, and S.C. Dhawale. Medicinal Chemistry Research, 2013. 22(10): p. 4633-4640. ISI[000323665600009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323851303654">Structure Based Discovery of New Inhibitors of HIV Replication.</a>Jones, A. and G. Varani. Abstracts of Papers of the American Chemical Society, 2013. 245. ISI[000323851303654].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324040900006">Identification of Small Peptides Inhibiting the Integrase-LEDGF/P75 Interaction through Targeting the Cellular Co-factor.</a>Cavalluzzo, C., F. Christ, A. Voet, A. Sharma, B.K. Singh, K.Y.J. Zhang, E. Lescrinier, M. De Maeyer, Z. Debyser, and E. Van der Eycken. Journal of Peptide Science, 2013. 19(10): p. 651-658. ISI[000324040900006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323851302804">Synthesis of HIV-1 Capsid Protein Inhibitors.</a> Brown, J., E. Moffitt, W. Gambrill, T. Jarvis, R. Chandler, M.T. Huggins, and M.F. Summers. Abstracts of Papers of the American Chemical Society, 2013. 245. ISI[000323851302804].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000310754100011">The Nonnucleoside Reverse Transcription Inhibitor MIV-160 Delivered from an Intravaginal Ring, but Not from a Carrageenan Gel, Protects against Simian/Human Immunodeficiency Virus-RT Infection.</a>Aravantinou, M., R. Singer, N. Derby, G. Calenda, P. Mawson, C.J. Abraham, R. Menon, S. Seidor, D. Goldman, J. Kenney, G. Villegas, A. Gettie, J. Blanchard, J.D. Lifson, M. Piatak, J.A. Fernandez-Romero, T.M. Zydowsky, N. Teleshova, and M. Robbiani. AIDS Research and Human Retroviruses, 2012. 28(11): p. 1467-1475. ISI[000310754100011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1011-102413.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
