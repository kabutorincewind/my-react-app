

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-08-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NcbfMjJXD9/fTjpfYkxo67MH7OK4rXGSKh5BQNw8oh5RBv5NciUfWDSDpaHqxi6q0HJVML6hOqHYVM5aHCvQ/S4JoxgXG/8oUumwxdTv7LyoaRu4nmsfuA+m3oU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0A8BE68D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: July 19 - August 1, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23893479">A New Phenylethanoid Glycoside with Antioxidant and anti-HBV Activity from Tarphochlamys affinis<i>.</i></a> Zhou, X.L., Q.W. Wen, X. Lin, S.J. Zhang, Y.X. Li, Y.J. Guo, and R.B. Huang. Archives of Pharmacal Research, 2013. <b>[Epub ahead of print]</b>; PMID[23893479].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23883864">Inactivation of Hepatitis B Virus Replication in Cultured Cells and in Vivo with Engineered Transcription Activator-like Effector Nucleases<i>.</i></a> Bloom, K., A. Ely, C. Mussolino, T. Cathomen, and P. Arbuthnot. Molecular Therapy, 2013. <b>[Epub ahead of print]</b>; PMID[23883864].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23729669">Inducible Interleukin 32 (IL-32) Exerts Extensive Antiviral Function via Selective Stimulation of Interferon Lambda1 (IFN-Lambda1)<i>.</i></a> Li, Y., J. Xie, X. Xu, L. Liu, Y. Wan, Y. Liu, C. Zhu, and Y. Zhu. The Journal of Biological Chemistry, 2013. 288(29): p. 20927-20941; PMID[23729669].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23896639">Characterizations of HCV NS5A Replication Complex Inhibitors<i>.</i></a> O&#39;Boyle Ii, D.R., J.H. Sun, P.T. Nower, J.A. Lemm, R.A. Fridell, C. Wang, J.L. Romine, M. Belema, V.N. Nguyen, D.R. Laurent, M. Serrano-Wu, L.B. Snyder, N.A. Meanwell, D.R. Langley, and M. Gao. Virology, 2013. <b>[Epub ahead of print]</b>; PMID[23896639].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23877701">Inhibition of Hepatitis C Virus Infection by DNA Aptamer Against Envelope Protein<i>.</i></a> Yang, D., X. Meng, Q. Yu, L. Xu, Y. Long, B. Liu, X. fang, and H. Zhu. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23877701].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23868571">A Hepatitis C Virus NS4B Inhibitor Suppresses Viral Genome Replication by Disrupting NS4B&#39;S Dimerization/Multimerization as Well as Its Interaction with NS5A<i>.</i></a> Choi, M., S. Lee, T. Choi, and C. Lee. Virus Genes, 2013. <b>[Epub ahead of print]</b>; PMID[23868571].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23803586">HCV NS5A Replication Complex Inhibitors. Part 5: Discovery of Potent and Pan-genotypic Glycinamide Cap Derivatives<i>.</i></a> Belema, M., V.N. Nguyen, D.R. St Laurent, O.D. Lopez, Y. Qiu, A.C. Good, P.T. Nower, L. Valera, D.R. O&#39;Boyle, 2nd, J.H. Sun, M. Liu, R.A. Fridell, J.A. Lemm, M. Gao, J.O. Knipe, N.A. Meanwell, and L.B. Snyder. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(15): P. 4428-4435; PMID[23803586].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23791079">High Potency Improvements to Weak Aryl Uracil HCV Polymerase Inhibitor Leads<i>.</i></a> Donner, P., J.T. Randolph, P. Huang, R. WagneR, C. Maring, B.H. Lim, L. Colletti, Y. Liu, R. Mondal, J. Beyer, G. Koev, K. Marsh, D. Beno, K. Longenecker, T. Pilot-Matias, W. Kati, A. Molla, and D. Kempf. Bioorganic &amp; Medicinal Chemistry LetterS, 2013. 23(15): P. 4367-4369; PMID[23791079].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23773864">Discovery of Novel P2 Substituted 4-Biaryl proline Inhibitors of Hepatitis C Virus NS3 Serine Protease<i>.</i></a> Bailey, M.D., T. Halmos, and C.T. Lemke. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(15): P. 4436-4440; PMID[23773864].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23773863">Peptide Backbone Replacement of Hepatitis C Virus NS3 Serine Protease C-terminal Cleavage Product Analogs: Discovery of Potent Succinamide Inhibitors<i>.</i></a> Bailey, M.D., J. Bordeleau, M. Garneau, M. Leblanc, C.T. Lemke, J. O&#39;Meara, P.W. White, and M. Llinas-Brunet. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(15): P. 4447-4452; PMID[23773863].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23890410">In Vitro Antiviral Activity of Plant Extracts from Asteraceae Medicinal Plants<i>.</i></a> Jaime, M.F., F. Redko, L.V. Muschietti, R.H. Campos, V.S. Martino, and L.V. Cavallaro. Virology Journal, 2013. 10(1): p. 245; PMID[23890410].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23640033">Aldol-type Compounds from Water-soluble Indole-3,4-Diones: Synthesis, Kinetics, and Antiviral Properties<i>.</i></a> Scala, A., M. Cordaro, A. Mazzaglia, F. Risitano, A. Venuti, M.T. Sciortino, and G. Grassi. Molecular Diversity, 2013. 17(3): p. 479-488; PMID[23640033].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23752515">A Modified Zinc Acetate Gel, a Potential Nonantiretroviral Microbicide, Is Safe and Effective against Simian-human Immunodeficiency Virus and Herpes Simplex Virus 2 Infection in Vivo<i>.</i></a> Kenney, J., A. Rodriguez, L. Kizima, S. Seidor, R. Menon, N. Jean-Pierre, P. Pugach, K. Levendosky, N. Derby, A. Gettie, J. Blanchard, M. Piatak, Jr., J.D. Lifson, G. Paglini, T.M. Zydowsky, M. Robbiani, and J.A. Fernandez Romero. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 4001-4009; PMID[23752515].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0719-080113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320561500019">Antimicrobial and Antiviral Activity-guided Fractionation from Scutia buxifolia reissek Extracts<i>.</i></a> Boligon, A.A., T.F. Kubica, D.N. Mario, T.F. de Brum, M. Piana, R. Weiblen, L. Lovato, S.H. Alves, R.C.V. Santos, C.F.D. Alves, and M.L. Athayde. Acta Physiologiae Plantarum, 2013. 35(7): p. 2229-2239; ISI[000320561500019].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320961100001">Effectiveness of Human, Camel, Bovine and Sheep Lactoferrin on the Hepatitis C Virus Cellular Infectivity: Comparison Study<i>.</i></a> El-Fakharany, E.M., L. Sanchez, H.A. Al-Mehdar, and E.M. Redwan. Virology Journal, 2013. 10; ISI[000320961100001].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320906600002">Development of a Multiplex Phenotypic Cell-based High Throughput Screening Assay to Identify Novel Hepatitis C Virus Antivirals<i>.</i></a> Kim, H.Y., X. Li, C.T. Jones, C.M. Rice, J.M. Garcia, A. Genovesio, M.A.E. Hansen, and M.P. Windisch. Antiviral Research, 2013. 99(1): p. 6-11; ISI[000320906600002].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0719-080113.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320678000012">A Facile Synthesis of (5-Hydroxy-4-oxo-4h-pyran-2-yl)methyl carboxylates and Their Antiviral Activity Against Hepatitis C Virus<i>.</i></a> Shimo, T., Y. Taketsugu, T. Goto, M. Toyama, K. Yoshimura, and M. Baba. Heterocycles, 2013. 87(6): P. 1355-1364; ISI[000320678000012].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0719-080113.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
