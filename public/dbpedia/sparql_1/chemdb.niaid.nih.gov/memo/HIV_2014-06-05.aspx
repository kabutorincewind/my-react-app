

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-06-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="v77HzktUo/TRCu2aMQnhFoy6gEnENfAXHy9xKN+hmV5aAwi6yLts+WGZiAEi088rcn/MZut8steV4SM/dGZpAgQNwSuA9ll2dnj9BJlgEoBMElSS2ai0gp9Bt2M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E5DDE298" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: May 23 - June 5, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24886938">Synthesis and Biological Evaluation of Novel 2-Arylalkylthio-5-iodine-6-substituted-benzyl-pyrimidine-4(3H)-ones as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Zhang, L., X. Tang, Y. Cao, S. Wu, Y. Zhang, J. Zhao, Y. Guo, C. Tian, Z. Zhang, J. Liu, and X. Wang. Molecules, 2014. 19(6): p. 7104-7121. PMID[24886938].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24828017">Logeracemin A, an anti-HIV Daphniphyllum Alkaloid Dimer with a New Carbon Skeleton from Daphniphyllum longeracemosum.</a> Xu, J.B., H. Zhang, L.S. Gan, Y.S. Han, M.A. Wainberg, and J.M. Yue. Journal of the American Chemical Society, 2014. 136(21): p. 7631-7633. PMID[24828017].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=17602734">Structural Features and anti-HIV-1 Activity of Novel Polysaccharides from Red Algae Grateloupia longifolia and Grateloupia filicina.</a> Wang, S.C., S.W. Bligh, S.S. Shi, Z.T. Wang, Z.B. Hu, J. Crowder, C. Branford-White, and C. Vella. International Journal of Biological Macromolecules, 2007. 41(4): p. 369-375. PMID[17602734].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24500189">Artificial Peptides Conjugated with Cholesterol and Pocket-specific Small Molecules Potently Inhibit Infection by Laboratory-adapted and Primary HIV-1 Isolates and Enfuvirtide-resistant HIV-1 Strains.</a> Wang, C., W. Shi, L. Cai, L. Lu, F. Yu, Q. Wang, X. Jiang, X. Xu, K. Wang, L. Xu, S. Jiang, and K. Liu. The Journal of Antimicrobial Chemotherapy, 2014. 69(6): p. 1537-1545. PMID[24500189].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24614386">2-Aminothiazolones as anti-HIV Agents That Act as gp120-CD4 Inhibitors.</a> Tiberi, M., C. Tintori, E.R. Ceresola, R. Fazi, C. Zamperini, P. Calandro, L. Franchi, M. Selvaraj, L. Botta, M. Sampaolo, D. Saita, R. Ferrarese, M. Clementi, F. Canducci, and M. Botta. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3043-3052. PMID[24614386].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24874515">A New Class of Multimerization Selective Inhibitors of HIV-1 Integrase.</a> Sharma, A., A. Slaughter, N. Jena, L. Feng, J.J. Kessl, H.J. Fadel, N. Malani, F. Male, L. Wu, E. Poeschla, F.D. Bushman, J.R. Fuchs, and M. Kvaratskhelia. Plos Pathogens, 2014. 10(5): p. e1004171. PMID[24874515].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24742657">Cannabinoid Inhibits HIV-1 Tat-stimulated Adhesion of Human Monocyte-like Cells to Extracellular Matrix Proteins.</a> Raborn, E.S., M. Jamerson, F. Marciano-Cabral, and G.A. Cabral. Life Sciences, 2014. 104(1-2): p. 15-23. PMID[24742657].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24767846">Design and Synthesis of Highly Potent HIV-1 Protease Inhibitors with Novel Isosorbide-derived P2 Ligands.</a> Qiu, X., G.D. Zhao, L.Q. Tang, and Z.P. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(11): p. 2465-2468. PMID[24767846].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24742150">Synthesis and Evaluation of Phosphorus Containing, Specific CDK9/CycT1 Inhibitors.</a> Nemeth, G., Z. Greff, A. Sipos, Z. Varga, R. Szekely, M. Sebestyen, Z. Jaszay, S. Beni, Z. Nemes, J.L. Pirat, J.N. Volle, D. Virieux, A. Gyuris, K. Kelemenics, E. Ay, J. Minarovits, S. Szathmary, G. Keri, and L. Orfi. Journal of Medicinal Chemistry, 2014. 57(10): p. 3939-3965. PMID[24742150].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=12895696">The Potential Molecular Targets of Marine Sulfated Polymannuroguluronate Interfering with HIV-1 Entry. Interaction between SPMG and HIV-1 rgp120 and CD4 Molecule.</a> Meiyu, G., L. Fuchuan, X. Xianliang, L. Jing, Y. Zuowei, and G. Huashi. Antiviral research, 2003. 59(2): p. 127-135. PMID[12895696].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24613801">Anti-HIV-1 Tigliane Diterpenoids from Excoecaria acertiflia Didr.</a> Huang, S.Z., X. Zhang, Q.Y. Ma, H. Peng, Y.T. Zheng, J.M. Hu, H.F. Dai, J. Zhou, and Y.X. Zhao. Fitoterapia, 2014. 95: p. 34-41. PMID[24613801].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24728319">Synthesis of New Anionic Carbosilane Dendrimers via Thiol-ene Chemistry and Their Antiviral Behaviour.</a> Galan, M., J. Sanchez Rodriguez, J.L. Jimenez, M. Relloso, M. Maly, F.J. de la Mata, M.A. Munoz-Fernandez, and R. Gomez. Organic &amp; Biomolecular Chemistry, 2014. 12(20): p. 3222-3237. PMID[24728319].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24663024">Preclinical Profile of BI 224436, a Novel HIV-1 Non-catalytic-site Integrase Inhibitor.</a> Fenwick, C., M. Amad, M.D. Bailey, R. Bethell, M. Bos, P. Bonneau, M. Cordingley, R. Coulombe, J. Duan, P. Edwards, L.D. Fader, A.M. Faucher, M. Garneau, A. Jakalian, S. Kawai, L. Lamorte, S. LaPlante, L. Luo, S. Mason, M.A. Poupart, N. Rioux, P. Schroeder, B. Simoneau, S. Tremblay, Y. Tsantrizos, M. Witvrouw, and C. Yoakim. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3233-3244. PMID[24663024].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0523-060514.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335620800005">Specific Reactivation of Latent HIV-1 with Designer Zinc-finger Transcription Factors Targeting the HIV-1 5&#39;-LTR Promoter.</a> Wang, P., X. Qu, X. Wang, X. Zhu, H. Zeng, H. Chen, and H. Zhu. Gene Therapy, 2014. 21(5): p. 490-495. ISI[000335620800005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335229500060">Protease Inhibitors Are Effective at Inhibiting Cell-to-cell HIV-1 Transfer.</a> Titanji, B.K., M. Aasa-Chapman, C. Jolly, and D. Pillay. Antiviral Therapy, 2013. 18: p. A68-A68. ISI[000335229500060].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335164100002">Use of Vaginal Rings Charged with Antiviral Drugs for Preventing HIV Infection in Women.</a> Memmi, M., T. Bourlet, and B. Pozzetto. Future Virology, 2014. 9(3): p. 227-230. ISI[000335164100002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334420300006">Structure and Antioxidant Activity of Polyphenols Derived from Propolis.</a> Kurek-Gorecka, A., A. Rzepecka-Stojko, M. Gorecki, J. Stojko, M. Sosada, and G. Swierczek-Zieba. Molecules, 2014. 19(1): p. 78-101. ISI[000334420300006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335125000005">Wikstroelide M Potently Inhibits HIV Replication by Targeting Reverse Transcriptase and Integrase Nuclear Translocation.</a> Zhang, X., S.Z. Huang, W.G. Gu, L.M. Yang, H. Chen, C.B. Zheng, Y.X. Zhao, D.C. Wan, and Y.T. Zheng. Chinese Journal of Natural Medicines, 2014. 12(3): p. 186-193. ISI[000335125000005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335289400021">Synthesis of Curdlan Derivatives Regioselectively Modified at C-6: O-(N)-Acylated 6-amino-6-deoxycurdlan.</a> Zhang, R.R. and K.J. Edgar. Carbohydrate Polymers, 2014. 105: p. 161-168. ISI[000335289400021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335273800009">Poligapolide, a PI3K/Akt Inhibitor in Immunodeficiency Virus Type 1 TAT-transduced CHEM5 Cells, Isolated from the Rhizome of Polygala tenuifolia.</a> Yoo, S.Y., T.K.V. Le, J.J. Jeong, and D.H. Kim. Chemical &amp; Pharmaceutical Bulletin, 2014. 62(5): p. 467-471. ISI[000335273800009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334806900001">Interferon-induced HERC5 Is Evolving under Positive Selection and Inhibits HIV-1 Particle Production by a Novel Mechanism Targeting Rev/RRRE-dependent RNA Nuclear Export.</a> Woods, M.W., J.G. Tong, S.K. Tom, P.A. Szabo, P.C. Cavanagh, J.D. Dikeakos, S.M.M. Haeryfar, and S.D. Barr. Retrovirology, 2014. 11: 27. ISI[000334806900001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335011900007">Synthesis and Antiviral Evaluation of 4&#39;-(1,2,3-Triazol-1-yl)thymidines.</a> Vernekar, S.K.V., L. Qiu, J. Zacharias, R.J. Geraghty, and Z.Q. Wang. MedChemComm, 2014. 5(5): p. 603-608. ISI[000335011900007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334735600010">Binding of N-substituted Pyrrole Derivatives to HIV-1 gp41.</a> Song, K.Z., J. Bao, Y.M. Sun, and J.Z.H. Zhang. Journal of Theoretical &amp; Computational Chemistry, 2014. 13(2): 1450018. ISI[000334735600010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335011900010">Biological Evaluation and Molecular Modelling of Didanosine Derivative.</a> Ravetti, S., C.A. De Candia, M.S. Gualdesi, S. Pampuro, G. Turk, M.A. Quevedo, and M.C. Brinon. MedChemComm, 2014. 5(5): p. 622-631. ISI[000335011900010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334480700005">Synthesis and Biological Evaluation of 5&#39;-O-Dicarboxylic Fatty Acyl Monoester Derivatives of anti-HIV Nucleoside Reverse Transcriptase Inhibitors.</a> Pemmaraju, B., H.K. Agarwal, D. Oh, K.W. Buckheit, R.W. Buckheit, R. Tiwari, and K. Parang. Tetrahedron Letters, 2014. 55(12): p. 1983-1986. ISI[000334480700005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334364300042">MIV-150-containing Intravaginal Rings Protect Macaque Vaginal Explants against SHIV-RT Infection.</a> Ouattara, L.A., P. Barnable, P. Mawson, S. Seidor, T.M. Zydowsky, L. Kizima, A. Rodriguez, J.A. Fernandez-Romero, M.L. Cooney, K.D. Roberts, A. Gettie, J. Blanchard, M. Robbiani, and N. Teleshova. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2841-2848. ISI[000334364300042].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334688400023">Pharmacokinetics of a CCR5 Inhibitor in Rhesus Macaques Following Vaginal, Rectal and Oral Application.</a> Malcolm, R.K., D. Lowry, P. Boyd, L. Geer, R.S. Veazey, L. Goldman, P.J. Klasse, R.J. Shattock, and J.P. Moore. Journal of Antimicrobial Chemotherapy, 2014. 69(5): p. 1325-1329. ISI[000334688400023].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334683100005">An Efficient Synthesis of New Khellactone-type Compounds Using Potassium Hydroxide as Catalyst via One-pot, Three-component Reaction.</a> Karami, B., K. Eskandari, and S. Khodabakhshi. Journal of the Iranian Chemical Society, 2014. 11(3): p. 631-637. ISI[000334683100005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334857000002">Discovery of Novel Inhibitors of HIV-1 Reverse Transcriptase through Virtual Screening of Experimental and Theoretical Ensembles.</a> Ivetac, A., S.E. Swift, P.L. Boyer, A. Diaz, J. Naughton, J.A.T. Young, S.H. Hughes, and J.A. McCammon. Chemical Biology &amp; Drug Design, 2014. 83(5): p. 521-531. ISI[000334857000002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334972900003">Structural Optimization of Cyclic Sulfonamide Based Novel HIV-1 Protease Inhibitors to Picomolar Affinities Guided by X-ray Crystallographic Analysis.</a> Ganguly, A.K., S.S. Alluri, C.H. Wang, A. Antropow, A. White, D. Caroccia, D. Biswas, E. Kang, L.K. Zhang, S.S. Carroll, C. Burlein, J. Fay, P. Orth, and C. Strickland. Tetrahedron, 2014. 70(18): p. 2894-2904. ISI[000334972900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334162700011">Modified Silicone Elastomer Vaginal Gels for Sustained Release of Antiretroviral HIV Microbicides.</a> Forbes, C.J., C.F. McCoy, D.J. Murphy, A.D. Woolfson, J.P. Moore, A. Evans, R.J. Shattock, and R.K. Malcolm. Journal of Pharmaceutical Sciences, 2014. 103(5): p. 1422-1432. ISI[000334162700011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334658200025">Structure-based Design, Synthesis and Validation of CD4-mimetic Small Molecule Inhibitors of HIV-1 Entry: Conversion of a Viral Entry Agonist to an Antagonist.</a> Courter, J.R., N. Madani, J. Sodroski, A. Schon, E. Freire, P.D. Kwong, W.A. Hendrickson, I.M. Chaiken, J.M. LaLonde, and A.B. Smith. Accounts of Chemical Research, 2014. 47(4): p. 1228-1237. ISI[000334658200025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334476700021">Neutralising Properties of Peptides Derived from CXCR4 Extracellular Loops Towards CXCL12 Binding and HIV-1 Infection.</a> Chevigne, A., V. Fievez, M. Szpakowska, A. Fischer, M. Counson, J.M. Plesseria, J.C. Schmit, and S. Deroo. Biochimica et Biophysica Acta-Molecular Cell Research, 2014. 1843(5): p. 1031-1041. ISI[000334476700021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334184400023">Synthesis, Molecular Docking and Antiviral Screening of Novel N&#39;-substitutedbenzylidene-2-(4-methyl-5,5-dioxido-3-phenylbenzo-E-pyrazo-lo-4,3-C 1,2 thiazin-1(4h)-yl)acetohydrazides.</a> Aslam, S., M. Ahmad, M.M. Athar, U.A. Ashfaq, J.M. Gardiner, C. Montero, M. Detorio, M. Parvez, and R.F. Schinazi. Medicinal Chemistry Research, 2014. 23(6): p. 2930-2946. ISI[000334184400023].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">35. <a href="http://link.springer.com/article/10.1007/BF02186319#page-1">Annual Variation in Composition Andin Vitro anti-HIV-1 Activity of the Sulfated Glucuronogalactan Fromschizymenia dubyi (Rhodophyta, Gigartinales).</a> Bourgougnon, N., M. Lahaye, B. Quemener, J. Chermann, M. Rimbert, M. Cormaci, G. Furnari, and J. Kornprobst. Journal of Applied Phycology, 1996. 8(2): p. 155-161. ISI[A1996VP07400009]</p>

    <p class="plaintext"><b>[WOS]</b> HIV_0523-060514.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">36. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140417&amp;CC=US&amp;NR=2014107225A1&amp;KC=A1">Therapeutic Application of Cembranoids against HIV Replication, HIV-associated Neurocognitive Disorders and HIV-induced Inflammation.</a> Ferchmin, P.A., V.A. Eterovic, J.W. Rodriguez, E.O. Rios-Olivares, and A.H.B. Martins. Patent. 2014. 2012-13649828 20140107225: 18pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">37. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140515&amp;CC=WO&amp;NR=2014072419A1&amp;KC=A1">Novel anti-HIV Compounds.</a> Franck, P., J. Heeres, B. Maes, S. Sergueev, and P.J. Lewi. Patent. 2014. 2013-EP73288 2014072419: 48pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0523-060514.</p>

    <br />

    <p class="plaintext">38. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140515&amp;CC=WO&amp;NR=2014074628A1&amp;KC=A1">Compounds for Treating HIV and Methods for Using the Compounds Capable of Inhibiting Nef Function.</a> Smithgall, T.E., L.A. Emert-Sedlak, B.W. Day, J. Zhao, and P.C. Iyer. Patent. 2014. 2013-US68791 2014074628: 124pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0523-060514.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
