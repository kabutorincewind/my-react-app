

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-06-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Gtz+TysP1wXT8XzTpamzJQg26SmU8uQFX+7hvopZIFL9oruWTV9wgE61jYSS3N7sw4O/QVmyUSd6hmThjep+GvcUvWFrmQW3n/XkiIEBzGcV+9yXhrL6TXWSZW4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6A34D388" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">May 27, 2009 - June 9, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19501055?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Drug discovery targeting epigenetic codes: The great potential of UHRF1, which links DNA methylation and histone modifications, as a drug target in cancers and toxoplasmosis.</a></p>

    <p class="NoSpacing">Unoki M, Brunet J, Mousli M.</p>

    <p class="NoSpacing">Biochem Pharmacol. 2009 Jun 3. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19501055 [PubMed - as supplied by publisher]</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19409796?ordinalpos=66&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, synthesis and evaluations of acridone derivatives using Candida albicans--search for MDR modulators led to the identification of an anti-candidiasis agent.</a></p>

    <p class="NoSpacing">Singh P, Kaur J, Yadav B, Komath SS.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Jun 1;17(11):3973-9. Epub 2009 Apr 18.</p>

    <p class="NoSpacing">PMID: 19409796 [PubMed - in process]</p>

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19463737?ordinalpos=50&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vitro activities of three synthetic peptides derived from epinecidin-1 and an anti-lipopolysaccharide factor against Propionibacterium acnes, Candida albicans, and Trichomonas vaginalis.</a></p>

    <p class="NoSpacing">Pan CY, Chen JY, Lin TL, Lin CH.</p>

    <p class="NoSpacing">Peptides. 2009 Jun;30(6):1058-68. Epub 2009 Feb 20.</p>

    <p class="NoSpacing">PMID: 19463737 [PubMed - in process]</p>

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19487443?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The antifungal activity of tamoxifen: in vitro, in vivo and mechanistic characterization.</a></p>

    <p class="NoSpacing">Dolan K, Montgomery S, Bucchiet B, Didone L, Wellington M, Krysan DJ.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Jun 1. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19487443 [PubMed - as supplied by publisher]</p> 

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19505802?ordinalpos=32&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vitro and ex vivo activity of peptide deformylase inhibitors against Mycobacterium tuberculosis H37Rv.</a></p>

    <p class="NoSpacing">Sharma A, Sharma S, Khuller GK, Kanwar AJ.</p>

    <p class="NoSpacing">Int J Antimicrob Agents. 2009 Jun 6. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19505802 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19504188?ordinalpos=42&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Structure-Activity Relationships of Tariquidar Analogs as Multidrug Resistance Modulators.</a></p>

    <p class="NoSpacing">Pajeva IK, Wiese M.</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">7. Olugbuyiro, JAO, et al.</p>
    <p class="NoSpacing">AntiMtb activity of triterpenoid-rich fractions from Spondias mombin L.</p>
    <p class="NoSpacing">AFRICAN JOURNAL OF BIOTECHNOLOGY  2009 (May) 8: 1807 - 1809</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
