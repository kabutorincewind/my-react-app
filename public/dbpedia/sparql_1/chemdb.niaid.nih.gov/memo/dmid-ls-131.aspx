

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-131.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="U1+j//Wyy0mm0ijC40OqKWawZ/FtxEFcpf32Og9SP39yfZNrdPTNZXm0w3ta2CCrdr8xLIckgv1AdToLBHiZAQTGYiYSIj4/wIA2fgEhOW7KbLUUFkhHuOf4VEU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="995F24F7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-131-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59321   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Enantioselective synthesis and antiviral activity of purine and pyrimidine cyclopentenyl C-nucleosides</p>

    <p>          Rao, JR, Schinazi, RF, and Chu, CK</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17085053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17085053&amp;dopt=abstract</a> </p><br />

    <p>2.     59322   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Antiviral 2,5-disubstituted imidazo[4,5-c]pyridines: From anti-pestivirus to anti-hepatitis C virus activity</p>

    <p>          Puerstinger, G, Paeshuyse, J, De, Clercq E, and Neyts, J</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17084081&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17084081&amp;dopt=abstract</a> </p><br />

    <p>3.     59323   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Fluoro-ketopyranosyl nucleosides: Synthesis and biological evaluation of 3-fluoro-2-keto-beta-d-glucopyranosyl derivatives of N(4)-benzoyl cytosine</p>

    <p>          Manta, S, Agelis, G, Botic, T, Cencic, A, and Komiotis, D</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079149&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079149&amp;dopt=abstract</a> </p><br />

    <p>4.     59324   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Antiviral drug combinations using a glycosylation modulator and a membrane fusion inhibitor</p>

    <p>          Nash, Robert James, Slingsby, Jason H, and Carroll, Miles William</p>

    <p>          PATENT:  WO <b>2006077427</b>  ISSUE DATE:  20060727</p>

    <p>          APPLICATION: 2006  PP: 85 pp.</p>

    <p>          ASSIGNEE:  (MNL Pharma Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     59325   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          New therapeutic options for hepatitis C</p>

    <p>          Waters, L and Nelson, M</p>

    <p>          Curr Opin Infect Dis <b>2006</b>.  19(6): 615-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17075339&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17075339&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59326   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Thienyl compounds for treating virus-related conditions</p>

    <p>          Olivo, Paul D, Buscher, Benjamin A, Dyall, Julie, Jockel-Balsarotti, Jennifer I, O&#39;Guin, Andrew K, Roth, Robert M, Franklin, Gary W, and Starkey, Gale W</p>

    <p>          PATENT:  WO <b>2006093518</b>  ISSUE DATE:  20060908</p>

    <p>          APPLICATION: 2005  PP: 343pp.</p>

    <p>          ASSIGNEE:  (Apath, LLC USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     59327   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Activities of oseltamivir and ribavirin used alone and in combination against infections in mice with recent isolates of influenza A (H1N1) and B viruses</p>

    <p>          Smee, DF, Wong, MH, Bailey, KW, and Sidwell, RW</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(4): 185-192</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17066897&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17066897&amp;dopt=abstract</a> </p><br />

    <p>8.     59328   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Inhibitors of enveloped virus infectivity</p>

    <p>          Cunningham, James and Chandran, Kartik</p>

    <p>          PATENT:  WO <b>2006091610</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 65pp.</p>

    <p>          ASSIGNEE:  (The Brigham and Women&#39;s Hospital, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     59329   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Peptide Inhibitors of West Nile NS3 Protease: SAR Study of Tetrapeptide Aldehyde Inhibitors</p>

    <p>          Knox, JE, Ma, NL, Yin, Z, Patel, SJ, Wang, WL, Chan, WL, Ranga, Rao KR, Wang, G, Ngew, X, Patel, V, Beer, D, Lim, SP, Vasudevan, SG, and Keller, TH</p>

    <p>          J Med Chem <b>2006</b>.  49(22): 6585-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064076&amp;dopt=abstract</a> </p><br />

    <p>10.   59330   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Structure of vaccinia virus thymidine kinase in complex with dTTP: insights for drug design</p>

    <p>          El Omari, K, Solaroli, N, Karlsson, A, Balzarini, J, and Stammers, DK</p>

    <p>          BMC Struct Biol <b>2006</b>.  6(1): 22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17062140&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17062140&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59331   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Antiviral activity of ribavirin against Hantaan virus correlates with production of ribavirin-5&#39;-triphosphate, not with inhibition of inosine monophosphate dehydrogenase</p>

    <p>          Sun, Y, Chung, DH, Chu, YK, Jonsson, CB, and Parker, WB</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17060520&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17060520&amp;dopt=abstract</a> </p><br />

    <p>12.   59332   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity against tobacco mosaic virus and 3D-QSAR of alpha-substituted-1,2,3-thiadiazoleacetamides</p>

    <p>          Zhao, WG, Wang, JG, Li, ZM, and Yang, Z</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17055269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17055269&amp;dopt=abstract</a> </p><br />

    <p>13.   59333   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of tricyclic nucleoside prodrugs for treating viral infections</p>

    <p>          Keicher, Jesse Daniel and Roberts, Christopher Don</p>

    <p>          PATENT:  US <b>2006194749</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006-37490  PP: 63pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   59334   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of heteroarylindolecarboxylates as antivirals</p>

    <p>          Botyanszki, Janos, Roberts, Christopher Don, Schmitz, Franz Ulrich, Gralapp, Joshua Michael, Griffith, Ronald Conrad, Shi, Dong-Fang, Leivers, Martin Robert, and Brewster, Rachel Elizabeth</p>

    <p>          PATENT:  WO <b>2006076529</b>  ISSUE DATE:  20060720</p>

    <p>          APPLICATION: 2006  PP: 203 pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   59335   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Potent Inhibition of Human Hepatitis B Virus Replication by a Host Factor Vps4</p>

    <p>          Chua, P., Lin, M., and Shih, C.</p>

    <p>          Virology <b>2006</b>.  354(1): 1-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241178500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241178500001</a> </p><br />

    <p>16.   59336   DMID-LS-131; PUBMED-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Comparison of the inhibitory effects of interferon alfacon-1 and ribavirin on yellow fever virus infection in a hamster model</p>

    <p>          Julander, JG, Morrey, JD, Blatt, LM, Shafer, K, and Sidwell, RW</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049380&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049380&amp;dopt=abstract</a> </p><br />

    <p>17.   59337   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Diketo acids on nucleobase scaffolds as inhibitors of Flaviviridae</p>

    <p>          Nair, Vasu, Chi, Guochen, and Uchil, Vinod R</p>

    <p>          PATENT:  US <b>2006223834</b>  ISSUE DATE:  20061005</p>

    <p>          APPLICATION: 2005-32589  PP: 36pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   59338   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Substituted 5-Benzyl-2-Phenyl-5h-Imidazo[4,5-C]Pyridines: a New Class of Pestivirus Inhibitors</p>

    <p>          Puerstinger, G. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(20): 5345-5349</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241344400019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241344400019</a> </p><br />

    <p>19.   59339   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Medicine composition containing thymopentin and interferon as antiviral and antitumor agents</p>

    <p>          Wang, Shujuan, Jia, Zhidan, Piao, Zhisong, and Wang, Zhenguo</p>

    <p>          PATENT:  CN <b>1833721</b>  ISSUE DATE: 20060920</p>

    <p>          APPLICATION: 1007-6271  PP: 8pp.</p>

    <p>          ASSIGNEE:  (Beijing Guodan Medicine Technological Development Co., Ltd. Peop. Rep. China and Jilin Yixin Pharmaceutical Co., Ltd.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   59340   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Structure-Guided Design of a Novel Class of Benzyl-Sulfonate Inhibitors for Influenza Virus Neuraminidase</p>

    <p>          Platis, D. <i>et al.</i></p>

    <p>          Biochemical Journal <b>2006</b>.  399: 215-223</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241348600005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241348600005</a> </p><br />

    <p>21.   59341   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          A Simple and Efficient Synthesis of 2-Deoxy-L-Ribose From 2-Deoxy-D-Ribose</p>

    <p>          Ji, Q. <i>et al.</i></p>

    <p>          Synlett <b>2006</b>.(15): 2498-2500</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241175500034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241175500034</a> </p><br />

    <p>22.   59342   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Drug compositions containing tetrahydro-2H-thiopyran-4-carboxamide derivatives, and anti-herpesvirus agent containing the same</p>

    <p>          Kontani, Toru, Miyata, Junji, Hamaguchi, Wataru, Kamikawa, Akio, Kono, Tomoaki, Suzuki, Hiroshi, and Sudo, Kenji</p>

    <p>          PATENT:  JP <b>2006241144</b>  ISSUE DATE:  20060914</p>

    <p>          APPLICATION: 2006-24024  PP: 21pp.</p>

    <p>          ASSIGNEE:  (Astellas Pharma Inc., Japan and Soyaku Gijutsu Kenkyusho K. K.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.   59343   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Curative-prophylactic pomade eliciting antiviral activity</p>

    <p>          Prokop&#39;eva, LA, Vinogradov, AK, Kungurtseva, NV, Lesnykh, OA, Mitrofanova, EG, and Gavinskii, YuV</p>

    <p>          PATENT:  RU <b>2282473</b>  ISSUE DATE: 20060827</p>

    <p>          APPLICATION: 2002-37587  PP: 9pp.</p>

    <p>          ASSIGNEE:  (ZAO \&quot;Evalar\&quot;, Russia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   59344   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Synthesis of Trifluoromethyl Ketone-Containing Glutamic Acid and Glutamine Peptides as Severe Acute Respiratory Syndrome Coronavirus 3cl Protease Inhibitors</p>

    <p>          Sydnes, M. <i>et al.</i></p>

    <p>          Journal of Peptide Science <b>2006</b>.  12: 109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239905600102">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239905600102</a> </p><br />

    <p>25.   59345   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          An Efficient Method for the Synthesis of Peptide Aldehyde Libraries Employed in the Discovery of Sars Corona Virus Main Protease Inhibitors</p>

    <p>          Schmidt, M. <i>et al.</i></p>

    <p>          Journal of Peptide Science <b>2006</b>.  12: 168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239905600337">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239905600337</a> </p><br />

    <p>26.   59346   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Picornaviral 3cp Inhibitors. Multiple-Peptide Synthesis in Parallel and Biological Evaluation</p>

    <p>          Minchev, I. <i>et al.</i></p>

    <p>          Journal of Peptide Science <b>2006</b>.  12: 217</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239905600531">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239905600531</a> </p><br />

    <p>27.   59347   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Anti-alpha-herpesvirus drugs</p>

    <p>          Coen, Donald M </p>

    <p>          Alpha Herpesviruses <b>2006</b>.: 361-381</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   59348   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Pharmaceutical preparation of N -[5-(aminosulfonyl)-4-methyl-1,3-thiazol-2-yl]-n-methyl-2-[4-(2-pyridinyl)phenyl] acetamide for use as antiviral agent</p>

    <p>          Laich, Tobias and Liebelt, Katrin</p>

    <p>          PATENT:  WO <b>2006103011</b>  ISSUE DATE:  20061005</p>

    <p>          APPLICATION: 2006  PP: 25pp.</p>

    <p>          ASSIGNEE:  (Aicuris G.m.b.H. &amp; Co. K.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   59349   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Method of screening for inhibitors of IRES-mediated translation and identification of antiviral peptides targeted to hepatitis C virus</p>

    <p>          Fear, Mark</p>

    <p>          PATENT:  WO <b>2006102720</b>  ISSUE DATE:  20061005</p>

    <p>          APPLICATION: 2006  PP: 197pp.</p>

    <p>          ASSIGNEE:  (Telethon Institute for Child Health Research, Australia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   59350   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Effects of Cidofovir Treatment on Cytokine Induction in Murine Models of Cowpox and Vaccinia Virus Infection</p>

    <p>          Knorr, C. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  72(2): 125-133</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241092900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241092900006</a> </p><br />

    <p>31.   59351   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Influenza virus NS1 protein protects against lymphohematopoietic pathogenesis in an in vivo mouse model</p>

    <p>          Hyland Lisa, Webby Richard, Sandbulte Matthew R, Clarke Ben, and Hou Sam</p>

    <p>          Virology <b>2006</b>.  349(1): 156-163</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   59352   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Antiviral-Resistant Hbv: Can We Prevent This Monster From Growing?</p>

    <p>          Lok, A.</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36: S22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300066">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300066</a> </p><br />

    <p>33.   59353   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          New Hcv Antiviral Therapies</p>

    <p>          Zeuzem, S.</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36: S49-S50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300143">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300143</a> </p><br />

    <p>34.   59354   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          A Cell-Based Assay for Evaluating Potential Antiviral Agents Against Hcv</p>

    <p>          Aviel, S. <i>et al.</i></p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36: S116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300350">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300350</a> </p><br />

    <p>35.   59355   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p><b>          Dithiocarbamate antiviral compounds for production of an agent for the treatment or prevention of a viral infection in the respiratory tract, and for a disinfecting agent</b> </p>

    <p>          Gaudernak, Elisabeth, Grassauer, Andreas, Kuchler, Ernst, Muster, Thomas, and Seipelt, Joachim </p>

    <p>          PATENT:  WO <b>2003007935</b>  ISSUE DATE:  20030130</p>

    <p>          APPLICATION: 2002  PP: 50 pp.</p>

    <p>          ASSIGNEE:  (Austria)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   59356   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Imidazo[1,2-b]pyridazines, Novel Nucleus with Potent and Broad Spectrum Activity against Human Picornaviruses: Design, Synthesis, and Biological Evaluation</p>

    <p>          Hamdouchi, Chafiq, Sanchez-Martinez, Concha, Gruber, Joseph, del Prado, Miriam, Lopez, Javier, Rubio, Almudena, and Heinz, Beverly A</p>

    <p>          Journal of Medicinal Chemistry <b>2003</b>.  46(20): 4333-4341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>37.   59357   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of 1,1-dioxodihydrobenzothiadiazines as antiviral agents</p>

    <p>          Darcy, Michael G, Dhanak, Dashyant, Duffy, Kevin J, Fitch, Duke M, Sarisky, Robert T, Shaw, Antony N, Tedesco, Rosanna, and Zimmerman, Michael N</p>

    <p>          PATENT:  WO <b>2003059356</b>  ISSUE DATE:  20030724</p>

    <p>          APPLICATION: 2002  PP: 103 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   59358   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of oxadiazolylphenoxyalkylisoxazoles as anti-picornaviral agents</p>

    <p>          Rhodes, Gerald and Nitz, Theodore J</p>

    <p>          PATENT:  WO <b>2003020271</b>  ISSUE DATE:  20030313</p>

    <p>          APPLICATION: 2002  PP: 37 pp.</p>

    <p>          ASSIGNEE:  (Viropharma Incorporated, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   59359   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of dioxolane and oxathiolane nucleosides as antivirals and inhibitors of RNA-dependent RNA viral polymerase</p>

    <p>          Carroll, Steven S, MacCoss, Malcolm, Kuo, Lawrence C, Olsen, David B, Bhat, Balkrishen, Eldrup, Anne Bettina, Prhavc, Marija, Malik, Leila, and Bera, Sanjib</p>

    <p>          PATENT:  WO <b>2003020222</b>  ISSUE DATE:  20030313</p>

    <p>          APPLICATION: 2002  PP: 82 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA and Isis Pharmaceuticals, Inc.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   59360   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of 6-[2-(1-pyridazinyl-4-piperidinyl)ethoxy]benzoxazoles and analogs as antiviral agents</p>

    <p>          Keith, Watson, Krippner, Guy, Stanislawski, Pauline, and McConnell, Darryl</p>

    <p>          PATENT:  WO <b>2002050045</b>  ISSUE DATE:  20020627</p>

    <p>          APPLICATION: 2001  PP: 76 pp.</p>

    <p>          ASSIGNEE:  (Biota Scientific Management Pty Ltd., Australia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   59361   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Protease inhibitors as potential antiviral agents for the treatment of picornaviral infections</p>

    <p>          Wang, QMay</p>

    <p>          Antiviral Agents <b>2001</b>.: 229-253</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   59362   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          In vitro antiviral activity of the anthraquinone chrysophanic acid against poliovirus</p>

    <p>          Semple, Susan J, Pyke, Simon M, Reynolds, Geoffrey D, and Flower, Robert LP</p>

    <p>          Antiviral Research <b>2001</b>.  49(3): 169-178</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   59363   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          1,2-Disubstituted 1,4-dihydro-4-oxoquinoline compounds and their antiviral activity</p>

    <p>          Tamura, Takashi, Kuriyama, Haruo, Agoh, Masanobu, Agoh, Yumi, Soga, Manabu, and Mori, Teruyo</p>

    <p>          PATENT:  EP <b>1081138</b>  ISSUE DATE: 20010307</p>

    <p>          APPLICATION: 2000-53137  PP: 64 pp.</p>

    <p>          ASSIGNEE:  (Maruishi Pharmaceutical Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>44.   59364   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of (heterocyclyl)alkoxy benzaldehyde oximes as antiviral agents</p>

    <p>          Wu, Wen-Yang, Watson, Keith, Mcconnell, Darryl, Jin, Betty, and Krippner, Guy</p>

    <p>          PATENT:  WO <b>2000078746</b>  ISSUE DATE:  20001228</p>

    <p>          APPLICATION: 2000  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (Biota Scientific Management Pty. Ltd., Australia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>45.   59365   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Anti-rhinovirus activity of 3-methylthio-5-aryl-4-isothiazolecarbonitrile derivatives</p>

    <p>          Garozzo, A, Cutri, CCC, Castro, A, Tempera, G, Guerrera, F, Sarva, MC, and Geremia, E</p>

    <p>          Antiviral Research <b>2000</b>.  45(3): 199-210</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   59366   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Anti-viral imidazo[1,2-a]pyridine derivatives and their pharmaceutical formulations and use</p>

    <p>          Ezquerra-Carrera, Jesus, Gruber, Joseph Michael, Hamdouchi, Chafiq Hamdouchi, Holmes, Richard Elmer, and Spitzer, Wayne Alfred</p>

    <p>          PATENT:  WO <b>9959587</b>  ISSUE DATE: 19991125</p>

    <p>          APPLICATION: 98  PP: 138 pp.</p>

    <p>          ASSIGNEE:  (Eli Lilly and Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>47.   59367   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Structure-assisted design of mechanism-based irreversible inhibitors of human rhinovirus 3C protease with potent antiviral activity against multiple rhinovirus serotypes</p>

    <p>          Matthews, DA, Dragovich, PS, Webber, SE, Fuhrman, SA, Patick, AK, Zalman, LS, Hendrickson, TF, Love, RA, Prins, TJ, Marakovits, JT, Zhou, R, Tikhe, J, Ford, CE, Meador, JW, Ferre, RA, Brown, EL, Binford, SL, Brothers, MA, Delisle, DM, and Worland, ST</p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>1999</b>.  96(20): 11000-11007</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>48.   59368   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          In vitro antiviral activity of AG7088, a potent inhibitor of human rhinovirus 3C protease</p>

    <p>          Patick, AK, Binford, SL, Brothers, MA, Jackson, RL, Ford, CE, Diem, MD, Maldonado, F, Dragovich, PS, Zhou, R, Prins, TJ, Fuhrman, SA, Meador, JW, Zalman, LS, Matthews, DA, and Worland, ST</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>1999</b>.  43(10): 2444-2450</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>49.   59369   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Anti-picornavirus activity of synthetic flavon-3-yl esters</p>

    <p>          Conti, C, Mastromarino, P, Sgro, R, and Desideri, N</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>1998</b>.  9(6): 511-515</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>50.   59370   DMID-LS-131; WOS-DMID-11/7/2006</p>

    <p class="memofmt1-2">          E7 Proteins From High- and Low-Risk Human Papillomaviruses Bind to Tgf-Beta-Regulated Smad Proteins and Inhibit Their Transcriptional Activity</p>

    <p>          Habig, M. <i>et al.</i></p>

    <p>          Archives of Virology <b>2006</b>.  151(10): 1961-1972</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240723300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240723300005</a> </p><br />
    <br clear="all">

    <p>51.   59371   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro evaluation of the anti-viral activity of N-[4-(1H(2H)-benzotriazol-1(2)-yl)phenyl]alkylcarboxamides</p>

    <p>          Carta, Antonio, Loriga, Giovanni, Piras, Sandra, Paglietti, Giuseppe, Ferrone, Marco, Fermeglia, Maurizio, Pricl, Sabrina, La Colla, Paolo, Secci, Barbara, Collu, Gabriella, and Loddo, Roberta</p>

    <p>          Medicinal Chemistry <b>2006</b>.  2(6): 577-589</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>52.   59372   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Application of quercetin-3-b-galactoside in medicine for inhibiting replication of SARS coronavirus and common cold virus HCoV-229E</p>

    <p>          Zhu, Weiliang, Pan, Chunmu, Chen, Lili, Shen, Xu, Chen, Gang, Luo, Cheng, Xu, Weijun, Luo, Xiaomin, Liu, Hong, Shen, Jianhua, Chen, Kaixian, and Jiang, Hualiang</p>

    <p>          PATENT:  CN <b>1830449</b>  ISSUE DATE: 20060913</p>

    <p>          APPLICATION: 1002-4349  PP: 12pp.</p>

    <p>          ASSIGNEE:  (Shanghai Institute of Materia Medica, Chinese Academy of Sciences Peop. Rep. China and Singapore Polytechnic)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>53.   59373   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          High and low risk human papillomaviruses: disease links, biology, immunology and vaccine development</p>

    <p>          Brinkman, Joeli A, Fahey, Laura M, Kast, Dieuwertje J, Da Silva, Diane M, and Kast, WMartin</p>

    <p>          Viral Oncogenesis <b>2006</b>.: 75-105</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>54.   59374   DMID-LS-131; SCIFINDER-DMID-11/7/2006</p>

    <p class="memofmt1-2">          Pyrrolo[2,3-b]pyridin-4-yl-amines and pyrrolo[2,3-b]pyrimidin-4-yl-amines as janus kinase inhibitors and their preparation, pharmaceutical compositions and use for treatment of diseases</p>

    <p>          Rodgers, James D, Wang, Heisheng, Combs, Andrew P, and Sparks, Richard B</p>

    <p>          PATENT:  WO <b>2006069080</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2005  PP: 86 pp.</p>

    <p>          ASSIGNEE:  (Incyte Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
