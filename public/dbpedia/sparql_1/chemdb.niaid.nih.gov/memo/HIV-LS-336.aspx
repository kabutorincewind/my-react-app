

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-336.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1HUf54CIDENJsrH7PVg0jifo/Ubm3uc1loA1g7Bcj4ycCGVqkm0yTg2BMNxIrB7rzy3cy4wE8grTHJ7sI54235jgdXAdsed+E8Ubb+nFlNMeh/AtwEZ0krCxSYI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D06B48E1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-336-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48185   HIV-LS-336; EMBASE-HIV-11/28/2005</p>

    <p class="memofmt1-2">          The R362A mutation at the C-terminus of CA inhibits packaging of human immunodeficiency virus type 1 RNA</p>

    <p>          Guo, Xiaofeng, Roy, Bibhuti Bhusan, Hu, Jing, Roldan, Ariel, Wainberg, Mark A, and Liang, Chen </p>

    <p>          Virology <b>2005</b>.  343(2): 190-200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4H5MYF3-2/2/d0bd77718a0a395f02cfdc3f7ed94447">http://www.sciencedirect.com/science/article/B6WXR-4H5MYF3-2/2/d0bd77718a0a395f02cfdc3f7ed94447</a> </p><br />

    <p>2.     48186   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Discovery and Characterization of Vicriviroc (SCH 417690), a CCR5 Antagonist with Potent Activity against Human Immunodeficiency Virus Type 1</p>

    <p>          Strizki, JM, Tremblay, C, Xu, S, Wojcik, L, Wagner, N, Gonsiorek, W, Hipkin, RW, Chou, CC, Pugliese-Sivo, C, Xiao, Y, Tagat, JR, Cox, K, Priestley, T, Sorota, S, Huang, W, Hirsch, M, Reyes, GR, and Baroudy, BM</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(12): 4911-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16304152&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16304152&amp;dopt=abstract</a> </p><br />

    <p>3.     48187   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Crystal Structures for HIV-1 Reverse Transcriptase in Complexes with Three Pyridinone Derivatives: A New Class of Non-Nucleoside Inhibitors Effective against a Broad Range of Drug-Resistant Strains</p>

    <p>          Himmel, DM, Das, K, Clark, AD Jr, Hughes, SH, Benjahad, A, Oumouch, S, Guillemont, J, Coupa, S, Poncelet, A, Csoka, I, Meyer, C, Andries, K, Nguyen, CH, Grierson, DS, and Arnold, E</p>

    <p>          J Med Chem <b>2005</b>.  48(24): 7582-7591</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16302798&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16302798&amp;dopt=abstract</a> </p><br />

    <p>4.     48188   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Preferential Targeting of CD4-CCR5 Complexes with Bifunctional Inhibitors: A Novel Approach to Block HIV-1 Infection</p>

    <p>          Mack, M, Pfirstinger, J, Haas, J, Nelson, PJ, Kufer, P, Riethmuller, G, and Schlondorff, D</p>

    <p>          J Immunol <b>2005</b>.  175(11): 7586-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16301668&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16301668&amp;dopt=abstract</a> </p><br />

    <p>5.     48189   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of monopyrrolinone-based HIV-1 protease inhibitors possessing augmented P2(&#39;) side chains</p>

    <p>          Smith, AB, Charnley, AK, Harada, H, Beiger, JJ, Cantin, LD, Kenesky, CS, Hirschmann, R, Munshi, S, Olsen, DB, Stahlhut, MW, Schleif, WA, and Kuo, LC</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16298527&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16298527&amp;dopt=abstract</a> </p><br />

    <p>6.     48190   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Aryl nucleoside H-phosphonates. Part 15: Synthesis, properties and, anti-HIV activity of aryl nucleoside 5&#39;-alpha-hydroxyphosphonates</p>

    <p>          Szymanska, A, Szymczak, M, Boryski, J, Stawinski, J, Kraszewski, A, Collu, G, Sanna, G, Giliberti, G, Loddo, R, and Colla, PL</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16290162&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16290162&amp;dopt=abstract</a> </p><br />

    <p>7.     48191   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 virus replication using small soluble Tat peptides</p>

    <p>          Agbottah, E, Zhang, N, Dadgar, S, Pumfery, A, Wade, JD, Zeng, C, and Kashanchi, F</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16289656&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16289656&amp;dopt=abstract</a> </p><br />

    <p>8.     48192   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          The effect of the HIV protease inhibitor ritonavir on proliferation, differentiation, lipogenesis, gene expression and apoptosis of human preadipocytes and adipocytes</p>

    <p>          Grigem, S, Fischer-Posovszky, P, Debatin, KM, Loizon, E, Vidal, H, and Wabitsch, M</p>

    <p>          Horm Metab Res  <b>2005</b>.  37(10): 602-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16278782&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16278782&amp;dopt=abstract</a> </p><br />

    <p>9.     48193   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Synthesis and biological activities of nucleoside-estradiol conjugates</p>

    <p>          Ali, H, Ahmed, N, Tessier, G, and van, Lier JE</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.  16(2): 317-319</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275069&amp;dopt=abstract</a> </p><br />

    <p>10.   48194   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Anti-HIV-1 protease activity of compounds from Boesenbergia pandurata</p>

    <p>          Cheenpracha, S, Karalai, C, Ponglimanont, C, Subhadhirasakul, S, and Tewtrakul, S</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263298&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263298&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48195   HIV-LS-336; PUBMED-HIV-11/28/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 9-(5&#39;,5&#39;-difluoro-5&#39;-phosphonopentyl)guanine derivatives for PNP-inhibitors</p>

    <p>          Hikishima, S, Isobe, M, Koyanagi, S, Soeda, S, Shimeno, H, Shibuya, S, and Yokomatsu, T</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263289&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263289&amp;dopt=abstract</a> </p><br />

    <p>12.   48196   HIV-LS-336; WOS-HIV-11/20/2005</p>

    <p class="memofmt1-2">          Human beta-defensins suppress human immunodeficiency virus infection: Potential role in mucosal protection</p>

    <p>          Sun, LL, Finnegan, CM, Kish-Catalone, T, Blumenthal, R, Garzino-Demo, P, Maggiore, GML, Berrone, S, Kleinman, C, Wu, ZB, Abdelwahab, S, Lu, WY, and Garzino-Demo, A</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(22): 14318-14329, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232997500048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232997500048</a> </p><br />

    <p>13.   48197   HIV-LS-336; WOS-HIV-11/20/2005</p>

    <p class="memofmt1-2">          High potency of indolyl aryl sulfone nonnucleoside inhibitors towards drug-resistant human immunodeficiency virus type 1 reverse transcriptase mutants is due to selective targeting of different mechanistic forms of the enzyme</p>

    <p>          Cancio, R, Silvestri, R, Ragno, R, Artico, M, De, Martino G, La, Regina G, Crespan, E, Zanoli, S, Hubscher, U, Spadari, S, and Maga, G</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(11): 4546-4554, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233020900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233020900016</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
