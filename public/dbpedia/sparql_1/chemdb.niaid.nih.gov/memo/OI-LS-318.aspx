

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-318.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8enVKGx1XVfy1sdYDCz1nBkIK4JZ8z6TOVe36h8hCDX3/lKjfZ5aCRM0tX5eQI/tdmiE8cj5jUGYf/B1dMxA3uHzple3AW2LOwYGuoziz6XrpwRS7x4AgenS9Kw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D7904AD1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-318-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         45053   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          Determination of the absolute configuration and solution conformation of the antifungal agents ketoconazole, itraconazole, and miconazole with vibrational circular dichroism</p>

    <p>          Dunmire, D, Freedman, TB, Nafie, LA, Aeschlimann, C, Gerber, JG, and Gal, J</p>

    <p>          Chirality <b>2005</b>.  17(S1): S101-S108</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15772975&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15772975&amp;dopt=abstract</a> </p><br />

    <p>2.         45054   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          In vitro activity of 2-cyclohexylidenhydrazo-4-phenyl-thiazole compared with those of amphotericin B and fluconazole against clinical isolates of Candida spp. and fluconazole-resistant Candida albicans</p>

    <p>          De Logu, A, Saddi, M, Cardia, MC, Borgna, R, Sanna, C, Saddi, B, and Maccioni, E</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15772140&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15772140&amp;dopt=abstract</a> </p><br />

    <p>3.         45055   OI-LS-318; EMBASE-OI-3/22/2005</p>

    <p class="memofmt1-2">          Identification of a novel antifungal nonapeptide generated by combinatorial approach</p>

    <p>          Kumar, Manish, Chaturvedi, Ashok K, Kavishwar, Amol, Shukla, PK, Kesarwani, AP, and Kundu, B</p>

    <p>          International Journal of Antimicrobial Agents <b>2005</b>.  25(4): 313-320</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4FN76MR-3/2/5d3f1e16a0cad3117059c3fdf00afa63">http://www.sciencedirect.com/science/article/B6T7H-4FN76MR-3/2/5d3f1e16a0cad3117059c3fdf00afa63</a> </p><br />

    <p>4.         45056   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          Inhibitors of type II NADH:menaquinone oxidoreductase represent a class of antitubercular drugs</p>

    <p>          Weinstein, EA, Yano, T, Li, LS, Avarbock, D, Avarbock, A, Helm, D, McColm, AA, Duncan, K, Lonsdale, JT, and Rubin, H</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15767566&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15767566&amp;dopt=abstract</a> </p><br />

    <p>5.         45057   OI-LS-318; EMBASE-OI-3/22/2005</p>

    <p class="memofmt1-2">          Iron and iron chelating agents modulate Mycobacterium tuberculosis growth and monocyte-macrophage viability and effector functions</p>

    <p>          Cronje, Leandra, Edmondson, Nicole, Eisenach, Kathleen D, and Bornman, Liza</p>

    <p>          FEMS Immunology and Medical Microbiology <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2T-4FP1GFJ-2/2/6c08624d607edbc4f2b8d01574fb2f01">http://www.sciencedirect.com/science/article/B6T2T-4FP1GFJ-2/2/6c08624d607edbc4f2b8d01574fb2f01</a> </p><br />
    <br clear="all">

    <p>6.         45058   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          Synthesis and Antimicrobial Activity of Some 5-[2-(Morpholin-4-yl)acetamido] and/or 5-[2-(4-Substituted piperazin-1-yl)acetamido]-2-(p-substituted phenyl)benzoxazoles</p>

    <p>          Temiz-Arpaci, O, Ozdemir, A, Yalcin, I, Yildiz, I, Aki-Sener, E, and Altanlar, N</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15765492&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15765492&amp;dopt=abstract</a> </p><br />

    <p>7.         45059   OI-LS-318; EMBASE-OI-3/22/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of 4-phenyl/cyclohexyl-5-(1-phenoxyethyl)-3-[N-(2-thiazolyl)acetamido]thio-4H-1,2,4-triazole derivatives</p>

    <p>          Turan-Zitouni, Gulhan, KaplancIklI, Zafer AsIm, YIldIz, Mehmet Taha, Chevallet, Pierre, and Kaya, Demet</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4FM017C-3/2/2eb43510896a5b4e9ccee0adc4d87f1e">http://www.sciencedirect.com/science/article/B6VKY-4FM017C-3/2/2eb43510896a5b4e9ccee0adc4d87f1e</a> </p><br />

    <p>8.         45060   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          Antifungal peptides: Origin, activity, and therapeutic potential</p>

    <p>          De Luca, AJ and Walsh, TJ</p>

    <p>          Rev Iberoam Micol <b>2000</b>.  17(4): 116-120</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15762805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15762805&amp;dopt=abstract</a> </p><br />

    <p>9.         45061   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          Molecular models of protein targets from Mycobacterium tuberculosis</p>

    <p>          Silveira, NJ, Uchoa, HB, Pereira, JH, Canduri, F, Basso, LA, Palma, MS, Santos, DS, and de, Azevedo WF Jr</p>

    <p>          J Mol Model (Online) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15759144&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15759144&amp;dopt=abstract</a> </p><br />

    <p>10.       45062   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          Synthesis, in vitro-Antimycobacterial Activity and Cytotoxicity of Some Alkyl alpha-(5-aryl-1, 3, 4-thiadiazole-2-ylthio)acetates</p>

    <p>          Foroumadi, A, Soltani, F, Moallemzadeh-Haghighi, H, and Shafiee, A</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15756694&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15756694&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       45063   OI-LS-318; PUBMED-OI-3/22/2005</p>

    <p class="memofmt1-2">          p38 MAPK-dependent and YY1-mediated chemokine receptors CCR5 and CXCR4 up-regulation in U937 cell line infected by Mycobacterium tuberculosis or Actinobacillus actinomycetemcomitans</p>

    <p>          Lei, J, Wu, C, Wang, X, and Wang, H</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  329(2): 610-615</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15737629&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15737629&amp;dopt=abstract</a> </p><br />

    <p>12.       45064   OI-LS-318; WOS-OI-3/13/2005</p>

    <p class="memofmt1-2">          Synthetic disaccharide analogs as potential substrates and inhibitors of a mycobacterial polyprenol monophosphomannose-dependent alpha-(1 -&gt; 6)-mannosyltransferase</p>

    <p>          Subramaniam, V, Gurcha, SS, Besra, GS, and Lowary, TL</p>

    <p>          TETRAHEDRON-ASYMMETRY <b>2005</b>.  16(2): 553-567, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227002600029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227002600029</a> </p><br />

    <p>13.       45065   OI-LS-318; EMBASE-OI-3/22/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus-related resistance mechanisms to interferon [alpha]-based antiviral therapy</p>

    <p>          Hofmann, Wolf Peter, Zeuzem, Stefan, and Sarrazin, Christoph</p>

    <p>          Journal of Clinical Virology <b>2005</b>.  32(2): 86-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4F4HB1P-1/2/8a627f9c69dadd98b50b810896c09dad">http://www.sciencedirect.com/science/article/B6VJV-4F4HB1P-1/2/8a627f9c69dadd98b50b810896c09dad</a> </p><br />

    <p>14.       45066   OI-LS-318; WOS-OI-3/13/2005</p>

    <p class="memofmt1-2">          Synthesis of 2-amino-4-ox -5-substitutedbenzylthiopyrrolo[2,3-d]pyrimidines as potential inhibitors of thymidylate synthase</p>

    <p>          Gangjee, A, Jain, HD, Phan, J, and Kisliuk, RL</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2005</b>.  42(1): 165-168, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227157500027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227157500027</a> </p><br />

    <p>15.       45067   OI-LS-318; WOS-OI-3/13/2005</p>

    <p class="memofmt1-2">          Roles of conserved proline and glycosyltransferase motifs of embC in biosynthesis of lipoarabinomannan</p>

    <p>          Berg, S, Starbuck, J, Torrelles, JB, Vissa, VD, Crick, DC, Chatterjee, D, and Brennan, PJ</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(7): 5651-5663, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227217100067">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227217100067</a> </p><br />

    <p>16.       45068   OI-LS-318; WOS-OI-3/13/2005</p>

    <p class="memofmt1-2">          Human immunodeficiency virus-reverse transcriptase inhibition and hepatitis C virus RNA-dependent RNA polymerase inhibition activities of fullerene derivatives</p>

    <p>          Mashino, T, Shimotohno, K, Ikegami, N, Nishikawa, D, Okuda, K, Takahashi, K, Nakamura, S, and Mochizuki, M</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(4): 1107-1109, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227135400047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227135400047</a> </p><br />
    <br clear="all">

    <p>17.       45069   OI-LS-318; WOS-OI-3/13/2005</p>

    <p class="memofmt1-2">          Syntheses C-18 dibenzoeyclooctadiene lignan derivatives as anti-HBsAg and anti-HBeAg agents</p>

    <p>          Kuo, YH, Wu, MD, Hung, CC, Huang, RL, Kuo, LMY, Shen, YC, and Ong, CW</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(5): 1555-1561, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227251300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227251300014</a> </p><br />

    <p>18.       45070   OI-LS-318; WOS-OI-3/13/2005</p>

    <p class="memofmt1-2">          Antioxidant and antiviral activities of plastoquinones from the brown alga Sargassum micracanthum, and a new chromene derivative converted from the plastoquinones</p>

    <p>          Iwashima, M, Mori, J, Ting, X, Matsunaga, T, Hayashi, K, Shinoda, D, Saito, H, Sankawa, U, and Hayashi, T</p>

    <p>          BIOLOGICAL &amp; PHARMACEUTICAL BULLETIN <b>2005</b>.  28(2): 374-377, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226995300037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226995300037</a> </p><br />

    <p>19.       45071   OI-LS-318; WOS-OI-3/13/2005</p>

    <p class="memofmt1-2">          Crystal structures of 2-methylisocitrate lyase in complex with product and with isocitrate inhibitor provide insight into lyase substrate specificity, catalysis and evolution</p>

    <p>          Liu, SJ, Lu, ZB, Han, Y, Melamud, E, Dunaway-Mariano, D, and Herzberg, O</p>

    <p>          BIOCHEMISTRY <b>2005</b>.  44(8): 2949-2962, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227252500023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227252500023</a> </p><br />

    <p>20.       45072   OI-LS-318; WOS-OI-3/20/2005</p>

    <p class="memofmt1-2">          Control of antiviral defenses through hepatitis C virus disruption of retinoic acid-inducible gene-I signaling</p>

    <p>          Foy, E, Li, K, Sumpter, R, Loo, YM, Johnson, CL, Wang, CF, Fish, PM, Yoneyama, M, Fujita, T, Lemon, SM, and Gale, M</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2005</b>.  102(8): 2986-2991, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227232400057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227232400057</a> </p><br />

    <p>21.       45073   OI-LS-318; WOS-OI-3/20/2005</p>

    <p class="memofmt1-2">          Phosphatidylcholine-specific phospholipase C but not gamma interferon regulate gene expression and secretion of CC Chemokine Ligand-2 (CCL-2) by human astrocytes during infection by Toxoplasma gondii</p>

    <p>          Durand, F, Brenier-Pinchart, MP, Berger, F, Marche, PN, Grillot, R, and Pelloux, H</p>

    <p>          PARASITE IMMUNOLOGY <b>2004</b>.  26(10): 419-422, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227391500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227391500005</a> </p><br />

    <p>22.       45074   OI-LS-318; WOS-OI-3/20/2005</p>

    <p class="memofmt1-2">          Biological characterization of periconicins, bioactive secondary metabolites, produced by Periconia sp OBW-15</p>

    <p>          Shin, DS, Oh, MN, Yang, HC, and Oh, KB</p>

    <p>          JOURNAL OF MICROBIOLOGY AND BIOTECHNOLOGY <b>2005</b>.  15(1): 216-220, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227309000034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227309000034</a> </p><br />
    <br clear="all">

    <p>23.       45075   OI-LS-318; WOS-OI-3/20/2005</p>

    <p class="memofmt1-2">          Organometallic-based antibacterial and antifungal compounds: transition metal complexes of 1,1 &#39;-diacetylferrocene-derived thiocarbohydrazone, carbohydrazone, thiosemicarbazone and semicarbazone</p>

    <p>          Chohan, ZH, Pervez, H, Khan, KM, and Supuran, CT</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2005</b>.  20(1): 81-88, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227432900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227432900013</a> </p><br />

    <p>24.       45076   OI-LS-318; WOS-OI-3/20/2005</p>

    <p class="memofmt1-2">          A relaxed discrimination of 2 &#39;-O-methyl-GTP relative to GTP between de novo and elongative RNA synthesis by the hepatitis c RNA-dependent RNA polymerase NS5B</p>

    <p>          Dutartre, H, Boretto, J, Guillemot, JC, and Canard, B</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(8): 6359-6368, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227332700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227332700016</a> </p><br />

    <p>25.       45077   OI-LS-318; WOS-OI-3/20/2005</p>

    <p class="memofmt1-2">          Targeting cell regulation promotes pathogen survival in macrophages</p>

    <p>          Reiner, NE</p>

    <p>          CLINICAL IMMUNOLOGY <b>2005</b>.  114(3): 213-215, 3</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227424600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227424600001</a> </p><br />

    <p>26.       45078   OI-LS-318; WOS-OI-3/20/2005</p>

    <p class="memofmt1-2">          Three oxygenated cyclohexenone derivatives produced by an endophytic fungus</p>

    <p>          Shiono, Y, Murayama, T, Takahashi, K, Okada, K, Katohda, S, and Ikeda, M</p>

    <p>          BIOSCIENCE BIOTECHNOLOGY AND BIOCHEMISTRY <b>2005</b>.  69(2): 287-292, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227415000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227415000005</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
