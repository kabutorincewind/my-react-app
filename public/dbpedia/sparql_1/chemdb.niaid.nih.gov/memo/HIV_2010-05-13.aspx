

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-05-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5VEPV1vw/1kNgSRPyLajR0/bZiLPbSdozRAl5oHrMkQR6c4x374dR+B5mv/9UgmM5qIP4fDWsw1XV90IvvFyueiSyBTOxU+Ybt0WJIZ0ThrSVMf2jtG3+Mxn4qQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D1DD3F42" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 30 - May 13, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20013639">Anti-HIV-1 diterpenoids from leaves and twigs of Polyalthia sclerophylla.</a> Saepou S, Pohmakotr M, Reutrakul V, Yoosook C, Kasisit J, Napaswad C, Tuchinda P. Planta Med. 2010 May;76(7):721-5. Epub 2009 Dec 14.PMID: 20013639</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20219553">In vitro resistance development for RO-0335, a novel diphenylether nonnucleoside reverse transcriptase inhibitor.</a> Javanbakht H, Ptak RG, Chow E, Yan JM, Russell JD, Mankowski MK, Hogan PA, Hogg JH, Vora H, Hang JQ, Li Y, Su G, Paul A, Cammack N, Klumpp K, Heilek G. Antiviral Res. 2010 May;86(2):212-9. Epub 2010 Feb 26.PMID: 20219553</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20347295">Synthesis and biological evaluation of novel 2-arylalkylthio-4-amino-6-benzyl pyrimidines as potent HIV-1 non-nucleoside reverse transcriptase inhibitors.</a>  Qin H, Liu C, Zhang J, Guo Y, Zhang S, Zhang Z, Wang X, Zhang L, Liu J. Bioorg Med Chem Lett. 2010 May 1;20(9):3003-5. Epub 2009 Apr 18.PMID: 20347295</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20371182">Synthesis and biological evaluation of novel C5 halogen-functionalized S-DABO as potent HIV-1 non-nucleoside reverse transcriptase inhibitors.</a> Qin H, Liu C, Guo Y, Wang R, Zhang J, Ma L, Zhang Z, Wang X, Cui Y, Liu J. Bioorg Med Chem. 2010 May 1;18(9):3231-7. Epub 2010 Mar 15.PMID: 20371182</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20435016">Bauhinia variegata var. variegata trypsin inhibitor: From isolation to potential medicinal applications.</a> Fang EF, Wong JH, Bah CS, Lin P, Tsao SW, Ng TB. Biochem Biophys Res Commun. 2010 May 7. [Epub ahead of print]PMID: 20435016</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20437159">Anti-HIV-1 efficacy of extracts from medicinal plants.</a> Lee SA, Hong SK, Suh CI, Oh MH, Park JH, Choi BW, Park SW, Paik SY. J Microbiol. 2010 Apr;48(2):249-52. Epub 2010 May 1.PMID: 20437159</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20439609">Development of Hexadecyloxypropyl Tenofovir (CMX157) for Treatment of Wild-Type and Nucleoside/Nucleotide Resistant HIV.</a> Lanier ER, Ptak RG, Lampert BM, Keilholz L, Hartman T, Buckheit RW Jr, Mankowski MK, Osterling MC, Almond MR, Painter GR. Antimicrob Agents Chemother. 2010 May 3. [Epub ahead of print]PMID: 20439609</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20439611">Syndecan-Fc Hybrid Molecule as a Potent In Vitro Microbicidal Anti-HIV-1 Agent.</a> Bobardt MD, Chatterji U, Schaffer L, de Witte L, Gallay PA. Antimicrob Agents Chemother. 2010 May 3. [Epub ahead of print]PMID: 20439611</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20439612">Novel Protease Inhibitors (PIs) Containing Macrocyclic Components and 3(R),3a(S),6a(R)-bis-Tetrahydrofuranylurethane (bis-THF) That Are Potent Against Multi-PI-Resistant HIV-1 Variants In Vitro.</a> Tojo Y, Koh Y, Amano M, Aoki M, Das D, Ghosh AK, Mitsuya H. Antimicrob Agents Chemother. 2010 May 3. [Epub ahead of print]PMID: 20439612</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20456372">Structure-Activity Relationships (SAR) Research of Thiourea Derivatives as Dual Inhibitors Targeting both HIV-1 Capsid and Human Cyclophilin A.</a> Chen K, Tan Z, He M, Li J, Tang S, Hewlett I, Yu F, Jin Y, Yang M. Chem Biol Drug Des. 2010 May 4. [Epub ahead of print]PMID: 20456372</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276619200018">New Dibenzocyclooctadiene Lignans from Schisandra sphenanthera and Their Pro-inflammatory Cytokine Inhibitory Activities.</a>  Ren, R; Ci, XX; Li, HZ; Li, HM; Luo, GJ; Li, RT; Deng, XM.  ZEITSCHRIFT FUR NATURFORSCHUNG SECTION B-A JOURNAL OF CHEMICAL SCIENCES, 2010; 65(2): 211-218.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276727300010">Synthesis, structural determination, and biological activity of new 7-hydroxy-3-pyrazolyl-4H-chromen-4-ones and their o-beta-D-glucosides.</a>  Hatzade, K; Taile, V; Gaidhane, P; Ingle, V.  TURKISH JOURNAL OF CHEMISTRY, 2010; 34(2): 241-254.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276636800018">Rational Design of Micro-RNA-like Bifunctional siRNAs Targeting HIV and the HIV Coreceptor CCR5.</a>  Ehsani, A; Saetrom, P; Zhang, J; Alluin, J; Li, HT; Snove, O; Aagaard, L; Rossi, JJ.  MOLECULAR THERAPY, 2010; 18(4): 796-802.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276687700009">Synthesis, evaluation and molecular modelling studies of some novel 3-(3,4-dihydroisoquinolin-2(1H)-yl)-N-(substituted-phenyl)propanamides as HIV-1 non-nucleoside reverse transcriptase inhibitors.</a>  Murugesan, S; Ganguly, S; Maga, G.  JOURNAL OF CHEMICAL SCIENCES, 2010; 122(2): 169-176.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000276590500014">Synthesis and HIV-1 Integrase Inhibitory Activity of Furanone Derivatives.</a>  Yu, SH; Zhao, ST; Liu, C; Zhong, Y; Zhao, GS.  CHEMICAL RESEARCH IN CHINESE UNIVERSITIES, 2010; 26(2): 225-229.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
