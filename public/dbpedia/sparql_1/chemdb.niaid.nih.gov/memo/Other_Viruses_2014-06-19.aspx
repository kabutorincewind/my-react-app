

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-06-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xau0ycegZPBJXlDY2ES85vXgBjhmHd5cXZNakBYD0Z5wkCeYW71f99bJDJYCoLrrQ/juuC6tSM2rb6Tf/4heX8nIYklvdRW4KpVZrOlYS/DCwS28viTy3lvZNu4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FFED573B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: June 6 - June 19, 2014</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24835816">B-Ring Modified Aurones as Promising Allosteric Inhibitors of Hepatitis C Virus RNA-dependent RNA Polymerase.</a> Meguellati, A., A. Ahmed-Belkacem, W. Yi, R. Haudecoeur, M. Crouillere, R. Brillet, J.M. Pawlotsky, A. Boumendjel, and M. Peuchmaur. European Journal of Medicinal Chemistry, 2014. 80(10): p. 579-592. PMID[24835816].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0606-061914.</p>

    <h2>Feline Immunodeficiency Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24813732">Evaluation of the Antiviral Efficacy of bis[1,2]Dithiolo[1,4]thiazines and bis[1,2]Dithiolopyrrole Derivatives against the Nucelocapsid Protein of the Feline Immunodeficiency Virus (FIV) as a Model for HIV Infection</a>. Asquith, C.R., M.L. Meli, L.S. Konstantinova, T. Laitinen, M. Perakyla, A. Poso, O.A. Rakitin, K. Allenspach, R. Hofmann-Lehmann, and S.T. Hilton. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(12): p. 2640-2644. PMID[24813732].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0606-061914.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24796673">Interleukin-10 Blocks in vitro Replication of Human Cytomegalovirus by Inhibiting the Virus-induced Autophagy in MRC5 Cells</a>. Wang, L., H. Zhang, J. Qian, K. Wang, and J. Zhu. Biochemical and Biophysical Research Communications, 2014. 448(4): p. 448-453. PMID[24796673].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0606-061914.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24932966">Engineered RNase P Ribozymes Effectively Inhibit Human Cytomegalovirus Gene Expression and Replication</a>. Yang, Z., G.P. Vu, H. Qian, Y.C. Chen, Y. Wang, M. Reeves, K. Zen, and F. Liu. Viruses, 2014. 6(6): p. 2376-2391. PMID[24932966].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0606-061914.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335921200021">Synthesis of 2 &#39;-O,4 &#39;-C-Alkylene-bridged ribonucleosides and Their Evaluation as Inhibitors of HCV NS5B Polymerase</a>. Chapron, C., R. Glen, M. La Colla, B.A. Mayes, J.F. McCarville, S. Moore, A. Moussa, R. Sarkar, M. Seifer, I. Serra, and A. Stewart. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(12): p. 2699-2702; ISI[000335921200021].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0606-061914.</p><br /> 

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336510100031">Optimization of Small-molecule Inhibitors of Influenza Virus Polymerase: From Thiophene-3-carboxamide to Polyamido Scaffolds</a>. Lepri, S., G. Nannetti, G. Muratore, G. Cruciani, R. Ruzziconi, B. Mercorelli, G. Palu, A. Loregian, and L. Goracci. Journal of Medicinal Chemistry, 2014. 57(10): p. 4337-4350; ISI[000336510100031].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0606-061914.</p><br /> 

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335874800029">Development of Bivalent Oleanane-type Triterpenes as Potent HCV Entry Inhibitors.</a> Yu, F., Y.Y. Peng, Q. Wang, Y.Y. Shi, L.L. Si, H. Wang, Y.X. Zheng, E. Lee, S.L. Xiao, M.R. Yu, Y.B. Li, C.L. Zhang, H.L. Tang, C.G. Wang, L.H. Zhang, and D.M. Zhou. European Journal of Medicinal Chemistry, 2014. 77(2): p. 258-268; ISI[000335874800029].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0606-061914.</p><br /> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
