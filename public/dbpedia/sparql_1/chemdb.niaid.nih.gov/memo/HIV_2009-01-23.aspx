

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-01-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KUP6qvaD8QJguqidqdRkNMVrAQn94awWjd8mwiXDrRKTbIa/V4wt63UakGQ0VniLV8WDMZCvfbFPDgzu4Vh+lij95bO8b8Qv9UC5yRUvxnmNoBAdUYT+NYTlZ0g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DD754336" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 7 - January 20, 2009</h1>

    <p class="memofmt2-2">Pubmed Citations:</p>

    <p class="title"><b>1)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19153609?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Structure, interaction and real-time monitoring of the enzymatic reaction of wild-type APOBEC3G.</a></p>

    <p class="authors">Furukawa A, Nagata T, Matsugami A, Habu Y, Sugiyama R, Hayashi F, Kobayashi N, Yokoyama S, Takaku H, Katahira M.</p>

    <p class="source">EMBO J. 2009 Jan 15. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19153609 [PubMed - as supplied by publisher]           </p> 
    <!--  HIV_0107-012009_01; PMID: 19153609  EMBO J.  2009 (16440) --><p class="lrlink"><a href="/Results.aspx?LITREFNO=16440">View Compound Data</a></p>

    <p class="title"><b>2)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19140683?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of Chiral Cyclopropyl Dihydro-Alkylthio-Benzyl-Oxopyrimidine (S-DABO) Derivatives as Potent HIV-1 Reverse Transcriptase Inhibitors with High Activity Against Clinically Relevant Mutants.</a></p>

    <p class="authors">Radi M, Maga G, Alongi M, Angeli L, Samuele A, Zanoli S, Bellucci L, Tafi A, Casaluce G, Giorgi G, Armand-Ugon M, Gonzalez E, Este&#769; JA, Baltzinger M, Bec G, Dumas P, Ennifar E, Botta M.</p>

    <p class="source">J Med Chem. 2009 Jan 13. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19140683 [PubMed - as supplied by publisher]           </p> 
    <!--  HIV_0107-012009_03; PMID: 19140683  J MED CHEM.  2009 (16439) --><p class="lrlink"><a href="/Results.aspx?LITREFNO=16439">View Compound Data</a></p>

    <p class="title"><b>3)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19112691?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Evaluation of the anti-HIV activity of natalizumab, an antibody against integrin alpha4.</a></p>

    <p class="authors">Pauls E, Ballana E, Moncunill G, Bofill M, Clotet B, Ramo-Tello C, Esté JA.</p>

    <p class="source">AIDS. 2009 Jan 14;23(2):266-8.</p>

    <p class="pmid">PMID: 19112691 [PubMed - in process]        </p>

    <p class="title"><b>4)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19153780?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Acylthiocarbamates as non-nucleoside HIV-1 reverse transcriptase inhibitors: docking studies and ligand-based CoMFA and CoMSIA analyses.</a></p>

    <p class="authors">Cichero E, Cesarini S, Spallarossa A, Mosti L, Fossa P.</p>

    <p class="source">J Mol Model. 2009 Jan 20. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19153780 [PubMed - as supplied by publisher]           </p> 
    <!--  HIV_0107-012009_06; PMID: 19153780  J MOL MODEL 2009 (16437) --><p class="lrlink"><a href="/Results.aspx?LITREFNO=16437">View Compound Data</a></p>
    
    <p class="title"><b>5)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19136668?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Suberoylanilide hydroxamic acid reactivates HIV from latently infected cells.</a></p>

    <p class="authors">Contreras X, Schweneker M, Chen CS, McCune JM, Deeks SG, Martin J, Peterlin BM.</p>

    <p class="source">J Biol Chem. 2009 Jan 9. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19136668 [PubMed - as supplied by publisher]           </p>

    <p class="pmid memofmt2-1">&nbsp;</p>

    <p class="title"><b>6)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19139066?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">How binding of small molecule and peptide ligands to HIV-1 TAR alters the RNA motional landscape.</a></p>

    <p class="authors">Bardaro MF Jr, Shajani Z, Patora-Komisarska K, Robinson JA, Varani G.</p>

    <p class="source">Nucleic Acids Res. 2009 Jan 12. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19139066 [PubMed - as supplied by publisher]           </p>

    <p class="pmid memofmt2-1">&nbsp;</p>

    <p class="title"><b>7)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19148268?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">miR-198 inhibits HIV-1 gene expression and replication in monocytes and its mechanism of action appears to involve repression of cyclin T1.</a></p>

    <p class="authors">Sung TL, Rice AP.</p>

    <p class="source">PLoS Pathog. 2009 Jan;5(1):e1000263. Epub 2009 Jan 16.</p>

    <p class="pmid">PMID: 19148268 [PubMed - in process]        </p>

    <p class="title"><b>8)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19099496?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Catalytic asymmetric synthesis of an HIV integrase inhibitor.</a></p>

    <p class="authors">Zhong YL, Krska SW, Zhou H, Reamer RA, Lee J, Sun Y, Askin D.</p>

    <p class="source">Org Lett. 2009 Jan 15;11(2):369-72.</p>

    <p class="pmid">PMID: 19099496 [PubMed - in process]        </p>

    <p class="title"><b>9)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19105629?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Dimer disruption and monomer sequestration by alkyl tripeptides are successful strategies for inhibiting wild-type and multidrug-resistant mutated HIV-1 proteases.</a></p>

    <p class="authors">Bannwarth L, Rose T, Dufau L, Vanderesse R, Dumond J, Jamart-Grégoire B, Pannecouque C, De Clercq E, Reboud-Ravaux M.</p>

    <p class="source">Biochemistry. 2009 Jan 20;48(2):379-87.</p>

    <p class="pmid">PMID: 19105629 [PubMed - in process]        </p>
    <!--  HIV_0107-012009_17; PMID: 19105629  BIOCHEMISTRY.  2009 (16432) --><p class="lrlink"><a href="/Results.aspx?LITREFNO=16432">View Compound Data</a></p>

    <p class="title"><b>10)</b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19072692?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Celebesides A-C and theopapuamides B-D, depsipeptides from an Indonesian sponge that inhibit HIV-1 entry.</a></p>

    <p class="authors">Plaza A, Bifulco G, Keffer JL, Lloyd JR, Baker HL, Bewley CA.</p>

    <p class="source">J Org Chem. 2009 Jan 16;74(2):504-12.</p>

    <p class="pmid">PMID: 19072692 [PubMed - in process]        </p> 

    <p class="memofmt2-2">ISI Web of Knowledge Citations:</p>

    <p class="title"><b>11)</b> <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000261926900011">Structural Characterization, Biological Effects, and Synthetic Studies on Xanthones from Mangosteen (Garcinia mangostana), a Popular Botanical Dietary Supplement</a></p>
    <p class="authors">Young-Won Chin, A. Douglas Kinghorn.</p>
    <p class="source">Mini Reviews in Organic Chemistry. 2008 Nov;5(4): 355-364.            </p>
    <p class="pmid">No Pubmed record</p>
    
    <p class="title"><b>12) </b><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262095500030">Sole copy of Z2-type human cytidine deaminase APOBEC3H has inhibitory activity against retrotransposons and HIV-1</a></p>
    <p class="authors">Tan L, Sarkis PT, Wang T, Tian C, Yu XF.</p>
    <p class="source">FASEB J. 2009 Jan;23(1):279-87.</p>
    <p class="pmid"><a href="http://www.ncbi.nlm.nih.gov/pubmed/18827027?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pubmed link</a></p>
    <p class="pmid">Pubmed ID: 18827027      </p>
    <!--  HIV_0107-012009_25; PMID: 18827027  FASEB J.  2009 (16429) --><p class="lrlink"><a href="/Results.aspx?LITREFNO=16429">View Compound Data</a></p>

    <p class="title"><b>13) </b><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000261984000017">Design and synthesis of 2-(2,6-dibromophenyl)-3-heteroaryl-1,3-thiazolidin-4-ones as anti-HIV agents</a></p>
    <p class="authors">Rawal RK, Tripathi R, Katti SB, Pannecouque C, De Clercq E.</p>
    <p class="source">Eur J Med Chem. 2008 Dec;43(12):2800-6.</p>
    <p class="pmid"><a href="http://www.ncbi.nlm.nih.gov/pubmed/18242784?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pubmed link</a></p>
    <p class="pmid">Pubmed ID: 18242784      </p>

    <p class="title"><b>14) </b><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000261908600046">Discovery of a potent, selective and orally bioavailable 3,9-diazaspiro[5.5]undeca-2-one CCR5 antagonist</a></p>
    <p class="authors">Yang H, Lin XF, Padilla F, Gabriel SD, Heilek G, Ji C, Sankuratri S, deRosier A, Berry P, Rotstein DM.</p>
    <p class="source">Bioorg Med Chem Lett. 2009 Jan 1;19(1):209-13.</p>
    <p class="pmid"><a href="http://www.ncbi.nlm.nih.gov/pubmed/19014885?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pubmed link</a></p>
    <p class="pmid">Pubmed ID: 19014885      </p>
    <!--  HIV_0107-012009_27; PMID: 19014885  BIOORG MED CHEM LETT 2009 (16427) --><p class="lrlink"><a href="/Results.aspx?LITREFNO=16427">View Compound Data</a></p>
    
    <p class="title"><b>15) </b><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000261877500004">Inhibition of HIV Budding by a Genetically Selected Cyclic Peptide Targeting the Gag-TSG101 Interaction</a></p>
    <p class="authors">Tavassoli A, Lu Q, Gam J, Pan H, Benkovic SJ, Cohen SN.</p>
    <p class="source">ACS Chem Biol. 2008 Dec 19;3(12):757-64.</p>
    <p class="pmid"><a href="http://www.ncbi.nlm.nih.gov/pubmed/19053244?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pubmed link</a></p>
    <p class="pmid">Pubmed ID: 19053244      </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
