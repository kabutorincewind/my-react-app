

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-01-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="T+1ixdsz0WOESBS2X7xmXMF6qeyZp4+vUIQk2sMfXTs7H8dudLgPPerKe3maVS0s1Yt4CpZb3vGS9m+qcCcNVNzVueFLt8ea4W4AGun+lDxM1uIJ3yVX6Atz0qw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CC5A82D6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: December 21 - January 3, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22945918">In Vitro and Structural Evaluation of PL-100 as a Potential Second-generation HIV-1 Protease Inhibitor.</a> Asahchop, E.L., M. Oliveira, P.K. Quashie, D. Moisi, J.L. Martinez-Cajas, B.G. Brenner, C.L. Tremblay, and M.A. Wainberg. The Journal of Antimicrobial Chemotherapy, 2013. 68(1): p. 105-112. PMID[22945918].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23206859">Inhibitors of HIV-1 Attachment. Part 11: The Discovery and Structure-Activity Relationships Associated with 4,6-Diazaindole Cores.</a> Bender, J.A., Z. Yang, B. Eggers, Y.F. Gong, P.F. Lin, D.D. Parker, S. Rahematpura, M. Zheng, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 218-222. PMID[23206859].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23123342">Ruthenium Arene Complexes as HIV-1 Integrase Strand Transfer Inhibitors.</a> Carcelli, M., A. Bacchi, P. Pelagatti, G. Rispoli, D. Rogolino, T.W. Sanchez, M. Sechi, and N. Neamati. Journal of Inorganic Biochemistry, 2013. 118: p. 74-82. PMID[23123342].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23136022">Identification of Xanthones from Swertia punicea Using High-performance Liquid Chromatography Coupled with Electrospray Ionization Tandem Mass Spectrometry.</a> Du, X.G., W. Wang, Q.Y. Zhang, J. Cheng, B. Avula, I.A. Khan, and D.A. Guo. Rapid Communications in Mass Spectrometry, 2012. 26(24): p. 2913-2923. PMID[23136022].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23266465">A Silicone Elastomer Vaginal Ring for HIV Prevention Containing Two Microbicides with Different Mechanisms of Action.</a> Fetherston, S.M., P. Boyd, C.F. McCoy, M.C. McBride, K.L. Edwards, S. Ampofo, and R. Karl Malcolm. European Journal of Pharmaceutical Sciences, 2012. <b>[Epub ahead of print]</b>. PMID[23266465].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23114768">Effects of Etravirine on the Pharmacokinetics of the Integrase Inhibitor S/GSK1265744.</a> Ford, S.L., E. Gould, S. Chen, Y. Lou, E. Dumont, W. Spreen, and S. Piscitelli. Antimicrobial Agents and Chemotherapy, 2013. 57(1): p. 277-280. PMID[23114768].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23263005">Cellular Pharmacology and Potency of HIV-1 Nucleoside Analogs in Primary Human Macrophages.</a> Gavegnano, C., M. Detorio, L. Bassit, S.J. Hurwitz, T. North, and R.F. Schinazi. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23263005].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23252879">Traditional Chinese Medicine Application in HIV: An in Silico Study.</a> Huang, H.J., Y.R. Jian, and C.Y. Chen. Journal of Biomolecular Structure &amp; Dynamics, 2012. <b>[Epub ahead of print]</b>. PMID[23252879].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23177258">Design and Synthesis of HIV-1 Protease Inhibitors for a Long-acting Injectable Drug Application.</a> Kesteleyn, B., K. Amssoms, W. Schepens, G. Hache, W. Verschueren, W. Van De Vreken, K. Rombauts, G. Meurs, P. Sterkens, B. Stoops, L. Baert, N. Austin, J. Wegner, C. Masungi, I. Dierynck, S. Lundgren, D. Jonsson, K. Parkes, G. Kalayanov, H. Wallberg, A. Rosenquist, B. Samuelsson, K. Van Emelen, and J.W. Thuring. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 310-317. PMID[23177258].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23206860">Synthesis and Biological Evaluation of Triazolothienopyrimidine Derivatives as Novel HIV-1 Replication Inhibitors.</a> Kim, J., J. Kwon, D. Lee, S. Jo, D.S. Park, J. Choi, E. Park, J.Y. Hwang, Y. Ko, I. Choi, M.K. Ju, J. Ahn, S.J. Han, T.H. Kim, J. Cechetto, J. Nam, S. Ahn, P. Sommer, M. Liuzzi, Z. No, and J. Lee. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 153-157. PMID[23206860].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23230829">Kadcoccitones A and B, Two New 6/6/5/5-Fused Tetracyclic Triterpenoids from Kadsura coccinea.</a> Liang, C.Q., Y.M. Shi, R.H. Luo, X.Y. Li, Z.H. Gao, X.N. Li, L.M. Yang, S.Z. Shang, Y. Li, Y.T. Zheng, H.B. Zhang, W.L. Xiao, and H.D. Sun. Organic Letters, 2012. 14(24): p. 6362-6365. PMID[23230829].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23271463">4862F, a New Inhibitor of HIV-1 Protease, from the Culture of Streptomyces I03A-04862.</a> Liu, X., M. Gan, B. Dong, T. Zhang, Y. Li, Y. Zhang, X. Fan, Y. Wu, S. Bai, M. Chen, L. Yu, P. Tao, W. Jiang, and S. Si. Molecules (Basel, Switzerland), 2012. 18(1): p. 236-243. PMID[23271463].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23167743">New in Silico and Conventional in Vitro Approaches to Advance HIV Drug Discovery and Design.</a> Maga, G., N. Veljkovic, E. Crespan, S. Spadari, J. Prljic, V. Perovic, S. Glisic, and V. Veljkovic. Expert Opinion on Drug Discovery, 2013. 8(1): p. 83-92. PMID[23167743].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23075516">Activities, Crystal Structures, and Molecular Dynamics of Dihydro-1H-isoindole Derivatives, Inhibitors of HIV-1 Integrase.</a> Metifiot, M., K. Maddali, B.C. Johnson, S. Hare, S.J. Smith, X.Z. Zhao, C. Marchand, T.R. Burke, Jr., S.H. Hughes, P. Cherepanov, and Y. Pommier. ACS Chemical Biology, 2012. <b>[Epub ahead of print]</b>. PMID[23075516].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23232916">Unraveling the Binding Interaction and Kinetics of a Prospective anti-HIV Drug with a Model Transport Protein: Results and Challenges.</a> Paul, B.K., D. Ray, and N. Guchhait. Physical Chemistry Chemical Physics : PCCP, 2012. 15(4): p. 1275-1287. PMID[23232916].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23261601">Enhancement of anti-HIV-1 Activity by Hot Spot Evolution of RANTES-derived Peptides.</a> Secchi, M., R. Longhi, L. Vassena, F. Sironi, S. Grzesiek, P. Lusso, and L. Vangelista. Chemistry &amp; Biology, 2012. 19(12): p. 1579-1588. PMID[23261601].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23097440">Mutations in Multiple Domains of Gag Drive the Emergence of in Vitro Resistance to the Phosphonate-containing HIV-1 Protease Inhibitor GS-8374.</a> Stray, K.M., C. Callebaut, B. Glass, L. Tsai, L. Xu, B. Muller, H.G. Krausslich, and T. Cihlar. Journal of Virology, 2013. 87(1): p. 454-463. PMID[23097440].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200254">Inhibitors of HIV-1 Attachment. Part 10. The Discovery and Structure-Activity Relationships of 4-Azaindole Cores.</a> Wang, T., Z. Yang, Z. Zhang, Y.F. Gong, K.A. Riccardi, P.F. Lin, D.D. Parker, S. Rahematpura, M. Mathew, M. Zheng, N.A. Meanwell, J.F. Kadow, and J.A. Bender. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 213-217. PMID[23200254].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200252">Inhibitors of HIV-1 Attachment. Part 7: Indole-7-carboxamides as Potent and Orally Bioavailable Antiviral Agents.</a> Yeung, K.S., Z. Qiu, Q. Xue, H. Fang, Z. Yang, L. Zadjura, C.J. D&#39;Arienzo, B.J. Eggers, K. Riccardi, P.Y. Shi, Y.F. Gong, M.R. Browning, Q. Gao, S. Hansel, K. Santone, P.F. Lin, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 198-202. PMID[23200252].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200244">Inhibitors of HIV-1 Attachment. Part 9: An Assessment of Oral Prodrug Approaches to Improve the Plasma Exposure of a Tetrazole-containing Derivative.</a> Yeung, K.S., Z. Qiu, Z. Yang, L. Zadjura, C.J. D&#39;Arienzo, M.R. Browning, S. Hansel, X.S. Huang, B.J. Eggers, K. Riccardi, P.F. Lin, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 209-212. PMID[23200244].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23200249">Inhibitors of HIV-1 Attachment. Part 8: The Effect of C7-Heteroaryl Substitution on the Potency, and in Vitro and in Vivo Profiles of Indole-based Inhibitors.</a> Yeung, K.S., Z. Qiu, Z. Yin, A. Trehan, H. Fang, B. Pearce, Z. Yang, L. Zadjura, C.J. D&#39;Arienzo, K. Riccardi, P.Y. Shi, T.P. Spicer, Y.F. Gong, M.R. Browning, S. Hansel, K. Santone, J. Barker, T. Coulter, P.F. Lin, N.A. Meanwell, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(1): p. 203-208. PMID[23200249].
    <br />
    <b>[PubMed]</b>. HIV_1221-010313.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311568300031">Rules of Engagement: Molecular Insights from Host-virus Arms Races.</a> Daugherty, M.D. and H.S. Malik. Annual Review of Genetics, 2012. 46: p. 677-700. ISI[000311568300031].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311631700006">Derivatization Chemistry of the Double-decker Dicobalt Sandwich Ion Targeted to Design Biologically Active Substances.</a> Gruner, B., P. Svec, Z. Hajkova, I. Cisarova, J. Pokorna, and J. Konvalinka. Pure and Applied Chemistry, 2012. 84(11): p. 2243-2262. ISI[000311631700006].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311674200003">Consensus Induced Fit Docking (cIFD): Methodology, Validation, and Application to the Discovery of Novel CRM1 Inhibitors.</a> Kalid, O., D.T. Warshaviak, S. Shechter, W. Sherman, and S. Shacham. Journal of Computer-Aided Molecular Design, 2012. 26(11): p. 1217-1228. ISI[000311674200003].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311770900019">Anti-UV/HIV Activity of Kampo Medicines and Constituent Plant Extracts.</a> Kato, T., N. Horie, T. Matsuta, U. Naoki, T. Shimoyama, T. Kaneko, T. Kanamoto, S. Terakubo, H. Nakashima, K. Kusama, and H. Sakagami. In Vivo, 2012. 26(6): p. 1007-1013. ISI[000311770900019].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311893400057">HIV Therapy by a Combination of Broadly Neutralizing Antibodies in Humanized Mice.</a> Klein, F., A. Halper-Stromberg, J.A. Horwitz, H. Gruell, J.F. Scheid, S. Bournazos, H. Mouquet, L.A. Spatz, R. Diskin, A. Abadir, T. Zang, M. Dorner, E. Billerbeck, R.N. Labitt, C. Gaebler, P.M. Marcovecchio, R.B. Incesu, T.R. Eisenreich, P.D. Bieniasz, M.S. Seaman, P.J. Bjorkman, J.V. Ravetch, A. Ploss, and M.C. Nussenzweig. Nature, 2012. 492(7427): p. 118-+. ISI[000311893400057].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311652900006">Use of Virtual Screening for Discovering Antiretroviral Compounds Interacting with the HIV-1 Nucleocapsid Protein.</a> Mori, M., P. Schult-Dietrich, B. Szafarowicz, N. Humbert, F. Debaene, S. Sanglier-Cianferani, U. Dietrich, Y. Mely, and M. Botta. Virus Research, 2012. 169(2): p. 377-387. ISI[000311652900006].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311234000061">Inhibition of 5 &#39;-UTR RNA Conformational Switching in HIV-1 Using Antisense PNAs.</a> Parkash, B., A. Ranjan, V. Tiwari, S.K. Gupta, N. Kaur, and V. Tandon. Plos One, 2012. 7(11). ISI[000311234000061].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311910500005">Membrane-active Peptides Derived from Picornavirus 2B Viroporin.</a> Sanchez-Martinez, S., V. Madan, L. Carrasco, and J.L. Nieva. Current Protein &amp; Peptide Science, 2012. 13(7): p. 632-643. ISI[000311910500005].
    <br />
    <b>[WOS]</b>. HIV_1221-010313.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
