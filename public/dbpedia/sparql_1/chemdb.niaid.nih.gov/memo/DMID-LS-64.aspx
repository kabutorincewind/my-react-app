

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-64.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dQpMLzfmuUOQzPe6Tc5K5Uahd+WyJxdfG1cWfi7CpXSkJP1n0c99DanmBB3NDspAR33RCtW/d1WTM59jlTybklOT9iVV7JWfp2CsSppL7PoltfRTJyfnA6gff0c=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BF017664" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-64-MEMO</p>

<p>    1.    5848   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Synthesis of beta-enantiomers of N4-hydroxy-3&#39;-deoxypyrimidine nucleosides and their evaluation against bovine viral diarrhoea virus and hepatitis C virus in cell culture</p>

<p>           Hollecker, L, Choo, H, Chong, Y, Chu, CK, Lostia, S, McBrayer, TR, Stuyver, LJ, Mason, JC, Du, J, Rachakonda, S, Shi, J, Schinazi, RF, and Watanabe, KA</p>

<p>           Antivir Chem Chemother 2004. 15(1): 43-55</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074714&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074714&amp;dopt=abstract</a> </p><br />

<p>    2.    5849   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of ATP-binding site directed potential inhibitors of nucleoside triphosphatases/helicases and polymerases of hepatitis C and other selected Flaviviridae viruses</p>

<p>           Bretner, M, Schalinski, S, Haag, A, Lang, M, Schmitz, H, Baier, A, Behrens, SE, Kulikowski, T, and Borowski, P</p>

<p>           Antivir Chem Chemother 2004. 15(1): 35-42</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074713&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074713&amp;dopt=abstract</a> </p><br />

<p>    3.    5850   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Inhibition of severe acute respiratory syndrome-associated coronavirus (SARSCoV) by calpain inhibitors and beta-D-N4-hydroxycytidine</p>

<p>           Barnard, DL, Hubbard, VD, Burton, J, Smee, DF, Morrey, JD, Otto, MJ, and Sidwell, RW</p>

<p>           Antivir Chem Chemother 2004. 15(1): 15-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074711&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074711&amp;dopt=abstract</a> </p><br />

<p>    4.    5851   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Antiviral activity of antimicrobial cationic peptides against Junin virus and herpes simplex virus</p>

<p>           Albiol Matanic, Vanesa C and Castilla, Viviana</p>

<p>           International Journal of Antimicrobial Agents 2004. 23(4): 382-389</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T7H-4BY3YK8-7/2/8ec8dfa1034aac9ab389f139a0ca42e1">http://www.sciencedirect.com/science/article/B6T7H-4BY3YK8-7/2/8ec8dfa1034aac9ab389f139a0ca42e1</a> </p><br />

<p>    5.    5852   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Pharmacoeconomic Evaluation of Oseltamivir as Prophylaxis against Influenza Infection</p>

<p>           Sakamaki, H, Ikeda, S, and Ikegami, N</p>

<p>           Yakugaku Zasshi 2004. 124(4): 207-16</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15067184&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15067184&amp;dopt=abstract</a> </p><br />

<p>    6.    5853   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Preparation and antiviral properties of new acyclic, achiral nucleoside analogues: 1- or 9-[3-hydroxy-2-(hydroxymethyl)prop-1-enyl]nucleobases and 1- or 9-[2,3-dihydroxy-2-(hydroxymethyl)propyl]nucleobases</p>

<p>           Boesen, T, Madsen, C, Sejer, Pedersen D, Nielsen, BM, Petersen, AB, Petersen, MA, Munck, M, Henriksen, U, Nielsen, C, and Dahl, O</p>

<p>           Org Biomol Chem 2004. 2(8): 1245-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064804&amp;dopt=abstract</a> </p><br />

<p>    7.    5854   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Synthesis and biological evaluation of novel tert-azido or tert-amino substituted penciclovir analogs</p>

<p>           Ok, Kim H, Won, Baek H, Ryong, Moon H, Kim, DK, Woo, Chun M, and Shin, Jeong L</p>

<p>           Org Biomol Chem 2004. 2(8): 1164-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064793&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064793&amp;dopt=abstract</a> </p><br />

<p>    8.    5855   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Alternative Approaches for Efficient Inhibition of Hepatitis C Virus Rna Replication by Small Interfering Rnas</b></p>

<p>           Kronke, J, Kittler, R, Buchholz, F, Windisch, MP, Pietschmann, T, Bartenschlager, R , and Frese, M</p>

<p>           Journal of Virology 2004. 78(7): 3436-3446</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    9.    5856   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Bioterrorism and emerging infectious disease - antimicrobials, therapeutics and immune-modulators. SARS coronavirus</p>

<p>           Shurtleff, AC </p>

<p>           IDrugs 2004. 7 (2): 91-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15057645&amp;dopt=abstract</a> </p><br />

<p>  10.    5857   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Antiviral drugs in current clinical use*1</p>

<p>           De Clercq, Erik</p>

<p>           Journal of Clinical Virology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4C40YN3-1/2/841b34d1a7c7a0e0f960fa18b5e0a969">http://www.sciencedirect.com/science/article/B6VJV-4C40YN3-1/2/841b34d1a7c7a0e0f960fa18b5e0a969</a> </p><br />

<p>  11.    5858   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Structure-activity relationships for the selectivity of hepatitis C virus NS3 protease inhibitors</p>

<p>           Poliakov, Anton, Johansson, Anja, Akerblom, Eva, Oscarsson, Karin, Samuelsson, Bertil, Hallberg, Anders, and Danielson, UHelena</p>

<p>           Biochimica et Biophysica Acta (BBA) - General Subjects 2004. 1672(1): 51-59</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T1W-4BW9NM9-1/2/280855c3c0525972fa44aa7e6f94c2bd">http://www.sciencedirect.com/science/article/B6T1W-4BW9NM9-1/2/280855c3c0525972fa44aa7e6f94c2bd</a> </p><br />

<p>  12.    5859   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           A target site for template-based design of measles virus entry inhibitors</p>

<p>           Plemper, RK, Erlandson, KJ, Lakdawala, AS, Sun, A, Prussia, A, Boonsombat, J, Aki-Sener, E, Yalcin, I, Yildiz, I, Temiz-Arpaci, O, Tekiner, B, Liotta, DC, Snyder, JP, and Compans, RW</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15056763&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15056763&amp;dopt=abstract</a> </p><br />

<p>  13.    5860   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Syntheses and neuraminidase inhibitory activity of multisubstituted cyclopentane amide derivatives</p>

<p>           Chand, P, Babu, YS, Bantia, S, Rowland, S, Dehghani, A, Kotian, PL, Hutchison, TL, Ali, S, Brouillette, W, El-Kattan, Y, and Lin, TH</p>

<p>           J Med Chem 2004. 47(8): 1919-29</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15055992&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15055992&amp;dopt=abstract</a> </p><br />

<p>  14.    5861   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Synergistic Inhibition of Intracellular Hepatitis C Virus Replication by Combination of Ribavirin and Interferon-Alpha</b></p>

<p>           Tanabe, Y, Sakamoto, N, Enomoto, N, Kurosaki, M, Ueda, E, Maekawa, S, Yamashiro, T, Nakagawa, M, Chen, CH, Kanazawa, N, Kakinuma, S, and Watanabe, M</p>

<p>           Journal of Infectious Diseases 2004. 189(7): 1129-1139</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    5862   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Inhibitors of hepatitis C virus NS3.4A protease. Part 3: P(2) proline variants</p>

<p>           Perni, RB, Farmer, LJ, Cottrell, KM, Court, JJ, Courtney, LF, Deininger, DD, Gates, CA, Harbeson, SL, Kim, JL, Lin, C, Lin, K, Luong, YP, Maxwell, JP, Murcko, MA, Pitlik, J, Rao, BG, Schairer, WC, Tung, RD, Van Drie, JH, Wilson, K, and Thomson, JA</p>

<p>           Bioorg Med Chem Lett 2004. 14(8): 1939-42</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15050632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15050632&amp;dopt=abstract</a> </p><br />

<p>  16.    5863   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Synthesis and biological activity of substituted 2,4,6-s-triazines</p>

<p>           Pandey, VK, Tusi, S, Tusi, Z, Joshi, M, and Bajpai, S</p>

<p>           Acta Pharm 2004. 54(1): 1-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15050040&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15050040&amp;dopt=abstract</a> </p><br />

<p>  17.    5864   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Human MxA protein inhibits the replication of Crimean-Congo hemorrhagic fever virus</p>

<p>           Andersson, I, Bladh, L, Mousavi-Jazi, M, Magnusson, KE, Lundkvist, A, Haller, O, and Mirazimi, A</p>

<p>           J Virol 2004. 78(8): 4323-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047845&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047845&amp;dopt=abstract</a> </p><br />

<p>  18.    5865   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Hepatitis C. Development of New Drugs and Clinical Trials: Promises and Pitfalls - Summary of an Aasld Hepatitis Single Topic Conference, Chicago, Il, February 27-March 1, 2003</b></p>

<p>           Pawlotsky, JM and Mchutchison, JG</p>

<p>           Hepatology 2004. 39(2): 554-567</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  19.    5866   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Purification and characterization of a new antiviral protein from the leaves of Pandanus amaryllifolius (Pandanaceae)</p>

<p>           Ooi, Linda SM, Sun, Samuel SM, and Ooi, Vincent EC</p>

<p>           The International Journal of Biochemistry &amp; Cell Biology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCH-4BWMPBV-2/2/1ac7f9c24b23697726aee97377fa68a7">http://www.sciencedirect.com/science/article/B6TCH-4BWMPBV-2/2/1ac7f9c24b23697726aee97377fa68a7</a> </p><br />

<p>  20.    5867   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Inhibitors of Hepatitis C Virus Ns3 Center Dot 4a Protease 2. Warhead Sar and Optimization</b></p>

<p>           Perni, RB, Pitlik, J, Britt, SD, Court, JJ, Courtney, LF, Deininger, DD, Farmer, LJ, Gates, CA, Harbeson, SL, Levin, RB, Lin, C, Lin, K, Moon, YC, Luong, YP, O&#39;malley, ET, Rao, BG, Thomson, JA, Tung, RD, Van Drie, JH, and Wei, YY</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(6): 1441-1446</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    5868   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>In Vitro Efficacy of Ganciclovir, Cidofovir, Penciclovir, Foscarnet, Idoxuridine, and Acyclovir Against Feline Herpesvirus Type-1</b></p>

<p>           Maggs, DJ and Clarke, HE</p>

<p>           American Journal of Veterinary Research 2004. 65(4): 399-403</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    5869   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Evaluation of antiviral activity against human herpesvirus 8 (HHV-8) and Epstein-Barr virus (EBV) by a quantitative real-time PCR assay</p>

<p>           Friedrichs, Claudia, Neyts, Johan, Gaspar, Gabor, Clercq, Erik De, and Wutzler, Peter</p>

<p>           Antiviral Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4BKGCCF-1/2/e55993bb7fad2f4b367592f0afd20239">http://www.sciencedirect.com/science/article/B6T2H-4BKGCCF-1/2/e55993bb7fad2f4b367592f0afd20239</a> </p><br />

<p>  23.    5870   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Telbivudine/Torcitabine Idenix/Novartis</p>

<p>           Hodge, RA</p>

<p>           Curr Opin Investig Drugs 2004. 5(2): 232-41</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043399&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043399&amp;dopt=abstract</a> </p><br />

<p>  24.    5871   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           The low molecular weight heparan sulfate-mimetic, PI-88, inhibits cell-to-cell spread of herpes simplex virus</p>

<p>           Nyberg, Kicki, Ekblad, Maria, Bergstrom, Tomas, Freeman, Craig, Parish, Christopher R, Ferro, Vito, and Trybala, Edward</p>

<p>           Antiviral Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4BP3D0C-1/2/79abfa95ad91d9b75160a006a0599f7d">http://www.sciencedirect.com/science/article/B6T2H-4BP3D0C-1/2/79abfa95ad91d9b75160a006a0599f7d</a> </p><br />

<p>  25.    5872   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Application of the trak-C(TM) HCV core assay for monitoring antiviral activity in HCV replication systems</p>

<p>           Cagnon, Laurence, Wagaman, Pamela, Bartenschlager, Ralf, Pietschmann, Thomas, Gao, Tiejun, Kneteman, Norman M, Tyrrell, David LJ, Bahl, Chander, Niven, Patrick, Lee, Stephen, and Simmen, Kenneth A</p>

<p>           Journal of Virological Methods 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T96-4BT1DPF-1/2/7ede67c178186b4353e0d5889bdd262d">http://www.sciencedirect.com/science/article/B6T96-4BT1DPF-1/2/7ede67c178186b4353e0d5889bdd262d</a> </p><br />

<p>  26.    5873   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Novel Compounds for the Treatment of Varicella-Zoster Virus Infections</b></p>

<p>           Visalli, RJ</p>

<p>           Expert Opinion on Therapeutic Patents 2004. 14(3): 355-365</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    5874   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Novel Phosphonate Nucleosides as Antiviral Agents</b></p>

<p>           Hwang, JT and Choi, JR</p>

<p>           Drugs of the Future 2004. 29(2): 163-177</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  28.    5875   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Bicyclic anti-VZV nucleosides: thieno analogues bearing an alkylphenyl side chain have reduced antiviral activity</p>

<p>           Angell, Annette, McGuigan, Christopher, Garcia Sevillano, Luis, Snoeck, Robert, Andrei, Graciela, De Clercq, Erik, and Balzarini, Jan</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4C4WWX9-5/2/d30d8fbff7c2dacfa919853295fa2859">http://www.sciencedirect.com/science/article/B6TF9-4C4WWX9-5/2/d30d8fbff7c2dacfa919853295fa2859</a> </p><br />

<p>  29.    5876   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Comparison of the inhibition of human metapneumovirus and respiratory syncytial virus by NMSO3 in tissue culture assays</p>

<p>           Wyde, Philip R, Moylett, Edina H, Chetty, Srikrishna N, Jewell, Alan, Bowlin, Terry L, and Piedra, Pedro A</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4C4JYPR-1/2/b2ea439949c0b2432611e5c3b2145465">http://www.sciencedirect.com/science/article/B6T2H-4C4JYPR-1/2/b2ea439949c0b2432611e5c3b2145465</a> </p><br />

<p>  30.    5877   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Efficacy of ganciclovir and cidofovir against human cytomegalovirus replication in SCID mice implanted with human retinal tissue</p>

<p>           Bidanset, Deborah J, Rybak, Rachel J, Hartline, Caroll B, and Kern, Earl R</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>           <!--HYPERLNK:--> http://www.sciencedirect.com/science/article/B6T2H-4C47FW1-1/2/88769d5f00948ba0108712fb59e6d538</p>

<p>  31.    5878   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Consideration of highly active antiretroviral therapy in the prevention and treatment of severe acute respiratory syndrome</p>

<p>           Chen, XP and Cao, Y</p>

<p>           Clin Infect Dis 2004. 38(7): 1030-2</p>

<p>           <!--HYPERLNK:--> http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15034838&amp;dopt=abstract</p>

<p>  32.    5879   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           2[beta],3[beta]-Difluorosialic acid derivatives structurally modified at the C-4 position: synthesis and biological evaluation as inhibitors of human parainfluenza virus type 1</p>

<p>           Ikeda, Kiyoshi, Kitani, Satoru, Sato, Kazuki, Suzuki, Takashi, Hosokawa, Chika, Suzuki, Yasuo, Tanaka, Kiyoshi, and Sato, Masayuki</p>

<p>           Carbohydrate Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TFF-4C2R35K-3/2/c108622192633c0776a04176033b9c12">http://www.sciencedirect.com/science/article/B6TFF-4C2R35K-3/2/c108622192633c0776a04176033b9c12</a> </p><br />

<p>  33.    5880   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           A simple assay for determining antiviral activity against Crimean-Congo hemorrhagic fever virus</p>

<p>           Paragas, J, Whitehouse, CA, Endy, TP, and Bray, M</p>

<p>           Antiviral Res  2004. 62(1): 21-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15026198&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15026198&amp;dopt=abstract</a> </p><br />

<p>  34.    5881   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Herpesviruses: a brief overview</p>

<p>           Ackermann, M</p>

<p>           Methods Mol Biol 2004. 256: 199-220</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15024168&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15024168&amp;dopt=abstract</a> </p><br />

<p>  35.    5882   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Design of N-acetyl-6-sulfo-beta-d-glucosaminide-based inhibitors of influenza virus sialidase</p>

<p>           Sasaki, K, Nishida, Y, Kambara, M, Uzawa, H, Takahashi, T, Suzuki, T, Suzuki, Y, and Kobayashi, K</p>

<p>           Bioorg Med Chem 2004. 12(6): 1367-75</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15018909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15018909&amp;dopt=abstract</a> </p><br />

<p>  36.    5883   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Influenza-induced tachypnea is prevented in immune cotton rats, but cannot be treated with an anti-inflammatory steroid or a neuraminidase inhibitor</p>

<p>           Eichelberger, Maryna C, Prince, Gregory A, and Ottolini, Martin G</p>

<p>           Virology 2004. In Press, Corrected Proof</p>

<p>           <!--HYPERLNK:--> http://www.sciencedirect.com/science/article/B6WXR-4BYJXNJ-3/2/a86144fe08f2ced1fc0e66bd873073f1</p>

<p>  37.    5884   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           SARS: prognosis, outcome and sequelae</p>

<p>           Chan, KS, Zheng, JP, Mok, YW, Li, YM, Liu, YN, Chu, CM, and Ip, MS</p>

<p>           Respirology 2003. 8 Suppl: S36-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15018132&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15018132&amp;dopt=abstract</a> </p><br />

<p>  38.    5885   DMID-LS-64; PUBMED-DMID-4/13/2004</p>

<p class="memofmt1-2">           Tenofovir disoproxil fumarate for the treatment of lamivudine-resistant hepatitis B</p>

<p>           Kuo, A, Dienstag, JL, and Chung, RT</p>

<p>           Clin Gastroenterol Hepatol 2004. 2(3): 266-72</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15017612&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15017612&amp;dopt=abstract</a> </p><br />

<p>  39.    5886   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Effect of Antimetabolite Immunosuppressants on Flaviviridae, Including Hepatitis C Virus</b></p>

<p>           Stangl, JR, Carroll, KL, Illichmann, M, and Striker, R</p>

<p>           Transplantation 2004. 77(4): 562-567</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  40.    5887   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Cutaneous infections of mice with vaccinia or cowpox viruses and efficacy of cidofovir</p>

<p>           Quenelle, Debra C, Collins, Deborah J, and Kern, Earl R</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4C4VYD4-1/2/5a33d0d14a292da7ad067ca84ef9510f">http://www.sciencedirect.com/science/article/B6T2H-4C4VYD4-1/2/5a33d0d14a292da7ad067ca84ef9510f</a> </p><br />

<p>  41.    5888   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Mutational Patterns Correlate With Genome Organization in Sars and Other Coronaviruses</b></p>

<p>           Grigoriev, A</p>

<p>           Trends in Genetics 2004. 20(3): 131-135</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  42.    5889   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Prevention of lethal respiratory vaccinia infections in mice with interferon-[alpha] and interferon-[gamma]</p>

<p>           Liu, Ge, Zhai, Qingzhu, Schaffner, Dustin J, Wu, Aiguo, Yohannes, Adiamseged, Robinson, Tanisha M, Maland, Matt, Wells, Jay, Voss, Thomas G, Bailey, Charlie, and Alibek, Ken </p>

<p>           FEMS Immunology and Medical Microbiology 2004. 40(3): 201-206</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2T-4B94KCY-1/2/c0cc6332e570d89ee2ce5786c5a31882">http://www.sciencedirect.com/science/article/B6T2T-4B94KCY-1/2/c0cc6332e570d89ee2ce5786c5a31882</a> </p><br />

<p>  43.    5890   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           A rapid detection method for Vaccinia virus, the surrogate for smallpox virus</p>

<p>           Donaldson, Kim A, Kramer, Marianne F, and Lim, Daniel V</p>

<p>           Biosensors and Bioelectronics 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TFC-4C2FF7G-1/2/b89e89307ffdfc75542bccb66c6dc7b4">http://www.sciencedirect.com/science/article/B6TFC-4C2FF7G-1/2/b89e89307ffdfc75542bccb66c6dc7b4</a> </p><br />

<p>  44.    5891   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Severe Acute Respiratory Syndrome Related Coronavirus Is Inhibited by Interferon-Alpha</b></p>

<p>           Stroher, U, Dicaro, A, Li, Y, Strong, JE, Aoki, F, Plummer, F, Jones, SM, and Feldmann, H</p>

<p>           Journal of Infectious Diseases 2004. 189(7): 1164-1167</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  45.    5892   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Polyomavirus Nephropathy: Morphology, Pathophysiology, and Clinical Management</b></p>

<p>           Nickeleit, V, Singh, HK, and Mihatsch, MJ</p>

<p>           Current Opinion in Nephrology and Hypertension 2003. 12(6): 599-605</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  46.    5893   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Old drugs as lead compounds for a new disease? Binding analysis of SARS coronavirus main proteinase with HIV, psychotic and parasite drugs</p>

<p>           Zhang, Xue Wu and Yap, Yee Leng</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4C4DWVY-3/2/4d203e58150749b6f866e1e0ca58e548">http://www.sciencedirect.com/science/article/B6TF8-4C4DWVY-3/2/4d203e58150749b6f866e1e0ca58e548</a> </p><br />

<p>  47.    5894   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Potential targets for anti-SARS drugs in the structural proteins from SARS related coronavirus</p>

<p>           Wu, Guang and Yan, Shaomin</p>

<p>           Peptides 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T0M-4C4C5GH-2/2/bd3a4294ab94733625b80b02d2124059">http://www.sciencedirect.com/science/article/B6T0M-4C4C5GH-2/2/bd3a4294ab94733625b80b02d2124059</a> </p><br />

<p>  48.    5895   DMID-LS-64; EMBASE-DMID-4/11/2004</p>

<p class="memofmt1-2">           Design, synthesis, and structure-activity relationships of pyrazolo[3,4-d]pyrimidines: a novel class of potent enterovirus inhibitors</p>

<p>           Chern, Jyh-Haur, Shia, Kak-Shan, Hsu, Tsu-An, Tai, Chia-Liang, Lee, Chung-Chi, Lee, Yen-Chun, Chang, Chih-Shiang, Tseng, Sung-Nien, and Shih, Shin-Ru</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4C0V4NG-3/2/6f7a052509b6b5669cfb60733a41cc79">http://www.sciencedirect.com/science/article/B6TF9-4C0V4NG-3/2/6f7a052509b6b5669cfb60733a41cc79</a> </p><br />

<p>  49.    5896   DMID-LS-64; WOS-DMID-4/11/2004</p>

<p>           <b>Implications of Finding Synergic in Vitro Drug-Drug Interactions Between Interferon-Alpha and Ribavirin for the Treatment of Hepatitis C Virus</b></p>

<p>           Buckwold, VE</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(3): 413-414</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
