

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-06-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="AjwRuU30I+ik8EVQk2Hz+tiEkeL19y+cwnJdSnG5DUNiRVkBohzx9boWRA7yDdrNp8h2qVjdppMG7dlaGnBRQ1LRzDcd/KdjUc3N/0k70qIcTvdpYpSPEiyGVtQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7418C7A4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: June 7 - June 20, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23774432">Non-competitive Inhibition of Hepatitis B Virus Reverse Transcriptase Priming and DNA Synthesis by the Nucleoside Analog Clevudine.</a> Jones, S.A., E. Murakami, W. Delaney, P. Furman, and J. Hu. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23774432].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23756190">Three New Secoiridoids, Swermacrolactones A-C and anti-Hepatitis B from Swertia macrosperma.</a> Wang, H.L., C.A. Geng, Y.B. Ma, X.M. Zhang, and J.J. Chen. Fitoterapia, 2013. <b>[Epub ahead of print]</b>; PMID[23756190].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>
    
    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23776459">Engineered External Guide Sequences Are Highly Effective in Inhibiting Gene Expression and Replication of Hepatitis B Virus in Cultured Cells.</a> Zhang, Z., G.P. Vu, H. Gong, C. Xia, Y.C. Chen, F. Liu, J. Wu, and S. Lu. Plos One, 2013. 8(6): p. e65268; PMID[23776459].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23490158">Stapled Peptide-based Membrane Fusion Inhibitors of Hepatitis C Virus.</a> Cui, H.K., J. Qing, Y. Guo, Y.J. Wang, L.J. Cui, T.H. He, L. Zhang, and L. Liu. Bioorganic &amp; Medicinal Chemistry, 2013. 21(12): p. 3547-3554; PMID[23490158].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23642966">Discovery of Pyrido[2,3-d]pyrimidine-based Inhibitors of HCV NS5A.</a> Degoey, D.A., D.A. Betebenner, D.J. Grampovnik, D. Liu, J.K. Pratt, M.D. Tufano, W. He, P. Krishnan, T.J. Pilot-Matias, K.C. Marsh, A. Molla, D.J. Kempf, and C.J. Maring. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(12): p. 3627-3630; PMID[23642966].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23774439">The Unique and Highly-selective Anti-cytomegalovirus Activities of Artemisinin-derived Dimer Diphenyl phosphate Stem from Combination of Dimer Unit and a Diphenyl phosphate Moiety.</a> He, R., M. Forman, B.T. Mott, R. Venkatadri, G.H. Posner, and R. Arav-Boger. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23774439].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23757191">Design, Synthesis, and Evaluation of WC5 Analogues as Inhibitors of Cytomegalovirus Immediate-early 2 Protein, a Promising Target for anti-HCMV Treatment.</a> Massari, S., B. Mercorelli, L. Sancineto, S. Sabatini, V. Cecchetti, G. Gribaudo, G. Palu, C. Pannecouque, A. Loregian, and O. Tabarrini. ChemMedChem, 2013. <b>[Epub ahead of print]</b>; PMID[23757191].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>

    <h2>HSV-2</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23761431">Purification and Characterization of a Novel anti-HSV-2 Protein with Antiproliferative and Peroxidase Activities from Stellaria media.</a> Shan, Y., Y. Zheng, F. Guan, J. Zhou, H. Zhao, B. Xia, and X. Feng. Acta Biochimica et Biophysica Sinica, 2013. <b>[Epub ahead of print]</b>; PMID[23761431].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0607-062013.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000318466200004">Screening and Identification of Compounds with Antiviral Activity against Hepatitis B Virus Using a Safe Compound Library and Novel Real-time Immune-absorbance PCR-based High Throughput System.</a> Lamontagne, J., C. Mills, R.C. Mao, C. Goddard, D.W. Cai, H.T. Guo, A. Cuconati, T. Block, and X.Y. Lu. Antiviral Research, 2013. 98(1): p. 19-26; ISI[000318466200004].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0607-062013.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
