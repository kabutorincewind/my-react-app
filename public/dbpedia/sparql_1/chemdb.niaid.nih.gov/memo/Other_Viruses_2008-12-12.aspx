

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2008-12-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="o03+vZW9aQTCrKAApa4r5ymYJ+BH6cMycoMGA7MDK0PRMQ7XOOly1LdN9Y3WKHKS7vpQ8vjIye4x9i2Ki2cHRNGCmR0y+Cx5WDiiGnpjdGQVA1jL2ltzKMWjx3Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F1D7937C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List:  November 27 - December  10, 2008</h1>

    <h2>Hepatitis Virus</h2>

    <p class="ListParagraphCxSpFirst">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19060918?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Natural products as promising drug candidates for the treatment of hepatitis B and C.</a>
    <br />
    Wohlfarth C, Efferth T.
    <br />
    Acta Pharmacol Sin. 2008 Dec 8. [Epub ahead of print]
    <br />
    PMID: 19060918 [PubMed - as supplied by publisher]  
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19055482?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">The Design, Synthesis, and Antiviral Activity of 4&#39;-Azidocytidine Analogues against Hepatitis C Virus Replication: The Discovery of 4&#39;-Azidoarabinocytidine.</a>
    <br />
    Smith DB, Kalayanov G, Sund C, Winqvist A, Pinho P, Maltseva T, Morisson V, Leveque V, Rajyaguru S, Pogam SL, Najera I, Benkestock K, Zhou XX, Maag H, Cammack N, Martin JA, Swallow S, Johansson NG, Klumpp K, Smith M.
    <br />
    J Med Chem. 2008 Dec 5. [Epub ahead of print]
    <br />
    PMID: 19055482 [PubMed - as supplied by publisher]  
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18954982?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Discovery of novel, potent and bioavailable proline-urea based macrocyclic HCV NS3/4A protease inhibitors.</a>
    <br />
    Vendeville S, Nilsson M, de Kock H, Lin TI, Antonov D, Classon B, Ayesa S, Ivanov V, Johansson PO, Kahnberg P, Eneroth A, Wikstrom K, Vrang L, Edlund M, Lindström S, Van de Vreken W, McGowan D, Tahri A, Hu L, Lenz O, Delouvroy F, Van Dooren M, Kindermans N, Surleraux D, Wigerinck P, Rosenquist A, Samuelsson B, Simmen K, Raboisson P.
    <br />
    Bioorg Med Chem Lett. 2008 Dec 1;18(23):6189-93. Epub 2008 Oct 5.
    <br />
    PMID: 18954982 [PubMed - in process]           
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18947995?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Non-nucleoside inhibitors of the hepatitis C virus NS5B RNA-dependant RNA polymerase: 2-aryl-3-heteroaryl-1,3-thiazolidin-4-one derivatives.</a>
    <br />
    Rawal RK, Katti SB, Kaushik-Basu N, Arora P, Pan Z.
    <br />
    Bioorg Med Chem Lett. 2008 Dec 1;18(23):6110-4. Epub 2008 Oct 8.
    <br />
    PMID: 18947995 [PubMed - in process]           </p>

    <p class="ListParagraphCxSpMiddle">&nbsp;</p>

    <p class="ListParagraphCxSpLast">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18824605?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Preclinical characteristics of the hepatitis C virus NS3/4A protease inhibitor ITMN-191 (R7227).</a>
    <br />
    Seiwert SD, Andrews SW, Jiang Y, Serebryany V, Tan H, Kossen K, Rajagopalan PT, Misialek S, Stevens SK, Stoycheva A, Hong J, Lim SR, Qin X, Rieger R, Condroski KR, Zhang H, Do MG, Lemieux C, Hingorani GP, Hartley DP, Josey JA, Pan L, Beigelman L, Blatt LM.
    <br />
    Antimicrob Agents Chemother. 2008 Dec;52(12):4432-41. Epub 2008 Sep 29.
    <br />
    PMID: 18824605 [PubMed - in process]           
    <br />
    <br /></p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
