

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-04-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="b1FfzzCGU5jZunDnxg7h7ENeElyKRlvgaW40hn0MrgWHdKTjshc+ShxaE/GjhsadivMA70qV7ui6EllTVjx1EcM/gdYKCRdipu+zRg8PYhC0AjW4QqgSqU4Xv6A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="059AD47E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">March 19 - April 1, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p />

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20347591">The discovery and structure-activity relationships of pyrano[3,4-b]indole based inhibitors of hepatitis C virus NS5B polymerase.</a> Laporte MG, Draper TL, Miller LE, Blackledge CW, Leister LK, Amparo E, Hussey AR, Young DC, Chunduru SK, Benetatos CA, Rhodes G, Gopalsamy A, Herbertz T, Burns CJ, Condon SM.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2010 Mar 6. [Epub ahead of print]PMID: 20347591 [PubMed - as supplied by publisher]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0319-040110.</p> 
    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20337460">Synthesis of New Acridone Derivatives, Inhibitors of NS3 Helicase, Which Efficiently and Specifically Inhibit Subgenomic HCV Replication.</a> Stankiewicz-Drogon&#769; A, Do&#776;rner B, Erker T, Boguszewska-Chachulska AM. J Med Chem. 2010 Mar 25. [Epub ahead of print]PMID: 20337460 [PubMed - as supplied by publisher]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0319-040110.</p>  
    <p />

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20303772">Structural development studies of anti-hepatitis C virus agents with a phenanthridinone skeleton.</a></p>

    <p class="NoSpacing">Nakamura M, Aoyama A, Salim MT, Okamoto M, Baba M, Miyachi H, Hashimoto Y, Aoyama H.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2010 Mar 2. [Epub ahead of print]PMID: 20303772 [PubMed - as supplied by publisher]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0319-040110.</p> 
    <p />

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20302300">Cyclic Sulfones as Novel P3-Caps for Hepatitis C Virus NS3/4A (HCV NS3/4A) Protease Inhibitors: Synthesis and Evaluation of Inhibitors with Improved Potency and Pharmacokinetic Profiles.</a></p>

    <p class="NoSpacing">Vela&#769;zquez F, Sannigrahi M, Bennett F, Lovey RG, Arasappan A, Bogen S, Nair L, Venkatraman S, Blackman M, Hendrata S, Huang Y, Huelgas R, Pinto P, Cheng KC, Tong X, McPhail AT, Njoroge FG.</p>

    <p class="NoSpacing">J Med Chem. 2010 Mar 19. [Epub ahead of print]PMID: 20302300 [PubMed - as supplied by publisher]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0319-040110.</p> 
    <p />

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275221500004">Benzimidazole Thumb Pocket I finger-loop inhibitors of HCV NS5B polymerase: Improved drug-like properties through C-2 SAR in three sub-series.</a> Beaulieu, Pierre L., et al. BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 20(6):2010. P. 1825-9.</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0319-040110.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275234800024">Induced-Fit Binding of the Macrocyclic Noncovalent Inhibitor TMC435 to its HCV NS3/NS4A Protease Target.</a> Cummings, Maxwell D., et al. ANGEWANDTE CHEMIE-INTERNATIONAL EDITION. 49(9):2010. P. 1652-5.</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0319-040110.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
