

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-301.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+VNi2G4pbgt45k+RH4rgGHspvGB/QZYiXt42Rpf0YksMOb8i0Puh+Pl8t4We0WJfUA/HvyjDhnGMtlM8y/AMGU0LH0Wx6vIZJ00cuOtpNF6EFMtgmsjYDLzsPFU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FD7DCDEC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-301-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43797   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p><b>           Effect of silver-carrying photocatalyst &quot;hikari-gintech&quot; on mycobacterial growth in vitro</b> </p>

<p>           Matsui, Y, Otomo, K, Ishida, S, Yanagihara, K, Kawanobe, Y, Kida, S, Taruoka, E, and Sugawara, I</p>

<p>           Microbiol Immunol 2004. 48(7): 489-95</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15272193&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15272193&amp;dopt=abstract</a> </p><br />

<p>     2.    43798   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           In vitro effects of resveratrol on the viability and infectivity of the microsporidian Encephalitozoon cuniculi</p>

<p>           Leiro, Jose, Cano, Ernesto, Ubeira, Florencio M, Orallo, Francisco, and Sanmartin, Manuel L</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(7): 2497-2501</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     3.    43799   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           5th antiviral drug discovery &amp; development summit</p>

<p>           Blair, W and Perros, M</p>

<p>           Expert Opin Investig Drugs 2004. 13(8): 1065-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268642&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268642&amp;dopt=abstract</a> </p><br />

<p>     4.    43800   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Polyamine metabolism in a member of the phylum Microspora (Encephalitozoon cuniculi): Effects of polyamine analogues</p>

<p>           Bacchi, Cyrus J, Rattendi, Donna, Faciane, Evangeline, Yarlett, Nigel, Weiss, Louis M, Frydman, Benjamin, Woster, Patrick, Wei, Benjamin, Marton, Laurence J, and Wittner, Murray</p>

<p>           Microbiology (Reading, United Kingdom) 2004. 150(5): 1215-1224</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     5.    43801   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           Recent developments in target identification against hepatitis C virus</p>

<p>           Brass, V, Blum, HE, and Moradpour, D</p>

<p>           Expert Opin Ther Targets 2004. 8(4): 295-307</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268625&amp;dopt=abstract</a> </p><br />

<p>     6.    43802   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           Ribozymes in the age of molecular therapeutics</p>

<p>           Bagheri, S and Kashani-Sabet, M</p>

<p>           Curr Mol Med 2004. 4(5): 489-506</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15267221&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15267221&amp;dopt=abstract</a> </p><br />

<p>     7.    43803   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           Characterization of a mitochondrion-like organelle in Cryptosporidium parvum</p>

<p>           Putignani, L, Tait, A, Smith, HV, Horner, D, Tovar, J, Tetley, L, and Wastling, JM</p>

<p>           Parasitology 2004. 129(Pt 1): 1-18</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15267107&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15267107&amp;dopt=abstract</a> </p><br />

<p>     8.    43804   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Cryptosproridium parvum genomics: impact on research and control</p>

<p>           Zhu, G and Abrahamsen, MS</p>

<p>           World Class Parasites 2004. 8(Patogenic Enteric Protozoa): 153-163</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     9.    43805   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           Induction of a Novel Class of Diacylglycerol Acyltransferases and Triacylglycerol Accumulation in Mycobacterium tuberculosis as It Goes into a Dormancy-Like State in Culture</p>

<p>           Daniel, J, Deb, C, Dubey, VS, Sirakova, TD, Abomoelak, B, Morbidoni, HR, and Kolattukudy, PE</p>

<p>           J Bacteriol 2004. 186(15): 5017-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15262939&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15262939&amp;dopt=abstract</a> </p><br />

<p>   10.    43806   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Evidence for mitochondrial-derived alternative oxidase in the apicomplexan parasite Cryptosporidium parvum: a potential anti-microbial agent target</p>

<p>           Roberts, Craig W, Roberts, Fiona, Henriquez, Fiona L, Akiyoshi, Donna, Samuel, Benjamin U, Richards, Thomas A, Milhous, Wilbur, Kyle, Dennis, McIntosh, Lee, Hill, George C, Chaudhuri, Minu, Tzipori, Saul, and McLeod, Rima</p>

<p>           International Journal for Parasitology 2004. 34(3): 297-308</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   11.    43807   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           (E)- and (Z)-1,2,4-Triazolylchromanone oxime ethers as conformationally constrained antifungals</p>

<p>           Emami, Saeed, Falahati, Mehraban, Banifatemi, Ali, Amanlou, Massoud, and Shafiee, Abbas</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(15): 3971-3976</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   12.    43808   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           BILN 2061: a major step toward new therapeutic strategies in hepatitis C</p>

<p>           Asselah, T and Marcellin, P</p>

<p>           J Hepatol 2004. 41(1): 178-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246233&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246233&amp;dopt=abstract</a> </p><br />

<p>   13.    43809   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Rhodanine-3-acetic acid derivatives as inhibitors of fungal protein mannosyl transferase 1 (PMT1)</p>

<p>           Orchard, Michael G, Neuss, Judi C, Galley, Carl MS, Carr, Andrew, Porter, David W, Smith, Phillip, Scopes, David IC, Haydon, David, Vousden, Katherine, Stubberfield, Colin R, Young, Kate, and Page, Martin</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(15): 3975-3978</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   14.    43810   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p><b>           Use of 1-phosphotidylinositol-4-phosphate 5-kinase (MSS4) from Candida or Aspergillus as an antifungal target and screening methods for MSS4 inhibitors</b>((Oxford Glycosciences (UK) Ltd, UK)</p>

<p>           Haydon, David John</p>

<p>           PATENT: WO 2004053150 A1;  ISSUE DATE: 20040624</p>

<p>           APPLICATION: 2003; PP: 23 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.     43811   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           Synthesis and Antimicrobial Activity of New 2-[p-Substituted-benzyl]-5-[substituted-carbonylamino]benzoxazoles</p>

<p>           Yildiz-Oren, I, Tekiner-Gulbas, B, Yalcin, I, Temiz-Arpaci, O, Aki-Sener, E, and Altanlar, N</p>

<p>           Arch Pharm (Weinheim) 2004. 337(7):  402-10</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15237391&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15237391&amp;dopt=abstract</a> </p><br />

<p>   16.    43812   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p><b>           Preparation of pyrazole derivatives as antifungal agents</b>((SSP Co., Ltd. Japan)</p>

<p>           Konno, Fujiko, Nakazawa, Kyoko, Hirota, Hiroyuki, Ishida, Kazuya, Kaneko, Yasushi, and Okouchi, Hisako</p>

<p>           PATENT: WO 2004033432 A1;  ISSUE DATE: 20040422</p>

<p>           APPLICATION: 2003; PP: 102 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   17.    43813   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Design, synthesis and biological evaluation of some pyrazole derivatives as anti-inflammatory-antimicrobial agents</p>

<p>           Bekhit, Adnan A and Abdel-Aziem, Tarek</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(8): 1935-1945</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   18.    43814   OI-LS-301; PUBMED-OI-7/26/2004</p>

<p class="memofmt1-3">           RNase P ribozyme inhibits cytomegalovirus replication by blocking the expression of viral capsid proteins</p>

<p>           Kim, K, Trang, P, Umamoto, S, Hai, R, and Liu, F</p>

<p>           Nucleic Acids Res 2004. 32(11): 3427-34</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220469&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220469&amp;dopt=abstract</a> </p><br />

<p>   19.    43815   OI-LS-301; WOS-OI-7/18/2004</p>

<p class="memofmt1-3">           Fungicidal effect of three new synthetic cationic peptides against Candida albicans</p>

<p>           Nikawa, H, Fukushima, H, Makihira, S, Hamada, T, and Samaranayake, LP</p>

<p>           ORAL DIS 2004. 10(4): 221-228</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221977900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221977900006</a> </p><br />

<p>   20.    43816   OI-LS-301; WOS-OI-7/18/2004</p>

<p class="memofmt1-3">           DNA minor groove binders as potential antitumor and antimicrobial agents</p>

<p>           Baraldi, PG, Bovero, A, Fruttarolo, F, Preti, D, Tabrizi, MA, Pavani, MG, and Romagnoli, R</p>

<p>           MED RES REV 2004. 24(4): 475-528</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222199300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222199300004</a> </p><br />

<p>   21.    43817   OI-LS-301; WOS-OI-7/18/2004</p>

<p class="memofmt1-3">           Dendritic cell trafficking and antigen presentation in the human immune response to Mycobacterium tuberculosis</p>

<p>           Marino, S, Pawar, S, Fuller, CL, Reinhart, TA, Flynn, JL, and Kirschner, DE</p>

<p>           J IMMUNOL 2004. 173(1): 494-506</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222170900063">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222170900063</a> </p><br />

<p>  22.     43818   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p><b>           Preparation of biguanide and dihydrotriazine derivatives for use in pharmaceutical compositions as antimicrobial agents</b>((Jacobus Pharmaceutical Company, Inc. USA)</p>

<p>           Jacobus, David P, Schiehser, Guy Alan, Shieh, Hong-Ming, Jensen, Norman P, and Terpinski, Jacek</p>

<p>           PATENT: WO 2004048320 A1;  ISSUE DATE: 20040610</p>

<p>           APPLICATION: 2003; PP: 83 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   23.    43819   OI-LS-301; WOS-OI-7/18/2004</p>

<p class="memofmt1-3">           Characterization of polynucleotide kinase/phosphatase enzymes from mycobacteriophages omega and Cjw1 and vibriophage KVP40</p>

<p>           Zhu, H, Yin, SM, and Shuman, S</p>

<p>           J BIOL CHEM 2004. 279(25): 26358-26369</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222003000056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222003000056</a> </p><br />

<p>   24.    43820   OI-LS-301; WOS-OI-7/18/2004</p>

<p class="memofmt1-3">           Characterization of Mycobacterium smegmatis expressing the Mycobacterium tuberculosis fatty acid synthase I (fas1) gene</p>

<p>           Zimhony, O, Vilcheze, C, and Jacobs, WR</p>

<p>           J BACTERIOL 2004. 186(13): 4051-4055</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222189500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222189500001</a> </p><br />

<p>   25.    43821   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p><b>           Antimycobacterial compounds</b> ((USA))</p>

<p>           Yatvin, Milton B and Pederson, Richard L</p>

<p>           PATENT: US 20040127506 A1;  ISSUE DATE: 20040701</p>

<p>           APPLICATION: 2003-16446; PP: 9 pp., Cont. of U.S. Ser. No. 994,974.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   26.    43822   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Influence of lipophilicity on the antimycobacterial activity of the hydrochlorides of piperidinylethyl esters of ortho-substituted phenylcarbamic acids</p>

<p>           Waisser, K, Drazkova, K, Cizmarik, J, and Kaustova, J</p>

<p>           Scientia Pharmaceutica 2004. 72(1): 43-49</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   27.    43823   OI-LS-301; WOS-OI-7/18/2004</p>

<p class="memofmt1-3">           Decision analysis to evaluate proof-of-principle trial design for a new drug candidate</p>

<p>           Viswanathan, V and Bayney, R</p>

<p>           INTERFACES 2004. 34(3): 206-207</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222197900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222197900004</a> </p><br />

<p>   28.    43824   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Synthesis of tetrahydro-2H-[1, 3, 5]thiadiazine-5-(4-pyridylcarboxamido)-2-thione with antitubercular activity</p>

<p>           Sriram, D, Mallika, KJyothi, and Yogeeswari, P</p>

<p>           Scientia Pharmaceutica 2004. 72(1): 35-41</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   29.    43825   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Synthesis and antimycobacterial activity of 1,2,4-triazole 3-benzylsulfanyl derivatives</p>

<p>           Klimesova, Vera, Zahajska, Lenka, Waisser, Karel, Kaustova, Jarmila, and Mollmann, Ute</p>

<p>           Farmaco 2004. 59(4): 279-288</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.     43826   OI-LS-301; WOS-OI-7/18/2004</p>

<p class="memofmt1-3">           Comparative genomic assessment of novel broad-spectrum targets for antibacterial drugs</p>

<p>           White, TA and Kell, DB</p>

<p>           COMP FUNCT GENOM 2004. 5(4): 304-327</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222231600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222231600001</a> </p><br />

<p>   31.    43827   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p><b>           Preparation of pyrrole derivatives as antimycobacterial compounds</b>((Lupin Limited, India)</p>

<p>           Arora, Sudershan Kumar, Sinha, Neelima, Jain, Sanjay, Upadhayaya, Ram Shankar, Jana, Gourhari, Ajay, Shankar, and Sinha, Rakesh Kumar</p>

<p>           PATENT: WO 2004026828 A1;  ISSUE DATE: 20040401</p>

<p>           APPLICATION: 2002; PP: 66 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   32.    43828   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Antimycobacterial N-pyridinylsalicylamides, isosters of salicylamides</p>

<p>           Waisser, Karel, Drazkova, Katerina, Kunes, Jiri, Klimesova, Vera, and Kaustova, Jarmila</p>

<p>           Farmaco 2004. 59(8): 615-625</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   33.    43829   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           The Arden house Conference on Tuberculosis, revisited: Perspectives for tuberculosis elimination in the United States</p>

<p>           Jereb, J, Albalak, R, and Castro, K</p>

<p>           SEM RESP CRIT CARE M 2004. 25(3): 255-269</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800003</a> </p><br />

<p>   34.    43830   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Mycobacterial infections caused by nontuberculous mycobacteria</p>

<p>           Heifets, L</p>

<p>           SEM RESP CRIT CARE M 2004. 25(3): 283-295</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800005</a> </p><br />

<p>   35.    43831   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Antimicrobial therapy of tuberculosis: Justification for currently recommended treatment regimens</p>

<p>           Mitchison, DA </p>

<p>           SEM RESP CRIT CARE M 2004. 25(3): 307-315</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800007</a> </p><br />

<p>   36.    43832   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Tuberculosis and HIV/AIDS: Epidemiological and clinical aspects (World perspective)</p>

<p>           Bock, N and Reichman, LB</p>

<p>           SEM RESP CRIT CARE M 2004. 25(3): 337-344</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222336800009</a> </p><br />

<p>   37.    43833   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral activity of monofluorinated cyclopropanoid nucleosides</p>

<p>           Rosen, Thomas C, De Clercq, Erik, Balzarini, Jan, and Haufe, Guenter</p>

<p>           Organic &amp; Biomolecular Chemistry 2004. 2(2): 229-237</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   38.    43834   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Synthesis and antimycobacterial activity of some beta-carboline alkaloids</p>

<p>           Begum, S, Hassan, SI, and Siddiqui, BS</p>

<p>           NAT PROD RES 2004. 18(4): 341-347</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222182400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222182400010</a> </p><br />

<p>   39.    43835   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           The Synthesis and Antiviral Activity of Glycyrrhizic Acid Conjugates with a-D-Glucosamine and Some Glycosylamines</p>

<p>           Kondratenko, RM, Baltina, LA, Mustafina, SR, Vasil&#39;eva, EV, Pompei, R, Deidda, D, Plyasunova, OA, Pokrovskii, AG, and Tolstikov, GA</p>

<p>           Russian Journal of Bioorganic Chemistry (Translation of Bioorganicheskaya Khimiya) 2004. 30(3): 275-282</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   40.    43836   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Endogenous polyamine levels in macrophages is sufficient to support growth of Toxoplasma gondii</p>

<p>           Seabra, SH, DaMatta, RA, de, Mello FG, and de, Souza W</p>

<p>           J PARASITOL 2004. 90(3): 455-460</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222292200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222292200003</a> </p><br />

<p>   41.    43837   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Antiviral activity of natural and synthetic brassinosteroids</p>

<p>           Wachsman, Monica B, Ramirez, Javier A, Talarico, Laura B, Galagovsky, Lydia R, and Coto, Celia E</p>

<p>           Current Medicinal Chemistry: Anti-Infective Agents 2004 . 3(2): 163-179</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   42.    43838   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Efficacy of ponazuril in vitro and in preventing and treating Toxoplasma gondii infections in mice</p>

<p>           Mitchell, SM, Zajac, AM, Davis, WL, and Lindsay, DS</p>

<p>           J PARASITOL 2004. 90(3): 639-642</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222292200032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222292200032</a> </p><br />

<p>   43.    43839   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Evaluation of antiviral activity against human herpesvirus 8 (HHV-8) and Epstein-Barr virus (EBV) by a quantitative real-time PCR assay</p>

<p>           Friedrichs, Claudia, Neyts, Johan, Gaspar, Gabor, De Clercq, Erik, and Wutzler, Peter</p>

<p>           Antiviral Research 2004. 62(3): 121-123</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   44.    43840   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Expression of hepatitis C virus core protein inhibits interferon-induced nuclear import of STATs</p>

<p>           Melen, K, Fagerlund, R, Nyqvist, M, Keskinen, P, and Julkunen, I</p>

<p>           J MED VIROL 2004. 73(4): 536-547</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222317200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222317200007</a> </p><br />

<p>   45.    43841   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Design, synthesis, and structure-activity relationships of pyrazolo[3,4-d]pyrimidines: a novel class of potent enterovirus inhibitors</p>

<p>           Chern, Jyh-Haur, Shia, Kak-Shan, Hsu, Tsu-An, Tai, Chia-Liang, Lee, Chung-Chi, Lee, Yen-Chun, Chang, Chih-Shiang, Tseng, Sung-Nien, and Shih, Shin-Ru</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(10): 2519-2525</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   46.    43842   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           YM-215343, a novel antifungal compound from Phoma sp QN04621</p>

<p>           Shibazaki, M, Taniguchi, M, Yokoi, T, Nagai, K, Watanabe, M, Suzuki, K, and Yamamoto, T</p>

<p>           J ANTIBIOT 2004. 57(6): 379-382</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222339800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222339800003</a> </p><br />

<p>  47.     43843   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Design and synthesis of DNA-intercalating 9-fluoren-b-O-glycosides as potential IFN-inducers, and antiviral and cytostatic agents</p>

<p>           Alcaro, S, Arena, A, Neri, S, Ottana, R, Ortuso, F, Pavone, B, and Vigorita, MG</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(7): 1781-1791</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   48.    43844   OI-LS-301; WOS-OI-7/25/2004</p>

<p class="memofmt1-3">           Mycobactetium avium subsp paratuberculosis fibronectin attachment protein facilitates M-cell targeting and invasion through a fibronectin bridge with host integrins</p>

<p>           Secott, TE, Lin, TL, and Wu, CC</p>

<p>           INFECT IMMUN 2004. 72(7): 3724-3732</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222282800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222282800004</a> </p><br />

<p>   49.    43845   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Synthesis and Antiviral Evaluation of Some 3&#39;-Fluoro Bicyclic Nucleoside Analogues</p>

<p>           McGuigan, Christopher, Carangio, Antonella, Snoeck, Robert, Andrei, Graciela, De Clercq, Erik, and Balzarini, Jan</p>

<p>           Nucleosides, Nucleotides &amp; Nucleic Acids 2004. 23(1 &amp; 2): 1-5</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   50.    43846   OI-LS-301; SCIFINDER-OI-7/21/2004</p>

<p class="memofmt1-3">           Inhibition of herpes simplex virus infection by lactoferrin is dependent on interference with the virus binding to glycosaminoglycans</p>

<p>           Marchetti, Magda, Trybala, Edward, Superti, Fabiana, Johansson, Maria, and Bergstrom, Tomas</p>

<p>           Virology 2004. 318(1): 405-413</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
