

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-04-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ItlHZP512HXsBEJDhmXjawOXrO78Lywx+fIlTy2s2nCAlIBEX0BNkH1t5S6hXecMQYohahjQOuaFplzEKUUkQ6WUuG7+ZG+bWcTzk0UKPaGMxgSJi0cG7WIwRJs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="44D5D23C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 1 - April 15, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20379971">Amino Acid Derivatives, Part 4: Synthesis and Anti-HIV Activity of New Naphthalene Derivatives.</a> Hamad NS, Al-Haidery NH, Al-Masoudi IA, Sabri M, Sabri L, Al-Masoudi NA. Arch Pharm (Weinheim). 2010 Apr 8. [Epub ahead of print]PMID: 20379971</p>

    <p>2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20378319">First isolation and characterization of a novel lectin with potent antitumor activity from a Russula mushroom.</a> Zhang G, Sun J, Wang H, Ng TB.  Phytomedicine. 2010 Apr 6. [Epub ahead of print]PMID: 20378319</p>

    <p>3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20377249">&quot;Viologen&quot; Dendrimers as Antiviral Agents: The Effect of Charge Number and Distance (dagger).</a>  Asaftei S, De Clercq E.  J Med Chem. 2010 Apr 8. [Epub ahead of print]PMID: 20377249</p>

    <p>4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20363737">Small molecular inhibitors for HIV-1 replication through specifically stabilizing APOBEC3G.</a>  Cen S, Peng ZG, Li XY, Li ZR, Ma J, Wang YM, Fan B, You XF, Wang YP, Liu F, Shao RG, Zhao LX, Yu L, Jiang JD.  J Biol Chem. 2010 Apr 2. [Epub ahead of print]PMID: 20363737</p>

    <p>5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20307984">Synthesis and biological evaluation of 4-(hydroxyimino)arylmethyl diarylpyrimidine analogues as potential non-nucleoside reverse transcriptase inhibitors against HIV.</a>  Feng XQ, Zeng ZS, Liang YH, Chen FE, Pannecouque C, Balzarini J, De Clercq E.  Bioorg Med Chem. 2010 Apr 1;18(7):2370-4. Epub 2010 Mar 9.PMID: 20307984</p>

    <p>6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20207537">Novel N-substituted benzimidazole CXCR4 antagonists as potential anti-HIV agents.</a> Miller JF, Turner EM, Gudmundsson KS, Jenkinson S, Spaltenstein A, Thomson M, Wheelan P. Bioorg Med Chem Lett. 2010 Apr 1;20(7):2125-8. Epub 2010 Feb 14.PMID: 20207537</p>

    <p>7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20194023">Synthesis of a novel tricyclic 1,2,3,4,4a,5,6,10b-octahydro-1,10-phenanthroline ring system and CXCR4 antagonists with potent activity against HIV-1.</a>  Catalano JG, Gudmundsson KS, Svolto A, Boggs SD, Miller JF, Spaltenstein A, Thomson M, Wheelan P, Minick DJ, Phelps DP, Jenkinson S.  Bioorg Med Chem Lett. 2010 Apr 1;20(7):2186-90. Epub 2010 Feb 12.PMID: 20194023</p>

    <p>8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20112333">Binding of multivalent anionic porphyrins to V3 loop fragments of an HIV-1 envelope and their antiviral activity.</a>  Watanabe K, Negi S, Sugiura Y, Kiriyama A, Honbo A, Iga K, Kodama EN, Naitoh T, Matsuoka M, Kano K.  Chem Asian J. 2010 Apr 1;5(4):825-34.PMID: 20112333</p>

    <p>9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20086149">Inhibition of human immunodeficiency virus type 1 by triciribine involves the accessory protein nef.</a>  Ptak RG, Gentry BG, Hartman TL, Watson KM, Osterling MC, Buckheit RW Jr, Townsend LB, Drach JC.  Antimicrob Agents Chemother. 2010 Apr;54(4):1512-9. Epub 2010 Jan 19.PMID: 20086149</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>10.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275705500010">Copper-catalyzed Synthesis and Antimicrobial Activity of Disubstituted 1,2,3-Triazoles Starting from 1-Propargyluracils and Ethyl (4-Azido-1,2,3-trihydroxybutyl)furan-3-carboxylate.</a>  El-Sayed, WA; Abdel-Rahman, AAH. ZEITSCHRIFT FUR NATURFORSCHUNG SECTION B-A JOURNAL OF CHEMICAL SCIENCES, 2010; 65(1): 57-66.</p>

    <p>11.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273043200026">Synthesis and Evaluation of in Vitro Biological Activity of 4-Substituted Arylpiperazine Derivatives of 1,7,8,9-Tetrachloro-10,10-dimethoxy-4-azatricyclo[5.2.1.0(2,6)]dec-8-ene -3,5-dione.</a>  Kossakowski, J; Pakosinska-Parys, M; Struga, M; Dybala, I; Koziol, AE; La Colla, P; Marongiu, LE; Ibba, C; Collu, D; Loddo, R.  MOLECULES, 2009; 14(12): 5189-5202.</p>

    <p>12.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275805900017">Discovery of a synthetic dual inhibitor of HIV and HCV infection based on a tetrabutoxy-calix[4] arene scaffold.</a>  Tsou, LK; Dutschman, GE; Gullen, EA; Telpoukhovskaia, M; Cheng, YC; Hamilton, AD.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(7): 2137-2139.</p>

    <p>13.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275805900023">Bioisostere of valtrate, anti-HIV principle by inhibition for nuclear export of Rev.</a>  Tamura, S; Shimizu, N; Fujiwara, K; Kaneko, M; Kimura, T; Murakami, N.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(7): 2159-2162.</p>

    <p>14.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275805900029">Synthesis of a novel tricyclic 1,2,3,4,4a,5,6,10b-octahydro-1,10-phenanthroline ring system and CXCR4 antagonists with potent activity against HIV-1.</a>  Catalano, JG; Gudmundsson, KS; Svolto, A; Boggs, SD; Miller, JF; Spaltenstein, A; Thomson, M; Wheelan, P; Minick, DJ; Phelps, DP; Jenkinson, S.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(7): 2186-2190.</p>

    <p>15.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275805900036">Discovery of novel (S)-alpha-phenyl-gamma-amino butanamide containing CCR5 antagonists via functionality inversion approach.</a>  Zhang, HS; Feng, DZ; Chen, L; Long, YQ.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(7): 2219-2223.</p>

    <p>16.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275662700009">Inhibition of HIV-1 Replication by a Bis-Thiadiazolbenzene-1,2-Diamine That Chelates Zinc Ions from Retroviral Nucleocapsid Zinc Fingers.</a>  Pannecouque, C; Szafarowicz, B; Volkova, N; Bakulev, V; Dehaen, W; Mely, Y; Daelemans, D.  ANTIMICROBIAL AGENTS AND CHEMOTHERAPY, 2010; 54(4): 1461-1468.</p>

    <p>17.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275956900057">Design, Synthesis and Anti-HIV Integrase Evaluation of N-(5-Chloro-8-Hydroxy-2-Styrylquinolin-7-yl)Benzenesulfonamide Derivatives.</a>  Jiao, ZG;   He, HQ; Zeng, CC; Tan, JJ; Hu, LM; Wang, CX.  MOLECULES, 2010; 15(3): 1903-1917.</p>

    <p>18.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275929300001">Novel Thiazolidinone Derivatives with an Uncommon Mechanism of Inhibition Towards HIV-1 Reverse Transcriptase.</a>  Pitta, E; Crespan, E; Geronikaki, A; Maga, G; Samuele, A.  LETTERS IN DRUG DESIGN &amp; DISCOVERY, 2010; 7(4): 228-234.</p>

    <p>19.    <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275949500004">Inhibition of HIV-1 Replication by Isoxazolidine and Isoxazole Sulfonamides.</a>  Loh, B; Vozzolo, L; Mok, BJ; Lee, CC; Fitzmaurice, RJ; Caddick, S; Fassati, A.  CHEMICAL BIOLOGY &amp; DRUG DESIGN, 2010; 75(5): 461-474.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
