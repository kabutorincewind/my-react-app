

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-331.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cFIZD6b957qxRcwF0NbqagDCCvq9MKk9iXIs3TQD2mlb+mUdi0EfSBlvKgQCeDh2gUQL8U7NpQP33wj6PKQU8W9VTyYLdE3OELNGABfH8DpyO07+IXjCVnBF12Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ECA9965D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-331-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47870   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Computational Studies and Drug Design for HIV-1 Reverse Transcriptase Inhibitors of 3&#39;,4&#39;-di-O-(S)-camphanoyl-(+)-cis-Khellactone (DCK) Analogs</p>

    <p>          Chen, HF, Fan, BT, Zhao, CY, Xie, L, Zhao, CH, Zhou, T, Lee, KH, and Allaway, G</p>

    <p>          J Comput Aided Mol Des <b>2005</b>.  19(4): 243-58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16163451&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16163451&amp;dopt=abstract</a> </p><br />

    <p>2.     47871   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Quantitative Structure-activity Relationship Analysis of Pyridinone HIV-1 Reverse Transcriptase Inhibitors using the k Nearest Neighbor Method and QSAR-based Database Mining</p>

    <p>          Medina-Franco, JL, Golbraikh, A, Oloff, S, Castillo, R, and Tropsha, A</p>

    <p>          J Comput Aided Mol Des <b>2005</b>.  19(4): 229-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16163450&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16163450&amp;dopt=abstract</a> </p><br />

    <p>3.     47872   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          Preparation of PNA-neamine conjugates as antiviral agents</p>

    <p>          Decout, Jean-Luc, Pandey, Virendra N, and Riguet, Emmanuel</p>

    <p>          PATENT:  WO <b>2005060573</b>  ISSUE DATE:  20050707</p>

    <p>          APPLICATION: 2004  PP: 67 pp.</p>

    <p>          ASSIGNEE:  (University of Medicine and Dentistry of New Jersey, USA and Universite Joseph Fourier)</p>

    <br />

    <p>4.     47873   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Synthesis, Anti-HIV Activity, and Metabolic Stability of New Alkenyldiarylmethane HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors</p>

    <p>          Deng, BL, Hartman, TL, Buckheit, RW Jr, Pannecouque, C, De, Clercq E, Fanwick, PE, and Cushman, M</p>

    <p>          J Med Chem <b>2005</b>.  48(19): 6140-6155</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16162014&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16162014&amp;dopt=abstract</a> </p><br />

    <p>5.     47874   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Naturally derived anti-HIV agents</p>

    <p>          Asres, K, Seyoum, A, Veeresham, C, Bucar, F, and Gibbons, S</p>

    <p>          Phytother Res <b>2005</b>.  19(7): 557-581</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16161055&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16161055&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47875   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Targeting HIV protease: From peptidomimetics to non-peptide inhibitors</p>

    <p>          Gago, F</p>

    <p>          IDrugs <b>1999</b>.  2(4): 309-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16158350&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16158350&amp;dopt=abstract</a> </p><br />

    <p>7.     47876   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Inhibition of CCR5-mediated infection by diverse R5 and R5X4 HIV and SIV isolates using novel small molecule inhibitors of CCR5: Effects of viral diversity, target cell and receptor density</p>

    <p>          Willey, S, Peters, PJ, Sullivan, WM, Dorr, P, Perros, M, and Clapham, PR</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16157392&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16157392&amp;dopt=abstract</a> </p><br />

    <p>8.     47877   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          A novel heme-containing protein with anti-HIV-1 activity from skin secretions of Bufo andrewsi</p>

    <p>          Zhao, Y, Jin, Y, Wang, JH, Wang, RR, Yang, LM, Lee, WH, Zheng, YT, and Zhang, Y</p>

    <p>          Toxicon <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16157359&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16157359&amp;dopt=abstract</a> </p><br />

    <p>9.     47878   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          Cell-based and biochemical screening approaches for the discovery of novel HIV-1 inhibitors</p>

    <p>          Westby, Mike, Nakayama, Grace R, Butler, Scott L, and Blair, Wade S</p>

    <p>          Antiviral Research <b>2005</b>.  67(3): 121-140</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   47879   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          New and improved 5,10-dihydrobenzo[b][1,8]naphthyridine-N-oxides as the next generation NNRTI&#39;s with better activity profiles against clinically relevant HIV-1 mutants</p>

    <p>          Srivastava, Anurag S, Tarby, Christine M, Johnson, Barry M, Rajagopal, Bhakthavatchalam, Curry, Matthew, Lin, Qiyan, Wang, Haisheng, Cocuzza, Anthony J, Bilder, Donna M, McHugh, Robert J, Patel, Mona, Bacheler, Lee T, Diamond, Sharon, Jeffrey, Susan, Klabe, Ronald M, Cordova, Beverly C, Garber, Sena, Logue, Kelly, Erickson-Viitanen, Susan, Trainor, George L, and Rodgers, James D</p>

    <p>          Abstracts of Papers, 230th ACS National Meeting, Washington, DC, United States, Aug. 28-Sept. 1, 2005 <b>2005</b>.: MEDI-181</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>11.   47880   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Covalent stabilization of coiled coils of the HIV gp41 N region yields extremely potent and broad inhibitors of viral infection</p>

    <p>          Bianchi, E, Finotto, M, Ingallinella, P, Hrin, R, Carella, AV, Hou, XS, Schleif, WA, Miller, MD, Geleziunas, R, and Pessi, A</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.  102(36): 12903-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16129831&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16129831&amp;dopt=abstract</a> </p><br />

    <p>12.   47881   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          Inhibiting replication of a retrovirus, such as human immunodeficiency virus (HIV), through modulation of HIV Vif- or Vpu-mediated ubiquitylation of host cell proteins, such as CEM15 or CD4</p>

    <p>          Payan, Donald G and Jenkins, Yonchu</p>

    <p>          PATENT:  WO <b>2005047476</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 109 pp.</p>

    <p>          ASSIGNEE:  (Rigel Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    
    <p>13.   47882   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          Aminopyrimidinimino isatin analogues: Design and synthesis of novel non-nucleoside HIV-1 reverse transcriptase inhibitors with broad-spectrum anti-microbial properties</p>

    <p>          Sriram, Dharmarajan, Bal, Tanushree Ratan, and Yogeeswari, Perumal</p>

    <p>          Medicinal Chemistry <b>2005</b>.  1(3): 277-285</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   47883   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          Lowering the Dose of Hydroxyurea Minimizes Toxicity and Maximizes Anti-HIV Potency</p>

    <p>          Lori, Franco, Pollard, RB, Whitman, L, Bakare, N, Blick, G, Shalit, P, Foli, A, Peterson, D, Tennenberg, A, Schrader, S, Rashbaum, B, Farthing, C, Herman, D, Norris, D, Greiger, P, Frank, I, Groff, A, Lova, L, Asmuth, D, and Lisziewicz, J</p>

    <p>          AIDS Research and Human Retroviruses <b>2005</b>.  21(4): 263-272</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   47884   HIV-LS-331; PUBMED-HIV-9/19/2005</p>

    <p class="memofmt1-2">          Long-Term Inhibition of HIV-1 Infection in Primary Hematopoietic Cells by Lentiviral Vector Delivery of a Triple Combination of Anti-HIV shRNA, Anti-CCR5 Ribozyme, and a Nucleolar-Localizing TAR Decoy</p>

    <p>          Li, MJ, Kim, J, Li, S, Zaia, J, Yee, JK, Anderson, J, Akkina, R, and Rossi, JJ</p>

    <p>          Mol Ther <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115802&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115802&amp;dopt=abstract</a> </p><br />

    <p>16.   47885   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          Syntheses and anti-viral properties of N-6 substituted derivatives of L-like 4&#39;-deoxy-5&#39;-noraristeromycin and oxyamino and hydroxylamino carbanucleosides</p>

    <p>          Serbessa, Tesfaye, Roy, Atanu, Yang, Minmin, and Schneller, Stewart W</p>

    <p>          229th ACS National Meeting <b>2005</b>.  229: MEDI-341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>17.   47886   HIV-LS-331; SCIFINDER-HIV-9/15/2005</p>

    <p class="memofmt1-2">          Preparation of pyrido[1,2-a]pyrazine-1,8-dione derivatives as HIV integrase inhibitors</p>

    <p>          Miyazaki, Susumu, Katoh, Susumu, Adachi, Kaoru, Isoshima, Hirotaka, Kobayashi, Satoru, Matsuzaki, Yuji, Watanabe, Wataru, Yamataka, Kazunobu, Kiyonari, Shinichi, and Wamaki, Shuichi</p>

    <p>          PATENT:  WO <b>2005016927</b>  ISSUE DATE:  20050224</p>

    <p>          APPLICATION: 2004  PP: 355 pp.</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <br />

    <p>18.   47887   HIV-LS-331; WOS-HIV-9/11/2005</p>

    <p class="memofmt1-2">          Darunavir - Anti-HIV agent HIV protease inhibitor</p>

    <p>          Sorbera, LA, Castaner, J, and Bayes, M</p>

    <p>          DRUGS OF THE FUTURE <b>2005</b>.  30(5): 441-449, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231410500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231410500001</a> </p><br />

    <p>19.   47888   HIV-LS-331; WOS-HIV-9/11/2005</p>

    <p class="memofmt1-2">          Etravirine - Anti-HIV agent reverse transcriptase inhibitor</p>

    <p>          Davies, SL, Castaner, J, Silvestre, JS, and Bayes, M</p>

    <p>          DRUGS OF THE FUTURE <b>2005</b>.  30(5): 462-468, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231410500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231410500003</a> </p><br />

    <p>20.   47889   HIV-LS-331; WOS-HIV-9/11/2005</p>

    <p class="memofmt1-2">          Maraviroc - Anti-HIV agent viral entry inhibitor chemokine CCR5 antagonist</p>

    <p>          Ginesta, JB, Castaner, J, Bozzo, J, and Bayes, M</p>

    <p>          DRUGS OF THE FUTURE <b>2005</b>.  30(5): 469-477, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231410500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231410500004</a> </p><br />

    <p>21.   47890   HIV-LS-331; WOS-HIV-9/18/2005</p>

    <p class="memofmt1-2">          L-carnitine, immunomodulation, and human immunodeficiency virus (HIV)-related disorders</p>

    <p>          Alesci, S, Gerschenson, M, and Ilias, L</p>

    <p>          MONATSHEFTE FUR CHEMIE <b>2005</b>.  136(8): 1493-1500, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231538500019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231538500019</a> </p><br />

    <p>22.   47891   HIV-LS-331; WOS-HIV-9/18/2005</p>

    <p class="memofmt1-2">          The synthetic peptide derived from the NH2-terminal extracellular region of an orphan G protein-coupled receptor, GPR1, preferentially inhibits infection of X4 HIV-1</p>

    <p>          Jinno-Oue, A, Shimizu, N, Soda, Y, Tanaka, A, Ohtsuki, T, Kurosaki, D, Suzuki, Y, and Hoshino, H</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(35): 30924-30934, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231487800034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231487800034</a> </p><br />

    <p>23.   47892   HIV-LS-331; WOS-HIV-9/18/2005</p>

    <p class="memofmt1-2">          New HIV-1 replication inhibitors of the styryquinoline class bearing aroyl/acyl groups at the C-7 position: Synthesis and biological activity</p>

    <p>          Normand-Bayle, M, Benard, C, Zouhiri, F, Mouscadet, JF, Leh, H, Thomas, CM, Mbemba, G, Desmaele, D, and d&#39;Angelo, J</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(18): 4019-4022, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231493400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231493400007</a> </p><br />

    <p>24.   47893   HIV-LS-331; WOS-HIV-9/18/2005</p>

    <p class="memofmt1-2">          Effect of change in nucleoside structure on the activation and antiviral activity of phosphoramidate derivatives</p>

    <p>          Venkatachalam, TK, Samuel, P, Qazi, S, and Uckun, FM</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(18): 5408-5423, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231493800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231493800008</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
