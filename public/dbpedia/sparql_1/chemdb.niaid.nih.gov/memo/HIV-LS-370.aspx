

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-370.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yMmynWSQBJrekRGfoTLtsKerYmqDNh6jpYrnCrz0FgXNqxJ3vJSMIF+hVR/dvhXTBEy2ReDPE+mDnB9HRMPzKw89/+SUFGg64NjIal39V+2gld+QiBUONozBBfA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="965E6799" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-370-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60418   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Study of the Effects on DNA of Two Novel Nucleoside Derivatives Synthesized as Potential Anti-HIV Agents</p>

    <p>          Maslat, AO, Bkhaitan, M, and Sheikha, GA</p>

    <p>          Drug Chem Toxicol <b>2007</b>.  30(1): 41-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17364863&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17364863&amp;dopt=abstract</a> </p><br />

    <p>2.     60419   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Nodulisporol and Nodulisporone, novel specific inhibitors of human DNA polymerase lambda from a fungus, Nodulisporium sp</p>

    <p>          Kamisuki, S, Ishimaru, C, Onoda, K, Kuriyama, I, Ida, N, Sugawara, F, Yoshida, H, and Mizushina, Y</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17363259&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17363259&amp;dopt=abstract</a> </p><br />

    <p>3.     60420   HIV-LS-370; EMBASE-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Synthesis of 3&#39;-O-phosphonomethyl nucleosides with an adenine base moiety</p>

    <p>          Vina, Dolores, Wu, Tongfei, Renders, Marleen, Laflamme, Genevieve, and Herdewijn, Piet</p>

    <p>          Tetrahedron <b>2007</b>.  63(12): 2634-2646</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4MVVSM9-5/2/662ad5390847e444dc74c12eb073ce3c">http://www.sciencedirect.com/science/article/B6THR-4MVVSM9-5/2/662ad5390847e444dc74c12eb073ce3c</a> </p><br />

    <p>4.     60421   HIV-LS-370; EMBASE-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Exploring molecular shape analysis of styrylquinoline derivatives as HIV-1 integrase inhibitors</p>

    <p>          Leonard, JThomas and Roy, Kunal</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript:  2646</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4N859SH-1/2/01ad0dfd49c7025821b9e28cfd62bce7">http://www.sciencedirect.com/science/article/B6VKY-4N859SH-1/2/01ad0dfd49c7025821b9e28cfd62bce7</a> </p><br />

    <p>5.     60422   HIV-LS-370; EMBASE-HIV-3/19/2007</p>

    <p class="memofmt1-2">          A new lectin from the sea worm Serpula vermicularis: Isolation, characterization and anti-HIV activity</p>

    <p>          Molchanova, Valentina, Chikalovets, Irina, Chernikov, Oleg, Belogortseva, Natalia, Li, Wei, Wang, Jian-Hua, Yang, Dong-Yun Ou, Zheng, Yong-Tang, and Lukyanov, Pavel</p>

    <p>          Comparative Biochemistry and Physiology Part C: Toxicology &amp; Pharmacology <b> 2007</b>.  145(2): 184-193 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W89-4MH29B9-1/2/738d8666d3c91182a46453910a571f2d">http://www.sciencedirect.com/science/article/B6W89-4MH29B9-1/2/738d8666d3c91182a46453910a571f2d</a> </p><br />
    <br clear="all">

    <p>6.     60423   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Comparison of Phosphorylation of 4&#39;-Ethynyl 2&#39;,3&#39;-dihydro-3&#39;-deoxythymidine with Other Anti-HIV Thymidine Analogs</p>

    <p>          Hsu, CH, Hu, R, Dutschman, GE, Yang, G, Krishnan, P, Tanaka, H, Baba, M, and Cheng, YC</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17353236&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17353236&amp;dopt=abstract</a> </p><br />

    <p>7.     60424   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Synthesis of Some Novel Substituted Purine Derivatives As Potential Anticancer, Anti-HIV-1 and Antimicrobial Agents</p>

    <p>          Rida, SM, Ashour, FA, El-Hawash, SA, El-Semary, MM, and Badr, MH</p>

    <p>          Arch Pharm (Weinheim) <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17351964&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17351964&amp;dopt=abstract</a> </p><br />

    <p>8.     60425   HIV-LS-370; EMBASE-HIV-3/19/2007</p>

    <p class="memofmt1-2">          HIV-1 matrix protein: A mysterious regulator of the viral life cycle</p>

    <p>          Bukrinskaya, Alissa</p>

    <p>          Virus Research  <b>2007</b>.  124(1-2): 1-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4MS9R2P-1/2/87b706836530887475acda1d1a2fa0c8">http://www.sciencedirect.com/science/article/B6T32-4MS9R2P-1/2/87b706836530887475acda1d1a2fa0c8</a> </p><br />

    <p>9.     60426   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Synthesis and evaluation of 2-(2,6-dihalophenyl)-3-pyrimidinyl-1,3-thiazolidin-4-one analogues as anti-HIV-1 agents</p>

    <p>          Rawal, RK, Tripathi, R, Katti, SB, Pannecouque, C, and De, Clercq E</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17349793&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17349793&amp;dopt=abstract</a> </p><br />

    <p>10.   60427   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Evidence for predominance of CCR5-using HIV-1 strains during highly active antiretroviral therapy</p>

    <p>          Wang, YM, Wang, B, Dyer, WB, Lachireddy, K, Peng, NK, and Saksena, NK</p>

    <p>          Curr HIV Res <b>2007</b>.  5(2): 221-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346136&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346136&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60428   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          HIV-1 integrase inhibitors are substrates for the multidrug transporter MDR1-P-glycoprotein</p>

    <p>          Cianfriglia, M, Dupuis, ML, Molinari, A, Verdoliva, A, Costi, R, Galluzzo, CM, Andreotti, M, Cara, A, Di, Santo R, and Palmisano, L</p>

    <p>          Retrovirology <b>2007</b>.  4(1): 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17343726&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17343726&amp;dopt=abstract</a> </p><br />

    <p>12.   60429   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Flazinamide, a novel beta-carboline compound with anti-HIV actions</p>

    <p>          Wang, YH, Tang, JG, Wang, RR, Yang, LM, Dong, ZJ, Du, L, Shen, X, Liu, JK, and Zheng, YT</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.  355(4): 1091-1095</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17336271&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17336271&amp;dopt=abstract</a> </p><br />

    <p>13.   60430   HIV-LS-370; EMBASE-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Raft-tropic Antivirals: 1: Synthesis and anti-HIV-1 Evaluation of Cholesten-containing Polyanios</p>

    <p>          Egorov, Y, Serbin, A, Alikhanova, O, Burshtein, M, Lupandin, S, and Bukrinskaya, A</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1W/2/0e059fb3748444fe524efbc8ebe07a06">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1W/2/0e059fb3748444fe524efbc8ebe07a06</a> </p><br />

    <p>14.   60431   HIV-LS-370; EMBASE-HIV-3/19/2007</p>

    <p class="memofmt1-2">          The Design, Synthesis and Anti-HIV Activity of a Selected Group of 2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-Dideoxyguanosine (d4G) and 2&#39;,3&#39;-Dideoxyguanosine (ddG) `ProTide&#39; Derivatives</p>

    <p>          Mehellou, Youcef, McGuigan, Christopher, and Balzarini, Jan</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-20/2/78af1a2d94a6e08008cde570545ca7ae">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-20/2/78af1a2d94a6e08008cde570545ca7ae</a> </p><br />

    <p>15.   60432   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Single amino acid substitution in HIV-1 integrase catalytic core causes a dramatic shift in inhibitor selectivity</p>

    <p>          Al-Mawsawi, LQ, Sechi, M, and Neamati, N</p>

    <p>          FEBS Lett <b>2007</b>.  581(6): 1151-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17328897&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17328897&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60433   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication in macrophages by a heterodinucleotide of lamivudine and tenofovir</p>

    <p>          Rossi, L, Franchetti, P, Pierige, F, Cappellacci, L, Serafini, S, Balestra, E, Perno, CF, Grifantini, M, Calio, R, and Magnani, M</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17327293&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17327293&amp;dopt=abstract</a> </p><br />

    <p>17.   60434   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Slow-, Tight-Binding HIV-1 Reverse Transcriptase Non-Nucleoside Inhibitors Highly Active against Drug-Resistant Mutants</p>

    <p>          Cancio, R, Mai, A, Rotili, D, Artico, M, Sbardella, G, Clotet-Codina, I, Este, JA, Crespan, E, Zanoli, S, Hubscher, U, Spadari, S, and Maga, G</p>

    <p>          ChemMedChem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17323401&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17323401&amp;dopt=abstract</a> </p><br />

    <p>18.   60435   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV studies of 2-adamantyl-substituted thiazolidin-4-ones</p>

    <p>          Balzarini, J, Orzeszko, B, Maurin, JK, and Orzeszko, A</p>

    <p>          Eur J Med Chem  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17321639&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17321639&amp;dopt=abstract</a> </p><br />

    <p>19.   60436   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          [d4U]-butyne-[HI-236] as a non-cleavable, bifunctional NRTI/NNRTI HIV-1 reverse-transcriptase inhibitor</p>

    <p>          Hunter, R, Muhanji, CI, Hale, I, Bailey, CM, Basavapathruni, A, and Anderson, KS</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317163&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317163&amp;dopt=abstract</a> </p><br />

    <p>20.   60437   HIV-LS-370; PUBMED-HIV-3/19/2007</p>

    <p class="memofmt1-2">          Tetranorclerodanes and clerodane-type diterpene glycosides from Dicranopteris dichotoma</p>

    <p>          Li, XL, Yang, LM, Zhao, Y, Wang, RR, Xu, G, Zheng, YT, Tu, L, Peng, LY, Cheng, X, and Zhao, QS </p>

    <p>          J Nat Prod <b>2007</b>.  70(2): 265-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17315963&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17315963&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   60438   HIV-LS-370; WOS-HIV-3/12/2007</p>

    <p class="memofmt1-2">          Apricitabine. Anti-HIV agent, nucleoside reverse transcriptase inhibitor</p>

    <p>          Revill, P and Serradell, N</p>

    <p>          DRUGS OF THE FUTURE <b>2006</b>.  31(12): 1035-1041, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244115300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244115300001</a> </p><br />

    <p>22.   60439   HIV-LS-370; WOS-HIV-3/12/2007</p>

    <p class="memofmt1-2">          Synthesis of glycyrrhizic acid conjugates containing L-lysine</p>

    <p>          Baltina, LA, Kondratenko, RM, Baltina, LA, Plyasunova, OA, Galin, FZ, and Tolstikov, GA</p>

    <p>          CHEMISTRY OF NATURAL COMPOUNDS <b>2006</b>.  42(5): 543-548, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244222300012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244222300012</a> </p><br />

    <p>23.   60440   HIV-LS-370; WOS-HIV-3/18/2007</p>

    <p class="memofmt1-2">          Human immunodeficiency virus (HIV) type 1 Vpr induces differential regulation of T cell costimulatory molecules: Direct effect of Vpr on T cell activation and immune function</p>

    <p>          Venkatachari, NJ, Majumder, B, and Ayyavoo, V</p>

    <p>          VIROLOGY <b>2007</b>.  358(2): 347-356, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244272500011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244272500011</a> </p><br />

    <p>24.   60441   HIV-LS-370; WOS-HIV-3/18/2007</p>

    <p class="memofmt1-2">          Effect of chloroquine on reducing HIV-I replication in vitro and the DC-SIGN mediated transfer of virus to CD4+ T-lymphocytes</p>

    <p>          Naarding, MA, Baan, E, Pollakis, G, and Paxton, WA</p>

    <p>          RETROVIROLOGY <b>2007</b>.  4: 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244367100001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244367100001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
