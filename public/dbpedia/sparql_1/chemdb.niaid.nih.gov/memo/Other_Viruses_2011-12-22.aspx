

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-12-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RmxHsLfxIULmX7LO+gKRnckWxMrFMPSCsBZJdf6KwgrqBbsYoMl7YYtKOHj5J/RURcGwOpPlH+YFM7lXwFuDwpGOlawEg/rqlrTjDDHGXG7taoWnY1crY1J10S4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="76ABFB7B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">December 9 - December 22, 2011</h1>

    <p> </p>

    <p class="memofmt2-2">Hepatitis A virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22156376">Functional Binding of Hexanucleotides to 3C Protease of Hepatitis A Virus.</a> Blaum, B.S., W. Wunsche, A.J. Benie, Y. Kusov, H. Peters, V. Gauss-Muller, T. Peters, and G. Sczakiel, Nucleic Acids Research, 2011. <b>[Epub ahead of print]</b>; PMID[22156376].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22183951">A Cell-permeable Hairpin Peptide Inhibits Hepatitis C Viral Nonstructural Protein 5A Mediated Translation and Virus Production.</a> Khachatoorian, R., V. Arumugaswami, P. Ruchala, S. Raychaudhuri, E.M. Maloney, E. Miao, A. Dasgupta, and S.W. French, Hepatology (Baltimore, Md.), 2011. <b>[Epub ahead of print]</b>; PMID[22183951].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22178556">Novel Substituted Pyrimidines as HCV Replication (replicase) Inhibitors.</a> Kwong, C.D., J.L. Clark, A.T. Fowler, F. Geng, H.S. Kezar, 3rd, A. Roychowdhury, R.C. Reynolds, J.A. Maddry, S. Ananthan, J.A. Secrist, 3rd, N.Y. Shih, J.J. Piwinski, C. Li, B. Feld, H.C. Huang, X. Tong, F. George Njoroge, and A. Arasappan, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22178556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22163194">Advancement into the Arctic Region for Bioactive Sponge Secondary Metabolites.</a> Abbas, S., M. Kelly, J. Bowling, J. Sims, A. Waters, and M. Hamann, Marine drugs, 2011. 9(11): p. 2423-2437; PMID[22163194].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22160684">A Macrocyclic HCV NS3/4A Protease Inhibitor Interacts with Protease and Helicase Residues in the Complex with its Full-length Target.</a> Schiering, N., A. D&#39;Arcy, F. Villard, O. Simic, M. Kamke, G. Monnet, U. Hassiepen, D.I. Svergun, R. Pulfer, J. Eder, P. Raman, and U. Bodendorf, Proceedings of the National Academy of Sciences of the United States of America, 2011. <b>[Epub ahead of print]</b>; PMID[22160684].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22155837">Pharmacodynamic Analysis of a Serine Protease Inhibitor (MK-4519) on Hepatitis C Virus Using a Novel in Vitro Pharmacodynamic System.</a> Brown, A.N., J.J. McSharry, J.R. Adams, R. Kulawy, R.J. Barnard, W. Newhard, A. Corbin, D.J. Hazuda, A. Louie, and G.L. Drusano, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22155837].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22148957">Structure-Activity-Relationship Development and Discovery of Potent Indole-Based Inhibitors of the Hepatitis C Virus NS5B Polymerase</a>. Chen, K.X., B. Vibulbhan, W. Yang, M. Sannigrahi, F. Velazquez, T.Y. Chan, S. Venkatraman, G.N. Anilkumar, Q. Zeng, F. Bennet, Y. Jiang, C.A. Lesburg, J. Duca, P. Pinto, S. Gavalas, Y. Huang, W. Wu, O. Selyutin, S. Agrawal, B. Feld, H.C. Huang, C. Li, K.C. Cheng, N.Y. Shih, J.A. Kozlowski, S.B. Rosenblum, and F.G. Njoroge, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22148957].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes Simplex Virus 2</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22173507">The anti-HSV-2 Effect of Alumen: In Vitro and in Vivo Experimental Studies.</a> Hong, L., X. Xu, L. Chen, B. Li, D. Wu, M. Hu, Q. Sun, X. Zhu, W. Wu, S. Hong, W. Ding, J. Min, and Q. Xu, Journal of Huazhong University of Science and Technology, 2011. 31(6): p. 828-833; PMID[22173507].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22155691">Efficacy of ASP2151, a Helicase-primase Inhibitor, against Thymidine Kinase-deficient Herpes Simplex Virus Type 2 Infection in Vitro and in Vivo.</a> Himaki, T., Y. Masui, K. Chono, T. Daikoku, M. Takemoto, B. Haixia, T. Okuda, H. Suzuki, and K. Shiraki, Antiviral Research, 2011. <b>[Epub ahead of print]</b>; PMID[22155691].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">SIV</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22161560">Suppression of Immunodeficiency Virus-Associated Neural Damage by the p75 Neurotrophin Receptor Ligand, LM11A-31, in an In Vitro Feline Model</a>. Meeker, R.B., W. Poulton, W.H. Feng, L. Hudson, and F.M. Longo, Journal of Neuroimmune Pharmacology : The Official Journal of the Society on NeuroImmune Pharmacology, 2011. <b>[Epub ahead of print]</b>; PMID[22161560].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296752300013">Quinoline Tricyclic Derivatives. Design, Synthesis and Evaluation of the Antiviral activity of Three New Classes of RNA-dependent RNA Polymerase Inhibitors.</a> Carta, A., I. Briguglio, S. Piras, P. Corona, G. Boatto, M. Nieddu, P. Giunchedi, M.E. Marongiu, G. Giliberti, F. Iuliano, S. Blois, C. Ibba, B. Busonera, and P. La Colla, Bioorganic &amp; Medicinal Chemistry, 2011. 19(23): p. 7070-7084; ISI[000296752300013].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297294300004">Inhibition of Hepatitis C Virus Replication Through Adenosine Monophosphate-activated Protein Kinase-dependent and -independent Pathways.</a> Nakashima, K., K. Takeuchi, K. Chihara, H. Hotta, and K. Sada, Microbiology and Immunology, 2011. 55(11): p. 774-782; ISI[000297294300004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297189900004">Effects of New Quinizarin Derivatives on Both HCV NS5B RNA Polymerase and HIV-1 Reverse Transcriptase Associated Ribonuclease H Activities</a>.Tramontano, E., T. Kharlamova, L. Zinzula, and F. Esposito, Journal of Chemotherapy, 2011. 23(5): p. 273-276; ISI[000297189900004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1209-122211.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
