

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-12-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ubUWHswrJn+qXDDtZZbMh/K15XUMe/+0w6iNDwEKDnQb6oG0LDuuiI8fMtuZUf0HMzRFHGT3q3b73tmA4U4YUTp7xIiuk4T36UAyLBoQBSvKWkctbcwaa0zMRi4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D54C7916" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">November 25 - December 8, 2011</h1>

    <p> </p>

    <p class="memofmt2-2">Hepatitis B Virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22131154">Lignans with anti-Hepatitis B Virus Activities from Phyllanthus niruri L.</a> Wei, W., X. Li, K. Wang, Z. Zheng, and M. Zhou, Phytotherapy Research : PTR, 2011. <b>[Epub ahead of print]</b>; PMID[22131154].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22127069">In Vitro anti-Hepatitis B and SARS Virus Activities of a Titanium-substituted-heteropolytungstate.</a> Qi, Y.F., H. Zhang, J. Wang, Y. Jiang, J. Li, Y. Yuan, S. Zhang, K. Xu, Y. Li, J. Niu, and E. Wang, Antiviral Research, 2011. <b>[Epub ahead of print]</b>; PMID[22127069].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1125-120811.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22119470">3-Heterocyclyl Quinolone Inhibitors of the HCV NS5B Polymerase.</a> Kumar, D.V., R. Rai, K.A. Brameld, J. Riggs, J.R. Somoza, R. Rajagopalan, J.W. Janc, Y.M. Xia, T.L. Ton, H. Hu, I. Lehoux, J.D. Ho, W.B. Young, B. Hart, and M.J. Green, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22119470].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22118679">Self-assembling Peptide Nanotubes with Antiviral Activity against Hepatitis C Virus.</a> Montero, A., P. Gastaminza, M. Law, G. Cheng, F.V. Chisari, and M.R. Ghadiri, Chemistry &amp; Biology, 2011. 18(11): p. 1453-1462; PMID[22118679].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22105803">(-)-Epigallocatechin-3-gallate is a New Inhibitor of Hepatitis C Virus Entry.</a> Calland, N., A. Albecka, S. Belouzard, C. Wychowski, G. Duverlie, V. Descamps, D. Hober, J. Dubuisson, Y. Rouille, and K. Seron, Hepatology (Baltimore, Md.), 2011. <b>[Epub ahead of print]</b>; PMID[22105803].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22104146">Novel HCV NS5B Polymerase Inhibitors: Discovery of Indole C2 acyl sulfonamides.</a> Anilkumar, G.N., O. Selyutin, S.B. Rosenblum, Q. Zeng, Y. Jiang, T.Y. Chan, H. Pu, L. Wang, F. Bennett, K.X. Chen, C.A. Lesburg, J. Duca, S. Gavalas, Y. Huang, P. Pinto, M. Sannigrahi, F. Velazquez, S. Venkatraman, B. Vibulbhan, S. Agrawal, E. Ferrari, C.K. Jiang, H.C. Huang, N.Y. Shih, F. George Njoroge, and J.A. Kozlowski, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22104146].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1125-120811.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22131951">Capilloquinol: A Novel Farnesyl Quinol from the Dongsha Atoll Soft Coral Sinularia capillosa.</a> Cheng, S.Y., K.J. Huang, S.K. Wang, and C.Y. Duh, Marine Drugs, 2011. 9(9): p. 1469-1476; PMID[22131951].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1125-120811.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296708100022">Hepatitis C Virus Nucleotide Inhibitors PSI-352938 and PSI-353661 Exhibit a Novel Mechanism of Resistance Requiring Multiple Mutations within Replicon RNA.</a> Lam, A.M., C. Espiritu, S. Bansal, H.M.M. Steuer, V. Zennou, M.J. Otto, and P.A. Furman, Journal of Virology, 2011. 85(23): p. 12334-12342; ISI[000296708100022].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296717600034">Synthesis and Antiviral Activity of 2 &#39;-Deoxy-2 &#39;-fluoro-2 &#39;-C-methyl-7-deazapurine nucleosides, Their Phosphoramidate Prodrugs and 5 &#39;-Triphosphates.</a> Shi, J.X., L.H. Zhou, H.W. Zhang, T.R. McBrayer, M.A. Detorio, M. Johns, L. Bassit, M.H. Powdrill, T. Whitaker, S.J. Coats, M. Gotte, and R.F. Schinazi, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(23): p. 7094-7098; ISI[000296717600034].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296535900040">A New Method for Induced Fit Docking (GENIUS) and Its Application to Virtual Screening of Novel HCV NS3-4A Protease Inhibitors.</a> Takaya, D., A. Yamashita, K. Kamijo, J. Gomi, M. Ito, S. Maekawa, N. Enomoto, N. Sakamoto, Y. Watanabe, R. Arai, H. Umeyama, T. Honma, T. Matsumoto, and S. Yokoyama, Bioorganic &amp; Medicinal Chemistry, 2011. 19(22): p. 6892-6905; ISI[000296535900040].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296867400001">Expression of an IRF-3 Fusion Protein and Mouse Estrogen Receptor, Inhibits hepatitis C Viral Replication in RIG-I-deficient Huh 7.5 Cells.</a> Yao, L.Y., X.B. Yan, H.J. Dong, D.R. Nelson, C. Liu, and X.Y. Li, Virology Journal, 2011. 8: p. 445; ISI[000296867400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1125-120811.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
