

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-05-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bZjB75QZqmrWgIOZrcfDAYLfF+X5Jceas5R5lNoSZXft11M9IYeMkTNlNWdk2rx7g0p3lRjlaF+N3umRyOEKXDSrPcc0yOyhAyceLwFDEUaKofmKcpXU9EItL6A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B59B1BCF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 29, 2011 - May 12, 2011</h1>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Pubmed citations</p>

    <p> </p>

    <p class="NoSpacing">1. Armstrong, K.L., T.H. Lee, and M. Essex, <a href="http://www.ncbi.nlm.nih.gov/pubmed/21402856">Replicative Fitness Costs of Nonnucleoside Reverse Transcriptase Inhibitor Drug Resistance Mutations on HIV Subtype C</a>. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 2146-53; PMID[21402856].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21561135">Stelleralides a-C, Novel Potent Anti-HIV Daphnane-Type Diterpenoids from Stellera Chamaejasme L.</a> Asada, Y., A. Sukemori, T. Watanabe, K.J. Malla, T. Yoshikawa, W. Li, K. Koike, C.H. Chen, T. Akiyama, K. Qian, K. Nakagawa-Goto, S.L. Morris-Natschke, and K.H. Lee. Organic Letters, 2011.  <b>[Epub ahead of print]</b>; PMID[21561135].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21525346">Natural Substrate Concentrations Can Modulate the Prophylactic Efficacy of Nucleotide HIV Reverse Transcriptase Inhibitors.</a> Garcia-Lerma, J.G., W. Aung, M.E. Cong, Q. Zheng, A.S. Youngpairoj, J. Mitchell, A. Holder, A. Martin, S. Kuklenyik, W. Luo, C.Y. Lin, D.L. Hanson, E. Kersh, C.P. Pau, A.S. Ray, J.F. Rooney, W.A. Lee, and W. Heneine. Journal of Virology, 2011.  <b>[Epub ahead of print]</b>; PMID[21525346].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21531716">Inhibitors of Histone Deacetylases: Correlation Between Isoform Specificity and Reactivation of HIV-1 from Latently Infected Cells.</a> Huber, K., G. Doyon, J. Plaks, E. Fyne, J.W. Mellors, and N. Sluis-Cremer. Journal of Biological Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21531716].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">5. Jain, S. and K.L. Rosenthal, <a href="http://www.ncbi.nlm.nih.gov/pubmed/21525865">The Gp41 Epitope, Qarvlavery, Is Highly Conserved and a Potent Inducer of Iga That Neutralizes HIV-1 and Inhibits Viral Transcytosis</a>. Mucosal Immunology, 2011.  <b>[Epub ahead of print]</b>; PMID[21525865].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21553876">Combination Anti-HIV Therapy with the Self-Assemblies of an Asymmetric Bolaamphiphilic Zidovudine/Didanosine Prodrug.</a> Jin, Y., R. Xin, L. Tong, L. Du, and M. Li. Molecular Pharmacology, 2011.  <b>[Epub ahead of print]</b>; PMID[21553876].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21539377">Novel 4,4-Disubstituted Piperidine-Based C-C Chemokine Receptor -5 Inhibitors with High Potency against Human Immunodeficiency Virus-1 and an Improved Human Ether-a-Go-Go Related Gene (Herg) Profile.</a> Kazmierski, W.M., D. Anderson, C. Aquino, B. Chauder, M. Duan, R. Ferris, T. Kenakin, C. Koble, D. Lang, M. McIntyre, J. Peckham, C. Watson, P. Wheelan, A. Spaltenstein, M. Wire, A. Svolto, and M. Youngman. Journal of Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21539377].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21555514">Cyclin K Inhibits HIV-1 Gene Expression and Replication by Interfering with Cdk9-Cyclin T1 Interaction in Nef Dependent Manner.</a> Khan, S.Z. and D. Mitra. Journal of Biological Chemistry, 2011.  [EPUB AHEAD OF PRINT]; PMID[21555514].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21561060">Sesquiterpene and Norsesquiterpene Derivatives from Sanicula Lamelligera and Their Biological Evaluation.</a> Li, X.S., X.J. Zhou, X.J. Zhang, J. Su, X.J. Li, Y.M. Yan, Y.T. Zheng, Y. Li, L.M. Yang, and Y.X. Cheng. Journal of Natural Products, 2011.  <b>[Epub ahead of print]</b>; PMID[21561060].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21446746">The L76v Drug Resistance Mutation Decreases the Dimer Stability and Rate of Autoprocessing of HIV-1 Protease by Reducing Internal Hydrophobic Contacts.</a> Louis, J.M., Y. Zhang, J.M. Sayer, Y.F. Wang, R.W. Harrison, and I.T. Weber. Biochemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21446746].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21534036">Dibenzocyclooctadiene Lignans from the Fruits of Schisandra Rubriflora and Their Anti-HIV-1 Activities.</a> Mu, H.X., X.S. Li, P. Fan, G.Y. Yang, J.X. Pu, H.D. Sun, Q.F. Hu, and W.L. Xiao. Journal of Asian Natural Products Research, 2011. 13(5): p. 393-399; PMID[21534036].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21534540">Tigliane-Type Diterpenoid Glycosides from Euphorbia Fischeriana.</a> Pan, L.L., P.L. Fang, X.J. Zhang, W. Ni, L. Li, L.M. Yang, C.X. Chen, Y.T. Zheng, C.T. Li, X.J. Hao, and H.Y. Liu. Journal of Natural Products, 2011.  <b>[Epub ahead of print]</b>; PMID[21534540].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21396746">Discovery of a Potent Peptidic Cyclophilin a Inhibitor Trp-Gly-Pro.</a> Pang, X., M. Zhang, L. Zhou, F. Xie, H. Lu, W. He, S. Jiang, L. Yu, and X. Zhang. European Journal of Medicinal Chemistry, 2011. 46(5): p. 1701-1705; PMID[21396746].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21548842">Therapeutic Compounds: Patent Evaluation of Wo2011011652a1.</a> Supuran, C.T. Expert Opinion on Therapeutic Patents, 2011.  <b>[Epub ahead of print]</b>; PMID[21548842].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21343443">4&#39;-C-Methyl-2&#39;-Deoxyadenosine and 4&#39;-C-Ethyl-2&#39;-Deoxyadenosine Inhibit HIV-1 Replication.</a> Vu, B.C., P.L. Boyer, M.A. Siddiqui, V.E. Marquez, and S.H. Hughes. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 2379-2389; PMID[21343443].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21539734">Antiviral Activity of Alpha-Helical Stapled Peptides Designed from the HIV-1 Capsid Dimerization Domain.</a> Zhang, H., F. Curreli, X. Zhang, S. Bhattacharya, A.A. Waheed, A. Cooper, D. Cowburn, E.O. Freed, and A.K. Debnath. Retrovirology, 2011. 8(1): p. 28; PMID[21539734].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288890800008">Chemoenzymatic Total Synthesis of Potent HIV Rnase H Inhibitor (-)-1,3,4,5-Tetragalloylapiitol.</a> Batwal, R.U., R.M. Patel, and N.P. Argade. Tetrahedron-Asymmetry, 2011. 22(2): p. 173-177; ISI[000288890800008].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289300000008">Isolation and Structure of a Novel Peptide Inhibitor of HIV-1 Integrase from Marine Polychaetes.</a> Elyakova, L.A., B.V. Vaskovsky, N.I. Khoroshilova, S.I. Vantseva, and Y.Y. Agapkina. Russian Journal of Bioorganic Chemistry, 2011. 37(2): p. 207-216; ISI[000289300000008].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289322500012">Anti-HIV and Immunomodulation Activities of Cacao Mass Lignin-Carbohydrate Complex.</a> Sakagami, H., M. Kawano, M.M. Thet, K. Hashimoto, K. Satoh, T. Kanamoto, S. Terakubo, H. Nakashima, Y. Haishima, Y. Maeda, and K. Sakurai. In Vivo, 2011. 25(2): p. 229-236; ISI[000289322500012].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289334500006">Resistance of a Human Immunodeficiency Virus Type 1 Isolate to a Small Molecule Ccr5 Inhibitor Can Involve Sequence Changes in Both Gp120 and Gp41.</a> Anastasopoulou, C.G., T.J. Ketas, R.S. Depetris, A.M. Thomas, P.J. Klasse, and J.P. Moore. Virology, 2011. 413(1): p. 47-59; ISI[000289334500006].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289590300005">Exploration of Novel Medicinal Leads by Use of Natural Products Inhibiting Nuclear Export of Proteins as Scaffolds.</a> Tamura, S. and N. Murakami. Journal of Synthetic Organic Chemistry Japan, 2011. 69(4): p. 393-402; ISI[000289590300005].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289613200009">Design, Synthesis, and X-Ray Structure of Substituted Bis-Tetrahydrofuran (Bis-Thf)-Derived Potent HIV-1 Protease Inhibitors.</a> Ghosh, A.K., C.D. Martyr, M. Steffey, Y.F. Wang, J. Agniswamy, M. Amano, I.T. Weber, and H. Mitsuya. Acs Medicinal Chemistry Letters, 2011. 2(4): p. 298-302; ISI[000289613200009].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289618600025">HIV-1 Escape from the Ccr5 Antagonist Maraviroc Associated with an Altered and Less-Efficient Mechanism of Gp120-Ccr5 Engagement That Attenuates Macrophage Tropism.</a> Roche, M., M.R. Jakobsen, J. Sterjovski, A. Ellett, F. Posta, B. Lee, B. Jubb, M. Westby, S.R. Lewin, P.A. Ramsland, M.J. Churchill, and P.R. Gorry. Journal of Virology, 2011. 85(9): p. 4330-4342; ISI[000289618600025].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289618600057">Consistent Inhibition of Hiv-1 Replication in Cd4(+) T Cells by Acyclovir without Detection of Human Herpesviruses.</a> McMahon, M.A., T.L. Parsons, L. Shen, J.D. Siliciano, and R.F. Siliciano. Journal of Virology, 2011. 85(9): p. 4618-4622; ISI[000289618600057].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289627800009">Alizarine Derivatives as New Dual Inhibitors of the HIV-1 Reverse Transcriptase-Associated DNA Polymerase and Rnase H Activities Effective Also on the Rnase H Activity of Non-Nucleoside Resistant Reverse Transcriptases.</a> Esposito, F., T. Kharlamova, S. Distinto, L. Zinzula, Y.C. Cheng, G. Dutschman, G. Floris, P. Markt, A. Corona, and E. Tramontano. FEBS Journal, 2011. 278(9): p. 1444-1457; ISI[000289627800009].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289652900001">Antiviral Propierties of 5,5 &#39;-Dithiobis-2-Nitrobenzoic Acid and Bacitracin against T-Tropic Human Immunodeficiency Virus Type 1.</a> Lara, H.H., L. Ixtepan-Turrent, E.N. Garza-Trevino, S.M. Flores-Tevino, G. Borkow, and C. Rodriguez-Padilla. Virology Journal, 2011. 8; ISI[000289652900001].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289655100033">Synthesis, Biological Evaluation and Molecular Modeling Studies of Quinolonyl Diketo Acid Derivatives: New Structural Insight into the HIV-1 Integrase Inhibition.</a> Vandurm, P., A. Guiguen, C. Cauvin, B. Georges, K. Le Van, C. Michaux, C. Cardona, G. Mbemba, J.F. Mouscadet, L. Hevesi, C. Van Lint, and J. Wouters. European Journal of Medicinal Chemistry, 2011. 46(5): p. 1749-1756; ISI[000289655100033].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289697800001">Synopsis of Some Recent Tactical Application of Bioisosteres in Drug Design.</a> Meanwell, N.A. Journal of Medicinal Chemistry, 2011. 54(8): p. 2529-2591; ISI[000289697800001].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289730600006">Novel HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors: A Patent Review (2005-2010).</a> Zhan, P. and X.Y. Liu. Expert Opinion on Therapeutic Patents, 2011. 21(5): p. 717-796; ISI[000289730600006].</p>

    <p class="NoSpacing"><b>[WOS]</b>, HIV_0429-051311.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
