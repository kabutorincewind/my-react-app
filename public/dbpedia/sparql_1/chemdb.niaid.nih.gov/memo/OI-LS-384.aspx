

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-384.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MUFZgjEDJLg+jE2eLfFEa9hTymxCuBSy7BDys7+loDirnmct/t0CIVBoGg5esVdUZs753iQJ7jxm9VUMAsJSbRLmq9z7TuEStqyTCoi7yk9F4z0VULK3Iz1l3i0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A8E7FCC4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH OI-LS-384-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p class="memofmt1-1"> </p>

    <p class="NoSpacing">1.         61796   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">An atom efficient, solvent-free, green synthesis and antimycobacterial evaluation of 2-amino-6-methyl-4-aryl-8-[(E)-arylmethylidene]-5,6,7,8-tetrahydro-4H-pyrano[3,2- c]pyridine-3-carbonitrilesKumar, RR, Perumal, S, Senthilkumar, P, Yogeeswari, P, and Sriram, D</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17933535&amp;dopt=abstract</p>

    <p>2.             61800   OI-LS-384; SCIFINDER-OI-10/24/2007</p>

    <p class="memofmt1-2">Synthesis of some 4-arylidenamino-4H-1,2,4-triazole-3-thiols and their antituberculosis activity</p>

    <p>Oezdemir, Ahmet, Turan-Zitouni, Gulhan, Kaplancikli, Zafer Asim, and Chevallet, Pierre</p>

    <p>J. Enzyme Inhib. Med. Chem. <b>2007</b>.  22(4): 511-516</p>

    <p> </p>

    <p>3.             61801   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">Screening of Terpenes and Derivatives for Antimycobacterial Activity; Identification of Geranylgeraniol and Geranylgeranyl Acetate as Potent Inhibitors of Mycobacterium tuberculosis in vitro</p>

    <p>Vik, A, James, A, and Gundersen, LL</p>

    <p>Planta Med <b>2007</b>.</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17924309&amp;dopt=abstract</p>

    <p>4.             61805   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">Design, synthesis and evaluation of peptide inhibitors of Mycobacterium tuberculosis ribonucleotide reductase</p>

    <p>Nurbo, J, Roos, AK, Muthas, D, Wahlstrom, E, Ericsson, DJ, Lundstedt, T, Unge, T, and Karlen, A</p>

    <p>J Pept Sci <b>2007</b>.</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17918768&amp;dopt=abstract</p>

    <p>5.             61807   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">Irreversible Inhibition of the Mycobacterium tuberculosis beta-Lactamase by Clavulanate</p>

    <p>Hugonnet, JE and Blanchard, JS</p>

    <p>Biochemistry <b>2007</b>.</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17915954&amp;dopt=abstract</p>

    <p>6.             61813   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">Synthesis, characterization and antimycobacterial activity of ag(i)-aspartame, ag(i)-saccharin and ag(i)-cyclamate complexes</p>

    <p>Cavicchioli, M, Leite, CQ, Sato, DN, and Massabni, AC</p>

    <p>Arch Pharm (Weinheim) <b>2007</b>.  340: 538-42</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17912678&amp;dopt=abstract</p>

    <p>7.             61815   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">Rational Design of 5&#39;-Thiourea-Substituted alpha-Thymidine Analogues as Thymidine Monophosphate Kinase Inhibitors Capable of Inhibiting Mycobacterial Growth</p>

    <p>Daele, IV, Munier-Lehmann, H, Froeyen, M, Balzarini, J, and Calenbergh, SV</p>

    <p>J Med Chem <b>2007</b>.              </p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17910427&amp;dopt=abstract</p>

    <p>8.             61825   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">Flavonoid inhibitors as novel antimycobacterial agents targeting Rv0636, a putative dehydratase enzyme involved in Mycobacterium tuberculosis fatty acid synthase II</p>

    <p>Brown, AK, Papaemmanouil, A, Bhowruth, V, Bhatt, A, Dover, LG, and Besra, GS</p>

    <p>Microbiology <b>2007</b>.  153: 3314-22</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17906131&amp;dopt=abstract</p>

    <p>9.             61831   OI-LS-384; PUBMED-OI-10/18/2007</p>

    <p class="memofmt1-2">Synthesis and Antifungal Activities of Novel 2-Aminotetralin Derivatives</p>

    <p>Yao, B, Ji, H, Cao, Y, Zhou, Y, Zhu, J, Lu, J, Li, Y, Chen, J, Zheng, C, Jiang, Y, Liang, R, and Tang, H</p>

    <p>J Med Chem <b>2007</b>.</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17900179&amp;dopt=abstract</p>

    <p>10.           61839   OI-LS-384; SCIFINDER-OI-10/24/2007</p>

    <p class="memofmt1-2">Synthesis, antifungal and antimycobacterial activities of new bis-imidazole derivatives, and prediction of their binding to P45014DM by molecular docking and MM/PBSA method</p>

    <p>Zampieri, Daniele, Mamolo, Maria Grazia, Vio, Luciano, Banfi, Elena, Scialino, Giuditta, Fermeglia, Maurizio, Ferrone, Marco, and Pricl, Sabrina</p>

    <p>Bioorg. Med. Chem. <b>2007</b>.  15(23): 7444-7458</p>

    <p> </p>

    <p>11.           61847   OI-LS-384; SCIFINDER-OI-10/24/2007</p>

    <p class="memofmt1-2">Treatment of viral diseases by 1,3,5-triazine nucleoside and nucleotide analogs, and preparation thereof</p>

    <p>Daifuku, Richard, Gall, Alexander, and Sergueev, Dmitri</p>

    <p>PATENT:  US <b>2007207973</b>  ISSUE DATE:  20070906</p>

    <p>APPLICATION: 2006-26869  PP: 55pp., Cont.-in-part of U.S. Ser. No. 670,915.</p>

    <p>ASSIGNEE:  (Koronis Pharmaceuticals, Inc. USA</p>

    <p> </p>

    <p>12.           61873   OI-LS-384; SCIFINDER-OI-10/24/2007</p>

    <p class="memofmt1-2">Metal-based antibacterial and antifungal amino acid derived Schiff bases: their synthesis, characterization and in vitro biological activity</p>

    <p>Chohan, Zahid H, Arif, M, and Sarfraz, M</p>

    <p>Appl. Organomet. Chem. <b>2007</b>.  21(4): 294-302              </p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
