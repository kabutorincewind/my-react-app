

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-05-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="veqVhhb1RfpvPi9CFKeJvgV7gMWdb832/GW+TJxCgn8c649gXw8GmZVhAL+KiNvlbSs9jVMD76Ts4cW3cAjv2uhVYm0YUFIRZiFnBI2vfxF7ZKivfbpqklMdhP8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DF9323D2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">May 13 - May 26, 2011</h1>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21570424">Early Inhibitors of Human Cytomegalovirus: State-of-Art and Therapeutic Perspectives</a>. Mercorelli, B., D. Lembo, G. Palu, and A. Loregian. Pharmacology &amp; Therapeutics, 2011. <b>[Epub ahead of print]</b>; PMID[21570424].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21566148">Phosphonoformic Acid Inhibits Viral Replication by Trapping the Closed Form of the DNA Polymerase</a>. Zahn, K.E., E.P. Tchesnokov, M. Gotte, and S. Doublie. The Journal of Biological Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21566148].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21565516">New Prodrugs of Adefovir and Cidofovir</a>. Tichy, T., G. Andrei, M. Dracinsky, A. Holy, J. Balzarini, R. Snoeck, and M. Krecmerova. Bioorganic &amp; Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21565516].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>

    
    <p class="memofmt2-2">Hepatitis C virus</p>


    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21450465">Sar Studies on a Series of N-Benzyl-4-heteroaryl-1-(phenylsulfonyl)piperazine-2-carboxamides: Potent Inhibitors of the Polymerase Enzyme (NS5B) of the Hepatitis C Virus</a>. Gentles, R.G., M. Ding, X. Zheng, L. Chupak, M.A. Poss, B.R. Beno, L. Pelosi, M. Liu, J. Lemm, Y.K. Wang, S. Roberts, M. Gao, and J. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 3142-7; PMID[21450465].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>
    
    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21601450">Novel, Potent, and Orally Bioavailable Phosphinic Acid Inhibitors of the Hepatitis C Virus NS3 Protease</a>. Clarke, M.O., X. Chen, A. Cho, W.E.t. Delaney, E. Doerffler, M. Fardis, M. Ji, M. Mertzman, R. Pakdaman, H.J. Pyun, T. Rowe, C.Y. Yang, X.C. Sheng, and C.U. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21601450].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21600932">Activity and the Metabolic Activation Pathway of the Potent and Selective Hepatitis C Virus Pronucleotide Inhibitor Psi-353661</a>. Furman, P.A., E. Murakami, C. Niu, A.M. Lam, C. Espiritu, S. Bansal, H. Bao, T. Tolstykh, H. Micolochick Steuer, M. Keilman, V. Zennou, N. Bourne, R.L. Veselenak, W. Chang, B.S. Ross, J. Du, M.J. Otto, and M.J. Sofia. Antiviral Research, 2011. <b>[Epub ahead of print]</b>; PMID[21600932].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21599979">Inhibition of Hepatitis C Virus 3a Genotype Entry through Glanthus Nivalis Agglutinin</a>. Ashfaq, U.A., M.S. Masoud, S. Khaliq, Z. Nawaz, and S. Riazuddin. Virology Journal, 2011. 8(1): p. 248; PMID[21599979].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>
    
    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21593143">Distinct Functions of NS5A in HCV RNA Replication Uncovered by Studies with the Ns5a Inhibitor Bms-790052</a>. Fridell, R.A., D. Qiu, L. Valera, C. Wang, R.E. Rose, and M. Gao. Journal of Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21593143].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>
    
    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21576451">Discovery of Potent Hepatitis C Virus NS5A Inhibitors with Dimeric Structures</a>. Lemm, J.A., J.E. Leet, D.R. O&#39;Boyle, 2nd, J.L. Romine, X.S. Huang, D.R. Schroeder, J. Alberts, J.L. Cantone, J.H. Sun, P.T. Nower, S.W. Martin, M.H. Serrano-Wu, N.A. Meanwell, L.B. Snyder, and M. Gao. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21576451].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>
    
    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21576430">Antiviral Activity and Mode of Action of TMC647078, a Novel Nucleoside Inhibitor of the HCV NS5B Polymerase</a>. Berke, J.M., L. Vijgen, S. Lachau-Durand, M.H. Powdrill, S. Rawe, E. Sjuvarsson, S. Eriksson, M. Gotte, E. Fransen, P. Dehertogh, C. Van den Eynde, L. Leclercq, T.H. Jonckers, P. Raboisson, M. Nilsson, B. Samuelsson, A. Rosenquist, G.C. Fanning, and T.I. Lin. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21576430].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21569385">Antiviral Activity of Acacia Nilotica against Hepatitis C Virus in Liver Infected Cells</a>. Rehman, S., U.A. Ashfaq, S. Riaz, T. Javed, and S. Riazuddin. Virology Journal, 2011. 8(1): p. 220; PMID[21569385].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>
    
    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21568888">Ligand and Structure Based Virtual Screening Strategies for Hit-Finding and Optimization of Hepatitis C Virus (HCV) Inhibitors</a>. Melagraki, G. and A. Afantitis. Current Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21568888].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="memofmt2-2">Herpes Simplex virus</p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21596749">Anti-Heparan Sulfate Peptides That Block Herpes Simplex Virus Infection in Vivo</a>. Tiwari, V., J. Liu, T. Valyi-Nagy, and D. Shukla. The Journal of biological chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21596749].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21576438">Inhibition of Herpes Simplex Virus Type 1 and 2 Infection by Peptide-Derivatized Dendrimers</a>. Luganini, A., S.F. Nicoletto, L. Pizzuto, G. Pirri, A. Giuliani, S. Landolfo, and G. Gribaudo. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21576438].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21573158">HSV Neutralization by the Microbicidal Candidate C5a</a>. de Witte, L., M.D. Bobardt, U. Chatterji, F.B. van Loenen, G.M. Verjans, T.B. Geijtenbeek, and P.A. Gallay. Plos One, 2011. 6(5): p. e18917; PMID[21573158].</p>
    <p class="NoSpacing"><b>[PubMed]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>


    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289859700014">Two New Lignans and Anti-HBV Constituents from Illicium Henryi</a>. Liu, J.F., Z.Y. Jiang, C.A. Geng, Q. Zhang, Y. Shi, Y.B. Ma, X.M. Zhang, and J.J. Chen. Chemistry &amp; Biodiversity, 2011. 8(4): p. 692-698; PMID[ISI:000289859700014].</p>
    <p class="NoSpacing"><b>[WOS]</b>. OV_0513-052611.</p>
    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290299100008">Tryptamine Derivatives as Novel Non-Nucleosidic Inhibitors against Hepatitis B Virus</a>. Qu, S.J., G.F. Wang, W.H. Duan, S.Y. Yao, J.P. Zuo, C.H. Tan, and D.Y. Zhu. Bioorganic &amp; Medicinal Chemistry, 2011. 19(10): p. 3120-3127; PMID[ISI:000290299100008].</p>
    <p class="NoSpacing"><b>[WOS]</b>. OV_0513-052611.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
