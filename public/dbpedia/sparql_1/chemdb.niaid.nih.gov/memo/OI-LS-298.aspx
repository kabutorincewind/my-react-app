

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-298.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ypt28EhXWWp8Kp30hrvUKvP9HaF04XPVnmxlMZ2m9WY+IMpXEBIe3vGRgkvxOQ8hqOsqW5RXU+tWWEioqc32syeW2gC1RlYRNsmf64uwe/QsV4r6hazthPPnTYk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B3D36D91" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-298-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43569   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Synthesis, absolute stereochemistry and molecular design of the new antifungal and antibacterial antibiotic produced by Streptomyces sp.201</p>

<p>           Boruwa, J, Kalita, B, Barua, NC, Borah, JC, Mazumder, S, Thakur, D, Gogoi, DK, and Bora, TC</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(13): 3571-3574</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CDHFTG-1/2/89df9c7e5129e56c8809cfdbf8395451">http://www.sciencedirect.com/science/article/B6TF9-4CDHFTG-1/2/89df9c7e5129e56c8809cfdbf8395451</a> </p><br />

<p>     2.    43570   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           The human immunodeficiency virus (HIV) protease inhibitor indinavir directly affects the opportunistic fungal pathogen Cryptococcus neoformans</p>

<p>           Blasi, Elisabetta, Colombari, Bruna, Francesca Orsi, Carlotta, Pinti, Marcello, Leonarda Troiano, Cossarizza, Andrea, Esposito, Roberto, Peppoloni, Samuela, Mussini, Cristina, and Neglia, Rachele</p>

<p>           FEMS Immunology and Medical Microbiology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2T-4CFV437-1/2/b73559b04110f8f95e4af2b0b6ec6aa9">http://www.sciencedirect.com/science/article/B6T2T-4CFV437-1/2/b73559b04110f8f95e4af2b0b6ec6aa9</a> </p><br />

<p>     3.    43571   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Accurate mapping of mutations of pyrazinamide-resistant Mycobacterium tuberculosis strains with a scanning-frame oligonucleotide microarray</p>

<p>           Wade, Mary Margaret, Volokhov, Dmitriy, Peredelchuk, Mike, Chizhikov, Vladimir, and Zhang, Ying</p>

<p>           Diagnostic Microbiology and Infectious Disease 2004. 49(2): 89-97</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T60-4CJC1NP-5/2/9a8113e6dfd39d1a08446af63d2cd2b3">http://www.sciencedirect.com/science/article/B6T60-4CJC1NP-5/2/9a8113e6dfd39d1a08446af63d2cd2b3</a> </p><br />

<p>     4.    43572   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Non-nucleoside inhibitors of the HCV polymerase</p>

<p>           Sarisky, RT</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15190019&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15190019&amp;dopt=abstract</a> </p><br />

<p>     5.    43573   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Microarrays for Mycobacterium tuberculosis</p>

<p>           Butcher, Philip D</p>

<p>           Tuberculosis 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WXK-4C7DD0F-1/2/a0861a97748b697448cf93a409a99d1a">http://www.sciencedirect.com/science/article/B6WXK-4C7DD0F-1/2/a0861a97748b697448cf93a409a99d1a</a> </p><br />

<p>     6.    43574   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           (+)-Bornyl Piperate, a New Monoterpene Ester from Piper aff. pedicellatum Roots</p>

<p>           Rukachaisirikul, T, Prabpai, S, Kongsaeree, P, and Suksamrarn, A</p>

<p>           Chem Pharm Bull (Tokyo) 2004. 52(6): 760-1</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187402&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187402&amp;dopt=abstract</a> </p><br />

<p>    7.     43575   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Number of Drugs to Treat Multidrug-resistant Tuberculosis</p>

<p>           Seung, KJ, Joseph, K, Hurtado, R, Rich, M, Shin, S, Furin, J, Leandre, F, Mukherjee, J, Farmer, P, and Caminero, JA</p>

<p>           Am J Respir Crit Care Med 2004. 169(12): 1336-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187016&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187016&amp;dopt=abstract</a> </p><br />

<p>     8.    43576   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Differential expression of mycothiol pathway genes: are they affected by antituberculosis drugs?</p>

<p>           Hayward, D, Wiid, I, and van Helden, P</p>

<p>           IUBMB Life 2004. 56(3): 131-138</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185746&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185746&amp;dopt=abstract</a> </p><br />

<p>     9.    43577   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Molecular Characterization of Rifampin- and Isoniazid-Resistant Mycobacterium tuberculosis Strains Isolated in Poland</p>

<p>           Sajduda, A, Brzostek, A, Poplawska, M, Augustynowicz-Kopec, E, Zwolska, Z, Niemann, S, Dziadek, J, and Hillemann, D</p>

<p>           J Clin Microbiol 2004. 42(6): 2425-31</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15184414&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15184414&amp;dopt=abstract</a> </p><br />

<p>   10.    43578   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Direct fluorometric measurement of hepatitis C virus helicase activity</p>

<p>           Boguszewska-Chachulska, AM, Krawczyk, M, Stankiewicz, A, Gozdek, A, Haenni, AL, and Strokovskaya, L</p>

<p>           FEBS Lett 2004. 567(2-3): 253-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178332&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178332&amp;dopt=abstract</a> </p><br />

<p>   11.    43579   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Synthesis and in vitro antifungal activity of 4-substituted phenylguanidinium salts</p>

<p>           Braunerova, G, Buchta, V, Silva, L, Kunes, J, and Palat, K Jr</p>

<p>           Farmaco 2004. 59(6): 443-50</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178306&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178306&amp;dopt=abstract</a> </p><br />

<p>   12.    43580   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           (1R,4S,5R)-3-Fluoro-1,4,5-trihydroxy-2-cyclohexene-1-carboxylic acid: the fluoro analogue of the enolate intermediate in the reaction catalyzed by type II dehydroquinases</p>

<p>           Frederickson, M, Roszak, AW, Coggins, JR, Lapthorn, AJ, and Abell, C</p>

<p>           Org Biomol Chem 2004. 2(11): 1592-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15162210&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15162210&amp;dopt=abstract</a> </p><br />

<p>  13.     43581   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Synthesis of a bromotyrosine-derived natural product inhibitor of mycothiol-S-conjugate amidase</p>

<p>           Fetterolf, Brandon and Bewley, Carole A</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(14): 3785-3788</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CGM744-3/2/22b750d323e43b88ac4d34f2e0d7c667">http://www.sciencedirect.com/science/article/B6TF9-4CGM744-3/2/22b750d323e43b88ac4d34f2e0d7c667</a> </p><br />

<p>   14.    43582   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p><b>           Amidine derived 1,3-diazabuta-1,3-dienes as potential antibacterial and antifungal agents</b> </p>

<p>           Bedi, Preet MS, Mahajan, Mohinder P, and Kapoor, Vijay K</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(14): 3821-3824</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CGM744-6/2/57e131828f3db6af23d6fca8bf1939f0">http://www.sciencedirect.com/science/article/B6TF9-4CGM744-6/2/57e131828f3db6af23d6fca8bf1939f0</a> </p><br />

<p>   15.    43583   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p><b>           Thiourea inhibitors of herpes viruses. Part 2: N-Benzyl-N&#39;-arylthiourea inhibitors of CMV</b> </p>

<p>           Bloom, Jonathan D, Dushin, Russell G, Curran, Kevin J, Donahue, Fran, Norton, Emily B, Terefenko, Eugene, Jones, Thomas R, Ross, Adma A, Feld, Boris, Lang, Stanley A, and DiGrandi, Martin J</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(13): 3401-3406</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CF5BWW-3/2/467e728a1d403c41ee898db05b90a2c0">http://www.sciencedirect.com/science/article/B6TF9-4CF5BWW-3/2/467e728a1d403c41ee898db05b90a2c0</a> </p><br />

<p>   16.    43584   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Pneumocystis activates human alveolar macrophage NF-kappaB signaling through mannose receptors</p>

<p>           Zhang, J, Zhu, J, Imrich, A, Cushion, M, Kinane, TB, and Koziel, H</p>

<p>           Infect Immun 2004. 72(6): 3147-60</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155616&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155616&amp;dopt=abstract</a> </p><br />

<p>   17.    43585   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Caspofungin: the first licensed antifungal drug of the novel echinocandin class</p>

<p>           DiNubile, Mark J, Motyl, Mary, and Sable, Carole A</p>

<p>           Clinical Microbiology Newsletter 2004. 26(11): 81-85</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T5D-4CG91SN-1/2/34404ca49623bc5aa947c74e61f2f861">http://www.sciencedirect.com/science/article/B6T5D-4CG91SN-1/2/34404ca49623bc5aa947c74e61f2f861</a> </p><br />

<p>   18.    43586   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Effects of voriconazole on Cryptococcus neoformans</p>

<p>           van Duin, D,  Cleare, W, Zaragoza, O, Casadevall, A, and Nosanchuk, JD</p>

<p>           Antimicrob Agents Chemother 2004. 48(6): 2014-2020</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155193&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155193&amp;dopt=abstract</a> </p><br />

<p>   19.    43587   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Inhibition of human cytomegalovirus signaling and replication by the immunosuppressant FK778</p>

<p>           Evers, David L, Wang, Xin, Huong, Shu-Mei, Andreoni, Kenneth A, and Huang, Eng-Shang</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4CGNBJ8-1/2/d49270fba8e338682b914b690533edbf">http://www.sciencedirect.com/science/article/B6T2H-4CGNBJ8-1/2/d49270fba8e338682b914b690533edbf</a> </p><br />

<p>  20.     43588   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           3,4&#39;,5-Trihydroxy-trans-stilbene (resveratrol) inhibits human cytomegalovirus replication and virus-induced cellular signaling</p>

<p>           Evers, David L, Wang, Xin, Huong, Shu-Mei, Huang, David Y, and Huang, Eng-Shang</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4CFV465-1/2/fbf7ee156d6b30960d1472d3d194c56b">http://www.sciencedirect.com/science/article/B6T2H-4CFV465-1/2/fbf7ee156d6b30960d1472d3d194c56b</a> </p><br />

<p>   21.    43589   OI-LS-298; PUBMED-OI-6/16/2004</p>

<p class="memofmt1-3">           Synthesis, antimicrobial activity and chemotherapeutic potential of inorganic derivatives of 2-(4(&#39;)-thiazolyl)benzimidazole{thiabendazole}: X-ray crystal structures of [Cu(TBZH)(2)Cl]Cl [Formula: see text] H(2)O [Formula: see text] EtOH and TBZH(2)NO(3) (TBZH=thiabendazole)</p>

<p>           Devereux, M, McCann, M, O, Shea D, Kelly, R, Egan, D, Deegan, C, Kavanagh, K, McKee, V, and Finn, G</p>

<p>           J Inorg Biochem 2004. 98(6): 1023-1031</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149811&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149811&amp;dopt=abstract</a> </p><br />

<p>   22.    43590   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           Serum amyloid P-component-mediated inhibition of the uptake of Mycobacterium tuberculosis by macrophages, in vitro</p>

<p>           Kaur, S and Singh, PP</p>

<p>           SCAND J IMMUNOL 2004. 59(5): 425-431</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221306400002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221306400002</a> </p><br />

<p>   23.    43591   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Antiviral therapies</p>

<p>           Hallenberger, Sabine</p>

<p>           Journal of Clinical Virology 2004.  30(Supplement 1): S22-S24</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4C009SK-3/2/edb1fc49cf7d309cb99e7149556cd145">http://www.sciencedirect.com/science/article/B6VJV-4C009SK-3/2/edb1fc49cf7d309cb99e7149556cd145</a> </p><br />

<p>   24.    43592   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Inhibitors of hepatitis C virus NS3.4A protease 2. Warhead SAR and optimization</p>

<p>           Perni, Robert B, Pitlik, Janos, Britt, Shawn D, Court, John J, Courtney, Lawrence F, Deininger, David D, Farmer, Luc J, Gates, Cynthia A, Harbeson, Scott L, and Levin, Rhonda B</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(6): 1441-1446</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BT8F6G-H/2/9bbd356c0a66e302e25d91dffc807e19">http://www.sciencedirect.com/science/article/B6TF9-4BT8F6G-H/2/9bbd356c0a66e302e25d91dffc807e19</a> </p><br />

<p>   25.    43593   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           Mycobacterium tuberculosis inhibits macrophage responses to IFN-gamma through myeloid differentiation factor 88-dependent and -independent mechanisms</p>

<p>           Fortune, SM, Solache, A, Jaeger, A, Hill, PJ, Belisle, JT, Bloom, BR, Rubin, EJ, and Ernst, JD</p>

<p>           J IMMUNOL 2004. 172(10): 6272-6280</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221276900061">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221276900061</a> </p><br />

<p>   26.    43594   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           Safety, pharmacokinetics and pharmacodynamic results of higher doses of albuferon in a phase 1/2 single and double dose-escalation study in treatment experienced subjects with chronic hepatitis C</p>

<p>           Balan, V, Sulkowski, M, Nelson, D, Everson, G, Dickson, R, Lambiase, L, Post, A, Redfield, R, Wiesner, R, Recta, J, Osborn, B, Novello, L, Freimuth, W, and Subramanian, M</p>

<p>           J HEPATOL 2004. 40: 135</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220950800457">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220950800457</a> </p><br />

<p>  27.     43595   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           522 Combination therapy with peginterferon alfa-2A (40KD) (PEGASYS(R)) plus ribavirin (COPEGUS(R)) in treatment-naive patients with chronic hepatitis C and genotype 1 infection: Individual estimated probability of sustained virological response (SVR)</p>

<p>           Weiland, O, Fried, MW, Hadziyannis, SJ, Foster, GR, Messinger, D, Freivogel, K, and Chaneac, M</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 154</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-MN/2/cd9c3203b9eb388223e5b118e5630a88">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-MN/2/cd9c3203b9eb388223e5b118e5630a88</a> </p><br />

<p>   28.    43596   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Structural variations in keto-glutamines for improved inhibition against hepatitis A virus 3C proteinase</p>

<p>           Jain, Rajendra P and Vederas, John C</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(14): 3655-3658</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CJCTNH-5/2/c6c63ca22a4656bcc0bd877278362e4c">http://www.sciencedirect.com/science/article/B6TF9-4CJCTNH-5/2/c6c63ca22a4656bcc0bd877278362e4c</a> </p><br />

<p>   29.    43597   OI-LS-298; EMBASE-OI-6/16/2004</p>

<p class="memofmt1-3">           Inhibition of cytomegalovirus infection by lactoferrin in vitro and in vivo</p>

<p>           Beljaars, Leonie, van der Strate, Barry WA, Bakker, Hester I, Reker-Smit, Catharina, van Loenen-Weemaes, Anne-miek, Wiegmans, Frouwke C, Harmsen, Martin C, Molema, Grietje, and Meijer, Dirk KF</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4CMHXVJ-1/2/21c251602e63142df90009d4b73c39d8">http://www.sciencedirect.com/science/article/B6T2H-4CMHXVJ-1/2/21c251602e63142df90009d4b73c39d8</a> </p><br />

<p>   30.    43598   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           Biochemical and genetic analysis of the four DNA ligases of mycobacteria</p>

<p>           Gong, CL, Martins, A, Bongiorno, P, Glickman, M, and Shuman, S</p>

<p>           J BIOL CHEM 2004. 279(20): 20594-20606</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221273800008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221273800008</a> </p><br />

<p>   31.    43599   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           Screening for hepatitis C virus antiviral activity with a cell-based secreted alkaline phosphatase reporter replicon system</p>

<p>           Bourne, N, Pyles, R, Yi, MK, Veselenak, R, Davis, M, and Lemon, S</p>

<p>           ANTIVIR RES 2004. 62(2): A50</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800060">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800060</a> </p><br />

<p>   32.    43600   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           In vitro combination studies of VX-950 (a HCV protease inhibitor) or VX-497 (an IMPDH inhibitor), two novel anti-HCV clinical candidates, with IFN-alpha</p>

<p>           Lin, K, Lin, C, and Kwong, AD</p>

<p>           ANTIVIR RES 2004. 62(2): A52</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800065">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800065</a> </p><br />

<p>   33.    43601   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           Antiviral activity of Glycyrrhizic Acid (GL) derivatives against SARS-coronavirus (SARS-CoV) and human cytomegalovirus (HCMV)</p>

<p>           Hoever, G, Baltina, L, Kondratenko, R, Baltina, L, Tolstikov, G, Doerr, HW, and Cinatl, J</p>

<p>           ANTIVIR RES 2004. 62(2): A75</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800121">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800121</a> </p><br />

<p>  34.     43602   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           Inhibition of hepatitis C virus RNA replication by benzimidazoles with high affinity for the 5 &#39;-untranslated region of the genomic RNA</p>

<p>           Swayze, EE, Seth, P, Jefferson, EA, Miyaji, A, Osgood, S, Ranken, R, Propp, S, Lowery, K, and Griffey, RH</p>

<p>           ANTIVIR RES 2004. 62(2): A82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800142">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800142</a> </p><br />

<p>   35.    43603   OI-LS-298; WOS-OI-6/6/2004</p>

<p class="memofmt1-3">           In vitro and in vivo evaluation of HCV polymerase inhibitors as potential drug candidates for treatment of chronic hepatitis C infection</p>

<p>           Ilan, E, Zauberman, A, Aviel, S, Nussbaum, O, Arazi, E, Ben-Moshe, O, Slama, D, Tzionas, I, Shoshany, Y, Lee, SW, Han, JJ, Park, SJ, Lee, GH, Park, EY, Shin, JC, Suh, JW, Kim, JW, and Dagan, S</p>

<p>           ANTIVIR RES 2004. 62(2): A83</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800144">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221142800144</a> </p><br />

<p>   36.    43604   OI-LS-298; WOS-OI-6/13/2004</p>

<p class="memofmt1-3">           A semi-empirical study of biflavonoid compounds with biological activity against tuberculosis</p>

<p>           Dias, JC, Rebelo, MM, and Alves, CN</p>

<p>           J MOL STRUC-THEOCHEM 2004. 676(1-3): 83-87</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221351100013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221351100013</a> </p><br />

<p>   37.    43605   OI-LS-298; WOS-OI-6/13/2004</p>

<p class="memofmt1-3">           Antibacterial and antifugal mono- and di-substituted symmetrical and unsymmetrical triazine-derived Schiff-bases and their transition metal complexes</p>

<p>           Chohan, ZH, Pervez, H, Rauf, A, Khan, KM, Maharvi, GM, and Supuran, CT</p>

<p>           J ENZYM INHIB MED CH 2004. 19(2): 161-168</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221450100009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221450100009</a> </p><br />

<p>   38.    43606   OI-LS-298; WOS-OI-6/13/2004</p>

<p class="memofmt1-3">           Adenylylation and catalytic properties of Mycobacterium tuberculosis glutamine synthetase expressed in Escherichia coli versus mycobacteria</p>

<p>           Mehta, R, Pearson, JT, Mahajan, S, Nath, A, Hickey, MJ, Sherman, DR, and Atkins, WM</p>

<p>           J BIOL CHEM 2004. 279(21): 22477-22482</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221417100097">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221417100097</a> </p><br />

<p>   39.    43607   OI-LS-298; WOS-OI-6/13/2004</p>

<p class="memofmt1-3">           Application of high throughput technologies to drug substance and drug product development</p>

<p>           Gardner, CR, Almarsson, O, Chen, HM, Morissette, S, Peterson, M, Zhang, Z, Wang, S, Lemmo, A, Gonzalez-Zugasti, J, Monagle, J, Marchionna, J, Ellis, S, McNulty, C, Johnson, A, Levinson, D, and Cima, M</p>

<p>           COMPUT CHEM ENG 2004. 28(6-7): 943-953</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221418000008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221418000008</a> </p><br />

<p>   40.    43608   OI-LS-298; WOS-OI-6/13/2004</p>

<p class="memofmt1-3">           Differential gene expression in mononuclear phagocytes infected with pathogenic and non-pathogenic mycobacteria</p>

<p>           McGarvey, JA, Wagner, D, and Bermudez, LE</p>

<p>           CLIN EXP IMMUNOL 2004. 136(3): 490-500</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221420700013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221420700013</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
