

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-04-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X+k2l5qjUfftjIsTLfmzCtNO0UI9YaET+KkTeHRtNHHtTFW0xVygTJJwseSXzJ1IHLn5fPeJ7EQh/jWzH4+a7NiKjEGHixH6+YPKE76l37ni89HdITF/J8tmJO0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8C6B01E3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">April 15, 2009 - April 28, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma]) </p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19389400?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Toxoplasma gondii: effects of Artemisia annua L. on susceptibility to infection in experimental models in vitro and in vivo.</a></p>

    <p class="NoSpacing">de Oliveira TS, Oliveira Silva DA, Rostkowska C, Béla SR, Ferro EA, Magalhães PM, Mineo JR.</p>

    <p class="NoSpacing">Exp Parasitol. 2009 Apr 20. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19389400 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19162220?ordinalpos=21&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Thioureides of 2-(phenoxymethyl)benzoic acid 4-R substituted: A novel class of anti-parasitic compounds.</a></p>

    <p class="NoSpacing">Müller J, Limban C, Stadelmann B, Missir AV, Chirita IC, Chifiriuc MC, Nitulescu GM, Hemphill A.</p>

    <p class="NoSpacing">Parasitol Int. 2009 Jun;58(2):128-35. Epub 2008 Dec 25.</p>

    <p class="NoSpacing">PMID: 19162220 [PubMed - in process]</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18775586?ordinalpos=17&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Hybrid molecules between benzenesulfonamides and active antimicrobial benzo[d]isothiazol-3-ones.</a></p>

    <p class="NoSpacing">Zani F, Incerti M, Ferretti R, Vicini P.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Jun;44(6):2741-7. Epub 2008 Jul 25.</p>

    <p class="NoSpacing">PMID: 18775586 [PubMed - in process]</p>  

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18793265?ordinalpos=84&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Differential expression of Aspergillus fumigatus protein in response to treatment with a novel antifungal compound, diethyl 4-(4-methoxyphenyl)-2,6-dimethyl-1,4-dihydropyridin-3,5-dicarboxylate.</a></p>

    <p class="NoSpacing">Chhillar AK, Yadav V, Kumar A, Kumar M, Parmar VS, Prasad A, Sharma GL.</p>

    <p class="NoSpacing">Mycoses. 2009 May;52(3):223-7. Epub 2008 Sep 12.</p>

    <p class="NoSpacing">PMID: 18793265 [PubMed - in process]</p>  

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19379501?ordinalpos=17&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Growth inhibition and ultrastructural alterations induced by Delta24(25)-sterol methyltransferase inhibitors in Candida spp. isolates, including non-albicans organisms.</a></p>

    <p class="NoSpacing">Ishida K, Cola Fernandes Rodrigues J, Dornelas Ribeiro M, Vila TV, de Souza W, Urbina JA, Nakamura CV, Rozental S.</p>

    <p class="NoSpacing">BMC Microbiol. 2009 Apr 20;9(1):74. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19379501 [PubMed - as supplied by publisher]</p> 

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19389221?ordinalpos=49&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vitro activity of pyronaridine against Plasmodium falciparum and comparative evaluation of anti-malarial drug susceptibility assays.</a></p>

    <p class="NoSpacing">Kurth F, Pongratz P, Belard S, Mordmuller B, Kremsner PG, Ramharter M.</p>

    <p class="NoSpacing">Malar J. 2009 Apr 23;8(1):79. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19389221 [PubMed - as supplied by publisher]</p>

    <h2>Antimicrobials, Antifungals (General)</h2> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19399540?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The anti-inflammatory non-antibiotic helper compound diclofenac: an antibacterial drug target.</a></p>

    <p class="NoSpacing">Mazumdar K, Dastidar SG, Park JH, Dutta NK.</p>

    <p class="NoSpacing">Eur J Clin Microbiol Infect Dis. 2009 Apr 28. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19399540 [PubMed - as supplied by publisher]</p>  

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19307123?ordinalpos=34&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Comparing anti-HIV, antibacterial, antifungal, micellar, and cytotoxic properties of tricarboxylato dendritic amphiphiles.</a></p>

    <p class="NoSpacing">Macri RV, Karlovská J, Doncel GF, Du X, Maisuria BB, Williams AA, Sugandhi EW, Falkinham JO 3rd, Esker AR, Gandour RD.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Apr 15;17(8):3162-8. Epub 2009 Mar 5.</p>

    <p class="NoSpacing">PMID: 19307123 [PubMed - in process]</p>  

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">9. Wang, Y.L., et al.</p>

    <p class="NoSpacing">Purification and characterization of novel antifungal peptide, mouse beta defensin-1, in Escherichia coli</p>

    <p class="NoSpacing">WORLD JOURNAL OF MICROBIOLOGY &amp; BIOTECHNOLOGY  2009 (May) 25:  917 - 920</p> 

    <p class="memofmt2-2">Citations from the Patent Listings for  O.I.</p>

    <p class="ListParagraph">10. Demirci, Betuel; Toyota, Masao; Demirci, Fatih; Dadandi, Mehmet Y.; Can Baser, K. Huesnue.</p>

    <p class="NoSpacing">Anticandidal pimaradiene diterpene from Phlomis essential oils.</p>

    <p class="NoSpacing">Comptes Rendus Chimie  (2009),  12(5),  612-621</p>  

    <p class="NoSpacing">11. Rokad SV, Tala SD, Akbari JD, Dhaduk MF, Joshi HS. Synthesis, antitubercular and antimicrobial activity of some new N-aryl-1,4-dihydropyridines containing furan nucleus. Journal of the Indian Chemical Society  (2009),  86(2),  186-191. </p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
