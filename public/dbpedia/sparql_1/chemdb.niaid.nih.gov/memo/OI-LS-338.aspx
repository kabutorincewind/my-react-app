

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-338.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="HPMGPe/CZNpXH14hv3ZrZIqrU259yODdNN+SLbCI+f33YqM03wQVDASueTDYMw+Yu4pVBIcIfA4EsHrEXjaQ98TNkKxZxkYa/WPOynpRNkkqG9ecCQ9woU3KkX0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="799D772A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-338-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48309   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          The discovery of finger loop inhibitors of the hepatitis C virus NS5B polymerase: Status and prospects for novel HCV therapeutics</p>

    <p>          Beaulieu, PL</p>

    <p>          IDrugs <b>2006</b>.  9(1): 39-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16374732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16374732&amp;dopt=abstract</a> </p><br />

    <p>2.     48310   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of 2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methyl purine nucleosides as inhibitors of hepatitis C virus RNA replication</p>

    <p>          Clark, JL, Mason, JC, Hollecker, L, Stuyver, LJ, Tharnish, PM, McBrayer, TR, Otto, MJ, Furman, PA, Schinazi, RF, and Watanabe, KA</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368235&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368235&amp;dopt=abstract</a> </p><br />

    <p>3.     48311   OI-LS-338; EMBASE-OI-12/27/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity of flavonoids</p>

    <p>          Cushnie, TPTim and Lamb, Andrew J</p>

    <p>          International Journal of Antimicrobial Agents <b>2005</b>.  26(5): 343-356</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4HC7708-2/2/d7ff23eb5b8540e4bb1f9ab03e0b5e6c">http://www.sciencedirect.com/science/article/B6T7H-4HC7708-2/2/d7ff23eb5b8540e4bb1f9ab03e0b5e6c</a> </p><br />

    <p>4.     48312   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          Randomized, double-blind, placebo-controlled study of peginterferon alfa-2a (40KD) plus ribavirin with or without amantadine in treatment-naive patients with chronic hepatitis C genotype 1 infection</p>

    <p>          Ferenci, P, Formann, E, Laferl, H, Gschwantler, M, Hackl, F, Brunner, H, Hubmann, R, Datz, C, Stauber, R, Steindl-Munda, P, Kessler, HH, Klingler, A, Gangl, A, and For, The Austrian Hepatitis Study Group</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16338019&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16338019&amp;dopt=abstract</a> </p><br />

    <p>5.     48313   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of substituted-10-methyl-1,2,3,4-tetrahydropyrazino[1,2-a]indoles</p>

    <p>          Tiwari, RK, Verma, AK, Chhillar, AK, Singh, D, Singh, J, Kasi, Sankar V, Yadav, V, Sharma, GL, and Chandra, R</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16377197&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16377197&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48314   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          Chronic Hepatitis C Virus Management: 2000-2005 Update (January)</p>

    <p>          Hughes, CA and Shafran, SD</p>

    <p>          Ann Pharmacother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368925&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368925&amp;dopt=abstract</a> </p><br />

    <p>7.     48315   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          The novel nucleoside analog R1479, 4&#39;-azidocytidine, is a potent inhibitor of NS5B dependent RNA synthesis and HCV replication in cell culture</p>

    <p>          Klumpp, K, Leveque, V, Le, Pogam S, Ma, H, Jiang, WR, Kang, H, Granycome, C, Singer, M, Laxton, C, Hang, JQ, Sarma, K, Smith, DB, Heindl, D, Hobbs, CJ, Merrett, JH, Symons, J, Cammack, N, Martin, JA, Devos, R, and Najera, I</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16316989&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16316989&amp;dopt=abstract</a> </p><br />

    <p>8.     48316   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of some alkyl [5-(nitroaryl)-1,3,4-thiadiazol-2-ylthio]propionates</p>

    <p>          Foroumadi, A, Kargar, Z, Sakhteman, A, Sharifzadeh, Z, Feyzmohammadi, R, Kazemi, M, and Shafiee, A</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359863&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359863&amp;dopt=abstract</a> </p><br />

    <p>9.     48317   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          Mutations conferring resistance to SCH6, a novel hepatitis C virus NS3/4A protease inhibitor: Reduced RNA replication fitness and partial rescue by second-site mutations</p>

    <p>          Yi, M, Tong, X, Skelton, A, Chase, R, Chen, T, Prongay, A, Bogen, SL, Saksena, AK, Njoroge, FG, Veselenak, RL, Pyles, RB, Bourne, N, Malcolm, BA, and Lemon, SM</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16352601&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16352601&amp;dopt=abstract</a> </p><br />

    <p>10.   48318   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          The first chemical synthesis of the core structure of the benzoylhydrazine-NAD adduct, a competitive inhibitor of the Mycobacterium tuberculosis enoyl reductase</p>

    <p>          Broussy, S, Bernardes-Genisson, V, Quemard, A, Meunier, B, and Bernadou, J</p>

    <p>          J Org Chem <b>2005</b>.  70(25): 10502-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16323864&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16323864&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48319   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          6-Benzylthioinosine analogues: Promising anti-toxoplasmic agents as inhibitors of the mammalian nucleoside transporter ENT1 (es)</p>

    <p>          Gupte, A, Buolamwini, JK, Yadav, V, Chu, CK, Naguib, FN, and El, Kouni MH</p>

    <p>          Biochem Pharmacol <b>2005</b>.  71(1-2): 69-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16310172&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16310172&amp;dopt=abstract</a> </p><br />

    <p>12.   48320   OI-LS-338; PUBMED-OI-12/27/2005</p>

    <p class="memofmt1-2">          p-Hydroxybenzoic acid synthesis in Mycobacterium tuberculosis</p>

    <p>          Stadthagen, G, Kordulakova, J, Griffin, R, Constant, P, Bottova, I, Barilone, N, Gicquel, B, Daffe, M, and Jackson, M</p>

    <p>          J Biol Chem <b>2005</b>.  280(49): 40699-706</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16210318&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16210318&amp;dopt=abstract</a> </p><br />

    <p>13.   48321   OI-LS-338; WOS-OI-12/18/2005</p>

    <p class="memofmt1-2">          Acute-phase reactants during murine tuberculosis: Unknown dimensions and new frontiers</p>

    <p>          Singh, PP and Kaur, S</p>

    <p>          TUBERCULOSIS <b>2005</b>.  85(5-6): 303-315, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233674400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233674400005</a> </p><br />

    <p>14.   48322   OI-LS-338; WOS-OI-12/18/2005</p>

    <p class="memofmt1-2">          Structural biology of mycobacterial proteins: The Bangalore effort</p>

    <p>          Vijayan, M</p>

    <p>          TUBERCULOSIS <b>2005</b>.  85(5-6): 357-366, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233674400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233674400010</a> </p><br />

    <p>15.   48323   OI-LS-338; WOS-OI-12/18/2005</p>

    <p class="memofmt1-2">          Attachment and invasion of Toxoplasma gondii and Neospora caninum to epithelial and fibroblast cell lines in vitro</p>

    <p>          Lei, Y, Davey, M, and Ellis, JT</p>

    <p>          PARASITOLOGY <b>2005</b>.  131: 583-590, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233604900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233604900001</a> </p><br />

    <p>16.   48324   OI-LS-338; WOS-OI-12/18/2005</p>

    <p class="memofmt1-2">          Highly lipophilic benzoxazoles with potential antibacterial activity</p>

    <p>          Vinsova, J, Horak, V, Buchta, V, and Kaustova, J</p>

    <p>          MOLECULES <b>2005</b>.  10(7): 783-793, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233710600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233710600006</a> </p><br />
    <br clear="all">

    <p>17.   48325   OI-LS-338; EMBASE-OI-12/27/2005</p>

    <p class="memofmt1-2">          Exploration of acyl sulfonamides as carboxylic acid replacements in protease inhibitors of the hepatitis C virus full-length NS3</p>

    <p>          Ronn, Robert, Sabnis, Yogesh A, Gossas, Thomas, Akerblom, Eva, Helena Danielson, U, Hallberg, Anders, and Johansson, Anja</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(2): 544-559</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H877DS-2/2/c217c935922c774b1816e61c4b8c21fe">http://www.sciencedirect.com/science/article/B6TF8-4H877DS-2/2/c217c935922c774b1816e61c4b8c21fe</a> </p><br />

    <p>18.   48326   OI-LS-338; WOS-OI-12/18/2005</p>

    <p><b>          Design, synthesis, and microbiological evaluation of new Candida albicans CYP51 inhibitors</b> </p>

    <p>          Schiaffella, F, Macchiarulo, A, Milanese, L, Vecchiarelli, A, Costantino, G, Pietrella, D, and Fringuelli, R</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(24): 7658-7666, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233654900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233654900016</a> </p><br />

    <p>19.   48327   OI-LS-338; WOS-OI-12/25/2005</p>

    <p class="memofmt1-2">          Finding new tricks for old drugs: An efficient route for public-sector drug discovery</p>

    <p>          O&#39;Connor, KA and Roth, BL</p>

    <p>          NATURE REVIEWS DRUG DISCOVERY <b>2005</b>.  4(12): 1005-1014, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233774900021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233774900021</a> </p><br />

    <p>20.   48328   OI-LS-338; WOS-OI-12/25/2005</p>

    <p class="memofmt1-2">          Gateways to clinical trials</p>

    <p>          Bayes, M, Rabasseda, X, and Prous, JR</p>

    <p>          METHODS AND FINDINGS IN EXPERIMENTAL AND CLINICAL PHARMACOLOGY <b>2005</b>.  27(8): 569-612, 44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233873800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233873800007</a> </p><br />

    <p>21.   48329   OI-LS-338; WOS-OI-12/25/2005</p>

    <p class="memofmt1-2">          Mutational and expression analysis of tbnat and its response to isoniazid</p>

    <p>          Sholto-Douglas-Vernon, C, Sandy, J, Victor, TC, Sim, E, and van, Helden PD</p>

    <p>          JOURNAL OF MEDICAL MICROBIOLOGY <b>2005</b>.  54(12): 1189-1197, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233831000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233831000011</a> </p><br />

    <p>22.   48330   OI-LS-338; WOS-OI-12/25/2005</p>

    <p class="memofmt1-2">          Synthesis of reduced xanthatin derivatives and in vitro evaluation of their antifungal activity</p>

    <p>          Pinel, B, Landreau, A, Seraphin, D, Larcher, G, Bouchara, JP, and Richomme, P</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2005</b>.  20(6): 575-579, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233856200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233856200007</a> </p><br />

    <p>23.   48331   OI-LS-338; WOS-OI-12/25/2005</p>

    <p class="memofmt1-2">          Simultaneous detection of isoniazid, rifampin, and ethambutol resistance of Mycobacterium tuberculosis by a single multiplex allele-specific polymerase chain reaction (PCR) assay</p>

    <p>          Yang, ZH, Durmaz, R, Yang, D, Gunal, S, Zhang, LX, Foxman, B, Sanic, A, and Marrs, CF</p>

    <p>          DIAGNOSTIC MICROBIOLOGY AND INFECTIOUS DISEASE <b>2005</b>.  53(3 ): 201-208, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233830600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233830600006</a> </p><br />

    <p>24.   48332   OI-LS-338; WOS-OI-12/25/2005</p>

    <p class="memofmt1-2">          New agents with anti mycobacterial activity</p>

    <p>          Marco-Contelles, J and Gomez-Sanchez, E</p>

    <p>          ARCHIV DER PHARMAZIE <b>2005</b>.  338(11): 562-563, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233783400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233783400008</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
