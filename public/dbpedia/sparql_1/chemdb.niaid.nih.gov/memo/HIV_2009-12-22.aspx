

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-12-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="g+wqLgsOiIGx7LdxfSASm3a4NmYfV0LJ1ftJHAmzoA3WUU5wuFB5+eNt490e4OpjED650Al/UwxWrM/aAXnBBOX8ehMzwZRebZP/OUf3FxzGWTMVM0wHM3Koxhg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F28E3B45" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  December 9 - December 22, 2009</h1> 

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20020452?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Synthesis and Anti-HIV Activity of [ddN]-[ddN] Dimers and Benzimidazole Nucleoside Dimers.</a> Li GR, Liu J, Pan Q, Song ZB, Luo FL, Wang SR, Zhang XL, Zhou X. Chem Biodivers. 2009 Dec 17;6(12):2200-2208. [Epub ahead of print]PMID: 20020452</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20006684?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">A bioactive isoprenylated xanthone and other constituents of Garciniaedulis.</a> Magadula JJ. Fitoterapia. 2009 Dec 16. [Epub ahead of print]PMID: 20006684</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20015992?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">The Potent Anti-HIV Activity of CXCL12{gamma} Correlates with Efficient CXCR4 Binding and Internalization.</a> Altenburg JD, Jin Q, Alkhatib B, Alkhatib G. J Virol. 2009 Dec 16. [Epub ahead of print]PMID: 20015992</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20014858?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">Multivalent Benzoboroxole Functionalized Polymers as gp120 Glycan Targeted Microbicide Entry Inhibitors.</a> Jay JI, Lai BE, Myszka DG, Mahalingam A, Langheinrich K, Katz DF, Kiser PF. Mol Pharm. 2009 Dec 16. [Epub ahead of print]PMID: 20014858</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20013136?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">Molecular docking and 3D-QSAR studies on triazolinone and pyridazinone, non-nucleoside inhibitor of HIV-1 reverse transcriptase.</a> Sivan SK, Manga V. J Mol Model. . [Epub ahead of print]PMID: 20013136</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20013639?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">Anti-HIV-1 Diterpenoids from Leaves and Twigs of Polyalthia sclerophylla.</a> Saepou S, Pohmakotr M, Reutrakul V, Yoosook C, Kasisit J, Napaswad C, Tuchinda P. Planta Med. . [Epub ahead of print]PMID: 20013639</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19954246?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=8">Small-sized human immunodeficiency virus type-1 protease inhibitors containing allophenylnorstatine to explore the S2&#39; pocket.</a>Hidaka K, Kimura T, Abdel-Rahman HM, Nguyen JT, McDaniel KF, Kohlbrenner WE, Molla A, Adachi M, Tamada T, Kuroki R, Katsuki N, Tanaka Y, Matsumoto H, Wang J, Hayashi Y, Kempf DJ, Kiso Y. J Med Chem. 2009 Dec 10;52(23):7604-17.PMID: 19954246</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19769332?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=9">Inhibitors of human immunodeficiency virus type 1 (HIV-1) attachment. 5. An evolution from indole to azaindoles leading to the discovery of 1-(4-benzoylpiperazin-1-yl)-2-(4,7-dimethoxy-1H-pyrrolo[2,3-c]pyridin-3-yl)ethane-1,2-dione (BMS-488043), a drug candidate that demonstrates antiviral activity in HIV-1-infected subjects.</a> Wang T, Yin Z, Zhang Z, Bender JA, Yang Z, Johnson G, Yang Z, Zadjura LM, D&#39;Arienzo CJ, DiGiugno Parker D, Gesenberg C, Yamanaka GA, Gong YF, Ho HT, Fang H, Zhou N, McAuliffe BV, Eggers BJ, Fan L, Nowicka-Sans B, Dicker IB, Gao Q, Colonno RJ, Lin PF, Meanwell NA, Kadow JF. J Med Chem. 2009 Dec 10;52(23):7778-87.PMID: 19769332</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20009920?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=13">Sensitivity of V75I HIV-1 reverse transcriptase mutant selected in vitro by acyclovir to anti-HIV drugs.</a> McMahon MA, Siliciano JD, Kohli RM, Siliciano RF. AIDS. . [Epub ahead of print]PMID: 20009920</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20001317?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=14">Palmitic Acid Is a Novel CD4 Fusion Inhibitor That Blocks HIV Entry and Infection.</a> Lee DY, Lin X, Paskaleva EE, Liu Y, Puttamadappa SS, Thornber C, Drake JR, Habulin M, Shekhtman A, Canki M. AIDS Res Hum Retroviruses. . [Epub ahead of print]PMID: 20001317</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267203900024">Tetrahydroxy Cyclic Urea-Potent Inhibitor for HIV-1 Protease Wild Type and Mutant Type - A Computational Design.</a>  Kanth, SS; Vijjulatha, M.  E-JOURNAL OF CHEMISTRY, 2008; 5(3): 584-592.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272150800031">Screening of new non-nucleoside reverse transcriptase inhibitors of HIV-1 based on traditional Chinese medicines database.</a>  Liu, T; Li, AX; Miao, YP; Wu, KZ; Ma, Y.  CHINESE CHEMICAL LETTERS, 2009; 20(11): 1386-1388.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272158100005">Synthesis and HIV-1 RNase H-activity of new alizarin acetonyl derivatives.</a>  Kharlamova, TV.  CHEMISTRY OF NATURAL COMPOUNDS, 2009; 45(5): 629-633.</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p> 

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>   

    <p class="memofmt2-2">&nbsp;</p>   
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
