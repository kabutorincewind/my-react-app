

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nOgVrESEwc4Rx0fh5Irvs6j5G9Yi2Dj4E+Gj9BjTN6DoSnwYYNgWyo6NTo6KVNkyJfzh8JbpXB7oNw3rUWHZl2mbGDQ4QNeQjWUU98EtxQiZQe/RYGOcB5bklao=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="49DAE7DD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: November, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27517812">Computational and Synthetic Approaches for Developing Lavendustin B Derivatives as Allosteric Inhibitors of HIV-1 Integrase.</a> Agharbaoui, F.E., A.C. Hoyte, S. Ferro, R. Gitto, M.R. Buemi, J.R. Fuchs, M. Kvaratskhelia, and L. De Luca. European Journal of Medicinal Chemistry, 2016. 123: p. 673-683. PMID[27517812].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27663546">Analysis of Quinolinequinone Reactivity, Cytotoxicity, and anti-HIV-1 Properties.</a> Alfadhli, A., A. Mack, L. Harper, S. Berk, C. Ritchie, and E. Barklis. Bioorganic &amp; Medicinal Chemistry, 2016. 24(21): p. 5618-5625. PMID[27663546]. PMCID[PMC5065790].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386763500014">Amino acid Derivatives. Part 6. Synthesis, in Vitro Antiviral Activity and Molecular Docking Study of New N-alpha-Amino acid Derivatives Conjugated Spacer Phthalimide Backbone.</a> Al-Masoudi, N.A., E. Abood, Z.T. Al-Maliki, W.A. Al-Masoudi, and C. Pannecouque. Medicinal Chemistry Research, 2016. 25(11): p. 2578-2588. ISI[000386763500014].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27620483">A Modified P1 Moiety Enhances in Vitro Antiviral Activity against Various Multidrug-resistant HIV-1 Variants and in Vitro Central Nervous System Penetration Properties of a Novel Nonpeptidic Protease Inhibitor, GRL-10413.</a> Amano, M., P.M. Salcedo-Gomez, R. Zhao, R.S. Yedidi, D. Das, H. Bulut, N.S. Delino, V.R. Sheri, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2016. 60(12): p. 7046-7059. PMID[27620483].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000383571300041">Anti-HIV-1 Activity of Compounds Derived from Marine Alga Canistrocarpus cervicornis.</a> Barros, C.D., C.C. Cirne-Santos, V. Garrido, I. Barcelos, P.R.S. Stephens, V. Giongo, V.L. Teixeira, and I. Paixao. Journal of Applied Phycology, 2016. 28(4): p. 2523-2527. ISI[000383571300041].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27677167">Hydroxytyrosol: A New Class of Microbicide Displaying Broad anti-HIV-1 Activity.</a> Bedoya, L.M., M. Beltran, P. Obregon-Calderon, J. Garcia-Perez, H.E. de la Torre, N. Gonzalez, M. Perez-Olmeda, D. Aunon, L. Capa, E. Gomez-Acebo, and J. Alcami. AIDS, 2016. 30(18): p. 2767-2776. PMID[27677167]. PMCID[PMC5106087].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27825797">An Analog of Camptothecin Inactive against Topoisomerase I is broadly Neutralizing of HIV-1 through Inhibition of Vif-Dependent APOBEC3G Degradation.</a> Bennett, R.P., R.A. Stewart, P.A. Hogan, R.G. Ptak, M.K. Mankowski, T.L. Hartman, R.W. Buckheit, Jr., B.A. Snyder, J.D. Salter, G.A. Morales, and H.C. Smith. Antiviral Research, 2016. 136: p. 51-59. PMID[27825797]. PMCID[PMC5125868].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600396">GRFT/Carrageenan Gel Inhibits HIV and HSV-2 in Human Cervical Mucosa.</a> Calenda, G., P. Barnable, K. Levendosky, K. Kleinbeck, J.A. Fernandez-Romero, B.O. Keefe, T.M. Zydowsky, and N. Teleshova. AIDS Research and Human Retroviruses, 2016. 32: p. 217-217. ISI[000386774600396].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27707628">Design, Synthesis and Evaluation of Small Molecule CD4-Mimics as Entry Inhibitors Possessing Broad Spectrum anti-HIV-1 Activity.</a> Curreli, F., D.S. Belov, R.R. Ramesh, N. Patel, A. Altieri, A.V. Kurkin, and A.K. Debnath. Bioorganic &amp; Medicinal Chemistry, 2016. 24(22): p. 5988-6003. PMID[27707628]. PMCID[PMC5079829].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600439">Hemoglobin Derived anti-HIV Peptide, HbAHP-25, Restrains HIV-1 Associated Barrier Dysfunction: Potential for Therapeutic Application.</a> Dar, T.B. and K.V.R. Reddy. AIDS Research and Human Retroviruses, 2016. 32: p. 239-239. ISI[000386774600439].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27765358">Sennoside A, Derived from the Traditional Chinese Medicine Plant Rheum l., is a New Dual HIV-1 Inhibitor Effective on HIV-1 Replication.</a> Esposito, F., I. Carli, C. Del Vecchio, L. Xu, A. Corona, N. Grandi, D. Piano, E. Maccioni, S. Distinto, C. Parolin, and E. Tramontano. Phytomedicine, 2016. 23(12): p. 1383-1391. PMID[27765358].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385389800102">Design of Boronic acid-attributed Carbon Dots on Inhibits HIV-1 Entry.</a> Fahmi, M.Z., W. Sukmayani, S.Q. Khairunisa, A.M. Witaningrum, D.W. Indriati, M.Q.Y. Matondang, J.Y. Chang, T. Kotaki, and M. Kameoka. RSC Advances, 2016. 6(95): p. 92996-93002. ISI[000385389800102].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600229">Preclinical Characterization of a Potent D-Peptide Inhibitor of HIV Entry: Cholesterol-Conjugated PIE12-trimer.</a> Francis, J.N., Y. Nishimura, A. Smith, B. Welch, M. Martin, and M. Kay. AIDS Research and Human Retroviruses, 2016. 32: p. 129-129. ISI[000386774600229].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600618">Characterization of an Oxidation Resistant Griffithsin for Use as an HIV Microbicide.</a> Fuqua, J., K. Hamorsky, L. Wang, L. Kramzer, N. Matoba, L. Rohan, and K. Palmer. AIDS Research and Human Retroviruses, 2016. 32: p. 333-333. ISI[000386774600618].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000381490100109">Dendronized PLGA Nanoparticles with Anionic Carbosilane Dendrons as Antiviral Agents against HIV Infection.</a> Galan, M., C. Fornaguera, P. Ortega, G. Caldero, R. Lorente, J.L. Jimenez, J. de la Mata, M.A. Munoz-Fernandez, C. Solans, and R. Gomez. RSC Advances, 2016. 6(77): p. 73817-73826. ISI[000381490100109].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385175500011">Novel Quinolone-3-carboxylic acid Derivatives as anti-HIV-1 Agents: Design, Synthesis, and Biological Activities.</a> Hajimahdi, Z., R. Zabihollahi, M.R. Aghasadeghi, S.H. Ashtiani, and A. Zarghi. Medicinal Chemistry Research, 2016. 25(9): p. 1861-1876. ISI[000385175500011].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600208">IQP-0528: The Pharmacokinetics of an anti-HIV NNRTI in Non-human Primates from Various Dosage Forms.</a> Ham, A., P. Srinivasan, L. Pereira, K. Buckheit, A. Martin, T. Singletary, J.N. Zhang, D. Katz, J. Smith, and R. Buckheit. AIDS Research and Human Retroviruses, 2016. 32: p. 116-116. ISI[000386774600208].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27647368">Synthesis of Arylated Coumarins by Suzuki-Miyaura Cross-coupling. Reactions and anti-HIV Activity.</a> Hamdy, A.M., Z. Khaddour, N.A. Al-Masoudi, Q. Rahman, C. Hering-Junghans, A. Villinger, and P. Langer. Bioorganic &amp; Medicinal Chemistry, 2016. 24(21): p. 5115-5126. PMID[27647368].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27484512">Therapeutic Potential of Coumarins as Antiviral Agents.</a> Hassan, M.Z., H. Osman, M.A. Ali, and M.J. Ahsan. European Journal of Medicinal Chemistry, 2016. 123: p. 236-255. PMID[27484512].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600428">Increased Activity of the Entry Inhibitor DS003, a BMS-378806 Analogue, through Binding to the CD4-Induced Epitope in HIV-1 gp120.</a> Herrera, C., S. Harman, P. Rogers, Y. Aldon, N. Armanasco, D. Stieh, J. Holt, J. Nutall, and R.J. Shattock. AIDS Research and Human Retroviruses, 2016. 32: p. 233-233. ISI[000386774600428].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600383">Silk-formulated Antiretrovirals as Candidate Microbicides.</a> Herrera, C., N. Olejniczak, L. Zhang, J. Coburn, D. Kaplan, and P.J. Liwang. AIDS Research and Human Retroviruses, 2016. 32: p. 210-210. ISI[000386774600383].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600206">Vaginal Safety Evaluation of a Triple Antiretroviral Drug-loaded Electrospun Fiber Microbicide in Nonhuman Primates.</a> Jiang, Y.H., D. Patton, Y.C. Sweeney, A. Hajheidari, A. Beyene, R. Stoddard, A. Blakney, E. Roberts, J. Phan, R. Edmark, and K.A. Woodrow. AIDS Research and Human Retroviruses, 2016. 32: p. 115-115. ISI[000386774600206].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000385905800002">Computer-aided Discovery of anti-HIV Agents.</a> Jorgensen, W.L. Bioorganic &amp; Medicinal Chemistry, 2016. 24(20): p. 4768-4778. ISI[000385905800002].
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600243">Testing Human anti-HIV Monoclonal Antibodies Produced in Nicotiana for Activity against Cell-associated HIV.</a> Kadasia, K., A. Chung, G. Alter, and D. Anderson. AIDS Research and Human Retroviruses, 2016. 32: p. 138-138. ISI[000386774600243].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386697200029">Synthesis, Crystal Structure, anti-HIV, and Antiproliferative Activity of New Oxadiazole and Thiazole Analogs.</a> Khan, M.U., S. Hameed, T. Akhtar, N.A. Al-Masoudi, W.A. Al-Masoudi, P.G. Jones, and C. Pannecouque. Medicinal Chemistry Research, 2016. 25(10): p. 2399-2409. ISI[000386697200029].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27869695">Studies in a Murine Model Confirm the Safety of Griffithsin and Advocate Its Further Development as a Microbicide Targeting HIV-1 and Other Enveloped Viruses.</a> Kouokam, J.C., A.B. Lasnik, and K.E. Palmer. Viruses, 2016. 8(311): 16pp. PMID[27869695].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27471910">Asymmetric Synthesis of a Potent HIV-1 Integrase Inhibitor.</a> Kuethe, J.T., G.R. Humphrey, M. Journet, Z. Peng, and K.G. Childers. Journal of Organic Chemistry, 2016. 81(21): p. 10256-10265. PMID[27471910].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27681137">Specific Inhibition of HIV Infection by the Action of Spironolactone in T Cells.</a> Lacombe, B., M. Morel, F. Margottin-Goguet, and B.C. Ramirez. Journal of Virology, 2016. 90(23): p. 10972-10980. PMID[27681137]. PMCID[PMC5110165].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600757">Anti-HIV-1 ADCC Antibodies Following Latency Reversal and Treatment Interruption.</a> Lee, W.S., T.A. Rasmussen, M. Tolstrup, L. Ostergaard, O.S. Sogaard, M.S. Parsons, and S.J. Kent. AIDS Research and Human Retroviruses, 2016. 32: p. 405-405. ISI[000386774600757].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27456163">Hydrophobic-core PEGylated Graft Copolymer-stabilized Nanoparticles Composed of Insoluble Non-Nucleoside Reverse Transcriptase Inhibitors Exhibit Strong anti-HIV Activity.</a> Leporati, A., M.S. Novikov, V.T. Valuev-Elliston, S.P. Korolev, A.L. Khandazhinskaya, S.N. Kochetkov, S. Gupta, J. Goding, E. Bolotin, M.B. Gottikh, and A.A. Bogdanov, Jr. Nanomedicine, 2016. 12(8): p. 2405-2413. PMID[27456163]. PMCID[PMC5116409].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27855210">Potent and Broad Inhibition of HIV-1 by a Peptide from the gp41 Heptad Repeat-2 Domain Conjugated to the CXCR4 Amino Terminus.</a> Leslie, G.J., J. Wang, M.W. Richardson, B.S. Haggarty, K.L. Hua, J. Duong, A.J. Secreto, A.P. Jordon, J. Romano, K.E. Kumar, J.J. DeClercq, P.D. Gregory, C.H. June, M.J. Root, J.L. Riley, M.C. Holmes, and J.A. Hoxie. Plos Pathogens, 2016. 12(11): p. e1005983. PMID[27855210].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27748590">Amidate Prodrugs of Deoxythreosyl Nucleoside Phosphonates as Dual Inhibitors of HIV and HBV Replication.</a> Liu, C., S.G. Dumbre, C. Pannecouque, C.S. Huang, R.G. Ptak, M.G. Murray, S. De Jonghe, and P. Herdewijn. Journal of Medicinal Chemistry, 2016. 59(20): p. 9513-9531. PMID[27748590].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27491414">Lipopolysaccharide Suppresses Human Immunodeficiency Virus 1 Reverse Transcription in Macrophages.</a> Liu, F.L., J.W. Zhu, D. Mu, and Y.T. Zheng. Archives of Virology, 2016. 161(11): p. 3019-3027. PMID[27491414].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600436">A Lectibody Targeting Env Glycans Exhibits Broad Antiviral Activity against HIV Virions and Infected Cells.</a> Matoba, N., A. Husk, J.C. Kouokam, K. Hamorsky, and T. Grooms. AIDS Research and Human Retroviruses, 2016. 32: p. 237-237. ISI[000386774600436].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386779300016">Synthesis and Antiviral Properties of New Derivatives of 2-(Alkylsulfanyl)-6-[1-(2,6-difluorophenyl)cyclopropyl]-5-methylpyrimidin-4(3H)-one.</a> Novakov, I.A., A.S. Yablokov, B.S. Orlinson, M.B. Navrotskii, I.A. Kirillov, A.A. Vernigora, A.S. Babushkin, V.V. Kachala, and D. Schols. Russian Journal of Organic Chemistry, 2016. 52(8): p. 1188-1193. ISI[000386779300016].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600577">Surfactant Protein D Inhibits HIV-1 Transfer through Vaginal Explants and Safety Studies Exhibit Broad Therapeutic Index.</a> Pandit, H., H. Yamamoto, T. Fashemi, U. Kishore, T. Madan, and R. Fichorova. AIDS Research and Human Retroviruses, 2016. 32: p. 312-312. ISI[000386774600577].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26524629">Deciphering Structure-activity Relationships in a Series of Tat/TAR Inhibitors.</a> Pascale, L., A.L. Gonzalez, A. Di Giorgio, M. Gaysinski, J. Teixido Closa, R.E. Tejedor, S. Azoulay, and N. Patino. Journal of Biomolecular Structure and Dynamics, 2016. 34(11): p. 2327-2338. PMID[26524629].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27645997">A New Class of Allosteric HIV-1 Integrase Inhibitors Identified by Crystallographic Fragment Screening of the Catalytic Core Domain.</a> Patel, D., J. Antwi, P.C. Koneru, E. Serrao, S. Forli, J.J. Kessl, L. Feng, N. Deng, R.M. Levy, J.R. Fuchs, A.J. Olson, A.N. Engelman, J.D. Bauman, M. Kvaratskhelia, and E. Arnold. Journal of Biological Chemistry, 2016. 291(45): p. 23569-23577. PMID[27645997]. PMCID[PMC5095411].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386774600158">Rational Design of Bispecific Antibodies Targeting HIV-1 Env to Achieve Enhanced Anti-viral Potency and Breadth.</a> Steinhardt, J., J. Guenaga, K. McKee, M. Louder, S. O&#39;Dell, C.I. Chiang, L. Lei, N. Doria-Rose, J. Mascola, and Y.X. Li. AIDS Research and Human Retroviruses, 2016. 32: p. 90-90. ISI[000386774600158].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000386218100018">Design, Synthesis and Biological Evaluation of Caffeoyl benzanilides as Dual Inhibitors of HIV Integrase and CCR5.</a> Sun, X.F., N.N. Fan, W.S. Xu, Y.X. Sun, X. Xie, Y. Guo, L.Y. Ma, J.Y. Liu, and X.W. Wang. MedChemComm, 2016. 7(10): p. 2028-2032. ISI[000386218100018].
    <br />
    <b>[WOS]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27645238">Antiviral Activity of Bictegravir (GS-9883), a Novel Potent HIV-1 Integrase Strand Transfer Inhibitor with an Improved Resistance Profile.</a> Tsiang, M., G.S. Jones, J. Goldsmith, A. Mulato, D. Hansen, E. Kan, L. Tsai, R.A. Bam, G. Stepan, K.M. Stray, A. Niedziela-Majka, S.R. Yant, H. Yu, G. Kukolj, T. Cihlar, S.E. Lazerwith, K.L. White, and H. Jin. Antimicrobial Agents and Chemotherapy, 2016. 60(12): p. 7086-7097. PMID[27645238].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">42. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27584592">A Chimeric Human APOBEC3A Protein with a Three Amino acid Insertion Confers Differential HIV-1 and Adeno-associated Virus Restriction.</a> Wang, Y.Q., Z.K. Wang, A. Pramanik, M.L. Santiago, J.M. Qiu, and E.B. Stephens. Virology, 2016. 498: p. 149-163. PMID[27584592].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">43. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27863502">Annexin A2 Antibodies but Not Inhibitors of the Annexin A2 Heterotetramer Impair Productive HIV-1 Infection of Macrophages in Vitro.</a> Woodham, A.W., A.M. Sanna, J.R. Taylor, J.G. Skeate, D.M. Da Silva, L.V. Dekker, and W.M. Kast. Virology Journal, 2016. 13(1): p. 187. PMID[27863502]. PMCID[PMC5116172].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">44. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27484516">2,4,5-Trisubstituted Thiazole Derivatives as HIV-1 NNRTIs Effective on Both Wild-type and Mutant HIV-1 Reverse Transcriptase: Optimization of the Substitution of Positions 4 and 5.</a> Xu, Z., J. Guo, Y. Yang, M. Zhang, M. Ba, Z. Li, Y. Cao, R. He, M. Yu, H. Zhou, X. Li, X. Huang, Y. Guo, and C. Guo. European Journal of Medicinal Chemistry, 2016. 123: p. 309-316. PMID[27484516].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">45. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27676157">Incorporation of Privileged Structures into Bevirimat Can Improve Activity against Wild-type and Bevirimat-resistant HIV-1.</a> Zhao, Y., Q. Gu, S.L. Morris-Natschke, C.H. Chen, and K.H. Lee. Journal of Medicinal Chemistry, 2016. 59(19): p. 9262-9268. PubMed[27676157].
    <br />
    <b>[PubMed]</b>. HIV_11_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">46. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161027&amp;CC=WO&amp;NR=2016172496A1&amp;KC=A1">Preparation of 2-Styrylcyclopropan-1-amine Derivatives as LSD1 Inhibitors and Uses Thereof.</a> Albrecht, B.K., J.E. Audia, A. Cote, M. Duplessis, V.S. Gehling, J.-C. Harmange, and R.G. Vaswani. Patent. 2016. 2016-US28864 2016172496: 274pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">47. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161027&amp;CC=WO&amp;NR=2016172424A1&amp;KC=A1">Preparation of Sulfonylureas as Inhibitors of Human Immunodeficiency Virus Replication.</a> Bender, J.A., R.G. Gentles, A. Pendri, A.X. Wang, N.A. Meanwell, B.R. Beno, R.A. Fridell, M. Belema, V.N. Nguyen, Z. Yang, G. Wang, S. Kumaravel, S. Thangathirupathy, R.O. Bora, S.M. Holehatti, M.R. Mettu, and M. Panda. Patent. 2016. 2016-US28762 2016172424: 319pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">48. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161027&amp;CC=WO&amp;NR=2016172425A1&amp;KC=A1">Preparation of bis(Amino Acid) Derivatives as Inhibitors of Human Immunodeficiency Virus Replication and Use for the Treatment of HIV Infection.</a> Bender, J.A., O.D. Lopez, V.N. Nguyen, Z. Yang, A.X. Wang, G. Wang, N.A. Meanwell, B.R. Beno, R.A. Fridell, M. Belema, and S. Thangathirupathy. Patent. 2016. 2016-US28763 2016172425: 350pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">49. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161117&amp;CC=WO&amp;NR=2016183093A1&amp;KC=A1">Inhibitors of Viruses.</a> Golden, J.E. and D. Chung. Patent. 2016. 2016-US31662 2016183093: 78pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">50. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161110&amp;CC=WO&amp;NR=2016178092A2&amp;KC=A2">Preparation of Novel C-3 Triterpenones with C-28 Reverse Amide Derivatives as HIV Inhibitors.</a> Reddy, B.P., G.L.D. Krupadanam, A.P. Reddy, K.B. Reddy, L.V.L. Subrahmanyam, and K.R. Reddy. Patent. 2016. 2016-IB811 2016178092: 148pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2016.</p>

    <br />

    <p class="plaintext">51. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130214&amp;CC=US&amp;NR=2013040961A1&amp;KC=A1">Oligomer Modified Diaromatic Substituted Compounds.</a> Riggs-Sauthier, J. and B.-L. Deng. Patent. 2016. 2012-13580580 9226970.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2016.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
