

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-376.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fWqag9azKnalzHTJ+1A4OWB4aoSjd5AKFBjP+JQ0dML4ZJiR2dgyeoOHAtWULq8x5VhXcZgQyO+yuTqQFPKwUB18FOQeMnr6UVaJcl5iBvCcRViFEK3Ag2keqaE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E334E117" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-376-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61036   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Antiviral and Antimicrobial Profiles of Selected Isoquinoline Alkaloids From Fumaria and Corydalis Species</p>

    <p>          Orhan, I. <i>et al.</i></p>

    <p>          Zeitschrift Fur Naturforschung C-a Journal of Biosciences <b>2007</b>.  62(1-2): 19-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245739200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245739200004</a> </p><br />

    <p>2.     61037   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Oxadiazole mannich bases: Synthesis and antimycobacterial activity</p>

    <p>          Ali, Mohamed Ashraf and Shaharyar, Mohammad</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(12): 3314-3316</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NF4F18-4/2/4dcb3d555c1a666ffdb8889aa9ab3b45">http://www.sciencedirect.com/science/article/B6TF9-4NF4F18-4/2/4dcb3d555c1a666ffdb8889aa9ab3b45</a> </p><br />

    <p>3.     61038   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Synthesis of Symmetrical C- and Pseudo-symmetrical O-Linked Disaccharide Analogs for Arabinosyltransferase Inhibitory Activity in Mycobacterium tuberculosis</p>

    <p>          Pathak, Ashish K, Pathak, Vibha, Riordan, James R, Suling, William J, Gurcha, Sudagar S, Besra, Gurdyal S, and Reynolds, Robert C</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 748</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NX2NGR-2/2/5a46bc1bd398300fa41646dafffa5818">http://www.sciencedirect.com/science/article/B6TF9-4NX2NGR-2/2/5a46bc1bd398300fa41646dafffa5818</a> </p><br />

    <p>4.     61039   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Ligand based virtual screening and biological evaluation of inhibitors of chorismate mutase (Rv1885c) from Mycobacterium tuberculosis H37Rv</p>

    <p>          Agrawal, Himanshu, Kumar, Ashutosh, Bal, Naresh Chandra, Siddiqi, Mohammad Imran, and Arora, Ashish</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(11): 3053-3058</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N9P4JN-1/2/9394e0f02abecadfebb9ec89b64ab95c">http://www.sciencedirect.com/science/article/B6TF9-4N9P4JN-1/2/9394e0f02abecadfebb9ec89b64ab95c</a> </p><br />

    <p>5.     61040   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Synthesis of 4-phenoxybenzamide adenine dinucleotide as NAD analogue with inhibitory activity against enoyl-ACP reductase (InhA) of Mycobacterium tuberculosis</p>

    <p>          Bonnac, Laurent, Gao, Guang-Yao, Chen, Liqiang, Felczak, Krzysztof, Bennett, Eric M, Xu, Hua, Kim, TaeSoo, Liu, Nina, Oh, HyeWon, Tonge, Peter J, and Pankiewicz, Krzysztof W</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  727</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NVT9ST-4/2/9ac22e7762a9507523270f3dfe3b1c92">http://www.sciencedirect.com/science/article/B6TF9-4NVT9ST-4/2/9ac22e7762a9507523270f3dfe3b1c92</a> </p><br />

    <p>6.     61041   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Antimycobacterial Activity From Cyanobacterial Extracts and Phytochemical Screening of Methanol Extract of Hapalosiphon</p>

    <p>          Rao, M. <i>et al.</i></p>

    <p>          Pharmaceutical Biology <b>2007</b>.  45(2): 88-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245677200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245677200002</a> </p><br />

    <p>7.     61042   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Crystallographic studies on the binding of isonicotinyl-NAD adduct to wild-type and isoniazid resistant 2-trans-enoyl-ACP (CoA) reductase from Mycobacterium tuberculosis</p>

    <p>          Dias, Marcio Vinicius Bertacine, Vasconcelos, Igor Bordin, Prado, Adriane Michele Xavier, Fadel, Valmir, Basso, Luiz Augusto, de Azevedo Jr, Walter Filgueira, and Santos, Diogenes Santiago</p>

    <p>          Journal of Structural Biology <b>2007</b>.  In Press, Uncorrected Proof: 205</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WM5-4NMWRBG-1/2/4bd6bd062be8d6e1517d720d8c5cb4f4">http://www.sciencedirect.com/science/article/B6WM5-4NMWRBG-1/2/4bd6bd062be8d6e1517d720d8c5cb4f4</a> </p><br />

    <p>8.     61043   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Newer tetracycline derivatives: Synthesis, anti-HIV, antimycobacterial activities and inhibition of HIV-1 integrase</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, Senchani, Geetha, and Banerjee, Debjani</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(8): 2372-2375</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MCMWJ9-1/2/cee6aa86925226a9541d49894620c43a">http://www.sciencedirect.com/science/article/B6TF9-4MCMWJ9-1/2/cee6aa86925226a9541d49894620c43a</a> </p><br />

    <p>9.     61044   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Superinfection Exclusion in Cells Infected With Hepatitis C Virus</p>

    <p>          Tscherne, D. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(8): 3693-3703</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900003</a> </p><br />

    <p>10.   61045   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          New Natural Intergenotypic (2/5) Recombinant of Hepatitis C Virus</p>

    <p>          Legrand-Abravanel, F. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(8): 4357-4362</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900068">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245692900068</a> </p><br />

    <p>11.   61046   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Parameters determining the relative efficacy of hydroxy-naphthoquinone inhibitors of the cytochrome bc1 complex</p>

    <p>          Kessl, Jacques J, Moskalev, Nikolai V, Gribble, Gordon W, Nasr, Mohamed, Meshnick, Steven R, and Trumpower, Bernard L</p>

    <p>          Biochimica et Biophysica Acta (BBA) - Bioenergetics <b>2007</b>.  1767(4): 319-326</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1S-4N4YMMW-1/2/06b9aa82b32b24e76a8037a02efb1a05">http://www.sciencedirect.com/science/article/B6T1S-4N4YMMW-1/2/06b9aa82b32b24e76a8037a02efb1a05</a> </p><br />

    <p>12.   61047   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Antimicrobial Activity of Marine Sponges From Uraba Gulf, Colombian Caribbean Region</p>

    <p>          Galeano, E. and Martinez, A.</p>

    <p>          Journal De Mycologie Medicale <b>2007</b>.  17(1): 21-24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245772400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245772400004</a> </p><br />
    <br clear="all">

    <p>13.   61048   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Synthesis, characterization and antimicrobial activity of new aliphatic sulfonamide</p>

    <p>          Ozbek, Neslihan, Katircioglu, Hikmet, Karacan, Nurcan, and Baykal, Tulay</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(15): 5105-5109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NS2GDD-5/2/ebeffa42266afe22d2de28d486c8af81">http://www.sciencedirect.com/science/article/B6TF8-4NS2GDD-5/2/ebeffa42266afe22d2de28d486c8af81</a> </p><br />

    <p>14.   61049   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Design, synthesis and molecular docking studies of novel triazole antifungal compounds</p>

    <p>          He, Qiu Qin, Li, Ke, Cao, Yong Bing, Dong, Huan Wen, Zhao, Li Hua, Liu, Chao Mei, and Sheng, Chun Quan</p>

    <p>          Chinese Chemical Letters <b>2007</b>.  18(6): 663-666</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B8G3X-4NVK227-3/2/7b977b09ded38b5bab211645261dcf1b">http://www.sciencedirect.com/science/article/B8G3X-4NVK227-3/2/7b977b09ded38b5bab211645261dcf1b</a> </p><br />

    <p>15.   61050   OI-LS-376; EMBASE-OI-6/11/2007</p>

    <p class="memofmt1-2">          Inhibitors of hepatitis C virus NS3.4A protease. Effect of P4 capping groups on inhibitory potency and pharmacokinetics</p>

    <p>          Perni, Robert B, Chandorkar, Gurudatt, Cottrell, Kevin M, Gates, Cynthia A, Lin, Chao, Lin, Kai, Luong, Yu-Ping, Maxwell, John P, Murcko, Mark A, Pitlik, Janos, Rao, Govinda, Schairer, Wayne C, Drie, John Van, and Wei, Yunyi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(12): 3406-3411</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NDDM03-1/2/7fbe660613e02d9dc3d24884ff9e8aa7">http://www.sciencedirect.com/science/article/B6TF9-4NDDM03-1/2/7fbe660613e02d9dc3d24884ff9e8aa7</a> </p><br />

    <p>16.   61051   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Update 2006/2007 - Treatment of Hepatitis</p>

    <p>          Anon</p>

    <p>          Drugs of the Future <b>2007</b>.  32(2): 187-190</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245698900010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245698900010</a> </p><br />

    <p>17.   61052   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Drugs Under Development for the Treatment of Hepatitis</p>

    <p>          Mealy, N., Lupone, B., and Tell, M.</p>

    <p>          Drugs of the Future <b>2007</b>.  32(2): 191-200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245698900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245698900011</a> </p><br />

    <p>18.   61053   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Drugs From Academia</p>

    <p>          Borman, S.</p>

    <p>          Chemical &amp; Engineering News <b>2007</b>.  85(16): 42-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245737800046">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245737800046</a> </p><br />
    <br clear="all">

    <p>19.   61054   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Relationship Between Mycobacterium Avium Subspecies Paratuberculosis, Il-1 Alpha, and Traf1 in Primary Bovine Monocyte-Derived Macrophages</p>

    <p>          Chiang, S. <i>et al.</i></p>

    <p>          Veterinary Immunology and Immunopathology <b>2007</b>.  116(3-4 ): 131-144</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245797600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245797600002</a> </p><br />

    <p>20.   61055   OI-LS-376; WOS-OI-6/1/2007</p>

    <p class="memofmt1-2">          Predicting Hcv Treatment Duration With an Hcv Protease Inhibitor Co-Administered With Peg-Ifn/Rbv by Modeling Both Wild-Type Virus and Low Level Resistant Variant Dynamics</p>

    <p>          Khunvichai, A. <i>et al.</i></p>

    <p>          Gastroenterology <b>2007</b>.  132(4): A740-A741</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606139">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245927606139</a> </p><br />

    <p>21.   61056   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis C Virus Replication and Expression by Small Interfering Rna Targeting Host Cellular Genes</p>

    <p>          Xue, Q. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100390">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100390</a> </p><br />

    <p>22.   61057   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Ribavirin Acts as Hepatitis C Virus Mutagen on Nonstructural 5b Quasispecies in Vitro and During Antiviral Therapy</p>

    <p>          Hofmann, W. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S164</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100429">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100429</a> </p><br />

    <p>23.   61058   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Favorable Cross-Resistance Profile of Two 432 Novel Hepatitis C Virus Inhibitors, Sch-503034 and Hcv-766, and Enhanced Anti-Replicon Activity Mediated by the Combined Use of Both Compounds</p>

    <p>          Howe, A. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S165</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100431">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100431</a> </p><br />

    <p>24.   61059   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Combinations of 2&#39;-Methylcytidine Analogs With Interferon Alpha-2b Lead to Synergistic Inhibition of Hepatitis C Virus Replication in a Replicon System</p>

    <p>          Bassit, L. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S217</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100570">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100570</a> </p><br />
    <br clear="all">

    <p>25.   61060   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Ketoprofen Plus Peg-Ifn Alpha-2a and Ribavirin as a Triple Treatment for Genotype 1 (G1) Chronic Hepatitis C (Chc): Results of a Phase Ii Randomized Controlled Trial</p>

    <p>          Gramenzi, A. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100610">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100610</a> </p><br />

    <p>26.   61061   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          R1479, a Novel Hepatitis C Polymerase Inhibitor: Lack of Pre-Existing Resistance Mutation Supports the Observed in Vivo High Barrier to Resistance</p>

    <p>          Le Pogam, S. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S235</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100620">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100620</a> </p><br />

    <p>27.   61062   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Novel Mode of Viral Decline During Telaprevir (Vx-950) and Peg-Ifn Combination Treatment Predicted by a New Combined Intracellular and Cellular Hepatitis C Viral Dynamics Model</p>

    <p>          Neumann, A. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S236</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100623">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100623</a> </p><br />

    <p>28.   61063   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          The Influence of Nucleoside Free Haart on the Treatment of Chronic Hepatitis C With Pegylated Interferon/Ribavirin Combination Treatment</p>

    <p>          Rockstroh, J. <i> et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S240</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100633">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100633</a> </p><br />

    <p>29.   61064   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Combination of Two Hepatitis C Virus Inhibitors, Sch 503034 and Nm107, Provides Enhanced Antireplicon Activity and Suppresses Emergence of Resistant Replicons</p>

    <p>          Ralston, R. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S298-S299</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100788">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100788</a> </p><br />

    <p>30.   61065   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Purines Bearing Phenanthroline or Bipyridine Ligands and Their Ru-Ii Complexes in Position 8 as Model Compounds for Electrochemical Dna Labeling - Synthesis, Crystal Structure, Electrochemistry, Quantum Chemical Calculations, Cytostatic and Antiviral Activity</p>

    <p>          Vrabel, M. <i>et al.</i></p>

    <p>          European Journal of Inorganic Chemistry <b>2007</b>.(12): 1752-1769</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246404900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246404900016</a> </p><br />
    <br clear="all">

    <p>31.   61066   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Inhibitory Effects of Various Type Ifn on the Hcv Rna Replication of Hcv Replicon Cells</p>

    <p>          Fei, R. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100441">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100441</a> </p><br />

    <p>32.   61067   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Novel 4-Azido-2-Deoxy-Nucleoside Analogs Are Potent Inhibitors of Ns5b-Dependent Hcv Replication</p>

    <p>          Smith, D. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S190</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100498">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100498</a> </p><br />

    <p>33.   61068   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Ach-806: a Potent Inhibitor of Hcv Replication With a Novel Mechanism of Action</p>

    <p>          Huang, M. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S221</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100582">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100582</a> </p><br />

    <p>34.   61069   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Inhibition of Hcv Replication by Psi-6130: Mechanism of Biochemical Activation and Inhibition</p>

    <p>          Furman, P. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S224</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100589">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100589</a> </p><br />

    <p>35.   61070   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Effects of Telapravir (Vx950) in a Liver Biopsy Model of Hcv Infection: Genotype 1 Virions Are Inhibited in Vitro but Other Genotypes Are Less Responsive</p>

    <p>          Hibbert, L. and Foster, G.</p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S227</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100598">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100598</a> </p><br />

    <p>36.   61071   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Inhibition of Hcv Replication by Psi-6130: Characterization of Activity in the Hcv Replicon System</p>

    <p>          Jiang, W. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S228</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100601">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100601</a> </p><br />

    <p>37.   61072   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          R1479, a Novel Nucleoside Analog and Potent Inhibitor of Hcv Replication, Does Not Affect Ribavirin-Induced Hemolysis and Erythrocyte Fragility in Vitro</p>

    <p>          Jiang, W. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S228</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100602">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100602</a> </p><br />
    <br clear="all">

    <p>38.   61073   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Nim811, a Cyclophilin Inhibitor, and Nm107, an Hcv Polymerase Inhibitor, Synergistically Inhibits Hcv Replication and Suppresses the Emergence of Resistance in Vitro</p>

    <p>          Lin, K. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S230</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100607">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100607</a> </p><br />

    <p>39.   61074   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Characterization of Pharmacokinetic/Pharmacodynamic Parameters for the Novel Hcv Polymerase Inhibitor a-848837</p>

    <p>          Molla, A. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S234-S235</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100618">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100618</a> </p><br />

    <p>40.   61075   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Short-Term Antiviral Activity and Safety of Ach-806 (Gs-9132), an Ns4a Antagonist, in Hcv Genotype I Infected Individuals</p>

    <p>          Pottage, J. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S294-S295</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100778">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100778</a> </p><br />

    <p>41.   61076   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          The Reduction of Both Cyclophilin B and Hcv-Rna by the Cyclophilin Inhibitor Debio-025 Confirms the Importance of Cyclophilin B for Hcv Replication in Man</p>

    <p>          Gallay, R. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S296</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100782">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100782</a> </p><br />

    <p>42.   61077   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          In Vitro Resistance Selection to the Hcv Nucleoside Polymerase Inhibitor R1479 and to the Protease Inhibitor Vx-950</p>

    <p>          Mccown, M. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S297-S298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100785">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100785</a> </p><br />

    <p>43.   61078   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Hcv Antiviral Activity and Resistance Analysis in Chronically Infected Chimpanzees Treated With Ns3/4a Protease and Ns513 Polymerase Inhibitors</p>

    <p>          Olsen, D. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2007</b>.  46: S298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100786">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246555100786</a> </p><br />
    <br clear="all">

    <p>44.   61079   OI-LS-376; WOS-OI-6/8/2007</p>

    <p class="memofmt1-2">          Il-2-Stimulated Natural Killer Cells Inhibit Hcv Replication: a Novel Concept to Prevent the Progression of Hcv Reinfection in Liver-Transplant Recipients Infected With Hcv.</p>

    <p>          Ohira, M. <i>et al.</i></p>

    <p>          American Journal of Transplantation <b>2007</b>.  7: 328</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246370201129">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246370201129</a> </p><br />

    <p>45.   61080   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          NAD(+)-dependent DNA ligase (Rv3014c) from Mycobacterium tuberculosis: Novel structure-function relationship and identification of a specific inhibitor</p>

    <p>          Srivastava, SK, Dube, D, Kukshal, V, Jha, AK, Hajela, K, and Ramachandran, R</p>

    <p>          Proteins <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17557328&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17557328&amp;dopt=abstract</a> </p><br />

    <p>46.   61081   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Phenotypic and structural analyses of HCV NS3 protease ARG155 variants: Sensitivity to telaprevir (VX-950) and interferon alpha</p>

    <p>          Zhou, Y, Muh, U, Hanzelka, BL, Bartels, DJ, Wei, Y, Rao, BG, Brennan, DL, Tigges, AM, Swenson, L, Kwong, AD, and Lin, C</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17556358&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17556358&amp;dopt=abstract</a> </p><br />

    <p>47.   61082   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Synthesis, characterisation and antimicrobial activity of copper(II) and manganese(II) complexes of coumarin-6,7-dioxyacetic acid (cdoaH(2)) and 4-methylcoumarin-6,7-dioxyacetic acid (4-MecdoaH(2)): X-ray crystal structures of [Cu(cdoa)(phen)(2)].8.8H(2)O and [Cu(4-Mecdoa)(phen)(2)].13H(2)O (phen=1,10-phenanthroline)</p>

    <p>          Creaven, BS, Egan, DA, Karcz, D, Kavanagh, K, McCann, M, Mahon, M, Noble, A, Thati, B, and Walsh, M</p>

    <p>          J Inorg Biochem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17555821&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17555821&amp;dopt=abstract</a> </p><br />

    <p>48.   61083   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Investigating the Metabolic Capabilities of Mycobacterium tuberculosis H37Rv Using the in silico Strain iNJ661 and Proposing Alternative Drug Targets</p>

    <p>          Jamshidi, N and Palsson, BO</p>

    <p>          BMC Syst Biol <b>2007</b>.  1(1): 26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17555602&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17555602&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>49.   61084   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Design and Synthesis of Classical and Nonclassical 6-Arylthio-2,4-diamino-5-ethylpyrrolo[2,3-d]pyrimidines as Antifolates</p>

    <p>          Gangjee, A, Zeng, Y, Talreja, T, McGuire, JJ, Kisliuk, RL, and Queener, SF</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17552508&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17552508&amp;dopt=abstract</a> </p><br />

    <p>50.   61085   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Hepatitis C virus: Virology, diagnosis and management of antiviral therapy</p>

    <p>          Chevaliez, S and Pawlotsky, JM</p>

    <p>          World J Gastroenterol <b>2007</b>.  13(17): 2461-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17552030&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17552030&amp;dopt=abstract</a> </p><br />

    <p>51.   61086   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Life and death in bovine monocytes: The fate of Mycobacterium avium subsp. paratuberculosis</p>

    <p>          Woo, SR, Heintz, JA, Albrecht, R, Barletta, RG, and Czuprynski, CJ</p>

    <p>          Microb Pathog <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548182&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548182&amp;dopt=abstract</a> </p><br />

    <p>52.   61087   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Disaccharide analogs as probes for glycosyltransferases in Mycobacterium tuberculosis</p>

    <p>          Pathak, AK, Pathak, V, Seitz, L, Gurcha, SS, Besra, GS, Riordan, JM, and Reynolds, RC</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17544276&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17544276&amp;dopt=abstract</a> </p><br />

    <p>53.   61088   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          (2R,3S)-(+)- and (2S,3R)-(-)-Halofuginone lactate: Synthesis, absolute configuration, and activity against Cryptosporidium parvum</p>

    <p>          Linder, MR, Heckeroth, AR, Najdrowski, M, Daugschies, A, Schollmeyer, D, and Miculka, C</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17544270&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17544270&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>54.   61089   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Substrate and inhibitor specificity of Mycobacterium avium dihydrofolate reductase</p>

    <p>          Bock, RA, Soulages, JL, and Barrow, WW</p>

    <p>          FEBS J <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542991&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542991&amp;dopt=abstract</a> </p><br />

    <p>55.   61090   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          C-3 Alkyl/Arylalkyl-2,3-dideoxy Hex-2-enopyranosides as Antitubercular Agents: Synthesis, Biological Evaluation, and QSAR Study</p>

    <p>          Saquib, M, Gupta, MK, Sagar, R, Prabhakar, YS, Shaw, AK, Kumar, R, Maulik, PR, Gaikwad, AN, Sinha, S, Srivastava, AK, Chaturvedi, V, Srivastava, R, and Srivastava, BS</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542574&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542574&amp;dopt=abstract</a> </p><br />

    <p>56.   61091   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Tropolone and its derivatives as inhibitors of the helicase activity of hepatitis C virus nucleotide triphosphatase/helicase</p>

    <p>          Borowski, P, Lang, M, Haag, A, and Baier, A</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(2): 103-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542155&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542155&amp;dopt=abstract</a> </p><br />

    <p>57.   61092   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Oxime and Oxime Ether Derivatives of 1,4-Benzothiazine Related to Oxiconazole</p>

    <p>          Milanese, L, Giacche, N, Schiaffella, F, Vecchiarelli, A, Macchiarulo, A, and Fringuelli, R</p>

    <p>          ChemMedChem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17541993&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17541993&amp;dopt=abstract</a> </p><br />

    <p>58.   61093   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Drug susceptibility testing of Mycobacterium tuberculosis complex using a high throughput, reproducible, absolute concentration method</p>

    <p>          van Klingeren, B, Dessens-Kroon, M, van der Laan, T, Kremer, K, and van Soolingen, D</p>

    <p>          J Clin Microbiol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17537932&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17537932&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>59.   61094   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          A Mycobacterium marinum Zone of Inhibition Assay as a Method for Screening Potential Antimycobacterial Compounds from Marine Extracts</p>

    <p>          Barker, LP, Lien, BA, Brun, OS, Schaak, DD, McDonough, KA, and Chang, LC</p>

    <p>          Planta Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17534789&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17534789&amp;dopt=abstract</a> </p><br />

    <p>60.   61095   OI-LS-376; PUBMED-OI-6/11/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of galactofuranosyl alkyl thioglycosides as inhibitors of mycobacteria</p>

    <p>          Davis, CB, Hartnell, RD, Madge, PD, Owen, DJ, Thomson, RJ, Chong, AK, Coppel, RL, and Itzstein, MV</p>

    <p>          Carbohydr Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17517379&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17517379&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
