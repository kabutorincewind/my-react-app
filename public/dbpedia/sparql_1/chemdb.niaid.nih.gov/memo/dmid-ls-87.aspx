

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-87.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wkalioke3A223zF+BvVXT5b3iPa6RJCv6J/Gb26vMEtZ1a4bSaoW/CVjA8ZZpBXE/HKuPy6VysUrgQG//Ot9kH1CxhwNByhixBUR4Ug4/JpcNmtU13IAqTm3emM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="259232CF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-87-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56178   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of Some Pyrazoloquinolines as Inhibitors of Herpes Simplex Virus Type 1 Replication</p>

    <p>          Bekhit, AA, El-Sayed, OA, Aboul-Enein, HY, Siddiqui, YM, and Al-Ahdal, MN</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15736285&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15736285&amp;dopt=abstract</a> </p><br />

    <p>2.     56179   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Newer respiratory virus infections: human metapneumovirus, avian influenza virus, and human coronaviruses</p>

    <p>          Fouchier, RA,  Rimmelzwaan, GF, Kuiken, T, and Osterhaus, AD</p>

    <p>          Curr Opin Infect Dis <b>2005</b>.  18(2): 141-146</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15735418&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15735418&amp;dopt=abstract</a> </p><br />

    <p>3.     56180   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A preparation of heterocyclic compounds, useful for the treatment of diseases associated with TNF-a, IL-1b, IL-6, and COX-1/COX-2</p>

    <p>          Dominguez, Celia, Harvey, Timothy Scot, Liu, Longbin, and Siegmund, Aaron</p>

    <p>          PATENT:  US <b>2005020592</b>  ISSUE DATE:  20050127</p>

    <p>          APPLICATION: 2004-45916  PP: 28 pp.</p>

    <p>          ASSIGNEE:  (Amgen Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     56181   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Architecture of Replication Compartments Formed during Epstein-Barr Virus Lytic Replication</p>

    <p>          Daikoku, T, Kudoh, A, Fujita, M, Sugaya, Y, Isomura, H, Shirata, N, and Tsurumi, T</p>

    <p>          J Virol <b>2005</b> .  79(6): 3409-3418</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731235&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731235&amp;dopt=abstract</a> </p><br />

    <p>5.     56182   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hcmv Activity of Novel 2 &#39;,3 &#39;,4 &#39;-Trimethyl Branched Carbocyclic Nucleosides</p>

    <p>          Kim, A. and Hong, J.</p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(1): 63-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226785500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226785500005</a> </p><br />

    <p>6.     56183   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory effect of cyclosporine A on hepatitis B virus replication in vitro and its possible mechanisms</p>

    <p>          Xia, WL, Shen, Y, and Zheng, SS</p>

    <p>          Hepatobiliary Pancreat Dis Int <b>2005</b>.  4(1): 18-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15730912&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15730912&amp;dopt=abstract</a> </p><br />

    <p>7.     56184   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Screening of inhibitors for influenza A virus using high-performance affinity chromatography and combinatorial peptide libraries</p>

    <p>          Zhao, R, Fanga, C, Yu, X, Liu, Y, Luo, J, Shangguan, D, Xiong, S, Su, T, and Liu, G</p>

    <p>          J Chromatogr A  <b>2005</b>.  1064(1): 59-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15729820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15729820&amp;dopt=abstract</a> </p><br />

    <p>8.     56185   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Efficacy and Toxicity of Zinc Salts as Candidate Topical Microbicides against Vaginal Herpes Simplex Virus Type 2 Infection</p>

    <p>          Bourne, N, Stegall, R, Montano, R, Meador, M, Stanberry, LR, and Milligan, GN</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 1181-1183</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728922&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728922&amp;dopt=abstract</a> </p><br />

    <p>9.     56186   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel acyclic nucleoside phosphonate analogues with potent anti-hepatitis B virus activities</p>

    <p>          Ying, C, Holy, A, Hockova, D, Havlas, Z, De Clercq, E, and Neyts, J</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 1177-1180</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728921&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728921&amp;dopt=abstract</a> </p><br />

    <p>10.   56187   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Activity and Mechanism of Action of Methylenecyclopropane Analogs of Nucleosides against Herpesvirus Replication</p>

    <p>          Kern, ER, Kushner, NL, Hartline, CB, Williams-Aziz, SL, Harden, EA, Zhou, S, Zemlicka, J, and Prichard, MN</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 1039-1045</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728900&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728900&amp;dopt=abstract</a> </p><br />

    <p>11.   56188   LR-13187; DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiadenovirus activities of several classes of nucleoside and nucleotide analogues</p>

    <p>          Naesens, L, Lenaerts, L, Andrei, G, Snoeck, R, Van Beers, D, Holy, A, Balzarini, J, and De Clercq, E</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 1010-1016</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728896&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728896&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   56189   LR-13188; DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mechanism of Action of T-705 against Influenza Virus</p>

    <p>          Furuta, Y, Takahashi, K, Kuno-Maekawa, M, Sangawa, H, Uehara, S, Kozaki, K, Nomura, N, Egawa, H, and Shiraki, K</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 981-986</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728892&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728892&amp;dopt=abstract</a> </p><br />

    <p>13.   56190   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human cytomegalovirus resistance to antiviral drugs</p>

    <p>          Gilbert, C and Boivin, G</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 873-883</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728878&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728878&amp;dopt=abstract</a> </p><br />

    <p>14.   56191   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A reconstituted replication and transcription system for Ebola virus Reston and comparison with Ebola virus Zaire</p>

    <p>          Boehmann, Yannik, Enterlein, Sven, Randolf, Anke, and Muehlberger, Elke</p>

    <p>          Virology <b>2005</b>.  332(1): 406-417</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   56192   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          &quot;Strong reasons make strong actions&quot; - The antiviral efficacy of NS3/4A protease inhibitors</p>

    <p>          Lemon, SM, Yi, M, and Li, K</p>

    <p>          Hepatology <b>2005</b>.  41(3): 671-674</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15723328&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15723328&amp;dopt=abstract</a> </p><br />

    <p>16.   56193   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The majority of the nucleotides in the top loop of the genomic 3&#39; terminal stem loop structure are cis-acting in a West Nile virus infectious clone</p>

    <p>          Elghonemy, Salwa, Davis, William G, and Brinton, Margo A</p>

    <p>          Virology <b>2005</b>.  331(2): 238-246</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   56194   LR-13236; DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity of Glycyrrhizic Acid Derivatives against SARS-Coronavirus</p>

    <p>          Hoever, G, Baltina, L, Michaelis, M, Kondratenko, R, Baltina, L, Tolstikov, GA, Doerr, HW, and Cinatl, J Jr</p>

    <p>          J Med Chem <b>2005</b>.  48(4): 1256-1259</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715493&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715493&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   56195   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Differential Induction of Antiviral Effects Against West Nile Virus in Primary Mouse Macrophages Derived From Flavivirus-Susceptible and Congenic Resistant Mice by Alpha/Beta Interferon and Poly(I-C)</p>

    <p>          Pantelic, L., Sivakumaran, H., and Urosevic, N.</p>

    <p>          Journal of Virology <b>2005</b>.  79(3): 1753-1764</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226634300041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226634300041</a> </p><br />

    <p>19.   56196   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel [2&#39;,5&#39;-Bis-O-(tert-butyldimethylsilyl)-beta-d-ribofuranosyl]- 3&#39;-spiro-5&#39; &#39;-(4&#39; &#39;-amino-1&#39; &#39;,2&#39; &#39;-oxathiole-2&#39; &#39;,2&#39; &#39;-dioxide) Derivatives with Anti-HIV-1 and Anti-Human-Cytomegalovirus Activity</p>

    <p>          de Castro, S,  Lobaton, E, Perez-Perez, MJ, San-Felix, A, Cordeiro, A, Andrei, G, Snoeck, R, De Clercq, E, Balzarini, J, Camarasa, MJ, and Velazquez, S</p>

    <p>          J Med Chem <b>2005</b>.  48(4): 1158-1168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715482&amp;dopt=abstract</a> </p><br />

    <p>20.   56197   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phenolics with antiviral activity from Millettia erythrocalyx and Artocarpus lakoocha</p>

    <p>          Likhitwitayawuid, K, Sritularak, B, Benchanak, K, Lipipun, V, Mathew, J, and Schinazi, RF</p>

    <p>          Nat Prod Res <b>2005</b>.  19(2): 177-182</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715263&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715263&amp;dopt=abstract</a> </p><br />

    <p>21.   56198   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sense antiviral compound and method for treating ssrna viral infection</p>

    <p>          Iversen, Patrick L</p>

    <p>          PATENT:  WO <b>2005013905</b>  ISSUE DATE:  20050217</p>

    <p>          APPLICATION: 2004  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Avi Biopharma, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   56199   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A preparation of bicyclic imidazole derivatives, useful for the treatment of viral infections mediated by flaviviridae family of viruses</p>

    <p>          Schmitz, Franz Ulrich, Roberts, Christopher Don, Griffith, Ronald Conrad, Botyanszki, Janos, Gezginci, Mikail Hakan, Gralapp, Joshua Michael, Shi, Dong Fang, and Liehr, Sebastian JR</p>

    <p>          PATENT:  WO <b>2005012288</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004  PP: 327 pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.   56200   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oseltamivir (Tamiflu(R)) and its potential for use in the event of an influenza pandemic</p>

    <p>          Ward, P, Small, I, Smith, J, Suter, P, and Dutkowski, R</p>

    <p>          J Antimicrob Chemother <b>2005</b>.  55 Suppl 1: i5-i21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15709056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15709056&amp;dopt=abstract</a> </p><br />

    <p>24.   56201   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nuclear import in viral infections</p>

    <p>          Greber, UF and Fornerod, M</p>

    <p>          Current Topics in Microbiology and Immunology <b>2005</b>.  285(Membrane Trafficking in Viral Replication): 109-138</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   56202   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Considerations for Use of Recombinant Adenoviral Vectors: Dose Effect on Hepatic Cytochromes P450</p>

    <p>          Callahan, S. <i>et al.</i></p>

    <p>          Journal of Pharmacology and Experimental Therapeutics <b>2005</b>.  312(2): 492-501</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226367100009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226367100009</a> </p><br />

    <p>26.   56203   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparing capsid assembly of primate lentiviruses and hepatitis B virus using cell-free systems</p>

    <p>          Lingappa, JR,  Newman, MA, Klein, KC, and Dooher, JE</p>

    <p>          Virology <b>2005</b>.  333(1): 114-123</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708597&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708597&amp;dopt=abstract</a> </p><br />

    <p>27.   56204   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Reporter cell lines for the detection of herpes simplex viruses</p>

    <p>          Kung, Szu-Hao</p>

    <p>          Methods in Molecular Biology (Totowa, NJ, United States) <b>2005</b>.  292(DNA Viruses): 73-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   56205   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Agents and strategies in development for improved management of herpes simplex virus infection and disease</p>

    <p>          Kleymann, Gerald</p>

    <p>          Expert Opinion on Investigational Drugs <b>2005</b>.  14(2): 135-161</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>29.   56206   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An in vitro model of hepatitis C virion production</p>

    <p>          Heller, T, Saito, S, Auerbach, J, Williams, T, Moreen, TR, Jazwinski, A, Cruz, B, Jeurkar, N, Sapp, R,  Luo, G, and Liang, TJ</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.  102(7): 2579-2583</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15701697&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15701697&amp;dopt=abstract</a> </p><br />

    <p>30.   56207   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of anti-herpetic and antioxidant activities, and cytotoxic and genotoxic effects of synthetic alkyl-esters of gallic acid</p>

    <p>          Savi, Luciane A, Leal, Paulo C, Vicira, Tiago O, Rosso, Rober, Nunes, Ricardo J, Yunes, Rosendo A, Creczynski-Pasa, Tania B, Barardi, Celia RM, and Slmoes, Claudia MO</p>

    <p>          Arzneimittel Forschung <b>2005</b>.  55(1): 66-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   56208   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and anti-viral activity of a series of d- and l-2&#39;-deoxy-2&#39;-fluororibonucleosides in the subgenomic HCV replicon system</p>

    <p>          Shi, J, Du, J, Ma, T, Pankiewicz, KW, Patterson, SE, Tharnish, PM, McBrayer, TR, Stuyver, LJ, Otto, MJ, Chu, CK, Schinazi, RF, and Watanabe, KA</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(5): 1641-1652</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698782&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15698782&amp;dopt=abstract</a> </p><br />

    <p>32.   56209   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 2-Amino-6-(4-[C-11]Methoxyphenylthio)9-[2-(Phosphonomethoxy)Ethyl]Purine Bis(2,2,2-Trifluoroethyl) Ester as a Novel Potential Pet Gene Reporter Probe for Hbv and Hsv-Tk in Cancers</p>

    <p>          Wang, J. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(2): 549-556</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4DTKBCM-5/2/e6ab5157c967c3b7fc48d7a220ae5a26">http://www.sciencedirect.com/science/article/B6TF8-4DTKBCM-5/2/e6ab5157c967c3b7fc48d7a220ae5a26</a> </p><br />

    <p>33.   56210   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, in vitro cytotoxic and antiviral activity of cis-[Pt(R(-) and S(+)-2-alpha-hydroxybenzylbenzimidazole)(2)Cl(2)] complexes</p>

    <p>          Gokce, M, Utku, S, Gur, S, Ozkul, A, and Gumus, F</p>

    <p>          Eur J Med Chem <b>2005</b>.  40(2): 135-141</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15694648&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15694648&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   56211   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human immunodeficiency virus-reverse transcriptase inhibition and hepatitis C virus RNA-dependent RNA polymerase inhibition activities of fullerene derivatives</p>

    <p>          Mashino, T, Shimotohno, K, Ikegami, N, Nishikawa, D, Okuda, K, Takahashi, K, Nakamura, S, and Mochizuki, M</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(4): 1107-1109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15686922&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15686922&amp;dopt=abstract</a> </p><br />

    <p>35.   56212   DMID-LS-87; PUBMED-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Azaphilones, furanoisophthalides, and amino acids from the extracts of Monascus pilosus-fermented rice (red-mold rice) and their chemopreventive effects</p>

    <p>          Akihisa, T, Tokuda, H, Yasukawa, K, Ukiya, M, Kiyota, A, Sakamoto, N, Suzuki, T, Tanabe, N, and Nishino, H</p>

    <p>          J Agric Food Chem <b>2005</b>.  53(3): 562-565</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15686402&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15686402&amp;dopt=abstract</a> </p><br />

    <p>36.   56213   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nidovirus Sialate-O-Acetylesterases: EVOLUTION AND SUBSTRATE SPECIFICITY OF CORONAVIRAL AND TOROVIRAL RECEPTOR-DESTROYING ENZYMES</p>

    <p>          Smits, Saskia L, Gerwig, Gerrit J, van Vliet, Arno LW, Lissenberg, Arjen, Briza, Peter, Kamerling, Johannis P, Vlasak, Reinhard, and de Groot, Raoul J</p>

    <p>          Journal of Biological Chemistry <b>2005</b>.  280(8): 6933-6941</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   56214   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The novel parainfluenza virus hemagglutinin-neuraminidase inhibitor BCX 2798 prevents lethal synergism between a paramyxovirus and Streptococcus pneumoniae</p>

    <p>          Alymova, Irina V, Portner, Allen, Takimoto, Toru, Boyd, Kelli L, Babu, YSudhakara, and McCullers, Jonathan A</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(1): 398-405</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   56215   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, X-Ray Crystal Structure Study, and Cytostatic and Antiviral Evaluation of the Novel Cycloalkyl-N-Aryl-Hydroxamic Acids</p>

    <p>          Barbaric, M. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(3): 884-887</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226881300026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226881300026</a> </p><br />
    <br clear="all">

    <p>39.   56216   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptide inhibitors of coronavirus derived from Spike protein S2 subunit heptad repeat regions</p>

    <p>          Kim, Peter S, Eckert, Debra, Garsky, Victor M, Freidinger, Roger M, Geleziunas, Romas, and Pessi, Antonello</p>

    <p>          PATENT:  WO <b>2005002500</b>  ISSUE DATE:  20050113</p>

    <p>          APPLICATION: 2004  PP: 69 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA and Istituto di Ricerche di Biologia Molecolare P. Angeletti, S. p. A.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   56217   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Stereoselective Synthesis and Antiviral Activity of Novel 4 &#39;(Alpha)-Hydroxymethyl and 6 &#39;(Alpha)-Methyl Dually Branched Carbocyclic Nucleosides</p>

    <p>          Kim, J., Choi, B., and Hong, J.</p>

    <p>          Bulletin of the Korean Chemical Society <b>2004</b>.  25(12): 1812-1816</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226854800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226854800011</a> </p><br />

    <p>41.   56218   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Generation and Characterization of Recombinant Influenza a (H1n1) Viruses Harboring Amantadine Resistance Mutations</p>

    <p>          Abed, Y., Goyette, N., and Boivin, G.</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(2): 556-559</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226709900012</a> </p><br />

    <p>42.   56219   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New antiviral drugs, vaccines and classic public health interventions against SARS coronavirus</p>

    <p>          Oxford, John S, Balasingam, Shobana, Chan, Charlotte, Catchpole, Andrew, and Lambkin, Robert</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2005</b>.  16(1): 13-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   56220   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Methods of treating severe acute respiratory syndrome virus infections</p>

    <p>          Denison, Mark</p>

    <p>          PATENT:  WO <b>2005011649</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (Vanderbilt University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>44.   56221   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Characterization of H5n1 Influenza a Viruses Isolated During the 2003-2004 Influenza Outbreaks in Japan</p>

    <p>          Mase, M. <i>et al.</i></p>

    <p>          Virology <b>2005</b>.  332(1): 167-176</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226688500016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226688500016</a> </p><br />

    <p>45.   56222   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of tetrahydrocarbazoles useful in the treatment of diseases associated with human papillomavirus infection</p>

    <p>          Boggs, Sharon Davis and Gudmundsson, Kristjan</p>

    <p>          PATENT:  WO <b>2005005386</b>  ISSUE DATE:  20050120</p>

    <p>          APPLICATION: 2004  PP: 70 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   56223   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Sulfonamide Derivatives</p>

    <p>          Supuran, C. <i>et al.</i></p>

    <p>          Mini-Reviews in Medicinal Chemistry <b>2004</b>.  4(2): 189-200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226486600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226486600006</a> </p><br />

    <p>47.   56224   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of substituted arylthioureas as inhibitors of viral replication</p>

    <p>          Phadke, Avinash, Quinn, Jesse, Ohkanda, Junko, Thurkauf, Andrew, Shen, Yiping, Liu, Cuixian, Chen, Dawei, and Li, Shouming</p>

    <p>          PATENT:  WO <b>2005007601</b>  ISSUE DATE:  20050127</p>

    <p>          APPLICATION: 2004  PP: 80 pp.</p>

    <p>          ASSIGNEE:  (Achillion Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>48.   56225   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Novel L- and D-Amino Acid Derivatives of Hydroxyurea and Hydantoins: Synthesis, X-Ray Crystal Structure Study, and Cytostatic and Antiviral Activity Evaluations</p>

    <p>          Opacic, N. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(2): 475-482</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226591700017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226591700017</a> </p><br />

    <p>49.   56226   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Colorimetric lactate dehydrogenase (LDH) assay for evaluation of antiviral activity against bovine viral diarrhoea virus (BVDV) in vitro</p>

    <p>          Baba, Chiaki, Yanagida, Koichiro, Kanzaki, Tamotsu, and Baba, M</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2005</b>.  16(1): 33-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>50.   56227   DMID-LS-87; SCIFINDER-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Replicon cell culture system as a valuable tool in antiviral drug discovery against hepatitis C virus</p>

    <p>          Horscroft, Nigel, Lai, Vicky CH, Cheney, Wayne, Yao, Nanhua, Wu, Jim Z, Hong, Zhi, and Zhong, Weidong</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2005</b>.  16(1): 1-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>51.   56228   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Human Papillomavirus E6 and E7 Inducible Oncogene, Hwapl, Exhibits Potential as a Therapeutic Target</p>

    <p>          Kuroda, M. <i>et al.</i></p>

    <p>          British Journal of Cancer <b>2005</b>.  92(2): 290-293</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226543800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226543800016</a> </p><br />

    <p>52.   56229   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cooperativity of Actin and Microtubule Elements During Replication of Respiratory Syncytial Virus</p>

    <p>          Kallewaard, N., Boxena, A., and Crowe, J.</p>

    <p>          Virology <b>2005</b>.  331(1): 73-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226210500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226210500006</a> </p><br />
    <br clear="all">

    <p>53.   56230   DMID-LS-87; WOS-DMID-3/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polymorphism in the Genome of Non-Passaged Human Polyomavirus Bk: Implications for Cell Tropism and the Pathological Role of the Virus</p>

    <p>          Moens, U. and Van Ghelue, M.</p>

    <p>          Virology <b>2005</b>.  331(2): 209-231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226329300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226329300001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
