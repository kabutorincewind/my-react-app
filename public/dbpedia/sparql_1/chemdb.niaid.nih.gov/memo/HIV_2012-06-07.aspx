

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-06-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="poKEEFWtxhRU1NaPlULGUSTROnO1/dURu5lxE7p8iPyY7RNdth23FceLcvEbs+P9FyM8EW1VaTETJHZwDyxYtcb/NfmTP82XjvnGr4nksWGgK+6Jkk0nKTd3QCE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1FF05743" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: May 25 - June 7, 2012</h1>

    <p class="memofmt2-2">PubMed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22426469">Amino Acid Derivatives of the (-) Enantiomer of Gossypol Are Effective Fusion Inhibitors of Human Immunodeficiency Virus Type 1.</a> An, T., W. Ouyang, W. Pan, D. Guo, J. Li, L. Li, G. Chen, J. Yang, S. Wu, and P. Tien, Antiviral Research, 2012. 94(3): p. 276-287; PMID[22426469].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22645343">Identification of the Platelet-derived Chemokine CXCL4/PF-4 as a Broad-Spectrum HIV-1 Inhibitor.</a> Auerbach, D.J., Y. Lin, H. Miao, R. Cimbro, M.J. Difiore, M.E. Gianolini, L. Furci, P. Biswas, A.S. Fauci, and P. Lusso, Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>; PMID[22645343].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22512549">Chemistry and Biology of Fascaplysin, a Potent Marine-derived CDK-4 Inhibitor.</a> Bharate, S.B., S. Manda, N. Mupparapu, N. Battini, and R.A. Vishwakarma, Mini-Reviews in Medicinal Chemistry, 2012. 12(7): p. 650-664; PMID[22512549].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22664975">Small Molecule Inhibitors of the LEDGF/p75 Binding Site of Integrase (LEDGINs) Block HIV Replication and Modulate Integrase Multimerization.</a> Christ, F., S. Shaw, J. Demeulemeester, B.A. Desimmie, A. Marchand, S. Butler, W. Smets, P. Chaltin, M. Westby, Z. Debyser, and C. Pickford, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22664975].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22665020">Unique Antineoplastic Profile of Saquinavir-NO, a Novel NO-derivative of the Protease Inhibitor Saquinavir, on the in vitro and in vivo Tumor Formation of A375 Human Melanoma Cells.</a> Donia, M., K. Mangano, P. Fagone, R. De Pasquale, F. Dinotta, M. Coco, J. Padron, Y. Al-Abed, G.A. Giovanni Lombardo, D. Maksimovic-Ivanic, S. Mijatovic, M.B. Zocca, V. Perciavalle, S. Stosic-Grujicic, and F. Nicoletti, Oncology Reports, 2012. <b>[Epub ahead of print]</b>; PMID[22665020].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22587761">Fullerene Derivative as anti-HIV Protease Inhibitor: Molecular Modeling and QSAR Approaches.</a> Ibrahim, M., N.A. Saleh, W.M. Elshemey, and A.A. Elsayed, Mini-Reviews in Medicinal Chemistry, 2012. 12(6): p. 447-451; PMID[22587761].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22507247">Design of Modified U1i Molecules against HIV-1 RNA.</a> Knoepfel, S.A., A. Abad, X. Abad, P. Fortes, and B. Berkhout, Antiviral Research, 2012. 94(3): p. 208-216; PMID[22507247].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22647699">Inhibiting Early-stage Events in HIV-1 Replication by Small-molecule Targeting of the HIV-1 Capsid.</a> Kortagere, S., N. Madani, M.K. Mankowski, A. Schon, I. Zentner, G. Swaminathan, A. Princiotto, K. Anthony, A. Oza, L.J. Sierra, S.R. Passic, X. Wang, D.M. Jones, E. Stavale, F.C. Krebs, J. Martin-Garcia, E. Freire, R.G. Ptak, J. Sodroski, S. Cocklin, and A.B. Smith, 3rd, Journal of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22647699].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22496222">Distinct Effects of Two HIV-1 Capsid Assembly Inhibitor Families That Bind the Same Site within the N-terminal Domain of the Viral CA Protein.</a> Lemke, C.T., S. Titolo, U. von Schwedler, N. Goudreau, J.F. Mercier, E. Wardrop, A.M. Faucher, R. Coulombe, S.S. Banik, L. Fader, A. Gagnon, S.H. Kawai, J. Rancourt, M. Tremblay, C. Yoakim, B. Simoneau, J. Archambault, W.I. Sundquist, and S.W. Mason, Journal of Virology, 2012. 86(12): p. 6643-6655; PMID[22496222].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21810081">Isolation and Identification of a Novel Polysaccharide-peptide Complex with Antioxidant, Anti-proliferative and Hypoglycaemic Activities from the Abalone Mushroom.</a> Li, N., L. Li, J.C. Fang, J.H. Wong, T.B. Ng, Y. Jiang, C.R. Wang, N.Y. Zhang, T.Y. Wen, L.Y. Qu, P.Y. Lv, R. Zhao, B. Shi, Y.P. Wang, X.Y. Wang, and F. Liu, Bioscience Reports, 2012. 32(3): p. 221-228; PMID[21810081].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22641382">Potent and Broad Neutralization of HIV-1 by a Llama Antibody Elicited by Immunization.</a> McCoy, L.E., A.F. Quigley, N.M. Strokappe, B. Bulmer-Thomas, M.S. Seaman, D. Mortier, L. Rutten, N. Chander, C.J. Edwards, R. Ketteler, D. Davis, T. Verrips, and R.A. Weiss, The Journal of Experimental Medicine, 2012. 209(6): p. 1091-1103; PMID[22641382].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21895456">In Vitro Antiviral Activities of Extracts Derived from Daucus maritimus Seeds.</a> Miladi, S., N. Abid, C. Debarnot, M. Damak, B. Canard, M. Aouni, and B. Selmi, Natural Product Research, 2012. 26(11): p. 1027-1032; PMID[21895456].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21866921">Toward Eradicating HIV Reservoirs in the Brain: Inhibiting P-glycoprotein at the Blood-brain Barrier with Prodrug Abacavir Dimers.</a> Namanja, H.A., D. Emmert, D.A. Davis, C. Campos, D.S. Miller, C.A. Hrycyna, and J. Chmielewski, Journal of the American Chemical Society, 2012. 134(6): p. 2976-2980; PMID[21866921].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22487595">Olean-18-ene Triterpenoids from Celastraceae Species Inhibit HIV Replication Targeting Nf-kB and Sp1 Dependent Transcription.</a> Osorio, A.A., A. Munoz, D. Torres-Romero, L.M. Bedoya, N.R. Perestelo, I.A. Jimenez, J. Alcami, and I.L. Bazzocchi, European Journal of Medicinal Chemistry, 2012. 52: p. 295-303; PMID[22487595].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22616796">Aetheramides A and B, Potent HIV-inhibitory Depsipeptides from a Myxobacterium of the New Genus &quot;Aetherobacter&quot;.</a> Plaza, A., R. Garcia, G. Bifulco, J.P. Martinez, S. Huttel, F. Sasse, A. Meyerhans, M. Stadler, and R. Muller, Organic Letters, 2012. 14(11): p. 2854-2857; PMID[22616796].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22549138">Truncated Phosphonated C-1&#39;-branched N,O-nucleosides: A New Class of Antiviral Agents.</a> Romeo, R., C. Carnovale, S.V. Giofre, G. Romeo, B. Macchi, C. Frezza, F. Marino-Merlo, V. Pistara, and U. Chiacchio, Bioorganic and Medicinal Chemistry, 2012. 20(11): p. 3652-3657; PMID[22549138].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22664974">Saquinavir Inhibits Early Events Associated with Establishment of HIV-1 Infection: Potential Role for Protease Inhibitors in Prevention.</a> Stefanidou, M., C. Herrera, N. Armanasco, and R.J. Shattock, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22664974].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0525-060712.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303484600326">Lentiviral Gene Therapy against HIV-1 Using a Novel TRIM21-cyclophilin a Fusion Restriction Factor.</a> Chan, E., T. Schaller, C.P. Tan, A.J. Thrasher, G.J. Towers, and W. Qasim, Molecular Therapy, 2012. 20: p. S128-S128; ISI[000303484600326].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303401500001">Monoclonal Antibody-based Candidate Therapeutics against HIV Type 1.</a> Chen, W.Z. and D.S. Dimitrov, Aids Research and Human Retroviruses, 2012. 28(5): p. 425-434; ISI[000303401500001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303017700075">Biophysical Property and Broad Anti-HIV Activity of Albuvirtide, a 3-Maleimimidopropionic Acid-modified Peptide Fusion Inhibitor.</a> Chong, H.H., X. Yao, C. Zhang, L.F. Cai, S. Cui, Y.C. Wang, and Y.X. He, Plos One, 2012. 7(3); ISI[000303017700075].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303579100003">Design, Synthesis, Antiviral, and Cytotoxic Evaluation of Novel Phosphonylated 1,2,3-triazoles as Acyclic Nucleotide Analogues</a><i>.</i> Glowacka, I.E., J. Balzarini, and A.E. Wroblewski, Nucleosides Nucleotides &amp; Nucleic Acids, 2012. 31(4): p. 293-318; ISI[000303579100003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303900600002">The Enhancement of RNAi against HIV in Vitro and in Vivo Using H-2K(k) Protein as a Sorting Method.</a> Gu, Y., W.H. Hou, C.Y. Xu, S.W. Li, J.W.K. Shih, and N.S. Xia, Journal of Virological Methods, 2012. 182(1-2): p. 9-17; ISI[000303900600002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303846900013">A Novel Type of Acyclic Nucleoside Phosphonates Derived from 2-(Phosphonomethoxy)propanoic Acid.</a> Kaiser, M.M., P. Jansa, M. Dracinsky, and Z. Janeba, Tetrahedron, 2012. 68(21): p. 4003-4012; ISI[000303846900013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303484600268">Antiviral Potency of Cleaved and Uncleaved anti-HIV RT Aptamers Expressed Intracellularly Via a Hammerhead Ribozyme Cassette.</a> Lange, M.J., T.K. Sharma, and D.H. Burke, Molecular Therapy, 2012. 20: p. S105-S105; ISI[000303484600268].</p>

    <p class="plaintext"> <b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303444600010">Bioactive Phenylpropanoids from Daphne feddei.</a> Lu, Y.L., X.S. Li, H.X. Mu, H.T. Huang, G.P. Li, and Q.F. Hu, Journal of the Brazilian Chemical Society, 2012. 23(4): p. 656-660; ISI[000303444600010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303471900003">Synthesis and Antiviral Activity of Benzimidazolyl- and Triazolyl-1,3,5-triazines.</a> Maarouf, A.R., A.A. Farahat, K.B. Selim, and H.M. Eisa, Medicinal Chemistry Research, 2012. 21(6): p. 703-710; ISI[000303471900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303618700012">Novel Anti-HIV-1 Activity Produced by Conjugating Unsulfated Dextran with Polyl-lysine.</a> Nakamura, K., T. Ohtsuki, H. Mori, H. Hoshino, A. Hoque, A. Oue, F. Kano, H. Sakagami, K. Tanamoto, H. Ushijima, N. Kawasaki, H. Akiyama, and H. Ogawa, Antiviral Research, 2012. 94(1): p. 89-97; ISI[000303618700012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303456900003">3D-QSAR Studies of Natural Flavonoid Compounds as Reverse Transcriptase Inhibitors.</a> Phosrithong, N., W. Samee, and J. Ungwitayatorn, Medicinal Chemistry Research, 2012. 21(5): p. 559-567; ISI[000303456900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303843300013">Synthesis, Antioxidant and Antimicrobial Evaluation of Thiazolidinone, Azetidinone Encompassing Indolylthienopyrimidines.</a> Saundane, A.R., M. Yarlakatti, P. Walmik, and V. Katkar, Journal of Chemical Sciences, 2012. 124(2): p. 469-481; ISI[000303843300013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303870000001">Modulation of HIV-1 Gag NC/p1 Cleavage Efficiency Affects Protease Inhibitor Resistance and Viral Replicative Capacity.</a> van Maarseveen, N.M., D. Andersson, M. Lepsik, A. Fun, P.J. Schipper, D. de Jong, C.A.B. Boucher, and M. Nijhuis, Retrovirology, 2012. <b>[Epub ahead of print]</b>; ISI[000303870000001]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303681400010">Synthesis of New 2-(N-Arylsulfonylindol-3-yl)-3-aryl-1,3-thiazolidin-4-ones as HIV-1 Inhibitors in Vitro.</a> Yang, R.G., L.M. Yang, Y.Z. Ke, N. Huang, R. Zhang, Y.T. Zheng, and H. Xu, Letters in Drug Design &amp; Discovery, 2012. 9(4): p. 415-420; ISI[000303681400010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303787100007">Small-molecule Inhibition of Human Immunodeficiency Virus Type 1 Replication by Targeting the Interaction between Vif and ElonginC</a><i>.</i> Zuo, T., D.L. Liu, W. Lv, X.D. Wang, J.W. Wang, M.Y. Lv, W.L. Huang, J.X. Wu, H.H. Zhang, H.W. Jin, L.R. Zhang, W. Kong, and X.H. Yu, Journal of Virology, 2012. 86(10): p. 5497-5507; ISI[000303787100007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0525-060712.</p>

    <h2>Patent citations</h2>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120503&amp;CC=WO&amp;NR=2012055031A1&amp;KC=A1">Preparation of Amino Acid Amides Containing a Sulfonamide Bond as HIV Protease Inhibitors.</a> Boyd, M.J., C. Molinaro, A. Roy, and V.L. Truong. Patent. 2012. 2011-CA1202 2012055031: 108pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120419&amp;CC=WO&amp;NR=2012049277A1&amp;KC=A1">Preparation of Pyridine carboxamides as CXCR4 Receptor Antagonists.</a> Brown, G., M. Higginbottom, A. Stewart, L. Patient, A. Carley, I. Simpson, E.D. Savory, K. Oliver, and A.G. Cole. Patent. 2012. 2011-EP67946 2012049277: 205pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120510&amp;CC=US&amp;NR=2012115775A1&amp;KC=A1">Nullbasic Mutant TAT Protein Modulates Splicing, Rev Protein Activity, mRNA Transport, and HIV-1 Infection, for Use in Treatment of HIV/AIDS.</a> Harrich, D. Patent. 2012. 2011-292425 20120115775: 28pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120503&amp;CC=US&amp;NR=2012108533A1&amp;KC=A1">Novel Phosphate Modified Nucleosides Useful as Substrates for Polymerases and as Antiviral Agents.</a> Herdewijn, P. and P. Marliere. Patent. 2012. 2012-266954 20120108533: 37pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">37. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120411&amp;CC=CN&amp;NR=102406634A&amp;KC=A">Ketonic acids as HIV-1 Integrase Inhibitors and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of AIDS.</a> Liu, J., X. Chen, J. Jin, and Y. Wu. Patent. 2012. 2010-10288737 102406634: 26pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120229&amp;CC=PL&amp;NR=210600B1&amp;KC=B1">Preparation of New Modified 2&#39;,3&#39;-Dideoxy-P1,P2-dinucleotides with Activity against Human Immunodeficiency Virus (HIV-1) and Pharmaceutical Agents Containing It.</a> Miazga, A., T. Kulikowski, A. Lipniacki, and A. Piasek. Patent. 2012. 2006-381409 210600: 6pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120503&amp;CC=WO&amp;NR=2012055034A1&amp;KC=A1">Preparation of Amino Acid Amides Containing a Sulfonamide Bond as HIV Protease Inhibitors.</a> Moradei, O.M., S. Crane, D.J. McKay, M.-E. Lebrun, and V.L. Truong. Patent. 2012. 2011-CA1206 2012055034: 145pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120412&amp;CC=WO&amp;NR=2012047993A2&amp;KC=A2">N-Hydroxypyrimidine-2,4-diones as Inhibitors of HIV and HCV and Their Preparation.</a> Wang, Z., R.J. Geraghty, R. Vince, and J. Tang. Patent. 2012. 2011-US54916 2012047993: 67pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060812.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
