

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-02-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="50U3LPSjgza5uGweshHBILzM7nN527yZ/es8L5wsVnqgHXAu9SbIlarUdOsvVIPtJjmZl7nuaJcqbLpDMdVKFFIGxuGnmeQDuRh/6FRSKXYcP5JW0lknjycsFy4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4D9E3F44" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: February 14 - February 27, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24461293">Dual Inhibition of HCV and HIV by Ring-expanded Nucleosides Containing the 5:7-Fused imidazo[4,5-e][1,3]diazepine Ring System. In Vitro Results and Implications.</a> Zhang, N., P. Zhang, A. Baier, L. Cova, and R.S. Hosmane. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(4): p. 1154-1157. PMID[24461293].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24387306">High-affinity Recognition of HIV-1 Frameshift-stimulating RNA Alters Frameshifting in Vitro and Interferes with HIV-1 Infectivity.</a> Ofori, L.O., T.A. Hilimire, R.P. Bennett, N.W. Brown, Jr., H.C. Smith, and B.L. Miller. Journal of Medicinal Chemistry, 2014. 57(3): p. 723-732. PMID[24387306].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24457088">Design and Synthesis of N1-Aryl-benzimidazoles 2-Substituted as Novel HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Monforte, A.M., S. Ferro, L. De Luca, G. Lo Surdo, F. Morreale, C. Pannecouque, J. Balzarini, and A. Chimirri. Bioorganic &amp; Medicinal Chemistry, 2014. 22(4): p. 1459-1467. PMID[24457088].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24563723">Preparation and Activities of Macromolecule Conjugates of the CCR5 Antagonist Maraviroc.</a> Asano, S., J. Gavrilyuk, D.R. Burton, and C.F. Barbas, 3rd. ACS Medicinal Chemistry Letters, 2014. 5(2): p. 133-137. PMID[24563723].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0214-022714.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330159700041">An Asymmetric Allylic Alkylation-smiles Rearrangement-sulfinate Addition Sequence to Construct Chiral Cyclic Sulfones.</a> Wang, Q.G., Q.Q. Zhou, J.G. Deng, and Y.C. Chen. Organic Letters, 2013. 15(18): p. 4786-4789. ISI[000330159700041].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330016100012">Evaluation of Baylis-Hillman Routes to 3-(Aminomethyl) Coumarin Derivatives.</a> Olasupo, I., N.R. Rose, R. Klein, L.A. Adams, O.B. Familoni, and P.T. Kaye. Synthetic Communications, 2014. 44(2): p. 251-258. ISI[000330016100012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329881200012">Sulfated Polysaccharides as Bioactive Agents from Marine Algae.</a> Ngo, D.H. and S.K. Kim. International Journal of Biological Macromolecules, 2013. 62: p. 70-75. ISI[000329881200012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330159500006">Preparation of Spiro[4.4]-Oxaphospholene and -Azaphospholene Systems from Carbohydrate Templates.</a> Moura, M., S. Josse, and D. Postel. Journal of Organic Chemistry, 2013. 78(18): p. 8994-9003. ISI[000330159500006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br />  

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329772700017">Dead-end Complexes Contribute to the Synergistic Inhibition of HIV-1 RT by the Combination of Rilpivirine, Emtricitabine, and Tenofovir.</a> Kulkarni, R., J.Y. Feng, M.D. Miller, and K.L. White. Antiviral Research, 2014. 101: p. 131-135. ISI[000329772700017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330120800010">Inhibition of Reverse Transcriptase and Taq DNA Polymerase by Compounds Possessing the Coumarin Framework.</a> Hugo, A.G., M.M. Jimena, M.C. Gladys, E.T. Carlos, and R.P. Carlos. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 760-764. ISI[000330120800010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330121400011">Synthesis and Biological Study of O-beta-D-Glucosides of 7-Hydroxy-3-(disubstituted imidazol-2-yl)-4H-chromen-4-ones.</a> Hatzade, K.M., V.S. Taile, and V.N. Ingle. Macroheterocycles, 2013. 6(2): p. 192-198. ISI[000330121400011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329618404717">Cyclophilin A Inhibitors as Potential anti-HIV Agents.</a> Gupta, M., D. Shah, P.T. Flaherty, and Z. Ambrose. Abstracts of Papers of the American Chemical Society, 2013. 246. ISI[000329618404717].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330120800020">Hydroxy Fatty acids for the Delivery of Dideoxynucleosides as anti-HIV Agents.</a> Gangadhara, K.L., E. Lescrinier, C. Pannecouque, and P. Herdewijn. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 817-820. ISI[000330120800020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330220400014">Cytotoxic, Cytostatic and HIV-1 PR Inhibitory Activities of the Soft Coral Litophyton arboreum.</a> Ellithey, M.S., N. Lall, A.A. Hussein, and D. Meyer. Marine Drugs, 2013. 11(12): p. 4917-4936. ISI[000330220400014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329742300015">Simultaneous Zinc-finger Nuclease Editing of the HIV Coreceptors CCR5 and CXCR4 Protects CD4(+) T Cells from HIV-1 Infection.</a> Didigu, C.A., C.B. Wilen, J.B. Wang, J. Duong, A.J. Secreto, G.A. Danet-Desnoyers, J.L. Riley, P.D. Gregory, C.H. June, M.C. Holmes, and R.W. Doms. Blood, 2014. 123(1): p. 61-69. ISI[000329742300015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329618404861">Conformational Preferences of the Aryl-x-aryl Motif: Evolution of Potent Next Generation HIV-1 Nonnucleoside Reverse Transcriptase Inhibitors (NNRTI&#39;s) That Contain a Biaryl Ether.</a> Anthony, N.J. Abstracts of Papers of the American Chemical Society, 2013. 246. ISI[000329618404861].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0214-022714.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
