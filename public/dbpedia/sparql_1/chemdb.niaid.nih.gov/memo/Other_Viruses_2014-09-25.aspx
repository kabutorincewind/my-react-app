

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-09-25.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KPUcNHz//w9jycBgCpBfexSU4JehYuUU4NWukHjGB7u8Te0vKWoAXZbgVojb5Rgn1NHhDZ0tc3F6/bk7i7F62FqFyH6czY/CYFm/lRMNKlE0rNuKMd0E6IYHnLQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A77F4E82" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: September 12 - September 25, 2014</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25232181">Broadly Neutralizing Antibodies Abrogate Established Hepatitis C Virus Infection.</a> de Jong, Y.P., M. Dorner, M.C. Mommersteeg, J.W. Xiao, A.B. Balazs, J.B. Robbins, B.Y. Winer, S. Gerges, K. Vega, R.N. Labitt, B.M. Donovan, E. Giang, A. Krishnan, L. Chiriboga, M.R. Charlton, D.R. Burton, D. Baltimore, M. Law, C.M. Rice, and A. Ploss. Science Translational Medicine, 2014. 6(254): p. 254ra129. PMID[25232181].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0912-092514.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25155387">Structure-based Design of a Novel Series of Azetidine Inhibitors of the Hepatitis C Virus NS3/4A Serine Protease.</a> Parsy, C., F.R. Alexandre, G. Brandt, C. Caillet, S. Cappelle, D. Chaves, T. Convard, M. Derock, D. Gloux, Y. Griffon, L. Lallos, F. Leroy, M. Liuzzi, A.G. Loi, L. Moulat, C. Musiu, H. Rahali, V. Roques, M. Seifer, D. Standring, and D. Surleraux. Bioorganic and Medicinal Chemistry Letters, 2014. 24(18): p. 4444-4449. PMID[25155387].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0912-092514.</p>

    <h2>Herpes Simplex Virus I</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25014745">Synthesis and Antiviral Activity of New Phenylimidazopyridines and N-Benzylidenequinolinamines Derived by Molecular Simplification of Phenylimidazo[4,5-g]quinolines.</a> Loddo, R., I. Briguglio, P. Corona, S. Piras, M. Loriga, G. Paglietti, A. Carta, G. Sanna, G. Giliberti, C. Ibba, P. Farci, and P. La Colla. European Journal of Medicinal Chemistry, 2014. 84: p. 8-16. PMID[25014745].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0912-092514.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340986100029">Syntheses of 1 &#39;,2 &#39;-Cyclopentyl Nucleosides as Potential Antiviral Agents.</a> Dang, Q., Z.B. Zhang, T.Q. Chen, B.Y. Tang, X.L. He, S.S. He, Y. Song, S. Bogen, V. Girijavallabhan, D.B. Olsen, and P.T. Meinke. Tetrahedron Letters, 2014. 55(36): p. 5092-5095. ISI[000340986100029].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0912-092514.</p><br /> 

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340558300007">Modulation of Triglyceride and Cholesterol Ester Synthesis Impairs Assembly of Infectious Hepatitis C Virus.</a> Liefhebber, J.M.P., C.V. Hague, Q.F. Zhang, M.J.O. Wakelam, and J. McLauchlan. Journal of Biological Chemistry, 2014. 289(31): p. 21276-21288. ISI[000340558300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0912-092514.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
