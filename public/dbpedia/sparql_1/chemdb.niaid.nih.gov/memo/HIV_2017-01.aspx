

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fINDw6q3VEM7bWI/6ILd0neg2sGbf3KK3gtwT5N/KJQXqY2SZWtvKco7zJEdyxUkKhwuN9azTQV33NYciMwFmWrk85DaG4ilxdlMfNclTBMtwdHyh9mx6Zxlrew=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C188586A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: January, 2017</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28002966">Development of Small Molecules with a Noncanonical Binding Mode to HIV-1 Trans Activation Response (Tar) RNA.</a> Abulwerdi, F.A., M.D. Shortridge, J. Sztuba-Solinska, R. Wilson, S.F.J. Le Grice, G. Varani, and J.S. Schneekloth. Journal of Medicinal Chemistry, 2016. 59(24): p. 11148-11160. PMID[28002966].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">2. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28079543">A Novel anti-HIV Immunotherapy to Cure HIV.</a> Ahmad, A. and C.R. Rinaldo. AIDS, 2017. 31(3): p. 447-449. PMID[28079543].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">3. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28147284">Benzotriazoles Reactivate Latent HIV-1 through Inactivation of STAT5 SUMOylation.</a> Bosque, A., K.A. Nilson, A.B. Macedo, A.M. Spivak, N.M. Archin, R.M. Van Wagoner, L.J. Martins, C.L. Novis, M.A. Szaniawski, C.M. Ireland, D.M. Margolis, D.H. Price, and V. Planelles. Cell Reports, 2017. 18(5): p. 1324-1334. PMID[28147284].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">4. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27894873">Design, Synthesis and anti-HIV-1 RT Evaluation of 2-(Benzyl(4-chlorophenyl)amino)-1-(piperazin-1-yl)ethanone Derivatives.</a> Chander, S., P. Wang, P. Ashok, L.M. Yang, Y.T. Zheng, and M. Sankaranarayanan. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(1): p. 61-65. PMID[27894873].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000390879400091">A Synthesis of (+/-)-Thia-Calanolide A, Its Resolution and in Vitro Biological Evaluation.</a> Chopade, A.U., M.U. Chopade, B.M. Chanda, D.D. Sawaikar, K.B. Sonawane, and M.K. Gurjar. Arabian Journal of Chemistry, 2016. 9: p. S1597-S1602. ISI[000390879400091].
    <br />
    <b>[WOS]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">6. <a href="https://www.ncbi.nlm.nih.gov/pubmed/26777885">Nevirapine Loaded Core Shell Gold Nanoparticles by Double Emulsion Solvent Evaporation: In Vitro and in Vivo Evaluation.</a> Dalvi, B.R., E.A. Siddiqui, A.S. Syed, S.M. Velhal, A. Ahmad, A.B. Bandivdekar, and P.V. Devarajan. Current Drug Delivery, 2016. 13(7): p. 1071-1083. PMDI[26777885].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">7. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27855563">Novel Diaryltriazines with a Picolinonitrile Moiety as Potent HIV-1 RT Inhibitors: A Patent Evaluation of WO2016059647(A2).</a> Huang, B., Z. Zhou, D. Kang, W. Li, Z. Chen, P. Zhan, and X. Liu. Expert Opinion on Therapeutic Patents, 2017. 27(1): p. 9-15. PMID[27855563].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">8. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28097091">Evaluation of Novel Protease Inhibitors against Darunavir-resistant Variants of HIV Type 1.</a> Inoue, M., D. Oyama, K. Hidaka, and M. Kameoka. FEBS Open Bio, 2017. 7(1): p. 88-95. PMID[28097091]. PMCID[PMC5221448].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">9. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27750153">Discovery of Novel Piperidine-substituted Indolylarylsulfones as Potent HIV NNRTIs via Structure-guided Scaffold Morphing and Fragment Rearrangement.</a> Li, X., P. Gao, B. Huang, Z. Zhou, Z. Yu, Z. Yuan, H. Liu, C. Pannecouque, D. Daelemans, E. De Clercq, P. Zhan, and X. Liu. European Journal of Medicinal Chemistry, 2017. 126: p. 190-201. PMID[27750153].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">10. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27697031">Reactivation of Latent HIV-1 in Latently Infected Cells by Coumarin Compounds: Hymecromone and Scoparone.</a> Li, X., H.X. Zeng, P.F. Wang, L. Lin, L. Liu, P.Y. Zhen, Y.Z. Fu, P.P. Lu, and H.Z. Zhu. Current HIV Research, 2016. 14(6): p. 484-490. ISI[27697031].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389521800002">Bioactive Cucurbitane Triterpenoids from the Tubers of Hemsleya penxianensis.</a> Li, Y.D., S.R. Yi, X.B. Sun, X.Y. Zhou, H.Y. Zhang, Y.Q. Wang, J.S. Yang, X.D. Xu, and G.X. Ma. Phytochemistry Letters, 2016. 18: p. 5-9. ISI[000389521800002].
    <br />
    <b>[WOS]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">12. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27677464">Synergistic Reactivation of Latent HIV-1 Provirus by PKA Activator Dibutyryl-camp in Combination with an HDAC Inhibitor.</a> Lim, H., K.C. Kim, J. Son, Y. Shin, C.H. Yoon, C. Kang, and B.S. Choi. Virus Research, 2017. 227: p. 1-5. PMID[27677464].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000390518900016">Periconiasins I and J, Two New Cytochalasans from an Endophytic Fungus Periconia sp.</a> Liu, J.M., D.W. Zhang, M. Zhang, X. Liu, R.D. Chen, J.L. Zhao, L. Li, N. Wang, and J.G. Dai. Tetrahedron Letters, 2016. 57(51): p. 5794-5797. ISI[000390518900016].
    <br />
    <b>[WOS]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">14. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27881646">Activation and Inactivation of Primary Human Immunodeficiency Virus Envelope Glycoprotein Trimers by CD4-mimetic Compounds.</a> Madani, N., A.M. Princiotto, C. Zhao, F. Jahanbakhshsefidi, M. Mertens, A. Herschhorn, B. Melillo, A.B. Smith, 3rd, and J. Sodroski. Journal of Virology, 2017. 91(3): e01880-16. PMID[27881646]. PMCID[PMC5244334].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">15. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27759481">Synthesis and Antiviral Evaluation of Fluorinated Acyclo-nucleosides and Their Phosphoramidates.</a> Mahmoud, S., H. Li, T.R. McBrayer, L. Bassit, S.F. Hammad, S.J. Coats, F. Amblard, and R.F. Schinazi. Nucleosides, Nucleotides and Nucleic Acids, 2017. 36(1): p. 66-82. PMID[27759481].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">16. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27272399">Polyoxypregnane Glycosides from the Roots of Marsdenia tenacissima and their anti-HIV Activities.</a> Pang, X., L.P. Kang, X.M. Fang, Y. Zhao, H.S. Yu, L.F. Han, H.T. Li, L.X. Zhang, B.L. Guo, L.Y. Yu, and B.P. Ma. Planta Medica, 2017. 83(1-02): p. 126-134. PMID[27272399].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">17. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27301660">Surfactin Derivatives from Micromonospora sp. CPCC 202787 and their anti-HIV Activities.</a> Pang, X., J. Zhao, X. Fang, H. Liu, Y. Zhang, S. Cen, and L. Yu. Journal of Antibiotics, 2017. 70(1): p. 105-108. PMID[27301660].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">18. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28115849">Potential Inhibition of HIV-1 Encapsidation by Oligoribonucleotide-dendrimer Nanoparticle Complexes.</a> Parboosing, R., L. Chonco, F.J. de la Mata, T. Govender, G.E. Maguire, and H.G. Kruger. International Journal of Nanomedicine, 2017. 12: p. 317-325. PMID[28115849]. PMCID[PMC5221794].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">19. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27795412">Design and in Vivo Characterization of Immunoconjugates Targeting HIV gp160.</a> Pincus, S.H., K. Song, G.A. Maresh, A. Frank, D. Worthylake, H.K. Chung, P. Polacino, D.H. Hamer, C.P. Coyne, M.G. Rosenblum, J.W. Marks, G. Chen, D. Weiss, V. Ghetie, E.S. Vitetta, J.E. Robinson, and S.L. Hu. Journal of Virology, 2017. 91(3): e01360-16. PMID[27795412]. PMCID[PMC5244325].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">20. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27852851">Identification of Human anti-HIV gp160 Monoclonal Antibodies That Make Effective Immunotoxins.</a> Pincus, S.H., K. Song, G.A. Maresh, D.H. Hamer, D.S. Dimitrov, W. Chen, M.Y. Zhang, V.F. Ghetie, P.Y. Chan-Hui, J.E. Robinson, and E.S. Vitetta. Journal of Virology, 2017. 91(3): e01955-16 . PMID[27852851]. PMCID[PMC5244328].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">21. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28097226">IFITM1 Targets HIV-1 Latently Infected Cells for Antibody-dependent Cytolysis.</a> Raposo, R.A., M. de Mulder Rougvie, D. Paquin-Proulx, P.M. Brailey, V.D. Cabido, P.M. Zdinak, A.S. Thomas, S.H. Huang, G.A. Beckerle, R.B. Jones, and D.F. Nixon. JCI Insight, 2017. 2(1): e85811. PMID[28097226]. PMCID[PMC5214598].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">22. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27908751">Enhanced Potency of Bivalent Small Molecule gp41 Inhibitors.</a> Sofiyev, V., H. Kaur, B.A. Snyder, P.A. Hogan, R.G. Ptak, P. Hwang, and M. Gochin. Bioorganic &amp; Medicinal Chemistry, 2017. 25(1): p. 408-420. PMID[27908751]. PMCID[PMC5260928].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">23. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27783659">Cold Atmospheric Plasma Inhibits HIV-1 Replication in Macrophages by Targeting Both the Virus and the Cells.</a> Volotskova, O., L. Dubrovsky, M. Keidar, and M. Bukrinsky. Plos One, 2016. 11(10): e0165322. PMID[27783659]. PMCID[PMC5081187].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">24. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27810592">Discovery of Novel 3-Hydroxypicolinamides as Selective Inhibitors of HIV-1 Integrase-LEDGF/p75 Interaction.</a> Zhang, F.H., B. Debnath, Z.L. Xu, L.M. Yang, L.R. Song, Y.T. Zheng, N. Neamati, and Y.Q. Long. European Journal of Medicinal Chemistry, 2017. 125: p. 1051-1063. PMID[27810592].
    <br />
    <b>[PubMed]</b>. HIV_01_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">25. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170112&amp;CC=WO&amp;NR=2017006260A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Kadow, J.F., B.N. Naidu, M. Patel, and P. Sivaprakasam. Patent. 2017. 2016-IB54048 2017006260: 60pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170112&amp;CC=WO&amp;NR=2017006261A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Kadow, J.F., B.N. Naidu, T. Wang, and Z. Yin. Patent. 2017. 2016-IB54049 2017006261: 82pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170112&amp;CC=WO&amp;NR=2017007712A1&amp;KC=A1">Use of Triggering Receptor Expressed on Myeloid Cells 1 (TREM-1) Inhibitors for Treatment, Elimination and Eradication of HIV-1 Infection.</a> Schinazi, R.F. and C. Gavengnano. Patent. 2017. 2016-US40694 2017007712: 135pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2017.</p>

    <br />

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170112&amp;CC=WO&amp;NR=2017007701A1&amp;KC=A1">Preparation of Phosphodiamide Compounds as Antiviral Agents.</a> Vachal, P. and Z. Guo. Patent. 2017. 2016-US40606 2017007701: 68pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2017.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
