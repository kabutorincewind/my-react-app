

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-08-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8X+aSE22C9DCnXyaK0F/uTjH5p4PbJn0xlw9SMgx60UW6SKVDhiW6ykGzS1kRaaA73m+VeRWZQBN2iMVxk/uhgAF2ErrEOYQqdQ7qPfq8lIpvcobGVhHhjHANJY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2DDECB10" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List:</p>

    <p class="memofmt2-h1">July 22, 2009 - August 4, 2009</p>

    <p class="memofmt2-2">HEPATITIS C VIRUS</p>

    <p class="title">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19628159?ordinalpos=51&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Boceprevir, an NS3 protease inhibitor of HCV.</a>
    <br>
    Berman K, Kwo PY.
    <br>
    Clin Liver Dis. 2009 Aug;13(3):429-39.
    <br>
    PMID: 19628159 [PubMed - in process]</p>

    <p class="title">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19628158?ordinalpos=52&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Ribavirin analogs.</a>
    <br>
    Shields WW, Pockros PJ.
    <br>
    Clin Liver Dis. 2009 Aug;13(3):419-27.
    <br>
    PMID: 19628158 [PubMed - in process]</p>

    <p class="title">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19624135?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibitors of the Hepatitis C Virus NS3 Protease with Basic Amine Functionality at the P3-Amino Acid N-Terminus: Discovery and Optimization of a New Series of P2-P4 Macrocycles.</a>
    <br>
    Harper S, Ferrara M, Crescenzi B, Pompei M, Palumbi MC, Dimuzio JM, Donghi M, Fiore F, Koch U, Liverton NJ, Pesci S, Petrocchi A, Rowley M, Summa V, Gardelli C.
    <br>
    J Med Chem. 2009 Jul 22. [Epub ahead of print]
    <br>
    PMID: 19624135 [PubMed - as supplied by publisher]</p>

    <p class="title">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19549584?ordinalpos=88&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of bisindolylmaleimides and indolocarbazoles as inhibitors of HCV replication by tube-capture-RT-PCR.</a>
    <br>
    Murakami Y, Noguchi K, Yamagoe S, Suzuki T, Wakita T, Fukazawa H.
    <br>
    Antiviral Res. 2009 Aug;83(2):112-7. Epub 2009 Apr 5.
    <br>
    PMID: 19549584 [PubMed - indexed for MEDLINE]</p>

    <p class="title">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19515564?ordinalpos=98&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Substituted benzothiadizine inhibitors of Hepatitis C virus polymerase.</a>
    <br>
    Shaw AN, Tedesco R, Bambal R, Chai D, Concha NO, Darcy MG, Dhanak D, Duffy KJ, Fitch DM, Gates A, Johnston VK, Keenan RM, Lin-Goerke J, Liu N, Sarisky RT, Wiggall KJ, Zimmerman MN.
    <br>
    Bioorg Med Chem Lett. 2009 Aug 1;19(15):4350-3. Epub 2009 May 28.
    <br>
    PMID: 19515564 [PubMed - in process]</p>

    <p class="title">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19507864?ordinalpos=100&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Structure-based design of a benzodiazepine scaffold yields a potent allosteric inhibitor of hepatitis C NS5B RNA polymerase.</a>
    <br>
    Vandyck K, Cummings MD, Nyanguile O, Boutton CW, Vendeville S, McGowan D, Devogelaere B, Amssoms K, Last S, Rombauts K, Tahri A, Lory P, Hu L, Beauchamp DA, Simmen K, Raboisson P.
    <br>
    J Med Chem. 2009 Jul 23;52(14):4099-102.
    <br>
    PMID: 19507864 [PubMed - in process]</p>

    <p class="title">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19505821?ordinalpos=101&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and biological activity of heteroaryl 3-(1,1-dioxo-2H-(1,2,4)-benzothiadizin-3-yl)-4-hydroxy-2(1H)-quinolinone derivatives as hepatitis C virus NS5B polymerase inhibitors.</a>
    <br>
    Tedesco R, Chai D, Darcy MG, Dhanak D, Fitch DM, Gates A, Johnston VK, Keenan RM, Lin-Goerke J, Sarisky RT, Shaw AN, Valko KL, Wiggall KJ, Zimmerman MN, Duffy KJ.
    <br>
    Bioorg Med Chem Lett. 2009 Aug 1;19(15):4354-8. Epub 2009 May 27.
    <br>
    PMID: 19505821 [PubMed - in process]</p>

    <p class="memofmt2-2">HERPES VIRUS</p>

    <p class="title">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19394201?ordinalpos=67&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A new homodimer of aciclovir as a prodrug with increased solubility and antiviral activity.</a>
    <br>
    Brandi G, Rossi L, Schiavano GF, Millo E, Magnani M.
    <br>
    Int J Antimicrob Agents. 2009 Aug;34(2):177-80. Epub 2009 Apr 24.
    <br>
    PMID: 19394201 [PubMed - in process]</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p class="ListParagraph">9. Chiacchio, U, et al</p>

    <p class="NoSpacing">Stereoselective Synthesis and Biological Evaluations of Novel 3 &#39;-Deoxy-4 &#39;-azaribonucleosides as Inhibitors of Hepatitis C Virus RNA Replication
    <br>
    JOURNAL OF MEDICINAL CHEMISTRY  2009 (July) 52: 4054 - 4057</p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
