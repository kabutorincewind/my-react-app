

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-98.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XZBcKw5L3610hho2ERxA0MSZvZQ8wOxZr2UyNiyiBGNEOSYkAyqBfqXVqLvGkoZ+KC7F8fPYUrLhWk4AGkMkecp49UK7YehXA/VvdLqpQcc55k3Bm/5sWplw9pM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E33B1B03" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-98-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57746   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Association of bovine papillomavirus e2 protein with nuclear structures in vivo</p>

    <p>          Kurg, R, Sild, K, Ilves, A, Sepp, M, and Ustav, M</p>

    <p>          J Virol <b>2005</b>.  79(16): 10528-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16051845&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16051845&amp;dopt=abstract</a> </p><br />

    <p>2.     57747   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Some Carbonucleoside Analogues</p>

    <p>          Alho, M. <i>et al.</i></p>

    <p>          Journal of Heterocyclic Chemistry <b>2005</b>.  42(5): 979-983</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230447200036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230447200036</a> </p><br />

    <p>3.     57748   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Activity and regulation of alpha interferon in respiratory syncytial virus and human metapneumovirus experimental infections</p>

    <p>          Guerrero-Plata, A, Baron, S, Poast, JS, Adegboyega, PA, Casola, A, and Garofalo, RP</p>

    <p>          J Virol <b>2005</b>.  79(16): 10190-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16051812&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16051812&amp;dopt=abstract</a> </p><br />

    <p>4.     57749   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Initial Results of a Phase 1b, Multiple-Dose Study of Vx-950, a Hepatitis C Virus Protease Inhibitor</p>

    <p>          Reesink, H. <i>et al.</i></p>

    <p>          Gastroenterology <b>2005</b>.  128(4): A697</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305474">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305474</a> </p><br />

    <p>5.     57750   DMID-LS-98; EMBASE-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Broad-spectrum inhibitor of viruses in the Flaviviridae family</p>

    <p>          Ojwang, Joshua O, Ali, Shoukath, Smee, Donald F, Morrey, John D, Shimasaki, Craig D, and Sidwell, Robert W</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GRH62W-1/2/a8d8400d802d871f10ee8c3b45db5beb">http://www.sciencedirect.com/science/article/B6T2H-4GRH62W-1/2/a8d8400d802d871f10ee8c3b45db5beb</a> </p><br />

    <p>6.     57751   DMID-LS-98; EMBASE-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of novel substituted 2-phenylpyrazolopyridines with potent activity against herpesviruses</p>

    <p>          Gudmundsson, Kristjan S, Johns, Brian A, Wang, Zhicheng, Turner, Elizabeth M, Allen, Scott H, Freeman, George A, Boyd, Jr FLeslie, Sexton, Connie J, and Selleseth, Dean W</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4GP1VBK-1/2/47985a9fde562072ff9318f679aa4b14">http://www.sciencedirect.com/science/article/B6TF8-4GP1VBK-1/2/47985a9fde562072ff9318f679aa4b14</a> </p><br />
    <br clear="all">

    <p>7.     57752   DMID-LS-98; EMBASE-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 5-haloethynyl- and 5-(1,2-dihalo)vinyluracil nucleosides: Antiviral activity and cellular toxicity</p>

    <p>          Escuret, Vanessa, Aucagne, Vincent, Joubert, Nicolas, Durantel, David, Rapp, Kimberly L, Schinazi, Raymond F, Zoulim, Fabien, and Agrofoglio, Luigi A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4GMJ915-1/2/b6ad5551627cfbda60806f1a54e039c2">http://www.sciencedirect.com/science/article/B6TF8-4GMJ915-1/2/b6ad5551627cfbda60806f1a54e039c2</a> </p><br />

    <p>8.     57753   DMID-LS-98; EMBASE-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Extracts and molecules from medicinal plants against herpes simplex viruses</p>

    <p>          Khan, Mahmud Tareq Hassan, Ather, Arjumand, Thompson, Kenneth D, and Gambari, Roberto</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GKW5T6-1/2/2b3499fbc23db425f10f62f0d7578b22">http://www.sciencedirect.com/science/article/B6T2H-4GKW5T6-1/2/2b3499fbc23db425f10f62f0d7578b22</a> </p><br />

    <p>9.     57754   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Emtricitabine: a Novel Nucleoside Reverse Transcriptase Inhibitor</p>

    <p>          Molina, J. and Cox, S.</p>

    <p>          Drugs of Today  <b>2005</b>.  41(4): 241-252</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230464500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230464500002</a> </p><br />

    <p>10.   57755   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent and persistent in vivo anti-HBV activity of chemically modified siRNAs</p>

    <p>          Morrissey, DV, Lockridge, JA, Shaw, L, Blanchard, K, Jensen, K, Breen, W, Hartsough, K, Machemer, L, Radka, S, Jadhav, V, Vaish, N, Zinnen, S, Vargeese, C, Bowman, K, Shaffer, CS, Jeffs, LB, Judge, A, Maclachlan, I, and Polisky, B</p>

    <p>          Nat Biotechnol  <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16041363&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16041363&amp;dopt=abstract</a> </p><br />

    <p>11.   57756   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptide-Aptamers as Specific Inhibitors of Hepatitis C Virus Ns3 Serine Protease</p>

    <p>          Benhar, I. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2005</b>.  42: 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000021</a> </p><br />

    <p>12.   57757   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Cell-Based Assay for Evaluating Potential Anti Viral Agents Against Hcv</p>

    <p>          Aviel, S. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2005</b>.  42: 156</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000427">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000427</a> </p><br />
    <br clear="all">

    <p>13.   57758   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Additive Antiviral Effect of Artemisinin and Ribavirin on an &quot;in Vitro&quot; Model of Hepatitis C Infection</p>

    <p>          Romero, M. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2005</b>.  42: 166</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000459">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000459</a> </p><br />

    <p>14.   57759   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Drugs in Development for Hepatitis B</p>

    <p>          Buti, M and Esteban, R</p>

    <p>          Drugs <b>2005</b>.  65 (11): 1451-1460</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033287&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033287&amp;dopt=abstract</a> </p><br />

    <p>15.   57760   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          5&#39;-homoneplanocin a inhibits hepatitis B and hepatitis C</p>

    <p>          Yang, M, Schneller, SW, and Korba, B</p>

    <p>          J Med Chem <b>2005</b>.  48(15): 5043-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033283&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033283&amp;dopt=abstract</a> </p><br />

    <p>16.   57761   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacokinetics and Pharmacodynamics of Valopicitabine (Nm283), a New Nucleoside Hcv Polymerase Inhibitor: Results From a Phase I/Ii Dose-Escalation Trial in Patients With Hcv-1 Infection</p>

    <p>          Zhou, X. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2005</b>.  42: 229</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000626">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000626</a> </p><br />

    <p>17.   57762   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of liposomal preparations of antibiotic geliomycin</p>

    <p>          Podolskaya, SV, Naryshkina, NA, Sorokoumova, GM, Kaplun, AP, Fedorova, NE, Medzhidova, AA, Kuts, AA, and Shvets, VI</p>

    <p>          Bull Exp Biol Med <b>2005</b>.  139(3): 349-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16027850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16027850&amp;dopt=abstract</a> </p><br />

    <p>18.   57763   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evolution of H9N2 influenza viruses from domestic poultry in Mainland China</p>

    <p>          Li, C, Yu, K, Tian, G, Yu, D, Liu, L, Jing, B, Ping, J, and Chen, H</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16026813&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16026813&amp;dopt=abstract</a> </p><br />

    <p>19.   57764   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pseudo-Peptides Derived From Isomannide as Potential Inhibitors of Serine Proteases</p>

    <p>          Muri, E. <i>et al.</i></p>

    <p>          Amino Acids <b>2005</b>.  28(4): 413-419</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229871100011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229871100011</a> </p><br />

    <p>20.   57765   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human cathelicidin (LL-37), a multifunctional peptide, is expressed by ocular surface epithelia and has potent antibacterial and antiviral activity</p>

    <p>          Gordon, YJ, Huang, LC, Romanowski, EG, Yates, KA, Proske, RJ, and McDermott, AM</p>

    <p>          Curr Eye Res <b>2005</b>.  30(5): 385-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16020269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16020269&amp;dopt=abstract</a> </p><br />

    <p>21.   57766   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Histone Deacetylase Inhibitors Induce Reactivation of Herpes Simplex Virus Type 1 in a Latency-Associated Transcript-Independent Manner in Neuronal Cells</p>

    <p>          Danaher, R. <i>et al.</i></p>

    <p>          Journal of Neurovirology <b>2005</b>.  11(3): 306-317</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230504600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230504600009</a> </p><br />

    <p>22.   57767   DMID-LS-98; LR-14912-LWC; EMBASE-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro and in vivo influenza virus-inhibitory effects of viramidine</p>

    <p>          Sidwell, Robert W, Bailey, KW, Wong, M-H, Barnard, DL, and Smee, DF</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GPVXD6-1/2/b5d8c0db5a3c50294412edee8dacaedc">http://www.sciencedirect.com/science/article/B6T2H-4GPVXD6-1/2/b5d8c0db5a3c50294412edee8dacaedc</a> </p><br />

    <p>23.   57768   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition, escape, and attenuated growth of severe acute respiratory syndrome coronavirus treated with antisense morpholino oligomers</p>

    <p>          Neuman, BW, Stein, DA, Kroeker, AD, Churchill, MJ, Kim, AM, Kuhn, P, Dawson, P, Moulton, HM, Bestwick, RK, Iversen, PL, and Buchmeier, MJ</p>

    <p>          J Virol <b>2005</b>.  79(15): 9665-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16014928&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16014928&amp;dopt=abstract</a> </p><br />

    <p>24.   57769   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Expressional Screening of Novel Interferon-Stimulated Genes for Antiviral Activity Against Hepatitis C Virus Replication</p>

    <p>          Itsui, Y. <i>et al.</i></p>

    <p>          Gastroenterology <b>2005</b>.  128(4): A712</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305543">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305543</a> </p><br />

    <p>25.   57770   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cidofovir for the treatment of recurrent respiratory papillomatosis: a review of the literature</p>

    <p>          Shehab, N, Sweet, BV, and Hogikyan, ND</p>

    <p>          Pharmacotherapy <b>2005</b>.  25(7): 977-89</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16006276&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16006276&amp;dopt=abstract</a> </p><br />

    <p>26.   57771   DMID-LS-98; EMBASE-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro activity of cycloSal-nucleoside monophosphates and polyhydroxycarboxylates against orthopoxviruses</p>

    <p>          Sauerbrei, A, Meier, C, Meerbach, A, Schiel, M, Helbig, B, and Wutzler, P</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GP6WCH-1/2/0139ddf72a971bc18608c064e97e3b3f">http://www.sciencedirect.com/science/article/B6T2H-4GP6WCH-1/2/0139ddf72a971bc18608c064e97e3b3f</a> </p><br />

    <p>27.   57772   DMID-LS-98; WOS-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Correlation Between Reduction Potentials and Inhibitory Effects on Epstein-Barr Virus Activation of Poly-Substituted Anthraquinones</p>

    <p>          Koyama, J. <i>et al.</i></p>

    <p>          Cancer Letters  <b>2005</b>.  225(2): 193-198</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230381400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230381400002</a> </p><br />

    <p>28.   57773   DMID-LS-98; EMBASE-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cidofovir peptide conjugates as prodrugs</p>

    <p>          McKenna, Charles E, Kashemirov, Boris A, Eriksson, Ulrika, Amidon, Gordon L, Kish, Phillip E, Mitchell, Stefanie, Kim, Jae-Seung, and Hilfinger, John M</p>

    <p>          Journal of Organometallic Chemistry <b>2005</b>.  690(10): 2673-2678</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGW-4G0M42W-1/2/09c6abdf4120cb04bc46971ef9c502fd">http://www.sciencedirect.com/science/article/B6TGW-4G0M42W-1/2/09c6abdf4120cb04bc46971ef9c502fd</a> </p><br />

    <p>29.   57774   DMID-LS-98; PUBMED-DMID-8/1/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Castanospermine, a potent inhibitor of dengue virus infection in vitro and in vivo</p>

    <p>          Anon <b>2005</b>.  79(14): 8698-8706</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994763&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994763&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
