

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-309.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xEafEwDygCUHId1vjpe5kYL8X+fmUaQewVA9VYQF8wyea1OGmSPRJVwk4gnUesYJHVtfPCwIXiXHRuk02yrCLK8fEziSR+tsdAn5pucik2YBQu+0h+TRG34uEg8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5CC9C46D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-309-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.        44345   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          A Novel Dipeptide-based HIV Protease Inhibitor Containing Allophenylnorstatine</p>

    <p>          Abdel-Rahman, HM, El-Koussi, NA, Alkaramany, GS, Youssef, AF, and Kiso, Y</p>

    <p>          Arch Pharm (Weinheim) <b>2004</b>.  337(11): 587-598</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15540219&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15540219&amp;dopt=abstract</a> </p><br />

    <p>2.        44346   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Improved Structure-Activity Relationship Analysis of HIV-1 Protease Inhibitors Using Interaction Kinetic Data</p>

    <p>          Shuman, CF, Vrang, L, and Danielson, UH</p>

    <p>          J Med Chem <b>2004</b>.  47(24): 5953-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537350&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537350&amp;dopt=abstract</a> </p><br />

    <p>3.        44347   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Design of Non-nucleoside Inhibitors of HIV-1 Reverse Transcriptase with Improved Drug Resistance Properties. 2</p>

    <p>          Freeman, GA, Andrews, Iii CW, Hopkins, AL, Lowell, GS, Schaller, LT, Cowan, JR, Gonzales, SS, Koszalka, GW, Hazen, RJ, Boone, LR, Ferris, RG, Creech, KL, Roberts, GB, Short, SA, Weaver, K, Reynolds, DJ, Milton, J, Ren, J, Stuart, DI, Stammers, DK, and Chan, JH</p>

    <p>          J Med Chem <b>2004</b>.  47(24): 5923-5936</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537347&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537347&amp;dopt=abstract</a> </p><br />

    <p>4.        44348   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Design of Non-Nucleoside Inhibitors of HIV-1 Reverse Transcriptase with Improved Drug Resistance Properties. 1</p>

    <p>          Hopkins, AL, Ren, J, Milton, J, Hazen, RJ, Chan, JH, Stuart, DI, and Stammers, DK</p>

    <p>          J Med Chem <b>2004</b>.  47(24): 5912-5922</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537346&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537346&amp;dopt=abstract</a> </p><br />

    <p>5.        44349   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Enfuvirtide: a new class of antiretroviral therapy for HIV infection</p>

    <p>          Leao, J, Frezzini, C, and Porter, S</p>

    <p>          Oral Dis <b>2004</b>.  10(6): 327-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15533206&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15533206&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.        44350   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Inhibition of Wild-Type and Mutant Human Immunodeficiency Virus Type 1 Proteases by GW0385 and Other Arylsulfonamides</p>

    <p>          Hanlon, MH, Porter, DJ, Furfine, ES, Spaltenstein, A, Carter, HL, Danger, D, Shu, AY, Kaldor, IW, Miller, JF, and Samano, VA</p>

    <p>          Biochemistry <b>2004</b>.  43(45): 14500-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15533054&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15533054&amp;dopt=abstract</a> </p><br />

    <p>7.        44351   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Antiviral activity of CYC202 in HIV-1 infected cells</p>

    <p>          Agbottah, E, de la Fuente, C, Nekhai, S, Barnett, A, Gianella-Borradori, A, Pumfery, A, and Kashanchi, F</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15531588&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15531588&amp;dopt=abstract</a> </p><br />

    <p>8.        44352   HIV-LS-309; SCIFINDER-HIV-11/9/2004</p>

    <p class="memofmt1-2">          Oxindoles for treatment of HIV infection, and preparation thereof</p>

    <p>          He, Yun, Jiang, Tao, Kuhen, Kelli L, Ellis, David Archer, Wu, Baogen, Wu, Tom Yao-Hsiang, and Bursulaya, Badry</p>

    <p>          PATENT:  WO <b>2004037247</b>  ISSUE DATE:  20040506</p>

    <p>          APPLICATION: 2003  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (IRM LLC, Bermuda and The Scripps Research Institute)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>9.        44353   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Nonnucleoside HIV-1 reverse transcriptase inhibitors; part 3. Synthesis and antiviral activity of 5-alkyl-2-[(aryl and alkyloxyl-carbonylmethyl)thio]-6-(1-naphthylmethyl) pyrimidin-4(3H)-ones</p>

    <p>          He, Y, Chen, F, Yu, X, Wang, Y, De, Clercq E, Balzarini, J, and Pannecouque, C</p>

    <p>          Bioorg Chem <b>2004</b>.: 536-548</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15533055&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15533055&amp;dopt=abstract</a> </p><br />

    <p>10.      44354   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Capravirine, a Nonnucleoside Reverse-Transcriptase Inhibitor in Patients Infected with HIV-1: A Phase 1 Study</p>

    <p>          Gewurz, BE, Jacobs, M, Proper, JA, Dahl, TA, Fujiwara, T, and Dezube, BJ</p>

    <p>          J Infect Dis <b>2004</b>.  190(11): 1957-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15529260&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15529260&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.      44355   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Abrogation of Vif function by peptide derived from the N-terminal region of the human immunodeficiency virus type 1 (HIV-1) protease</p>

    <p>          Hutoran, M, Britan, E, Baraz, L, Blumenzweig, I, Steinitz, M, and Kotler, M</p>

    <p>          Virology <b>2004</b>.  330(1): 261-270</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15527851&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15527851&amp;dopt=abstract</a> </p><br />

    <p>12.      44356   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type 1 replication by siRNA targeted to the highly conserved primer binding site</p>

    <p>          Han, W, Wind-Rotolo, M, Kirkman, RL, and Morrow, CD</p>

    <p>          Virology <b>2004</b>.  330(1): 221-32</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15527848&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15527848&amp;dopt=abstract</a> </p><br />

    <p>13.      44357   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Inhibition of certain strains of HIV-1 by cell surface polyanions in the form of cholesterol-labeled oligonucleotides</p>

    <p>          Ahn, KS, Ou, W, and Silver, J</p>

    <p>          Virology <b>2004</b>.  330(1): 50-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15527833&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15527833&amp;dopt=abstract</a> </p><br />

    <p>14.      44358   HIV-LS-309; SCIFINDER-HIV-11/9/2004</p>

    <p class="memofmt1-2">          Nuclear import of HIV-1 integrase is inhibited in vitro by styrylquinoline derivatives</p>

    <p>          Mousnier, Aurelie, Leh, Herve, Mouscadet, Jean-Francois, and Dargemont, Catherine</p>

    <p>          Molecular Pharmacology <b>2004</b>.  66(4): 783-788</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>15.      44359   HIV-LS-309; SCIFINDER-HIV-11/9/2004</p>

    <p class="memofmt1-2">          Recent advances in the development of HIV-1 Tat-based vaccines</p>

    <p>          Caputo, Antonella, Gavioli, Riccardo, and Ensoli, Barbara</p>

    <p>          Current HIV Research <b>2004</b>.  2(4): 357-376</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>16.      44360   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          CpG oligodeoxynucleotides block human immunodeficiency virus type 1 replication in human lymphoid tissue infected ex vivo</p>

    <p>          Schlaepfer, E, Audige, A, von, Beust B, Manolova, V, Weber, M, Joller, H, Bachmann, MF, Kundig, TM, and Speck, RF</p>

    <p>          J Virol <b>2004</b>.  78(22): 12344-54</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507621&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507621&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.      44361   HIV-LS-309; SCIFINDER-HIV-11/9/2004</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by disruption of the processing of the viral Gag capsid-spacer peptide 1 protein to p24-Gag using betulin derivatives, such as 3-O-(3&#39;,3&#39;-dimethylsuccinyl) betulinic acid</p>

    <p>          Salzwedel, Karl, Li, Feng, Wild, Carl T, Allaway, Graham P, and Freed, Eric O</p>

    <p>          PATENT:  WO <b>2004069166</b>  ISSUE DATE:  20040819</p>

    <p>          APPLICATION: 2004  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>18.      44362   HIV-LS-309; SCIFINDER-HIV-11/9/2004</p>

    <p class="memofmt1-2">          Preparation of nucleosides useful as antiviral, antineoplastic, antibacterial, and antitumor agents</p>

    <p>          Sartorelli, Alan C, Cheng, Yung-Chi, and Liu, Mao-Chin</p>

    <p>          PATENT:  US <b>2004116362</b>  ISSUE DATE:  20040617</p>

    <p>          APPLICATION: 2002-58989  PP: 28 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>19.      44363   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          Concerted inhibitory activities of Phyllanthus amarus on HIV replication in vitro and ex vivo</p>

    <p>          Notka, F, Meier, G, and Wagner, R</p>

    <p>          Antiviral Res <b>2004</b>.  64(2): 93-102</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498604&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498604&amp;dopt=abstract</a> </p><br />

    <p>20.      44364   HIV-LS-309; SCIFINDER-HIV-11/9/2004</p>

    <p class="memofmt1-2">          Modified nucleosides as antiviral agents</p>

    <p>          Stuyver, Lieven J and Chu, Chung K</p>

    <p>          PATENT:  WO <b>2004043402</b>  ISSUE DATE:  20040527</p>

    <p>          APPLICATION: 2003  PP: 49 pp.</p>

    <p>          ASSIGNEE:  (Pharmasset, Ltd. USA and University of Georgia Research Foundation, Inc.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>21.      44365   HIV-LS-309; PUBMED-HIV-11/15/2004</p>

    <p class="memofmt1-2">          IL-10 production induced by HIV-1 Tat stimulation of human monocytes is dependent on the activation of PKC beta(II) and delta isozymes</p>

    <p>          Contreras, X, Bennasser, Y, and Bahraoui, E</p>

    <p>          Microbes Infect <b>2004</b>.  6(13): 1182-90</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15488737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15488737&amp;dopt=abstract</a> </p><br />

    <p>22.      44366   HIV-LS-309; WOS-HIV-11/7/2004</p>

    <p class="memofmt1-2">          QSAR study for HEPT derivatives, inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Mracec, M, Ivan, D, and Mracec, M</p>

    <p>          REVUE ROUMAINE DE CHIMIE <b>2004</b>.  49(5): 431-435, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224293900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224293900004</a> </p><br />
    <br clear="all">

    <p>23.      44367   HIV-LS-309; SCIFINDER-HIV-11/9/2004</p>

    <p class="memofmt1-2">          Synthesis of potentially antiviral 2&#39;,3&#39;-dideoxy-2&#39;-fluoro-3&#39;-(hydroxyamino)nucleosides</p>

    <p>          Wang, Songqing, Chang, Junbiao, Pan, Shifeng, and Zhao, Kang</p>

    <p>          Helvetica Chimica Acta <b>2004</b>.  87(2): 327-339</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.      44368   HIV-LS-309; WOS-HIV-11/7/2004</p>

    <p class="memofmt1-2">          Anti-Vpr activity of a yeast chaperone protein</p>

    <p>          Benko, Z, Liang, D, Agbottah, E, Hou, J, Chiu, K, Yu, M, Innis, S, Reed, P, Kabat, W, Elder, RT, Di, Marzio P, Taricani, L, Ratner, L, Young, PG, Bukrinsky, M, and Zhao, RYQ</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(20): 11016-11029, 14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224229000019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224229000019</a> </p><br />

    <p>25.      44369   HIV-LS-309; WOS-HIV-11/7/2004</p>

    <p class="memofmt1-2">          The current status of, and challenges in, the development of CCR5 inhibitors as therapeutics for HIV-1 infection</p>

    <p>          Maeda, K, Nakata, H, Ogata, H, Koh, Y, Miyakawa, T, and Mitsuya, H</p>

    <p>          CURRENT OPINION IN PHARMACOLOGY <b>2004</b>.  4(5): 447-452, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224262200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224262200004</a> </p><br />

    <p>26.      44370   HIV-LS-309; WOS-HIV-11/7/2004</p>

    <p class="memofmt1-2">          Selection of active ScFv to G-protein-coupled receptor CCR5 using surface antigen-mimicking peptides</p>

    <p>          Zhang, Y, Pool, C, Sadler, K, Yan, HP, Edl, J, Wang, XH, Boyd, JG, and Tam, JP</p>

    <p>          BIOCHEMISTRY <b>2004</b>.  43(39): 12575-12584, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224231500019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224231500019</a> </p><br />

    <p>27.      44371   HIV-LS-309; WOS-HIV-11/7/2004</p>

    <p class="memofmt1-2">          In vitro comparison of topical microbicides for prevention of human immunodeficiency virus type 1 transmission</p>

    <p>          Dezzutti, CS, James, VN, Ramos, A, Sullivan, ST, Siddig, A, Bush, TJ, Grohskopf, LA, Paxton, L, Subbarao, S, and Hart, CE</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(10): 3834-3844, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224217000029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224217000029</a> </p><br />

    <p>28.      44372   HIV-LS-309; WOS-HIV-11/7/2004</p>

    <p class="memofmt1-2">          Mannose-specific plant lectins from the Amaryllidaceae family qualify as efficient microbicides for prevention of human immunodeficiency virus infection</p>

    <p>          Balzarini, J, Hatse, S, Vermeire, K, Princen, K, Aquaro, S, Perno, CF, De, Clercq E, Egberink, H, Vanden, Mooter G, Peumans, W, Van, Damme E, and Schols, D</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(10): 3858-3870, 13</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224217000032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224217000032</a> </p><br />

    <p>29.      44373   HIV-LS-309; WOS-HIV-11/14/2004</p>

    <p class="memofmt1-2">          Nevirapine plus zidovudine to prevent mother-to-child transmission of HIV</p>

    <p>          Herzmann, C and Karcher, H</p>

    <p>          NEW ENGLAND JOURNAL OF MEDICINE <b>2004</b>.  351(19): 2013-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224851300017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224851300017</a> </p><br />

    <p>30.      44374   HIV-LS-309; WOS-HIV-11/14/2004</p>

    <p class="memofmt1-2">          Study on HIV-1 RT inhibitor - I. Synthesis of 1,3-dibenzyl-6-(3,4-epoxybutyl)uracil</p>

    <p>          Wang, XW, Zhang, ZL, Liu, J, Ma, XY, and Liu, JY</p>

    <p>          CHINESE JOURNAL OF ORGANIC CHEMISTRY <b>2004</b>.  24(10): 1271-1273, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224369300020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224369300020</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
