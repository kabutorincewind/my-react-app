

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-11-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hsQHb2Tt+zpcu6q+3v02X01o3hel6GcihO7/t2qPIy3V4sXKhnkFBoi95dAE38T/z2Q2dh8LrryAFS80K+C98ZPOXnAbYoSoVZyngrhfC7lvw3c6qAE8iuBlPi4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="52239545" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: October 24 - November 6, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25081563">Design, Synthesis, and Biological Activities of Novel Azole-bonded beta-Hydroxypropyl oxime O-ethers.</a> Behrouz, S., M.N. Rad, S. Rostami, M. Behrouz, E. Zarehnezhad, and A. Zarehnezhad. Molecular Diversity, 2014. 18(4): p. 797-808. PMID[25081563].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25338676">Relevant and Selective Activity of Pancratium illyricum L. Against Candida albicans Clinical Isolates: A Combined Effect on Yeast Growth and Virulence.</a> Bonvicini, F., F. Antognoni, C. Iannello, A. Maxia, F. Poli, and G.A. Gentilomi. BMC Complementary and Alternative Medicine, 2014. 14(1): p. 409. PMID[25338676].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25190736">Antifungal Susceptibility and Virulence Attributes of Animal-derived Isolates of Candida parapsilosis Complex.</a> Brilhante, R.S., J. Rodrigues Tde, S. Castelo-Branco Dde, C.E. Teixeira, B. Macedo Rde, S.P. Bandeira, L. Pereira de Alencar, A.J. Monteiro, A. Cordeiro Rde, J. Bandeira Tde, J.L. Moreira, J.J. Sidrim, and M.F. Rocha. Journal of Medical Microbiology, 2014. 63(Pt 11): p. 1568-1572. PMID[25190736].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25060742">Biofilm Removal and Antimicrobial Activity of Two Different Air-polishing Powders: An in Vitro Study.</a> Drago, L., M. Del Fabbro, M. Bortolin, C. Vassena, E. De Vecchi, and S. Taschieri. Journal of Periodontology, 2014. 85(11): p. e363-e369. PMID[25060742].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25149455">Novel 1,5,7-Trihydroxy-3-hydroxy methyl anthraquinone Isolated from Terrestrial streptomyces Sp. (eri-26) with Antimicrobial and Molecular Docking Studies.</a> Duraipandiyan, V., N.A. Al-Dhabi, C. Balachandran, M.K. Raj, M.V. Arasu, and S. Ignacimuthu. Applied Biochemistry and Biotechnology, 2014. 174(5): p. 1784-1794. PMID[25149455].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24724892">In Vitro Antifungal Effect and Inhibitory Activity on Biofilm Formation of Seven Commercial Mouthwashes.</a> Fu, J., P. Wei, C. Zhao, C. He, Z. Yan, and H. Hua. Oral Diseases, 2014. 20(8): p. 815-820. PMID[24724892].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25199776">In Vitro Combination of Voriconazole and Miltefosine against Clinically Relevant Molds.</a> Imbert, S., M. Palous, I. Meyer, E. Dannaoui, D. Mazier, A. Datry, and A. Fekkar. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6996-6998. PMID[25199776].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25136021">In Vitro Combination of Isavuconazole with Micafungin or Amphotericin B Deoxycholate against Medically Important Molds.</a> Katragkou, A., M. McCarthy, J. Meletiadis, V. Petraitis, P.W. Moradi, G.E. Strauss, M.M. Fouant, L.L. Kovanda, R. Petraitiene, E. Roilides, and T.J. Walsh. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6934-6937. PMID[25136021].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25106507">Chemical Synthesis, Structure-Activity Relationship, and Properties of Shepherin I: A Fungicidal Peptide Enriched in Glycine-glycine-histidine Motifs.</a> Remuzgo, C., T.S. Oewel, S. Daffre, T.R. Lopes, F.H. Dyszy, S. Schreier, G.M. Machado-Santelli, and M. Teresa Machini. Amino Acids, 2014. 46(11): p. 2573-2586. PMID[25106507].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25202127">Synergistic Effect of Amphotericin B and Tyrosol on Biofilm Formed by Candida krusei and Candida tropicalis from Intrauterine Device Users.</a> Shanmughapriya, S., H. Sornakumari, A. Lency, S. Kavitha, and K. Natarajaseenivasan. Medical Mycology, 2014. 52(8): p. 853-861. PMID[25202127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25053172">Biosynthesis and Characterization of Silver Nanoparticles Produced by Pleurotus ostreatus and their Anticandidal and Anticancer Activities</a>. Yehia, R.S. and H. Al-Sheikh. World Journal of Microbiology and Biotechnology, 2014. 30(11): p. 2797-2803. PMID[25053172].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25155986">Structure-guided Design of Thiazolidine Derivatives as Mycobacterium tuberculosis Pantothenate Synthetase Inhibitors.</a> Devi, P.B., G. Samala, J.P. Sridevi, S. Saxena, M. Alvala, E.G. Salina, D. Sriram, and P. Yogeeswari. ChemMedChem, 2014. 9(11): p. 2538-2547. PMID[25155986].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25238443">Synthesis, Structure-activity Relationship Studies, and Antibacterial Evaluation of 4-Chromanones and Chalcones, as well as Olympicin A and Derivatives.</a> Feng, L., M.M. Maddox, M.Z. Alam, L.S. Tsutsumi, G. Narula, D.F. Bruhn, X. Wu, S. Sandhaus, R.B. Lee, C.J. Simmons, Y.C. Tse-Dinh, J.G. Hurdle, R.E. Lee, and D. Sun. Journal of Medicinal Chemistry, 2014. 57(20): p. 8398-8420. PMID[25238443].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25224000">In Vitro Susceptibility of Mycobacterium tuberculosis Isolates to an Oral Carbapenem Alone or in Combination with Beta-lactamase Inhibitors.</a> Horita, Y., S. Maeda, Y. Kazumi, and N. Doi. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 7010-7014. PMID[25224000].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24750991">Synthesis and Antitubercular Screening of [(2-Chloroquinolin-3-yl)methyl] Thiocarbamide Derivatives.</a> Kumar, S., N. Upmanyu, O. Afzal, and S. Bawa. Chemical Biology &amp; Drug Design, 2014. 84(5): p. 522-530. PMID[24750991].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br />   

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25218911">Synthesis, Antimycobacterial and Antibacterial Evaluation of L-[(1R, 2S)-2-Fluorocyclopropyl]fluoroquinolone Derivatives Containing an Oxime Functional Moiety.</a> Liu, H., J. Huang, J. Wang, M. Wang, M. Liu, B. Wang, H. Guo, and Y. Lu. European Journal of Medicinal Chemistry, 2014. 86: p. 628-638. PMID[25218911].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25182645">Nitrotriazole- and Imidazole-based Amides and Sulfonamides as Antitubercular Agents.</a> Papadopoulou, M.V., W.D. Bloomer, H.S. Rosenzweig, A. Arena, F. Arrieta, J.C. Rebolledo, and D.K. Smith. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6828-6836. PMID[25182645].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25218910">Development of 2-(4-Oxoquinazolin-3(4H)-yl)acetamide Derivatives as Novel Enoyl-acyl Carrier Protein Reductase (InhA) Inhibitors for the Treatment of Tuberculosis.</a> Pedgaonkar, G.S., J.P. Sridevi, V.U. Jeankumar, S. Saxena, P.B. Devi, J. Renuka, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2014. 86: p. 613-627. PMID[25218910].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25259627">Structural Basis for Inhibition of Mycobacterial and Human Adenosine Kinase by 7-Substituted 7-(het)aryl-7-deazaadenine Ribonucleosides.</a> Snasel, J., P. Naus, J. Dostal, A. Hnizda, J. Fanfrlik, J. Brynda, A. Bourderioux, M. Dusek, H. Dvorakova, J. Stolarikova, H. Zabranska, R. Pohl, P. Konecny, P. Dzubak, I. Votruba, M. Hajduch, P. Rezacova, V. Veverka, M. Hocek, and I. Pichova. Journal of Medicinal Chemistry, 2014. 57(20): p. 8268-8279. PMID[25259627].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25165007">Biological Evaluation of Potent Triclosan-derived Inhibitors of the Enoyl-acyl Carrier Protein Reductase InhA in Drug-sensitive and Drug-resistant Strains of Mycobacterium tuberculosis.</a> Stec, J., C. Vilcheze, S. Lun, A.L. Perryman, X. Wang, J.S. Freundlich, W. Bishai, W.R. Jacobs, Jr., and A.P. Kozikowski. ChemMedChem, 2014. 9(11): p. 2528-2537. PMID[25165007].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25337810">Identification of MMV Malaria Box Inhibitors of Perkinsus marinus Using an ATP-based Bioluminescence Assay.</a> Aleman Resto, Y. and J.A. Fernandez Robledo. PLoS One, 2014. 9(10): p. e111051. PMID[25337810].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25114141">In Vitro Susceptibility of Plasmodium vivax to Antimalarials in Colombia.</a> Fernandez, D., C. Segura, M. Arboleda, G. Garavito, S. Blair, and A. Pabon. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6354-6359. PMID[25114141].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25114138">Antimalarial Activity of the Myxobacterial Macrolide Chlorotonil A.</a> Held, J., T. Gebru, M. Kalesse, R. Jansen, K. Gerth, R. Muller, and B. Mordmuller. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6378-6384. PMID[25114138].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br />  

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25147153">1H-1,2,3-Triazole Tethered Mono- and Bis-ferrocenylchalcone-beta-lactam Conjugates: Synthesis and Antimalarial Evaluation.</a> Kumar, K., B. Pradines, M. Madamet, R. Amalvict, and V. Kumar. European Journal of Medicinal Chemistry, 2014. 86: p. 113-121. PMID[25147153].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25147149">Highly Potent Anti-proliferative Effects of a Gallium(III) Complex with 7-Chloroquinoline thiosemicarbazone as a Ligand: Synthesis, Cytotoxic and Antimalarial Evaluation.</a> Kumar, K., S. Schniper, A. Gonzalez-Sarrias, A.A. Holder, N. Sanders, D. Sullivan, W.L. Jarrett, K. Davis, F. Bai, N.P. Seeram, and V. Kumar. European Journal of Medicinal Chemistry, 2014. 86: p. 81-86. PMID[25147149].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25142786">A New Meroterpenoid Tatenoic Acid from the Fungus Neosartorya tatenoi KKU-2NK23.</a> Yim, T., K. Kanokmedhakul, S. Kanokmedhakul, W. Sanmanoch, and S. Boonlue. Natural Product Research, 2014. 28(21): p. 1847-1852. PMID[25142786].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1024-110614.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342454100013">Antimicrobial and Antioxidant Activities of the Essential Oils of Some Aromatic Medicinal Plants (Pulicaria inuloides-Asteraceae and Ocimum forskolei-Lamiaceae).</a> Al-Hajj, N.Q.M., H.X. Wang, C.Y. Ma, Z.X. Lou, M. Bashari, and R. Thabit. Tropical Journal of Pharmaceutical Research, 2014. 13(8): p. 1287-1293. ISI[000342454100013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342730200007">Synthesis and Anti-mycobacterial Activity of New 4-Thiazolidinone and 1,3,4-Oxadiazole Derivatives of Isoniazid.</a> Bhat, M.A. Acta Poloniae Pharmaceutica, 2014. 71(5): p. 763-770. ISI[000342730200007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342351900002">Nitrite Impacts the Survival of Mycobacterium tuberculosis in Response to Isoniazid and Hydrogen Peroxide.</a> Cunningham-Bussel, A., F.C. Bange, and C.F. Nathan. MicrobiologyOpen, 2013. 2(6): p. 901-911. ISI[000342351900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342527400026">Volatile Oils from Ferula asafoetida Varieties and their Antimicrobial Activity.</a> Divya, K., K. Ramalakshmi, P.S. Murthy, and L.J.M. Rao. LWT-Food Science and Technology, 2014. 59(2): p. 774-779. ISI[000342527400026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000341883700018">Incorporation of Triphenylphosphonium Functionality Improves the Inhibitory Properties of Phenothiazine Derivatives in Mycobacterium tuberculosis.</a> Dunn, E.A., M. Roxburgh, L. Larsen, R.A.J. Smith, A.D. McLellan, A. Heikal, M.P. Murphy, and G.M. Cook. Bioorganic &amp; Medicinal Chemistry, 2014. 22(19): p. 5320-5328. ISI[000341883700018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342659600021">Carum copticum and Thymus vulgaris Oils Inhibit Virulence in Trichophyton rubrum and Aspergillus spp.</a> Khan, M.S.A., I. Ahmad, and S.S. Cameotra. Brazilian Journal of Microbiology, 2014. 45(2): p. 523-531. ISI[000342659600021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342198300016">In Vitro Activity of the Brazil Nut (Bertholletia excelsa HBK) Oil in Aflatoxigenic Strains of Aspergillus parasiticus.</a> Martins, M., A.M. Klusczcovski, and V.M. Scussel. European Food Research and Technology, 2014. 239(4): p. 687-693. ISI[000342198300016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000341821300010">Some New Four Coordinated Hg(II) Complexes: Synthesis, Characterization, Electrochemical Behavior, Antibacterial/Antifungal Properties, and Theoretical Investigation.</a> Montazerozohori, M., S. Yadegari, A. Naghiha, and A. Hojjati. Synthesis and Reactivity in Inorganic Metal-Organic and Nano-metal Chemistry, 2015. 45(1): p. 48-57. ISI[000341821300010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342726300006">Novel Ethyl 1,5-disubstituted-1H-pyrazole-3-carboxylates as a New Class of Antimicrobial Agents.</a> Radwan, A.A., M.M. Ghorab, M.S. Alsaid, and F.K. Alanazi. Acta Pharmaceutica, 2014. 64(3): p. 335-344. ISI[000342726300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342730200010">Antifungal and Cytotoxic Activities of Nannorrhops ritchiana Roots Extract.</a> Rashid, R., F. Mukhtar, and A. Khan. Acta Poloniae Pharmaceutica, 2014. 71(5): p. 789-793. ISI[000342730200010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342215300005">In Vitro Evaluation of Combination of Polyenes with EDTA against Aspergillus spp. by Different Methods (FICI and CI Model).</a> Ruhil, S., V. Kumar, M. Balhara, M. Malik, S. Dhankhar, M. Kumar, and A.K. Chhillar. Journal of Applied Microbiology, 2014. 117(3): p. 643-653. ISI[000342215300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=%20WOS:000342312600004">Chemical Composition and Antimicrobial Activity of Essential Oils from Different Parts of Daucus littoralis Smith subsp hyrcanicus Rech. f.</a> Yousefbeyk, F., A.R. Gohari, M.H.S. Sourmaghi, M. Amini, H. Jamalifar, M. Amin, F. Golfakhrabadi, N. Ramezani, and G. Amin. Journal of Essential Oil Bearing Plants, 2014. 17(4): p. 570-576. ISI[000342312600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1024-110614.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
