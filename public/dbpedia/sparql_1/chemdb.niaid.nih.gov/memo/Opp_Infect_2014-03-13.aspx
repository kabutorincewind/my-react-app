

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-03-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5JBz1RfjDeBiTqYaX/zHQEhnlKr528IqdUEsZGj+NGbYyx3+wfDiw+g5z9ff+R7bPhICzvl8UPYjXdhZaBDZJAAA6WUHnYRd+elHIAAgmL2GR6l851E7XXygB80=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EA13F1E1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: February 28 - March 13, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24577908">In Vitro Biological Activity of Decoction of Joshanda.</a>Abdullah, H. Inayat, H. Khan, L. Khan, M.I. Khan, S. Hassan, and M.A. Khan. Pakistan Journal of Pharmaceutical Sciences, 2014. 27(2): p. 239-243. PMID[24577908].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24480358">Synthesis and Biological Evaluation of Thiazoline Derivatives as New Antimicrobial and Anticancer Agents.</a> Altintop, M.D., Z.A. Kaplancikli, G.A. Ciftci, and R. Demirel. European Journal of Medicinal Chemistry, 2014. 74: p. 264-277. PMID[24480358].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24513049">Synthesis and Anti-candidal Activity of N-(4-Aryl/Cyclohexyl)-2-(pyridine-4-yl carbonyl) hydrazinecarbothioamide.</a> Bhat, M.A., A.A. Khan, S. Khan, M.A. Al-Omar, M.K. Parvez, M.S. Al-Dosari, and A. Al-Dhfyan. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(5): p. 1299-1302. PMID[24513049].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24412556">Antimicrobial, Antioxidant and Cytotoxic Activities of Propolis from Melipona orbignyi (Hymenoptera, Apidae).</a>Campos, J.F., U.P. Dos Santos, L.F. Macorini, A.M. de Melo, J.B. Balestieri, E.J. Paredes-Gamero, C.A. Cardoso, K. de Picoli Souza, and E.L. Dos Santos. Food and Chemical Toxicology, 2014. 65: p. 374-380. PMID[24412556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24412414">Antifungal Activity of Lactobacilli and Its Relationship with 3-Phenyllactic acid Production.</a> Cortes-Zavaleta, O., A. Lopez-Malo, A. Hernandez-Mendoza, and H.S. Garcia. International Journal of Food Microbiology, 2014. 173: p. 30-35. PMID[24412414].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24366745">Synergistic Effect of the Flavonoid Catechin, Quercetin, or Epigallocatechin Gallate with Fluconazole Induces Apoptosis in Candida tropicalis Resistant to Fluconazole.</a> da Silva, C.R., J.B. de Andrade Neto, R. de Sousa Campos, N.S. Figueiredo, L.S. Sampaio, H.I. Magalhaes, B.C. Cavalcanti, D.M. Gaspar, G.M. de Andrade, I.S. Lima, G.S. de Barros Viana, M.O. de Moraes, M.D. Lobo, T.B. Grangeiro, and H.V. Nobre Junior. Antimicrobial Agents and Chemotherapy, 2014. 58(3): p. 1468-1478. PMID[24366745].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24485783">Antifungal Ether diglycosides from Matayba guianensis Aublet.</a> de Assis, P.A., P.N. Theodoro, J.E. de Paula, A.J. Araujo, L.V. Costa-Lotufo, S. Michel, R. Grougnet, M. Kritsanida, and L.S. Espindola. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(5): p. 1414-1416. PMID[24485783].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24462847">Antifungal Activity of Oligochitosans (Short Chain Chitosans) against Some Candida Species and Clinical Isolates of Candida albicans: Molecular Weight-activity Relationship.</a>Kulikov, S.N., S.A. Lisovskaya, P.V. Zelenikhin, E.A. Bezrodnykh, D.R. Shakirova, I.V. Blagodatskikh, and V.E. Tikhonov. European Journal of Medicinal Chemistry, 2014. 74: p. 169-178. PMID[24462847].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24595088">Preparation and Antimicrobial Action of Three Tryptic Digested Functional Molecules of Bovine Lactoferrin.</a> Rastogi, N., N. Nagpal, H. Alam, S. Pandey, L. Gautam, M. Sinha, K. Shin, N. Manzoor, J.S. Virdi, P. Kaur, S. Sharma, and T.P. Singh. Plos One, 2014. 9(3): p. e90011. PMID[24595088].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24487187">Synthesis, Antifungal Activities and Molecular Docking Studies of Novel 2-(2,4-Difluorophenyl)-2-hydroxy-3-(1H-1,2,4-triazol-1-yl)propyl dithiocarbamates.</a> Zou, Y., S. Yu, R. Li, Q. Zhao, X. Li, M. Wu, T. Huang, X. Chai, H. Hu, and Q. Wu. European Journal of Medicinal Chemistry, 2014. 74: p. 366-374. PMID[24487187].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24581308">Plasmodium Vivax and Plasmodium Falciparum ex Vivo Susceptibility to Anti-malarials and Gene Characterization in Rondonia, West Amazon, Brazil.</a> Aguiar, A.C., D.B. Pereira, N.S. Amaral, L. De Marco, and A.U. Krettli. Malaria Journal, 2014. 13(1): p. 73. PMID[24581308].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24580778">In Vivo Antimalarial Activity of the Crude Leaf Extract and Solvent Fractions of Croton macrostachyus Hocsht. (Euphorbiaceae) against Plasmodium berghei in Mice.</a> Bantie, L., S. Assefa, T. Teklehaimanot, and E. Engidawork. BMC Complementary and Alternative Medicine, 2014. 14(1): p. 79. PMID[24580778].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24497437">Synthesis, Antimalarial Properties, and SAR Studies of Alkoxyurea-based HDAC Inhibitors.</a>Hansen, F.K., T.S. Skinner-Adams, S. Duffy, L. Marek, S.D. Sumanadasa, K. Kuna, J. Held, V.M. Avery, K.T. Andrews, and T. Kurz. ChemMedChem, 2014. 9(3): p. 665-670. PMID[24497437].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23988774">Potential Antimicrobial Agents for the Treatment of Multidrug-resistant Tuberculosis.</a> Alsaad, N., B. Wilffert, R. van Altena, W.C. de Lange, T.S. van der Werf, J.G. Kosterink, and J.W. Alffenaar. European Respiratory Journal, 2014. 43(3): p. 884-897. PMID[23988774].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24463645">Synthesis, Structure-Activity Relationship of Iodinated-4-aryloxymethyl-coumarins as Potential Anti-cancer and Anti-mycobacterial Agents.</a> Basanagouda, M., V.B. Jambagi, N.N. Barigidad, S.S. Laxmeshwar, V. Devaru, and Narayanachar. European Journal of Medicinal Chemistry, 2014. 74: p. 225-233. PMID[24463645].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24448419">Biological Evaluation of Diazene Derivatives as Anti-tubercular Compounds.</a> Cappoen, D., V. Majce, C. Uythethofken, D. Urankar, V. Mathys, M. Kocevar, L. Verschaeve, S. Polanc, K. Huygen, and J. Kosmrlj. European Journal of Medicinal Chemistry, 2014. 74: p. 85-94. PMID[24448419].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24482078">Inhibition of Mycobacterium tuberculosis Transaminase BioA by Aryl Hydrazines and Hydrazides.</a> Dai, R., D.J. Wilson, T.W. Geders, C.C. Aldrich, and B.C. Finzel. ChemBioChem, 2014. 15(4): p. 575-586. PMID[24482078].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24268596">Antimycobacterial Activity of Nitrogen Heterocycles Derivatives: Bipyridine Derivatives. Part III.</a> Danac, R. and Mangalagiu, II. European Journal of Medicinal Chemistry, 2014. 74: p. 664-670. PMID[24268596].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24253289">In Vivo Evaluation of Antibiotic Activity against Mycobacterium abscessus.</a> Lerat, I., E. Cambau, R. Roth Dit Bettoni, J.L. Gaillard, V. Jarlier, C. Truffot, and N. Veziris. The Journal of Infectious Diseases, 2014. 209(6): p. 905-912. PMID[24253289].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23627296">Syntheses and Antimycobacterial Activities of [(2S,3R)-2-(Amino)-4-(arenesulfonamido)-3-hydroxy-1-phenylbutane Derivatives.</a>Moreth, M., C.R. Gomes, M.C. Lourenco, R.P. Soares, M.N. Rocha, C.R. Kaiser, M.V. de Souza, S.M. Wardell, and J.L. Wardell. Journal of Medicinal Chemistry, 2014. 10(2): p. 189-200. PMID[23627296].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24486416">Synthesis and Evaluation of Anti-tubercular Activity of 6-(4-Substitutedpiperazin-1-yl) phenanthridine Analogues.</a>Nagesh, H.N., N. Suresh, K. Mahalakshmi Naidu, B. Arun, J. Padma Sridevi, D. Sriram, P. Yogeeswari, and K.V. Chandra Sekhar. European Journal of Medicinal Chemistry, 2014. 74: p. 333-339. PMID[24486416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24366743">Spiroindolone That Inhibits PfATPase4 Is a Potent, Cidal Inhibitor of Toxoplasma gondii Tachyzoites in Vitro and in Vivo.</a>Zhou, Y., A. Fomovska, S. Muench, B.S. Lai, E. Mui, and R. McLeod. Antimicrobial Agents and Chemotherapy, 2014. 58(3): p. 1789-1792. PMID[24366743].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0228-031314.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330519900009">Antibacterial and Antifungal Activity of Holothuria leucospilota Isolated from Persian Gulf and Oman Sea.</a> Adibpour, N., F. Nasr, F. Nematpour, A. Shakouri, and A. Ameri. Jundishapur Journal of Microbiology, 2014. 7(1): p. E8708 . ISI[000330519900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330518700097">Effectiveness of Voriconazole in the Treatment of Aspergillus fumigatus Associated Asthma.</a> Agbetile, J., M. Bourne, A. Fairs, B. Hargadon, D. Desai, C. Broad, J. Morley, P. Bradding, C. Brightling, R. Green, P. Haldar, C. Pashley, I. Pavord, and A. Wardlaw. Thorax, 2013. 68: p. A48-A48. ISI[000330518700097].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330866900001">Fumigant Antifungal Activity of Corymbia citriodora and Cymbopogon nardus Essential Oils and Citronellal against Three Fungal Species.</a> Aguiar, R.W.D., M.A. Ootani, S.D. Ascencio, T.P.S. Ferreira, M.M. dos Santos, and G.R. dos Santos. Scientific World Journal, 2014, 492138: p. 8. ISI[000330866900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330554400082">Designing and Synthesis of Novel Antimicrobial Heterocyclic Analogs of Fatty acids.</a> Ahmad, A., A. Ahmad, H. Varshney, A. Rauf, M. Rehan, N. Subbarao, and A.U. Khan. European Journal of Medicinal Chemistry, 2013. 70: p. 887-900. ISI[000330554400082].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330552200012">Antifungal Activity of Metabolites from the Marine Sponges Amphimedon Sp and Monanchora arbuscula against Aspergillus flavus Strains Isolated from Peanuts (Arachis hypogaea).</a> Arevabini, C., Y.D. Crivelenti, M.H. de Abreu, T.A. Bitencourt, M.F.C. Santos, R.G.S. Berlinck, E. Hajdu, R.O. Beleboni, A.L. Fachin, and M. Marins. Natural Product Communications, 2014. 9(1): p. 33-36. ISI[000330552200012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331180300001">The Dual Role of Candida glabrata Drug: H+ Antiporter CgAqr1 (Orf CAGL0J09944g) in Antifungal Drug and Acetic acid Resistance.</a> Costa, C., A. Henriques, C. Pires, J. Nunes, M. Ohno, H. Chibana, I. Sa-Correia, and M.C. Teixeira. Frontiers in Microbiology, 2013. 4: 170. ISI[000331180300001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330768100038">Synthesis and Characterization of Novel Benzimidazole Bearing Pyrazoline Derivatives as Potential Antimicrobial Agents.</a> Desai, N.C., D.D. Pandya, G.M. Kotadiya, and P. Desai. Medicinal Chemistry Research, 2014. 23(3): p. 1474-1487. ISI[000330768100038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330768100038">Composition, Antifungal and Antioxidant Properties of Hyssopus officinalis L. Subsp Pilifer (Pant.) Murb. Essential Oil and Deodorized Extracts.</a> Dzamic, A.M., M.D. Sokovic, M. Novakovic, M. Jadranin, M.S. Ristic, V. Tesevic, and P.D. Marin. Industrial Crops and Products, 2013. 51: p. 401-407. ISI[000330820800056].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330576600005">Antifungal Activity of Ag: Hydroxyapatite Thin Films Synthesized by Pulsed Laser Deposition on Ti and Ti Modified by TiO2 Nanotubes Substrates.</a> Erakovic, S., A. Jankovic, C. Ristoscu, L. Duta, N. Serban, A. Visan, I.N. Mihailescu, G.E. Stan, M. Socol, O. Iordache, I. Dumitrescu, C.R. Luculescu, D. Janackovic, and V. Miskovic-Stankovic. Applied Surface Science, 2014. 293: p. 37-45. ISI[000330576600005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330431800026">Screening of Indigenous Bacteria from Rhizosphere of Maize (Zea mays L.) for their Plant Growth Promotion Ability and Antagonism against Fungal and Bacterial Pathogens.</a> Farooq, U. and A. Bano. Journal of Animal and Plant Sciences, 2013. 23(6): p. 1642-1652. ISI[000330431800026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330561600018">Antitubercular Specific Activity of Ibuprofen and the Other 2-Arylpropanoic acids Using the HT-SPOTi Whole-cell Phenotypic Assay.</a> Guzman, J.D., D. Evangelopoulos, A. Gupta, K. Birchall, S. Mwaigwisya, B. Saxty, T.D. McHugh, S. Gibbons, J. Malkinson, and S. Bhakta. BMJ Open, 2013. 3(6): p. e002672. ISI[000330561600018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330552200034">Chemical Compositions and Antimicrobial Activity of the Essential Oils of Hornstedtia havilandii (Zingiberaceae).</a> Hashim, S.E., H.M. Sirat, and C.H. Yen. Natural Product Communications, 2014. 9(1): p. 119-120. ISI[000330552200034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330554400054">Co(II) and Cu(II) Pyrophosphate Complexes Have Selectivity and Potency against Mycobacteria Including Mycobacterium tuberculosis.</a> Hoffman, A.E., M. De Stefano, C. Shoen, K. Gopinath, D.F. Warner, M. Cynamon, and R.P. Doyle. European Journal of Medicinal Chemistry, 2013. 70: p. 589-593. ISI[000330554400054].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330554400014">Thiazole-aminopiperidine Hybrid Analogues: Design and Synthesis of Novel Mycobacterium tuberculosis GyrB Inhibitors.</a> Jeankumar, V.U., J. Renuka, P. Santosh, V. Soni, J.P. Sridevi, P. Suryadevara, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2013. 70: p. 143-153. ISI[000330554400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330768100005">Synthesis, Characterization, Biological Activity, and 3D-Qsar Studies on Some Novel Class of Pyrrole Derivatives as Antitubercular Agents.</a> Joshi, S.D., U.A. More, S.R. Dixit, H.H. Korat, T.M. Aminabhavi, and A.M. Badiger. Medicinal Chemistry Research, 2014. 23(3): p. 1123-1147. ISI[000330768100005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331039800001">Flower-Shaped ZnO Nanoparticles Synthesized by a Novel Approach at Near-room Temperatures with Antibacterial and Antifungal Properties.</a> Khan, M.F., M. Hameedullah, A.H. Ansari, E. Ahmad, M.B. Lohani, R.H. Khan, M.M. Alam, W. Khan, F.M. Husain, and I. Ahmad. International Journal of Nanomedicine, 2014. 9: p. 853-864. ISI[000331039800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330431800024">Investigation of Selected Serbian Lichens for Antioxidant, Antimicrobial and Anticancer Properties.</a> Kosanic, M., B. Rankovic, and T. Stanojkovic. Journal of Animal and Plant Sciences, 2013. 23(6): p. 1628-1633. ISI[000330431800024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330554400007">Azole-carbodithioate Hybrids as Vaginal Anti-candida Contraceptive Agents: Design, Synthesis and Docking Studies.</a> Kumar, L., N. La, V. Kumar, A. Sarswat, S. Jangir, V. Bala, L. Kumar, B. Kushwaha, A.K. Pandey, M.I. Siddiqi, P.K. Shukla, J.P. Maikhuri, G. Gupta, and V.L. Sharma. European Journal of Medicinal Chemistry, 2013. 70: p. 68-77. ISI[000330554400007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330914900022">The Application of GC-MS Combined with Chemometrics for the Identification of Antimicrobial Compounds from Selected Commercial Essential Oils.</a> Maree, J., G. Kamatou, S. Gibbons, A. Viljoen, and S. Van Vuuren. Chemometrics and Intelligent Laboratory Systems, 2014. 130: p. 172-181. ISI[000330914900022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330519900007">In Vitro Anti-candida Activity of the Hydroalcoholic Extracts of Heracleum persicum Fruit against Phatogenic Candida Species.</a> Nejad, B.S., M. Rajabi, A.Z. Mamoudabadi, and M. Zarrin. Jundishapur Journal of Microbiology, 2014. 7(1): p. E8703. ISI[000330519900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330637500036">Multicenter Study of Anidulafungin and Micafungin MIC Distributions and Epidemiological Cutoff Values for Eight Candida Species and the CLSI M27-A3 Broth Microdilution Method.</a> Pfaller, M.A., A. Espinel-Ingroff, B. Bustamante, E. Canton, D.J. Diekema, A. Fothergill, J. Fuller, G.M. Gonzalez, J. Guarro, C. Lass-Florl, S.R. Lockhart, E. Martin-Mazuelos, J.F. Meis, L. Ostrosky-Zeichner, T. Pelaez, G. St-Germain, and J. Turnidge. Antimicrobial Agents and Chemotherapy, 2014. 58(2): p. 916-922. ISI[000330637500036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330262100005">Identification of Novel Inhibitors against Mycobacterium tuberculosis L-Alanine dehydrogenase (MTB-AlaDH) through Structure-based Virtual Screening.</a> Saxena, S., P.B. Devi, V. Soni, P. Yogeeswari, and D. Sriram. Journal of Molecular Graphics &amp; Modelling, 2014. 47: p. 37-43. ISI[000330262100005]. <b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331252200020">Evaluation of Antimicrobial Activity of Extract, Fractions and Isolated Substances from Calliandra umbellifera Benth.</a> Silva, T.S., J.M. Gomes, C.P. De Menezes, M.F. Agra, M.S. Da Silva, E.O. Lima, and J.F. Tavares. Latin American Journal of Pharmacy, 2013. 32(9): p. 1408-1411. ISI[000331252200020]. <b>[WOS]</b>. OI_0228-031314.</p>

    <br />

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330554400033">Novel Camphane-based Anti-tuberculosis Agents with Nanomolar Activity.</a> Stavrakov, G., V. Valcheva, I. Philipova, and I. Doytchinova. European Journal of Medicinal Chemistry, 2013. 70: p. 372-379. ISI[000330554400033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0228-031314.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
