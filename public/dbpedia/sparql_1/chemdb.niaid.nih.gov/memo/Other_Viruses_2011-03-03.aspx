

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-03-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EE9k9s7f1crTWInNKCDlPEsrk3/cNyi78hH5cWTncfoiLL8xhat46mLd0WZ2RUxo1sZBE6sB0ZwTjatW9+vDmZKx3z6QoI+e4nLHseUmAYrsFQBaYB76sf/DTwE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="33823EC1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">February 18 - March 3, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21342525">Peptide Inhibition of Human Cytomegalovirus Infection</a>. Melnik, L.I., R.F. Garry, and C.A. Morris. Virology Journal, 2011. 8(1): p. 76; PMID[21342525].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0218-030311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21356589">Isatin-Beta-Thiosemicarbazones as Potent Herpes Simplex Virus Inhibitors</a>. Kang, I.J., L.W. Wang, T.A. Hsu, A. Yueh, C.C. Lee, Y.C. Lee, C.Y. Lee, Y.S. Chao, S.R. Shih, and J.H. Chern. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21356589].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0218-030311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21365705">Anti-Hepatitis B Virus Active Lactones from the Traditional Chinese Herb: Swertia Mileensis</a>. Geng, C.A., L.J. Wang, X.M. Zhang, Y.B. Ma, X.Y. Huang, J. Luo, R.H. Guo, J. Zhou, Y. Shen, A.X. Zuo, Z.Y. Jiang, and J.J. Chen. Chemistry (Weinheim an der Bergstrasse, Germany), 2011. <b>[Epub ahead of print]</b>; PMID[21365705].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21333535">Antiviral Agents 3. Discovery of a Novel Small Molecule Non-Nucleoside Inhibitor of Hepatitis B Virus (Hbv)</a>. Crosby, I.T., D.G. Bourke, E.D. Jones, T.P. Jeynes, S. Cox, J.A. Coates, and A.D. Robertson. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21333535].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0218-030311.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21357300">Inx-08189, a Phosphoramidate Prodrug of 6-O-Methyl-2&#39;-C-Methyl Guanosine, Is a Potent Inhibitor of Hepatitis C Virus Replication with Excellent Pharmacokinetic and Pharmacodynamic Properties</a>. Vernachio, J.H., B. Bleiman, K.D. Bryant, S. Chamberlain, D. Hunley, J. Hutchins, B. Ames, E. Gorovits, B. Ganguly, A. Hall, A. Kolykhalov, Y. Liu, J. Muhammad, N. Raja, C.R. Walters, J. Wang, K. Williams, J.M. Patti, G. Henson, K. Madela, M. Aljarah, A. Gilles, and C. McGuigan. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21357300].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21354229">Naringenin Inhibits the Assembly and Long-Term Production of Infectious Hepatitis C Virus Particles through a Ppar-Mediated Mechanism</a>. Goldwasser, J., P.Y. Cohen, W. Lin, D. Kitsberg, P. Balaguer, S.J. Polyak, R.T. Chung, M.L. Yarmush, and Y. Nahmias. Journal of Hepatology, 2011. <b>[Epub ahead of print]</b>; PMID[21354229].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0218-030311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285724800037">Molecular Modeling Based Approach, Synthesis and in Vitro Assay to New Indole Inhibitors of Hepatitis C Ns3/4a Serine Protease</a>. Ismail, N.S.M. and M. Hattori. Bioorganic &amp; Medicinal Chemistry, 2011. 19(1): p. 374-383; ISI[000285724800037].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285437000057">Preparation and Antiherpetic Activities of Chemically Modified Polysaccharides from Polygonatum Cyrtonema Hua</a>. Liu, X.X., Z.J. Wan, L. Shi, and X.X. Lu. Carbohydrate Polymers, 2011. 83(2): p. 737-742; ISI[000285437000057].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285726300020">Effect of Tunicamycin on the Biogenesis of Hepatitis C Virus Glycoproteins</a>. Reszka, N., E. Krol, A.H. Patel, and B. Szewczyk. Acta Biochimica Polonica, 2010. 57(4): p. 541-546; ISI[000285726300020].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286432100006">Salt Stress Enhancement of Antioxidant and Antiviral Efficiency of Spirulina Platensis</a>. Shalaby, E.A., S.M.M. Shanab, and V. Singh. Journal of Medicinal Plants Research, 2010. 4(24): p. 2622-2632; ISI[000286432100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0218-030311.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
