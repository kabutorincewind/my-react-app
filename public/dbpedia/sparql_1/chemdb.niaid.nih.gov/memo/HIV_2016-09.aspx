

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q3ffyILm0QAX52HdRgZqPYNB7F6E6mOTE4riUifiewgm8OjrotxKyWEFIT9sR3LHleghB6FZc2GnhRHQZK6wO/YmrFId9kGMeXjyS/fvzvcGHKieORu7dn9xPmM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6A346492" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: September, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27652979">Fragment-based Discovery of a New Family of Non-peptidic Small-molecule Cyclophilin Inhibitors with Potent Antiviral Activities.</a> Ahmed-Belkacem, A., L. Colliandre, N. Ahnou, Q. Nevers, M. Gelin, Y. Bessin, R. Brillet, O. Cala, D. Douguet, W. Bourguet, I. Krimm, J.M. Pawlotsky, and J.F. Guichou. Nature Communications, 2016. 7(12777): 11pp. PMID[27652979]. PMCID [PMC5036131].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">2. <a href="https://www.ncbi.nlm.nih.gov/pubmed/26736075">Production of a Highly Potent Epoxide through the Microbial Metabolism of 3-Acetoxyurs-11-en-13,28-olide by Aspergillus niger Culture.</a> Ali, S., M. Nisar, and H. Gulab. Pharmaceutical Biology, 2016. 54(9): p. 1942-1946. PMID[26736075].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">3. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27331774">Covalent Inhibition of HIV-1 Integrase by N-succinimidyl Peptides.</a> Chandra, K., P. Das, S. Mamidi, M. Hurevich, A. Iosub-Amir, N. Metanis, M. Reches, and A. Friedler. ChemMedChem, 2016. 11(18): p. 1987-1994. PMID[27331774].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">4. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27616133">Abacavir + Dolutegravir + Lamivudine for the Treatment of HIV.</a> Comi, L. and F. Maggiolo. Expert Opinion in Pharmacotherapy, 2016. 17(15): p. 2097-2106. PMID[27616133].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">5. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27546050">Rational Design, Synthesis and Evaluation of Coumarin Derivatives as Protein-Protein Interaction Inhibitors.</a> De Luca, L., F.E. Agharbaoui, R. Gitto, M.R. Buemi, F. Christ, Z. Debyser, and S. Ferro. Molecular Informatics, 2016. 35(8-9): p. 460-473. PMID[27546050].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">6. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27563405">Aligning Potency and Pharmacokinetic Properties for Pyridine-Based NCINIs</a>. Fader, L.D., M. Bailey, E. Beaulieu, F. Bilodeau, P. Bonneau, Y. Bousquet, R.J. Carson, C. Chabot, R. Coulombe, J.M. Duan, C. Fenwick, M. Garneau, T. Halmos, A. Jakalian, C. James, S.H. Kawai, S. Landry, S.R. LaPlante, S.W. Mason, S. Morin, N. Rioux, B. Simoneau, S. Surprenant, B. Thavonekham, C. Thibeault, T. Trinh, Y. Tsantrizos, J. Tsoung, C. Yoakim, and D. Wernic. ACS Medicinal Chemistry Letters, 2016. 7(8): p. 797-801. PMID[27563405]. PMCID[PMC4983734].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000381144200006">Activity of Phosphino Palladium(II) and Platinum(II) Complexes against HIV-1 and Mycobacterium Tuberculosis.</a> Gama, N.H., A.Y.F. Elkhadir, B.G. Gordhan, B.D. Kana, J. Darkwa, and D. Meyer. Biometals, 2016. 29(4): p. 637-650. ISI[000381144200006].
    <br />
    <b>[WOS]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">8. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27400283">Discovery of Novel anti-HIV Agents via Cu(I)-catalyzed Azide-alkyne Cycloaddition (CuAAC) Click Chemistry-based Approach.</a> Gao, P., L. Sun, J. Zhou, X. Li, P. Zhan, and X. Liu. Expert Opinion on Drug Discovery, 2016. 11(9): p. 857-871. PMID[27400283].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">9. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27434587">Development of an Attenuated Tat Protein as a Highly-effective Agent to Specifically Activate HIV-1 Latency.</a> Geng, G., B. Liu, C. Chen, K. Wu, J. Liu, Y. Zhang, T. Pan, J. Li, Y. Yin, J. Zhang, F. Huang, F. Yu, J. Chen, X. Ma, J. Zhou, E. Kuang, C. Liu, W. Cai, and H. Zhang. Molecular Therapy, 2016. 24(9): p. 1528-1537. PMID[27434587].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">10. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27541578">Design, Synthesis, and Evaluation of Thiophene[3,2-d]pyrimidine Derivatives as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors with Significantly Improved Drug Resistance Profiles.</a> Kang, D., Z. Fang, Z. Li, B. Huang, H. Zhang, X. Lu, H. Xu, Z. Zhou, X. Ding, D. Daelemans, E. De Clercq, C. Pannecouque, P. Zhan, and X. Liu. Journal of Medicinal Chemistry, 2016. 59(17): p. 7991-8007. PMID[27541578].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">11. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27062197">Design, Synthesis, and Biological Evaluation of Novel 5-Alkyl-6-adamantylmethylpyrimidin-4(3H)-ones as HIV-1 Non-Nucleoside Reverse-Transcriptase Inhibitors.</a> Li, W., B. Huang, D. Kang, E. De Clercq, D. Daelemans, C. Pannecouque, P. Zhan, and X. Liu. Chemical Biology &amp; Drug Design, 2016. 88(3): p. 380-385. PMID[27062197].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">12. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27617994">Synthesis and anti-HIV-1 Activity Evaluation for Novel 3a,6a-Dihydro-1H-pyrrolo[3,4-c]pyrazole-4,6-dione Derivatives.</a> Liu, G.N., R.H. Luo, Y. Zhou, X.J. Zhang, J. Li, L.M. Yang, Y.T. Zheng, and H. Liu. Molecules, 2016. 21(9): p. E1198. PMID[27617994].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">13. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27155865">Glycosyl Phosphatidylinositol-anchored C34 Peptide Derived from Human Immunodeficiency Virus Type 1 Gp41 Is a Potent Entry Inhibitor.</a> Liu, L., M. Wen, Q. Zhu, J.T. Kimata, and P. Zhou. Journal of Neuroimmune Pharmacology, 2016. 11(3): p. 601-610. PMID[27155865].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">14. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27501911">Arylazolyl(azinyl)thioacetanilides. Part 20: Discovery of Novel Purinylthioacetanilides Derivatives as Potent HIV-1 NNRTIs via a Structure-based Bioisosterism Approach</a>. Lu, X., X. Li, J. Yang, B. Huang, D. Kang, F. Zhao, Z. Zhou, E. De Clercq, D. Daelemans, C. Pannecouque, P. Zhan, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2016. 24(18): p. 4424-4433. PMID[27501911].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">15. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27431232">Fullerene Derivatives Strongly Inhibit HIV-1 Replication by Affecting Virus Maturation without Impairing Protease Activity.</a> Martinez, Z.S., E. Castro, C.S. Seong, M.R. Ceron, L. Echegoyen, and M. Llano. Antimicrobial Agents and Chemotherapy, 2016. 60(10): p. 5731-5741. PMID[27431232]. <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">16. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27641362">Linear Biocompatible Glyco-polyamidoamines as Dual Action Mode Virus Infection Inhibitors with Potential as Broad-spectrum Microbicides for Sexually Transmitted Diseases.</a> Mauro, N., P. Ferruti, E. Ranucci, A. Manfredi, A. Berzi, M. Clerici, V. Cagno, D. Lembo, A. Palmioli, and S. Sattin. Scientific Reports, 2016. 6(33393): 8pp. PMID[27641362]. PMCID[PMC5027566].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">17. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27512074">Novel Acylguanidine-based Inhibitor of HIV-1.</a> Mwimanzi, P., I. Tietjen, S.C. Miller, A. Shahid, K. Cobarrubias, N.N. Kinloch, B. Baraki, J. Richard, A. Finzi, D. Fedida, Z.L. Brumme, and M.A. Brockman. Journal of Virology, 2016. 90(20): p. 9495-9508. PMID[27512074].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">18. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27489345">Structural Basis of HIV Inhibition by Translocation-defective RT Inhibitor 4 &#39;-Ethynyl-2-fluoro-2 &#39;-deoxyadenosine (EFdA).</a> Salie, Z.L., K.A. Kirby, E. Michailidis, B. Marchand, K. Singh, L.C. Rohan, E.N. Kodama, H. Mitsuya, M.A. Parniak, and S.G. Sarafianos. Proceedings of the National Academy of Sciences of the United States of America, 2016. 113(33): p. 9274-9279.PMID[27489345]. PMCID[PMC4995989].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">19. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27398994">Pyrazolo[1,5-a]pyrimidine-based Macrocycles as Novel HIV-1 Inhibitors: A Patent Evaluation of WO2015123182.</a> Sun, L., P. Gao, P. Zhan, and X. Liu. Expert Opinion on Therapeutic Patents, 2016. 26(9): p. 979-986. PMID[27398994].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">20. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27350006">Croton megalobotrys Mull Arg. and Vitex doniana (Sweet): Traditional Medicinal Plants in a Three-step Treatment Regimen That Inhibit in Vitro Replication of HIV-1.</a> Tietjen, I., T. Gatonye, B.N. Ngwenya, A. Namushe, S. Simonambanga, M. Muzila, P. Mwimanzi, J. Xiao, D. Fedida, Z.L. Brumme, M.A. Brockman, and K. Andrae-Marobela. Journal of Ethnopharmacology, 2016. 191: p. 331-340. PMID[27350006].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">21. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27607424">HIV Protease Inhibitors Apoptotic Effect in SH-SY5Y Neuronal Cell Line.</a> Tricarico, P.M., R.F. de Oliveira Franca, S. Pacor, V. Ceglia, S. Crovella, and F. Celsi. Cellular Physiology and Biochemistry, 2016. 39(4): p. 1463-1470. PMID[27607424].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">22. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27222146">RbAp48, a Novel Inhibitory Factor That Regulates the Transcription of Human Immunodeficiency Virus Type 1.</a> Wang, J., J. Yang, Z.X. Yang, X.Y. Lu, C.Z. Jin, L.F. Cheng, and N.P. Wu. International Journal of Molecular Medicine, 2016. 38(1): p. 267-274. PMID[27222146].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">23. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27604950">M2BP Inhibits HIV-1 Virion Production in a Vimentin Filaments-dependent Manner.</a> Wang, Q., X.L. Zhang, Y.L. Han, X.L. Wang, and G.X. Gao. Scientific Reports, 2016. 6(32736): 14pp. PMID[27604950]. PMCID[PMC5015019].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">24. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27454202">Zinc-stabilized Chitosan-chondroitin sulfate Nanocomplexes for HIV-1 Infection Inhibition Application.</a> Wu, D., A. Ensinas, B. Verrier, C. Primard, A. Cuvillier, G. Champier, S. Paul, and T. Delair. Molecular Pharmaceutics, 2016. 13(9): p. 3279-3291. PMID[27454202].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">25. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27062664">Synthesis of Pyrazine-1,3-thiazine Hybrid Analogues as Antiviral Agent against HIV-1, Influenza a (H1N1), Enterovirus 71 (Ev71), and Coxsackievirus B3 (CVB3).</a> Wu, H.M., K. Zhou, T. Wu, and Y.G. Cao. Chemical Biology &amp; Drug Design, 2016. 88(3): p. 411-421. PMID[27062664].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">26. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27411556">Discovery of HIV Type1 Aspartic Protease Hit Compounds through Combined Computational Approaches.</a> Xanthopoulos, D., E. Kritsi, C.T. Supuran, M.G. Papadopoulos, G. Leonis, and P. Zoumpoulakis. ChemMedChem, 2016. 11(15): p. 1646-1652. PMID[27411556].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">27. <a href="https://www.ncbi.nlm.nih.gov/pubmed/26802545">Design, Synthesis and anti-HIV Evaluation of Novel Diarylpyridine Derivatives Targeting the Entrance Channel of NNRTI Binding Pocket.</a> Yang, J.P., W.M. Chen, D.W. Kang, X.Y. Lu, X. Li, Z.Q. Liu, B.S. Huang, D. Daelemans, C. Pannecouque, E. De Clercq, P. Zhan, and X.Y. Liu. European Journal of Medicinal Chemistry, 2016. 109: p. 294-304. PMID[26802545].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">28. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27547792">Data on Synthesis of Methylene bisphosphonates and Screening of Their Inhibitory Activity Towards HIV Reverse Transcriptase.</a> Yanvarev, D.V., A.N. Korovina, N.N. Usanov, O.A. Khomich, J. Vepsalainen, E. Puljula, M.K. Kukhanova, and S.N. Kochetkov. Data in Brief, 2016. 8: p. 1157-1167. PMID[27547792]. PMC4978200[PMC4978200].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">29. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27225069">Ribosome-inactivating Proteins from Root Tubers and Seeds of Trichosan-thes kirilowii and Other Trichosanthes Species.</a> Ye, X.J., C.C.W. Ng, J.H. Wong, T.B. Ng, G.H.H. Chan, S.Z. Guan, and O. Sha. Protein and Peptide Letters, 2016. 23(8): p. 699-706. PMID[27225069].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">30. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27594443">Venom Components of Iranian Scorpion Hemiscorpius lepturus Inhibit the Growth and Replication of Human Immunodeficiency Virus 1 (HIV-1).</a> Zabihollahi, R., K. Pooshang Bagheri, Z. Keshavarz, F. Motevalli, G. Bahramali, S.D. Siadat, S.B. Momen, D. Shahbazzadeh, and M.R. Aghasadeghi. Iran Biomed J, 2016. 20(5): p. 259-265. PMID[27594443].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">31. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27666394">Rational Improvement of Gp41-targeting HIV-1 Fusion Inhibitors: An Innovatively Designed Ile-Asp-Leu Tail with Alternative Conformations.</a> Zhu, Y., S. Su, L. Qin, Q. Wang, L. Shi, Z. Ma, J. Tang, S. Jiang, L. Lu, S. Ye, and R. Zhang. Scientific Reports, 2016. 6(31983): 9pp. PMID[27666394]. PMCID[PMC5036048].
    <br />
    <b>[PubMed]</b>. HIV_09_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">32. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=WO&amp;NR=2016128541A1&amp;KC=A1">Preparation of Triazole and Tetrazole Inhibitors of Human DDX3 Helicase as Antiviral Therapeutic Agents.</a> Meyerhans, A., M.-A. Martinez de la Sierra, A. Brai, R. Fazi, C. Tintori, M. Botta, J.-E. Araque, and J. Martinez. Patent. 2016. 2016-EP52990 2016128541: 174pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">33. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=WO&amp;NR=2016130628A1&amp;KC=A1">Griffithsin Mutants.</a> O&#39;Keefe, B.R., T. Moulaei, K.E. Palmer, L.C. Rohan, and J.L. Fuqua. Patent. 2016. 2016-US17267 2016130628: 32pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160824&amp;CC=EP&amp;NR=3058940A1&amp;KC=A1">Quinoline Derivatives for Use in the Treatment or Prevention of Viral Infection, in Particular HIV Infection.</a> Scherrer, D., A. Garcel, N. Campos, J. Tazi, A. Vautrin, F. Mahuteau-Betzer, R. Najman, and P. Fornarelli. Patent. 2016. 2015-305277 3058940: 39pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160728&amp;CC=US&amp;NR=2016213647A1&amp;KC=A1">Compositions and Methods for Inhibiting Viral Infection by Administering AR-12 and Its Analogs.</a> Schlesinger, L., J. Yount, A. Zukiwski, S. Proniuk, M.J. Tunon, and K. Zandi. Patent. 2016. 2016-15008306 20160213647: 43pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_09_2016.</p>

    <br />

    <p class="plaintext">36. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=US&amp;NR=2016235750A1&amp;KC=A1">Small Molecules Targeting HIV-1 Nef.</a> Smithgall, T.E. Patent. 2016. 2016-15042069 20160235750: 16pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_09_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
