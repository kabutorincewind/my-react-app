

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-04-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ielxM5LZlpYP15qEGEQeW3BnqbC8Sawh/QKgwtV60Je/vUrXy96ulQVlZJJcQ+/PTnxvJGOf6BJq+qtQOeQryG3hiAxR1xInqkDub5NhnGs3wBm4uqiHTxOxlAQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="967C3AC5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 29 - April 11, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23434420">Anti-HBV Active Constituents from Piper longum.</a> Jiang, Z.Y., W.F. Liu, X.M. Zhang, J. Luo, Y.B. Ma, and J.J. Chen. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2123-2127; PMID[23434420].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23353737">Anti-Hepatitis B Virus and Anti-cancer Activities of Novel Isoflavone Analogs.</a> Zhang, Y., H. Zhong, Z. Lv, M. Zhang, T. Zhang, Q. Li, and K. Li. European Journal of Medicinal Chemistry, 2013. 62: p. 158-167; PMID[23353737].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0329-041113.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23466233">Synthesis and Evaluation of Non-dimeric HCV NS5A Inhibitors.</a> Amblard, F., H. Zhang, L. Zhou, J. Shi, D.R. Bobeck, J.H. Nettles, S. Chavre, T.R. McBrayer, P. Tharnish, T. Whitaker, S.J. Coats, and R.F. Schinazi. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2031-2034; PMID[23466233]. <b>[PubMed]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23453067">Synthesis and Evaluation against Hepatitis C Virus of 7-Deaza Analogues of 2&#39;-C-Methyl-6-O-methyl guanosine Nucleoside and L-Alanine ester phosphoramidates.</a> Bourdin, C., C. McGuigan, A. Brancale, S. Chamberlain, J. Vernachio, J. Hutchins, E. Gorovits, A. Kolykhalov, J. Muhammad, J. Patti, G. Henson, B. Bleiman, K.D. Bryant, B. Ganguly, D. Hunley, A. Obikhod, C.R. Walters, J. Wang, C.V. Ramamurty, S.K. Battina, and C. Srivinas Rao. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2260-2264; PMID[23453067].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23517538">Achiral Pyrazinone-based Inhibitors of the Hepatitis C Virus NS3 Protease and Drug-Resistant Variants with Elongated Substituents Directed toward the S2 Pocket.</a> Gising, J., A.K. Belfrage, H. Alogheli, A. Ehrenberg, E. Akerblom, R. Svensson, P. Artursson, A. Karlen, U.H. Danielson, M. Larhed, and A. Sandstrom. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[23517538].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23544424">Hepatitis C Replication Inhibitors That Target the Viral NS4B Protein.</a> Miller, J.F., P.Y. Chong, J.B. Shotwell, J.G. Catalano, V.W. Tai, J.M. Fang, A.L. Banka, C.D. Roberts, M.K. Youngman, H. Zhang, Z. Xiong, A. Mathis, J.J. Pouliot, R.K. Hamatake, D.J. Price, J.W. Seal, L.L. Stroup, K.L. Creech, L.H. Carballo, D. Todd, A. Spaltenstein, S.M. Furst, Z. Hong, and A.J. Peat. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[23544424].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23567084">NS5A Inhibitors in the Treatment of Hepatitis C.</a> Pawlotsky, J.M. Journal of Hepatology, 2013. <b>[Epub ahead of print]</b>; PMID[23567084].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23490373">IL-28b (IFN-lambda3) and IFN-alpha Synergistically Inhibit HCV Replication.</a> Shindo, H., S. Maekawa, K. Komase, M. Miura, M. Kadokura, R. Sueki, N. Komatsu, K. Shindo, F. Amemiya, Y. Nakayama, T. Inoue, M. Sakamoto, A. Yamashita, K. Moriishi, and N. Enomoto. Journal of Viral Hepatitis, 2013. 20(4): p. 281-289; PMID[23490373].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0329-041113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315568800001">Telaprevir: Protease Inhibitor Drug Against Hepatitis C.</a> Bourel-Bonnet, L. and E. Kellenberger. Virologie, 2013. 17(1): p. 3-5; ISI[000315568800001].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315957100003">5-(Perylen-3-yl) ethynyl-arabino-uridine (AUY11), an Arabino-based Rigid Amphipathic Fusion Inhibitor, Targets Virion Envelope Lipids to Inhibit Fusion of Influenza Virus, Hepatitis C Virus, and Other Enveloped Viruses.</a> Colpitts, C.C., A.V. Ustinov, R.F. Epand, R.M. Epand, V.A. Korshun, and L.M. Schang. Journal of Virology, 2013. 87(7): p. 3640-3654; ISI[000315957100003].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315648400014">Macromolecular Prodrugs of Ribavirin Combat Side Effects and Toxicity with No Loss of Activity of the Drug.</a> Kryger, M.B.L., B.M. Wohl, A.A.A. Smith, and A.N. Zelikin. Chemical Communications, 2013. 49(26): p. 2643-2645; ISI[000315648400014].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0329-041113.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315756700002">Dioscin&#39;s Antiviral Effect in Vitro.</a> Liu, C.H., Y. Wang, C.C. Wu, R.J. Pei, J.H. Song, S.Y. Chen, and X.W. Chen. Virus Research, 2013. 172(1-2): p. 9-14; ISI[000315756700002].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0329-041113.</p> 
    
    <br />
    
    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000315643400010">The Postbinding Activity of Scavenger Receptor Class B Type I Mediates Initiation of Hepatitis C Virus Infection and Viral Dissemination.</a> Zahid, M.N., M. Turek, F. Xiao, V.L.D. Thi, M. Guerin, I. Fofana, P. Bachellier, J. Thompson, L. Delang, J. Neyts, D. Bankwitz, T. Pietschmann, M. Dreux, F.L. Cosset, F. Grunert, T.F. Baumert, and M.B. Zeisel. Hepatology, 2013. 57(2): p. 492-504; ISI[000315643400010].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0329-041113.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
