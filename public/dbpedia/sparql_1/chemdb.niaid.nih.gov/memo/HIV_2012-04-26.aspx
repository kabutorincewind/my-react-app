

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-04-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="2ScQ/WoTf+kP4WArFNZbDJjHKqrEeAnSXGx+izf06yiT6Dc9/6tnmew3efVcbKb4rTZJcis4ymFEKeLgfp5YRScuyfaV3E7XN9wR8UdKyAKdAAzX9tjCySEgWd4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="09BF44BB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: April 13 - April 26, 2012</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22458611">Synthesis and Biological Activity of Potent HIV-1 Protease Inhibitors Based on Phe-Pro Dihydroxyethylene Isosteres.</a> Benedetti, F., F. Berti, S. Budal, P. Campaner, F. Dinon, A. Tossi, R. Argirova, P. Genova, V. Atanassov, and A. Hinkov, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22458611].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22528484">Sialic acid Associated to &#945;v&#946;3 Integrin Mediates HIV-1 Tat Interaction and Endothelial Cell Proangiogenic Activation.</a> Chiodelli, P., C. Urbinati, S. Mitola, E. Tanghetti, and M. Rusnati, Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22528484].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22414613">Synthesis and Biological Evaluation of Novel Small Non-peptidic HIV-1 PIs: The Benzothiophene Ring as an Effective Moiety.</a> Chiummiento, L., M. Funicello, P. Lupattelli, F. Tramutola, F. Berti, and F. Marino-Merlo, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2948-2950; PMID[22414613].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22511760">Structural Insight of HIV-1 Fusion Inhibitor CP621-652 Discovers the Critical Residues for Viral Entry and Inhibition.</a> Chong, H., X. Yao, Z. Qiu, B. Qin, R. Han, S. Waltersperger, M. Wang, S. Cui, and Y. He, Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22511760].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22524483">Design, Synthesis and Antiviral Activity of Entry Inhibitors that Target the CD4-binding Site of HIV-1.</a> Curreli, F., S. Choudhury, I. Pyatkin, V.P. Zagorodnikov, A.K. Bulay, A. Altieri, Y.D. Kwon, P.D. Kwong, and A.K. Debnath, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22524483].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22508308">Activity of Human Immunodeficiency Virus Type 1 Protease Inhibitors against the Initial Autocleavage in Gag-Pol Polyprotein Processing.</a> Davis, D.A., E.E. Soule, K.S. Davidoff, S.I. Daniels, N.E. Naiman, and R. Yarchoan, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22508308].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22504900">Synthesis, anti-HIV Activity Studies, and in Silico Rationalization of Cyclobutane-fused Nucleosides.</a> Figueras, A., R. Miralles-Lluma, R. Flores, A. Rustullet, F. Busque, M. Figueredo, J. Font, R. Alibes, and J.D. Marechal, ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22504900].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22514337">Initial HIV- 1 Antigen-specific CD8+ T cells In Acute HIV-1 Inhibit Transmitted Founder Virus Replication.</a> Freel, S.A., R.A. Picking, G. Ferrari, H. Ding, C. Ochsenbauer, J.C. Kappes, J. Kirchherr, K. Soderberg, K.J. Weinhold, C.K. Cunningham, T. Denny, J.A. Crump, M.S. Cohen, A.J. McMichael, B.F. Haynes, and G.D. Tomaras, Journal of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22514337].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22497421">Structure-based Design, Synthesis, and Characterization of Dual Hotspot Small-Molecule HIV-1 Entry Inhibitors.</a> Lalonde, J.M., Y.D. Kwon, D.M. Jones, A.W. Sun, J.R. Courter, T. Soeta, T. Kobayashi, A.M. Princiotto, X. Wu, A. Schon, E. Freire, P.D. Kwong, J.R. Mascola, J. Sodroski, N. Madani, and A.B. Smith, 3rd, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22497421].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22417649">Synthesis and Antiviral Activity of 6-Deoxycyclopropavir, a New Prodrug of Cyclopropavir.</a> Li, C., D.C. Quenelle, M.N. Prichard, J.C. Drach, and J. Zemlicka, Bioorganic &amp; Medicinal Chemistry, 2012. 20(8): p. 2669-2674; PMID[22417649].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22530578">Recent Advances in the Treatment of HIV/HBV and HIV/HCV Co-infection.</a> Masgala, A., S. Bonovas, and G.K. Nikolopoulos, Mini Reviews in Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22530578].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22527633">Do Opioids Activate Latent HIV-1 by Down-regulating Anti-HIV microRNAs?</a> Purohit, V., R.S. Rapaka, J. Rutter, and D. Shurtleff, Journal of Neuroimmune Pharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22527633].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22430735">Morphine and Galectin-1 Modulate HIV-1 Infection of Human Monocyte-derived Macrophages.</a> Reynolds, J.L., W.C. Law, S.D. Mahajan, R. Aalinkeel, B. Nair, D.E. Sykes, M.J. Mammen, K.T. Yong, R. Hui, P.N. Prasad, and S.A. Schwartz, Journal of Immunology, 2012. 188(8): p. 3757-3765; PMID[22430735].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22513406">Balancing Antiviral Potency and Host Toxicity: Identifying a Nucleotide Inhibitor with an Optimal Kinetic Phenotype for HIV-1 Reverse Transcriptase.</a> Sohl, C.D., R. Kasiviswanathan, J. Kim, U. Pradere, R.F. Schinazi, W.C. Copeland, H. Mitsuya, M. Baba, and K.S. Anderson, Molecular Pharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22513406].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22512579">Perspectives on Developing Small Molecule Inhibitors Targeting HIV-1 Integrase.</a> Tan, J.J., C. Liu, X.H. Sun, X.J. Cong, L.M. Hu, C.X. Wang, and X.J. Liang, Mini Reviews in Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22512579].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22489874">Synthesis of (6-(13)C)Pyrimidine Nucleotides as Spin-labels for RNA Dynamics.</a> Wunderlich, C.H., R. Spitzer, T. Santner, K. Fauster, M. Tollinger, and C. Kreutz, Journal of the American Chemical Society, 2012. <b>[Epub ahead of print]</b>; PMID[22489874].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22515365">Second-site Suppressors of HIV-1 Capsid Mutations: Restoration of Intracellular Activities without Correction of Intrinsic Capsid Stability Defects.</a> Yang, R., J. Shi, I.J. Byeon, J. Ahn, J.H. Sheehan, J. Meiler, A.M. Gronenborn, and C. Aiken, Retrovirology, 2012. 9(1): p. 30; PMID[22515365].
    <br />
    <b>[PubMed]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301123400056.%20%5bWOS%5d.%20WOS_0413-042612">HIV Pol Inhibits HIV Budding and Mediates the Severe Budding Defect of Gag-Pol.</a> Gan, X. and S.J. Gould, Plos One, 2012. 7(1); ISI[000301123400056].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301719500001">Systemic Inhibition of Myeloid Dendritic Cells by Circulating HLA Class I Molecules in HIV-1 Infection.</a> Huang, J.H., M. Al-Mozaini, J. Rogich, M.F. Carrington, K. Seiss, F. Pereyra, M. Lichterfeld, and X.G. Yu, Retrovirology, 2012. 9; ISI[000301719500001].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301846100005">Short Cyclic Peptides Derived from the C-terminal Sequence of Alpha 1-antitrypsin Exhibit Significant anti-HIV-1 Activity.</a> Jia, Q.Y., X.F. Jiang, F. Yu, J.Y. Qiu, X.Y. Kang, L.F. Cai, L. Li, W.G. Shi, S.W. Liu, S.B. Jiang, and K.L. Liu, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(7): p. 2393-2395; ISI[000301846100005].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301846100032">A Novel 3,4-Dihydropyrimidin-2(1H)-one: HIV-1 Replication Inhibitors with Improved Metabolic Stability.</a> Kim, J., T. Ok, C. Park, W. So, M. Jo, Y. Kim, M. Seo, D. Lee, S. Jo, Y. Ko, I. Choi, Y. Park, J. Yoon, M.K. Ju, J. Ahn, S.J. Han, T.H. Kim, J. Cechetto, J. Namc, M. Liuzzi, P. Sommer, and Z. No, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(7): p. 2522-2526; ISI[000301846100032].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000302257100006">Identification of anti-HIV and anti-Reverse Transcriptase activity from Tetracera scandens.</a> Kwon, H.S., J.A. Park, J.H. Kim, and J.C. You, Bmb Reports, 2012. 45(3): p. 165-170; ISI[000302257100006].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301639600006">A Flexible Model of HIV-1 Latency Permitting Evaluation of Many Primary CD4 T-Cell Reservoirs.</a> Lassen, K.G., A.M. Hebbeler, D. Bhattacharyya, M.A. Lobritz, and W.C. Greene, Plos One, 2012. 7(1); ISI[000301639600006].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301550800054">Hydrophobic Core Flexibility Modulates Enzyme Activity in HIV-1 Protease.</a> Mittal, S., Y.F. Cai, M.N.L. Nalam, D.N.A. Bolon, and C.A. Schiffer, Journal of the American Chemical Society, 2012. 134(9): p. 4163-4168; ISI[000301550800054].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301459900078">DBSA Catalyzed, One-pot Three-component &quot;on Water&quot; Green Protocol for the Synthesis of 2,3-Disubstituted 4-thiazolidinones.</a> Prasad, D., A. Preetam, and M. Nath, Rsc Advances, 2012. 2(7): p. 3133-3140; ISI[000301459900078].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301914100019">Thioredoxin-1 and Protein Disulfide Isomerase Catalyze the Reduction of Similar Disulfides in HIV gp120.</a> Reiser, K., K.O. Francois, D. Schols, T. Bergman, H. Jornvall, J. Balzarini, A. Karlsson, and M. Lundberg, International Journal of Biochemistry &amp; Cell Biology, 2012. 44(3): p. 556-562; ISI[000301914100019].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301516000061">Synthesis of Fused Indazole Ring Systems and Application to Nigeglanine hydrobromide.</a> Sather, A.C., O.B. Berryman, and J. Rebek, Organic Letters, 2012. 14(6): p. 1600-1603; ISI[000301516000061].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301846100001">Optimization of 2,4-Diarylanilines as Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Sun, L.Q., B.J. Qin, L. Huang, K.D. Qian, C.H. Chen, K.H. Lee, and L. Xie, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(7): p. 2376-2379; ISI[000301846100001].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301903300012">A Facile and Efficient Iodination of 5,6-di(Arylamino)pyridine-2,3-diones.</a> Xie, W.L., Y. Zhou, C.P. Xiao, L. Xie, X.F. Tang, and R.Y. Liu, Journal of Chemical Research, 2012(2): p. 103-104; ISI[000301903300012].
    <br />
    <b>[WOS]</b>. HIV_0413-042612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
