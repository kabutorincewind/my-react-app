

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-73.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+icFXi6HRayYBwvCMEnLACH/qBV0u17XZZVHWydKRErYYxz5NH+N+1DsVNaEeOQnVt8XwhLZtBYBNwn8Ax+tB+kN7j6gmT3WSEImRgDRLVv/I4r9mSVdDdoYV/Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8DE8B243" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-73-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    6293   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Efficacy of alpha interferon therapy for lamivudine resistance in chronic hepatitis B</p>

<p>           Danalioglu, A, Kaymakoglu, S, Cakaloglu, Y, Demir, K, Karaca, C, Durakoglu, Z, Bozaci, M, Badur, S, Cevikbas, U, and Okten, A</p>

<p>           Int J Clin Pract 2004. 58(7): 659-61</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15311721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15311721&amp;dopt=abstract</a> </p><br />

<p>     2.    6294   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Current antiviral therapy for chronic hepatitis B</p>

<p>           Lim, YS and Suh, DJ</p>

<p>           J Korean Med Sci 2004. 19(4): 489-94</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308835&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308835&amp;dopt=abstract</a> </p><br />

<p>     3.    6295   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Glycodendritic structures: promising new antiviral drugs</p>

<p>           Rojo, J and Delgado, R</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308605&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308605&amp;dopt=abstract</a> </p><br />

<p>     4.    6296   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           The aetiology of SARS: Koch&#39;s postulates fulfilled</p>

<p>           Osterhaus, AD, Fouchier, RA, and Kuiken, T</p>

<p>           Philos Trans R Soc Lond B Biol Sci 2004. 359(1447): 1081-1082</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15306393&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15306393&amp;dopt=abstract</a> </p><br />

<p>     5.    6297   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Antiviral activity of Undaria pinnati fi da against herpes simplex virus</p>

<p>           Thompson, KD and Dragar, C</p>

<p>           Phytother Res  2004. 18(7): 551-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15305315&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15305315&amp;dopt=abstract</a> </p><br />

<p>     6.    6298   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           3,4&#39;,5-Trihydroxy-trans-stilbene (resveratrol) inhibits human cytomegalovirus replication and virus-induced cellular signaling</p>

<p>           Evers, DL, Wang, X, Huong, SM, Huang, DY, and Huang, ES</p>

<p>           Antiviral Res  2004. 63(2): 85-95</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302137&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15302137&amp;dopt=abstract</a> </p><br />

<p>        </p>

<p>    7.     6299   DMID-LS-73; WOS-DMID-8/18/2004</p>

<p>           <b>Sensitivity of Ns3 Serine Proteases From Hepatitis C Virus Genotypes 2 and 3 to the Inhibitor Biln 2061</b></p>

<p>           Thibeault, D, Bousquet, C, Gingras, R, Lagace, L, Maurice, R, White, PW, and Lamarre, D</p>

<p>           Journal of Virology 2004. 78(14): 7352-7359</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     8.    6300   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Benzoic acid inhibitors of influenza virus neuraminidase</p>

<p>           Luo, M</p>

<p>           Acta Crystallogr D Biol Crystallogr 95. 51(Pt 4): 504-10</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299837&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299837&amp;dopt=abstract</a> </p><br />

<p>     9.    6301   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Crimean-Congo haemorrhagic fever virus: sequence analysis of the small RNA segments from a collection of viruses world wide</p>

<p>           Hewson, R, Chamberlain, J, Mioulet, V, Lloyd, G, Jamil, B, Hasan, R, Gmyl, A, Gmyl, L, Smirnova, SE, Lukashev, A, Karganova, G, and Clegg, C</p>

<p>           Virus Research 2004. 102(2): 185-189</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   10.    6302   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Structures of four methyltetrazole-containing antiviral compounds in human rhinovirus serotype 14</p>

<p>           Giranda, VL</p>

<p>           Acta Crystallogr D Biol Crystallogr 95. 51(Pt 4): 496-503</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299836&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299836&amp;dopt=abstract</a> </p><br />

<p>   11.    6303   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Orthomyxovirus replication, transcription, and polyadenylation</p>

<p>           Neumann, G, Brownlee, GG, Fodor, E, and Kawaoka, Y</p>

<p>           Curr Top Microbiol Immunol 2004. 283: 121-43</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15298169&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15298169&amp;dopt=abstract</a> </p><br />

<p>   12.    6304   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Evidence of airborne transmission of SARS</p>

<p>           Fowler, RA, Scales, DC, and Ilan, R</p>

<p>           N Engl J Med 2004. 351(6): 609-11; author reply 609-11</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297990&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297990&amp;dopt=abstract</a> </p><br />

<p>   13.    6305   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Ebola and Marburg virus-like particles activate human myeloid dendritic cells</p>

<p>           Bosio, Catharine M, Moore, Brian D, Warfield, Kelly L, Ruthel, Gordon, Mohamadzadeh, Mansour, Aman, MJavad, and Bavari, Sina</p>

<p>           Virology 2004. 326(2): 280-287</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>        </p>

<p>  14.     6306   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Potent Inhibition of SARS-Associated Coronavirus (SCoV) Infection and Replication by Type I Interferons (IFN-alpha/beta) but Not by Type II Interferon (IFN-gamma)</p>

<p>           Zheng, B, He, ML, Wong, KL, Lum, CT, Poon, LL, Peng, Y, Guan, Y, Lin, MC, and Kung, HF</p>

<p>           J Interferon Cytokine Res 2004. 24(7): 388-90</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296649&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296649&amp;dopt=abstract</a> </p><br />

<p>   15.    6307   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Evidence of airborne transmission of SARS</p>

<p>           Tong, TR and Liang, C</p>

<p>           N Engl J Med 2004. 351(6): 609-11; author reply 609-11</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15295057&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15295057&amp;dopt=abstract</a> </p><br />

<p>   16.    6308   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           In vitro selection of RNA aptamers against the HCV NS3 helicase domain</p>

<p>           Nishikawa, F, Funaji, K, Fukuda, K, and Nishikawa, S</p>

<p>           Oligonucleotides 2004. 14(2): 114-29</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15294075&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15294075&amp;dopt=abstract</a> </p><br />

<p>   17.    6309   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral activities of novel 2&#39;, 4&#39;- or 3&#39;, 4&#39;-doubly branched carbocyclic nucleosides as potential antiviral agents</p>

<p>           Oh, CH and Hong, JH</p>

<p>           Arch Pharm (Weinheim) 2004. 337(8):  457-63</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15293266&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15293266&amp;dopt=abstract</a> </p><br />

<p>   18.    6310   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Molecular mechanisms of filovirus entry</p>

<p>           Chan, Stephen Y and Goldsmith, Mark A</p>

<p>           CONFERENCE: Ebola and Marburg Viruses 2004: 91-135</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   19.    6311   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Human macrophage C-type lectin specific for galactose and N-acetylgalactosamine promotes filovirus entry</p>

<p>           Takada, Ayato, Fujioka, Kouki, Tsuiji, Makoto, Morikawa, Akiko, Higashi, Nobuaki, Ebihara, Hideki, Kobasa, Darwyn, Feldmann, Heinz, Irimura, Tatsuro, and Kawaoka, Yoshihiro</p>

<p>           Journal of Virology 2004. 78(6): 2943-2947</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   20.    6312   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           A Highly Unusual Palindromic Transmembrane Helical Hairpin Formed by SARS Coronavirus E Protein</p>

<p>           Arbely, E, Khattari, Z, Brotons, G, Akkawi, M, Salditt, T, and Arkin, IT</p>

<p>           J Mol Biol 2004. 341(3): 769-79</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15288785&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15288785&amp;dopt=abstract</a> </p><br />

<p>   21.    6313   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Rodent models for the study of therapy against flavivirus infections</p>

<p>           Charlier, Nathalie, Leyssen, Pieter, de Clercq, Erik, and Neyts, Johan</p>

<p>           Antiviral Research 2004. 63(2): 67-77</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   22.    6314   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Modified antiviral nucleoside derivatives and formulation for treating Flaviviridae infections</b> ((F. Hoffmann-La Roche Ag, Switz.)</p>

<p>           Martin, Joseph Armstrong</p>

<p>           PATENT: WO 2004062676 A1;  ISSUE DATE: 20040729</p>

<p>           APPLICATION: 2003; PP: 24 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   23.    6315   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Bicyclic carbohydrate compounds useful in the treatment of infections caused by Flaviviridae sp., such as hepatitis C and bovine viral diarrhea viruses</b>((Kemin Pharma L.C., USA)</p>

<p>           Sas, Benedikt, Van Hemel, Johan, Vandenkerckhove, Jan, Peys, Eric, Van Der Eycken, Johan, Ruttens, Bart, Balzarini, Jan, Declercq, Erik, and Nuyts, Johan</p>

<p>           PATENT: WO 2004062575 A2;  ISSUE DATE: 20040729</p>

<p>           APPLICATION: 2003; PP: 23 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   24.    6316   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Hindered nucleoside analogs as antiflaviviridae agents</p>

<p>           Manfredini, Stefano, Angusti, Angela, Veronese, Augusto Cesare, Durini, Elisa, Vertuani, Silvia, Nalin, Federico, Solaroli, Nicola, Pricl, Sabrina, Ferrone, Marco, Mura, Massimo, Piano, Maria Assunta, Poddesu, Barbara, Cadeddu, Alessandra, La Colla, Paolo, and Loddo, Roberta</p>

<p>           Pure and Applied Chemistry 2004. 76(5): 1007-1015</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   25.    6317   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Antiviral activity of the red marine alga Ceramium rubrum</p>

<p>           Serkedjieva, J</p>

<p>           Phytother Res  2004. 18(6): 480-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15287074&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15287074&amp;dopt=abstract</a> </p><br />

<p>   26.    6318   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Phlorotannins as antiviral agents and food additives</b>((Kumamoto Prefecture, Japan and D.N.H. Chip Kenkyusho K. K.))</p>

<p>           Nagayama, Kiminori, Kimura, Takeshi, Hirayama, Izumi, Nakamura, Takashi, Iwamura, Yoshitoshi, Ezoe, Shinsuke, and Ginnaga, Akihiro</p>

<p>           PATENT: JP 2004189648 A2;  ISSUE DATE: 20040708</p>

<p>           APPLICATION: 2002-30233; PP: 15 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   27.    6319   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Treatment of Flaviviridae infection with 2&#39;-branched nucleosides and another mutation-inducing drug such as interferon</b>((Idenix (Cayman) Limited, Cayman I. and Universita Degli Studi Di Cagliari))</p>

<p>           Sommadossi, Jean-Pierre, La Colla, Paolo, Standring, David, Bichko, Vadim, and Qu, Lin</p>

<p>           PATENT: WO 2004046331 A2;  ISSUE DATE: 20040603</p>

<p>           APPLICATION: 2003; PP: 166 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   28.    6320   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Treatment of cytomegalovirus disease with valganciclovir in renal transplant recipients: a single center experience</p>

<p>           Babel, N, Gabdrakhmanova, L, Juergensen, JS, Eibl, N, Hoerstrup, J, Hammer, M, Rosenberger, C, Hoeflich, C, Frei, U, Rohde, F, Volk, HD, and Reinke, P</p>

<p>           Transplantation 2004. 78(2): 283-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280691&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280691&amp;dopt=abstract</a> </p><br />

<p>   29.    6321   DMID-LS-73; WOS-DMID-8/18/2004</p>

<p>           <b>Beta-Lactams as Versatile Synthetic Intermediates for the Preparation of Heterocycles of Biological Interest</b></p>

<p>           Alcaide, B and Almendros, P</p>

<p>           Current Medicinal Chemistry 2004. 11(14): 1921-1949</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   30.    6322   DMID-LS-73; WOS-DMID-8/18/2004</p>

<p>           <b>9-Fluorenon-4-Carboxamides: Synthesis, Conformational Analysis, Anti-Hsv-2, and Immunomodulatory Evaluation. Note Ii</b></p>

<p>           Alcaro, S, Arena, A, Di Bella, R, Neri, S, Ottana, R, Ortuso, F, Pavone, B, and Vigorita, MG</p>

<p>           Arkivoc 2004: 334-348</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   31.    6323   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Antiviral random sequence oligonucleotides lacking complementarity to target genomes and their therapeutic uses</b> ((Replicor, Inc. Can.)</p>

<p>           Vaillant, Andrew and Juteau, Jean-Marc</p>

<p>           PATENT: WO 2004024919 A1;  ISSUE DATE: 20040325</p>

<p>           APPLICATION: 2003; PP: 161 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   32.    6324   DMID-LS-73; WOS-DMID-8/18/2004</p>

<p>           <b>Uuuuunu Oligonucleotide Inhibition of Rna Synthesis in Vaccinia Virus Cores</b></p>

<p>           Mohamed, MR and Niles, EG</p>

<p>           Virology 2004. 324(2): 493-500</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   33.    6325   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Antiviral properties and cytotoxic activity of platinum(II) complexes with 1,10-phenanthrolines and acyclovir or penciclovir</p>

<p>           Margiotta, N, Bergamo, A, Sava, G, Padovano, G, De, Clercq E, and Natile, G</p>

<p>           J Inorg Biochem 2004. 98(8): 1385-90</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15271515&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15271515&amp;dopt=abstract</a> </p><br />

<p>   34.    6326   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           5th antiviral drug discovery &amp; development summit</p>

<p>           Blair, W and Perros, M</p>

<p>           Expert Opin Investig Drugs 2004. 13(8): 1065-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268642&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268642&amp;dopt=abstract</a> </p><br />

<p>  35.     6327   DMID-LS-73; PUBMED-DMID-8/18/2004</p>

<p class="memofmt1-3">           Recent developments in target identification against hepatitis C virus</p>

<p>           Brass, V, Blum, HE, and Moradpour, D</p>

<p>           Expert Opin Ther Targets 2004. 8(4): 295-307</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15268625&amp;dopt=abstract</a> </p><br />

<p>   36.    6328   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Inhibition of human herpes simplex virus type 2 by interferon g and tumor necrosis factor a is mediated by indoleamine 2,3-dioxygenase</p>

<p>           Adams, O, Besken, K, Oberdoerfer, C, MacKenzie, CR, Ruessing, D, and Daeubener, W</p>

<p>           Microbes and Infection 2004. 6(9): 806-812</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   37.    6329   DMID-LS-73; WOS-DMID-8/18/2004</p>

<p>           <b>Potential Targets for Anti-Sars Drugs in the Structural Proteins From Sars Related Coronavirus</b></p>

<p>           Wu, G and Yan, SM</p>

<p>           Peptides 2004 . 25(6): 901-908</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   38.    6330   DMID-LS-73; WOS-DMID-8/18/2004</p>

<p>           <b>Rnase P Ribozyme Inhibits Cytomegalovirus Replication by Blocking the Expression of Viral Capsid Proteins</b></p>

<p>           Kim, K, Trang, P, Umamoto, S, Hai, R, and Liu, FY</p>

<p>           Nucleic Acids Research 2004. 32(11): 3427-3434</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   39.    6331   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           An improved synthesis of 7,8-epoxy-1,3,11-cembratriene-15R(a), 16-diol, a cembranoid of marine origin with a potent cancer chemopreventive activity</p>

<p>           Fahmy, Hesham, Khalifa, Sherief I, Konoshima, Takao, and Zjawiony, Jordan K</p>

<p>           Marine Drugs 2004. 2(1): 1-7</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   40.    6332   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Diarylmethylpiperazines as prophylactic or therapeutic agents for viral myocarditis</b>((UCB, S. A. Belg.)</p>

<p>           Matsumori, Akira and Kouzan, Serge</p>

<p>           PATENT: WO 2004004728 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 26 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   41.    6333   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Anti-infective compositions for hepatitis C virus</b> ((Smithkline Beecham Corporation, USA)</p>

<p>           Fitch, Duke M </p>

<p>           PATENT: WO 2004058150 A2;  ISSUE DATE: 20040715</p>

<p>           APPLICATION: 2003; PP: 33 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  42.     6334   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Preparation of 3-(benzothiadiazin-3-yl)quinolines as HCV anti-infectives</b>((Smithkline Beecham Corporation, USA)</p>

<p>           Chai, Deping, Duffy, Kevin J, Fitch, Duke M, Shaw, Antony N, Tedesco, Rosanna, Wiggall, Kenneth J, and Zimmerman, Michael N</p>

<p>           PATENT: WO 2004052313 A2;  ISSUE DATE: 20040624</p>

<p>           APPLICATION: 2003; PP: 88 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   43.    6335   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Anti-infectives compounds and use for treating hepatitis C virus infection associated diseases</b>((Smithkline Beecham Corporation, USA)</p>

<p>           Chai, Deping, Duffy, Kevin J, Fitch, Duke M, Shaw, Antony N, Tedesco, Rosanna, Wiggall, Kenneth J, and Zimmerman, Michael N</p>

<p>           PATENT: WO 2004052312 A2;  ISSUE DATE: 20040624</p>

<p>           APPLICATION: 2003; PP: 54 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   44.    6336   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Novel inhibitors of poxvirus replication</b>((The Research Foundation of State University of New York, USA)</p>

<p>           Niles, Edward G and Mohamed, Mohamed R</p>

<p>           PATENT: WO 2004060323 A2;  ISSUE DATE: 20040722</p>

<p>           APPLICATION: 2004; PP: 65 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   45.    6337   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p class="memofmt1-3">           Advancements in the battle against severe acute respiratory syndrome</p>

<p>           Hui, David SC and Wong, Gary WK</p>

<p>           Expert Opinion on Pharmacotherapy 2004. 5(8): 1687-1693</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   46.    6338   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Peptides that bind to DNA and inhibit DNA replication, and antiviral use</b>((USA))</p>

<p>           Bullock, Peter</p>

<p>           PATENT: US 2004147006 A1;  ISSUE DATE: 20040729</p>

<p>           APPLICATION: 2003-46418; PP: 43 pp., Cont.-in-part of Appl. No. PCT/US02-14372.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   47.    6339   DMID-LS-73; SCIFINDER-DMID-8/17/2004</p>

<p><b>           Antisense oligonucleotides, ribozymes and DNAzymes targeting human papillomavirus genes E6 &amp; E7 for treatment of HPV infections associated with cervical cancer</b> ((The Penn State Research Foundation, USA)</p>

<p>           Clawson, Gary A, Pan, Wei-Hua, Christensen, Neil, and Thiboutot, Diane</p>

<p>           PATENT: WO 2004002416 A2;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 65 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
