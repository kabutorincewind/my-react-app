

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-339.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="d2aHeJfcT5NIkks7Q2jOiV1YnrrstSt0ssa+mQnEX23sn42LC1AJz7oCwZ3CXmRXAcfrSxZI5LtziON91GWlBI/z/mTQozPB2mQtC7lrvENqnkYFrgUYV/qQJ7E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="29A474DC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-339-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48352   OI-LS-339; PUBMED-OI-1/9/2006</p>

    <p class="memofmt1-2">          Anti-HIV, Anti-Poxvirus, and Anti-SARS Activity of a Nontoxic, Acidic Plant Extract from the Trifollium Species Secomet-V/anti-Vac Suggests That It Contains a Novel Broad-Spectrum Antiviral</p>

    <p>          Kotwal, GJ, Kaczmarek, JN, Leivers, S, Ghebremariam, YT, Kulkarni, AP, Bauer, G, DE, Beer C, Preiser, W, and Mohamed, AR</p>

    <p>          Ann N Y Acad Sci <b>2005</b>.  1056: 293-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387696&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387696&amp;dopt=abstract</a> </p><br />

    <p>2.     48353   OI-LS-339; PUBMED-OI-1/9/2006</p>

    <p class="memofmt1-2">          Fungal pathogens research: novel and improved molecular approaches for the discovery of antifungal drug targets</p>

    <p>          Tournu, H, Serneels, J, and Van, Dijck P</p>

    <p>          Curr Drug Targets <b>2005</b>.  6(8): 909-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16375674&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16375674&amp;dopt=abstract</a> </p><br />

    <p>3.     48354   OI-LS-339; SCIFINDER-OI-1/3/2006</p>

    <p class="memofmt1-2">          Candidacidal activity of a monoclonal antibody that binds with glycosyl moieties of proteins of Candida albicans</p>

    <p>          Kavishwar, Amol and Shukla, PK</p>

    <p>          Medical Mycology <b>2006</b>.  44(2): 159-167</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     48355   OI-LS-339; PUBMED-OI-1/9/2006</p>

    <p class="memofmt1-2">          Antiviral hepatitis and antiretroviral drug interactions</p>

    <p>          Perronne, C</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16360231&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16360231&amp;dopt=abstract</a> </p><br />

    <p>5.     48356   OI-LS-339; WOS-OI-1/1/2006</p>

    <p class="memofmt1-2">          Enhanced pathogenicity of Candida albicans pre-treated with subinhibitory concentrations of fluconazole in a mouse model of disseminated candidiasis</p>

    <p>          Navarathna, DHMLP, Hornby, JM, Hoerrmann, N, Parkhurst, AM, Duhamel, GE, and Nickerson, KW</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2005</b>.  56(6): 1156-1159, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233989900030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233989900030</a> </p><br />
    <br clear="all">

    <p>6.     48357   OI-LS-339; SCIFINDER-OI-1/3/2006</p>

    <p class="memofmt1-2">          Preparation of azaquinazolines as antiviral agents</p>

    <p>          Wunberg, Tobias, Baumeister, Judith, Jeske, Mario, Suessmeier, Frank, Zimmermann, Holger, Henninger, Kerstin, and Lang, Dieter</p>

    <p>          PATENT:  DE <b>102004022672</b>  ISSUE DATE: 20051124</p>

    <p>          APPLICATION: 2004  PP: 33 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     48358   OI-LS-339; WOS-OI-1/1/2006</p>

    <p class="memofmt1-2">          Coinfection with HIV and hepatitis C virus and immune restoration during HAART</p>

    <p>          Manfredi, R</p>

    <p>          CLINICAL INFECTIOUS DISEASES <b>2006</b>.  42(2): 298-299, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233981800023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233981800023</a> </p><br />

    <p>8.     48359   OI-LS-339; SCIFINDER-OI-1/3/2006</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Activity of Some 2-Substituted 3-Formyl-and 3-Cyano-5,6-Dichloroindole Nucleosides</p>

    <p>          Williams, John, Drach, John, and Townsend, Leroy</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1613-1626</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48360   OI-LS-339; SCIFINDER-OI-1/3/2006</p>

    <p class="memofmt1-2">          Phosphoralaninate Pronucleotides of Pyrimidine Methylenecyclopropane Analogues of Nucleosides: Synthesis and Antiviral Activity</p>

    <p>          Ambrose, Amalraj, Zemlicka, Jiri, Kern, Earl, Drach, John, Gullen, Elizabeth, and Cheng, Yung-Chi</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1763-1774</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48361   OI-LS-339; WOS-OI-1/9/2006</p>

    <p class="memofmt1-2">          Searching for more effective HCVNS3 protease inhibitors via modification of corilagin</p>

    <p>          Wang, Y, Yang, XS, Li, ZQ, Zhang, W, Chen, LR, and Xu, XJ</p>

    <p>          PROGRESS IN NATURAL SCIENCE <b>2005</b>.  15(10): 896-901, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234007400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234007400004</a> </p><br />

    <p>11.   48362   OI-LS-339; SCIFINDER-OI-1/3/2006</p>

    <p class="memofmt1-2">          Preparation of morpholinylanilino quinazoline derivatives for use as antiviral agents</p>

    <p>          Spencer, Keith, Dennison, Helena, Matthews, Neil, Barnes, Michael, and Chana, Surinder</p>

    <p>          PATENT:  WO <b>2005105761</b>  ISSUE DATE:  20051110</p>

    <p>          APPLICATION: 2005  PP: 55 pp.</p>

    <p>          ASSIGNEE:  (Arrow Therapeutics Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   48363   OI-LS-339; SCIFINDER-OI-1/3/2006</p>

    <p class="memofmt1-2">          Methods for preparing 7-(2&#39;-substituted-b-D-ribofuranosyl)-4-(NR2R3)-5-(substituted ethyn-1-yl)-pyrrolo[2,3-d]pyrimidine derivatives as antiviral agents</p>

    <p>          Roberts, Christopher D, Keicher, Jesse D, and Dyatkina, Natalia B</p>

    <p>          PATENT:  US <b>2005215510</b>  ISSUE DATE:  20050929</p>

    <p>          APPLICATION: 2004-53137  PP: 28 pp., Cont.-in-part of U.S. Ser. No. 861,311.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   48364   OI-LS-339; WOS-OI-1/9/2006</p>

    <p class="memofmt1-2">          Identification of regions involved in enzymatic stability of peptide deformylase of Mycobacterium tuberculosis</p>

    <p>          Saxena, R and Chakraborti, PK</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2005</b>.  187(23): 8216-8220, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234009200038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234009200038</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
