

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-02-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="l836f3WJymXhoyVW0U2D5WOLBGs8o6DyvBIDG/xSRUBbmfIIb91/fl60+B5ymCYPf2BZ83OKvagymsSzYjrGI4hrbfeimi+Kn/OsY/yW+Vg2WIEPzoJxef2jhyU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="17CCFC9F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: January 31 - February 13, 2014</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23997183">Pharmacological Disruption of Hepatitis C NS5A Protein Intra- and Intermolecular Conformations<i>.</i></a> Bhattacharya, D., I.H. Ansari, R. Hamatake, J. Walker, W.M. Kazmierski, and R. Striker. The Journal of General Virology, 2014. 95(Pt 2): p. 363-372; PMID[23997183].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24405705">Novel, Sulfonamide Linked Inhibitors of the Hepatitis C Virus NS3 Protease<i>.</i></a> Kirschberg, T.A., N.H. Squires, H. Yang, A.C. Corsa, Y. Tian, N. Tirunagari, X.C. Sheng, and C.U. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 969-972; PMID[24405705].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24383925">MicroRNA-130a Inhibits HCV Replication by Restoring the Innate Immune Response<i>.</i></a> Li, S., X. Duan, Y. Li, B. Liu, I. McGilvray, and L. Chen. Journal of Viral Hepatitis, 2014. 21(2): p. 121-128; PMID[24383925].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24307579">Inhibition of Hepatitis C Virus Production by Aptamers against the Core Protein<i>.</i></a> Shi, S., X. Yu, Y. Gao, B. Xue, X. Wu, X. Wang, D. Yang, and H. Zhu. Journal of Virology, 2014. 88(4): p. 1990-1999; PMID[24307579].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24277030">In Vitro Combination of Anti-cytomegalovirus Compounds Acting through Different Targets: Role of the Slope Parameter and Insights into Mechanisms of Action<i>.</i></a> Cai, H., A. Kapoor, R. He, R. Venkatadri, M. Forman, G.H. Posner, and R. Arav-Boger. Antimicrobial Agents and Chemotherapy, 2014. 58(2): p. 986-994; PMID[24277030].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24270403">The Extract of Elaeocarpus sylvestris Inhibits Human Cytomegalovirus Immediate Early Gene Expression and Replication in Vitro<i>.</i></a> To, K.P., S.C. Kang, and Y.J. Song. Molecular Medicine Reports, 2014. 9(2): p. 744-748; PMID[24270403].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24498308">Zebrafish 3-O-Sulfotransferase-4 Generated Heparan sulfate Mediates HSV-1 Entry and Spread<i>.</i></a> Antoine, T.E., A. Yakoub, E. Maus, D. Shukla, and V. Tiwari. Plos One, 2014. 9(2): p. e87302; PMID[24498308].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24315793">Inhibitory Activity and Mechanism of Two Scorpion Venom Peptides against Herpes Simplex Virus Type 1<i>.</i></a> Hong, W., T. Li, Y. Song, R. Zhang, Z. Zeng, S. Han, X. Zhang, Y. Wu, W. Li, and Z. Cao. Antiviral Research, 2014. 102: p. 1-10; PMID[24315793].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0131-021314.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329445700440">Preclinical Pharmacokinetics and in Vitro Metabolism of BMS-605339, a Novel HCV NS3 Protease Inhibitor<i>.</i></a> Jenkins, S., P. Scola, F. McPhee, J. Knipe, C. Gesenberg, V. Arora, M. Sinz, and K. Santone. Drug Metabolism Reviews, 2014. 45: p. 219-220; ISI[000329445700440].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0131-013014.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329714300001">Triterpenoid Saponins Isolated from Platycodon grandiflorum Inhibit Hepatitis C Virus Replication<i>.</i></a> Kim, J.W., S.J. Park, J.H. Lim, J.W. Yang, J.C. Shin, S.W. Lee, J.W. Suh, and S.B. Hwang. Evidence-Based Complementary and Alternative Medicine, 2013. 560417: p. 11; ISI[000329714300001].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0131-013014.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329323900010">A Screen for Genetic Suppressor Elements of Hepatitis C Virus Identifies a Supercharged Protein Inhibitor of Viral Replication<i>.</i></a> Simeon, R.L. and Z.L. Chen. Plos One, 2013. 8(12): p. e84022; ISI[000329323900010].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0131-013014.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
