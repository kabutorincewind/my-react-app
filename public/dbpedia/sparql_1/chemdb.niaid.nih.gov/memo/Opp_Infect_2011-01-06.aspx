

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-01-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="197RA8jJ2voo3a65dxUoAkFw8OszwZFPBQsNZK0GoU4l078Fa8ecueO13EQI34mvAM3++xi+WkrIdisVvVSSKXGgMBv2M3aXvNLPJnRC4Pgce3LPIOWVLUHZ2DE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E0E3DF1E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">December 22 - January 6, 2011</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21181734">Synthesis and Antimicrobial Activity of Some New N-Substituted Quinoline Derivatives of 1H-Pyrazole.</a> Thumar, N.J. and M.P. Patel. Archiv Der Pharmazie, 2010. <b>[Epub ahead of print]</b>; PMID[21181734].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21134759">Benzothieno[3,2-b]quinolinium and 3-(phenylthio)quinolinium compounds: Synthesis and evaluation against opportunistic fungal pathogens.</a>Boateng, C.A., S.V. Eyunni, X.Y. Zhu, J.R. Etukala, B.A. Bricker, M.K. Ashfaq, M.R. Jacob, S.I. Khan, L.A. Walker, and S.Y. Ablordeppey. Bioorganic &amp;  Medicinal Chemistry, 2011. 19(1): p. 458-70; PMID[21134759].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21134751">3-[Benzimidazo- and 3-[benzothiadiazoleimidazo-(1,2-c)quinazolin-5-yl]-2H-chromene-2-ones as potent antimicrobial agents.</a>Kuarm, B.S., Y.T. Reddy, J.V. Madhav, P.A. Crooks, and B. Rajitha. Bioorganic and Medicinal Chemistry Letters, 2011. 21(1): p. 524-7; PMID[21134751].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21095127">Microwave assisted one pot synthesis of some novel 2,5-disubstituted 1,3,4-oxadiazoles as antifungal agents.</a> Sangshetti, J.N., A.R. Chabukswar, and D.B. Shinde. Bioorganic and Medicinal Chemistry Letters, 2011. 21(1): p. 444-8; PMID[21095127].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21093119">Penetratin analogues acting as antifungal agents.</a> Garibotto, F.M., A.D. Garro, A.M. Rodriguez, M. Raimondi, S.A. Zacchino, A. Perczel, C. Somlai, B. Penke, and R.D. Enriz. European Journal of Medicinal Chemistry, 2011. 46(1): p. 370-7; PMID[21093119].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21084135">Synthesis and biological evaluation of novel 2,4-disubstituted-1,3-thiazoles as anti-Candida spp. agents.</a> Chimenti, F., B. Bizzarri, A. Bolasco, D. Secci, P. Chimenti, A. Granese, S. Carradori, M. D&#39;Ascenzio, D. Lilli, and D. Rivanera. European Journal of Medicinal Chemistry, 2011. 46(1): p. 378-82; PMID[21084135].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20738748">Antifungal activity of lawsone methyl ether in comparison with chlorhexidine.</a>Sritrairat, N., N. Nukul, P. Inthasame, A. Sansuk, J. Prasirt, T. Leewatthanakorn, U. Piamsawad, A. Dejrudee, P. Panichayupakaranant, K. Pangsomboon, N. Chanowanna, J. Hintao, R. Teanpaisan, W. Chaethong, P. Yongstar, N. Pruphetkaew, V. Chongsuvivatwong, and W. Nittayananta. Journal of Oral Pathology and Medicine, 2011. 40(1): p. 90-96; PMID[20738748].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19703269">Resveratrol and its antifungal activity against Candida species.</a> Weber, K., B. Schulz, and M. Ruhnke. Mycoses, 2011. 54(1): p. 30-3; PMID[19703269].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p class="memofmt2-3"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21199931">Susceptibility of clinical Mycobacterium tuberculosis isolates to a potentially less toxic derivate of linezolid; PNU-100480.</a> Alffenaar, J.W., T. van der Laan, S. Simons, T.S. van der Werf, P.J. van de Kasteele, H. de Neeling, and D. van Soolingen. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21199931].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21186194">Chemical modification of capuramycins to enhance antibacterial activity.</a>Bogatcheva, E., T. Dubuisson, M. Protopopova, L. Einck, C.A. Nacy, and V.M. Reddy. The Journal of Antimicrobial Chemotherapy, 2010. <b>[Epub ahead of print]</b>; PMID[21186194].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21145625">Synthesis and anti-mycobacterial activities of triazoloquinolones.</a> Carta, A., M. Palomba, I. Briguglio, P. Corona, S. Piras, D. Jabes, P. Guglierame, P. Molicotti, and S. Zanetti European Journal of Medicinal Chemistry, 2011. 46(1): p. 320-6; PMID[21145625].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21146257">Synthesis and in vitro antimycobacterial activity of 8-OCH(3) ciprofloxacin methylene and ethylene isatin derivatives.</a> Feng, L.S., M.L. Liu, S. Zhang, Y. Chai, B. Wang, Y.B. Zhang, K. Lv, Y. Guan, H.Y. Guo, and C.L. Xiao. European Journal of Medicinal Chemistry, 2011. 46(1): p. 341-8; PMID[21146257].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21168771">Synthetic UDP-furanoses as potent inhibitors of mycobacterial galactan biogenesis.</a>Peltier, P., M. Belanova, P. Dianiskova, R. Zhou, R.B. Zheng, J.A. Pearcey, M. Joe, P.J. Brennan, C. Nugier-Chauvin, V. Ferrieres, T.L. Lowary, R. Daniellou, and K. Mikusova. Chemistry and Biology. Journal of Chemical Information and Modeling, 2010. 17(12): p. 1356-66; PMID[21168771].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21167722">Synthesis of sugar-amino acid-nucleosides as potential glycosyltransferase inhibitors.</a>Vembaiyan, K., J.A. Pearcey, M. Bhasin, T.L. Lowary, and W. Zou. Bioorganic &amp;  Medicinal Chemistry, 2011. 19(1): p. 58-66; PMID[21167722].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21106378">Design, synthesis and antimycobacterial activities of 1-methyl-2-alkenyl-4(1H)-quinolones.</a>Wube, A.A., A. Hufner, C. Thomaschitz, M. Blunder, M. Kollroser, R. Bauer, and F. Bucar. Bioorganic &amp;  Medicinal Chemistry, 2011. 19(1): p. 567-79; PMID[21106378].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1221-010511.</p> 

    <p class="memofmt2-4"> </p>

    <p class="memofmt2-4"> </p>

    <p class="memofmt2-4">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284980000007">Synthesis and biological evaluation of substituted alpha- and beta-2,3-dihydrofuran naphthoquinones as potent anticandidal agents.</a>  Freire, C.P.V., S.B. Ferreira, N.S.M. de Oliveira, A.B.J. Matsuura, I.L. Gama, F.D. da Silva, M. de Souza, E.S. Lima, and V.F. Ferreira. Medchemcomm, 2010. 1(3): p. 229-232; ISI[000284980000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1221-010511.     </p>

    <p> </p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
