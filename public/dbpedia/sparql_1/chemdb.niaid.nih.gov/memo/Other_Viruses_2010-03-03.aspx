

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-03-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ar3MJOGeM53HSI81D5/N1jOpoEpjHGwQ9tRaGUC7mBvlf5hp+z/uAGpBCW0YlJL1HlEH+WBd9v9W9sdfv4VIztEa3vTAt0ZKPDePCCuawKP7yd/1u2JcrmMc0kE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AC2BF62B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">February 17 - March 3, 2010</p>

    <p> </p>

    <h2 class="memofmt2-2">Hepatitis C Virus</h2> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20194503?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=22">Mechanisms of activity and inhibition of the hepatitis C virus RNA-dependent RNA-polymerase.</a></p>

    <p class="NoSpacing">Reich S, Golbik RP, Geissler R, Lilie H, Behrens SE.</p>

    <p class="NoSpacing">J Biol Chem. 2010 Mar 1. [Epub ahead of print]PMID: 20194503 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20163176?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=146">Discovery of Vaniprevir (MK-7009), a Macrocyclic Hepatitis C Virus NS3/4a Protease Inhibitor.</a></p>

    <p class="NoSpacing">McCauley JA, McIntyre CJ, Rudd MT, Nguyen KT, Romano JJ, Butcher JW, Gilbert KF, Bush KJ, Holloway MK, Swestock J, Wan BL, Carroll SS, Dimuzio JM, Graham DJ, Ludmerer SW, Mao SS, Stahlhut MW, Fandozzi CM, Trainor N, Olsen DB, Vacca JP, Liverton NJ.</p>

    <p class="NoSpacing">J Med Chem. 2010 Feb 17. [Epub ahead of print]PMID: 20163176 [PubMed - as supplied by publisher]</p>
    <p />

    <h2 class="memofmt2-2">Cytomegalovirus</h2> 

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20194695?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">The 6-Aminoquinolone WC5 Inhibits Human Cytomegalovirus Replication at an Early Stage by Interfering with the Transactivating Activity of Viral Immediate-Early 2 Protein.</a></p>

    <p class="NoSpacing">Loregian A, Mercorelli B, Muratore G, Sinigalia E, Pagni S, Massari S, Gribaudo G, Gatto B, Palumbo M, Tabarrini O, Cecchetti V, Palù G.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2010 Mar 1. [Epub ahead of print]PMID: 20194695 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20194528?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">Novel anti-cytomegalovirus activity of immunosuppressant mizoribine and its synergism with ganciclovir.</a></p>

    <p class="NoSpacing">Kuramoto T, Daikoku T, Yoshida Y, Takemoto M, Oshima K, Eizuru Y, Kanda Y, Miyawaki T, Shiraki K.</p>

    <p class="NoSpacing">J Pharmacol Exp Ther. 2010 Mar 1. [Epub ahead of print]PMID: 20194528 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20167488?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=36">The design and development of 2-aryl-2-hydroxy ethylamine substituted 1H,7H-pyrido[1,2,3-de]quinoxaline-6-carboxamides as inhibitors of human cytomegalovirus polymerase.</a></p>

    <p class="NoSpacing">Tanis SP, Strohbach JW, Parker TT, Moon MW, Thaisrivongs S, Perrault WR, Hopkins TA, Knechtel ML, Oien NL, Wieber JL, Stephanski KJ, Wathen MW.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2010 Jan 25. [Epub ahead of print]PMID: 20167488 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274300200011">Curcumin inhibits hepatitis C virus replication via suppressing the Akt-SREBP-1 pathway.</a></p>

    <p class="NoSpacing"> Kim, KyeongJin, Kim, Kook Hwan, Kim, Hye Young, Cho, Hyun Kook, Sakamoto, Naoya, Cheong, JaeHun.  FEBS LETTERS. 584(4):2010. P.707-12.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
