

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-03-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8nQwo46DeQcJIn0UEM694VMyQaaRI7Ny23A7BfL0eX7R/QMe5aGFoESdi4Sa16UImUq7dNje3+Greb9Qu1ZfckzOkPIS0AYwvDZ238rmVIIr9Sx1NNfDa2ko6FE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F563E714" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">February 17 - March 3, 2010</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p> </p>

    <p class="plaintext">1.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20189786?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=21">In vitro synergistic efficacy of combination of amphotericin B with Myrtus communis essential oil against clinical isolates of Candida albicans.</a> Mahboubi, M. and F. Ghazian Bidgoli. Phytomedicine; PMID[20189786].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">2.<a href="http://www.ncbi.nlm.nih.gov/pubmed/19686813?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=177">A novel antifungal anthraquinone from seeds of Aegle marmelos Correa (family Rutaceae).</a> Mishra, B.B., N. Kishore, V.K. Tiwari, D.D. Singh, and V. Tripathi. Fitoterapia. 81(2): p. 104-7; PMID[19686813].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20138517?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=80">Stereospecific synthesis of oximes and oxime ethers of 3-azabicycles: A SAR study towards antimicrobial agents.</a> Parthiban, P., P. Rathika, V. Ramkumar, S.M. Son, and Y.T. Jeong. Bioorg Med Chem Lett. 20(5): p. 1642-1647; PMID[20138517].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">4.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20053480?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=103">Synthesis and antimicrobial activities of novel 1,5-diaryl pyrazoles.</a> Ragavan, R.V., V. Vijayakumar, and N.S. Kumari. Eur J Med Chem. 45(3): p. 1173-80; PMID[20053480].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">5.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20005020?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=130">Antimicrobial study of newly synthesized 6-substituted indolo[1,2-c]quinazolines.</a> Rohini, R., P. Muralidhar Reddy, K. Shanker, A. Hu, and V. Ravinder. Eur J Med Chem. 45(3): p. 1200-5; PMID[20005020].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">6.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20034706?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=114">New 14-membered octaazamacrocyclic complexes: synthesis, spectral, antibacterial and antifungal studies.</a> Singh, D.P., K. Kumar, and C. Sharma. Eur J Med Chem. 45(3): p. 1230-6; PMID[20034706].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">7.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20184325?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=29">Identification of Inhibitors of Drug-Resistant Candida albicans Strains from a Library of Bicyclic Peptidomimetic Compounds.</a> Trabocchi, A., C. Mannino, F. Machetti, F. De Bernardis, S. Arancia, R. Cauda, A. Cassone, and A. Guarna. J Med Chem; PMID[20184325].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">8.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20167587?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=56">Polymyxin B, in combination with fluconazole, exerts a potent fungicidal effect.</a> Zhai, B., H. Zhou, L. Yang, J. Zhang, K. Jung, C.Z. Giam, X. Xiang, and X. Lin. J Antimicrob Chemother; PMID[20167587].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20138519?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=180">Novel molecular hybrids of cinnamic acids and guanylhydrazones as potential antitubercular agents.</a> Bairwa, R., M. Kakwani, N.R. Tawari, J. Lalchandani, M.K. Ray, M.G. Rajan, and M.S. Degani. Bioorg Med Chem Lett. 20(5): p. 1623-1625; PMID[20138519].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20034708?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=268">New 1,3-oxazolo[4,5-c]quinoline derivatives: synthesis and evaluation of antibacterial and antituberculosis properties.</a> Eswaran, S., A.V. Adhikari, and R. Ajay Kumar. Eur J Med Chem. 45(3): p. 957-66; PMID[20034708].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20186169?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=64">Calpinactam, a new anti-mycobacterial agent, produced by Mortierella alpina FKI-4905.</a> Koyama, N., S. Kojima, K. Nonaka, R. Masuma, M. Matsumoto, S. Omura, and H. Tomoda. J Antibiot (Tokyo); PMID[20186169].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20099253?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=214">Recent advances toward the inhibition of mAG and LAM synthesis in Mycobacterium tuberculosis.</a> Umesiri, F.E., A.K. Sanki, J. Boucau, D.R. Ronning, and S.J. Sucheck. Med Res Rev. 30(2): p. 290-326; PMID[20099253].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20167798?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=142">Targeting mycobacterium protein tyrosine phosphatase B for antituberculosis agents.</a> Zhou, B., Y. He, X. Zhang, J. Xu, Y. Luo, Y. Wang, S.G. Franzblau, Z. Yang, R.J. Chan, Y. Liu, J. Zheng, and Z.Y. Zhang. Proc Natl Acad Sci U S A; PMID[20167798].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0217-030310.</p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p class="plaintext">14. <a href="http://apps.isiknowledge.com/InboundService.do?Func=Frame&amp;product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000274320900011&amp;SID=3AbNalCElA79Pbb%40oeI&amp;Init=Yes&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Multiple effects of amprenavir against Candida albicans.</a> Braga-Silva, L.A., S.S.V. Mogami, R.S. Valle, I.D. Silva-Neto, and A.L.S. Santos. Fems Yeast Research. 10(2): p. 221-224; ISI[000274320900011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000274203000045&amp;SID=3AbNalCElA79Pbb%40oeI&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis and in vitro microbiological evaluation of novel 4-aryl-5-isopropoxycarbonyl-6-methyl-3,4-dihydropyrimidinones.</a> Chitra, S., D. Devanathan, and K. Pandiarajan. European Journal of Medicinal Chemistry. 45(1): p. 367-371; ISI[000274203000045].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000274153600009&amp;SID=3AbNalCElA79Pbb%40oeI&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Salicylanilide carbamates: Antitubercular agents active against multidrug-resistant Mycobacterium tuberculosis strains.</a> Ferriz, J.M., K. Vavrova, F. Kunc, A. Imramovsky, J. Stolarikova, E. Vavrikova, and J. Vinsova. Bioorganic &amp; Medicinal Chemistry. 18(3): p. 1054-1061; ISI[000274153600009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000274270900037&amp;SID=3AbNalCElA79Pbb%40oeI&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Catalysis and Inhibition of Mycobacterium tuberculosis Methionine Aminopeptidase.</a> Lu, J.P., S.C. Chai, and Q.Z. Ye. Journal of Medicinal Chemistry. 53(3): p. 1329-1337; ISI[000274270900037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0217-030310.</p>

    <p> </p>

    <p class="plaintext">18.<a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000274203000016&amp;SID=3AbNalCElA79Pbb%40oeI&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Discovery of novel antitubercular 2,10-dihydro-4aH-chromeno[3,2-c]pyridin-3-yl derivatives.</a> Sriram, D., P. Yogeeswari, M. Dinakaran, D. Banerjee, P. Bhat, and S. Gadhwal. European Journal of Medicinal Chemistry. 45(1): p. 120-123; ISI[000274203000016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0217-030310.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
