

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-04-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="VGXQlKE89w1ShqffQDqzkFzGIa0RhXPNCC4jRJzPFqIUhPBR6QRp0lBarVioBLqzGQj4obStH9Oecb7FRsSGMFdgM115k1WohNFFrkPDdBIgVtHM131igrk6lM8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2BF0240C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">April 15 - April 28, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21176865">Evaluation of three novel azasterols against Toxoplasma gondii.</a> Martins-Duarte, E.S., L. Lemgruber, S.O. Lorente, L. Gros, F. Magaraci, I.H. Gilbert, W. de Souza, and R.C. Vommaro. Veterinary Parasitology. 2011. 177(1-2): p. 157-161; PMID[21176865].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p> 

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21402476">Carbonic anhydrase inhibitors. Inhibition of the beta-class enzymes from the fungal pathogens Candida albicans and Cryptococcus neoformans with branched aliphatic/aromatic carboxylates and their derivatives.</a> Carta, F., A. Innocenti, R.A. Hall, F.A. Muhlschlegel, A. Scozzafava, and C.T. Supuran. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(8): p. 2521-2526; PMID[21402476].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21507482">The targeted antibacterial and antifungal properties of magnetic nanocomposite of iron oxide and silver nanoparticles.</a> Prucek, R., J. Tucek, M. Kilianova, A. Panacek, L. Kvitek, J. Filip, M. Kolar, K. Tomankova, and R. Zboril. Biomaterials, 2011. <b>[Epub ahead of print]</b>; PMID[21507482].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">4.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21518839">Effect of Fluconazole, Amphotericin B and Caspofungin against Candida albicans Biofilms under Conditions of Flow and on Biofilm Dispersion.</a> Uppuluri, P., A. Srinivasan, A. Ramasubramanian, and J.L. Lopez-Ribot. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21518839].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>  

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p> 

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21517741">New Patentable Use of an Old Neuroleptic Compound Thioridazine to Combat Tuberculosis: A Gene Regulation Perspective.</a> Dutta, N.K., K. Mazumdar, S.G. Dastidar, P.C. Karakousis, and L. Amaral. Recent Patents on Antiinfective Drug Discovery. 2011. <b>[Epub ahead of print]</b>; PMID[21517741].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">6.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21392992">Susceptibility and mode of binding of the Mycobacterium tuberculosis cysteinyl transferase mycothiol ligase to tRNA synthetase inhibitors.</a> Gutierrez-Lugo, M.T. and C.A. Bewley. Bioorganic &amp; Medicinal Chemistry Letters. 2011. 21(8): p. 2480-2483; PMID[21392992].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">7.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21517742">Targeting the Human Macrophage with Combinations of Drugs and Inhibitors of Ca(2+) and K(+) Transport to Enhance the Killing of Intracellular Multi-Drug Resistant M. tuberculosis (MDR-TB) - a Novel, Patentable Approach to Limit the Emergence of XDR-TB.</a> Martins, M. Recent Patents on Antiinfective Drug Discovery. 2011. <b>[Epub ahead of print]</b>; PMID[21517742].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21429744">Facile synthesis and stereochemical investigation of Mannich base derivatives: Evaluation of antioxidant property and antituberculostic potency.</a> Parthiban, P., V. Subalakshmi, K. Balasubramanian, M.N. Islam, J.S. Choi, and Y.T. Jeong. Bioorganic &amp; Medicinal Chemistry Letters. 2011. 21(8): p. 2287-2296; PMID[21429744].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21439822">Inhibition of the M. tuberculosis 3beta-hydroxysteroid dehydrogenase by azasteroids.</a> Thomas, S.T., X. Yang, and N.S. Sampson. Bioorganic &amp; Medicinal Chemistry Letters. 2011. 21(8): p. 2216-2219; PMID[21439822].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">10.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21396814">Synthesis and biological evaluation of 4-styrylcoumarin derivatives as inhibitors of TNF-alpha and IL-6 with anti-tubercular activity.</a> Upadhyay, K., A. Bavishi, S. Thakrar, A. Radadiya, H. Vala, S. Parekh, D. Bhavsar, M. Savant, M. Parmar, P. Adlakha, and A. Shah. Bioorganic &amp; Medicinal Chemistry Letters. 2011. 21(8): p. 2547-2549; PMID[21396814].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">11.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21517740">The Broad-spectrum Antimycobacterial Activities of Phenothiazines, InVitro: Somewhere in All of this there May be Patentable Potentials.</a> van Ingen, J., Recent Patents on Antiinfective Drug Discovery, 2011. <b>[Epub ahead of print]</b>; PMID[21517740].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21498685">Organic Synthesis Toward Small-Molecule Probes and Drugs Special Feature: Discovery of new antimalarial chemotypes through chemical methodology and library development.</a> Brown, L.E., K. Chih-Chien Cheng, W.G. Wei, P. Yuan, P. Dai, R. Trilles, F. Ni, J. Yuan, R. Macarthur, R. Guha, R.L. Johnson, X.Z. Su, M.M. Dominguez, J.K. Snyder, A.B. Beeler, S.E. Schaus, J. Inglese, and J.A. Porco, Jr. Proceedings of the National Academy of Sciences of the United States of America, 2011. 108(17): p. 6775-6780; PMID[21498685].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21348447">Didemnidines A and B, Indole Spermidine Alkaloids from the New Zealand Ascidian Didemnum sp.</a> Finlayson, R., A.N. Pearce, M.J. Page, M. Kaiser, M.L. Bourguet-Kondracki, J.L. Harper, V.L. Webb, and B.R. Copp. Journal of Natural Products, 2011. 74(4): p. 888-892; PMID[21348447].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21341709">Cytotoxic Pentacyclic and Tetracyclic Aromatic Sesquiterpenes from Phomopsis archeri.</a>  Hemtasin, C., S. Kanokmedhakul, K. Kanokmedhakul, C. Hahnvajanawong, K. Soytong, S. Prabpai, and P. Kongsaeree. Journal of Natural Products, 2011. 74(4): p. 609-613; PMID[21341709].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21513544">Larvicidal, Antimicrobial and Brine shrimp Activities of Extracts from Cissampelos mucronata and Tephrosia villosa from Coast region, Tanzania.</a> Nondo, R.S., Z.H. Mbwambo, A.W. Kidukuli, E.M. Innocent, M.J. Mihale, P. Erasto, and M.J. Moshi. Bmc Complementary and Alternative Medicine, 2011. 11(1): p. 33; PMID[21513544].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21518844">Use of the NP-40 Detergent Mediated Assay in the Discovery of Inhibitors of {beta}-hematin Crystallization.</a> Sandlin, R.D., M.D. Carter, P.J. Lee, J.M. Auschwitz, S.E. Leed, J.D. Johnson, and D.W. Wright. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21518844]. [<b>Pubmed</b>]. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21503279">Phenolic compounds from Achillea millefolium L. and their bioactivity.</a>  Vitalini, S., G. Beretta, M. Iriti, S. Orsenigo, N. Basilico, S. Dall&#39;acqua, M. Iorizzi, and G. Fico. Acta Biochimica Polonica, 2011.<b>[Epub ahead of print]</b>; PMID[21503279].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21487780">In vitro antiplasmodial activity and cytotoxicity of crude extracts and compounds from the stem bark of Kigelia africana (Lam.) Benth (Bignoniaceae).</a> Zofou, D., A.B. Kengne, M. Tene, M.N. Ngemenya, P. Tane, and V.P. Titanji. Parasitology Research, 2011. <b>[Epub ahead of print]</b>; PMID[21487780].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0415-042811.</p> 

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph"> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289152900009">Antimicrobial activity and chemical composition of essential oil of Pelargonium odoratissimum.</a> Andrade, M.A., M.G. Cardoso, L.R. Batista, J.M. Freire, and D.L. Nelson. Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2011. 21(1): p. 47-52; ISI[000289152900009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289001900006">Synthesis and biological activities of new Mannich bases of chlorokojic acid derivatives.</a> Aytemir, M.D. and B. Ozcelik. Medicinal Chemistry Research, 2011. 20(4): p. 443-452; ISI[000289001900006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289001900002">Synthesis of new series of pyrazolo 4,3-d pyrimidin-7-ones and pyrido 2,3-d pyrimidin-4-ones for their bacterial and cyclin-dependent kinases (CDKs) inhibitory activities.</a> Geffken, D., R. Soliman, F.S.G. Soliman, M.M. Abdel-Khalek, and D.A.E. Issa. Medicinal Chemistry Research, 2011. 20(4): p. 408-420; ISI[000289001900002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288853400040">Fungicidal Properties of the Essential Oil of Hesperozygis marifolia on Aspergillus flavus Link</a>. Gonzalez-Chavez, M.M., N.C. Cardenas-Ortega, C.A. Mendez-Ramos, and S. Perez-Gutierrez. Molecules, 2011. 16(3): p. 2501-2506; ISI[000288853400040].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288375400009">Essential Metabolites of Mycobacterium tuberculosis and Their Mimics.</a> Lamichhane, G., J.S. Freundlich, S. Ekins, N. Wickramaratne, S.T. Nolan, and W.R. Bishai Mbio, 2011. 2(1); ISI[000288375400009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289110000026">Evaluation Antimicrobial and Antiadhesive Properties of the Biosurfactant Lunasan Produced by Candida sphaerica UCP 0995.</a> Luna, J.M., R.D. Rufino, L.A. Sarubbo, L.R.M. Rodrigues, J.A.C. Teixeira, and G.M. de Campos-Takaki. Current Microbiology, 2011. 62(5): p. 1527-1534; ISI[000289110000026].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289072300023">Microwave-assisted synthesis and characterization of novel symmetrical substituted 19-membered tetrathiadiaza metal-free and metallophthalocyanines and investigation of their biological activities.</a>  Nas, A., E.C. Kaya, H. Kantekin, A. Sokmen, and V. Cakir. Journal of Organometallic Chemistry, 2011. 696(8): p. 1659-1663; ISI[000289072300023].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289274300003">New Antifungal Xanthones from the Seeds of Rhus coriaria L.</a> Singh, O., M. Ali, and N. Akhtar. Zeitschrift fur naturforschung section C-A Journal of Biosciences, 2011. 66(1-2): p. 17-23; ISI[000289274300003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289274300003">Synthesis and antimycobacterial activity of prodrugs of indeno 2,1-c quinoline derivatives.</a> Upadhayaya, R.S., P.D. Shinde, S.A. Kadam, A.N. Bawane, A.Y. Sayyed, R.A. Kardile, P.N. Gitay, S.V. Lahore, S.S. Dixit, A. Foldesi, and J. Chattopadhyaya. European Journal of Medicinal Chemistry, 2011. 46(4): p. 1306-1324; ISI[000289048400034].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000288979400011">Mefloquine derivatives: Crystal structures and anti-tubercular activities of diphenyl ((R*,S*)-2,8-bis(trifluoromethyl)quinolin-4-yl)-piperidin-2-yl- methanolato-O,N boron and (+/-)-erythro-mefloquinium tetraphenylborate solvates.</a>  Wardell, J.L., M.V.N. de Souza, S. Wardell, and M.C.S. Lourenco. Journal of Molecular Structure, 2011. 990(1-3): p. 67-74; ISI[000288979400011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0415-042811.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
