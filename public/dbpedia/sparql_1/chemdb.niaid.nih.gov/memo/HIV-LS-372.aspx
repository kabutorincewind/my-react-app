

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-372.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="HSGcygBCa/iPkw4Z06s5Av08jz+vwvXtTvCMq5MP/EX2K+qABledGhbtT7k1Ykx8vU0JDtVUtoi/b+VXTsQupIximu8LVO6dLO+ZIQq1xO+IT6nY5E6AptwyNXI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CF7BFF49" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-372-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60648   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Caffeoylglycolic and caffeoylamino acid derivatives, halfmers of l-chicoric acid, as new HIV-1 integrase inhibitors</p>

    <p>          Lee, SU, Shin, CG, Lee, CK, and Lee, YS</p>

    <p>          Eur J Med Chem  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434650&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17434650&amp;dopt=abstract</a> </p><br />

    <p>2.     60649   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Mutations in Human Immunodeficiency Virus Type-1 RNase H Primer Grip Enhance 3&#39;-Azido-3&#39;-Deoxythymidine Resistance</p>

    <p>          Delviks-Frankenberry, KA, Nikolenko, GN, Barr, R, and Pathak, VK</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17428874&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17428874&amp;dopt=abstract</a> </p><br />

    <p>3.     60650   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Cytidine deaminases APOBEC3G and APOBEC3F interact with HIV-1 integrase and inhibit proviral DNA formation</p>

    <p>          Luo, K, Wang, T, Liu, B, Tian, C, Xiao, Z, Kappes, J, and Yu, XF</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17428847&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17428847&amp;dopt=abstract</a> </p><br />

    <p>4.     60651   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Dihydroxypyrimidine-4-carboxamides as Novel Potent and Selective HIV Integrase Inhibitors</p>

    <p>          Pace, P, Francesco, ME, Gardelli, C, Harper, S, Muraglia, E, Nizi, E, Orvieto, F, Petrocchi, A, Poma, M, Rowley, M, Scarpelli, R, Laufer, R, Paz, OG, Monteagudo, E, Bonelli, F, Hazuda, D, Stillmock, KA, and Summa, V</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17428043&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17428043&amp;dopt=abstract</a> </p><br />

    <p>5.     60652   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Tetraphenylporphyrin-cobalt(III) Bis(1,2-dicarbollide) Conjugates: From the Solution Characteristics to Inhibition of HIV Protease</p>

    <p>          Kubat, P, Lang, K, Cigler, P, Kozisek, M, Matejicek, P, Janda, P, Zelinger, Z, Prochazka, K, and Kral, V</p>

    <p>          J Phys Chem B <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425351&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425351&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60653   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of trivalent CD4-mimetic miniproteins</p>

    <p>          Li, H, Guan, Y, Szczepanska, A, Moreno-Vargas, AJ, Carmona, AT, Robina, I, Lewis, GK, and Wang, LX</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17412600&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17412600&amp;dopt=abstract</a> </p><br />

    <p>7.     60654   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Synergistic inhibition of HIV-1 infection by combinations of soluble polyanions with other potential microbicides</p>

    <p>          Gantlett, KE, Weber, JN, and Sattentau, QJ</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17408760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17408760&amp;dopt=abstract</a> </p><br />

    <p>8.     60655   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Syntheses and anti-HIV activities of (+/-)-norcarbovir and (+/-)-norabacavir</p>

    <p>          Huang, W, Miller, MJ, De, Clercq E, and Balzarini, J</p>

    <p>          Org Biomol Chem <b>2007</b>.  5(8): 1164-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17406712&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17406712&amp;dopt=abstract</a> </p><br />

    <p>9.     60656   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Anti-retroviral activity of GMP-grade stampidine against genotypically and phenotypically nucleoside reverse transcriptase inhibitor resistant recombinant human immunodeficiency virus. An in vitro study</p>

    <p>          Uckun, FM, DuMez, D, Qazi, S, Tibbles, H, and Venkatachalam, TK</p>

    <p>          Arzneimittelforschung <b>2007</b>.  57(2): 112-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17396622&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17396622&amp;dopt=abstract</a> </p><br />

    <p>10.   60657   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Two New Compounds and Anti-HIV Active Constituents from Illicium verum</p>

    <p>          Song, WY, Ma, YB, Bai, X, Zhang, XM, Gu, Q, Zheng, YT, Zhou, J, and Chen, JJ</p>

    <p>          Planta Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17394104&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17394104&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60658   HIV-LS-372; PUBMED-HIV-4/18/2007</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-HIV activities of didanosine prodrugs</p>

    <p>          Sriram, D, Yogeeswari, P, Babu, NR, and Kurre, PN</p>

    <p>          J Enzyme Inhib Med Chem <b>2007</b>.  22(1): 51-55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17373547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17373547&amp;dopt=abstract</a> </p><br />

    <p>12.   60659   HIV-LS-372; WOS-HIV-4/8/2007</p>

    <p class="memofmt1-2">          Langerin is a natural barrier to HIV-1 transmission by Langerhans cells</p>

    <p>          de Witte, L, Nabatov, A, Pion, M, Fluitsma, D, de Jong, MAWP, de Gruijl, T, Piguet, V, van Kooyk, Y, and Geijtenbeek, TBH</p>

    <p>          NATURE MEDICINE <b>2007</b>.  13(3): 367-371, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244715700051">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244715700051</a> </p><br />

    <p>13.   60660   HIV-LS-372; WOS-HIV-4/8/2007</p>

    <p class="memofmt1-2">          Effect of the HIV protease inhibitor TMC114, coadministered with low-dose ritonavir, on the pharmacokinetics of digoxin in healthy volunteers</p>

    <p>          Sekar, VJ, El Malt, M, De Paepe, E, Mack, R, De Pauw, M, Vangeneugden, T, Lefebvre, E, and Hoetelmans, R</p>

    <p>          CLINICAL PHARMACOLOGY &amp; THERAPEUTICS <b>2007</b>.  81: S82-S83, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244782900242">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244782900242</a> </p><br />

    <p>14.   60661   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          GS-8374, a Novel Phosphonate HIV Protease Inhibitor with Potent In Vitro Antiretroviral Activity, Low Metabolic Toxicity, and Favorable Resistance Profile</p>

    <p>          Callebaut, Christian, Stray, Kirsten, Tsai, Luong, Xu, Lianhong, He, Gong-Xin, Mulato, Andrew, Priskich, Tina, Parkin, Neil, Lee, William, and Cihlar, Tomas</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A27-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3/2/dddbdc2129b6abe92e78ff74b45fa58a">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3/2/dddbdc2129b6abe92e78ff74b45fa58a</a> </p><br />

    <p>15.   60662   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Synthesis, Anti-HIV and CD4 Down-Modulation Activities of Novel CADA Compounds</p>

    <p>          Anugu, Sreenivasa, Bell, Thomas, Duffy, Noah, Vermeire, Kurt, and Schols, Dominique</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A48-A30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1T/2/025446fa16a7fcc80531b6ea8d70e3ca">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1T/2/025446fa16a7fcc80531b6ea8d70e3ca</a> </p><br />

    <p>16.   60663   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Design, Synthesis, Anti-HIV and Cytotoxicity of Novel Heterocyclic Compounds</p>

    <p>          Selvam, Periyasamy, Murugesh, Narayanan, Chandramohan, Markandavel, De Clercq, Erik, and Pannecouque, Christophe</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A51-A30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-22/2/e436907421d76e8c98c88b1f0b2e7be7">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-22/2/e436907421d76e8c98c88b1f0b2e7be7</a> </p><br />
    <br clear="all">

    <p>17.   60664   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Design, Synthesis and Anti-HIV Activity of Some Novel Isatin Derivatives</p>

    <p>          Selvam, Periyasamy, Murugesh, Narayanan, Chandramohan, Markandavel, Debyser, Zeger, Witvrouw, Myriam, and Engelborghs, Yves</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A68-A67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3J/2/3f46bbe8b7e0bc8019c4a8e4791d33d8">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3J/2/3f46bbe8b7e0bc8019c4a8e4791d33d8</a> </p><br />

    <p>18.   60665   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Targeting the Sticky Fingers of HIV-1</p>

    <p>          Blumenthal, Robert and Dimitrov, Dimiter S</p>

    <p>          Cell <b>2007</b>.  129(2): 243-245</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WSN-4NHMXD1-8/2/3791ccd3cac222b79120792bf7c7138a">http://www.sciencedirect.com/science/article/B6WSN-4NHMXD1-8/2/3791ccd3cac222b79120792bf7c7138a</a> </p><br />

    <p>19.   60666   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Discovery and Optimization of a Natural HIV-1 Entry Inhibitor Targeting the gp41 Fusion Peptide</p>

    <p>          Munch, Jan, Standker, Ludger, Adermann, Knut, Schulz, Axel, Schindler, Michael, Chinnadurai, Raghavan, Pohlmann, Stefan, Chaipan, Chawaree, Biet, Thorsten, Peters, Thomas, Meyer, Bernd, Wilhelm, Dennis, Lu, Hong, Jing, Weiguo, Jiang, Shibo, Forssmann, Wolf-Georg, and Kirchhoff, Frank</p>

    <p>          Cell <b>2007</b>.  129(2): 263-275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WSN-4NHMXD1-D/2/9f96eb66ad3ba7437b9a667c481e9006">http://www.sciencedirect.com/science/article/B6WSN-4NHMXD1-D/2/9f96eb66ad3ba7437b9a667c481e9006</a> </p><br />

    <p>20.   60667   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Safety and efficacy of the HIV-1 integrase inhibitor raltegravir (MK-0518) in treatment-experienced patients with multidrug-resistant virus: a phase II randomised controlled trial</p>

    <p>          Grinsztejn, Beatriz, Nguyen, Bach-Yen, Katlama, Christine, Gatell, Jose M, Lazzarin, Adriano, Vittecoq, Daniel, Gonzalez, Charles J, Chen, Joshua, Harvey, Charlotte M, and Isaacs, Robin D</p>

    <p>          The Lancet <b>2007</b>.  369(9569): 1261-1269</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1B-4NGCX8Y-12/2/aada5f0e19035eba9ebae4992a624a9d">http://www.sciencedirect.com/science/article/B6T1B-4NGCX8Y-12/2/aada5f0e19035eba9ebae4992a624a9d</a> </p><br />

    <p>21.   60668   HIV-LS-372; WOS-HIV-4/15/2007</p>

    <p class="memofmt1-2">          Regioselective synthesis and anti-HIV activity of the novel 2-and 4-substituted pyrazolo[4,5-e][1,2,4]thiadiazines</p>

    <p>          Liu, XY, Yan, RZ, Chen, NG, and Xu, WF</p>

    <p>          CHINESE CHEMICAL LETTERS <b>2007</b>.  18(2): 137-140, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244770600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244770600006</a> </p><br />
    <br clear="all">

    <p>22.   60669   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Entry inhibitor-based microbicides are active in vitro against HIV-1 isolates from multiple genetic subtypes</p>

    <p>          Ketas, Thomas J, Schader, Susan M, Zurita, Juan, Teo, Esther, Polonis, Victoria, Lu, Min, Klasse, Per Johan, and Moore, John P</p>

    <p>          Virology <b>2007</b>.  In Press, Corrected Proof: 256</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NFXG6Y-2/2/ed3efb80176545e7ecccbfbcf312615e">http://www.sciencedirect.com/science/article/B6WXR-4NFXG6Y-2/2/ed3efb80176545e7ecccbfbcf312615e</a> </p><br />

    <p>23.   60670   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          In vitro anti-HIV and -HSV activity and safety of sodium rutin sulfate as a microbicide candidate</p>

    <p>          Tao, Jian, Hu, Qinxue, Yang, Jing, Li, Rurun, Li, Xiuyi, Lu, Chengping, Chen, Chaoyin, Wang, Ling, Shattock, Robin, and Ben, Kunlong</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 304</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4NFS18K-1/2/bf9c5c337af293bfc819359fc67d1d28">http://www.sciencedirect.com/science/article/B6T2H-4NFS18K-1/2/bf9c5c337af293bfc819359fc67d1d28</a> </p><br />

    <p>24.   60671   HIV-LS-372; EMBASE-HIV-4/19/2007</p>

    <p class="memofmt1-2">          Synthesis and evaluation of 2&#39;-substituted cyclobutyl nucleosides and nucleotides as potential anti-HIV agents</p>

    <p>          Li, Yongfeng, Mao, Shuli, Hager, Michael W, Becnel, Kimberlynne D, Schinazi, Raymond F, and Liotta, Dennis C</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  304</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NDDM03-3/2/c4fefd14b8e816a47d7c378ca0a7c070">http://www.sciencedirect.com/science/article/B6TF9-4NDDM03-3/2/c4fefd14b8e816a47d7c378ca0a7c070</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
