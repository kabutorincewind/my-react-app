

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-343.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BjksyT523IILUq+gOD4LsxqFJhlmm9xjQrC/AEScsLnji4u7QpR2CCCcif6v5nS6uNaRlYp26wzBaS5qtWRgGukE8IOABrgXtkuJ1vttgLucZ5wpVuzACF0N2kg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0924ABD0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-343-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48543   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Microwave-Accelerated Synthesis of P1&#39;-Extended HIV-1 Protease Inhibitors Encompassing a Tertiary Alcohol in the Transition-State Mimicking Scaffold</p>

    <p>          Ekegren, JK, Ginman, N, Johansson, A, Wallberg, H, Larhed, M, Samuelsson, B, Unge, T, and Hallberg, A</p>

    <p>          J Med Chem <b>2006</b>.  49(5): 1828-1832</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509598&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509598&amp;dopt=abstract</a> </p><br />

    <p>2.     48544   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Synthesis and Anti-HIV Activity of d- and l-Thietanose Nucleosides</p>

    <p>          Choo, H, Chen, X, Yadav, V, Wang, J, Schinazi, RF, and Chu, CK</p>

    <p>          J Med Chem <b>2006</b>.  49(5): 1635-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509580&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509580&amp;dopt=abstract</a> </p><br />

    <p>3.     48545   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Novel HIV-1 Integrase Inhibitors Derived from Quinolone Antibiotics</p>

    <p>          Sato, M, Motomura, T, Aramaki, H, Matsuda, T, Yamashita, M, Ito, Y, Kawakami, H, Matsuzaki, Y, Watanabe, W, Yamataka, K, Ikeda, S, Kodama, E, Matsuoka, M, and Shinkai, H</p>

    <p>          J Med Chem <b>2006</b>.  49(5): 1506-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509568&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509568&amp;dopt=abstract</a> </p><br />

    <p>4.     48546   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Synthesis, antiviral activity, and pharmacokinetic evaluation of P3 pyridylmethyl analogs of oximinoarylsulfonyl HIV-1 protease inhibitors</p>

    <p>          Randolph, JT, Huang, PP, Flosi, WJ, Degoey, D, Klein, LL, Yeung, CM, Flentge, C, Sun, M, Zhao, C, Dekhtyar, T, Mo, H, Colletti, L, Kati, W, Marsh, KC, Molla, A, and Kempf, DJ </p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16504523&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16504523&amp;dopt=abstract</a> </p><br />

    <p>5.     48547   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Tetrazole thioacetanilides: Potent non-nucleoside inhibitors of WT HIV reverse transcriptase and its K103N mutant</p>

    <p>          Muraglia, E, Kinzel, OD, Laufer, R, Miller, MD, Moyer, G, Munshi, V, Orvieto, F, Palumbi, MC, Pescatore, G, Rowley, M, Williams, PD, and Summa, V</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16503141&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16503141&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48548   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type 1 replication with artificial transcription factors targeting the highly conserved primer-binding site</p>

    <p>          Eberhardy, SR, Goncalves, J, Coelho, S, Segal, DJ, Berkhout, B, and Barbas, CF 3rd</p>

    <p>          J Virol <b>2006</b>.  80(6): 2873-83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16501096&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16501096&amp;dopt=abstract</a> </p><br />

    <p>7.     48549   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Cyclophilin A and TRIM5{alpha} Independently Regulate Human Immunodeficiency Virus Type 1 Infectivity in Human Cells</p>

    <p>          Sokolskaja, E, Berthoux, L, and Luban, J</p>

    <p>          J Virol <b>2006</b>.  80(6): 2855-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16501094&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16501094&amp;dopt=abstract</a> </p><br />

    <p>8.     48550   HIV-LS-343; SCIFINDER-HIV-2/27/2006</p>

    <p class="memofmt1-2">          C-Glycoside analogues of b-galactosylceramide with a simple ceramide substitute: Synthesis and binding to HIV-1 gp120</p>

    <p>          Augustin, Line A, Fantini, Jacques, and Mootoo, David R</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(4): 1182-1188</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48551   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Interaction of small molecule inhibitors of HIV-1 entry with CCR5</p>

    <p>          Seibert, C, Ying, W, Gavrilov, S, Tsamis, F, Kuhmann, SE, Palani, A, Tagat, JR, Clader, JW, McCombie, SW, Baroudy, BM, Smith, SO, Dragic, T, Moore, JP, and Sakmar, TP</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16494916&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16494916&amp;dopt=abstract</a> </p><br />

    <p>10.   48552   HIV-LS-343; SCIFINDER-HIV-2/27/2006</p>

    <p class="memofmt1-2">          QSAR studies on the use of 5,6-dihydro-2-pyrones as HIV-1 protease inhibitors</p>

    <p>          Agrawal, Vijay K, Singh, Jyoti, Mishra, Krishna C, Khadikar, Padmakar V, and Jaliwala, Yusuf Ali</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2006</b>.(2): 162-177 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   48553   HIV-LS-343; SCIFINDER-HIV-2/27/2006</p>

    <p class="memofmt1-2">          Preparation of amino acid-containing nucleotides as prodrugs and antiviral agents</p>

    <p>          Boojamra, Constantine G, Lin, Kuei-Ying, Mackman, Richard L, Markevitch, David Y, Petrakovsky, Oleg V, Ray, Adrian S, and Zhang, Lijun</p>

    <p>          PATENT:  WO <b>2006015261</b>  ISSUE DATE:  20060209</p>

    <p>          APPLICATION: 2005  PP: 173 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   48554   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type 1 infection in macrophages by an alpha-v integrin blocking antibody</p>

    <p>          Bosch, B, Clotet-Codina, I, Blanco, J, Pauls, E, Coma, G, Cedeno, S, Mitjans, F, Llano, A, Bofill, M, Clotet, B, Piulats, J, and Este, JA</p>

    <p>          Antiviral Res <b>2006</b>.  69(3): 173-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16473416&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16473416&amp;dopt=abstract</a> </p><br />

    <p>13.   48555   HIV-LS-343; SCIFINDER-HIV-2/27/2006</p>

    <p class="memofmt1-2">          Induction of APOBEC3 family proteins, a defensive maneuver underlying interferon-induced anti-HIV-1 activity</p>

    <p>          Peng, Gang, Lei, Ke Jian, Jin, Wenwen, Greenwell-Wild, Teresa, and Wahl, Sharon M</p>

    <p>          Journal of Experimental Medicine <b>2006</b>.  203(1): 41-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48556   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Susceptibility to protease inhibitors in HIV-2 primary isolates from patients failing antiretroviral therapy</p>

    <p>          Rodes, B, Sheldon, J, Toro, C, Jimenez, V, Alvarez, MA, and Soriano, V</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464891&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464891&amp;dopt=abstract</a> </p><br />

    <p>15.   48557   HIV-LS-343; PUBMED-HIV-3/6/2006</p>

    <p class="memofmt1-2">          Mining the NCI antiviral compounds for HIV-1 integrase inhibitors</p>

    <p>          Deng, J, Kelley, JA, Barchi, JJ, Sanchez, T, Dayam, R, Pommier, Y, and Neamati, N</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16460953&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16460953&amp;dopt=abstract</a> </p><br />

    <p>16.   48558   HIV-LS-343; SCIFINDER-HIV-2/27/2006</p>

    <p class="memofmt1-2">          Herbal extract with antiviral properties</p>

    <p>          Abdullahi, Jacob J</p>

    <p>          PATENT:  US <b>2006024386</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2005-58730</p>

    <p>          ASSIGNEE:  (Nigeria)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   48559   HIV-LS-343; WOS-HIV-2/26/2006</p>

    <p class="memofmt1-2">          Identification of sanguinarine as a novel HIV protease inhibitor from high-throughput screening of 2,000 drugs and natural products with a cell-based assay</p>

    <p>          Cheng, TJ, Goodsell, DS, and Kan, CC</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(5): 364-371, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167800003</a> </p><br />
    <br clear="all">

    <p>18.   48560   HIV-LS-343; WOS-HIV-2/26/2006</p>

    <p class="memofmt1-2">          Structure based design of inhibitors of aspartic protease of HIV-1</p>

    <p>          Frecer, V, Jedinak, A, Tossi, A, Berti, F, Benedetti, F, Romeo, D, and Miertus, S</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(8): 638-646, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168100010</a> </p><br />

    <p>19.   48561   HIV-LS-343; WOS-HIV-2/26/2006</p>

    <p class="memofmt1-2">          p27(SJ), a novel protein in St John&#39;s Wort, that suppresses expression of HIV-1 genome</p>

    <p>          Darbinian-Sarkissian, N, Darbinyan, A, Otte, J, Radhakrishnan, S, Sawaya, BE, Arzumanyan, A, Chipitsyna, G, Popov, Y, Rappaport, J, Amini, S, and Khalili, K</p>

    <p>          GENE THERAPY <b>2006</b>.  13(4): 288-295, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235184000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235184000002</a> </p><br />

    <p>20.   48562   HIV-LS-343; WOS-HIV-3/5/2006</p>

    <p class="memofmt1-2">          How can (-)-epigallocatechin gallate from green tea prevent HIV-1 infection? Mechanistic insights from computational modeling and the implication for rational design of anti-HIV-1 entry inhibitors</p>

    <p>          Hamza, A and Zhan, CG</p>

    <p>          JOURNAL OF PHYSICAL CHEMISTRY B <b>2006</b>.  110(6): 2910-2917, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235373400063">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235373400063</a> </p><br />

    <p>21.   48563   HIV-LS-343; WOS-HIV-3/5/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of carbocyclic 3 &#39;-azidothymidine (AZT) analogues and their cycloSal-phosphate triesters</p>

    <p>          Ludek, OR, Balzarini, J, and Meier, C</p>

    <p>          EUROPEAN JOURNAL OF ORGANIC CHEMISTRY <b>2006</b>.(4): 932-940, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235404000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235404000013</a> </p><br />

    <p>22.   48564   HIV-LS-343; WOS-HIV-3/5/2006</p>

    <p class="memofmt1-2">          Anti-HIV I/II activity and molecular cloning of a novel mannose/sialic acid-binding lectin from rhizome of Polygonatum cyrtonema Hua</p>

    <p>          An, J, Liu, JZ, Wu, CF, Li, J, Dai, L, Van Damme, E, Balzarini, J, De Clercq, E, Chen, F, and Bao, JK</p>

    <p>          ACTA BIOCHIMICA ET BIOPHYSICA SINICA <b>2006</b>.  38(2): 70-78, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235370000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235370000001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
