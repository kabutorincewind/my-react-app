

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-05-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7a8kuFt4DkE9U62EE3TKPhMSbbv9QY/jaV5W3qYE9WAwqToBCaALC++MWzmFwrcrCwL4JDqtDU2/3WYp6skeoiG4jIRTa0ET79Y9KY1qawDj6bO2P+FlA+tvgQI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E43391F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: April 24 - May 7, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25847769">Novel Imidazo[2,1-b]-1,3,4-thiadiazoles as Promising Antifungal Agents against Clinical Isolate of Cryptococcus neoformans.</a> Alwan, W.S., R. Karpoormath, M.B. Palkar, H.M. Patel, R.A. Rane, M.S. Shaikh, A. Kajee, and K.P. Mlisana. European Journal of Medicinal Chemistry, 2015. 95: p. 514-525. PMID[25847769].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25614071">Screening of Antimicrobial Activity of Macroalgae Extracts from the Moroccan Atlantic Coast.</a> El Wahidi, M., B. El Amraoui, M. El Amraoui, and T. Bamhaoud. Annales Pharmaceutiques Françaises, 2015. 73(3): p. 190-196. PMID[25614071].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25280048">In Vitro Pharmacological Screening of Three Newly Synthesised Pyrimidine Derivatives.</a> Khan, Z.U., A.U. Khan, P. Wan, Y. Chen, D. Kong, S. Khan, and K. Tahir. Natural Product Research, 2015. 29(10): p. 933-938. PMID[25280048].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25558076">Antifungal Activity of Compounds Targeting the Hsp90-calcineurin Pathway against Various Mould Species.</a> Lamoth, F., B.D. Alexander, P.R. Juvvadi, and W.J. Steinbach. Journal of Antimicrobial Chemotherapy, 2015. 70(5): p. 1408-1411. PMID[25558076].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25659694">Amidated Pectin-based Wafers for Econazole Buccal Delivery: Formulation Optimization and Antimicrobial Efficacy Estimation.</a> Mura, P., N. Mennini, I. Kosalec, S. Furlanetto, S. Orlandini, and M. Jug. Carbohydrate Polymers, 2015. 121: p. 231-240. PMID[25659694].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25893812">Ieodoglucomide C and Ieodoglycolipid, New Glycolipids from a Marine-derived Bacterium Bacillus licheniformis 09IDYM23.</a> Tareq, F.S., H.S. Lee, Y.J. Lee, J.S. Lee, and H.J. Shin. Lipids, 2015. 50(5): p. 513-519. PMID[25893812].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25681127">Natural Antimicrobial Peptides against Mycobacterium tuberculosis.</a> Abedinzadeh, M., M. Gaeini, and S. Sardari. Journal of Antimicrobial Chemotherapy, 2015. 70(5): p. 1285-1289. PMID[25681127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25788466">Novel 1,4-Substituted-1,2,3-triazoles as Antitubercular Agents.</a> Altimari, J.M., S.C. Hockey, H.I. Boshoff, A. Sajid, and L.C. Henderson. ChemMedChem, 2015. 10(5): p. 787-791. PMID[25788466].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25733512">Impact of beta-Lactamase Inhibition on the Activity of Ceftaroline against Mycobacterium tuberculosis and Mycobacterium abscessus.</a> Dubee, V., D. Soroka, M. Cortes, A.L. Lefebvre, L. Gutmann, J.E. Hugonnet, M. Arthur, and J.L. Mainardi. Antimicrobial Agents and Chemotherapy, 2015. 59(5): p. 2938-2941. PMID[25733512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25812481">Synthesis and Evaluation of 1,2-Trans Alkyl Galactofuranoside Mimetics as Mycobacteriostatic Agents.</a> Dureau, R., M. Gicquel, I. Artur, J.P. Guegan, B. Carboni, V. Ferrieres, F. Berree, and L. Legentil. Organic &amp; Biomolecular Chemistry, 2015. 13(17): p. 4940-4952. PMID[25812481].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25797161">Novel 2-(2-Phenalkyl)-1H-benzo[d]imidazoles as Antitubercular Agents. Synthesis, Biological Evaluation and Structure-activity Relationship.</a> Gobis, K., H. Foks, K. Suchan, E. Augustynowicz-Kopec, A. Napiorkowska, and K. Bojanowski. Bioorganic &amp; Medicinal Chemistry, 2015. 23(9): p. 2112-2120. PMID[25797161].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25819330">Synthesis and Antimycobacterial Properties of Ring-Substituted 6-Hydroxynaphthalene-2-carboxanilides.</a> Kos, J., E. Nevin, M. Soral, I. Kushkevych, T. Gonec, P. Bobal, P. Kollar, A. Coffey, J. O&#39;Mahony, T. Liptaj, K. Kralova, and J. Jampilek. Bioorganic &amp; Medicinal Chemistry, 2015. 23(9): p. 2035-2043. PMID[25819330].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25819094">Synthesis and Biological Evaluation of Nonionic Substrate Mimics of UDP-Galp as Candidate Inhibitors of UDP Galactopyranose Mutase (UGM).</a> Kuppala, R., S. Borrelli, K. Slowski, D.A. Sanders, K.P. Ravindranathan Kartha, and B.M. Pinto. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(9): p. 1995-1997. PMID[25819094].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25801151">Design and Synthesis of Novel Quinoline-aminopiperidine Hybrid Analogues as Mycobacterium tuberculosis DNA GyraseB Inhibitors.</a> Medapi, B., J. Renuka, S. Saxena, J.P. Sridevi, R. Medishetti, P. Kulkarni, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2015. 23(9): p. 2062-2078. PMID[25801151].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25905816">Tropical Plant Extracts Modulating the Growth of Mycobacterium ulcerans.</a> Mougin, B., R.B. Tian, and M. Drancourt. Plos One, 2015. 10(4): p. e0124626. PMID[25905816].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25794789">Synthesis and Biological Evaluation of New Imidazo[2,1-b][1,3,4]thiadiazole-benzimidazole Derivatives.</a> Ramprasad, J., N. Nayak, U. Dalimba, P. Yogeeswari, D. Sriram, S.K. Peethambar, R. Achur, and H.S. Kumar. European Journal of Medicinal Chemistry, 2015. 95: p. 49-63. PMID[25794789].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25818768">Design and Synthesis of Novel Anti-tuberculosis Agents from the Celecoxib Pharmacophore.</a> Salunke, S.B., A.K. Azad, N.P. Kapuriya, J.M. Balada-Llasat, P. Pancholi, L.S. Schlesinger, and C.S. Chen. Bioorganic &amp; Medicinal Chemistry, 2015. 23(9): p. 1935-1943. PMID[25818768].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25840796">Identification of Novel Class of Falcipain-2 Inhibitors as Potential Antimalarial Agents.</a> Chakka, S.K., M. Kalamuddin, S. Sundararaman, L. Wei, S. Mundra, R. Mahesh, P. Malhotra, A. Mohmmed, and L.P. Kotra. Bioorganic &amp; Medicinal Chemistry, 2015. 23(9): p. 2221-2240. PMID[25840796].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25791675">Discovery of New Thienopyrimidinone Derivatives Displaying Antimalarial Properties toward both Erythrocytic and Hepatic Stages of Plasmodium.</a> Cohen, A., P. Suzanne, J.C. Lancelot, P. Verhaeghe, A. Lesnard, L. Basmaciyan, S. Hutter, M. Laget, A. Dumetre, L. Paloque, E. Deharo, M.D. Crozet, P. Rathelot, P. Dallemagne, A. Lorthiois, C.H. Sibley, P. Vanelle, A. Valentin, D. Mazier, S. Rault, and N. Azas. European Journal of Medicinal Chemistry, 2015. 95: p. 16-28. PMID[25791675].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25815413">Synthesis and Potent Antimalarial Activity of Kalihinol B.</a> Daub, M.E., J. Prudhomme, K. Le Roch, and C.D. Vanderwal. Journal of the American Chemical Society, 2015. 137(15): p. 4912-4915. PMID[25815413].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25736371">Amodiaquine-ciprofloxacin: A Potential Combination Therapy against Drug Resistant Malaria.</a> Falajiki, Y.F., O. Akinola, O.O. Abiodun, C.T. Happi, A. Sowunmi, and G.O. Gbotosho. Parasitology, 2015. 142(6): p. 849-854. PMID[25736371].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25871440">Antiplasmodial and Cytotoxic Triterpenoids from the Bark of the Cameroonian Medicinal Plant Entandrophragma congoense.</a> Happi, G.M., S.F. Kouam, F.M. Talontsi, M. Lamshoft, S. Zuhlke, J.O. Bauer, C. Strohmann, and M. Spiteller. Journal of Natural Products, 2015. 78(4): p. 604-614. PMID[25871440].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25654185">Antimalarial Activity of 4-Amidinoquinoline and 10-Amidinobenzonaphthyridine Derivatives.</a> Korotchenko, V., R. Sathunuru, L. Gerena, D. Caridha, Q. Li, M. Kreishman-Deitrick, P.L. Smith, and A.J. Lin. Journal of Medicinal Chemistry, 2015. 58(8): p. 3411-3431. PMID[25654185].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25803573">Design and Synthesis of a Screening Library Using the Natural Product Scaffold 3-Chloro-4-hydroxyphenylacetic acid.</a> Kumar, R., M.C. Sadowski, C. Levrier, C.C. Nelson, A.J. Jones, J.P. Holleran, V.M. Avery, P.C. Healy, and R.A. Davis. Journal of Natural Products, 2015. 78(4): p. 914-918. PMID[25803573].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25691631">Simultaneous Administration of 2-Aminoethyl Diphenylborinate and Chloroquine Reverses Chloroquine Resistance in Malaria Parasites.</a> Mossaad, E., W. Furuyama, M. Enomoto, S. Kawai, K. Mikoshiba, and S. Kawazu. Antimicrobial Agents and Chemotherapy, 2015. 59(5): p. 2890-2892. PMID[25691631].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25801154">Investigation into Novel Thiophene- and Furan-based 4-Amino-7-chloroquinolines Afforded Antimalarials that Cure Mice.</a> Opsenica, I.M., T.Z. Verbic, M. Tot, R.J. Sciotti, B.S. Pybus, O. Djurkovic-Djakovic, K. Slavic, and B.A. Solaja. Bioorganic &amp; Medicinal Chemistry, 2015. 23(9): p. 2176-2186. PMID[25801154].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25817773">Synthesis and in Vitro Antiplasmodial Evaluation of 7-Chloroquinoline-chalcone and 7-Chloroquinoline-ferrocenylchalcone conjugates.</a> Raj, R., A. Saini, J. Gut, P.J. Rosenthal, and V. Kumar. European Journal of Medicinal Chemistry, 2015. 95: p. 230-239. PMID[25817773].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25784585">Targeting the Erythrocytic and Liver Stages of Malaria Parasites with S-Triazine-based Hybrids.</a> Rodrigues, C.A., R.F. Frade, I.S. Albuquerque, M.J. Perry, J. Gut, M. Machado, P.J. Rosenthal, M. Prudencio, C.A. Afonso, and R. Moreira. ChemMedChem, 2015. 10(5): p. 883-890. PMID[25784585].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0424-050715.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352577000010">Probiotic Lactobacillus rhamnosus GR-1 and Lactobacillus reuteri RC-14 Exhibit Strong Antifungal Effects against Vulvovaginal Candidiasis-causing Candida glabrata Isolates.</a> Chew, S.Y., Y.K. Cheah, H.F. Seow, D. Sandai, and L.T.L. Than. Journal of Applied Microbiology, 2015. 118(5): p. 1180-1190. ISI[000352577000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352050200016">In Vitro Evaluation of the Activity of Thiosemicarbazone Derivatives against Mycotoxigenic Fungi Affecting Cereals.</a> Degola, F., C. Morcia, F. Bisceglie, F. Mussi, G. Tumino, R. Ghizzoni, G. Pelosi, V. Terzi, A. Buschini, F.M. Restivo, and T. Lodi. International Journal of Food Microbiology, 2015. 200: p. 104-111. ISI[000352050200016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351650500005">Evaluation of Antifungal Activity of Hydroalcoholic Extracts of Citrullus colocynthis Fruit.</a> Eidi, S., H.G. Azadi, N. Rahbar, and H.R. Mehmannavaz. Journal of Herbal Medicine, 2015. 5(1): p. 36-40. ISI[000351650500005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351560600026">Antimicrobial, Antioxidant and Cytotoxicity Potential of Manihot multifida (L.) Crantz (Euphorbiaceae).</a> Fabri, R.L., D.S. De Sa, A.P.O. Pereira, E. Scio, D.S. Pimenta, and L.M. Chedier. Anais da Academia Brasileira de Ciencias, 2015. 87(1): p. 303-311. ISI[000351560600026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351626200006">Synthesis and in Vitro Antimicrobial Activity of 1,3,4-Oxadiazole-2-thiol and its Analogs</a>. Galge, R., A. Raju, M.S. Degani, and B.N. Thorat. Journal of Heterocyclic Chemistry, 2015. 52(2): p. 352-357. ISI[000351626200006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351646100025">Synthesis, QSAR and Anticandidal Evaluation of 1,2,3-Triazoles Derived from Naturally Bioactive Scaffolds.</a> Irfan, M., B. Aneja, U. Yadava, S.I. Khan, N. Manzoor, C.G. Daniliuc, and M. Abid. European Journal of Medicinal Chemistry, 2015. 93: p. 246-254. ISI[000351646100025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351626200022">Synthesis of Novel Dibenzo-18-crown-6-ether-Functionalized Benzimidazoles and its Applications in Colorimetric Recognition to Hg2+ and as Antifungal Agents.</a> Jagadale, S.D., A.D. Sawant, P.P. Patil, D.R. Patil, A.G. Mulik, D.R. Chandam, S.A. Sankpal, and M.B. Deshmukh. Journal of Heterocyclic Chemistry, 2015. 52(2): p. 468-472. ISI[000351626200022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352139800069">Synthesis, Spectral, Thermal Behaviors, and Antimicrobial Studies of Some Nanostructure Five Coordinated Hg(II) Complexes.</a> Jahromi, S.M., M. Montazerozohori, and S.M. Jahromi. Journal of Thermal Analysis and Calorimetry, 2015. 120(1): p. 591-602. ISI[000352139800069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351767200001">Evaluation of Antioxidant, Total Phenol and Flavonoid Content and Antimicrobial Activities of Artocarpus altilis (Breadfruit) of Underutilized Tropical Fruit Extracts.</a> Jalal, T.K., I.A. Ahmed, M. Mikail, L. Momand, S. Draman, M.L.M. Isa, M. Rasad, M.N. Omar, M. Ibrahim, and R.A. Wahab. Applied Biochemistry and Biotechnology, 2015. 175(7): p. 3231-3243. ISI[000351767200001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352288900009">Synthesis, Characterization and in Vitro Evaluation of Cytotoxicity and Antimicrobial Activity of Chitosan-metal Nanocomposites.</a> Kaur, P., R. Thakur, M. Barnela, M. Chopra, A. Manuja, and A. Chaudhury. Journal of Chemical Technology and Biotechnology, 2015. 90(5): p. 867-873. ISI[000352288900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352033900017">Cyclic Colisporifungin and Linear Cavinafungins, Antifungal Lipopeptides Isolated from Colispora cavincola.</a> Ortiz-Lopez, F.J., M.C. Monteiro, V. Gonzalez-Menendez, J.R. Tormo, O. Genilloud, G.F. Bills, F. Vicente, C.W. Zhang, T. Roemer, S.B. Singh, and F. Reyes. Journal of Natural Products, 2015. 78(3): p. 468-475. ISI[000352033900017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351646100052">Synthesis, Biological Evaluation and 3D QSAR Study of 2,4-Disubstituted Quinolines as Anti-tuberculosis Agents.</a> Patel, S.R., R. Gangwal, A.T. Sangamwar, and R. Jain. European Journal of Medicinal Chemistry, 2015. 93: p. 511-522. ISI[000351646100052].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352239300006">Chemical Compositions and Biological Activities of the Essential Oils of Beilschmiedia madang Blume (Lauraceae).</a> Salleh, W., F. Ahmad, and K.H. Yen. Archives of Pharmacal Research, 2015. 38(4): p. 485-493. ISI[000352239300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351662000007">Chemical Study of Hortia superba (Rutaceae) and Investigation of the Antimycobacterial Activity of Crude Extracts and Constituents Isolated from Hortia Species.</a> Severino, V.G.P., A.F. Monteiro, M. da Silva, R. Lucarini, and C.H.G. Martins. Quimica Nova, 2015. 38(1): p. 42-45. ISI[000351662000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352199000029">Inhibitory Effect of Plant (Poly)Phenolics on Growth and Biofilm Formation by Candida albicans.</a> Shahzad, M., L. Sherry, R. Rajendran, C.A. Edwards, E. Combet, and G. Ramage. Proceedings of the Nutrition Society, 2014. 73(OCE1): p. E28-E28. ISI[000352199000029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br />  

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351780700007">Design, Synthesis and Biological Evaluation of Novel Fluorinated Heterocyclic Hybrid Molecules Based on Triazole &amp; Quinoxaline Scaffolds Lead to Highly Potent Antimalarials and Antibacterials.</a> Shekhar, A.C., B.P.V. Lingaiah, P.S. Rao, B. Narsaiah, A.D. Allanki, and P.S. Sijwali. Letters in Drug Design &amp; Discovery, 2015. 12(5): p. 393-407. ISI[000351780700007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352077600003">Antimicrobial Activity of Natural Products from the Flora of Northern Ontario, Canada.</a> Vandal, J., M.M. Abou-Zaid, G. Ferroni, and L.G. Leduc. Pharmaceutical Biology, 2015. 53(6): p. 800-806. ISI[000352077600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351551700004">Synthesis, Characterisation, and Antifungal Activities of Novel Benzodiazaborines.</a> Yang, J., B.J. Johnson, A.A. Letourneau, C.M. Vogels, A. Decken, F.J. Baerlocher, and S.A. Westcott. Australian Journal of Chemistry, 2015. 68(3): p. 366-372. ISI[000351551700004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p><br /> 

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351646100059">Synthesis and Evaluation of Antimycobacterial Activity of New Benzimidazole Aminoesters.</a> Yoon, Y.K., M.A. Ali, A.C. Wei, T.S. Choon, and R. Ismail. European Journal of Medicinal Chemistry, 2015. 93: p. 614-624. ISI[000351646100059].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0424-050715.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
