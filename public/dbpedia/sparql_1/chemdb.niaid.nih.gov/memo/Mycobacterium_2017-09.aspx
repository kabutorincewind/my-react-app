

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="UevetbiSfFkc2mjD1mM2en2rYPFWZeskzSZxuRXGQcpOTahFPN6XTCmizIy9+nsS3vZ+prgjuajNuE0BjSr1ZTxLVdhgR5/1ArfbNt6Uj+xqmqBU5Y5mR+s8JHs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="682207E0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
   <h1 class="memofmt2-h1"><a name="_Toc479675800"><i>Mycobacterium</i> Citations List: September, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28715707">Design, Synthesis and 2D QSAR Study of Novel Pyridine and Quinolone Hydrazone Derivatives as Potential Antimicrobial and Antitubercular Agents.</a> Abdelrahman, M.A., I. Salama, M.S. Gomaa, M.M. Elaasser, M.M. Abdel-Aziz, and D.H. Soliman. European Journal of Medicinal Chemistry, 2017. 138: p. 698-714. PMID[28715707].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28842600">Inhibiting Mycobacterial Tryptophan Synthase by Targeting the Inter-subunit Interface.</a> Abrahams, K.A., J.A.G. Cox, K. Futterer, J. Rullas, F. Ortega-Muro, N.J. Loman, P.J. Moynihan, E. Perez-Herran, E. Jimenez, J. Esquivias, D. Barros, L. Ballell, C. Alemparte, and G.S. Besra. Scientific Reports, 2017. 7(9430): 15pp. PMID[28842600]. PMCID[PMC5573416].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28846409">Nitazoxanide Analogs Require Nitroreduction for Antimicrobial Activity in Mycobacterium smegmatis.</a> Buchieri, M.V., M. Cimino, S. Rebollo-Ramirez, C. Beauvineau, A. Cascioferro, S. Favre-Rochex, O. Helynck, D. Naud-Martin, G. Larrouy-Maumus, H. Munier-Lehmann, and B. Gicquel. Journal of Medicinal Chemistry, 2017. 60(17): p. 7425-7433. PMID[28846409].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408644100053">Synthesis of Triazole Derivatives of 9-Ethyl-9H-carbazole and Dibenzo[b,d]furan and Evaluation of Their Antimycobacterial and Immunomodulatory Activity.</a> Chirke, S.S., J.S. Krishna, B.B. Rathod, S.R. Bonam, V.M. Khedkar, B.V. Rao, H.M.S. Kumar, and P.R. Shetty. ChemistrySelect, 2017. 2(24): p. 7309-7318. ISI[000408644100053].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28689097">Design, Synthesis and Antitubercular Potency of 4-Hydroxyquinolin-2(1H)-ones.</a> de Macedo, M.B., R. Kimmel, D. Urankar, M. Gazvoda, A. Peixoto, F. Cools, E. Torfs, L. Verschaeve, E.S. Lima, A. Lycka, D. Milicevic, A. Klasek, P. Cos, S. Kafka, J. Kosmrlj, and D. Cappoen. European Journal of Medicinal Chemistry, 2017. 138: p. 491-500. PMID[28689097].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28922808">The Discovery of Ceftazidime/Avibactam as an Anti-mycobacterium avium Agent.</a> Deshpande, D., S. Srivastava, M.L. Chapagain, P.S. Lee, K.N. Cirrincione, J.G. Pasipanodya, and T. Gumbo. The Journal of Antimicrobial Chemotherapy, 2017. 72(suppl_2): p. i36-i42. PMID[28922808].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28873466">Rational Design of Drug-like Compounds Targeting Mycobacterium marinum Melf Protein.</a> Dharra, R., S. Talwar, Y. Singh, R. Gupta, J.D. Cirillo, A.K. Pandey, M. Kulharia, and P.K. Mehta. Plos One, 2017. 12(9): e0183060. PMID[28873466]. PMCID[PMC5584760].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000410401200002">Synthesis and Antitubercular Activity of Some Novel 2,5-Dimethyl pyrrolyl benzohydrazide Derivatives.</a> Dixit, S.R., S.D. Joshi, V.H. Kulkarni, and T. Panneerselvam. Indian Journal of Heterocyclic Chemistry, 2017. 27(2): p. 117-121. ISI[000410401200002].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28802409">Exploring the Potential of T7 Bacteriophage Protein Gp2 as a Novel Inhibitor of Mycobacterial RNA Polymerase.</a> du Plessis, J., R. Cloete, L. Burchell, P. Sarkar, R.M. Warren, A. Christoffels, S. Wigneshweraraj, and S.L. Sampson. Tuberculosis, 2017. 106: p. 82-90. PMID[28802409].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28905050">Identification of Inhibitors Targeting Mycobacterium tuberculosis Cell Wall Biosynthesis via Dynamic Combinatorial Chemistry.</a> Fu, J., H. Fu, M. Dieu, I. Halloum, L. Kremer, Y. Xia, W. Pan, and S.P. Vincent. Chemical Communications, 2017. 53(77): p. 10632-10635. PMID[28905050].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28582669">QSAR-driven Design, Synthesis and Discovery of Potent Chalcone Derivatives with Antitubercular Activity.</a> Gomes, M.N., R.C. Braga, E.M. Grzelak, B.J. Neves, E. Muratov, R. Ma, L.L. Klein, S. Cho, G.R. Oliveira, S.G. Franzblau, and C.H. Andrade. European Journal of Medicinal Chemistry, 2017. 137: p. 126-138. PMID[28582669].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000410398900003">Synthesis of New Pyrazolyl pyrrole Derivatives as Antitubercular Agents.</a> Hallikeri, C.S., S.D. Joshi, B. Yenni, S. Dixit, and V.H. Kulkarni. Indian Journal of Heterocyclic Chemistry, 2017. 27(1): p. 17-23. ISI[000410398900003].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28735047">Sesamol Exhibits Potent Antimycobacterial Activity: Underlying Mechanisms and Impact on Virulence Traits.</a> Hans, S., S. Sharma, S. Hameed, and Z. Fatima. Journal of Global Antimicrobial Resistance, 2017. 10: p. 228-237. PMID[28735047].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28697688">Plumbago auriculata Leaf Extract-mediated AgNPs and Its Activities as Antioxidant, anti-TB and Dye Degrading Agents.</a> Jaryal, N. and H. Kaur. Journal of Biomaterials Science-Polymer Edition, 2017. 28(16): p. 1847-1858. PMID[28697688].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28723103">Synthesis of a Tiacumicin B Protected Aglycone.</a> Jeanne-Julien, L., G. Masson, E. Astier, G. Genta-Jouve, V. Servajean, J.M. Beau, S. Norsikian, and E. Roulland. Organic Letters, 2017. 19(15): p. 4006-4009. PMID[28723103].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28742349">Exploring the Sponge Consortium Plakortis symbiotica-Xestospongia deweerdtae as a Potential Source of Antimicrobial Compounds and Probing the Pharmacophore for Antituberculosis Activity of Smenothiazole A by Diverted Total Synthesis.</a> Jimenez-Romero, C., J.E. Rode, Y.M. Perez, S.G. Franzblau, and A.D. Rodriguez. Journal of Natural Products, 2017. 80(8): p. 2295-2303. PMID[28742349].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000410398900004">Synthesis and in Vitro Antimycobacterial Activity of 7-3-(Substituted) phenylprop-2-enoyl-2,3-diphenyl-5H,10H-benzo[G]quinoxaline-5,10-diones.</a> Kumar, S., N. Kumar, and S. Drabu. Indian Journal of Heterocyclic Chemistry, 2017. 27(1): p. 25-31. ISI[000410398900004].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28734205">Identification of Mycobacterium tuberculosis Enoyl-Acyl Carrier Protein Reductase Inhibitors: A Combined in-Silico and in-Vitro Analysis.</a> Lone, M.Y., M. Athar, V.K. Gupta, and P.C. Jha. Journal of Molecular Graphics &amp; Modelling, 2017. 76: p. 172-180. PMID[28734205].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28752666">Mannich Ketones as Possible Antimycobacterial Agents.</a> Lutz, Z., K. Orban, A. Bona, L. Mark, G. Maasz, L. Prokai, L. Seress, and T. Lorand. Archiv der Pharmazie, 2017. 350(9): e1700102. PMID[28752666].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408319400009">Design and Synthesis of Some Novel Fluorobenzimidazoles Substituted with Structural Motifs Present in Physiologically Active Natural Products for Antitubercular Activity.</a> Nandha, B., L.G. Nargund, S.L. Nargund, and K. Bhat. Iranian Journal of Pharmaceutical Research, 2017. 16(3): p. 929-942. ISI[000408319400009].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408299900075">Molecular Modeling of Drug-pathophysiological Mtb Protein Targets: Synthesis of Some 2-Thioxo-1,3-thiazolidin-4-one Derivatives as Anti-tubercular Agents.</a> Noorulla, K.M., A.J. Suresh, V. Devaraji, B. Mathew, and D. Umesh. Journal of Molecular Structure, 2017. 1147: p. 682-696. ISI[000408299900075].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28862683">Mechanochemical Synthesis and Biological Evaluation of Novel Isoniazid Derivatives with Potent Antitubercular Activity.</a> Oliveira, P.F.M., B. Guidetti, A. Chamayou, C. Andre-Barres, J. Madacki, J. Kordulakova, G. Mori, B.S. Orena, L.R. Chiarelli, M.R. Pasca, C. Lherbet, C. Carayon, S. Massou, M. Baron, and M. Baltas. Molecules, 2017. 22(9): E1457. PMID[28862683].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28937657">Synthesis of 2,4-Diaminopyrimidine Core-based Derivatives and Biological Evaluation of Their Anti-tubercular Activities.</a> Ouyang, Y., H. Yang, P. Zhang, Y. Wang, S. Kaur, X. Zhu, Z. Wang, Y. Sun, W. Hong, Y.F. Ngeow, and H. Wang. Molecules, 2017. 22(10): E1592. PMID[28937657].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28912539">Diphenyleneiodonium chloride (DPIC) Displays Broad-spectrum Bactericidal Activity.</a> Pandey, M., A.K. Singh, R. Thakare, S. Talwar, P. Karaulia, A. Dasgupta, S. Chopra, and A.K. Pandey. Scientific Reports, 2017. 7(11521): 8pp. PMID[28912539]. PMCID[PMC5599662].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408399300001">Mycobacterium tuberculosis Sterol 14 alpha-Demethylase Inhibitor Sulfonamides: Identified by High-throughput Screening.</a> Pradhan, S. and C. Sinha. Journal of the Indian Chemical Society, 2017. 94(5): p. 457-468. ISI[000408399300001].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28267891">Design, Synthesis, and in Vitro Antituberculosis Activity of Benzo[6,7]cyclohepta[1,2-b]pyridine-1,3,4-oxadiazole Derivatives.</a> Sajja, Y., S. Vanguru, H.R. Vulupala, L. Nagarapu, Y. Perumal, D. Sriram, and J.B. Nanubolu. Chemical Biology &amp; Drug Design, 2017. 90(4): p. 496-500. PMID[28267891].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28806050">Linking High-throughput Screens to Identify MoAs and Novel Inhibitors of Mycobacterium tuberculosis Dihydrofolate Reductase.</a> Santa Maria, J.P., Jr., Y. Park, L. Yang, N. Murgolo, M.D. Altman, P. Zuck, G. Adam, C. Chamberlin, P. Saradjian, P. Dandliker, H.I.M. Boshoff, C.E. Barry, 3rd, C. Garlisi, D.B. Olsen, K. Young, M. Glick, E. Nickbarg, and P.S. Kutchukian. ACS Chemical Biology, 2017. 12(9): p. 2448-2456. PMID[28806050].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000408319400026">Synthesis and Biological Evaluation of Thiosemicarbazide Derivatives Endowed with High Activity toward Mycobacterium bovis.</a> Sardari, S., S. Feizi, A.H. Rezayan, P. Azerang, S.M. Shahcheragh, G. Ghavami, and A. Habibi. Iranian Journal of Pharmaceutical Research, 2017. 16(3): p. 1128-1140. ISI[000408319400026].
    <br />
    <b>[WOS]</b>. TB_09_2017.</p><br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28880230">Design, Synthesis, Antimycobacterial Evaluation, and in Silico Studies of 3-(Phenylcarbamoyl)-pyrazine-2-carboxylic acids.</a> Semelkova, L., P. Janoscova, C. Fernandes, G. Bouz, O. Jandourek, K. Konecna, P. Paterova, L. Navratilova, J. Kunes, M. Dolezal, and J. Zitko. Molecules, 2017. 22(9): E1491. PMID[28880230].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28278765">Antimycobacterial, Docking and Molecular Dynamic Studies of Pentacyclic triterpenes from Buddleja saligna Leaves.</a> Singh, A., K.N. Venugopala, M.A. Khedr, M. Pillay, K.U. Nwaeze, Y. Coovadia, F. Shode, and B. Odhav. Journal of Biomolecular Structure &amp; Dynamics, 2017. 35(12): p. 2654-2664. PMID[28278765].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28827112">Design, Synthesis, and Evaluation of Substituted Nicotinamide adenine dinucleotide (NAD+) Synthetase Inhibitors as Potential Antitubercular Agents.</a> Wang, X., Y.M. Ahn, A.G. Lentscher, J.S. Lister, R.C. Brothers, M.M. Kneen, B. Gerratana, H.I. Boshoff, and C.S. Dowd. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(18): p. 4426-4430. PMID[28827112].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28671682">A Small-molecule Allosteric Inhibitor of Mycobacterium tuberculosis Tryptophan Synthase.</a> Wellington, S., P.P. Nag, K. Michalska, S.E. Johnston, R.P. Jedrzejczak, V.K. Kaushik, A.E. Clatworthy, N. Siddiqi, P. McCarren, B. Bajrami, N.I. Maltseva, S. Combs, S.L. Fisher, A. Joachimiak, S.L. Schreiber, and D.T. Hung. Nature Chemical Biology, 2017. 13(9): p. 943-950. PMID[28671682].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28646656">Azide-alkyne Cycloaddition Towards 1H-1,2,3-Triazole-tethered Gatifloxacin and Isatin Conjugates: Design, Synthesis and in Vitro Anti-mycobacterial Evaluation.</a> Xu, Z., X.F. Song, Y.Q. Hu, M. Qiang, and Z.S. Lv. European Journal of Medicinal Chemistry, 2017. 138: p. 66-71. PMID[28646656].
    <br />
    <b>[PubMed]</b>. TB_09_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170908&amp;CC=WO&amp;NR=2017149551A1&amp;KC=A1">Preparation of Substituted Aurone Alkaloids as Anti-mycobacterial Agents.</a> Gudup, S.S., S. Kumar, H.P. Aruri, U. Singh, G. Munagala, K.R. Yempalla, S. Singh, I.A. Khan, V.R. Asrey, and P.P. Singh. Patent. 2017. 2017-IN50078 2017149551: 35pp.
    <br />
    <b>[Patent]</b>. TB_09_2017.</p><br />

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170831&amp;CC=WO&amp;NR=2017147003A1&amp;KC=A1">Novel Macrocyclic Antibiotics and Uses Thereof in Treatment of Tuberculosis.</a> Hughes, D., L.L. Ling, A. Nitti, A.J. Peoples, and A. Spoering. Patent. 2017. 2017-US18294 2017147003: 47pp.
    <br />
    <b>[Patent]</b>. TB_09_2017.</p><br />

    <p class="plaintext">36. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170914&amp;CC=WO&amp;NR=2017155909A1&amp;KC=A1">Preparation of Quinoline Compounds as Antibacterial Agents and Uses Thereof.</a> Upton, A.M., C.B. Cooper, K.J.L. Marcel, J.E.G. Guillemont, W.M.M. Van Den Broeck, B.D. Palmer, and Z. Ma. Patent. 2017. 2017-US21031 2017155909: 186pp.
    <br />
    <b>[Patent]</b>. TB_09_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
