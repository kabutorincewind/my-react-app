

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-361.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pXwK1yk5vMLITASBVkcEzUwpWdlUTWUuXO6U6Y4u7+D+VdW/InqcdgGpCR/CrYo3404P3C2NUTWtH8pamSV6acn6HN8KJ+OlLAV8/8OTpNUKrrN8nAfFjTHG62A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="47AB26A2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-361-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59396   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-tubercular activity of a series of N&#39;-(disubstitutedbenzoyl)isoniazid derivatives</p>

    <p>          Neves, Ivan Jr, Lourenco, Maria CS, de Miranda, Guilherme de BP, Vasconcelos, Thatyana RA, Pais, Karla C, de A, Jose dos P Jr, Wardell, Solange MSV, Wardell, James L, and de Souza, Marcus VN</p>

    <p>          Letters in Drug Design &amp; Discovery <b>2006</b>.  3(6): 424-428</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     59397   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Topology and mutational analysis of the single Emb arabinofuranosyltransferase of Corynebacterium glutamicum as a model of Emb proteins of Mycobacterium tuberculosis</p>

    <p>          Seidel, M, Alderwick, LJ, Sahm, H, Besra, GS, and Eggeling, L</p>

    <p>          Glycobiology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17088267&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17088267&amp;dopt=abstract</a> </p><br />

    <p>3.     59398   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Activity of 6-O-substituted macrolides against Mycobacterium tuberculosis</p>

    <p>          Zhu, Zhaohai, Petukhova, Valentina Z, Pan, Dahua, Wan, Baojie, Wang, Yuehong, Krasnykh, Olga, Yu, Gengli, Schellinger, Kate, and Franzblau, Scott G</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-349</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     59399   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Evaluation of 3-deaza-adenosine analogues as ligands for adenosine kinase and inhibitors of Mycobacterium tuberculosis growth</p>

    <p>          Long, MC, Allan, PW, Luo, MZ, Liu, MC, Sartorelli, AC, and Parker, WB</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17085766&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17085766&amp;dopt=abstract</a> </p><br />

    <p>5.     59400   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Brunsvicamides A-C: sponge-related cyanobacterial peptides with Mycobacterium tuberculosis protein tyrosine phosphatase inhibitory activity</p>

    <p>          Mueller, Daniela, Krick, Anja, Kehraus, Stefan, Mehner, Christian, Hart, Mark, Kuepper, Frithjof C, Saxena, Krishna, Prinz, Heino, Schwalbe, Harald, Janning, Petra, Waldmann, Herbert, and Koenig, Gabriele M</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(16): 4871-4878</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     59401   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Monocytes infected with Mycobacterium tuberculosis regulate MAP kinase-dependent astrocyte MMP-9 secretion</p>

    <p>          Harris, JE, Green, JA, Elkington, PT, and Friedland, JS</p>

    <p>          J Leukoc Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079649&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079649&amp;dopt=abstract</a> </p><br />

    <p>7.     59402   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          SMALL INTERFERING RNA TARGETED TO HEPATITIS C VIRUS 5&#39;NONTRANSLATED REGION EXERTS POTENT ANTIVIRAL EFFECT</p>

    <p>          Kanda, T, Steele, R, Ray, R, and Ray, RB</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079316&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079316&amp;dopt=abstract</a> </p><br />

    <p>8.     59403   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Novel antituberculosis compounds, hirsutellones A, C, and C</p>

    <p>          Isaka, Masahiko, Hywel-Jones, Nigel L, Somrithipol, Sayanh, Kirtikara, Kanyawim, Palittapongarnpim, Prasit, and Thebtaranonth, Yodhathai</p>

    <p>          PATENT:  US <b>2006122252</b>  ISSUE DATE:  20060608</p>

    <p>          APPLICATION: 2005-23378  PP: 21 pp.</p>

    <p>          ASSIGNEE:  (National Center for Genetic Engineering, Thailand and Biotechnology, National Science &amp; Technology Development Agency</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     59404   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Multidrug-resistant tuberculosis management in resource-limited settings</p>

    <p>          Nathanson, E, Lambregts-van, Weezenbeek C, Rich, ML, Gupta, R, Bayona, J, Blondal, K, Caminero, JA, Cegielski, JP, Danilovits, M, Espinal, MA, Hollo, V, Jaramillo, E, Leimane, V, Mitnick, CD, Mukherjee, JS, Nunn, P, Pasechnikov, A, Tupasi, T, Wells, C, and Raviglione, MC</p>

    <p>          Emerg Infect Dis <b>2006</b>.  12(9): 1389-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17073088&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17073088&amp;dopt=abstract</a> </p><br />

    <p>10.   59405   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 4-methylideneisoxazolidin-5-ones - A new class of highly cytotoxic alpha-methylidene-gamma-lactones</p>

    <p>          Rozalski, M, Krajewska, U, Panczyk, M, Mirowski, M, Rozalska, B, Wasek, T, and Janecki, T</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17069934&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17069934&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59406   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          The 5-Deoxy-5-methylthio-xylofuranose Residue in Mycobacterial Lipoarabinomannan. Absolute Stereochemistry, Linkage Position, Conformation, and Immunomodulatory Activity</p>

    <p>          Joe, Maju, Sun, Daniel, Taha, Hashem, Completo, Gladys C, Croudace, Joanne E, Lammas, David A, Besra, Gurdyal S, and Lowary, Todd L</p>

    <p>          Journal of the American Chemical Society <b>2006</b>.  128(15): 5059-5072</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   59407   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          In Vitro Inhibition of Toxoplasma gondii by Four New Derivatives of Artemisinin</p>

    <p>          Jones-Brando, L, D&#39;Angelo, J, Posner, GH, and Yolken, R</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17060514&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17060514&amp;dopt=abstract</a> </p><br />

    <p>13.   59408   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Synthesis, antimycobacterial and antitumor activities of new (1,1-dioxido-3-oxo-1,2-benzisothiazol-2(3H)-yl)methyl N,N-disubstituted dithiocarbamate/O-alkyldithiocarbonate derivatives</p>

    <p>          Guezel, Oezlen and Salman, Aydin</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(23): 7804-7815</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   59409   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Kenyon, James and Trischman, Jacqueline</p>

    <p>          &lt;10 Journal Title&gt; <b>2006</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   59410   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Synthesis and antituberculosis activity of 2-(aryl/alkylamino)-5-(4-aminophenyl)-1,3,4-thiadiazoles and their Schiff bases</p>

    <p>          Solak, Nilufer and Rollas, Sevim</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2006</b>.(12): 173-181</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   59411   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Editorial: The treatment of multi-drug resistant tuberculosis - a return to the pre-antibiotic era?</p>

    <p>          Olle-Goig, JE</p>

    <p>          Trop Med Int Health <b>2006</b>.  11(11): 1625-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17054741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17054741&amp;dopt=abstract</a> </p><br />

    <p>17.   59412   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Structure-activity relationships at the 5-position of thiolactomycin: An intact (5R)-isoprene unit is required for activity against the condensing enzymes from Mycobacterium tuberculosis and Escherichia coli</p>

    <p>          Kim, Pilho, Zhang, Yong-Mei, Shenoy, Gautham, Nguyen, Quynh-Anh, Boshoff, Helena I, Manjunatha, Ujjini H, Goodwin, Michael B, Lonsdale, John, Price, Allen C, Miller, Darcie J, Duncan, Ken, White, Stephen W, Rock, Charles O, Barry, Clifton E III, and Dowd, Cynthia S</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(1): 159-171</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   59413   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Cost of treatment for multidrug-resistant tuberculosis in South Korea</p>

    <p>          Kang, YA, Choi, YJ, Cho, YJ, Lee, SM, Yoo, CG, Kim, YW, Han, SK, Shim, YS, and Yim, JJ</p>

    <p>          Respirology <b>2006</b>.  11(6): 793-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17052310&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17052310&amp;dopt=abstract</a> </p><br />

    <p>19.   59414   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          siRNA-resistance in treated HCV replicon cells is correlated with the development of specific HCV mutations</p>

    <p>          Konishi, M, Wu, CH, Kaito, M, Hayashi, K, Watanabe, S, Adachi, Y, and Wu, GY</p>

    <p>          J Viral Hepat <b>2006</b>.  13(11): 756-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17052275&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17052275&amp;dopt=abstract</a> </p><br />

    <p>20.   59415   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Design and synthesis of a novel nanomolar inhibitor of Toxoplasma gondii dihydrofolate reductase, its salt form and analogs</p>

    <p>          Gangjee, Aleem, Lin, Xin, Biondo, Lisa R, and Queener, Sherry F</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-363</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   59416   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Rifampicin plus pyrazinamide versus isoniazid for treating latent tuberculosis infection: a meta-analysis</p>

    <p>          Gao, XF, Wang, L, Liu, GJ, Wen, J, Sun, X, Xie, Y, and Li, YP</p>

    <p>          Int J Tuberc Lung Dis <b>2006</b>.  10(10): 1080-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17044199&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17044199&amp;dopt=abstract</a> </p><br />

    <p>22.   59417   OI-LS-361; PUBMED-OI-11/13/2006</p>

    <p class="memofmt1-2">          Antituberculosis drug resistance in the south of Vietnam: prevalence and trends</p>

    <p>          Huong, NT, Lan, NT, Cobelens, FG, Duong, BD, Co, NV, Bosman, MC, Kim, SJ, van, Soolingen D, and Borgdorff, MW</p>

    <p>          J Infect Dis <b>2006</b>.  194(9): 1226-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17041848&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17041848&amp;dopt=abstract</a> </p><br />

    <p>23.   59418   OI-LS-361; WOS-OI-11/6/2006</p>

    <p class="memofmt1-2">          What is Cryptosporidium? Reappraising its biology and phylogenetic affinities</p>

    <p>          Barta, JR and Thompson, RCA</p>

    <p>          TRENDS IN PARASITOLOGY <b>2006</b>.  22(10): 463-468, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241254500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241254500007</a> </p><br />

    <p>24.   59419   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          In vitro susceptibilities of Malaysian clinical isolates of Cryptococcus neoformans var. grubii and Cryptococcus gattii to five antifungal drugs</p>

    <p>          Tay, ST, Haryanty, TTanty, Ng, KP, Rohani, MY, and Hamimah, H</p>

    <p>          Mycoses <b>2006</b>.  49(4): 324-330</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   59420   OI-LS-361; WOS-OI-11/6/2006</p>

    <p class="memofmt1-2">          Antiprotozoal and antimicrobial activities of Centaurea species growing in Turkey</p>

    <p>          Karamenderes, C, Khan, S, Tekwani, BL, Jacob, MR, and Khan, IA</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2006</b>.  44(7): 534-539, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241171700008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241171700008</a> </p><br />

    <p>26.   59421   OI-LS-361; SCIFINDER-OI-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of 2-thienylacetylenes as antiviral agents</p>

    <p>          Wunberg, Tobias, Baumeister, Judith, Gottschling, Dirk, Henninger, Kerstin, Koletzki, Diana, Pernerstorfer, Josef, Urban, Andreas, Birkmann, Alexander, Harrenga, Axel, and Lobell, Mario</p>

    <p>          PATENT:  WO <b>2006072348</b>  ISSUE DATE:  20060713</p>

    <p>          APPLICATION: 2005  PP: 83 pp.</p>

    <p>          ASSIGNEE:  (Aicuris Gmbh &amp; Co. KG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   59422   OI-LS-361; WOS-OI-11/6/2006</p>

    <p class="memofmt1-2">          Preparation of His-tagged armored RNA phage particles as a control for real-time reverse transcription-PCR detection of severe acute respiratory syndrome coronavirus</p>

    <p>          Cheng, YJ, Niu, JJ, Zhang, YY, Huang, JW, and Li, QG</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2006</b>.  44(10): 3557-3561, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241138800015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241138800015</a> </p><br />

    <p>28.   59423   OI-LS-361; WOS-OI-11/6/2006</p>

    <p class="memofmt1-2">          Evaluation of molecular-beacon, TaqMan, and fluorescence resonance energy transfer probes for detection of antibiotic resistance-conferring single nucleotide Polymorphisms in mixed Mycobacterium tuberculosis DNA extracts</p>

    <p>          Yesilkaya, H, Meacci, F, Niemann, S, Hillemann, D, Rusch-Gerdes, S, Barer, MR, Andrew, PW, and Oggioni, MR</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2006</b>.  44(10): 3826-3829, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241138800060">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241138800060</a> </p><br />

    <p>29.   59424   OI-LS-361; WOS-OI-11/6/2006</p>

    <p class="memofmt1-2">          A new antifungal macrolide, eushearilide, isolated from Eupenicillium shearii</p>

    <p>          Hosoe, T, Fukushima, K, Takizawa, K, Itabashi, T, Kawahara, N, Vidotto, V, and Kawai, KI</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2006</b>.  59(9): 597-600, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241312900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241312900008</a> </p><br />

    <p>30.   59425   OI-LS-361; WOS-OI-11/6/2006</p>

    <p class="memofmt1-2">          Biochemical and functional characterization of triosephosphate isomerase from Mycobacterium tuberculosis H37Rv</p>

    <p>          Mathur, D, Malik, G, and Garg, LC</p>

    <p>          FEMS MICROBIOLOGY LETTERS <b>2006</b>.  263(2): 229-235, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241383100016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241383100016</a> </p><br />

    <p>31.   59426   OI-LS-361; WOS-OI-11/6/2006</p>

    <p><b>          Design, synthesis, and antifungal activities in vitro of novel tetrahydroisoquinoline compounds based on the structure of lanosterol 14 alpha-demethylase (CYP51) of fungi</b> </p>

    <p>          Zhu, J, Lu, JG, Zhou, YJ, Li, YW, Cheng, J, and Zheng, CH</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(20): 5285-5289, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241344400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241344400006</a> </p><br />

    <p>32.   59427   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Experimental study of the effects of probiotics on Cryptosporidium parvum infection in neonatal rats</p>

    <p>          Guitard, J, Menotti, J, Desveaux, A, Alimardani, P, Porcher, R, Derouin, F, and Kapel, N</p>

    <p>          PARASITOLOGY RESEARCH <b>2006</b>.  99(5): 522-527, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241385100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241385100002</a> </p><br />

    <p>33.   59428   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Are second-line drugs necessary to control mulfidrug-resistant tuberculosis?</p>

    <p>          Nardell, EA and Mitnick, CD</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2006</b>.  194(9): 1194-1196, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241418100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241418100002</a> </p><br />

    <p>34.   59429   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Mucoralactone A: An unusual steroid from the liquid culture of Mucor plumbeus</p>

    <p>          Ata, A, Conci, LJ, and Orhan, I</p>

    <p>          HETEROCYCLES <b>2006</b>.  68(10): 2097-+, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241420800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241420800008</a> </p><br />

    <p>35.   59430   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Current status of some antituberculosis drugs and the development of new antituberculous agents with special reference to their in vitro and in vivo antimicrobial activities</p>

    <p>          Tomioka, H</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2006</b>.  12(31): 4047-4070, 24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241488300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241488300006</a> </p><br />

    <p>36.   59431   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Bioactive constituents of the root bark of Artocarpus rigidus subsp rigidus</p>

    <p>          Namdaung, U, Aroonrerk, N, Suksamrarn, S, Danwisetkanjana, K, Saenboonrueng, J, Arjchomphu, W, and Suksamrarn, A</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2006</b>.  54(10): 1433-1436, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241549300015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241549300015</a> </p><br />

    <p>37.   59432   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Sulfate metabolism in mycobacteria</p>

    <p>          Schelle, MW and Bertozzi, CR</p>

    <p>          CHEMBIOCHEM <b>2006</b>.  7(10): 1516-1524, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241392400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241392400005</a> </p><br />
    <br clear="all">

    <p>38.   59433   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Syntheses and studies of quinolone-cephalosporins as potential anti-tuberculosis agents</p>

    <p>          Zhao, GY, Miller, MJ, Franzblau, S, Wan, BJ, and Mollmann, U</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(21): 5534-5537, 4</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241447300011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241447300011</a> </p><br />

    <p>39.   59434   OI-LS-361; WOS-OI-11/13/2006</p>

    <p class="memofmt1-2">          Now, later or never? Challenges associated with hepatitis C treatment</p>

    <p>          McNally, S and Temple-Smith, M</p>

    <p>          AUSTRALIAN AND NEW ZEALAND JOURNAL OF PUBLIC HEALTH <b>2006</b>.  30(5): 422-427, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241440200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241440200006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
