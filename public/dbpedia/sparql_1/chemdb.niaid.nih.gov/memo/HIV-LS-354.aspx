

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-354.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q/8GxphL98hamvUG6np7tyNH2pOYK+R9gBC1npblly0+vtjmiNxDCT+DFK16nr526UtwSC/W9XTRtsV/TJDeb068a8JHmf+U++Jezl14+mnueSWXTibqfbjfdJ4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="41DFE3F4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-354-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49240   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          New developments in natural products-based anti-AIDS research</p>

    <p>          Yu, D, Morris-Natschke, SL, and Lee, KH</p>

    <p>          Med Res Rev <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16888749&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16888749&amp;dopt=abstract</a> </p><br />

    <p>2.     49241   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Full Validation of an Analytical Method for the HIV-Protease Inhibitor Atazanavir in Combination With 8 Other Antiretroviral Agents and its Applicability to Therapeutic Drug Monitoring</p>

    <p>          Rezk, NL, Crutchley, RD, Yeh, RF, and Kashuba, AD</p>

    <p>          Ther Drug Monit <b>2006</b>.  28(4): 517-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16885719&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16885719&amp;dopt=abstract</a> </p><br />

    <p>3.     49242   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Alkoxyalkyl esters of (s)-9-[3-hydroxy-2-(phosphonomethoxy)propyl]adenine are potent inhibitors of the replication of wild-type and drug-resistant human immunodeficiency virus type 1 in vitro</p>

    <p>          Hostetler, KY, Aldern, KA, Wan, WB, Ciesla, SL, and Beadle, JR</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(8): 2857-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870786&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870786&amp;dopt=abstract</a> </p><br />

    <p>4.     49243   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Novel nonnucleoside inhibitors that select nucleoside inhibitor resistance mutations in human immunodeficiency virus type 1 reverse transcriptase</p>

    <p>          Zhang, Z, Walker, M, Xu, W, Shim, JH, Girardet, JL, Hamatake, RK, and Hong, Z</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(8): 2772-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870771&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870771&amp;dopt=abstract</a> </p><br />

    <p>5.     49244   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Antiviral acyclic nucleoside phosphonates structure activity studies</p>

    <p>          Holy, A</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16857275&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16857275&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     49245   HIV-LS-354; EMBASE-HIV-8/7/2006</p>

    <p><b>          Non-infectious fluorimetric assay for phenotyping of drug-resistant HIV proteinase mutants</b> </p>

    <p>          Majerova-Uhlikova, Tat&#39;ana, Dantuma, Nico P, Lindsten, Kristina, Masucci, Maria G, and Konvalinka, Jan</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36(1): 50-59</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4JF997D-1/2/89c8dc65c97d977b92f58a4387dcf50a">http://www.sciencedirect.com/science/article/B6VJV-4JF997D-1/2/89c8dc65c97d977b92f58a4387dcf50a</a> </p><br />

    <p>7.     49246   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Molecular tongs containing amino acid mimetic fragments: new inhibitors of wild-type and mutated HIV-1 protease dimerization</p>

    <p>          Bannwarth, L, Kessler, A, Pethe, S, Collinet, B, Merabet, N, Boggetto, N, Sicsic, S, Reboud-Ravaux, M, and Ongeri, S</p>

    <p>          J Med Chem <b>2006</b>.  49(15): 4657-64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854071&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854071&amp;dopt=abstract</a> </p><br />

    <p>8.     49247   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Sequence-based design and discovery of peptide inhibitors of HIV-1 integrase: insight into the binding mode of the enzyme</p>

    <p>          Li, HY, Zawahir, Z, Song, LD, Long, YQ, and Neamati, N</p>

    <p>          J Med Chem <b>2006</b>.  49(15): 4477-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854053&amp;dopt=abstract</a> </p><br />

    <p>9.     49248   HIV-LS-354; PUBMED-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Antiretroviral potential of human tripartite motif-5 and related proteins</p>

    <p>          Zhang, F, Hatziioannou, T, Perez-Caballero, D, Derse, D, and Bieniasz, PD</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828831&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828831&amp;dopt=abstract</a> </p><br />

    <p>10.   49249   HIV-LS-354; WOS-HIV-7/30/2006</p>

    <p class="memofmt1-2">          Production of a short recombinant C4V3HIV-1 immunogen that induces strong anti-HIV responses by systemic and mucosal routes without the need of adjuvants</p>

    <p>          Varona-Santos, JT, Vazquez-Padron, RI, and Moreno-Fierros, L</p>

    <p>          VIRAL IMMUNOLOGY <b>2006</b>.  19(2): 237-249, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238851500013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238851500013</a> </p><br />
    <br clear="all">

    <p>11.   49250   HIV-LS-354; EMBASE-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Regiocontrolled synthesis and HIV inhibitory activity of unsymmetrical binaphthoquinone and trimeric naphthoquinone derivatives of conocurvone</p>

    <p>          Stagliano, Kenneth W, Emadi, Ashkan, Lu, Zhenhai, Malinakova, Helena C, Twenter, Barry, Yu, Min, Holland, Louis E, Rom, Amanda M, Harwood, John S, and Amin, Ronak</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(16): 5651-5665</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4K427P4-1/2/6595913c61e93147a7044284c12f3806">http://www.sciencedirect.com/science/article/B6TF8-4K427P4-1/2/6595913c61e93147a7044284c12f3806</a> </p><br />

    <p>12.   49251   HIV-LS-354; EMBASE-HIV-8/7/2006</p>

    <p class="memofmt1-2">          Synthesis, X-ray crystal structural study, antiviral and cytostatic evaluations of the novel unsaturated acyclic and epoxide nucleoside analogues</p>

    <p>          Kristafor, Vedran, Raic-Malic, Silvana, Cetina, Mario, Kralj, Marijeta, Suman, Lidija, Pavelic, Kresimir, Balzarini, Jan, De Clercq, Erik, and Mintas, Mladen</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4KJDWSS-7/2/7d1d7556f5c1c206a29194ef5b307822">http://www.sciencedirect.com/science/article/B6TF8-4KJDWSS-7/2/7d1d7556f5c1c206a29194ef5b307822</a> </p><br />

    <p>13.   49252   HIV-LS-354; WOS-HIV-7/30/2006</p>

    <p class="memofmt1-2">          HIV-1 protease inhibitor ritonavir potentiates the effect of 1,25-dihydroxyvitamin D-3 to induce growth arrest and differentiation of human myeloid leukemia cells via down-regulation of CYP24</p>

    <p>          Ikezoe, T, Bandobashi, K, Yang, Y, Takeuchi, E, Sekiguchi, N, Sakai, S, Koeffler, HP, and Taguchi, H</p>

    <p>          LEUKEMIA RESEARCH <b>2006</b>.  30(8): 1005-1011, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238936600014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238936600014</a> </p><br />

    <p>14.   49253   HIV-LS-354; WOS-HIV-7/30/2006</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial and anti-HIV activity of some novel benzenesulfonamides bearing 2,5-disubstituted-1,3,4-oxadiazole moiety</p>

    <p>          Iqbal, R, Zareef, M, Ahmed, S, Zaidi, JH, Arfan, M, Shafique, M, and Al-Masoudi, NA</p>

    <p>          JOURNAL OF THE CHINESE CHEMICAL SOCIETY <b>2006</b>.  53(3): 689-696, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238874800024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238874800024</a> </p><br />

    <p>15.   49254   HIV-LS-354; WOS-HIV-7/30/2006</p>

    <p class="memofmt1-2">          From ligand to complexes: Inhibition of human immunodeficiency virus type 1 integrase by beta-diketo acid metal complexes</p>

    <p>          Sechi, M, Bacchi, A, Carcelli, M, Compari, C, Duce, E, Fisicaro, E, Rogolino, D, Gates, P, Derudas, M, Al-Mawsawi, LQ, and Neamati, N</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(14): 4248-4260, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238805800027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238805800027</a> </p><br />

    <p>16.   49255   HIV-LS-354; WOS-HIV-7/30/2006</p>

    <p class="memofmt1-2">          Three novel terpenoids from Schisandra pubescens var. pubinervis</p>

    <p>          Huang, SX, Yang, J, Xiao, WL, Zhu, YL, Li, RT, Li, LM, Pu, JX, Li, X, Li, SH, and Sun, HD</p>

    <p>          HELVETICA CHIMICA ACTA <b>2006</b>.  89(6): 1169-1175, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238858000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238858000009</a> </p><br />
    <br clear="all">

    <p>17.   49256   HIV-LS-354; WOS-HIV-8/6/2006</p>

    <p class="memofmt1-2">          Creation of low toxic reverse-transcriptase inhibitory nucleosides that prevent the emergence of drug-resistant HIV variants</p>

    <p>          Ohrui, H, Hayakawa, H, Kohgo, S, Matsuoka, M, Kodama, E, and Mitsuya, H</p>

    <p>          JOURNAL OF SYNTHETIC ORGANIC CHEMISTRY JAPAN <b>2006</b>.  64(7): 716-723, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239108100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239108100002</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
