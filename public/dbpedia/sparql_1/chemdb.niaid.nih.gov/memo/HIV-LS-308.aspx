

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-308.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fymOxcL26BH5Gs363uo0ZKwtDzQj2bMr1p3DeXsBJTOK4kVtRC1Fn2DYCG+jsr8kFyi7lYyXuaU+wmgB7Nh2aijm6520W5QRiYb7hEUm/6doM85zLxM3caXVyXw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FE0D9D1F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-308-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44289   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          MECHANISM-BASED INACTIVATION OF CYTOCHROME P450 3A (CYP3A) BY HIV PROTEASE INHIBITORS</p>

    <p>          Ernest, Ii CS, Hall, SD, and Jones, DR</p>

    <p>          J Pharmacol Exp Ther <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15523003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15523003&amp;dopt=abstract</a> </p><br />

    <p>2.         44290   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          Studies of nonnucleoside HIV-1 reverse transcriptase inhibitors. Part 1: Design and synthesis of thiazolidenebenzenesulfonamides</p>

    <p>          Masuda, N, Yamamoto, O, Fujii, M, Ohgami, T, Fujiyasu, J, Kontani, T, Moritomo, A, Orita, M, Kurihara, H, Koga, H, Nakahara, H, Kageyama, S, Ohta, M, Inoue, H, Hatta, T, Suzuki, H, Sudo, K, Shimizu, Y, Kodama, E, Matsuoka, M, Fujiwara, M, Yokota, T, Shigeta, S, and Baba, M</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(23): 6171-82</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519161&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519161&amp;dopt=abstract</a> </p><br />

    <p>3.         44291   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          In Vitro Combination of Amdoxovir and the Inosine Monophosphate Dehydrogenase Inhibitors Mycophenolic Acid and Ribavirin Demonstrates Potent Activity against Wild-Type and Drug-Resistant Variants of Human Immunodeficiency Virus Type 1</p>

    <p>          Borroto-Esoda, K, Myrick, F, Feng, J, Jeffrey, J, and Furman, P</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(11): 4387-94</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504868&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504868&amp;dopt=abstract</a> </p><br />

    <p>4.         44292   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          N-Substituted Pyrrole Derivatives as Novel Human Immunodeficiency Virus Type 1 Entry Inhibitors That Interfere with the gp41 Six-Helix Bundle Formation and Block Virus Fusion</p>

    <p>          Jiang, S, Lu, H, Liu, S, Zhao, Q, He, Y, and Debnath, AK</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(11): 4349-59</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504864&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504864&amp;dopt=abstract</a> </p><br />

    <p>5.         44293   HIV-LS-308; EMBASE-HIV-11/4/2004</p>

    <p class="memofmt1-2">          In vitro synthesis of enzymatically active HIV-1 protease for rapid phenotypic resistance profiling</p>

    <p>          Hoffmann, Dieter, Buchberger, Bernd, and Nemetz, Cordula</p>

    <p>          Journal of Clinical Virology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4DJ4W65-1/2/84ba6fb2c7b5b20c71dd1c43532e39a7">http://www.sciencedirect.com/science/article/B6VJV-4DJ4W65-1/2/84ba6fb2c7b5b20c71dd1c43532e39a7</a> </p><br />
    <br clear="all">

    <p>6.         44294   HIV-LS-308; EMBASE-HIV-11/4/2004</p>

    <p class="memofmt1-2">          Adaptive inhibitors of the HIV-1 protease</p>

    <p>          Ohtaka, Hiroyasu and Freire, Ernesto</p>

    <p>          Progress in Biophysics and Molecular Biology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TBN-4DH29MG-1/2/00541d0de5226a8c62ac0544acd4e865">http://www.sciencedirect.com/science/article/B6TBN-4DH29MG-1/2/00541d0de5226a8c62ac0544acd4e865</a> </p><br />

    <p>7.         44295   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          A mechanistic study of 3-aminoindazole cyclic urea HIV-1 protease inhibitors using comparative QSAR</p>

    <p>          Garg, R and Bhhatarai, B</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(22): 5819-31</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498658&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498658&amp;dopt=abstract</a> </p><br />

    <p>8.         44296   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          A structural basis for the acute effects of HIV protease inhibitors on GLUT4 intrinsic activity</p>

    <p>          Hertel, J, Struthers, H, Baird, Horj C, and Hruz, PW</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15496402&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15496402&amp;dopt=abstract</a> </p><br />

    <p>9.         44297   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          The impact of the M184V substitution on drug resistance and viral fitness</p>

    <p>          Wainberg, MA</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(1): 147-51</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482179&amp;dopt=abstract</a> </p><br />

    <p>10.       44298   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          Atazanavir: improving the HIV protease inhibitor class</p>

    <p>          Becker, S</p>

    <p>          Expert Rev Anti Infect Ther <b>2003</b>.  1(3): 403-13</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482137&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482137&amp;dopt=abstract</a> </p><br />

    <p>11.       44299   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          The next generation of HIV/AIDS drugs: novel and developmental antiHIV drugs and targets</p>

    <p>          Turpin, JA</p>

    <p>          Expert Rev Anti Infect Ther <b>2003</b>.  1(1): 97-128</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482105&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482105&amp;dopt=abstract</a> </p><br />

    <p>12.       44300   HIV-LS-308; EMBASE-HIV-11/4/2004</p>

    <p class="memofmt1-2">          Effects of pravastatin on lipoproteins and endothelial function in patients receiving human immunodeficiency virus protease inhibitors</p>

    <p>          Stein, James H, Merwood, Michelle A, Bellehumeur, Jennifer L, Aeschlimann, Susan E, Korcarz, Claudia E, Underbakke, Gail L, Mays, Maureen E, and Sosman, James M</p>

    <p>          American Heart Journal <b>2004</b>.  147(4): 713</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W9H-4C40S52-1H/2/8f6cec51a0f5ba401b331a70af6bd239">http://www.sciencedirect.com/science/article/B6W9H-4C40S52-1H/2/8f6cec51a0f5ba401b331a70af6bd239</a> </p><br />

    <p>13.       44301   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          Structural and thermodynamic basis for the binding of TMC114, a next-generation human immunodeficiency virus type 1 protease inhibitor</p>

    <p>          King, NM, Prabu-Jeyabalan, M, Nalivaika, EA, Wigerinck, P, de, Bethune MP, and Schiffer, CA</p>

    <p>          J Virol <b>2004</b>.  78(21): 12012-21</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479840&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479840&amp;dopt=abstract</a> </p><br />

    <p>14.       44302   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          Identification by phage display selection of a short peptide able to inhibit only the strand transfer reaction catalyzed by human immunodeficiency virus type 1 integrase</p>

    <p>          Desjobert, C,  de Soultrait, VR, Faure, A, Parissi, V, Litvak, S, Tarrago-Litvak, L, and Fournier, M</p>

    <p>          Biochemistry <b>2004</b>.  43(41): 13097-13105</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476403&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476403&amp;dopt=abstract</a> </p><br />

    <p>15.       44303   HIV-LS-308; PUBMED-HIV-11/4/2004</p>

    <p class="memofmt1-2">          Purification of a novel ribonuclease from dried fruiting bodies of the edible wild mushroom Thelephora ganbajun</p>

    <p>          Wang, HX and Ng, TB</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  324(2): 855-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474506&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474506&amp;dopt=abstract</a> </p><br />

    <p>16.       44304   HIV-LS-308; WOS-HIV-10/25/2004</p>

    <p class="memofmt1-2">          Ring finger protein ZIN interacts with human immunodeficiency virus type 1 Vif</p>

    <p>          Feng, F, Davis, A, Lake, JA, Carr, J, Xia, W, Burrell, C, and Li, P</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(19): 10574-10581, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223964300039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223964300039</a> </p><br />

    <p>17.       44305   HIV-LS-308; WOS-HIV-10/25/2004</p>

    <p class="memofmt1-2">          GW-873140 - Anti-HIV therapy HIV entry inhibitor CCR5 antagonist</p>

    <p>          McIntyre, JA</p>

    <p>          DRUGS OF THE FUTURE <b>2004</b>.  29(7): 677-679, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223948800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223948800002</a> </p><br />

    <p>18.       44306   HIV-LS-308; WOS-HIV-10/25/2004</p>

    <p class="memofmt1-2">          Effective inhibition of HIV-1 replication in cultured cells by external guide sequences and ribonuclease P</p>

    <p>          Barnor, JS, Endo, Y, Habu, Y, Miyano-Kurosaki, N, Kitano, M, Yamamoto, H, and Takaku, H</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2004</b>.  14(19): 4941-4944, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224013000027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224013000027</a> </p><br />

    <p>19.       44307   HIV-LS-308; WOS-HIV-10/25/2004</p>

    <p class="memofmt1-2">          Synthesis of enantiomerically pure D-FDOC, an anti-HIV agent</p>

    <p>          Mao, SL, Bouygues, M, Welch, C, Biba, M, Chilenski, J, Schinazi, RF, and Liotta, DC</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2004</b>.  14(19): 4991-4994, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224013000037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224013000037</a> </p><br />

    <p>20.       44308   HIV-LS-308; WOS-HIV-10/31/2004</p>

    <p class="memofmt1-2">          Effect of a CCR5 inhibitor on viral loads in macaques dual-infected with R5 and X4 primate immunodeficiency viruses</p>

    <p>          Wolinsky, SM, Veazey, RS, Kunstman, KJ, Klasse, PJ, Dufour, J, Marozsan, AJ, Springer, MS, and Moore, JP</p>

    <p>          VIROLOGY <b>2004</b>.  328(1): 19-29, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224117300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224117300003</a> </p><br />

    <p>21.       44309   HIV-LS-308; WOS-HIV-10/31/2004</p>

    <p class="memofmt1-2">          Efficiency of a second-generation HIV-1 protease inhibitor studied by molecular dynamics and absolute binding free energy calculations</p>

    <p>          Lepsik, M, Kriz, Z, and Havlas, Z</p>

    <p>          PROTEINS-STRUCTURE FUNCTION AND BIOINFORMATICS <b>2004</b>.  57(2 ): 279-293, 15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223926200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223926200005</a> </p><br />

    <p>22.       44310   HIV-LS-308; WOS-HIV-10/31/2004</p>

    <p class="memofmt1-2">          Determination of the new HIV-protease inhibitor atazanavir by liquid chromatography after solid-phase extraction</p>

    <p>          Colombo, S, Guignard, N, Marzolini, C, Telenti, A, Biollaz, J, and Decosterd, LA</p>

    <p>          JOURNAL OF CHROMATOGRAPHY B-ANALYTICAL TECHNOLOGIES IN THE BIOMEDICAL AND LIFE SCIENCES <b>2004</b>.  810(1): 25-34, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224010000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224010000004</a> </p><br />

    <p>23.       44311   HIV-LS-308; WOS-HIV-10/31/2004</p>

    <p class="memofmt1-2">          The role of the cyclic peptide backbone in the anti-HIV activity of the cyclotide kalata B1</p>

    <p>          Daly, NL, Gustafson, KR, and Craik, DJ</p>

    <p>          FEBS LETTERS <b>2004</b>.  574(1-3): 69-72, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224041600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224041600012</a> </p><br />
    <br clear="all">

    <p>24.       44312   HIV-LS-308; WOS-HIV-10/31/2004</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial, and anti-HIV-1 activity of certain 5-(I-adamantyl)-2-substituted thio-1,3,4-oxadiazoles and 5-(1-adamantyl)-3-substituted aminomethyl-1,3,4-oxadiazoline-2-thiones</p>

    <p>          El-Emam, AA, Al-Deeb, OA, Al-Omar, M, and Lehmann, J</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(19): 5107-5113, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224014000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224014000009</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
