

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-09-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XS4qoxkihpYSMexCV3bPHOb23WQwrtgBbHN2JnCNZnYC1ewWk7/Ts1XfUCkIbuUCeLAZbIFgf4Oqv28HIip/QM5K/6JUR06gNOckcHTIhhObBR7A44jvsMsvEcs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2A948F64" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List:</p>

    <p class="memofmt2-h1">September 3 - September 16, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20838466">A novel small molecule inhibitor of hepatitis C virus entry.</a> Baldick CJ, Wichroski MJ, Pendri A, Walsh AW, Fang J, Mazzucco CE, Pokornowski KA, Rose RE, Eggers BJ, Hsu M, Zhai W, Zhai G, Gerritz SW, Poss MA, Meanwell NA, Cockett MI, Tenney DJ. PLoS Pathogens. 2010; 6(9). PMID[20838466]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0903-091610.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20823284">Preclinical Characterization of BI 201335, a C-terminal Carboxylic Acid Inhibitor of the Hepatitis C Virus NS3-NS4A Protease.</a> White PW, Llinàs-Brunet M, Amad M, Bethell RC, Bolger G, Cordingley MG, Duan J, Garneau M, Lagacé L, Thibeault D, Kukolj G. Antimicrobial Agents &amp; Chemotherapy. 2010. <b>[Epub ahead of print]</b>. PMID[20823284]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0903-091610.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20823220">Rigid amphipathic fusion inhibitors, small molecule antiviral compounds against enveloped viruses.</a></p>

    <p class="NoSpacing">St Vincent MR, Colpitts CC, Ustinov AV, Muqadas M, Joyce MA, Barsby NL, Epand RF, Epand RM, Khramyshev SA, Valueva OA, Korshun VA, Tyrrell DL, Schang LM. Proceedings of the National Academy of Science U S A. 2010. <b>[Epub ahead of print]</b>. PMID[20823220]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0903-091610.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20813419">Iron inhibits replication of infectious hepatitis C virus in permissive Huh7.5.1 cells.</a> Fillebeen C, Pantopoulos K. Journal of Hepatology. 2010. <b>[Epub ahead of print]</b>. PMID[20813419]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0903-091610.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20812360">Hepatitis C virus nonstructural protein 5A inhibitors: novel target-now for new trials and new treatment strategies.</a> Thompson AJ, Clark PJ, McHutchison JG. Hepatology. 2010. 52(3):1162-4. PMID[20812360]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0903-091610.</p>

    <p class="memofmt2-2">Herpes</p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20809641">Synthesis of Ester Prodrugs of 9-(S)-[3-Hydroxy-2-(phosphonomethoxy)propyl]-2,6-diaminopurine (HPMPDAP) as Anti-Poxvirus Agents.</a> Krec&#780;merova&#769; M, Holy&#769; A, Andrei G, Pomeisl K, Tichy&#769; T, Br&#780;ehova&#769; P, Masoji&#769;dkova&#769; M, Drac&#780;i&#769;nsky&#769; M, Pohl R, Laflamme G, Naesens L, Hui H, Cihlar T, Neyts J, De Clercq E, Balzarini J, Snoeck R. Journal of Medicinal Chemistry. 2010 Sep 1. <b>[Epub ahead of print]</b>. PMID[20809641]</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0903-091610.</p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.I.</p>

    <p class="NoSpacing">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278428000204%3e">Potent In Vivo Inhibition of Hepatitis C Virus Replication by Mycophenolic Acid in Mice.</a></p>

    <p class="NoSpacing">Pan, Qiuwei, de Ruiter, Petra, Metselaar, Herold J., Kwekkeboom, Jaap, Kazemier, Geert, Tilanus, Hugo W., Janssen, Harry L. A., van der Ulan, Luc J. W. Liver Transplantation. 16(6):2010. S131.</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0903-091610.</p>

    <p></p>

    <p class="NoSpacing">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278565100387">New details of inhibition of HCV helicase by lead compounds derived from benzotriazole.</a> Polkowska-Nowakowska, A., Gozdek, A., Zhukov, I., Najda, A., Boguszewska-Chachulska, A., Bretner, M., Poznanski, J. FEBS Journal. 277:2010. 108-9.</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0903-091610.</p>

    <p></p>

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281247000050">Design and synthesis of HCV agents with sequential triple inhibitory potentials.</a> Zhu, Tianmin, Fawzi, Mahdi B., Flint, Michael, Kong, Fangming, Szeliga, Jan, Tsao, Russ, Howe, Anita Y. M., Pan, Weitao. Bioorganic &amp; Medicinal Chemistry Letters. 20(17):2010. 5212-6.</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0903-091610.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
