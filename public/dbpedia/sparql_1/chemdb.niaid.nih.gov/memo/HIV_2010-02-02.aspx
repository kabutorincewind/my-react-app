

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-02-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Qq02DJiHjCynfmoSWbkJGJ5A380MnHTXJ7CDYnQCXuD5F0AAfr6dOmy/sVvQF0hXcXddtYX97FOSwAz1a2Ty+M6dbsmSg09Zta6wYuvuokAAbCxW7MISB+LDvfs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2FADDAAC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 20 - February 2, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20119629?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">The anti-viral protein of trichosanthin penetrates into human immunodeficiency virus type 1.</a>  Zhao W, Feng D, Sun S, Han T, Sui S.  Acta Biochim Biophys Sin (Shanghai). 2010 Feb;42(2):91-7.PMID: 20119629</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20112915?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">5-Modified-2&#39;-dU and 2&#39;-dC as Mutagenic Anti HIV-1 Proliferation Agents: Synthesis and Activity.</a>  El Safadi Y, Paillart JC, Laumond G, Aubertin AM, Burger A, Marquet R, Vivet-Boudou V.  J Med Chem. 2010 Jan 29. [Epub ahead of print]PMID: 20112915</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20112182?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">Anti-HIV Activities of the Compounds Isolated from Polygonum cuspidatum and Polygonum multiflorum.</a>  Lin HW, Sun MX, Wang YH, Yang LM, Yang YR, Huang N, Xuan LJ, Xu YM, Bai DL, Zheng YT, Xiao K.  Planta Med. 2010 Jan 28. [Epub ahead of print]PMID: 20112182</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20112173?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">IDX-899, an aryl phosphinate-indole non-nucleoside reverse transcriptase inhibitor for the potential treatment of HIV infection.</a>  Klibanov OM, Kaczor RL.  Curr Opin Investig Drugs. 2010 Feb;11(2):237-45.PMID: 20112173</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20112170?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">GSK-1349572, a novel integrase inhibitor for the treatment of HIV infection.</a>  Vandeckerckhove L.  Curr Opin Investig Drugs. 2010 Feb;11(2):203-12.PMID: 20112170</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20108932?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">Synthesis of New Thienyl Ring Containing HIV-1 Protease Inhibitors: Promising Preliminary Pharmacological Evaluation against Recombinant HIV-1 Proteases ( section sign).</a>  Bonini C, Chiummiento L, De Bonis M, Di Blasio N, Funicello M, Lupattelli P, Pandolfo R, Tramutola F, Berti F.  J Med Chem. 2010 Jan 28. [Epub ahead of print]PMID: 20108932</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20095617?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">A Lectin with Anti-HIV-1 Reverse Transcriptase, Antitumor, and Nitric Oxide Inducing Activities from Seeds of Phaseolus vulgaris cv. Extralong Autumn Purple Bean.</a>  Fang EF, Lin P, Wong JH, Tsao SW, Ng TB.  J Agric Food Chem. 2010 Jan 22. [Epub ahead of print]PMID: 20095617</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20089805?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=8">CANNABINOID INHIBITION OF MACROPHAGE MIGRATION TO THE TAT PROTEIN OF HIV-1 IS LINKED TO THE CB2 CANNABINOID RECEPTOR.</a>  Raborn ES, Cabral GA.  J Pharmacol Exp Ther. 2010 Jan 20. [Epub ahead of print]PMID: 20089805</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20088757?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=9">TSG101: A Novel Anti-HIV-1 Drug Target.</a>  Chen H, Liu X, Li Z, Zhan P, De Clercq E.  Curr Med Chem. 2010 Jan 21. [Epub ahead of print]PMID: 20088757</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19995924?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=12">Antiviral efficacy of the novel compound BIT225 against HIV-1 release from human macrophages.</a>  Khoury G, Ewart G, Luscombe C, Miller M, Wilkinson J.  Antimicrob Agents Chemother. 2010 Feb;54(2):835-45. Epub 2009 Dec 7.PMID: 19995924</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19969396?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=13">Synthesis and antiviral activity of boranophosphonate isosteres of AZT and d4T monophosphates.</a>  Barral K, Priet S, De Michelis C, Sire J, Neyts J, Balzarini J, Canard B, Alvarez K.  Eur J Med Chem. 2010 Feb;45(2):849-856. Epub 2009 Nov 12.PMID: 19969396</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19933797?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=17">TMC278, a next-generation nonnucleoside reverse transcriptase inhibitor (NNRTI), active against wild-type and NNRTI-resistant HIV-1.</a>  Azijn H, Tirry I, Vingerhoets J, de Béthune MP, Kraus G, Boven K, Jochmans D, Van Craenenbroeck E, Picchio G, Rimsky LT.  Antimicrob Agents Chemother. 2010 Feb;54(2):718-27. Epub 2009 Nov 23.PMID: 19933797</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19900481?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=18">Lignan, sesquilignans and dilignans, novel HIV-1 protease and cytopathic effect inhibitors purified from the rhizomes of Saururus chinensis.</a>  Lee J, Huh MS, Kim YC, Hattori M, Otake T.  Antiviral Res. 2010 Feb;85(2):425-428. Epub 2009 Nov 10.PMID: 19900481</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273546600007">Structural and functional studies of the potent anti-HIV chemokine variant P2-RANTES.</a>  Jin, HJ; Kagiampakis, I; Li, PW; LiWang, PJ.  SO PROTEINS-STRUCTURE FUNCTION AND BIOINFORMATICS, 2010; 78(2): 295-308. </p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273409700005">Synthesis and Anti-HIV Activity of [ddN]-[ddN] Dimers and Benzimidazole Nucloeoside Dimers.</a>  Li, GR; Liu, J; Pan, Q; Song, ZB; Luo, FL; Wang, SR; Zhang, XL; Zhou, X.  CHEMISTRY &amp; BIODIVERSITY, 2009; 6(12): 2200-2208.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273666800018">Anti-inflammatory and antibacterial profiles of selected compounds found in South African propolis.</a>  Du Toit, K; Buthelezi, S; Bodenstein, J.  SOUTH AFRICAN JOURNAL OF SCIENCE, 2009; 105(11-12): 470-472.</p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273613500040">Synthesis, anti-HIV and anti-oxidant activities of caffeoyl 5,6-anhydroquinic acid derivatives.</a>  Ma, CM; Kawahata, T; Hattori, M; Otake, T; Wang, LL; Daneshtalab, M.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(2): 863-869.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
