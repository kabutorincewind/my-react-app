

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-12-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XzwNm205TIa107gV1TkIycgAPNwN71nbRngsc6SdDOMyL1m4JuMneMKaqqcghWFa0+7zekj4T1UvvonSxH+ZJBwNdenH2LRoxbTWpGaNMEGqH2tMYd4kHqEy8Xg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BF08B10B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">December 10 - December 21, 2010</p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21167209">Antiviral Activity of Ganciclovir and Artesunate Towards Human Cytomegalovirus in Astrocytoma Cells</a>. Schnepf, N., J. Corvo, M.J. Pors, and M.C. Mazeron. Antiviral Research, 2010. <b>[Epub ahead of print]</b>; PMID[21167209].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21171142">In Vitro Anti Hsv-1 and Hsv-2 Activity of Tanacetum Vulgare Extracts and Isolated Compounds: An Approach to Their Mechanisms of Action</a>. Alvarez, A.L., S. Habtemariam, M. Juan-Badaturuge, C. Jackson, and F. Parra. Phytotherapy research : PTR, 2010. <b>[Epub ahead of print]</b>; PMID[21171142].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21141805">Inhibition of Herpes Simplex Virus Type 1 Infection by Silver Nanoparticles Capped with Mercaptoethane Sulfonate</a>. Baram-Pinto, D., S. Shukla, N. Perkas, A. Gedanken, and R. Sarid. Bioconjugate chemistry, 2009. 20(8): p. 1497-1502; PMID[21141805].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17074481">Design and Synthesis of Novel Bis(L-Amino Acid) Ester Prodrugs of 9-[2-(Phosphonomethoxy)Ethyl]Adenine (Pmea) with Improved Anti-Hbv Activity</a>. Fu, X., S. Jiang, C. Li, J. Xin, Y. Yang, and R. Ji. Bioorganic &amp; Medicinal Chemistry Letters, 2007. 17(2): p. 465-470; PMID[17074481].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17499889">Identification of 1-Isopropylsulfonyl-2-Amine Benzimidazoles as a New Class of Inhibitors of Hepatitis B Virus</a>. Li, Y.F., G.F. Wang, Y. Luo, W.G. Huang, W. Tang, C.L. Feng, L.P. Shi, Y.D. Ren, J.P. Zuo, and W. Lu. European Journal of Medicinal Chemistry, 2007. 42(11-12): p. 1358-1364; PMID[17499889].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/12086499">2-Amino-6-Arylthio-9-[2-(Phosphonomethoxy)Ethyl]Purine Bis(2,2,2-Trifluoroethyl) Esters as Novel Hbv-Specific Antiviral Reagents</a>. Sekiya, K., H. Takashima, N. Ueda, N. Kamiya, S. Yuasa, Y. Fujimura, and M. Ubasawa. Journal of Medicinal Chemistry, 2002. 45(14): p. 3138-42; PMID[12086499].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21145140">Structure-Activity Relationships Study of 6-Chloro-4-(2-Chlorophenyl)-3-(2-Hydroxyethyl) Quinolin-2(1h)-One Derivatives as Novel Non-Nucleoside Anti-Hepatitis B Virus Agents</a>. Guo, R.H., Q. Zhang, Y.B. Ma, J. Luo, C.A. Geng, L.J. Wang, X.M. Zhang, J. Zhou, Z.Y. Jiang, and J.J. Chen. European Journal of Medicinal Chemistry, 2011. 46(1): p. 307-319; PMID[21145140].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21170333">Comparative Analysis of the Lambda-Interferons Il-28a and Il-29 Regarding Their Transcriptome and Their Antiviral Properties against Hepatitis C Virus</a>. Diegelmann, J., F. Beigel, K. Zitzmann, A. Kaul, B. Goke, C.J. Auernhammer, R. Bartenschlager, H.M. Diepolder, and S. Brand. Plos One, 2010. 5(12): p. e15200; PMID[21170333].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21167889">Ns5a Inhibitors: A New Breakthrough for the Treatment of Chronic Hepatitis C</a>. Asselah, T. Journal of Hepatology, 2010. <b>[Epub ahead of print]</b>; PMID[21167889].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21167208">Hepatitis C Virus Complete Life Cycle Screen for Identification of Small Molecules with Pro- or Antiviral Activity</a>. Gentzsch, J., B. Hinkelmann, L. Kaderali, H. Irschik, R. Jansen, F. Sasse, R. Frank, and T. Pietschmann. Antiviral Research, 2010. <b>[Epub ahead of print]</b>; PMID[21167208].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21147189">Il28b Inhibits Hepatitis C Virus Replication through the Jak-Stat Pathway</a>. Zhang, L., N. Jilg, R.X. Shao, W. Lin, D.N. Fusco, H. Zhao, K. Goto, L.F. Peng, W.C. Chen, and R.T. Chung. Journal of Hepatology, 2010. <b>[Epub ahead of print]</b>; PMID[21147189].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21145867">Me3738 Enhances the Effect of Interferon and Inhibits Hepatitis C Virus Replication Both in Vitro and in Vivo</a>. Abe, H., M. Imamura, N. Hiraga, M. Tsuge, F. Mitsui, T. Kawaoka, S. Takahashi, H. Ochi, T. Maekawa, C.N. Hayes, C. Tateno, K. Yoshizato, S. Murakami, N. Yamashita, T. Matsuhira, K. Asai, and K. Chayama. Journal of Hepatology, 2010. <b>[Epub ahead of print]</b>; PMID[21145867].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21145235">Quinolones as Hcv Ns5b Polymerase Inhibitors</a>. Kumar, D.V., R. Rai, K.A. Brameld, J.R. Somoza, R. Rajagopalan, J.W. Janc, Y.M. Xia, T.L. Ton, M.B. Shaghafi, H. Hu, I. Lehoux, N. To, W.B. Young, and M.J. Green. Bioorganic &amp; Medicinal Chemistry Letters, 2010. <b>[Epub ahead of print]</b>; PMID[21145235].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21141896">Discovery of (7r)-14-Cyclohexyl-7-{[2-(Dimethylamino)Ethyl](Methyl) Amino}-7,8-Dihydro-6h-Indolo[1,2-E][1,5]Benzoxazocine-11-Carboxylic Acid (Mk-3281), a Potent and Orally Bioavailable Finger-Loop Inhibitor of the Hepatitis C Virus Ns5b Polymerase</a>. Narjes, F., B. Crescenzi, M. Ferrara, J. Habermann, S. Colarusso, M. Del Rosario Rico Ferreira, I. Stansfield, A.C. Mackay, I. Conte, C. Ercolani, S. Zaramella, M.C. Palumbi, P. Meuleman, G. Leroux-Roels, C. Giuliano, F. Fiore, S. Di Marco, P. Baiocco, U. Koch, G. Migliaccio, S. Altamura, R. Laufer, R. De Francesco, and M. Rowley. Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[21141896].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1210-122110.</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284332900028">Synthesis and Biological Evaluations of P4-Benzoxaborole-Substituted Macrocyclic Inhibitors of Hcv Ns3 Protease</a>. Ding, C.Z., Y.K. Zhang, X.F. Li, Y. Liu, S.M. Zhang, Y.S. Zhou, J.J. Plattner, S.J. Baker, L.A. Liu, M.S. Duan, R.L. Jarvest, J.J. Ji, W.M. Kazmierski, M.D. Tallant, L.L. Wright, G.K. Smith, R.M. Crosby, A.A. Wang, Z.J. Ni, W.X. Zou, and J. Wright. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7317-7322; ISI[000284332900028].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284332900039">Straightforward Synthesis of Triazoloacyclonucleotide Phosphonates as Potential Hcv Inhibitors</a>. Elayadi, H., M. Smietana, C. Pannecouque, P. Leyssen, J. Neyts, J.J. Vasseur, and H.B. Lazrek. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7365-7368; ISI[000284332900039].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284918000004">Iron Inhibits Replication of Infectious Hepatitis C Virus in Permissive Huh7.5.1 Cells</a>. Fillebeen, C. and K. Pantopoulos. Journal of Hepatology, 2010. 53(6): p. 995-999; ISI[000284918000004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284332900023">Synthesis and Anti-Hcv Activity of a New 2 &#39;-Deoxy-2 &#39;-Fluoro-2 &#39;-C-Methyl Nucleoside Analogue</a>. Hu, W.D., P.A. Wang, C.J. Song, Z.L. Pan, Q.A. Wang, X.H. Guo, X.J. Yu, Z.H. Shen, S.Y. Wang, and J.B. Chang. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7297-7298; ISI[000284332900023].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284287200024">2 &#39;-Deoxy-2 &#39;-Spirocyclopropylcytidine Revisited: A New and Selective Inhibitor of the Hepatitis C Virus Ns5b Polymerase</a>. Jonckers, T.H.M., T.I. Lin, C. Buyck, S. Lachau-Durand, K. Vandyek, S. Van Hoof, L.A.M. Vandekerckhove, L.L. Hu, J.M. Berke, L. Vijgen, L.L.A. Dillen, M.D. Cummings, H. de Kock, M. Nilsson, C. Sund, C. Rydegard, B. Samuelsson, A. Rosenquist, G. Fanning, K. Van Emelen, K. Simmen, and P. Raboisson. Journal of Medicinal Chemistry, 2010. 53(22): p. 8150-8160; ISI[000284287200024].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284158000013">Gene Therapeutic Approach for Inhibiting Hepatitis C Virus Replication Using a Recombinant Protein That Controls Interferon Expression</a>. Joo, C.H., U. Lee, Y.R. Nam, J.U. Jung, H. Lee, Y.K. Cho, and Y.K. Kim. Antimicrobial Agents and Chemotherapy, 2010. 54(12): p. 5048-5056; ISI[000284158000013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284332900068">Synthesis of New Acylsulfamoyl Benzoxaboroles as Potent Inhibitors of Hcv Ns3 Protease</a>. Li, X.F., Y.K. Zhang, Y. Liu, S.M. Zhang, C.Z. Ding, Y.S. Zhou, J.J. Plattner, S.J. Baker, L.A. Liu, W. Bu, W.M. Kazmierski, L.L. Wright, G.K. Smith, R.L. Jarvest, M.S. Duan, J.J. Ji, J.P. Cooper, M.D. Tallant, R.M. Crosby, K. Creech, Z.J. Ni, W.X. Zou, and J. Wright. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7493-7497; ISI[000284332900068].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284535900014">Reductive Condensation between Beta-Keto Esters and Aldehydes: Preparation of Novel Carbon-Linked Dihydropyrone Inhibitors of Hepatitis C Virus Polymerase</a>. Linton, M.A., J. Gonzalez, H. Li, J. Tatlock, T. Jewell, S. Johnson, M. Drowns, L. Patel, J. Blazel, and M. Ornelas. Synthesis-Stuttgart, 2010. 23: p. 4015-4020; ISI[000284535900014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284585500005">Phosphoramidate Prodrugs Deliver with Potency against Hepatitis C Virus</a>. Mehellou, Y. Chemmedchem, 2010. 5(11): p. 1841-1842; ISI[000284585500005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284650400005">Recent Patents on Nucleic Acid-Based Antiviral Therapeutics</a>. Mishra, S., S. Kim, and D.K. Lee. Recent Patents on Anti-Cancer Drug Discovery, 2010. 5(3): p. 255-271; ISI[000284650400005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284878700059">Noncytotoxic Inhibition of Cytomegalovirus Replication through Nk Cell Protease Granzyme M-Mediated Cleavage of Viral Phosphoprotein 71</a>. van Domselaar, R., L.E. Philippen, R. Quadir, E. Wiertz, J.A. Kummer, and N. Bovenschen. Journal of Immunology, 2010. 185(12): p. 7605-7613; ISI[000284878700059].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284332900008">Synthesis and Biological Evaluation of 1h-Benzimidazol-5-Ols as Potent Hbv Inhibitors</a>. Zhao, Y.F., Y.J. Liu, D. Chen, Z.Q. Wei, W.Z. Liu, and P. Gong. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(24): p. 7230-7233; ISI[000284332900008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1210-122110.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
