

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-03-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="LO8vzZs/8ceALpmVqGdG6UOZvm33TakQHc8mbjP6Y/mJWcv01vUymRBWX+FP2AwOfb4ydkrwZD2ZdX2EOL0OeLcsRFKqnM+pz+7QI0JPiAIz5qkhwjyWzc9mpF4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E44FB04D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 14 - March 27, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24582984">Activity of Caffeic acid Derivatives against Candida albicans Biofilm.</a> De Vita, D., L. Friggeri, F.D. D&#39;Auria, F. Pandolfi, F. Piccoli, S. Panella, A.T. Palamara, G. Simonetti, L. Scipione, R. Di Santo, R. Costi, and S. Tortorella. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(6): p. 1502-1505. PMID[24582984].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24508829">Toward New Classes of Potent Antibiotics: Synthesis and Antimicrobial Activity of Novel Metallosaldach-imidazolium Salts.</a> Elshaarawy, R.F. and C. Janiak. European Journal of Medicinal Chemistry, 2014. 75: p. 31-42. PMID[24508829].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211443">All-natural Composite Wound Dressing Films of Essential Oils Encapsulated in Sodium Alginate with Antimicrobial Properties.</a>Liakos, I., L. Rizzello, D.J. Scurr, P.P. Pompa, I.S. Bayer, and A. Athanassiou. International Journal of Pharmaceutics, 2014. 463(2): p. 137-145. PMID[24211443].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24606327">Structure-Activity Relationships among Antifungal Nylon-3 Polymers: Identification of Materials Active against Drug-resistant Strains of Candida albicans.</a> Liu, R., X. Chen, S.P. Falk, B.P. Mowery, A.J. Karlsson, B. Weisblum, S.P. Palecek, K.S. Masters, and S.H. Gellman. Journal of the American Chemical Society, 2014. 136(11): p. 4333-4442. PMID[24606327].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24530493">Synthesis and Antimicrobial Evaluation of Amide Derivatives of Benzodifuran-2-carboxylic acid.</a> Soni, J.N. and S.S. Soman. European Journal of Medicinal Chemistry, 2014. 75: p. 77-81. PMID[24530493].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24334014">Structural, Molecular Orbital and Optical Characterizations of Solvatochromic Mixed Ligand Copper(II) Complex of 5,5-Dimethyl cyclohexanate 1,3-dione and N,N,N&#39;,N&#39;N&#39;&#39;-Pentamethyldiethylenetriamine.</a> Taha, A., A.A. Farag, A.H. Ammar, and H.M. Ahmed. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2014. 122: p. 512-520. PMID[24334014].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24582981">Discovery of Novel bis-Oxazolidinone Compounds as Potential Potent and Selective Antitubercular Agents.</a> Ang, W., W. Ye, Z. Sang, Y. Liu, T. Yang, Y. Deng, Y. Luo, and Y. Wei. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(6): p. 1496-1501. PMID[24582981].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24556148">Benzimidazole-based Compounds Kill Mycobacterium tuberculosis.</a> Gong, Y., S. Somersan Karakaya, X. Guo, P. Zheng, B. Gold, Y. Ma, D. Little, J. Roberts, T. Warrier, X. Jiang, M. Pingle, C.F. Nathan, and G. Liu. European Journal of Medicinal Chemistry, 2014. 75: p. 336-353. PMID[24556148].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24333643">Salicylanilide Pyrazinoates Inhibit in Vitro Multidrug-resistant Mycobacterium tuberculosis Strains, Atypical Mycobacteria and Isocitrate lyase.</a> Kratky, M., J. Vinsova, E. Novotna, and J. Stolarikova. European Journal of Pharmaceutical Sciences, 2014. 53: p. 1-9. PMID[24333643].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24530490">Design, Synthesis and Antitubercular Evaluation of Novel Series of N-[4-(Piperazin-1-yl)phenyl]cinnamamide Derivatives.</a> Patel, K.N. and V.N. Telvekar. European Journal of Medicinal Chemistry, 2014. 75: p. 43-56. PMID[24530490].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24565972">Development of Novel Tetrahydrothieno[2,3-c]pyridine-3-carboxamide Based Mycobacterium tuberculosis Pantothenate synthetase Inhibitors: Molecular Hybridization from Known Antimycobacterial Leads.</a> Samala, G., P.B. Devi, R. Nallangi, J.P. Sridevi, S. Saxena, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2014. 22(6): p. 1938-1947. PMID[24565972].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24582983">4,5-Dihydro-1H-pyrazolo[3,4-d]pyrimidine Containing Phenothiazines as Antitubercular Agents.</a> Siddiqui, A.B., A.R. Trivedi, V.B. Kataria, and V.H. Shah. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(6): p. 1493-1495. PMID[24582983].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24565970">Stereoselective Preparation of Pyridoxal 1,2,3,4-tetrahydro-beta-carboline Derivatives and the Influence of Their Absolute and Relative Configuration on the Proliferation of the Malaria Parasite Plasmodium falciparum.</a> Brokamp, R., B. Bergmann, I.B. Muller, and S. Bienz. Bioorganic &amp; Medicinal Chemistry, 2014. 22(6): p. 1832-1837. PMID[24565970].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24651068">Theoretical and Experimental Studies of New Modified Isoflavonoids as Potential Inhibitors of Topoisomerase I from Plasmodium falciparum.</a> Cortopassi, W.A., J. Penna-Coutinho, A.C. Aguiar, A.S. Pimentel, C.D. Buarque, P.R. Costa, B.R. Alves, T.C. Franca, and A.U. Krettli. Plos One, 2014. 9(3): p. e91191. PMID[24651068].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24618129">In Vitro and in Vivo Combination of Cepharanthine with Anti-malarial Drugs.</a> Desgrouas, C., J. Dormoi, C. Chapus, E. Ollivier, D. Parzy, and N. Taudon. Malaria Journal, 2014. 13(1): p. 90. PMID[24618129].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24530492">Design, Synthesis and Biological Evaluation of 3-[4-(7-Chloro-quinolin-4-yl)-piperazin-1-yl]-propionic acid Hydrazones as Antiprotozoal Agents.</a> Inam, A., S.M. Siddiqui, T.S. Macedo, D.R. Moreira, A.C. Leite, M.B. Soares, and A. Azam. European Journal of Medicinal Chemistry, 2014. 75: p. 67-76. PMID[24530492].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24534536">Synthesis of 4&#39;-(2-Ferrocenyl)-2,2&#39;:6&#39;2&#39;&#39;-terpyridine: Characterization and Antiprotozoal Activity of Mn(II), Co(II), Ni(II), Cu(II) and Zn(II) Complexes.</a> Juneja, A., T.S. Macedo, D.R. Magalhaes Moreira, M.B. Pereira Soares, A.C. Lima Leite, J. Kelle de Andrade Lemoine Neves, V.R. Alves Pereira, F. Avecilla, and A. Azam. European Journal of Medicinal Chemistry, 2014. 75: p. 203-210. PMID[24534536].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24611932">Mollemycin A: An Antimalarial and Antibacterial Glyco-hexadepsipeptide-polyketide from an Australian Marine-derived Streptomyces sp. (CMB-M0244).</a> Raju, R., Z.G. Khalil, A.M. Piggott, A. Blumenthal, D.L. Gardiner, T.S. Skinner-Adams, and R.J. Capon. Organic Letters, 2014. 16(6): p. 1716-1719. PMID[24611932].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24561670">Synthesis and Study of Cytotoxic Activity of 1,2,4-trioxane- and Egonol-derived Hybrid Molecules against Plasmodium falciparum and Multidrug-resistant Human Leukemia Cells.</a> Reiter, C., A. Capci Karagoz, T. Frohlich, V. Klein, M. Zeino, K. Viertel, J. Held, B. Mordmuller, S. Emirdag Ozturk, H. Anil, T. Efferth, and S.B. Tsogoeva. European Journal of Medicinal Chemistry, 2014. 75: p. 403-412. PMID[24561670].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0314-032714</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331068800002">HPLC Profiling with at-line Microdilution Assay for the Early Identification of Anti-fungal Compounds in Plants from French Polynesia</a>. Bertrand, S., C. Petit, L. Marcourt, R. Ho, K. Gindro, M. Monod, and J.L. Wolfender. Phytochemical Analysis, 2014. 25(2): p. 106-112. ISI[000331068800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331003400012">Synthesis, Antimicrobial and Cytotoxic Activity of 2-Azetidinone Derivatives of Pyridyl benzimidazoles.</a> Desai, N.C., D.D. Pandya, G.M. Kotadiya, and P. Desai. Medicinal Chemistry Research, 2014. 23(4): p. 1725-1741. ISI[000331003400012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331003400038">Synthesis and Evaluation of New Chalcones, Derived Pyrazoline and Cyclohexenone Derivatives as Potent Antimicrobial, Antitubercular and Antileishmanial Agents.</a> Monga, V., K. Goyal, M. Steindel, M. Malhotra, D.P. Rajani, and S.D. Rajani. Medicinal Chemistry Research, 2014. 23(4): p. 2019-2032. ISI[000331003400038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330766600034">Docking Simulation, Synthesis and Biological Evaluation of Novel Pyridazinone Containing Thymol as Potential Antimicrobial Agents.</a> Nagle, P., Y. Pawar, A. Sonawane, S. Bhosale, and D. More. Medicinal Chemistry Research, 2014. 23(2): p. 918-926. ISI[000330766600034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330766600027">Synthesis and QSAR Studies of Some Novel Disubstituted 1,2,4-Triazoles as Antimicrobial Agents.</a> Panda, S.S. and S.C. Jain. Medicinal Chemistry Research, 2014. 23(2): p. 848-861. ISI[000330766600027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331157500011">Molecular Structures and Biological Evaluation of 2-Chloro-3-(N-alkylamino)-1,4-napthoquinone Derivatives as Potent Antifungal Agents.</a> Pawar, O., A. Patekar, A. Khan, L. Kathawate, S. Haram, G. Markad, V. Puranik, and S. Salunke-Gawali. Journal of Molecular Structure, 2014. 1059: p. 68-74. ISI[000331157500011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331026100004">Antifungal, Antiaflatoxin and Antioxidant Potential of Chemically Characterized Boswellia carterii Birdw Essential Oil and its in Vivo Practical Applicability in Preservation of Piper nigrum L. Fruits.</a> Prakash, B., P.K. Mishra, A. Kedia, and N.K. Dubey. LWT-Food Science and Technology, 2014. 56(2): p. 240-247. ISI[000331026100004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330901200007">4-Aminoquinoline-beta-lactam Conjugates: Synthesis, Antimalarial, and Antitubercular Evaluation.</a> Raj, R., C. Biot, S. Carrere-Kremer, L. Kremer, Y. Guerardel, J. Gut, P.J. Rosenthal, and V. Kumar. Chemical Biology &amp; Drug Design, 2014. 83(2): p. 191-197. ISI[000330901200007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331128400008">Identification of Shikimate Kinase Inhibitors among anti-Mycobacterium tuberculosis Compounds by LC-MS.</a> Simithy, J., N. Reeve, J.V. Hobrath, R.C. Reynolds, and A.I. Calderon. Tuberculosis, 2014. 94(2): p. 152-158. ISI[000331128400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331003400014">DFT-based Prediction of Antifungal and Insecticidal Activities of Perfluorophenyl Antimony(III) and Antimony(V) Chlorides.</a> Singhal, K., V.K. Sahu, P. Singh, and P. Raj. Medicinal Chemistry Research, 2014. 23(4): p. 1758-1767. ISI[000331003400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330800300005">Antifungal Properties of Nanosized ZnS Particles Synthesised by Sonochemical Precipitation.</a> Suyana, P., S.N. Kumar, B.S.D. Kumar, B.N. Nair, S.C. Pillai, A.P. Mohamed, K.G.K. Warrier, and U.S. Hareesh. RSC Advances, 2014. 4(17): p. 8439-8445. ISI[000330800300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0314-032714.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
