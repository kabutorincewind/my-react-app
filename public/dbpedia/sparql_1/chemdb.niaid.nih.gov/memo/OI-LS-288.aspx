

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-288.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ZWjg2RtTY7gALVPAB7J6gHgzXmb7iZbjg4U0JB1c59iT+DK/8iYr3mzUCHuBIyv3M5BSYC3dBWFhQIxY1OG1OsvzYDWSnIN5VkLrADBSMuf7OX9AqweN2ZU2++I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6848333F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-288-MEMO</b> </p>

<p>    1.    42732   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           An inorganic iron complex that inhibits wild-type and an isoniazid-resistant mutant 2-trans-enoyl-ACP (CoA) reductase from Mycobacterium tuberculosis</p>

<p>           Oliveira, JS, Sousa, EH, Basso, LA, Palaci, M, Dietze, R, Santos, DS, and Moreira, S I</p>

<p>           Chem Commun (Camb) 2004. 3: 312-313</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14740053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14740053&amp;dopt=abstract</a> </p><br />

<p>    2.    42733   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Interferon alfa2a induction therapy in combination with ribavirin and amantadine for the treatment of naive patients with chronic HCV infection</p>

<p>           Engler, S, Flechtenmacher, C, Wiedemann, KH, Gugler, R, Stremmel, W, and Kallinowski, B</p>

<p>           J Viral Hepat  2004. 11(1): 60-68</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14738559&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14738559&amp;dopt=abstract</a> </p><br />

<p>    3.    42734   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           A new era of antifungal therapy</p>

<p>           Wingard, John R and Leather, Helen</p>

<p>           Biology of Blood and Marrow Transplantation 2004. 10(2): 73-90</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B758K-4BJW4M5-5/2/574593b22743b6af0ca9b035be0af8ea">http://www.sciencedirect.com/science/article/B758K-4BJW4M5-5/2/574593b22743b6af0ca9b035be0af8ea</a> </p><br />

<p>    4.    42735   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           LAAE-14, a new anti-inflammatory drug, increases the survival of Candida albicans-inoculated mice</p>

<p>           Lucas, Rut, Villamon, Eva, Paya, Miguel, Alves, Mario, del Olmo, Esther, Gozalbo, Daniel, and Gil, MLuisa</p>

<p>           FEMS Immunology and Medical Microbiology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2T-4BG839G-1/2/3a3fe439729e9adb018d3717ccf7193b">http://www.sciencedirect.com/science/article/B6T2T-4BG839G-1/2/3a3fe439729e9adb018d3717ccf7193b</a> </p><br />

<p>    5.    42736   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           Antibacterial and antifungal activity of Syzygium jambolanum seeds</p>

<p>           Chandrasekaran, M and Venkatesalu, V</p>

<p>           Journal of Ethnopharmacology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T8D-4BHK2XV-2/2/cf96226eeaffaafd8fecd1ed1d8e11d2">http://www.sciencedirect.com/science/article/B6T8D-4BHK2XV-2/2/cf96226eeaffaafd8fecd1ed1d8e11d2</a> </p><br />

<p>    6.    42737   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Iron enhances the antituberculous activity of pyrazinamide</p>

<p>           Somoskovi, A, Wade, MM, Sun, Z, and Zhang, Y</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14729751&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14729751&amp;dopt=abstract</a> </p><br />

<p>    7.    42738   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           New [alpha]-(N)-heterocyclichydrazones: evaluation of anticancer, anti-HIV and antimicrobial activity</p>

<p>           Savini, Luisa, Chiasserini, Luisa, Travagli, Valter, Pellerano, Cesare, Novellino, Ettore, Cosentino, Sofia, and Pisano, MBarbara</p>

<p>           European Journal of Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VKY-4BH6GNB-1/2/2f764a343911557af1aae5cf79697e52">http://www.sciencedirect.com/science/article/B6VKY-4BH6GNB-1/2/2f764a343911557af1aae5cf79697e52</a> </p><br />

<p>    8.    42739   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Inhibition of human cytomegalovirus replication by small interfering RNAs</p>

<p>           Wiebusch, L,  Truss, M, and Hagemeier, C</p>

<p>           J Gen Virol 2004. 85(Pt 1): 179-184</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14718633&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14718633&amp;dopt=abstract</a> </p><br />

<p>    9.    42740   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           Anti-HIV natural product (+)-calanolide A is active against both drug-susceptible and drug-resistant strains of Mycobacterium tuberculosis</p>

<p>           Xu, Ze-Qi, Barrow, William W, Suling, William J, Westbrook, Louise, Barrow, Esther, Lin, Yuh-Meei, and Flavin, Michael T</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BFXN3R-2/2/6837f7311c451d6ea9e36ec1ae81e6ed">http://www.sciencedirect.com/science/article/B6TF8-4BFXN3R-2/2/6837f7311c451d6ea9e36ec1ae81e6ed</a> </p><br />

<p>  10.    42741   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           In vitro activities of voriconazole, posaconazole, and fluconazole against 4,169 clinical isolates of Candida spp. and Cryptococcus neoformans collected during 2001 and 2002 in the ARTEMIS global antifungal surveillance program</p>

<p>           Pfaller, MA, Messera, SA, Boyken, L, Hollis, RJ, Rice, C, Tendolkar, S, and Diekema, DJ</p>

<p>           Diagnostic Microbiology and Infectious Disease 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T60-4BD5HNX-1/2/9e549770ea0ad4866b2c26eaf613468b">http://www.sciencedirect.com/science/article/B6T60-4BD5HNX-1/2/9e549770ea0ad4866b2c26eaf613468b</a> </p><br />

<p>  11.    42742   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           The role of antifungal susceptibility testing in the therapy of candidiasis*1</p>

<p>           Hospenthal, Duane R, Murray, Clinton K, and Rinaldi, Michael G</p>

<p>           Diagnostic Microbiology and Infectious Disease 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T60-4BD5HNX-4/2/d6d3c2a78ea87d11698006918f0d1b86">http://www.sciencedirect.com/science/article/B6T60-4BD5HNX-4/2/d6d3c2a78ea87d11698006918f0d1b86</a> </p><br />

<p>  12.    42743   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           Acetylene-based analogues of thiolactomycin, active against Mycobacterium tuberculosis mtFabH fatty acid condensing enzyme</p>

<p>           Senior, Suzanne J, Illarionov, Petr A, Gurcha, Sudagar S, Campbell, Ian B, Schaeffer, Merrill L, Minnikin, David E, and Besra, Gurdyal S</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(2): 373-376</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4B5JMN6-7/2/a0d226c6e42cd8bd67d34d3952d2c957">http://www.sciencedirect.com/science/article/B6TF9-4B5JMN6-7/2/a0d226c6e42cd8bd67d34d3952d2c957</a> </p><br />

<p>  13.    42744   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Bioactive flavonoids from Kaempferia parviflora</p>

<p>           Yenjai, C, Prasanphen, K, Daodee, S, Wongpanich, V, and Kittakoop, P</p>

<p>           Fitoterapia 2004. 75(1): 89-92</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693228&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693228&amp;dopt=abstract</a> </p><br />

<p>  14.    42745   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           Human herpesvirus 8: an update</p>

<p>           De Paoli, Paolo</p>

<p>           Microbes and Infection 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VPN-4BHJ8HY-2/2/fbff85ac91611828a22624dd1faaac64">http://www.sciencedirect.com/science/article/B6VPN-4BHJ8HY-2/2/fbff85ac91611828a22624dd1faaac64</a> </p><br />

<p>  15.    42746   OI-LS-288; EMBASE-OI-1/29/2004</p>

<p class="memofmt1-2">           Non-nucleoside inhibitors of the hepatitis C virus NS5B polymerase: discovery of benzimidazole 5-carboxylic amide derivatives with low-nanomolar potency</p>

<p>           Beaulieu, Pierre L, Bos, Michael, Bousquet, Yves, DeRoy, Patrick, Fazal, Gulrez, Gauthier, Jean, Gillard, James, Goulet, Sylvie, McKercher, Ginette, and Poupart, Marc-Andre</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BH63MS-R/2/5c760fd8539c3f7e60cb31b0f8c48466">http://www.sciencedirect.com/science/article/B6TF9-4BH63MS-R/2/5c760fd8539c3f7e60cb31b0f8c48466</a> </p><br />

<p>  16.    42747   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Catalase-Peroxidase Activity Has No Influence on Virulence in a Murine Model of Tuberculosis</b></p>

<p>           Cardona, PJ,  Gordillo, S, Amat, I, Diaz, J, Lonca, J, Vilaplana, C, Pallares, A, Llatjos, R, Ariza, A, and Ausina, V</p>

<p>           Tuberculosis 2003. 83(6): 351-359</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    42748   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Pneumocystis Pneumonia in Humans Is Caused by P Jiroveci Not P Carinii</b></p>

<p>           Miller, R</p>

<p>           Thorax 2004. 59(1): 83</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  18.    42749   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Multiplex PCR Amplimer Conformation Analysis for Rapid Detection of gyrA Mutations in Fluoroquinolone-Resistant Mycobacterium tuberculosis Clinical Isolates</p>

<p>           Cheng, AF, Yew, WW, Chan, EW, Chin, ML, Hui, MM, and Chan, RC</p>

<p>           Antimicrob Agents Chemother 2004.  48(2): 596-601</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14742214&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14742214&amp;dopt=abstract</a> </p><br />

<p>  19.    42750   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Systematic, Genome-Wide Identification of Host Genes Affecting Replication of a Positive-Strand Rna Virus</b></p>

<p>           Kushner, DB,  Lindenbach, BD, Grdzelishvili, VZ, Noueiry, AO, Paul, SM, and Ahlquist, P</p>

<p>           Proceedings of the National Academy of Sciences of the United States of America 2003. 100(26): 15764-15769</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  20.    42751   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Disruption of Hepatitis C Virus Rna Replication Through Inhibition of Host Protein Geranylgeranylation</b></p>

<p>           Ye, J, Wang, CF, Sumpter, R, Brown, MS, Goldstein, JL, and Gale, M</p>

<p>           Proceedings of the National Academy of Sciences of the United States of America 2003. 100(26): 15865-15870</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    42752   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Antimycobacterial Natural Products</b></p>

<p>           Copp, BR</p>

<p>           Natural Product Reports 2003. 20(6): 535-557</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  22.    42753   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Specific inhibition of human cytomegalovirus glycoprotein B-mediated fusion by a novel thiourea small molecule</p>

<p>           Jones, TR, Lee, SW, Johann, SV, Razinkov, V, Visalli, RJ, Feld, B, Bloom, JD, and O&#39;Connell, J</p>

<p>           J Virol 2004. 78(3): 1289-1300</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14722284&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14722284&amp;dopt=abstract</a> </p><br />

<p>  23.    42754   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Inhibition of S-Phase Cyclin-Dependent Kinase Activity Blocks Expression of Epstein-Barr Virus Immediate-Early and Early Genes, Preventing Viral Lytic Replication</b></p>

<p>           Kudoh, A, Daikoku, T, Sugaya, Y, Isomura, H, Fujita, M, Kiyono, T, Nishiyama, Y, and Tsurumi, T</p>

<p>           Journal of Virology 2004. 78(1): 104-115</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    42755   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           In vitro antimicrobial activity of propolis and synergism between propolis and antimicrobial drugs</p>

<p>           Stepanovic, S, Antic, N, Dakic, I, and Svabic-Vlahovic, M</p>

<p>           Microbiol Res 2003. 158(4): 353-357</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14717457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14717457&amp;dopt=abstract</a> </p><br />

<p>  25.    42756   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Structural and Functional Analysis of Human Cytomegalovirus Us3 Protein</b></p>

<p>           Misaghi, S, Sun, ZYJ, Stern, P, Gaudet, R, Wagner, G, and Ploegh, H</p>

<p>           Journal of Virology 2004. 78(1): 413-423</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    42757   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Saving quinolones for tuberculosis</p>

<p>           Bedi, RS</p>

<p>           J Assoc Physicians India 2003. 51: 933-933</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14710994&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14710994&amp;dopt=abstract</a> </p><br />

<p>  27.    42758   OI-LS-288; PUBMED-OI-1/27/2004</p>

<p class="memofmt1-2">           Detection and typing of HSV-1, HSV-2, CMV and EBV by quadruplex PCR</p>

<p>           Shin, CH, Park, GS, Hong, KM, and Paik, MK</p>

<p>           Yonsei Med J 2003. 44(6): 1001-1007</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14703608&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14703608&amp;dopt=abstract</a> </p><br />

<p>  28.    42759   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Mode of Action of Ponazuril Against Toxoplasma Gondii Tachyzoites in Cell Culture</b></p>

<p>           Mitchell, SM, Zajac, AM, Davis, WL, and Lindsay, DS</p>

<p>           Journal of Eukaryotic Microbiology 2003. 50: 689-690 </p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  29.    42760   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Initial Binding of the Broad Spectrum Antiviral Nucleoside Ribavirin to the Hepatitis C Virus Rna Polymerase</b></p>

<p>           Bougie, I and Bisaillon, M</p>

<p>           Journal of Biological Chemistry 2003. 278(52): 52471-52478</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.    42761   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Unique Mechanism of Action of the Thiourea Drug Isoxyl on Mycobacterium Tuberculosis</b></p>

<p>           Phetsuksiri, B, Jackson, M, Scherman, H, Mcneil, M, Besra, GS, Baulard, AR, Slayden, RA, Debarber, AE, Barry, CE, Baird, MS, Crick, DC, and Brennan, PJ</p>

<p>           Journal of Biological Chemistry 2003. 278(52): 53123-53130</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    42762   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Early Drug Safety Evaluation: Biomarkers, Signatures, and Fingerprints</b></p>

<p>           Roberts, R, Cain, K, Coyle, B, Freathy, C, Leonard, JF, and Gautier, JC</p>

<p>           Drug Metabolism Reviews 2003. 35(4): 269-275</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    42763   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Detection and counting of Cryptosporidium parvum in HCT-8 cells by flowcytometry</p>

<p>           Mele, R, Gomes, MMA, Tosini, F, and Pozio, E</p>

<p>           PARASITE 2003. 10(4): 297-302</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187666600002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187666600002</a> </p><br />

<p>  33.    42764   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Synthesis and biological evaluation of quinazoline-4-thiones</p>

<p>           Kubicova, L, Sustr, M, Kral&#39;ova, K, Chobot, V, Vytlacilova, J, Jahodar, L, Vuorela, P, Machacek, M, and Kaustova, J</p>

<p>           MOLECULES 2003. 8 (11): 756-769</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187789100001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187789100001</a> </p><br />

<p>  34.    42765   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Synthesis and bioactivity of erythromycin derivatives</p>

<p>           Chen, MW, Muri, EMF, Jacob, M, and Williamson, JS</p>

<p>           MED CHEM RES 2003. 12(3): 111-129</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187744000001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187744000001</a> </p><br />

<p>  35.    42766   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Anticryptosporidial activity of furan derivative G1 and its inclusion complex with beta-cyclodextrin</p>

<p>           Castro-Hermida, JA, Gomez-Couso, H, Ares-Mazas, ME, Gonzalez-Edia, MM, Castaneda-Cancio, N, Otero-Espinar, FJ, and Blanco-Mendez, J</p>

<p>           J PHARM SCI-US 2004. 93(1): 197-206</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187668400021">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187668400021</a> </p><br />

<p>  36.    42767   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Heliotropamide, a novel oxopyrrolidine-3-carboxamide from Heliotropium ovalifolium</p>

<p>           Guntern, A, Ioset, JR, Queiroz, EF, Sandor, P, Foggin, CM, and Hostettmann, K</p>

<p>           J NAT PROD 2003. 66(12): 1550-1553</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187718800006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187718800006</a> </p><br />

<p>  37.    42768   OI-LS-288; WOS-OI-1/22/2004</p>

<p>           <b>Synthesis and Antiviral Evaluation of Novel 3 &#39;- and 4 &#39;-Doubly Branched Carbocyclic Nucleosides as Potential Antiviral Agents</b></p>

<p>           Hong, JH</p>

<p>           Archives of Pharmacal Research 2003. 26(12): 1109-1116</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  38.    42769   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           A novel antibacterial iridoid and triterpene from Caiophora coronata</p>

<p>           Khera, S, Woldemichael, GM, Singh, MP, Suarez, E, and Timmermann, BN</p>

<p>           J NAT PROD 2003. 66(12): 1628-1631</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187718800023">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187718800023</a> </p><br />

<p>  39.    42770   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Purification and identification of an antifungal agent from Streptomyces sp. KH-614 antagonistic to rice blast fungus, Pyricularia oryzae</p>

<p>           Rhee, KH</p>

<p>           J MICROBIOL BIOTECHN 2003. 13(6): 984-988</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187661400022">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187661400022</a> </p><br />

<p>  40.    42771   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Antimycobacterial activity of piperidinylpropyl esters of alkoxy-substituted phenylcarbamic acids</p>

<p>           Waisser, K, Drazkova, K, Cizmarik, J, and Kaustova, J</p>

<p>           FOLIA MICROBIOL 2003. 48(5): 585-587</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187615000004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187615000004</a> </p><br />

<p>  41.    42772   OI-LS-288; WOS-OI-1/25/2004</p>

<p class="memofmt1-2">           Antifungal activity of some bis-5-methylbenzimidazole compounds</p>

<p>           Kucukbay, H, Durmaz, R, Okyucu, N, and Gunal, S</p>

<p>           FOLIA MICROBIOL 2003. 48(5): 679-681</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187615000019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187615000019</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
