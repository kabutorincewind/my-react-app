

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-06-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="iXOYxK+1/qxpt6Cq98jr3BxuyLETrS4xpGCueq7NBFK8J/BiKkPYfPqH3N8Lpkkt1Xe0eZ4/r32wUht6kFfHtIk708loTDprmcDLOQYc+w8m8fJxShGRugbmoiQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="670F5632" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: May 24 - June 6, 2013</h1>

    <h2>Hepatitis A Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23722879">Molecular Biology and Inhibitors of Hepatitis A Virus</a><i>.</i> Debing, Y., J. Neyts, and H.J. Thibaut. Medicinal Research Reviews, 2013.  <b>[Epub ahead of print]</b>; PMID[23722879].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23576513">Sulfamoylbenzamide Derivatives Inhibit the Assembly of Hepatitis B Virus Nucleocapsids</a><i>.</i> Campagna, M.R., F. Liu, R. Mao, C. Mills, D. Cai, F. Guo, X. Zhao, H. Ye, A. Cuconati, H. Guo, J. Chang, X. Xu, T.M. Block, and J.T. Guo. Journal of Virology, 2013. 87(12): p. 6931-6942; PMID[23576513].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23415804">GS-9620, an Oral Agonist of Toll-Like Receptor-7, Induces Prolonged Suppression of Hepatitis B Virus in Chronically Infected Chimpanzees</a><i>.</i> Lanford, R.E., B. Guerra, D. Chavez, L. Giavedoni, V.L. Hodara, K.M. Brasky, A. Fosdick, C.R. Frey, J. Zheng, G. Wolfgang, R.L. Halcomb, and D.B. Tumas. Gastroenterology, 2013. 144(7): p. 1508-1517 e10; PMID[23415804].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23701235">Silibinin Inhibits Hepatitis C Virus Entry into Hepatocytes by Hindering Clathrin-dependent Trafficking</a><i>.</i> Blaising, J., P.L. Levy, C. Gondeau, C. Phelip, M. Varbanov, E. Teissier, F. Ruggiero, S.J. Polyak, N.H. Oberlies, T. Ivanovic, S. Boulant, and E.I. Pecheur. Cellular Microbiology, 2013.  <b>[Epub ahead of print]</b>; PMID[23701235].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23598249">Design and Synthesis of L- and D-Phenylalanine Derived Rhodanines with Novel C5-Arylidenes as Inhibitors of HCV NS5B Polymerase</a><i>.</i> Patel, B.A., R. Krishnan, N. Khadtare, K.R. Gurukumar, A. Basu, P. Arora, A. Bhatt, M.R. Patel, D. Dana, S. Kumar, N. Kaushik-Basu, and T.T. Talele. Bioorganic &amp; Medicinal Chemistry, 2013. 21(11): p. 3262-3271; PMID[23598249].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23741428">2-Octynoic acid Inhibits Hepatitis C Virus Infection through Activation of AMP-activated Protein Kinase</a><i>.</i> Yang, D., B. Xue, X. Wang, X. Yu, N. Liu, Y. Gao, C. Liu, and H. Zhu. PloS One, 2013. 8(5): p. e64932; PMID[23741428].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23623492">Synthesis and Evaluation of Janus Type Nucleosides as Potential HCV NS5B Polymerase Inhibitors</a><i>.</i> Zhou, L., F. Amblard, H. Zhang, T.R. McBrayer, M.A. Detorio, T. Whitaker, S.J. Coats, and R.F. Schinazi. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(11): p. 3385-3388; PMID[23623492].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <h2>Herpes Simplex 1Virus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23724015">The Lantibiotic Peptide Labyrinthopeptin A1 Demonstrates Broad anti-HIV and anti-HSV Activity with Potential for Microbicidal Applications</a><i>.</i> Ferir, G., M.I. Petrova, G. Andrei, D. Huskens, B. Hoorelbeke, R. Snoeck, J. Vanderleyden, J. Balzarini, S. Bartoschek, M. Bronstrup, R.D. Sussmuth, and D. Schols. PloS One, 2013. 8(5): p. e64010; PMID[23724015].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23716050">Theaflavin-3,3&#39;-digallate and Lactic acid Combinations Reduce Herpes Simplex Virus Infectivity</a><i>.</i> Isaacs, C.E. and W. Xu. Antimicrobial Agents and Chemotherapy, 2013.  <b>[Epub ahead of print]</b>; PMID[23716050].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23606629">Synthesis and Anti-herpetic Activity of Phosphoramidate Protides</a><i>.</i> Maiti, M., L. Persoons, G. Andrei, R. Snoeck, J. Balzarini, and P. Herdewijn. ChemMedChem, 2013. 8(6): p. 985-993; PMID[23606629].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23536670">Griffithsin Protects Mice from Genital Herpes by Preventing Cell-to-Cell Spread</a><i>.</i> Nixon, B., M. Stefanidou, P.M. Mesquita, E. Fakioglu, T. Segarra, L. Rohan, W. Halford, K.E. Palmer, and B.C. Herold. Journal of Virology, 2013. 87(11): p. 6257-6269; PMID[23536670].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23740985">Pyrrolidine Dithiocarbamate Inhibits Herpes Simplex Virus Type 1 and 2 Replication and Its Activity May Be Mediated through Dysregulation of Ubiquitin-Proteasome System</a><i>.</i> Qiu, M., Y. Chen, L. Cheng, Y. Chu, H.Y. Song, and Z.W. Wu. Journal of Virology, 2013.  <b>[Epub ahead of print]</b>; PMID[23740985].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23571549">Wnt Modulating Agents Inhibit Human Cytomegalovirus Replication</a><i>.</i> Kapoor, A., R. He, R. Venkatadri, M. Forman, and R. Arav-Boger. Antimicrobial Agents and Chemotherapy, 2013. 57(6): p. 2761-2767; PMID[23571549].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0524-060613.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316582400006">Efficient Synthesis of Novel Thieno 3,2-B -, 2,3-C - and 3,2-C Pyridones by Sonogashira Coupling of Bromothiophenes with Terminal Alkynes and Subsequent Intramolecular C-N Bond-forming Reaction</a><i>.</i> Iaroshenko, V.O., S. Ali, T.M. Babar, M.S.A. Abbasi, V.Y. Sosnovskikh, A. Villinger, A. Tolmachev, and P. Langer. Tetrahedron, 2013. 69(15): p. 3167-3181; ISI[000316582400006].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0524-060613.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
