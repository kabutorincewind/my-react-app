

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-01-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+ieyMAXl3KmMCloyWe9cV2L8CH3rldeuZh+lI2wQ58wf2Ny4Kn4BGr920dhrbSfhyBuR+LkyLKPp3LWlSfhR4sT/6lWVqimxrfoG+aSMB+fRR/yJAnmfnDpAEqQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ADF39E89" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  December 23 - January 5, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20049904?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">Anti-retroviral activity of TRIM5alpha.</a>  Nakayama EE, Shioda T.  Rev Med Virol. 2010 Jan 4. [Epub ahead of print]PMID: 20049904</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20047920?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Pradimicin S, a highly-soluble non-peptidic small-size carbohydrate-binding antibiotic, is an anti-HIV drug lead for both microbicidal and systemic use.</a> Balzarini J, François K, Van Laethem K, Hoorelbeke B, Renders M, Auwerx J, Liekens S, Oki T, Igarashi Y, Schols D. Antimicrob Agents Chemother. 2010 Jan 4. [Epub ahead of print]PMID: 20047920</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20045672?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">A sensitive HIV-1 envelope induced fusion assay identifies fusion enhancement of thrombin.</a>  Cheng DC, Zhong GC, Su JX, Liu YH, Li Y, Wang JY, Hattori T, Ling H, Zhang FM.  Biochem Biophys Res Commun. 2009 Dec 31. [Epub ahead of print]PMID: 20045672</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20043638?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">Synthesis and Structure-Activity Relationships of Azamacrocyclic C-X-C Chemokine Receptor 4 Antagonists: Analogues Containing a Single Azamacrocyclic Ring are Potent Inhibitors of T-Cell Tropic (X4) HIV-1 Replication.</a>  Bridger GJ, Skerlj RT, Hernandez-Abad PE, Bogucki DE, Wang Z, Zhou Y, Nan S, Boehringer EM, Wilson T, Crawford J, Metz M, Hatse S, Princen K, De Clercq E, Schols D.  J Med Chem. 2009 Dec 31. [Epub ahead of print]PMID: 20043638</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20043009?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">Pharmacokinetics and Pharmacodynamics of GS-9350: A Novel Pharmacokinetic Enhancer Without Anti-HIV Activity.</a>  Mathias AA, German P, Murray BP, Wei L, Jain A, West S, Warren D, Hui J, Kearney BP.  Clin Pharmacol Ther. 2009 Dec 30. [Epub ahead of print]PMID: 20043009</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20040702?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">Integrase Inhibitors: A Novel Class of Antiretroviral Agents (January) (CE).</a>  Schafer JJ, Squires KE.  Ann Pharmacother. 2009 Dec 29. [Epub ahead of print]PMID: 20040702</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20039648?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">Oxovanadium(IV) Cyclam and Bicyclam Complexes: Potential CXCR4 Receptor Antagonists.</a>  Ross A, Soares DC, Covelli D, Pannecouque C, Budd L, Collins A, Robertson N, Parsons S, De Clercq E, Kennepohl P, Sadler PJ.  Inorg Chem. 2009 Dec 29. [Epub ahead of print]PMID: 20039648</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20038621?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=8">Stage-Dependent Inhibition of HIV-1 Replication by Antiretroviral Drugs in Cell Culture.</a>  Donahue DA, Sloan RD, Kuhl BD, Bar-Magen T, Schader SM, Wainberg MA.  Antimicrob Agents Chemother. 2009 Dec 28. [Epub ahead of print]PMID: 20038621</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19968236?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=9">Highly functionalized daphnane diterpenoids from Trigonostemon thyrsoideum.</a>  Zhang L, Luo RH, Wang F, Jiang MY, Dong ZJ, Yang LM, Zheng YT, Liu JK.  Org Lett. 2010 Jan 1;12(1):152-5.PMID: 19968236</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19955519?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=10">A high-affinity inhibitor of human CD59 enhances complement-mediated virolysis of HIV-1: implications for treatment of HIV-1/AIDS.</a>  Hu W, Yu Q, Hu N, Byrd D, Amet T, Shikuma C, Shiramizu B, Halperin JA, Qin X.  J Immunol. 2010 Jan 1;184(1):359-68. Epub 2009 Dec 2.PMID: 19955519</p>

    <p class="title"> </p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19937615?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=11">Total synthesis, characterization, and conformational analysis of the naturally occurring hexadecapeptide integramide A and a diastereomer.</a>  De Zotti M, Damato F, Formaggio F, Crisma M, Schievano E, Mammi S, Kaptein B, Broxterman QB, Felock PJ, Hazuda DJ, Singh SB, Kirschbaum J, Brückner H, Toniolo C.  Chemistry. 2010 Jan 4;16(1):316-27.PMID: 19937615</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272647300011">Quantifying the relationship between HIV-1 susceptibility to CCR5 antagonists and virus affinity for antagonist-occupied co-receptor.</a> Buontempo, PJ; Wojcik, L; Buontempo, CA; Ogert, RA; Strizki, JM; Howe, JA; Ralston, R. VIROLOGY, 2009; 395(2): 268-279.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272561000011">Characterisation of a haemagglutinin from Hokkaido red bean (Phaseolus vulgaris cv. Hokkaido red bean).</a> Wong, JH Wan, CT; Ng, TB. L OF THE SCIENCE OF FOOD AND AGRICULTURE, 2010; 90(1): 70-77.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270855100198">The histone deacetylase inhibitor ITF2357 decreases surface CXCR4 and CCR5 expression in CD4+ T-cells and monocytes and induces latent HIV-1 expression in vitro.</a> Matalon, S; Palmer, BE; Nold, MF; Furlan, A; Fossati, G; Mascagni, P; Dinarello, CA. CYTOKINE, 2009; 48(1-2): 61-61.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272763400011">Structure of HIV-1 Reverse Transcriptase with the Inhibitor beta-Thujaplicinol Bound at the RNase H Active Site.</a>  Himmel, DM; Maegley, KA; Pauly, TA; Bauman, JD; Das, K; Dharia, C; Clark, AD; Ryan, K; Hickey, MJ; Love, RA; Hughes, SH; Bergqvist, S; Arnold, E.  STRUCTURE, 2009; 17(12): 1625-1635.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272766600001">Ganoderma lucidum: A Potent Pharmacological Macrofungus.</a> Sanodiya, BS; Thakur, GS; Baghel, RK; Prasad, GBKS; Bisen, PS. CURRENT PHARMACEUTICAL BIOTECHNOLOGY, 2009; 10(8): 717-742.</p>

    <p> </p>

    <p> </p>           
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
