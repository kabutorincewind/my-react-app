

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-08-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="p3AXt0G4VngCw8auorWahM46L60JUlwlfjOtZTqE6KMS4+ENJTAtSE8mRtdQ/pDf10LBS4OZQa88xTH4wzgW5DdVEOU1S5bPn7yq9K6PhEK7Awx+j/AWKZdTV1U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8E9FEAC7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 15 - August 28, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24887590">Triangular Gold Nanoparticles Conjugated with Peptide Ligands: A New Class of Inhibitor for Candida albicans Secreted Aspartyl Proteinase.</a> Jebali, A., F.H. Hajjar, S. Hekmatimoghaddam, B. Kazemi, J.M. De La Fuente, and M. Rashidi. Biochemical Pharmacology, 2014. 90(4): p. 349-355. PMID[24887590].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25010937">Synthesis, Biological Evaluation and Structure-activity Correlation Study of a Series of Imidazol-based Compounds as Candida albicans Inhibitors.</a> Moraca, F., D. De Vita, F. Pandolfi, R. Di Santo, R. Costi, R. Cirilli, F.D. D&#39;Auria, S. Panella, A.T. Palamara, G. Simonetti, M. Botta, and L. Scipione. European Journal of Medicinal Chemistry, 2014. 83: p. 665-673. PMID[25010937].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24992076">Synthesis and Biological Evaluation of Novel N-Aryl maleimide Derivatives Clubbed with alpha-Hydroxyphosphonates.</a> Patil, N.S., G.B. Deshmukh, S.V. Patil, A.D. Bholay, and N.D. Gaikwad. European Journal of Medicinal Chemistry, 2014. 83: p. 490-497. PMID[24992076].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25140804">In Vivo Efficacy of a Synthetic Coumarin Derivative in a Murine Model of Aspergillosis.</a> Singh, S., R. Dabur, M.M. Gatne, B. Singh, S. Gupta, S. Pawar, S.K. Sharma, and G.L. Sharma. Plos One, 2014. 9(8): p. e103039. PMID[25140804].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24747845">Biosynthesis and Characterization of Acalypha indica Mediated Copper Oxide Nanoparticles and Evaluation of its Antimicrobial and Anticancer Activity.</a> Sivaraj, R., P.K. Rahman, P. Rajiv, S. Narendhran, and R. Venckatesh. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2014. 129: p. 255-258. PMID[24747845].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24972340">Design, Synthesis and Docking Studies of Some Novel (R)-2-(4&#39;-Chlorophenyl)-3-(4&#39;-nitrophenyl)-1,2,3,5-tetrahydrobenzo[4,5]imidazo[1,2-c]pyrimidin-4-ol Derivatives as Antitubercular Agents.</a> Barot, K.P., S.V. Jain, N. Gupta, L. Kremer, S. Singh, V.B. Takale, K. Joshi, and M.D. Ghate. European Journal of Medicinal Chemistry, 2014. 83: p. 245-255. PMID[24972340].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24967731">Diarylthiazole: An Antimycobacterial Scaffold Potentially Targeting PrrB-PrrA Two-component System.</a> Bellale, E., M. Naik, V. Vb, A. Ambady, A. Narayan, S. Ravishankar, V. Ramachandran, P. Kaur, R. McLaughlin, J. Whiteaker, S. Morayya, S. Guptha, S. Sharma, A. Raichurkar, D. Awasthy, V. Achar, P. Vachaspati, B. Bandodkar, M. Panda, and M. Chatterji. Journal of Medicinal Chemistry, 2014. 57(15): p. 6572-6582. PMID[24967731].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25010936">Ultrasound-assisted One-pot Four-component Synthesis of Novel 2-Amino-3-cyanopyridine Derivatives Bearing 5-Imidazopyrazole Scaffold and their Biological Broadcast.</a> Kalaria, P.N., S.P. Satasia, J.R. Avalani, and D.K. Raval. European Journal of Medicinal Chemistry, 2014. 83: p. 655-664. PMID[25010936].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25141257">Pharmacokinetics-pharmacodynamics Analysis of Bicyclic 4-Nitroimidazole Analogs in a Murine Model of Tuberculosis.</a> Lakshminarayana, S.B., H.I. Boshoff, J. Cherian, S. Ravindran, A. Goh, J. Jiricek, M. Nanjundappa, A. Nayyar, M. Gurumurthy, R. Singh, T. Dick, F. Blasco, C.E. Barry, 3rd, P.C. Ho, and U.H. Manjunatha. Plos One, 2014. 9(8): p. e105222. PMID[25141257].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24956553">Synthesis and Antiparasitic Activity of New bis-Arylimidamides: DB766 Analogs Modified in the Terminal Groups.</a> Liu, Z.Y., T. Wenzler, R. Brun, X. Zhu, and D.W. Boykin. European Journal of Medicinal Chemistry, 2014. 83: p. 167-173. PMID[24956553].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25124944">In Vitro and in Vivo Anti-malarial Activity of Limonoids Isolated from the Residual Seed Biomass from Carapa guianensis (Andiroba) Oil Production.</a> Pereira, T.B., E.S.L.F. Rocha, R.C. Amorim, M.R. Melo, R.C. Zacardi de Souza, M.N. Eberlin, E.S. Lima, M.C. Vasconcellos, and A.M. Pohlit. Malaria Journal, 2014. 13(1): p. 317. PMID[25124944].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24946216">Synthesis and in Vitro Evaluation of 4-Trichloromethylpyrrolo[1,2-a]quinoxalines as New Antiplasmodial Agents.</a> Primas, N., P. Suzanne, P. Verhaeghe, S. Hutter, C. Kieffer, M. Laget, A. Cohen, J. Broggi, J.C. Lancelot, A. Lesnard, P. Dallemagne, P. Rathelot, S. Rault, P. Vanelle, and N. Azas. European Journal of Medicinal Chemistry, 2014. 83: p. 26-35. PMID[24946216].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25007124">N-Aryl-2-aminobenzimidazoles: Novel, Efficacious, Antimalarial Lead Compounds.</a> Ramachandran, S., P.S. Hameed, A. Srivastava, G. Shanbhag, S. Morayya, N. Rautela, D. Awasthy, S. Kavanagh, S. Bharath, J. Reddy, V. Panduga, K.R. Prabhakar, R. Saralaya, R. Nanduri, A. Raichurkar, S. Menasinakai, V. Achar, M.B. Jimenez-Diaz, M.S. Martinez, I. Angulo-Barturen, S. Ferrer, L.M. Sanz, F.J. Gamo, S. Duffy, V.M. Avery, D. Waterson, M.C. Lee, O. Coburn-Flynn, D.A. Fidock, P.S. Iyer, S. Narayanan, V. Hosagrahara, and V.K. Sambandamurthy. Journal of Medicinal Chemistry, 2014. 57(15): p. 6642-6652. PMID[25007124].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25147607">Identification of Novel Phenyl Butenonyl C-glycosides with Ureidyl and Sulfonamidyl Moieties as Antimalarial Agents.</a> Ramakrishna, K.K., S. Gunjan, A.K. Shukla, V.R. Pasam, V.M. Balaramnavar, A. Sharma, S. Jaiswal, J. Lal, R. Tripathi, Anubhooti, R. Ramachandran, and R.P. Tripathi. ACS Medicinal Chemistry Letters, 2014. 5(8): p. 878-883. PMID[25147607].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25147620">Lead Optimization of Imidazopyrazines: A New Class of Antimalarial with Activity on Plasmodium Liver Stages.</a> Zou, B., A. Nagle, A.K. Chatterjee, S.Y. Leong, L.J. Tan, W.L. Sim, P. Mishra, P. Guntapalli, D.C. Tully, S.B. Lakshminarayana, C.S. Lim, Y.C. Tan, S.N. Abas, C. Bodenreider, K.L. Kuhen, K. Gagaring, R. Borboa, J. Chang, C. Li, T. Hollenbeck, T. Tuntland, A.M. Zeeman, C.H. Kocken, C. McNamara, N. Kato, E.A. Winzeler, B.K. Yeung, T.T. Diagana, P.W. Smith, and J. Roland. ACS Medicinal Chemistry Letters, 2014. 5(8): p. 947-950. PMID[25147620].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25140751">Design, Synthesis and Biological Characterization of Thiazolidin-4-one Derivatives as Promising Inhibitors of Toxoplasma gondii.</a> D&#39;Ascenzio, M., B. Bizzarri, C. De Monte, S. Carradori, A. Bolasco, D. Secci, D. Rivanera, N. Faulhaber, C. Bordon, and L. Jones-Brando. European Journal of Medicinal Chemistry, 2014. 86C: p. 17-30. PMID[25140751].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0815-082814.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338912200005">Synthesis, Characterization and Biological Evaluation of Some Novel Benzimidazole Derivatives.</a> Ahmadi, A. Bulgarian Chemical Communications, 2014. 46(2): p. 245-252. ISI[000338912200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339469500045">Pyrazolylbenzyltriazoles as Cyclooxygenase Inhibitors: Synthesis and Biological Evaluation as Dual Anti-inflammatory and Antimicrobial Agents.</a> Chandna, N., J.K. Kapoor, J. Grover, K. Bairwa, V. Goyal, and S.M. Jachak. New Journal of Chemistry, 2014. 38(8): p. 3662-3672. ISI[000339469500045].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339034600006">Synthesis, Characterization and Antimicrobial Evaluation of 2,5-Disubstituted-4-thiazolidinone Derivatives.</a> Deep, A., S. Jain, P.C. Sharma, S.K. Mittal, P. Phogat, and M. Malhotra. Arabian Journal of Chemistry, 2014. 7(3): p. 287-291. ISI[000339034600006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339259200095">Structure-Activity Relationship Study of the Plant-derived Decapeptide OSIP108 Inhibiting Candida albicans Biofilm Formation.</a> Delattin, N., K. De Brucker, D.J. Craik, O. Cheneval, B. De Coninck, B.P.A. Cammue, and K. Thevissen. Antimicrobial Agents and Chemotherapy, 2014. 58(8): p. 4974-4977. ISI[000339259200095].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339456400011">Synthesis and Antimicrobial Screening of 1,3,4-Oxadiazole and Clubbed Thiophene Derivatives.</a> Desai, N.C., A.M. Dodiya, K.M. Rajpara, and Y.M. Rupala. Journal of Saudi Chemical Society, 2014. 18(3): p. 255-261. ISI[000339456400011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339039100046">Synthesis, Antibacterial and Antitubercular Activities of Benzimidazole Bearing Substituted 2-Pyridone Motifs.</a> Desai, N.C., N.R. Shihory, G.M. Kotadiya, and P. Desai. European Journal of Medicinal Chemistry, 2014. 82: p. 480-489. ISI[000339039100046].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339228700058">Effect of Ferrocene-substituted Porphyrin RL-91 on Candida albicans Biofilm Formation.</a> Lippert, R., S. Vojnovic, A. Mitrovic, N. Jux, I. Ivanovic-Burmazovic, B. Vasiljevic, and N. Stankovic. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3506-3511. ISI[000339228700058].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339457500064">Anticandidal Activity of Cell Extracts from 13 Probiotic Lactobacillus Strains and Characterisation of Lactic Acid and a Novel Fatty acid Derivative from One Strain.</a> Nyanzi, R., M.D. Awouafack, P. Steenkamp, P.J. Jooste, and J.N. Eloff. Food Chemistry, 2014. 164: p. 470-475. ISI[000339457500064].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339456400003">Spectral Characterization and Biological Evaluation of Schiff Bases and their Mixed Ligand Metal Complexes Derived from 4,6-Diacetylresorcinol.</a> Pandya, J.H., R.N. Jadeja, and K.J. Ganatra. Journal of Saudi Chemical Society, 2014. 18(3): p. 190-199. ISI[000339456400003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339133400026">Synthesis, Crystal Structures and Antimicrobial Activity of Dinuclear Copper(II) Complexes with bis-Schiff Bases.</a> Qiu, X.Y. Chinese Journal of Inorganic Chemistry, 2014. 30(7): p. 1667-1672. ISI[000339133400026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339618600027">Characterization of Antibacterial and Hemolytic Activity of Synthetic Pandinin 2 Variants and Their Inhibition against Mycobacterium tuberculosis.</a> Rodriguez, A., E. Villegas, A. Montoya-Rosales, and B. Rivas-Santiago. Plos One, 2014. 9(7): p. e101742. ISI[000339618600027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338979100018">Lead Optimization of 1,4-Azaindoles as Antimycobacterial Agents.</a> Shirude, P.S., R.K. Shandil, M.R. Manjunatha, C. Sadler, M. Panda, V. Panduga, J. Reddy, R. Saralaya, R. Nanduri, A. Ambady, S. Ravishankar, V.K. Sambandamurthy, V. Humnabadkar, L.K. Jena, R.S. Suresh, A. Srivastava, K.R. Prabhakar, J. Whiteaker, R.E. McLaughlin, S. Sharma, C.B. Cooper, K. Mdluli, S. Butler, P.S. Iyer, S. Narayanan, and M. Chatterji. Journal of Medicinal Chemistry, 2014. 57(13): p. 5728-5737. ISI[000338979100018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338845300004">In Vitro Antimicrobial Screening of Methanolic Extraxts of Cleome chelidonii and Cleonme gynandra.</a> Sridhar, N., B. Kiran, D.T. Sasidhar, and L.K. Kanthal. Bangladesh Journal of Pharmacology, 2014. 9(2): p. 161-166. ISI[000338845300004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339138600011">Additivity and Synergy between an Antimicrobial Peptide and Inhibitory Ions.</a> Walkenhorst, W.F., J.N. Sundrud, and J.M. Laviolette. Biochimica Et Biophysica Acta-Biomembranes, 2014. 1838(9): p. 2234-2242. ISI[000339138600011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339540200004">Novel Anticandidal Activity of a Recombinant Lampetra japonica RGD3 Protein.</a> Wu, C.P., L. Lu, Y.Y. Zheng, X. Liu, R. Xiao, J.H. Wang, and Q.W. Li. Journal of Microbiology and Biotechnology, 2014. 24(7): p. 905-913. ISI[000339540200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br />  

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339456400013">Synthesis and Antimicrobial Screening of Tetra Schiff Bases of 1,2,4,5-tetra (5-Amino-1,3,4-thiadiazole-2-yl)benzene.</a> Yousif, E., E. Rentschler, N. Salih, J. Salimon, A. Hameed, and M. Katan. Journal of Saudi Chemical Society, 2014. 18(3): p. 269-275. ISI[000339456400013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0815-082814.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
