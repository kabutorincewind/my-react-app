

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-314.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WNAH08dD45jaLrqur3PFgtc7GmFL2WeOgru7DAYHazAP61LusbRtpENn4XfW1isDxi1Wpomdvo6762mHhMyLp1cvM4z8SZTMpxDSAxCY/XYdjR0C0O11VZ59jzw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="54774A73" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-314-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44755   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Korean medicinal plant extracts exhibit antiviral potency against viral hepatitis</p>

    <p>          Jacob, JR, Korba, BE, You, JE, Tennant, BC, and Kim, YH</p>

    <p>          J Altern Complement Med <b>2004</b>.  10(6): 1019-1026</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673997&amp;dopt=abstract</a> </p><br />

    <p>2.         44756   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Modeling In Vivo Pharmacokinetics and Pharmacodynamics of Moxifloxacin Therapy for Mycobacterium tuberculosis Infection by Using a Novel Cartridge System</p>

    <p>          Ginsburg, AS, Lee, J, Woolwine, SC, Grosset, JH, Hamzeh, FM, and Bishai, WR</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(2): 853-856</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673788&amp;dopt=abstract</a> </p><br />

    <p>3.         44757   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Bactericidal Action of Gatifloxacin, Rifampin, and Isoniazid on Logarithmic- and Stationary-Phase Cultures of Mycobacterium tuberculosis</p>

    <p>          Paramasivan, CN, Sulochana, S, Kubendiran, G, Venkatesan, P, and Mitchison, DA</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(2): 627-631</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673743&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673743&amp;dopt=abstract</a> </p><br />

    <p>4.         44758   OI-LS-314; EMBASE-OI-1/23/2005</p>

    <p class="memofmt1-2">          Electron transfer kinetics and mechanistic study of the thionicotinamide coordinated to the pentacyanoferrate(III)/(II) complexes: a model system for the in vitro activation of thioamides anti-tuberculosis drugs</p>

    <p>          Sousa, Eduardo HS, Pontes, Daniel L, Diogenes, Izaura CN, Lopes, Luiz GF, Oliveira, Jaim S, Basso, Luiz A, Santos, Diogenes S, and Moreira, Icaro S</p>

    <p>          Journal of Inorganic Biochemistry <b>2005</b>.  99(2): 368-375</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGG-4DRBFG5-2/2/8a81c0d7286bf6503c4a1c7e2432fe3c">http://www.sciencedirect.com/science/article/B6TGG-4DRBFG5-2/2/8a81c0d7286bf6503c4a1c7e2432fe3c</a> </p><br />

    <p>5.         44759   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Mutation of tlyA Confers Capreomycin Resistance in Mycobacterium tuberculosis</p>

    <p>          Maus, CE, Plikaytis, BB, and Shinnick, TM</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(2): 571-577</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673735&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673735&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44760   OI-LS-314; EMBASE-OI-1/23/2005</p>

    <p class="memofmt1-2">          Ziziphine N, O, P and Q, new antiplasmodial cyclopeptide alkaloids from Ziziphus oenoplia var. brunoniana</p>

    <p>          Suksamrarn, Sunit, Suwannapoch, Narisara, Aunchai, Natthachai, Kuno, Mayuso, Ratananukul, Piniti, Haritakun, Rachada, Jansakul, Chawewan, and Ruchirawat, Somsak</p>

    <p>          Tetrahedron <b>2005</b>.  61(5): 1175-1180</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4F1504V-1/2/56b571250eaf583201cc645429d1bde4">http://www.sciencedirect.com/science/article/B6THR-4F1504V-1/2/56b571250eaf583201cc645429d1bde4</a> </p><br />

    <p>7.         44761   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Antifungal Drugs</p>

    <p>          Anon</p>

    <p>          Treat Guidel Med Lett <b>2005</b>.  3(30): 7-14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15671963&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15671963&amp;dopt=abstract</a> </p><br />

    <p>8.         44762   OI-LS-314; EMBASE-OI-1/23/2005</p>

    <p class="memofmt1-2">          Design, development and synthesis of mixed bioconjugates of piperic acid-glycine, curcumin-glycine/alanine and curcumin-glycine-piperic acid and their antibacterial and antifungal properties</p>

    <p>          Mishra, Satyendra, Narain, Upma, Mishra, Roli, and Misra, Krishna</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FB93PD-1/2/b9bb40e0e6be2e7c9adf58da3ba71846">http://www.sciencedirect.com/science/article/B6TF8-4FB93PD-1/2/b9bb40e0e6be2e7c9adf58da3ba71846</a> </p><br />

    <p>9.         44763   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Successful treatment of resistant chronic hepatitis C virus infection with high dose pegylated interferon</p>

    <p>          Khokhar, N, Butt, MB, and Gill, ML</p>

    <p>          J Coll Physicians Surg Pak <b>2005</b>.  15(1): 41-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15670525&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15670525&amp;dopt=abstract</a> </p><br />

    <p>10.       44764   OI-LS-314; EMBASE-OI-1/23/2005</p>

    <p class="memofmt1-2">          DAHP synthase from Mycobacterium tuberculosis H37Rv: cloning, expression, and purification of functional enzyme</p>

    <p>          Rizzi, Caroline, Frazzon, Jeverson, Ely, Fernanda, Weber, Patricia G, da Fonseca, Isabel O, Gallas, Michelle, Oliveira, Jaim S, Mendes, Maria A, de Souza, Bibiana M, and Palma, Mario S</p>

    <p>          Protein Expression and Purification <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WPJ-4F00RT8-1/2/d26e8c36daf69be29a6e9168e5c2f605">http://www.sciencedirect.com/science/article/B6WPJ-4F00RT8-1/2/d26e8c36daf69be29a6e9168e5c2f605</a> </p><br />
    <br clear="all">

    <p>11.       44765   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          D-3-phosphoglycerate dehydrogenase from mycobacterium tuberculosis is a link between the E. coli and mammalian enzymes</p>

    <p>          Dey, S, Hu, Z, Xu, XL, Sacchettini, JC, and Grant, GA</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15668250&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15668250&amp;dopt=abstract</a> </p><br />

    <p>12.       44766   OI-LS-314; EMBASE-OI-1/23/2005</p>

    <p class="memofmt1-2">          Pyrosequencing-based approach for rapid detection of rifampin-resistant Mycobacterium tuberculosis</p>

    <p>          Zhao, Jin Rong, Bai, Yu Jie, Zhang, Qing Hua, Wang, Yan, Luo, Ming, and Yan, Xiao Jun</p>

    <p>          Diagnostic Microbiology and Infectious Disease <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T60-4F7Y9BC-2/2/c186d21984f8b1c037e1cfe78db49541">http://www.sciencedirect.com/science/article/B6T60-4F7Y9BC-2/2/c186d21984f8b1c037e1cfe78db49541</a> </p><br />

    <p>13.       44767   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Role of Sulfated Glycans in Adherence of the Microsporidian Encephalitozoon intestinalis to Host Cells In Vitro</p>

    <p>          Hayman, JR, Southern, TR, and Nash, TE</p>

    <p>          Infect Immun <b>2005</b>.  73(2): 841-848</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664924&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664924&amp;dopt=abstract</a> </p><br />

    <p>14.       44768   OI-LS-314; EMBASE-OI-1/23/2005</p>

    <p class="memofmt1-2">          Synthesis of Schiff bases of 4-(4-aminophenyl)-morpholine as potential antimicrobial agents</p>

    <p>          Panneerselvam, Perumal, Nair, Rajasree R, Vijayalakshmi, Gudaparthi, Subramanian, Ekambaram Harihara, and Sridhar, Seshaiah Krishnan</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4F6CRHW-1/2/01f9e87218fc8630ea5dca86ee922f02">http://www.sciencedirect.com/science/article/B6VKY-4F6CRHW-1/2/01f9e87218fc8630ea5dca86ee922f02</a> </p><br />

    <p>15.       44769   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Antiproliferative synergism of azasterols and antifolates against Toxoplasma gondii</p>

    <p>          Dantas-Leite, L, Urbina, JA, Souza, W, and Vommaro, RC</p>

    <p>          Int J Antimicrob Agents <b>2005</b>.  25(2): 130-135</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664482&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.       44770   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          The High-resolution Structure of LeuB (Rv2995c) from Mycobacterium tuberculosis</p>

    <p>          Singh, RK, Kefala, G, Janowski, R, Mueller-Dieckmann, C, von, Kries JP, and Weiss, MS</p>

    <p>          J Mol Biol <b>2005</b>.  346(1): 1-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15663922&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15663922&amp;dopt=abstract</a> </p><br />

    <p>17.       44771   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Synthesis and preliminary in vitro evaluation of antimycobacterial activity of new pyrrolo[1,2-a] quinoxaline-carboxylic acid hydrazide derivatives</p>

    <p>          Guillon, J, Reynolds, RC, Leger, JM, Guie, MA, Massip, S, Dallemagne, P, and Jarry, C</p>

    <p>          J Enzyme Inhib Med Chem <b>2004</b>.  19(6): 489-495</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662953&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15662953&amp;dopt=abstract</a> </p><br />

    <p>18.       44772   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of helioxanthin analogues</p>

    <p>          Yeo, H, Li, Y, Fu, L, Zhu, JL, Gullen, EA , Dutschman, GE, Lee, Y, Chung, R, Huang, ES, Austin, DJ, and Cheng, YC</p>

    <p>          J Med Chem <b>2005</b>.  48(2): 534-546</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15658867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15658867&amp;dopt=abstract</a> </p><br />

    <p>19.       44773   OI-LS-314; EMBASE-OI-1/23/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-viral activity of a series of D- and L-2&#39;-deoxy-2&#39;-fluororibonucleosides in the subgenomic HCV replicon system</p>

    <p>          Shi, Junxing, Du, Jinfa, Ma, Tianwei, Pankiewicz, Krzysztof W, Patterson, Steven E, Tharnish, Phillip M, McBrayer, Tamara R, Stuyver, Lieven J, Otto, Michael J, and Chu, Chung K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4F7B3YX-2/2/910ab35de4553066eb91451c41c0889e">http://www.sciencedirect.com/science/article/B6TF8-4F7B3YX-2/2/910ab35de4553066eb91451c41c0889e</a> </p><br />

    <p>20.       44774   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Isoniazid resistance and the future of drug-resistant tuberculosis</p>

    <p>          Cohen, T, Becerra, MC, and Murray, MB</p>

    <p>          Microb Drug Resist <b>2004</b>.  10(4): 280-285</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650371&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15650371&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.       44775   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity of phenothiazines</p>

    <p>          Amaral, L, Viveiros, M, and Molnar, J</p>

    <p>          In Vivo <b>2004</b> .  18(6): 725-731</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646813&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646813&amp;dopt=abstract</a> </p><br />

    <p>22.       44776   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Disruption of ergosterol biosynthesis, growth, and the morphological transition in Candida albicans by sterol methyltransferase inhibitors containing sulfur at C-25 in the sterol side chain</p>

    <p>          Kanagasabai, R, Zhou, W, Liu, J, Nguyen, TT, Veeramachaneni, P, and Nes, WD</p>

    <p>          Lipids <b>2004</b>.  39(8): 737-746</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15638241&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15638241&amp;dopt=abstract</a> </p><br />

    <p>23.       44777   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Nucleotides and pronucleotides of 2,2-bis(hydroxymethyl)methylenecyclopropane analogues of purine nucleosides: synthesis and antiviral activity</p>

    <p>          Yan, Z, Kern, ER, Gullen, E, Cheng, YC, Drach, JC, and Zemlicka, J</p>

    <p>          J Med Chem <b>2005</b>.  48(1): 91-99</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634003&amp;dopt=abstract</a> </p><br />

    <p>24.       44778   OI-LS-314; PUBMED-OI-1/28/2005</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis KatG(S315T) catalase-peroxidase retains all active site properties for proper catalytic function</p>

    <p>          Kapetanaki, SM, Chouchane, S, Yu, S , Zhao, X, Magliozzo, RS, and Schelvis, JP</p>

    <p>          Biochemistry <b>2005</b>.  44(1): 243-252</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15628865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15628865&amp;dopt=abstract</a> </p><br />

    <p>25.       44779   OI-LS-314; WOS-OI-1/16/2005</p>

    <p class="memofmt1-2">          Pharmacology and structure-activity relationships of bioactive polycyclic cage compounds: A focus on pentacycloundecane derivatives</p>

    <p>          Geldenhuys, WJ, Malan, SF, Bloomquist, JR, Marchand, AP, and Van, der Schyf CJ</p>

    <p>          MEDICINAL RESEARCH REVIEWS <b>2005</b>.  25(1): 21-48, 28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225701400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225701400002</a> </p><br />

    <p>26.       44780   OI-LS-314; WOS-OI-1/16/2005</p>

    <p class="memofmt1-2">          Sterol metabolism in the opportunistic pathogen Pneumocystis: Advances and new insights</p>

    <p>          Kaneshiro, ES</p>

    <p>          LIPIDS <b>2004</b>.  39 (8): 753-761, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225616100006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225616100006</a> </p><br />

    <p>27.       44781   OI-LS-314; WOS-OI-1/16/2005</p>

    <p class="memofmt1-2">          Iron chelator daphnetin against Pneumocystis carinii in vitro</p>

    <p>          Ye, B, Zheng, YQ, Wu, WH, and Zhang, J</p>

    <p>          CHINESE MEDICAL JOURNAL <b>2004</b>.  117(11): 1704-1708, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225673700019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225673700019</a> </p><br />

    <p>28.       44782   OI-LS-314; WOS-OI-1/16/2005</p>

    <p class="memofmt1-2">          Meta-analysis: a randomized trial of peginterferon plus ribavirin for the initial treatment of chronic hepatitis C genotype 4 (vol 20, pg 931, 2004)</p>

    <p>          Khuroo, MS, Khuroo, MS, and Dahab, ST</p>

    <p>          ALIMENTARY PHARMACOLOGY &amp; THERAPEUTICS <b>2004</b>.  20(11-12): 1388-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225684400019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225684400019</a> </p><br />

    <p>29.       44783   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          Epidemiology of microsporidiosis: sources and modes of transmission</p>

    <p>          Didier, ES, Stovall, ME, Green, LC, Brindley, PJ, Sestak, K, and Didier, P</p>

    <p>          VETERINARY PARASITOLOGY <b>2004</b>.  126(1-2): 145-166, 22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225824800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225824800009</a> </p><br />

    <p>30.       44784   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          Discovery of pyrano[3,4-b]indoles as potent and selective HCVNS5B polymerase inhibitors</p>

    <p>          Gopalsamy, A, Lim, K, Ciszewski, G, Park, K, Ellingboe, JW, Bloom, J, Insaf, S, Upeslacis, J, Mansour, TS, Krislinamurthy, G, Damarla, M, Pyatski, Y, Ho, D, Howe, AYM, Orlowski, M, Feld, B, and O&#39;Connell, J</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2004</b>.  47(26): 6603-6608, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225748500022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225748500022</a> </p><br />

    <p>31.       44785   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          (Z)- and (E)-[2-f uoro-2-(hydroxymethyl)cyclopropylidene]methylpurines and -pyrimidines, a new class of methylenecyclopropane analogues of nucleosides: Synthesis and antiviral activity</p>

    <p>          Zhou, SM, Kern, ER, Gullen, E, Cheng, YC, Drach, JC, Matsumi, S, Mitsuya, H, and Zemlicka, J</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2004</b>.  47(27): 6964-6972, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225952400035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225952400035</a> </p><br />

    <p>32.       44786   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          16 alpha-bromoepiandrosterone restores T helper cell type 1 activity and accelerates chemotherapy-induced bacterial clearance in a model of progressive pulmonary tuberculosis</p>

    <p>          Hernandez-Pando, R, Aguilar-Leon, D, Orozco, H, Serrano, A, Ahlem, C, Trauer, R, Schramm, B, Reading, C, Fincke, J, and Rook, GAW</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2005</b>.  191(2): 299-306, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225889500020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225889500020</a> </p><br />

    <p>33.       44787   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          A new triterpene from Luculia pinciana Hook</p>

    <p>          Kang, WY, Du, ZZ, Yang, XS, and Hao, XJ</p>

    <p>          JOURNAL OF ASIAN NATURAL PRODUCTS RESEARCH <b>2005</b>.  7(1): 91-94, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225842200016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225842200016</a> </p><br />

    <p>34.       44788   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          Antimicrobial peptides: premises and promises</p>

    <p>          Reddy, KVR, Yedery, RD, and Aranha, C</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2004</b>.  24(6): 536-547, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225925600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225925600002</a> </p><br />

    <p>35.       44789   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          Co-induction of methyltransferase Rv0560c by naphthoquinones and fibric acids suggests attenuation of isoprenoid quinone action in Mycobacterium tuberculosis</p>

    <p>          Garbe, TR</p>

    <p>          CANADIAN JOURNAL OF MICROBIOLOGY <b>2004</b>.  50(10): 771-778, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225904800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225904800001</a> </p><br />

    <p>36.       44790   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          Aminothiazole inhibitors of HCV RNA polymerase</p>

    <p>          Shipps, GW, Deng, YQ, Wang, T, Popovici-Muller, J, Curran, PJ, Rosner, KE, Cooper, AB, Girijavallabhan, V, Butkiewicz, N, and Cable, M</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(1): 115-119, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225773600021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225773600021</a> </p><br />

    <p>37.       44791   OI-LS-314; WOS-OI-1/23/2005</p>

    <p class="memofmt1-2">          Avoiding safety problems with drug candidates: Issues and approaches</p>

    <p>          Guengerich, FP </p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  227: U42-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655700193">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655700193</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
