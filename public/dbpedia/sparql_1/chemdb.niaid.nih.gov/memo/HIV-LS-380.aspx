

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-380.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wpffnWgRHb6oVCk0AW5EJX9Hno/luzHEXxcIX8Fo0LmBDJVZ8PwKMVTkeF3FYWKqjdzS8uoeHBXstUTDcf8a3LhG/fjk8LeKzYUVBVyw2CVJSWeAi79gRx2kyo4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E01507BF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-380-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61404   HIV-LS-380; EMBASE-HIV-8/6/2007</p>

    <p class="memofmt1-2">          The Nucleoside Analogs 4&#39;C-Methyl Thymidine and 4&#39;C-Ethyl Thymidine Block DNA Synthesis by Wild-type HIV-1 RT and Excision Proficient NRTI Resistant RT Variants</p>

    <p>          Boyer, Paul L, Julias, John G, Ambrose, Zandrea, Siddiqui, Maqbool A, Marquez, Victor E, and Hughes, Stephen H</p>

    <p>          Journal of Molecular Biology <b>2007</b>.  371(4): 873-882</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4NS2J7P-1/2/8430d4c29634638659c2c9de6c8563ef">http://www.sciencedirect.com/science/article/B6WK7-4NS2J7P-1/2/8430d4c29634638659c2c9de6c8563ef</a> </p><br />

    <p>2.     61405   HIV-LS-380; EMBASE-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Cell surface expression of CCR5 and other host factors influence the inhibition of HIV-1 infection of human lymphocytes by CCR5 ligands</p>

    <p>          Ketas, Thomas J, Kuhmann, Shawn E, Palmer, Ashley, Zurita, Juan, He, Weijing, Ahuja, Sunil K, Klasse, Per Johan, and Moore, John P</p>

    <p>          Virology <b>2007</b>.  364(2): 281-290</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NFXG6Y-1/2/49a805791bb2c71ec09a66770920c924">http://www.sciencedirect.com/science/article/B6WXR-4NFXG6Y-1/2/49a805791bb2c71ec09a66770920c924</a> </p><br />

    <p>3.     61406   HIV-LS-380; EMBASE-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Synthesis of novel HIV protease inhibitors (PI) with activity against PI-resistant virus</p>

    <p>          Raghavan, Subharekha, Lu, Zhijian, Beeson, Teresa, Chapman, Kevin T, Schleif, William A, Olsen, David B, Stahlhut, Mark, Rutkowski, Carrie A, Gabryelski, Lori, Emini, Emilio, and Tata, James R</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 453</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4P9SNC1-1/2/25c7783472346256565d636417d6851d">http://www.sciencedirect.com/science/article/B6TF9-4P9SNC1-1/2/25c7783472346256565d636417d6851d</a> </p><br />

    <p>4.     61407   HIV-LS-380; WOS-HIV-8/3/2007</p>

    <p class="memofmt1-2">          Nitroimidazoles, Part 3. Synthesis and Anti-Hiv Activity of New N-Alkyl-4-Nitroimidazoles Bearing Benzothiazole and Benzoxazole Backbones</p>

    <p>          Al-Soud, Y. <i>et al.</i></p>

    <p>          Zeitschrift Fur Naturforschung Section B-a Journal of Chemical Sciences <b>2007</b>.  62(4): 523-528</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247015500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247015500006</a> </p><br />

    <p>5.     61408   HIV-LS-380; EMBASE-HIV-8/6/2007</p>

    <p class="memofmt1-2">          In vitro fidelity of the prototype primate foamy virus (PFV) RT compared to HIV-1 RT</p>

    <p>          Boyer, Paul L, Stenbak, Carolyn R, Hoberman, David, Linial, Maxine L, and Hughes, Stephen H</p>

    <p>          Virology <b>2007</b>.  In Press, Corrected Proof: 353</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4P5RVR0-2/2/128d900378297cc67a93a1684388a15f">http://www.sciencedirect.com/science/article/B6WXR-4P5RVR0-2/2/128d900378297cc67a93a1684388a15f</a> </p><br />
    <br clear="all">

    <p>6.     61409   HIV-LS-380; EMBASE-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Quantitative structure activity relationship study on EC50 of anti-HIV drugs</p>

    <p>          Hongzong, Si, Shuping, Yuan, Kejun, Zhang, Aiping, Fu, Yun-Bo, Duan, and Zhide, Hu</p>

    <p>          Chemometrics and Intelligent Laboratory Systems <b>2007</b>.  In Press, Accepted Manuscript: 353</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TFP-4P4NPBY-1/2/1df63d4bd3ed6dac5d3eaa1ccb30d478">http://www.sciencedirect.com/science/article/B6TFP-4P4NPBY-1/2/1df63d4bd3ed6dac5d3eaa1ccb30d478</a> </p><br />

    <p>7.     61410   HIV-LS-380; EMBASE-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Theoretical study on the HIV-1 integrase inhibitor 1-(5-chloroindol-3-yl)-3-hydroxy-3-(2H-tetrazol-5-yl)-propenone (5CITEP)</p>

    <p>          Nunthaboot, Nadtanet, Pianwanit, Somsak, Parasuk, Vudhichai, Kokpol, Sirirat, and Wolschann, Peter</p>

    <p>          Journal of Molecular Structure <b>2007</b>.  In Press, Accepted Manuscript: 88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGS-4P429HN-1/2/e72a255d79a105d045fa4a86c58c58b0">http://www.sciencedirect.com/science/article/B6TGS-4P429HN-1/2/e72a255d79a105d045fa4a86c58c58b0</a> </p><br />

    <p>8.     61411   HIV-LS-380; EMBASE-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Recombination favors the evolution of drug resistance in HIV-1 during antiretroviral therapy</p>

    <p>          Carvajal-Rodriguez, Antonio, Crandall, Keith A, and Posada, David</p>

    <p>          Infection, Genetics and Evolution <b>2007</b>.  7(4): 476-483</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W8B-4N1T1T5-1/2/4bb90b208a85728074792990e0108ed4">http://www.sciencedirect.com/science/article/B6W8B-4N1T1T5-1/2/4bb90b208a85728074792990e0108ed4</a> </p><br />

    <p>9.     61412   HIV-LS-380; WOS-HIV-8/3/2007</p>

    <p class="memofmt1-2">          A New Cadinane Sesquiterpene With Significant Anti-Hiv-1 Activity From the Cultures of the Basidiomycete Tyromyces Chioneus</p>

    <p>          Liu, D. <i>et al.</i></p>

    <p>          Journal of Antibiotics <b>2007</b>.  60(5): 332-334</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247117400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247117400007</a> </p><br />

    <p>10.   61413   HIV-LS-380; WOS-HIV-8/3/2007</p>

    <p class="memofmt1-2">          Synthesis and Pharmacological Activity of Urea and Thiourea Derivatives of 4-Azatricyclo[5.2.2.0(2,6)] Undec-8-Ene-3,5-Dione</p>

    <p>          Struga, M. <i>et al.</i></p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2007</b>.  55(5): 796-799</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247078000016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247078000016</a> </p><br />

    <p>11.   61414   HIV-LS-380; WOS-HIV-8/3/2007</p>

    <p class="memofmt1-2">          Qsar Analysis of 5,6-Dihydro-4-Hydroxy-2-Pyrone Analogues: Hiv-1 Protease Inhibitor</p>

    <p>          Jaiswal, M. <i>et al.</i></p>

    <p>          Asian Journal of Chemistry <b>2007</b>.  19(5): 4110-4112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246955400117">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246955400117</a> </p><br />
    <br clear="all">

    <p>12.   61415   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Griffithsin, a potent HIV entry inhibitor, is an excellent candidate for anti-HIV microbicide</p>

    <p>          Emau, P, Tian, B, O&#39;keefe, BR, Mori, T, McMahon, JB, Palmer, KE, Jiang, Y, Bekele, G, and Tsai, CC</p>

    <p>          J Med Primatol  <b>2007</b>.  36(4-5): 244-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17669213&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17669213&amp;dopt=abstract</a> </p><br />

    <p>13.   61416   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Three-year immune reconstitution in PI-sparing and PI-containing antiretroviral regimens in advanced HIV-1 disease</p>

    <p>          Samri, A, Goodall, R, Burton, C, Imami, N, Pantaleo, G, Kelleher, A, Poli, G, Gotch, F, and Autran, B</p>

    <p>          Antivir Ther <b>2007</b>.  12(4): 553-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668564&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668564&amp;dopt=abstract</a> </p><br />

    <p>14.   61417   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Some HIV antiretrovirals increase oxidative stress and alter chemokine, cytokine or adiponectin production in human adipocytes and macrophages</p>

    <p>          Lagathu, C, Eustace, B, Prot, M, Frantz, D, Gu, Y, Bastard, JP, Maachi, M, Azoulay, S, Briggs, M, Caron, M, and Capeau, J</p>

    <p>          Antivir Ther <b>2007</b>.  12(4): 489-500</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668557&amp;dopt=abstract</a> </p><br />

    <p>15.   61418   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Synthesis, Biological Activity, and Crystal Structure of Potent Nonnucleoside Inhibitors of HIV-1 Reverse Transcriptase That Retain Activity against Mutant Forms of the Enzyme</p>

    <p>          Morningstar, ML, Roth, T, Farnsworth, DW, Smith, MK, Watson, K, Buckheit, RW Jr, Das, K, Zhang, W, Arnold, E, Julias, JG, Hughes, SH, and Michejda, CJ</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17663538&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17663538&amp;dopt=abstract</a> </p><br />

    <p>16.   61419   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Systematic evaluation of allele-specific real-time PCR for the detection of minor HIV-1 variants with pol and env resistance mutations</p>

    <p>          Paredes, R, Marconi, VC, Campbell, TB, and Kuritzkes, DR</p>

    <p>          J Virol Methods <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17662474&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17662474&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   61420   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Immunomodulatory effects of two HIV protease inhibitors, Saquinavir and Ritonavir, on lymphocytes from healthy seronegative individuals</p>

    <p>          Delmonte, OM, Bertolotto, G, Ricotti, E, and Tovo, PA</p>

    <p>          Immunol Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17659786&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17659786&amp;dopt=abstract</a> </p><br />

    <p>18.   61421   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          Identification and Characterization of UK-201844, A Novel Inhibitor That Interferes with HIV-1 gp160 Processing</p>

    <p>          Blair, WS, Cao, J, Jackson, L, Jimenez, J, Peng, Q, Wu, H, Isaacson, J, Butler, S, Chu, A, Graham, J, Malfait, AM, Tortorella, M, and Patick, AK</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17646410&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17646410&amp;dopt=abstract</a> </p><br />

    <p>19.   61422   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          EVALUATION OF HEXADECYLOXYPROPYL-9-R-[2-(PHOSPHONOMETHOXY)PROPYL]-ADENINE, CMX157, AS A POTENTIAL TREATMENT OF HIV-1 AND HEPATITIS B VIRUS INFECTIONS</p>

    <p>          Painter, GR, Almond, MR, Trost, LC, Lampert, BM, Neyts, J, De, Clercq E, Korba, BE, Aldern, KA, Beadle, JR, and Hostetler, KY</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17646420&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17646420&amp;dopt=abstract</a> </p><br />

    <p>20.   61423   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          The antimicrobial peptide LL-37 inhibits HIV-1 replication</p>

    <p>          Bergman, P, Walter-Jallow, L, Broliden, K, Agerberth, B, and Soderlund, J</p>

    <p>          Curr HIV Res <b>2007</b>.  5(4): 410-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627504&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627504&amp;dopt=abstract</a> </p><br />

    <p>21.   61424   HIV-LS-380; PUBMED-HIV-8/6/2007</p>

    <p class="memofmt1-2">          DDB1 and Cul4A are required for HIV-1 Vpr-induced G2 arrest</p>

    <p>          Tan, L, Ehrlich, E, and Yu, XF</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626091&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626091&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
