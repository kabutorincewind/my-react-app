

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-03-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="LIA+XaMlkvq/CKgn0mQawus/FHtJw+Z68izK2j/vrawve5OQGbG52YOD68A0GM2swuWMMnW6gDDVx6AZ1DQgI4Vn4ZhzZpodkBrxhN4QiKzYSl3+aItjUHDFgiw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8815DB0C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 17 - March 3, 2010</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19830655?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Anti-HIV-1 and Anti-Inflammatory Lupanes from the Leaves, Twigs, and Resin of Garcinia hanburyi.</a> Reutrakul V, Anantachoke N, Pohmakotr M, Jaipetch T, Yoosook C, Kasisit J, Napaswa C, Panthong A, Santisuk T, Prabpai S, Kongsaeree P, Tuchinda P. Planta Med. 2010 Mar;76(4):368-371. Epub 2009 Oct 14.PMID: 19830655</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19995673?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">Synthesis and biological activity of derivatives of the marine quinone avarone.</a> Bozi&#263; T, Novakovi&#263; I, Gasi&#263; MJ, Jurani&#263; Z, Stanojkovi&#263; T, Tufegdzi&#263; S, Kljaji&#263; Z, Sladi&#263; D. Eur J Med Chem. 2010 Mar;45(3):923-9. Epub 2009 Nov 26.PMID: 19995673</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20023216?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">CCR6 ligands inhibit HIV by inducing APOBEC3G.</a>Lafferty MK, Sun L, Demasi L, Lu W, Garzino-Demo A. Blood. 2010 Feb 25;115(8):1564-71. Epub 2009 Dec 18.PMID: 20023216</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20034711?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">Synthesis, antibacterial and antiviral properties of curcumin bioconjugates bearing dipeptide, fatty acids and folic acid.</a>Singh RK, Rai D, Yadav D, Bhargava A, Balzarini J, De Clercq E. Eur J Med Chem. 2010 Mar;45(3):1078-86. Epub 2010 Jan 19.PMID: 20034711</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20137928?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">N1-Heterocyclic pyrimidinediones as non-nucleoside inhibitors of HIV-1 reverse transcriptase.</a> Mitchell ML, Son JC, Lee IY, Lee CK, Kim HS, Guo H, Wang J, Hayes J, Wang M, Paul A, Lansdon EB, Chen JM, Eisenberg G, Geleziunas R, Xu L, Kim CU. Bioorg Med Chem Lett. 2010 Mar 1;20(5):1585-1588. Epub 2010 Jan 21.PMID: 20137928</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20166763?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">Naturally Occurring Variability in the Envelope Glycoprotein of HIV-1 and Development of Cell Entry Inhibitors.</a> Brower ET, Scho&#776;n A, Freire E. Biochemistry. 2010 Feb 26. [Epub ahead of print]PMID: 20166763</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20171172?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=8">Mechanism of action of the HIV-1 integrase inhibitory peptide LEDGF 361-370.</a> Hayouka Z, Levin A, Maes M, Hadas E, Shalev DE, Volsky DJ, Loyter A, Friedler A. Biochem Biophys Res Commun. 2010 Feb 18. [Epub ahead of print]PMID: 20171172</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20172044?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=9">Inhibition of active HIV-1 replication by NF-kappaB inhibitor DHMEQ.</a> Miyake A, Ishida T, Yamagishi M, Hara T, Umezawa K, Watanabe T, Horie R. Microbes Infect. 2010 Feb 18. [Epub ahead of print]PMID: 20172044</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20194691?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=10">3-Hydroxyphthalic Anhydride-modified Chicken Ovalbumin Exhibits Potent and Broad Anti-HIV-1 Activity: a Potential Microbicide for Preventing Sexual Transmission of HIV-1.</a> Li L, He L, Tan S, Guo X, Lu H, Qi Z, Pan C, An X, Jiang S, Liu S. Antimicrob Agents Chemother. 2010 Mar 1. PMID: 20194691</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20195818?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=11">Design and synthesis of 3&#39;-fluoropenciclovir analogues as antiviral agents.</a> Choi MH, Lee CK, Kim HD. Arch Pharm Res. 2010 Feb;33(2):197-202. Epub 2010 Feb 24.PMID: 20195818</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20196122?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=12">Solution structure of LC5, the CCR5- derived peptide for HIV-1 inhibition.</a> Miyamoto K, Togiya K, Kitahara R, Akasaka K, Kuroda Y. J Pept Sci. 2010 Mar 1. [Epub ahead of print]PMID: 20196122</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274113500003">Anti-HIV Activity of New Substituted 1,3,4-Oxadiazole Derivatives and their Acyclic Nucleoside Analogues.</a>  El-Sayed, WA; El-Essawy, FA; Ali, OM; Nasr, BS; Abdalla, MM; Abdel-Rahman, AAH.  ZEITSCHRIFT FUR NATURFORSCHUNG SECTION C-A JOURNAL OF BIOSCIENCES, 2009; 64(11-12): 773-778.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274330300034">The Potent Anti-HIV Activity of CXCL12 gamma Correlates with Efficient CXCR4 Binding and Internalization.</a>  Altenburg, JD; Jin, QW; Alkhatib, B; Alkhatib, G.  JOURNAL OF VIROLOGY, 2010; 84(5): 2563-2572.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274270900045">Highly Suppressing Wild-Type HIV-1 and Y181C Mutant HIV-1 Strains by 10-Chloromethyl-11-demethyl-12-oxo-calanolide A with Druggable Profile.</a>  Xue, H; Lu, XF; Zheng, PR; Liu, L; Han, CY; Hu, JP; Liu, ZJ; Ma, T; Li, Y; Wang, L; Chen, ZW; Liu, G.  JOURNAL OF MEDICINAL CHEMISTRY, 2010; 53(3): 1397-1401.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274203000021">Design and synthesis of novel P2 substituents in diol-based HIV protease inhibitors.</a>  Meredith, JA; Wallberg, H; Vrang, L; Oscarson, S; Parkes, K; Hallberg, A; Samuelsson, B.  EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY, 2010; 45(1): 160-170.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274052000005">Synthesis and Microbiological Activity of Thiourea Derivatives of 4-Azatricyclo[5.2.2.0(2,6)]undec-8-ene-3,5-dione.</a>  Struga, M; Rosolowski, S; Kossakowski, J; Stefanska, J.  ARCHIVES OF PHARMACAL RESEARCH, 2010; 33(1): 47-54.</p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274375500003">Molecular cloning, over expression, and activity studies of a peptidic HIV-1 protease inhibitor: Designed synthetic gene to functional recombinant peptide.</a>  Vathipadiekal, V; Umasankar, PK; Patole, MS; Rao, M.  PEPTIDES, 2010; 31(1): 16-21.</p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274436100004">Trichoderone, a novel cytotoxic cyclopentenone and cholesta-7, 22-diene-3 beta, 5 alpha, 6 beta-triol, with new activities from the marine-derived fungus Trichoderma sp.</a>  You, JL; Dai, HQ; Chen, ZH; Liu, GJ; He, ZX; Song, FH; Yang, X; Fu, HA; Zhang, LX; Chen, XP.  JOURNAL OF INDUSTRIAL MICROBIOLOGY &amp; BIOTECHNOLOGY, 2010; 37(3): 245-252.</p>

    <p>19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274286500163">Research Introduction of lamellarin alpha 20-Sulfate, a selective inhibitor of HIV-1 integrase.</a>  Jin, YS; Wang, AL; Zhang, XH; Li, DP; You, YC.  PROCEEDINGS OF 2009 INTERNATIONAL CONFERENCE OF NATURAL PRODUCT AND TRADITIONAL MEDICINE, VOLS 1 AND 2, 2009; 696-698.</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274425500035">Novel 1,3-dihydro-benzimidazol-2-ones and their analogues as potent non-nucleoside HIV-1 reverse transcriptase inhibitors.</a> Monforte, AM; Logoteta, P; De Luca, L; Iraci, N; Ferro, S; Maga, G; De Clercq, E; Pannecouque, C; Chimirri, A. BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(4): 1702-1710.</p>

    <p> </p>

    <p class="memofmt2-1">Other citations:</p>

    <p class="title">21.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/16124119?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=19">Drug interactions and anti-HIV therapy.</a>  Highleyman L. BETA. 2005 Summer;17(4):20-9. No abstract available. PMID: 16124119</p>

    <p class="title">22.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/11365998?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=58">ABT-378: a second generation protease inhibitor.</a>Highleyman L. BETA. 1998 Oct:8, 55.PMID: 11365998</p>

    <p class="title">23.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/9145853?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Antiviral properties of palinavir, a potent inhibitor of the human immunodeficiency virus type 1 protease.</a>  Lamarre D, Croteau G, Wardrop E, Bourgon L, Thibeault D, Clouette C, Vaillancourt M, Cohen E, Pargellis C, Yoakim C, Anderson PC.  Antimicrob Agents Chemother. 1997 May;41(5):965-71.PMID: 9145853</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
