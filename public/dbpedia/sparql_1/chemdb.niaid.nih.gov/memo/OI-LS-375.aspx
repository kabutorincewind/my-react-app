

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-375.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lb4jMXYVgfe2c/1FtehwoCowVtkECQfZkbIIeSm6Xe0FAPebDRAoYVKlIAY2aa3cO3L0XwQ5tSpkM83QvbTMCb3qmIANs1AxTEwND0k1s88AbMy8VE/9CWojogY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="13D69AC4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-375-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60923   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of some thiazolyl and thiadiazolyl derivatives of 1H-pyrazole as anti-inflammatory antimicrobial agents</p>

    <p>          Bekhit, AA, Ashour, HM, Abdel, Ghany YS, Bekhit, AE, and Baraka, A</p>

    <p>          Eur J Med Chem  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17532544&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17532544&amp;dopt=abstract</a> </p><br />

    <p>2.     60924   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Antimicrobial and cytotoxic activity of agelasine and agelasimine analogs</p>

    <p>          Vik, Anders, Hedner, Erik, Charnock, Colin, Tangen, Linda W, Samuelsen, Orjan, Larsson, Rolf, Bohlin, Lars, and Gundersen, Lise-Lotte</p>

    <p>          Bioorg. Med. Chem. <b>2007</b>.  15(12): 4016-4037</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60925   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Development and characterization of a recombinant cDNA-based hepatitis C virus system</p>

    <p>          Ndjomou, J, Liu, Y, O&#39;malley, J, Ericsson, M, and He, JJ</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17531196&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17531196&amp;dopt=abstract</a> </p><br />

    <p>4.     60926   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Synergistic antimycobacterial activities of sesquiterpene lactones from Laurus spp</p>

    <p>          Luna-Herrera, J, Costa, MC, Gonzalez, HG, Rodrigues, AI, and Castilho, PC</p>

    <p>          J. Antimicrob. Chemother. <b>2007</b>.  59(3): 548-552</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     60927   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Synthesis and HCV inhibitory properties of 9-deaza- and 7,9-dideaza-7-oxa-2&#39;-C-methyladenosine</p>

    <p>          Butora, G, Olsen, DB, Carroll, SS, McMasters, DR, Schmitt, C, Leone, JF, Stahlhut, M, Burlein, C, and Maccoss, M</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17521911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17521911&amp;dopt=abstract</a> </p><br />

    <p>6.     60928   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          5-Alkynyl Analogs of Arabinouridine and 2&#39;-Deoxyuridine: Cytostatic Activity against Herpes Simplex Virus and Varicella-Zoster Thymidine Kinase Gene-Transfected Cells</p>

    <p>          Cristofoli, WA, Wiebe, LI, Clercq, ED, Andrei, G, Snoeck, R, Balzarini, J, and Knaus, EE</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17518459&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17518459&amp;dopt=abstract</a> </p><br />

    <p>7.     60929   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Mechanistic studies of electrophilic protease inhibitors of full length hepatic C virus (HCV) NS3</p>

    <p>          Poliakov, A, Sandstrom, A, Akerblom, E, and Danielson, UH</p>

    <p>          J Enzyme Inhib Med Chem <b>2007</b>.  22(2): 191-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17518346&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17518346&amp;dopt=abstract</a> </p><br />

    <p>8.     60930   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Antitubercular activity of novel substituted 4,5-dihydro-1H-1-pyrazolylmethanethiones</p>

    <p>          Ali, MA and Yar, MS</p>

    <p>          J Enzyme Inhib Med Chem <b>2007</b>.  22(2): 183-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17518345&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17518345&amp;dopt=abstract</a> </p><br />

    <p>9.     60931   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Immunopathogenesis of hepatitis C virus infection and hepatic fibrosis: New insights into antifibrotic therapy in chronic hepatitis C</p>

    <p>          Teixeira, R, Marcos, LA, and Friedman, SL</p>

    <p>          Hepatol Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17517074&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17517074&amp;dopt=abstract</a> </p><br />

    <p>10.   60932   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Lipoprotein lipase mediates hepatitis C virus (HCV) cell entry and inhibits HCV infection</p>

    <p>          Andreo, U, Maillard, P, Kalinina, O, Walic, M, Meurs, E, Martinot, M, Marcellin, P, and Budkowska, A</p>

    <p>          Cell Microbiol  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17517063&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17517063&amp;dopt=abstract</a> </p><br />

    <p>11.   60933   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Managing multiple and extensively drug-resistant tuberculosis and HIV</p>

    <p>          Padayatchi, N and Friedland, G</p>

    <p>          Expert Opin Pharmacother <b>2007</b>.  8(8): 1035-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17516869&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17516869&amp;dopt=abstract</a> </p><br />

    <p>12.   60934   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Unified QSAR approach to antimicrobials. Part 2: Predicting activity against more than 90 different species in order to halt antibacterial resistance</p>

    <p>          Prado-Prado, Francisco J, Gonzalez-Diaz, Humberto, Santana, Lourdes, and Uriarte, Eugenio</p>

    <p>          Bioorg. Med. Chem. <b>2007</b>.  15(2): 897-902</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   60935   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Discovery of novel dialkyl substituted thiophene inhibitors of HCV by in silico screening of the NS5B RdRp</p>

    <p>          Louise-May, S, Yang, W, Nie, X, Liu, D, Deshpande, MS, Phadke, AS, Huang, M, and Agarwal, A</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17512198&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17512198&amp;dopt=abstract</a> </p><br />

    <p>14.   60936   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Post-antifungal effects of the antifungal compound 2-(3,4-dimethyl-2,5-dihydro-1H-pyrrol-2-yl)-1-methylethyl pentanoate on Aspergillus fumigatus</p>

    <p>          Dabur, R, Mandal, TK, and Sharma, GL</p>

    <p>          J Med Microbiol <b>2007</b>.  56(Pt 6): 815-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17510268&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17510268&amp;dopt=abstract</a> </p><br />

    <p>15.   60937   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          High coverage with HAART is required to substantially reduce the number of deaths from tuberculosis: system dynamics simulation</p>

    <p>          Atun, RA, Lebcir, RM, Drobniewski, F, McKee, M, and Coker, RJ</p>

    <p>          Int J STD AIDS  <b>2007</b>.  18(4): 267-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17509178&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17509178&amp;dopt=abstract</a> </p><br />

    <p>16.   60938   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          High throughput screening method for antimicrobial formulations</p>

    <p>          Novak, John S and Yuan, James TC</p>

    <p>          PATENT:  US <b>2007042453</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 2006-31296  PP: 8pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60939   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Antimicrobial evaluation of some arylsulfanylpyrazinecarboxylic Acid derivatives</p>

    <p>          Jampilek, J, Dolezal, M, and Buchta, V</p>

    <p>          Med Chem <b>2007</b>.  3(3): 277-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17504199&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17504199&amp;dopt=abstract</a> </p><br />

    <p>18.   60940   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Development and validation of a cell culture based assay for in vitro assessment of anticryptosporidial compounds</p>

    <p>          Najdrowski M, Heckeroth A R, Wackwitz C, Gawlowska S, Mackenstedt U, Kliemt D, and Daugschies A</p>

    <p>          Parasitol Res <b>2007</b>.  101(1): 161-7.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   60941   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Antimicrobial activity of Diospyros anisandra</p>

    <p>          Borges-Argaez, R, Canche-Chay, CI, Pena-Rodriguez, LM, Said-Fernandez, S, and Molina-Salinas, GM</p>

    <p>          Fitoterapia <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17498888&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17498888&amp;dopt=abstract</a> </p><br />

    <p>20.   60942   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p><b>          Antimicrobial activity of a series of thiosemicarbazones and their ZnII and PdII complexes</b> </p>

    <p>          Kizilcikli, I, Kurt, YD, Akkurt, B, Genel, AY, Birteksoz, S, Otuk, G, and Ulkuseven, B</p>

    <p>          Folia Microbiol. (Prague, Czech Repub.) <b>2007</b>.  52(1): 15-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60943   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Discovery of micafungin (FK463): a novel antifungal drug derived from a natural product lead</p>

    <p>          Fujie, Akihiko </p>

    <p>          Pure Appl. Chem. <b>2007</b>.  79(4): 603-614</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   60944   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Comparison of the effects of three different anti-fungus drugs on Candida albicans of murine vaginal mucosa</p>

    <p>          Chen, S, Li, S, Liu, Z, Wu, Y, Tu, Y, and Li, J</p>

    <p>          J Huazhong Univ Sci Technolog Med Sci <b>2007</b>.  27(2): 209-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17497300&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17497300&amp;dopt=abstract</a> </p><br />

    <p>23.   60945   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Study of spectral properties and determination of the biological activity of a series of 4-[(4-chlorobenzyl)oxy]azobenzenes</p>

    <p>          Moanta, Anca, Rau, Gabriela, and Radu, Stelian</p>

    <p>          Rev. Chim. (Bucharest, Rom.) <b>2007</b>.  58(2): 229-231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   60946   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Identification of two Mycobacterium smegmatis lipoproteins exported by a SecA2-dependent pathway</p>

    <p>          Gibbons, HS, Wolschendorf, F, Abshire, M, Niederweis, M, and Braunstein, M</p>

    <p>          J Bacteriol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17496088&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17496088&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>25.   60947   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Structures of mycobacterium tuberculosis 1-Deoxy-D-Xylulose 5-Phosphate reductoisomerase provide new insights into catalysis</p>

    <p>          Henriksson, LM, Unge, T, Carlsson, J, Aqvist, J, Mowbray, SL, and Jones, TA</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17491006&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17491006&amp;dopt=abstract</a> </p><br />

    <p>26.   60948   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Preparation of pyranodibenzofurans and related compounds with antifungal and antibacterial activity</p>

    <p>          Brodin, Priscille Marie Monique, Prado, Soizic, Cole, Stewart T, Koch, Michel, Tillequin, Fran ois, and Michel, Sylvie</p>

    <p>          PATENT:  EP <b>1770089</b>  ISSUE DATE: 20070404</p>

    <p>          APPLICATION: 2005-29904  PP: 39pp.</p>

    <p>          ASSIGNEE:  (Institut Pasteur, Fr., Centre National de la Recherche Scientifique (CNRS), and Universite Rene Descartes)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   60949   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Rational Design, Synthesis, and Evaluation of Nanomolar Type II Dehydroquinase Inhibitors</p>

    <p>          Payne, RJ, Peyrot, F, Kerbarh, O, Abell, AD, and Abell, C</p>

    <p>          ChemMedChem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17487900&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17487900&amp;dopt=abstract</a> </p><br />

    <p>28.   60950   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Isolation, characterization and antifungal activity of Streptomyces sampsonii GS 1322</p>

    <p>          Jain, Praveen Kumar and Jain, PC</p>

    <p>          Indian J. Exp. Biol. <b>2007</b>.  45(2): 203-206</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   60951   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Unique applications of novel antifungal drug combinations</p>

    <p>          Onyewu, Chiatogu and Heitman, Joseph</p>

    <p>          Anti-Infect. Agents Med. Chem. <b>2007</b>.  6(1): 3-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   60952   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Risk of acquired drug resistance during short-course directly observed treatment of tuberculosis in an area with high levels of drug resistance</p>

    <p>          Cox, HS, Niemann, S, Ismailov, G, Doshetov, D, Orozco, JD, Blok, L, Rusch-Gerdes, S, and Kebede, Y</p>

    <p>          Clin Infect Dis <b>2007</b>.  44(11): 1421-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17479936&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17479936&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>31.   60953   OI-LS-375; PUBMED-OI-5/29/2007</p>

    <p class="memofmt1-2">          Fungicidal activities of dihydroferulic Acid alkyl ester analogues</p>

    <p>          Beck, JJ, Kim, JH, Campbell, BC, and Chou, SC</p>

    <p>          J Nat Prod <b>2007</b>.  70(5): 779-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17469871&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17469871&amp;dopt=abstract</a> </p><br />

    <p>32.   60954   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Antifungal activity of C3a and C3a-derived peptides against Candida</p>

    <p>          Sonesson, Andreas, Ringstad, Lovisa, Andersson Nordahl, Emma, Malmsten, Martin, Moergelin, Matthias, and Schmidtchen, Artur</p>

    <p>          Biochim. Biophys. Acta, Biomembr. <b>2007</b>.  1768(2): 346-353</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   60955   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          An antifungal protein from Escherichia coli</p>

    <p>          Yadav V, Mandhan R, Pasha Q, Pasha S, Katyal A, Chhillar A K, Gupta J, Dabur R, and Sharma G L </p>

    <p>          J Med Microbiol <b>2007</b>.  56(Pt 5): 637-44.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   60956   OI-LS-375; WOS-OI-5/18/2007</p>

    <p class="memofmt1-2">          Modern analytical approaches to high-throughput drug discovery</p>

    <p>          Gomez-Hens, A and Aguilar-Caballos, MP</p>

    <p>          TRAC-TRENDS IN ANALYTICAL CHEMISTRY <b>2007</b>.  26(3): 171-182</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245499300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245499300014</a> </p><br />

    <p>35.   60957   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Antifungal Activity of the Novel Adduct, GX-95, of Silver with Nanometer-scale Particles to Peptidic Hydrolysates from Collagen</p>

    <p>          Yaguchi Takashi, Takizawa Kyoko, Taguchi Hideaki, Tanaka Reiko, Kubota Tadashi, Kubota Yoshiaki, Kubota Masaaki, and Fukushima Kazutaka</p>

    <p>          Nippon Ishinkin Gakkai Zasshi <b>2007</b>.  48(2): 97-100.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   60958   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of symmetrical two-tailed dendritic tricarboxylato amphiphiles</p>

    <p>          Sugandhi, Eko W, Falkinham, Joseph O, and Gandour, Richard D</p>

    <p>          Bioorg. Med. Chem. <b>2007</b>.  15(11): 3842-3853</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   60959   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Pleofungins, novel inositol phosphorylceramide synthase inhibitors, from Phoma sp. SANK 13899. I. Taxonomy, fermentation, isolation, and biological activities</p>

    <p>          Yano, Tatsuya, Aoyagi, Azusa, Kozuma, Shiho, Kawamura, Yoko, Tanaka, Isshin, Suzuki, Yasuhiro, Takamatsu, Yasuyuki, Takatsu, Toshio, and Inukai, Masatoshi</p>

    <p>          J. Antibiot. <b>2007</b>.  60(2): 136-142</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>38.   60960   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Atorvastatin does not exhibit antiviral activity against HCV at conventional doses: a pilot clinical trial</p>

    <p>          O&#39;Leary, Jacqueline G, Chan, Jessica L, McMahon, Cory M, and Chung, Raymond T</p>

    <p>          Hepatology (Hoboken, NJ, U. S.) <b>2007</b>.  45(4): 895-898</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   60961   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          New cyclic peptide compounds</p>

    <p>          Neya, Masahiro, Yoshimura, Seiji, Kamijyo, Kazunori, Makino, Takuya, Yasuda, Minoru, Yamanaka, Toshio, Tsujii, Eisaku, and Yamagishi, Yukiko</p>

    <p>          PATENT:  WO <b>2007049803</b>  ISSUE DATE:  20070503</p>

    <p>          APPLICATION: 2006  PP: 124pp.</p>

    <p>          ASSIGNEE:  (Astellas Pharma Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   60962   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Quinazoline derivatives, useful in treating or preventing a flaviviridae infection, processes for preparing them, and pharmaceutical compositions containing them</p>

    <p>          Mathews, Neil, Thomas, Alexander James Floyd, Spencer, Keith Charles, Dennison, Helena, Barnes, Michael Christopher, Chana, Surinder Singh, Jennens, Lyn, and Pilkington, Christopher John</p>

    <p>          PATENT:  WO <b>2007042782</b>  ISSUE DATE:  20070419</p>

    <p>          APPLICATION: 2006  PP: 35pp.</p>

    <p>          ASSIGNEE:  (Arrow Therapeutics Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   60963   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          2-Heteroarylmethyl-4-(methoxymethyl)-N-acyl pyrrolidine compounds as antiviral agents, their preparation, pharmaceutical compositions, and use for treatment of hepatitis C</p>

    <p>          Haigh, David, Hartley, Charles David, Howes, Peter David, Jarvest, Richard Lewis, Lazarides, Linos, Nerozzi, Fabrizio, and Smith, Stephen Allan</p>

    <p>          PATENT:  WO <b>2007039142</b>  ISSUE DATE:  20070412</p>

    <p>          APPLICATION: 2006  PP: 55pp.</p>

    <p>          ASSIGNEE:  (Glaxo Group Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   60964   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          2-Heteroarylmethyl-4-pyrazin-2-yl-N-acyl pyrrolidine compounds as antiviral agents, their preparation, pharmaceutical compositions, and use for treatment of hepatitis C</p>

    <p>          Haigh, David, Hartley, Charles David, Howes, Peter David, Lazarides, Linos, Nerozzi, Fabrizio, and Smith, Stephen Allan</p>

    <p>          PATENT:  WO <b>2007039143</b>  ISSUE DATE:  20070412</p>

    <p>          APPLICATION: 2006  PP: 50pp.</p>

    <p>          ASSIGNEE:  (Glaxo Group Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   60965   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Preparation of 4-carboxypyrazoles as antivirals for treatment of hepatitis C virus (HCV) infection</p>

    <p>          Bravi, Gianpaolo, Cheasty, Anne Goretti, Corfield, John Andrew, Grimes, Richard Martin, Harrison, David, Hartley, Charles David, Howes, Peter David, Medhurst, Katrina Jane, Meeson, Malcolm Lees, Mordaunt, Jacqueline Elizabeth, Shah, Pritom, Slater, Martin John, and White, Gemma Victoria</p>

    <p>          PATENT:  WO <b>2007039146</b>  ISSUE DATE:  20070412</p>

    <p>          APPLICATION: 2006  PP: 275pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>44.   60966   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Preparation of 5,6-dimethylthieno[2,3-d]pyrimidine derivatives as antiviral agents</p>

    <p>          Han, Cheol Kyu, Yoon, Jeonghyeok, Kim, Nam-Doo, and Kim, Jin-Ah</p>

    <p>          PATENT:  WO <b>2007035010</b>  ISSUE DATE:  20070329</p>

    <p>          APPLICATION: 2005  PP: 23pp.</p>

    <p>          ASSIGNEE:  (Equispharm Co., Ltd S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>45.   60967   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Optimization of N-alkyl indole allosteric inhibitors of the HCV NS5B polymerase enzyme</p>

    <p>          Pacini, Barbara, Altamura, Sergio, Avolio, Salvatore, Bisbocci, Monica, DiFilippo, Marcello, DiMarco, Stefania, Gonzalez Paz, Odalys, Laufer, Ralph, Narjes, Frank, Paonessa, Giacomo, Rowley, Michael, and Harper, Steven</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-084</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   60968   OI-LS-375; SCIFINDER-OI-5/21/2007</p>

    <p class="memofmt1-2">          Benzodiazepines as HCV inhibitors and their preparation, pharmaceutical compositions and use in the treatment of HCV infection</p>

    <p>          Bonfanti, Jean-Francois, Doublet, Frederic Marc Maurice, Nyanguile, Origene, Raboisson, Pierre Jean-Marie Bernard, Rebstock, Anne-Sophie Helene Marie, and Boutton, Carlo Willy Maurice</p>

    <p>          PATENT:  WO <b>2007026024</b>  ISSUE DATE:  20070308</p>

    <p>          APPLICATION: 2006  PP: 196pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>47.   60969   OI-LS-375; WOS-OI-5/18/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of 1-(4-aryl-2-thiazolyl)-3-(2-thienyl)-5-aryl-2-pyrazoline derivatives</p>

    <p>          Ozdemir, A, Turan-Zitouni, G, Kaplancikli, ZA, Revial, G, and Guven, K</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2007</b>.  42(3): 403-409</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245475900014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245475900014</a> </p><br />

    <p>48.   60970   OI-LS-375; WOS-OI-5/18/2007</p>

    <p class="memofmt1-2">          Synthesis of pseudopeptides based L-tryptophan as a potential antimicrobial agent</p>

    <p>          Lv, J, Yin, L, Liu, TT, and Wang, YM</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2007</b>.  17(6): 1601-1607</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245492400023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245492400023</a> </p><br />

    <p>49.   60971   OI-LS-375; WOS-OI-5/18/2007</p>

    <p class="memofmt1-2">          Activities of DL-alpha-difluoromethylarginine and polyamine analogues against Cryptosporidium parvum infection in a T-cell receptor alpha-deficient mouse model</p>

    <p>          Yarlett, N, Waters, WR, Harp, JA, Wannemuehler, MJ, Morada, M, Bellcastro, J, Upton, SJ, Marton, LJ, and Frydman, BJ</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2007</b>.  51(4): 1234-1239</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500016</a> </p><br />
    <br clear="all">

    <p>50.   60972   OI-LS-375; WOS-OI-5/18/2007</p>

    <p class="memofmt1-2">          Low-oxygen-recovery assay for high-throughput screening of compounds against nonreplicating Mycobacterium tuberculosis</p>

    <p>          Cho, SH, Warit, S, Wan, BJ, Hwang, CH, Pauli, GF, and Franzblau, SG</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2007</b>.  51(4): 1380-1385</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500037</a> </p><br />

    <p>51.   60973   OI-LS-375; WOS-OI-5/18/2007</p>

    <p class="memofmt1-2">          Drug therapy of experimental tuberculosis (TB): Improved outcome by combining SQ109, a new diamine antibiotic, with existing TB drugs</p>

    <p>          Nikonenko, BV, Protopopova, M, Sarnala, R, Einck, L, and Nacy, CA</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2007</b>.  51(4): 1563-1565</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500071">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500071</a> </p><br />

    <p>52.   60974   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          siVirus: web-based antiviral siRNA design software for highly divergent viral sequences</p>

    <p>          Naito, Y, Ui-Tei, K, Nishikawa, T, Takebe, Y, and Saigo, K</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2006</b>.  34: W448-W450</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245650200090">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245650200090</a> </p><br />

    <p>53.   60975   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          FAF-Drugs: free ADME/tox filtering of compound collections</p>

    <p>          Miteva, MA, Violas, S, Montes, M, Gomez, D, Tuffery, P, and Villoutreix, BO</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2006</b>.  34: W738-W744</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245650200149">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245650200149</a> </p><br />

    <p>54.   60976   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          Nigerasperones A similar to C, new monomeric and dimeric naphtho-gamma-pyrones from a marine alga-derived endophytic fungus Aspergillus niger EN-13</p>

    <p>          Zhang, Y, Li, XM, and Wang, BG</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2007</b>.  60(3): 204-210</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245648800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245648800006</a> </p><br />

    <p>55.   60977   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          Spread of extensively drug-resistant tuberculosis</p>

    <p>          Samper, S and Martin, C</p>

    <p>          EMERGING INFECTIOUS DISEASES <b>2007</b>.  13(4): 647-648</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245558200026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245558200026</a> </p><br />

    <p>56.   60978   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          Evidence for in vivo inhibition of CMV infection by the quinazoline class protein kinase inhibitor gefitinib</p>

    <p>          Schleiss, M, McVoy, M, Cui, XH, Choi, Y, Anderson, J, Stamminger, T, Klebl, B, Eickhoff, J, and Marschall, M</p>

    <p>          ANTIVIRAL RESEARCH <b>2007</b>.  74(3): A34-A35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600020</a> </p><br />

    <p>57.   60979   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          The cyclophilin inhibitor Debio-025 is a potent inhibitor of hepatitis C virus replication in vitro with a unique resistance profile</p>

    <p>          Coelmont, L, Paeshuyse, J, Kaptein, S, Vliegen, I, Kaul, A, De Clercq, E, Rosenwirth, B, Scalfaro, P, Crabbe, R, Bartenschlager, R, Dumont, JM, and Neyts, J</p>

    <p>          ANTIVIRAL RESEARCH <b>2007</b>.  74(3): A39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600030</a> </p><br />

    <p>58.   60980   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          Design and synthesis of novel anti-HCMV agents: Modifications to the bicyclic pyrimidine base</p>

    <p>          Adak, R, McGuigan, C, Snoeck, R, Andrei, G, De Clercq, E, and Balzarini, J</p>

    <p>          ANTIVIRAL RESEARCH <b>2007</b>.  74(3): A71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600107">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600107</a> </p><br />

    <p>59.   60981   OI-LS-375; WOS-OI-5/25/2007</p>

    <p class="memofmt1-2">          Novel 4&#39;-azido-2&#39;-deoxy-nucleoside analogs are potent inhibitors of NS5B-dependent HCV replication</p>

    <p>          Smiths, D, Ma, H, Le Pogam, S, Leveque, V, Browns, C, Johansson, NG, Kalayanov, G, Eriksson, S, Usova, E, Sund, C, Winqist, A, Maltseva, T, Smith, M, Martin, J, Najera, I, and Klumpp, K</p>

    <p>          ANTIVIRAL RESEARCH <b>2007</b>.  74(3): A36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246043600023</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
