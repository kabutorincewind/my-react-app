

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-03-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bvHxByvPBhbV2/5+HPhsvHpvZ3mY0CN3cf/SpQv0J3TOFDSqU2zu7mPSJHNvQ8I13iyUPvq9cPpB8EG4fKEaqha70qetdqBfDMVzfw87eB1nrPFEC1RhhDyHdPg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E3C635AC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 4 - March 17, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19275497?ordinalpos=32&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Etravirine, a Next-Generation Nonnucleoside Reverse-Transcriptase Inhbitor.</a>  Johnson LB, Saravolatz LD.  Clin Infect Dis. 2009 Mar 10. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19275497</p> 

    <p class="title">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19217781?ordinalpos=55&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The use of oxadiazole and triazole substituted naphthyridines as HIV-1 integrase inhibitors. Part 1: Establishing the pharmacophore.</a>  Johns BA, Weatherhead JG, Allen SH, Thompson JB, Garvey EP, Foster SA, Jeffrey JL, Miller WH.  Bioorg Med Chem Lett. 2009 Mar 15;19(6):1802-6. Epub 2009 Jan 30.</p>

    <p class="pmid">PMID: 19217781</p> 

    <p class="title">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19217284?ordinalpos=56&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">1,3,4-Oxadiazole substituted naphthyridines as HIV-1 integrase inhibitors. Part 2: SAR of the C5 position.</a>  Johns BA, Weatherhead JG, Allen SH, Thompson JB, Garvey EP, Foster SA, Jeffrey JL, Miller WH.  Bioorg Med Chem Lett. 2009 Mar 15;19(6):1807-10. Epub 2009 Jan 30.</p>

    <p class="pmid">PMID: 19217284</p> 

    <p class="title">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19193109?ordinalpos=59&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Proof of activity with AMD11070, an orally bioavailable inhibitor of CXCR4-tropic HIV type 1.</a>  Moyle G, DeJesus E, Boffito M, Wong RS, Gibney C, Badel K, MacFarland R, Calandra G, Bridger G, Becker S; X4 Antagonist Concept Trial Study Team.  Clin Infect Dis. 2009 Mar 15;48(6):798-805.</p>

    <p class="title">PMID: 19193109</p> 

    <p class="title">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19289522?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antiviral Activity of MK-4965: A Novel Non-nucleoside Reverse Transcriptase Inhibitor.</a>  Lai MT, Munshi V, Touch S, Tynebor RM, Tucker TJ, McKenna PM, Williams TM, Distefano DJ, Hazuda DJ, Miller MD.  Antimicrob Agents Chemother. 2009 Mar 16. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19289522</p> 

    <p class="title">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19289098?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Sifuvirtide, a potent HIV fusion inhibitor peptide.</a>  Wang RR, Yang LM, Wang YH, Pang W, Tam SC, Tien P, Zheng YT.  Biochem Biophys Res Commun. 2009 Mar 13. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19289098</p> 

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p class="title">7.      <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263587800007">3D-QSAR Studies on DATAs and DAPYs for HIV-RT Inhibitors using CoMFA and CoMSIA Approaches.</a> Park HY, Ju SM, Lee DY, Zhang H, Kim CK.  QSAR &amp; COMBINATORIAL SCIENCE, 2009 Feb; 28(2):218-225. </p> 

    <p class="title">8.      <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263502500003">Computational studies of the binding mode and 3D-QSAR analyses of symmetric formimidoester disulfides: a new class of non-nucleoside HIV-1 reverse transcriptase inhibitor.</a>  Cichero E, Cesarini S, Spallarossa A, Mosti L, Fossa P.  JOURNAL OF MOLECULAR MODELING.  2009 Apr; 15(4):357-367. </p> 

    <p class="title">9.      <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263668700009">Studies on the esterifications of 9-(hydroxyimino)-4-methyl-8,9-dihydrofuro[2,3-h]chromen-2-one with acid chlorides under different conditions.</a>  Sun RS, Wang Y, Xia P.  Sun RS, Wang Y, Xia P.  CHINESE CHEMICAL LETTERS, 2008 Jul; 19(7):791-794. </p> 

    <p class="title">10.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262104700080">Inhibition of HIV-1 by DpT-Based Iron Chelators.</a>  Debebe Z, Kumar K, Ammosova T, Niu XM, Richardson DR, Gordeuk VR, Nekhai S.  BLOOD, 2008 Nov; 112(11):36-37.</p> 

    <p class="title">11.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263810100004">An efficient synthetic route to substituted tetrahydropyrimidines by Cu(OTf)O-2-mediated nucleophilic ring-opening followed by the [4+2] cycloaddition of N-tosylazetidines with nitriles.</a>  Ghorai MK, Das K, Kumar A.  TETRAHEDRON LETTERS, 2009 Mar; 50(10):1105-1109. </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
