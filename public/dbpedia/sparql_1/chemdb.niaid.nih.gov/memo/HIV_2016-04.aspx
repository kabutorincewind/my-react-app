

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="qBpKt7RQfVOxhXNRsRxrphHtsa+WVbh8dnqql91QbGI5/zrHVRq/Eb7vfoHQ9fnMnUUaQhFMxtqE17hZLr7KXk7JXDNXewGtCQcxyOiLnVBqJMp0W2nTxMoGI2U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CE605A06" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: April, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000372435700002">Triterpene Constituents of Euphorbia erythradenia Bioss. and Their anti-HIV Activity.</a> Ayatollahi, A.M., S.M. Zarei, A. Memarnejadian, M. Ghanadian, M.H. Moghadam, and F. Kobarfard. Iranian Journal of Pharmaceutical Research, 2016. 15: p. 19-27. ISI[000372435700002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26446906">A Broad Spectrum anti-HIV Inhibitor Significantly Disturbs V1/V2 Domain Rearrangements of HIV-1 gp120 and Inhibits Virus Entry.</a> Berinyuy, E. and M.E. Soliman. Journal of Receptors and Signal Transduction Research, 2016. 36(2): p. 119-129. PMID[26446906].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26851491">Dasatinib Inhibits HIV-1 Replication through the Interference of SAMHD1 Phosphorylation in CD4+ T Cells.</a> Bermejo, M., M.R. Lopez-Huertas, J. Garcia-Perez, N. Climent, B. Descours, J. Ambrosioni, E. Mateos, S. Rodriguez-Mora, L. Rus-Bercial, M. Benkirane, J.M. Miro, M. Plana, J. Alcami, and M. Coiras. Biochemical Pharmacology, 2016. 106: p. 30-45. PMID[26851491].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26968647">Design and Synthesis of Small Molecule-Sulfotyrosine Mimetics that Inhibit HIV-1 Entry.</a> Dogo-Isonagie, C., S.L. Lee, K. Lohith, H. Liu, S.R. Mandadapu, S. Lusvarghi, R.D. O&#39;Connor, and C.A. Bewley. Bioorganic &amp; Medicinal Chemistry, 2016. 24(8): p. 1718-1728. PMID[26968647].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26313923">Synthesis and Antiviral Evaluation of 1-[(2-Phenoxyethyl)oxymethyl] and 6-(3,5-Dimethoxybenzyl) Analogues of HIV Drugs Emivirine and TNK-651.</a> El-Brollosy, N.R. and R. Loddo. Drug Research, 2016. 66(4): p. 181-188. PMID[26313923].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26797100">Synthesis and in Vitro Screening of New Series of 2,6-Dipeptidyl-anthraquinones: Influence of Side Chain Length on HIV-1 Nucleocapsid Inhibitors.</a> Frecentese, F., A. Sosic, I. Saccone, E. Gamba, K. Link, A. Miola, M. Cappellin, M.G. Cattelan, B. Severino, F. Fiorino, E. Magli, A. Corvino, E. Perissutti, D. Fabris, B. Gatto, G. Caliendo, and V. Santagada. Journal of Medicinal Chemistry, 2016. 59(5): p. 1914-1924. PMID[26797100].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000373627000008">Testing anti-HIV Activity of Antiretroviral Agents in Vitro Using Flow Cytometry Analysis of CEM-GFP Cells Infected with Transfection-Derived HIV-1 NL4-3.</a> Frezza, C., S. Grelli, M. Federico, F. Marino-Merlo, A. Mastino, and B. Macchi. Journal of Medical Virology, 2016. 88(6): p. 979-986. ISI[000373627000008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27093399">Sulforaphane Inhibits HIV Infection of Macrophages Through Nrf2.</a> Furuya, A.K., H.J. Sharifi, R.M. Jellinger, P. Cristofano, B. Shi, and C.M. de Noronha. Plos Pathogens, 2016. 12(4): p. e1005581. PMID[27093399]. PMCID[PMC4836681].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27112451">1,6-Bis[(Benzyloxy)methyl]uracil Derivatives-Novel Antivirals with Activity against HIV-1 and Influenza H1N1 Virus.</a> Geisman, A.N., V.T. Valuev-Elliston, A.A. Ozerov, A.L. Khandazhinskaya, A.O. Chizhov, S.N. Kochetkov, C. Pannecouque, L. Naesens, K.L. Seley-Radtke, and M.S. Novikov. Bioorganic &amp; Medicinal Chemistry, 2016. 24(11): p. 2476-2485. PMID[27112451].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27080941">Litsea Species as Potential Antiviral Plant Sources.</a> Guan, Y., D. Wang, G.T. Tan, N. Van Hung, N.M. Cuong, J.M. Pezzuto, H.H. Fong, D.D. Soejarto, and H. Zhang. The American Journal of Chinese Medicine, 2016. 44(2): p. 275-290. PMID[27080941].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26789641">Polyanionic Macromolecular Prodrugs of Ribavirin: Antiviral Agents With a Broad Spectrum of Activity.</a> Hinton, T.M., K. Zuwala, C. Deffrasnes, S. Todd, S.N. Shi, G.A. Marsh, M. Dearnley, B.M. Wohl, M. Tolstrup, and A.N. Zelikin. Advanced Healthcare Materials, 2016. 5(5): p. 534-540. PMID[26789641].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000372950600002">Naturally Occurring Thiophenes: Isolation, Purification, Structural Elucidation, and Evaluation of Bioactivities.</a> Ibrahim, S.R.M., H.M. Abdallah, A.M. El-Halawany, and G.A. Mohamed. Phytochemistry Reviews, 2016. 15(2): p. 197-220. ISI[000372950600002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26973048">Exploring Isoxazole and Carboxamide Derivatives as Potential Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Kurup, S.S. and K.A. Joshi. Journal of Molecular Graphics and Modeling, 2016. 65: p. 113-128. PMID[26973048].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26830226">A Minor Subset of Super Elongation Complexes Plays a Predominant Role in Reversing HIV-1 Latency.</a> Li, Z.C., H.S. Lu, and Q. Zhou. Molecular and Cellular Biology, 2016. 36(7): p. 1194-1205. PMID[26830226]. PMCID[PMC4800791].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27070547">Novel HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitor Agents: Optimization of Diarylanilines with High Potency against Wild-type and Rilpivirine-resistant E138K Mutant Virus.</a> Liu, N., L. Wei, L. Huang, F. Yu, W. Zheng, B. Qin, D.Q. Zhu, S.L. Morris-Natschke, S. Jiang, C.H. Chen, K.H. Lee, and L. Xie. Journal of Medicinal Chemistry, 2016. 59(8): p. 3689-3704. PMID[27070547].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27067814">The BET Inhibitor OTX015 Reactivates Latent HIV-1 Through P-TEFb.</a> Lu, P., X. Qu, Y. Shen, Z. Jiang, P. Wang, H. Zeng, H. Ji, J. Deng, X. Yang, X. Li, H. Lu, and H. Zhu. Scientific Reports, 2016. 6: p. 24100. PMID[27067814]. PMCID[PMC4828723].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26985324">Small-molecule CD4-mimics: Structure-based Optimization of HIV-1 Entry Inhibition.</a> Melillo, B., S.Y. Liang, J. Park, A. Schon, J.R. Courter, J.M. LaLonde, D.J. Wendler, A.M. Princiotto, M.S. Seaman, E. Freire, J. Sodroski, N. Madani, W.A. Hendrickson, and A.B. Smith. ACS Medicinal Chemistry Letters, 2016. 7(3): p. 330-334. PMID[26985324]. PMCID[PMC4789667].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26964674">Antiretroviral (HIV-1) Activity of Azulene Derivatives.</a> Peet, J., A. Selyutina, and A. Bredihhin. Bioorganic &amp; Medicinal Chemistry, 2016. 24(8): p. 1653-1657. PMID[26964674].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27032748">The G-quadruplex-forming Aptamer AS1411 Potently Inhibits HIV-1 Attachment to the Host Cell.</a> Perrone, R., E. Butovskaya, S. Lago, A. Garzino-Demo, C. Pannecouque, G. Palu, and S.N. Richter. International Journal of Antimicrobial Agents, 2016. 47(4): p. 311-316. PMID[27032748]. PMCID[PMC4840014].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26427611">Anti-HIV-1 Integrase Effect of Compounds from Aglaia andamanica Leaves and Molecular Docking Study with Acute Toxicity Test in Mice.</a> Puripattanavong, J., P. Tungcharoen, P. Chaniad, S. Pianwanit, and S. Tewtrakul. Pharmaceutical Biology, 2016. 54(4): p. 654-659. PMID[26427611].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000372425600061">Evaluation and Synthesis of AZT Prodrugs with Optimized Chemical Stabilities: Experimental and Theoretical Analyses.</a> Ribone, S.R., E.M. Schenfeld, M. Madrid, A.B. Pierini, and M.A. Quevedo. New Journal of Chemistry, 2016. 40(3): p. 2383-2392. ISI[000372425600061].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26785139">2,6-Di(Arylamino)-3-fluoropyridine Derivatives as HIV Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Sergeyev, S., A.K. Yadav, P. Franck, J. Michiels, P. Lewi, J. Heeres, G. Vanham, K.K. Arien, C. Velde, H. De Winter, and B.U.W. Maes. Journal of Medicinal Chemistry, 2016. 59(5): p. 1854-1868. PMID[26785139].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000372756700004">New 1(2H)-Phthalazinone Derivatives as Potent Nonpeptidic HIV-1 Protease Inhibitors: Molecular Docking Studies, Molecular Dynamics Simulation, Oral Bioavailability and ADME Prediction.</a> Sroczynski, D., Z. Malinowski, A.K. Szczesniak, and W. Pakulska. Molecular Simulation, 2016. 42(8): p. 628-641. ISI[000372756700004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26988305">Inhibitors of HIV-1 Maturation: Development of Structure-Activity Relationship for C-28 Amides Based on C-3 Benzoic acid-modified Triterpenoids</a>. Swidorski, J.J., Z. Liu, S.Y. Sit, J. Chen, Y. Chen, N. Sin, B.L. Venables, D.D. Parker, B. Nowicka-Sans, B.J. Terry, T. Protack, S. Rahematpura, U. Hanumegowda, S. Jenkins, M. Krystal, I.B. Dicker, N.A. Meanwell, and A. Regueiro-Ren. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(8): p. 1925-1930. PMID[26988305].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26810656">Ebselen, a Small-molecule Capsid Inhibitor of HIV-1 Replication.</a> Thenin-Houssier, S., I.M. de Vera, L. Pedro-Rosa, A. Brady, A. Richard, B. Konnick, S. Opp, C. Buffone, J. Fuhrmann, S. Kota, B. Billack, M. Pietka-Ottlik, T. Tellinghuisen, H. Choe, T. Spicer, L. Scampavia, F. Diaz-Griffero, D.J. Kojetin, and S.T. Valente. Antimicrobial Agents and Chemotherapy, 2016. 60(4): p. 2195-2208. PMID[26810656]. PMCID[PMC4808204].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26957201">HIV-1 Capsid Inhibitors as Antiretroviral Agents.</a> Thenin-Houssier, S. and S.T. Valente. Current HIV Research, 2016. 14(3): p. 270-282. PMID[26957201]. PMCID[PMC4785820].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26946324">Inhibition of HIV-1 Reverse Transcriptase Dimerization by Small Molecules.</a> Tintori, C., A. Corona, F. Esposito, A. Brai, N. Grandi, E.R. Ceresola, M. Clementi, F. Canducci, E. Tramontano, and M. Botta. ChemBioChem, 2016. 17(8): p. 683-688. PMID[26946324].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27103798">Antiviral Mechanism of Polyanionic Carbosilane Dendrimers against HIV-1.</a> Vacas-Cordoba, E., M. Maly, F.J. De la Mata, R. Gomez, M. Pion, and M.A. Munoz-Fernandez. International Journal of Nanomedicine, 2016. 11: p. 1281-1294. PMID[27103798]. PMCID[PMC4827595].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27108399">Design, Discovery, Modelling, Synthesis, and Biological Evaluation of Novel and Small, Low Toxicity S-triazine Derivatives as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Viira, B., A. Selyutina, A.T. Garcia-Sosa, M. Karonen, J. Sinkkonen, A. Merits, and U. Maran. Bioorganic &amp; Medicinal Chemistry, 2016. 24(11): p. 2519-2529. PMID[27108399].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27039251">Design, Synthesis and anti-HIV-1 Evaluation of Hydrazide-based Peptidomimetics as Selective Gelatinase Inhibitors.</a> Yang, L., P. Wang, J.F. Wu, L.M. Yang, R.R. Wang, W. Pang, Y.G. Li, Y.M. Shen, Y.T. Zheng, and X. Li. Bioorganic &amp; Medicinal Chemistry, 2016. 24(9): p. 2125-2136. PMID[27039251].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_04_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160324&amp;CC=WO&amp;NR=2016044808A1&amp;KC=A1">Substituted Phenylpyrrolecarboxamides with Activity against HIV Infection.</a> Debnath, A.K., F. Curreli, P.D. Kwong, and Y.D. Kwon. Patent. 2016. 2015-US51086 2016044808: 103pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_04_2016.</p>

    <br />

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160421&amp;CC=WO&amp;NR=2016061131A1&amp;KC=A1">Anti-HIV Compositions and Methods for Reactivating Latent Immunodeficiency Virus Using a SMYD2 Inhibitor.</a> Ott, M. and D. Boehm. Patent. 2016. 2015-US55377 2016061131: 80pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_04_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
