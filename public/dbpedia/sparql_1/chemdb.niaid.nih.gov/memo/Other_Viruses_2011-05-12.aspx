

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-05-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="daXJn4KcrwOYsRytPiZmpZMtQkgqgWsKHeas8hU+wj+zFJl8IkK4qveCn3c0pDaOukwNfjdQacbKtzttDWDKarGsDS1JsrW+u3J/9c3yHsYacFcB+y6q8lIJ0zw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7966AD5E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">April 29 - May 12, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Epstein-Barr virus</p> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21528487">Inhibitory Activities of Microalgal Extracts Against Epstein-Barr Virus DNA Release from Lymphoblastoid Cells</a>. Kok, Y.Y., W.L. Chu, S.M. Phang, S.M. Mohamed, R. Naidu, P.J. Lai, S.N. Ling, J.W. Mak, P.K. Lim, P. Balraj, and A.S. Khoo. Journal of Zhejiang University. Science. B, 2011. 12(5): p. 335-345; PMID[21528487].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0429-051211.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21396962">A Reporter System for Epstein-Barr Virus (Ebv) Lytic Replication: Anti-Ebv Activity of the Broad Anti-Herpesviral Drug Artesunate</a>. Auerochs, S., K. Korn, and M. Marschall. Journal of Virological Methods, 2011. 17<b>3</b>(2): p. 334-339; PMID[21396962].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0429-051211.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21553812">Thiazolides as Novel Antiviral Agents: I. Inhibition of Hepatitis B Virus Replication</a>. Stachulski, A.V., C. Pidathala, E. Row, R. Sharma, N. Berry, M. Iqbal, S. Allman, G. Edwards, B.E. Korba, E.J. Semple, and J.F. Rossignol. Journal of Medicinal Chemistry, 2011. <b>Epub ahead of print</b>; PMID[21553812].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0429-051211.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21383094">Preclinical Characterization of Naturally Occurring Polyketide Cyclophilin Inhibitors from the Sanglifehrin Family</a>. Gregory, M.A., M. Bobardt, S. Obeid, U. Chatterji, N.J. Coates, T. Foster, P. Gallay, P. Leyssen, S.J. Moss, J. Neyts, E.A.M. Nur, J. Paeshuyse, M. Piraee, D. Suthar, T. Warneck, M.Q. Zhang, and B. Wilkinson. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 1975-1981; PMID[21383094].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0429-051211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289281600021">Fused Heterocyclic Amido Compounds as Anti-Hepatitis C Virus Agents</a>. Aoyama, H., K. Sugita, M. Nakamura, A. Aoyama, M.T.A. Salim, M. Okamoto, M. Baba, and Y. Hashimoto. Bioorganic &amp; Medicinal Chemistry, 2011. 19(8): p. 2675-2687; ISI[000289281600021].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0429-051211.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289318400060">Mycophenolic Acid Enhances the Expression of Interferon-Stimulated Genes and Inhibits Hepatitis C Virus Infection Independent of Impdh</a>. Pan, Q., P.E. de Ruiter, J. de Jonge, G. Kazemier, J. Kwekkeboom, H.J. Metselaar, H.W. Tilanus, H.L.A. Janssen, and L.J.W. van der Laan. American Journal of Transplantation, 2011. 11: p. 47-47; ISI[000289318400060].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0429-051211.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289404700022">Serpin Induced Antiviral Activity of Prostaglandin Synthetase-2 against Hiv-1 Replication</a>. Whitney, J.B., M. Asmal, and R. Geiben-Lynn. Plos One, 2011. 6(4); ISI[000289404700022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0429-051211.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
