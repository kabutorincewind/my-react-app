

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-326.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="moD5RSqvXwbRv1qwh9NCUqP09koPv//IYiXsyWasCz6Io4W88SfVyBnA2BuWyan0C4ke/H/HLPBdXTKeiw2JPzgBV/HI5EuFFhQx/S7zbvOq22XnFdcOVEeC5xs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F54BE800" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-326-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47600   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Geminal Disulfones as HIV-1 Integrase Inhibitors</p>

    <p>          Meadows, DC, Mathews, TB, North, TW, Hadd, MJ, Kuo, CL, Neamati, N, and Gervay-Hague, J</p>

    <p>          J Med Chem <b>2005</b>.  48(14): 4526-4534</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15999991&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15999991&amp;dopt=abstract</a> </p><br />

    <p>2.     47601   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Identification of N-phenyl-N&#39;-(2,2,6,6-tetramethyl-piperidin-4-yl)-oxalamides as a new class of HIV-1 entry inhibitors that prevent gp120 binding to CD4</p>

    <p>          Zhao, Q, Ma, L, Jiang, S, Lu, H, Liu, S, He, Y, Strick, N, Neamati, N, and Debnath, AK</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15996703&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15996703&amp;dopt=abstract</a> </p><br />

    <p>3.     47602   HIV-LS-326; EMBASE-HIV-7/11/2005</p>

    <p class="memofmt1-2">          The anti-HIV-1 effect of scutellarin</p>

    <p>          Zhang, Gao-Hong, Wang, Qian, Chen, Ji-Jun, Zhang, Xue-Mei, Tam, Siu-Cheung, and Zheng, Yong-Tang</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4GKWC17-6/2/be21d9d7c2f71c1c5c7c8de22a1160e5">http://www.sciencedirect.com/science/article/B6WBK-4GKWC17-6/2/be21d9d7c2f71c1c5c7c8de22a1160e5</a> </p><br />

    <p>4.     47603   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Synthesis of 6-(3,5-Dichlorobenzyl) Derivatives as Isosteric Analogues of the HIV Drug 6-(3,5-Dimethylbenzyl)-1-(ethoxymethyl)-5-isopropyluracil (GCA-186)</p>

    <p>          Sorensen, ER, El-Brollosy, NR, Jorgensen, PT, Pedersen, EB, and Nielsen, C</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15996003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15996003&amp;dopt=abstract</a> </p><br />

    <p>5.     47604   HIV-LS-326; EMBASE-HIV-7/11/2005</p>

    <p class="memofmt1-2">          HIV chemokine receptor inhibitors as novel anti-HIV drugs</p>

    <p>          Princen, Katrien and Schols, Dominique</p>

    <p>          Cytokine &amp; Growth Factor Reviews <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T5V-4GJVBG7-1/2/7a08787d0807434f9f47e45cfaa13c4b">http://www.sciencedirect.com/science/article/B6T5V-4GJVBG7-1/2/7a08787d0807434f9f47e45cfaa13c4b</a> </p><br />
    <br clear="all">

    <p>6.     47605   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Structure-based design, synthesis, and in vitro assay of novel nucleoside analog inhibitors against HIV-1 reverse transcriptase</p>

    <p>          Liu, X, Xie, W, and Huang, RH</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993595&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15993595&amp;dopt=abstract</a> </p><br />

    <p>7.     47606   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Novel P1 chain-extended HIV protease inhibitors possessing potent anti-HIV activity and remarkable inverse antiviral resistance profiles</p>

    <p>          Miller, JF, Brieger, M, Furfine, ES, Hazen, RJ, Kaldor, I, Reynolds, D, Sherrill, RG, and Spaltenstein, A</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(15): 3496-500</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15990305&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15990305&amp;dopt=abstract</a> </p><br />

    <p>8.     47607   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of new N-substituted-N&#39;-(3,5-di/1,3,5-trimethylpyrazole-4-yl)thiourea/urea derivatives</p>

    <p>          Kaymakcioglu, BK, Rollas, S, Korcegez, E, and Aricioglu, F</p>

    <p>          Eur J Pharm Sci <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15990284&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15990284&amp;dopt=abstract</a> </p><br />

    <p>9.     47608   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Antiviral activity of 4-benzyl pyridinone derivatives as HIV-1 reverse transcriptase inhibitors</p>

    <p>          Andreola, ML, Nguyen, CH, Ventura, M, Tarrago-Litvak, L, and Legraverend, M</p>

    <p>          Expert Opin Emerg Drugs <b>2001</b>.  6(2): 225-238</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15989523&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15989523&amp;dopt=abstract</a> </p><br />

    <p>10.   47609   HIV-LS-326; EMBASE-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Chemokine receptor-5 (CCR5) is a receptor for the HIV entry inhibitor peptide T (DAPTA)</p>

    <p>          Polianova, Maria T, Ruscetti, Francis W, Pert, Candace B, and Ruff, Michael R</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GDSCC8-1/2/d5d363c1b8e90b0b0b3db65273be8a64">http://www.sciencedirect.com/science/article/B6T2H-4GDSCC8-1/2/d5d363c1b8e90b0b0b3db65273be8a64</a> </p><br />
    <br clear="all">

    <p>11.   47610   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Interaction of silver nanoparticles with HIV-1</p>

    <p>          Elechiguerra, JL, Burt, JL, Morones, JR, Camacho-Bragado, A, Gao, X, Lara, HH, and Jose, Yacaman M</p>

    <p>          J Nanobiotechnology <b>2005</b>.  3(1): 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15987516&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15987516&amp;dopt=abstract</a> </p><br />

    <p>12.   47611   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Biyouyanagin A, an Anti-HIV Agent from Hypericum chinense L. var. salicifolium</p>

    <p>          Tanaka, N, Okasaka, M, Ishimaru, Y, Takaishi, Y, Sato, M, Okamoto, M, Oshikawa, T, Ahmed, SU, Consentino, LM, and Lee, KH</p>

    <p>          Org Lett <b>2005</b>.  7(14): 2997-2999</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15987189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15987189&amp;dopt=abstract</a> </p><br />

    <p>13.   47612   HIV-LS-326; EMBASE-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Intracellular measurements of anti-HIV drugs indinavir, amprenavir, saquinavir, ritonavir, nelfinavir, lopinavir, atazanavir, efavirenz and nevirapine in peripheral blood mononuclear cells by liquid chromatography coupled to tandem mass spectrometry</p>

    <p>          Colombo, S, Beguin, A, Telenti, A, Biollaz, J, Buclin, T, Rochat, B, and Decosterd, LA</p>

    <p>          Journal of Chromatography B <b>2005</b>.  819(2): 259-276</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6X0P-4FVCBT9-1/2/927c6044c4faca73d227467f2a121d47">http://www.sciencedirect.com/science/article/B6X0P-4FVCBT9-1/2/927c6044c4faca73d227467f2a121d47</a> </p><br />

    <p>14.   47613   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Safety, pharmacokinetics, and efficacy of (+/-)-beta-2&#39;,3&#39;-dideoxy-5-fluoro-3&#39;-thiacytidine with efavirenz and stavudine in antiretroviral-naive human immunodeficiency virus-infected patients</p>

    <p>          Herzmann, C, Arasteh, K, Murphy, RL, Schulbin, H, Kreckel, P, Drauz, D, Schinazi, RF, Beard, A, Cartee, L, and Otto, MJ</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(7): 2828-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980356&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980356&amp;dopt=abstract</a> </p><br />

    <p>15.   47614   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Pharmacology and pharmacokinetics of the antiviral agent beta-D-2&#39;,3&#39;-dideoxy-3&#39;-oxa-5-fluorocytidine in cells and rhesus monkeys</p>

    <p>          Hernandez-Santiago, BI, Chen, H, Asif, G, Beltran, T, Mao, S, Hurwitz, SJ, Grier, J, McClure, HM, Chu, CK, Liotta, DC, and Schinazi, RF</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(7): 2589-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980324&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980324&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   47615   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Cyclotriazadisulfonamides: promising new CD4-targeted anti-HIV drugs</p>

    <p>          Vermeire, K and Schols, D</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980096&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980096&amp;dopt=abstract</a> </p><br />

    <p>17.   47616   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Anti-HIV properties of cationic fullerene derivatives</p>

    <p>          Marchesan, S,  Da Ros, T, Spalluto, G, Balzarini, J, and Prato, M</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(15): 3615-3618</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15978810&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15978810&amp;dopt=abstract</a> </p><br />

    <p>18.   47617   HIV-LS-326; EMBASE-HIV-7/11/2005</p>

    <p class="memofmt1-2">          The N137 and P140 amino acids in the p51 and the P95 amino acid in the p66 subunit of human immunodeficiency virus type 1 (HIV-1) reverse transcriptase are instrumental to maintain catalytic activity and to design new classes of anti-HIV-1 drugs</p>

    <p>          Auwerx, Joeri, Van Nieuwenhove, Joke, Rodriguez-Barrios, Fatima, de Castro, Sonia, Velazquez, Sonsoles, Ceccherini-Silberstein, Francesca, De Clercq, Erik, Camarasa, Maria-Jose, Perno, Carlo-Federico, Gago, Federico, and Balzarini, Jan</p>

    <p>          FEBS Letters <b>2005</b>.  579(11): 2294-2300</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4FSNV90-3/2/ea23d92714192e5279f5b34e4348deab">http://www.sciencedirect.com/science/article/B6T36-4FSNV90-3/2/ea23d92714192e5279f5b34e4348deab</a> </p><br />

    <p>19.   47618   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          CCR5 N-terminal region plays a critical role in HIV-1 inhibition by toxoplasma gondii derived cyclophilin-18</p>

    <p>          Golding, H, Khurana, S, Yarovinsky, F, King, LR, Abdoulaeva, G, Antonsson, L, Owman, C, Platt, EJ, Kabat, D, Andersen, JF, and Sher, A</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15975927&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15975927&amp;dopt=abstract</a> </p><br />

    <p>20.   47619   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Simultaneous determination of six HIV protease inhibitors (amprenavir, indinavir, lopinavir, nelfinavir, ritonavir and saquinavir), the active metabolite of nelfinavir (M8) and non-nucleoside reverse transcriptase inhibitor (efavirenz) in human plasma by high-performance liquid chromatography</p>

    <p>          Hirabayashi, Y, Tsuchiya, K, Kimura, S, and Oka, S</p>

    <p>          Biomed Chromatogr <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15971289&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15971289&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   47620   HIV-LS-326; PUBMED-HIV-7/11/2005</p>

    <p class="memofmt1-2">          Role of T139 in the human immunodeficiency virus type 1 (HIV-1) reverse transcriptase (RT) sensitivity to (+)-calanolide A</p>

    <p>          Auwerx, J, Rodriguez-Barrios, F, Ceccherini-Silberstein, F, San-Felix, A, Velazquez, S, De, Clercq E, Camarasa, MJ, Perno, CF, Gago, F, and Balzarini, J</p>

    <p>          Mol Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15961674&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15961674&amp;dopt=abstract</a> </p><br />

    <p>22.   47621   HIV-LS-326; WOS-HIV-7/3/2005</p>

    <p class="memofmt1-2">          Antiretroviral compounds: Mechanisms underlying failure of HAART to eradicate HIV-1</p>

    <p>          Shehu-Xhilaga, M, Tachedjian, G, Crowe, SM, and Kedzierska, K</p>

    <p>          CURRENT MEDICINAL CHEMISTRY <b>2005</b>.  12(15): 1705-1719, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229763600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229763600001</a> </p><br />

    <p>23.   47622   HIV-LS-326; WOS-HIV-7/3/2005</p>

    <p class="memofmt1-2">          Synthesis of 2-(aminocarbonylmethylthio)-1H-imidazoles as novel Capravirine analogues</p>

    <p>          Loksha, YM, El-Barbary, AA, El-Badawi, MA, Nielsen, C, and Pedersen, EB</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(13): 4209-4220, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229811900009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229811900009</a> </p><br />

    <p>24.   47623   HIV-LS-326; WOS-HIV-7/10/2005</p>

    <p class="memofmt1-2">          The molecular mechanism of human resistance to HIV-1 infection in persistently infected individuals - A review, hypothesis and implications</p>

    <p>          Becker, Y</p>

    <p>          VIRUS GENES <b>2005</b>.  31(1): 113-120, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229908000014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229908000014</a> </p><br />

    <p>25.   47624   HIV-LS-326; WOS-HIV-7/10/2005</p>

    <p class="memofmt1-2">          Isolation and characterization of the ALP1 protease from Aspergillus fumigatus and its protein inhibitor from Physarium polycephalum</p>

    <p>          Davies, DA, Kalinina, NA, Samokhvalova, LV, Malakhova, GV, Scott, G, Venning, G, Volynskaya, AM, and Nesmeyanov, VA</p>

    <p>          RUSSIAN JOURNAL OF BIOORGANIC CHEMISTRY <b>2005</b>.  31(3): 229-237, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230025700007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230025700007</a> </p><br />

    <p>26.   47625   HIV-LS-326; WOS-HIV-7/10/2005</p>

    <p class="memofmt1-2">          Sesquiterpenes and butenolides, natural anti-HIV constituents from Litsea verticillata</p>

    <p>          Zhang, HJ, Van Hung, N, Cuong, NM, Soejarto, DD, Pezzuto, JM, Fong, HHS, and Tan, GT</p>

    <p>          PLANTA MEDICA <b>2005</b>.  71(5): 452-457</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229864200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229864200013</a> </p><br />
    <br clear="all">

    <p>27.   47626   HIV-LS-326; WOS-HIV-7/10/2005</p>

    <p class="memofmt1-2">          Pharmacokinetics of didanosine and drug resistance mutations in infants exposed to zidovudine during gestation or postnatally and treated with didanosine or zidovudine in the first three months of life</p>

    <p>          Kovacs, A, Cowles, MK, Britto, P, Capparelli, E, Fowler, MG, Moye, J, McIntosh, K, Rathore, MH, Pitt, J, and Husson, RN</p>

    <p>          PEDIATRIC INFECTIOUS DISEASE JOURNAL <b>2005</b>.  24(6): 503-509, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230042900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230042900005</a> </p><br />

    <p>28.   47627   HIV-LS-326; WOS-HIV-7/10/2005</p>

    <p class="memofmt1-2">          Antiviral activity of lamivudine in salvage therapy for multidrug-resistant HIV-1 infection</p>

    <p>          Campbell, TB, Shulman, NS, Johnson, SC, Zolopa, AR, Young, RK, Bushman, L, Fletcher, CV, Lanier, ER, Merigan, TC, and Kuritzkes, DR</p>

    <p>          CLINICAL INFECTIOUS DISEASES <b>2005</b>.  41(2): 236-242, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229890800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229890800016</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
