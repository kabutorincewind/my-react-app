

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-04-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="E0qF5D1hfVLxbHRVIGGwreB5i2yyfgvi9zXbzRSUTjRhumCbMw+Ev6tMN4dy0IARI9A3BJsWYAc1seqXRtq44rA8v8CVtX5p+nxu157hi/bHCatQ//LllPFLzlM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C5A30E04" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List:  March 18, 2009 - March 31, 2009</h1>

    <p class="memofmt2-2">Cytomegalovirus</p>

    <p class="ListParagraph">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19226140?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">4-Benzyloxy-gamma-sultone derivatives: discovery of a novel family of non-nucleoside inhibitors of human cytomegalovirus and varicella zoster virus.</a></p>

    <p class="NoSpacing">De Castro S, García-Aparicio C, Andrei G, Snoeck R, Balzarini J, Camarasa MJ, Velázquez S.</p>

    <p class="NoSpacing">J Med Chem. 2009 Mar 26;52(6):1582-91.</p>

    <p class="NoSpacing">PMID: 19226140 [PubMed - in process]</p>

    <p class="memofmt2-2">Hepatitis Virus</p>

    <p class="ListParagraph">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19332375?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Identification of novel inhibitors of HCV RNA-dependent RNA polymerase by pharmacophore-based virtual screening and in vitro evaluation.</a></p>

    <p class="NoSpacing">Ryu K, Kim ND, Choi SI, Han CK, Yoon JH, No KT, Kim KH, Seong BL.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Mar 18. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19332375 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19332062?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Oncostatin M Synergistically Inhibits HCV RNA Replication In Combination With Interferon-alpha.</a></p>

    <p class="NoSpacing">Ikeda M, Mori K, Ariumi Y, Dansako H, Kato N.</p>

    <p class="NoSpacing">FEBS Lett. 2009 Mar 27. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19332062 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19330426?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Pseudo-peptides derived from isomannide: inhibitors of serine proteases.</a></p>

    <p class="NoSpacing">Barros TG, Pinheiro S, Williamson JS, Tanuri A, Gomes M Jr, Pereira HS, Brindeiro RM, Neto JB, Antunes OA, Muri EM.</p>

    <p class="NoSpacing">Amino Acids. 2009 Mar 28. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19330426 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19307358?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Preclinical Characterization of PF-00868554, a Potent Nonnucleoside Inhibitor of the Hepatitis C Virus RNA-dependent RNA Polymerase.</a></p>
    
    <p class="NoSpacing">Shi ST, Herlihy KJ, Graham JP, Nonomiya J, Rahavendran SV, Skor H, Irvine R, Binford S, Tatlock J, Li H, Gonzalez J, Linton A, Patick AK, Lewis C.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Mar 23. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19307358 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19226162?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Inhibitors of hepatitis C virus polymerase: synthesis and biological characterization of unsymmetrical dialkyl-hydroxynaphthalenoyl-benzothiadiazines.</a></p>

    <p class="NoSpacing">Wagner R, Larson DP, Beno DW, Bosse TD, Darbyshire JF, Gao Y, Gates BD, He W, Henry RF, Hernandez LE, Hutchinson DK, Jiang WW, Kati WM, Klein LL, Koev G, Kohlbrenner W, Krueger AC, Liu J, Liu Y, Long MA, Maring CJ, Masse SV, Middleton T, Montgomery DA, Pratt JK, Stuart P, Molla A, Kempf DJ.</p>

    <p class="NoSpacing">J Med Chem. 2009 Mar 26;52(6):1659-69.</p>

    <p class="NoSpacing">PMID: 19226162 [PubMed - in process]</p>

    <p class="ListParagraph">7.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19328038?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Transfusion of autologous cytokine-induced killer cells inhibits viral replication in patients with chronic hepatitis B virus infection.</a></p>

    <p class="NoSpacing">Shi M, Fu J, Shi F, Zhang B, Tang Z, Jin L, Fan Z, Zhang Z, Chen L, Wang H, Lau GK, Wang FS.</p>

    <p class="NoSpacing">Clin Immunol. 2009 Mar 25. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19328038 [PubMed - as supplied by publisher]</p>

    <p class="memofmt2-2">SARS Corona Virus</p>

    <p class="ListParagraph">8.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19144641?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Structural Basis of Inhibition Specificities of 3C and 3C-like Proteases by Zinc-coordinating and Peptidomimetic Compounds.</a></p>

    <p class="NoSpacing">Lee CC, Kuo CJ, Ko TP, Hsu MF, Tsui YC, Chang SC, Yang S, Chen SJ, Chen HC, Hsu MC, Shih SR, Liang PH, Wang AH.</p>

    <p class="NoSpacing">J Biol Chem. 2009 Mar 20;284(12):7646-7655. Epub 2009 Jan 14.</p>

    <p class="NoSpacing">PMID: 19144641 [PubMed - as supplied by publisher]</p>

    <p class="memofmt2-2">Influenza Virus</p>

    <p class="ListParagraph">9.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19301094?ordinalpos=10&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Expression of mouse beta-defensin-3 in MDCK cells and its anti-influenza-virus activity.</a></p>
    
    <p class="NoSpacing">Jiang Y, Wang Y, Kuang Y, Wang B, Li W, Gong T, Jiang Z, Yang D, Li M.</p>
    
    <p class="NoSpacing">Arch Virol. 2009 Mar 20. [Epub ahead of print]</p>
    
    <p class="NoSpacing">PMID: 19301094 [PubMed - as supplied by publisher]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
