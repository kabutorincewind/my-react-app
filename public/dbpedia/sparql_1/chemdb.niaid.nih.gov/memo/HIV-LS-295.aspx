

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-295.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5SjzMrJPVRbB0Hr+0Vo8xA9QN4InmhHmaEZiPkDjgvSbniyMPfjOP3Qf9SoDraA9cUT71druGdXpwmm+kPZpz03SreE6UvjhbqZrzm01uwoQF7M7ELdFF5eG6UY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="808CD3B1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-295-MEMO</b> </p>

<p>    1.    43307   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Rational Design and Synthesis of Novel Dimeric Diketoacid-Containing Inhibitors of HIV-1 Integrase: Implication for Binding to Two Metal Ions on the Active Site of Integrase</p>

<p>           Long, YQ, Jiang, XH, Dayam, R, Sanchez, T, Shoemaker, R, Sei, S, and Neamati, N</p>

<p>           J Med Chem 2004. 47(10): 2561-2573</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115398&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115398&amp;dopt=abstract</a> </p><br />

<p>    2.    43308   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p class="memofmt1-2">           The Reverse Transcriptase (RT) Mutation V118I is Associated with Virologic Failure on Abacavir-based Antiretroviral Treatment (ART) in HIV-1 Infection</p>

<p>           Saeberg, Paer, Koppel, Kristina, Bratt, Goeran, Fredriksson, Eva Lena, Hejdeman, Bo, Sitbon, Gisela, and Sandstroem, Eric</p>

<p>           Scandinavian Journal of Infectious Diseases 2004. 36(1): 40-45</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    3.    43309   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           An unusual &quot;senseless&quot; 2&#39;,5&#39;-oligoribonucleotide with potent anti-HIV activity</p>

<p>           Aboul-Fadl, T, Agrawal, VK, Buckheit, RW Jr, and Broom, AD</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(3): 545-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113022&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113022&amp;dopt=abstract</a> </p><br />

<p>    4.    43310   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Design, synthesis and evaluation of potential inhibitors of HIV gp120-CD4 interactions</p>

<p>           Boussard, C, Klimkait, T, Mahmood, N, Pritchard, M, and Gilbert, IH</p>

<p>           Bioorg Med Chem Lett 2004. 14(10):  2673-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109676&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109676&amp;dopt=abstract</a> </p><br />

<p>    5.    43311   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Bis-Anthracycline Antibiotics Inhibit Human Immunodeficiency Virus Type 1 Transcription</p>

<p>           Kutsch, O, Levy, DN, Bates, PJ, Decker, J, Kosloff, BR, Shaw, GM, Priebe, W, and Benveniste, EN</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1652-1663</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105117&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105117&amp;dopt=abstract</a> </p><br />

<p>    6.    43312   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Novel 4&#39;-Substituted Stavudine Analog with Improved Anti-Human Immunodeficiency Virus Activity and Decreased Cytotoxicity</p>

<p>           Dutschman, GE, Grill, SP, Gullen, EA, Haraguchi, K, Takeda, S, Tanaka, H, Baba, M, and Cheng, YC</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1640-1646</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105115&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105115&amp;dopt=abstract</a> </p><br />

<p>    7.    43313   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Effects of Drug Resistance Mutations L100I and V106A on the Binding of Pyrrolobenzoxazepinone Nonnucleoside Inhibitors to the Human Immunodeficiency Virus Type 1 Reverse Transcriptase Catalytic Complex</p>

<p>           Locatelli, GA, Campiani, G, Cancio, R, Morelli, E, Ramunno, A, Gemma, S, Hubscher, U, Spadari, S, and Maga, G</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1570-1580</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105107&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105107&amp;dopt=abstract</a> </p><br />

<p>    8.    43314   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p><b>           Synergistic attachment inhibitor-fusion inhibitor compositions for the prevention and treatment of acquired immunodeficiency syndrome</b>((Tanox, Inc. USA)</p>

<p>           Fung, Sek Chung</p>

<p>           PATENT: WO 2004028473 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 22 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    9.    43315   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p><b>           Treatment of viral diseases by 1,3,5-triazine nucleoside and nucleotide analogs, and preparation thereof</b> ((Koronis Pharmaceuticals, Incorporated USA)</p>

<p>           Daifuku, Richard, Gall, Alexander, and Sergueev, Dmitri</p>

<p>           PATENT: WO 2004028454 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 108 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  10.    43316   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Tipranavir: the first nonpeptidic protease inhibitor</p>

<p>           Cheonis, N</p>

<p>           BETA 2004. 16(2): 15-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104065&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104065&amp;dopt=abstract</a> </p><br />

<p>  11.    43317   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p><b>           Preparation and pharmaceutical compositions of indole, azaindole and related heterocyclic 4-alkenyl piperidine amides</b> ((USA))</p>

<p>           Wang, Tao, Kadow, John F, Meanwell, Nicholas A, Yeung, Kap-Sun, Zhang, Zhongxing, Yin, Zhiwei, Qiu, Zhilei, Deon, Daniel H, James, Clint A, Ruediger, Edward H, and Bachand, Carol</p>

<p>           PATENT: US 20040063744 A1;  ISSUE DATE: 20040401</p>

<p>           APPLICATION: 2003-32154; PP: 181 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    43318   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           A 14-Day Dose-Response Study of the Efficacy, Safety, and Pharmacokinetics of the Nonpeptidic Protease Inhibitor Tipranavir in Treatment-Naive HIV-1-Infected Patients</p>

<p>           McCallister, S, Valdez, H, Curry, K, MacGregor, T, Borin, M, Freimuth, W, Wang, Y, and Mayers, DL</p>

<p>           J Acquir Immune Defic Syndr 2004.  35(4): 376-382</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15097154&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15097154&amp;dopt=abstract</a> </p><br />

<p>  13.    43319   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p><b>           Cellular protein Inip76, its presence in HIV-1 integrase protein complex, role in retroviral integration, and use as drug target</b>((K U Leuven Research &amp; Development, Belg.)</p>

<p>           Cherepanov, Peter, Debyser, Zeger, and De Clercq, Erik</p>

<p>           PATENT: GB 2393440 A1;  ISSUE DATE: 20040331</p>

<p>           APPLICATION: 2002-22361; PP: 50 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  14.    43320   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Pharmacokinetic Evaluation and Short-Term Activity of Stavudine, Nevirapine, and Nelfinavir Therapy in HIV-1-Infected Adults</p>

<p>           Skowron, G, Leoung, G, Hall, DB, Robinson, P, Lewis, R, Grosso, R, Jacobs, M, Kerr, B, MacGregor, T, Stevens, M, Fisher, A, Odgen, R, and Yen-Lieberman, B</p>

<p>           J Acquir Immune Defic Syndr 2004.  35(4): 351-358</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15097151&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15097151&amp;dopt=abstract</a> </p><br />

<p>  15.    43321   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p><b>           TAT-gp120 interaction-based mechanism for HIV-1 entry into host cells, and peptides inhibiting this mechanism</b> ((Creabilis Therapeutics S.R.L., Italy)</p>

<p>           Bussolino, Federico and Marchio, Serena</p>

<p>           PATENT: WO 2004024173 A2;  ISSUE DATE: 20040325</p>

<p>           APPLICATION: 2003; PP: 87 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    43322   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p><b>           Mutational profiles in HIV-1 protease correlated with phenotypic drug resistance</b>((Tibotec Pharmaceuticals Ltd., Ire.)</p>

<p>           De Meyer, Sandra, Azijn, Hilde, and De Bethune, Marie-pierre TMMG</p>

<p>           PATENT: WO 2004003817 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 32 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    43323   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Phase II trial of infusional cyclophosphamide, doxorubicin, and etoposide in patients with HIV-associated non-Hodgkin&#39;s lymphoma: an Eastern Cooperative Oncology Group Trial (E1494)</p>

<p>           Sparano, JA, Lee, S, Chen, MG, Nazeer, T, Einzig, A, Ambinder, RF, Henry, DH, Manalo, J, Li, T, and Von, Roenn JH</p>

<p>           J Clin Oncol 2004. 22(8): 1491-500</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084622&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084622&amp;dopt=abstract</a> </p><br />

<p>  18.    43324   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Novel halogenated nitrobenzylthioinosine analogs as es nucleoside transporter inhibitors</p>

<p>           Gupte, A and Buolamwini, JK</p>

<p>           Bioorg Med Chem Lett 2004. 14(9): 2257-60</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081020&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081020&amp;dopt=abstract</a> </p><br />

<p>  19.    43325   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Measurement of anti-HIV activity of marketed vaginal products and excipients using a PBMC-based in vitro assay</p>

<p>           Rohan, LC, Ratner, D, McCullough, K, Hiller, SL, and Gupta, P</p>

<p>           Sex Transm Dis 2004. 31(3): 143-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15076925&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15076925&amp;dopt=abstract</a> </p><br />

<p>  20.    43326   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p class="memofmt1-2">           The human immunodeficiency virus type 1 ribosomal frameshifting site is an invariant sequence determinant and an important target for antiviral therapy</p>

<p>           Biswas, Preetha, Jiang, Xi, Pacchia, Annmarie L, Dougherty, Joseph P, and Peltz, Stuart W</p>

<p>           Journal of Virology 2004. 78(4): 2082-2087</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    43327   HIV-LS-295; PUBMED-HIV-5/3/2004</p>

<p class="memofmt1-2">           Synthesis and biological evaluation of novel tert-azido or tert-amino substituted penciclovir analogs</p>

<p>           Ok, Kim H, Won, Baek H, Ryong, Moon H, Kim, DK, Woo, Chun M, and Shin, Jeong L</p>

<p>           Org Biomol Chem 2004. 2(8): 1164-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064793&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064793&amp;dopt=abstract</a> </p><br />

<p>  22.    43328   HIV-LS-295; SCIFINDER-HIV-4/27/2004</p>

<p><b>           Polymer conjugates with HIV gp41-derived peptides and their therapeutic use</b>((Trimeris, Inc. USA)</p>

<p>           Bray, Brian, Kang, Myung-Chol, Tvermoes, Nicolai, Kinder, Daniel, and Lackey, John William</p>

<p>           PATENT: WO 2004029073 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 79 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    43329   HIV-LS-295; WOS-HIV-4/25/2004</p>

<p class="memofmt1-2">           Synthesis and anti HIV-1 activity of 2-[[2-(3,5-dimethylphenoxy)ethyl]thio]pyrimidin4(3 H)-ones</p>

<p>           Novikov, MS, Ozerov, AA, Sim, OG, and Buckheit, RW</p>

<p>           KHIM GETEROTSIKL+ 2004(1): 42-47</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220011500005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220011500005</a> </p><br />

<p>  24.    43330   HIV-LS-295; WOS-HIV-4/25/2004</p>

<p class="memofmt1-2">           Design, synthesis and preliminary evaluation of peptidomimetic inhibitors of HIV aspartic protease with an epoxyalcohol core</p>

<p>           Benedetti, F, Berti, F, Miertus, S, Romeo, D, Schillani, F, and Tossi, A</p>

<p>           ARKIVOC 2003: 140-154</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220561500013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220561500013</a> </p><br />

<p>  25.    43331   HIV-LS-295; WOS-HIV-4/25/2004</p>

<p class="memofmt1-2">           Synthesis of the 5 &#39;-phosphonate of 4(S)-(6-amino-9H-purin-9-yl)tetrahy ro-2(S)-furanmethanol [S,S-IsoddA]</p>

<p>           Nair, V and Sharma, PK</p>

<p>           ARKIVOC 2003: 10-14</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220561700002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220561700002</a> </p><br />

<p>  26.    43332   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Pharmacokinetic characteristics of ritonavir, zidovudine, lamivudine, and stavudine in children with human immunodeficiency virus infection</p>

<p>           Fletcher, CV, Yogev, R, Nachman, SA, Wiznia, A, Pelton, S, McIntosh, K, and Stanley, K</p>

<p>           PHARMACOTHERAPY 2004. 24(4): 453-459</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220624600004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220624600004</a> </p><br />

<p>  27.    43333   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Orally bioavailable competitive CCR5 antagonists</p>

<p>           Thoma, G, Nuninger, F, Schaefer, M, Akyel, KG, Albert, R, Beerli, C, Bruns, C, Francotte, E, Luyten, M, MacKenzie, D, Oberer, L, Streiff, MB, Wagner, T, Walter, H, Weckbecker, G, and Zerwes, HG</p>

<p>           J MED CHEM 2004. 47(8): 1939-1955</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220642100010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220642100010</a> </p><br />

<p>  28.    43334   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Selective inhibition of HIV-1 reverse transcriptase (HIV-1 RT) RNase H by small RNA hairpins and dumbbells</p>

<p>           Hannoush, RN, Carriero, S, Min, KL, and Damha, MJ</p>

<p>           CHEMBIOCHEM 2004. 5(4): 527-533</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220773500016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220773500016</a> </p><br />

<p>  29.    43335   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           On the role of interferon regulatory factors in HIV-1 replication</p>

<p>           Marsili, G, Borsetti, A, Sgarbanti, M, Remoli, AL, Ridolfi, B, Stellacci, E, Ensoli, B, and Battistini, A</p>

<p>           ANN NY ACAD SCI 2003. 1010: 29-42</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189446300005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189446300005</a> </p><br />

<p>  30.    43336   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Six-year follow-up of HIV-1-infected adults in a clinical trial of antiretroviral therapy with indinavir, zidovudine, and lamivudine</p>

<p>           Gulick, RM, Meibohm, A, Havlir, D, Eron, JJ, Mosley, A, Chodakewitz, JA, Isaacs, R, Gonzalez, C, McMahon, D, Richman, DD, Robertson, M, and Mellors, JW</p>

<p>           AIDS 2003. 17(16): 2345-2349</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672200009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672200009</a> </p><br />

<p>  31.    43337   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Resistance of HIV-1 to multiple antiretroviral drugs in France: a 6-year survey (1997-2002) based on an analysis of over 7000 genotypes</p>

<p>           Tamalet, C, Fantini, J, Tourres, C, and Yahi, N</p>

<p>           AIDS 2003. 17(16): 2383-2388</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672200014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672200014</a> </p><br />

<p>  32.    43338   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           An open-label assessment of TMC 125-a new, next-generation NNRTI, for 7 days in HIV-1 infected individuals with NNRT1 resistance</p>

<p>           Gazzard, BG, Pozniak, AL, Rosenbaum, W, Yeni, GP, Staszewski, S, Arasteh, K, De, Dier K, Peeters, M, Woodfall, B, Stebbing, J, and Vant&#39;, Klooster GAE</p>

<p>           AIDS 2003. 17(18): F49-F54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672800001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672800001</a> </p><br />

<p>  33.    43339   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           HIV biological variability unveiled: frequent isolations and chimeric receptors reveal unprecedented variation of coreceptor use</p>

<p>           Karlsson, I, Antonsson, L, Shi, Y, Karlsson, A, Albert, J, Leitner, T, Olde, B, Owman, C, and Fenyo, EM</p>

<p>           AIDS 2003. 17(18): 2561-2569</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672800003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672800003</a> </p><br />

<p>  34.    43340   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Molecular mapping of epitopes for interaction of HIV-1 as well as natural ligands with the chemokine receptors, CCR5 and CXCR4</p>

<p>           Antonsson, L, Boketoft, A, Garzino-Demo, A, Olde, B, and Owman, C</p>

<p>           AIDS 2003. 17(18): 2571-2579</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672800004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220672800004</a> </p><br />

<p>  35.    43341   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Clinical and immunological impact of HIV envelope V3 sequence variation after starting initial triple antiretroviral therapy</p>

<p>           Brumme, ZL, Dong, WWY, Yip, B, Wynhoven, B, Hoffman, NG, Swanstrom, R, Jensen, MA, Mullins, JI, Hogg, RS, Montaner, JSG, and Harrigan, PR</p>

<p>           AIDS 2004. 18(4): F1-F9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220647400001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220647400001</a> </p><br />

<p>  36.    43342   HIV-LS-295; WOS-HIV-5/2/2004</p>

<p class="memofmt1-2">           Evaluation of nevirapine and/or hydroxyurea with nucleoside reverse transcriptase inhibitors in treatment-naive HIV-1-infected subjects</p>

<p>           Blanckenberg, DH, Wood, R, Horban, A, Beniowski, M, Boron-Kaczmarska, A, Trocha, H, Halota, W, Schmidt, RE, Fatkenheuer, G, Jessen, H, and Lange, JMA</p>

<p>           AIDS 2004. 18(4): 631-640</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220647400007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220647400007</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
