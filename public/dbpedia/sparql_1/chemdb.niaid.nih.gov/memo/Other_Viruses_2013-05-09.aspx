

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-05-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="418ygIm43k/UyXoAC+u0ilPBUU4XA48kye31PRJFURHGseTSfn3tyzeGb954bYpWNVs9Uz0mNZLHNfY3eGcGbhLVq7GmF7KQfzOakA16Mi6cPN+lYYG4Zinz89s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F91CE054" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: April 26 - May 9, 2013</h1>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23475956">Bortezomib and SAHA Synergistically Induce ROS-Driven Caspase-dependent Apoptosis of Nasopharyngeal Carcinoma and Block Replication of Epstein-Barr Virus.</a> Hui, K.F., B.H. Lam, D.N. Ho, S.W. Tsao, and A.K. Chiang. Molecular Cancer Therapeutics, 2013.  <b>[Epub ahead of print]</b>; PMID[23475956].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23350710">Sulfated Small Molecules Targeting EBV in Burkitt Lymphoma: From in Silico Screening to the Evidence of in Vitro Effect on Viral Episomal DNA.</a> Lima, R.T., H. Seca, A. Palmeira, M.X. Fernandes, F. Castro, M. Correia-da-Silva, M.S. Nascimento, E. Sousa, M. Pinto, and M.H. Vasconcelos. Chemical Biology &amp; Drug Design, 2013. 81(5): p. 631-644; PMID[23350710].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23449792">Maribavir Inhibits Epstein-Barr Virus Transcription through the EBV Protein Kinase.</a> Whitehurst, C.B., M.K. Sanders, M. Law, F.Z. Wang, J. Xiong, D.P. Dittmer, and J.S. Pagano. Journal of Virology, 2013. 87(9): p. 5311-5315; PMID[23449792].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23246506">The Entry Inhibitor Myrcludex-B Efficiently Blocks Intrahepatic Virus Spreading in Humanized Mice Previously Infected with Hepatitis B Virus.</a> Volz, T., L. Allweiss, M.B. MB, M. Warlich, A.W. Lohse, J.M. Pollok, A. Alexandrov, S. Urban, J. Petersen, M. Lutgehetmann, and M. Dandri. Journal of Hepatology, 2013. 58(5): p. 861-867; PMID[23246506].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23511018">Evaluation of anti-HCV Activity and SAR Study of (+)-Lycoricidine through Targeting of Host Heat-stress Cognate 70 (Hsc70).</a> Chen, D.Z., J.D. Jiang, K.Q. Zhang, H.P. He, Y.T. Di, Y. Zhang, J.Y. Cai, L. Wang, S.L. Li, P. Yi, Z.G. Peng, and X.J. Hao. Bioorganic &amp; Medicinal Chemistry Letters, 2013. <b>23</b>(9): p. 2679-2682; PMID[23511018].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17713156">Resistance Profiling of Hepatitis C Virus Protease Inhibitors Using Full-length NS3.</a> Dahl, G., A. Sandstrom, E. Akerblom, and U.H. Danielson. Antiviral Therapy, 2007. 12(5): p. 733-740; PMID[17713156].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23629699">Identification of PTC725: An Orally Bioavailable Small Molecule That Selectively Targets the Hepatitis C Virus NS4B Protein.</a> Gu, Z., J.D. Graci, F.C. Lahser, J. Breslin, S.P. Jung, J.H. Crona, P. McMonagle, E. Xia, S. Liu, G. Karp, J. Zhu, S. Huang, A. Nomeir, M. Weetall, N.G. Almstead, S.W. Peltz, X. Tong, R. Ralston, and J.M. Colacino. Antimicrobial Agents and Chemotherapy, 2013.  <b>[Epub ahead of print]</b>; PMID[23629699].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23559483">Synthesis and Cytostatic and Antiviral Activities of 2&#39;-Deoxy-2&#39;,2&#39;-difluororibo- and 2&#39;-Deoxy-2&#39;-fluororibonucleosides Derived from 7-(Het)aryl-7-deazaadenines.</a> Perlikova, P., L. Eberlin, P. Menova, V. Raindlova, L. Slavetinska, E. Tloustova, G. Bahador, Y.J. Lee, and M. Hocek. ChemMedChem, 2013. 8(5): p. 832-846; PMID[23559483].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23545108">Discovery of a Novel Series of Non-nucleoside Thumb Pocket 2 HCV NS5B Polymerase Inhibitors.</a> Stammers, T.A., R. Coulombe, J. Rancourt, B. Thavonekham, G. Fazal, S. Goulet, A. Jakalian, D. Wernic, Y. Tsantrizos, M.A. Poupart, M. Bos, G. McKercher, L. Thauvette, G. Kukolj, and P.L. Beaulieu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(9): p. 2585-2589; PMID[23545108].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23629709">ACH-806, an NS4A Antagonist, Inhibits Hepatitis C Virus Replication through Altering the Composition of Viral Replication Complexes.</a> Yang, W., Y. Sun, X. Hou, Y. Zhao, J. Fabrycki, D. Chen, X. Wang, A. Agarwal, A. Phadke, M. Deshpande, and M. Huang. Antimicrobial Agents and Chemotherapy, 2013.  <b>[Epub ahead of print]</b>; PMID[23629709].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23637403">Antiviral Activity of Trappin-2 and Elafin in Vitro and in Vivo against Genital Herpes.</a> Drannik, A.G., K. Nag, J.M. Sallenave, and K.L. Rosenthal. Journal of Virology, 2013.  <b>[Epub ahead of print]</b>; PMID[23637403].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0426-050913.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316407400047">N-Substituted benzyl matrinic acid Derivatives Inhibit Hepatitis C Virus (HCV) Replication through Down-regulating Host Heat-stress Cognate 70 (Hsc70) Expression.</a> Du, N.N., Z.G. Peng, C.W. Bi, S. Tang, Y.H. Li, J.R. Li, Y.P. Zhu, J.P. Zhang, Y.X. Wang, J.D. Jiang, and D.Q. Song. Plos One, 2013. 8(3); ISI[000316407400047].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000316770100035">Design of Histidine-rich Peptides with Enhanced Bioavailability and Inhibitory Activity against Hepatitis C Virus.</a> Hong, W., R.H. Zhang, Z.Y. Di, Y.W. He, Z.H. Zhao, J. Hu, Y.L. Wu, W.X. Li, and Z.J. Cao. Biomaterials, 2013. 34(13): p. 3511-3522; ISI[000316770100035].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000316831300018">Design, Synthesis and Antiviral Activity of 2-(3-Amino-4-piperazinylphenyl)chromone Derivatives.</a> Kim, M.K., H. Yoon, D.L. Barnard, and Y. Chong. Chemical &amp; Pharmaceutical Bulletin, 2013. 61(4): p. 486-488; ISI[000316831300018].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000316642900066">Anti-Hepatitis B Virus Lignans from the Root of Streblus asper.</a> Li, J., A.P. Meng, X.L. Guan, J. Li, Q. Wu, S.P. Deng, X.J. Su, and R.Y. Yang. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2238-2244; ISI[000316642900066].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0426-050913.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000316770300043">Discovery of Novel Tricyclic Indole Derived Inhibitors of HCV NS5B RNA Dependent RNA Polymerase.</a> Venkatraman, S., F. Velazquez, S. Gavalas, W.L. Wu, K.X. Chen, A.G. Nair, F. Bennett, Y.H. Huang, P. Pinto, Y.H. Jiang, O. Selyutin, B. Vibulbhan, Q.B. Zeng, C. Lesburg, J. Duca, H.C. Huang, S. Agrawal, C.K. Jiang, E. Ferrari, C. Li, J. Kozlowski, S. Rosenblum, N.Y. Shih, and F.G. Njoroge. Bioorganic &amp; Medicinal Chemistry, 2013. 21(7): p. 2007-2017; ISI[000316770300043].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0426-050913.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
