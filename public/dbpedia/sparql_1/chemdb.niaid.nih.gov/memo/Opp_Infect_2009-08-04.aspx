

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-08-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+UO4wriD0kgaBfONpTTaiLA5aFXLHZHBPZ05n/1z9LjD7xvnIW+nW6hAaFIkaC0lkvu4PUu6qvC/DewxUVfOsl6ckvip8ixlKVpVLykNZ7CXWWw51uBnXRQFGXo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F1FC15F3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">July 22, 2009 - August 4, 2009</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19650630?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of New Linear Guanidines and Macrocyclic Amidinourea Derivatives Endowed with High Antifungal Activity against Candida spp. and Aspergillus spp.</a></p>

    <p class="NoSpacing">Manetti F, Castagnolo D, Raffi F, Zizzari AT, Rajama&#776;ki S, D&#39;Arezzo S, Visca P, Cona A, Fracasso ME, Doria D, Posteraro B, Sanguinetti M, Fadda G, Botta M.</p>

    <p class="NoSpacing">J Med Chem. 2009 Aug 3. [Epub ahead of print]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19648579?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of novel cell wall destabilizing antifungal compounds using a conditional Aspergillus nidulans protein kinase C mutant.</a></p>

    <p class="NoSpacing">Mircus G, Hagag S, Levdansky E, Sharon H, Shadkchan Y, Shalit I, Osherov N.</p>

    <p class="NoSpacing">J Antimicrob Chemother. 2009 Jul 31. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19648579 [PubMed - as supplied by publisher]</p> 

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">3. Castagnolo, D., et al., <i>Synthesis, biological evaluation, and SAR study of novel pyrazole analogues as inhibitors of Mycobacterium tuberculosis: part 2. Synthesis of rigid pyrazolones.</i> Bioorg Med Chem, 2009. <b>17</b>(15): p. 5716-21 <a href="http://www.ncbi.nlm.nih.gov/pubmed/19581099?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19581099?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">4. Fassihi, A., et al., <i>Synthesis and antitubercular activity of novel 4-substituted imidazolyl-2,6-dimethyl-N3,N5-bisaryl-1,4-dihydropyridine-3,5-dicarboxamides.</i> Eur J Med Chem, 2009. <b>44</b>(8): p. 3253-8 <a href="http://www.ncbi.nlm.nih.gov/pubmed/19371979?ordinalpos=15&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19371979?ordinalpos=15&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">5. Molina-Salinas, G.M., et al., <i>Antituberculosis activity of natural and semisynthetic azorellane and mulinane diterpenoids.</i> Fitoterapia, 2009 <a href="http://www.ncbi.nlm.nih.gov/pubmed/19635530?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19635530?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">6. Pandey, J., et al., <i>Synthesis and antitubercular screening of imidazole derivatives.</i> Eur J Med Chem, 2009. 44(8): p. 3350-5 <a href="http://www.ncbi.nlm.nih.gov/pubmed/19272678?ordinalpos=16&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19272678?ordinalpos=16&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">7. Petrelli, R., et al., <i>Selective inhibition of nicotinamide adenine dinucleotide kinases by dinucleoside disulfide mimics of nicotinamide adenine dinucleotide analogues.</i> Bioorg Med Chem, 2009. <b>17</b>(15): p. 5656-64 <a href="http://www.ncbi.nlm.nih.gov/pubmed/19596199?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19596199?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">8. Zalavadiya, P., et al., <i>Multi-component synthesis of dihydropyrimidines by iodine catalyst at ambient temperature and in-vitro antimycobacterial activity.</i> Arch Pharm (Weinheim), 2009. <b>342</b>(8): p. 469-75 <a href="http://www.ncbi.nlm.nih.gov/pubmed/19565601?ordinalpos=10&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19565601?ordinalpos=10&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>

    <h2>Antimicrobials, Antifungals (General)</h2>

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19648006?ordinalpos=18&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antitubercular activity of alpha,omega-diaminoalkanes, H(2)N(CH(2))(n)NH(2).</a></p>

    <p class="NoSpacing">Vergara FM, Henriques MD, Candea AL, Wardell JL, De Souza MV.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Jul 22. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19648006 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19646867?ordinalpos=19&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and antibacterial activity of 7-(1,2,3,4-tetrahydropyrrolo[1,2-a]pyrazin-7-yl) quinolones.</a></p>

    <p class="NoSpacing">Zhu B, Marinelli BA, Goldschmidt R, Foleno BD, Hilliard JJ, Bush K, Macielag MJ.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Jul 22. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19646867 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19635954?ordinalpos=31&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel broad-spectrum bis-(imidazolinylindole) derivatives with potent antibacterial activity against antibiotic-resistant strains.</a></p>

    <p class="NoSpacing">Panchal RG, Ulrich RL, Lane D, Butler MM, Houseweart C, Opperman T, Williams JD, Peet NP, Moir DT, Nguyen T, Gussio R, Bowlin T, Bavari S.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Jul 27. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19635954 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">12. Walton, JGA, et al.</p>

    <p class="NoSpacing">Synthesis and biological evaluation of functionalized tetrahydro-beta-carboline analogues as inhibitors of Toxoplasma gondii invasion</p>

    <p class="NoSpacing">ORGANIC &amp; BIOMOLECULAR CHEMISTRY  2009 7: 3049 - 3060</p> 

    <p class="ListParagraph">13. Gutierrez-Lugo, MT, et al.</p>

    <p class="NoSpacing">Dequalinium, a New Inhibitor of Mycobacterium tuberculosis Mycothiol Ligase Identified by High-Throughput Screening</p>

    <p class="NoSpacing">JOURNAL OF BIOMOLECULAR SCREENING  2009 (July) 14: 643 - 652</p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
