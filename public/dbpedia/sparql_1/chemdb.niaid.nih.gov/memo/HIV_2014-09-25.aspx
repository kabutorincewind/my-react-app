

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-09-25.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QGdSMTuCl3cv5Kd7IHkQaWucMUOxaitIc8rlLRvrLhPaZ005Lj7b23Oms+EakBTvpALfE019Iks8zQKbHjthzmRTgYcEnZ02UlJPXfDTQHYmNUDrz/qdsBfc56U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6E3C0B2B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: September 12 - September 25, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25156906">Conjugation of a Nonspecific Antiviral Sapogenin with a Specific HIV Fusion Inhibitor: A Promising Strategy for Discovering New Antiviral Therapeutics.</a> Wang, C., L. Lu, H. Na, X. Li, Q. Wang, X. Jiang, X. Xu, F. Yu, T. Zhang, J. Li, Z. Zhang, B. Zheng, G. Liang, L. Cai, S. Jiang, and K. Liu. Journal of Medicinal Chemistry, 2014. 57(17): p. 7342-7354. PMID[25156906].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">2<a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25107902">. Identification of RFPL3 Protein as a Novel E3 Ubiquitin Ligase Modulating the Integration Activity of Human Immunodeficiency Virus, Type 1 Preintegration Complex Using a Microtiter Plate-based Assay.</a> Tan, B.H., Y. Suzuki, H. Takahashi, P.H. Ying, C. Takahashi, Q. Han, W.X. Chin, S.H. Chao, T. Sawasaki, N. Yamamoto, and Y. Suzuki. Journal of Biological Chemistry, 2014. 289(38): p. 26368-26382. PMID[25107902].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25155019">Passive Transfer of Modest Titers of Potent and Broadly Neutralizing anti-HIV Monoclonal Antibodies Block SHIV Infection in Macaques.</a> Shingai, M., O.K. Donau, R.J. Plishka, A. Buckler-White, J.R. Mascola, G.J. Nabel, M.C. Nason, D. Montefiori, B. Moldt, P. Poignard, R. Diskin, P.J. Bjorkman, M.A. Eckhaus, F. Klein, H. Mouquet, J.C. Cetrulo Lorenzi, A. Gazumyan, D.R. Burton, M.C. Nussenzweig, M.A. Martin, and Y. Nishimura. The Journal of Experimental Medicine, 2014. 211(10): p. 2061-2074. PMID[25155019].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24969820">Novel High-throughput Screen Identifies an HIV-1 Reverse Transcriptase Inhibitor with a Unique Mechanism of Action.</a> Sheen, C.W., O. Alpturk, and N. Sluis-Cremer. The Biochemical Journal, 2014. 462(3): p. 425-432. PMID[24969820].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25221866">Enantiomerically Pure Phosphonated Carbocyclic 2&#39;-Oxa-3&#39;-azanucleosides: Synthesis and Biological Evaluation.</a> Romeo, R., C. Carnovale, S.V. Giofre, G. Monciino, M.A. Chiacchio, C. Sanfilippo, and B. Macchi. Molecules, 2014. 19(9): p. 14406-14416. PMID[25221866].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25176328">Semi-synthesis of Oxygenated Dolabellane Diterpenes with Highly in Vitro anti-HIV-1 Activity.</a> Pardo-Vargas, A., F.A. Ramos, C.C. Cirne-Santos, P.R. Stephens, I.C. Paixao, V.L. Teixeira, and L. Castellanos. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(18): p. 4381-4383. PMID[25176328].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">7. Novel <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25176191">Indole Based NNRTIs with Improved Potency against Wild Type and Resistant HIV.</a> Muller, R., I. Mulani, A.E. Basson, N. Pribut, M. Hassam, L. Morris, W.A. van Otterlo, and S.C. Pelly. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(18): p. 4376-4380. PMID[25176191].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">8. Functional <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24988251">and Structural Characterization of 2-Amino-4-phenylthiazole Inhibitors of the HIV-1 Nucleocapsid Protein with Antiviral Activity.</a> Mori, M., A. Nucci, M.C. Lang, N. Humbert, C. Boudier, F. Debaene, S. Sanglier-Cianferani, M. Catala, P. Schult-Dietrich, U. Dietrich, C. Tisne, Y. Mely, and M. Botta. ACS Chemical Biology, 2014. 9(9): p. 1950-1955. PMID[24988251].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25234608">Novel Theoretically Designed HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors Derived from Nevirapine.</a> Liu, J., X. He, and J.Z. Zhang. Journal of Molecular Modeling, 2014. 20(10): p. 2451. PMID[25234608].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">10. Modular <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25144111">Assembly of Purine-like Bisphosphonates as Inhibitors of HIV-1 Reverse Transcriptase.</a> Lacbay, C.M., J. Mancuso, Y.S. Lin, N. Bennett, M. Gotte, and Y.S. Tsantrizos. Journal of Medicinal Chemistry, 2014. 57(17): p. 7435-7449. PMID[25144111].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25056130">Cell-penetrating, Dimeric Alpha-helical Peptides: Nanomolar Inhibitors of HIV-1 Transcription.</a> Jang, S., S. Hyun, S. Kim, S. Lee, I.S. Lee, M. Baba, Y. Lee, and J. Yu. Angewandte Chemie International Edition, 2014. 53(38): p. 10086-10089. PMID[25056130].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25128456">Discovery of a Novel HIV-1 Integrase Inhibitor from Natural Compounds through Structure Based Virtual Screening and Cell Imaging.</a> Gu, W.G., X. Zhang, D.T. Ip, L.M. Yang, Y.T. Zheng, and D.C. Wan. FEBS Letters, 2014. 588(18): p. 3461-3468. PMID[25128456].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0912-092514.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341051800001">MXB Binds to the HIV-1 Core and Prevents the Uncoating Process of HIV-1.</a> Fricke, T., T.E. White, B. Schulte, D. Vieira, A. Dharan, E.M. Campbell, A. Brandariz-Nunez, and F. Diaz-Griffero. Retrovirology, 2014. 11(68): 14pp. ISI[000341051800001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000340939700009">Sydnone Sulfonamide Derivatives as Antibacterial, Antifungal, Antiproliferative and anti-HIV Agents.</a> Asundaria, S.T., C. Pannecouque, C.E. De, and K.C. Patel. Pharmaceutical Chemistry Journal, 2014. 48(4): p. 260-268. ISI[000340939700009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341062700007">Synthesis and Evaluation of Novel Nucleic acid Derivatives as Bioactive Substances.</a> Sakakibara, N. Yakugaku Zasshi-Journal of the Pharmaceutical Society of Japan, 2014. 134(9): p. 965-972. ISI[000341062700007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341236900015">New Lignans from the Leaves and Stems of Schisandra chinensis and Their anti-HIV-1 Activities.</a> Shi, Y.M., W.M. Zhong, H. Chen, R.R. Wang, S.Z. Shang, C.Q. Liang, Z.H. Gao, Y.T. Zheng, W.L. Xiao, and H.D. Sun. Chinese Journal of Chemistry, 2014. 32(8): p. 734-740. ISI[000341236900015].
    <br />
    <b>[WOS]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341065800017">Synthesis and Biological Evaluation of a Novel Class of beta-Carboline Derivatives.</a> Chen, H., P.C. Gao, M. Zhang, W. Liao, and J.W. Zhang. New Journal of Chemistry, 2014. 38(9): p. 4155-4166. ISI[000341065800017].
    <br />
    <b>[WOS]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341071000002">Synthesis, Crystal Structure and Anti-integrase Activity of 25,27-bis[(Z)-4-(p-Methoxyphenyl)-4-hydroxybut-3-en-2-one-1-methyl]-26,28-dihydroxycalix[4]arene.</a> Luo, Z.G., Y. Zhao, C. Ma, L. Cao, S.H. Ai, J.S. Hu, and X.M. Xu. Chinese Journal of Structural Chemistry, 2014. 33(8): p. 1117-1122. ISI[000341071000002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0912-092514.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341024300013">Design and Synthesis of Potent Macrocyclic HIV-1 Protease Inhibitors Involving P1-P2 Ligands.</a> Ghosh, A.K., G.E. Schiltz, L.N. Rusere, H.L. Osswald, D.E. Walters, M. Amano, and H. Mitsuya. Organic &amp; Biomolecular Chemistry, 2014. 12(35): p. 6842-6854. ISI[000341024300013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0912-092514.</p>

    <br />  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
