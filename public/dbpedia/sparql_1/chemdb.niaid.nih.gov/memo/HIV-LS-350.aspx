

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-350.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="YWd0aHPXPyevIOJfMkGXaFW+O3DWQxoXjDlHNKdLZT3onJKOxo8ClxjFKUP9sUBFEsH93+eWu6uJ9zb287fjYy0q3wO2mudwGAKd4u2FV5s8pGq+xC3ugYhAh2E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="44DD94A7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-350-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48993   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          QSAR for non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Duchowicz, PR, Fernandez, M, Caballero, J, Castro, EA, and Fernandez, FM</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16766190&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16766190&amp;dopt=abstract</a> </p><br />

    <p>2.     48994   HIV-LS-350; EMBASE-HIV-6/12/2006</p>

    <p class="memofmt1-2">          Silencing of HIV by hairpin-loop-structured DNA oligonucleotide</p>

    <p>          Moelling, Karin, Abels, Susanne, Jendis, Joerg, Matskevich, Alexey, and Heinrich, Jochen</p>

    <p>          FEBS Letters <b>2006</b>.  580(14): 3545-3550</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4K126S7-5/2/c5a2eb14780c24cfc2f3494551f33a77">http://www.sciencedirect.com/science/article/B6T36-4K126S7-5/2/c5a2eb14780c24cfc2f3494551f33a77</a> </p><br />

    <p>3.     48995   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          1&#39;S-1&#39;-Acetoxychavicol acetate isolated from Alpinia galanga inhibits human immunodeficiency virus type 1 replication by blocking Rev transport</p>

    <p>          Ye, Y and Li, B</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 7): 2047-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16760408&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16760408&amp;dopt=abstract</a> </p><br />

    <p>4.     48996   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of carboacyclic nucleosides with (Z) and (E)-9-[4,4-bis(hydroxymethyl)]-2-butenyl side chain</p>

    <p>          Tang, Y, Muthyala, R, and Vince, R</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759867&amp;dopt=abstract</a> </p><br />

    <p>5.     48997   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          In vitro evaluation of the anti-HIV activity and metabolic interactions of tenofovir and emtricitabine</p>

    <p>          Borroto-Esoda, K, Vela, JE, Myrick, F, Ray, AS, and Miller, MD</p>

    <p>          Antivir Ther <b>2006</b>.  11(3): 377-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759055&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759055&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48998   HIV-LS-350; EMBASE-HIV-6/12/2006</p>

    <p class="memofmt1-2">          Simple determination of the HIV protease inhibitor atazanavir in human plasma by high-performance liquid chromatography with UV detection</p>

    <p>          Loregian, Arianna, Pagni, Silvana, Ballarin, Elisa, Sinigalia, Elisa, Parisi, Saverio Giuseppe, and Palu, Giorgio</p>

    <p>          Journal of Pharmaceutical and Biomedical Analysis <b>2006</b> .  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGX-4K4WMSM-9/2/cfcd6ea47700f6f4c04b9651339bf487">http://www.sciencedirect.com/science/article/B6TGX-4K4WMSM-9/2/cfcd6ea47700f6f4c04b9651339bf487</a> </p><br />

    <p>7.     48999   HIV-LS-350; EMBASE-HIV-6/12/2006</p>

    <p class="memofmt1-2">          Potent inhibition of HIV-1 gene expression and TAT-mediated apoptosis in human T cells by novel mono- and multitarget anti-TAT/Rev/Env ribozymes and a general purpose RNA-cleaving DNA-enzyme</p>

    <p>          Unwalla, Hoshang, Chakraborti, Samitabh, Sood, Vikas, Gupta, Nidhi, and Banerjea, Akhil C</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K4PVV7-2/2/6d48c8d6ca6b8bf8dc31a9ad13f94d27">http://www.sciencedirect.com/science/article/B6T2H-4K4PVV7-2/2/6d48c8d6ca6b8bf8dc31a9ad13f94d27</a> </p><br />

    <p>8.     49000   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          Recent Progress in the Design of Small Molecule Inhibitors of HIV RNase H</p>

    <p>          Klumpp, K and Mirzadegan, T</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(15): 1909-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724956&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724956&amp;dopt=abstract</a> </p><br />

    <p>9.     49001   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          Assault on Resistance: The Use of Computational Chemistry in the Development of Anti-HIV Drugs</p>

    <p>          Smith, MB, Smith, RH Jr, and Jorgensen, WL</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(15): 1843-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724951&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724951&amp;dopt=abstract</a> </p><br />

    <p>10.   49002   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          HIV-1 Reverse Transcriptase Inhibitors: Drug Resistance and Drug Development</p>

    <p>          Sluis-Cremer, N and Ross, T</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(15): 1809-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724948&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724948&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   49003   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          Design, molecular modeling, synthesis, and anti-HIV-1 activity of new indolyl aryl sulfones. Novel derivatives of the indole-2-carboxamide</p>

    <p>          Ragno, R, Coluccia, A, La, Regina G, De, Martino G, Piscitelli, F, Lavecchia, A, Novellino, E, Bergamini, A, Ciaprini, C, Sinistro, A, Maga, G, Crespan, E, Artico, M, and Silvestri, R</p>

    <p>          J Med Chem <b>2006</b>.  49(11): 3172-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722636&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722636&amp;dopt=abstract</a> </p><br />

    <p>12.   49004   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          Nonsymmetrical cyclic urea inhibitors of HIV-1 aspartic protease</p>

    <p>          Edwards, P</p>

    <p>          Drug Discov Today <b>2006</b>.  11(11-12): 569-570</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713910&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713910&amp;dopt=abstract</a> </p><br />

    <p>13.   49005   HIV-LS-350; PUBMED-HIV-6/14/2006</p>

    <p class="memofmt1-2">          Azadideoxyadenosine: Synthesis, enzymology, and anti-HIV activity</p>

    <p>          Nishonov, AA, Ma, X, and Nair, V</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16698265&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16698265&amp;dopt=abstract</a> </p><br />

    <p>14.   49006   HIV-LS-350; WOS-HIV-6/4/2006</p>

    <p class="memofmt1-2">          Proteasome inhibitors uncouple rhesus TRIM5 alpha restriction of HIV-1 reverse transcription and infection</p>

    <p>          Wu, XL, Anderson, JL, Campbell, EM, Joseph, AM, and Hope, TJ</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2006</b>.  103(19): 7465-7470, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237589000049">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237589000049</a> </p><br />

    <p>15.   49007   HIV-LS-350; WOS-HIV-6/4/2006</p>

    <p class="memofmt1-2">          Limenin, a defensin-like peptide with multiple exploitable activities from shelf beans</p>

    <p>          Wong, JH and Ng, TB</p>

    <p>          JOURNAL OF PEPTIDE SCIENCE <b>2006</b>.  12(5): 341-346, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237479900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237479900005</a> </p><br />

    <p>16.   49008   HIV-LS-350; WOS-HIV-6/4/2006</p>

    <p class="memofmt1-2">          Discovery and development of small-molecule chemokine coreceptor CCR5 antagonists</p>

    <p>          Palani, A and Tagat, JR</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(10): 2851-2857, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237555400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237555400001</a> </p><br />
    <br clear="all">

    <p>17.   49009   HIV-LS-350; WOS-HIV-6/4/2006</p>

    <p class="memofmt1-2">          Progress in identifying peptides and small-molecule inhibitors targeted to gp41 of HIV-1</p>

    <p>          Debnath, AK</p>

    <p>          EXPERT OPINION ON INVESTIGATIONAL DRUGS <b>2006</b>.  15(5): 465-478, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237554400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237554400002</a> </p><br />

    <p>18.   49010   HIV-LS-350; WOS-HIV-6/4/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of phosphonated dihydroisoxazole nucleosides</p>

    <p>          Romeo, G, Iannazzo, D, Piperno, A, Romeo, R, Saglimbeni, M, Chiacchio, MA, Balestrieri, E, Macchi, B, and Mastino, A</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2006</b>.  14(11): 3818-3824, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237498500020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237498500020</a> </p><br />

    <p>19.   49011   HIV-LS-350; WOS-HIV-6/4/2006</p>

    <p class="memofmt1-2">          Discovery of GS9148, a novel nucleotide HIV reverse transcriptase (RT) inhibitor</p>

    <p>          Mackman, R, Boojamra, C, Chen, J, Douglas, J, Grant, D, Kim, C, Lin, KY, Markevitch, D, Prasad, V, Ray, A, Zhang, L, and Cihlar, T</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  70(1): A40-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237011200038</a> </p><br />

    <p>20.   49012   HIV-LS-350; WOS-HIV-6/11/2006</p>

    <p class="memofmt1-2">          Improved QSAR modeling of anti-hiv-1 acivities by means of the optimized correlation weights of local graph invariants</p>

    <p>          Marino, DJG, Castro, EA, and Toropov, A</p>

    <p>          CENTRAL EUROPEAN JOURNAL OF CHEMISTRY <b>2006</b>.  4(1): 135-148, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237702500010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237702500010</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
