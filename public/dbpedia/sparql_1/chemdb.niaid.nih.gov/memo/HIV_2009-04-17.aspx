

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-04-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="p5pIJICjL2cd7ZDQL/tn1tY0D8ZuQ/d8+r0RfYkSO8G4Buj0N0UmEVFf6xCs3VEpxoEZVhM3ywUIvMb0HzEnvIdc6BnMH9xFm2qSYqiM63dgPE0lNZNNV/78Az0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2B747DCD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 1 - April 14, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19358515?ordinalpos=11&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of Novel HIV Entry Inhibitors for the CXCR4 Receptor by Prospective Virtual Screening.</a> Pe&#769;rez-Nueno VI, Pettersson S, Ritchie DW, Borrell JI, Teixido&#769; J. J Chem Inf Model. 2009 Apr 9. [Epub ahead of print]</p>

    <p class="pmid"> PMID: 19358515</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19353609?ordinalpos=22&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Enzymatic Glycosylation of Triazole-Linked GlcNAc/Glc-Peptides: Synthesis, Stability and Anti-HIV Activity of Triazole-Linked HIV-1 gp41 Glycopeptide C34 Analogues.</a> Huang W, Groothuys S, Heredia A, Kuijpers BH, Rutjes FP, van Delft FL, Wang LX. Chembiochem. 2009 Apr 7. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19353609</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19353537?ordinalpos=23&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Non-nucleoside HIV-1 reverse transcriptase inhibitors. Part 13: synthesis of fluorine-containing diaryltriazine derivatives for in vitro anti-HIV evaluation against wild-type strain.</a> Xiong YZ, Chen FE, Balzarini J, De Clercq E, Pannecouque C. Chem Biodivers. 2009 Apr;6(4):561-8.</p>

    <p class="pmid">PMID: 19353537</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19349871?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The Differential Binding and Activity of PRO 2000 Against Diverse HIV-1 Envelopes.</a> Sachdev DD, Zerhouni-Layachi B, Ortigoza M, Profy AT, Tuen M, Hioe CE, Klotman ME. J Acquir Immune Defic Syndr. 2009 Apr 4. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19349871</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19348416?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Water-Soluble Prodrugs of the Human Immunodeficiency Virus Protease Inhibitors Lopinavir and Ritonavir.</a> Degoey DA, Grampovnik DJ, Flosi WJ, Marsh KC, Wang XC, Klein LL, McDaniel KF, Liu Y, Long MA, Kati WM, Molla A, Kempf DJ. J Med Chem. 2009 Apr 6. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19348416</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19300828?ordinalpos=53&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and anti-HIV activity of conformationally restricted bicyclic hexahydroisobenzofuran nucleoside analogs.</a> Díaz-Rodríguez A, Sanghvi YS, Fernández S, Schinazi RF, Theodorakis EA, Ferrero M, Gotor V. Org Biomol Chem. 2009 Apr 7;7(7):1415-23. Epub 2009 Feb 11.</p>

    <p class="pmid">PMID: 19300828</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19271749?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Structure-activity relationships for a series of quinoline-based compounds active against replicating and nonreplicating Mycobacterium tuberculosis.</a>Lilienkampf A, Mao J, Wan B, Wang Y, Franzblau SG, Kozikowski AP. J Med Chem. 2009 Apr 9;52(7):2109-18.</p>

    <p class="pmid">PMID: 19271749</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19269831?ordinalpos=63&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and biological evaluation of N4-(hetero)arylsulfonylquinoxalinones as HIV-1 reverse transcriptase inhibitors.</a> Xu B, Sun Y, Guo Y, Cao Y, Yu T. Bioorg Med Chem. 2009 Apr 1;17(7):2767-74. Epub 2009 Feb 25.</p>

    <p class="pmid">PMID: 19269831</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19269185?ordinalpos=65&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Amide-containing diketoacids as HIV-1 integrase inhibitors: synthesis, structure-activity relationship analysis, and biological activity.</a> Li H, Wang C, Sanchez T, Tan Y, Jiang C, Neamati N, Zhao G. Bioorg Med Chem. 2009 Apr 1;17(7):2913-9. Epub 2009 Feb 7.</p>

    <p class="pmid">PMID: 19269185</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19291106?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Modelling of cytotoxicity data (CC50) of anti-HIV 1-[5-chlorophenyl) sulfonyl]-1H-pyrrole derivatives using calculated molecular descriptors and Levenberg-Marquardt artificial neural network.</a> Arab Chamjangali M. Chem Biol Drug Des. 2009 Apr;73(4):456-65.</p>

    <p class="pmid">PMID: 19291106</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19193796?ordinalpos=106&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">O-linked N-acetylglucosaminylation of Sp1 inhibits the human immunodeficiency virus type 1 promoter.</a> Jochmann R, Thurau M, Jung S, Hofmann C, Naschberger E, Kremmer E, Harrer T, Miller M, Schaft N, Stürzl M. J Virol. 2009 Apr;83(8):3704-18. Epub 2009 Feb 4.</p>

    <p class="pmid">PMID: 19193796</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19026554?ordinalpos=135&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design and synthesis of novel dihydroquinoline-3-carboxylic acids as HIV-1 integrase inhibitors.</a> Sechi M, Rizzi G, Bacchi A, Carcelli M, Rogolino D, Pala N, Sanchez TW, Taheri L, Dayam R, Neamati N. Bioorg Med Chem. 2009 Apr 1;17(7):2925-35. Epub 2008 Nov 6.</p>

    <p class="pmid">PMID: 19026554</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18954921?ordinalpos=136&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel modifications in the series of O-(2-phthalimidoethyl)-N-substituted thiocarbamates and their ring-opened congeners as non-nucleoside HIV-1 reverse transcriptase inhibitors.</a> Spallarossa A, Cesarini S, Ranise A, Bruno O, Schenone S, La Colla P, Collu G, Sanna G, Secci B, Loddo R. Eur J Med Chem. 2009 Apr;44(4):1650-63. Epub 2008 Sep 30.</p>

    <p class="pmid">PMID: 18954921</p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19356241?ordinalpos=17&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Isolation and characterization of a small antiretroviral molecule affecting HIV-1 capsid morphology.</a>  Abdurahman S, Vegvari A, Levi M, Hoglund S, Hogberg M, Tong W, Romero I, Balzarini J, Vahlne A.  Retrovirology. 2009 Apr 8;6(1):34. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19356241</p>

    <p class="title">15.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19356128?ordinalpos=18&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Capsid (CA) Protein as a Novel Drug Target: Recent Progress in the Research of HIV-1 CA Inhibitors.</a>  Zhang J, Liu X, De Clercq E. Mini Rev Med Chem. 2009 Apr;9(4):510-8.</p>

    <p class="pmid">PMID: 19356128</p>

    <p class="title">16.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19356121?ordinalpos=19&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">First mariner mos1 transposase inhibitors.</a> Bouchet N, Bischerour J, Germon S, Guillard J, Dubernet M, Viaud-Massuard MC, Delelis O, Ryabinin V, Bigot Y, Augé-Gouillou C. Mini Rev Med Chem. 2009 Apr;9(4):431-9.</p>

    <p class="pmid">PMID: 19356121</p>

    <p class="title">17.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18614259?ordinalpos=141&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anti-HIV evaluation of benzo[d]isothiazole hydrazones.</a> Vicini P, Incerti M, La Colla P, Loddo R. Eur J Med Chem. 2009 Apr;44(4):1801-7. Epub 2008 Jul 9.</p>

    <p class="pmid">PMID: 18614259</p>

    <p class="title">18.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19141712?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Transport of lamivudine [(-)-beta-L-2&#39;,3&#39;-dideoxy-3&#39;-thiacytidine] and high-affinity interaction of nucleoside reverse transcriptase inhibitors with human organic cation transporters 1, 2, and 3.</a> Minuesa G, Volk C, Molina-Arcas M, Gorboulev V, Erkizia I, Arndt P, Clotet B, Pastor-Anglada M, Koepsell H, Martinez-Picado J. J Pharmacol Exp Ther. 2009 Apr;329(1):252-61. Epub 2009 Jan 13.</p>

    <p class="pmid">PMID: 19141712</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p class="title">19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264402900087">In silico screening of oligosaccharide processing glycosidase inhibitor for HIV infection.</a>  Hakamata W, Takebe Y, Ushijima Y, Nishio T, Oku T. J PHARMACOL SCI, 2009; 109: 32P. </p>

    <p class="title">20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264533400010">Binding Free Energy Calculations of Adenosine Deaminase Inhibitor and the Effect of Methyl Substitution in Inhibitors.</a>Kosugi T, Nakanishi I, Kitaura K.  J CHEM INF MODEL, 2009; 49(3): 615-22.</p>

    <p class="title">21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264490200061">GERANYL DERIVATIVES OF SALSOLINOL SHOW INCREASED BIOLOGICAL ACTIVITIES SO HETEROCYCLES.</a> Iwasa K, Okada S, Nishiyama Y, Takeuchi S, Moriyasu M, Tode C, Sugiura M, Takeuchi A, Tokuda H, Takeda K, Liu YN, Wu PC, Bastow KF, Akiyama T, Lee KH. HETEROCYCLES, 2009; 77(2): 1355-69.</p>

    <p class="title">22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264676600012">Synergistic efficacy of combination of enfuvirtide and sifuvirtide, the first- and next-generation HIV-fusion inhibitors.</a> Pan C, Lu H, Qi Z, Jiang SB. AIDS, 2009; 639-41. </p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
