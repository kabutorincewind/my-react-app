

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-79.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RVWPYU7S3TxB0A5PNP+K1NxMKistohRN81VIff1RV7E3/nT4o1ahsOsQzGrNCBDpUIYl9URbbe0X5+LUjZwL1OoVWQHzESWHBxo/YB0GQfCyD7K5hAISQFN3GHI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C72EA149" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-79-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.        6538   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Selegiline and its salts as antiviral agents</p>

    <p>          Ando, Akinobu, Ochiai, Hiroshi, and Imanishi, Nobuko</p>

    <p>          PATENT:  JP <b>2004292399</b>  ISSUE DATE:  20041021</p>

    <p>          APPLICATION: 2003-24107  PP: 8 pp.</p>

    <p>          ASSIGNEE:  (Fujimoto Corporation, Ltd. Japan</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>2.        6539   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Effect of integrated traditional Chinese and Western medicine on SARS: A review of clinical evidence</p>

    <p>          Zhang, MM, Liu, XM, and He, L</p>

    <p>          World J Gastroenterol <b>2004</b>.  10(23): 3500-3505</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15526373&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15526373&amp;dopt=abstract</a> </p><br />

    <p>3.        6540   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Respiratory syncytial virus and other pneumoviruses: a review of the international symposium-RSV 2003</p>

    <p>          Schmidt, AC, Johnson, TR, Openshaw, PJ, Braciale, TJ, Falsey, AR, Anderson, LJ, Wertz, GW, Groothuis, JR, Prince, GA, Melero, JA, and Graham, BS</p>

    <p>          Virus Res <b>2004</b>.  106(1): 1-13</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15522442&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15522442&amp;dopt=abstract</a> </p><br />

    <p>4.        6541   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Short-term antiviral efficacy of BILN 2061, a hepatitis C virus serine protease inhibitor, in hepatitis C genotype 1 patients</p>

    <p>          Hinrichsen, H, Benhamou, Y, Wedemeyer, H, Reiser, M, Sentjens, RE, Calleja, JL, Forns, X, Erhardt, A, Cronlein, J, Chaves, RL, Yong, CL, Nehmiz, G, and Steinmann, GG</p>

    <p>          Gastroenterology <b>2004</b>.  127(5): 1347-1355</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15521004&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15521004&amp;dopt=abstract</a> </p><br />

    <p>5.        6542   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Antiviral Drugs for Prophylaxis and Treatment of Influenza</p>

    <p>          Anon</p>

    <p>          Med Lett Drugs Ther <b>2004</b>.  46(1194): 85-87</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15520621&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15520621&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.        6543   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 5R- and 5S-methyl substituted d- and l-configuration 1,3-dioxolane nucleoside analogs</p>

    <p>          Bera, S, Malik, L, Bhat, B, Carroll, SS, Hrin, R, Maccoss, M, McMasters, DR, Miller, MD, Moyer, G, Olsen, DB, Schleif, WA, Tomassini, JE, and Eldrup, AB</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(23): 6237-6247</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519166&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519166&amp;dopt=abstract</a> </p><br />

    <p>7.        6544   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Amino acid prodrugs of acyclovir as possible antiviral agents against ocular HSV-1 infections: Interactions with the neutral and cationic amino acid transporter on the corneal epithelium</p>

    <p>          Anand, B, Katragadda, S, Nashed, Y, and Mitra, A</p>

    <p>          Curr Eye Res <b>2004</b>.  29(2-3): 153-166</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15512962&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15512962&amp;dopt=abstract</a> </p><br />

    <p>8.        6545   DMID-LS-79; WOS-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis C Virus Nonstructural Protein, Helicase Activity, and Viral Replication by a Recombinant Human Antibody Clone</p>

    <p>          Prabhu, R. <i>et al.</i></p>

    <p>          American Journal of Pathology <b>2004</b>.  165(4): 1163-1173</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>9.        6546   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Synthesis, antiviral activity, and mode of action of some 3-substituted 2,5,6-trichloroindole 2&#39;- and 5&#39;-deoxyribonucleosides</p>

    <p>          Williams, JD,  Ptak, RG, Drach, JC, and Townsend, LB</p>

    <p>          J Med Chem <b>2004</b>.  47(23): 5773-5782</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509176&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509176&amp;dopt=abstract</a> </p><br />

    <p>10.      6547   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of 3-formyl- and 3-cyano-2,5,6-trichloroindole nucleoside derivatives</p>

    <p>          Williams, JD,  Chen, JJ, Drach, JC, and Townsend, LB</p>

    <p>          J Med Chem <b>2004</b>.  47(23): 5766-5772</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509175&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509175&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.      6548   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Design, synthesis, and antiviral activity of certain 3-substituted 2,5,6-trichloroindole nucleosides</p>

    <p>          Williams, JD,  Chen, JJ, Drach, JC, and Townsend, LB</p>

    <p>          J Med Chem <b>2004</b>.  47(23): 5753-5765</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509174&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509174&amp;dopt=abstract</a> </p><br />

    <p>12.      6549   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Ebola virus ecology: a continuing mystery</p>

    <p>          Feldmann, Heinz, Wahl-Jensen, Victoria, Jones, Steven M, and Stroeher, Ute</p>

    <p>          Trends in Microbiology <b>2004</b>.  12(10): 433-437</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>13.      6550   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of polyhalogenated imidazole nucleosides: dimensional analogues of 2,5,6-trichloro-1-(beta-D-ribofuranosyl)benzimidazole</p>

    <p>          Chien, TC, Saluja, SS, Drach, JC, and Townsend, LB</p>

    <p>          J Med Chem <b>2004</b>.  47(23): 5743-5752</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509173&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509173&amp;dopt=abstract</a> </p><br />

    <p>14.      6551   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          What is the best antiviral agent for influenza infection?</p>

    <p>          Fagan, HB and  Moeller, AH</p>

    <p>          Am Fam Physician <b>2004</b>.  70(7): 1331-1332</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15508545&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15508545&amp;dopt=abstract</a> </p><br />

    <p>15.      6552   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          New class of orthopoxvirus antiviral drugs that block viral maturation</p>

    <p>          Byrd, CM, Bolken, TC, Mjalli, AM, Arimilli, MN, Andrews, RC, Rothlein, R, Andrea, T, Rao, M, Owens, KL, and Hruby, DE</p>

    <p>          J Virol <b>2004</b> .  78(22): 12147-12156</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507601&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507601&amp;dopt=abstract</a> </p><br />

    <p>16.      6553   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Possible mode of action of antiherpetic activities of a proteoglycan isolated from the mycelia of Ganoderma lucidum in vitro</p>

    <p>          Liu, J, Yang, F, Ye, LB, Yang, XJ, Timani, KA, Zheng, Y, and Wang, YH</p>

    <p>          J Ethnopharmacol <b>2004</b>.  95(2-3): 265-272</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507347&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507347&amp;dopt=abstract</a> </p><br />

    <p>17.      6554   DMID-LS-79; WOS-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Gm3-Binding Peptides Inhibit Influenza Virus Infection</p>

    <p>          Matsubara, T. <i> et al.</i></p>

    <p>          Glycobiology <b>2004</b>.  14(11): 1155</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>18.      6555   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Novel Chemical Class of pUL97 Protein Kinase-Specific Inhibitors with Strong Anticytomegaloviral Activity</p>

    <p>          Herget, T, Freitag, M, Morbitzer, M, Kupfer, R, Stamminger, T, and Marschall, M</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(11): 4154-4162</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504835&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504835&amp;dopt=abstract</a> </p><br />

    <p>19.      6556   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          L protein, the RNA-dependent RNA polymerase of hantaviruses</p>

    <p>          Kukkonen, SK, Vaheri, A, and Plyusnin, A</p>

    <p>          Arch Virol <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15503219&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15503219&amp;dopt=abstract</a> </p><br />

    <p>20.      6557   DMID-LS-79; WOS-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Antioxidant and Antitumor Promoting Activities of the Flavonoids From Hedychium Thyrsiforme</p>

    <p>          Jasril <i>et al.</i></p>

    <p>          Pharmaceutical Biology <b>2003</b>.  41(7): 506-511</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>21.      6558   DMID-LS-79; WOS-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Polyadenylation-Dependent Screening Assay for Respiratory Syncytial Virus Rna Transcriptase Activity and Identification of an Inhibitor</p>

    <p>          Mason, S. <i>et al.</i></p>

    <p>          Nucleic Acids Research <b>2004</b>.  32(16): 4758-4767</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>22.      6559   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Antiviral Effects of Sulfated Exopolysaccharide from the Marine Microalga Gyrodinium impudicum Strain KG03</p>

    <p>          Yim, Joung Han, Kim, Sung Jin, Ahn, Se Hun, Lee, Chong Kyo, Rhie, Ki Tae, and Lee, Hong Kum</p>

    <p>          Marine Biotechnology <b>2004</b>.  6(1): 17-25</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>23.      6560   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          A cell-based beta-lactamase reporter gene assay for the identification of inhibitors of hepatitis C virus replication</p>

    <p>          Zuck, P, Murray, EM, Stec, E, Grobler, JA, Simon, AJ, Strulovici, B, Inglese, J, Flores, OA, and Ferrer, M</p>

    <p>          Anal Biochem <b>2004</b>.  334(2): 344-355</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15494142&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15494142&amp;dopt=abstract</a> </p><br />

    <p>24.      6561   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          High-Throughput Screening Identifies Inhibitors of the SARS Coronavirus Main Proteinase</p>

    <p>          Blanchard, JE, Elowe, NH, Huitema, C, Fortin, PD, Cechetto, JD, Eltis, LD, and Brown, ED</p>

    <p>          Chem Biol <b>2004</b>.  11(10): 1445-1453</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15489171&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15489171&amp;dopt=abstract</a> </p><br />

    <p>25.      6562   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Molecular characterization of a panel of murine monoclonal antibodies specific for the SARS-coronavirus</p>

    <p>          Gubbins, MJ, Plummer, FA, Yuan, XY, Johnstone, D, Drebot, M, Andonova, M, Andonov, A, and Berry, JD</p>

    <p>          Mol Immunol <b>2005</b>.  42(1): 125-136</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15488951&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15488951&amp;dopt=abstract</a> </p><br />

    <p>26.      6563   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of new benzothiadiazine dioxide derivatives</p>

    <p>          Tait, Annalisa, Luppi, Amedeo, and Cermelli, Claudio</p>

    <p>          Journal of Heterocyclic Chemistry <b>2004</b>.  41(5): 747-753</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.      6564   DMID-LS-79; WOS-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Identification of Immunodominant Sites on the Spike Protein of Severe Acute Respiratory Syndrome (Sars) Coronavirus: Implication for Developing Sars Diagnostics and Vaccines</p>

    <p>          He, Y. <i>et al.</i></p>

    <p>          Journal of Immunology <b>2004</b>.  173(6): 4050-4057</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>28.      6565   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          In search of a small-molecule inhibitor for respiratory syncytial virus</p>

    <p>          Douglas, JL</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(4): 625-639</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482225&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482225&amp;dopt=abstract</a> </p><br />

    <p>29.      6566   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Adefovir dipivoxil in the treatment of chronic hepatitis B virus infection</p>

    <p>          Hadziyannis, SJ and Papatheodoridis, GV</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(4): 475-483</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482214&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482214&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>30.      6567   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          New antiviral therapies for hepatitis C</p>

    <p>          O&#39;Leary, J and Chung, RT</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(2): 235-243</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482189&amp;dopt=abstract</a> </p><br />

    <p>31.      6568   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Therapeutic options for human herpesvirus-8/Kaposi&#39;s sarcoma-associated herpesvirus-related disorders</p>

    <p>          Aoki, Y and Tosato, G</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(2): 213-225</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482187&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482187&amp;dopt=abstract</a> </p><br />

    <p>32.      6569   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Antiviral activity against HSV-2 of sulfated polysaccharide from Cyathula officinalis Kuan in vitro</p>

    <p>          Liu, Yinghua, He, Kaize, Yang, Min, Pu, Qiang, and Zhang, Junfeng</p>

    <p>          Yingyong Yu Huanjing Shengwu Xuebao <b>2004</b>.  10(1): 46-50</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>33.      6570   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Thymalfasin for the treatment of chronic hepatitis B</p>

    <p>          Chien, RN and Liaw, YF</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(1): 9-16</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482167&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482167&amp;dopt=abstract</a> </p><br />

    <p>34.      6571   DMID-LS-79; WOS-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Effect of Adenine Arabinoside on Severe Herpesvirus Hominis Infections in Man (Reprinted From the Journal of Infectious Disesases, Vol 128, 1973)</p>

    <p>          Ch&#39;ien, L. <i>et al.</i></p>

    <p>          Journal of Infectious Diseases <b>2004</b>.  190(7): 1362-1367</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>35.      6572   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Current and emerging therapeutic approaches to hepatitis C infection</p>

    <p>          Durantel, D, Escuret, V, and Zoulim, F</p>

    <p>          Expert Rev Anti Infect Ther <b>2003</b>.  1(3): 441-454</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482141&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482141&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>36.      6573   DMID-LS-79; WOS-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Valacyclovir and Acyclovir for Suppression of Shedding of Herpes Simplex Virus in the Genital Tract</p>

    <p>          Gupta, R. <i>et al.</i></p>

    <p>          Journal of Infectious Diseases <b>2004</b>.  190(8): 1374-1381</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>37.      6574   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Potential of acyclic nucleoside phosphonates in the treatment of DNA virus and retrovirus infections</p>

    <p>          De, Clercq E</p>

    <p>          Expert Rev Anti Infect Ther <b>2003</b>.  1(1): 21-43</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482100&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482100&amp;dopt=abstract</a> </p><br />

    <p>38.      6575   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Influenza viruses resistant to the antiviral drug oseltamivir: transmission studies in ferrets</p>

    <p>          Herlocher, ML, Truscon, R, Elias, S, Yen, HL, Roberts, NA, Ohmit, SE, and Monto, AS</p>

    <p>          J Infect Dis <b>2004</b>.  190(9): 1627-1630</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15478068&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15478068&amp;dopt=abstract</a> </p><br />

    <p>39.      6576   DMID-LS-79; PUBMED-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Synthesis, antitumor and antiviral properties of some 1,2,4-triazole derivatives</p>

    <p>          Al-Soud, YA, Al-Dweri, MN, and Al-Masoudi, NA</p>

    <p>          Farmaco <b>2004</b> .  59(10): 775-783</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474054&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474054&amp;dopt=abstract</a> </p><br />

    <p>40.      6577   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          A Novel Class of Phosphonate Nucleosides. 9-[(1-Phosphonomethoxycyclopropyl)methyl]guanine as a Potent and Selective Anti-HBV Agent</p>

    <p>          Choi, Jong-Ryoo, Cho, Dong-Gyu, Roh, Kee Y, Hwang, Jae-Taeg, Ahn, Sinbyoung, Jang, Hyun S, Cho, Woo-Young, Kim, Kyong W, Cho, Young-Gyo, Kim, Jeongmin, and Kim, Yong-Zu</p>

    <p>          Journal of Medicinal Chemistry <b>2004</b>.  47(11): 2864-2869</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>41.      6578   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Benzothiazole compounds, compositions and methods for treatment and prophylaxis of rotavirus infections and associated diseases</p>

    <p>          Bailey, Thomas R and Pevear, Daniel C</p>

    <p>          PATENT:  WO <b>2004078115</b>  ISSUE DATE:  20040916</p>

    <p>          APPLICATION: 2004  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Viropharma, Incorporated USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>42.      6579   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          Antivirals and antiviral strategies</p>

    <p>          De Clercq, Erik</p>

    <p>          Nature Reviews Microbiology <b>2004</b>.  2(9): 704-720</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>43.      6580   DMID-LS-79; SCIFINDER-DMID-11/9/2004</p>

    <p class="memofmt1-2">          The combined use of cruciferous indoles and chelators for the treatment of papillomavirus-related conditions</p>

    <p>          Zeligs, Michael A</p>

    <p>          PATENT:  WO <b>2004071425</b>  ISSUE DATE:  20040826</p>

    <p>          APPLICATION: 2004  PP: 90 pp.</p>

    <p>          ASSIGNEE:  (Bioresponse, LLC USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
