

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-11-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1OqXmiG7sIya/W9qjuWNaKbleeRd0M51jdl0hHKRoaCNx7j8WFRHBPLoBmrZx++1q0EaP4GVLumSSkW6HnR6LtzA2J/MRejwvemeo+Dfgp522VKqd8Ymii6R138=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1FCBC166" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: October 25 - November 7, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24113364">Synthesis and Evaluation of (Z)-2,3-Diphenylacrylonitrile Analogs as Anti-cancer and Anti-microbial Agents.</a> Alam, M.S., Y.J. Nam, and D.U. Lee. European Journal of Medicinal Chemistry, 2013. 69: p. 790-797. PMID[24113364].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24156656">1-Aryl-3-(1H-imidazol-1-yl)propan-1-ol Esters: Synthesis, Anti-candida Potential and Molecular Modeling Studies.</a> Attia, M.I., A.A. Radwan, A.S. Zakaria, M.S. Almutairi, and S.W. Ghoneim. Chemistry Central Journal, 2013. 7(1): p. 168. PMID[24156656].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24165199">Antimicrobial Activity and Cytotoxicity of the Ethanol Extract, Fractions and Eight Compounds Isolated from Eriosema robustum (Fabaceae).</a> Awouafack, M.D., L.J. McGaw, S. Gottfried, R. Mbouangouere, P. Tane, M. Spiteller, and J.N. Eloff. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 289. PMID[24165199].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23280633">Antifungal Activity of 10 Guadeloupean Plants.</a> Biabiany, M., V. Roumy, T. Hennebelle, N. Francois, B. Sendid, M. Pottier, M. Aliouat el, I. Rouaud, F. Lohezic-Le Devehat, H. Joseph, P. Bourgeois, S. Sahpaz, and F. Bailleul. Phytotherapy Research, 2013. 27(11): p. 1640-1645. PMID[23280633].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23766486">Clinical Features of Patients with Infections Caused by Candida guilliermondii and Candida fermentati and Antifungal Susceptibility of the Isolates at a Medical Centre in Taiwan, 2001-10.</a> Chen, C.Y., S.Y. Huang, J.L. Tang, W. Tsay, M. Yao, B.S. Ko, W.C. Chou, H.F. Tien, and P.R. Hsueh. The Journal of Antimicrobial Chemotherapy, 2013. 68(11): p. 2632-2635. PMID[23766486].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23896552">Antifungal Effect and Pore-forming Action of Lactoferricin B Like Peptide Derived from Centipede Scolopendra subspinipes Mutilans.</a> Choi, H., J.S. Hwang, and D.G. Lee. Biochimica et Biophysica Acta, 2013. 1828(11): p. 2745-2750. PMID[23896552].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24041890">In Vitro Susceptibility of Aspergillus fumigatus to Isavuconazole: Correlation with Itraconazole, Voriconazole, and Posaconazole.</a> Gregson, L., J. Goodwin, A. Johnson, L. McEntee, C.B. Moore, M. Richardson, W.W. Hope, and S.J. Howard. Antimicrobial Agents and Chemotherapy, 2013. 57(11): p. 5778-5780. PMID[24041890].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23959309">Determination of Isavuconazole Susceptibility of Aspergillus and Candida Species by the Eucast Method.</a> Howard, S.J., C. Lass-Florl, M. Cuenca-Estrella, A. Gomez-Lopez, and M.C. Arendrup. Antimicrobial Agents and Chemotherapy, 2013. 57(11): p. 5426-5431. PMID[23959309].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24012381">Intermolecular Interaction of Voriconazole Analogues with Model Membrane by DSC and NMR, and Their Antifungal Activity Using NMR Based Metabolic Profiling.</a> Kalamkar, V., M. Joshi, V. Borkar, S. Srivastava, and M. Kanyalkar. Bioorganic &amp; Medicinal Chemistry, 2013. 21(21): p. 6753-6762. PMID[24012381].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23446490">In Vitro Synergistic Activity of Diketopiperazines Alone and in Combination with Amphotericin B or Clotrimazole against Candida albicans.</a> Kumar, S.N., B. Nambisan, C. Mohandas, and A. Sundaresan. Folia Microbiologica, 2013. 58(6): p. 475-482. PMID[23446490].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24099527">Guanylated Polymethacrylates: A Class of Potent Antimicrobial Polymers with Low Hemolytic Activity.</a> Locock, K.E., T.D. Michl, J.D. Valentin, K. Vasilev, J.D. Hayball, Y. Qu, A. Traven, H.J. Griesser, L. Meagher, and M. Haeussler. Biomacromolecules, 2013. <b>[Epub ahead of print]</b>. PMID[24099527].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23280933">Antimicrobial, Antioxidant and Anti-inflammatory Activity of Young Shoots of the Smoke Tree, Cotinus Coggygria scop.</a> Marcetic, M., D. Bozic, M. Milenkovic, N. Malesevic, S. Radulovic, and N. Kovacevic. Phytotherapy Research, 2013. 27(11): p. 1658-1663. PMID[23280933].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23876294">Mechanism of Action of Novel Synthetic Dodecapeptides against Candida albicans.</a> Maurya, I.K., C.K. Thota, J. Sharma, S.G. Tupe, P. Chaudhary, M.K. Singh, I.S. Thakur, M. Deshpande, R. Prasad, and V.S. Chauhan. Biochimica et Biophysica Acta, 2013. 1830(11): p. 5193-5203. PMID[23876294].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23773119">Antifungal Activity of Silver Nanoparticles in Combination with Nystatin and Chlorhexidine Digluconate against Candida albicans and Candida glabrata Biofilms.</a> Monteiro, D.R., S. Silva, M. Negri, L.F. Gorup, E.R. de Camargo, R. Oliveira, D.B. Barbosa, and M. Henriques. Mycoses, 2013. 56(6): p. 672-680. PMID[23773119].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24019229">Synthetic Analogs of Anoplin Show Improved Antimicrobial Activities.</a> Munk, J.K., L.E. Uggerhoj, T.J. Poulsen, N. Frimodt-Moller, R. Wimmer, N.T. Nyberg, and P.R. Hansen. Journal of Peptide Science, 2013. 19(11): p. 669-675. PMID[24019229].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22653870">Screening of Microbial Contamination and Antimicrobial Activity of Sea Cucumber Holothuria polii.</a> Omran, N.E. and N.G. Allam. Toxicology and Industrial Health, 2013. 29(10): p. 944-954. PMID[22653870].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24056147">Microwave Assisted Synthesis of Dihydrobenzo[4,5]imidazo[1,2-a]pyrimidin-4-ones; Synthesis, in Vitro Antimicrobial and Anticancer Activities of Novel Coumarin Substituted Dihydrobenzo[4,5]imidazo[1,2-a]pyrimidin-4-ones.</a> Puttaraju, K.B., K. Shivashankar, Chandra, M. Mahendra, V.P. Rasal, P.N. Venkata Vivek, K. Rai, and M.B. Chanu. European Journal of Medicinal Chemistry, 2013. 69: p. 316-322. PMID[24056147].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24164115">Antifungal Cyclic Lipopeptides from Bacillus amyloliquefaciens Strain BO5A.</a> Romano, A., D. Vitullo, M. Senatore, G. Lima, and V. Lanzotti. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[24164115].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24178908">Photodynamic Antimicrobial Chemotherapy (PACT) Inhibits Biofilm Formation by Candida albicans, Increasing Both ROS Production and Membrane Permeability.</a> Rosseti, I.B., L.R. Chagas, and M.S. Costa. Lasers in Medical Science, 2013. <b>[Epub ahead of print]</b>. PMID[24178908].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24063539">Indolo[3,2-a]carbazoles from a Deep-water Sponge of the Genus Asteropus.</a> Russell, F., D. Harmody, P.J. McCarthy, S.A. Pomponi, and A.E. Wright. Journal of Natural Products, 2013. 76(10): p. 1989-1992. PMID[24063539].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24055150">Susceptibility Profile of a Brazilian Yeast Stock Collection of Candida Species Isolated from Subjects with Candida-associated Denture Stomatitis with or without Diabetes.</a> Sanita, P.V., E.G. Mima, A.C. Pavarina, J.H. Jorge, A.L. Machado, and C.E. Vergani. Oral Surgery Oral Medicine Oral Pathology and Oral Radiology, 2013. 116(5): p. 562-569. PMID[24055150].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24044873">Synthesis and Antimicrobial Activity of Polyhalobenzonitrile Quinazolin-4(3H)-one Derivatives.</a> Shi, L.P., K.M. Jiang, J.J. Jiang, Y. Jin, Y.H. Tao, K. Li, X.H. Wang, and J. Lin. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(21): p. 5958-5963. PMID[24044873].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23865534">Use of Isothermal Microcalorimetry to Quantify the Influence of Glucose and Antifungals on the Growth of Candida albicans in Urine.</a> Wernli, L., G. Bonkat, T.C. Gasser, A. Bachmann, and O. Braissant. Journal of Applied Microbiology, 2013. 115(5): p. 1186-1193. PMID[23865534].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24030481">Antimicrobial Ergosteroids and Pyrrole Derivatives from Halotolerant Aspergillus flocculosus PT05-1 Cultured in a Hypersaline Medium.</a> Zheng, J., Y. Wang, J. Wang, P. Liu, J. Li, and W. Zhu. Extremophiles, 2013. 17(6): p. 963-971. PMID[24030481].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24035198">Trimethoprim/Sulfamethoxazole Susceptibility of Mycobacterium tuberculosis.</a> Alsaad, N., T. van der Laan, R. van Altena, K.R. Wilting, T.S. van der Werf, Y. Stienstra, D. van Soolingen, and J.W. Alffenaar. International Journal of Antimicrobial Agents, 2013. 42(5): p. 472-474. PMID[24035198].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24164193">Synthetic Thioamide, Benzimidazole, Quinolone and Derivatives with Carboxylic acid and Ester Moieties. A Strategy in the Design of Antituberculosis Agents.</a> Ashfaq, M., S.S. Shah, T. Najam, M.M. Ahmad, R. Tabassum, and G. Rivera. Current Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24164193].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24044875">Synthesis, Characterization and in Vitro Biological Evaluation of Some Novel 1,3,5-Triazine-Schiff Base Conjugates as Potential Antimycobacterial Agents.</a> Avupati, V.R., R.P. Yejella, V.R. Parala, K.N. Killari, V.M. Papasani, P. Cheepurupalli, V.R. Gavalapu, and B. Boddeda. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(21): p. 5968-5970. PMID[24044875].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24189255">Bactericidal Activity and Mechanism of Action of AZD5847: A Novel Oxazolidinone for the Treatment of Tuberculosis.</a> Balasubramanian, V., S. Solapure, H. Iyer, A. Ghosh, S. Sharma, P. Kaur, R. Deepthi, V. Subbulakshmi, V. Ramya, V. Ramachandran, M. Balganesh, L. Wright, D. Melnick, S.L. Butler, and V.K. Sambandamurthy. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24189255].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23305394">Current Prospects of Synthetic Curcumin Analogs and Chalcone Derivatives against Mycobacterium tuberculosis.</a> Bukhari, S.N., S.G. Franzblau, I. Jantan, and M. Jasamai. Medicinal Chemistry, 2013. 9(7): p. 897-903. PMID[23305394].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24156740">Sulfonamides Incorporating Fluorine and 1,3,5-Triazine Moieties Are Effective Inhibitors of Three Beta-class Carbonic Anhydrases from Mycobacterium Tuberculosis.</a> Ceruso, M., D. Vullo, A. Scozzafava, and C.T. Supuran. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24156740].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24168665">In Vitro Anti-mycobacterial Activity of Selected Medicinal Plants against Mycobacterium tuberculosis and Mycobacterium bovis Strains.</a> Gemechu, A., M. Giday, G. Ameni, and A. Worku. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 291. PMID[24168665].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24075143">Antimycobacterial and Herbicidal Activity of Ring-substituted 1-Hydroxynaphthalene-2-carboxanilides.</a> Gonec, T., J. Kos, I. Zadrazilova, M. Pesko, S. Keltosova, J. Tengler, P. Bobal, P. Kollar, A. Cizek, K. Kralova, and J. Jampilek. Bioorganic &amp; Medicinal Chemistry, 2013. 21(21): p. 6531-6541. PMID[24075143].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24095750">2-(2-Hydrazinyl)thiazole Derivatives: Design, Synthesis and in Vitro Antimycobacterial Studies.</a> Makam, P., R. Kankanala, A. Prakash, and T. Kannan. European Journal of Medicinal Chemistry, 2013. 69: p. 564-576. PMID[24095750].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24074842">Studies on Substituted Benzo[h]quinazolines, Benzo[g]indazoles, Pyrazoles, 2,6-Diarylpyridines as Anti-tubercular Agents.</a> Maurya, H.K., R. Verma, S. Alam, S. Pandey, V. Pathak, S. Sharma, K.K. Srivastava, A.S. Negi, and A. Gupta. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(21): p. 5844-5849. PMID[24074842].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24075144">Structure-Activity Relationships of 2-Aminothiazoles Effective against Mycobacterium tuberculosis.</a> Meissner, A., H.I. Boshoff, M. Vasan, B.P. Duckworth, C.E. Barry, 3rd, and C.C. Aldrich. Bioorganic &amp; Medicinal Chemistry, 2013. 21(21): p. 6385-6397. PMID[24075144].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24016834">Synthesis and Evaluation of alpha-Ketotriazoles and alpha, beta-Diketotriazoles as Inhibitors of Mycobacterium tuberculosis.</a> Menendez, C., F. Rodriguez, A.L. Ribeiro, F. Zara, C. Frongia, V. Lobjois, N. Saffon, M.R. Pasca, C. Lherbet, and M. Baltas. European Journal of Medicinal Chemistry, 2013. 69: p. 167-173. PMID[24016834].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24188534">Manganese(II) Complexes with Thiosemicarbazones as Potential anti-Mycobacterium tuberculosis Agents.</a> Oliveira, C.G., P.I. Maia, P.C. Souza, F.R. Pavan, C.Q. Leite, R.B. Viana, A.A. Batista, O.R. Nascimento, and V.M. Deflon. Journal of Inorganic Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24188534].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24045008">New Derivatives of Salicylamides: Preparation and Antimicrobial Activity against Various Bacterial Species.</a> Pauk, K., I. Zadrazilova, A. Imramovsky, J. Vinsova, M. Pokorna, M. Masarikova, A. Cizek, and J. Jampilek. Bioorganic &amp; Medicinal Chemistry, 2013. 21(21): p. 6574-6581. PMID[24045008].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24077526">Development of 3-Phenyl-4,5,6,7-tetrahydro-1H-pyrazolo[4,3-c]pyridine Derivatives as Novel Mycobacterium tuberculosis Pantothenate Synthetase Inhibitors.</a> Samala, G., P.B. Devi, R. Nallangi, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2013. 69: p. 356-364. PMID[24077526].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23497141">Antimalarial Screening Via Large-scale Purification of Plasmodium falciparum Ca(2+) -ATPase 6 and in Vitro Studies.</a> David-Bosne, S., I. Florent, A.M. Lund-Winther, J.B. Hansen, M. Buch-Pedersen, P. Machillot, M. le Maire, and C. Jaxel. The FEBS Journal, 2013. 280(21): p. 5419-5429. PMID[23497141].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24165175">Potential Efficacy of Citicoline as Adjunct Therapy in the Treatment of Cerebral Malaria.</a> El-Assaad, F., V. Combes, G.E. Grau, and R. Jambou. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24165175].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">42. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24012380">Synthesis and Antiprotozoal Activity of Dicationic 2,6-Diphenylpyrazines and Aza-analogues.</a> Hu, L., A. Patel, L. Bondada, S. Yang, M.Z. Wang, M. Munde, W.D. Wilson, T. Wenzler, R. Brun, and D.W. Boykin. Bioorganic &amp; Medicinal Chemistry, 2013. 21(21): p. 6732-6741. PMID[24012380].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">43. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24077524">In Vitro Antimalarial Activity, beta-Haematin Inhibition and Structure-Activity Relationships in a Series of Quinoline triazoles.</a> Joshi, M.C., K.J. Wicht, D. Taylor, R. Hunter, P.J. Smith, and T.J. Egan. European Journal of Medicinal Chemistry, 2013. 69: p. 338-347. PMID[24077524].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">44. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24158489">Synergy of Ferrous Ion on 5-Aminolevulinic acid Mediated Growth Inhibition of Plasmodium falciparum.</a> Komatsuya, K., M. Hata, E.O. Balogun, K. Hikosaka, S. Suzuki, K. Takahashi, T. Tanaka, M. Nakajima, S.I. Ogura, S. Sato, and K. Kita. Journal of Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24158489].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">45. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24035097">Imidazopyridazines as Potent Inhibitors of Plasmodium falciparum Calcium-dependent Protein Kinase 1 (PFCDPK1): Preparation and Evaluation of Pyrazole Linked Analogues.</a> Large, J.M., S.A. Osborne, E. Smiljanic-Hurley, K.H. Ansell, H.M. Jones, D.L. Taylor, B. Clough, J.L. Green, and A.A. Holder. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(21): p. 6019-6024. PMID[24035097].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">46. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23995215">Synthesis and in Vitro and in Vivo Evaluation of Antimalarial Polyamines.</a> Liew, L.P., A.N. Pearce, M. Kaiser, and B.R. Copp. European Journal of Medicinal Chemistry, 2013. 69: p. 22-31. PMID[23995215].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">47. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23979751">Transdermal Glyceryl trinitrate as an Effective Adjunctive Treatment with Artemether for Late-stage Experimental Cerebral Malaria.</a> Orjuela-Sanchez, P., P.K. Ong, G.M. Zanini, B. Melchior, Y.C. Martins, D. Meays, J.A. Frangos, and L.J. Carvalho. Antimicrobial Agents and Chemotherapy, 2013. 57(11): p. 5462-5471. PMID[23979751].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">48. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24077527">Squaric acid/4-Aminoquinoline Conjugates: Novel Potent Antiplasmodial Agents.</a> Ribeiro, C.J., S.P. Kumar, J. Gut, L.M. Goncalves, P.J. Rosenthal, R. Moreira, and M.M. Santos. European Journal of Medicinal Chemistry, 2013. 69: p. 365-372. PMID[24077527].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">49. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24125849">Flavones as Isosteres of 4(1H)-Quinolones: Discovery of Ligand Efficient and Dual Stage Antimalarial Lead Compounds.</a> Rodrigues, T., A.S. Ressurreicao, F.P. da Cruz, I.S. Albuquerque, J. Gut, M.P. Carrasco, D. Goncalves, R.C. Guedes, D.J. Dos Santos, M.M. Mota, P.J. Rosenthal, R. Moreira, M. Prudencio, and F. Lopes. European Journal of Medicinal Chemistry, 2013. 69: p. 872-880. PMID[24125849].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">50. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24090569">Antiparasitic Effect of a Fraction Enriched in Tight-binding Protease Inhibitors Isolated from the Caribbean Coral Plexaura homomalla.</a> Salas-Sarduy, E., A. Cabrera-Munoz, A. Cauerhff, Y. Gonzalez-Gonzalez, S.A. Trejo, A. Chidichimo, L. Chavez-Planes Mde, and J.J. Cazzulo. Experimental Parasitology, 2013. 135(3): p. 611-622. PMID[24090569].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">51. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24012713">Synthesis, Characterization, Antiparasitic and Cytotoxic Evaluation of Thioureas Conjugated to Polyamine Scaffolds.</a> Stringer, T., D. Taylor, C. de Kock, H. Guzgay, A. Au, S.H. An, B. Sanchez, R. O&#39;Connor, N. Patel, K.M. Land, P.J. Smith, D.T. Hendricks, T.J. Egan, and G.S. Smith. European Journal of Medicinal Chemistry, 2013. 69: p. 90-98. PMID[24012713].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">52. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24035096">Chemical Investigation of an Antimalarial Chinese Medicinal Herb Picrorhiza scrophulariiflora.</a> Wang, H., W. Zhao, V. Choomuenwai, K.T. Andrews, R.J. Quinn, and Y. Feng. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(21): p. 5915-5918. PMID[24035096].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">53. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23979747">In Vitro Effects of Novel Ruthenium Complexes in Neospora caninum and Toxoplasma gondii Tachyzoites.</a> Barna, F., K. Debache, C.A. Vock, T. Kuster, and A. Hemphill. Antimicrobial Agents and Chemotherapy, 2013. 57(11): p. 5747-5754. PMID[23979747].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">54. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24090919">Design, Synthesis and Biological Evaluation of WC-9 Analogs as Antiparasitic Agents.</a> Elicio, P.D., M.N. Chao, M. Galizzi, C. Li, S.H. Szajnman, R. Docampo, S.N. Moreno, and J.B. Rodriguez. European Journal of Medicinal Chemistry, 2013. 69: p. 480-489. PMID[24090919].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1025-110713.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">55. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324847900004">Effect of Jujube Honey on Candida albicans Growth and Biofilm Formation.</a> Ansari, M.J., A. Al-Ghamdi, S. Usmani, N.S. Al-Waili, D. Sharma, A. Nuru, and Y. Al-Attal. Archives of Medical Research, 2013. 44(5): p. 352-360. ISI[000324847900004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">56. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324832800020">A Novel Calcium-dependent Protein Kinase Inhibitor as a Lead Compound for Treating Cryptosporidiosis.</a> Castellanos-Gonzalez, A., A.C. White, K.K. Ojo, R.S.R. Vidadala, Z.S. Zhang, M.C. Reid, A.M.W. Fox, K.R. Keyloun, K. Rivas, A. Irani, S.M. Dann, E.K. Fan, D.J. Maly, and W.C. Van Voorhis. Journal of Infectious Diseases, 2013. 208(8): p. 1342-1348. ISI[000324832800020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">57. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325121800003">Synthesis and Antifungal Activity of Terpenyl-1,4-naphthoquinone and 1,4-Anthracenedione Derivatives.</a> Castro, M.A., A.M. Gamito, V. Tangarife-Castatno, B. Zapata, J.M.M. del Corral, A.C. Mesa-Arango, L. Betancur-Galvis, and A. San Feliciano. European Journal of Medicinal Chemistry, 2013. 67: p. 19-27. ISI[000325121800003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">58. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325076400039">Exploration of Antimicrobial Potential of Pyrazolo[3,4-b]pyridine Scaffold Bearing Benzenesulfonamide and Trifluoromethyl Moieties.</a> Chandak, N., S. Kumar, P. Kumar, C. Sharma, K.R. Aneja, and P.K. Sharma. Medicinal Chemistry Research, 2013. 22(11): p. 5490-5503. ISI[000325076400039].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">59. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325076400010">Green Approach Towards Synthesis of Substituted Pyrazole-1,4-dihydro,9-oxa,1,2,6,8-tetrazacyclopentano[b]naphthalene-5-one Derivatives as Antimycobacterial Agents.</a> Chobe, S.S., R.D. Kamble, S.D. Patil, A.P. Acharya, S.V. Hese, O.S. Yemul, and B.S. Dawane. Medicinal Chemistry Research, 2013. 22(11): p. 5197-5203. ISI[000325076400010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">60. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325121800012">Synthesis and Antimycobacterial Activity of Analogues of the Bioactive Natural Products Sampangine and Cleistopholine.</a> Claes, P., D. Cappoen, B.M. Mbala, J. Jacobs, B. Mertens, V. Mathys, L. Verschaeve, K. Huygen, and N. De Kimpe. European Journal of Medicinal Chemistry, 2013. 67: p. 98-110. ISI[000325121800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">61. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324658000001">Synthesis of Quinoline-Pyrazoline Based Thiazole Derivatives Endowed with Antimicrobial Activity.</a> Desai, N.C., V.V. Joshi, K.M. Rajpara, H.V. Vaghani, and H.M. Satodiya. Indian Journal of Chemistry Section B - Organic Chemistry Including Medicinal Chemistry, 2013. 52(9): p. 1191-1201. ISI[000324658000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">62. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324946600001">Synthesis, Characterization, and Thermal and Antimicrobial Activities of Some Novel Organotin(IV): Purine Base Complexes.</a> Jain, R., R. Singh, and N.K. Kaushik. Journal of Chemistry, 2013. 568195: p. 12. ISI[000324946600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">63. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325035100022">Purification, Molecular Cloning, and Antimicrobial Activity of Peptides from the Skin Secretion of the Black-Spotted Frog, Rana nigromaculata.</a> Li, A., Y. Zhang, C. Wang, G. Wu, and Z.C. Wang. World Journal of Microbiology &amp; Biotechnology, 2013. 29(10): p. 1941-1949. ISI[000325035100022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">64. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325076400006">The Antimycobacterial MICs, SARs, and QSARs of Some Ethnobotanically Selected Phytocompounds.</a> Muriuki, B., J.O. Midiwo, P.M. Mbugua, and J.M. Keriko. Medicinal Chemistry Research, 2013. 22(11): p. 5141-5152. ISI[000325076400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">65. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324586900006">In Vitro Antimicrobial Activity of Total Extracts of the Leaves of Petiveria alliacea L. (Anamu).</a> Pacheco, A.O., J.M. Moran, Z.G. Giro, A.H. Rodriguez, R.J. Mujawimana, K.T. Gonzalez, and S.S. Frometa. Brazilian Journal of Pharmaceutical Sciences, 2013. 49(2): p. 241-250. ISI[000324586900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">66. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324325200032">Metal Complexes of Moxifloxacin-imidazole Mixed Ligands: Characterization and Biological Studies.</a> Soayed, A.A., H.M. Refaat, and D.A.N. El-Din. Inorganica Chimica Acta, 2013. 406: p. 230-240. ISI[000324325200032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">67. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325135500004">Antifungal Susceptibility, Exoenzyme Production and Cytotoxicity of Novel Oximes against Candida.</a> Souza, J.L.S., F. Nedel, M. Ritter, P.H.A. Carvalho, C.M.P. Pereira, and R.G. Lund. Mycopathologia, 2013. 176(3-4): p. 201-210. ISI[000325135500004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">68. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324924200014">Evaluation of the Effect of Cassia surattensis Burm. f., Flower Methanolic Extract on the Growth and Morphology of Aspergillus niger.</a> Sumathy, V., Z. Zakaria, Y. Chen, L.Y. Latha, S.L. Jothy, S. Vijayarathna, and S. Sasidharan. European Review for Medical and Pharmacological Sciences, 2013. 17(12): p. 1648-1654. ISI[000324924200014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">69. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324977900001">Synthesis and Biological Evaluation of Acyclic Phosphonic acid Nucleoside Derivatives.</a> Wainwright, P., A. Maddaford, X.R. Zhang, H. Billington, D. Leese, R. Glen, D.C. Pryde, D.S. Middleton, P.T. Stephenson, and S. Sutton. Nucleosides, Nucleotides &amp; Nucleic Acids, 2013. 32(9): p. 477-492. ISI[000324977900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br />  

    <p class="plaintext">70. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325076400017">Synthesis and Fungicidal Evaluation of Some New Anilinopyrimidine Derivatives.</a> Waly, M.A., E.T. Bader-Eldien, M.E. Aboudobarah, and E.T. Aboumosalam. Medicinal Chemistry Research, 2013. 22(11): p. 5267-5273. ISI[000325076400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br /> 

    <p class="plaintext">71. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324850500013">Synthesis and in Vitro Antimicrobial Activity of Novel 2-(4-(Substituted-carboxamido)benzyl/phenyl)benzothiazoles.</a> Yilmaz, S., I. Yalcin, F. Kaynak-Onurdag, S. Ozgen, I. Yildiz, and E. Aki. Croatica Chemica Acta, 2013. 86(2): p. 223-231. ISI[000324850500013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1025-110713.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
