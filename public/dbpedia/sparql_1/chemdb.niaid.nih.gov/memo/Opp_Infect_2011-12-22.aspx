

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-12-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="LHF44gU8IhYDFSb4lrNZtgorGOEYM9DRpoHOT/ksswuiCXGOYoaRPCm3w7SdPbgbGpYIWK/qZ0bDkiS4vHBwgpAYPRHum0F6jl2A5/qznIHLDUUsQtcFRNRRGHU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="70FE1F64" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">December 9 - December 22, 2011</h1>  

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22179427">Chemical Constituents of the Methanolic Extract of Leaves of Leiothrix spiralis Ruhland and Their Antimicrobial Activity.</a> Araujo, M.G., F. Hilario, L.G. Nogueira, W. Vilegas, L.C. Santos, and T.M. Bauab, Molecules, 2011. 16(12): p. 10479-10490; PMID[22179427].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22169937">Characterization, Anti-Inflammatory and in Vitro Antimicrobial Activity of Some Novel Alkyl/Aryl Substituted Tertiary Alcohols.</a> Baseer, M., F.L. Ansari, Z. Ashraf, and R. Saeedulhaq. Molecules, 2011. 16(12): p. 10337-10346; PMID[22169937].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22025617">Native Thrombocidin-1 and Unfolded Thrombocidin-1 Exert Antimicrobial Activity via Distinct Structural Elements.</a> Kwakman, P.H., J. Krijgsveld, L. de Boer, L.T. Nguyen, L. Boszhard, J. Vreede, H.L. Dekker, D. Speijer, J.W. Drijfhout, A.A. Te Velde, W. Crielaard, H.J. Vogel, C.M. Vandenbroucke-Grauls, and S.A. Zaat, Journal of Biological Chemistry, 2011. 286(50): p. 43506-43514; PMID[22025617].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22071302">A New Antifungal Briarane Diterpenoid from the Gorgonian Junceella juncea Pallas.</a> Murthy, Y.L., D. Mallika, A. Rajack, and G. Damodar Reddy, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(24): p. 7522-7525; PMID[22071302].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21979249">Sodium Tripolyphosphate: An Excipient with Intrinsic in Vitro Anti-candida Activity.</a> Palmeira-de-Oliveira, R., A. Palmeira-de-Oliveira, C. Gaspar, S. Silvestre, J. Martinez-de-Oliveira, M.H. Amaral, and L. Breitenfeld, International Journal of Pharmaceutics, 2011. 421(1): p. 130-134; PMID[21979249].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22186954">Echevarria, Microwave-Assisted Synthesis of New N1,N4-Substituted thiosemicarbazones.</a> Reis, C.M., D.S. Pereira, O. Paiva Rde, L.F. Kneipp, and A. Molecules, 2011. 16(12): p. 10668-10684; PMID[22186954].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22174935">Bisbibenzyls, a New Type of Antifungal Agent, Inhibit Morphogenesis Switch and Biofilm Formation through Upregulation of DPP3 in Candida albicans.</a> Zhang, L., W. Chang, B. Sun, M. Groh, A. Speicher, and H. Lou, Plos One, 2011. 6(12): p. e28953; PMID[22174935].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22071303">Molecular Properties Prediction and Synthesis of Novel 1,3,4-Oxadiazole Analogues as Potent Antimicrobial and Antitubercular Agents.</a> Ahsan, M.J., J.G. Samy, H. Khalilullah, M.S. Nomani, P. Saraswat, R. Gaur, and A. Singh, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(24): p. 7246-7250; PMID[22071303].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22017513">Difluoromethylbenzoxazole pyrimidine thioether Derivatives: A Novel Class of Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Boyer, J., E. Arnoult, M. Medebielle, J. Guillemont, J. Unge, and D. Jochmans, Journal of Medicinal Chemistry, 2011. 54(23): p. 7974-7985; PMID[22017513].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22061642">Efficient Synthesis and in Vitro Antitubercular Activity of 1,2,3-Triazoles as Inhibitors of Mycobacterium tuberculosis.</a> Shanmugavelan, P., S. Nagarajan, M. Sathishkumar, A. Ponnuswamy, P. Yogeeswari, and D. Sriram, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(24): p. 7273-7276; PMID[22061642].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22061826">Synthesis and Inhibitory Activity of Thymidine Analogues Targeting Mycobacterium tuberculosis Thymidine Monophosphate Kinase.</a> Van Poecke, S., H. Munier-Lehmann, O. Helynck, M. Froeyen, and S. Van Calenbergh, Bioorganic &amp; Medicinal Chemistry, 2011. 19(24): p. 7603-7611; PMID[22061826].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22148514">Fluorocyclines. 1. 7-Fluoro-9-pyrrolidinoacetamido-6-demethyl-6-deoxytetracycline: A Potent, Broad Spectrum Antibacterial Agent.</a> Xiao, X.Y., D.K. Hunt, J. Zhu, R.B. Clark, N. Dunwoody, C. Fyfe, T.H. Grossman, W.J. O&#39;Brien, L. Plamondon, M. Ronn, C. Sun, W.Y. Zhang, and J.A. Sutcliffe, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22148514].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22155838">Novel Potent Metallocenes against Liver Stage Malaria.</a> Matos, J., P.d.C. F, E. Cabrita, J. Gut, F. Nogueira, V.E. do Rosario, R. Moreira, P.J. Rosenthal, M. Prudencio, and P. Gomes, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22155838].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22061825">Bis(oxyphenylene)benzimidazoles: A Novel Class of Anti-Plasmodium falciparum Agents.</a> Mayence, A., J.J. Vanden Eynde, M. Kaiser, R. Brun, N. Yarlett, and T.L. Huang, Bioorganic &amp; Medicinal Chemistry, 2011. 19(24): p. 7493-7500; PMID[22061825].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22155828">A Novel Carbon Monoxide-Releasing Molecule (CO-RM) Fully Protects Mice from Severe Malaria.</a> Pena, A.C., N. Penacho, L. Mancio-Silva, R. Neres, J.D. Seixas, A.C. Fernandes, C.C. Romao, M.M. Mota, G.J. Bernardes, and A. Pamplona, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22155828].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22148880">Antimalarial Activity of 9a-N Substituted 15-Membered Azalides with Improved in Vitro and in Vivo Activity over Azithromycin.</a> Peric, M., A. Fajdetic, R. Rupcic, S. Alihodzic, D. Ziher, M. Bukvic Krajacic, K.S. Smith, Z. Ivezic Schonfeld, J. Padovan, G. Landek, D. Jelic, A. Hutinec, M. Mesic, A. Ager, W.Y. Ellis, W.K. Milhous, C. Ohrt, and R. Spaventi, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22148880].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22071523">Triterpenoids as Inhibitors of Erythrocytic and Liver Stages of Plasmodium Infections.</a> Ramalhete, C., F.P. da Cruz, D. Lopes, S. Mulhovo, V.E. Rosario, M. Prudencio, and M.J. Ferreira, Bioorganic &amp; Medicinal Chemistry, 2011. 19(24): p. 7474-7481; PMID[22071523].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21872416">Screening Medicinal Plants for the Detection of Novel Antimalarial Products Applying the Inhibition of Beta-Hematin Formation.</a> Vargas, S., K. Ndjoko Ioset, A.E. Hay, J.R. Ioset, S. Wittlin, and K. Hostettmann, Journal of Pharmaceutical and Biomedical Analysis, 2011. 56(5): p. 880-886; PMID[21872416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22055713">Synthesis and Antimalarial Evaluation of Novel Isocryptolepine Derivatives.</a> Whittell, L.R., K.T. Batty, R.P. Wong, E.M. Bolitho, S.A. Fox, T.M. Davis, and P.E. Murray, Bioorganic &amp; Medicinal Chemistry, 2011. 19(24): p. 7519-7525; PMID[22055713].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p class="MsoNoSpacing"></p>

    <p> </p>

    <p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22055204">Synthesis and Antileishmanial Evaluation of 1-Aryl-4-(4,5-dihydro-1H-imidazol-2-yl)-1H-pyrazole Derivatives.</a> Dos Santos, M.S., M.L. Oliveira, A.M. Bernardino, R.M. de Leo, V.F. Amaral, F.T. de Carvalho, L.L. Leon, and M.M. Canto-Cavalheiro, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(24): p. 7451-7454; PMID[22055204].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22174820">Development of Derivatives of 3, 3&#39;-Diindolylmethane as Potent Leishmania donovani bi-subunit Topoisomerase Ib Poisons.</a> Roy, A., S. Chowdhury, S. Sengupta, M. Mandal, P. Jaisankar, I. D&#39;Annessa, A. Desideri, and H.K. Majumder, Plos One, 2011. 6(12): p. e28493; PMID[22174820].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTI-PROTOZOAL COMPOUNDS</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22179265">In Vitro Effects of Aqueous Extracts of Astragalus membranaceus and Scutellaria baicalensis Georgi on Toxoplasma gondii.</a> Yang, X., B. Huang, J. Chen, S. Huang, H. Zheng, Z.R. Lun, J. Shen, Y. Wang, and F. Lu, Parasitology Research, 2011. <b>[Epub ahead of print]</b>; PMID[22179265].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296852800025">Synthesis and Antifungal Activity of Some 2-Benzothiazolylthioacetyl Amino Acid and Peptide Derivatives.</a> Aboelmagd, A., Ali I. A. I., Salem E. M. S., and Abdel-Razik M., Arkivoc, 2011. (ix): p. 337-353; ISI[000296852800025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297013200009">New Flavanones from the Leaves of Cryptocarya chinensis and Their Antituberculosis Activity.</a> Chou, T.H., Chen J. J., Peng C. F., Cheng M. J., and Chen I. S., Chemistry &amp; Biodiversity, 2011. 8(11): p. 2015-2024; ISI[000297013200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297216900012">Antimicrobial Activity, Cytotoxicity and Intracellular Growth Inhibition of Portuguese Thymus Essential Oils.</a> Dandlen, S.A., Lima A. S., Mendes M. D., Miguel M. G., Faleiro M. L., Sousa M. J., Pedro L. G., Barroso J. G., and Figueiredo A. C., Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2011. 21(6): p. 1012-1024; ISI[000297216900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122211.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297013200022">Anticandidal Activity of the Essential Oil of Nepeta transcaucasica Grossh.</a> Iscan, G., Kose Y. B., Demirci B., and Baser K. H. C., Chemistry &amp; Biodiversity, 2011. 8(11): p. 2144-2148; ISI[000297013200022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297465300022">Chemical Composition, Antioxidant and Antifungal Potential of Melaleuca alternifolia (Tea Tree) and Eucalyptus globulus Essential Oils against Oral Candida Species.</a> Noumi, E., Snoussi M., Hajlaoui H., Trabelsi N., Ksouri R., Valentin E., and Bakhrouf A., Journal of Medicinal Plants Research, 2011. 5(17): p. 4147-4156; ISI[000297465300022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122211.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
