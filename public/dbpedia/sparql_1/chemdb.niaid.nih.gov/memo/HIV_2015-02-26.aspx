

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-02-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dsogMiFuEnFOgFn45IUtF7HfGDB70RoWcMvwfujbCj7UYJdMvhfvAsHstmKYP/TI75LjLMyB3OOIQOgfMT8XOhPl/2MQfrC99RnXX5BKeBc0rv4SJfo921FLIAk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="05ED4B75" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: February 13 - February 26, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25618597">Design and Synthesis of N-Methylpyrimidone Derivatives as HIV-1 Integrase Inhibitors.</a> Wang, Y., J. Rong, B. Zhang, L. Hu, X. Wang, and C. Zeng. Bioorganic &amp; Medicinal Chemistry, 2015. 23(4): p. 735-741. PMID[25618597].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0213-022615.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25619867">Inhibition of the Enhancement of Infection of Human Immunodeficiency Virus by Semen-derived Enhancer of Virus Infection Using Amyloid-targeting Polymeric Nanoparticles.</a> Sheik, D.A., L. Brooks, K. Frantzen, S. Dewhurst, and J. Yang. ACS Nano, 2015. 9(2): p. 1829-1836. PMID[25619867].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0213-022615.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25616343">Synthesis and Biological Evaluation of Phosphoramidate Prodrugs of Two Analogues of 2-Deoxy-D-ribose-1-phosphate Directed to the Discovery of Two Carbasugars as New Potential anti-HIV Leads.</a> Hamon, N., M. Slusarczyk, M. Serpi, J. Balzarini, and C. McGuigan. Bioorganic &amp; Medicinal Chemistry, 2015. 23(4): p. 829-838. PMID[25616343].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0213-022615.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25642994">Synthesis of EFdA Via a Diastereoselective Aldol Reaction of a Protected 3-Keto furanose.</a> Fukuyama, K., H. Ohrui, and S. Kuwahara. Organic Letters, 2015. 17(4): p. 828-831. PMID[25642994].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0213-022615.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25689224">High-affinity RNA Aptamers against the HIV-1 Protease Inhibit Both in Vitro Protease Activity and Late Events of Viral Replication.</a> Duclair, S., A. Gautam, A. Ellington, and V.R. Prasad. Molecular Therapy. Nucleic Acids, 2015. 4: p. e228. PMID[25689224].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0213-022615.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25592712">Dithiocarbamate-thiourea Hybrids Useful as Vaginal Microbicides Also Show Reverse Transcriptase Inhibition: Design, Synthesis, Docking and Pharmacokinetic Studies.</a> Bala, V., S. Jangir, D. Mandalapu, S. Gupta, Y.S. Chhonker, N. Lal, B. Kushwaha, H. Chandasana, S. Krishna, K. Rawat, J.P. Maikhuri, R.S. Bhatta, M.I. Siddiqi, R. Tripathi, G. Gupta, and V.L. Sharma. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 881-886. PMID[25592712].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0213-022615.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000348003500006">Hairpin Oligonucleotides Forming G-Quadruplexes: New Aptamers with anti-HIV Activity.</a> Romanucci, V., M. Gaglione, A. Messere, N. Potenza, A. Zarrelli, S. Noppen, S. Liekens, J. Balzarini, and F.G. Di. European Journal of Medicinal Chemistry, 2015. 89: p. 51-58. ISI[000348003500006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0213-022615.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347976500090">Halolactones Are Potent HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Han, X., H.M. Wu, C.N. Dong, P. Tien, W. Xie, S.W. Wu, and H.B. Zhou. RSC Advances, 2015. 5(13): p. 10005-10013. ISI[000347976500090].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0213-022615.</p>
    
    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000348455203227">Synthesis of HIV-1 Capsid Protein Inhibitors.</a> Jarvis, T.S., A. Chaudhry, A. Schrock, M.F. Summers, and M.T. Huggins. Abstracts of Papers of the American Chemical Society, 2014. 247. ISI[000348455203227].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0213-022515.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000348867500003">Synthesis, HIV-1 RT Inhibitory, Antibacterial, Antifungal and Binding Mode Studies of Some Novel N-Substituted 5-benzylidine-2,4-thiazolidinediones.</a> Bahare, R.S., S. Ganguly, K. Choowongkomon, and S. Seetaha. DARU-Journal of Pharmaceutical Sciences, 2015. 23. ISI[000348867500003].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0213-022515.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
