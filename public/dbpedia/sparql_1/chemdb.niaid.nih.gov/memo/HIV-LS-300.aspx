

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-300.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OnbkCjvpF1add8ae4C6SRAX3iY2Xww4UOGNCla0lIx7g8I0Owowmv8HPBlFh0oeKM0NGOi2zJUhxCvfIfsC8LrawwouuLHGXpEQzVozjJ4q8poobbpdYcYKcSzA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="400F60FA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-300-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43673   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Nuclear import of HIV-1 integrase is inhibited in vitro by styrylquinoline derivatives</p>

<p>           Mousnier, A, Leh, H, Mouscadet, JF, and Dargemont, C</p>

<p>           Mol Pharmacol  2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247318&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247318&amp;dopt=abstract</a> </p><br />

<p>     2.    43674   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Effect of polyanion-resistance on HIV-1 infection</p>

<p>           Bobardt, MD, Armand-Ugon, M, Clotet, I, Zhang, Z, David, G, Este, JA, and Gallay, PA</p>

<p>           Virology 2004. 325(2): 389-98</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246277&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246277&amp;dopt=abstract</a> </p><br />

<p>     3.    43675   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Effect of stereochemistry on the anti-HIV activity of chiral thiourea compounds</p>

<p>           Venkatachalam, TK, Mao, C, and Uckun, FM</p>

<p>           Bioorg Med Chem 2004. 12(15): 4275-84</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246104&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246104&amp;dopt=abstract</a> </p><br />

<p>     4.    43676   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Anti-AIDS agents. Part 61: Anti-HIV activity of new podophyllotoxin derivatives</p>

<p>           Zhu, XK, Guan, J, Xiao, Z, Mark, Cosentino L, and Lee, KH</p>

<p>           Bioorg Med Chem 2004. 12(15): 4267-73</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246103&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246103&amp;dopt=abstract</a> </p><br />

<p>     5.    43677   HIV-LS-300; EMBASE-HIV-7/15/2004</p>

<p class="memofmt1-3">           Antiviral activity of diterpenes isolated from the Brazilian marine alga Dictyota menstrualis against human immunodeficiency virus type 1 (HIV-1)</p>

<p>           Pereira, HS, Leao-Ferreira, LR, Moussatche, N, Teixeira, VL, Cavalcanti, DN, Costa, LJ, Diaz, R, and Frugulhetti, ICPP</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4CVTW51-1/2/02e737654c2203a1d5b16e7f0dcc2deb">http://www.sciencedirect.com/science/article/B6T2H-4CVTW51-1/2/02e737654c2203a1d5b16e7f0dcc2deb</a> </p><br />

<p>     6.    43678   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Preparation and anti-HIV activities of retrojusticidin B analogs and azalignans</p>

<p>           Sagar, KS, Chang, CC, Wang, WK, Lin, JY, and Lee, SS</p>

<p>           Bioorg Med Chem 2004. 12(15): 4045-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246082&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246082&amp;dopt=abstract</a>     </p>

<p>    7.     43679   HIV-LS-300; EMBASE-HIV-7/15/2004</p>

<p class="memofmt1-3">           N-linked glycosylation in the CXCR4 N-terminus inhibits binding to HIV-1 envelope glycoproteins</p>

<p>           Wang, Jianbin, Babcock, Gregory J, Choe, Hyeryun, Farzan, Michael, Sodroski, Joseph, and Gabuzda, Dana</p>

<p>           Virology 2004. 324(1): 140-150</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WXR-4CBW1GG-1/2/1962afa617600e8be026f81685b514ca">http://www.sciencedirect.com/science/article/B6WXR-4CBW1GG-1/2/1962afa617600e8be026f81685b514ca</a> </p><br />

<p>     8.    43680   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Simple, Short Peptide Derivatives of a Sulfonylindolecarboxamide (L-737,126) Active in Vitro against HIV-1 Wild Type and Variants Carrying Non-Nucleoside Reverse Transcriptase Inhibitor Resistance Mutations</p>

<p>           Silvestri, R, Artico, M, De Martino, G, La Regina, G, Loddo, R, La Colla, M, and La Colla, P</p>

<p>           J Med Chem 2004. 47(15): 3892-3896</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15239667&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15239667&amp;dopt=abstract</a> </p><br />

<p>     9.    43681   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Structure-Based Design, Synthesis, and Structure-Activity Relationship Studies of Novel Non-nucleoside Adenosine Deaminase Inhibitors</p>

<p>           Terasaka, T, Kinoshita, T, Kuno, M, Seki, N, Tanaka, K, and Nakanishi, I</p>

<p>           J Med Chem 2004. 47(15): 3730-3743</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15239652&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15239652&amp;dopt=abstract</a> </p><br />

<p>   10.    43682   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           HIV-1 Integrase Complexes with DNA Dissociate in the Presence of Short Oligonucleotides Conjugated to Acridine</p>

<p>           Pinskaya, M, Romanova, E, Volkov, E, Deprez, E, Leh, H, Brochon, JC, Mouscadet, JF, and Gottikh, M</p>

<p>           Biochemistry 2004. 43(27): 8735-8743</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15236582&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15236582&amp;dopt=abstract</a> </p><br />

<p>   11.    43683   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Novel mechanism of inhibition of HIV-1 reverse transcriptase a new nonnucleoside analog, KM-1</p>

<p>           Wang, LZ, Kenyon, GL, and Johnson, KA</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231830&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231830&amp;dopt=abstract</a> </p><br />

<p>   12.    43684   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Increasing number of anti-HIV drugs but no definite cure. Review of anti-HIV drugs</p>

<p>           Stolk, LM and Luers, JF</p>

<p>           Pharm World Sci 2004. 26(3): 133-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15230359&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15230359&amp;dopt=abstract</a> </p><br />

<p>  13.     43685   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Small molecules targeting severe acute respiratory syndrome human coronavirus</p>

<p>           Wu, CY, Jan, JT, Ma, SH, Kuo, CJ, Juan, HF, Cheng, YS, Hsu, HH, Huang, HC, Wu, D, Brik, A, Liang, FS, Liu, RS, Fang, JM, Chen, ST, Liang, PH, and Wong, CH</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(27): 10012-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15226499&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15226499&amp;dopt=abstract</a> </p><br />

<p>   14.    43686   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral activity of P1&#39; arylsulfonamide azacyclic urea HIV protease inhibitors</p>

<p>           Huang, PP, Randolph, JT, Klein, LL, Vasavanonda, S, Dekhtyar, T, Stoll, VS, and Kempf, DJ</p>

<p>           Bioorg Med Chem Lett 2004. 14(15):  4075-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225729&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225729&amp;dopt=abstract</a> </p><br />

<p>   15.    43687   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Lysine sulfonamides as novel HIV-protease inhibitors: N-disubstituted ureas</p>

<p>           Stranix, BR, Sauve, G, Bouzide, A, Cote, A, Sevigny, G, Yelle, J, and Perron, V</p>

<p>           Bioorg Med Chem Lett 2004. 14(15):  3971-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225709&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225709&amp;dopt=abstract</a> </p><br />

<p>   16.    43688   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           The sequence of the CA-SP1 junction accounts for the differential sensitivity of HIV-1 and SIV to the small molecule maturation inhibitor 3-O-{3&#39;,3&#39;-dimethylsuccinyl}-betulinic acid</p>

<p>           Zhou, J, Chen, CH, and Aiken, C</p>

<p>           Retrovirology  2004. 1(1): 15</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225375&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225375&amp;dopt=abstract</a> </p><br />

<p>   17.    43689   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           New 3-O-acyl betulinic acids from Strychnos vanprukii Craib</p>

<p>           Nguyen, QC, Nguyen, VH, Santarsiero, BD, Mesecar, AD, Nguyen, MC, Soejarto, DD, Pezzuto, JM, Fong, HH, and Tan, GT</p>

<p>           J Nat Prod 2004. 67(6): 994-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217281&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15217281&amp;dopt=abstract</a> </p><br />

<p>   18.    43690   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Model system for high-throughput screening of novel human immunodeficiency virus protease inhibitors in Escherichia coli</p>

<p>           Cheng, TJ, Brik, A, Wong, CH, and Kan, CC</p>

<p>           Antimicrob Agents Chemother 2004.  48(7): 2437-47</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215092&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215092&amp;dopt=abstract</a> </p><br />

<p>  19.     43691   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           New manzamine alkaloids from an Indo-Pacific sponge. Pharmacokinetics, oral availability, and the significant activity of several manzamines against HIV-I, AIDS opportunistic infections, and inflammatory diseases</p>

<p>           Yousaf, M, Hammond, NL, Peng, J, Wahyuono, S, McIntosh, KA, Charman, WN, Mayer, AM, and Hamann, MT</p>

<p>           J Med Chem 2004. 47(14): 3512-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15214779&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15214779&amp;dopt=abstract</a> </p><br />

<p>   20.    43692   HIV-LS-300; EMBASE-HIV-7/15/2004</p>

<p class="memofmt1-3">           Inhibition of human immunodeficiency virus type I integrase by naphthamidines and 2-aminobenzimidazoles</p>

<p>           Middleton, Tim, Lim, Hock B, Montgomery, Debra, Rockway, Todd, Tang, Hua, Cheng, Xueheng, Lu, Liangjun, Mo, Hongmei, Kohlbrenner, William E, Molla, Akhteruzzaman, and Kati, Warren M</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4CMYD2B-2/2/729b18549d89f7f90a8c6354cf55f865">http://www.sciencedirect.com/science/article/B6T2H-4CMYD2B-2/2/729b18549d89f7f90a8c6354cf55f865</a> </p><br />

<p>   21.    43693   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Molecular mechanism of dioxolane nucleosides against 3TC resistant M184V mutant HIV</p>

<p>           Chong, Y and Chu, CK</p>

<p>           Antiviral Res  2004. 63(1): 7-13</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15196815&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15196815&amp;dopt=abstract</a> </p><br />

<p>   22.    43694   HIV-LS-300; EMBASE-HIV-7/15/2004</p>

<p class="memofmt1-3">           Anti-HIV activity of some synthetic lignanolides and intermediates</p>

<p>           Sancho, Rocio, Medarde, Manuel, Sanchez-Palomino, Sonsoles, Madrigal, Blanca M, Alcami, Jose, Munoz, Eduardo, and San Feliciano, Arturo</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CSYKVG-2/2/288f29f42247279ac2bfb0c41f99b96e">http://www.sciencedirect.com/science/article/B6TF9-4CSYKVG-2/2/288f29f42247279ac2bfb0c41f99b96e</a> </p><br />

<p>   23.    43695   HIV-LS-300; PUBMED-HIV-7/13/2004</p>

<p class="memofmt1-3">           Modeling the HIV protease inhibitor adherence-resistance curve by use of empirically derived estimates</p>

<p>           Bangsberg, DR, Porco, TC, Kagay, C, Charlebois, ED, Deeks, SG, Guzman, D, Clark, R, and Moss, A</p>

<p>           J Infect Dis 2004. 190(1): 162-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15195256&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15195256&amp;dopt=abstract</a> </p><br />

<p>   24.    43696   HIV-LS-300; EMBASE-HIV-7/15/2004</p>

<p class="memofmt1-3">           Site-directed PEGylation of trichosanthin retained its anti-HIV activity with reduced potency in vitro</p>

<p>           Wang, Jian-Hua, Tam, Siu-Cheung, Huang, Hai, Ouyang, Dong-Yun, Wang, Yuan-Yuan, and Zheng, Yong-Tang</p>

<p>           Biochemical and Biophysical Research Communications 2004. 317(4): 965-971</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4C2R1PG-F/2/e3dfe38a33b532248de907b07cb5fc87">http://www.sciencedirect.com/science/article/B6WBK-4C2R1PG-F/2/e3dfe38a33b532248de907b07cb5fc87</a> </p><br />

<p>  25.     43697   HIV-LS-300; EMBASE-HIV-7/15/2004</p>

<p class="memofmt1-3">           Configurations of metallocyclams and relevance to anti-HIV activity</p>

<p>           Hunter, Tina M, Paisey, Stephen J, Park, Hye-seo, Cleghorn, Laura, Parkin, Andrew, Parsons, Simon, and Sadler, Peter J</p>

<p>           Journal of Inorganic Biochemistry 2004. 98(5): 713-719</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TGG-4BBHDVP-1/2/30f44b68e2045a87a48f499bb9cc8d97">http://www.sciencedirect.com/science/article/B6TGG-4BBHDVP-1/2/30f44b68e2045a87a48f499bb9cc8d97</a> </p><br />

<p>   26.    43698   HIV-LS-300; WOS-HIV-7/4/2004</p>

<p class="memofmt1-3">           Severe acute respiratory syndrome coroavirus (SARS-CoV) infection inhibition using spike protein heptad repeat-derived peptides</p>

<p>           Bosch, BJ, Martina, BEE, van, der Zee R, Lepault, J, Haijema, BJ, Versluis, C, Heck, AJR, de, Groot R, Osterhaus, ADME, and Rottier, PJM</p>

<p>           P NATL ACAD SCI USA 2004. 101(22): 8455-8460</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221831800037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221831800037</a> </p><br />

<p>   27.    43699   HIV-LS-300; WOS-HIV-7/4/2004</p>

<p class="memofmt1-3">           Structural mimicry of retroviral Tat proteins by constrained, beta-hairpin peptidomimetics: Ligands with high affinity and selectivity for viral TAR RNA regulatory elements</p>

<p>           Athanassiou, Z, Dias, RLA, Moehle, K, Dobson, N, Varani, G, and Robinson, JA</p>

<p>           J AM CHEM SOC  2004. 126(22): 6906-6913</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221828200029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221828200029</a> </p><br />

<p>   28.    43700   HIV-LS-300; WOS-HIV-7/4/2004</p>

<p class="memofmt1-3">           A theta-defensin composed exclusively of D-amino acids is active against HIV-1</p>

<p>           Owen, SM, Rudolph, D, Wang, W, Cole, AM, Sherman, MA, Waring, AJ, Lehrer, RI, and Lal, RB</p>

<p>           J PEPT RES 2004. 63(6): 469-476</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221741500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221741500003</a> </p><br />

<p>   29.    43701   HIV-LS-300; WOS-HIV-7/4/2004</p>

<p class="memofmt1-3">           CD4 down-modulating compounds with potent anti-HIV activity</p>

<p>           Vermeire, K, Schols, D, and Bell, TW</p>

<p>           CURR PHARM DESIGN 2004. 10(15): 1795-1803</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221784200010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221784200010</a> </p><br />

<p>   30.    43702   HIV-LS-300; WOS-HIV-7/4/2004</p>

<p class="memofmt1-3">           Synthesis and activity of phosphinic tripeptide inhibitors of cathepsin C</p>

<p>           Pawel, A, Pawelczak, M, Hurek, J, and Kafarski, P</p>

<p>           BIOORG MED CHEM LETT 2004. 14(12):  3113-3116</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221776800020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221776800020</a> </p><br />

<p>   31.    43703   HIV-LS-300; WOS-HIV-7/4/2004</p>

<p class="memofmt1-3">           1,2,5-substituted derivatives of 2-phenylpyrrolidine</p>

<p>           Tamazyan, R, Karapetyan, H, Martirisyan, A, Martirosyan, V, Harutyunyan, G, and Gasparyan, S</p>

<p>           ACTA CRYSTALLOGR C 2004. 60: O390-O392</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221790600029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221790600029</a> </p><br />

<p>   32.    43704   HIV-LS-300; WOS-HIV-7/4/2004</p>

<p class="memofmt1-3">           Bergenin monohydrate from the rhizomae of Astilbe chinensis</p>

<p>           Ye, YP, Sun, HX, and Pan, YJ</p>

<p>           ACTA CRYSTALLOGR C 2004. 60: O397-O398</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221790600032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221790600032</a> </p><br />

<p>  33.     43705   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           CP-MLR/PLS directed structure-activity modeling of the HIV-1 RT inhibitory activity of 2,3-diaryl-1,3-thiazolidin-4-ones</p>

<p>           Prabhakar, YS, Solomon, VR, Rawal, RK, Gupta, MK, and Katti, SB</p>

<p>           QSAR COMB SCI  2004. 23(4): 234-244</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222006100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222006100003</a> </p><br />

<p>   34.    43706   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           Inhibition of HIV-1 veplication by GB virus C infection through increases in RANTES, MIP-1 alpha, MIP-1 beta, and SDF-1</p>

<p>           Xiang, JH, George, SL, Wunschmann, S, Chang, Q, Klinzman, D, and Stapleton, JT</p>

<p>           LANCET 2004. 363(9426): 2040-2046</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222159800010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222159800010</a> </p><br />

<p>   35.    43707   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           New carbohydrate specificity and HIV-1 fusion blocking activity of the cyanobacteirial protein MVL : NMR, ITC and sedimentation equilibrium studies</p>

<p>           Bewley, CA, Cai, ML, Ray, S, Ghirando, R, Yamaguchi, M, and Muramoto, K</p>

<p>           J MOL BIOL 2004. 339(4): 901-914</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221919100016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221919100016</a> </p><br />

<p>   36.    43708   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           Hybrids of [TSAO-T]-[Foscarnet]: The first conjugate of foscarnet with a non-nucleoside reverse transcriptase inhibitor through a labile covalent ester bond</p>

<p>           Velazquez, S, Lobaton, E, De, Clercq E, Koontz, DL, Mellors, JW, Balzarini, J, and Camarasa, MJ</p>

<p>           J MED CHEM 2004. 47(13): 3418-3426</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221963400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221963400010</a> </p><br />

<p>   37.    43709   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           Rapeseed protein hydrolysates: a source of HIV protease peptide inhibitors</p>

<p>           Yust, MDM, Pedroche, J, Megias, C, Giron-Calle, J, Alaiz, M, Millan, F, and Vioque, J</p>

<p>           FOOD CHEM 2004. 87(3): 387-392</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221892700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221892700011</a> </p><br />

<p>   38.    43710   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral activity of 2,4-diamino-5-cyano-6-[2-(phosphono ethoxy)ethoxy]pyrimidine and related compounds</p>

<p>           Hockova, D, Holy, A, Masojidkova, M, Andrei, G, Snoeck, R, De, Clercq E, and Balzarini, J</p>

<p>           BIOORGAN MED CHEM 2004. 12(12): 3197-3202</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221937600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221937600009</a> </p><br />

<p>   39.    43711   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           Recent advances in the development of HIV-1 vaccines using replication-incompetent adenovirus vectors</p>

<p>           Shiver, JW and Emini, EA</p>

<p>           ANNU REV MED 2004. 55: 355-372</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221918000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221918000021</a> </p><br />

<p>   40.    43712   HIV-LS-300; WOS-HIV-7/12/2004</p>

<p class="memofmt1-3">           Alpha-defensins inhibit HIV infection of macrophages through upregulation of CC-chemokines</p>

<p>           Guo, CJ, Tan, N, Song, L, Douglas, SD, and Ho, WZ</p>

<p>           AIDS 2004. 18(8): 1217-1218</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221888200020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221888200020</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
