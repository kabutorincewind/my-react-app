

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-323.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8Loo1Q0jyUb1CjwGo2YQOAxrDYHilz3d6xQId+ZGKFaP7SvYG6JoFlsorcBhEKHPr3G1uZBO1cbeLqxuL2KLXXKoviX/HUk/3Rq40dA79MHNSH/h7EaNi7H7yK8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0518DC1F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-323-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45351   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Antimycobacterial Agents Differ with Respect to Their Bacteriostatic versus Bactericidal Activities in Relation to Time of Exposure, Mycobacterial Growth Phase, and Their Use in Combination</p>

    <p>          Bakker-Woudenberg, IA, van Vianen, W, van Soolingen, D, Verbrugh, HA, and van Agtmael, MA</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(6): 2387-2398</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917538&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917538&amp;dopt=abstract</a> </p><br />

    <p>2.     45352   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Bactericidal Activity of the Nitroimidazopyran PA-824 in a Murine Model of Tuberculosis</p>

    <p>          Tyagi, S, Nuermberger, E, Yoshimatsu, T, Williams, K, Rosenthal, I, Lounis, N, Bishai, W, and Grosset, J</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(6): 2289-2293</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917523&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15917523&amp;dopt=abstract</a> </p><br />

    <p>3.     45353   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Interferons alpha, beta, gamma each inhibit hepatitis C virus replication at the level of internal ribosome entry site-mediated translation</p>

    <p>          Dash, S, Prabhu, R, Hazari, S, Bastian, F, Garry, R, Zou, W, Haque, S, Joshi, V, Regenstein, FG, and Thung, SN</p>

    <p>          Liver Int <b>2005</b>.  25(3): 580-594</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15910496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15910496&amp;dopt=abstract</a> </p><br />

    <p>4.     45354   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Inhibitors of the NTPase/helicases of hepatitis C and related Flaviviridae viruses</p>

    <p>          Bretner, M, Najda, A, Podwinska, R, Baier, A, Paruch, K, Lipniacki, A, Piasek, A, Borowski, P, and Kulikowski, T</p>

    <p>          Acta Pol Pharm <b>2004</b>.  61 Suppl: 26-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15909930&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15909930&amp;dopt=abstract</a> </p><br />

    <p>5.     45355   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of 2/3-arylthio- and 2,3-bis(arylthio)-5-hydroxy-/5-methoxy-1,4-naphthoquinones</p>

    <p>          Ryu, Chung-Kyu, Shim, Ju-Yeon, Chae, Mi Jin, Choi, Ik Hwa, Han, Ja-Young, Jung, Ok-Jai, Lee, Jung Yoon, and Jeong, Seong Hee</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(5): 438-444</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     45356   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Molecular dynamics simulation studies of the wild-type, I21V and I16T mutants of isoniazid resistant Mycobacterium tuberculosis Enoyl Reductase (InhA) in complex with NADH: Towards the understanding of NADH-InhA different affinities</p>

    <p>          Schroeder, EK, Basso, LA, Santos, DS, and Norberto, de Souza O</p>

    <p>          Biophys J <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908576&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908576&amp;dopt=abstract</a> </p><br />

    <p>7.     45357   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Contribution of the Mycobacterium tuberculosis MmpL Protein Family to Virulence and Drug Resistance</p>

    <p>          Domenech, P, Reed, MB, and Barry, CE</p>

    <p>          Infect Immun <b>2005</b>.  73(6): 3492-3501</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908378&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15908378&amp;dopt=abstract</a> </p><br />

    <p>8.     45358   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Racemic and optically active 2-methoxy-4-oxatetradecanoic acids: novel synthetic fatty acids with selective antifungal properties</p>

    <p>          Carballeira, NM, O&#39;neill, R, and Parang, K</p>

    <p>          Chem Phys Lipids <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15899476&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15899476&amp;dopt=abstract</a> </p><br />

    <p>9.     45359   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis isocitrate lyases 1 and 2 are jointly required for in vivo growth and virulence</p>

    <p>          Munoz-Elias, EJ and McKinney, JD</p>

    <p>          Nat Med <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15895072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15895072&amp;dopt=abstract</a> </p><br />

    <p>10.   45360   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          Use of an animal model of disseminated candidiasis in the evaluation of antifungal therapy</p>

    <p>          Andes, D</p>

    <p>          Methods Mol Med <b>2005</b>.  118: 111-128</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888938&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888938&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   45361   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Total syntheses of the novel (+-)-2-methoxy-4-oxatetradecanoic acid and (S)-2-methoxy-4-oxatetradecanoic acid</p>

    <p>          O&#39;Neill, Rosann and Carballeira, Nestor M</p>

    <p>          229th ACS National Meeting <b>2005</b>.: ORGN-813</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   45362   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          The inhibition of Candida albicans by selected essential oils and their major components</p>

    <p>          Tampieri, MP,  Galuppi, R, Macchioni, F , Carelle, MS, Falcioni, L, Cioni, PL, and Morelli, I</p>

    <p>          Mycopathologia <b>2005</b>.  159(3): 339-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15883716&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15883716&amp;dopt=abstract</a> </p><br />

    <p>13.   45363   OI-LS-323; PUBMED-OI-5/31/2005</p>

    <p class="memofmt1-2">          In vitro and in vivo antimycobacterial activity of an antihypertensive agent methyl-L-DOPA</p>

    <p>          Dutta, NK, Mazumdar, K, Dastidar, SG, Chakrabarty, AN, Shirataki, Y, and Motohashi, N</p>

    <p>          In Vivo <b>2005</b> .  19(3): 539-545</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15875773&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15875773&amp;dopt=abstract</a> </p><br />

    <p>14.   45364   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Synthesis of thiosemicarbazone and 4-thiazolidinone derivatives and their in vitro anti-Toxoplasma gondii activity</p>

    <p>          Tenorio, Romulo P, Carvalho, Cristiane S, Pessanha, Carla S, de Lima, Jose G, de Faria, Antonio R, Alves, Antonio J, de Melo, Edesio JT, and Goes, Alexandre JS</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(10): 2575-2578</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   45365   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Crystallization and preliminary X-ray crystallographic analysis of 3-deoxy-D-arabino-heptulosonate-7-phosphate synthase from Mycobacterium tuberculosis</p>

    <p>          Webby, Celia J, Lott, JShaun, Baker, Heather M, Baker, Edward N, and Parker, Emily J</p>

    <p>          Acta Crystallographica, Section F: Structural Biology and Crystallization Communications <b>2005</b>.  F61(4): 403-406</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   45366   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Cytotoxicity, antitumoral and antimycobacterial activity of tetrazole and oxadiazole derivatives</p>

    <p>          De Souza, AO,  Pedrosa, MTC, Alderete, JB, Cruz, AF, Prado, MAF, Alves, RB, and Silva, CL</p>

    <p>          PHARMAZIE <b>2005</b>.  60(5): 396-397, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228942400016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228942400016</a> </p><br />

    <p>17.   45367   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Pharmacokinetics of antiviral agents for the treatment of cytomegalovirus infection</p>

    <p>          Baillie, GMark </p>

    <p>          American Journal of Health-System Pharmacy <b>2005</b>.  62(Suppl. 1): S14-S17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.   45368   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Design, Synthesis and Antiviral Activity of Novel 4,5-Disubstituted 7-(b-D-Ribofuranosyl)pyrrolo[2,3-d][1,2,3]triazines and the Novel 3-Amino-5-methyl-1-(b-D-ribofuranosyl)- and 3-Amino-5-methyl-1-(2-deoxy-b-D-ribofuranosyl)-1,5-dihydro-1,4,5,6,7,8-hexaazaacenaphthylene as Analogues of Triciribine</p>

    <p>          Migawa, Michael T, Drach, John C, and Townsend, Leroy B</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(11): 3840-3851</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   45369   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Antiviral activities of plant triterpenes, their derivatives and ribavirin phosphonates</p>

    <p>          Il&#39;icheva, TN, Kulishova, LA, Shul&#39;ts, EE, Petrenko, NI, Uzenkova, NV, Yas&#39;ko, MV, Serova, OA, Volkov, GN, Belanov, EF, Tolstikov, GA, and Pokrovskii, AG</p>

    <p>          Vestnik Rossiiskoi Akademii Meditsinskikh Nauk <b>2005</b>.(2): 26-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   45370   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          In vitro activity and mechanism of action of methylenecyclopropane analogs of nucleosides against herpesvirus replication</p>

    <p>          Kern, Earl R, Kushner, Nicole L, Hartline, Caroll B, Williams-Aziz, Stephanie L, Harden, Emma A, Zhou, Shaoman, Zemlicka, Jiri, and Prichard, Mark N</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(3): 1039-1045</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   45371   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of 7-O-substituted-4-methyl-2H-2-chromenone derivatives vs Mycobacterium tuberculosis</p>

    <p>          Yee, SW, Shah, B, and Simons, C</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2005</b>.  20(2): 109-113, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000001</a> </p><br />

    <p>22.   45372   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Anticandidal properties of N-acylpeptides containing an inhibitor of glucosamine-6-phosphate synthase</p>

    <p>          Andruszkiewicz, R, Zieniawa, T, and Walkowiak, A</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2005</b>.  20(2): 115-121, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000002</a> </p><br />

    <p>23.   45373   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Synthesis and antituberculosis activity of new 3-alkylsulfanyl-1,2,4-triazole derivatives</p>

    <p>          Kaplancikli, ZA, Turan-Zitouni, G, and Chevallet, P</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2005</b>.  20(2): 179-182, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000009</a> </p><br />

    <p>24.   45374   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          In-vitro antibacterial, antifungal and cytotoxic properties of sulfonamide-derived Schiff&#39;s bases and their metal complexes</p>

    <p>          Chohan, ZH, Ul-Hassan, M, Khan, KM, and Supuran, CT</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2005</b>.  20(2): 183-188, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228805000010</a> </p><br />
    <br clear="all">

    <p>25.   45375   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Crystal structure of Mycobacterium tuberculosis D-3-phosphoglycerate dehydrogenase</p>

    <p>          Dey, S, Grant, GA, and Sacchettini, JC</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(15): 14892-14899, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228236800068">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228236800068</a> </p><br />

    <p>26.   45376   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Use of indomethacin and indomethacin derivatives as broad-spectrum antiviral drugs, and corresponding pharmaceutical compositions</p>

    <p>          Santoro, Maria Gabriella</p>

    <p>          PATENT:  WO <b>2005013980</b>  ISSUE DATE:  20050217</p>

    <p>          APPLICATION: 2004  PP: 24 pp.</p>

    <p>          ASSIGNEE:  (Universita&#39; Degli Studi di Roma &#39;tor Vergata&#39;, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   45377   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Synthesis and Evaluation of S-Acyl-2-thioethyl Esters of Modified Nucleoside 5&#39;-Monophosphates as Inhibitors of Hepatitis C Virus RNA Replication</p>

    <p>          Prakash, Thazha P, Prhavc, Marija, Eldrup, Anne B, Cook, PDan, Carroll, Steven S, Olsen, David B, Stahlhut, Mark W, Tomassini, Joanne E, MacCoss, Malcolm, Galloway, Sheila M, Hilliard, Catherine, and Bhat, Balkrishen</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(4): 1199-1210</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   45378   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;-b-C-methyl toyocamycin and sangivamycin analogs as potential HCV inhibitors</p>

    <p>          Ding, Yili, An, Haoyun, Hong, Zhi, and Girardet, Jean-Luc</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(3): 725-727</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   45379   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Molecular epidemiology: A tool for understanding control of tuberculosis transmission</p>

    <p>          Daley, CL</p>

    <p>          CLINICS IN CHEST MEDICINE <b>2005</b>.  26(2): 217-+, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228823000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228823000006</a> </p><br />

    <p>30.   45380   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          New drugs for tuberculosis: Current status and future prospects</p>

    <p>          O&#39;Brien, RJ and Spigelman, M</p>

    <p>          CLINICS IN CHEST MEDICINE <b>2005</b>.  26(2): 327-+, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228823000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228823000013</a> </p><br />

    <p>31.   45381   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of ferrocenyl ethambutol analogues and ferrocenyl diamines</p>

    <p>          Razafimahefa, D, Ralambomanana, DA, Hammouche, L, Pelinski, L, Lauvagie, S, Bebear, C, Brocard, J, and Maugein, J</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(9): 2301-2303, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228888200021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228888200021</a> </p><br />
    <br clear="all">

    <p>32.   45382   OI-LS-323; WOS-OI-5/22/2005</p>

    <p class="memofmt1-2">          Antisense oligonucleotides inhibiting ribosomal functions in mycobacteria</p>

    <p>          Demchenko, YN, Zenkova, MA, and Vlassov, VV</p>

    <p>          BIOLOGY BULLETIN <b>2005</b>.  32(2): 101-107, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228796700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228796700001</a> </p><br />

    <p>33.   45383   OI-LS-323; SCIFINDER-OI-5/24/2005</p>

    <p class="memofmt1-2">          Triple Antiviral Therapy with Amantadine for IFN-Ribavirin Nonresponders with Recurrent Posttransplantation Hepatitis C</p>

    <p>          Bizollon, Thierry, Adham, Mustapha, Pradat, Pierre, Chevallier, Michelle, Ducerf, Christian, Baulieux, Jacques, Zoulim, Fabian, and Trepo, Christian</p>

    <p>          Transplantation <b>2005</b>.  79(3): 325-329</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   45384   OI-LS-323; WOS-OI-5/30/2005</p>

    <p class="memofmt1-2">          A member of the cAMP receptor protein family of transcription regulators in Mycobacterium tuberculosis is required for virulence in mice and controls transcription of the rpfA gene coding for a resuscitation promoting factor</p>

    <p>          Rickman, L, Scott, C, Hunt, DM, Hutchinson, T, Menendez, MC, Whalan, R, Hinds, J, Colston, MJ, Green, J, and Buxton, RS</p>

    <p>          MOLECULAR MICROBIOLOGY <b>2005</b>.  56(5): 1274-1286, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228975300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228975300014</a> </p><br />

    <p>35.   45385   OI-LS-323; WOS-OI-5/30/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus replicons escape RNA interference induced by a short interfering RNA directed against the NS5b coding region</p>

    <p>          Wilson, JA and Richardson, CD</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(11): 7050-7058, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400049">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400049</a> </p><br />

    <p>36.   45386   OI-LS-323; WOS-OI-5/30/2005</p>

    <p class="memofmt1-2">          Identification of an SH3-binding motif in a new class of methionine aminopeptidases from Mycobacterium tuberculosis suggests a mode of interaction with the ribosome</p>

    <p>          Addlagatta, A, Quillin, ML, Omotoso, O, Liu, JO, and Matthews, BW</p>

    <p>          BIOCHEMISTRY <b>2005</b>.  44(19): 7166-7174, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229057600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229057600008</a> </p><br />

    <p>37.   45387   OI-LS-323; WOS-OI-5/30/2005</p>

    <p class="memofmt1-2">          Inhibitory effect of aureobasidin A on Toxoplasma gondii</p>

    <p>          Sonda, S, Sala, G, Ghidoni, R, Hemphill, A, and Pieters, J</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(5): 1794-1801, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228982600019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228982600019</a> </p><br />
    <br clear="all">

    <p>38.   45388   OI-LS-323; WOS-OI-5/30/2005</p>

    <p class="memofmt1-2">          Inhibitory effect of 2 &#39;-substituted nucleosides on hepatitis C virus replication correlates with metabolic properties in replicon cells</p>

    <p>          Tomassini, JE, Getty, K, Stahlhut, MW, Shim, S, Bhat, B, Eldrup, AB, Prakash, TP, Carroll, SS, Flores, O, MacCoss, M, McMasters, DR, Migliaccio, G, and Olsen, DB</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(5): 2050-2058, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228982600052">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228982600052</a> </p><br />

    <p>39.   45389   OI-LS-323; WOS-OI-5/30/2005</p>

    <p class="memofmt1-2">          HCV anti-viral agents</p>

    <p>          Griffith, RC, Lou, L, Roberts, CD, and Schmitz, U</p>

    <p>          ANNUAL REPORTS IN MEDICINAL CHEMISTRY, VOL 39 <b>2004</b>.  39: 223-237, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228477500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228477500018</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
