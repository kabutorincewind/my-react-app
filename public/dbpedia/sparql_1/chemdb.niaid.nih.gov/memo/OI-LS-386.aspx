

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-386.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="py8GnhT48RvIf7WtoOYB+E0YdEOpktysno/QiTpp2RiFQ8V6Nz1Tz5pWvf0wXplVpQvlgrkmWmGD8sJmHEc5/o86eP4m4BFopJYdFeTmArohUkyhYchUAMov8sU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="570E453D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p> </p>

    <p class="memofmt1-1"> </p>

    <p class="memofmt1-1">ONLINE DATABASE SEARCH OI-LS-386-MEMO</p>

    <p> </p>

    <p>1.       62092   OI-LS-386; PUBMED-OI-11/26/2007</p>

    <p class="memofmt1-2">Synthesis and antimicrobial activity of some novel nucleoside analogues of adenosine and 1,3-dideazaadenosine</p>

    <p>Srivastava R,  Bhargava A, and Singh RK</p>

    <p>Bioorg Med Chem Lett <b>2007</b>.  17(22): 6239-44</p>

    <p class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17890082&amp;dopt=abstract</p>

    <p>2.    62098   OI-LS-386; WOS-IO-11/26/2007</p>

    <p class="memofmt1-2">Efficient Synthesis of Iminoctadine, a Potent Antifungal Agent and Polyamine Oxidase Inhibitor (Pao)</p>

    <p>Raff, F., Corelli, F., and Botta, M.</p>

    <p>Synthesis-Stuttgart <b>2007</b>.(19): 3013-3016</p>

    <p class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000250344400011</p>

    <p> </p>

    <p>3.    62119   OI-LS-386; SCIFINDER-OI-11/21/2007</p>

    <p class="memofmt1-2">Preparation of antimycotic and antimycobacterial thiosalicylanilides</p>

    <p>Kubicovai, Lenka, Sedlaik, Miloai, Aaustr, Martin, Pravda, Martin, Chobot, Vladima R, Skaila, Pavel, Buchta, Vladima R, Machaiaiek, Miloai, and Waisser, Karel</p>

    <p>PATENT:  CZ <b>297581</b>  ISSUE DATE: 20070110</p>

    <p>APPLICATION: 2005  PP: 27pp.</p>

    <p>ASSIGNEE:  (Universita Karlova V Praze, Farmaceutickai Fakulta V Hradci Krailova Czech Rep. and Universita Pardubice, Fakulta Chemicko-Technologickai</p>

    <p> </p>

    <p>4.    62148   OI-LS-386; SCIFINDER-OI-11/21/2007</p>

    <p class="memofmt1-2">Medical fungicides containing 3-[(di- or tetrahydro)isoquinolin-1-yl]quinolines</p>

    <p>Ito, Hiroyuki and Tamagawa, Yasushi</p>

    <p>PATENT:  JP <b>2007269686</b>  ISSUE DATE:  20071018</p>

    <p>APPLICATION: 2006-31294  PP: 54pp.</p>

    <p>ASSIGNEE:  (Sankyo Agro Co., Ltd. Japan</p>

    <p> </p>

    <p>5.    62155   OI-LS-386; SCIFINDER-OI-11/21/2007</p>

    <p class="memofmt1-2">Preparation of indoloquinolines and related compounds as antifungals and parasiticides</p>

    <p>Ablordeppey, Seth Y</p>

    <p>PATENT:  US <b>2007232640</b>  ISSUE DATE:  20071004</p>

    <p>APPLICATION: 2006  PP: 14pp.</p>

    <p>ASSIGNEE:  (USA)</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
