

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="2013Archive.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ObbZmrzQ0rBQ3MjkkMHg2RHTJ3T7Uh+BPG6SUFAsNTHGOsjK13yYtbMlipjWGYQdj4aGKhB3/O95DHPn6lJNQliMjzfTsomY7NWnMJJl60lfXDrgKOtiLbwOo34=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B9609275" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="description">
		<h1>Literature Surveillance Memos</h1>

        <p>This page contains links to bi-weekly literature surveillance memos that identify relevant published research therapies on pre-clinical experimental therapies for HIV, the opportunistic infections (OIs) associated with AIDS and other viral pathogens.</p>
        <p>Note: Where available, URL links to article abstracts have been included for the convenience of the user. Abstract links to PubMed (ncbi.nlm.nih.gov) are available to all users.  Abstract links to Web of Science (publishorperish.nih.gov) or Science Direct (sciencedirect.com) are only available to NIH staff inside the NIH firewall or to other registered users of these Websites.</p>
        
        <div id="memo">
            <table id="memotable">
                <tr><th class="MemoColHIVArchive">HIV</th><th class="MemoColOIArchive">Opportunistic Infections</th><th class="MemoColOVArchive">Other Viral Pathogens</th></tr>
                <tr><td><a href="HIV_2013-12-19.aspx" target="_blank">HIV 2013-12-19</a></td><td><a href="Opp_Infect_2013-12-19.aspx" target="_blank">Opp Infect 2013-12-19</a></td><td><a href="Other_Viruses_2013-12-19.aspx" target="_blank">Other Viruses 2013-12-19</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-12-05.aspx" target="_blank">HIV 2013-12-05</a></td><td><a href="Opp_Infect_2013-12-05.aspx" target="_blank">Opp Infect 2013-12-05</a></td><td><a href="Other_Viruses_2013-12-05.aspx" target="_blank">Other Viruses 2013-12-05</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-11-21.aspx" target="_blank">HIV 2013-11-21</a></td><td><a href="Opp_Infect_2013-11-21.aspx" target="_blank">Opp Infect 2013-11-21</a></td><td><a href="Other_Viruses_2013-11-21.aspx" target="_blank">Other Viruses 2013-11-21</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-11-07.aspx" target="_blank">HIV 2013-11-07</a></td><td><a href="Opp_Infect_2013-11-07.aspx" target="_blank">Opp Infect 2013-11-07</a></td><td><a href="Other_Viruses_2013-11-07.aspx" target="_blank">Other Viruses 2013-11-07</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-10-24.aspx" target="_blank">HIV 2013-10-24</a></td><td><a href="Opp_Infect_2013-10-24.aspx" target="_blank">Opp Infect 2013-10-24</a></td><td><a href="Other_Viruses_2013-10-24.aspx" target="_blank">Other Viruses 2013-10-24</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-09-26.aspx" target="_blank">HIV 2013-09-26</a></td><td><a href="Opp_Infect_2013-09-26.aspx" target="_blank">Opp Infect 2013-09-26</a></td><td><a href="Other_Viruses_2013-09-26.aspx" target="_blank">Other Viruses 2013-09-26</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-09-12.aspx" target="_blank">HIV 2013-09-12</a></td><td><a href="Opp_Infect_2013-09-12.aspx" target="_blank">Opp Infect 2013-09-12</a></td><td><a href="Other_Viruses_2013-09-12.aspx" target="_blank">Other Viruses 2013-09-12</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-08-29.aspx" target="_blank">HIV 2013-08-29</a></td><td><a href="Opp_Infect_2013-08-29.aspx" target="_blank">Opp Infect 2013-08-29</a></td><td><a href="Other_Viruses_2013-08-29.aspx" target="_blank">Other Viruses 2013-08-29</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-08-15.aspx" target="_blank">HIV 2013-08-15</a></td><td><a href="Opp_Infect_2013-08-15.aspx" target="_blank">Opp Infect 2013-08-15</a></td><td><a href="Other_Viruses_2013-08-15.aspx" target="_blank">Other Viruses 2013-08-15</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-08-01.aspx" target="_blank">HIV 2013-08-01</a></td><td><a href="Opp_Infect_2013-08-01.aspx" target="_blank">Opp Infect 2013-08-01</a></td><td><a href="Other_Viruses_2013-08-01.aspx" target="_blank">Other Viruses 2013-08-01</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-07-18.aspx" target="_blank">HIV 2013-07-18</a></td><td><a href="Opp_Infect_2013-07-18.aspx" target="_blank">Opp Infect 2013-07-18</a></td><td><a href="Other_Viruses_2013-07-18.aspx" target="_blank">Other Viruses 2013-07-18</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-07-04.aspx" target="_blank">HIV 2013-07-04</a></td><td><a href="Opp_Infect_2013-07-04.aspx" target="_blank">Opp Infect 2013-07-04</a></td><td><a href="Other_Viruses_2013-07-04.aspx" target="_blank">Other Viruses 2013-07-04</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-06-20.aspx" target="_blank">HIV 2013-06-20</a></td><td><a href="Opp_Infect_2013-06-20.aspx" target="_blank">Opp Infect 2013-06-20</a></td><td><a href="Other_Viruses_2013-06-20.aspx" target="_blank">Other Viruses 2013-06-20</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-06-06.aspx" target="_blank">HIV 2013-06-06</a></td><td><a href="Opp_Infect_2013-06-06.aspx" target="_blank">Opp Infect 2013-06-06</a></td><td><a href="Other_Viruses_2013-06-06.aspx" target="_blank">Other Viruses 2013-06-06</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-05-23.aspx" target="_blank">HIV 2013-05-23</a></td><td><a href="Opp_Infect_2013-05-23.aspx" target="_blank">Opp Infect 2013-05-23</a></td><td><a href="Other_Viruses_2013-05-23.aspx" target="_blank">Other Viruses 2013-05-23</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-05-09.aspx" target="_blank">HIV 2013-05-09</a></td><td><a href="Opp_Infect_2013-05-09.aspx" target="_blank">Opp Infect 2013-05-09</a></td><td><a href="Other_Viruses_2013-05-09.aspx" target="_blank">Other Viruses 2013-05-09</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-04-25.aspx" target="_blank">HIV 2013-04-25</a></td><td><a href="Opp_Infect_2013-04-25.aspx" target="_blank">Opp Infect 2013-04-25</a></td><td><a href="Other_Viruses_2013-04-25.aspx" target="_blank">Other Viruses 2013-04-25</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-04-11.aspx" target="_blank">HIV 2013-04-11</a></td><td><a href="Opp_Infect_2013-04-11.aspx" target="_blank">Opp Infect 2013-04-11</a></td><td><a href="Other_Viruses_2013-04-11.aspx" target="_blank">Other Viruses 2013-04-11</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-03-28.aspx" target="_blank">HIV 2013-03-28</a></td><td><a href="Opp_Infect_2013-03-28.aspx" target="_blank">Opp Infect 2013-03-28</a></td><td><a href="Other_Viruses_2013-03-28.aspx" target="_blank">Other Viruses 2013-03-28</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-03-14.aspx" target="_blank">HIV 2013-03-14</a></td><td><a href="Opp_Infect_2013-03-14.aspx" target="_blank">Opp Infect 2013-03-14</a></td><td><a href="Other_Viruses_2013-03-14.aspx" target="_blank">Other Viruses 2013-03-14</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-02-28.aspx" target="_blank">HIV 2013-02-28</a></td><td><a href="Opp_Infect_2013-02-28.aspx" target="_blank">Opp Infect 2013-02-28</a></td><td><a href="Other_Viruses_2013-02-28.aspx" target="_blank">Other Viruses 2013-02-28</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-02-14.aspx" target="_blank">HIV 2013-02-14</a></td><td><a href="Opp_Infect_2013-02-14.aspx" target="_blank">Opp Infect 2013-02-14</a></td><td><a href="Other_Viruses_2013-02-14.aspx" target="_blank">Other Viruses 2013-02-14</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-01-31.aspx" target="_blank">HIV 2013-01-31</a></td><td><a href="Opp_Infect_2013-01-31.aspx" target="_blank">Opp Infect 2013-01-31</a></td><td><a href="Other_Viruses_2013-01-31.aspx" target="_blank">Other Viruses 2013-01-31</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-01-17.aspx" target="_blank">HIV 2013-01-17</a></td><td><a href="Opp_Infect_2013-01-17.aspx" target="_blank">Opp Infect 2013-01-17</a></td><td><a href="Other_Viruses_2013-01-17.aspx" target="_blank">Other Viruses 2013-01-17</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                  
                <tr><td><a href="HIV_2013-01-03.aspx" target="_blank">HIV 2013-01-03</a></td><td><a href="Opp_Infect_2013-01-03.aspx" target="_blank">Opp Infect 2013-01-03</a></td><td><a href="Other_Viruses_2013-01-03.aspx" target="_blank">Other Viruses 2013-01-03</a></td></tr>                                                                                                                                                                                                                                                                                                                                                                                  
            </table>
			<fieldset id="memobottom">
			    <br /><br /><br />
			    <fieldset class="submitbuttons" id="memobottomrow">
                    <ul id="memobottomrowbar">
                        <li><a href="2004Archive.aspx">2004</a></li> 
	    		    	<li><a href="2005Archive.aspx">2005</a></li>
	    		    	<li><a href="2006Archive.aspx">2006</a></li>
	    			    <li><a href="2007Archive.aspx">2007</a></li>
	    			    <li><a href="2009Archive.aspx">2008-2009</a></li>
	    			    <li><a href="2010Archive.aspx">2010</a></li>
	    			    <li><a href="2011Archive.aspx">2011</a></li>
  				        <li><a href="2012Archive.aspx">2012</a></li>
   				        <li><a href="2014Archive.aspx">2014</a></li>    
   				        <li><a href="2015Archive.aspx">2015</a></li>    
   				        <li><a href="2016Archive.aspx">2016</a></li>       				        
   				        <li><a href="memos.aspx">2017</a></li>    
                    </ul>                                      
    			    <br /><br />  				    			    
			    </fieldset>
            </fieldset>
            
        </div>

    </div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
