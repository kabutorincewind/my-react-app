

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-02-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="rXydNmQxCrJiw0kCIlT75Ye6kA6TS/ECqH2aqchxgR2L9iGgprajeS7SHa+2ZmZflDOvtNXY9nEUPkKZxItxKFe4o1Vq0GOm0aTXEbaZL7rUYyaSgPgSkBbzWAo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E3E91EF9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 21 - February 3, 2009</h1>

    <p class="memofmt2-2">Pubmed Citations</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19178289?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Derivatives of 5-Nitro-furan-2-carboxylic Acid Carbamoylmethyl Ester Inhibit RNase H Activity Associated with HIV-1 Reverse Transcriptase.</a>  Fuji H, Urano E, Futahashi Y, Hamatake M, Tatsumi J, Hoshino T, Morikawa Y, Yamamoto N, Komano J.  J Med Chem. 2009 Jan 29.  [Epub ahead of print]</p>

    <p class="pmid">PMID: 19178289</p>


    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19019450?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Tetra-chloro-(bis-(3,5-dimethylpyrazolyl)methane)gold(III) chloride: An HIV-1 reverse transcriptase and protease inhibitor.</a>  Fonteh PN, Keter FK, Meyer D, Guzei IA, Darkwa J.  J Inorg Biochem. 2009 Feb;103(2):190-4. Epub 2008 Oct 14.</p>

    <p class="pmid">PMID: 19019450</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19105658?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Docking studies on a new human immunodeficiency virus integrase-Mg-DNA complex: phenyl ring exploration and synthesis of 1H-benzylindole derivatives through fluorine substitutions.</a>  Ferro S, De Luca L, Barreca ML, Iraci N, De Grazia S, Christ F, Witvrouw M, Debyser Z, Chimirri A.  J Med Chem. 2009 Jan 22;52(2):569-73.</p>

    <p class="pmid">PMID: 19105658</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19175319?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel Indazole Non-Nucleoside Reverse Transcriptase Inhibitors Using Molecular Hybridization Based on Crystallographic Overlays (dagger).</a>  Jones LH, Allan G, Barba O, Burt C, Corbau R, Dupont T, Kno&#776;chel T, Irving S, Middleton DS, Mowbray CE, Perros M, Ringrose H, Swain NA, Webster R, Westby M, Phillips C.  J Med Chem. 2009 Jan 28.</p>

    <p class="pmid">PMID: 19175319</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19135378?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Conformational analysis of trimeric maleimide substituted 1,5,9-triazacyclododecane HIV fusion scaffolds.</a>  Remmert S, Hollis H, Parish CA.  Bioorg Med Chem. 2009 Feb 1;17(3):1251-8.</p>

    <p class="pmid">PMID: 19135378</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19117780?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">CoMFA and CoMSIA studies on thiazolidin-4-one as anti-HIV-1 agents.</a>  Murugesan V, Prabhakar YS, Katti SB.  J Mol Graph Model. 2009 Feb;27(6):735-43. Epub 2008 Nov 21.</p>

    <p class="pmid">PMID: 19117780</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19175308?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Cleavage of Four Carbon-Carbon Bonds during Biosynthesis of the Griseorhodin A Spiroketal Pharmacophore.</a>  Yunt Z, Reinhardt K, Li A, Engeser M, Dahse HM, Gu&#776;tschow M, Bruhn T, Bringmann G, Piel J.  J Am Chem Soc. 2009 Jan 28. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19175308</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19170521?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Specific Targeting of Highly Conserved Residues in the HIV-1 Reverse Transcriptase Primer Grip Region. 2. Stereoselective Interaction to Overcome the Effects of Drug Resistant Mutations.</a>  Butini S, Brindisi M, Cosconati S, Marinelli L, Borrelli G, Coccone SS, Ramunno A, Campiani G, Novellino E, Zanoli S, Samuele A, Giorgi G, Bergamini A, Mattia MD, Lalli S, Galletti B, Gemma S, Maga G.  J Med Chem. 2009 Jan 26. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19170521</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19114310?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Bisubstrate Inhibitors of the MYST HATs Esa1 and Tip60.</a>  Wu J, Xie N, Wu Z, Zhang Y, Zheng YG.  Bioorg Med Chem. 2009 Feb 1;17(3):1381-6. Epub 2008 Dec 24.</p>

    <p class="pmid">PMID: 19114310</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19178153?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Potent Inhibitors of HIV-1 Integrase Display a Two-Step, Slow-Binding Inhibition Mechanism Which Is Absent in a Drug-Resistant T66I/M154I Mutant.</a>  Garvey EP, Schwartz B, Gartland MJ, Lang S, Halsey W, Sathe G, Carter HL, Weaver KL.  Biochemistry. 2009 Jan 29.</p>

    <p class="pmid">PMID: 19178153</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19186189?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The drug monosodium luminol (GVT) preserves crypt-villus epithelial organization and allows survival of infected intestinal T cells in mice infected with the ts1 retrovirus.</a>  Scofield VL, Yan M, Kuang X, Kim SJ, Wong PK.  Immunol Lett. 2009 Jan 29. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19186189</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19177495?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Screening HIV-1 antigenic peptides as receptors for antibodies and CD4 in allosteric nanosensors.</a>  Ferraz RM, Rodríguez-Carmona E, Ferrer-Miralles N, Meyerhans A, Villaverde A. J Mol Recognit. 2009 Jan 28. [Epub ahead of print]</p>

    <p class="title">PMID: 19177495</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19109014?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and biological evaluation of 2-phenylquinolones targeted at Tat/TAR recognition.</a>  Manfroni G, Gatto B, Tabarrini O, Sabatini S, Cecchetti V, Giaretta G, Parolin C, Del Vecchio C, Calistri A, Palumbo M, Fravolini A.  Bioorg Med Chem Lett. 2009 Feb 1;19(3):714-7.</p>

    <p class="pmid">PMID: 19109014</p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19164520?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">FK506-binding protein (FKBP) partitions a modified HIV protease inhibitor into blood cells and prolongs its lifetime in vivo.</a>  Marinec PS, Chen L, Barr KJ, Mutz MW, Crabtree GR, Gestwicki JE.  Proc Natl Acad Sci U S A. 2009 Feb 3;106(5):1336-41. Epub 2009 Jan 21.</p>

    <p class="pmid">PMID: 19164520</p>

    <p class="title">15.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19153047?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">5&#39;-O-Aliphatic and amino acid ester prodrugs of (-)-beta-D-(2R,4R)-dioxolane-thymine (DOT): synthesis, anti-HIV activity, cytotoxicity and stability studies.</a>  Liang Y, Sharon A, Grier JP, Rapp KL, Schinazi RF, Chu CK.  Bioorg Med Chem. 2009 Feb 1;17(3):1404-9. Epub 2008 Nov 5.</p>

    <p class="pmid">PMID: 19153047</p> 

    <p class="title">16.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19135380?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Solution- and solid-phase synthesis and anti-HIV activity of maslinic acid derivatives containing amino acids and peptides.</a>  Parra A, Rivas F, Lopez PE, Garcia-Granados A, Martinez A, Albericio F, Marquez N, Muñoz E.  Bioorg Med Chem. 2009 Feb 1;17(3):1139-45. Epub 2008 Dec 25.</p>

    <p class="pmid">PMID: 19135380</p> 

    <p class="title">17.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18608742?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The strand transfer oligonucleotide inhibitors of HIV-integrase.</a>  Snásel J, Rosenberg I, Paces O, Pichová I.  J Enzyme Inhib Med Chem. 2009 Feb;24(1):241-6.</p>

    <p class="pmid">PMID: 18608742</p>

    <p class="memofmt2-2">ISI Web of Knowledge Citations:</p>

    <p class="title">18.   <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=Alerting&amp;UT=000262427000041&amp;SID=3DCOcjbNhNl3EPka9Af&amp;SrcAuth=Alerting&amp;mode=FullRecord&amp;customersID=Alerting&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_f">Synthesis of 2 &#39;-deoxy-4 &#39;-c-hydroxymethyl-4 &#39;-thioribonucleosides and their 2 &#39;,3 &#39;-dideoxy and 2 &#39;,3 &#39;-didehydro-2 &#39;,3 &#39;-dideoxy analogues.</a>  Nokami J. Mae M, Fukutake S, Ubuka T, Yamada M.  Heterocycles.  2008 Nov  1;76(2):1337-1360.</p>

    <p class="pmid">No Pubmed record.</p>

    <p class="title">19.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18597499?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and characterization of DNA adducts from the HIV reverse transcriptase inhibitor nevirapine.</a>  Antunes AM, Duarte MP, Santos PP, da Costa GG, Heinze TM, Beland FA, Marques MM.  Chem Res Toxicol. 2008 Jul;21(7):1443-56. Epub 2008 Jul 3.</p>

    <p class="pmid"><a href="http://www.ncbi.nlm.nih.gov/pubmed/18597499?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pubmed Link</a></p>

    <p class="pmid">PMID: 18597499</p>

    <p class="title">20.   <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=Alerting&amp;UT=000262234500231&amp;SID=3DCOcjbNhNl3EPka9Af&amp;SrcAuth=Alerting&amp;mode=FullRecord&amp;customersID=Alerting&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_f">6-Chloro-1-(3,5-dimethylphenylsulfonyl)-1H-benzimidazol-2(3H)-one.</a>  Meneghetti F, Bombieri G, Logoteta P, De Luca L.  Acta Crystallographica Section E-Structure Reports Online.  2009 Jan:65:O159-U2648.  Part 1. </p>

    <p class="pmid">No Pubmed record.</p> 

    <p class="title">21.   <a href="http://apps.isiknowledge.com/full_record.do?product=WOS&amp;search_mode=GeneralSearch&amp;qid=14&amp;SID=3DCOcjbNhNl3EPka9Af&amp;page=1&amp;doc=1">Molecular docking studies of dithionitrobenzoic acid and its related compounds to protein disulfide isomerase: computational screening of inhibitors to HIV-1 entry.</a>  Gowthaman U, Jayakanthan M, Sundar D.  BMC Bioinformatics. 2008 Dec 12;9 Suppl 12:S14.</p>

    <p class="pmid"><a href="http://www.ncbi.nlm.nih.gov/pubmed/19091013?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pubmed Link</a></p>

    <p class="pmid">PMID: 19091013</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
