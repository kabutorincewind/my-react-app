

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-10-25.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zwiCy/+6bfZj4N6MejaCXbJ+bNZ+3ZMUm1aoKGaElIPmDiLvxg2nkTl9lpknZuMSFlFDTFyZjJFXvfHfvgUlkb+q2KOB3Cx6oOp2dtw3tMatx8qejAI/QYUZPm4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7DC1D8C7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: October 12 - October 25, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22917809">The Antimicrobial, Antioxidative, Anti-inflammatory Activity and Cytotoxicity of Different Fractions of Four South African Bauhinia Species Used Traditionally to Treat Diarrhoea.</a> Ahmed, A.S., E.E. Elgorashi, N. Moodley, L.J. McGaw, V. Naidoo, and J.N. Eloff. Journal of Ethnopharmacology, 2012. 143(3): p. 826-839. PMID[22917809].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23072646">Analysis toward Innovative Herbal Antibacterial &amp; Antifungal Drugs.</a> Dhankhar, S., M. Kumar, S. Ruhil, M. Balhara, and A.K. Chhillar. Recent Patents on Anti-infective Drug Discovery, 2012. <b>[Epub ahead of print]</b>. PMID[23072646].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22877865">Antimicrobial Properties of Cyclodextrin-antiseptics-complexes Determined by Microplate Laser Nephelometry and ATP Bioluminescence Assay.</a> Finger, S., C. Wiegand, H.J. Buschmann, and U.C. Hipler. International Journal of Pharmaceutics, 2012. 436(1-2): p. 851-856. PMID[22877865].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23081773">Antifungal Effects of Citronella Oil against Aspergillus niger ATCC 16404.</a> Li, W.R., Q.S. Shi, Y.S. Ouyang, Y.B. Chen, and S.S. Duan. Applied Microbiology and Biotechnology, 2012. <b>[Epub ahead of print]</b>. PMID[23081773].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22995772">Structure-Activity Relationship Study of Nitrosopyrimidines Acting as Antifungal Agents.</a> Olivella, M., A. Marchal, M. Nogueras, A. Sanchez, M. Melguizo, M. Raimondi, S. Zacchino, F. Giannini, J. Cobo, and R.D. Enriz. Bioorganic &amp; Medicinal Chemistry, 2012. 20(20): p. 6109-6122. PMID[22995772].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23084595">Chlorhexidine Is a Highly Effective Topical Broad-spectrum Agent against Candida spp.</a> Salim, N., C. Moore, N. Silikas, J. Satterthwaite, and R. Rautemaa. International Journal of Antimicrobial Agents, 2012. <b>[Epub ahead of print]</b>. PMID[23084595].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012-102512.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23086442">Antimycobacterial and Antileishmanial Effects of Microfungi Isolated from Tropical Regions in Mexico.</a> Gamboa-Angulo, M., G.M. Molina-Salinas, M. Chan-Bacab, S.R. Peraza-Sanchez, G. Heredia, S.C. de la Rosa-Garcia, and M. Reyes-Estebanez. Parasitology Research, 2012. <b>[Epub ahead of print]</b>. PMID[23086442].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012_102512.</p>  
    <p> </p>
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22995771">Antitubercular Nitrofuran Isoxazolines with Improved Pharmacokinetic Properties.</a> Rakesh, D. Bruhn, D.B. Madhura, M. Maddox, R.B. Lee, A. Trivedi, L. Yang, M.S. Scherman, J.C. Gilliland, V. Gruppo, M.R. McNeil, A.J. Lenaerts, B. Meibohm, and R.E. Lee. Bioorganic &amp; Medicinal Chemistry, 2012. 20(20): p. 6063-6072. PMID[22995771].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012_102512.</p> 
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22967765">Synthesis and Evaluation of Novel 1,3,4-Oxadiazole Derivatives of Marine Bromopyrrole Alkaloids as Antimicrobial Agent.</a> Rane, R.A., S.D. Gutte, and N.U. Sahu. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(20): p. 6429-6432. PMID[22967765].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012_102512.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22967767">Allylic Thiocyanates as a New Class of Antitubercular Agents.</a> Silveira, G.P., M. Ferreira, L. Fernandes, G.C. Moraski, S. Cho, C. Hwang, S.G. Franzblau, and M.M. Sa. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(20): p. 6486-6489. PMID[22967767].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012_102512.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23069618">Antimalarial and Anticancer Activities of Artemisinin-Quinoline Hybrid-dimers and Pharmacokinetic Properties in Mice.</a> Lombard, M.C., D. N&#39;Da D, J.C. Breytenbach, N.I. Kolesnikova, C.T. Ba, S. Wein, J. Norman, P. Denti, H. Vial, and L. Wiesner. European Journal of Pharmaceutical Sciences, 2012. <b>[Epub ahead of print]</b>. PMID[23069618].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1012-102512.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307689100012">Antimicrobial Activity of New Synthesized [(Oxadiazolyl)methyl]phenytoin Derivatives.</a> Ali, O.M., W.A. El-Sayed, S.A. Eid, N.A.M. Abdelwahed, and A.A.H. Abdel-Rahman. Acta Poloniae Pharmaceutica, 2012. 69(4): p. 657-667. ISI[000307689100012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308511300010">Antimicrobial Activity of Methanol Extracts of Abietinella abietina, Neckera crispa, Platyhypnidium riparoides, Cratoneuron filicinum and Campylium protensum Mosses.</a> Bukvicki, D., M. Veljic, M. Sokovic, S. Grujic, and P.D. Marin. Archives of Biological Sciences, 2012. 64(3): p. 911-916. ISI[000308511300010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308458100014">Synthesis and Preliminary Evaluation of N-Acylhydrazone Compounds as Antibacterial and Antifungal Agents.</a> Cachiba, T.H., B.D. Carvalho, D.T. Carvalho, M. Cusinato, C.G. Prado, and A.L.T. Dias. Quimica Nova, 2012. 35(8): p. 1566-1569. ISI[WOS:000308458100014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307402000014">Inhibitory Effect of Some Spices Powder and Its Oils on Pathogenic Microorganisms in Liquid Media.</a> El-Kholie, E.M., M.A.T. Abdelreheem, and S.A. Khader. African Journal of Microbiology Research, 2012. 6(16): p. 3791-3796. ISI[000307402000014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p>   
    <p> </p>
    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308271900015">Structure and Antimycobacterial Activity of the Novel Organometallic Pd(C-bzan)(SCN)(dppp) Compound.</a> Ferreira, J.G., A. Stevanato, A.M. Santana, A.E. Mauro, A.V.G. Netto, R.C.G. Frem, F.R. Pavan, C.Q.F. Leite, and R.H.A. Santos. Inorganic Chemistry Communications, 2012. 23: p. 63-66. ISI[000308271900015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308562400014">Synthesis and Evaluation of N-Substituted Imidazole Derivatives for Antimicrobial Activity.</a> Gupta, N. and D.P. Pathak. Indian Journal of Pharmaceutical Sciences, 2011. 73(6): p. 674-U118. ISI[000308562400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307920400017">Synthesis, Spectroscopic, Antimicrobial and DNA Cleavage Studies of New Co(II), Ni(II), Cu(II), Cd(II), Zn(II) and Hg(II) Complexes with Naphthofuran-2-carbohydrazide Schiff Base.</a> Halli, M.B. and R.B. Sumathi. Journal of Molecular Structure, 2012. 1022: p. 130-138. ISI[000307920400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308697100008">Synthesis of Novel Azetidinone Derivatives as Antitubercular Agents.</a> Himaja, M., A. Karigar, M.V. Ramana, D. Munirajasekhar, and M.S. Sikarwar. Letters in Drug Design &amp; Discovery, 2012. 9(6): p. 611-617. ISI[000308697100008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308568300063">Molecular Target Validation, Antimicrobial Delivery, and Potential Treatment of Toxoplasma gondii Infections.</a> Lai, B.S., W.H. Witola, K. El Bissati, Y. Zhou, E. Mui, A. Fomovska, and R. McLeod. Proceedings of the National Academy of Sciences of the United States of America, 2012. 109(35): p. 14182-14187. ISI[000308565300063].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00030875850001">Synthesis of Substituted Thioureas and Their Sulfur Heterocyclic Systems of P-Amino Acid as Antimycobacterial Agents.</a> Makki, M., R.M. Abdel-Rahman, H.M. Faidallah, and K.A. Khan. Journal of Chemistry, 2013. ISI[000308758500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308959700013">Synthesis, Identification, and Antibacterial Activity of New Sulfonamide Nanoparticles.</a> Nabipour, H. Ieee Transactions on Nanobioscience, 2012. 11(3): p. 296-303. ISI[000308959700013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308411900007">In Silico Discovery and Virtual Screening of Multi-target Inhibitors for Proteins in Mycobacterium tuberculosis.</a> Speck-Planche, A., V.V. Kleandrova, F. Luan, and M. Cordeiro. Combinatorial Chemistry &amp; High Throughput Screening, 2012. 15(8): p. 666-673. ISI[000308411900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306950200133">Biochemical and Functional Analysis of Two Plasmodium falciparum Blood-Stage 6-Cys Proteins: P12 and P41.</a> Taechalertpaisarn, T., C. Crosnier, S.J. Bartholdson, A.N. Hodder, J. Thompson, L.Y. Bustamante, D.W. Wilson, P.R. Sanders, G.J. Wright, J.C. Rayner, A.F. Cowman, P.R. Gilson, and B.S. Crabb. PloS One, 2012. 7(7). ISI[000306950200133].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p> 
    <p> </p>
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308921500007">Medicinal and Culinary Herbs as Environmentally Safe Inhibitors of Dangerous Toxinogenic Plant and Human Fungal Pathogens.</a> Zabka, M., R. Pavela, and T. Sumikova. African Journal of Microbiology Research, 2012. 6(35): p. 6468-6475. ISI[000308921500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1012-102512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
