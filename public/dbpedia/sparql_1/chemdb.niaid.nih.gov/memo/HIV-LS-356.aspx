

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-356.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="aAYZNtVWT44RaoDftYmJF+/qSplHEyewTZb1TsP5PlqKvrU0Gm/YqaMFU8gJsgw0OuLAjKud06BdK9XXSoq5NRwjrrBO0K2EP8Fq9NisfmcvyMLI1M1XO4ys9Lk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="502B12AD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-356-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58840   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Parameters of inhibition of HIV-1 infection by small anionic microbicides</p>

    <p>          Vzorov, AN, Bozja, J, Dixon, DW, Marzilli, LG, and Compans, RW</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16949681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16949681&amp;dopt=abstract</a> </p><br />

    <p>2.     58841   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors, Part 7. Synthesis, Antiviral Activity, and 3D-QSAR Investigations of Novel 6-(1-Naphthoyl) HEPT Analogues</p>

    <p>          Ji, L, Chen, FE, Feng, XQ, De, Clercq E, Balzarini, J, and Pannecouque, C</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2006</b>.  54(9): 1248-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946529&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946529&amp;dopt=abstract</a> </p><br />

    <p>3.     58842   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Compounds from rose (Rosa rugosa) flowers with human immunodeficiency virus type 1 reverse transcriptase inhibitory activity</p>

    <p>          Fu, M, Ng, TB, Jiang, Y, Pi, ZF, Liu, ZK, Li, L, and Liu, F</p>

    <p>          J Pharm Pharmacol <b>2006</b>.  58(9): 1275-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945187&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945187&amp;dopt=abstract</a> </p><br />

    <p>4.     58843   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          HIV-1 reverse transcriptase (RT) genotypic patterns and treatment characteristics associated with the K65R RT mutation</p>

    <p>          Boucher, S, Recordon-Pinson, P, Ragnaud, JM, Dupon, M, Fleury, H, and Masquelier, B</p>

    <p>          HIV Med <b>2006</b>.  7(5): 294-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945074&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945074&amp;dopt=abstract</a> </p><br />

    <p>5.     58844   HIV-LS-356; EMBASE-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Perspectives on the development of acyclic nucleotide analogs as antiviral drugs: Special Issue To Honour Professor Erik De Clercq</p>

    <p>          Lee, William A and Martin, John C</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 254-259</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K714X4-1/2/8fd0eb4b6ff8e0005e4dd710dd0026dc">http://www.sciencedirect.com/science/article/B6T2H-4K714X4-1/2/8fd0eb4b6ff8e0005e4dd710dd0026dc</a> </p><br />
    <br clear="all">

    <p>6.     58845   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Anti-AIDS Agents 69. Moronic Acid and Other Triterpene Derivatives as Novel Potent Anti-HIV Agents</p>

    <p>          Yu, D, Sakurai, Y, Chen, CH, Chang, FR, Huang, L, Kashiwada, Y, and Lee, KH</p>

    <p>          J Med Chem <b>2006</b>.  49(18): 5462-5469</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16942019&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16942019&amp;dopt=abstract</a> </p><br />

    <p>7.     58846   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Inhibition of Multidrug-Resistant HIV-1 by Interference with Cellular S-adenosylmethionine Decarboxylase Activity</p>

    <p>          Schafer, B, Hauber, I, Bunk, A, Heukeshoven, J, Dusedau, A, Bevec, D, and Hauber, J</p>

    <p>          J Infect Dis <b>2006</b>.  194(6): 740-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16941339&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16941339&amp;dopt=abstract</a> </p><br />

    <p>8.     58847   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Novel function of prothymosin alpha as a potent inhibitor of human immunodeficiency virus type 1 gene expression in primary macrophages</p>

    <p>          Mosoian, A, Teixeira, A, High, AA, Christian, RE, Hunt, DF, Shabanowitz, J, Liu, X, and Klotman, M</p>

    <p>          J Virol <b>2006</b>.  80(18): 9200-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940531&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940531&amp;dopt=abstract</a> </p><br />

    <p>9.     58848   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Inhibition of Human Immunodeficiency Virus Type 1 Entry by a Binding Domain of Porphyromonas gingivalis Gingipain</p>

    <p>          Xie, H, Belogortseva, NI, Wu, J, Lai, WH, and Chen, CH</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(9): 3070-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940103&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940103&amp;dopt=abstract</a> </p><br />

    <p>10.   58849   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Chain-terminating dinucleoside tetraphosphates are substrates for DNA polymerization by HIV-1 reverse transcriptase with increased activity against thymidine analogue resistant mutants</p>

    <p>          Meyer, PR, Smith, AJ, Matsuura, SE, and Scott, WA</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940076&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   58850   HIV-LS-356; EMBASE-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Absence of detectable viremia in a perinatally HIV-1-infected teenager after discontinuation of antiretroviral therapy</p>

    <p>          Feeney, Margaret E, Tang, Yanhua, Rathod, Almas, Kneut, Catherine, and McIntosh, Kenneth</p>

    <p>          Journal of Allergy and Clinical Immunology <b>2006</b>.  118(2): 324-330</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WH4-4KB10YF-7/2/6882f6c48839968c2d0cdfc1ae7c196b">http://www.sciencedirect.com/science/article/B6WH4-4KB10YF-7/2/6882f6c48839968c2d0cdfc1ae7c196b</a> </p><br />

    <p>12.   58851   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Antiviral Activity, Pharmacokinetics, and Dose Response of the HIV-1 Integrase Inhibitor GS-9137 (JTK-303) in Treatment-Naive and Treatment-Experienced Patients</p>

    <p>          Dejesus, E, Berger, D, Markowitz, M, Cohen, C, Hawkins, T, Ruane, P, Elion, R, Farthing, C, Zhong, L, Cheng, AK, McColl, D, and Kearney, BP</p>

    <p>          J Acquir Immune Defic Syndr <b>2006</b>.  43(1): 1-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16936557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16936557&amp;dopt=abstract</a> </p><br />

    <p>13.   58852   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Biological Activities and 3D QSAR Studies of a Series of Delisea pulchra (cf. fimbriata) Derived Natural Products</p>

    <p>          Wright, AD, de Nys, R, Angerhofer, CK, Pezzuto, JM, and Gurrath, M</p>

    <p>          J Nat Prod <b>2006</b>.  69(8): 1180-1187</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16933872&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16933872&amp;dopt=abstract</a> </p><br />

    <p>14.   58853   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Optimization of pyrimidinyl- and triazinyl-amines as non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Thakur, VV, Kim, JT, Hamilton, AD, Bailey, CM, Domaoal, RA, Wang, L, Anderson, KS, and Jorgensen, WL</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16931015&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16931015&amp;dopt=abstract</a> </p><br />

    <p>15.   58854   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Expression of Nef Downregulates CXCR4, the Major Coreceptor of Human Immunodeficiency Virus, From the Surface of Target Cells and Thereby Enhances Resistance to Superinfection</p>

    <p>          Venzke, S, Michel, N, Allespach, I, Fackler, OT, and Keppler, OT</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16928758&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16928758&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   58855   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Bis-Tetrahydrofuran: a Privileged Ligand for Darunavir and a New Generation of HIV Protease Inhibitors That Combat Drug Resistance</p>

    <p>          Ghosh, AK, Ramu, Sridhar P, Kumaragurubaran, N, Koh, Y, Weber, IT, and Mitsuya, H</p>

    <p>          ChemMedChem <b>2006</b>.  1(9): 939-950</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16927344&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16927344&amp;dopt=abstract</a> </p><br />

    <p>17.   58856   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Anti-HIV-1 immunotoxin 3B3(Fv)-PE38: enhanced potency against clinical isolates in human PMBCs and macrophages, and negligible hepatotoxicity in macaques</p>

    <p>          Kennedy, PE, Bera, TK, Wang, QC, Gallo, M, Wagner, W, Lewis, MG, Berger, EA, and Pastan, I</p>

    <p>          J Leukoc Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16923920&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16923920&amp;dopt=abstract</a> </p><br />

    <p>18.   58857   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          A novel antiretroviral class (fusion inhibitors) in the management of HIV infection. Present features and future perspectives of enfuvirtide (T-20)</p>

    <p>          Manfredi, R and Sabbatani, S</p>

    <p>          Curr Med Chem <b>2006</b>.  13(20): 2369-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16918361&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16918361&amp;dopt=abstract</a> </p><br />

    <p>19.   58858   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Replacement of the metabolically labile methyl esters in the alkenyldiarylmethane series of non-nucleoside reverse transcriptase inhibitors with isoxazolone, isoxazole, oxazolone, or cyano substituents</p>

    <p>          Deng, BL, Hartman, TL, Buckheit, RW Jr , Pannecouque, C, De Clercq, E, and Cushman, M</p>

    <p>          J Med Chem <b>2006</b>.  49(17): 5316-5323</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913721&amp;dopt=abstract</a> </p><br />

    <p>20.   58859   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Biotinylated biphenyl ketone-containing 2,4-dioxobutanoic acids designed as HIV-1 integrase photoaffinity ligands</p>

    <p>          Zhao, XZ, Semenova, EA, Liao, C, Nicklaus, M, Pommier, Y, and Burke, TR Jr</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908168&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908168&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   58860   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Evaluation of a new fourth generation enzyme-linked immunosorbent assay, the LG HIV Ag-Ab Plus, with a combined HIV p24 antigen and anti-HIV-1/2/O screening test</p>

    <p>          Yeom, JS, Jun, G, Chang, Y, Sohn, MJ, Yoo, S, Kim, E, Ryu, SH, Kang, HJ, Kim, YA, Ahn, SY, Cha, JE, Youn, ST, and Park, JW</p>

    <p>          J Virol Methods <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908076&amp;dopt=abstract</a> </p><br />

    <p>22.   58861   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Biology of CCR5 and its role in HIV infection and treatment</p>

    <p>          Lederman, MM, Penn-Nicholson, A, Cho, M, and Mosier, D</p>

    <p>          JAMA <b>2006</b>.  296(7): 815-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16905787&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16905787&amp;dopt=abstract</a> </p><br />

    <p>23.   58862   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p><b>          Increased efficacy of HIV-1 neutralization by antibodies at low CCR5 surface concentration</b> </p>

    <p>          Choudhry, V, Zhang, MY, Harris, I, Sidorov, IA, Vu, B, Dimitrov, AS, Fouts, T, and Dimitrov, DS</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  348(3): 1107-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904645&amp;dopt=abstract</a> </p><br />

    <p>24.   58863   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Simple synthesis and anti-hiv activity of novel 3&#39;-vinyl branched apiosyl pyrimidine nucleosides</p>

    <p>          Oh, CH, Kim, JW, and Hong, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(8): 871-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901819&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901819&amp;dopt=abstract</a> </p><br />

    <p>25.   58864   HIV-LS-356; PUBMED-HIV-9/5/2006</p>

    <p class="memofmt1-2">          Unexpected Novel Binding Mode of Pyrrolidine-Based Aspartyl Protease Inhibitors: Design, Synthesis and Crystal Structure in Complex with HIV Protease</p>

    <p>          Specker, E, Bottcher, J, Brass, S, Heine, A, Lilie, H, Schoop, A, Muller, G, Griebenow, N, and Klebe, G</p>

    <p>          ChemMedChem <b>2006</b>.  1(1): 106-117</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16892342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16892342&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>26.   58865   HIV-LS-356; WOS-HIV-8/27/2006</p>

    <p class="memofmt1-2">          Variations of the P2 group in HIV-1 protease inhibitors containing a tertiary alcohol in the transition-state mimicking scaffold</p>

    <p>          Ekegren, JK, Gising, J, Wallberg, H, Larhed, M, Samuelsson, B, and Hallberg, A</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2006</b>.  4(16): 3040-3043, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239508100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239508100003</a> </p><br />

    <p>27.   58866   HIV-LS-356; WOS-HIV-8/27/2006</p>

    <p class="memofmt1-2">          Structure-activity relationships of [2 &#39;,5 &#39;-bis-O-(tert-butyldimethylsilyl)-beta-D-r bofuranosyl]-3 &#39;-spiro-5 &#39;&#39;-(4 &#39;&#39;-amino-1 &#39;&#39;,2 &#39;&#39;-oxathiole-2 &#39;&#39;,2 &#39;&#39;-dioxide) thymine derivatives as inhibitors of HIV-1 reverse transcriptase dimerization</p>

    <p>          Sluis-Cremer, N, Hamamouch, N, San, Felix A, Velazquez, S, Balzarini, J, and Camarasa, MJ</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(16): 4834-4841, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239459800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239459800007</a> </p><br />

    <p>28.   58867   HIV-LS-356; WOS-HIV-8/27/2006</p>

    <p class="memofmt1-2">          Synthesis and studies of novel homoveratryl based thiohydantoins as antibacterial as well as anti-HIV agents</p>

    <p>          Patel, RB, Desai, KR, and Chikhalia, KH</p>

    <p>          INDIAN JOURNAL OF CHEMISTRY SECTION B-ORGANIC CHEMISTRY INCLUDING MEDICINAL CHEMISTRY <b>2006</b>.  45 (7): 1716-1721, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239475200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239475200007</a> </p><br />

    <p>29.   58868   HIV-LS-356; WOS-HIV-8/27/2006</p>

    <p class="memofmt1-2">          Microwave-assisted synthesis of fluoroquinolones and their nucleosides as inhibitors of HIV integrase</p>

    <p>          Adams, MM, Bats, JW, Nikolaus, NV, Witvrouw, M, Debyser, Z, and Engels, JW</p>

    <p>          COLLECTION OF CZECHOSLOVAK CHEMICAL COMMUNICATIONS <b>2006</b>.  71(7): 978-990, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900005</a> </p><br />

    <p>30.   58869   HIV-LS-356; WOS-HIV-9/3/2006</p>

    <p class="memofmt1-2">          Synthesis and HPLC-purification of [Br-77]TMC125-R165335 (etravirine), a new anti-HIV drug of the DAPY-NNRTI class</p>

    <p>          De Spiegeleer, B, Dumont, F, Peremans, K, Burvenich, C, Van, Vooren L, Rosier, J, Baert, L, Wigerinck, P, and Slegers, G</p>

    <p>          JOURNAL OF LABELLED COMPOUNDS &amp; RADIOPHARMACEUTICALS <b>2006</b>.  49(8): 683-686, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239579500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239579500003</a> </p><br />

    <p>31.   58870   HIV-LS-356; WOS-HIV-9/3/2006</p>

    <p class="memofmt1-2">          Controversial approaches in HIV infection: Is there a role for lopinavir/ritonavir monotherapy?</p>

    <p>          Chan-Tack, KM and Edozien, A</p>

    <p>          INFECTIONS IN MEDICINE <b>2006</b>.  23(8): 342-+, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239774100001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239774100001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
