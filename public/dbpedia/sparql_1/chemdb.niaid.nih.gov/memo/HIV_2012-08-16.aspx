

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-08-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xMJZj3S6qx+Y0GVJDVmQmxw1Um5BZ/8dZ7OifQruBztxRFQ6m9yFT6lX+f9XZQY9YBFRQOd1/sHzgkN6YkX4gW41j9Jaztt6yYZcxESVat9yxEG7XJV8/0IOfKM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EAF2F0B3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 3 - August 16, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22872679">Sphingopeptides: Dihydrosphingosine-based Fusion Inhibitors against Wild-type and Enfuvirtide-resistant HIV-1.</a> Ashkenazi, A., M. Viard, L. Unger, R. Blumenthal, and Y. Shai. FASEB Journal, 2012. <b>[Epub ahead of print]</b>; PMID[22872679].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22866663">Selective Targeting of the Repressive Transcription Factors YY1 and cMyc to Disrupt Quiescent Human Immunodeficiency Viruses.</a> Barton, K.M. and D.M. Margolis. AIDS Research and Human Retroviruses, 2012. <b>[Epub ahead of print]</b>; PMID[22866663].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22879603">The M-T Hook Structure Is Critical for Design of HIV-1 Fusion Inhibitors.</a> Chong, H., X. Yao, J. Sun, Z. Qiu, M. Zhang, S. Waltersperger, M. Wang, S. Cui, and Y. He. The Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22879603].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22818973">Synthesis of Betulinic Acid Derivatives as Entry Inhibitors against HIV-1 and Bevirimat-resistant HIV-1 Variants.</a> Dang, Z., K. Qian, P. Ho, L. Zhu, K.H. Lee, L. Huang, and C.H. Chen. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(16): p. 5190-5194. PMID[22818973].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22871093">Synthesis of Novel Mannoside Glycolipid Conjugates for Inhibition of HIV-1 Trans-infection.</a> Dehuyser, L., E. Schaeffer, O. Chaloin, C.G. Mueller, R. Baati, and A. Wagner. Bioconjugate Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22871093].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22875968">A Quantitative Measurement of Antiviral Activity of anti-HIV-1 Drugs against SIV Infection: Dose Response Curve Slope Strongly Influences Class-specific Inhibitory Potential.</a> Deng, K., M.C. Zink, J.E. Clements, and R.F. Siliciano. Journal of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22875968].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22873630">The Application of Bioisosteres in Drug Design for Novel Drug Discovery: Focusing on Acid Protease Inhibitors.</a> Hamada, Y. and Y. Kiso. Expert Opinion on Drug Discovery, 2012. <b>[Epub ahead of print]</b>; PMID[22873630].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22885523">Inhibitory Effects of Chloroquine on the Activation of Plasmacytoid Dendritic Cells in SIVmac239-Infected Chinese Rhesus Macaques.</a> Ma, J.P., H.J. Xia, G.H. Zhang, J.B. Han, L.G. Zhang, and Y.T. Zheng. Cellular &amp; Molecular Immunology, 2012. <b>[Epub ahead of print]</b>; PMID[22885523].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22858844">3-Phenylcoumarins as Inhibitors of HIV-1 Replication.</a> Olmedo, D., R. Sancho, L.M. Bedoya, J.L. Lopez-Perez, E. Del Olmo, E. Munoz, J. Alcami, M.P. Gupta, and A. San Feliciano. Molecules, 2012. 17(8): p. 9245-9257. PMID[22858844].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22827702">Stereoselective Synthesis and Antiviral Activity of Methyl-substituted Cyclosal-pronucleotides.</a> Rios Morales, E.H., J. Balzarini, and C. Meier. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22827702].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22892890">CXCR4 Chemokine Receptor Antagonists: Nickel(II) Complexes of Configurationally Restricted Macrocycles.</a> Smith, R., D. Huskens, D. Daelemans, R.E. Mewis, C.D. Garcia, A.N. Cain, T.N. Freeman, C. Pannecouque, E.D. Clercq, D. Schols, T.J. Hubin, and S.J. Archibald. Dalton Transactions, 2012. <b>[Epub ahead of print]</b>; PMID[22892890].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22688327">A Ligand-based Approach for the in Silico Discovery of Multi-target Inhibitors for Proteins Associated with HIV Infection.</a> Speck-Planche, A., V.V. Kleandrova, F. Luan, and M.N. Cordeiro. Molecular BioSystems, 2012. 8(8): p. 2188-2196. PMID[22688327].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22856541">Design, Synthesis, and Preclinical Evaluations of Novel 4-Substituted 1,5-diarylanilines as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitor (NNRTI) Drug Candidates.</a> Sun, L.Q., L. Zhu, K. Qian, B. Qin, L. Huang, C.H. Chen, K.H. Lee, and L. Xie. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22856541].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22739361">Chemical Methods for Degradation of Target Proteins Using Designed Light-activatable Organic Molecules.</a> Tanimoto, S., D. Takahashi, and K. Toshima. Chemical Communications, 2012. 48(62): p. 7659-7671. PMID[22739361].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22878423">The Activity of the Integrase Inhibitor Dolutegravir against HIV-1 Variants Isolated from Raltegravir-treated Adults.</a> Underwood, M.R., B.A. Johns, A. Sato, J.N. Martin, S.G. Deeks, and T. Fujiwara. Journal of AcquiredImmune DeficiencySyndromes, 2012. <b>[Epub ahead of print]</b>; PMID[22878423].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22679024">Structural Basis of Potent and Broad HIV-1 Fusion Inhibitor Cp32m.</a> Yao, X., H. Chong, C. Zhang, Z. Qiu, B. Qin, R. Han, S. Waltersperger, M. Wang, Y. He, and S. Cui. Journal of Biological Chemistry, 2012. 287(32): p. 26618-26629. <b>[Epub ahead of print]</b>; PMID[22679024].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0803-081612.</p>

    <h2>ISI Web of Knowledge Citations</h2>
    
17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306312700022">Synthesis and Antimicrobial Activity of New 1,2,3-Triazolopyrimidine Derivatives and Their Glycoside and Acyclic Nucleoside Analogs.</a> El-Sayed, W.A., O.M. Ali, M.S. Faheem, I.F. Zied, and A.A.H. Abdel-Rahman. Journal of Heterocyclic Chemistry, 2012. 49(3): p. 607-612. ISI[000306312700022].


    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>

18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306312700003">Synthesis and Characterization Via Molecular Quantum Parameters of 2H-Thiazolo 3,2-a pyrimidine-3,5,7(6h)-trione.</a> Hamama, W.S., M.A. Ismail, M.S. Soliman, S. Shaaban, and H.H. Zoorob. Journal of Heterocyclic Chemistry, 2012. 49(3): p. 494-498. ISI[000306312700003].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306272100006">15-Deoxy-delta(12,14)-prostaglandin J(2) Inhibits Human Immunodeficiency Virus-1 Tat-induced Monocyte Chemoattractant Protein-1/CCL2 Production by Blocking the Extracellular Signal-regulated Kinase-1/2 Signaling Pathway Independently of Peroxisome Proliferator-activated Receptor-&#947; and Heme Oxygenase-1 in Rat Hippocampal Slices.</a> Kim, S.E., E.O. Lee, J.H. Yang, J.H.L. Kang, Y.H. Su, and Y.H. Chong. Journal of Neuroscience Research, 2012. 90(9): p. 1732-1742. ISI[000306272100006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>

20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306277000023">Highly Concise and Stereoselective Total Synthesis of (5R,7S)-Kurzilactone.</a> Mohapatra, D.K., P. Karthik, and J.S. Yadav. Helvetica Chimica Acta, 2012. 95(7): p. 1226-1230. ISI[000306277000023].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000305921300010">Cytotoxic and anti-HIV Phenanthroindolizidine Alkaloids from Cryptocarya chinensis.</a> Wu, T.S., C.R. Su, and K.H. Lee. Natural Product Communications, 2012. 7(6): p. 725-727. ISI[000305921300010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306247400020">Quantitative Structure-Activity Relationship (QSAR) on New Substituted Thiazol-2-yliedene-benzamides as Potential anti-HIV Agents.</a> Al-Masoudi, N.A., B. Salih, J. Jassim and A. Saeed. Journal of Computational and Theoretical Nanoscience, 2012. 9(5): p. 752-756. ISI[000306247400020].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000305990800018">Rapid Catalyst Identification for the Synthesis of the Pyrimidinone Core of HIV Integrase Inhibitors.</a> Bellomo, A., N. Celebi-Olcum, X.D. Bu, N. Rivera, R.T. Ruck, C.J. Welch, K.N. Houk, and S.D. Dreher. Angewandte Chemie, 2012. 51(28): p. 6912-6915. ISI[000305990800018].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000305981800005">Diterpenes from Marine Brown Alga Dictyota guineensis (Dictyotaceae, Phaeophyceae).</a> De-Paula, J.C., D.N. Cavalcanti, Y. Yoneshigue-Valentin, and V.L. Teixeira. Brazilian Journal of Pharmacognosy, 2012. 22(4): p. 736-740. ISI[000305981800005].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000305981800006">Inhibitory Effect of a Brazilian Marine Brown Alga Spatoglossum schroederi on Biological Activities of Lachesis muta Snake Venom.</a> Domingos, T.F.S., F.A. Ortiz-Ramirez, R.C. Villaca, D.N. Cavalcanti, E.F. Sanchez, V.L. Teixeira, and A.L. Fuly. Brazilian Journal of Pharmacognosy, 2012. 22(4): p. 741-747. ISI[000305981800006].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306151900027">Concise Synthesis of the anti-HIV Nucleoside EFdA.</a> Kageyama, M., T. Miyagi, M. Yoshida, T. Nagasawa, H. Ohrui and H. Kuwahara. Bioscience Biotechnology and Biochemistry, 2012. 76(6): p. 1219-1225. ISI[000306151900027].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306101300049">Synthesis and Structure-Activity Relationship Studies of 1,3-Disubstituted 2-propanols as BACE-1 Inhibitors.</a> Kumar, A.B., J.M. Anderson, A.L. Melendez and R. Manetsch. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4740-4744. ISI[000306101300049].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000305981800025">HIV-1 Reverse Transcriptase: A Potential Target for Marine Products.</a> Miceli, L.A., A.M.T. de Souza, C.R. Rodrigues, I.C.N.P. Paixao, V.L. Teixeira, and H.C. Castro. Brazilian Journal of Pharmacognosy, 2012. 22(4): p. 881-888. ISI[000305981800025].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>

29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306101300015">Synthesis of Nucleoside 5 &#39;-Boranophosphorothioate Derivatives Using an H-Boranophosphonate monoester as a Precursor.</a> Oka, N., Y. Takayama, K. Ando and T. Wada. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4571-4574. ISI[000306101300015].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>
    
30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306299600008">Antimicrobial, anti-TB, Anticancer and anti-HIV Evaluation of New S-Triazine-based Heterocycles.</a> Patel, R.V., P. Kumari, D.P. Rajani, C. Pannecouque, E. De Clercq, and K.H. Chikhalia. Future Medicinal Chemistry, 2012. 4(9): p. 1053-1065. ISI[000306299600008].


    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <p> </p>

31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306171900011">Solid-phase Microwave Assisted Synthesis of Curcumin Analogs.</a> Pore, D., R. Alli, A.S.C. Prabhakar, R.R. Alavala, U. Kulandaivelu and S. Boyapati. Letters in Organic Chemistry, 2012. 9(6): p. 447-450. ISI[000306171900011].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>

    <h2>Latent HIV Reactivation</h2>

32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306335000010">Reactivation of Latent HIV-1 by a Wide Variety of Butyric Acid-producing Bacteria.</a> Imai, K., K. Yamada, M. Tamura, K. Ochiai and T. Okamoto. Cellular and Molecular Life Sciences, 2012. 69(15): p. 2583-2592. ISI[000306335000010].

    <p class="plaintext"><b>[WOS]</b>. HIV_0803-081612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
