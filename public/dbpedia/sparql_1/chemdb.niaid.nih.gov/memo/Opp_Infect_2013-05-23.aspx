

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-05-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="idtANhfrNNQ5jEPn6xZKM7T6MT71xORKTykotXC0keQ6YYharx/GMMECO8RxMjDKFt2cK9VgLTDDmQOKDqI0SNooMSh2QaygvHVrOxDlyZWrc7srRE4rQraeOl0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4BB4A101" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: May 10 - May 23, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23684665">Differences in Composition of Honey Samples and Their Impact on the Antimicrobial Activities against Drug Multiresistant Bacteria and Pathogenic Fungi.</a> Al-Waili, N., A. Al Ghamdi, M.J. Ansari, Y. Al-Attar, A. Osman, and K. Salom. Archives of Medical Research, 2013. <b>[Epub ahead of print]</b>. PMID[23684665].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23587426">Synthesis and Antimicrobial Activity of Novel Amphiphilic Aromatic Amino Alcohols.</a> de Almeida, A.M., T. Nascimento, B.S. Ferreira, P.P. de Castro, V.L. Silva, C.G. Diniz, and M. Le Hyaric. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(10): p. 2883-2887. PMID[23587426].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23657806">Synthesis and Biological Evaluation of Some Novel Urea and Thiourea Derivatives of Isoxazolo[4,5-d]pyridazine and Structurally Related Thiazolo[4,5-d]pyridazine as Antimicrobial Agents.</a> Faidallah, H.M., S.A. Rostom, S.A. Basaif, M.S. Makki, and K.A. Khan. Archives of Pharmacal Research, 2013. <b>[Epub ahead of print]</b>. PMID[23657806].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23684720">Antimicrobial Activity of Schinus lentiscifolius (Anacardiaceae).</a> Gehrke, I.T., A.T. Neto, M. Pedroso, C.P. Mostardeiro, I.B. Manica Da Cruz, U.F. Silva, V. Ilha, Dalcol, II, and A.F. Morel. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23684720].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23689717">Identification and Mechanism of Action of the Plant Defensin NaD1 as a New Member of the Antifungal Drug Arsenal against Candida albicans.</a> Hayes, B.M., M.R. Bleackley, J.L. Wiltshire, M.A. Anderson, A. Traven, and N.L. van der Weerden. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23689717].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23689724">Potentiation of Azole Antifungals by 2-Adamantanamine.</a> Lafleur, M.D., L. Sun, I. Lister, J. Keating, A. Nantel, L. Long, M. Ghannoum, J. North, R.E. Lee, K. Coleman, T. Dahl, and K. Lewis. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23689724].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23688235">Antimicrobial Activity and Cytotoxicity of Triterpenes Isolated from Leaves of Maytenus undata (Celastraceae).</a> Mokoka, T.A., L.J. McGaw, L.K. Mdee, V.P. Bagla, E.O. Iwalewa, and J.N. Eloff. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 111. PMID[23688235].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23681942">Synthesis and Biological Evaluation of Pyrazoline Derivatives Bearing an Indole Moiety as New Antimicrobial Agents.</a> Ozdemir, A., M.D. Altintop, Z.A. Kaplancikli, G. Turan-Zitouni, H. Karaca, and Y. Tunali. Archiv der Pharmazie, 2013. <b>[Epub ahead of print]</b>. PMID[23681942].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23689719">High-throughput Screening of a Collection of Known Pharmacologically Active Small Compounds for the Identification of Candida albicans Biofilm Inhibitors.</a> Siles, S.A., A. Srinivasan, C.G. Pierce, J.L. Lopez-Ribot, and A.K. Ramasubramanian. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23689719].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23676473">Effects of Chirality on the Antifungal Potency of Methylated Succinimides Obtained by Aspergillus fumigatus Biotransformations. Comparison with Racemic Ones.</a> Sortino, M., A. Postigo, and S. Zacchino. Molecules, 2013. 18(5): p. 5669-5683. PMID[23676473].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23684728">Anti-aspergillus Activity of Green Coffee 5-O-Caffeoyl quinic acid and Its Alkyl Esters.</a> Suarez-Quiroz, M.L., A. Alonso Campos, G. Valerio Alfaro, O. Gonzalez-Rios, P. Villeneuve, and M.C. Figueroa-Espinoza. Microbial Pathogenesis, 2013. <b>[Epub ahead of print]</b>. PMID[23684728].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23686966">Antifungal Attributes of Siderophore Produced by the Pseudomonas aeruginosa JAS-25.</a> Sulochana, M.B., S.Y. Jayachandra, S.K. Kumar, and A. Dayanand. Journal of Basic Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23686966].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23661699">Structural and Biochemical Characterization of Compounds Inhibiting Mycobacterium tuberculosis PanK.</a> Bjorkelid, C., T. Bergfors, A.K. Raichurkar, K. Mukherjee, K. Malolanarasimhan, B. Bandodkar, and T.A. Jones. The Journal of Biological Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23661699].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23680126">Antiprotozoal and Antimycobacterial Activities of Persea americana Seeds.</a> Jimenez-Arellanes, A., J. Luna-Herrera, R. Ruiz-Nicolas, J. Cornejo-Garrido, A. Tapia, and L. Yepez-Mulia. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 109. PMID[23680126].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23676086">Identification of M. tuberculosis Thioredoxin Reductase Inhibitors Based on High-throughput Docking Using Constraints.</a> Koch, O., T. Jaeger, K. Heller, P.C. Khandavalli, J. Pretzel, K. Becker, L. Flohe, and P.M. Selzer. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23676086].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23680030">Inhibition of Mycobacterial Alanine Racemase Activity and Growth by Thiadiazolidinones.</a> Lee, Y., S. Mootien, C. Shoen, M. Destefano, P. Cirillo, O.A. Asojo, K.R. Yeung, M. Ledizet, M.H. Cynamon, P.A. Aristoff, R.A. Koski, P.A. Kaplan, and K.G. Anthony. Biochemical Pharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23680030].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23462211">Antitubercular Diterpenoids from Vitex trifolia.</a> Tiwari, N., J. Thakur, D. Saikia, and M.M. Gupta. Phytomedicine : International Journal of Phytotherapy and Phytopharmacology, 2013. 20(7): p. 605-610. PMID[23462211].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23684005">In Vitro Activities of PNU-100480 and Linezolid against Drug-susceptible and Drug-resistant Mycobacterium tuberculosis Isolates.</a> Yip, P.C., K.M. Kam, E.T. Lam, R.C. Chan, and W.W. Yew. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>. PMID[23684005].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23570789">Substituted Imidazopyridazines Are Potent and Selective Inhibitors of Plasmodium falciparum Calcium-dependent Protein Kinase 1 (PfCDPK1).</a> Chapman, T.M., S.A. Osborne, N. Bouloc, J.M. Large, C. Wallace, K. Birchall, K.H. Ansell, H.M. Jones, D. Taylor, B. Clough, J.L. Green, and A.A. Holder. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(10): p. 3064-3069. PMID[23570789].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23680158">Antiplasmodial and Anti-inflammatory Activities of Canthium henriquesianum (K. Schum), a Plant Used in Traditional Medicine in Burkina Faso.</a> Ilboudo, D.P., N. Basilico, S. Parapini, Y. Corbett, S. D&#39;Alessandro, M. Dell&#39;agli, P. Coghi, S.D. Karou, R. Sawadogo, C. Gnoula, J. Simpore, J.B. Nikiema, D. Monti, E. Bosisio, and D. Taramelli. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23680158].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23689711">Broad Spectrum Antimalarial Activity of Peptido sulfonyl fluorides, a New Class of Proteasome Inhibitors.</a> Tschan, S., A.J. Brouwer, P.R. Werkhoven, A.M. Jonker, L. Wagner, S. Knittel, M.N. Aminake, G. Pradel, F. Joanny, R.M. Liskamp, and B. Mordmuller. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23689711].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23663567">In Vitro Evaluation of beta-Carboline Alkaloids as Potential Anti-toxoplasma Agents.</a> Alomar, M.L., F.A. Rasse-Suriani, A. Ganuza, V.M. Coceres, F.M. Cabrerizo, and S.O. Angel. BMC Research Notes, 2013. 6: p. 193. PMID[23663567].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23668331">Optimization of Benzoxazole-based Inhibitors of Cryptosporidium parvum Inosine 5&#39;-Monophosphate Dehydrogenase.</a> Gorla, S.K., M. Kavitha, M. Zhang, J.E. Chin, X. Liu, B. Striepen, M. Makowska-Grzyska, Y. Kim, A. Joachimiak, L. Hedstrom, and G.D. Cuny. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23668331].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23675474">Regulation of IDO Activity by Oxygen Supply: Inhibitory Effects on Antimicrobial and Immunoregulatory Functions.</a> Schmidt, S.K., S. Ebel, E. Keil, C. Woite, J.F. Ernst, A.E. Benzin, J. Rupp, and W. Daubener. Plos One, 2013. 8(5): p. e63301. PMID[23675474].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0510-052313.</p> 

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317183000010">Evaluation of Antimycobacterial Activity of a Sulphonamide Derivative.</a> Agertt, V.A., L.L. Marques, P.C. Bonez, T.V. Dalmolin, G.N. Manzoni de Oliveira, and M.M. Anraku de Campos. Tuberculosis, 2013. 93(3): p. 318-321. ISI[000317183000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316912800024">Coumarinyl Pyrazole Derivatives of Inh: Promising Antimycobacterial Agents.</a> Aragade, P., M. Palkar, P. Ronad, and D. Satyanarayana. Medicinal Chemistry Research, 2013. 22(5): p. 2279-2283. ISI[000316912800024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316611700079">Synthesis and Characterization of Celecoxib Derivatives as Possible Anti-inflammatory, Analgesic, Antioxidant, Anticancer and anti-HCV Agents.</a> Kucukguzel, S.G., I. Coskun, S. Aydin, G. Aktay, S. Gursoy, O. Cevik, O.B. Ozakpinar, D. Ozsavci, A. Sener, N. Kaushik-Basu, A. Basu, and T.T. Talele. Molecules, 2013. 18(3): p. 3595-3614. ISI[000316611700079].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317305200006">Successful Establishment and Evaluation of a New Animal Model for Studying the Hepatitis B Virus YVDD Mutant.</a> Ma, Y.-X., Z.-W. Song, X. Teng, L.-J. Fu, M.-L. Hao, S.-J. Chen, W.-Z. Xu, and H.-X. Gu. Archives of Virology, 2013. 158(4): p. 785-791. ISI[000317305200006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317187400006">Antimicrobial Activity and Composition Profile of Grape (Vitis vinifera) Pomace Extracts Obtained by Supercritical Fluids.</a> Oliveira, D.A., A.A. Salvador, A. Smania, Jr., E.F.A. Smania, M. Maraschin, and S.R.S. Ferreira. Journal of Biotechnology, 2013. 164(3): p. 423-432. ISI[000317187400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317153100033">Anti-Hepatitis B Virus Activity of alpha-DDB-DU, a Novel Nucleoside Analogue, in Vitro and in Vivo.</a> Zheng, L.-Y., L.-M. Zang, Q.-H. Yang, W.-Q. Yu, X.-Z. Fang, Y.-H. Zhang, X.-J. Zhao, N. Wan, Y.-T. Zhang, Q.-D. Wang, and J.-B. Chang. European Journal of Pharmacology, 2013. 702(1-3): p. 258-263. ISI[000317153100033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317543300007">In Vitro Effects of Ivermectin and Sulphadiazine on Toxoplasma gondii.</a> Bilgin, M., T. Yildirim, and M. Hokelek. Balkan Medical Journal, 2013. 30(1): p. 19-22. ISI[000317543300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317333900040">Evaluation of anti-HCV Activity and SAR Study of (+)-Lycoricidine through Targeting of Host Heat-stress Cognate 70 (Hsc70).</a> Chen, D.-Z., J.-D. Jiang, K.-Q. Zhang, H.-P. He, Y.-T. Di, Y. Zhang, J.-Y. Cai, L. Wang, S.-L. Li, P. Yi, Z.-G. Peng, and X.-J. Hao. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(9): p. 2679-2682. ISI[000317333900040].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317333900055">Design, Synthesis and Structure-Activity Relationships of 3,5-Diaryl-1H-pyrazoles as Inhibitors of Arylamine N-Acetyltransferase.</a> Fullam, E., J. Talbot, A. Abuhammed, I. Westwood, S.G. Davies, A.J. Russell, and E. Sim. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(9): p. 2759-2764. ISI[000317333900055].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317467600048">Activities of Systemically Administered Echinocandins against in Vivo Mature Candida Albicans Biofilms Developed in a Rat Subcutaneous Model.</a> Kucharkova, S., N. Sharma, I. Spriet, J. Maertens, P. Van Dijck, and K. Lagrou. Antimicrobial Agents and Chemotherapy, 2013. 57(5): p. 2365-2368. ISI[000317467600048].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317709400001">A Cyclic Peptide Mimic of an RNA Recognition Motif of Human La Protein Is a Potent Inhibitor of Hepatitis C Virus.</a> Manna, A.K., A. Kumar, U. Ray, S. Das, G. Basu, and S. Roy. Antiviral Research, 2013. 97(3): p. 223-226. ISI[000317709400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317403200017">Design, Synthesis and Anti-tuberculosis Activity of 1-Adamantyl-3-Heteroaryl Ureas with Improved in Vitro Pharmacokinetic Properties.</a> North, E.J., M.S. Scherman, D.F. Bruhn, J.S. Scarborough, M.M. Maddox, V. Jones, A. Grzegorzewicz, L. Yang, T. Hess, C. Morisseau, M. Jackson, M.R. McNeil, and R.E. Lee. Bioorganic &amp; Medicinal Chemistry, 2013. 21(9): p. 2587-2599. ISI[000317403200017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317333900021">Discovery of a Novel Series of Non-nucleoside Thumb Pocket 2 HCV NS5B Polymerase Inhibitors.</a> Stammers, T.A., R. Coulombe, J. Rancourt, B. Thavonekham, G. Fazal, S. Goulet, A. Jakalian, D. Wernic, Y. Tsantrizos, M.-A. Poupart, M. Boes, G. McKercher, L. Thauvette, G. Kukolj, and P.L. Beaulieu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(9): p. 2585-2589. ISI[000317333900021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317522900002">Synthesis and Evaluation as Antitubercular Agents of 5-Arylethenyl and 5-(Hetero)aryl-3-isoxazolecarboxylate.</a> Vergelli, C., A. Cilibrizzi, L. Crocetti, A. Graziano, V. Dal Piaz, B. Wan, Y. Wang, S. Franzblau, and M.P. Giovannoni. Drug Development Research, 2013. 74(3): p. 162-172. ISI[000317522900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317490300008">Anticandidal Activity and Biocompatibility of a Rechargeable Antifungal Denture Material.</a> Villar, C.C., A.L. Lin, Z. Cao, X.R. Zhao, L.A. Wu, S. Chen, Y. Sun, and C.K. Yeh. Oral Diseases, 2013. 19(3): p. 287-295. ISI[000317490300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0510-052313.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
