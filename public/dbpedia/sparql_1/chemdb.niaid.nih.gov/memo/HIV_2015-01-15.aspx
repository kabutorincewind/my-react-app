

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-01-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MuEqVFvdCswyYftypL9Hm2cO8PY02acLZ9qsf92c85jL8NTQyXevyF6n5ISJqwnujuN6b6C/6FGMtepbcPzTcLT2CMS6V3+v114fV18zcGfeaEtGF9dioxU2RNQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="70075F2C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 2 - January 15, 2015</h1>

    <p class="memofmt2-1">PubMed Citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24811313">Polymers Fight HIV: Potent (Pro)Drugs Identified through Parallel Automated Synthesis.</a> Zuwala, K., A.A. Smith, A. Postma, C. Guerrero-Sanchez, P. Ruiz-Sanchis, J. Melchjorsen, M. Tolstrup, and A.N. Zelikin. Advanced Healthcare Materials, 2015. 4(1): p. 46-50. PMID[24811313].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0102-011515.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25403229">Designed Transcription Activator-like Effector Proteins Efficiently Induced the Expression of Latent HIV-1 in Latently Infected Cells.</a> Wang, X., P. Wang, Z. Fu, H. Ji, X. Qu, H. Zeng, X. Zhu, J. Deng, P. Lu, S. Zha, Z. Song, and H. Zhu. AIDS Research and Human Retroviruses, 2015. 31(1): p. 98-106. PMID[25403229].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0102-011515.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25398876">Evaluation of anti-HIV-1 Mutagenic Nucleoside Analogues.</a> Vivet-Boudou, V., C. Isel, Y. El Safadi, R.P. Smyth, G. Laumond, C. Moog, J.C. Paillart, and R. Marquet. Journal of Biological Chemistry, 2015. 290(1): p. 371-383. PMID[25398876].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0102-011515.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25576356">Inhibition of HIV-1 Entry by the Tricyclic Coumarin GUT-70 through the Modification of Membrane Fluidity.</a> Matsuda, K., S. Hattori, R. Kariya, Y. Komizu, E. Kudo, H. Goto, M. Taura, R. Ueoka, S. Kimura, and S. Okada. Biochemical and Biophysical Research Communications, 2015. PMID[25576356].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0102-011515.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25482445">Isolation and Characterization of 2&#39;-F-RNA Aptamers against Whole HIV-1 Subtype C Envelope Pseudovirus.</a> London, G.M., B.M. Mayosi, and M. Khati. Biochemical and Biophysical Research Communications, 2015. 456(1): p. 428-433. PMID[25482445].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0102-011515.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25523431">Design, Synthesis, and Biological Evaluation of Novel 2H-Pyran-2-one Derivatives as Potential HIV-1 Reverse Transcriptase Inhibitors.</a> Defant, A., I. Mancini, R. Tomazzolli, and J. Balzarini. Archiv der Pharmazie, 2015. 348(1): p. 23-33. PMID[25523431].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0102-011515.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000346369900005">Carbocyclodipeptides as Modified Nucleosides: Synthesis and anti-HIV Activities.</a> Chhikara, B.S., M.S. Rao, V.K. Rao, A. Kumar, K.W. Buckheit, R.W. Buckheit, and K. Parang. Canadian Journal of Chemistry, 2014. 92(12): p. 1145-1149. ISI[000346369900005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0102-011515.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">8. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141218&amp;CC=WO&amp;NR=2014200880A1&amp;KC=A1">Preparation of Fused Tricyclic Heterocyclic Compounds as HIV Integrase Inhibitors.</a> Embrey, M.W., K. Babaoglu, A. Walji, P. Coleman, and J.S. Wai. Patent. 2014. 2014-US41464 2014200880: 76pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0102-011515.</p>

    <br />

    <p class="plaintext">9. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141218&amp;CC=WO&amp;NR=2014198834A1&amp;KC=A1">Peptides with Antagonistic Activities against Natural CXC-chemokine Receptor 4 Mediated HIV-1 NL4-3 Infection and Other Diseases.</a> Forssmann, W.-G., F. Kirchhoff, J. Muench, and L. Staendker. Patent. 2014. 2014-EP62252 2014198834: 39pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0102-011515.</p>

    <br />

    <p class="plaintext">10. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141231&amp;CC=WO&amp;NR=2014206336A1&amp;KC=A1">A HIV-1 Fusion Inhibitor with Long Half-life.</a> Jiang, S., L. Lu, W. Xu, R. Wang, and J. Huang. Patent. 2014. 2014-CN80953 2014206336: 29pp ; Chemical Indexing Equivalent to 159:600595 (CN).</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0102-011515.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
