

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-317.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="m4qJdYp8C0krwMZHWPNxJF8q4rAknKVEGnjQVucLG9GguMp08fERhazOFhu1IMuWis3nyRG1clxBu6j9jUtu528SzAbsegC6TaWL4dNdtC3ecsKXa97QQummrv4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="009B850F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-317-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44932   HIV-LS-317; SCIFINDER-HIV-3/1/2005</p>

    <p class="memofmt1-2">          A novel HIV-1 antiviral high throughput screening approach for the discovery of HIV-1 inhibitors</p>

    <p>          Blair, Wade S, Isaacson, Jason, Li, Xinqiang, Cao, Joan, Peng, Qinghai, Kong, George FZ, and Patick, Amy K</p>

    <p>          Antiviral Research <b>2005</b>.  65(2): 107-116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.         44933   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Activity profiles of deoxynucleoside kinases and 5&#39;-nucleotidases in cultured adipocytes and myoblastic cells: insights into mitochondrial toxicity of nucleoside analogs</p>

    <p>          Rylova, SN, Albertioni, F, Flygh, G, and Eriksson, S</p>

    <p>          Biochem Pharmacol <b>2005</b>.  69(6): 951-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15748706&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15748706&amp;dopt=abstract</a> </p><br />

    <p>3.         44934   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Artificial zinc-finger fusions targeting Sp1 binding sites and TAR potently repress transcription and replication of HIV-1</p>

    <p>          Kim, YS, Kim, JM, Jung, DL, Kang, JE, Lee, S, Kim, JS, Seol, W, Shin, HC, Kwon, HS, Lint, CV, Hernandez, N, and Hur, MW</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743774&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743774&amp;dopt=abstract</a> </p><br />

    <p>4.         44935   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Synthesis of Phosphonated Carbocyclic 2&#39;-Oxa-3&#39;-aza-nucleosides: Novel Inhibitors of Reverse Transcriptase</p>

    <p>          Chiacchio, U, Balestrieri, E, Macchi, B, Iannazzo, D, Piperno, A, Rescifina, A, Romeo, R, Saglimbeni, M, Sciortino, MT, Valveri, V, Mastino, A, and Romeo, G</p>

    <p>          J Med Chem <b>2005</b>.  48(5): 1389-1394</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743182&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743182&amp;dopt=abstract</a> </p><br />

    <p>5.         44936   HIV-LS-317; SCIFINDER-HIV-3/1/2005</p>

    <p class="memofmt1-2">          Synthesis and evaluation of CCR5 antagonists containing modified 4-piperidinyl-2-phenyl-1-(phenylsulfonylamino)-butane</p>

    <p>          Shah, Shrenik K, Chen, Natalie, Guthikonda, Ravindra N, Mills, Sander G, Malkowitz, Lorraine, Springer, Martin S, Gould, Sandra L, DeMartino, Julie A, Carella, Anthony, Carver, Gwen, Holmes, Karen, Schleif, William A, Danzeisen, Renee, Hazuda, Daria, Kessler, Joseph, Lineberger, Janet, Miller, Michael, Emini, Emilio A, and MacCoss, Malcolm</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(4): 977-982</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.         44937   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Synergistic anti-viral effect of oxanosine and ddI against human immunodeficiency virus</p>

    <p>          Nakamura, M, Ogawa, T, Yokono, A, Nakamori, S, Ohno, T, and Umezawa, K</p>

    <p>          Biomed Pharmacother <b>2005</b>.  59(1-2): 47-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15740935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15740935&amp;dopt=abstract</a> </p><br />

    <p>7.         44938   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Design of novel bioisosteres of beta-diketo acid inhibitors of HIV-1 integrase</p>

    <p>          Sechi, M, Sannia, L, Carta, F, Palomba, M, Dallocchio, R, Dessi, A, Derudas, M, Zawahir, Z, and Neamati, N</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(1): 41-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739621&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739621&amp;dopt=abstract</a> </p><br />

    <p>8.         44939   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Inhibition of Phosphorolysis Catalyzed by HIV-1 Reverse Transcriptase Is Responsible for the Synergy Found in Combinations of 3&#39;-Azido-3&#39;-deoxythymidine with Nonnucleoside Inhibitors</p>

    <p>          Cruchaga, C, Odriozola, L, Andreola, M, Tarrago-Litvak, L, and Martinez-Irujo, JJ</p>

    <p>          Biochemistry <b>2005</b>.  44(9): 3535-3546</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15736963&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15736963&amp;dopt=abstract</a> </p><br />

    <p>9.         44940   HIV-LS-317; SCIFINDER-HIV-3/1/2005</p>

    <p class="memofmt1-2">          Trimeric envelope protein gp41 helical conformation stabilization by peptide mimetics inhibits membrane fusion-mediated cell entry by HIV-1</p>

    <p>          Tam, James P, Yu, Qitao, Lu, Yi-An, and Yang, Jin-Long</p>

    <p>          PATENT:  WO <b>2005007831</b>  ISSUE DATE:  20050127</p>

    <p>          APPLICATION: 2004  PP: 71 pp.</p>

    <p>          ASSIGNEE:  (Vanderbilt University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.       44941   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          In Vitro Activity of Structurally Diverse Nucleoside Analogs against Human Immunodeficiency Virus Type 1 with the K65R Mutation in Reverse Transcriptase</p>

    <p>          Parikh, UM, Koontz, DL, Chu, CK, Schinazi, RF, and Mellors, JW</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 1139-44</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728915&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728915&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44942   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Crystal structure of a cross-reaction complex between an anti-HIV-1 protease antibody and an HIV-2 protease peptide</p>

    <p>          Rezacova, P, Brynda, J, Lescar, J, Fabry, M, Horejsi, M, Sieglova, I, Sedlacek, J, and Bentley, GA</p>

    <p>          J Struct Biol <b>2005</b>.  149(3): 332-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15721587&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15721587&amp;dopt=abstract</a> </p><br />

    <p>12.       44943   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          HIV-1-Associated Uracil DNA Glycosylase Activity Controls dUTP Misincorporation in Viral DNA and Is Essential to the HIV-1 Life Cycle</p>

    <p>          Priet, S, Gros, N, Navarro, JM, Boretto, J, Canard, B, Querat, G, and Sire, J</p>

    <p>          Mol Cell <b>2005</b>.  17(4): 479-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15721252&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15721252&amp;dopt=abstract</a> </p><br />

    <p>13.       44944   HIV-LS-317; SCIFINDER-HIV-3/1/2005</p>

    <p class="memofmt1-2">          Drug resistance mutations in the nucleotide binding pocket of human immunodeficiency virus type 1 reverse transcriptase differentially affect the phosphorolysis-dependent primer unblocking activity in the presence of stavudine and zidovudine and its inhibition by efavirenz</p>

    <p>          Crespan, Emmanuele, Locatelli, Giada A, Cancio, Reynel, Huebscher, Ulrich, Spadari, Silvio, and Maga, Giovanni</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(1): 342-349</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.       44945   HIV-LS-317; SCIFINDER-HIV-3/1/2005</p>

    <p class="memofmt1-2">          Novel anti-viral agents based upon derivatives of the aromatic heterocycle phenanthridine</p>

    <p>          Tor, Yitzhak and Luedtke, Nathan</p>

    <p>          PATENT:  WO <b>2005016343</b>  ISSUE DATE:  20050224</p>

    <p>          APPLICATION: 2004  PP: 58 pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.       44946   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Isolation of trichogin, an antifungal protein from fresh fruiting bodies of the edible mushroom Tricholoma giganteum</p>

    <p>          Guo, Y, Wang, H, and Ng, TB</p>

    <p>          Peptides <b>2005</b>.  26(4): 575-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752570&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752570&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.       44947   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Challenges for the clinical development of new nucleoside reverse transcriptase inhibitors for HIV infection</p>

    <p>          Wainberg, MA, Sawyer, JP, Montaner, JS, Murphy, RL, Kuritzkes, DR, and Raffi, F</p>

    <p>          Antivir Ther <b>2005</b>.  10(1): 13-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15751760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15751760&amp;dopt=abstract</a> </p><br />

    <p>17.       44948   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Selective inhibition of HIV-1 reverse transcriptase-associated ribonuclease H activity by hydroxylated tropolones</p>

    <p>          Budihas, SR, Gorshkova, I, Gaidamakov, S, Wamiru, A, Bona, MK, Parniak, MA, Crouch, RJ, McMahon, JB, Beutler, JA, and Le, Grice SF</p>

    <p>          Nucleic Acids Res <b>2005</b>.  33(4): 1249-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15741178&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15741178&amp;dopt=abstract</a> </p><br />

    <p>18.       44949   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          A new pinoresinol-type lignan from Ligularia kanaitizensis</p>

    <p>          Li, YS, Wang, ZT, Zhang, M, Luo, SD, and Chen, JJ</p>

    <p>          Nat Prod Res <b>2005</b>.  19(2): 125-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715255&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715255&amp;dopt=abstract</a> </p><br />

    <p>19.       44950   HIV-LS-317; PUBMED-HIV-3/9/2005</p>

    <p class="memofmt1-2">          Quantitative structure-activity relationship by CoMFA for cyclic urea and nonpeptide-cyclic cyanoguanidine derivatives on wild type and mutant HIV-1 protease</p>

    <p>          Avram, S, Bologa, C, and Flonta, ML</p>

    <p>          J Mol Model (Online) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15714296&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15714296&amp;dopt=abstract</a> </p><br />

    <p>20.       44951   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          The trans-Golgi network-associated human ubiquitin-protein ligase POSH is essential for HIV Woe 1 production</p>

    <p>          Alroy, I, Tuvia, S, Greener, T, Gordon, D, Barr, HM, Taglicht, D, Mandil-Levin, R, Ben-Avraham, D, Konforty, D, Nir, L, Levius, O, Bicoviski, V, Dori, M, Cohen, S, Yaar, L, Erez, O, Propheta-Meiran, O, Koskas, M, Caspi-Bachar, E, Alchanati, I, Sela-Brown, A, Moskowitz, H, Tessmer, U, Schubert, U, and Reiss, Y</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2005</b>.  102(5): 1478-1483, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226877300042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226877300042</a> </p><br />
    <br clear="all">

    <p>21.       44952   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Peptide nucleic acids as epigenetic inhibitors of HIV-1</p>

    <p>          Sei, S</p>

    <p>          LETTERS IN PEPTIDE SCIENCE <b>2003</b>.  10(3-4): 269-286, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226731200010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226731200010</a> </p><br />

    <p>22.       44953   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          A synthetic peptide fragment derived from RANTES is a potent inhibitor of HIV-1 infectivity despite a surprising lack of CCR5 receptor affinity</p>

    <p>          Ramnarine, E, DeVico, AL, and Vigil-Cruz, SC</p>

    <p>          LETTERS IN PEPTIDE SCIENCE <b>2003</b>.  10(5-6): 637-643, 7</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226731400033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226731400033</a> </p><br />

    <p>23.       44954   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Potent anti-R5 human immunodeficiency virus type 1 effects of a CCR5 antagonist, AK602/ONO4128/GW873140, in a novel human peripheral blood mononuclear cell nonobese diabetic-SCID, interleukin-2 receptor gamma-chain-knocked-out AIDS mouse model</p>

    <p>          Nakata, H, Maeda, K, Miyakawa, T, Shibayama, S, Matsuo, M, Takaoka, Y, Ito, M, Koyanagi, Y, and Mitsuya, H</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(4): 2087-2096, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226772100012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226772100012</a> </p><br />

    <p>24.       44955   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Kinetics of human immunodeficiency virus type 1 decay following entry into resting CD4(+) T cells</p>

    <p>          Zhou, Y, Zhang, H, Siliciano, JD, and Siliciano, RF</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(4): 2199-2210, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226772100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226772100023</a> </p><br />

    <p>25.       44956   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Fungal phenalenones inhibit HIV-1 integrase</p>

    <p>          Shiomi, K, Matsui, R, Isozaki, M, Chiba, H, Sugai, T, Yamaguchi, Y, Masuma, R, Tomoda, H, Chiba, T, Yan, H, Kitamura, Y, Sugiura, W, Omura, S, and Tanaka, H</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2005</b>.  58(1): 65-68, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226818200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226818200008</a> </p><br />

    <p>26.       44957   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Identification of diverse microbial metabolites as potent inhibitors of HIV-1 tat transactivation</p>

    <p>          Jayasuriya, H, Zink, DL, Polishook, JD, Bills, GF, Dombrowski, AW, Genilloud, O, Pelaez, FF, Herranz, L, Quamina, D, Lingham, RB, Danzeizen, R, Graham, PL, Tomassini, JE, and Singh, SB</p>

    <p>          CHEMISTRY &amp; BIODIVERSITY <b>2005</b>.  2(1): 112-122, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226963500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226963500008</a> </p><br />
    <br clear="all">

    <p>27.       44958   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Stereoselective synthesis and antiviral activity of novel 4 &#39;(alpha)-hydroxymethyl and 6 &#39;(alpha)-methyl dually branched carbocyclic nucleosides</p>

    <p>          Kim, JW, Choi, BG, and Hong, JH</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY <b>2004</b>.  25(12): 1812-1816, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226854800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226854800011</a> </p><br />

    <p>28.       44959   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Enantiomerical excess determination, purification and biological evaluation of (3S) and (3R) alpha,beta-butenolide analogues of isobenzofuranone</p>

    <p>          Lipka, E, Vaccher, MP, Vaccher, C, and Len, C</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(3): 501-504, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700002</a> </p><br />

    <p>29.       44960   HIV-LS-317; WOS-HIV-2/27/2005</p>

    <p class="memofmt1-2">          Insight into the mechanism of a peptide inhibitor of HIV reverse transcriptase dimerization</p>

    <p>          Depollier, J, Hourdou, ML, Aldrian-Herrada, G, Rothwell, P, Restle, T, and Divita, G</p>

    <p>          BIOCHEMISTRY <b>2005</b>.  44(6): 1909-1918, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226969400016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226969400016</a> </p><br />

    <p>30.       44961   HIV-LS-317; WOS-HIV-3/6/2005</p>

    <p class="memofmt1-2">          Novel [2 &#39;,5 &#39;-bis-O-(ter -butyldimethylsilyl)-beta-D-ribofuranosyl]-3 &#39;-spiro-5 &#39;&#39;-(4 &#39;&#39;-amino-1 &#39;&#39;,2 &#39;&#39;-oxathiole-2 &#39;&#39;,2 &#39;&#39;-dioxide) derivatives with anti-HIV-1 and anti-human-cytomegalovirus activity</p>

    <p>          de Castro, S,  Lobaton, E, Perez-Perez, MJ, San-Felix, A, Cordeiro, A, Andrei, G, Snoeck, R, De Clercq, E, Balzarini, J, Camarasa, MJ, and Velazquez, S</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(4): 1158-1168, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227115500026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227115500026</a> </p><br />

    <p>31.       44962   HIV-LS-317; WOS-HIV-3/6/2005</p>

    <p class="memofmt1-2">          High-throughput screening method of inhibitors that block the interaction between 2 helical regions of HIV-1 gp41</p>

    <p>          Jin, BS, Lee, WK, Ahn, K, Lee, MK, and Yu, YG</p>

    <p>          JOURNAL OF BIOMOLECULAR SCREENING <b>2005</b>.  10(1): 13-19, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227050000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227050000002</a> </p><br />

    <p>32.       44963   HIV-LS-317; WOS-HIV-3/6/2005</p>

    <p class="memofmt1-2">          Defensins: Natural anti-HIV peptides</p>

    <p>          Chang, TL and Klotman, ME</p>

    <p>          AIDS REVIEWS <b>2004</b>.  6(3): 161-168, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227043100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227043100005</a> </p><br />

    <p>33.       44964   HIV-LS-317; WOS-HIV-3/6/2005</p>

    <p class="memofmt1-2">          Mechanism and inhibition of human immunodeficiency virus type 1 reverse transcriptase (HIV-1 RT) by an imidazolidine nucleotide analog</p>

    <p>          Kerr, SG, Dhareshwar, SS, Froimowitz, M, and Kalman, TI</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U174-U175, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712800717">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712800717</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
