

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-08-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pF6t3/ehhOpHCshX4Qlt0ITxGtE/7ZmVxdeuZtqFV7rjLhSQjWE3PTQiM0UNYMxvaAC8Ir7JSIpfD6+RXDQ4PQlr1VbkmpgsqTVegkFbYMF2TA9L3w/ftAY/sYU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BB4ABEAB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">August 5 - August 18, 2011</p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21827337">Inhibition of Hepatitis B Virus and Human Immunodeficiency Virus (HIV-1) Replication by Warscewiczia coccinea (Vahl) Kl. (Rubiaceae) Ethanol Extract.</a> Quintero, A., R. Fabbro, M. Maillo, M. Barrios, M.B. Milano, A. Fernandez, B. Williams, F. Michelangeli, H.R. Rangel, and F.H. Pujol. Natural Product Research, 2011. <b>[Epub ahead of print]</b>; PMID[21827337].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21823575">Swerilactones L-O, Secoiridoids with C(12) and C(13) Skeletons from Swertia mileensis.</a> Geng, C.A., X.M. Zhang, Y.B. Ma, J. Luo, and J.J. Chen. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21823575].</p>

    <p class="NoSpacing">[PubMed]. OV_0805-081811.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21840715">Novel HCV NS5B Polymerase Inhibitors: Discovery of Indole 2-carboxylic acids with C3-Heterocycles.</a> Anilkumar, G.N., C.A. Lesburg, O. Selyutin, S.B. Rosenblum, Q. Zeng, Y. Jiang, T.Y. Chan, H. Pu, H. Vaccaro, L. Wang, F. Bennett, K.X. Chen, J. Duca, S. Gavalas, Y. Huang, P. Pinto, M. Sannigrahi, F. Velazquez, S. Venkatraman, B. Vibulbhan, S. Agrawal, N. Butkiewicz, B. Feld, E. Ferrari, Z. He, C.K. Jiang, R.E. Palermo, P. McMonagle, H.C. Huang, N.Y. Shih, G. Njoroge, and J.A. Kozlowski. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21840715].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21837753">The Green Tea Polyphenol Epigallocatechin-3-gallate (EGCG) Inhibits Hepatitis C Virus (HCV) Entry.</a> Ciesek, S., T. von Hahn, C.C. Colpitts, L.M. Schang, M. Friesland, J. Steinmann, M.P. Manns, M. Ott, H. Wedemeyer, P. Meuleman, T. Pietschmann, and E. Steinmann. Hepatology (Baltimore, Md.), 2011. <b>[Epub ahead of print]</b>; PMID[21837753].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21821416">Inhibition of the Helicase Activity of the HCV NS3 Protein by Symmetrical Dimeric Bis-benzimidazoles.</a> Tunitskaya, V.L., A.V. Mukovnya, A.A. Ivanov, A.V. Gromyko, A.V. Ivanov, S.A. Streltsov, A.L. Zhuze, and S.N. Kochetkov. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21821416].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Feline Immunodeficiency Virus</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21813188">Restriction of the Felid Lentiviruses by a Synthetic Feline TRIM5-CypA Fusion.</a> Dietrich, I., W.A. McEwan, M.J. Hosie, and B.J. Willett. Veterinary Immunology and Immunopathology, 2011. <b>[Epub ahead of print]</b>; PMID[21813188].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">HCMV</p> 

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21843554">The Unique Antiviral Activity of Artesunate is Broadly Effective against Human Cytomegaloviruses Including Therapy-resistant Mutants.</a> Chou, S., G. Marousek, S. Auerochs, T. Stamminger, J. Milbradt, and M. Marschall. Antiviral Research, 2011. <b>[Epub ahead of print]</b>; PMID[21843554].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21812420">Tyrosine-Based 1-(S)-[3-Hydroxy-2-(phosphonomethoxy)propyl]cytosine and -adenine ((S)-HPMPC and (S)-HPMPA) Prodrugs: Synthesis, Stability, Antiviral Activity, and in Vivo Transport Studies.</a> Zakharova, V.M., M. Serpi, I.S. Krylov, L.W. Peterson, J.M. Breitenbach, K.Z. Borysko, J.C. Drach, M. Collins, J.M. Hilfinger, B.A. Kashemirov, and C.E. McKenna. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21812420].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes Simplex virus</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21827957">Antiviral Effects of Blackberry Extract against Herpes Simplex Virus Type 1.</a> Danaher, R.J., C. Wang, J. Dai, R.J. Mumper, and C.S. Miller. Oral Surgery, Oral Medicine, Oral Pathology, Oral Radiology, and Endodontics, 2011. 112(3): p. e31-35; PMID[21827957].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21822410">Antiviral Activities of Sulfated Polysaccharides Isolated from Sphaerococcus coronopifolius (Rhodophytha, Gigartinales) and Boergeseniella thuyoides (Rhodophyta, Ceramiales).</a> Bouhlal, R., C. Haslin, J.C. Chermann, S. Colliec-Jouault, C. Sinquin, G. Simon, S. Cerantola, H. Riadi, and N. Bourgougnon. Marine drugs, 2011. 9(7): p. 1187-1209; PMID[21822410].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0805-081811. </p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293320100055">Antioxidant, Antibacterial, and Antiviral Effects of Lactuca sativa Extracts.</a> Edziri, H.L., M.A. Smach, S. Ammar, M.A. Mahjoub, Z. Mighri, M. Aouni, and M. Mastouri. Industrial Crops and Products, 2011. 34(1): p. 1182-1185; ISI[000293320100055].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293320600008">Acridine Derivatives as anti-BVDV Agents.</a> Tonelli, M., G. Vettoretti, B. Tasso, F. Novelli, V. Boido, F. Sparatore, B. Busonera, A. Ouhtit, P. Farci, S. Blois, G. Giliberti, and P. La Colla. Antiviral Research, 2011. 91(2): p. 133-141; ISI[000293320600008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292913400022">Synthesis and Antiviral Evaluation of 9-(S)- 3-Alkoxy-2-(phosphonomethoxy)propyl nucleoside alkoxyalkyl esters: Inhibitors of Hepatitis C Virus and HIV-1 Replication.</a> Valiaeva, N., D.L. Wyles, R.T. Schooley, J.B. Hwu, J.R. Beadle, M.N. Prichard, and K.Y. Hostetler. Bioorganic &amp; Medicinal Chemistry, 2011. 19(15): p. 4616-4625; ISI[000292913400022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0805-081811.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
