

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1YSbhuu2IxIRBCDXge5J4F7PM8bMdMPYbiIJx9Q0fhb+1RdxduyVJDenOvaIzUqfAagiJ7QB23V0AltUgouF7kHDgk5zxEdJaktdQYnAgsGM3Bc0rchoDjNN34Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="455FFB1E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: July, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000379345300052">A Small Molecule, Which Competes with MAdCAM-1, Activates Integrin alpha(4)beta(7) and Fails to Prevent Mucosal Transmission of SHIV-SF162P3.</a> Arrode-Bruses, G., D. Goode, K. Kleinbeck, J. Wilk, I. Frank, S. Byrareddy, J. Arthos, B. Grasperge, J. Blanchard, T. Zydowsky, A. Gettie, and E. Martinelli. Plos Pathogens, 2016. 12(6): e1005720. ISI[000379345300052].
    <br />
    <b>[WOS]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27137461">HIV-Inhibitory Michellamine-type Dimeric Naphthylisoquinoline Alkaloids from the Central African Liana Ancistrocladus congolensis.</a> Bringmann, G., C. Steinert, D. Feineis, V. Mudogo, J. Betzin, and C. Scheller. Phytochemistry, 2016. 128: p. 71-81. PMID[27137461].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27437081">Discovery of MK-8718, an HIV Protease Inhibitor Containing a Novel Morpholine Aspartate Binding Group.</a> Bungard, C.J., P.D. Williams, J.E. Ballard, D.J. Bennett, C. Beaulieu, C. Bahnck-Teets, S.S. Carroll, R.K. Chang, D.C. Dubost, J.F. Fay, T.L. Diamond, T.J. Greshock, L. Hao, M.K. Holloway, P.J. Felock, J.J. Gesell, H.P. Su, J.J. Manikowski, D.J. McKay, M. Miller, X. Min, C. Molinaro, O.M. Moradei, P.G. Nantermet, C. Nadeau, R.I. Sanchez, T. Satyanarayana, W.D. Shipe, S.K. Singh, V.L. Truong, S. Vijayasaradhi, C.M. Wiscount, J.P. Vacca, S.N. Crane, and J.A. McCauley. ACS Medicinal Chemistry Letters, 2016. 7(7): p. 702-707. PMID[27437081]. PMC4948015[PMC4948015].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27124676">Synthesis and in Vitro anti-HIV-1 Activity of a Series of N-Arylsulfonyl-3-propionylindoles.</a> Che, Z.P., Y.E. Tian, Z.J. Hu, Y.W. Chen, S.M. Liu, and G.Q. Chen. Zeitschrift fur Naturforschung Section C-a Journal of Biosciences, 2016. 71(5-6): p. 105-109. PMID[27124676].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27058352">Development of Synthetic Light-chain Antibodies as Novel and Potent HIV Fusion Inhibitors.</a> Cunha-Santos, C., T.N. Figueira, P. Borrego, S.S. Oliveira, C. Rocha, A. Couto, C. Cantante, Q. Santos-Costa, J.M. Azevedo-Pereira, C.M. Fontes, N. Taveira, F. Aires-Da-Silva, M.A. Castanho, A.S. Veiga, and J. Goncalves. AIDS, 2016. 30(11): p. 1691-1701. PMID[27058352].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27122585">Colony-stimulating Factor 1 Receptor Antagonists Sensitize Human Immunodeficiency Virus Type 1-Infected Macrophages to Trail-mediated Killing.</a> Cunyat, F., J.N. Rainho, B. West, L. Swainson, J.M. McCune, and M. Stevenson. Journal of Virology, 2016. 90(14): p. 6255-6262. PMID[27122585]. PMCID[PMC4936142].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27225230">The Discovery of Allyltyrosine Based Tripeptides as Selective Inhibitors of the HIV-1 Integrase Strand-transfer Reaction.</a> Dalton, N., C.P. Gordon, T.P. Boyle, N. Vandegraaf, J. Deadman, D.I. Rhodes, J.A. Coates, S.G. Pyne, P.A. Keller, and J.B. Bremner. Organic &amp; Biomolecular Chemistry, 2016. 14(25): p. 6010-6023. PMID[27225230].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27244595">Triple Activity of Lamivudine Releasing Sulfonated Polymers against HIV-1.</a> Danial, M., A.H. Andersen, K. Zuwala, S. Cosson, C.F. Riber, A.A. Smith, M. Tolstrup, G. Moad, A.N. Zelikin, and A. Postma. Molecular Pharmaceutics, 2016. 13(7): p. 2397-2410. PMID[27244595].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27389367">Probing Lipophilic Adamantyl Group as the P1-ligand for HIV-1 Protease Inhibitors: Design, Synthesis, Protein X-Ray Structural Studies, and Biological Evaluation.</a> Ghosh, A.K., H.L. Osswald, K. Glauninger, J. Agniswamy, Y.F. Wang, H. Hayashi, M. Aoki, I.T. Weber, and H. Mitsuya. Journal of Medicinal Chemistry, 2016. 59(14): p. 6826-6837. PMID[27389367].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27310139">Prothymosin-alpha Variants Elicit anti-HIV-1 Response via TLR4 Dependent and Independent Pathways.</a> Gusella, G.L., A. Teixeira, J. Aberg, V.N. Uversky, and A. Mosoian. Plos One, 2016. 11(6): e0156486. PMID[27310139]. PMCID[PMC4910978].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26593063">High-mannose Specific Lectin and Its Recombinants from a Carrageenophyta Kappaphycus alvarezii represent a potent anti-HIV Activity through High-affinity binding to the Viral Envelope Glycoprotein gp120.</a> Hirayama, M., H. Shibata, K. Imamura, T. Sakaguchi, and K. Hori. Marine Biotechnology, 2016. 18(1): p. 144-160. PMID[26593063].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27381288">Shutdown of HIV-1 Transcription in T Cells by Nullbasic, a Mutant Tat Protein.</a> Jin, H., D. Li, H. Sivakumaran, M. Lor, L. Rustanti, N. Cloonan, S. Wani, and D. Harrich. mBio, 2016. 7(4). PMID[27381288]. PMCID[PMC4958243].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27181351">Toll-interacting Protein Inhibits HIV-1 Infection and Regulates Viral Latency.</a> Li, C., W.D. Kuang, D. Qu, and J.H. Wang. Biochemical and Biophysical Research Communications, 2016. 475(2): p. 161-168. PMID[27181351].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27405089">Iron (II) Supramolecular Helicates Interfere with the HIV-1 Tat-Tar RNA Interaction Critical for Viral Replication.</a> Malina, J., M.J. Hannon, and V. Brabec. Scientific Reports, 2016. 6(29674): 6pp. PMID[27405089]. PMCID[PMC4940744].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27154517">Chemically Sulfated Natural Galactomannans with Specific Antiviral and Anticoagulant Activities.</a> Muschin, T., D. Budragchaa, T. Kanamoto, H. Nakashima, K. Ichiyama, N. Yamamoto, H. Shuqin, and T. Yoshida. International Journal of Biological Macromolecules, 2016. 89: p. 415-420. PMID[27154517].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27295094">Vitamin D3 Inhibits TNFalpha-induced Latent HIV Reactivation in J-LAT Cells.</a> Nunnari, G., P. Fagone, F. Lazzara, A. Longo, D. Cambria, G. Di Stefano, M. Palumbo, L. Malaguarnera, and M. Di Rosa. Molecular and Cellular Biochemistry, 2016. 418(1-2): p. 49-57. PMID[27295094].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27105027">Design, Synthesis and anti-HIV Activity of Novel Quinoxaline Derivatives.</a> Patel, S.B., B.D. Patel, C. Pannecouque, and H.G. Bhatt. European Journal of Medicinal Chemistry, 2016. 117: p. 230-240. PMID[27105027].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27399760">A Truncated Nef Peptide from SIVcpz Inhibits the Production of HIV-1 Infectious Progeny.</a> Sabino Cunha, M., T. Lima Sampaio, B.M. Peterlin, and L. Jesus da Costa. Viruses, 2016. 8(7): E189 . PMID[27399760].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27381603">Synergism of Alkaline Extract of the Leaves of Sasa senanensis Rehder and Antiviral Agents.</a> Sakagami, H., K. Fukuchi, T. Kanamoto, S. Terakubo, H. Nakashima, T. Natori, M. Suguro-Kitajima, H. Oizumi, T. Yasui, and T. Oizumi. In Vivo, 2016. 30(4): p. 421-426. PMID[27381603].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27438728">Efficient Inhibition of HIV Replication in the Gastrointestinal and Female Reproductive Tracts of Humanized BLT Mice by EFdA.</a> Shanmugasundaram, U., M. Kovarova, P.T. Ho, N. Schramm, A. Wahl, M.A. Parniak, and J.V. Garcia. Plos One, 2016. 11(7): p. e0159517. PMID[27438728].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27092410">The Design of 8-Hydroxyquinoline Tetracyclic Lactams as HIV-1 Integrase Strand Transfer Inhibitors.</a> Velthuisen, E.J., B.A. Johns, D.P. Temelkoff, K.W. Brown, and S.C. Danehower. European Journal of Medicinal Chemistry, 2016. 117: p. 99-112. PMID[27092410].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27450669">Abolishing HIV-1 Infectivity Using a Polypurine Tract-specific G-quadruplex-forming Oligonucleotide.</a> Voges, M., C. Schneider, M. Sinn, J.S. Hartig, R. Reimer, J. Hauber, and K. Moelling. BMC Infectious Diseases, 2016. 16(1): p. 358. PMID[27450669]. PMCID[PMC4957839].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27283261">3-Hydroxypyrimidine-2,4-dione-5-N-benzylcarboxamides Potently Inhibit HIV-1 Integrase and RNase H.</a> Wu, B., J. Tang, D.J. Wilson, A.D. Huber, M.C. Casey, J. Ji, J. Kankanala, J. Xie, S.G. Sarafianos, and Z. Wang. Journal of Medicinal Chemistry, 2016. 59(13): p. 6136-6148. PMID[27283261]. PMCID[PMC4945466].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27469256">Molecular Cloning and anti-HIV-1 Activities of APOBEC3s from Northern Pig-tailed Macaques (Macaca leonina).</a> Zhang, X.L., J.H. Song, W. Pang, and Y.T. Zheng. Dongwuxue Yanjiu, 2016. 37(4): p. 246-251. PMID[27469256].
    <br />
    <b>[PubMed]</b>. HIV_07_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">25. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160623&amp;CC=US&amp;NR=2016176870A1&amp;KC=A1">Preparation of polycyclic-Carbamoylpyridone Compounds and Their Pharmaceutical Use.</a> Bacon, E.M., Z.R. Cai, J.J. Cottell, M. Ji, H. Jin, S.E. Lazerwith, P.A. Morganelli, and H.-J. Pyun. Patent. 2016. 2015-14977347 20160176870: 101pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160630&amp;CC=WO&amp;NR=2016105532A1&amp;KC=A1">Preparation of Fused Pyrimidines as HIV Reverse Transcriptase Inhibitors Useful in Treatment and Prevention of HIV Infection.</a> Jansa, P., M. Kvasnica, and R.L. Mackman. Patent. 2016. 2015-US308 2016105532: 79pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160630&amp;CC=WO&amp;NR=2016105534A1&amp;KC=A1">Preparation of Isoquinoline Compounds as HIV Reverse Transcriptase Inhibitors Useful in Treatment and Prevention of HIV Infection.</a> Jansa, P., R.L. Mackman, Y.E. Hu, and E. Lansdon. Patent. 2016. 2015-US310 2016105534: 81pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_07_2016.</p>

    <br />

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160630&amp;CC=WO&amp;NR=2016105564A1&amp;KC=A1">Preparation of Quinazoline Derivatives for the Treatment of HIV Infection.</a> Jansa, P., T. Simon, E. Lansdon, Y.E. Hu, O. Baszczynski, M. Dejmek, and R.L. Mackman. Patent. 2016. 2015-US460 2016105564: 185pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_07_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
