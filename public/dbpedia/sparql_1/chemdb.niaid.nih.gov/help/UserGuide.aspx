

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - User Guide</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="UserGuide.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5ovYUhaON4r/QPaP6WtpK3HTgqUqHRrBZyoyVi6/GSEDaR+3butF1wkKG+gzUNioxWUSs8BOtV28o/z0lvdVeLU5FWsUfQOo24DpFQnDbJa+yvkRNny0jE9pln8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BD0E8F53" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="UserGuide.aspx">User Guide</a></li>
		    		<li><a href="../memo/Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="../memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="userguide">
        <h1>User Guide</h1>
    
        <h2>Quick Tips</h2>
	    <ul>
        	<li>On our home page, you will find a <b>Simple Search</b> option that allows you to perform a quick text search through the database for chemical or literature entries by keyword.</li>
        	<li>From the home page or the left-hand sidebar, you can navigate through multiple <b>Advanced Search</b> options, allowing you to tailor your search according to chemical properties, biological properties or information about a specific literature reference.</li>
    	    <li>Please be patient, as searches can take up to a minute. You can keep track of your query&#39;s progress by watching the status bar in your browser window.</li>
        	<li>Your search session will time out after 20 minutes of inactivity.</li>
        	<li>We cannot provide samples of any compound described in this database. In some cases, samples may be available through the <a href="http://dtp.nci.nih.gov/branches/dscb/repo_open.html" target="_blank">NCI/DTP Open Chemical Repository</a>.</li>
        </ul>

        <h2>Simple text search</h2>
        <ul>
        	<li>The home page of ChemDB features a <b>Simple Search</b> option.</li>
    	    <li>After you enter a keyword, <b>Simple Search</b> searches all fields in our database for your keyword(s).</li>
        	<li><b>Simple</b> <b>Search</b> is not case sensitive.</li>
        	<li>You can select to perform your <b>Simple Search</b> in either our reference library or compound database.</li>
	    </ul>
	    <br />
        <p>For example, if you type <i>reverse transcriptase</i> into the <b>Simple Search</b> box on our home page, leaving the <b>Compounds</b> option selected, and hitting <b>Search</b>, every field in the database will be searched for the term <i>reverse transcriptase</i><b>.</b> Your results will include entries with the words <i>reverse transcriptase</i> in the &quot;target&quot; or &quot;enzyme&quot; fields<b>.</b> You can perform another search with the same keywords, <i>reverse transcriptase</i>, and select the <b>Literature</b> option before hitting <b>Search</b>. In <i>this</i> case, your results will come from our literature database and will include all articles with the term <i>reverse transcriptase</i> in the title.</p>

        <h2>Advanced search options</h2>
	    <ul>
    	    <li>The <b>Advanced Search</b> page can be accessed through either the chemical or biological search portals on the left-hand sidebar of the home page.</li>
        	<li>The only difference between the chemical and biological portals is the initial configuration of the search page.
        	    <ul>
    	    	    <li>The <b>Chemical</b> portal is configured with expanded <b>Compound Data</b> and <b>Structure Search</b> sections.</li>
        		    <li>The <b>Biological</b> portal is configured with expanded <b>Cell Based Anti-HIV Assay Data</b>, <b>Anti-HIV Enzyme Inhibition Data</b> and <b>Anti-Opportunistic Infection Data</b> sections.</li>
        	    </ul>
        	</li>
    	    <li>If you wish to search on both biological and chemical parameters, you can expand any closed section yourself by clicking on the red icon to the left of the section heading.</li>
    	    <li>If you minimize a section, any search criteria you entered into that section will be lost.</li>
        </ul>

        <h3>Boolean</h3>
        <p>Our <b>Advanced Search</b> tool gives you the option of executing a Boolean search. Boolean search lets you look for combinations of up to three criteria using Boolean connector terms (i.e., AND, OR, AND NOT).</p>
        <ul>
    	    <li><b>You must select at least two search criteria before attempting to specify a Boolean connector term.</b></li>
        	<li>After you specify your search criteria, you can modify the search logic by choosing your fields and operators from the drop down lists in the Boolean Search section.</li>
        	<li>By default, AND is used for all search criteria combinations.</li>
        	<li>If you select more than three search criteria, AND will automatically be used for all connector terms.</li>
        	<li>Here is a detailed explanation of Boolean connector terms:</li>
        </ul>
        <div id="boolconnectors" class="nopadding">
            <table border="0">
                <tr><td width="75%"><b>AND:</b> Narrows search and retrieves records containing all of the fields it separates. In the example above, the AND operator returns only those results which contain both Search Field #1 and Search Field #2.</td><td><img width="175" height="95" src="../images/andimage.jpg" alt="AND"></img></td></tr>
                <tr><td width="75%"><b>AND NOT</b>: Use NOT to exclude a term. Records with the first term will be retrieved, but any records with the second term will be eliminated.</td><td><img width="194" height="95" src="../images/andnotimage.jpg" alt="AND NOT"></img></td></tr>
                <tr><td width="75%"><b>OR:</b> Broadens the search and retrieves records containing any of the fields it separates. In the example above, the OR operator returns all of the results of Search Field #1 and all of the results of Search Field #2 (including those which contain both).</td><td><img width="187" height="98" src="../images/orimage.jpg" alt="OR"></img></td></tr>
            </table>
        </div>
	
        <h3>Chemical Search Portal</h3>
        <p>The chemical portal opens with the <b>Compound Data</b> and <b>Structure Search</b> sections expanded.</p>

        <h4>Compound Data</h4>
    	<ul>
        	<li><b>Chemical Name</b>: This search parameter offers a great deal of flexibility, allowing you to search for the <b>exact name</b>, for words the name <b>begins with</b> or <b>ends with</b>, for words the name <b>contains</b>, or for other names that <b>sound like</b> the name for which you are searching. You can search by the <a href="http://www.iupac.org/" target="_blank">IUPAC</a> name or by common or alternate names at the same time (default setting, <b>Names and Synonyms</b>); you can search for just the IUPAC name (<b>Name only</b>), or for only the common and alternate names (<b>Synonyms only</b>).<br /><b>The chemical names displayed on this website are generated by Accelrys’ Isentris program, using the Open Eye library and IUPAC standards. </b>Names are generated for as many compounds as possible, but Isentris is unable to name every compound on the website. If the program fails to generate the name for any part of the structure, it displays the symbols, [?], for that portion of the compound’s name. We are unable to QC every name generated by Isentris, so if you run across an error, please let us know and we will correct our records. For peptides and oligonucleotides the sequence is used as the chemical name in place of an Isentris generated name.</li>
        	<li><b>Company</b>: Sometimes entries contain data on a company affiliated with a certain compound, typically because the company tested the compound, markets the drug or has a patent on the compound.</li>
        	<li><b>Chemical Class</b>: Each compound is classified based on functional groups that are part of that compound&#39;s structure.</li>
        	<li><b>Target Class</b>: In some cases, compounds are classified based on the organism, biological process or protein that compound acts upon.</li>
        	<li><b>AIDS#</b>: A unique identification number assigned to each chemical substance or to each combination of two or more chemical substances that constitutes one record in the Division of AIDS Anti-HIV/OI/TB Therapeutics Database.</li>
        	<li><b>NSC#</b>: A <a href="http://www.cancer.gov/" target="_blank">National Cancer Institute</a> (NCI) internal identification number assigned to each distinct chemical substance recorded in <a href="http://dtp.nci.nih.gov/branches/dscb/repo_open.html" target="_blank">their database</a>.</li>
        	<li><b>Molecular Formula:</b> This is a standard scientific notation that lists the kind and number of atoms in a molecule. For example, water has two hydrogen atoms and one oxygen atom and its molecular formula is H<sub>2</sub>O. To search for molecular formula in this database, it should be entered with one space between the elements. In addition, please note that the elements of any molecular formula (which ever ones are present) should be entered only in the following order: C H Br Cl F I N O P S. For example, the proper search format for water would be <i>H2 O</i>.</li>
        	<li><b>CAS#:</b> A unique identification number assigned by the <a href="http://www.cas.org/" target="_blank">Chemical Abstracts Service</a> (CAS) to each distinct chemical substance recorded in the CAS Chemical Registry System. This number may contain up to 9 digits.</li>
        	<li><b>Molecular Weight</b>: The sum of the atomic weights of all the atoms in a molecule. You have three options when searching molecular weight: molecules with a molecular weight equal to or less than your search entry (<b>&lt;=</b>), molecules with a molecular weight equal to or greater than your search entry (<b>&gt;=</b>), or a <b>Range</b> of molecular weights. When searching a range of molecular weights a second field will automatically appear, such that you should enter the bounds of the range into individual fields.</li>
        	<li><b>LogP</b>: The partition coefficient for a neutral molecule calculated automatically based on the structure of the molecule by an algorithm from Symyx, <a href="Help.aspx#LogP" target="_blank">described here</a>. You have three options when searching LogP: molecules with a LogP equal to or less than your search entry (<b>&lt;=</b>), molecules with a LogP equal to or greater than your search entry (<b>&gt;=</b>), or a <b>Range</b> of LogP values. When searching a range of LogP values a second field will automatically appear, such that you should enter the bounds of the range into individual fields.</li>
        	<li><a name="lipinski"></a><b>Lipinski Score:</b> Christopher Lipinski and his co-authors reported a method of predicting the absorption and permeability properties of an orally administered compound based on cut-off values for four parameters: LogP (where P is the octanol-water partition coefficient), H-bond donors, H-bond acceptors and the molecular weight. The &quot;Rule of Five&quot; is derived from the observation that if all four parameter values for a compound are less than five (LogP and H-bond donors) or multiples of five (ten for H-bond acceptors and five hundred for molecular weight), then this compound is predicted to have good absorption or permeability properties.<sup class="supscr"><a href="#ftnref1">[1]</a></sup> Listed below are Lipinski&#39;s rules:
        	    <ul>
	        	    <li>Not more than 5 hydrogen bond donors (nitrogen or oxygen atoms with one or more hydrogen atoms)</li>
        	    	<li>Not more than 10 hydrogen bond acceptors (nitrogen or oxygen atoms)</li>
        	    	<li>A molecular weight under 500 Daltons</li>
            		<li>An octanol-water partition coefficient log P of less than 5</li>
    		    </ul>
        	<br />
    	    <p><b>The SCORE calculated and posted in this database is defined as the number of parameters (out of four) that are less than or equal to the cut-off values originally proposed by Lipinski&#39;s &quot;Rule of Five.&quot;</b> This database shares this score to aid the user in searching for compounds that may demonstrate favorable absorption or permeability properties (oral bioavailability). Any compound with a score greater than or equal to two may have oral bioavailability. These scores are only a prediction about a compound&#39;s oral bioavailability; therefore, other criteria should be considered when determining a compound&#39;s usefulness as a drug. Exceptions will always exist, such as substrates for transporters and natural products.</p>
    	    </li>
	    </ul>
    	
        <h4>Structure Search</h4>
        <p><b>MarvinSketch:</b> MarvinSketch is an advanced, Java-based chemical editor for drawing chemical structures from ChemAxon. To enter a structure, click on the <b>structure input</b> box and the editor will pop up in a new window. A detailed user&#39;s guide for Marvin Sketch can be found by following this <a href="../marvin/help/sketch/sketch-index.html" target="_blank">link</a>.</p>
	    <ul>
        	<li><b>SMILES string</b>: Simplified molecular input line entry specification (SMILES) strings are linear strings that unambiguously encode the two dimensional or three dimensional structure of a compound<b>.</b> You can upload a structure into the MarvinSketch editor by pasting a SMILES string into the <b>SMILES String search field</b> and then pressing <b>Load SMILES.</b></li>
        </ul>
        <p>After entering or uploading a structure into MarvinSketch, you can perform a database structure search using one of these operators:</p>
        <ul>
    	    <li><b>Similarity</b>: Finds compounds that are structurally similar to your structure, using the degree of similarity specified.</li>
		    <li><b>Flexmatch</b>: Finds compounds that match your structure given the flexibility option specified.</li>
    	    <li><b>Substructure Search</b>: Finds compounds that contain your structure. A substructure is a portion of a larger molecular structure.</li>
    	    <li><b>Exact Match</b>: Finds compounds that are an exact match of your structure.</li>
	    </ul>
	
        <h3>Biological Search Portal</h3>
        <p>The biological portal opens with the <b>Cell Based Anti-HIV Assay Data</b>, <b>Anti-HIV Enzyme Inhibition Data</b>, and <b>Anti-Opportunistic Infection Data</b> sections expanded.</p>

        <h4>Cell Based Anti-HIV Assay Data</h4>
	    <ul>
    	    <li><b>HIV Target:</b> The enzyme or cellular process at which the inhibitor or drug is targeted in a cellular anti-viral assay aimed at inhibiting or preventing HIV proliferation.</li>
    	    <li><b>Cell Type:</b> Cell line used for cellular anti-HIV assay.</li>
    	    <li><b>EC<sub>50</sub></b>: For the purposes of this database, EC<sub>50</sub> reflects the concentration of compound necessary to inhibit HIV proliferation by 50%.</li>
    	    <li><b>IC<sub>50</sub></b>: For the purposes of this database, IC<sub>50</sub> reflects the concentration of compound necessary to inhibit host cell growth by 50% when describing a Cell Based Anti-HIV assay.</li>
    	    <li><b>TI:</b>: This stands for Therapeutic Index, which is a ratio of the therapeutic dose of a compound (the dose at which it inhibits target activity by 50%) and the lethal dose of a compound (the dose at which it inhibits the growth of host cells by 50%). Using our nomenclature, <b>TI=IC<sub>50</sub>/EC<sub>50</sub></b>.</li>
        </ul>

        <h4>Anti-HIV Enzyme Inhibition Data</h4>
        <ul>
        	<li><b>Enzyme:</b> Historically, this field listed the name of a specific enzyme (RDDP, DDDP, RT-K103N, F227L, etc.) used in the assay. In protein binding studies, the name of the protein:ligand (GP120:CD4, CCR5:RANTES and others) is cited in this field. The database development team is currently evaluating the utility of this field. Future versions of the database may merge this field with the Target field described below.</li>
    	    <li><b>Target:</b> Historically, this field listed the name of the HIV infection- or replication-related viral or cellular component(s) and or event(s) on which the compound either shows its effect or is expected to show its effect in cell-based studies. Examples include: Reverse Transcriptase (RT), Protease (PR), Integrase (INTG), GP120, GP41, CXCR4, CCR5, CD4, NCP7, TAT, REV, RRE, etc. The database development team is currently evaluating the utility of this field. Future versions of the database may merge this field with the Enzyme field described above.</li>
    	    <li><b>IC<sub>50</sub> or Ki</b>: This field searches both IC<sub>50</sub> data and Ki data for Anti-HIV enzyme assays. For the purposes of this database, IC<sub>50</sub> reflects the concentration of compound necessary to inhibit targeted enzyme activity 50% when describing an enzyme inhibition assay. Ki is the inhibitor dissociation constant. The database development team is currently evaluating the utility of this search option. Future versions of the database may separate this search field into two separate search functions.</li>
        </ul>
        
        <h4>Anti-Opportunistic Infection Data</h4>
        <ul>
        	<li><b>Pathogen</b>: Microorganisms and viruses targeted by a compound in a growth inhibition assay.</li>
        	<li><b>OI Target</b>: Protein or pathway targeted by a compound in a growth inhibition assay.</li>
        	<li><b>Enzyme Inhibition</b>: A relative rating of the activity of a compound against an enzyme. The database development team is currently evaluating the utility of this search option. Future versions of the database may remove this field.</li>
        	<li><b>Growth Inhibition</b>: A relative rating of the activity of a compound against a pathogen. The database development team is currently evaluating the utility of this search option. Future versions of the database may remove this field.</li>
        	<li><b>In vivo activity</b>: A relative rating of the activity of a compound against a pathogen in an <i>in vivo</i> model. The database development team is currently evaluating the utility of this search option. Future versions of the database may remove this field.</li>
        	<li><b>MIC or IC<sub>50</sub></b>: This field searches both the Minimum Inhibitory Concentration and IC<sub>50</sub> data entered for opportunistic infections. The database development team is currently evaluating the utility of this search option. Future versions of the database may separate this search field into two separate search functions.</li>
        </ul>
    
        <h2>Literature Search Portal</h2>
	    <ul>
	        <li><b>LitRef#:</b> A unique identification number assigned to each reference indexed in the Division of AIDS Anti-HIV/OI/TB Therapeutics Database.</li>
        	<li><b>Year:</b> The year an article was published.</li>
	        <li><b>Author:</b> Perform a simple text search of all authors listed in the database. Comma separated words are searched independently; for example <i>Smith, Sam</i> would search for <i>Smith and Sam</i>. In most cases, the full author list for each paper has been indexed in our database.</li>
	        <li><b>Title:</b> Perform a simple text search of all titles in the database. It is not necessary to enter the entire title; however, entering part of a title will return all articles containing the exact sequence of words entered.</li>
    	    <li><b>Journal:</b> Journal names are primarily entered in abbreviated format, consistent with <a href="http://www.ncbi.nlm.nih.gov/sites/entrez?Db=journals&amp;Cmd=DetailsSearch&amp;Term=currentlyindexed%5bAll%5d" target="_blank">National Library of Medicine abbreviations</a>. Searching for an unabbreviated journal name will return an incomplete list of results.</li>
	        <li><b>Volume:</b> Journal volume in which the source article appeared.</li>
	    </ul>
	
        <h2>Literature Availability</h2>
        <p>Many articles are available in PDF format through this website to NIH staff on the NIH network. Additionally, NIH staff can request paper copies of any paper referenced in this database through <a href="../ContactUs.aspx" target="_blank">this contact form</a>. <b>We cannot provide papers to non-NIH personnel</b>.</p>

        <h2>Surveillance Memos</h2>
        <p>We publish a <a href="../memo/memos.aspx" target="_blank">biweekly literature surveillance memo</a> that identifies relevant research published on pre-clinical experimental therapies for HIV, opportunistic infections (OIs) associated with AIDS, and other viral pathogens. Where available, URL links to article abstracts are included in the memo. Abstract links to PubMed (ncbi.nlm.nih.gov) are available to all users. Abstract links to Web of Science (<a href="http://www.isiknowledge.com/" target="_blank">http://www.isiknowledge.com</a>) or Science Direct (sciencedirect.com) are only available to NIH staff inside the NIH firewall or to other registered users of these Websites.</p>

        <p></p>
        <p></p>
    </div>
    <div id="footnote">
        <br clear="all" />
        <hr />
        <br />
        <div id="reference">
            <a name="ftnref1"></a>
            <p>[1] Christopher A. Lipinski, Franco Lombardo, Beryl W. Dominy and Paul J. Feeney, &quot;Experimental and computational approaches to estimate solubility and permeability in drug discovery and development settings.&quot; Adv. Drug Delivery Rev., 1997, 23(1-3), 3-25.&nbsp;&nbsp;</p>
        </div>
    </div>
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
