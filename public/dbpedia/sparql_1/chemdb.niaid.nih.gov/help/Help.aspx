

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Help</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Help.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9eT0rOkq3nrbN0s/ugjJfI30pCiX9ZAwIfwPAALjtnYaa8KFvQPud+gsuTDly6HuaDrG/8OGJq67CK0BdqV+uO1kiG9Q4g+GItZN80CifTKecDHrmK9OwGFdZIg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D17D9664" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="UserGuide.aspx">User Guide</a></li>
		    		<li><a href="../memo/Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="../memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="userguide">
        <h1>Help</h1>

        <br /><br />
        <p>
            <a href="#A">A</a> |
            <a href="#B">B</a> |
            <a href="#C">C</a> |
            <a href="#D">D</a> |
            <a href="#E">E</a> |
            <a href="#F">F</a> |
            <a href="#G">G</a> |
            <a href="#H">H</a> |
            <a href="#I">I</a> |
            <a href="#J">J</a> |
            <a href="#K">K</a> |
            <a href="#L">L</a> |
            <a href="#M">M</a> |
            <a href="#N">N</a> |
            <a href="#O">O</a> |
            <a href="#P">P</a> |
            <a href="#Q">Q</a> |
            <a href="#R">R</a> |
            <a href="#S">S</a> |
            <a href="#T">T</a> |
            <a href="#U">U</a> |
            <a href="#V">V</a> |
            <a href="#W">W</a> |
            <a href="#X">X</a> |
            <a href="#Y">Y</a> |
            <a href="#Z">Z</a>
        </p>
        <p />

        <h5><a name="A" id="A">A</a></h5>

        <h2>AIDS #</h2>
        <p>A unique identification number assigned to each chemical substance or to each combination of two or more chemical substances that constitutes one record in the Division of AIDS Anti-HIV/OI/TB Therapeutics Database.</p> 

        <h2>Author</h2>
        <p>The author field searches for references by author and can be accessed from the literature search page.</p>

        <h5><a name="B" id="B">B</a></h5>
        <h3>Boolean</h3>
        <p>Our <b>Advanced Search</b> tool gives you the option of executing a Boolean search. Boolean search lets you look for combinations of up to three criteria using Boolean connector terms (i.e., AND, OR, AND NOT).</p>
        <ul>
    	    <li><b>You must select at least two search criteria before attempting to specify a Boolean connector term.</b></li>
        	<li>After you specify your search criteria, you can modify the search logic by choosing your fields and operators from the drop down lists in the Boolean Search section.</li>
        	<li>By default, AND is used for all search criteria combinations.</li>
        	<li>If you select more than three search criteria, AND will automatically be used for all connector terms.</li>
        	<li>Here is a detailed explanation of Boolean connector terms:</li>
        </ul>
        <div id="boolconnectors" class="nopadding">
            <table border="0">
                <tr><td width="75%"><b>AND:</b> Narrows search and retrieves records containing all of the fields it separates. In the example above, the AND operator returns only those results which contain both Search Field #1 and Search Field #2.</td><td><img width="175" height="95" src="../images/andimage.jpg" alt="AND"></img></td></tr>
                <tr><td width="75%"><b>AND NOT</b>: Use NOT to exclude a term. Records with the first term will be retrieved, but any records with the second term will be eliminated.</td><td><img width="194" height="95" src="../images/andnotimage.jpg" alt="AND NOT"></img></td></tr>
                <tr><td width="75%"><b>OR:</b> Broadens the search and retrieves records containing any of the fields it separates. In the example above, the OR operator returns all of the results of Search Field #1 and all of the results of Search Field #2 (including those which contain both).</td><td><img width="187" height="98" src="../images/orimage.jpg" alt="OR"></img></td></tr>
            </table>
        </div>
        
        <h5><a name="C" id="C">C</a></h5>

            <h2>CAS#</h2>
            <p>A unique identification number assigned by the Chemical Abstracts Service (CAS) to each distinct chemical substance recorded in the CAS Chemical Registry System. This number may contain up to 9 digits.</p>

            <h2>Cell Type</h2>
            <p>The cell line used in a cellular anti-HIV assay.</p>

            <h2>Chemical Class</h2>
            <p>A category assigned to a compound based on functional groups that are part of that compound&#39;s structure. </p>

            <h2>Chemical Name</h2>
            <p>This search parameter offers a great deal of flexibility, allowing you to search for the <b>exact name</b>, for words the name <b>begins with</b> or <b>ends with</b>, for words the name <b>contains</b>, or for other names that <b>sound like</b> the name for which you are searching. You can search by the IUPAC name or by common or alternate names at the same time (default setting, <b>Names and Synonyms</b>); you can search for just the IUPAC name (<b>Name only</b>), or for only the common and alternate names (<b>Synonyms only</b>).</p>
        	<br />            
            <p><b>The chemical names displayed on this website are generated by Accelrys’ Isentris program, using the Open Eye library and IUPAC standards. </b>Names are generated for as many compounds as possible, but Isentris is unable to name every compound on the website. If the program fails to generate the name for any part of the structure, it displays the symbols, [?], for that portion of the compound’s name. We are unable to QC every name generated by Isentris, so if you run across an error, please let us know and we will correct our records. For peptides and oligonucleotides the sequence is used as the chemical name in place of an Isentris generated name.</p>

            <h2>Company</h2>
            <p>Sometimes entries contain data on companies affiliated with a certain compound, typically because the company tested the compound, markets the drug, or has a patent on the compound. You can search for company in the <b>Advanced Search, Compound Data section.</b></p>

        <h5><a name="D" id="D">D</a></h5>
    
        <h5><a name="E" id="E">E</a></h5>
    
            <h2>EC<sub>50</sub></h2>
            <p>For the purposes of this database, EC<sub>50</sub> reflects the concentration of compound necessary to inhibit HIV proliferation by 50%.</p>

            <h2>Enzyme</h2>
            <p> Historically, this field listed the name of a specific enzyme (RDDP, DDDP, RT-K103N, F227L, etc.) used in the assay.  In protein binding studies, the name of the protein:ligand (GP120:CD4, CCR5:RANTES and others) is cited in this field. The database development team is currently evaluating the utility of this field. Future versions of the database may merge this field with the Target field described below.</p>

            <h2>Enzyme Inhibition</h2>
            <p>A relative rating of the activity of a compound against an enzyme.</p>

            <h2>Exact Match</h2>
            <p>The exact match search option finds compounds that are an exact match of your structure.</p>

        <h5><a name="F" id="F">F</a></h5>
    
            <h2>Flexmatch</h2>
            <p> The flexmatch structure search option finds compounds that match your structure given a specified degree of flexibility.</p>

        <h5><a name="G" id="G">G</a></h5>

            <h2>Growth Inhibition</h2>
            <p>A relative rating of the activity of a compound against a pathogen.</p>

        <h5><a name="H" id="H">H</a></h5>

            <h2>HIV Target</h2>
            <p>The enzyme or cellular process at which the inhibitor or drug is targeted in a cellular anti-viral assay aimed at inhibiting or preventing HIV proliferation.</p>
    
        <h5><a name="I" id="I">I</a></h5>
    
            <h2>IC<sub>50</sub></h2>
            <p>For the purposes of this database, IC<sub>50</sub> reflects the concentration of compound necessary to inhibit host cell growth by 50% when describing a Cell Based Anti-HIV assay.</p>

            <h2><em>In vivo</em> activity</h2>
            <p>A relative rating of the activity of a compound against a pathogen in an <i>in vivo</i> model.</p>

        <h5><a name="J" id="J">J</a></h5>

            <h2>Journal</h2>
            <p>The title of the journal in which the source article appeared.Journal names are primarily entered in abbreviated format, consistent with National Library of Medicine abbreviations. Searching for an unabbreviated journal name will return an incomplete list of results.</p>

        <h5><a name="K" id="K">K</a></h5>

            <h2>Ki</h2>
            <p>Ki is the inhibitor dissociation constant.</p>

        <h5><a name="L" id="L">L</a></h5>
            
            <a name="Lipinski"></a>
            <h2>Lipinski Score</h2>
            <p>Christopher Lipinski and his co-authors reported a method of predicting the absorption and permeability properties of an orally administered compound based on cut-off values for four parameters: LogP (where P is the octanol-water partition coefficient), H-bond donors, H-bond acceptors and the molecular weight. The &quot;Rule of Five&quot; is derived from the observation that if all four parameter values for a compound are less than five (LogP and H-bond donors) or multiples of five (ten for H-bond acceptors and five hundred for molecular weight), then this compound is predicted to have good absorption or permeability properties.<sup class="supscr"><a href="#ftnref1">[1]</a></sup> Listed below are Lipinski&#39;s rules:</p>
            <ul>
                <li>Not more than 5 hydrogen bond donors (nitrogen or oxygen atoms with one or more hydrogen atoms)</li>
                <li>Not more than 10 hydrogen bond acceptors (nitrogen or oxygen atoms)</li>
                <li>A molecular weight under 500 Daltons</li>
                <li>An octanol-water partition coefficient log P of less than 5</li>
            </ul>
        	<br />
    	    <p><b>The SCORE calculated and posted in this database is defined as the number of parameters (out of four) that are less than or equal to the cut-off values originally proposed by Lipinski&#39;s &quot;Rule of Five.&quot;</b> This database shares this score to aid the user in searching for compounds that may demonstrate favorable absorption or permeability properties (oral bioavailability). Any compound with a score greater than or equal to two may have oral bioavailability. These scores are only a prediction about a compound&#39;s oral bioavailability; therefore, other criteria should be considered when determining a compound&#39;s usefulness as a drug. Exceptions will always exist, such as substrates for transporters and natural products.</p>
    	    
            <h2>LitRef#</h2>
            <p>A unique identification number assigned to each reference indexed in the Division of AIDS Anti-HIV/OI/TB Therapeutics Database.</p>

            <a name="LogP"></a>
            <h2>LogP</h2>
            <p>The term, LogP, is defined as the logarithm of the octanol-water partition coefficient (P) for a compound. The partition coefficient is a ratio of concentrations of the unionized or neutral form of a compound in octanol and water under standard conditions at 25 <sup class="supscrblk">O</sup>C.  The LogP values for compounds in our database are estimated by the Symyx AlogP program. This program is based on the fragmental method by V. N. Viswandadhan et al.<sup class="supscr"><a href="#ftnref2" title="">[2]</a>,<a href="#ftnref3" title="">[3]</a></sup></p>

        <h5><a name="M" id="M">M</a></h5>

            <h2>MarvinSketch</h2>
            <p>MarvinSketch is an advanced, Java-based chemical editor for drawing chemical structures from ChemAxon. To enter a structure, click on the <b>structure input</b> box and the editor will pop up in a new window. A detailed user&#39;s guide for Marvin Sketch can be found by following this link.</p>

            <h2>MIC</h2>
            <p>The Minimum Inhibitory Concentration is the lowest concentration of a compound that will prevent the growth of a microorganism.</p>

            <h2>Molecular Formula</h2>
            <p>This is a standard scientific notation that lists the kind and number of atoms in a molecule. For example, water has two hydrogen atoms and one oxygen atom and its molecular formula is H<sub>2</sub>O.</p>

            <h2>Molecular Weight</h2>
            <p>The sum of the atomic weights of all the atoms in a molecule.</p>

        <h5><a name="N" id="N">N</a></h5>

            <h2>NSC#</h2>
            <p>An NSC#(The National Cancer Institute(NCI) internal identification number) is assigned to each distinct chemical substance recorded in their database. You can search for NSC# in the <b>Advanced Search, Compound Data section.</b></p>

        <h5><a name="O" id="O">O</a></h5>

            <h2>OI Target</h2>
            <p>The protein or pathway targeted by a compound in a growth inhibition assay</p>

        <h5><a name="P" id="P">P</a></h5>
    
            <h2>Pathogen</h2>
            <p>The microorganisms and viruses targeted by a compound in a growth inhibition assay.</p>

        <h5><a name="Q" id="Q">Q</a></h5>

        <h5><a name="R" id="R">R</a></h5>

        <h5><a name="S" id="S">S</a></h5>

            <h2>Similarity</h2>
            <p class="NoSpacing">As part of our compound based search, this option finds compounds that are structurally similar to your structure, using the degree of similarity specified.</p>

            <h2>SMILES string</h2>
            <p>Simplified molecular input line entry specification (SMILES) strings are linear strings that unambiguously encode the two dimensional or three dimensional structure of a compound<b>.</b> You can upload a structure into the MarvinSketch editor by pasting a SMILES string into the <b>SMILES String search field</b> and then pressing <b>Load SMILES.</b></p>

            <h2>Substructure Search</h2>
            <p>A substructure is a portion of a larger molecular structure. The substructure search option finds compounds that contain the specified structure.</p>

        <h5><a name="T" id="T">T</a></h5>

            <h2>Target Class</h2>
            <p>A category assigned to a compound based on the organism, biological process or protein that the compound acts upon<b>.</b></p>

        <h2>TI</h2>
        <p>This stands for Therapeutic Index, which is a ratio of the therapeutic dose of a compound (the dose at which it inhibits target activity by 50%) and the lethal dose of a compound (the dose at which it inhibits the growth of host cells by 50%). Using our nomenclature, <b>TI=IC<sub>50</sub>/EC<sub>50</sub></b>.  </p>

        <h5><a name="U" id="U">U</a></h5>
        
        <h5><a name="V" id="V">V</a></h5>
	
            <h2>Volume</h2>
            <p>Journal volume in which the source article appeared.</p> 

        <h5><a name="W" id="W">W</a></h5>
    
        <h5><a name="X" id="X">X</a></h5>

        <h5><a name="Y" id="Y">Y</a></h5>
    
        <h5><a name="Z" id="Z">Z</a></h5>
    </div>

    <div id="footnote">
        <br clear="all" />
        <hr />
        <br />
        <div id="reference">
            <a name="ftnref1"></a>
            <p>[1] Christopher A. Lipinski, Franco Lombardo, Beryl W. Dominy and Paul J. Feeney, &quot;Experimental and computational approaches to estimate solubility and permeability in drug discovery and development settings.&quot; Adv. Drug Delivery Rev., 1997, 23(1-3), 3-25.&nbsp;&nbsp;</p>
            <a name="ftnref2"></a>
            <p>[2] Atomic Physicochemical Parameters for Three Dimensional Structure Directed Quantitative Structure-Activity Relationships.4. Additional Parameters for Hydrophobic and Dispersive Interactions and Their Application for an Automated Superposition of Certain Naturally Occurring Nucleoside Antibiotics. V. N. Viswandadhan, A. K. Ghose, G. R. Revankar, R. K. Robins; J. Chem. Inf. Comput. Sci., 1989, 29, 163-172</p>
            <a name="ftnref3"></a>
            <p>[3] Predition of Hydrophobic (Lipophilic) Properties of Small Organic Molecules Using Fragmental Methods: An Analysis of ALOGP and CLOGP Methods. A. K. Ghose, V. N. Viswanadhan, J. J. Wendoloski; J. Phys. Chem. A, 1998, 102, 3762-3773</p>

        </div>
    </div>
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
