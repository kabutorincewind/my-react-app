

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - HCV Drugs in Development Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DrugDevelopmentHCV.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="On80KsEZh5/6xOrqyvrVkLvONl6a99xDEX6c33o3URRig8iQxklAhAzLCqhad7PLdUIybhDp3kvQZoCtS+LUGW41cTKWgTEzlV+WmbT5SzXsej9lGFGd4M6ZGQPhKiKHsiumvi1RfVxekAochIqN7tc9Lmdc61mI4AswLKNIdGJU2pHCMevX0GjHUCDWh8epDJOOvjFk8Rxa+AKy4k7e1oKY98GVOANyavOdhxPkJX4vsTdoTfTpSVJSwqskj3Ku8qDD9hWaD9TUT+ksBbaBIcs+AfNfwItZQp/Yv5JOpK/3GAlchPgS6vdlnsB6OU1U6RB+jahgCTwXtsCU61hlSd8rTyeLQ2LxkH+jm3Q+7OXhTafW2+/ePoRttvx4a+4J9SBV9h8gcQyvTwWoxUhRvqwQ5oHGPfcRhWh54V14aOxSvtxlCj8NAuuCJDJMCgey6SoDjl6i1yJZs5nww+XjE9gAKc7ZbaZbOFVCLC+iglh891qY2OSYTaYj+yJWpkpNeI6xd+c4d1kFyevKnsrTRBY2L7FJcD6On2tPXLxPiM+OPddFEkzfULS5SyiX9eRXOKaJ6axSVhL8EpoFFPjdmTD96upu312L8fVfx2wQ02JDHJG+vGjm+zfWvvoRafTUOR4pzsX3gC9aTQUE19gELKCgi+jf1L0zUYCgneUWTqpZ4o/V0FULYP0AbYafIQMvyqjovsdTqAXVOa2tLMrq9Isp2GiKibHYX6kHOs5LQqXRUDlV5AbFW4IqhAUakyNgRwJDcE3/Bl3dziZ4xpCWF06FpHArOyZ0cbjTwJwkSOjuMt5C5C48qd2ZQktvXuxVBHYj9w4FoWb3TE4TcDjguNich7mH8eGg3nrnvtbELiohfyMQgbLb48zjQUZS5hwPQ1sBpd/uD0Zqf3vafUDPFqO8ab03Jg4/xnVPwy5UtGEtIuy1CpQ5IeSCsCvKOzR4kw4u31yoBzo6G+PQY96K6t/n2DADx8rJiS5k+3DK/CCoTuIpESXwbt6rrmvHWrnikdeItPyN/cdYJOm8Sgn9f8K5UAZGLpxhgiUSjF1gYYcBxxy4lZjdvpfXVnQHr4yViUqDLizCDytI/Tzw5J70vTS/Wpc7KnNqgmebQr0C4KpTYr5vdINobBo/+Pfh4qvo5yf04gMzfhbGL1LG3Y8Nt1rPgmpKSQBVMZ1q/8AwPYiYbSMm7l26QVzGNlZhW1ZwZChlCX41ZqHwCWR9baBtqWH/fsW6nTRSVWBnIYFrxMHM8P3IkQJbLxNKFjTKvKWQwXWtBGCIMptmHRAG47lY//26iorrtwqD9d5xOtuXcmLJghb/FXsPPuZ0CNGQ4m7Ml9+NuexFShCO6v0h+5DiqqmoVhTAidrTLHNmVvXeoGLVWy9GdttJT//KY17no+ktV16245sOlwfWeO4+v54KTUJIDrJeZp+/zKMcYa87y0foLFH13lrBxnegaL3+G8yektJ+MrkXwjosx4IaY/j8qdixEnefv6ao+w9h7DA20nvsVzLyAwqnOmgheQ7ulCL/EvF1EVcIJq73Zk74RqhmNsFqHmKm5fvo+MlMwdPjLN9HDKU6HYpkBx5UWVR7o3Qvt95HIxISAyIrh74Ss8HGkYW4Z3k1VK7P0kGBTEoKLod32oPpWJi7ED7mQ/pydhYvSoJZXviowmB3YtQNUa5iEQdMwliplopYF/5poA2jwhVuVhduni0oPSWgswu0R62QhOZXWMetTRudsd9nKdY6eIjIN8TGtChfbmbLbRWOahdrS5z432s1ixnlu0QcJ4tWzOl2OZH7UCYdv2nwRU3PKa/lovNrBm7x3YZMWaLfR06Nk17bWQdDhEtyCFGcQAmQni+L2RAL32ZxRaKKKuDg/1D1CHgfINfTmcR0l9EJfacWTx26NBffS1O/X4kCjhTEpT+cnGGQfXEMJsCN36a5qCEX7Ao+8gCPcnKCyQPDLLaUkOtatjG843QKwJCzQvxBxs4Jk5THMw80lt2Uj+O00NtJ4P/wdpp6drweExqe/pCnoSoX9jTB1gIqOOrEZDgHq19Vz3VaOTIr9VpInmK/eBBs+1ELtX/Ul4mWVZCW6ObrFTjBCE8U7iA6rIp6zEbdxWqfVLXwMRFbejWiY6YHPiIj1JfMYLWAeMv17BN/pAhfnbq3NV+4Hd6UYcAJHYSudYLVTruvlWKbXSvXwruUlw0oKpdluyEX9fCkahHJrJYnTve1t5e33k+1JWnAbe6e8MnJFnz9g9I7ZTL833QpwKl4ExVVp8T1aeO8P79orbYZpknVOpTYQdM7p1MARG8mhkuMXOBsMpD096hNZlTXugCyv60OVlK6+QnfKkj8iN8UmrxAPAVZL+nHz49c7gRTsE/J6jgWRGr9Xoabqh/pEvGNm2DpGzzvbKlsEihrLbJsRQYN0lllXAyJ6IvaTPDxUq7l0GC2D3FlXtbSvkNWlnuSjBITdwKki2InKjPAjVTM0kmAVmZRpSf1f44tKZ6m9VA3ovRCILrlDWoEVEhze3OGc8RHGodwMacQIdjfbNh2gYml3PLwMaAAjRRmnDfMArgZX1UF+SU9e+R4toFlZ9/J+AvJHsmWR6QGilrgp+SNqWHprKaf7//Ce/Nqgf81dd9Xq3p/+Y1vh3KY2YOuUIgnCubO6rnRfPZkyWT2YkENqZ43AoGKqLLDdc+RbEvV5TgNByWHm0fVwhYXyYwh2/hS7bhIvQtzdHZG19QlIsMHhaiEKtoUTiPO498AF2qhZF5WWcWYKarIbTLe4Rw1zIWPfJ34sgknSc1a6yYMki6UHVwWbdJioxW1qZFWt5JdjNG3QBMwDKq3r3ZdIdPmHPcBFv6ThO1K/j2sUDNozIwDQLfrh/d5iMF/YQhIk48Mg2saTCFtISmpwzissvYyYn82BvC6hz8IleS0/F7eWGinjpxHe59bgindIjClRz3C3iYF/MEOSXCxbDb4BToO1GgvKGwjCSNlpKbQVOyHt/xTRMPlGX9694HOo35bo+fWL9bRaG9SN3MT183fK5BKIbyHOqBfbu0SDZuXYTq8HTkHSq8KBFOkiJmq/OjEJOv5j09cwBT61tmOki7RYD/KKUMJPgGRwCHKTvE1thG+itF9mkyUb5ISGIJcybrLbqbbwMz/M+5+ky1wMkJy+ALGVZ7fQHmRvBmG44v50uvoeoxNLZ0Tw5Fio0BoaPnVMqE2Re8sS9XkV/0JiwHqSO61NwPxmAa9oWO1kGFs8pS9WCnUSblVEKKqLdiIxTLmNnk43EtQB1LSMzHDmOwuFu7v0C3UqovHEiq027sqVkvpj0IgtcP5irwrb8/nIEE0Vw1+CHtHNOBhA0uoKiOvNUVhPGVQfNbAsjlukqIlP4HUrjrYRJfkltQpwa/OUkCJQXg3rjWz4ffaM1j3qWZQZqBXvCKs99D8iCAHHYrDcO7C+orvXrEDwG70UJCAnQhGbR+AN9iEIv4gbjeYj0bTh56aqj72UxdKHBHof+2AAkl64H9aH5X+Dk4cJjwPFNK4KwdvEvAtxweKSQEnYgKuffUPmrV3qbgktJSY+pYUAX3umsLcUC9f5MfHCApLKRp5u4rJitsgCHkpymVHt3uRR3JhWh4CK5Zb9lgxgkj7ex53rOm6hJ3WrVZke1HQjpnfSzsycooQOxB6+xWy/Ze7ybwL9wJDrIZfjt4Q10jgaYNVTBcaTUWwFIbp3+zQcIHqBOqa2GM58bpmfTnmle2PgM2dD6Xh3VHNo8zkacdv3ppC3KW/syHBjWAPvZv7Y3BgS2obZbdsU7LQnW79dY2o31OHPphtmhkqer+cIgWF5xDe3KnUQ6SubTi/LNY4eRoNSq/BcnpsIV47N+lGu5i+23qz877SV/e2NDYoA1y6EvLk/0/CfC02dZS0Iv+4RnyWAZLa9bU2/hTUeDvTc8YmzQXnKDTLX46dwta2s2nz3dmNiLsVjA+gsaopwl/QKNZk0iMkXkk+PW8LO8lT1jEVuikAvv/f9KDx0koKzbzNAaVZYQhUeZdUZ1rtz/+mc+uq1CvQSVG375rmtKzc/1xzDNt1TExb1JrGzsDoEX19rspjxUbbh/m5Ih98ZQawDCs2r1jtVQNkqs6hLvuZVL9nhe7wniGHKTNinmp4pPLhwg7DIm/6vEn+jZZ1GTHmyxYTh5km9v87suSoSXSNv7AZa2dJtwttZFt1DBaiIfz7Biy5osIoN0T9np/bhyUU7wum/uX1OQ4RsBbUBE2UQuqwFAL5KSfoA1fCrFobvj4t/VSDZVcRSoNVC1kcyZqTUWbl9d+6w/putFy5ahVxaD04Bgn5G7k+UccHBUjpahZTgVMidlafqffv/ALmZe+ZMZZQ9d4EG9vtwl5ST2iFjB33ZDdn8jyp+xEbQoEmvpDtH46hi1rhg/h2dqUgn/EUtT9aMk/XFlKIyRQC7TL+tw9Qi7jvB9LZYflSC+zihiuhW/fCmsoPFVvAQZj/8sCJKUzIFfnc54acKwfQi0BHhth/smlSJ0p2NVbh4kAcWk2Wl7Xz68OFoKF9OL4+45r0fyW99oZybpN/0TiFgKj+BPLxFNNOQ9LsZVjrbzfWVhRt2M5ogLl3N1QS6WXfniABiU1y5F0u95Gy6vdJyb3sIZRm71Yw4R5iftL9lz0zsXpUT9qOtGs6mpirfR6n6UuXgcjQ4WlN6vAWcRsA4QEQLGjo4ZUW1KT4P6y7FF967NOt6OqjIcJ4huIe9ojuApRdXK1QYaip797aCvbW5ut6j1UgFgjTkUed2XMbsNsQVQ8VkSvsCBJGmfi8IV9blkW5H7WWmyJoEs+pvDTENcQi29OusQsbTQie6NmHvLJpyhjgxUuW+6PyDWKILdQZk9WFAhU4CouVl5L/bBrbtJcO2HJeBUB8plEXu/zPX0M6lT43cFgw5xqHXla6naylHO+tU10CGI5sw476i9ks4jWkIub5D3cF0qmtsOl7j3V2TPZG/+RTEuBLy2oj4XbLE64ZqaUf4s5kk1p+Wo7pZCMxRH3O6aJ3M40J58xzcpMJFd1qgLGq4ywE9OijQ8mlDJL7sUm+bYqjM8cEnl3bxLa3Pkc/cqoZFknwp11JF4ysYFzlziusmnBzFmxuAMrLUH0DYI/eJlPaUvPH36MZvB1fspZqPqud5MK3q1RGf/txVxs2hkl7Vamb+diU+vvPbCpevRWStHqGHx5tKRmVSai613bEaRQ+ku3I0zli9dRoZg1Y/cWH+wSLt9FgGmMaR0V5Gb5BMbmKqgGKn2tnm5iUZCXGoORcePmuhZVX5F+5mKfz66GZTSKb3XfflyTkBcvsyL+m67OVHMTEZBUSa4zHx9p/eqjok2paLDv0Cc7eHNhSMylJkIvk/y9Z/O5aa3M7n7hXXg6HfsxXJ5bVj4GH8Q4mdq/5nATwUJx+mpU9zVuLHMPUIPpKwLZD/Njp3lltSodFL2u8K09zrBpmCq8FY13mZdmAm/ImbdUC8adS9PE+sKbZb7wWpi5Ev1Ry4tjuslGgXvgL6v+hgzzEt/IgxEpylzwqISHs6Qs5g0C5YpjTUtAq7RHKDpEOFo5rULz/NWCVhJ9d+O2U0OlrdmcTxLgxBsNBe6gjjWanmEDsf+kTCroFhqa18He4miJCPJmJTdSHVSAqalhjOin8PuyixjqR7tIaBKWXTfINxUNwaOQrt2J2oefo6oagXCcUsS+PAmh2wllynRR+aaYe4wgqELedGMY04kpDOoMhZQjpLdKLtXJTdzqMyGM3XkHRBzEuFqohAMFrcF8q6LhIaIftewzMXC9BSUQbw6eODYfIEnen/ftBWGGEYMI6BLCfUHFC18ovmJyUYu117VW6uQJIf9NKnFgLRBgGROumdZqdNhBD3nplQFUp3XfOsXtp3ScMJW3Eyk5xSSddcBvYpWaW06lus4NpcX7yZmJqIS1bWOHZ4MLzxqMxXrl/9Yi744dmYAdh3bxnFs3JzSr1GnLSeQy0eO0Xzvh6GUGTS6/+VG+eBkO2+tDDENzC06iQ8HKlli3ve9o+F/knQ/mXB3x1n0OOj07RnSEfJnKfG6vRSR8294oZ4zaW3vVyxXp9t7wcrnpD475QD0L150asNCym6vlXlaWy5IQ0wPBfg+vr8HWVZf8WK8d1c7jnCtRSneKHP+WspZrXYPakoXiURwUKrjW5ijrj4tWCZc9umqdEtpQ123JiyfhLGPSM66196gUv+mXhEL0Nk6LLXu5MK4466uCjKAg2GWhqayGGXEBG8XIlJR6qLWoDfMdkk4SLowIjBu5GrD5nHdBjb/ZeTva1v8q3hwr5uUrJJqlaTan96cDOlewt7XD1fWzj6w8whX2q0jHM5SoCNY6ihWs54bQTn9Sm/3vZJfTEnJSHrFrs7LqFrulOpP6EuOal0l2lIV9CjG/m+0gnFCO9QpK3QwBIeg+1OaZtvotAT4nJbwISOX7X4zhQV1GwMsgVecPHx+OfHFLurzv6iVR5o7mjaVTXEu+8hP9oAC2Uce20/Mol8BjLIWVnthPjvaO9OUpF3YOQTvcyT8jWecbB9zHYe5p0n/TujHrSFpcqsdR/AJQ/2BmpEL1ZIg/f+LNuDvZMhRX4GKhlqC8SvxR5Qf6izF+wjk7HcbRYw9+LQ/h1aSdkSR2YUX12SuF62MrQ5a/mOFwkcL+LPdb+4chPqE8Wj5aN4X3Gim9Zp3HDF0UxKkgiVh7Qwh15mdt1DLvJQJwey0rsvf2js5OfLC2AKAz0tFfYlVuZwG7HtuY/ZikGUNFF1EfvYWoEA2kxqJRPdpwxa1HTXr+DQmrVmx050xd9zmXU1L7ZyYQFTKOu+H3w7HOU3ISNWtLPBNxGxXDJficXZudPXc3JtUk7DgM+nf9dp8v04r9m8sL8lDtuw2POyYDij+fWjZPKaTkFml3irx6nYz1eVwmhmpPTyz0BvDAH6xCsh0QnW0n1LX7HnFtTPnSXhNo90Mb6kc75q+lnsBx5iV2+8lvZsCI1AAwOG+InZPm+H9ZKL5CvcivMkBJ3S68Hfp6cHsKGDk7Cnbl+a6o8Ay7dCOR6Sk7hklu3GTMyazOjgFAqNgyYrWjmnu8cavCX8IWWlFvvEJOcNwBpF7RxqPa9YDCv5XB0dXRbrBJX3VkWxvajFSb/wBFa68WAk5PGSe+2bjEz2OfZPEVOi8FUx21tHcvtLVg49wMC6yyKbQ/EVfaYpuRZCfcXpGigM8bBKjkDb3Gh+5zOxqL6MKL10vqAXYqxXh+whI2ZSkfNy5LQLo7X3QWNaY0k5W1Us7KlUQr9OCQafX+ZsulzgG1LJbnTGZa7OC7HbiQiF8ex+HQpsXd2WbXXngTVvYM8zD2EzxMpbRAoqp2xbIqsiexFRXJ5sjU1roejdAo8FTiULutI1u5BmY2KoN6kTH6H8o9jwLqRsvWr/L/ruS0PgSjfw+J9GjV2s1gCDCmQzAW8bOIYF5gBDDNTz+1HZ0CiAGeJ4ODWwN4n52VCOG+oy/at5d872GiuLLM7esh/fT1q8xW+7BzKHEM3b8/UUmOifMdcBOZHviUXQfROCpjdGpbqPrtW1q6JDm4PCQLd8enEEsoOyCh4zwX1PtoIpcg/8jsMCgot3uwh7Sc52BIk5OQz0x8BXTXyPlcK1NCGpV+YFykkP9qO0FIaaKzDfeQt260dipBAwtUOGxnKjiXgfAil29y/TGKl/Ir6pQXYvapVb3vRUbCJQZYdWaNW9QCxiw3qeMnpfHF4TmgVFD/BP9o6OAMLyaZ1jrTM3C8LZJ2+CXO1eEmx0DYpNxUK4kz2DBvg2jz3we5ZDKCtHGt4LMWx5zYIy5wGwdJZYAQqS9z0F/fQPqdnrPmzAo7OfYOcEiEy1CngcFCOyGMRwM4qb5dJEamgX6rZi8ntPWnogzJOZ1es+4utGVKDMVGSbT/lKqNZV5d+fvDOMiGXuMeuOS4R5km5b36ZSyXerJQkAMZzycwUAO6+zTrD8zRiz3iIRrvqVr5tKfWqtx2VS5iuNG7E6rBqNdJY3palyFh13SiJvD+xrfloVlNpnHj/MTDBUGsE6cJqa7n8NtOuPSvEDNtf4YUyWEKQ1aEmd63GSMC+MZ0IwpGSDTg0T1cC1UmeYHnsE4M4uZDOpe+zePua9mbgQaFg/l4zV8RjvXew+OaGwciAo0vV+PY0jr1zWUpdKfD8XG+AWeScPHXWsDrp3Sl5n9t9nOD96T9K4itEge1c5sy7HJ1yDRR+oC3P3N6KbiFIQGuRuQa8m/RoZ9tKWgPJshPuxmgKbbZe3lA/OQ3U7VJufvcJzqa4yBWSWkWX4RluoEvT1HfJE9m59WdYo0zzqCAu4ddQxKb762+7yYaHJQqXvlYHlqdRXb4RY0Eidob20p1khERMesgOd5s8epQlYga+kxk6MyzzXta9W/1P9FRWnj3s4ziWDNyEQTwNK0vnBFc927eoFlF/eJbpCskU6NzXjwk/3VevzQk7z4M5A2iFpZ/5EF3kwncsb+iIiepk3iLZN6qQIJ1kmHIp4tbCz33x/BIpnYn5NMkLQdC5SOdIZwhT+rydshM2RMBRUR9POuFeTChCGU+VWI4Saa/dIV5wZxBI0umVCc3Lh3m7J0bW6QxUlocBdUCygZwbGTaS273bwJqd5rloumy+xwU1rQwOUUxPHCfBkdY0t58jsmSX1yH59yzNriZFnO6Ge7A5zb3LDvLvhR8tMP2rsz0bPrK8JozS54iNPkPrVQNTK/4GLlaThcAQxZhsq2RFAfIPcfG/p9bPN3snQr1i4p/biWwfKYhLTzARUFHEARKbP1mSBSu7lQHtKw4knnpuheRWtcEMrvcYQRTuQEGuQUuIIJ5vNoaNhj08l8lkVtgluYT4yMELLwPgWcorSaXbF0mcfIMXMel2TYkGpM5VBoyGtjoFRsKPNZUMdtoUtw36aLeIiYFKfIXD7Y4/8HrJ+2vWU+zLb9LyDbZ1dVFwKnGsHSdUAEqDvzriBAdQfUTQXx2NxZSAyQR8htuR8A7XRihBvhkOqeYiSrHB9nx963zb9R0OgDWbPzlxNHq8Fibvjg86Txr4kF9Gw13hHd+BuhcCgbhcodsyvNLwC+YFvHGtq07z40DRskVyFgGSYaC5yWv6Gr05esrWCvBMpTOhgd73r0HH8x0qE8a36PhjqBnZ8vrcKgVdkHstdJqHGrpTDu0eePR28uvck7jWFfEYSFnTUUbGa+kTUXWCk74TsOKk3XbHAP2cj6KWbXbhHSjXkVCKwn8OPz27wUpmbyXt9wkvf/5/646jyV+YOhCRYkvPanfsDCCymwbSXLgCsp2zyS4WCwjUPuGH+ryjHsV8uLjDmnnujFQfLDkOcBmKhcf1lgBfVu85KvMK5WKCIQyqJAAzTHGmmHE7Lo9oNMq2Qzv4ZPpnprMyrepIkj9TIviUjok4ZVEAupqVAGjFbTymNiP+3hxwhVOxqroliJiKSlAUeILIC6FXABvvjrYHs1gPbzqGLjU67KAyuIvb6UX2HGD7ekxwcQE0awyqnQchjq3oerowQ5kebrS4KBERezP6W+eK7tP12SBiQDfiu+QUbhIFUblfpuiQ3fsQWyvbo6gQtzpmrM7rwNwnc0L08mL/3cL3t2p7HDiSo7SGX3j1GXtyUnLiz9pn5ctpLY/Ek1IKOc3TfwTYjS7tcbswWIwtQlxNS6ipGQvdDW1LjZxWwvgtHHuV6uyynJJM91+WuMAJrftlTXfILs79EiYa4/W6hxlYHxZUNfOgdagQ8tire+G7hIrjacJaJPq6infCkfVNt4urfhMJnZombpua95PtJbSV3qBuxYwTbvedXH9UL+AOBQdGPBI570MhvPDqhqAkoAAiruPLiElnPbpVE2uhIbaTu4dj4mBMA35QKw5FH//ZZE95ZqBn8vssJDlgHpD7r4PfYqi8LiEAwiHR/30do5OrNzWA/TlhRNwp1b5ZmH3GR/WdXnW2Bfpzdk2PNozo9/YXDU3TbLNgzigMNQ2DmYtod+ABRZB+lWdGT+PvCx+MKoXfmTIR/Qr+D1ZYgyjFhB4PeH5wkTDO48rdzl2073hSsmih6GvYNX5wEkp8ejirqRlydM17aRVPlzix3jjSFudh9BPYKNxfXH2fYT/cjcPnAJQj/rf3xJS49IW7Idcd/Lwn3psTe4onn6mGvH8zBZl1ya3wrsu5du85pbW90w3aWp0NjSVInRfXrXUeFaSrVH0DjQ4HkvEIElHCeGACwayarycL/JJrSYG88WIAlU0cv6qMklzRnEaNSsQZRUXlMR5m0lDaEv+J8/Xl9f1OHStZ5AJ/EF5nPo4nTayBGtEzutmlGTcrZ1uETll4DJ/8zyOJdGbiZK+if0yam6Qf+75AMq2CsANvJlNNl5kVP6UPiaEHV95nzhkrSVqXYu7E2YD5dQoBylhjLH/m7yU4lv+6/+KJODDdLOzbqmsQlV8M0VZOFER7Qq81aiZ82B9vLItHvQa/tY2Dd2P/KHdus92Vx300Q5slDhNkIhdIG+Alp/DYHKHrTFEiYqJGLBwdvtWBV95P3ejeXLGZ9Lu+J8c6H2kkgcti8hL3n0sGbfqdRvrgeVyyCpnYhsQB9c3q6aIaB+WuFy8/SS4ymbV277sXjOdg/eLeaA6MoKutDAhAc2T1ar8hlpE5C7ZZLnbskBjwH/thYZSEeMVCY7p2q+8T58YgcEQyxfgl6M1OJidp0w6eKtkydfKHS0vAMxhLO7SnQDjDpKdlgls7qKCeBMT90IYyT2vkak0qkGuydpvmvBhGHsr5ZjGPqKD8U2MMrf43LUgxUucAlu5DlAaeHTbsjOZ/Sm1HPg8krqLf1/NivgLy+iQYUikJR1lIy1weOPGUTdVb3p/ZTLS/g3ZR1Rsw07WwwJyNWWx5L1oCFKfd53M/TojzopyIPjBpiRxNF0OiO6uemvLk2siia1atc+kbFvbah/vxJ2lgQF85Tf38EUN/igWMiQS/7MM9JzmiKTxV6ti8hxd5MJGYt9qL3vlMhYNGTmKMTXUEtTooJqkYMALfvzpFqaZMk6SRKeb4E/u334jAXYlrDlX2CY1Lkai4sc/XkW0cPXNnyZxgW+Y/5IP/5yf0O+JGdJZtIwNO0bCk4WiKcQ1QiNEo5BiCiUoBkedhxIqKPob6qEl/WSVefgoZ5Ah3eu0HDOtrVBi62n+ZMUZUqSB3w5vGVo8o5QxD4URw6Kn/sMYCs/2v9TEwdvPsE7ZQekhLNxGUP5yJ/WnQYKLDAbNvTfI/hBTjs92l76wjlFB+H7Ag+DoOKBpH6+aUWbqffES1evn2AzOsAxNxXYUPdHamQ03zVBd36s3hTmo5qAmAOGgeRlbuzDEogmCnypnwJz3Wbp3/+Rc/xQlhhZJtsyzvVtFipfvSTwgDH9iQT1B5/0DXkOoKoapRya5wW6wISms/bQDcu/NH/1xvIskqm4mPzzt7GY00c+8YHXvLtNJundBhoJGy9eNPnIRkcxI7IJTE2PTLeBpsn/c4a5Lqo76mInPZGaKD2OtxIT1wkTyCdmTqQcxRMSG19XuLxcedIvNIOWfr9Kud32xoccDuXs7CD4vk4xkSbZ1m2UCkRhWRPa3sL1xClXGe2pgT/r0FgMNxgvolesnLNuepBOtgtmZRgLQ7xSMSAKcWIgctoVaR99zeXXjCl+IhLZioqH+ihQqGu4GvJKjEdSrD8OlW7cgoVAbfYPVr5jKJCloxPzl5jMNqLzopLJGxK66EbgNYB0uAkz7bUioQFqK8VCeatvZuneT03Lp1HVKi1LbItca+2czSzobg0yZou8cI7qKVgGz7qAWSqENLwZU4g5IjlI3WdLkzdl2hafobJwcCXTm8bdWbvz18igif48YhwH2U/zn2bMuxYHo+lLgBAXuxg/0g4nVymG0M82U9DTcZjJtxJhrydIwGUrSJ8Gn5lkspf+ecrWVr1HENQ0mfFe0Nhdqlv44lGqafw3gR1H6AEj6jq87z2S7rohF+ioEA96FnQ9fsqGmMtwJHVdnQhsls2GIXShUKM24fGdmzKzHw1gAf0jmD4lyifM1V67bevtHw2CyUxmL6NA/RJ/lvOzfO/ImB92YM+50fQWpgujKmD9908i/y/uZTnvxjFGPA91cXDmpN7XoworNttLHF9T0FTUEAcjurBmAd+8vSHEHU1NQjypSty3O+9SVYKIlj5LaoUgM1tT94BKiQKsJSUipKU2YqBb0qbcG51HQNxiuSRV2rLFmleRbgZ1Z5csu/DAVzg5u4up9wOkzEluMSDeqmC3+oX7gg+jbD03RvHPwDk7viiOkIk+eFLXXK5b4BFC8IAuJDP2EHO7wgQjqLajIC5ZArUKh6cfSYPdtBPz9lq3UTCeBpmOkZ7WdOowQj8Q4Zx7hVv+/KaaH07MahzDeyBffu9VaHn5g0MQOLj0MKISViGwDLlrD9gBhr1n9zKpOS8hLqwCHZhuXTdcjU0BfsCV+JArXpqtDi4QWbgyPCeTKXfy066h1np7UrXNAGL1iaOjVOeUZGGL611CDitFyqcIfMA/T+fMqLADUMrO2bfqM1RDVV8uDSj6bIr3tiDkSzjXIOGybMf8a3JqN7MzyuxHca+/kIqc9JguOUJ56itguMkRQRfn4NSvQR2IHSirPMYl2wzJt7lrXDxjKddufwSPD+kVF+qZGNCEe2RnX4hkro+u4N/jutXxrBJrev3loDORv17BNE3OK5qYswd88b0QmNsoXl71m92jqa1gOwJneXYGN/Cs/9CxOZ0+0VfnQy1XSEF+1Se0SQMfAEnbOzj3zjv/JqjkxOnVEyeZWgC//nIidM2+bJQtZoyHvCcT5VL1CLnC1hQjsZ92VoNwMspW9LWiRQOtN2NlKHzkA6Jv5VmoD/XKebF+mxBPEIQ2F6eRwY/F225KpaWuhjuB7OLCOPhvj6wN82PSXL5VBxMfIxZw8a701ehwSYMFo2sesq2Vi2R/wzQvSBpvrCaMQ7gPvMp3k9NjMATZtcN5Ih7UovTN1Rh92w1HyLipg5KgxijgERejXJt4SYSdz+h9k/jpo4YN+yuLsM/oo945lHfcZucxni1lqBwyN12lxefsE13/ne195KOjh+E3uFgLFZw2SaAvpp7A/zRtPU73uqaMl1h6R9p7iB9cgPxuLf7+K+X0t9kNAfZIL9pSGnpglWLqekNjpcH0IttXM0JnQRqyRDKpwgBTW5sppF1gfruuzV2Qx0G0lMfSvz2Tx9//43fzRDRC65LIHizLHIIhW80amuDv5jTJwtdFHActB6Ek5RNjnKV3bgsqS683zyMJ0MRz5d32EJfd17EOF6KcfOfLA3BWAKgFuRprGsXQw2aSpzMqUcL9ym9AGR69YI8Pqvu5JUDbeCYb5b0fRd3CEWPtUSQP/dh/r99Pi5aW1yWWb3uY4Na/KqRBYn+Kpxver0cs9zpzA6IeTZDlDFbbLjCsuLJpscNthQ+7wxO0sNo+kTUq1VSeVfnynJmhPOLUAf/dcqfhh85sIf2XM8XQdLO8RXHJiNGRwk2X+XaNW6WO3ZUsq5wlh0KTrd7/Kqtv4VX6nLbHjhKAbzddcVPur3a99iTaBl1NFO55xx761gsomf0X+5fxCx4pWFq5HfQu7Twxn6Y9Oo4CgW8tm/AMvPJSpcKL1WIt4AkElP7G4cP1+ZvMslgEd5evrZIfW5Ti0M3yYeboZWxiuDd0scT7kYfklEHj6TD9QsDQ91iwfwOGcITlTZLRoez6nxaEXk1SszXuT14esxkljAH/utcTRmm30GdweQxtpIi9RbjLF4CulIFosAV9QYHE6/S+PDWx6AnOxR9Mt1z+SXETqoVUG2w6odA0c8JSGFn/f1dOjL03Li0MA0zA8aXO/zzEn+e2j/5kVKm1FYGWVDC3YUFyIH/fTNsqQpNj+PeVdd8Uysegrwq0SFM6c7cilChFSqEBISvxkpJK0PQcnnHcx6VUfvzqaT2uNEVJ2r+rMaK4qrEaI9sUVbLVscgDW2L3AyksWKYDd6zojPqy6ihftYqEbrGkedXTBf0WSgywZXErDSSWVPUm/78Pn5BHxbsZ1yIyVT9qLAVXe4LI+gyLD69/r4uWHDB1i2UFv+VL/PHXdLQpTIqNQN+XwHk66FwxyEvdABz6AgnfVcjx/BM5+HJRaGBMKD80T/AV3vl4cnIzfpHVbF8RkturWxhcBnxVodcTtBBFrcl6BQBu75vKRL9vquXHDHm/9qSuApjeI722ZuGdS1wcGt77cTtFqovbEySF7IpFmjseM/S6gKiq4Vf8E5p86ru7HVbdyqdslCU6dOERi4LgszAB4egJByr4vHQUAXKsdvGOu3ChJh0xWR8+WG1OWcHWSWMTbV9DCq/jvuglzT1gipiNMF0XrstTHZYErZa6BVQGKqzkhtjZWBo9S6XNgsx1ZhPNc9VLVs7er5dIqPV4c2LRLtYuMJfOAEBkEjIBKW2kFB2V5Bi3Vpw6+P5FGkjxTv/6/IA65dYuLBZdOPHnUcPzEEMBwEE5GZ9f9QKkNmJ2Kcj3X+MTwt2wjbAW0Q1INgOt2SwOpaYLMg+jLmjEsWIIV5PqTfB7eTr+/7icrndxssy9s82nGT3GYt7m4MY+KkV96wrQO/gpwcFHd7caypPEJakhOB1nX1l270BnaSBRxTZyR9ndtQIT9vhRgBIOgiNtxQOnTir4fsPTc9Psf5zqUxukZR3mV0v4YNguXFBuoqBSpOt7NO8fa2Z70DYVWe2P7hIRXcEbQBxajb5p1mzio1KvZlaz9ZSpChEjGSkWNaF43NA5nCtEOdTRTP8Q7luEJ6Bh/roRXoDjYbkLepBKjXDdnr43btRe7A9fNJSoVz2grgQRN1KUoCi0Z1sqL/G725KQsDNJwN3T2893sFwb4TtxHWiY7JWnzdMVxRlrE/RoVqRArk80KVvLL8zEd76YzzHo0p58aydwm3OkXOBW4TrHnwqQjW7/Ha6J7UQ0PiD1NIQdaXu6bG4oKI5y+7iPEkhwO49oP06OTFAbHcIjulAJ3G0g7JoMbTcyH8Z8GtPicbnrn17jHUqeBMf0+7EsQWyDEjOLa3Tj2td42W2g+AfmT6d+WqKN9VwnVz5Fv1vzHfhKexLDZKwe3wRcX33ekURAc4rp33bAeBq1nm+HqPoa7mVsOu9scOjuiKA7gqlJsnOlKZhnWm41ExW0u4y1G/L0JUS1mJrID70p9nPosKpDOzu/Cx4A8cXdN55vpMZUTViujCTAaLXr2Zgg8KW/P7mIWr4jFd9zSLaoAjQT8XeqzqKXgECT8lq/hIcQb3jvd6R+7UAG4Xf9R9DDbpLEi6PH+yaRLmVeBjGcjJmWMa5C/eOilXqDt0z1bMRqNXk68I3l5bNH98umasmhBoay+o2unvcnRD8xrWpff8N5CrfJztDH5iyaPvAttR/Zp67wHlVNu7Le0uF60vbb2EY2jFBYyBc1pLoDkn1eeqFM9PNRxPqMxG4eQtrl0KLXgxi4Duvj7xUxGd3YPogRtMGFeCsZOJDvkYQJG+PusKGRHu7HEvbrqXiOWytt91meH93Z3qXpazSk7Hxky46cn1lhiIumKHG4rrH4W4SQr0z+W991tqBAlN7stHjdKQOfdwLjiKrKsDJ+WgEyXtr42HqtrWBn8dPLON3cLnmDaKeD2/Pj0kBWJH0yHJ8N60kn9RxwJuzcpP7TqtWj7HZ3pTtjowSM/Rxmuf+LLr/i4tj5usXgolp6S6S/Jncofb4xWkFixMZQ2Gf2KnojH8uvje7k8FzTUNqcTtC3i5XHBy9aOMfbXvbR2CEYfLraqRpRwsVFCVzdVw0B7DrnBXdqARe/iT/O4Pk9xnxrtyAHq1S5yrMACET2AP6SHDjD8s7pFR3Lel3ExjykIhOJsTWJb+n3LbuLMECkqcLxFBO+T6muZGz9mxF0wmZthGg92ooawp40pUvwOkyswfa9/c8ijSjfKYWsPDw0tkK5cwebXxAMKiaAGOmBx6oQ56k+EdC2rk12c5MSJ+mxI1Z5EbN6J0vkEb3ugG/+qk8UfBE2obti12zPU5XgcaJMPlr9FD+hgHc97SrawKyjWUt8LL3JCYTUjYlXNPhnHTk2AsR24F5adsme7LMCpQsWrYXsqVMpdGtni2EEqh7mXPU13KYFNOYiZW5FUfO8/grsJoqje5p5zv4XuZrtRNYYuHwKsGHOT8nPGoA/Jxmjht3SkPmmN4cSNIyaZ92yGz6i1fogzuFKoCZ+FAc7pyv7HbLLwCB4+j2MFPvS/y2PEP44PQwdSmswGeTmuxIlPVRcifk3aPy/4O6aONeLOWNeTZZ3Xb+rhO09ql1iCZI4RuA03ssUKx+cXFC0C+MgtDNx1b+aabYZC7Quc8kEsg63E06g7SxHjwx/5wbrQ79ezs0DrD3S2AEqO/3TOMgnvK5jnda4LOnL/YJsBM6J0Igxa7GHzl9VUb9y3lzMQjI1wjZm6E6rSYo+3s6bszuFLE+L2P4PbBB14XZ7509jF2UZKJ7wr0JRTf9NyIJ9f+J1JKFbkMsyDo66Uy81vVROX4DMXSAL97SNzYeGdOuG97Kxk9ZicBu60MKcgu8bPiYo/p1baDAaburXNyB77eUxb3fDiYgCkkMgIgYIxPPHLeHqzTjwDixr+5/Nji79LPBPx2sdQVSfjlBP7GCFONDQAaq9FLQlRR9/Awa5ErIQDW4HEq6rbDBW1OQW8Qi/B4+4K5uZWRu09pcdjDJ0Mg6n5eXvzm2Mye43xY0/WweOXH8ECfbXoKPOxeYLcZVztmmvBiCcQ1bXU+48T+VGODSeQV8ZG+f39jjqQDUNm3VJWdozNcf4WVFj/73+Ky74jW5s8ToH53iXT0XRl8/iqvOjZQBK+xMot5Mkt9Fk+KYr2ITdXEU+3Hm+J1fZWqwA+RNnNHYVRSnMwB1ggiXpl1IMMV5Nvul6luipg4BDBkR7OK7cBwD1teVDxroeXHrBDsx7g37kqR2yBHyBBwSJH5r/EnMnfFia+FEUY+0YKefhEex6tSHBh0CZEZ997nYlC+OeAlW2hrVo3pTN1zCpSwT0hLQXbT6qkK+/9wKZjThpCkEeTpEtFtpUZoC/M3SdbK8DPRYYsA6QCWJ2PY1K3xj4WfFlLPvO2psa4gwV6KA3mg+kfgwBGRsO/uaDcD9jUE8IlzUJz7aNEwZZNWwlkmkPCrW6QLwb+ZfYwVfylLB6MXEBOHA+O3xiVBNlEW87MUXWhFDQpBxAbYLCldBdEJthn2DitZ0D70b7Pt2tNOuaNwAd9Xvb1QhOtCgUN38O+JjgLJEV1E+IHXNcWDd2+0/Di1nXmh6ykWIMPKCzoomQLuILxrCfAc2D7LDiTCSSSDgNWx2xQaXDz8uYuDewBhPbA7zC2scq7w2Kva+yWdnWUe42rmBz7+A+1587sfOQGI4BuRpxwLD60oND4euPMJrmKdRq3K/8zn85sxCmwvFjrO8AybgBWDM3Z+5jSK5laDC9qDrVhaHZoK/LfFkBSG1AvUcwZQkiwZN8DOy8AZXGCE9rIYCplGt+s3kAGZJVEoCxUldtxAtduFwTxcJZRhQYDDMGyrRRBil/vEWVNtgf5G7a5VG+NTF/eLJx6yIaBsR52OxG2odm3ruyoO4uXpgm5mh4EGNxxwm731JQt2c50GbsHfw7bPE6KUVAMxjLeM1PcwogBSKzu2tV/XmtCR2kIhS4FY/2sxvPoVW91mR9QFSYxphZ+k7+a14KEmPtiO5JwsPuDL5mkwaXKwrdQvgUL+xoUX0oExSfuZ+d+XuWMN0ikgTymqgYM01Et5H/MnML++hGbLb9M4XlFvborJv7wqT7pOUrRLaT4uGBX9bH9+Pz1HJv1mq1nRQfOq2aJJo+60xzl0DIuZnD9PKcoV7gYuLyWFJ6e6v/+eD+NpfwuWl2B7SHbqUtyU0v+sGwuuded+1Dhs1KefZd9iNbb/ABCdKX93HcI5pqCf+Dg/cFUHZ2g7ISTw6f6D28YTQw03U07G4fda7tua2cxRMQFldsnAnjrCISPLnWiieRN4cFuTo8FLDLS8vZ9BWaR7AepC9e9fDlrBKREL44eUq35NmHL7qLYh4G3inLIXadr/uUn7kMMjvlCUmnRqKAplpr++SCFUDroozvU2jOSm26Wi4jyXWHelmQe6D3C88ksAUc8h0x/N7ZybdjfU27MYQ/V4vZLMSeaZrKt3EBXBKK4Zd6HmOYuAbSQoj7Ud+vbH3smcw9ZOChClHlroZHFxojOlqzLfqvQ/IA5It8F+pjtdDEz2sR0kLQoToAv4JMf9kG7D3t+DVBGybmRgZW7ZZMwOlFoCWME1W8kOQaLBtzeXHAa/VXASsgFf3UPQGVvSQFtCv7VnXWEZVA28ny8uH5MjeopjOWGENuC3W2HQQNHQYXOT20CguRjbcnlHthuipIOYFvq54Chd0cqvOEvUs9MV5Bj61jrdsNNAMFdJzfNc/sza7vo0gDzceLeeL/cVpSXpx0MIKFjJLocihOUvPgRq+ahZEh4f87bClmxr4X0vUYrweknMw8iyDX51vKq9iw6su7ngB9OgFz33lK2BZGFRPxl8OuVNW5bzsOckaWb07xUSEIh+iMY1YH1R7ViS1FC4/Oo9ix03hiTOhWsbsui4IwKExXXadE9NNBx/+KhPmsgvF/bXJs0/jHnuwzv8QBnPd0WQlwppDGciWUyG/awZZuJHOQVgE1GQ2HFP/ocZ1DVN2udByz5wHfcqs+OcDQPcwjyks7P53hV1WVeyJ1ZHaRMjRgFQfCS4St1c0vMl7TAYaBgIjqhl7aOy3Njjk4Pf5Bv9FnRpux59Dab2Xqn9eGgHU2nS0WD38PQ4lcSmdlv1LAYvOpdqP/ngyCudGRpdFUL55qNwHsEre77jfXfgW+5B/qGM9sk3UFDwLeuz+LAs92MS8+JZpml1jWNP2jKKHn1/5aIOITduVGnaYxNmxAhRfwRvH2PTTizGrINZtDPWhWlELgkJ9lln8q/5p3GlKBThhvGWOUeio29tw0sgUl9uYPlzoPMx8vnT4v/9BurCr2udnCBiIBDHLhXo6uTNBx2bHZgk/xdeqOlOV0qSmii9amacdNud8BsawiG9mohahmCAA2+fpm8ZiERXrDt11SSxF0DN+VOaDcOoDgAQ61nk+EIRq0GeH4RTw35Yx7rLHLSl+NHKdmakfCbyRrdiezBkycwkqzb32OQOH0s1Hr9tvRyVluHA3070GPGDis46mda0EcJqtaOPBDqjkY8f4hegRkRv0nBkDYsvSiMbOF6sjOS0hqcPRAF0U6hbUa0YrRe5qu3mBelyUOzsVo6LqA3hAZj8AthLxY6q+BtJo1eKFyTfkeiT8PtI+fHS+URJrKqjty5lpHLsnjwi4wOEML1PUqU8eDkvXZPMinAkJfi9dlYgg5mLjFiTb3o1yEgBdkH3cxuJS6whc9ie9ihyjjFdpFt8cBT+gzuKNW861s1NJ+xF7bBzjzJoyBFt15LKCqN8b5PcEgc9S+M1m1mStmlNn32CZWsyhni8BwYzW8A/mFSYKaFHdaiPbicNG1aLR7OooKsMKQQ+r/8k5VWH3Pku2Rbb6VnNbKCMAvqCS2AWh9tO9Aswvvb/tvpYNx+M0oxI7acbGGyh/CvIWScjNUVF4N6ONN8C4qrbHBRlf/eT7v51upjmNcX3Ieg0/W8qGYpsN1tR1mPKdAKy14gCt0Vxz7q8TDmJE/ndq7Lj+g+G+DBf+ZTB56AasIVxGoC2go7ejHroNso6MpKrCozO5efZlYz1jVJ6YQV5bViXRELhHOlQJVoEPo7zix0ABNj6KdvLx3+tZOlE1cR7n05HTMnzdVex70evQLHn+alrEXD4yr8D7s+lskHfHkwv+znFLgZWD04lRHl8bLRTi/HNvYZsyLF8ZPen+06XG1Fmtnr+LXhKNcJZAD4Qzej7ElxGfgELQ/1qdHra6a15l++U4ZYl7/Eh1lIooPzJNwJVJPSniBUGEfMvBweCCNGLeNBZg8HJehrstZfidXqS6KINNj5MPTZ65R6mX5pRev/A/YGuSOBXxtgKwDsjHhr0ukVs5jAl6wXR217fFTG9Tr+8KMShl0Im2PFniM4ZZaHzxaVtdwMFK6K681zS6+IgWglsKhcbf6eeDfZW5g2Jnh33j+feHHc2KgxfHG2jy/8H47q3qYbbx3GtA3Mpwdy2l45D/arvayzE1Og5WOsYFaiPkn4UTvql68nyl+KeBOvy6XocUJCbHR7BemxNBq2MBu6Z0qaYOSYX5ECiRXwVvcNO3lMDFBS5aW2tU/vKm2fibz0MPGsCmm9cvxgcdJ7q5DLSuTYcVfoL2rXZjhUQNrFSVIJdXWOQYZxpjWCckSLgRot9WMoMArs75TYiJTIRRyHd3tT0fXvWo55GsdxP/UrHdW4uRBctRYwpKVGsJs0NWH8HICDzd9r5aVNoW1T3JPuLPiIHDCGpPm9jKOqDUwXquHbRpK4TUJMhfjm7Vw5emxzOjCOj3fqpeMgiU7WTXnJfL0XhEcqonoDI/u0DulAyxKlcMq6XCYejCDHSoW0m60yQNy9n1n9yervP8ObrEWPwJ09sxXTLfANE98nrGmU3P6tfEWru2WOu5Go55dHKnNlieIWkt3LFqdwvxPcOupHvHl5pcXfkx/MAWme4iccE4h+sOhssXIBI4z3qH41vuXmkDXGkCuTVuAJ31rUPohZdggg+xieAvwYW3Pa8PyIBGjUqtE2cpTiEJ7mdi+zR9fpPnP6KDAtTmCehlAbNq4354qAVasitQ3tfd9YndO0z0Hnl2tZ4Kgf9aJDYlb1TjXbxRGlXIi9G8WZpUrx4x+KQ6067XGjcwLSMcBbp0MEZ0+1SQMrjKmocHB+lqiB5UEtU8PbcP+flWx915PdM98fyQp2FNernfvrp0/VACRh4ZrJ/XMY65yRTuwPqy3Xi5p7rf5UlnBmP8Wbh0js+cWpdJUlWaZLgF/r2YwXCCtPkwnnImPfS1A1jvH6escgOULe/QS7MMTLoBw2iulosB1xmhFVyJXbPbR9p+gQfHvFWZigLolseDB75TiQ3MbTyAaUiKCW147e27iNedogO+Gl3hN7Yw+S/zddfGdpgUqJmTtb1xXFh/9zWIMiKawkDhIXmcxqUVILiDhQEp47QxmzNRWZA7/caFeTlaurasnsfGGaJx+Mn6qxSBOVhilWa+v8a0S4cF9TYU9FMCWu52fqsu828Dldcd5lXzUacH+5P99/G1D/OfjsS8T2JyytBsBXg8loaVbUdTVAIeGjUEfO2xKm9ctc0Y7T1+cw+cRtdtNL48TnupUYVO2LEptEr4LzXnLxt9l2rHn8fAmGK6xPsta/OIOIFAHM5+EygFSMEDFJqlFKvh/yBpHf2qYt6D2nYMggNMXkQJ1k9f2Fd2lvR4Fv3fzxpVEmX4BaQGeieU3Q2cc2Nyzj4LsxVieG/xWYpjyjVjvMUe604XPSt1VbFZr68NE+AwzI9vqiS7YrqtzM9ZRkBYKVOXTuSyhCU+Yv4h6EJLMMAYGgn/nyxKLHHv+M4944YPsJStW/pjyPEbUEb07YdvU+INzloRB2CBWZpNNF3BUjl4glvoMWXQQ5MSk+qDFHGcIb+ZSfJYeVmp/RRDT7uts26bprPYzH6xJ2GRR10kPd4EPHe3pM9mhokdOD9uRnCnvdCyZzt2SX9dQHpfRmELP3KYf0dnF/fZaIYGfiicjCelyLuEl2WNFvGc3lzEjJu44ZmOlVgD0dwgu39ZYV68U7YCf6+kDUgE10UzPJ702Rvk2lNv+b1Z0YINzo1QiafW1ne1rMtF0vMfrlaXspvLqjdIZnCH5DNJjwTG2FOzC5yFE4rKHGJ3OSpHTzJghwdlu47wco4sWlnSwLCq7Nye/U5PEjFj90pdSoYVJ5wPd9cxXJMSKcY4p6bLeB1a6wKOF4zKYRsttJU8rJh+jiUWjrJmtjbTfh0H6PYl8E14OGXKOnyGu8LARYa28KLz8/BDsNu4hH/SL1vIm310h47+xrklYK5AWHEPE8/qxeSooVGIvsQ/OJB0U6zyyffNvvjw0EHLiIvqR2wdzkd0cwn2D21TI8DaWlB2XuJNK0Sdq/7v1EtjHAR0N6iOFAw6HMNebXcH/Ui5SD6bJ+788UQwUfrOpAOaCcqaud1Xv8BnX78bJPo3HEnBbtovbihV7JConxNaBzhvOjX90G7ORbz2QZE88u8oWzHNFWZOHSWbZLMbnhFErSBgzJ1FYpXBBYK4aQCtCkA79+wdFtffr1AQTyWzlqtmiwIEINCpbkRXznG2Mb3tS2MO/giY6b3P6jugQsrJczLWtIGyRrIOCLCOWZO8AnFfw7wxTiy3jWvNFISMGYidFHrbPQSb3i5tGNyQ4AcOqmwxPl6zcWN+sw/A7eb5Bpq/UEvDeUjQa9WkkE3FecemzhRt/Naq3WKEgCt55AI3ez++rWkHi1uQLeoKT8ywO7EOSZxJ3u23s7wdn/b++yPktun6fGUgah9zHnpvMoXUYa7Vz2DWv+WUJq8uPLeSs4usf5ArwynaCg4TiD4gXsZ4q55Fsqn3kHQdAyrXlfOesnbJ299a4zXWAa0qZll2x3KMhkuCcleonPb5uIOrFt6eh9ba3tVRUFPATV5lG0sz31YWMTFP9OU7atrMComGF070sRTwvlnFDOrfV3qeikCKko5ZKx9F+1n5DqXFukTNf2pKmgTERsnMp6/m5tZLjVdWVYbLmE+rYfCsdQFpbIZRf9FuxJYJr6GT9krgtPSGMe/QuHFj/xx2G76JlQjhoG9U2IQ+3fucWx/28R5FEQPDhzeeCkWgW5c4UUGqogspcKn17pQLI+1cQOSLDcPwC96EnQNzXP6GiZJcE6oMSfErmYseD2ObvimPSp+lA2IgKKnioRZqAunHwwyft3HxuZA+FQsxN1SM5AlAs9/zVEE4yldT6rEeGKpNNLlsd5/7+T4zeocGY7XRmt00ItRHbc00pOERGbIqSgspdMOwyyA8/8Mhy1MnqdF8hFoE6rvqur6rhxJzCyk+dxsSB3mctm+tR9AqZm8jTEjAh9rBJ1+32uLcTpRRx+aHc7ggY6LqJGEtGtUq2IEzo+Fmd2XagRA0XfQE91RYwwP3nptjQGazHBIT+/UC/JZq6XxwG71VNHgNmRYoqqcOmF4X58qtilkh+k1Y/1ffsUb+p4TWEEdDg/fo8pz8VM5BP3kikoVjxTerCdbah1cfq+xBPirLi7RrS2XXFkWxQa+WU9M/V9TJl/Bvbyq2dmXtuviKZAk036vM+X0Qm63EOO4V8K5La6Jrs86BJ5DzQCwyhMh5VvdwvHKyHq0j2p1nlOzaEJEUwWCAL8F+Ac++CJzGbC9rF82aGeH0A==" />

<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="memo/Highlights.aspx">Announcements</a> <img src="images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	            
        
<div id="content">	
	<div id="description">
		<h1>HCV Drugs in Development (Archived)</h1>
		<br />
		<div id="drugsindev">
            <div id="druglist">
                <table cellspacing="0" rules="all" bordercolor="#115169" border="1" id="ctl00__mainContent_dgDrugDev">
	<tr align="center" valign="middle" bgcolor="#396F9D">
		<td class="ColDrugClass"><font color="White" size="3"><b>Drug Class</b></font></td><td class="ColDrugCompany"><font color="White" size="3"><b>Company</b></font></td><td class="ColDrugName"><font color="White" size="3"><b>Drug Name</b></font></td><td class="ColDrugAltName"><font color="White" size="3"><b>Alt Name</b></font></td><td class="ColDrugStatus"><font color="White" size="3"><b>Drug Status</b></font></td><td class="ColDrugNotes"><font color="White" size="3"><b>Notes</b></font></td><td class="ColAIDSNo"><font color="White" size="3"><b>AIDS#</b></font></td>
	</tr><tr>
		<td>A3 Adenosine receptor Agonist</td><td>Can-Fite BioPharma</td><td>CF102</td><td>CI-IB-MECA</td><td>Phase 1/2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Antihistamine</td><td>&nbsp;</td><td>Chlorcyclizine</td><td>&nbsp;</td><td>Phase 1</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl03_Link" href="CompoundDetails.aspx?AIDSNO=033660">033660</a>                             
                            </td>
	</tr><tr>
		<td>Caspase Inhibitor</td><td>Gilead Sciences</td><td>GS-9450</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Caspase Inhibitor</td><td>Conatus Pharmaceuticals Inc; Pfizer</td><td>PF-03491390</td><td>IDN-6556</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Cyclophilin B Inhibitor</td><td>Aventis; Scynexis</td><td>SCY-635</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl06_Link" href="CompoundDetails.aspx?AIDSNO=189067">189067</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Cyclophilin B Inhibitor</td><td>Novartis Pharmaceuticals</td><td>NIM-811</td><td>[Me-Ile-4] Cyclosporin A; SDZ-811</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl07_Link" href="CompoundDetails.aspx?AIDSNO=006796">006796</a>                             
                            </td>
	</tr><tr>
		<td>Cyclophilin D Inhibitor</td><td>Novartis Pharmaceuticals</td><td>Alisporivir</td><td>Debio-025; DEB025</td><td>Phase 3</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl08_Link" href="CompoundDetails.aspx?AIDSNO=268533">268533</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>DGAT1 inhibitor</td><td>Novartis Pharmaceuticals</td><td>LCQ-908</td><td>LCQ-908-ABA.002</td><td>Phase 2</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>E2 glycoprotein MAb</td><td>Mass Biologics</td><td>MBL-HCV1</td><td>&nbsp;</td><td>Phase 2</td><td>Ongoing, Not Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Entry Inhibitor</td><td>ItherX</td><td>ITX-5061</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Nucleoside Inhibitor</td><td>Hoffmann-La Roche</td><td>RO-5428029</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Idenix Pharmaceuticals</td><td>IDX 21459</td><td>&nbsp;</td><td>Phase 1</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Idenix Pharmaceuticals</td><td>IDX-184</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl14_Link" href="CompoundDetails.aspx?AIDSNO=556345">556345</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Vertex Pharmaceuticals Incorporated</td><td>Lomibuvir</td><td>VX-222; VCH-222</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl15_Link" href="CompoundDetails.aspx?AIDSNO=543506">543506</a>                             
                            </td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Pharmasset/Roche</td><td>Mericitabine</td><td>RG-7128; RO-5024048</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl16_Link" href="CompoundDetails.aspx?AIDSNO=550380">550380</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Merck</td><td>MK-3281</td><td>&nbsp;</td><td>Phase 1</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl17_Link" href="CompoundDetails.aspx?AIDSNO=510563">510563</a>                             
                            </td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Merck</td><td>MK-8408</td><td>&nbsp;</td><td>Phase 2</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Viropharma; Pfizer; Gilead Sciences</td><td>Nesbuvir</td><td>HCV-796</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl19_Link" href="CompoundDetails.aspx?AIDSNO=376743">376743</a>                             
                            </td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Hoffmann-La Roche</td><td>Setrobuvir</td><td>ANA-598</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl20_Link" href="CompoundDetails.aspx?AIDSNO=497335">497335</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Theravance Biopharma</td><td>TD-6450</td><td>TF-6450</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Gilead Sciences</td><td>Tegobuvir</td><td>GS-9190; GS-333126</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl22_Link" href="CompoundDetails.aspx?AIDSNO=485775">485775</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Medivir; Tibotec Pharmaceuticals</td><td>TMC-649128</td><td>&nbsp;</td><td>Phase 1</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Gilead Sciences</td><td>GS-9669</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl24_Link" href="CompoundDetails.aspx?AIDSNO=543505">543505</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Gilead Sciences</td><td>GS 6620</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl25_Link" href="CompoundDetails.aspx?AIDSNO=558193">558193</a>                             
                            </td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Pfizer</td><td>Filibuvir</td><td>PF-00868554</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl26_Link" href="CompoundDetails.aspx?AIDSNO=490021">490021</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Boehringer Ingelheim Pharmaceuticals</td><td>Deleobuvir</td><td>BI-207127</td><td>Phase 3</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Abbott (AbbVie)</td><td>Dasabuvir</td><td>ABT-333</td><td>Phase 3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Idenix Pharmaceuticals</td><td>Valopicitabine</td><td>NM-283</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl29_Link" href="CompoundDetails.aspx?AIDSNO=418137">418137</a>                             
                            </td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Roche</td><td>Balapiravir</td><td>R-1626; RO-4588161</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Alios</td><td>ALS-2200</td><td>VX-135</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Alios BioPharma</td><td>AL-335</td><td>&nbsp;</td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Achillion</td><td>ACH-3102</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl33_Link" href="CompoundDetails.aspx?AIDSNO=565907">565907</a>                             
                            </td>
	</tr><tr>
		<td>HCV Polymerase Inhibitor</td><td>Abbott (AbbVie)</td><td>ABT-072</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Polymerase Inhibitor</td><td>Boehringer Ingelheim Pharmaceuticals</td><td>BILB 1941</td><td>&nbsp;</td><td>Phase 1/2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Abbott (AbbVie)</td><td>ABT-450</td><td>&nbsp;</td><td>Phase 3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>Abbott (AbbVie)</td><td>ABT-493</td><td>&nbsp;</td><td>Phase 2</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Bristol-Myers Squibb</td><td>Asunaprevir</td><td>BMS-650032</td><td>Phase 3</td><td>Ongoing, Not Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl38_Link" href="CompoundDetails.aspx?AIDSNO=501741">501741</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>Bristol-Myers Squibb</td><td>Beclabuvir</td><td>BMS-791325</td><td>Phase 3</td><td>Ongoing, Not Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl39_Link" href="CompoundDetails.aspx?AIDSNO=493957">493957</a>                             
                            </td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Boehringer Ingelheim Pharmaceuticals</td><td>Ciluprevir</td><td>BILN 2061</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl40_Link" href="CompoundDetails.aspx?AIDSNO=195599">195599</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>Intermune; Roche</td><td>Danoprevir</td><td>RG-7227; ITMN-191</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl41_Link" href="CompoundDetails.aspx?AIDSNO=488730">488730</a>                             
                            </td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Boehringer Ingelheim Pharmaceuticals</td><td>Faldaprevir</td><td>BI-201335</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl42_Link" href="CompoundDetails.aspx?AIDSNO=506842">506842</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>TaiGen Biotechnology</td><td>Furaprevir</td><td>TG-2349</td><td>Phase 2</td><td>Not Yet Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Gilead Sciences</td><td>GS-9256</td><td>&nbsp;</td><td>Phase 2b</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>Idenix Pharmaceuticals</td><td>IDX-320</td><td>&nbsp;</td><td>Phase 1/2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Rottapharm Madaus</td><td>Legalon&trade; </td><td>Sil; SHS; Silibinin sodium hemisuccinate </td><td>Phase 2/3</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl46_Link" href="CompoundDetails.aspx?AIDSNO=513548">513548</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>Schering-Plough</td><td>Narlaprevir</td><td>SCH-900518</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl47_Link" href="CompoundDetails.aspx?AIDSNO=501790">501790</a>                             
                            </td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Presido</td><td>Ravidasvir</td><td>PPI-668</td><td>Phase 2/3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl48_Link" href="CompoundDetails.aspx?AIDSNO=565898">565898</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>Merck</td><td>Vanihep&trade;(Japan)</td><td>Vaniprevir; MK-7009</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl49_Link" href="CompoundDetails.aspx?AIDSNO=496721">496721</a>                             
                            </td>
	</tr><tr>
		<td>HCV Protease Inhibitor</td><td>Gilead Sciences</td><td>Vedroprevir</td><td>GS-9451</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl50_Link" href="CompoundDetails.aspx?AIDSNO=551450">551450</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Protease Inhibitor</td><td>Vertex Pharmaceuticals Incorporated</td><td>VX-985</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Protease Inhibitor </td><td>Merck</td><td>Grazoprevir</td><td>MK-5172</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl52_Link" href="CompoundDetails.aspx?AIDSNO=501731">501731</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Abbott (AbbVie)</td><td>ABT-530</td><td>&nbsp;</td><td>Phase 2</td><td>Active, Not Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Novartis Pharmaceuticals</td><td>Zalbin&trade;</td><td>Albinterferon; Albuferon; alb-IFN</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl54_Link" href="CompoundDetails.aspx?AIDSNO=184128">184128</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Valeant Pharmaceuticals; Kadmon Pharmaceuticals </td><td>Taribavirin</td><td>Viramidine; Ribamidine; ICN-3142</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl55_Link" href="CompoundDetails.aspx?AIDSNO=000198">000198</a>                             
                            </td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Achillion</td><td>Sovaprevir</td><td>ACH-0141625; ACH-1625; Prodrug of ACH-1095</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl56_Link" href="CompoundDetails.aspx?AIDSNO=491583">491583</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Idenix Pharmaceuticals</td><td>Samatasvir</td><td>IDX-719; IDX-380; MK-1894</td><td>Phase 1/2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl57_Link" href="CompoundDetails.aspx?AIDSNO=564114">564114</a>                             
                            </td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Presidio Pharmaceuticals; Ascletis</td><td>PPI-461</td><td>&nbsp;</td><td>Phase 1b </td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl58_Link" href="CompoundDetails.aspx?AIDSNO=501737">501737</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Abbott (AbbVie)</td><td>Ombitasvir</td><td>ABT-267</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl59_Link" href="CompoundDetails.aspx?AIDSNO=541999">541999</a>                             
                            </td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Gilead Sciences</td><td>Ledipasvir</td><td>GS-5885</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl60_Link" href="CompoundDetails.aspx?AIDSNO=557593">557593</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>GlaxoSmithKline</td><td>GSK2336805</td><td>JNJ56914845</td><td>Phase 2a</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl61_Link" href="CompoundDetails.aspx?AIDSNO=563249">563249</a>                             
                            </td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Gilead Sciences</td><td>GS-9857</td><td>&nbsp;</td><td>Phase 2</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Gilead Sciences</td><td>GS-5816</td><td>&nbsp;</td><td>Phase 2</td><td>Not Yet Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Merck</td><td>Elbasvir</td><td>MK-8742</td><td>Phase 2/3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Novartis Pharmaceuticals</td><td>EDP-239</td><td>&nbsp;</td><td>Phase 1</td><td>Active, Not Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Bristol-Myers Squibb</td><td>Daclatasvir</td><td>BMS-790052</td><td>NDA</td><td>2015</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl66_Link" href="CompoundDetails.aspx?AIDSNO=503558">503558</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Eiger BioPharmaceuticals</td><td>Clemizole</td><td>&nbsp;</td><td>Phase 1b</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl67_Link" href="CompoundDetails.aspx?AIDSNO=033090">033090</a>                             
                            </td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>ZymoGenetics; Bristol-Myers Squibb</td><td>BMS-914143; PEG-rIL-29</td><td>(man-made IL-29); PEG_IFN_lambda; BMS-914143</td><td>Phase 3</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Bristol-Myers Squibb</td><td>BMS-824393</td><td>&nbsp;</td><td>Phase 2a</td><td>Withdrawn</td><td>&nbsp;</td>
	</tr><tr>
		<td>HCV Replication Inhibitor</td><td>Arrow Theraputics</td><td>AZD-7295</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl70_Link" href="CompoundDetails.aspx?AIDSNO=506699">506699</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HCV Replication Inhibitor</td><td>Romark</td><td>Alinia&trade;</td><td>Nitazoxanide; Xerovirin-C</td><td>Phase 2/3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl71_Link" href="CompoundDetails.aspx?AIDSNO=057131">057131</a>                             
                            </td>
	</tr><tr>
		<td>Immunomodulator</td><td>Cytheris SA</td><td>CYT107</td><td>Glyco-r-hIL-7; Interleukin-7</td><td>Phase 1/2a</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl72_Link" href="CompoundDetails.aspx?AIDSNO=398475">398475</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Immunomodulator</td><td>Biocad</td><td>Algeron</td><td>Cepeginterferon alfa 2b</td><td>Phase 3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Immunomodulator</td><td>Anadys Pharmaceuticals</td><td>ANA-773</td><td>RO-6864018</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Immunomodulator</td><td>Dynavax Technologies Corporation</td><td>SD101</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Immunomodulator</td><td>Idera Pharmaceuticals, Inc.</td><td>IMO-2125</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Immunomodulator and Anti-inflammatory </td><td>Novelos Therapeutics; Cellectar Biosciences</td><td>NOV-205</td><td>BAM-205 (Glutathione and Inosine); Molixan</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Interferon alpha 2b agonist</td><td>Biolex Therapeutics</td><td>Locteron&trade;</td><td>Controlled Release Interferon alpha 2a</td><td>Phase 3</td><td>Not Yet Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl78_Link" href="CompoundDetails.aspx?AIDSNO=001582">001582</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>MMP inhibitor/Anti-fibrogenic</td><td>Conatus Pharmaceuticals Inc.</td><td>CTS-1027</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>MicroRNA Inhibitor</td><td>Santaris Pharma</td><td>Miravirsen </td><td>Anti-miR-122; SPC-3649</td><td>Phase 2</td><td>Ongoing, Not Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Natural Product</td><td>PhytoHealth Corporation</td><td>PHN-121</td><td>&nbsp;</td><td>Phase 1/2</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>Not Specified</td><td>Novartis Pharmaceuticals</td><td>BZF-961</td><td>&nbsp;</td><td>Phase 1/2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>PD-1 receptor MAb</td><td>Bristol-Myers Squibb</td><td>Nivolumab</td><td>MDX-1106; BMS-936558; ONO-4538</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Stimulates T-helper cells</td><td>SciClone Pharmaceuticals</td><td>Golotimod</td><td>SCV-07; (gamma-glutamyl-L-tryptophan)</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl84_Link" href="CompoundDetails.aspx?AIDSNO=346667">346667</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Unknown</td><td>Merck</td><td>MK-2248</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Unknown</td><td>Merck</td><td>MK-1075</td><td>&nbsp;</td><td>Phase 1</td><td>Not Yet Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Unknown</td><td>Merck</td><td>MK-7680</td><td>&nbsp;</td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Virucidal/Anti-phosphotidylserine</td><td>Peregrine Pharmaceuticals</td><td>Tarvacin&trade;</td><td>Bavituximab</td><td>Phase 1/2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>toll-like receptor-7 agonist</td><td>Gilead Sciences</td><td>GS-9620</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr>
</table>
            </div>		
            <div id="drugtblinks">
                <br /><br />
                Some useful sites where additional information on these drugs may be found:
                <br /><br />
                <a href="http://aidsinfo.nih.gov" target="_blank"><img src="images/logo_wht_medium.gif" alt="AIDS Info Gov Logo" border="0" /></a>
                <br /><br />
                <a href="http://www.clinicaltrials.gov" target="_blank"><img src="images/ctgov_big_ttl.gif" alt="Clinical Trials Gov Logo" border="0" /></a>                
            </div>
        </div>            
    </div>					
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="SiteMap.aspx">Site Map</a> | 
        	<a href="help/Help.aspx">Help</a> | 
        	<a href="ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8923051F" /></form>
</body>
</html>
