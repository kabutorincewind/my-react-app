

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - HIV Drugs in Development Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DrugDevelopmentHIV.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uhT7An/cDPX8yWejxNkOcIPKCm5AoTAV0l17yFGzOloGBgIEfwv8ard9fug4/ZzayvTIBrWuw8C1q+JQ3jJ1+720vWMOO3McVnf//lr8IvDgn9+TrxAKfYLw0+hSDItOxXdFm4FQUEMNu5PjsGufrxByZ9CDYa5icP/kSZ58z5vg+oMOmxQpx6sokFPCTUjW+Wafm8/8jxsdpesutt/voi/FDPVdU+SzONp+DKLwQE4bwGCzURBKcFDY5p2YV2dSe6Q4JfhdTVCeimIAVD01trl72SyMIqhcyGQG23JEavpnkiyf8Jx+3qjcGnrwea5H+CuUSL4lb9QORexiYp8unQxTsQFQKlYBKWbH266bDpwkVH1wNx4VKyt6snewO2YhDU3McbbaN4sTcKtWUpO0OIT4BcF1BYK/OJo0kM6p0ChrS6j9h2i+TY4w7sU6wvsyOX49oCISZGcTsqnmkZg8sZa/w34Z1SSjndhzn64F2YOkxD7NgFTskpH/N5BKezY//a5+v9Pvuce+3c3VOF2PG8eZUsNH10nACq/4PX0AylnxxqSRBtlqzF8E3mTdN+KvV1Dl0NYZ4cYMPjMZBbs6oCRUXBGEo4Zrd0G4YHCGjojrEqCjy8egva3nz7RNuSG4bPTYGPJdD+8FmDPbAb5yIMNMKUNSUUkX20IkC6JuBJQXI+FidZsNhrbpaFKW3auSG2rfN2y1+GRWuQ6mMqe5Ks0mXgp83gXIT0HKyKslDgaED0GUELO4R20eQMOZDoTDDbrNgboitTTKnF2ckQp68uR203f7isZmQXPaw2Pd52+UaURJrSOBdwbfQzPPlqYhZWkg6G80yGUxD6FKTx4AREMw8J+aCQekkSMEFP93Ll21PW7Gll0q+2+Kc9ExGGZww2qbzXsdhWGM9+XruZSw3+UianYzNVgRGX/Msfonj6CXc/qSZdI/d2pYboj167jznpA/AIiaPM2zrV/E24y6EeUckTdIB/EKSrQfuvNy1NAcey94zz4mCM7o1qc7emwIh4ZgUZHEwH7atzb6DUAQuJ9JkNBdP78iXiwnd42185IrR3BedG4ZKASvzCkMTRPjAIrPaW2nUQ4YkorFkcNbdnhyZwloMamVqfQW3Q2g2t8/ZhAm6kjdYvr+SBvtiurKQUKih2wIxImhmtlQtDYMWW3bFIWodCdIj89PDbQO+sjEjUXLYjhgzGaaJdpPzcDfJ4HHQp74FGzAaQvQX9pSBHahG4k7TqD188Bo9x3QOJ4w47UCqmwVafu7E2yj2khU5ADNto65cvHlaTa7dML9VbikMZ7PGpbZcdTEH7T+kVjfQHiPZPbpRdBxvhg068NlKnZhipSgeMAFacmFRYNVLgBh6/mwBqgWtFQxEosnQxCDB2D0+jxl/Cf8WfAoXuDNn0y0hwO9ESWG6tUtJQJnoaRAXfQMJSeqXf+5o0I4sN6zzb4Di8pab+ZcrzV6Pzx0RQpwAr+3HOnbDx6LM8GqF/cxXVDemBbjNq1g2oV7Dfchv8PZDUHaBbP5dCZXkZZBXQu/Xrn9/NI2kzV6mQYwl3IrPFFwqtiodTzQEt/RGuMkUmGnud/7ZrV9sXEMmOKEBKKAX3RKVSjpU3ZCtdV6iSTer8hYib2ih7ZEUHYQhrVfvZZvw3nk16r4MTNdDfsUPFX4gZ1JKHDq8ileStdigl04nDf9UJjc79S/i4smkfb1xkWKAzFgGSoMEqMNt/djeqVBcJERfW/zEAK7OLNajDu15O4r28LQ0NGaCBMN9APaYW4fN31k96kBbUGvbK6LdBhkOnAptyfE6z1i0cKNNggLdoylaSWYgxKF0tesz7bzqNrtp+1ESAHIsh9HbxzjHBLfMKdtgudjfr+ip5cE/rahZORSLjTiogjdpnCD3CgwrDhYDgGIXeTutg2r0Yn1xNKMfcVKBqzecY2LH7tX9h/MPs7GHTpRhKTUVWR4ZbBt38LvMKfptCyfOXOxXmlstp9qRiJhaVVFOBiZNaKMwRHxdKINAptTURcsb+nTAL8+qgDn10BY50JW788RY/SMOU/OmNBsNYa7BE8rr8FzuHjy0lGu3Ghw7O0QIBCd/QoWUr6cnYqPWvavwhdvTzat6KfMR77UbcUAyRN0dHAQEOUQZrqOnTd+3jzU0Ojj5+f6FdytfdzyZpcrgS24TEsjV3mapWcbT0DjX9buQeyLvSpTIdfFCOMTwnH4/BQKpy/68ss6r5wghbTmf0kjDmXBvje0J0vCB8ki17NpWCUuNew+Tre1w6pcvM4VU4D+rtCeR0YpAMMYjhiqqbmSUdvG18tH9RGy+AZLw1mGft0HQsR/klr1r4gj2AUYfuP/w2tKH9PVfx94Uf252oPsKf09tBGANKayXTb+XUP1QZ2jXxKt0l73fLtV20Ul6jDwZWYuW/iFPx/O21IW4/9Fusgl0XTSLrRAvNAG2w+GHDKASBCvBXDz7rI76iKUIidvNcBTpu/iUu1NQZvKKf0y9ozA8p5FVU3rZXBs+PALR3GmFflYHBuHTPUURptHA45alAqjxybUOwD0O5KJzN7eV7zVLpZ6MfJEqZsJAmSXktdIKZW+th/OL76S3SEkog3pWeIzBs1HKGQUA+PKCWMfeZd+/ii3CGNyHCuFqx9OP8IBcaSEJQxsH+QcZtVuV2iWku86CN80hCT/D+/p5IV2jT0IKqVC8QtsGjpbWndD9mr1BGfNmjaRJnVNQTT8VmDrBcixx1Uvl4aLIdXxtLxRgEjw1AT6NlaVZqJC2yF65SRZc7H+PZjtBVK4yLGik5quTiZ+px/GihzAlT4tFEEyQqeYk5jJXPTHOGxRObg/IeZ94Zzb6uCIroSPfdgLA7nYpFINGpbMgLF+noEsfKuGHlB8zPeG3KX89jx11z4Tu89EOhkqv6OposNtWkILhyJkxRwvICf6Ad6yJp2+kYqh4M66UMAlHktvLgsTmqw8lMuqHMB8s7jXzqW6hdIlclnY6Kmn/PkCQVY4Es3zs1Xx+DoYmMU03P3NGYj/zG5L2sRgtSLUwqedn/Fy4I3jLDe5C4SV2NHLz4xAxBFfcppVV+eA26TEZhXAKLhnm7fuQmT8pfOBBbh3ebHgZ3V6BxxCcaKEcYl2zrla6mtTjYZ/aZjpeCIPhZJtwqFF1F+k8HA5sMn1uGyjLDokmVqXvyI6/bAYj/WKQLRKnD7KAyWgsCef0Byvzr5nrXLWhN8TqWnlDqTY59oSg6WQihg03M3RQrLyYV71CoVLKECJK16XqNbdsTX3KVn5by0gt1ah/g3Qx/65d5mY++7pUJunI+bMpJg90MXlJqFIKOX4F4Zn4g6MuGE0zhZey9u0N9lWxQAY7nBaiDYUsfqfI89IZkyP6LDUE0ivcktdf609YKREelaZvKFOjCWJjYZGPPyCnajE8pu747opzoOUg2pNrzSM7v0IOO4PMd3wmv6tUsZWFMff5zCDHmcAyC11qbqUD8aCE+3WtE6/rY6aRqE0HgURTgEwy+g5YQeSCVcoFjCAwcpBkRZryrOWdOckrAtYDIOdDPflOObh67NITgHQTYr9oO5DTo45r9ytI2Xiaa7FErLRQVlAe2BIKPXAMxY3nuy+P/jwwxT0P2L40HFVAIfJ93jJzbi5X7N6B4XOY4dsQE6R9L2RZqn0IQngZCU75rlbXCnchcawPL6IFaCB+MgnfQ3euvOfG4G2YGvPp9PsAi+s1NU2NPVNblq64HujahMGz145GWsKRWF6UfGkAsxISAQ+wN1ru0qegsdrWktzyfBfYd7Ee8EQZsav3qgv5FBlLJhU3tqAF/3zemrljf6PYtVImyDQCkbwQIPiLlOLPEFs1KTPjVoGVmzcxbWtwn153yl/rHFOs0vsV+W5eS98bTAfhJSg+EyZmUzgbYgxe0yX6i4DGRlwDoEVltNOkhfltA6Um+Ydr8wKTNshlykUi9z7Midgmg2drPmCYKXbq6u2ehGJQ08neAQU4x+Gec585QQWwalPFBp2O6ntmx1SyZUd6UuUCzIaasrY9Gju2O2ZhvCIVdkaoPIBxQhndvwTzRVJtoWKEYAUcIFw2xMq9odvciumNjRwsVS1TyMFtrnCUhdcN9ZVabwqpWXKryRME83U6ydFQtEweNF/BV4WxT8OW+YGmy4cuxufVMuJRmZ+zqjLRB2fif09HnAvha7sKA8KwS1Dn8BIbyPyH5gKtlmiuoKMojFxSDr8E3qZHUkM8C57NoIwqhcUqkq+fS7/fea4dPGE5nRsif4eQiNEGvQcsSBmcwG5P/8B1QRh4IJxhR6PNqqdINbZPCHnhKDSIUutHCkKKj/LO64IRc5VJ0l/v2q2a02N+uNStCNMVwSsWM7ZYyTARr9l4BSbHRwFu7i7ZvhlkmTVg0T4cIN3limrERpp9NBG/HS3LR6RGpUq+5j4myvGx6wsSPGe3VG3Ius+l1pkKCRNNfZLm/EMqvoEHfSf+PzuxHASi3DV8W40cinuuYJaQfe7wUNVQ+9X5pBf+Cpwvi6BmoeQ+Ddm7lnbPEvK3GAdEHJYDqLGVgaYtz8yk6bvBuSPHi9dr/+Hl9j0DterVi6Vqk7xwaHdGvCvB3GnWADo+E0eYRSASGlNkHs8yqs2Q3UHpY6AG9WR8ndXKDrhNfrRVozCCJ6Y8dttdBI29KyQyry8ybw1oPEWkA7Gwq7iQGHOjTKJzWUBNqAvCtH+tXI0aGwfpjX4u46IDoBvtdx035Fs062cUjjfRhN9Em/bzDUX9vdL+arV3m1aD6AgPEye+1CoWYnAzDyjU0wwAQmYt82cWhInM1pui/ZPRDPond/vH4r3NOKuNMgpfsLujTbClCnY2bqL6dpkCY3fWg4TvomKp/6a95w6IgCV2BoAQxz67aRFnTBhw2exVjtr4VjmIaDpb6pOebGq7tKi8BDMRexKq54THAub778qiT+2N0LZ+uyU/ppD25sRzzK6U5NAn3Hk8T4CqoVuDkVZ9b5gwHEn+7ITPpn5f7aVoXVBhfsc0klKv5TdljIiwSfhFcQ0Y0nxBgrd7q6U/uVopMkvKBQlTVS5FD7ARmhS5HQGP/wKJrnRI2Xw/Qc/XAZ8xC2mh007P6u90G67xS/LKTd0Q+x3X9mYAkSQALWCaFJw/tcTqej1lE7Db/FCtXxz/rlkCV7h/PsCA+ey1ArheOXa7VSOcGPPzYkvvLzL2i+dzNp5HI2P6HOeEhlNffzfWKjRt2sfP9WeGHNxVibPh45+QxyccogRRhKRcOZszkOHVFno/zzXB9OP0q64pHFj/5qo4dY4aDdf/i5ZEWeI66C+XsCmjQnrkFB2jkZ5n4W/WIb/IWmf/iRBuB57ejcl2fq+xeHwnZFYTYbn0t9m4Yh4aektqvCcSbnIMv4kAYQe2WMekGosRhB/Cvm39y65K3PAE6qdbWLJpY7nldUvrcfjcWc3kg1dWjYbdClDJMVGSrsahs0V1PcHAR7kIHg2XcD4OY1hEKyF8y50miuM8rWEkawTupa4WU4lO6tU1M1XQW20/rh5hLzbEw8s3UxeFFBBf36/1wMOit14qw47QQA5cNfy1zm0c/1c3K3uH7AcQXOWaG3fct09XR8Cue7Lml4QwCc68BeERWBfck0BtNKtxmRnk/IjTE2ADG8WminPei28IIPrk7BVBRdLBbfAGmCVm5uyhzWfitwiQvdalCD9188aNnRgLE+MgMG/81C5RaDe/zQuyI9jroVh0xZVqJnqKNNwZpATB5klsjVEAKjKsetgEyKwvl1Smz6tAliYm5eXNNhKnHmy53es7+5E2D6Cb909fnIdx4JHNX6nLkmLqgCBRZMfvzur1CBOAoxN/4qPG5yIdiKtFlJFYh+MDCZWrc3DAiWdzBRyx10s+uM6uLZFLA8i2nZNQ0LNnOszViOw5UzR3o2rCTG8bLCOOltP0Br1wfoq8z+upVn+8gyziqACrxT7uuKD0/nnTdDnOpb3iAOFdRxfMNZUJD2BFT85GCruf1xCJXekWrkHtEYRZFB7wAKWLN/cUhHqCPh6LqszyZtd8ujreSHL78Hwl0gadOEwDSby/r/2cKHJs6PbTWg+nXcc654D1AK97FcLMQ3Y9gTiq7ILCmoqww2cE1DOxb+5mCi6s/Cjbczrr8WJFDWi/Z/ck0BKKKXJBip8PgZfXjpNXS8wrwHZHcMxbd0IA2BQbu/J3IiKJ4Dkku4rOq6DIA1jelTQ37U7mCWO1dSTir6oxdmcMrUz6RrSuOHlRjXVy2rVIjZJbFPbDVRyKfB+u/wxiKuBwYs4mrXqEfdNqocFADXdmo5HvqrfX2rhyipsr5RYUucuSvyM9BlIB9WxfjBNRVUAjNQuioetyvYNobEk88a3lfvKtowZvQzu0LlHWf/wkl867LvnD921Ewk6qZ9rMp46OIQxM1vMdxAHqiwPwu43Sk14bO6pXF+uRrP2W+bMI1iEXT3fGefz4jFOnrJhEJTFEz9X6EEdT1E4t9QvxY7lDOZlAQscwlNuw3ohgXwVbWgiMYfLJsGM8HuTfBg2KvMuynYDXGBhmkfEl9Tl4kQW2nedddUqkowomj0fqimXzHmZO9rXn437eehOD7YY/jPXDOZMIs0ZrMGUSw1VJxB9Enw1T9qDyK8TRorxpT68ECr4bIdjd/MI3ZPyve84EGODkvHELOdYHYVOI6441OTZECh0jUaH4HdmBP/EGHkT+hW/o/owTZDHNEzNHVHMF8B7iDKYbLjIz3Aof5diJ5sUm3+6jznGXdGN4KOX+Pb5DwQ5xaKREwT1bN5ZFGeQWg8S7MuK2L9am+SeUja7DrNn6Mq2Cj3JbitdcHw+FRmezmQj/L5/FTkO1K+NKnYiXt/JE3TXfLidyh71FoSjn7xNcJBKhpPZP62Ypn2d6EOvz4NHKPXiyH/czPpx3vycOWSfczKpLscYMCAM5ixGAmL0BNJ5w5fgeRp+V3W4CBnMrjsAEfOKGIoB6ve8778wy8Vmoa1QtCzsHL+xpXuEsrA4vkH2VyNxnKm31nSUZEJKmtp+8sF4LORzuoYYsxCli5ZRnv0g36QIIKO1pYWetaTI2/02r2s/OY18DjxF3kJJ08Cdkfvlq36w9hkn4dJ21AhVG7/d47sg7nium/6DT0kYXxTkXmqrw9njuGsaZis0rpAvyMTI9ZsZjYNYiXFdYVKnOl/IjAZIrPfV9wtrhFIP0cyySUcisvkZJpkr4TZ3jMJxEDd4qgJpd9VpCwt10RG1WZSby/4RFW6jRUs7JOgiFvK/TItohNFHdGGuCN0oEWsrlo5hiTjL8HMHHwBfKA/Ke6mJBeVs5yYiJGDk3iji3NW5HtO9AfGQT5f1+r/Ff7/QnVeXTBMKzAGnHP1gItzNf8FuaKQZmiXWSmiqmkzX7AN2+R1uX/gCCTZZJS6Ki9z+scO8xdrQzIqJTXgXFSru/7YKnSBrkyO0otszNwrYKi7S0E1CWy7iMnp3d4XGBZnDlx3kjGvsmBZVITz0vqWbOzwXUeSsnGKXFmKr7QJSMSICDbDwUOjj4qC06J4dSDn2iqj4cWo22caWqTDp1B6xEOq+pB5LR8LyYf2NyQws3X71C4d7/yc+PJPCDnhh9w1M/3r/EQUDmsfO0LwBR+9LOnVoeHjueYS6qea6sNeP4dbfQlB937tBZtsK50L7jQCM3F+NnNw0kXRqTHIfmmUGuxxptty8c9A7ciWqQ+H5SoJaAj64K6h9ioiTqcnQ/wcME667ZqV4b9nGbhHArQ309KjBXEVpLKo/WREHPshI8X7veAd/xlsQYChH3od6hwQ/Ij8EjehJKX8ex5Phqln9fKLBBl1OQcAN95LV4hds3PLNNoHJk76vJAAQUbaVMRG4HIcosylRQAsvA+E7VO3rI2nzYd1Z2SMxNLzEPaKm+V6YnhqSQoxIoyul8t2JxZjlie9tc5EVsH7M9FU9kNabeDgIf8W3jWohYw+BK+cfslMVv7sS8jwlXusLhzKVqezKB2UOTCv4JAj88YJqHYA1h410uZ7oqPIxm8ion3uZnCUsGkDY6PCRLC9R3c2iw28FPeXW2FOBS/NUmTjniP3mawjOP+qzpIRLWGvZCWcobWfZca6xKIQzwuXYTqosaSup5mxyvg2+Unn5ZmPLMNA7gunmrMk00wqm9txMKShb+ZcETmbQI+Z4UxvbqXcxkqkSD/QJc4ogfQ3feEnsoNV7TF/N5VinRsU9ri+sOXgJZZZwGFRtwSgmxwMUzD2oJH+MQy6vT0VGLuf5H0+cT6zPR8oS5XCtfOtl7PU1a2A0CStaGAhnVRcaJEUlbyRUNhv/IE/gI9C0xA7WpXEirqi+WiP+wQofdnOrRKqxTOYbg6wU0pjd52l1VNezlwO5OI6yVpLmaEPMfvPQHkuQjHQS9up33wXNDto2h+o6uPeYtFeGwMAu7BMVoKuYnGfOc4Gx09NexuYZkR26LJgqHydB1OcE4Fj5A/JE5apOdMUNQdJznUPZ1P9yQFpOzk2Qf5JooGCvOnRqUeqYglZ5DvwHHNkN4ldJAzlfI/P8AVnAK1px95s3cV1oiZJQ0LIG9xYTjysfS5SR3uXfQiMvYrKm74egl05lX9rQdAsSqluJMt19s/YGySlX/vACZZQS/GvibILLs1MsCLqe3IONKR+LPKjUFnODMVXsfkroiBW/L7Xw1Z/KPeeh0vCTduU5tR5G8XM45UeEr0DOyNr1W9XaFepRy/EGGpCibzFU9ItRS6kvlvnHX2DCwG2wsGKW0sSmK+TXMm0GlL6D2TKh87PXJWVBe1pa+o527tJfpPfdik9Ym6p6Rvuo8ytqwAiNvpff2Jo2pr4NWpuc8xx8LO1zGJhWurSm/T4ayL+fz6LtETMX/F5dDEHOtFt1O07wKTodsxc2plwVlcmFcxe3mYnAfydG0rYzCg3LCjzb4FyTbeJw58HVVXKR0grP30/+xv72GC1s2NYjao/dB4YNxdyIbNLhw2NVRgopq5g0FJQFW98d6DBmXRMqnL9ogP1/JwQUbIYI69eAB/wDv1gDcorY3yctgmIGmO/0POA0q6GzaA1Nt1ZY/GFVvP6mhl5AZZAnzI+21ttd1XlH0yvUsJxpi6tYeo8OYEH2l3ZiLDnhvlzrX54V0D4fmzPlsatW6EVMJOgwkAMMf3xxfbJCN/BUoVuE3FA2KIZnXog7H7hWaRhZJx113dnSUDpxpqhEQolPRRTlEfsU/cPsoGAi8I7qZrwczJNAcjleoYcTF/BInwnE9+0orhEHVM36zjYkDmHmkKSxyAwxKzCwh241zMmhnk8HS59UqF9+lkjMONupIT7uoZiYiZ+ddDHSdOg+c7rYP4xXs1BnmefuyPDkba0H85IgwHD7Pvly2+RP25IoQFXCZNoKFkVCWVDlefUC/YTZvDRIvai6D5PJbqMGOwy2WFBdPKdxy0no8w5Y2jRA1XNAbH/MFQ0j0lZLWi5mLciMt9L92YNPoDX8uKzteJJwWrfEDdIbZ+JcvEPJO5y/7WXcBAicL/SO67PDl8ir4ThC59GgismGJLukMYCFZkpdXSD8oUUABTj5VcOmxbTWU37nIyi66drAOamSmR1phz2NvG91vHklmvhi6em1dDZy4AeMB+/2P6D5A+yFHHVj/DkBy/9wi1oi15+fX3PPsY2gNwpP1fdrR8x4LZxdAAu+1N5rW/6dDKrHSglPOmAUVjU7pJXRoNFltQVAy/YMv9qH2X6iQuPYk+w79qmQZmIdqbdtY9ZQsPUpJ0mA1JMmv/xsbhmZSh/Ayr3yn3NLW9KuWNi9pHjFIOFfaWvOIDJm+d/Q8OfM/bDvE3hGSu4oPT89v9LdicF6sPB53nYFiwqPfaSRmCPB+SpGTsWwqvf9Vr8+wmqD10DIlNFoI+5F7tkwMHghShQiiFXmLPKg7rEH9DOcfJs+yyGXHrunyU+zSYEqwTl0F4vaM+dY6+aihjXq8mN26XL5LBlcOo/+qxFb2gtjx38PdnrxxphYY1teo3zZM8c+Q8hMGAHalfetk5N15rno+NHzZzR9wSoPa9Xpr1xvjqI3MEnFaSLsjStQOFqtKVeFnx+l0paXHcDfwqsIzsZ73q4Pxu3upiqnlgkqDqpRMtlGmynG6XcwwYILlAczYgIvl6FqOvGm5dMmNvCKzU4aji+QGxVoZ0dIwMbxrW0IxuGEhtvm+cORvv/QTtPHxVQdN+cIV9RxebhcVxcU8zyV2R5uNBC+dVmInf5Ug/WL0p8zJYwsWhLCTO4ZrZJl+G3CeU3aHtq6bwGpJaPaHqcjaLXpP62Vc4KxvaEYUEpI002kx0CkZEVrFUaJ0jvhp2rwspn9onpjdWiWDOyTWucO5wEgvs1eY96Iyq2NZYyWCiNeAzd9bzD0t9CbudKFgw57u7uyVWoFAUURLuoScfp2kAkP7Gore62j++v4IZPu5V6DqyDTsKp7zxCQMvtq7tdXy2Pp8b+LsrQEJ64HPbTyrNVisuxkaWaIsmfmzMrDT9ac72EwwIb5rDp8OmuQbQSsHiKmKlV2K9n89hxGmIUObm/tfhUjn1SYJ9HfxaB6oJzDau+kQM1ls6QNqyaovcQSDr7U3grvN6V3FO7hPEJhUGZaCRHILt4tw/1xRQWwbMfTC1WI3BFv1q8E6/LUl3GfPKOX6fE3YgDW7QP0HIQripwoR+TE+VCAIoVmZJ7PiR/AWjKdibyR22ZO/FTNv2EiAIkEVYVWEh7nA30apAjG0yAQ73IlnBHyCZDO8uhGDqwYF6QC3PrM/6nykqzJTkJJAJ+i3SZN89pb0QPdqITehCvkYADjFWNK4Q5B05XyYD+5Pq0TnRcxzCiB8fcsAP4DdwWgcxtIc+aq0WrPpT6pEf9Zb/eID6ObVvdBLxtjlciwXNbN+P9EKnDP72qgMcZegvzrXoyTqB1ksB3BUtXmCQhv+Kc4zpz9Zy53Yx4xIG0wuQGoBcMPJY7ztpyZl17n9Mf591HJuQNAD0XdITW462iWwLMRqgYrm2fwPvcR3EFVRiBeJy3ituOt/3fCt3W1cNSF8qa9NF4mH8NLtRE7XoUafr3tjhDiIlUlimM4s8ltBO3HLG2aL5fTX8xNh120d00zv5AFGseXHoTVcKPqiLyFh7JyP+F2IIxNacRlvCJVxDfQLcRE493WlaAdvx9gobcPriWf8BD/Jg831eT/ydugV7yJyvoPq+Ekx0RuHGl44vCu6+Iqw0AFgfzgSHjrwVAm33Po9CXbUdDXANXppKTIVwfRUxPoXSyLOAjK8jLWdPkXkDurkfcWFTdKv6DxM1l79gtGrAifzViz3iuAbYeFmlaflhAjnHU5YtJWJmATmgq0+3oZ4eBnspapAIdI9z/gV0kQAiQA3FxWALfFACr9nLVB2rYvh3qgIssWdWbfSDbfVxMlgLiL6WPCB69ogDAIhOvoRAFdS4z3wMHCXTN6hOEoq2/gI0VX9jeb3CBxEnWMapK+441fnOdadDDBR4H/ROw0G2Vc18f9rPnysKSfaHVQi1zLtccyR+9p1qil/sCtsAXmo4xwR6SwEMavZbvq5QvCz4XXI457pOoLNBqcU5exi0sfXyHM/0rwSUTzkg8jOWfMpD6fg2IsKQFASVIzuzd4cDsI3iBnMlq8gWV61Tg4GWcPI/hD5j1rCHlSn+9zDKRMdVWk9irqTjcG52NITpO+QSLdvnLLHAxzlDm+mCnUM0aP9nJ0/8XNrNKH/KLtODzzDFjzbHwxf1qNRhG1+i8DXacKFZ64uE2QH8Rb0WGJifdBgmGKyhyUSKotCcScQn/LLfVtdZsRzd9q3m9zwmQTxADxRkyjeRxokKhLhteWrnwg6bCREr+BWLykNOQiHyW950hKZjhKusoqQdbTxApAwaLCA4POZyg38WBqPfwJ9doatMWX2LWfi/e3fJIcLOO4glnRgISFh4onGHAsCuLgHHRLMJYAWRE67Tu4a+BQ7IKiBNkIUrLMoG2BreaaoLPBu9xuxD/y9zPvH0Vtywvg6P1vSYV4UTd0tuuSvGv9KTNuHY1IUHoWlgLTL5z4d/6mirESERXAjumvlm5ugtokRI6WKfCXVooxcbdPKAkt6IYTmCeWMoh7t0IDknxiHAi+W0dvkZceiuM2GbeGd0aCNZ9myJzyfq/uEosJIAct+mvR3Gy29qnwFYKKMVNIrLvHX8fZ5QE7WmV7uBpjqtQ8lsiwIADqNkLHmcxfKqqNqr83l3MUTzXdq6O5huztx4p3+6ja6F7nZJ/rKi7jTrADnl/NWa9y2YHwGJnujqQilghhk1YVcvv0gytzYjloFqPt13A87/ayrb4HZIclm082kiwMd1bWY3/v8xZj5Eke0DxNH0sHW8lw7abIyzT+x+PByYc9E13VB9l6vy3r1/ptIZw6XQrcusOXWLfa8FWDYJqhxX1FjgbzEtKfW19MUdxjk/OyEK1+OWnrOLQh9Z4Op9BBMw+XWszuhOi7Bu1jqfA1X7l3pTUS7sWl8ySG394qbGwCOcnElxRRfm/kSdGckvpp4STDosGA/CyrbyMFED88ECcztmxCa6QgAiuu8orq62+6hxxFZshImCNX6+Wlu6hgpnd6ByM+D0CcJpFxH0YCOkyGqvuHI9t/ogX9GCppwZb1LAYXgA4HyeaK451WyfH6nUk/P/aVN8LLDM9EHIHC2gHGfiXCOS9i89UwI2v7UolvRg0yu/ZFz0naBREPRr3Yz7ITDRqFtJBDqoYRtIxKSRm46gnZ/rDpka/XMBEDi01Y1TFxXL0q0mitFOLk61OWEwxQ1Lv7WLIRlppcKY2CZ3yMuwUYixdve2D6I4XEUbTMPsaojqRdh05Ro9D2/tcB6nADNVYtyECT4ahopU+x3lydakT04KUq9UJ/+n0pdPUaSKHoez8eF6eR6dPksFagTc5b1B2hojvb3QoS8au41wbVcQBC0tE+Rmv9VwJx62cWyMEYxJzXvKDTymtgdOu5YeC4X3JUaFw1OpAG4kshb+2xXrAALu4rPeBccyaERo84WkwJMkpfZaZqHKMfZ60MrLqa9i4OEPGnGdTohN3fOWbny4omXo7y1zkeon/xfMcy948AeKqeRuAi0wyx+GZ1OPwLRk3aAy31QGx11HV6+DOv5OlA2NISqwwsgWn4DBK8vFi2XVpnUsdSWoZSyeGvWR/tqwDauisQjZtbtPu44gIn1h5f3zfkcUWk5r7tihUgpIaLcZFyilGSDNMrNUfbfczow+mTZgPnc92H/to7RDChhX+xlTWp2l6juEU6FlMyaju7nogBbbzkeTNA1YyOLKFamOqyt6lkarIVnNUhPRlFF5YZa1FibPc8Pqz1CLwuJcMD7a17N3sPOE78c35F2HYHCL9MBbGemha7gfn53oUIWDJ/fYhrxzfNq5t3RbawrRvFXu7m6Nv9NR/UCMiyV+tTFUN6y6iZJWo1z8A6nMIepBMqZn/K66vZCAr5E27jFVg2FlrlJLJ4wWY1+suokrz7TF1jgRYVAVff1WcS5kzMlMil0J0zrgO4SctrwfRDvppfes2v5PvpS2dk0OCW9gd0zJxf473GBIl4R2WvJk3Ul6q6bdC2eIrgWq5sUQZx2CjA750V/kswXHDoiSDMBH4VEZCZfYO0eHHkBHThaiuQo2acFDgJDhmcHYQ79VJfyG6Bhd7jVujeajxpHZsarKkC/d26JZWi2UanwFGXwyyi/7uazUyrFMfDBLvqD9V8zPFX6UHZUdhX1kJ/k5/1cQNcZQNgGtR+xg5mNl+Ive4SAy1aGXwwY3aECZ6Roh899Z55Z2TUOIFKG6XjagAfVo7RyrU2082ldDYuiMCSow6a6zqmUxiG7uzaTDFzuiBKKUTiNLUtfa4/8uvv81wXQTJkzi8F0HaT/Q+qHZZH1T4aEtFRjJA1+lEDn1PU8dF096crgClF4C0euJEvfN/4GVy66wpSHUGRmrcXcsa5XfOXUQQmWKHZZfEjVJmNr2Eeu+xJsrmkSEoCxcmtBi+0LiU3GFk7U6nny/cU3wQVkUByrUUfw9+BS8G7LKp2cR0k4kfwkpU2+y68Tkp7sHpF7qOXoh/tJn4VUekIbVuIYcV97tGQV+CtYzCQSq4E67WM0akSOetbHj/UJXfRtVkU3Ri8M9wdejK1/WR0s/OYDrXEYL2JSoU6khVb5VaWRHnor0v88mlaxCDWcWQmF71cm59Leopxx71vU+CgGYumkmD1GwlEZxelCArEUcEOfaW8BHUuUGD0kFigGJ8VLRKkAk++tEHjy59GoCmDStXsiBtOKyKjvzLtn6MqydRrw/FIToWADCISWFccqgnWaVYWiJPEFZ3r0QO9+alHbnoSLqlw3iIcFLGoZlY/8jtnBfKAEtrdgcNlhpPwvEGaVOMFdn8GIUwNNHomeh6Fzc9S5YcXJAMb8htkhAJSU+Z9Ch6XhQecW9VI6Te2viqEUEZAhxwew8IICKlh96dEnpCBQNutLWuneBHZ37eURoP0WoH5lwFHsyKLlmTomW299gd7GVL9W1S6340qGFykoUf5D4xHnsJBKHIh8yq9zkUWKeGiQTCTHH58dVkRYLu6YnQoyWm4fieL3AjDrszna6Jo5a+jgwamyqrhM9kp8cWHkM3Ammaj8lG5wp0OojE0YhoFcdyn4DSZ87UYkEa+H8VVzfabhwoTv6RoGi41xsjer87cDZvkhm4LT9jkC5gEG1+fqS/WZL6utCH903yb3WXgr4B3dnSfx3zu6gwZrtuVt+PjvIdPDuTqGhwalgm1AG02I/7NYu609cy8wUhYsVhr2bCm7hoQ7dj+tvngJHmxYlQKay7ESN+oFpWmUzPpTqSsXYGY9sVZATmxkmvpJ55nWDBdf6TWxKbpVFfRCYXQPuKoSU+fyTTYQfUv3HpkGzWHrF9y0/Yh2oZS3i+1Z7Lb1iYUWSxrdE8QjhdX3Vgm4LoJnaoyBBvLXVh6uPch2WygTSG6OSY8lKeMAWSgpd167iuwTgwHuzrTDfLDDzNqXlUbfuoIu+1vg2taw1iGTCjQQF3NWqZSZjh7vh56ILdDLeY3FCVK6y/nKrV0yS+pLg23UDHRIhDI2hh+9oZTNbMWTI7/bGCETKX5iZH7VseueLG5FxwTCCis3OsWXTVQGezsRP8I1nHCQr5zjHhE2AZmXks5rWDQiLVOiBlLh2dt60971ZyVZeem314jR/g2+LBcHjgsFKlEju5vwlzVw3fvIJVvBryLR5ykC+Ctkt0A3l06b/r7lT2Vf+U6WaYeO6z5F7lFIiNZY9yCJo7i8zwuoSaaN+MfPKyEch2OCn25rrFyhfQSetoXounTHzw7HBhISeBtznDx/h7omZAjD4msf1r/irjbKGAtfgp8YtXlrFujT7uupAwzNZCeTK0ugYyFN49ekVCMBvVPlgwrBsA+oa73OEBV6prw/oHIn3o+l00V/9AYiTVjeH+EcyBiHufuhN+cliBJAx+qUAL2Gin5HYuIKOcwvjzI8Imk485E3+tF9LMa8SeXMOEL5l4sW/pseYp1Nd6f5E0nTKChqGfs34gzVoGp/MxQ8xIGNAstPbcFMFdDrYbULU7rLR235y0gD6OvLnXPPWrFaP2jjFgxmvPh5AFN+EnmrC4AEdSzd+es48Xll4yw1Py5CN6K70zPicIwPPDf2vUFrF+HDsrI1gSZsl+6bQhfCAhSl3orAJsYElw2EEHPMWaNmj3w2ePa/tNGZkM115zmOVj99ZKVQQf4rFuhr7LrG6wnlIS5c53ve7neiILQuJIJuXedGXhGayJOJYwIoXzNYHLndAenXgq2o9+Z3syPcXFTLQjqkpMoHO5p4VJb4aX1LWLZy5cvQy4rtJEm/qN9yesPnZ1aWEVI3t4BfaNtvLRoUNo4o2M3ctR8mtApK/r5us7HgoPDG7CcVB0qRQVkdqpsGBne88ej5W5aM/ZRnbyzbFRKj4fLjcDCFJZk9Zm914XcTVgov7ft5XFJ6xuSgKf6diXanTcMgQ+iK0FJq/vw+OK/3Denff7T2HUPSSXasCv31kw3F3xG3f8HJ7ZijovBM3ET57RqKcp3Wdw+XHWYegpnaDxge2JbsoeoT0G6dWQNUO29GfOqrk1xwLAo7p+agJjzrY9xYCEHcPLBzpCsmwxB0K7bPG15qybH7Uoby/TqrR0jwaqPw+Q5l35Jq0pej+CyPzwm/PbFQ7kzLaJaye3GMqELq5OTS86TQMwVObfQ/Rhuq6/p07m7imgkOz0gTH5HzotdxhzBxs63JRmBfZE15/T0JW20CpfaBqSozY5ovjlgambR8qzOICa1itVWAxdF0iN7r6NvmWgnNGWfpW9WHFsc2biyJtSOXpATPTeeJZ0IgpTJkv5eQT5HP6jakcWjBCTun7KZrSjTrR895dIXqqLwDmi5u7D1/zCxLzJ6pjTzAH8qi2Sfe6GtqDjI7Pz/gL8wV/RMv5XgCl+Ejxt93PJ4wMMZ68hZmP/UeRfb3YL/B/sVMVfuJEUfLrJYYoruSbcV4JxD42E1MdibdGrhuuPGfHDvZwJ04ymVbvmfAwWnex7gAXr7k8MNm34cNTMkXnxYG/fe7yJDOgFt6eLXTKlyq/akNGOh9J5P/ZWaSMz7A+jfBdK6kdt5+Z+eJO3pBtQXCIT3g0xPMAqUccGWEJVNpCOwFyd9by5wDmRHTuKQs8UoM/FH2SZ3EGDwoIQve+AMSfQVSG7AR+7mViyDtJNEnslrueVvTVTRCYchkcX9UHfpnRu3J2AVZxOtTBKugvHRgJVw+r2KAz/wwwVydsVLlf53w7ihIF9f/aCIfe8XNfCf6YZRtdajpqoqGrALvcdq2V6xTMNYZhIicpGrnGB/oPdRsKN/WYduj2/pDaI4zveDsTOsHGKe1Etrp69Ft8waFQXOOhkMFjTTBLnwnXse3tX7QAtTmFqj5fJlNmecd3TvvZz5cm2zdDZjHTr2OGLGTF3LvoWKLO2Cs8OuHxn8FATVvoB4mzyIJPP4TZBTa/2qyUHWcLYMrch/lQgm5CjKpr9jUwSCRqBY0I9S+aH37VqS1ySdVLR3Amdl1AJTyRozqfwnDthE8SpwfNQM5Hho5btT29qPblOBe492W1Jh//kHP+JBBVN2X6qKPXqgLadGw8GjxjFtICz+MxwZbeFIoUVtFBU7VUVUWVBg2KzPB5HYU3aXeuq+CBlNA6AOALyp9gBCW0wnponSt/5pL6wDnBNxuMpNqe+AlR252kdZVFJDNd41loDh6Yad2O3WU+AiwNw9oqBT0VxOA/BY0GTr63Cuar0TswqdrPlOA+dTxO4YBIeoZ25tOobctD7qabmyrPQf6HgsnzbmvZRcQvdd0QHgOYSr0ifJBRX/rdxWM0z469/ITLajrXblhmxgQQSLZI9vtjK6SE6RqXPOtc1j0WCWWq6TP3sJq9TC4xY7myv4/8Tf+LuY8pLQLUAYqkshaHkp+gWTeXpfyMjz/MjRq/r1ZJajTu3/NtCz/eMZPZ0rHqoMc/bPTn0r3Icm2dqJ7lArFmNdcITJv9zNhAbP3/JdUX/DhAA4CujLWxK20r9YokMY0R+AG75JD07P5VCKI+M9ZjbVuf6NY2TNpZC7unpEitpoL6mWikpHKPrsGJY0O9/pNfs6SUv/MIafrD5l77D58krynC75f38OIgxH9hzMk7BzjhZEIz7m7awhuFcZPLVuckOyNKEkoJeYlkEJm1V6RsKZW9Db/z0W9sZKx84qNkCUj4cZy9MghOqdkbWiXE4+DkpKTgkexRtkzTjvYEtfzHdtNgpveXbep8IdxwFc8XucRopsFML7uNSkHhMwgh0y7YB3T/96aUdoAGtdALUZ2SGiBaaijOd72ramLFbiiFtHX/npvT4FR2VQv74G9C7QngeVUrSQ9OK2clQHN9GaKUXnLYD2ZSsZiOX0dyaGuBtNahkYhkEWnF70bB5Rhl7a2GrLsLLKog/T1486zIViNrdNnXbfwxlzK4wdGlrrZXFiwzfK7rfYMrZg+18CIRexwxbWBpcweGtPCCo0iEwmg1MNfsHVMUn48WgCGrYL3dbpm3tlM+XE6G3wunz/A49OIMVzKGAorF6x8bEknNLAsVGEY5VnBZvbPjuvSTsnvVzGupUXcbaTXaNgdOq9kmpfs9APZlO4KGrPzD8rkpKX05mjOS3fp/H3odcYJDJDpUJ6iL1uepYQ7lrzSpuHTQHqgMVL6TsTLLfWvGaAGou4pNG1H1Ykcdt1tbd2c/xdPCWSIKcwI1wWtFQccJiWHjsFkJJkU/VdnwXTVQWtBo8gIOlzFnBrRgFd2/DEm//C8IHlz+n6xIxOW1yxRmzfNLB8kZjsDgwNOKcED9k5yV9UgGsJ5sAUP5G5TDXXVRrBxHPh6sQhTH9dRE/kWk6iRxO60gG6aOoTsmbSzViJ5pxGyqC1gh9rdVHk0CqwW6R391QvADzyHW/56Ketpo1KyL1F3GClHi+4rQBN4DED8zxDVkLdKfiiSfoBOS90/lVNzkRl1AUsxrkQrZA8EVANvFKVeP0VEWto0WQaLiVwHROsenVaMJvUd0eftZbOsb1G8TbdkZkVuJZ9HQVNo03BE9KW88x0lkMl6jKHMZ3tJES+qrXbYR9dSrhAM4gQGw5h0XSDEK3ewgB2zLMbn3btuES8YeiDcygP+rwDxE0aQ01ZlzyfGP6I4UsOC5sUQJj1QgrAeXfLWmMesHx3Pa4x5MKs6SsOsQUo5fgUCL7U1nTvVqBHtSVt7+8/Ftz9I7UznBlayo2ji63rouDwmZEni09cAa5pO47/8ni/9nX8pLlfjX8xekiYmSk9JvZvoECx7BobHRwCTVUCN9480oZMkV9lFzoWteMcgVz7CSH3W5ssb0B8hBDQWg7FJZdTzNZs3Ov3o42QKarP7UAmhcugmI9kQZ0p6Gf+mjcc4twj8PnQXxOGrLTBgDgMB4+yZRciVZaf3A0YS7cz9UHAE8HhHUljaD9UffZ1gfMXhcJuQ5Pu4432igb5DFljxveXQ0SG0ZaO8jesvIDjBeYTJJmaMeDbtgCiHh4xq8nkcSg+/ICvNmDfOn4pTeF1PHQyry7n01JI8XQP8RLYpFouTLxrsGlubESb76Y88yPBdDl6QRhssjZ0ntau/LxoETN/ByL2/oled30hVvDNc6sYZJwhmZ1Lty/IoKErHXYR0hxIu2MvlIpD9eghKtOcLUsRKVbLWLkJCtcRWPb2BSuqjbHWcRLSIJjbtZlUSq0Rvdcj4hO5lFEcKiJVMRhStlQdjqcCUbW4b2Vwx+95Getr8st07HjUJUzbXpW4yq9RKRgtATqo+0+g28XO59vwntR073GJNSTwBWwzlPqDpURnx1UCPBSmBbTsaSqat4pn7t1C6IRG1SP8uNc7n292Mi9+qBOyKcK06dUzMmr5QyT2zYc23XjoT9RAtcZh7kHLVtcRC9+hHlY2j3bSfAvLre2NjFFxF2xVaUrVKEHhhtFrZi4/hqEz7m7bAEfil56qtBCSB759DZgru1A21IOP/hGX3ITd63OnOQ/22P3LaW4JlUj9XB6niwsemwTIlNqIC96UGVrWlMPUlNuLwwWZrVl6o7FfOpkhDTIao/x4xGPRnLoB2+YWXw1WhpnR8XawTNw40/v59VphbsAPHf4igT9+wMEDkjHnNujOGNFH4Gihh1M1TN5ztBicALjhUEl+bvE3HLjSk0KFM9Z6tAWlmiqogEeP0CgPEQGtkhON+OuZFSF/o4qtmpC15hEQDg1cZinTJ16xhNpBQHi2XWVk3mmXJ/mKfWUfrFouD3NWzlpIeQtfwcrtEHPgcJ1TgMP4VWiqcNmrSkPkI3mNIlQY/O8Vd4DaiRTc+EdikJYndT7A2QWbVJHzJwlGam31UffsfHjhOU992nuZNCYPzp/jts9NNwm3zq6uCZT6TPyJpVpNyqkdKlKv/L2vfx0cz+RtwQKe9jzvpm84KKKbnRT6QY9ivw3tmKJE4opQvqPKWVW9tr4NOPahDHb7DEFdo0pwYwvabkUZyEBAna7SD7BiDgLurz0dHEodtsTrminEVHkjwMTE20i5EVPPQy3xe2GCMRHvDIodlp3jhZNqgy0CqVNZm35sXMyt8SKlG7yU4pEpeLF3rCu3ZfirXvQshA6Jt+K7KFDRFtDmyq+yA4bbjky0oxZ9/Z+gS+YE0pJ4fiAn04o2TFng+onzdUq9BpMOTCav8WldHfLncjG0JHpxHpA3EFCx/SKCH8VKPbIExneCHlj+0ikg9L1UDxytcSz02p57FxtG69VcCqFtbnMhi5TN9mSmM3zzfQVraamoVEh9vSkt6pCv43HAl8VeAQHvj3huqLTnHcp8i++50CflMEmCAv6+G3JobFofaC2AaqFpesO3aa3M9KK63KZAb0Bfgf+YdHDjyak0F8aICeLTJrxSUFuHTlvlEAh2xZvPWVRq121uB7+qiqioneteNCERoBGvZG/z/uQNMfC8bfhGtvyt0InAnTC6NGQqb9DM41ZPd0Sce9OCsRh99cnWehBjV2SWsxVuuJ/95vopMM6IS6BteAndtrUmjwZ30kTfTyAtBicwjZCKn54N13xq0EemhvXfvIR26UAazzJ9b0A8ImuDzGihQrHtuLihtKVEibi0w6IzrtRK+GzUKB2n1jUAFhUezROMyXEIZ4owfhTjW3rO1xb+PUZwSk5ZQ6JhqCo41xZmIkPmm/4ZRwIiZogiVK76x7Lzql7G/DzzCVt8rA90U+/p7U4N8mFEeLe8iKDwWdu18lpqPHR6BQXf1h5K0xWOzUWDJ6TQrIzuLzoaXb6xOWmdrmnIOCSyoo/fWF7M+5HDY16peuTQakFYqba5TxUrBlPgTNBALUmWma7iQz+UR5JlA81fRRyMbVDOl3A1ONrESYcrLOcyaL6ApsPwtY/O4RozasdERXsYWRP0GGzud4IiDaQTk/h0fQYsEdPfARcVg9o91PpzGnnqg9NT5Ecjla3bn4CzwIb02qxRZWJmEpnK2x50PCqmiby6ta/IPnyCbd7lMjeQ7oaL19hzH6ps0F3gTOxfgcK/T946zxd/HWXbwqmBIr6G+gypq8mFrzVBar9XY5/3g9guzECpr3uYIMfCvuUwxPGmKpy6Lnw2CW8eQd78ROBd6gR7RYhrAoXFAi4QsgDEf44T1s+A2WV4UFEcB92yQYM2xtxzJenyqPRB62NJSHm2X86j6F01Vtdif9ypiYuj9710WRZvrmf8D+PglzP+16N8hSa6EPQdqjTTiNhqaCg53P9Nk/xWGjlMB6Kt3aswy19uMqJXwmZXYq4id/x6OymAHa0mPHCdPMGi2v49skgZxm2agLueONPktEvPHPWZwisjx7HTgwiyU+9585N7BtcnXBgcCR221b8a0CM/7Y6N3Os5mPUrljsEWAiT28+s4/w9QtbR1rbpn0mKAu5StM1pJ5AG+iTd5JqppfbrH3oLG/th9m8w7fgDbU+RiiryvFw+3XtDB3pPWNMS9ckYhJYowxZf7iiy/p7NwFAglKI7pUxrnZq3tDTuqF9KI1nOzV7p/FDxS4vPh+Yexeg1Em+HosoNgNHpf02e3sxPXrO0x+CYo17iInltmSKzrOtrkCS+Fx4CHk2HiqBWmoOlMTLyw0eeigKm5ZCdk28gAjY1SWE6s5uxr03YOEEohsNKU/DwPJEF1Jb86pv2Gp6TTR30UgkiBID6xInA0/Gxw8wrc+b5tYCNbD7vp2NOWNaNEXau1T9KI9JkLOuW68al5sNOd6s2MsFU2YZ3wF/QN3GEquwSZ+ZpsJTOAdFMV3XFMv+KOLCBZPldsvpTQfS7uILVhG2A6ldgvTVk148RGI/fOd+wX7CXXQPTXPIYf3PDsUqjLXYYIYfynSJXwSDwHespXsVlQIhrztsm9D/C3zWUk9gs30QJQ1/wpoenKU0GCA5Mkx6+QWCwEpkh8olyCWispc4VGfM/DQU01SdGUOTTV7MGH9Gw9EWJZclo5WZBnq6JLafLTTRQzBjRgB26LydNlBXfOBrzwlPeIS2KCEM4Z0sdCt1jEoU1VEakS3XyRaYKuAb3vq6lvwAI2aT2Zg3Wv0LhWkshtYaKG7Z6FfQaUg/CXg6xHcOmC83sqC2HgWwvvtzbraH2Wk6gjmYmcX58QjMyfIO+ubNT6c5hHagsZNPA4VHX4Vy/g5iMTDiisMtC4NANjs86EOzI03jPUR7hXVuaVDd0RkrasSYN4kPHWyV45b8mi7yGirOj6Y0897eyYuuCsWbetkZz8PxxZSM2ZadCXXu6mswAs3Q1IYnezRrqsMxnIsCipHk/fbXvAFmaIkOyKZCLgmZSZ9xTkGLzNzEXrz851IsoO6bH3ZeO6MF98OHu4qLvsMOcsi65OgugEhyMbOa/Yun3G2G0Zc4TR4c3SbxfDE4OeA00ZTm6QJSn4EJfpid/t1v1lEJhKXRetGYDhXvDNLPTTbt5PY3StUB3ec2iX2j3Z/gFEcsL7d4XyUS8ASRhhfWZcazArnu8z29mxgMnvVWZZaCUqu20I+Ohpq6YVw5WOxKGrUAedP+tg34sWZ6GzEe2pbLsY+3QTNoABsD/ZqMGSEb1eWUYEReSo5tSv05BhRBT2erxHw9WzmHLhibW5rwoc8qBEGcuNlp+JkwAAha8aEhxonnPO/0a6Jy6XyxZedBi2AJUzR2f5/wkjdzOm+kld4FSuKaZfyJomiKfwbTVUGJh8BpnG0eeUhYRwIw1X1meO9cvcsH2JlBFlRv7NPWLFUvcZu6QQ6Q/Vg3EkYoZCdSpaYrwyNzvBMuLIURUcwHkpogFqN+PLmSH+qElxlARP26FHxXsXGU/XHZQpGsEtoAV2djidguO5YXnXgraFpYy0/xzQABDucQxb4qz9puxR/u/32QR3ENk9DbZlRapok/OsZ3uE0nsBiSGCAzC0NzcmkBXVZdcaDy7wIp/3zCOwsMSbmwlKwJkneWO2itZXAYjJrltGI5vmz7kuvuKexyyrGLo7CAc+nsaPEHUbuv5SxCZH1H0HA7835QpOJeS9EalaUwKd7hFAjONYm+9Bfi9F3HcVCL7SQla9624gnSTpVHkIieM1iF5bOwbGplWqTM6Z9fnC6ULm9wqfHM+iOecBNKfQ6YgK1YALCPnIOwATMC1thCgTESHKwHF/9+KH6ptdr0O3iqvrpNkXW09CiScOyoZPeycq6mmg45V4WnpJiKA9Eaosmmdm5aeGPSV2kQxcAzijiAWSvrGQ3OhySa+sfktSJXRIITgbZg9QrVnCpYL6Ch9aksRHUbiVkjkks71/B0NFNk5D144HWsiDIHkF8+iioafLF4O37afmjuRiZ0OwObha5+NGgVbmwpWkf3bHFL6c4mrAXxX4ubuR3FCruVj17cVeAai3pBHJkFJcVq98xG0eOHn09spWhmhjSW4gIWHZiPOyD6plGpoM5oCdnuTvCb0bPczFXJFsiQSqY8jxYNC2ESvlZBAeLmIHsOX0Bep6t9bJY1Nx4xo0/LKNHVYXrUMYefdu5xlOXnfa2ZuA8MI2LoLM8/L1LAyBXpl/XT0wEdZ0TqV7nNOr/qPsCuWIp8ubzZ2/h9fNDG2eRvw+PK/vFBtxB4P1T6cOffX/ZdFMt4J1sOKebmllCEOl6zMmMt3JXE6J48WkxsXkpHBCvn+AtuRpArXcXxF+OvdErEAKu3baTDylLWnwa7NJ9aJzZM+V3faahuvHoxC2QTte0wnZrLb2sNFbM3XCTnApXRcceMtwWoe3asl8M+q4dxCMta96lLqNimz14IHc4tnJIgrTBrnGy/eDRS928/uRBqrMxLMY0Z9qSJA5T6tx0sp57vplBT7+487FPu3s9dgmBBn/UKn5igcxl52kEOX1exxWFF9lE5Fgvj0dAd6Fq/NvvJXgeOKb0gvywJj/YqPf0sNr8vtAebAc8BeVqX0zTmy5Q/QsoJmbjc=" />

<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="memo/Highlights.aspx">Announcements</a> <img src="images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	            
        
<div id="content">	
	<div id="description">
		<h1>HIV Drugs in Development</h1>
		<br />
		<div id="drugsindev">
            <div id="druglist">
                <table cellspacing="0" rules="all" bordercolor="#115169" border="1" id="ctl00__mainContent_dgDrugDev">
	<tr align="center" valign="middle" bgcolor="#396F9D">
		<td class="ColDrugClass"><font color="White" size="3"><b>Drug Class</b></font></td><td class="ColDrugCompany"><font color="White" size="3"><b>Company</b></font></td><td class="ColDrugName"><font color="White" size="3"><b>Drug Name</b></font></td><td class="ColDrugAltName"><font color="White" size="3"><b>Alt Name</b></font></td><td class="ColDrugStatus"><font color="White" size="3"><b>Drug Status</b></font></td><td class="ColDrugNotes"><font color="White" size="3"><b>Notes</b></font></td><td class="ColAIDSNo"><font color="White" size="3"><b>AIDS#</b></font></td>
	</tr><tr>
		<td>Biological Response Modifier </td><td>Hemispherx; Rega Institute For Medical Research</td><td>Atvogen&trade;</td><td>Ampligen, AMP, Poly(I)-Poly(C12U)</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl02_Link" href="CompoundDetails.aspx?AIDSNO=000136">000136</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>CRM1 Inhibitor</td><td>Karyopharm Therapeutics</td><td>Verdinexor</td><td>KPT335</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Combination</td><td>Merck</td><td>MK-1439A</td><td>Doravirine and Lamivudine and Tenofovir disoproxil fumarate</td><td>Phase 3</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Combination</td><td>ViiV Healthcare; GlaxoSmithKline</td><td>Tivicay&trade;  and Edurant&trade; </td><td>Dolutegravir and Rilpivirine</td><td>New Drug Application (NDA)</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr><tr>
		<td>Combination</td><td>St. Stephens AIDS Trust</td><td>Tivicay and Rezolsta</td><td>Dolutegravir and Cobocistat </td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Combination</td><td>Tibotec Pharmaceuticals, Janssen Scientific Affairs, LLC</td><td>Prezista&trade;  and Emtriva&trade;  and Tybost&trade; and Tenofovir alafenamide</td><td>Darunavir and Emtricitabine and Cobicistat and Tenofovir alafenamide</td><td>Phase 3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Combination</td><td>Mylan</td><td>TLE400</td><td>Efavirenz, Lamivudine and Tenofovir Disoproxil Fumarate</td><td>New Drug Application (NDA)</td><td>&nbsp;</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Combination</td><td>Gilead Sciences</td><td>Bictegravir and Emtriva&trade;  and  Tenofovir alafenamide</td><td>Bictegravir and Emtricitabine and  Tenofovir alafenamide</td><td>Phase 3</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr>
		<td>Combination</td><td>GlaxoSmithKline</td><td>Tivicay&trade; and Epivir&trade; </td><td>Dolutegravir and Lamivudine</td><td>Phase 3</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Entry and Fusion Inhibitor</td><td>Sangamo BioSciences</td><td>SB-728-T</td><td>&nbsp;</td><td>Phase 1/2</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr>
		<td>Entry and Fusion Inhibitor</td><td>United BioPharma</td><td>UB-421</td><td>monocloinal antibody; mAb dB4C7</td><td>Phase 3</td><td>Not Yet Open</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Entry and Fusion Inhibitor</td><td>Sangamo BioSciences</td><td>SB-728-HSC</td><td>SB-728mR-HSPC</td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Entry and Fusion Inhibitor</td><td>CytoDyn, Inc.; Progenics Pharmaceuticals</td><td>PRO-140</td><td>PA14</td><td>Phase 2/3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl14_Link" href="CompoundDetails.aspx?AIDSNO=088103">088103</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Entry and Fusion Inhibitor</td><td>Pfizer</td><td>PF-00232798</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl15_Link" href="CompoundDetails.aspx?AIDSNO=582838">582838</a>                             
                            </td>
	</tr><tr>
		<td>Entry and Fusion Inhibitor</td><td>Incyte Corporation</td><td>INCB-009471</td><td>INCB-9471</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl16_Link" href="CompoundDetails.aspx?AIDSNO=503495">503495</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Entry and Fusion Inhibitor</td><td>Tanox; Biogen Idec; Taimed Biologies</td><td>Ibalizumab</td><td>TNX-355; Hu5A8</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl17_Link" href="CompoundDetails.aspx?AIDSNO=209859">209859</a>                             
                            </td>
	</tr><tr>
		<td>Entry and Fusion Inhibitor</td><td>Bristol-Myers Squibb</td><td>Fostemsavir</td><td>BMS-663068; prodrug of BMS-626529</td><td>Phase 3</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Entry and Fusion Inhibitor</td><td>Tobira Therapeutics</td><td>Cenicriviroc</td><td>TBR-652; TAK-652</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl19_Link" href="CompoundDetails.aspx?AIDSNO=224423">224423</a>                             
                            </td>
	</tr><tr>
		<td>Entry and Fusion Inhibitor</td><td>Genzyme Corporation</td><td>AMD-070</td><td>AMD-11070</td><td>Phase 2/3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl20_Link" href="CompoundDetails.aspx?AIDSNO=495149">495149</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Entry and Fusion Inhibitor</td><td>Rapid Pharmaceuticals</td><td>Adaptavir</td><td>RAP101; monomeric d-Ala-peptide T-amide (mDAPTA)</td><td>Phase 2</td><td>Unknown</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl21_Link" href="CompoundDetails.aspx?AIDSNO=052004">052004</a>                             
                            </td>
	</tr><tr>
		<td>Entry and Fusion Inhibitor</td><td>Merck (Schering-Plough)</td><td>Vicriviroc</td><td>SCH-417690, SCH-D, VCV</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl22_Link" href="CompoundDetails.aspx?AIDSNO=210504">210504</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HIV Latency Reversing Agent</td><td>The Peter Doherty Institute for Infection and Immunity</td><td>Antabuse&trade; and Zolinza&trade;</td><td>Disulfiram and Vorinostat</td><td>Phase 1/2</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>HIV Latency Reversing Agent; Histone Deacetylase (HDAC) Inhibitor</td><td>Merck</td><td>Zolinza&trade;</td><td>Vorinostat; MK-0683, Suberoylanilide Hydroxamic Acid (SAHA), CCRIS 8456</td><td>Phase 1/2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl24_Link" href="CompoundDetails.aspx?AIDSNO=186714">186714</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HIV Latency Reversing Agent; Histone Deacetylase (HDAC) Inhibitor</td><td>Aarhus University Hospital</td><td>Romidepsin</td><td>&nbsp;</td><td>Phase 2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl25_Link" href="CompoundDetails.aspx?AIDSNO=565088">565088</a>                             
                            </td>
	</tr><tr>
		<td>HIV Latency Reversing Agent; Histone Deacetylase (HDAC) Inhibitor</td><td>Abbott Labs</td><td>Epival&trade;; Depakote&trade;</td><td>Valproic acid, Divalproex sodium, VPA</td><td>Phase 2</td><td>Unknown</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl26_Link" href="CompoundDetails.aspx?AIDSNO=057177">057177</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>HIV Latency-Reversing Agent; Toll-Like Receptor 3 (TLR-3) Agonist</td><td>Oncovir, Inc</td><td>Hiltonol&trade;</td><td>Poly-ICLC</td><td>Phase 1/2</td><td>Ongoing</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl27_Link" href="CompoundDetails.aspx?AIDSNO=187148">187148</a>                             
                            </td>
	</tr><tr>
		<td>Immunomodulator</td><td>The Research Council of Norway</td><td>Etoricoxib</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Immunomodulator</td><td>Amgen</td><td>Enbrel&trade;</td><td>Etanercept; p75 TNFR-Fc fusion protein</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Immunomodulator</td><td>Fred Hutchinson Cancer Research Center</td><td>CellCept&trade;; Myfortic&trade;</td><td>Mycophenolate Mofetil</td><td>Phase 1/2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl30_Link" href="CompoundDetails.aspx?AIDSNO=059828">059828</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Immunomodulator</td><td>National Institute of Allergy and Infectious Diseases (NIAID)</td><td>Arava&trade;</td><td> Leflunomide</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl31_Link" href="CompoundDetails.aspx?AIDSNO=009747">009747</a>                             
                            </td>
	</tr><tr>
		<td>Immunomodulator</td><td>Pfizer (Wyeth)</td><td>Ammonium Trichlorotellurate; Ossirene</td><td>AS-101</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl32_Link" href="CompoundDetails.aspx?AIDSNO=000042">000042</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Immunomodulator</td><td>University of Minnesota</td><td>IL-15</td><td>Interleukin 15</td><td>Phase 1</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl33_Link" href="CompoundDetails.aspx?AIDSNO=398478">398478</a>                             
                            </td>
	</tr><tr>
		<td>Immunomodulator</td><td>GlaxoSmithKline</td><td>Tucaresol</td><td>589C</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl34_Link" href="CompoundDetails.aspx?AIDSNO=029800">029800</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Immunomodulator</td><td>Hoffmann-La Roche</td><td>Pegasys&trade;</td><td>PEG IFN alpha-2a</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Immunomodulator</td><td>University of Aarhus</td><td>Lefitolimod</td><td>MGN-1703</td><td>Phase 1/2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Integrase Inhibitor</td><td>Boehringer Ingelheim</td><td>BI-224436</td><td>&nbsp;</td><td>Phase 1</td><td>Withdrawn</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl37_Link" href="CompoundDetails.aspx?AIDSNO=566575">566575</a>                             
                            </td>
	</tr><tr>
		<td>Integrase Inhibitor</td><td>Gilead Sciences</td><td>Bictegravir</td><td>GS-9883</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl38_Link" href="CompoundDetails.aspx?AIDSNO=092431">092431</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Integrase Inhibitor</td><td>ViiV Healthcare; GlaxoSmithKline</td><td>Cabotegravir</td><td>GSK1265744; S-265744 </td><td>Phase 3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Integrase Inhibitor</td><td>ViiV Healthcare; GlaxoSmithKline</td><td>GSK1247303</td><td>&nbsp;</td><td>Phase 1</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Janus Kinase Inhibitor</td><td>National Institute of Allergy and Infectious Diseases (NIAID)</td><td>Ruxolitinib</td><td>&nbsp;</td><td>Phase 2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl41_Link" href="CompoundDetails.aspx?AIDSNO=564288">564288</a>                             
                            </td>
	</tr><tr>
		<td>Maturation Inhibitor</td><td>Bristol-Myers Squibb</td><td>BMS-955176</td><td>3532795</td><td>Phase 2</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Maturation Inhibitor</td><td>ViiV Healthcare; GlaxoSmithKline</td><td>GSK2838232</td><td>&nbsp;</td><td>Phase 2</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Microbicide</td><td>Population Council Center for Biomedical Research</td><td>Carraguard&trade;</td><td>Carragaen, Carrageenan gum, Carrageenin, Irish moss extract, PC-515, Sea Algal Extract</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl44_Link" href="CompoundDetails.aspx?AIDSNO=000100">000100</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Microbicide</td><td>Tibotec Pharmaceuticals; Janssen;  Johnson & Johnson</td><td>Dapivirine</td><td>TMC-120, GEL-02, R147681, DAP</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl45_Link" href="CompoundDetails.aspx?AIDSNO=105293">105293</a>                             
                            </td>
	</tr><tr>
		<td>Microbicide</td><td>International Partnership for Microbicides, Inc.</td><td>DS003</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl46_Link" href="CompoundDetails.aspx?AIDSNO=335384">335384</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Microbicide</td><td>Johns Hopkins University</td><td>DuoGel</td><td>IQP-0528</td><td>Phase 1</td><td>Not yet recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl47_Link" href="CompoundDetails.aspx?AIDSNO=104929">104929</a>                             
                            </td>
	</tr><tr>
		<td>Microbicide</td><td>Population Council</td><td>Griffithsin Gel</td><td>&nbsp;</td><td>Phase 1</td><td>Not Yet Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Microbicide</td><td>GlaxoSmithKline</td><td>GSK706769</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Microbicide</td><td>Population Council</td><td>PC-1005</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Microbicide</td><td>Myrexis, Inc.; Panacos Pharmaceuticals</td><td>Bevirimat</td><td>BVM, PA-457, MPC-4326, DSB, YK-FH312</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl51_Link" href="CompoundDetails.aspx?AIDSNO=028530">028530</a>                             
                            </td>
	</tr><tr>
		<td>Microbicide</td><td>Reprotect, Inc.</td><td>BufferGel&trade;</td><td>Carbomer 974P, Carbopol 974P, Carbopol polymer</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl52_Link" href="CompoundDetails.aspx?AIDSNO=180064">180064</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Microbicide</td><td>Procept/Interneuron/Indevus</td><td>Pro-2000</td><td>PRO-2000/5</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl53_Link" href="CompoundDetails.aspx?AIDSNO=032942">032942</a>                             
                            </td>
	</tr><tr>
		<td>Microbicide</td><td>Gilead Sciences</td><td>Tenofovir gel</td><td>GS-1278; PMPA gel, Apropovir, TFV</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl54_Link" href="CompoundDetails.aspx?AIDSNO=021800">021800</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Microbicide</td><td>Uniroyal/Crompton Corp/Biosyn</td><td>UC781</td><td>Thiocarboxanilide</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl55_Link" href="CompoundDetails.aspx?AIDSNO=029940">029940</a>                             
                            </td>
	</tr><tr>
		<td>Microbicide</td><td>Starpharma</td><td>VivaGel&trade;</td><td>SPL-7013 gel</td><td>Phase 1/2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl56_Link" href="CompoundDetails.aspx?AIDSNO=223863">223863</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Monoclonal Antibody</td><td>St George's, University of London</td><td>P2G12</td><td>&nbsp;</td><td>Phase 1</td><td>Not Yet Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>NNRTI</td><td>Kainos Medicine</td><td>KM-023</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NNRTI</td><td>ViiV Healthcare; GlaxoSmithKline</td><td>GSK2248761</td><td>IDX-899</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl59_Link" href="CompoundDetails.aspx?AIDSNO=485324">485324</a>                             
                            </td>
	</tr><tr>
		<td>NNRTI</td><td>Merck & Co</td><td>Doravirine</td><td>MK-1439</td><td>Phase 3</td><td>Ongoing</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl60_Link" href="CompoundDetails.aspx?AIDSNO=525576">525576</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NNRTI</td><td>Boehringer Ingelheim</td><td>BILR 355 BS</td><td>&nbsp;</td><td>Phase 2</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl61_Link" href="CompoundDetails.aspx?AIDSNO=212773">212773</a>                             
                            </td>
	</tr><tr>
		<td>NNRTI</td><td>Pfizer</td><td>Lersivirine</td><td>UK453,061</td><td>Phase 2</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl62_Link" href="CompoundDetails.aspx?AIDSNO=497274">497274</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NNRTI</td><td>St Stephens AIDS Trust</td><td>TMC278-LA</td><td> a long-acting formulation of Janssen's Rilpivirine</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl63_Link" href="CompoundDetails.aspx?AIDSNO=169030">169030</a>                             
                            </td>
	</tr><tr>
		<td>NNRTI</td><td>Viriom</td><td>VM-1500</td><td>Elpida, elpivirine, prodrug of VM-1500A, prodrug of elsulfavirine</td><td>Phase 2/3</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NRTI</td><td>Achillion Pharmaceuticals; Vion Pharmaceuticals</td><td>Elvucitabine</td><td>ACH-126443; beta-L-Fd4C</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl65_Link" href="CompoundDetails.aspx?AIDSNO=060327">060327</a>                             
                            </td>
	</tr><tr>
		<td>NRTI</td><td>Chimerix</td><td>CMX-157</td><td> Hexadecyloxypropyl ester of Tenofovir, HDP-(R)-PMPA</td><td>Phase 1/2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl66_Link" href="CompoundDetails.aspx?AIDSNO=477886">477886</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NRTI</td><td>Oncolys BioPharma; Brisol-Myers Squibb</td><td>Censavudine; (renaming of Festinavir)</td><td>4'-ethynyl-d4T; BMS-986001; OBP-601</td><td>Phase 2</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl67_Link" href="CompoundDetails.aspx?AIDSNO=209894">209894</a>                             
                            </td>
	</tr><tr>
		<td>NRTI</td><td>Avexa</td><td>Apricitabine</td><td>(-)-Dotc; ATC; AVX-754; BCH-10618; BCH-10619; BCH-10652; SPD-754</td><td>Phase 3</td><td>Withdrawn</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl68_Link" href="CompoundDetails.aspx?AIDSNO=058066">058066</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NRTI</td><td>RFS Pharma; Biochem Pharma; Triangle Pharmaceuticals; Gilead Sciences</td><td>Amdoxovir</td><td>DAPD</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl69_Link" href="CompoundDetails.aspx?AIDSNO=005431">005431</a>                             
                            </td>
	</tr><tr>
		<td>NRTI</td><td>Koronis</td><td>KP-1461 (Prodrug of KP1212)</td><td>SN-1461 (Prodrug of SN1212)</td><td>Phase 2</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl70_Link" href="CompoundDetails.aspx?AIDSNO=293211">293211</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NRTI</td><td>Gilead Sciences</td><td>Tenofovir Alafenamide (Prodrug of tenofovir)</td><td>GS-7340</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl71_Link" href="CompoundDetails.aspx?AIDSNO=046774">046774</a>                             
                            </td>
	</tr><tr>
		<td>NRTI</td><td>Pharmasset; Dupont Pharmaceuticals; Vion</td><td>Reverset &trade;</td><td>Dexelvucitabine; DFC, 5F-D4C, beta D-d4FC, DPC 817, RA 131423, RVT</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl72_Link" href="CompoundDetails.aspx?AIDSNO=000572">000572</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>NRTI</td><td>Glaxo Wellcome; GlaxoSmithKline; Pharmasset, Inc.</td><td>Racivir</td><td>RCV; (+/-)FTC, 5-FSddC, LDS-022, PSI 5004</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl73_Link" href="CompoundDetails.aspx?AIDSNO=005245">005245</a>                             
                            </td>
	</tr><tr>
		<td>NRTI</td><td>Merck Sharp & Dohme Corp.</td><td>MK-8591</td><td>EFdA</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl74_Link" href="CompoundDetails.aspx?AIDSNO=343654">343654</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Protease Inhibitor</td><td>Concert Pharmaceuticals</td><td>C-10276</td><td>Isotopic Analog of Atazanavir (ATV)</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Protease Inhibitor</td><td>Concert Pharmaceuticals</td><td>C-10297</td><td>Isotopic Analog of Atazanavir (ATV)</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Protease Inhibitor</td><td>Concert Pharmaceuticals</td><td>C-10299</td><td>Isotopic Analog of Atazanavir (ATV)</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Protease Inhibitor</td><td>Temple University</td><td>TMB-607</td><td>PPL-100; MK-8122</td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Protease Inhibitor</td><td>Tibotec Pharmaceuticals</td><td>TMC-310911</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl79_Link" href="CompoundDetails.aspx?AIDSNO=483146">483146</a>                             
                            </td>
	</tr><tr>
		<td>Proteasome Inhibitor</td><td>Mayo Clinic;</td><td>Ninlaro&trade;</td><td>Ixazomib</td><td>Phase 1/2</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Rev Inhibitor</td><td>Abivax S.A</td><td>ABX464</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl81_Link" href="CompoundDetails.aspx?AIDSNO=516635">516635</a>                             
                            </td>
	</tr><tr>
		<td>Toll-Like Receptor 7 (TLR-7) Agonist</td><td>Gilead Sciences</td><td>Vesatolimod</td><td>GS-9620</td><td>Phase 1</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl82_Link" href="CompoundDetails.aspx?AIDSNO=574840">574840</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Unidentified</td><td>Merck</td><td>MK-1972</td><td>&nbsp;</td><td>Phase 1</td><td>Terminated</td><td>&nbsp;</td>
	</tr>
</table>
            </div>		
            <div id="drugtblinks">
                <br /><br />
                Some useful sites where additional information on these drugs may be found:
                <br /><br />
                <a href="http://aidsinfo.nih.gov" target="_blank"><img src="images/logo_wht_medium.gif" alt="AIDS Info Gov Logo" border="0" /></a>
                <br /><br />
                <a href="http://www.clinicaltrials.gov" target="_blank"><img src="images/ctgov_big_ttl.gif" alt="Clinical Trials Gov Logo" border="0" /></a>                
            </div>
        </div>
    </div>					
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="SiteMap.aspx">Site Map</a> | 
        	<a href="help/Help.aspx">Help</a> | 
        	<a href="ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="970224A7" /></form>
</body>
</html>
