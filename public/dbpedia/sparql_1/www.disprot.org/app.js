'use strict';
var cacheBustSuffix = Date.now();

// Declare app level module which depends on views, and components
angular.module('disprot-ui', [
    'ngRoute',
    'ngCookies',
    'ngResource',
    'LocalStorageModule',
    'disprot.navbar',
    'disprot.home',
    'disprot.search',
    'disprot.list',
    'disprot.auth',
    'disprot.submit',
    'disprot.entry',
    'disprot.entry7',
    'disprot.entry7.colormap',
    'disprot.results',
    'disprot.help',
    'disprot.about',
	//'disprot.assessment',
    'disprot.curators',
    'disprot.dashboard',
    'disprot.contact',
    'disprot.stats',
    'ui.bootstrap',
    'ui-notification'
])

    .constant('WSROOT', (function () {
        // default to prod WS
        var WS = 'http://www.disprot.org/ws';

        // cannot use $location or $window, so had to step back to plain js
        if (/.*beta.*/.test(window.location.hostname)) {
            // beta ws (running on caronte)
            WS = 'http://beta.disprot.bio.unipd.it/ws';
        } else if (/.*local.*/.test(window.location.hostname)) {
            // localhost ws for local dev
            WS = 'http://0.0.0.0:8088';
        }

        console.log(WS);
        return WS
    })())

    /* ROUTING */

    .config(['$locationProvider', '$routeProvider', 'NotificationProvider', 'localStorageServiceProvider', '$qProvider', function ($locationProvider, $routeProvider, NotificationProvider, localStorageServiceProvider, $qProvider) {
        $qProvider.errorOnUnhandledRejections(false);

        $locationProvider
            .html5Mode({
                enabled: true,
                requireBase: false
            })
            .hashPrefix('!');

        $routeProvider
            .when('/', {
                templateUrl: 'modules/home/home.html?cache-bust=' + cacheBustSuffix,
                controller: 'homeCtrl'
            })
            .when('/about', {
                templateUrl: 'modules/about/about.html?cache-bust=' + cacheBustSuffix,
                controller: 'aboutCtrl'
            })
            .when('/help', {
                templateUrl: 'modules/help/help.html?cache-bust=' + cacheBustSuffix,
                controller: 'helpCtrl'
            })
            // .when('/login', {
            //     templateUrl: 'modules/auth/auth.html?cache-bust=' + cacheBustSuffix,
            //     controller: 'authCtrl'
            // })
            .when('/list', {
                templateUrl: 'modules/list/list.html?cache-bust=' + cacheBustSuffix,
                controller: 'listCtrl'
            })
            .when('/browse', {
                templateUrl: 'modules/browse/browse.html?cache-bust=' + cacheBustSuffix,
                controller: 'browseCtrl'
            })
            .when('/search', {
                templateUrl: 'modules/search/search.html?cache-bust=' + cacheBustSuffix,
                controller: 'searchCtrl'
            })
            .when('/contact', {
                templateUrl: 'modules/contact/contact.html?cache-bust=' + cacheBustSuffix,
                controller: 'contactCtrl'
            })
            .when('/stats', {
                templateUrl: 'modules/stats/stats.html?cache-bust=' + cacheBustSuffix,
                controller: 'statsCtrl'
            })
            // Route to the critical assessment data (by Damiano)
            .when('/assessment', {
                templateUrl: '/modules/assessment/assessment.html?cache-bust=' + cacheBustSuffix
            })
            /*.when('/submit', {
                templateUrl: 'modules/submit/submit.html?cache-bust=' + cacheBustSuffix,
                controller: 'submitCtrl',
                resolve: {
                    check: function ($rootScope, $location) {
                        if (!($rootScope.globals.currentUser)) {
                            $location.path('/');
                            $rootScope.$emit('show.alert.error', {message: 'Forbidden: you are not logged in.'});
                        }
                    }
                }
            })
            .when('/edit', {
                templateUrl: 'modules/submit/submit.html?cache-bust=' + cacheBustSuffix,
                controller: 'submitCtrl',
                resolve: {
                    check: function ($rootScope, $location) {
                        if (!($rootScope.globals.currentUser)) {
                            $location.path('/');
                            $rootScope.$emit('show.alert.error', {message: 'Forbidden: you are not logged in.'});
                        }
                    }
                }
            })
             .when('/dashboard', {
             templateUrl: 'modules/dashboard/dashboard.html?cache-bust=' + cacheBustSuffix,
             controller: 'dashboardCtrl',
             resolve: {
             check: function ($rootScope, $location) {
             if (!($rootScope.globals.currentUser)) {
             $location.path('/');
             $rootScope.$emit('show.alert.error', {message: 'Forbidden: you are not logged in.'});
             }
             }
             }
             })*/
            // old entries route
            .when('/:entry_id', {
                templateUrl: '/modules/entry7/entry7.html?cache-bust=' + cacheBustSuffix,
                controller: 'entry7Ctrl',
                reloadOnSearch: false,
                resolve: {
                    check: function ($rootScope, $location, $route) {
                        if (!(/DP\d{5}/.test($route.current.params.entry_id))) {
                            $location.path('/');
                            $rootScope.$emit('show.alert.error', {message: 'Invalid URL: disprot.org/' + $route.current.params.entry_id});
                        }
                    }
                }
            })
            .otherwise({redirectTo: '/'});

        NotificationProvider.setOptions({
            delay: 7000,
            // startTop : 20,
            // startRight : 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });

        localStorageServiceProvider
            .setPrefix(window.location.hostname)
            .setStorageType('localStorage')
            .setDefaultToCookie(true)
            .setNotify(false, false)
            .setStorageCookieDomain(window.location.hostname)
            .setStorageCookie(30, '/', false)

    }])

    .run(function ($rootScope, Notification) {
        $rootScope.$on('show.alert.primary', function (event, data) {
            Notification.primary(data)
        })
    })
    .run(function ($rootScope, Notification) {
        $rootScope.$on('show.alert.success', function (event, data) {
            Notification.success(data)
        })
    })
    .run(function ($rootScope, Notification) {
        $rootScope.$on('show.alert.error', function (event, data) {
            Notification.error(data)
        })
    })


    // COMMON SERVICES

    .factory('viewport', [function () {
        return function isBreakpoint(alias) {
            return $('.device-' + alias).is(':visible');
        }
    }])

    .factory('statsService', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/dbStats', {}, {
            getStats: {method: 'GET', isArray: false},
            getStatsLight: {method: 'GET', params: {light: true}, isArray: false}
        })
    }])

    .factory('str2col', [function () {
        return function (inStr) {
            if (inStr) {
                var hashCode = function (str) { // java String#hashCode
                    var hash = 0;
                    for (var i = 0; i < str.length; i++) {
                        hash = str.charCodeAt(i) + ((hash << 5) - hash);
                    }

                    return hash;
                };

                var intToRGB = function (i) {
                    var c = (i & 0x00FFFFFF)
                        .toString(16)
                        .toUpperCase();
                    return "00000".substring(0, 6 - c.length) + c;
                };
                return '#' + intToRGB(hashCode(inStr));
            } else {
                return null;
            }
        }
    }])

    .factory('blobDownload', [function () {
        return function (data, filename) {

            if (typeof data === 'object') {
                data = JSON.stringify(data, undefined, 2);
            }

            var blob = new Blob([data], {type: 'text/json'});

            // FOR IE:

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob, filename);
            }
            else {
                var e = document.createEvent('MouseEvents'),
                    a = document.createElement('a');

                a.download = filename;
                a.href = window.URL.createObjectURL(blob);
                a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
                e.initEvent('click', true, false, window,
                    0, 0, 0, 0, 0, false, false, false, false, 0, null);
                a.dispatchEvent(e);
            }
        }
    }])

    // COMMON FILTERS

    .filter('capitalize', function () {
        return function (input, all) {
            return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }) : '';
        };
    })

    .filter('arr2obj', function () {
        return function (arr) {
            var res;
            if (arr.length === 1) {
                res = arr[0];
            } else {
                console.error('[arr2obj] Malformed input. Length of input array is ' + arr.length)
            }
            return res
        }
    })

    .filter('isArray', function () {
        return function (input) {
            return angular.isArray(input);
        }
    })

    .filter('isNumber', function () {
        return function (input) {
            return angular.isNumber(input);
        }
    })

    .filter('join', function () {
        return function (inArr, sep) {
            return inArr.join(sep);
        }
    })

    .filter('isString', function () {
        return function (input) {
            return angular.isString(input);
        }
    })

    .filter('removeChar', function () {
        return function (input, char, replacement) {
            return input.replace(new RegExp(char, 'g'), replacement);
        }
    })

    .filter('getLength', function () {
        return function (input) {
            var res;
            if (angular.isArray(input) || angular.isObject(input)) {
                res = input.length;
            }
            return res
        }
    })

    .filter('dropKeys', function () {
        return function (obj, drop) {
            var res = {};
            for (var key in obj) {
                if (obj.hasOwnProperty(key) &&
                    drop.indexOf(key) === -1) {
                    res[key] = obj[key];
                }
            }
            return res;
        }
    })

    // COMMON DIRECTIVES

    .directive('noReload', function () {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {

                elem.on('click', function (e) {
                    e.preventDefault();
                });

            }
        };
    });