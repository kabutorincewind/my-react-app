/**
 * Created by ftabaro on 17/06/16.
 */

angular.module('disprot.entry', [])

// SERVICES

    .factory('entryService', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/get/:id')
    }])

    // FILTERS

    .filter('getEntryFeature', function () {
        return function (inArr, key, mode) {
            mode = mode ? mode : 'comparison';

            var res;

            // this returns value of a key
            if (angular.isArray(inArr) && inArr.length === 1) {
                res = inArr[0][key];

            } else if (angular.isArray(inArr) && inArr.length !== 1) {
                // this returns a boolean. true if at least two elements are equale, false otherwhise
                if (mode === 'comparison') {
                    res = inArr.map(function (curr) {
                        return curr[key];
                    }).some(function (curr, idx, arr) {
                        return typeof curr == 'boolean' ? curr : curr === arr[idx - 1];
                    });
                    // this returns arrays.
                } else {

                    res = inArr.map(function (curr) {
                        return curr[key];
                    })
                }
            } else if (angular.isObject(inArr)) {
                // try to return che requested key
                res = inArr[key];
            }
            return res;
        }
    })

    .filter('filterEntry', function () {
        return function (inArr, keys) {
            if (!angular.isArray(keys)) {
                keys = new Array(keys);
            }
            return inArr.filter(function (curr) {
                return keys.indexOf(curr.record_type) !== -1;
            })
        }
    })

    // DIRECTIVES

    .directive('customPanel', [function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/modules/entry/partials/panel.html',
            link: {
                pre: function (scope, elm, attrs) {
                    scope.content = scope.$eval(attrs.data);
                    scope.type = attrs.type;
                    if (attrs.idx) {
                        scope.idx = attrs.idx;
                    }
                    if (attrs.referenceSequence) {
                        scope.reference_sequence = scope.$eval(attrs.referenceSequence);
                    }
                },
                post: function (scope, elm, attrs) {
                    scope.show = true;
                    scope.title = attrs.title;

                    // scope.$on('featureSelected', function (ev, data) {

                    //  need to set active property of tab according to the id of the div where
                    //  the region was clicked

                    // })
                }
            }
        }
    }])

    .directive('featuresTable', function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: '/modules/entry/partials/table.html',
            link: function (scope, elm, attrs) {
                scope.tableTitle = attrs.tableTitle;
                attrs.$observe('tableData', function (newVal) {
                        var pat = /\{.+:.*\}/;
                        if (pat.test(newVal)) {
                            scope.tableData = JSON.parse(newVal);
                        }
                    }
                );
            }
        }
    })

    .directive('sequenceViewer', [function () {
        return {
            restrict: 'E',
            scope: {
                seq: '='
            },
            template: '<div id="{{viewer_id}}"></div>',
            link: function (scope, elm, attrs) {
                scope.viewer_id = attrs.viewerId;
                scope.$watch('seq', function (n, o) {
                    var sv = new Sequence(n);

                    sv.render('#' + scope.viewer_id, {
                        'charsPerLine': 80,
                        'wrapAminoAcids': true,
                        'title': attrs.title,
                        'badge': false,
                        'search': true,
                        'showLineNumbers': true
                    });
                })
            }
        }
    }])

    .directive('featuresViewer', function ($timeout) {
        return {
            restrict: 'E',
            scope: true,
            template: '<div id="{{viewer_id}}"></div>',
            link: function (scope, elm, attrs) {
                var fv;

                function drawFeatures() {

                    var fv_regions = angular.isArray(scope.regions[0]) ?
                        scope.regions.map(function (curr, idx) {
                            return {
                                x: curr[0],
                                y: curr[1],
                                id: idx + 1
                            }
                        }) : [{
                        x: scope.regions[0],
                        y: scope.regions[1],
                        id: 1
                    }];

                    fv.addFeature({
                        data: [{x: scope.variant[0], y: scope.variant[1]}],
                        name: 'Variant',
                        color: 'green',
                        type: 'rect'
                    });

                    fv.addFeature({
                        data: fv_regions,
                        name: 'Regions',
                        color: 'blue',
                        type: 'rect'
                    });

                    fv.onFeatureSelected(function (feat) {
                        scope.$emit('featureSelected', feat.detail);
                    })
                }

                scope.viewer_id = attrs.viewerId;
                scope.ref = scope.$eval(attrs.ref);
                scope.variant = scope.$eval(attrs.variant);
                scope.regions = scope.$eval(attrs.regions);

                $timeout(function () {

                    fv = new FeatureViewer(scope.ref,
                        '#' + scope.viewer_id, {
                            showAxis: true,
                            showSequence: true,
                            brushActive: true, //zoom
                            toolbar: false, //current zoom & mouse position
                            bubbleHelp: false,
                            zoomMax: 10 //define the maximum range of the zoom
                        });
                    drawFeatures();
                })
            }
        }
    })

    .directive('buttonsWithCheck', function () {
        return {
            restrict: 'E',
            scope: {
                val2check: '=',
                trigger: '=',
                label: '='
            },
            templateUrl: '/modules/entry/partials/buttonsWithCheck.html',
            link: function (scope, elm, attrs) {
                scope.initArray = function (i) {
                    var res = new Array(i);
                    for (var ii = 0; ii < i; ii++) {
                        res[ii] = ii + 1;
                    }
                    return res;
                }
            }
        }
    })

    // CONTROLLER

    .controller('entryCtrl', ['$scope', '$filter', '$routeParams', 'entryService',
        function ($scope, $filter, $routeParams, entryService) {

            $scope.loading = true;

            $scope.setRegionFilter = function (i) {
                $scope.filter_region = {
                    region_id: i
                }
            };

            /*$scope.setEvidence = function (i) {
             $scope.filter_evidence = {
             evidence_id: i
             }
             };*/

            entryService.query({id: $routeParams.entry_id}, function (result) {
                $scope.entry = result;
                $scope.entry.count = result.reduce(function (out, curr, idx, inArr) {
                    var record_type = curr['record_type'];
                    out[record_type] = out.hasOwnProperty(record_type) ? out[record_type] += 1 : 1;
                    return out
                }, {});

                $scope.setRegionFilter(1);
                // $scope.setEvidence(1);

                $scope.loading = false;

            });
        }])
;


