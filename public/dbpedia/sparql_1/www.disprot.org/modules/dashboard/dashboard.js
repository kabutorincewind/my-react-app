/**
 * Created by ftabaro on 07/07/16.
 */
angular.module('disprot.dashboard', [])

    .factory('getNewEntriesListing', ['WSROOT', '$resource',
        function (WSROOT, $resource) {
            return $resource(WSROOT + '/entries7', {})
        }])

    .factory('getUsers', ['WSROOT', '$resource',
        function (WSROOT, $resource) {
            return $resource(WSROOT + '/users')
        }])

    .controller('dashboardCtrl', ['$rootScope', '$scope', '$location', 'getNewEntriesListing', '$filter', 'getUsers', 'uiGridConstants',
        function ($rootScope, $scope, $location, getNewEntriesListing, $filter, getUsers, uiGridConstants) {


            $scope.gridOptions = {
                enableSorting: true,
                enableColumnResizing: true,
                enableResizing: true,
                enableFiltering: true,
                enableGridMenu: true,
                showGridFooter: true,
                showColumnFooter: true,
                //rowTemplate:'<div ng-class="{\'green\':true, \'blue\':row.entity.obsolete }"><div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }" ui-grid-cell></div></div>',

                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                },
            };

            var columns = [
                {
                    field: 'View',
                    enableSorting: false,
                    cellTemplate: '<div class="text-center"><button class="btn btn-xs btn-default" ng-click="grid.appScope.goToView(row.entity.disprot_id)"><i class="glyphicon glyphicon-eye-open"></i></button></div>',
                    // cellClass: 'grid-align-center',
                    enableFiltering: false,
                    enableColumnMenu: false,
                    width: 50
                },
                {
                    field: 'Edit',
                    enableSorting: false,
                    cellTemplate: '<div class="text-center"><button class="btn btn-xs btn-default" ng-click="grid.appScope.goToEdit(row.entity.disprot_id)"><i class="glyphicon glyphicon-edit"></i></button></div>',
                    // cellClass: 'grid-align-center',
                    enableFiltering: false,
                    enableColumnMenu: false,
                    width: 50
                },
//                                {
//                                    field: 'obsolete',
//                                },
                {
                    field: 'disprot_id',
                    cellClass: function (grid, row) {
                        if (row.entity.obsolete) {
                            return 'grid-red';
                        }
                        return null;
                    }
                },
                {
                    field: 'uniprot_accession'
                },
                {
                    field: 'protein_name',
                    width: 200
                },
                {
                    field: 'protein_type'
                },
                {
                    field: 'sequence.length',
                    type: 'number',
                    cellClass: 'grid-align-center'
                },
                {
                    name: 'Regions',
                    field: 'region_count',
                    type: 'number',
                    cellClass: 'grid-align-center',
                    aggregationType: uiGridConstants.aggregationTypes.sum
                },
                {
                    displayName: 'Functions',
                    field: 'funct_count',
                    type: 'number',
                    cellClass: 'grid-align-center',
                    aggregationType: uiGridConstants.aggregationTypes.sum
                },
                {
                    field: 'last_curator',
                    filter: {
                        term: $rootScope.globals.currentUser.username,
                        type: uiGridConstants.filter.EXACT
                    },
                    sort: {
                        direction: uiGridConstants.ASC,
                        priority: 1
                    }
                },
                {
                    field: 'group'
                },
                {
                    field: 'last_edit_date',
                    // type: 'date',
                    cellFilter: 'date',
                    footerCellFilter: 'date',
                    aggregationType: uiGridConstants.aggregationTypes.max,
                    sort: {
                        direction: uiGridConstants.DESC,
                        priority: 0
                    }
                },
                {
                    field: 'creation_date',
                    // type: 'date',
                    cellFilter: 'date',
                    footerCellFilter: 'date',
                    aggregationType: uiGridConstants.aggregationTypes.max
                }
            ];

            $scope.gridOptions.columnDefs = columns;


            getUsers.query({}, function (resUser) {
                //console.log(resUser);
                var groups = {};
                resUser.forEach(function (curr) {
                    groups[curr.user] = curr.group;
                });

                getNewEntriesListing.query({}, function (res) {

                    // Count regions by disprot_id
                    var region_count = {};
                    var funct_count = {};
                    $filter('filter')(res, {record_type: "region_record"}).forEach(function (curr) {
                        if (!(curr.disprot_id in region_count)) {
                            region_count[curr.disprot_id] = 0;
                            funct_count[curr.disprot_id] = 0;
                        }

                        region_count[curr.disprot_id]++;

                        var mfun = curr.funct.mfun[0] != null ? curr.funct.mfun.length : 0;
                        var tran = curr.funct.tran[0] != null ? curr.funct.tran.length : 0;
                        var part = curr.funct.part[0] != null ? curr.funct.part.length : 0;

                        funct_count[curr.disprot_id] += mfun + tran + part;
                    });

                    // Assign region count
                    $filter('filter')(res, {record_type: "protein_record"}).forEach(function (curr) {
                        // assign group name
                        curr.group = groups[curr.last_curator];

                        if (curr.disprot_id in region_count) {
                            curr.region_count = region_count[curr.disprot_id];
                            curr.funct_count = funct_count[curr.disprot_id];

                        } else {
                            curr.region_count = 0;
                            curr.funct_count = 0;
                        }
                    });


                    // parse dates for footer aggregation
                    res.forEach(function (curr) {
                        if (curr.hasOwnProperty('creation_date')) curr.creation_date = Date.parse(curr.creation_date);
                        if (curr.hasOwnProperty('last_edit_date')) curr.last_edit_date = Date.parse(curr.last_edit_date);
                    });

                    $scope.gridOptions.data = $filter('filter')(res, {record_type: "protein_record"});

                });

                $scope.goToEdit = function (disprot_id) {
                    if (!disprot_id) {
                        $scope.$emit('show.alert.error', {message: 'no Disprot ID.'})
                    } else {
                        $location.path('/edit').search({entry_id: disprot_id});
                    }
                };

                $scope.goToView = function (disprot_id) {
                    if (!disprot_id) {
                        $scope.$emit('show.alert.error', {message: 'no Disprot ID.'})
                    } else {
                        $location.path(disprot_id.toUpperCase());
                    }
                }
            });
        }]);