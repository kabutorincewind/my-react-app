/**
 * Created by ftabaro on 17/10/16.
 */


angular.module('disprot.contact', [])

    .factory('sendMail', ['$resource', 'WSROOT', function ($resource, WSROOT) {
        return $resource(WSROOT + '/feedback')
    }])


    // custom validator for subject select ;)
    .directive('validateSubject', [function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$validators.subject = function (modelVal, viewVal) {
                    var value = modelVal || viewVal;
                    return value !== ''
                }
            }
        }
    }])

    .directive('validateDpid', ['browseService', function (browseService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, elm, attrs, ctrl) {
                var unpre = /^[OPQ][0-9][A-Z0-9]{3}[0-9](-[0-9]+)*$|^[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}(-[0-9]+)*$/;

                browseService.query({
                    record_type: 'protein_record',
                    projection: {disprot_id: 1}
                }).$promise.then(function (inArr) {
                    dpid_list = inArr.map(function (curr) {
                        return curr.disprot_id;
                    });

                    var res;
                    ctrl.$validators.dpid = function (modelVal, viewVal) {

                        var value = modelVal || viewVal;
                        var feedback_selection = ctrl.$$parentForm.subject_select.$modelValue || ctrl.$$parentForm.subject_select.$viewValue;

                        if (['new_data', 'data_error'].indexOf(feedback_selection) != -1) {
                            res = dpid_list.indexOf(value) != -1 || unpre.test(value);
                        } else {
                            res = true
                        }

                        return res
                    }
                });
            }
        }
    }])

    .controller('contactCtrl', ['$scope', 'sendMail', 'browseService', 'localStorageService',
        function ($scope, sendMail, browseService, localStorageService) {

            // var lsKey = localStorageService.keys();
            // if (lsKey.indexOf('dpid_list') === -1) {
            //
            //     var unbind_dpid_list = localStorageService.bind($scope, 'dpid_list');
            //
            //     browseService.query({
            //         record_type: 'protein_record',
            //         projection: {disprot_id: 1}
            //     }).$promise.then(function (inArr) {
            //         $scope.dpid_list = inArr.map(function (curr) {
            //             return curr.disprot_id;
            //         });
            //     });
            // } else {
            //     $scope.dpid_list = localStorageService.get('dpid_list');
            // }
            $scope.disable_form = false;

            var vm = {
                subject: '',
                object: '',
                name: '',
                email: '',
                message: ''
            };

            $scope.re = {
                pmid: /^[0-9]+$/
            };

            $scope.model = angular.copy(vm);

            $scope.submit_new_data = false;

            $scope.$watch('model.subject', function (n, o) {
                if (n !== o && ['new_data', 'data_error'].indexOf(n) != -1) {
                    $scope.submit_new_data = true;
                    // angular.element('#feedback_input_dpid').attr('validate-dpid', '')
                } else {
                    $scope.submit_new_data = false;
                    // angular.element('#feedback_input_dpid').removeAttr('validate-dpid')
                }
            });

            $scope.submit = function () {
                $scope.disable_form = true;

                var postdata = angular.copy($scope.model);

                if (['new_data', 'data_error'].indexOf(postdata.subject) != -1) {

                    var header = '';

                    if (postdata.hasOwnProperty('dpid')) {
                        header += 'DisProt ID: ' + postdata.dpid;
                        delete postdata.dpid
                    }

                    if (postdata.hasOwnProperty('unpid')) {
                        header += '\tUniProt ID: ' + postdata.unpid;
                        delete postdata.unpid
                    }

                    if (postdata.hasOwnProperty('pmid')) {
                        header += '\nPMID: ' + postdata.pmid;
                        delete postdata.pmid
                    }

                    if (postdata.hasOwnProperty('start')) {
                        header += '\nStart residue: ' + postdata.start;
                        delete postdata.start
                    }

                    if (postdata.hasOwnProperty('end')) {
                        header += '\tEnd residue: ' + postdata.end;
                        delete postdata.end
                    }

                    postdata.message = postdata.object + '\n\n' + header + '\n\n' + postdata.message;

                } else {
                    postdata.message = postdata.object + '\n\n' + postdata.message;
                }

                delete postdata.object;
                sendMail.save(postdata, function (res) {
                    if (res.hasOwnProperty('success')) {
                        $scope.$emit('show.alert.success', {message: 'Message sent successfully!'})
                    } else {
                        $scope.$emit('show.alert.error', {message: 'Error while sending the message.'})
                    }
                })
            };

            $scope.reset = function () {
                $scope.model = angular.copy(vm);
                $scope.submitted = false;
            };

            $scope.example = function () {
                $scope.model = {
                    subject: 'bug',
                    name: 'Francesco Tabaro',
                    object: 'some object',
                    email: 'francesco.tabaro@gmail.com',
                    message: 'THIS IS SPARTAAAAA!!'
                };
            };

            $scope.runValidation = function () {
                $scope.contactForm.feedback_input_dpid.$validate();
            }
        }])
;