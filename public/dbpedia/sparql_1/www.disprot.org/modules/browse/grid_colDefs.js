/**
 * Created by ftabaro on 20/09/16.
 */

angular.module('grid_column_definitions', ['ui.grid', 'disprot.curators'])

    .constant('field2names', {
        "features": 'Uniprot sequence features',
        "mobidb": 'MobiDB',
        // obsolete: 'Deleted',
        "removed": 'Deleted',
        "organism": 'Organism',
        "pdb_list": 'PDB(s)',
        "pfam": 'Pfam(s)',
        "protein_name": 'Protein name',
        "protein_type": 'Sequence type',
        "sequence": 'Sequence',
        "synonyms": 'Synonyms',
        "uniprot_accession": 'UniProt accession',
        "comment": 'Author comment',
        "creation_date": 'Creation date',
        "disprot_id": 'DisProt id',
        "end": 'End',
        "funct": 'Function',
        "last_curator": 'Curator',
        "last_edit_date": 'Last edit date',
        "method": 'Detection method',
        "name": 'Region name',
        "pmid": 'PumMed id',
        // primary_method: 'Primary detection method',
        "record_type": 'Record type',
        // secondary_method: 'Secondary detection method',
        "start": 'region start',
        // xr_id: 'Cross-reference id',
        // xr_name: 'Cross-reference database',
        "tags": 'Ambiguities',
        "xr": 'Cross-reference',
        "homologs": 'Homologous entries',
        "taxonomy": 'Taxonomy',
        "extent": 'Region length',
        "position": 'Region position',
        "stats_mfun": 'MFUN (count)',
        "stats_tran": 'TRAN (count)',
        "stats_part": 'PART (count)'
    })

    .factory('field2column', ['uiGridConstants', 'field2names', function (uiGridConstants, field2names) {
        return {
            // features: 'Uniprot sequence features',
            // mobidb: 'MobiDB',
            // obsolete: 'Deleted',
            organism: {
                field: 'organism',
                width: '13%'
            },
            pdb_list: {
                field: 'pdb_list',
                width: '20%'
            },
            pfam: {
                field: 'pfam',
                displayName: field2names['pfam'],
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field].length > 1 ? row.entity[col.field].join(", ") : row.entity[col.field][0]}}</div>',
                width: '26%'
            },
            protein_name: {
                field: 'protein_name',
                width: '30%',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '<a ng-href="{{row.entity.disprot_id}}" target="_blank">{{row.entity[col.field]}}' +
                '</a></div>'
            },
            protein_type: {
                field: 'protein_type',
                displayName: field2names['protein_type'],
                width: '15%'
            },
            sequence: {
                field: 'sequence',
                displayName: field2names['sequence'],
                width: '40%'
            },
            synonyms: {
                field: 'synonyms',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field].length > 1 ? row.entity[col.field].join(", ") : row.entity[col.field][0]}}</div>',
                width: '26%'
            },
            uniprot_accession: {
                field: 'uniprot_accession',
                width: '8%',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '<a ng-href="http://www.uniprot.org/uniprot/{{row.entity[col.field]}}" target="_blank">{{row.entity[col.field]}}' +
                '</a></div>'
            },
            // comment: 'Author comment',
            creation_date: {
                field: 'creation_date',
                displayName: 'Creation date',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field] | date : "dd-M-yyyy"}}</div>',
                width: '8%'
            },
            disprot_id: {
                field: 'disprot_id',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '<a ng-href="{{row.entity[col.field]}}" target="_blank">{{row.entity[col.field]}}' +
                '</a></div>',
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 0
                },
                width: '8%'
            },
            end: {
                field: 'end',
                width: '5%'
            },
            // funct: 'Function',
            last_curator: {
                field: 'last_curator',
                displayName: field2names['last_curator'],
                width: '13%',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field]}}</div>',
            },
            last_edit_date: {
                field: 'last_edit_date',
                displayName: field2names['last_edit_date'],
                width: '8%',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field] | date : "dd-M-yyyy"}}</div>'
            },
            method: {
                name: 'Detection method',
                field: 'method.name',
                width: '30%'
            },
            name: {
                field: 'name',
                displayName: field2names['name'],
                width: '20%',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field] | capitalize}}</div>'
            },
            pmid: {
                displayName: field2names['pmid'],
                field: 'pmid',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '<a ng-href="http://www.ncbi.nlm.nih.gov/pubmed/{{row.entity[col.field]}}">{{row.entity[col.field]}}' +
                '</a></div>',
                width: '10%'

            },
            // primary_method: 'Primary detection method',
            // record_type: 'Record type',
            // secondary_method: 'Secondary detection method',
            start: {
                field: 'start',
                width: '5%'
            },
            // xr_id: 'Cross-reference id',
            // xr_name: 'Cross-reference database',
            xr: {
                field: 'xr',
                displayName: field2names['xr'],
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field].length > 1 ? row.entity[col.field].join(", ") : row.entity[col.field][0]}}</div>',
                width: '10%'
            },
            extent: {
                name: 'Length',
                field: 'extent',
                width: '6%'
            },
            region_position: {
                name: 'Region position',
                field: 'position',
                width: '10%'
            },

            stats_mfun: {
                name: 'MFUN',
                field: 'stats.mfun',
                displayName: field2names['stats_mfun'],
                width: '5%'
            },
            stats_part: {
                name: 'PART',
                field: 'stats.part',
                displayName: field2names['stats_part'],
                width: '5%'
            },
            stats_tran: {
                name: 'TRAN',
                field: 'stats.tran',
                displayName: field2names['stats_tran'],
                width: '5%'
            },

            tags: {
                field: 'tags',
                displayName: 'Ambiguities',
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field].length > 1 ? row.entity[col.field].join(", ") : row.entity[col.field][0]}}</div>',
                width: '10%'
            },
            taxonomy: {
                field: 'taxonomy',
                displayName: field2names['taxonomy'],
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field].length > 1 ? row.entity[col.field].join(" > ") : row.entity[col.field][0]}}</div>',
                width: '25%'
            },
            homologs: {
                field: 'homologs',
                displayName: field2names['homologs'],
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                '{{row.entity[col.field].length > 1 ? row.entity[col.field].join(", ") : row.entity[col.field][0]}}</div>',
                width: '20%'
            }
        }
    }])

    .factory('defaultColumnsFactory', ['field2column', function (field2column) {
        return {
            protein_default: [
                field2column['disprot_id'],
                field2column['uniprot_accession'],
                field2column['protein_name'],
                field2column['organism'],
                field2column['taxonomy'],
                field2column['homologs']

            ],
            region_default: [
                field2column['disprot_id'],
                field2column['extent'],
                field2column['region_position'],
                field2column['method'],
                field2column['stats_mfun'],
                field2column['stats_tran'],
                field2column['stats_part'],
                field2column['xr'],
                field2column['tags'],
                field2column['last_curator']

            ]
        }
    }]);
