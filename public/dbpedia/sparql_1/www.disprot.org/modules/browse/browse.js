/**
 * Created by francesco on 05/06/16.
 */

angular.module('disprot.list', [
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.autoResize',
    'ui.grid.grouping',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.exporter',
    'ui.grid.moveColumns',
    'ui.grid.grouping',
    'grid_column_definitions'
])

    .value('mongo_projections', {
        _id: 0,
        mobidb: 0
        // record_type: 1,
        // disprot_id: 1,
        // uniprot_accession: 1,
        // protein_name: 1,
        // synonyms: 1,
        // organism: 1,
        // method: 1,
        // funct: 1,
        // pmid: 1,
        // start: 1,
        // end: 1,
        // comment : 1,
        // creation_date: 1,
        // features: 1,
        // last_edit_date: 1,
        // pdb_list: 1,
        // pfam: 1,
        // protein_type: 1,
        // sequence: 1,
        // name: 1,
        // primary_method: 1,
        // secondary_method: 1,
        // xr_id: 1,
        // xr_name: 1
    })


    .factory('browseService', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/browse/:record_type', {future_release: false})
    }])

    /*.factory('downloadService', ['WSROOT', '$resource', function (WSROOT, $resource) {
     return $resource(WSROOT + '/dl', {}, {
     post: {method: 'POST', isArray: true}
     })
     }])*/


    .controller('browseCtrl', ['defaultColumnsFactory', 'field2names', 'field2column', 'mongo_projections', '$scope', '$filter',
        'browseService', 'uiGridConstants', 'uiGridGroupingConstants', 'blobDownload', 'localStorageService',
        'curators', '$http', 'WSROOT',
        function (defaultColumnsFactory, field2names, field2column, mongo_projections, $scope, $filter, browseService,
                  uiGridConstants, uiGridGroupingConstants, blobDownload, localStorageService, curators, $http, WSROOT) {

            var pcol_blacklist = ['features', 'record_type', 'last_curator', 'protein_type'];
            var rcol_blacklist = ['mobidb', 'funct', 'record_type'];

            $scope.loading = true;
            $scope.field2names = field2names;

            $scope.gridOptions = {
                enableSorting: true,
                enableColumnResizing: true,
                enableResizing: true,
                enableFiltering: true,
                paginationPageSizes: [50, 100, 150],
                paginationPageSize: 50,
                useExternalPagination: false,
                // enableGridMenu: true,
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                }

            };

            $scope.table = 'proteins';
            $scope.setTable = function (val) {
                $scope.table = val;
            };

            $scope.checkTable = function (val) {
                return {active: $scope.table == val};
            };

            function map_fields(cn) {
                if (cn == 'stats.mfun') {
                    cn = 'stats_mfun'
                } else if (cn == 'stats.tran') {
                    cn = 'stats_tran'
                } else if (cn == 'stats.part') {
                    cn = 'stats_part'
                } else if (cn == 'method.name') {
                    cn = 'method'
                }
                return cn
            }

            function map_fields_reverse(cn) {
                if (cn == 'stats_mfun') {
                    cn = 'stats.mfun'
                } else if (cn == 'stats_tran') {
                    cn = 'stats.tran'
                } else if (cn == 'stats_part') {
                    cn = 'stats.part'
                } else if (cn == 'method') {
                    cn = 'method.name'
                }
                return cn
            }

            $scope.dl_options = {
                format: 'json',
                row_selection: 'all',
                col_selection: 'all',
                aggregate: false
            };
            $scope.col_options = {};

            $scope.$watch('dl_options.aggregate', function (n, o) {
                if (n != o && n) {
                    $scope.dl_options.col_selection = 'all';
                }
            });

            function browseCall_cb(result) {

                var proteins = $filter('filter')(result, {record_type: 'protein_record'});

                var regions = $filter('filter')(result, {record_type: 'region_record'});
                regions.forEach(function (curr) {
                    curr.extent = +curr.end - +curr.start;
                    curr.position = curr.start + '-' + curr.end;
                    curr.stats = {
                        mfun: curr.funct.mfun.reduce(function (out, curr) {
                            if (curr !== null) {
                                out++
                            }
                            return out
                        }, 0),
                        tran: curr.funct.tran.reduce(function (out, curr) {
                            if (curr !== null) {
                                out++
                            }
                            return out
                        }, 0),
                        part: curr.funct.part.reduce(function (out, curr) {
                            if (curr !== null) {
                                out++
                            }
                            return out
                        }, 0)
                    };
                    // curr.last_curator = curators[curr.last_curator].name;
                });

                var pcols = Object.keys(proteins[0]);
                pcol_blacklist.forEach(function (curr) {
                    pcols.splice(pcols.indexOf(curr), 1);
                });

                var rcols = Object.keys(regions[0]);
                rcol_blacklist.forEach(function (curr) {
                    rcols.splice(rcols.indexOf(curr), 1);
                });

                var rbkp, cbkp;
                $scope.$watch('table', function (newVal) {
                    if (newVal === 'proteins') {
                        rbkp = angular.copy($scope.gridOptions.columnDefs);
                        $scope.gridOptions.columnDefs = cbkp || angular.copy(defaultColumnsFactory.protein_default);
                        $scope.available_columns = pcols;
                        $scope.gridOptions.data = proteins;
                    } else if (newVal == 'regions') {
                        cbkp = angular.copy($scope.gridOptions.columnDefs);
                        $scope.gridOptions.columnDefs = rbkp || angular.copy(defaultColumnsFactory.region_default);
                        $scope.available_columns = rcols;
                        $scope.available_columns = $scope.available_columns.concat(['extent', 'position', 'stats_mfun', 'stats_tran', 'stats_part']);
                        $scope.gridOptions.data = regions;
                    }

                    $scope.default_fields = $scope.gridOptions.columnDefs.map(function (curr) {
                        var cn = map_fields(curr.field);
                        return cn
                    })
                });
                // $scope.loading = false;
            }

            var lsKey = localStorageService.keys();
            if (lsKey.indexOf('disprot-browse-data') === -1 || (+new Date() - lsKey.indexOf('disprot-browse-data-date')) > 2.628e+9) {

                browseService.query({
                    record_type: 'all',
                    projection: mongo_projections
                }, function (browse_data) {
                    localStorageService.set('disprot-browse-data', browse_data);
                    localStorageService.set('disprot-browse-data-date', +new Date());
                    $scope.result = browse_data;
                    browseCall_cb($scope.result);
                    $scope.loading = false;
                })

            } else {
                $scope.loading = false;
                $scope.result = localStorageService.get('disprot-browse-data');
                browseCall_cb($scope.result);
            }

            $scope.default_cols = function () {
                if ($scope.table === 'proteins') {
                    $scope.gridOptions.columnDefs = angular.copy(defaultColumnsFactory.protein_default);
                } else {
                    $scope.gridOptions.columnDefs = angular.copy(defaultColumnsFactory.region_default);
                }
                for (var k in $scope.col_options) {
                    if ($scope.default_fields.indexOf(k) === -1) {
                        $scope.col_options[k] = false;
                    }
                }
            };

            $scope.checkCB = function (cn) {
                cn = map_fields(cn);
                return $scope.default_fields.indexOf(cn) != -1;
            };

            $scope.show_all_cols = function () {
                $scope.available_columns.forEach(function (curr) {
                    $scope.pushToGrid(curr, false);
                    $scope.col_options[curr] = true;
                })
            };

            $scope.pushToGrid = function (colName, rm) {

                rm = angular.isUndefined(rm) ? true : rm;

                var new_cols = angular.copy($scope.gridOptions.columnDefs);


                if (new_cols.every(function (curr) {
                        var cn = map_fields(curr.field);
                        return cn !== colName
                    })) {

                    new_cols.push(field2column[colName])

                } else if (rm) {
                    var idx = new_cols.reduce(function (out, curr, curr_idx) {
                        var cn = map_fields(curr.field);
                        if (cn == colName)
                            out = curr_idx;
                        return out
                    }, 0);
                    new_cols.splice(idx, 1);
                }

                $scope.gridOptions.columnDefs = new_cols;
            };


            $scope.dl = function () {

                $scope.$emit('show.alert.success', {message: 'Download requested... be patient!'});

                var dpids, filename;

                // get disprot ids from the rows via gridApi
                if ($scope.dl_options.row_selection == 'visible') {

                    dpids = $scope.gridApi.grid.rows.filter(function (curr) {
                        return curr.visible
                    }).map(function (curr) {
                        return curr.entity.disprot_id;
                    });

                    filename = 'disprot_filter.' + $scope.dl_options.format;

                } else if ($scope.dl_options.row_selection == 'selected') {

                    dpids = $scope.gridApi.selection.getSelectedRows($scope.gridApi.grid)
                        .map(function (curr) {
                            return curr.disprot_id;
                        });

                    filename = 'disprot_filter.' + $scope.dl_options.format;

                } else if ($scope.dl_options.row_selection == 'all') {

                    dpids = $scope.gridOptions.data.map(function (curr) {
                        return curr.disprot_id
                    });

                    filename = 'disprot.' + $scope.dl_options.format;
                }

                // generate postdata object and start filling it
                var postdata = {
                    query: {
                        dpids: dpids.filter(function (curr, idx, inArr) {
                            return inArr.indexOf(curr) == idx
                        })
                    },
                    format: $scope.dl_options.format
                };

                if ($scope.dl_options.aggregate) {
                    postdata.aggregate = true;
                }

                if (!($scope.dl_options.aggregate)) {

                    if ($scope.table == 'regions') {
                        postdata.query.record_type = 'region_record';

                    } else if ($scope.table == 'proteins') {
                        postdata.query.record_type = 'protein_record';
                    }
                }

                if ($scope.dl_options.col_selection == 'visible') {
                    postdata.projection = $scope.gridOptions.columnDefs.reduce(function (out, curr) {
                        out[curr.field] = 1;
                        return out;
                    }, {});
                }

                $http({
                    method: 'POST',
                    url: WSROOT + '/dl',
                    data: postdata
                }).then(function (result) {
                    var dl = result.data;
                    blobDownload(dl, filename)
                }, function (error) {
                    $scope.$emit('show.alert.error', {message: 'Request returned an error.'});
                });

            }
        }]);