'use strict';

angular.module('disprot.auth', [])

    /*.config(['$routeProvider', function ($routeProvider) {
     $routeProvider.when('/login', {
     templateUrl: 'auth/auth.html',
     controller: 'AuthCtrl'
     });
     }])*/

    .factory('LoginCall', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/login', {}, {post: {method: 'POST'}});
    }])

    .factory('AuthenticationService', ['LoginCall', 'Base64', '$cookieStore', '$http', '$rootScope', '$location',
        function (LoginCall, Base64, $cookieStore, $http, $rootScope, $location) {
            var service = {};
            service.Login = function (username, password, callback) {

                /* Use this for real authentication
                 ----------------------------------------------*/
                //var authData = {'username': username, 'password': password};
                var authData = {'user': Base64.encode(username + ':' + password)};

                LoginCall.post(authData, function (res) {
                    return callback(null, res);
                }, function (err) {
                    return callback(err);
                });
            };

            service.SetCredentials = function (username, password, fullname) {

                var authdata = Base64.encode(username + ':' + password);
                $rootScope.globals = {
                    currentUser: {
                        username: username,
                        authdata: authdata,
						name: fullname
                    }
                };
                $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
                $cookieStore.put('globals', $rootScope.globals);

                // $rootScope.$emit('credentialsSet', $rootScope.globals.currentUser.username);
                $rootScope.$emit('show.alert.success', {message: 'Logged in as ' + $rootScope.globals.currentUser.username});

                console.log('User credentials set.');
            };

            service.ClearCredentials = function () {
                $rootScope.globals = {};
                $cookieStore.remove('globals');
                $http.defaults.headers.common.Authorization = undefined;
                console.log('User credentials cleared.');
                // $location.path('/');
            };
            return service;
        }])

    .factory('Base64', [function () {
        /* jshint ignore:start */

        var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        return {
            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;
                do {
                    chr1 = input.charCodeAt(i ++);
                    chr2 = input.charCodeAt(i ++);
                    chr3 = input.charCodeAt(i ++);
                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;
                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);
                return output;
            },
            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;
                // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                var base64test = /[^A-Za-z0-9\+\/\=]/g;
                if (base64test.exec(input)) {
                    window.alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
                }
                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
                do {
                    enc1 = keyStr.indexOf(input.charAt(i ++));
                    enc2 = keyStr.indexOf(input.charAt(i ++));
                    enc3 = keyStr.indexOf(input.charAt(i ++));
                    enc4 = keyStr.indexOf(input.charAt(i ++));
                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;
                    output = output + String.fromCharCode(chr1);
                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);
                return output;
            }
        };
    }])

    .run(function ($rootScope, $location, $cookieStore, $http, AuthenticationService) {
        $rootScope.location = $location;

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        // $rootScope.$on('$locationChangeStart', function (event, next, current) {
        //     // redirect to login page if not logged in
        //     if ($location.path() !== '/' && !$rootScope.globals.currentUser) {
        //         $location.path('/');
        //     }
        // });

        // add listener to logout ;)
        $rootScope.$on('logout.event', function () {
            AuthenticationService.ClearCredentials();
            $rootScope.$broadcast('show.alert.success', {message: 'Successfully logged out.'})
        })
    })

    .controller('authCtrl', ['$scope', '$location', 'AuthenticationService', function ($scope, $location, AuthenticationService) {
        $scope.user = {};

        $scope.submit = function () {
            // $scope.dataLoading = true;
            
            AuthenticationService.Login($scope.user.name, $scope.user.pass, function (err, res) {
                if (err) {
                    $scope.$emit('show.alert.error', {message: err.data.message});
                    return;
                }
                AuthenticationService.SetCredentials($scope.user.name, $scope.user.pass);
                // $location.path('/');
            });
        }
    }])

    .controller('usrOptCtrl', ['$rootScope', '$scope', '$uibModalInstance', 'Notification', function ($rootScope, $scope, $uibModalInstance) {

        $scope.user = $rootScope.globals.currentUser;

        $scope.ok = function () {
            $uibModalInstance.close();
            $scope.$emit('show.alert.primary', {message: 'Success?'});
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);