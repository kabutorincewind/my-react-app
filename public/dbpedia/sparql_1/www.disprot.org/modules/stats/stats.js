/**
 * Created by ftabaro on 18/10/16.
 */


angular.module('disprot.stats', [])

/*
 .directive('disprotPie', function ($filter) {
 return {
 restrict: 'E',
 scope: {
 plotData: '=',
 colors: '='
 },
 template: '<div id="pieArea"></div>',
 link: function (scope, elm, attrs) {

 var w = elm[0].clientWidth,
 h = w / 2,
 radius = Math.min(w, h) / 2,
 colors = scope.colors;

 var arc = d3.svg.arc()
 .outerRadius(radius - 10)
 .innerRadius(0);

 var labelArc = d3.svg.arc()
 .outerRadius(radius - 20)
 .innerRadius(radius - 20);

 var svg = d3.select('#pieArea')
 .append("svg")
 .attr("width", w)
 .attr("height", h)
 .append("g")
 .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")");

 scope.$watch('plotData', function (n, o) {
 if (!angular.isUndefined(n) && n !== o) {

 var pie = d3.layout.pie()
 .sort(null)
 .value(function (d) {
 return d.count
 });

 var g = svg.selectAll(".arc")
 .data(pie(scope.plotData))
 .enter().append("g")
 .attr("class", "arc");

 var c = 0;
 g.append("path")
 .attr("d", arc)
 .style("fill", function (d) {
 var col = colors[c];
 c += 1;
 return col
 });

 g.append("text")
 .attr("transform", function (d) {
 return "translate(" + labelArc.centroid(d) + ")";
 })
 .attr("dy", ".35em")
 .text(function (d) {
 var label = d.data.label.split('_')[0];
 label = $filter('capitalize')(label);
 label = d.data.count > 1 ? label + 's' : label;
 return label
 })

 }
 });
 }
 }
 })
 */


    .controller('statsCtrl', ['$scope', '$location', '$anchorScroll', 'localStorageService', 'statsService', 'curators', function ($scope, $location, $anchorScroll, localStorageService, statsService, curators) {

        function parse_taxonomy() {
            if (angular.isObject($scope.taxonomy["0"])) {
                for (var key in $scope.taxonomy) {
                    if (["0", "1"].indexOf(key) != -1) {
                        var final = [];
                        for (var key2 in $scope.taxonomy[key]) {
                            final.push({
                                term: key2,
                                value: $scope.taxonomy[key][key2]
                            })
                        }
                        $scope.taxonomy[key] = final;
                    }
                }
            }
        }

        $scope.tableToggle = {
            curators: true,
            methods: true,
            kingdoms: true,
            reign: true
        };

        // localStorageService.clearAll();
        var lsKey = localStorageService.keys();
        if (lsKey.indexOf('curators') === -1 &&
            lsKey.indexOf('methods') === -1 &&
            lsKey.indexOf('taxononmy') === -1) {

            $scope.loading = true;

            var unbind_curators = localStorageService.bind($scope, 'curators');
            var unbind_methods = localStorageService.bind($scope, 'methods');
            var unbind_taxonomy = localStorageService.bind($scope, 'taxonomy');

            statsService.getStats().$promise.then(function (result) {
                $scope.curators = result.curators;
                $scope.methods = result.methods;
                $scope.taxonomy = result.taxonomy;
                parse_taxonomy();

                $scope.loading = false;
            });
        } else {
            $scope.loading = false;

            $scope.curators = localStorageService.get('curators');
            $scope.methods = localStorageService.get('methods');
            $scope.taxonomy = localStorageService.get('taxonomy');

            // parse_taxonomy();

        }

        $anchorScroll.yOffset = 170;
        $scope.goTo = function (hash, event) {
            $location.hash(hash);
            $anchorScroll()
        };

        $scope.setClass = function (anchor_name) {
            var re = new RegExp(anchor_name, "g");
            return {
                active: re.test($location.hash())
            }
        };

    }])
;