/**
 * Created by francesco on 05/06/16.
 */

angular.module('disprot.list', [
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.autoResize',
    'ui.grid.grouping'
])

// .config(['$routeProvider', function ($routeProvider) {
//     $routeProvider.when('/list', {
//         templateUrl: 'list/list.html',
//         controller: 'listCtrl'
//     });
// }])

    .factory('browseService', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/browse/:record_type')
    }])

    .controller('listCtrl', ['$scope', 'browseService', 'uiGridGroupingConstants',
        function ($scope, browseService, uiGridGroupingConstants) {
            $scope.loading = true;

            $scope.table = 'proteins';
            $scope.setTable = function (val) {
                $scope.table = val;
            };

            $scope.checkTable = function (val) {
                return {active: $scope.table == val};
            };

            browseService.query({record_type: 'protein_record'}, function (proteins) {
                browseService.query({record_type: 'region_record'}, function (regions) {
                    var columns;

                    $scope.gridOptions = {
                        enableSorting: true,
                        enableColumnResizing: true,
                        enableResizing: true,
                        // enableFiltering: true,
                        // enableGridMenu: true,
                        onRegisterApi: function (gridApi) {
                            $scope.gridApi = gridApi;
                        }

                    };

                    $scope.$watch('table', function (newVal) {
                        if (newVal == 'proteins') {
                            columns = [
                                {
                                    field: 'original_disprot_id',
                                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                                    '<a ng-href="{{row.entity[col.field]}}">{{row.entity[col.field]}}' +
                                    '</a></div>'
                                },
                                {
                                    field: 'uniprot_id'
                                },
                                {
                                    field: 'protein_name'
                                },
                                {
                                    field: 'synonyms'
                                },
                                {
                                    field: 'source_organism'
                                }
                            ];

                            $scope.gridOptions.columnDefs = columns;
                            $scope.gridOptions.data = proteins;

                        } else if (newVal == 'regions') {
                            columns = [
                                {
                                    field: 'protein_id'
                                },
                                {
                                    field: 'variant_id'
                                },
                                {
                                    field: 'region_id'
                                },
                                {
                                    field: 'region_position'
                                },
                                {
                                    field: 'region_type'
                                },
                                {
                                    field: 'region_name'
                                }
                            ];

                            $scope.gridOptions.columnDefs = columns;
                            $scope.gridOptions.data = regions;
                        }
                    });

                    $scope.loading = false;
                })
            })
        }]);