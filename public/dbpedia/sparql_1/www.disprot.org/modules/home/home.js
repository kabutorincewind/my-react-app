'use strict';

angular.module('disprot.home', [])

/* .config(['$routeProvider', function ($routeProvider) {
 $routeProvider.when('/home', {
 templateUrl: 'home/home.html',
 controller: 'HomeCtrl'
 });
 }])*/

    .directive('homeSideBar', ['$http', '$window', function ($http, $window) {
        return {
            restrict: 'E',
            templateUrl: 'modules/home/partials/sideBar.html',
            scope: true,
            link: function (scope, element, attributes) {

                var scale = 0.8;
                var target = angular.element(document.querySelector('#changelog-content'));

                target.css('overflow-y', 'auto').css('max-height', Math.round($window.innerHeight * scale).toString() + 'px');

                angular.element($window).bind('resize', function () {
                    target.css('max-height', Math.round($window.innerHeight * scale).toString() + 'px')
                });

                $http.get('static/changelog.json').then(function (content) {
                    scope.changelog = content.data;
                })
            }
        }
    }])


    .controller('homeCtrl', ['$scope', 'statsService', function ($scope, statsService) {

        statsService.getStatsLight({}, function (res) {
            $scope.stats = res;
        })

    }]);