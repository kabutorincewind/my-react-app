/**
 * Created by francesco on 05/06/16.
 */

angular.module('disprot.submit', ['ui.select', 'ngSanitize'])

    .value('model_object', {
        protein: {
            last_curator: null,
            creation_date: null,
            last_edit_date: null,
            // protein_type: null,
            sequence: null
        },
        regions: []
    })

    .value('region_object', {
        start: null,
        end: null,
        pmid: null,
        method: null,
        xr_name: null,
        xr_id: null,
        name: null,
        funct: {
            mfun: [],
            tran: [],
            part: []
        },
        creation_date: null,
        last_edit_date: null,
        last_curator: null,
        reference_disprot_id: null,
        comment: null
    })

    .value('cm', {
        D: '#FF0000',
        S: '#2B65EC',
        C: '#FFFFFF',
        d: '#FFA500',
        s: '#ADD8E6'
    })

    .factory('checkDisorderData', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/disorder', {}, {
            post: {method: 'POST', isArray: false}
        })
    }])

    .factory('getDisorderData', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/getDisorder/:entry_id')
    }])

    .factory('getOntologies', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/ontologies')
    }])

    .factory('uploadRegions', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/updateEntry', {}, {
            post: {method: 'POST', isArray: false}
        })
    }])

    .factory('testRouteService', ['$location', function ($location) {
        return function (route) {
            var patt = new RegExp(route, 'g');
            return patt.test($location.path());
        }
    }])

    .factory('generateDrawObjects', ['str2col', 'cm', function (str2col, cm) {
        return {
            generateFvPfamTrack: function (inArr, label) {
                return [inArr.map(function (curr, idx) {
                    return {
                        x: +curr.start,
                        y: +curr.end,
                        color: str2col(curr.accession),
                        id: curr.accession + '_' + (idx + 1).toString(),
                        description: curr.description
                    }
                }), label, 'lightslategray']
            },
            generateFvMobidbTrack: function (inArr, label, id) {
                // 'xray_consensus_'
                return [inArr.map(function (curr, idx) {
                    return {
                        x: +curr.start,
                        y: +curr.end,
                        color: cm[curr.ann],
                        id: id + (idx + 1).toString()
                    }
                }), label, 'lightslategray']
            },
            generatePdbTracks: function (inArr, filter_array) {
                var res = {};
                inArr.filter(function (curr) {
                    return filter_array.indexOf(curr.type) != -1
                }).forEach(function (curr) {
                    var track_name = curr.type.toLowerCase().substr(0, 1) + '_' + curr.id.toLowerCase() + '_'
                        + curr.chains.reduce(function (out, c, c_idx, arr) {
                            out += c.id + '/';
                            if (c_idx == arr.length - 1) {
                                out = out.substr(0, out.length - 1);
                                if (out.length >= 10) {
                                    out = out.substr(0, 7) + '...'
                                }
                            }
                            return out
                        }, '');

                    res[curr.id] = [curr.consensus.map(function (c, i) {
                        return {
                            x: +c.start,
                            y: +c.end,
                            color: cm[c.ann],
                            id: curr.id.toLowerCase() + '_' + (i + 1).toString()
                        }
                    }), track_name, 'lightslategray']
                });
                return res
            }
        }
    }])

    .factory('openRegionHelper', ['$uibModal', function ($uibModal) {
        return function (scope, region_data, nregions) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/submit/partials/regionHelper.html',
                controller: 'regionHelperCtrl',
                scope: scope,
                size: 'sm',
                resolve: {
                    region_data: function () {
                        return region_data
                    },
                    nregions: function () {
                        return nregions
                    }
                }
            });
            return modalInstance
        }
    }])

    .directive('validMethod', function ($q) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                ctrl.$asyncValidators.methodInput = function (modelValue, viewValue) {

                    var def = $q.defer();

                    if (ctrl.$isEmpty(modelValue) || viewValue === null) {

                        def.reject();
                    } else {

                        def.resolve()
                    }

                    return def.promise;
                };
            }
        };
    })

    .directive('regionForm', ['$filter', 'getOntologies', function ($filter, getOntologies) {
        return {
            restrict: "E",
            scope: {
                idx: '=',
                content: '=',
                seqlen: '='
            },
            templateUrl: "/modules/submit/partials/region_form.html",
            link: function (scope, elm, attrs) {

                // Functions for region function management
                scope.pushFunction = function (target_string) {
                    var ontology = target_string.split('.')[0];
                    scope.content.funct[ontology].push([null, null]);
                };

                scope.rmFunction = function (target_string) {
                    var ontology = target_string.split('.')[0];
                    var idx = +(target_string.split('.')[1]);
                    scope.content.funct[ontology].splice(idx, 1);
                };

                scope.groupMfun = function (item) {
                    var key = item.id.split('.');
                    return scope.ontologies.mfun_primary[key[0]];
                };

                scope.groupMethods = function (item) {
                    var key = item.id;
                    return scope.ontologies.method_group[key];
                };

                scope.clearRegionFunctions = function () {
                    scope.content.funct.mfun = [null];
                    scope.content.funct.tran = [null];
                    scope.content.funct.part = [null];
                };

                scope.ontologies = {
                    mfun: [],
                    mfun_primary: {},
                    tran: [],
                    part: [],
                    ec: [],
                    method: [],
                    method_group: {}
                };


                getOntologies.query({}, function (result) {

                    scope.ontologies.mfun = $filter('filter')(result, {ontology: 'MFUN'});
                    scope.ontologies.mfun.forEach(function (curr) {
                        if (curr.id.indexOf('.') === -1)
                            scope.ontologies.mfun_primary[curr.id.split('.')[0]] = curr.name;
                    });
                    scope.ontologies.mfun = $filter('filter')(result, {ontology: 'MFUN', level: 1});

                    scope.ontologies.tran = $filter('filter')(result, {ontology: 'TRAN', level: 1});
                    scope.ontologies.part = $filter('filter')(result, {ontology: 'PART', level: 1});
                    // scope.ontologies.ec = $filter('filter')(result, {ontology: 'EC', level: 1});


                    var all_methods = $filter('filter')(result, {ontology: 'DMET', level: 1});
                    all_methods.forEach(function (curr) {
                        var group;
                        if (/(xray)|(nmr)/i.test(curr.id)) {
                            group = 'Primary detection methods'
                        } else {
                            group = 'Other detection methods'
                        }
                        scope.ontologies.method_group[curr.id] = group;
                    });
                    scope.ontologies.methods = all_methods;
                });
            }
        }
    }])

    .directive('multirowButtons', [function () {
        return {
            restrict: "E",
            scope: {
                index: '=thisIdx',
                addFunction: '=',
                removeFunction: '=',
                cpFunction: '=',
                size: '=',
                showRemove: '='
            },
            template: '<div class="btn-group pull-right">' +
            '<button id="cpBtn" type="button" class="btn btn-default" ng-class="size" ng-click="cpFunction(index)" ng-if="cpFunction"><i class="glyphicon glyphicon-duplicate"></i></button>' +
            '<button id="addBtn" type="button" class="btn btn-default" ng-class="size" ng-click="addFunction(index)"><i class="glyphicon glyphicon-plus-sign"></i></button>' +
            '<button id="removeBtn" type="button" class="btn btn-default" ng-class="size" ng-click="removeFunction(index)" ng-if="showRemove"><i class="glyphicon glyphicon-minus-sign"></i></button>' +
            '</div>'
        }
    }])

    .directive('featureViewerSubmit', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            template: '<div id="{{viewerId}}" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>',
            // template: '<div id="{{viewerId}}" style="width : 100%"></div>',
            scope: {
                viewerId: '@',
                drawData: '=',
                refresh: '=',
                ref: '=',
                orderTracks: '='
            },
            link: function (scope, elm, attrs) {
                var fv;

                // workaround to sort dict, if required.
                if (scope.orderTracks) {
                    var revDict = {}, sortValues = [], keys = Object.keys(scope.drawData);
                    for (var i = 0; i < keys.length; i++) {
                        var k = keys[i];
                        sortValues.push(scope.drawData[k][1]);
                        revDict[scope.drawData[k][1]] = k;
                    }
                    sortValues.sort();
                }


                $timeout(function () {
                    start()

                });

                scope.$watch('refresh', function (n, o) {
                    if (n !== o) {
                        fv.clearInstance();
                        angular.element(document.querySelector('#' + scope.viewerId)).empty();
                        start()
                    }
                }, true);

                // viewer functions
                var start = function () {
                    init();
                    draw();

                    fv.onFeatureSelected(function (feat) {
                        // console.log(feat.detail);
                        scope.$emit('featureSelected', feat.detail);
                    });
                };

                var init = function () {
                    fv = new FeatureViewer(scope.ref,
                        '#' + scope.viewerId, {
                            showAxis: true,
                            showSequence: true,
                            brushActive: true, //zoom
                            toolbar: false, //current zoom & mouse position
                            bubbleHelp: false,
                            zoomMax: 10 //define the maximum range of the zoom
                        });
                };

                var addFeature = function (k) {
                    if ((scope.drawData[k][0][0]['x'] != null &&
                        scope.drawData[k][0][0]['y'] != null) ||
                        k == 'dummy'
                    ) {
                        fv.addFeature({
                            data: scope.drawData[k][0],
                            name: scope.drawData[k][1],
                            color: scope.drawData[k][2],
                            type: 'rect'
                        })
                    }
                };

                var draw = function () {
                    if (scope.orderTracks) {
                        for (var i = 0; i < sortValues.length; i++) {
                            var ki = sortValues[i];
                            var k = revDict[ki];
                            addFeature(k)
                        }
                    } else {
                        for (var k in scope.drawData) {
                            // console.log(scope.drawData[k])
                            if (scope.drawData.hasOwnProperty(k) &&
                                scope.drawData[k][0].length > 0) {
                                addFeature(k)
                            }
                        }
                    }
                };


            }
        }
    }])

    .controller('submitCtrl', ['model_object', 'region_object', 'cm', '$rootScope', '$scope', '$location', '$filter', '$uibModal', 'checkDisorderData', 'getDisorderData', 'testRouteService', 'generateDrawObjects', 'openRegionHelper', 'getOntologies', 'uploadRegions',
        function (model_object, region_object, cm, $rootScope, $scope, $location, $filter, $uibModal, checkDisorderData, getDisorderData, testRouteService, generateDrawObjects, openRegionHelper, getOntologies, uploadRegions) {

            var submit = testRouteService('submit');

            $scope.show = {};
            $scope.new_entry = false;
            $scope.error = false;

            var initFunctions = function (idx) {
                $scope.model.regions[idx].funct.mfun.push(null);
                $scope.model.regions[idx].funct.tran.push(null);
                $scope.model.regions[idx].funct.part.push(null);
            };

            var initUser = function (idx) {
                $scope.model.regions[idx].last_curator = $rootScope.globals.currentUser.username;
                $scope.model.regions[idx].creation_date = new Date();
                $scope.model.regions[idx].last_edit_date = $scope.model.regions[idx].creation_date;
            };
            var initMethod = function (idx) {
                $scope.model.regions[idx].method = null;
                // $scope.model.regions[idx].primary_method = "None";
                // $scope.model.regions[idx].secondary_method = null;
            };

            $scope.newSearch = function () {
                $scope.steps = ['Start'];

                $scope.model = angular.copy(model_object);
                $scope.model.regions.push(angular.copy(region_object));
                // $scope.model.protein.protein_type = "native";

                initFunctions(0);
                initUser(0);
                initMethod(0);

                $scope.model.protein.last_curator = $rootScope.globals.currentUser.username;
                $scope.model.protein.creation_date = new Date();

                $scope.loading = false;
                $scope.submitted = false;
                $scope.error = false;
            };

            if (submit) {
                $scope.newSearch();
            } else {
                $scope.loading = true;
                $scope.submitted = false;
                $scope.error = false;
            }

            $scope.re = {
                // unp: /^[OPQ][0-9][A-Z0-9]{3}[0-9]$|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}$/,
                unp: /^[OPQ][0-9][A-Z0-9]{3}[0-9](-[0-9]+)*$|^[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}(-[0-9]+)*$/,
                // disprot: /^DP[0-9]{5}$|^DP[0-9]{5}_C[0-9]{3}$/,
                disprot: /^DP[0-9]{5}(_A[0-9]{3})*$/
            };

            var old_regions;
            var toremove_regions = [];

            var queryCallback = function (docs) {

                // console.log(result)

                if (submit) {
                    $scope.model.protein = docs;
                    $scope.reference_sequence = docs.sequence;

                    // Retrieve regions already annotated
                    var query = {entry_id: $scope.model.protein.disprot_id};
                    getDisorderData.query(query, function (result_reg) {
                        result_reg = $filter('filter')(result_reg, {record_type: 'region_record'});
                        if (result_reg.length > 0)
                            $scope.model.regions = result_reg;

                    })

                } else {

                    $scope.model = angular.copy(model_object);
                    var protein_record = $filter('filter')(docs, {record_type: 'protein_record'});
                    $scope.model.protein = $filter('arr2obj')(protein_record);
                    $scope.reference_sequence = $scope.model.protein.sequence;


                    $scope.model.regions = $filter('filter')(docs, {record_type: 'region_record'});

                    // console.log($scope.model.regions)

                    if ($scope.model.regions.length === 0) {

                        $scope.model.regions.push(angular.copy(region_object));

                        initFunctions(0);
                        initUser(0);
                        initMethod(0);

                    } else {
                        old_regions = angular.copy($scope.model.regions);
                    }

                    // console.log($scope.model.regions);
                }

                $scope.drawConsensus = {};

                if ($scope.model.protein.hasOwnProperty('pfam')) {
                    $scope.drawConsensus.pfam = generateDrawObjects.generateFvPfamTrack($scope.model.protein.pfam, 'Pfam');
                }

                if ($scope.model.protein.hasOwnProperty('mobidb')) {
                    if ($scope.model.protein.mobidb.hasOwnProperty('consensus')) {
                        if ($scope.model.protein.mobidb.consensus.hasOwnProperty('pdb_nmr')) {
                            $scope.drawConsensus.pdb_nmr = generateDrawObjects.generateFvMobidbTrack($scope.model.protein.mobidb.consensus.pdb_nmr, 'NMR', 'nmr_consensus_');
                        }
                        if ($scope.model.protein.mobidb.consensus.hasOwnProperty('pdb_xray')) {
                            $scope.drawConsensus.pdb_xray = generateDrawObjects.generateFvMobidbTrack($scope.model.protein.mobidb.consensus.pdb_xray, 'X-ray', 'xray_consensus_');
                        }
                        if ($scope.model.protein.mobidb.consensus.hasOwnProperty('predictors')) {
                            $scope.drawConsensus.predictors = generateDrawObjects.generateFvMobidbTrack($scope.model.protein.mobidb.consensus.predictors, 'Predictors', 'predictors_consensus_');
                        }
                        $scope.sort_consensus = false;
                        // console.log($scope.drawConsensus)
                    }
                    if ($scope.model.protein.mobidb.hasOwnProperty('pdb')) {
                        $scope.drawDetails = generateDrawObjects.generatePdbTracks($scope.model.protein.mobidb.pdb, ['xray', 'nmr']);
                        $scope.sort_pdb = true;
                    }
                }

                if (submit) $scope.steps.push('Manual Disorder Annotation');

                $scope.loading = false;
                $scope.submitted = true;
                $scope.refresh = {};
                $scope.seqLen = $scope.model.protein.sequence.length;
            };

            $scope.newEntryToggle = function () {
                $scope.new_entry = true;
            };

            if (submit) {
                $scope.proceed = function () {

                    $scope.steps.push('Automatic Features Computation');
                    $scope.loading = true;

                    // Choose the correct ID based on user click
                    if ($scope.new_entry)
                        delete $scope.model.protein.disprot_id;
                    else
                        delete $scope.model.protein.uniprot_id;
                    $scope.new_entry = false;


                    checkDisorderData.post($scope.model, function (result) {
                        queryCallback(result);

                        // update viewer with known regions
                        $scope.model.regions.forEach(function (_, idx) {
                            updateFV(idx);
                        });

                    }, function (error) {
                        //console.log(error.data.message);
                        $scope.loading = false;
                        $scope.submitted = false;

                        $scope.error = true;
                        $scope.errorMessage = error.data.message;
                        // $scope.$emit('show.alert.error', {message: error.data.message})
                    });
                };

            } else {
                var query = $location.search();
                getDisorderData.query(query, function (result) {

                    queryCallback(result);

                    // update viewer with known regions
                    $scope.model.regions.forEach(function (_, idx) {
                        updateFV(idx);
                    });

                })
            }

            var insertRegion = function (idx, newRegion) {

                var l = $scope.model.regions.length;
                var before = $scope.model.regions.slice(0, idx);
                var after = $scope.model.regions.slice(idx, l + 1);
                $scope.model.regions = before.concat(newRegion, after);
            };

            var validateRegion = function (idx) {
                return $scope.model.regions[idx].pmid &&
                    $scope.model.regions[idx].start &&
                    $scope.model.regions[idx].end
            };

            var updateFV = function (idx) {
                if (validateRegion(idx)) {
                    var refresh = true;
                    if ($scope.drawConsensus.hasOwnProperty($scope.model.regions[idx].pmid)) {
                        if ($scope.drawConsensus[$scope.model.regions[idx].pmid][0].some(function (curr) {
                                return curr.x == $scope.model.regions[idx].start && curr.y == $scope.model.regions[idx].end
                            })) {
                            refresh = false;
                        } else {
                            $scope.drawConsensus[$scope.model.regions[idx].pmid][0].push({
                                x: $scope.model.regions[idx].start,
                                y: $scope.model.regions[idx].end,
                                id: $scope.model.regions[idx].pmid
                            })
                        }

                    } else {
                        $scope.drawConsensus[$scope.model.regions[idx].pmid] = [
                            [
                                {
                                    x: $scope.model.regions[idx].start,
                                    y: $scope.model.regions[idx].end,
                                    id: $scope.model.regions[idx].pmid
                                }
                            ], $scope.model.regions[idx].pmid, 'black'];
                    }
                    if (refresh) $scope.refresh.consensus = !$scope.refresh.consensus;
                    // $scope.$emit('show.alert.success', {message: 'Region ' + (idx + 1).toString() + ' valid. Viewer updated.'});
                } else {
                    $scope.error = true;
                    // $scope.$emit('show.alert.error', {message: 'Region ' + (idx + 1).toString() + ' invalid.'});
                }
            };

// Regions management functions
            $scope.addRegion = function (idx) {
                updateFV(idx);

                var new_region_object = angular.copy(region_object);
                new_region_object.last_curator = $rootScope.globals.currentUser.username;
                new_region_object.creation_date = new Date();
                new_region_object.last_edit_date = new_region_object.creation_date;

                insertRegion(idx + 1, new_region_object);
                initFunctions(idx + 1);
                initUser(idx + 1);
                initMethod(idx + 1);
                // $scope.$emit('show.alert.success', {message: 'New region ' + (idx + 1).toString() + ' created.'});
            };

            $scope.removeRegion = function (idx) {
                toremove_regions.push($scope.model.regions[idx]);
                $scope.model.regions.splice(idx, 1);
                // $scope.$emit('show.alert.error', {message: 'Region ' + (idx + 1).toString() + ' removed.'});

            };

            $scope.copyRegion = function (source_idx) {
                updateFV(source_idx);

                var new_region_object = angular.copy($scope.model.regions[source_idx]);
                new_region_object.last_curator = $rootScope.globals.currentUser.username;
                new_region_object.creation_date = new Date();
                new_region_object.last_edit_date = new_region_object.creation_date;

                insertRegion(source_idx, new_region_object);
                // $scope.$emit('show.alert.success', {message: 'Region ' + (source_idx + 1).toString() + ' copied.'});
            };

            $scope.resetRegions = function () {
                toremove_regions = angular.copy($scope.model.regions);
                $scope.model.regions = [];
                $scope.model.regions.push(angular.copy(region_object));
                initFunctions(0);
                initUser(0);
                initMethod(0);
                $scope.$emit('show.alert.error', {message: 'Region list reset.'});
            };

//		
//		$scope.$on('featureSelected', function (event, data) {
//
//			var modalInstance = openRegionHelper($scope, data, $scope.model.regions.length);
//
//			modalInstance.result.then(function (data) {
//				console.log("data",data);
//				$scope.model.regions[+data.target_region - 1].start = data.start;
//				$scope.model.regions[+data.target_region - 1].end = data.end;
//				if (data.hasOwnProperty('method')) {
//					$scope.model.regions[+data.target_region - 1].method = data.method;
//				} else {
//					$scope.model.regions[+data.target_region - 1].xr_name = data.xr_name;
//					$scope.model.regions[+data.target_region - 1].xr_id = data.xr_id;
//					$scope.model.regions[+data.target_region - 1].name = data.description;
//				}
//			});
//		});


            /*$scope.addRegion = function () {
             var modalInstance = $uibModal.open({
             animation: true,
             templateUrl: 'modules/submit/partials/add_track_modal.html',
             controller: 'addTrackCtrl',
             scope: $scope,
             size: 'lg',
             resolve: {
             data: function () {
             return $scope.model;
             }
             }
             });
             };*/

            /*$scope.$on('add.fv.track', function (event, data) {
             $scope.drawConsensus['test'] = [data, 'test', 'black'];
             $scope.refresh.consensus = true;
             });*/

            $scope.example1 = function () {
                $scope.model.protein.uniprot_id = 'P04637';
                // $scope.model.protein.protein_type = 'native';
            };

            $scope.example2 = function () {
                $scope.model.protein.uniprot_id = 'Q92838-3';
                // $scope.model.protein.protein_type = 'isoform';
            };


            $scope.example3 = function () {
                $scope.model.protein.disprot_id = 'DP00086';
                // $scope.model.protein.protein_type = 'native';
            };


            $scope.example4 = function () {
                $scope.model.protein.disprot_id = 'DP00084_A002';
                // $scope.model.protein.protein_type = 'isoform';
            };


            $scope.checkRoute = function (route) {
                return testRouteService(route)
            };

            $scope.sendData = function () {

                // $scope.show.submit = true;
                var postdata = {
                    protein: $filter('dropKeys')($scope.model.protein, ['mobidb', 'pfam']),
                    regions: $scope.model.regions
                };

                //console.log(postdata)

                if (!(submit)) {
                    postdata.old_regions = old_regions;
                    postdata.toremove_regions = toremove_regions;
                }

                uploadRegions.post(postdata, function (result) {
                    console.log($scope.model.protein.disprot_id)
                    $location.path('/entry/' + $scope.model.protein.disprot_id);

                    var msg = 'New regions: ' + result.nups.toString() + '. Modified regions: ' + result.nmod.toString() + '. Deleted regions: ' + result.ndel.toString() + '.';
                    $scope.$emit('show.alert.success', {message: msg})
                }, function (error) {
                    $scope.$emit('show.alert.error', {message: error.data.message})
                })
            };

            $scope.removeProtein = function () {

                // $scope.show.submit = true;
                var postdata = {
                    protein: $filter('dropKeys')($scope.model.protein, ['mobidb', 'pfam']),
                    remove_protein: true,
                    regions: $scope.model.regions
                };
                console.log(postdata);
                uploadRegions.post(postdata, function (result) {

                    $location.path('/dashboard');

                    $scope.$emit('show.alert.success', {message: "Removed protein"})
                }, function (error) {
                    $scope.$emit('show.alert.error', {message: "Failed to remove protein"})
                })
            };


            /*$scope.model = {
             owner: $scope.user,
             protein: {
             record_type: 'protein_record'
             },
             variant: {
             record_type: 'variant_record'
             },
             // regions: {},
             // evidences: {}
             regions: [
             {
             region_id: "...",
             evidences: [
             {evidence_id : "..."},
             {}
             ]
             }
             ]
             };*/

            /*$scope.pushToModel = function (key, idx, rid) {
             // rid is an optional parameter, it is a string, e.g. 1-2.
             // the first number is the region id, the second the evidence id.

             // It maps the region_id which the current evidence belong
             rid = rid || null;

             var obj;
             console.log(key)
             if (key == 'evidences') {
             obj = {
             record_type: 'evidence_record',
             region_id: rid,
             evidence_id: idx,
             method_parameters: [],
             method_additives: [],
             struct_funct_type: [],
             funct_class: [],
             funct_subclass: []
             };

             // obj.region_id = +rid.split('-')[0];
             console.log(rid)
             $scope.model.regions[rid - 1].evidences.push(obj)
             } else if (key == 'regions') {
             obj = {
             record_type: 'region_record',
             region_id: idx,
             region_position: new Array(2),
             evidences: []
             };
             $scope.model.regions.push(obj)
             } else if (key == 'additives') {
             $scope.model.regions[rid - 1].evidences[idx - 1].method_additives.push(new Array(3));
             } else if (key == 'parameters') {
             console.log(rid)
             console.log(idx)
             $scope.model.regions[rid - 1].evidences[idx - 1].method_parameters.push(new Array(3));
             }
             };

             $scope.getSelectData = function (path, key) {
             getFile(path)
             .then(function (result) {
             $scope[key] = result.data;
             });
             };

             $scope.checkForm = function () {
             var modalInstance = $uibModal.open({
             animation: true,
             templateUrl: 'modules/submit/partials/confirmation_modal.html',
             controller: 'confirmSubmissionCtrl',
             size: 'lg',
             resolve: {
             data: function () {
             return $scope.model;
             }
             }
             });
             };

             $scope.reset = function () {
             var modalInstance = $uibModal.open({
             animation: true,
             template: '<div class="modal-header">' +
             '<h3 class="modal-title">Reset Submission Form</h3>' +
             '</div>' +
             '<div class="modal-body">' +
             '<p>Confirm?</p>' +
             '</div>' +
             '<div class="modal-footer">' +
             '<button class="btn btn-success" type="button" ng-click="confirm()">Confirm</button>' +
             '<button class="btn btn-warning" type="button" ng-click="cancel()">Discard</button>' +
             '</div>',
             controller: 'resetSubmissionCtrl',
             size: 'sm'
             });
             }*/


        }])

    /* .controller('addTrackCtrl', ['$scope', '$uibModalInstance', 'data',
     function ($scope, $uibModalInstance, data) {
     $scope.model = data;

     $scope.confirm = function () {

     $scope.$emit('add.fv.track', [{
     x: 1,
     y: 10,
     id: 'lol'
     }]);

     $uibModalInstance.close();
     };

     $scope.reset = function () {
     $scope.regions = [1];
     $scope.$broadcast('reset.data')
     };

     $scope.cancel = function () {
     $uibModalInstance.dismiss('cancel');
     };
     }]);*/

    /* .controller('confirmSubmissionCtrl', ['$scope', '$uibModalInstance', 'submitData', 'data',
     function ($scope, $uibModalInstance, submitData, data) {
     $scope.data = data;

     $scope.confirm = function () {
     submitData.post(data, function (res) {
     console.log(res);
     $scope.$emit('show.alert.success', {message: 'Submission successful.'});
     });
     $uibModalInstance.close();
     };

     $scope.cancel = function () {

     $scope.$emit('show.alert.error', {message: 'Submission deleted.'});
     $uibModalInstance.dismiss('cancel');
     };
     }])

     .controller('resetSubmissionCtrl', ['$scope', '$locationS', '$window', '$uibModalInstance',
     function ($scope, $location, $window, $uibModalInstance) {
     $scope.confirm = function () {
     $scope.$emit('show.alert.error', {message: 'Reset'});
     $uibModalInstance.close();
     $window.location.reload();
     };

     $scope.cancel = function () {
     $uibModalInstance.dismiss('cancel');
     };
     }]);
     */

    .controller('regionHelperCtrl', ['$scope', '$filter', '$timeout', '$uibModalInstance', 'region_data', 'nregions',
        function ($scope, $filter, $timeout, $uibModalInstance, region_data, nregions) {
            $scope.show = {};
            if (/(ray)|(nmr)/i.test(region_data.id)) {
                $scope.show.method = true;
                region_data.method = $filter('capitalize')(region_data.id.split('_')[0]);
            } else {
                $scope.show.method = false;
                if (/pf/i.test(region_data.id)) {
                    region_data.xr_name = 'Pfam';
                    region_data.xr_id = region_data.id.split('_')[0].toUpperCase();
                } else {
                    region_data.xr_name = 'PDB';
                    region_data.xr_id = region_data.id.split('_')[0].toLowerCase();
                }

            }
            $scope.region_data = region_data;

            $scope.regions = [];
            for (var i = 0; i < nregions; i++) {
                $scope.regions.push(i + 1);
            }

            $scope.confirm = function () {
                $uibModalInstance.close($scope.region_data);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };

        }]);