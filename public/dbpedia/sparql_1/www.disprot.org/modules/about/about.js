/**
 * Created by ftabaro on 28/06/16.
 */

angular.module('disprot.about', ['disprot.curators'])

    .factory('getOntologies', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/ontologies', {})
    }])

    .controller('aboutCtrl', ['$scope', '$location', '$anchorScroll', 'statsService', 'getOntologies', 'browseService', 'blobDownload',
        function ($scope, $location, $anchorScroll, statsService, getOntologies, browseService, blobDownload) {

            // $scope.pieColors = ["#A5A729", "#E8D262", "#FFBE3F", "#FF6F49"];

//        statsService.getStats({}, function (res) {
//            $scope.stats = res;
//
//            var plotStats = [];
//            for (var k in res.type) {
//                plotStats.push({
//                    label: k,
//                    count: res.type[k]
//                })
//            }
//            $scope.plotStats = plotStats;
//
//        });

            $scope.tableToggle = {
                'mfun': false,
                'tran': false,
                'part': false,
                'met': false
            };

            $scope.ontologies = getOntologies.query();


            $anchorScroll.yOffset = 170;
            $scope.goTo = function (hash, event) {
                $location.hash(hash);
                $anchorScroll()
            };

            $scope.setClass = function (anchor_name) {
                var re = new RegExp(anchor_name, "g");
                return {
                    active: re.test($location.hash())
                }
            };

            $scope.showTable = function (selected_key) {
                for (var key in $scope.tableToggle) {
                    if (key == selected_key) {
                        $scope.tableToggle[key] = !$scope.tableToggle[key];
                    } else {
                        $scope.tableToggle[key] = false;
                    }
                }
            };

            $scope.dl = function (ont, format) {

                var filenames = {
                    mfun: 'disprot_molecular_function',
                    tran: 'disprot_molecular_transition',
                    part: 'disprot_molecular_partner',
                    dmet: 'disprot_methods'
                };


                var data_arr = angular.copy($scope.ontologies).filter(function (curr) {
                    var test;
                    if (curr.hasOwnProperty('level')) {
                        test = curr.ontology == ont.toUpperCase() && curr.level == 1;
                    } else {
                        test = curr.ontology == ont.toUpperCase();
                    }
                    return test
                }).map(function (curr) {

                    delete curr.level;
                    delete curr.ontology;

                    return curr
                });

                if (format == 'json') {
                    blobDownload(data_arr, filenames[ont] + '.json');
                } else {
                    blobDownload(data_arr.reduce(function (out, curr, idx) {
                        if (idx == 0) {
                            out += '"';
                            out += Object.keys(curr).filter(function (curr2) {
                                return curr2 !== "$$hashKey";
                            }).join('";"');
                            out += '"\n';
                        }
                        for (var key in curr) {
                            if (curr.hasOwnProperty(key) && key != '$$hashKey') {
                                out += '"' + curr[key] + '";'
                            }
                        }
                        out = out.slice(0, -1);
                        out += '\n';
                        return out
                    }, ""), filenames[ont] + '.csv')
                }

            }
        }]);