/**
 * Created by ftabaro on 27/09/16.
 */

angular.module('disprot.curators', [])

// awk -F \\t '{printf("\"%s\":\{\"name\":\"%s\"\,\"proteins\":[],\"regions\":0,\"residues\":0},",$1,$3); if (NR%2==0) printf("\n")}END{printf("\n")}' users_file
    .value('curators', {
        "zdosztanyi": {
            "name": "Zsuzsanna Dosztanyi",
            "group": "Dosztanyi",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "aelofsson": {"name": "Arne Elofsson", "group": "Elofsson", "proteins": [], "regions": 0, "residues": 0},
        "ktsirigos": {
            "name": "Konstantinos D. Tsirigos",
            "group": "Elofsson",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "pwarholm": {"name": "Per Warholm", "group": "Elofsson", "proteins": [], "regions": 0, "residues": 0},
        "akajava": {"name": "Andrey Kajava", "group": "Kajava", "proteins": [], "regions": 0, "residues": 0},
        "droche": {"name": "Daniel Roche", "group": "Kajava", "proteins": [], "regions": 0, "residues": 0},
        "aschramm": {"name": "Antoine Schramm", "group": "Longhi", "proteins": [], "regions": 0, "residues": 0},
        "esalladini": {"name": "Edoardo Salladini", "group": "Longhi", "proteins": [], "regions": 0, "residues": 0},
        "slonghi": {"name": "Sonia Longhi", "group": "Longhi", "proteins": [], "regions": 0, "residues": 0},
        "smribeiro": {
            "name": "Sandra Macedo-Ribeiro",
            "group": "Macedo-Ribeiro",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "ahatos": {"name": "András Hatos", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "ameszaros": {"name": "Attila Meszaros", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "atantos": {"name": "Agnes Tantos", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "bszabo": {"name": "Beata Szabo", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "eschad": {"name": "Eva Schad", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "lkalmar": {"name": "Lajos Kalmar", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "mmacossay": {
            "name": "Mauricio Macossay Castillo",
            "group": "Tompa",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "nmurvai": {"name": "Nikoletta Murvai", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "ptompa": {"name": "Peter Tompa", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "tlazar": {"name": "Tamas Lazar", "group": "Tompa", "proteins": [], "regions": 0, "residues": 0},
        "agasparini": {
            "name": "Alessandra Gasparini",
            "group": "Tosatto",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "biocomp": {"name": "DisProt admin", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "dpiovesan": {"name": "Damiano Piovesan", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "eleonardi": {"name": "Emanuela Leonardi", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "fquaglia": {"name": "Federica Quaglia", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "ftabaro": {"name": "Francesco Tabaro", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "ftonello": {"name": "Fiorella Tonello", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "gminervini": {
            "name": "Giovanni Minervini",
            "group": "Tosatto",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "imicetic": {"name": "Ivan Mičetić", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "maspromonte": {
            "name": "Maria Cristina Aspromonte",
            "group": "Tosatto",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "mnecci": {"name": "Marco Necci", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "stosatto": {"name": "Silvio Tosatto", "group": "Tosatto", "proteins": [], "regions": 0, "residues": 0},
        "nveljkovic": {
            "name": "Nevena Veljković",
            "group": "Veljković",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "rdavidovic": {
            "name": "Radoslav Davidović",
            "group": "Veljković",
            "proteins": [],
            "regions": 0,
            "residues": 0
        },
        "jpujols": {"name": "Jordi Pujols", "group": "Ventura", "proteins": [], "regions": 0, "residues": 0},
        "sventura": {"name": "Salvador Ventura", "group": "Ventura", "proteins": [], "regions": 0, "residues": 0},
        "wvranken": {"name": "Wim Vranken", "group": "Vranken", "proteins": [], "regions": 0, "residues": 0},
        "ndavey": {"name": "Norman Davey", "group": "Davey", "proteins": [], "regions": 0, "residues": 0}
    });
