/**
 * Created by francesco on 05/06/16.
 */

angular.module('disprot.navbar', [
        'ui.bootstrap'
    ])

    .directive('disprotNavbar', function () {
        return {
            restrict: 'E',
            scope: true,
            controller: 'navbarCtrl',
            templateUrl: 'modules/navbar/navbar.html'
        }
    })

    .controller('navbarCtrl', ['$rootScope', '$scope', '$location', '$uibModal', function ($rootScope, $scope, $location, $uibModal) {
        $rootScope.$watch("globals", function (newVal) {
            if (newVal.hasOwnProperty('currentUser')) {
                $scope.username = $rootScope.globals.currentUser.username;
            } else {
                $scope.username = null;
            }
        });

        $scope.setClass = function (path) {
            return {active: $location.path() == path}
        };

        $scope.openModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/navbar/partials/modal.html',
                controller: 'ModalInstanceCtrl',
                size: 'sm'
            });
        };

        $scope.openSettings = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modules/auth/partials/user_settings.html',
                controller: 'usrOptCtrl',
                size: 'lg'
            });
        };
    }])

    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
        $scope.logout = function () {
            $scope.$emit('logout.event');
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);