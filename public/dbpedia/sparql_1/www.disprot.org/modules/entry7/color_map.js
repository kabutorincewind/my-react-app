/**
 * Created by ftabaro on 24/08/16.
 */
'use strict';

/*
 Edit the colors object to get fully updated feature viewer coloring.
 Add keys from the "ontologies" collection to allow coloring of new annotations.

 This script exports only cm.
 */

var colors = {
    evidences: {
        primary: '#00007F',
        secondary: '#7F0000',
        na: '#778899'
    },
    mfun: {
        MFUN_01: "#00007F",
        MFUN_02: "#002AFF",
        MFUN_03: "#00D4FF",
        MFUN_04: "#7FFF7F",
        MFUN_05: "#FFD400",
        MFUN_06: "#FF2A00",
        // MFUN_10: "#7F0000",
        na: '#778899'
    },
    tran: {
        "order_state": '#00007F', // order state
        "disorder_state": '#00D4FF', // disorder state
        "order_to_disorder_transition": '#FFD400', // order - disorder transition
        "disorder_to_order_transition": '#7F0000', // disorder - order transition
        "na": '#778899'
    },
    part: {
        PART_01: '#00007F',
        PART_02: '#002AFF',
        PART_03: '#00D4FF',
        PART_04: '#7FFF7F',
        PART_05: '#FFD400',
        PART_06: '#FF2A00',
        PART_07: '#7F0000',
        na: '#778899'
    }
};

angular.module('disprot.entry7.colormap', [])

    .constant('legend', colors)

    .constant('term2name', {
        primary: 'Primary detection method',
        secondary: 'Secondary detection method',
        MFUN_01: "Entropic chain",
        MFUN_02: "Molecular recognition – assembler",
        MFUN_03: "Molecular recognition - scavenger",
        MFUN_04: "Molecular recognition – effectors",
        MFUN_05: "Molecular recognition - display site",
        MFUN_06: "Molecular recognition - chaperone",
        // MFUN_10: "Unknown",
        "order_state": 'Order State', // order state
        "disorder_state": 'Disorder State', // disorder state
        "order_to_disorder_transition": 'Order to Disorder Transition', // order - disorder transition
        "disorder_to_order_transition": 'Disorder to Order Transition', // disorder - order transition
        PART_01: 'Protein-protein Binding',
        PART_02: 'Protein-DNA Binding',
        PART_03: "Protein-RNA Binding",
        PART_04: 'Protein-lipid Binding',
        PART_05: 'Protein-metal Binding',
        PART_06: 'Protein-inorganic Salt Binding',
        PART_07: 'Protein-small Molecule Binding',

        na: 'N/A'
    })

    .constant('color_schema', {
        mobidb: {
            D: '#FF0000',
            S: '#2B65EC',
            C: '#FFFFFF',
            s: '#ADD8E6',
            d: '#FFA500'
        },
        evidences: {
            NMR: colors['evidences']['primary'],
            XRAY: colors['evidences']['primary'],

            ESIFTICRMS: colors['evidences']['secondary'],
            IFLUO: colors['evidences']['secondary'],
            FLUOPA: colors['evidences']['secondary'],
            FDQ: colors['evidences']['secondary'],
            DMET: colors['evidences']['secondary'],
            AFM: colors['evidences']['secondary'],
            FRET: colors['evidences']['secondary'],
            FPROBE: colors['evidences']['secondary'],
            IMMUNO: colors['evidences']['secondary'],
            BF: colors['evidences']['secondary'],
            HDE: colors['evidences']['secondary'],
            FTIRS: colors['evidences']['secondary'],
            AU: colors['evidences']['secondary'],
            FCD: colors['evidences']['secondary'],
            MSHDE: colors['evidences']['secondary'],
            ORD: colors['evidences']['secondary'],
            NCD: colors['evidences']['secondary'],
            RAA: colors['evidences']['secondary'],
            GEL: colors['evidences']['secondary'],
            RAMAN: colors['evidences']['secondary'],
            SANS: colors['evidences']['secondary'],
            DLS: colors['evidences']['secondary'],
            THERMAL: colors['evidences']['secondary'],
            PH: colors['evidences']['secondary'],
            SAXS: colors['evidences']['secondary'],
            SLS: colors['evidences']['secondary'],
            SRCD: colors['evidences']['secondary'],
            VISCO: colors['evidences']['secondary'],
            DSC: colors['evidences']['secondary'],
            RSEM: colors['evidences']['secondary'],
            ROA: colors['evidences']['secondary'],
            TTQ: colors['evidences']['secondary'],
            PNMR: colors['evidences']['secondary'],
            SDSPAGE: colors['evidences']['secondary'],
            EPR: colors['evidences']['secondary'],
            SP: colors['evidences']['secondary'],
            DSF: colors['evidences']['secondary'],
            VS: colors['evidences']['secondary'],

            na: colors['evidences']['na']
        },
        mfun: {
            "MFUN_01": colors['mfun']['MFUN_01'],
            "MFUN_01.001": colors['mfun']['MFUN_01'],
            "MFUN_01.002": colors['mfun']['MFUN_01'],
            "MFUN_01.003": colors['mfun']['MFUN_01'],
            "MFUN_01.004": colors['mfun']['MFUN_01'],
            "MFUN_01.005": colors['mfun']['MFUN_01'],
            "MFUN_01.006": colors['mfun']['MFUN_01'],

            "MFUN_02": colors['mfun']['MFUN_02'],
            "MFUN_02.001": colors['mfun']['MFUN_02'],
            "MFUN_02.002": colors['mfun']['MFUN_02'],
            "MFUN_02.003": colors['mfun']['MFUN_02'],
            "MFUN_02.004": colors['mfun']['MFUN_02'],
            "MFUN_02.005": colors['mfun']['MFUN_02'],

            "MFUN_03": colors['mfun']['MFUN_03'],
            "MFUN_03.001": colors['mfun']['MFUN_03'],
            "MFUN_03.002": colors['mfun']['MFUN_03'],
            "MFUN_03.003": colors['mfun']['MFUN_03'],

            "MFUN_04": colors['mfun']['MFUN_04'],
            "MFUN_04.001": colors['mfun']['MFUN_04'],
            "MFUN_04.002": colors['mfun']['MFUN_04'],
            "MFUN_04.003": colors['mfun']['MFUN_04'],
            "MFUN_04.004": colors['mfun']['MFUN_04'],
            "MFUN_04.005": colors['mfun']['MFUN_04'],
            "MFUN_04.006": colors['mfun']['MFUN_04'],

            "MFUN_05": colors['mfun']['MFUN_05'],
            "MFUN_05.001": colors['mfun']['MFUN_05'],
            "MFUN_05.002": colors['mfun']['MFUN_05'],
            "MFUN_05.003": colors['mfun']['MFUN_05'],
            "MFUN_05.004": colors['mfun']['MFUN_05'],
            "MFUN_05.005": colors['mfun']['MFUN_05'],
            "MFUN_05.006": colors['mfun']['MFUN_05'],

            "MFUN_06": colors['mfun']['MFUN_06'],
            "MFUN_06.001": colors['mfun']['MFUN_06'],
            "MFUN_06.002": colors['mfun']['MFUN_06'],
            "MFUN_06.003": colors['mfun']['MFUN_06'],
            "MFUN_06.004": colors['mfun']['MFUN_06'],

            // "MFUN_10": colors['mfun']['MFUN_10'],
            // "MFUN_10.001": colors['mfun']['MFUN_10'],

            na: colors['mfun']['na']
        },
        tran: {
            TRAN_01: colors['tran']["order_state"], // o
            TRAN_02: colors['tran']["disorder_state"], // d
            TRAN_03: colors['tran']["disorder_state"], // d
            TRAN_04: colors['tran']["disorder_state"], // d
            TRAN_05: colors['tran']["disorder_to_order_transition"], // d-o
            TRAN_06: colors['tran']["disorder_to_order_transition"], // d-o
            TRAN_07: colors['tran']["disorder_to_order_transition"], // d-o
            TRAN_08: colors['tran']["order_to_disorder_transition"], // o-d
            TRAN_09: colors['tran']["order_to_disorder_transition"], // o-d
            TRAN_10: colors['tran']["order_to_disorder_transition"], // o-d
            TRAN_11: colors['tran']["order_to_disorder_transition"], // o-d
            TRAN_12: colors['tran']["disorder_to_order_transition"], // d-o
            TRAN_13: colors['tran']["order_to_disorder_transition"], // o-d
            TRAN_14: colors['tran']["order_to_disorder_transition"], // o-d
            TRAN_15: colors['tran']["disorder_to_order_transition"], // d-o
            TRAN_16: colors['tran']["disorder_to_order_transition"], // d-o

            na: colors['tran']['na']
        },
        part: {
            "PART_01": colors['part']['PART_01'],
            "PART_02": colors['part']['PART_02'],
            "PART_03": colors['part']['PART_03'],
            "PART_03.001": colors['part']['PART_03'],
            "PART_03.002": colors['part']['PART_03'],
            "PART_03.003": colors['part']['PART_03'],
            "PART_03.004": colors['part']['PART_03'],
            "PART_04": colors['part']['PART_04'],
            "PART_05": colors['part']['PART_05'],
            "PART_06": colors['part']['PART_06'],
            "PART_07": colors['part']['PART_07'],

            na: colors['part']['na']
        }
    });

