/**
 * Created by ftabaro on 12/08/16.
 *
 * This module uses functions declared in on disprot.entry (filters) and disprot.submit (directives).
 *
 */

angular.module('disprot.entry7', ['disprot.entry7.colormap', 'ui.bootstrap'])

    .constant('tags2name', {
        AMBLIT: 'Ambiguous literature evidence.',
        AMBEXP: 'Ambiguous experimental evidence.',
        AMBSEQ: 'Ambiguous experimental sequence construct.'
    })

    .value('static_legend', [
        {
            "DisProt Disorder": "#a7662b",
            "DisProt Context-dependent": "#6200D1"
        },
        {
            "PDB Disorder": "#FF0000",
            "PDB Structure": "#2B65EC",
            "PDB Ambiguous": "#FFFFFF"
        },
        {
            "Predicted Disorder": "#FFA500",
            "Predicted Structure": "#ADD8E6"
            // "N/A": "#778899"
        }
    ])

    .factory('entry7Service', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/get/:id', {future_release: false})
    }])

    .factory('setFVcolors', ['legend', 'color_schema', function (legend, color_schema) {
        return function (drawDetails, schema) {

            function updateColorsFunctions() {

                // if all region objects have exactly one functional annotation, go on and assign the color
                if (!(drawDetails[k][0].every(function (curr) {
                        return curr.funct[schema].length == 1
                    }))) {
                    // if there is more than one functional annotation for a region,
                    // duplicate the object and drop one function per resulting obj
                    // then assign the color

                    drawDetails[k][0].forEach(function (curr, curr_idx, inArr) {

                        if (curr.funct[schema].length > 1) {

                            for (var i = 0; i < curr.funct[schema].length - 1; i++) {
                                var new_item = angular.copy(curr);

                                // new_item.id = new_item.funct[schema][i];

                                new_item.funct[schema] = [curr.funct[schema][i + 1]];

                                inArr.push(new_item)
                            }

                            curr.funct[schema] = [curr.funct[schema][0]];
                        }

                    });

                }

                // assign colors (each element of curr.funct[schema] array must have exactly one element)
                drawDetails[k][0].forEach(function (curr) {
                    // curr.id = curr.funct[schema][0];
                    curr.description = curr.funct[schema][0];
                    curr.color = color_schema[schema][curr.funct[schema][0]];
                });
            }

            for (var k in drawDetails) {

                if (schema == 'evidences') {

                    drawDetails[k][0].forEach(function (curr) {
                        // curr.id = curr.method;
                        curr.description = curr.method;
                        curr.color = color_schema[schema][curr.method]
                    })

                } else {

                    updateColorsFunctions()

                }
            }

            return legend[schema]
        }
    }])

    .factory('computeLegendNodes', [function () {
        return function (terms, element_width) {
            // some margin + set maximum width
            var margin = 50;
            var w = element_width - margin;

            // set each element width and height in pixel
            var elm_width = 270;
            var elm_height = 20;

            var svg_height = 0;

            // elements per row
            var nblocks = 1;
            if (w > elm_width)
                nblocks = Math.floor(w / elm_width);

            var res = [];
            Object.keys(terms).forEach(function (curr, i) {
                // compute row and col index for each element
                var row = Math.floor(i / nblocks);
                var col = Math.floor(i % nblocks);

                // compute svg canvas height (at each iteration increase)
                svg_height = (row * elm_height) + 20;

                // save results
                res.push({
                    row: row * elm_height,
                    col: col * elm_width,
                    term: curr,
                    color: terms[curr]
                })
            });

            /*function compute_legend() {
             var res = [];

             var w = elm.width() - margin;
             var row = 0, row_step = 30;
             var col = 0;

             Object.keys(scope.legendTerms).forEach(function (curr, i) {

             var elm_width = term2name[curr].length * 8 + 30;

             if ((col + elm_width) > w) {
             row += row_step;
             col = 0;
             }

             // var row = Math.floor(i / nblocks);
             // var col = Math.floor(i % nblocks);

             res.push({
             row: row,
             col: col,
             term: curr,
             color: scope.legendTerms[curr]
             });

             col += elm_width;
             });
             scope.svg_height = row + 50;
             return res
             };*/

            return {
                legend: res,
                canvas_height: svg_height
            }
        }
    }])

    .directive('disprotLegend', ['$timeout', '$window', 'term2name', 'computeLegendNodes', function ($timeout, $window, term2name, computeLegendNodes) {
        return {
            restrict: 'E',
            scope: {
                legendTerms: '=',
                refresh: '='
            },
            templateUrl: 'modules/entry7/partials/legend.html',
            link: function (scope, elm, attrs) {
                scope.term2name = term2name;

                function setLegend() {

                    var legend_data = computeLegendNodes(scope.legendTerms, elm.width());

                    scope.svg_height = legend_data.canvas_height;

                    legend_data.legend.forEach(function (curr) {
                        curr.color = scope.legendTerms[curr.term];
                    });

                    scope.legend = legend_data.legend;
                }

                scope.$watch('refresh', function () {
                    setLegend();
                });

                angular.element($window).bind('resize', function () {

                    setLegend();
                    scope.$digest();
                });
            }
        }
    }])

    .filter('format_cross_ref', function () {
        return function (inArr) {
            var res = inArr.reduce(function (out, curr) {
                out += curr.db + " " + curr.id + ";";
                return out;
            }, "");
            return res.substring(0, res.length - 1);
        }
    })

    .filter('split', function () {
        return function (inStr, substr_length) {
            var substrings = [];
            for (var i = 0; i < inStr.length; i++) {
                if (i % substr_length === 0) {
                    substrings.push(inStr.slice(i, i + substr_length));
                }
            }
            return substrings
        }
    })

    .controller('entry7Ctrl', ['$scope', '$routeParams', '$filter', '$window', '$location', 'setFVcolors', 'generateDrawObjects', 'entry7Service', 'computeLegendNodes', 'str2col', 'blobDownload', 'viewport', 'curators', 'tags2name', 'static_legend',
        function ($scope, $routeParams, $filter, $window, $location, setFVcolors, generateDrawObjects, entry7Service, computeLegendNodes, str2col, blobDownload, viewport, curators, tags2name, static_legend) {

            $scope.loading = true;
            $scope.error = false;
            // $scope.curators = curators;
            $scope.tags2name = tags2name;
            $scope.static_legend = static_legend;

            var curr_color_schema = 'evidences', drawDetails_bkp, drawData_bkp;

            function restore_drawDetails() {
                if (drawDetails_bkp) {
                    $scope.drawDetails = angular.copy(drawDetails_bkp);
                } else {
                    drawDetails_bkp = angular.copy($scope.drawDetails);
                }
            }

            function filter_regions_array() {
                // filter out ambiguous regions
                $scope.regions = $scope.regions.filter(function (curr) {
                    return curr.tags[0] === null;
                });

            }

            $scope.showAllEvidences = function () {
                $scope.regions = $filter('filter')($scope.entry, {record_type: 'region_record'});
                $scope.regions = $scope.regions.sort(function (a, b) {
                    if (b.start > a.start) {
                        return false;
                    } else if (b.start < a.start) {
                        return true;
                    } else {
                        if ((b.end - b.start) > (a.end - a.start))
                            return true;
                        return false
                    }
                });
                if ($scope.ambiguities_flag) {
                    $scope.regions = $scope.regions.filter(function (curr) {
                        return curr.tags[0] === null
                    })
                }

            };

            $scope.filter_ambiguities_from_drawData = function () {
                $scope.drawData = angular.copy(drawData_bkp);

                if (angular.isUndefined($scope.ambiguities_flag)) {
                    $scope.ambiguities_flag = false;
                } else {
                    $scope.ambiguities_flag = !$scope.ambiguities_flag;
                }

                if ($scope.ambiguities_flag) {
                    var drawDetails_filter = {};

                    for (var key in $scope.drawDetails) {
                        if ($scope.drawDetails[key][0][0].tags[0] === null) {
                            drawDetails_filter[key] = $scope.drawDetails[key];
                        }
                    }
                    $scope.drawDetails = drawDetails_filter;
                    filter_regions_array();

                    // remove consensus with ambiguous data
                    // delete $scope.drawData['overall_consensus']
                } else {
                    restore_drawDetails();
                    setFVcolors($scope.drawDetails, curr_color_schema);
                    $scope.showAllEvidences();
                    // delete consensus without ambiguous data
                    // delete $scope.drawData['consensus_no_ambiguities']
                }
                $scope.refresh_consensus = $scope.refresh_fv = !$scope.refresh_fv;
            };

            entry7Service.query({id: $routeParams.entry_id}, function (result) {

                $scope.entry = result;
                $scope.regions = $filter('filter')($scope.entry, {record_type: 'region_record'});
                $scope.regions = $scope.regions.sort(function (a, b) {
                    if (b.start > a.start) {
                        return false;
                    } else if (b.start < a.start) {
                        return true;
                    } else {
                        if ((b.end - b.start) > (a.end - a.start))
                            return true;
                        return false
                    }
                });


                // get unique ambiguities from region_objects
                $scope.ambiguities = $scope.regions.reduce(function (out, curr) {
                    curr.tags.forEach(function (curr2) {
                        if (curr2 !== null) {
                            out.push(curr2);
                        }
                    });
                    return out
                }, []).filter(function (curr, idx, inArr) {
                    return inArr.indexOf(curr) === idx;
                });

                var draw_obj = $filter('filter')(result, {record_type: 'fv_record'});

                var drawData = $filter('getEntryFeature')(draw_obj, 'drawData');


                // assign fv color based on pfam or chain/peptide id
                for (var key in drawData) {
                    if (['chain', 'peptide', 'pfam'].indexOf(key) !== -1 &&
                        drawData[key][0] != null) {
                        drawData[key][0].forEach(function (curr) {
                            var id;
                            if (key == 'pfam') {
                                id = curr.id.split('_')[0];
                            } else {
                                id = curr.id;
                            }
                            curr.color = str2col(id);
                        });
                    }
                }
                drawData_bkp = angular.copy(drawData);
                $scope.drawData = drawData;

                // get drawDetails object, get a bkp and filter for ambiguous region
                $scope.drawDetails = $filter('getEntryFeature')(draw_obj, 'drawDetails');
                drawDetails_bkp = angular.copy($scope.drawDetails);
                $scope.filter_ambiguities_from_drawData();

                // set default colors and color schema
                $scope.legend = setFVcolors($scope.drawDetails, 'evidences');
                curr_color_schema = 'evidences';

                $scope.loading = false;
            }, function (error) {
                if (error.data) {
                    $scope.error_message = error.data;
                }
                $scope.loading = false;
                $scope.error = true;
            });

            $scope.showOverviewDetails = function (ont) {
                if (!$scope.show.hasOwnProperty(ont)) {
                    $scope.show[ont] = false;
                }
                $scope.show[ont] = !$scope.show[ont];
            };


            $scope.setColorSchema = function (schema) {

                // restore_drawDetails();

                // assign new colors and get new legend
                $scope.legend = setFVcolors($scope.drawDetails, schema);

                // refresh viewer and legend
                $scope.refresh_legend = $scope.refresh_fv = !$scope.refresh_fv;

                curr_color_schema = schema;
            };

            $scope.colorButtonsClass = function (schema) {
                return {active: schema == curr_color_schema};
            };


            $scope.dl_entry = function (filename) {

                var entry = angular.copy($scope.entry), dpid;

                var data = entry.reduce(function (out, curr) {
                    if (curr.record_type == 'protein_record') {

                        dpid = curr.disprot_id;

                        delete curr.record_type;
                        // delete curr.last_curator;
                        delete curr._id;
                        out.protein = curr;
                    } else if (curr.record_type == 'region_record') {

                        delete curr._id;
                        delete curr.record_type;
                        // delete curr.last_curator;
                        delete curr.paper;

                        if (!(out.hasOwnProperty('regions'))) {
                            out.regions = [];
                        }
                        out.regions.push(curr)
                    }
                    return out
                }, {});

                var filename = filename || dpid + ".json";

                blobDownload(data, filename);
            };

            $scope.check_pmid = function (pmid) {
                return /^\d+$/.test(pmid);
            };

            $scope.$on('featureSelected', function (event, clicked_region) {

                var regions = $filter('filter')($scope.entry, {record_type: 'region_record'});
                regions = regions.sort(function (a, b) {
                    if (b.start > a.start) {
                        return false;
                    } else if (b.start < a.start) {
                        return true;
                    } else {
                        if ((b.end - b.start) > (a.end - a.start))
                            return true;
                        return false
                    }
                });


                var tot = regions.length;
                if (/\d+-\d+-\d+-[A-Z]+/.test(clicked_region.id)) {

                    var region = clicked_region.id.toString();
                    var start = +region.split("-")[0];
                    var end = +region.split("-")[1];
                    var pmid = region.split("-")[2];
                    var method = region.split("-")[3];

                    $scope.regions = regions.filter(function (curr) {
                        return curr.start == start && curr.end == end && curr.method.id == method && curr.pmid == pmid;
                    });

                } else {

                    // filter for regions in range of clicked one
                    $scope.regions = regions.filter(function (curr) {
                        console.log(curr.start, curr.end);
                        return (curr.start >= clicked_region.start && curr.start <= clicked_region.end) ||
                            (curr.end <= clicked_region.end && curr.end >= clicked_region.start) ||
                            (curr.start <= clicked_region.start && curr.end >= clicked_region.end)
                    });

                }

                if ($scope.ambiguities_flag) {
                    $scope.regions = $scope.regions.filter(function (curr) {
                        return curr.tags[0] === null;
                    });

                }


                $scope.$emit('show.alert.success', {message: "Successfully filtered region evidences. Showing " + $scope.regions.length + "/" + tot + " region evidences."});
                $scope.$apply();

            });

            function init_sv() {
                if (viewport('lg')) {
                    $scope.split_par = 80;
                    $scope.sv_class_small = {"col-lg-1": true};
                    $scope.sv_class_large = {"col-lg-11": true};
                }
                if (viewport('md')) {
                    $scope.split_par = 60;
                    $scope.sv_class_small = {"col-md-1": true};
                    $scope.sv_class_large = {"col-md-11": true};
                }
                if (viewport('sm')) {
                    $scope.split_par = 40;
                    $scope.sv_class_small = {"col-sm-1": true};
                    $scope.sv_class_large = {"col-sm-11": true};
                }
                if (viewport('xs')) {
                    $scope.split_par = 30;
                    $scope.sv_class_small = {"hidden-xs": true};
                    $scope.sv_class_large = {"col-xs-12": true};
                }
            }

            init_sv();
            angular.element($window).bind('resize', function () {
                init_sv();
                $scope.$apply();
            });

            $scope.edit = function () {
                $location.path('/edit').search({entry_id: $routeParams.entry_id})
            }
        }])
;
