/**
 * Created by ftabaro on 28/06/16.
 */

angular.module('disprot.help', [])

    .controller('helpCtrl', ['$scope', '$location', '$anchorScroll', function ($scope, $location, $anchorScroll) {

        $scope.show = {
            blast_format: false,
            browse_example: false,
            dbStat_example: false,
            api: false,
            data: false,
            ui: false
        };

        $anchorScroll.yOffset = 170;
        $scope.goTo = function (hash, event) {
            $location.hash(hash);
            $anchorScroll()
        };

        $scope.setClass = function (anchor_name) {
            var re = new RegExp("^" + anchor_name + "$", "g");
            return {
                active: re.test($location.hash())
            }
        };

    }]);