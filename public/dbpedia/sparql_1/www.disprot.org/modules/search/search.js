/**
 * Created by ftabaro on 15/06/16.
 */

angular.module('disprot.search', [])

// SERVICES

    .factory('textSearchService', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/text_search', {}, {post: {method: 'POST', isArray: true}})
    }])

    .factory('blastSearch', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/blastSearch', {})
    }])

    .factory('blastCheck', ['WSROOT', '$resource', function (WSROOT, $resource) {
        return $resource(WSROOT + '/blastResult', {})
    }])

    // DIRECTIVES

    .directive('disprotSmallSearch', function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: 'modules/search/partials/searchSmall.html',
            link: function (scope, elm, attrs) {
                if (attrs.placeholder) {
                    scope.placeholder = attrs.placeholder;
                } else {
                    scope.placeholder = "";
                }
            }
        }
    })
    .directive('disprotBlastSearch', function () {
        return {
            restrict: 'E',
            scope: true,
            templateUrl: 'modules/search/partials/blastSearch.html',
            link: function (scope, elm, attrs) {

            }
        }
    })

    // CONTROLLER

    .controller('searchCtrl', ['$scope', '$location', '$window', '$filter', '$interval', 'textSearchService', 'blastSearch', 'blastCheck', '$routeParams',
        function ($scope, $location, $window, $filter, $interval, textSearchService, blastSearch, blastCheck, $routeParams) {

            $scope.showResults = {
                simple: false,
                blast: false
            };
            $scope.loading = false;
            if ($routeParams.showResults && $routeParams.loading && $routeParams.term) {
                $scope.showResults.simple = $routeParams.showResults === "true";
                $scope.loading = $routeParams.loading === "true";
                submitSearch($routeParams.term);
            }

            $scope.search = {};

            $scope.searchFromHome = function (term) {
                if (term) {
                    $scope.search.term = null;
                    $location.path('/search').search({
                        'showResults': 'false',
                        'loading': 'true',
                        'term': term
                    });
                }
            };

            $scope.submitSearch = submitSearch;

            function submitSearch(term) {

                term = term || $scope.search.term;

                if (term) {
                    $scope.$emit('loading', {
                        loading: true,
                        showSimple: false
                    });

                    textSearchService.post({term: term}, function (res) {
                        $scope.$emit('showResults', {
                            data: res,
                            showResults: true,
                            loading: false
                        });
                    });

                } else {
                    $window.alert('Please specify a search term.')
                }
            }


            $scope.$on('loading', function (event, data) {
                $scope.loading = data.loading;
                $scope.showResults.simple = data.showSimple;
            });

            $scope.$on('showResults', showResults);

            function showResults(event, data) {

                $scope.results = data.data.length > 0 ? data.data : 'No results found.';

                $scope.noRes = $filter('isString')($scope.results);
                // if (! $scope.noRes) {
                //     $scope.setFilter('protein');
                // }


                $scope.loading = data.loading;
                $scope.showResults.simple = data.showResults;

            }

            $scope.reset = function () {
                $scope.loading = false;
                $scope.showResults.simple = false;
                $scope.showResults.blast = false;
                $scope.results = null;
                $scope.search.term = null;
                $scope.blast.seq = null;
            };

            $scope.blast = {seq: ''};

            $scope.example1 = function () {
                $scope.blast.seq = ">DP00086\n" +
                    "MEEPQSDPSVEPPLSQETFS\n" +
                    "DLWKLLPENNVLSPLPSQAM\n" +
                    "DDLMLSPDDIEQWFTEDPGP\n" +
                    "DEAPRMPEAAPPVAPAPAAP\n" +
                    "TPAAPAPAPSWPLSSS"
            };

            $scope.submitBlast = function () {

                var query_split = $scope.blast.seq.split('\n');
                var header = query_split[0];
                var sequence = query_split.slice(1).join("");

                if (header.substring(0, 1) !== '>') {
                    $scope.error = 'Malformed FASTA: missing header.';
                    return

                }
                if (header.substring(1) === "") {
                    $scope.error = 'Malformed FASTA: missing sequence id.';
                    return

                }

                $scope.loading = true;
                blastSearch.save({
                    query: sequence
                }, function (result) {
                    $scope.$emit('ping_blast', result);
                })

            };

            $scope.$on('ping_blast', function (ev, data) {
                var stopInterval = function () {
                    if (angular.isDefined(stop)) {
                        $interval.cancel(stop);
                        stop = undefined;
                    }
                    $scope.loading = false;
                    $scope.showResults.blast = true;
                };

                var stop = $interval(function () {
                    $scope.loading = true;
                    $scope.showResults.blast = false;

                    var hash = data.hash;

                    var blast_process = JSON.parse($window.atob(hash));

                    blastCheck.get({
                        blst_query: blast_process.query_filename,
                        blst_res: blast_process.result_filename
                    }, function (result) {
                        if (result.hasOwnProperty('blast')) {
                            $scope.res = result.blast;
                            stopInterval()
                        }
                    });
                }, 2500, 30);

            });


            // $scope.setFilter = function (inStr) {
            //     $scope.record_type = {
            //         record_type : inStr
            //     }
            // }
        }]);
