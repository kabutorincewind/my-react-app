AC   PRU00297;
DC   Domain;
TR   PROSITE; PS50873; PEROXIDASE_4; 1; level=0
XX
Names: Plant heme peroxidase
Function: Removal of H(2)O(2), oxidation of toxic reductants
XX
case <FTGroup:1> and <FTGroup:2> and <FTGroup:3> and <FTTag:viacarbo>
CC   -!- FUNCTION: Removal of H(2)O(2), oxidation of toxic reductants,
CC       biosynthesis and degradation of lignin, suberization, auxin
CC       catabolism, response to environmental stresses such as wounding,
CC       pathogen attack and oxidative stress. These functions might be
CC       dependent on each isozyme/isoform in each plant tissue.
CC   -!- CATALYTIC ACTIVITY: Donor + H(2)O(2) = oxidized donor + 2 H(2)O.
CC   -!- COFACTOR:
CC       Name=heme b; Xref=ChEBI:CHEBI:60344;
CC       Note=Binds 1 heme b (iron(II)-protoporphyrin IX) group per
CC       subunit.;
CC   -!- COFACTOR:
CC       Name=Ca(2+); Xref=ChEBI:CHEBI:29108;
CC       Note=Binds 2 calcium ions per subunit.;
CC   -!- SUBCELLULAR LOCATION: Secreted.
CC   -!- SIMILARITY: Belongs to the peroxidase family. Classical plant
CC       (class III) peroxidase subfamily.
end case
XX
case <Feature:PS50873:168=H>
DR   PROSITE; PS00435; PEROXIDASE_1; 1; trigger=no
end case
case <Feature:PS50873:42=H>
DR   PROSITE; PS00436; PEROXIDASE_2; 1; trigger=no
end case
XX
case <FTGroup:1> and <FTGroup:2> and <FTGroup:3> and <FTTag:viacarbo>
GO   GO:0016491; F:oxidoreductase activity
GO   GO:0004601; F:peroxidase activity
GO   GO:0009055; F:electron carrier activity
GO   GO:0055114; P:oxidation-reduction process
GO   GO:0006979; P:response to oxidative stress
GO   GO:0042744; P:hydrogen peroxide catabolic process
KW   Secreted
KW   Oxidoreductase
KW   Peroxidase
KW   Hydrogen peroxide
end case
case <Feature:PS50873:168=H>
GO   GO:0020037; F:heme binding
GO   GO:0005506; F:iron ion binding
KW   Iron
KW   Heme
end case
case <Feature:PS50873:1=Q>
KW   Pyrrolidone carboxylic acid
end case
case <FTGroup:1> or <FTGroup:2>
GO   GO:0005509; F:calcium ion binding
KW   Calcium
end case
case <Feature:PS50873:168=H> or <FTGroup:1> or <FTGroup:2>
KW   Metal-binding
end case
case <FTTag:disulf>
KW   Disulfide bond
end case
FT   From: PS50873
FT   METAL        43     43       Calcium #1.
FT   Group: 1; Condition: [DE]
FT   METAL        46     46       Calcium #1; via carbonyl oxygen.
FT   Group: 1; Condition: [VI]
FT   METAL        48     48       Calcium #1; via carbonyl oxygen.
FT   Group: 1; Condition: G
FT   METAL        50     50       Calcium #1.
FT   Group: 1; Condition: [DE]
FT   METAL        52     52       Calcium #1.
FT   Group: 1; Condition: [ST]
FT   METAL       169    169       Calcium #2.
FT   Group: 2; Condition: [TS]
FT   METAL       222    222       Calcium #2.
FT   Group: 2; Condition: [DE]
FT   METAL       225    225       Calcium #2.
FT   Group: 2; Condition: [TS]
FT   METAL       230    230       Calcium #2.
FT   Group: 2; Condition: [DE]
FT   SITE         38     38       Transition state stabilizer.
FT   Group: 3; Condition: R
FT   ACT_SITE     42     42       Proton acceptor.
FT   Group: 3; Condition: H
FT   METAL       168    168       Iron (heme axial ligand).
FT   Group: 3; Condition: H
FT   DISULFID     11     90
FT   Tag: disulf; Condition: C-x*-C
FT   DISULFID     44     49
FT   Tag: disulf, viacarbo; Condition: C-x*-C
FT   DISULFID     96    298
FT   Tag: disulf; Condition: C-x*-C
FT   DISULFID    175    207
FT   Tag: disulf; Condition: C-x*-C
FT   MOD_RES       1      1       Pyrrolidone carboxylic acid.
FT   Condition: Q
case <FTTag:viacarbo>
FT   BINDING     138    138       Substrate; via carbonyl oxygen.
FT   Condition: P
end case
XX
Chop: Nter=0; Cter=0;
Size: 250-384;
Related: None;
Repeats: 1;
Topology: Undefined;
Example: P0DI10; Q67Z07;
Scope:
 Eukaryota
Comments: None
XX
# Revision 1.30 2017/06/06
//
