// does not work if put after it is used in document
// used each time ':regex' appears
jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
    validLabels = /^(data|css):/,
    attr = {
        method: matchParams[0].match(validLabels) ?
        matchParams[0].split(':')[0] : 'attr',
        property: matchParams.shift().replace(validLabels,'')
    },
    regexFlags = 'ig',
    regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}

$(document).ready(function() {

    $(':regex(id, _more$)').hide();
    $(':regex(id, _plus$)').hide();

    // '>> more' <=> 'less <<'
    // e.g. #matrix_more_opt
    $(':regex(id, _more_opt$)').click(function(){
        // e.g. #matrix_more
        var more = '#' + $(this).attr('id').replace('_opt','');
        var more_string = more + '_string';
        $(more).toggle();
        $(more_string).toggle();
        if ($(this).html().match(/more$/)) {
            $(this).html('less &laquo;');
        }
        else {
            $(this).html('&raquo; more');
            replace(validLabels,'')
        }
    });

    $(':regex(id, _plus_opt$)').click(function(){
        // e.g. #matrix_more
        var more = '#' + $(this).attr('id').replace('_opt','');
        $(more).toggle();
        if ($(this).html().match(/-/)) {
            // replace - by +
            $(this).text($(this).text().replace('-', '+'));
        }
        // -
        else {
            // replace + by -
            $(this).text($(this).text().replace('+', '-'));
        }
    });

});