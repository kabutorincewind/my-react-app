function window_open(url) {
	window.open(url, '_blank', 'channelmode=no,directories=no,fullscreen=no,height=500,left=0,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,top=0,width=1500'); // javascript function to open a new window
}

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    }
    else {
        $('#sticky').removeClass('stick');
    }
}


window.onload=hideAllMenus;
function showMenu(menuNumber) {
    hideAllMenus();
    if (document.getElementById('menu'+menuNumber)) {
        document.getElementById('menu'+menuNumber).style.display='block';
    }
}

function hideAllMenus() {
    for (var i = 1; i<=10; i++) {
        if (document.getElementById('menu'+i)) {document.getElementById('menu'+i).style.display='none';}
    }
}

// DOMContentLoaded means when the DOM Objects of the document are
// fully loaded and seen by JavaScript
document.addEventListener("DOMContentLoaded", function() {
});


function replace_empty (id, input_text){
    $('#' + id).html(input_text);

}

function cursor_wait() {
    document.body.style.cursor = 'wait';
}

function cursor_auto() {
    document.body.style.cursor = 'auto';
}
