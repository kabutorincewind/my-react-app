// does not work if put after it is used in document
// used each time ':regex' appears
jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
    validLabels = /^(data|css):/,
    attr = {
        method: matchParams[0].match(validLabels) ?
        matchParams[0].split(':')[0] : 'attr',
        property: matchParams.shift().replace(validLabels,'')
    },
    regexFlags = 'ig',
    regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}
function validateEmail(sEmail) {
    var filter_email = /^([\w-\.]+)@((\[[0-9]{1,5}\.[0-9]{1,5}\.[0-9]{1,5}\.)|(([\w-]+\.)+))([a-zA-Z]{2,5}|[0-9]{1,5})(\]?)$/;
    if (filter_email.test(sEmail)) {
        return true;
    }
    return false;
}
$(document).ready(function() {

    // #meta1, #meta2 and #meta3 address alternative STEP1 and STEP2
    //
    // #meta1: 'Enter PROTEIN sequences'
    // #meta2: 'Enter MOTIFS'
    // #meta3: 'Enter PROTEIN sequences and MOTIFS'

////////////////////////////////////////////////////////////////////

    // When one of the '#meta' is shown, the others are hidden
    // By default, #meta1 is the one shown

    // not_chosen, disable and hide all meta (all STEP1 and 2)
    $(':regex(id, ^meta_opt\\d+$)').addClass('not_chosen');
    $(':regex(id, ^meta\\d+$) :input').attr('disabled', true);
    // or
    //$(':regex(id, ^meta\\d+$) :input').addAttr('disabled', true);
    $(':regex(id, ^meta\\d+$)').hide();


    // chosen, enable, show #meta1 (STEP 1 and 2 of meta1)
    $('#meta_opt1').removeClass('not_chosen').addClass('chosen');
    $('input:radio[name=meta][value=opt1]').prop('checked', true);
    // swithClass does not work, I don't know why
    // $('#meta_opt1').switchClass('not_chosen', 'chosen', 'fast');
    $('#meta1 :input').attr('disabled', false);
    // or
    // $('#meta1 :input').removeAttr('disabled');

    $('input:radio[name=meta1_protein][value=opt1]').prop('checked', true);

    $('#meta1').show();


    if ($('textarea[name=sig]').val() == '') {
    }
    else {
        $(function(){
            $('#meta_opt2').click();
        });
    }

////////////////////////////////////////////////////////////////////
    // STEP 1 for meta1

    // not_chosen2, disable, hide all meta1_protein
    $(':regex(id, ^meta1_protein_opt\\d+$)').addClass('not_chosen2');
    $(':regex(id, ^meta1_protein\\d+$) :input').attr('disabled', true);
    $(':regex(id, ^meta1_protein\\d+$)').hide();

    // chosen2, enable, show meta1_protein1
    $('#meta1_protein_opt1').removeClass('not_chosen2').addClass('chosen2');
    $('#meta1_protein1 :input').attr('disabled', false);
    $('#meta1_protein1').show();

////////////////////////////////////////////////////////////////////
    // STEP 3, same for all meta

    // only enabled and shown for:
    // meta1 if meta1_protein2 (user db)
    // meta2
    // meta3 if meta3_protein2 (user db)

    // by default meta1_protein1 is shown, so disable and hide
    $('#max_matches :input').attr('disabled', true);
    $('#max_matches').hide();

    // #email enabled and shown for all meta but disabled and hidden
    // by default
    $('input:checkbox[name=email_opt]').prop('checked', false);
    $('#email :input').attr('disabled', true);
    $('#email').hide();

////////////////////////////////////////////////////////////////////

    $(':regex(id, ^meta_opt\\d+$)').mouseover(function(){
        $(this).css('background-color','#DFEFFF');
        $(this).css('color','#000000');
    });
    $(':regex(id, ^meta_opt\\d+$)').mouseout(function(){
        $(this).css('background-color','');
        $(this).css('color','');
    });

    $(':regex(id, ^meta[123]_protein_opt\\d+$)').mouseover(function(){
        $(this).css('color','#000000');
    });
    $(':regex(id, ^meta[123]_protein_opt\\d+$)').mouseout(function(){
        $(this).css('color','');
    });
////////////////////////////////////////////////////////////////////

    // removes space at beginning and at the end and replaces content

    // for instance if user enters ' PS50240  ' in a textarea
    // trims to 'PS50240' and replaces in the textarea in question
    // textarea
    $('form[name=scan] textarea').change(function(event){
        $(this).val($.trim($(this).val()));
    });

    // applies to all input type text
    $('form[name=scan] input:text').change(function(event){
        $(this).val($.trim($(this).val()));
    });

////////////////////////////////////////////////////////////////////
    // When clicking 'START THE SCAN'
    $('form[name=scan]').submit(function(event){
        ////////////////////////////////////////////////////////////
        // START THE SCAN
        ////////////////////////////////////////////////////////////

        // concerns meta2
        // meta2 STEP2, if user has chosen UniProtKB, checks
        // that Swiss-Prot or TrEMBL is checked
        if ($('input:radio[name=meta]:checked').val() == 'opt2') {
            if ($('input:radio[name=meta2_protein]:checked').val() == 'opt1') {
                if ($('input:checkbox[name=db][value=sp]').is(':checked') || $('input:checkbox[value=tr]').is(':checked')) {
                }
                else {
                    event.preventDefault();
                    alert('Please select a protein database.');
                    return false;
                }
            }
        }

        // if the email address is required, checks that the address
        // is valid
        if ($('input:text[name=email]').attr('required') == 'required') {
            var sEmail = $('input:text[name=email]').val();
            if (validateEmail(sEmail)) {
            }
            else {
                event.preventDefault();
                alert('Please enter a valid email address.');
                return false;
            }
        }


        // verify if input value are positive integers fo
        // minhits, max_x, minsize and maxsize else
        // assigns value = 1 for minhits, 0 for max_x, '' for
        // minsize and maxsize (default values)
        var sMinhits = $('input:text[name=minhits]').val();
        var sMax_x = $('input:text[name=max_x]').val();
        var sMinsize = $('input:text[name=minsize]').val();
        var sMaxsize = $('input:text[name=maxsize]').val();

        var filter_number = /^\d+$/;

        if (filter_number.test(sMinhits) && sMinhits != '0') {
        }
        else {
            $('input:text[name=minhits]').val('1');
        }

        if (filter_number.test(sMax_x)) {
        }
        else {
            $('input:text[name=max_x]').val('0');
        }

        if (filter_number.test(sMinsize)) {
        }
        else {
            $('input:text[name=minsize]').val('');
        }

        if (filter_number.test(sMaxsize) && sMaxsize != '0' ) {
        }
        else {
            $('input:text[name=maxsize]').val('');
        }

        if (filter_number.test(sMinsize) && filter_number.test(sMaxsize) && parseInt(sMinsize, 10) > parseInt(sMaxsize, 10)) {
            event.preventDefault();
            var alert_length = 'Filters: minimal length (';
            alert_length = alert_length + sMinsize;
            alert_length = alert_length + ') cannot be greater than';
            alert_length = alert_length + ' maximal length (';
            alert_length = alert_length + sMaxsize;
            alert_length = alert_length + ').';
            alert(alert_length);
            return false;
        }
    });
////////////////////////////////////////////////////////////////////////////////

    // When clicking id meta_opt1, meta_opt2 or meta_opt3
    $(':regex(id, ^meta_opt\\d+$)').click(function(){
        // e.g. meta1
        var meta = $(this).attr('id').replace('_opt','');
        // e.g. 1
        var meta_nb = meta.replace('meta', '');

        $('input:radio[name=meta][value=opt' + meta_nb + ']').prop('checked', true);

        // not_chosen, disable and hide all meta (all STEP1 and 2)
        $(':regex(id, ^meta_opt\\d+$)').removeClass('chosen').addClass('not_chosen');
        $(':regex(id, ^meta\\d+$) :input').attr('disabled', true);
        $(':regex(id, ^meta\\d+$)').hide();

        // chosen, enable, show chosen meta (STEP 1 and 2 )
        // e.g user choses 'Enter Motifs':
        // -#meta_opt2: from not_chosen to chosen
        // -#meta2 :input: enable
        // -#meta2: show
        $('#' + $(this).attr('id')).removeClass('not_chosen').addClass('chosen');
        $('#' + $(this).attr('id').replace('_opt','') + ' :input').attr('disabled', false);
        $('#' + $(this).attr('id').replace('_opt','')).show();

        ////////////////////////////////////////////////////////////

        // concerns all meta:
        // meta1 STEP2
        // meta2 STEP1
        // meta3 STEP2
        // scan at level -1 for profile is unchecked
        $('input:checkbox[name=lowscore]').prop('checked', false);

        // concerns all meta STEP3
        // output format: by default, all the output format options
        // are available and the selected option is nice

        $('select[name=output] option[value=nice]').remove();
        $('select[name=output] option[value=html]').remove();
        $('select[name=output] option[value=plain]').remove();
        $('select[name=output] option[value=fasta]').remove();
        $('select[name=output] option[value=tabular]').remove();
        $('select[name=output] option[value=list]').remove();

        $(new Option('Graphical view', 'nice')).appendTo('select[name=output]');
        $(new Option('Simple view', 'html')).appendTo('select[name=output]');
        $(new Option('Text', 'plain')).appendTo('select[name=output]');
        $(new Option('FASTA', 'fasta')).appendTo('select[name=output]');
        $(new Option('Table', 'tabular')).appendTo('select[name=output]');
        $(new Option('Matchlist', 'list')).appendTo('select[name=output]');

        $('select[name=output]').val('nice');

        // concerns all meta STEP3
        // only enabled and shown for:
        // meta1 if meta1_protein2 (user db)
        // meta2
        // meta3 if meta3_protein2 (user db)
        $('#max_matches :input').attr('disabled', true);
        $('#max_matches').hide();
        $('input:checkbox[name=compseq]').prop('checked', false);
        $('#comp_seq_message').text("If you choose this option, not all output formats are available.");

        // concerns all meta STEP3
        $('input:checkbox[name=email_opt]').prop('checked', false);
        $('input:checkbox[name=email_opt]').attr('disabled', false);
        $('input:text[name=email]').attr('required', false);
        $('#email :input').val('');
        $('#email :input').attr('disabled', true);
        $('#email').hide();

        // Concerns all meta: meta1 STEP1, meta2 STEP2 and meta3 STEP1
        // not_chosen2, disable and hide all meta(nb)_protein(nb)
        $(':regex(id, ^meta[123]_protein_opt\\d+$)').removeClass('chosen2').addClass('not_chosen2');
        $(':regex(id, ^meta[123]_protein\\d+$) :input').attr('disabled', true);
        $(':regex(id, ^meta[123]_protein\\d+$)').hide();
        // chosen2, enable and show meta1_protein1 if meta_opt1
        // chosen2, enable and show meta2_protein1 if meta_opt2
        // chosen2, enable and show meta3_protein1 if meta_opt3
        $(':regex(id, ^meta[123]_protein_opt1$)').removeClass('not_chosen2').addClass('chosen2');
        $('#' + $(this).attr('id').replace('_opt','') + '_protein1 :input').attr('disabled', false);
        $('#' + $(this).attr('id').replace('_opt','') + '_protein1' ).show();

        $(':regex(name, ^meta[123]_protein$)[value=opt1]').prop('checked', true);

        ////////////////////////////////////////////////////////////

        // Concerns meta1 STEP1 and meta3 STEP1
        $('#' + meta + '_ex_seq').show();

        ////////////////////////////////////////////////////////////

        // Concerns meta2 STEP1 and meta3 STEP2
        // '>> More' <=> 'Less <<'
        $(':regex(id, ^meta[23]_more_opt$)').html('&raquo; More');
        $(':regex(id, ^meta[23]_more$)').hide();

        // Concerns meta2 STEP1 and meta3 STEP2
        // '>> Options' <=> 'Options <<'
        $(':regex(id, ^meta[23]_add_opt$)').html('&raquo; Options');
        $(':regex(id, ^meta[23]_add$) :input').attr('disabled', true);
        $(':regex(id, ^meta[23]_add$)').hide();

        // Concerns meta2 STEP1 and meta3 STEP2
        $('input[name=max_x]').val('0');
        $('select[name=matchmode]').val('');

        ////////////////////////////////////////////////////////////
        // Concerns meta1 STEP2

        // exclude patterns with a high probability of occurence
        $('input:checkbox[name=skip]').prop('checked', true);
        // exclude profile
        $('input:checkbox[name=noprofile]').prop('checked', false);


        ////////////////////////////////////////////////////////////
        // Concerns meta2 STEP2

        // meta2_protein1 (UniProtKB)
        $('input:checkbox[value=sp]').prop('checked', true);
        $('input:checkbox[value=tr]').prop('checked', false);
        $('input:checkbox[name=varsplic]').prop('checked', true);

        // Exclude fragments
        $('input:checkbox[name=nofrag]').prop('checked', false);

        // STEP 2 >> Filters <-> Filters <<
        $('#filters_opt').html('&raquo; Filters');
        $(':regex(id, ^filters\\d+$) :input').val('');
        $(':regex(id, ^filters\\d+$) :input').attr('disabled', true);
        $(':regex(id, ^filters\\d+$)').hide();


        $('#random_link').hide();

        if ($(this).attr('id') == 'meta_opt2') {
            // STEP 3
            // max nb of matches to be displayed
            $('#comp_seq_message').text("If you choose this option, a maximum of 1'000 matched sequences can be displayed and not all output formats are available.");
            $('select[name=maxhits]').val('10000');
            $('#max_matches :input').attr('disabled', false);
            $('#max_matches').show();
        }

    });

    // #meta1_protein_opt1 or #meta1_protein_opt2
    // #meta2_protein_opt1, #meta2_protein_opt2, ...
    // #meta3_protein_opt1 or #meta3_protein_opt2
    $(':regex(id, ^meta[123]_protein_opt\\d+$)').click(function(){
        var meta = $(this).attr('id').replace(/_protein_opt\d+$/,'');
        var option = $(this).attr('id').replace(/^meta[123]_protein_/,'');

        $('input:radio[name=' + meta + '_protein][value=' + option + ']').prop('checked', true);

        $(':regex(id, ^meta[123]_protein_opt\\d+$)').removeClass('chosen2').addClass('not_chosen2');
        $(':regex(id, ^meta[123]_protein\\d+$) :input').attr('disabled', true);
        $(':regex(id, ^meta[123]_protein\\d+$)').hide();

        $('#' + $(this).attr('id')).removeClass('not_chosen2').addClass('chosen2');
        $('#' + $(this).attr('id').replace('_opt','') + ' :input').attr('disabled', false);
        $('#' + $(this).attr('id').replace('_opt','')).show();


        // STEP 3 (Submit your job) max nb of matches
        // to be displayed
        $('select[name=maxhits]').val('10000');


        if ( (meta == 'meta1' || meta == 'meta3') && option == 'opt1' ) {
            $( '#' + meta + '_ex_seq').show();
            // STEP 3 (Submit your job) max nb of matches
            // to be displayed
            $('#max_matches :input').attr('disabled', true);
            $('#max_matches').hide();
            // STEP 3 (Submit your job) Retrieve complete sequences
            $('#comp_seq_message').text("If you choose this option, not all output formats are available.");
        }
        else {
            $( '#' + meta + '_ex_seq').hide();
            // STEP 3 (Submit your job) max nb of matches
            // to be displayed
            $('#max_matches :input').attr('disabled', false);
            $('#max_matches').show();
            // STEP 3 (Submit your job) Retrieve complete sequences
            $('#comp_seq_message').text("If you choose this option, a maximum of 1'000 matched sequences can be displayed and not all output formats are available.");
            if ($('input:checkbox[name=compseq]').is(':checked')) {
                $('select[name=maxhits]').val('1000');
            }
        }
        if (meta == 'meta2') {
            $('input:checkbox[name=nofrag]').prop('checked', false);
            $('input:checkbox[name=nofrag]').attr('disabled', true);
            // $(':regex(id, ^filters\\d+$) :input').val('');
            $(':regex(id, ^filters\\d+$) :input').attr('disabled', true);
            $('#random_link').hide();
            if (option == 'opt1') {
                $('input:checkbox[value=sp]').prop('checked', true);
                $('input:checkbox[name=varsplic]').prop('checked', true);
                $('input:checkbox[value=tr]').prop('checked', false);
                $('input:checkbox[name=nofrag]').attr('disabled', false);
                if ($('#filters_opt').html().match(/^Filters/)) {
                    $(':regex(id, ^filters\\d+$) :input').attr('disabled', false);
                }
            }
            else if (option == 'opt2') {
                if ($('#filters_opt').html().match(/^Filters/)) {
                    $('#filters1 :input').attr('disabled', false);
                }
            }
            else if (option == 'opt4') {
                $('input:radio[name=randomizedb][value=reversed]').prop('checked', true);
                $('#random_link').show();
            }
        }
    });

   // Example of PROTEIN sequences (#meta1 and #meta3)
   // fills in textarea[name=seq] with examples
   $(':regex(id, ^meta[13]_ex_seq$)').click(function(){
        var ac_ex1 = 'P98073';
        var id_ex1 = 'ENTK_HUMAN';
        var pdb_ex1 = '4DGJ';
        var fasta_ex1 = '>sp|P98073|ENTK_HUMAN Enteropeptidase OS=Homo sapiens GN=TMPRSS15 PE=1 SV=3\n';
        fasta_ex1 = fasta_ex1 + 'MGSKRGISSRHHSLSSYEIMFAALFAILVVLCAGLIAVSCLTIKESQRGAALGQSHEARA\n';
        fasta_ex1 = fasta_ex1 + 'TFKITSGVTYNPNLQDKLSVDFKVLAFDLQQMIDEIFLSSNLKNEYKNSRVLQFENGSII\n';
        fasta_ex1 = fasta_ex1 + 'VVFDLFFAQWVSDENVKEELIQGLEANKSSQLVTFHIDLNSVDILDKLTTTSHLATPGNV\n';
        fasta_ex1 = fasta_ex1 + 'SIECLPGSSPCTDALTCIKADLFCDGEVNCPDGSDEDNKMCATVCDGRFLLTGSSGSFQA\n';
        fasta_ex1 = fasta_ex1 + 'THYPKPSETSVVCQWIIRVNQGLSIKLSFDDFNTYYTDILDIYEGVGSSKILRASIWETN\n';
        fasta_ex1 = fasta_ex1 + 'PGTIRIFSNQVTATFLIESDESDYVGFNATYTAFNSSELNNYEKINCNFEDGFCFWVQDL\n';
        fasta_ex1 = fasta_ex1 + 'NDDNEWERIQGSTFSPFTGPNFDHTFGNASGFYISTPTGPGGRQERVGLLSLPLDPTLEP\n';
        fasta_ex1 = fasta_ex1 + 'ACLSFWYHMYGENVHKLSINISNDQNMEKTVFQKEGNYGDNWNYGQVTLNETVKFKVAFN\n';
        fasta_ex1 = fasta_ex1 + 'AFKNKILSDIALDDISLTYGICNGSLYPEPTLVPTPPPELPTDCGGPFELWEPNTTFSST\n';
        fasta_ex1 = fasta_ex1 + 'NFPNSYPNLAFCVWILNAQKGKNIQLHFQEFDLENINDVVEIRDGEEADSLLLAVYTGPG\n';
        fasta_ex1 = fasta_ex1 + 'PVKDVFSTTNRMTVLLITNDVLARGGFKANFTTGYHLGIPEPCKADHFQCKNGECVPLVN\n';
        fasta_ex1 = fasta_ex1 + 'LCDGHLHCEDGSDEADCVRFFNGTTNNNGLVRFRIQSIWHTACAENWTTQISNDVCQLLG\n';
        fasta_ex1 = fasta_ex1 + 'LGSGNSSKPIFPTDGGPFVKLNTAPDGHLILTPSQQCLQDSLIRLQCNHKSCGKKLAAQD\n';
        fasta_ex1 = fasta_ex1 + 'ITPKIVGGSNAKEGAWPWVVGLYYGGRLLCGASLVSSDWLVSAAHCVYGRNLEPSKWTAI\n';
        fasta_ex1 = fasta_ex1 + 'LGLHMKSNLTSPQTVPRLIDEIVINPHYNRRRKDNDIAMMHLEFKVNYTDYIQPICLPEE\n';
        fasta_ex1 = fasta_ex1 + 'NQVFPPGRNCSIAGWGTVVYQGTTANILQEADVPLLSNERCQQQMPEYNITENMICAGYE\n';
        fasta_ex1 = fasta_ex1 + 'EGGIDSCQGDSGGPLMCQENNRWFLAGVTSFGYKCALPNRPGVYARVSRFTEWIQSFLH\n';
        var ac_ex2 = 'P98073\nQ3SYW2\nQ867B7\nP23604\nQ04962\nH2QKV6\nA5PF02\nF6ZWI6\nQ56IB8\nG5BQ09';
        var id_ex2 = 'ENTK_HUMAN\nCO2_BOVIN\nHGF_CANFA\nACH1_LONAC\nFA12_CAVPO\nH2QKV6_PANTR\nA5PF02_PIG\nF6ZWI6_HORSE\nQ56IB8_OSTNU\nG5BQ09_HETGA';
        var fast_ex2 = '';
        if ($('textarea[name=seq]').val() == ac_ex1) {
            $('textarea[name=seq]').val(id_ex1);
        }
        else if ($('textarea[name=seq]').val() == id_ex1) {
            $('textarea[name=seq]').val(pdb_ex1);
            return true;
        }
        else if ($('textarea[name=seq]').val() == pdb_ex1) {
            $('textarea[name=seq]').val(fasta_ex1);
            return true;
        }
        else if ($('textarea[name=seq]').val() == fasta_ex1) {
            $('textarea[name=seq]').val(ac_ex2);
            return true;
        }
        else if ($('textarea[name=seq]').val() == ac_ex2) {
            $('textarea[name=seq]').val(id_ex2);
            return true;
        }
        else {
            $('textarea[name=seq]').val(ac_ex1);
        }
    });



    // Example of MOTIFS (#meta2 and #meta3)
    // fills in textarea[name=sig] with examples
    $(':regex(id, ^meta[23]_ex_motif$)').click(function(){
        if ($('textarea[name=sig]').val() == 'PS50240') {
            $('textarea[name=sig]').val('TRYPSIN_DOM');
        }
        else if ($('textarea[name=sig]').val() == 'TRYPSIN_DOM') {
            $('textarea[name=sig]').val('P-x(2)-G-E-S-G(2)-[AS]');
        }
        else if ($('textarea[name=sig]').val() == 'P-x(2)-G-E-S-G(2)-[AS]') {
            $('textarea[name=sig]').val('PS50240 and PS50068');
        }
        else if ($('textarea[name=sig]').val() == 'PS50240 and PS50068') {
            $('textarea[name=sig]').val('PS50240 and not ( PS00134 or PS00135 )');
        }
        else if ($('textarea[name=sig]').val() == 'PS50240 and not ( PS00134 or PS00135 )') {
            $('textarea[name=sig]').val('PS50240 and P-x(2)-G-E-S-G(2)-[AS]');
        }
        else {
            $('textarea[name=sig]').val('PS50240');
        }
    });

    // When user enters text into textarea[name=seq] of #meta1
    // the text in textarea[name=seq] of #meta3 becomes the same
    // and vice versa
    $('textarea[name=seq]').change(function () {
        $('textarea[name=seq]').val($(this).val());
    });

    $('input:text[name=userdbcode]').change(function () {
        $('input:text[name=userdbcode]').val($(this).val());
    });

    // When user enters text into textarea[name=sig] of #meta2
    // the text in textarea[name=sig] of #meta3 becomes the same
    // and vice versa
    $('textarea[name=sig]').change(function () {
        $('textarea[name=sig]').val($(this).val());
    });


    // #meta1 STEP 2 running the scan at a low level
    // (concerns profiles only) is not compatible
    // with excluding profiles from the scan
    $('input:checkbox[name=noprofile]').click(function(){
        if ($(this).is(':checked')) {
            $('input:checkbox[name=lowscore]').prop('checked', false);
            $('input:checkbox[name=lowscore]').attr('disabled', true);
        }
        else {
            $('input:checkbox[name=lowscore]').attr('disabled', false);
        }
    });


    // #meta2_more_opt STEP 1 and #meta3_more_opt STEP 2
    // '>> More' <=> 'Less <<'
    // e.g. meta2_more_opt
    $(':regex(id, ^meta[23]_more_opt$)').click(function(){
        // e.g. #meta2_more
        var more = '#' + $(this).attr('id').replace('_opt','');
        $(more).toggle();
        if ($(this).html().match(/More$/)) {
            $(this).html('Less &laquo;');
        }
        else {
            $(this).html('&raquo; More');
        }
    });


    // meta2_add_opt STEP 1 and
    // meta3_add_opt STEP 2
    // '>> Options' <=> 'Options <<'
    // e.g. meta2_add_opt
    $(':regex(id, ^meta[23]_add_opt$)').click(function(){
        // e.g. #meta2_add
        var add_opt = '#' + $(this).attr('id').replace('_opt','');
        // e.g. #meta2_add :input
        var add_input = add_opt + ' :input';
        $(add_opt).toggle();
        if ($(this).html().match(/Options$/)) {
            $(this).html('Options &laquo;');
            $(add_input).attr('disabled', false);
        }
        else {
            $(this).html('&raquo; Options');
            $(add_input).attr('disabled', true);
        }
    });


    // #meta2 STEP 2
    $('#filters_opt').click(function(){
        $('#filters1').toggle();
        $('#filters2').toggle();
        if ($(this).html().match(/Filters$/)) {
            $(this).html('Filters &laquo;');
            if ($(this).html().match(/Options$/));
            if ($('input:radio[name=meta2_protein][value=opt1]').is(':checked')) {
                $(':regex(id, ^filters\\d+$) :input').attr('disabled', false);
            }
            else if ($('input:radio[name=meta2_protein][value=opt2]').is(':checked')) {
                $('#filters1 :input').attr('disabled', false);
                $('#filters2 :input').attr('disabled', true);
            }
            else if ($('input:radio[name=meta2_protein][value=opt3]').is(':checked')) {
                $(':regex(id, ^filters\\d+$) :input').attr('disabled', true);
            }
        }
        else {
            $(this).html('&raquo; Filters');
            $(':regex(id, ^filters\\d+$) :input').attr('disabled', true);
        }
    });

    // #meta2 STEP 2 checkbox[value=sp]
    $('input:checkbox[value=sp]').click(function(){
        if($(this).is(':checked')) {
            if ($('select[name=tissuefilter]').val() == '') {
                $('input:checkbox[name=varsplic]').prop('checked', true);
                $('input:checkbox[name=varsplic]').attr('disabled', false);
            }
        }
        else {
            $('input:checkbox[name=varsplic]').prop('checked', false);
            $('input:checkbox[name=varsplic]').attr('disabled', true);
        }
    });
    // #meta2 STEP 2
    // filter on tissue expression in incompatible with
    // Swiss-Prot splice variants
    //
    $('select[name=tissuefilter]').click(function () {
        if ($(this).val() == '') {
            if ($('input:checkbox[name=db][value=sp]').is(':checked')) {
                $('input:checkbox[name=varsplic]').prop('checked', true);
                $('input:checkbox[name=varsplic]').attr('disabled', false);
            }
        }
        else {
            $('input:checkbox[name=varsplic]').prop('checked', false);
            $('input:checkbox[name=varsplic]').attr('disabled', true);
        }
    });

    // STEP 3 (Submit your job)
    // 'Maximum number of displayed matches'
    // maxhits if displayed for:
    // -meta1 if meta1_protein_opt2
    // -meta2
    // -meta3 if meta3_protein_opt2
    $('select[name=maxhits]').click(function(){

        $('select[name=output] option[value=nice]').remove();
        $('select[name=output] option[value=html]').remove();
        $('select[name=output] option[value=plain]').remove();
        $('select[name=output] option[value=fasta]').remove();
        $('select[name=output] option[value=tabular]').remove();
        $('select[name=output] option[value=list]').remove();

        $(new Option('Text', 'plain')).appendTo('select[name=output]');
        $(new Option('FASTA', 'fasta')).appendTo('select[name=output]');
        $(new Option('Table', 'tabular')).appendTo('select[name=output]');
        $(new Option('Matchlist', 'list')).appendTo('select[name=output]');

        if ($(this).val() == '100000') {
            $('input:checkbox[name=email_opt]').prop('checked', true);
            $('input:checkbox[name=email_opt]').attr('disabled', true);
            $('#email :input').attr('disabled', false);
            $('input:text[name=email]').attr('required', true);
            $('#email').show();

            $('select[name=output]').val('plain');
        }
        else {

            $('input:checkbox[name=email_opt]').attr('disabled', false);
            $('input:text[name=email]').attr('required', false);

            var sEmail = $('input:text[name=email]').val().replace(/ /g,'');
            if (validateEmail(sEmail)) {
                $('select[name=output]').val('plain');
            }
            else {
                $(new Option('Simple view', 'html')).prependTo('select[name=output]');
                $(new Option('Graphical view', 'nice')).prependTo('select[name=output]');
                $('select[name=output]').val('nice');
            }
        }
    });

    // If user choses to retrieve complete sequences
    // the number max nb of matches that can be displayed is 1'000
    // => set maxhits to 1000 and disable maxhits
    //
    // 'Matchlist' is removed from the output format options
    // 'Graphical view' is removed fromt the output format options
    // only in meta2 (rich viewer does not display database retrieved
    // sequences)
    // NB: The output format 'Matchlist' is removed
    //     because if you get a matchlist, you don't ge complete
    //     sequences
    $('input:checkbox[name=compseq]').click(function(){

        $('input:checkbox[name=email_opt]').attr('disabled', false);
        $('input:text[name=email]').attr('required', false);

        $('select[name=output] option[value=nice]').remove();
        $('select[name=output] option[value=html]').remove();
        $('select[name=output] option[value=plain]').remove();
        $('select[name=output] option[value=fasta]').remove();
        $('select[name=output] option[value=tabular]').remove();
        $('select[name=output] option[value=list]').remove();

        $(new Option('Text', 'plain')).appendTo('select[name=output]');
        $(new Option('FASTA', 'fasta')).appendTo('select[name=output]');
        $(new Option('Table', 'tabular')).appendTo('select[name=output]');

        var sEmail = $('input:text[name=email]').val().replace(/ /g,'');

        if ($(this).is(':checked')) {
            $('select[name=maxhits]').val('1000');
            $('select[name=maxhits]').attr('disabled', true);

            if (validateEmail(sEmail)) {
                $('select[name=output]').val('plain');
            }
            else {
                $(new Option('Simple view', 'html')).prependTo('select[name=output]');
                var meta_nb = $('input:radio[name=meta]:checked').val().replace(/opt/,'');
                if (meta_nb == '2') {
                    $('select[name=output]').val('html');
                }
                else {
                    $(new Option('Graphical view', 'nice')).prependTo('select[name=output]');
                    $('select[name=output]').val('nice');
                }
            }
        }
        else {
            $('select[name=maxhits]').val('10000');
            $('select[name=maxhits]').attr('disabled', false);

            $(new Option('Matchlist', 'list')).appendTo('select[name=output]');
            if (validateEmail(sEmail)) {
                $('select[name=output]').val('plain');
            }
            else {
                $(new Option('Simple view', 'html')).prependTo('select[name=output]');
                $(new Option('Graphical view', 'nice')).prependTo('select[name=output]');
                $('select[name=output]').val('nice');
            }
        }
    });

    $('input:checkbox[name=email_opt]').click(function(){
        if ($(this).is(':checked')) {
            $('#email :input').attr('disabled', false);
            $('#email').show();
        }
        else {
            $('#email :input').val('');
            $('#email :input').attr('disabled', true);
            $('#email').hide();

            $('select[name=output] option[value=nice]').remove();
            $('select[name=output] option[value=html]').remove();
            $('select[name=output] option[value=plain]').remove();
            $('select[name=output] option[value=fasta]').remove();
            $('select[name=output] option[value=tabular]').remove();
            $('select[name=output] option[value=list]').remove();

            // 3 situations:
            // -maxhits = 1000 and compseq is checked
            // -maxhits = 1000 and compseq is unchecked
            // -maxhits = 10000 and compseq is unchecked (compseq forces
            //maxhits = 1000)
            // NB maxhits = 100000 not possible becaues
            // maxhits = 100000 forces email_opt checked and
            // disables email_opt then user cannot uncheck it
            $(new Option('Simple view', 'html')).appendTo('select[name=output]');
            $(new Option('Text', 'plain')).appendTo('select[name=output]');
            $(new Option('FASTA', 'fasta')).appendTo('select[name=output]');
            $(new Option('Table', 'tabular')).appendTo('select[name=output]');

            // -maxhits = 1000 and compseq is checked
            if ($('input:checkbox[name=compseq]').is(':checked')) {
                $('select[name=output]').val('html');
            }
            // -maxhits = 1000 and compseq is unchecked
            // -maxhits = 10000 and compseq is unchecked (compseq
            //forces maxhits = 1000)
            else {
                $(new Option('Matchlist', 'list')).appendTo('select[name=output]');
                $(new Option('Graphical view', 'nice')).prependTo('select[name=output]');
                $('select[name=output]').val('nice');
            }
        }
    });


    $('input:text[name=email]').change(function () {
        var sEmail = $(this).val().replace(/ /g,'');
        $('input:text[name=email]').val(sEmail);

        // do nothing because email addressed is required
        // so even if there's no email or no valid email entered
        // should behave as if the email was valid and limit
        // the output format to 'Text', 'FASTA', 'Table'
        // or 'Matchlist'
        // NB compseq is necessarily unchecked
        if ($('select[name=maxhits]').val() == '100000') {
        }
        // 3 situations:
        // -maxhits = 1000 and compseq is checked
        // -maxhits = 1000 and compseq is unchecked
        // -maxhits = 10000 and compseq is unchecked (compseq forces
        //maxhits = 1000)
        else {
            $('select[name=output] option[value=nice]').remove();
            $('select[name=output] option[value=html]').remove();
            $('select[name=output] option[value=plain]').remove();
            $('select[name=output] option[value=fasta]').remove();
            $('select[name=output] option[value=tabular]').remove();
            $('select[name=output] option[value=list]').remove();

            $(new Option('Text', 'plain')).appendTo('select[name=output]');
            $(new Option('FASTA', 'fasta')).appendTo('select[name=output]');
            $(new Option('Table', 'tabular')).appendTo('select[name=output]');

            // -maxhits = 1000 and compseq is checked
            if ($('input:checkbox[name=compseq]').is(':checked')) {
                if (validateEmail(sEmail)) {
                    $('select[name=output]').val('plain');
                }
                else {
                    $(new Option('Simple view', 'html')).prependTo('select[name=output]');
                    $('select[name=output]').val('html');
                }
            }
            // -maxhits = 1000 and compseq is unchecked
            // -maxhits = 10000 and compseq is unchecked (compseq
            //forces maxhits = 1000)
            else {
                $(new Option('Matchlist', 'list')).appendTo('select[name=output]');
                if (validateEmail(sEmail)) {
                    $('select[name=output]').val('plain');
                }
                else {
                    $(new Option('Simple view', 'html')).prependTo('select[name=output]');
                    $(new Option('Graphical view', 'nice')).prependTo('select[name=output]');
                    $('select[name=output]').val('nice');

                }
            }
        }
    });



//     KEEP: useful to see what is enabled or disable
//     $(document).click(function(){
//         $('[id^=meta]').show();
//     });

});