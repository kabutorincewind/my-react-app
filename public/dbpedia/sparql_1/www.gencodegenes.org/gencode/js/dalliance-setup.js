/* globals Browser: true, Chainset: true */
/*jsl:ignore*/

/*begging of new additions*/
function gencodeFIP(feature, info) {
    'use strict';
    var isGene = false;
    for (var hi = 0; hi < info.hit.length; ++hi) {
      if (info.hit[hi].isSuperGroup){
        isGene = true;
      }
    }

    if (!isGene) {
      info.setTitle('Transcript: ' + feature.label);
      info.add('Transcript ID', feature.label);
      info.add('Transcript biotype', feature.method);

    } else {
      info.setTitle('Gene: ' + feature.geneId);
    }

    info.add('Gene ID', feature.geneId);
    info.add('Gene name', feature.geneName);
    info.add('Gene biotype', feature.geneBioType);

    if (!isGene) {
      info.add('Transcript attributes', feature.tags);
    }
        
  }
/*end of addition*/


var sources = [{name:                 'Genome',
                       twoBitURI:            'http://www.biodalliance.org/datasets/hg38.2bit',
                       tier_type: 'sequence',
                       pinned: true},
                      {name: 'GENCODE version 27',
                       bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.v27.annotation.bb',
                       stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode2.xml',
                       collapseSuperGroups: true,
                       trixURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.v27.annotation.ix',
                       noSourceFeatureInfo: true,
                       featureInfoPlugin: gencodeFIP},
                      {name: 'GENCODE update',
                       bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/update_trackhub/data/hg38.bb',
                       stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode2.xml',
                       collapseSuperGroups: true,
                       trixURI: 'http://ngs.sanger.ac.uk/production/gencode/update_trackhub/data/hg38.anno.ix',
                       noSourceFeatureInfo: true,
                       featureInfoPlugin: gencodeFIP},
                       {name: 'Repeats',
                       desc: 'Repeat annotation from UCSC',
                       bwgURI: 'http://www.biodalliance.org/datasets/GRCh38/repeats.bb',
                       stylesheet_uri: 'http://www.biodalliance.org/stylesheets/bb-repeats2.xml'},
                       {name: 'Conservation',
                       bwgURI: 'http://www.biodalliance.org/datasets/phastCons46way.bw',
                       noDownsample: true,
                       mapping: 'hg19ToHg38'}];


for (var gencodeRelease = 7; gencodeRelease <= 19; ++gencodeRelease) {
  sources.push({
    name: 'GENCODE version ' + gencodeRelease,
    bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.v' + gencodeRelease + '.annotation.bb',
    stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode2.xml',
    collapseSuperGroups: true,
    trixURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.v' + gencodeRelease + '.annotation.ix',
    noSourceFeatureInfo: true,
    featureInfoPlugin: gencodeFIP,
    disabled: true,
    group: 'GENCODE Annotation Releases',
    mapping: 'hg19ToHg38'
  });
}


for (var gencodeRelease = 20; gencodeRelease <= 26; ++gencodeRelease) {
  sources.push({
    name: 'GENCODE version ' + gencodeRelease,
    bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.v' + gencodeRelease + '.annotation.bb',
    stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode2.xml',
    collapseSuperGroups: true,
    trixURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.v' + gencodeRelease + '.annotation.ix',
    noSourceFeatureInfo: true,
    featureInfoPlugin: gencodeFIP,
    disabled: true,
    group: 'GENCODE Annotation Releases'
  });
}


var b = new Browser({
    chr:                 '22',
    viewStart:           30700000,
    viewEnd:             30900000,
    cookieKey:           'human-grc_h38',

    fullScreen: true,

    coordSystem: {
      speciesName: 'Human',
      taxon: 9606,
      auth: 'GRCh',
      version: '38',
      ucscName: 'hg38'
    },

    chains: {
      hg19ToHg38: new Chainset('http://www.derkholm.net:8080/das/hg19ToHg38/', 'GRCh37', 'GRCh38', {
        speciesName: 'Human',
        taxon: 9606,
        auth: 'GRCh',
        version: 37,
        ucscName: 'hg19'
      })
    },

    sources: sources,

    browserLinks: {
      Ensembl: 'http://www.ensembl.org/Homo_sapiens/Location/View?r=${chr}:${start}-${end}',
      UCSC: 'http://genome.ucsc.edu/cgi-bin/hgTracks?db=hg38&position=chr${chr}:${start}-${end}',
      Sequence: 'http://www.derkholm.net:8080/das/hg38comp/sequence?segment=${chr}:${start},${end}'
    },

    hubs: ['http://www.broadinstitute.org/compbio1/PhyloCSFtracks/trackHub/hub.txt']
    /*hubs: ['http://ftp.ebi.ac.uk/pub/databases/ensembl/encode/integration_data_jan2011/hub.txt']
        /*   'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/hub.txt']
*/
  });


/*jsl:end*/



b.fooBar = 'baz';
