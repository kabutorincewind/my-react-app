/* globals Browser: true, Chainset: true */
/*jsl:ignore*/

/*begging of new additions*/
function gencodeFIP(feature, info) {
    'use strict';
    var isGene = false;
    for (var hi = 0; hi < info.hit.length; ++hi) {
      if (info.hit[hi].isSuperGroup){
        isGene = true;
      }
    }

    if (!isGene) {
      info.setTitle('Transcript: ' + feature.label);
      info.add('Transcript ID', feature.label);
      info.add('Transcript biotype', feature.method);

    } else {
      info.setTitle('Gene: ' + feature.geneId);
    }

    info.add('Gene ID', feature.geneId);
    info.add('Gene name', feature.geneName);
    info.add('Gene biotype', feature.geneBioType);

    if (!isGene) {
      info.add('Transcript attributes', feature.tags);
    }
        
  }
/*end of addition*/


var sources = [{name:                 'Genome',
                       twoBitURI:            'http://www.biodalliance.org/datasets/GRCm38/mm10.2bit',
                       tier_type: 'sequence',
                       pinned: true},
                      {name: 'GENCODE version M16',
                       bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.vM16.annotation.bb',
                       stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode.xml',
                       collapseSuperGroups: true,
                       trixURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.vM16.annotation.ix',
                       noSourceFeatureInfo: true,
                       featureInfoPlugin: gencodeFIP},
                      {name: 'GENCODE update',
                       bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/update_trackhub/data/mm10.bb',
                       stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode.xml',
                       collapseSuperGroups: true,
                       trixURI: 'http://ngs.sanger.ac.uk/production/gencode/update_trackhub/data/mm10.anno.ix',
                       noSourceFeatureInfo: true,
                       featureInfoPlugin: gencodeFIP},
                      {name: 'Repeats',
                       desc: 'Repeat annotation from UCSC',
                       bwgURI: 'http://www.biodalliance.org/datasets/GRCm38/repeats.bb',
                       stylesheet_uri: 'http://www.biodalliance.org/stylesheets/bb-repeats2.xml'}];


for (var gencodeRelease = 1; gencodeRelease <= 1; ++gencodeRelease) {
  sources.push({
    name: 'GENCODE version M' + gencodeRelease,
    bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.vM' + gencodeRelease + '.annotation.bb',
    stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode.xml',
    collapseSuperGroups: true,
    trixURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.vM' + gencodeRelease + '.annotation.ix',
    noSourceFeatureInfo: true,
    featureInfoPlugin: gencodeFIP,
    disabled: true,
    group: 'GENCODE Annotation Releases',
    mapping: 'mm9ToMm10'
  });

}

for (var gencodeRelease = 2; gencodeRelease <= 15; ++gencodeRelease) {
  sources.push({
    name: 'GENCODE version M' + gencodeRelease,
    bwgURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.vM' + gencodeRelease + '.annotation.bb',
    stylesheet_uri: 'http://www.biodalliance.org/stylesheets/gencode.xml',
    collapseSuperGroups: true,
    trixURI: 'http://ngs.sanger.ac.uk/production/gencode/trackhub/data/gencode.vM' + gencodeRelease + '.annotation.ix',
    noSourceFeatureInfo: true,
    featureInfoPlugin: gencodeFIP,
    disabled: true,
    group: 'GENCODE Annotation Releases'
  });


}




var b = new Browser({
    chr:                 '19',
    viewStart:           30700000,
    viewEnd:             30900000,
    cookieKey:           'mouse38',

    fullScreen: true,

    coordSystem: {
      speciesName: 'Mouse',
      axon: 10090,
      auth: 'GRCm',
      version: 38,
      ucscName: 'mm10'
    },

    chains: {
      mm9ToMm10: new Chainset('http://www.derkholm.net:8080/das/mm9ToMm10/', 'NCBIM37', 'GRCm38', {
        speciesName: 'Mouse',
        taxon: 10090,
        auth: 'NCBIM',
        version: 37,
        ucscName: 'mm9'
      })
    },

    sources: sources,

    browserLinks: {
      Ensembl: 'http://www.ensembl.org/Mus_musculus/Location/View?r=${chr}:${start}-${end}',
      UCSC: 'http://genome.ucsc.edu/cgi-bin/hgTracks?db=mm10&position=chr${chr}:${start}-${end}',
      Sequence: 'http://www.derkholm.net:8080/das/mm10comp/sequence?segment=${chr}:${start},${end}'
    },

    hubs: ['http://www.broadinstitute.org/compbio1/PhyloCSFtracks/trackHub/hub.txt']

  });


/*jsl:end*/



b.fooBar = 'baz';
