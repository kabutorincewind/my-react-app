/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var baseURL="http://" + ($(location).attr('href').split("/"))[2];
function printPage() {
    window.print();
}

function setFrameTitle(){
    if(parent.document){
        parent.document.title = document.title;
    }

}
function jscheck(){
    if(document.getElementById("javascript_check")){
        document.getElementById("javascript_check").style.display="none";
    }
}

function add2Favorite(url, title){
    //alert(title + ":" + url);
    //document.title = title;
    if (document.all){
        // Add to Favorites (Internet Explorer)
        //alert("IE");
        window.external.AddFavorite(url,title);
    }else if(window.sidebar){
        // Add to Bookmarks (Mozilla Firefox)
        //alert("FF")
        window.sidebar.addPanel(title, url, '');
    }else if(window.opera && window.print){
        // Opera
        //alert("Other")
        var elem = document.createElement('a');
        elem.setAttribute('href',url);
        elem.setAttribute('title',title);
        elem.setAttribute('rel','sidebar');
        // required to work in opera 7+
        elem.click();
    }else{
        alert("This function in only supported on Firefox and IE")
    }
}
function hideOrShowSection(sectionId, obj){
    //alert(obj.childNodes[0].nodeValue="Show");
    var sect = document.getElementById(sectionId);
    if(sect){
        if (sect.style.display=='none'){
            sect.style.display='block';
            if(obj != null){
                obj.childNodes[0].nodeValue="Hide";
            }
        }else{
            sect.style.display='none';
            if(obj != null){
                obj.childNodes[0].nodeValue="Show";
            }
        }
    }
}

function user_login_widget(){
    $.getJSON(baseURL + "/combrex-user/services/loginInfo.jsp?callback=?",
        function(data){
            var Div_widget = document.getElementById("loginWidget");
            var myaccountURL=baseURL + "/combrex-user/profile.jsp";
            if(data && Div_widget){
                if(data.username=="") {
                    Div_widget.innerHTML = "<a href=\"" + data.loginURL + "\">Log In</a>";
                }
                else {
                    Div_widget.innerHTML = "<a href=\"" + myaccountURL + "\" target=\"_blank\">" + data.username + "</a>&nbsp;|&nbsp;<a href=\"" + data.logoutURL + "\">Log Out</a>";
                }
            }else{
                if(Div_widget){
                    Div_widget.innerHTML="<a href=\""+ baseURL + "/combrex-user/profile.jsp\" target=\"_blank\">My Account</a>";
                }
            }
        }
        );
}
function dispFullCdd(pssmid){
    var cddFullText = document.getElementById("cdd_"+pssmid);
    var cddMoreLink = document.getElementById("dis_cdd_" + pssmid);
    cddFullText.style.display="inline";
    cddMoreLink.style.display="none";
}

function howto_list() {
    function showHowTo() {
        var position = $("#tab_howto").position();
        $("#fun_sub_howto").css("top", position.top + 18);
        $("#fun_sub_howto").css("left", position.left-5);
        $( "#fun_sub_howto" ).show( "blind", {}, 750 );
    };
    function hideHowTo() {
        $( "#fun_sub_howto" ).hide( "blind", {}, 750 );
    };
    $( "#fun_howto" ).click(function() {
        showHowTo();
        if(disp_timeout !=0){
            clearTimeout(disp_timeout);
        }
        return false;
    });
    $( "#fun_sub_howto" ).hide();
    var disp_timeout=0;
    $("#fun_sub_howto").hover(
        function(){
            if(disp_timeout !=0){
                clearTimeout(disp_timeout);
            }
        },
        function(){
            if(disp_timeout !=0){
                clearTimeout(disp_timeout);
            }
            //var this_elm=this;
            disp_timeout=setTimeout(
                function(){
                    disp_timeout=0;
                    hideHowTo();
                },1000);
        }
        )
}
function advSearch() {
    function showAdvSearch() {
        $( "#advSearch" ).show( "blind", {}, 750 );
    };
    function hideAdvSearch() {
        $( "#advSearch" ).hide( "blind", {}, 750 );
    };
    $( "#disp_advSearch" ).click(function() {
        if( $("#disp_advSearch").html()=="more options"){
            $("#Search").hide();
            $("#moreOptions").hide();
            //$("#disp_advSearch").html("");
            $(".firstInp").css("vertical-align", "bottom")
            showAdvSearch();
        }else{
            $("#disp_advSearch").html("more options");
            hideAdvSearch();
            $(".firstInp").css("vertical-align", "middle")
             $("#Search").show();
        }
        return false;
    });
    $( "#close_advSearch" ).click(function() {
        $("#disp_advSearch").html("more options");
        $("#Search").show();
        $("#moreOptions").show();
        hideAdvSearch();
        $(".firstInp").css("vertical-align", "middle")
        return false;
    });
    $( "#advSearch" ).hide();
}

function toolstip_dialog(){
    $(function() {
        $( "#dialog" ).dialog({
            autoOpen: false
        });
    });
    $(function(){
        $(".dialog_tooltips").click( function() {
            tx=this;
            var title=$($(tx).contents()[0]).text();
            var content=tx.title;
            $("#dialog").html("<p>" + content + "</p>");
            $("#dialog" ).dialog("option", "title", title);
            $("#dialog").dialog('open');
        });
    });
    $(function(){
        $(".dialog_tooltips").hover(
            function(){
                $($(this).contents()[0]).wrap("<span class=\"dialog_link_mouseIn\"></span>");
            },
            function(){
                $($($(this).contents()[0]).contents()[0]).unwrap();
            }
            )
    });
}

function jump_down_link(){
    $(".jupdDownLink").filter(
        function(index){
            var target=$($(this).children("a")[0]).attr("href").substr(1);
            if($("#"+target).length > 0){
                return false;
            }else{
                return true;
            }
        }).css("display", "none");
}

function auto_complete()
{
  function selectItem(li) {
          findValue(li);
  }
  function formatItem(row) {
          return row[0];
  }
  function findValue(li) {
          if( li == null ) return alert("No match!");
          if( !!li.extra ) var sValue = li.extra[0];
          else var sValue = li.selectValue;
  }
  $(document).ready(function () {
      $("#speciesText").autocomplete(
        "xhr/speciesText.jsp",
        {
          delay:10,
          minChars:1,
          matchSubset:1,
          matchContains:0,
          cacheLength:10,
          width: 500,
          onItemSelect:selectItem,
          onFindValue:findValue,
          formatItem:formatItem,
          autoFill:false,
          extraParams:{}
        }
      );
  });
}
function share_list() {
    function showList() {
        var position = $("#shareBTN").position();
        $("#share_submenu").css("top", position.top + 15);
        $("#share_submenu").css("left", position.left-350);
        $("#share_submenu" ).show( "blind", {}, 750 );
    };
    function hideList() {
        $("#share_submenu" ).hide( "blind", {}, 750 );
    };
    $( "#shareBTN" ).click(function() {
        showList();
        if(disp_timeout !=0){
            clearTimeout(disp_timeout);
        }
        return false;
    });
    $( "#share_submenu" ).hide();
    var disp_timeout=0;
    $("#share_submenu").hover(
        function(){
            if(disp_timeout !=0){
                clearTimeout(disp_timeout);
            }
        },
        function(){
            if(disp_timeout !=0){
                clearTimeout(disp_timeout);
            }
            //var this_elm=this;
            disp_timeout=setTimeout(
                function(){
                    disp_timeout=0;
                    hideList();
                },1000);
        }
        )
}

  function loadPubmed()
  {
    var pmids="";
    $(".pubmed").each(function(idx, elt){
      pmids += $(elt).attr("pmid") + ",";
    });
    if (pmids.length > 0)
    {
      $.getJSON("http://entrezajax.appspot.com/esummary?callback=?",
      {db : 'pubmed',
       apikey : '3556356054f2431e6f43a084e40e2e92',
       id : pmids},
      function(data)
      {
	$(data.result).each(function(ix, e)
	{
	  $(".pubmed[pmid=" + e.Id + "]").html("<a href=\"http://www.ncbi.nlm.nih.gov/pubmed/" + e.Id + "\" target=\"_blank\">" + e.Title + "</a><br>" + e.AuthorList + "<br>" + e.FullJournalName + " " + e.PubDate + "<br/><br/>");
	});
      });
    }
  }

  $(document).ready(function()
  {
    $.getJSON("DAI?command=SciBay&fun=xhr&xhrcmd=phenotypes&_view=view/json.jsp",
      function(data)
      {
	var selValue = $("#phenotype").attr("selectedValue");
	$.each(data.phenotypes, function(idx, entry)
	{
	  var opt = $("<option></option>").attr("value",entry.phenotypeId).text(entry.phenotype);
	  if (entry.phenotypeId==selValue)
	  {
	    opt.attr("selected", 1);
	  }
	  $("#phenotype").append(opt);
	});
      });
    });

addLoadEvent(setFrameTitle);
addLoadEvent(jscheck);
addLoadEvent(user_login_widget);
addLoadEvent(howto_list);
addLoadEvent(advSearch);
addLoadEvent(toolstip_dialog);
addLoadEvent(auto_complete);
addLoadEvent(share_list);
addLoadEvent(jump_down_link);