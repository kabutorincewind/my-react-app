

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Combrex: Submitted Predictions</title>
        <link rel="stylesheet" type="text/css" href="common.css"></link>
        <link type="text/css" rel="stylesheet" href="jquery/css/smoothness/jquery-ui-1.8.6.custom.css"></link>
        <script type="text/javascript" src="jquery/js/jquery.js"></script>
        <script type="text/javascript" src="jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="utility.js"></script>
        <script type="text/javascript" src="common.js"></script>
        <style type="text/css">
            /*
            #main{
                height:100%;
            }
            */
            #leftPanel{
                width:10px;
            }
            #mainContent{
                margin-right: 100px;
                margin-left: 20px;
                /*float: right;*/
            }
            .geneName{
                width: 350px;
            }
            .genePredFun{
                width: 350px;
                overflow: hidden;
                white-space: nowrap;
                padding: 5px 10px 5px 10px;
            }
            .tit{
                font-weight: bold;
            }
            #mainAnnounce{
                width: 100%;

            }
        </style>
        <script type="text/javascript">
            function displayGeneDescription(gid){
                if(document.getElementById(gid).style.display == 'block'){
                    document.getElementById(gid).style.display='none';
                }else{
                    document.getElementById(gid).style.display='block';
                }
            }
            function displayLeftPanel(){
                document.getElementById('leftPanel').style.width='200px';
                document.getElementById('mainContent').style.marginLeft='210px';
                document.getElementById('showLeft').style.display='none'
            }
            function hideLeftPanel(){
                document.getElementById('leftPanel').style.width='10px';
                document.getElementById('mainContent').style.marginLeft="10px";
                document.getElementById('showLeft').style.display='block';
            }
            function underDevelope(txt){
                var confirmDialog = new objDialog("cnfirmDialog",txt,"","", "");
                confirmDialog.center=true;
                confirmDialog.displayCancel=false;
                confirmDialog.show();
            }
        </script>
        
<!-- combrex.bu.edu -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-428391-5']);
            _gaq.push(['_setDomainName', 'none']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>


    </head>
    <body>
        


<div id="head">
    <div id="links">
        <b class=""><a href="./">Home</a></b>
    </div>
    <div id="users">
        <div class="rightLink" id="shareBTN"><a href="#shareBTN">Share/Save this page</a></div>
        <div class="rightLink"><div id="loginWidget" style="display:inline;"></div></div>
        <div id="share_submenu">
            <div id="share_submenu_content">
                <p style="font-weight: bold">Share this page on...</p>
                <p>
                    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=Prediction_Description.jsp' show_faces="false" width="350" action="recommend" font=""></fb:like>
                </p>
                <p>
                    <a href="http://twitter.com/share" class="twitter-share-button" data-url='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=Prediction_Description.jsp' data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                </p>
                <hr />
                <p><span style="font-weight: bold">Grab the link</span><br />
                    Here's a link to this page. Just copy and paste.
                    <input type="text" style="width:350px;" value='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=Prediction_Description.jsp' /><br />
                    <input type="button" onclick="add2Favorite('http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=Prediction_Description.jsp','Combrex: Contact')" value="Add to bookmark" />
                    <br />Or press Ctrl+D (Windows) or Command/Cmd + D (Mac) to bookmark this page
                </p>
            </div>
        </div>
    </div>
    <div style="left: 0pt; opacity: 1;" class="gbh"></div>
    <div style="right: 0pt; opacity: 1;" class="gbh"></div>
</div>
<script type="text/javascript" src="jquery/plugins/autocomplete-mod/jquery.autocomplete.js" />
<script type="text/javascript">
</script>

<div id="searchMain" style=" ">&nbsp;
    <form action="DAI?command=SciBay&fun=search" method="post">
        <table cellpadding="0" cellspacing="0" id="basicSearch" style=" ">
            <tbody>
                <tr>
                    <td id="smallLego" title="Back to Home">
                        <a style="text-decoration: none;" href="index.jsp"><img src="logo_tmp.png" width="200" height="45" style="border: 1px solid #666666" alt="Combrex"/></a>
                    </td>
                    <td class="firstInp" nowrap="nowrap" align="left" valign="middle" style="width:300px;">
                        <input style="width:280px;" maxlength="200" name="keyword" id="keyword" type="text" value="" />
                    </td>
                    <td class="firstInp" >
                        Enter Keyword, gene symbol, or identifier <input name="Search" id="Search" value="Search" type="submit" />                            (<span id="moreOptions"><a href="#" id="disp_advSearch" title="Limit your search results by specifying gene experimental validation status, protein cluster experimental validation status, and species name. You can also limit your search results to include only genes with high quality predictions of gene function">more options</a>; </span>
                        <a href="help_center.jsp?tip" target="_bnalk">Help</a> )
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="advSearch" cellspacing="0" cellpadding="0" >
            <tbody>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px; padding: 2px 0px;">
                        <input style="width:280px;" maxlength="200" name="speciesText" id="speciesText" type="text" value=""  />
                    </td>
                    <td>Enter species name</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 2px 0px;">
                        <select style="width:280px;" name="geneStatus" id="geneStatus">
                            <option value="all" selected="true">[All]</option>
                            <option value="0" >Gold (experimental evidence, curated)</option>
                            <option value="1" >Green (experimental evidence, uncurated)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="3" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                    <td>Gene functional status (select)</td>
                </tr>
                <!--
                <tr>
                    <td>
                        Protein Cluster Status:
                        <select name="proteinClusterStatus" id="proteinClusterStatus">
                            <option value="all"  selected="true">No preference</option>
                            <option value="1" >Green (experimental evidence)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="-1" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                </tr>
                -->
                <tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 5px 0px;">
                        <select style="width:280px;" name="phenotype" id="phenotype" selectedValue="">
                            <option value="-1">[All]</option>
                        </select>
                    </td>
                    <td>Phenotype association (select)[<a href="DAI?command=SciBay&fun=phenotypes">View list of phenotypes</a>]</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Check to restrict search to:
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPathogens" value="1"  /> Genes in Category A-C and Emerging/Re-Emerging pathogens
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="curated" value="1"  /> Genes in curated Protein Clusters
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px">
                        <input type="checkbox" name="hasPrediction" value="1" /> Genes with predictions submitted to COMBREX <div class="letterBox">P</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasStructure" value="1"  /> Genes with solved structures in PDB <div class="letterBox">S</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasHumanHomology" value="1"  /> Genes with Pfam domains shared with human proteins <div class="letterBox">H</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPurified" value="1"  /> Genes with products purified by PSI <div class="letterBox">U</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isCloned" value="1"  /> Genes cloned by PSI <div class="letterBox">C</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height:50px">
                        <input name="Search2" id="Search2" value="Search" type="submit" title="Acceptable Search Terms: NCBI gene ID, Uniprot accession number, RefSeq protein ID, Enzyme Commission (EC) number, NCBI Protein Cluster ID, gene symbol, gene name or name fragment, keywords "/>
                        <a href="#" id="close_advSearch" >close options</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

</div>

        <div id="functionalBar">
            <ul id="functionalLink2">
    <li class="headlink"><a href="index.jsp">Home</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink">Search</li>
-->
    <li class="vbar">|</li>
    <li class="headlink" id="tab_howto"><a href="#" title="" id="fun_howto">What You Can Do...</a>
	<div id="fun_sub_howto">
            <ul>
                <!--<li><a href="instructions_for_bid_submittal.html" title="Submit a bid to functionally validate a prediction" target="_blank">Submit a grant proposal</a></li>-->
                <li><a href="guide_to_submit_predictions.html" target="_blank">Submit Prediction</a></li>
                <li><a href="guide_to_submit_annotation.html" target="_blank">Submit Annotation</a></li>
                <li><a href="nominate_gene_for_high_priority_list.html" target="_blank">Nominate a gene for High Priority List</a></li>
            </ul>
        </div>
    </li>
    <!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="genome.jsp" title="Lists of the genes within the genomes of E. coli MG1655 and H. pylori 26695">Genome</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="statstic.jsp">Statistics</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="download.jsp" title="Download the Gold Standard Dataset and the Green Gene Set">Download</a></li>
    -->
    <li class="vbar">|</li>
    <li class="headlink"><a href="help_center.jsp">Help Center</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="acknowledgments.jsp">Acknowledgments</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="http://www.combrex.org/" target="_blank">About</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="https://spreadsheets0.google.com/viewform?hl=en&formkey=dEI5VG1xNGYydVhPZGlnM0NvM3hmWWc6MQ#gid=0" target="_blank">Bug Reporting/Feature Request</a></li>
-->
    <li class="vbar">|</li>
    <li class="headlink"><a href="contact.jsp">Contact Us</a></li>
    <li class="vbar">|</li>
<!--
    <li class="headlink"><a href="announcement.pdf">Grant Announcement</a></li>
    <li class="vbar">|</li>
-->
    <li class="headlink"><a href="whatIsNew.jsp">News</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="publications.jsp">Publications</a></li>

</ul>

        </div>
        <div id="main">
            <div id="mainContent">
                <div id="mainAnnounce">
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_6">OperonDB</a>(<a href="DAI?command=SciBay&fun=prediction&predictionid=6">Gene List</a>)
                        </div>
                        <div class="announceContent">
                            <p>OperonDB is based on a method that detects and analyzes conserved pairs of adjacent genes located on the same DNA strand in two or more bacterial genomes. For each conserved gene pair, OperonDB estimates the probability that the genes belong to the same operon by taking into account alternative possibilities that explain why the genes are adjacent in several genomes. Prediction of operon structure depends on conservation of gene order and orientation in two or more species. Genes within an operon often have related functions. Operon structure provides information about the function of genes within an operon.</p>
                            <p>
                                <ul>
                                    <li>Mihaela Pertea, Kunmi Ayanbule, Megan Smedinghoff and Steven L. Salzberg. OperonDB: a comprehensive database of predicted operons in microbial genomes. Nucleic Acids Res. 2009 Jan;37(Database issue):D479-82. Epub 2008 Oct 23.</li>
                                    <li><a href="http://operondb.cbcb.umd.edu/cgi-bin/operondb/operons.cgi">Weblink: http://operondb.cbcb.umd.edu/cgi-bin/operondb/operons.cgi</a></li>
                                </ul>
                            </p>

                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_5">Domain Fusion</a> ( <a href="DAI?command=SciBay&fun=prediction&predictionid=5">Gene List</a>)
                        </div>
                        <div class="announceContent">
                            <p>Domain fusion allows for the prediction of a functional relationship between two distinct genes in an organism depending on an instance where those two genes are fused as a continuous sequence in another organism. The fusion gene can indicate a relationship between genes that are independent in another organism. The fused gene suggests a relationship between the component genes which is not necessarily due to sequence similarity. Fusion links frequently relate genes of the same functional category. The function of an uncharacterized gene within a fusion link can be inferred from the known function of the gene to which it is fused.</p>
                            <p>
                                <ul>
                                    <li>Yanai, I., A. Derti, and C. DeLisi, Genes linked by fusion events are generally of the same functional category: a systematic analysis of 30 microbial genomes. Proc Natl Acad Sci U S A, 2001. 98(14): p.7940-5.</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_4">Phylogenetic Profile</a> (<a href="DAI?command=SciBay&fun=prediction&predictionid=4"> Gene List</a>)
                        </div>
                        <div class="announceContent">
                            <p>Phylogenetic profiling infers the function of a gene from another gene with known function with an identical pattern of presence and absence across a set of phylogenetically distributed genomes. The profile of a gene consists of the pattern of occurrence of its orthologs across a set of genomes. Orthologs here are used as defined in the COG database. Two genes are assumed to be functionally related if the correlation between their profiles is greater than would be expected by chance.</p>
                            <p>
                                <ul>
                                    <li>Wu, J., S. Kasif, and C. DeLisi, Identification of functional links between genes using phylogenetic profiles. Bioinformatics, 2003. 19(12): p. 1524-30.</li>
                                    <li>Wu, J., Z. Hu, and C. DeLisi, Gene annotation and network inference by phylogenetic profiling. BMC Bioinformatics, 2006. 7: p. 80.</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_3">Gene Neighborhood</a> (<a href="DAI?command=SciBay&fun=prediction&predictionid=3">Gene List</a>)
                        </div>
                        <div class="announceContent">
                            <p>Related function between a pair of genes can be inferred from the conservation of proximity between the two genes across many genomes. The probability that neighboring genes encode proteins within the same biological pathway depends on the number of genomes in which the proximity of the genes is conserved.  The conserved order of genes implies selective bias which suggests related function.  This method produces links between ortholog families validated by observed proximity in genomes representing multiple phylogenetic groups. </p>
                            <p>
                                <ul>
                                    <li>Yanai, I., J.C. Mellor, and C. DeLisi, Identifying functional links between genes using conserved chromosomal proximity. Trends Genet, 2002. 18(4): p. 176-9.</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_2">Horn Predictions (Data Mining of Enzymes)</a> (<a href="DAI?command=SciBay&fun=prediction&predictionid=2">Gene LIst</a>)
                        </div>
                        <div class="announceContent">
                            <p>The Horn lab uses Data Mining of Enzymes (DME) to determine whether a protein is an enzyme and, if so then determines its EC classification. DME compares the sequence of a protein with a list of Specific Peptides (SPs) to search for matches of SPs within a given protein sequence. SPs are subsequences of amino acids within an enzyme and are responsible for an enzyme's specific function. They cover most of the annotated active and binding site amino acids and occur in the 3-D pockets that are proximate to the active site. SPs are extracted from enzyme sequences and are specific to levels of the Enzyme Commission functional hierarchy. Given a sequence of a protein, DME searches through a list of SPs to find alignment between SPs and subsequences of the protein. DME uses the EC assignment associated with each SP and provides the predicted EC for the query protein.</p>
                            <p>
                                <ul>
                                    <li>Weingart U, Lavi Y, Horn D. Data mining of enzymes using specific peptides. BMC Bioinformatics 2009, 10: 446.</li>
                                    <li>Kunik V, Meroz Y, Solan Z, Sandbank B, Weingart U, Ruppin E, Horn D. Functional representation of enzymes by specific peptides. PLOS Comp Biol 2007 , 3(8):e167.</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_7">Vitkup Predictions</a> (<a href="DAI?command=SciBay&fun=prediction&predictionid=7">Gene List</a>)
                        </div>
                        <div class="announceContent">
                            <p>The Vitkup lab's approach integrates sequence-based and context-based correlations to probabilistically predict global metabolic networks for completely sequenced microbial genomes.
                                The method is based on Gibbs sampling of an entire metabolic network and provides probabilities/confidence for all metabolic annotation and alternative assignments.
                                For predictions submitted to COMBREX, they focus on predictions of specific metabolic molecular functions, i.e. four digits of the Enzyme Commission (EC) numbers.
                                Based on calculated and optimized context-based correlations as well as metabolic flux-balanced reconstructions, the genes responsible for many new metabolic molecular
                                functions are predicted. Context genomic correlations such as chromosomal gene clustering, phylogenetic profiles and gene fusion can provide important functional clues even
                                if sequence homology information is remote or absent.</p>
                            <p>
                                <ul>
                                    <li>Hsiao TL, Revelles O, Chen L, Sauer U, Vitkup D. Automatic policing of biochemical annotations using genomic correlations. Nat Chem Biol. 2010;6:34-40</li>
                                    <li>Chen L, Vitkup D. Predicting genes for orphan metabolic activities using phylogenetic profiles. Genome Biol. 2006;7:R17.</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_8">Berkeley Phylogenomics Group Loose Predictions</a> (<a href="DAI?command=SciBay&fun=prediction&predictionid=8">Gene List</a>)
                        </div>
                        <div class="announceContent">
                            <p>This is a preliminary upload of functional annotations based on PHOG orthology (prior to the COMBREX workshop). We provide them to facilitate working out any issues that may arise while we are meeting face-to-face. Berkeley PHOG does not yet assign numerical confidence values, but I (Ruchira) have assigned the confidence value "0.2" to all annotations in this file to indicate our most permissive, "loose" parameter setting (equivalent to the tree threshold used for human-fruit fly orthology calls). We expect this set of annotations to change in two ways: 1) A subset of these gene annotations may be overridden subsequently with functional annotations based on a smaller tree threshold distance (i.e., with higher confidence values). In this case the method field (which contains the supporting PHOG accessions) will also be updated. 2) The majority of H. pylori proteins are present in PhyloFacts trees; however, we are continuing to build new trees to improve the annotation of a subset of H. pylori proteins through the use of advanced remote homolog detection clustering methods. We expect this to result in several additional annotations at various confidence levels.
homology information is remote or absent.</p>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            <a name="pid_1">Methyltransferase</a> (<a href="DAI?command=SciBay&fun=prediction&predictionid=1">Gene List</a>)
                        </div>
                        <div class="announceContent">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="foot">
            <!--
            <div style=" font-size: smaller"><a href="contact.jsp">Contact Us</a> | <a href="http://www.bu.edu/bioinformatics/" target="_blank">Bioinformatics Program, Boston University</a></div>
-->
        </div>

    </body>
</html>
