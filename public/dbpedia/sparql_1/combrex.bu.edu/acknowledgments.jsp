
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>COMBREX: Acknowledgements</title>
        <link rel="stylesheet" type="text/css" href="common.css" />
        <link type="text/css" rel="stylesheet" href="jquery/css/smoothness/jquery-ui-1.8.6.custom.css"></link>
        <script type="text/javascript" src="jquery/js/jquery.js"></script>
        <script type="text/javascript" src="jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="utility.js"></script>
        <script type="text/javascript" src="common.js"></script>
        <style type="text/css">
            #mainContent{
                /*overflow:inherit;*/
                margin-left: 10px;
                clear: left;
                margin-top: 20px;
                width: 95%;
                height: 100%;
            }
            .geneName{
                width: 350px;
            }
            .genePredFun{
                width: 350px;
                overflow: hidden;
                white-space: nowrap;
                padding: 5px 10px 5px 10px;
            }
        </style>
        
<!-- combrex.bu.edu -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-428391-5']);
            _gaq.push(['_setDomainName', 'none']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>


    </head>
    <body>
        


<div id="head">
    <div id="links">
        <b class=""><a href="./">Home</a></b>
    </div>
    <div id="users">
        <div class="rightLink" id="shareBTN"><a href="#shareBTN">Share/Save this page</a></div>
        <div class="rightLink"><div id="loginWidget" style="display:inline;"></div></div>
        <div id="share_submenu">
            <div id="share_submenu_content">
                <p style="font-weight: bold">Share this page on...</p>
                <p>
                    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=acknowledgments.jsp' show_faces="false" width="350" action="recommend" font=""></fb:like>
                </p>
                <p>
                    <a href="http://twitter.com/share" class="twitter-share-button" data-url='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=acknowledgments.jsp' data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                </p>
                <hr />
                <p><span style="font-weight: bold">Grab the link</span><br />
                    Here's a link to this page. Just copy and paste.
                    <input type="text" style="width:350px;" value='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=acknowledgments.jsp' /><br />
                    <input type="button" onclick="add2Favorite('http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=acknowledgments.jsp','Combrex: Acknowledgements')" value="Add to bookmark" />
                    <br />Or press Ctrl+D (Windows) or Command/Cmd + D (Mac) to bookmark this page
                </p>
            </div>
        </div>
    </div>
    <div style="left: 0pt; opacity: 1;" class="gbh"></div>
    <div style="right: 0pt; opacity: 1;" class="gbh"></div>
</div>
<script type="text/javascript" src="jquery/plugins/autocomplete-mod/jquery.autocomplete.js" />
<script type="text/javascript">
</script>

<div id="searchMain" style=" ">&nbsp;
    <form action="DAI?command=SciBay&fun=search" method="post">
        <table cellpadding="0" cellspacing="0" id="basicSearch" style=" ">
            <tbody>
                <tr>
                    <td id="smallLego" title="Back to Home">
                        <a style="text-decoration: none;" href="index.jsp"><img src="logo_tmp.png" width="200" height="45" style="border: 1px solid #666666" alt="Combrex"/></a>
                    </td>
                    <td class="firstInp" nowrap="nowrap" align="left" valign="middle" style="width:300px;">
                        <input style="width:280px;" maxlength="200" name="keyword" id="keyword" type="text" value="" />
                    </td>
                    <td class="firstInp" >
                        Enter Keyword, gene symbol, or identifier <input name="Search" id="Search" value="Search" type="submit" />                            (<span id="moreOptions"><a href="#" id="disp_advSearch" title="Limit your search results by specifying gene experimental validation status, protein cluster experimental validation status, and species name. You can also limit your search results to include only genes with high quality predictions of gene function">more options</a>; </span>
                        <a href="help_center.jsp?tip" target="_bnalk">Help</a> )
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="advSearch" cellspacing="0" cellpadding="0" >
            <tbody>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px; padding: 2px 0px;">
                        <input style="width:280px;" maxlength="200" name="speciesText" id="speciesText" type="text" value=""  />
                    </td>
                    <td>Enter species name</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 2px 0px;">
                        <select style="width:280px;" name="geneStatus" id="geneStatus">
                            <option value="all" selected="true">[All]</option>
                            <option value="0" >Gold (experimental evidence, curated)</option>
                            <option value="1" >Green (experimental evidence, uncurated)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="3" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                    <td>Gene functional status (select)</td>
                </tr>
                <!--
                <tr>
                    <td>
                        Protein Cluster Status:
                        <select name="proteinClusterStatus" id="proteinClusterStatus">
                            <option value="all"  selected="true">No preference</option>
                            <option value="1" >Green (experimental evidence)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="-1" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                </tr>
                -->
                <tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 5px 0px;">
                        <select style="width:280px;" name="phenotype" id="phenotype" selectedValue="">
                            <option value="-1">[All]</option>
                        </select>
                    </td>
                    <td>Phenotype association (select)[<a href="DAI?command=SciBay&fun=phenotypes">View list of phenotypes</a>]</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Check to restrict search to:
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPathogens" value="1"  /> Genes in Category A-C and Emerging/Re-Emerging pathogens
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="curated" value="1"  /> Genes in curated Protein Clusters
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px">
                        <input type="checkbox" name="hasPrediction" value="1" /> Genes with predictions submitted to COMBREX <div class="letterBox">P</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasStructure" value="1"  /> Genes with solved structures in PDB <div class="letterBox">S</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasHumanHomology" value="1"  /> Genes with Pfam domains shared with human proteins <div class="letterBox">H</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPurified" value="1"  /> Genes with products purified by PSI <div class="letterBox">U</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isCloned" value="1"  /> Genes cloned by PSI <div class="letterBox">C</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height:50px">
                        <input name="Search2" id="Search2" value="Search" type="submit" title="Acceptable Search Terms: NCBI gene ID, Uniprot accession number, RefSeq protein ID, Enzyme Commission (EC) number, NCBI Protein Cluster ID, gene symbol, gene name or name fragment, keywords "/>
                        <a href="#" id="close_advSearch" >close options</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

</div>

        <div id="functionalBar">
            <ul id="functionalLink2">
    <li class="headlink"><a href="index.jsp">Home</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink">Search</li>
-->
    <li class="vbar">|</li>
    <li class="headlink" id="tab_howto"><a href="#" title="" id="fun_howto">What You Can Do...</a>
	<div id="fun_sub_howto">
            <ul>
                <!--<li><a href="instructions_for_bid_submittal.html" title="Submit a bid to functionally validate a prediction" target="_blank">Submit a grant proposal</a></li>-->
                <li><a href="guide_to_submit_predictions.html" target="_blank">Submit Prediction</a></li>
                <li><a href="guide_to_submit_annotation.html" target="_blank">Submit Annotation</a></li>
                <li><a href="nominate_gene_for_high_priority_list.html" target="_blank">Nominate a gene for High Priority List</a></li>
            </ul>
        </div>
    </li>
    <!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="genome.jsp" title="Lists of the genes within the genomes of E. coli MG1655 and H. pylori 26695">Genome</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="statstic.jsp">Statistics</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="download.jsp" title="Download the Gold Standard Dataset and the Green Gene Set">Download</a></li>
    -->
    <li class="vbar">|</li>
    <li class="headlink"><a href="help_center.jsp">Help Center</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="acknowledgments.jsp">Acknowledgments</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="http://www.combrex.org/" target="_blank">About</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="https://spreadsheets0.google.com/viewform?hl=en&formkey=dEI5VG1xNGYydVhPZGlnM0NvM3hmWWc6MQ#gid=0" target="_blank">Bug Reporting/Feature Request</a></li>
-->
    <li class="vbar">|</li>
    <li class="headlink"><a href="contact.jsp">Contact Us</a></li>
    <li class="vbar">|</li>
<!--
    <li class="headlink"><a href="announcement.pdf">Grant Announcement</a></li>
    <li class="vbar">|</li>
-->
    <li class="headlink"><a href="whatIsNew.jsp">News</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="publications.jsp">Publications</a></li>

</ul>

        </div>
        <div id="main">
            <div id="mainContent">
                <div id="headLine">
                    <h2>Acknowledgments</h2>
                </div>
                <div id="jumpDownList">
                    <p>
                        Jump to section on this page:
                        <span class="jupdDownLink" style="margin-left: 6px; margin-right: 6px;"><a href="#funding">Funding</a></span>
                        <span class="jupdDownLink" style="margin-left: 6px; margin-right: 6px;"><a href="#data">Major Source of Data and Annotations</a></span>
                        <span class="jupdDownLink" style="margin-left: 6px; margin-right: 6px;"><a href="#predictions">Function Predictions Submitted</a></span>
                        <span class="jupdDownLink"><a href="#grants">Participating Experimental Laboratories</a></span>
                    </p>
                </div>
                <div id="sections">

                    <!-- Funding -->
                    <div class="section">
                        <p class="utilLink">
                            <a href="#" onclick="hideOrShowSection('sec_funding',this);return false;">Hide</a>
                            |
                            <a href="#">Top</a>
                        </p>
                        <div class="sectionHead" title="">
                            <a id="funding">Funding</a>
                        </div>
                        <div class="sectionContent" id="sec_funding">

                            NIGMS/NIH Grant 1-RC2-Gm-092602-01

                        </div>
                    </div>

                    <!-- Data  -->
                    <div class="section">
                        <p class="utilLink">
                            <a href="#" onclick="hideOrShowSection('sec_data',this);return false;">Hide</a>
                            |
                            <a href="#">Top</a>
                        </p>
                        <div class="sectionHead" title="Data">
                            <a id="data">Major Sources of Data and Annotation</a>
                        </div>
                        <div class="sectionContent" id="sec_data">
                            <table class="sectionTable" cellpadding="0" cellspacing="0" >
                                <tbody>
                                    <tr>
                                        <td width="45%">
                                            National Center for Biotechnology Information (NCBI)
                                        </td>
                                        <td>
                                            <ul>
                                                <li><a href="http://www.ncbi.nlm.nih.gov/proteinclusters" target="_blank">Protein Clusters</a></li>
                                                <li><a href="http://www.ncbi.nlm.nih.gov/Genbank/" target="_blank">GenBank</a></li>
                                                <li><a href="http://www.ncbi.nlm.nih.gov/RefSeq/" target="_blank">Reference Sequence</a></li>
                                                <li><a href="http://www.ncbi.nlm.nih.gov/Structure/cdd/wrpsb.cgi" target="_blank">Conserved Domain Database (CDD)</a></li>
                                                <li><a href="http://www.ncbi.nlm.nih.gov/taxonomy" target="_blank">Taxonomy</a></li>
                                            </ul>
                                        </td>
                                    </tr
                                    <tr>
                                        <td>
                                            UniProt Consortium
                                        </td>
                                        <td>
                                            <a href="http://www.uniprot.org/help/uniprotkb" target="_blank">UniProtKB</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            The Gene Ontology Consortium
                                        </td>
                                        <td>
                                            <a href="http://www.geneontology.org/" target="_blank">Gene Ontology (GO)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            SRI International
                                        </td>
                                        <td>
                                            <a href="http://ecocyc.org/" target="_blank">EcoCyc</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Wellcome Trust / Sanger Institute
                                        </td>
                                        <td>
                                            <a href="http://pfam.sanger.ac.uk/" target="_blank">Pfam</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Research Collaboratory for Structural Bioinformatics
                                        </td>
                                        <td>
                                            <a href="http://www.rcsb.org/pdb/home/home.do" target="_blank">Protein Data Bank (PDB)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kanehisa Laboratories
                                        </td>
                                        <td>
                                            <a href="http://www.genome.jp/kegg/" target="_blank">Kyoto Encyclopedia of Genes and Genomes (KEGG)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            UCSF
                                        </td>
                                        <td>
                                            <a href="http://sfld.rbvi.ucsf.edu" target="_blank">Structure-Function Linkage Database(SFLD)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mark Pallen
                                        </td>
                                        <td>
                                            <a href="http://www.xbase.ac.uk/pseudodb/" target="_blank">xBASE (PseudoDB)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            EcoliHub
                                        </td>
                                        <td>
                                            <a href="http://www.ecolihub.org/" target="_blank">EcoliHub</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- Predictions  -->
                    <div class="section">
                        <p class="utilLink">
                            <a href="#" onclick="hideOrShowSection('sec_predictions',this);return false;">Hide</a>
                            |
                            <a href="#">Top</a>
                        </p>
                        <div class="sectionHead" title="Predictions">
                            <a id="predictions">Function Predictions Submitted</a>
                        </div>
                        <div class="sectionContent" id="sec_predictions">
                            <table class="sectionTable" cellpadding="0" cellspacing="0" >
                                <tbody>
                                    <tr>
                                        <td width="45%">
                                            DeLisi, Charles
                                        </td>
                                        <td>
                                            Boston University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Greiner, Russell
                                        </td>
                                        <td>
                                            University of Alberta
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Horn, David
                                        </td>
                                        <td>
                                            Tel Aviv University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Palssøn, Bernhard
                                        </td>
                                        <td>
                                            University of California, San Diego
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Salzberg, Steven <a href="http://operondb.cbcb.umd.edu" target="_blank">(OperonDB)</a>
                                        </td>
                                        <td>
                                            University of Maryland
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Segrè, Daniel
                                        </td>
                                        <td>
                                            Boston University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sjölander, Kimmen
                                        </td>
                                        <td>
                                            University of California, Berkeley
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Vitkup, Dennis
                                        </td>
                                        <td>
                                            Columbia University
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- Grants  -->
                    <div class="section">
                        <p class="utilLink">
                            <a href="#" onclick="hideOrShowSection('sec_grants',this);return false;">Hide</a>
                            |
                            <a href="#">Top</a>
                        </p>
                        <div class="sectionHead" title="">
                            <a id="grants">Participating Experimental Laboratories</a>
                        </div>
                        <div class="sectionContent" id="sec_grants">
                            <table class="sectionTable" cellpadding="0" cellspacing="0" >
                                <tbody>
                                    <tr>
                                        <td>
                                            Almo, Steven <sup>1</sup>
                                        </td>
                                        <td>
                                            Albert Einstein College of Medicine
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Armstrong, Richard N. <sup>1</sup>
                                        </td>
                                        <td>
                                            Vanderbilt University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Begley, Tadhg <sup>1</sup>
                                        </td>
                                        <td>
                                            Texas A&M University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bergerat, Agnes <sup>1</sup>
                                        </td>
                                        <td>
                                            Boston University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bhagwat, Ashok <sup>2</sup>
                                        </td>
                                        <td>
                                            Wayne State University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Blumenthal, Robert M. <sup>2</sup>
                                        </td>
                                        <td>
                                            University of Toledo
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bollinger, J. Martin <sup>2</sup>
                                        </td>
                                        <td>
                                            Pennsylvania State University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Booker, Squire <sup>1</sup>
                                        </td>
                                        <td>
                                            Pennsylvania State University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Brenner, Steven E. <sup>2</sup>
                                        </td>
                                        <td>
                                            University of California, Berkeley
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Chang, Woo-Suk <sup>2</sup>
                                        </td>
                                        <td>
                                            University of Texas-Arlington
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Columbus, Linda <sup>2</sup>
                                        </td>
                                        <td>
                                            University of Virginia
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cronan, John E. <sup>1</sup>
                                        </td>
                                        <td>
                                            University of Illinois, Urbana-Champaign
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            de Crécy-Lagard, Valérie <sup>2</sup>
                                        </td>
                                        <td>
                                            University of Florida, Gainesville
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Deutscher, Murray <sup>1</sup>
                                        </td>
                                        <td>
                                            University of Miami
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Dunaway-Mariano, Debra <sup>1</sup>
                                        </td>
                                        <td>
                                            University of New Mexico
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Engelward, Bevin <sup>1</sup>
                                        </td>
                                        <td>
                                            MIT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ferguson, Donald <sup>2</sup>
                                        </td>
                                        <td>
                                            Miami University (Ohio)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ferrer, Manuel <sup>2</sup>
                                        </td>
                                        <td>
                                            Institute of Catalysis CSIC
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Gadda, Giovanni <sup>2</sup>
                                        </td>
                                        <td>
                                            Georgia State University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Gerlt, John A. <sup>1</sup>
                                        </td>
                                        <td>
                                            University of Illinois, Urbana-Champaign
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Guthrie, Ellen <sup>1</sup>
                                        </td>
                                        <td>
                                            New England Biolabs
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Krebs, Carsten <sup>2</sup>
                                        </td>
                                        <td>
                                            Pennsylvania State University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Morgan, Richard D. <sup>2</sup>
                                        </td>
                                        <td>
                                            New England Biolabs
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Osterman, Andrei <sup>2</sup>
                                        </td>
                                        <td>
                                            Sanford-Burnham Medical Research Institute
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Poulter, C. Dale <sup>1</sup>
                                        </td>
                                        <td>
                                            University of Utah
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Raushel, Frank <sup>1</sup>
                                        </td>
                                        <td>
                                            Texas A&M University
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Robbins, Phil <sup>1</sup>
                                        </td>
                                        <td>
                                            Boston University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Rudd, Kenneth <sup>2</sup>
                                        </td>
                                        <td>
                                            University of Miami
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Saier, Milton <sup>1</sup>
                                        </td>
                                        <td>
                                            University of California, San Diego
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Samson, Leona <sup>1</sup>
                                        </td>
                                        <td>
                                            MIT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Setterdahl, Aaron <sup>2</sup>
                                        </td>
                                        <td>
                                            Indiana University Southeast
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Silhavy, Tom <sup>1</sup>
                                        </td>
                                        <td>
                                            Princeton University
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Söll, Dieter <sup>2</sup>
                                        </td>
                                        <td>
                                            Yale University
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Spain, Jim <sup>2</sup>
                                        </td>
                                        <td>
                                            Georgia Institute of Technology
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Walker, Graham <sup>1</sup>
                                        </td>
                                        <td>
                                            MIT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Weigele, Peter <sup>1</sup>
                                        </td>
                                        <td>
                                            New England Biolabs
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Xu, Shuang-yong <sup>1</sup>
                                        </td>
                                        <td>
                                            New England Biolabs
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Yakunin, Alexander <sup>2</sup>
                                        </td>
                                        <td>
                                            University of Toronto
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            1  Offered support.<br/>
                                            2  Experimental grant funded.
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="foot">
            <!--
            <div style=" font-size: smaller"><a href="contact.jsp">Contact Us</a> | <a href="http://www.bu.edu/bioinformatics/" target="_blank">Bioinformatics Program, Boston University</a></div>
-->
        </div>
    </body>
</html>

