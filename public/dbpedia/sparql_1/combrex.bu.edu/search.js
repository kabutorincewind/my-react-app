/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function checkSearch(){
    if(document.getElementById("HeadSearch")){
        document.getElementById("HeadSearch").onsubmit = function(ev){
            var ev = ev || window.event;
            var kw = document.getElementById("keyword").value;
            if(kw.replace(/^\s+|\s+$/g,"") == ""){
                var noInputDialog = new objDialog("NoInputDialog","Please Input Any Keyword","","", "");
                noInputDialog.center=true;
                noInputDialog.displayCancel=false;
                noInputDialog.show();
                return false;
            }
            {
                return true;
            }
        }

    }
}


function searchFieldDescription(){
    var searchDescription = "Keyword, Entrez Gene Id, Locus Tag, Protein Cluster Accession";
    if(document.getElementById("keyword")){
        document.getElementById("keyword").onmouseout = function(ev){
            var ev = ev || window.event;
            var kwField = document.getElementById("keyword");
            if(kwField.value == ""){
                kwField.value=searchDescription;
                kwField.style.color="gray";
            }else{
                kwField.style.color="black";
            }
        }
        document.getElementById("keyword").onmouseover = function(ev){
            var ev = ev || window.event;
            var kwField = document.getElementById("keyword");
            if(kwField.value == searchDescription){
                kwField.value="";
                kwField.style.color="black";
            }
        }
        document.getElementById("keyword").onchange = function(ev){
            var ev = ev || window.event;
            var kwField = document.getElementById("keyword");
            if(kwField.value != searchDescription){
                kwField.style.color="black";
            }
        }
    }
}

function searchExample(exp){
    if(document.getElementById("keyword")){
        var kwField = document.getElementById("keyword");
        kwField.value=exp;
        kwField.style.color="Black";
    }
}
//addLoadEvent(checkSearch);
//addLoadEvent(searchFieldDescription);