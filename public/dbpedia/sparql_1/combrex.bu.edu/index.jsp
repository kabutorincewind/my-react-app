
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>COMBREX Database</title>
        <link rel="stylesheet" type="text/css" href="common.css" />
        <link type="text/css" rel="stylesheet" href="jquery/css/smoothness/jquery-ui-1.8.6.custom.css" />
        <script type="text/javascript" src="jquery/js/jquery.js"></script>
        <script type="text/javascript" src="jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="jquery/plugins/autocomplete-mod/jquery.autocomplete.js"></script>
        <script type="text/javascript" src="utility.js"></script>
        <script type="text/javascript" src="search.js"></script>
        <script type="text/javascript" src="common.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".progressThermometer").each(function(idx, elt) {
                    $.post("DAI?command=SciBay&fun=xhr&xhrcmd=taxInfo",
                    {taxId : $(elt).attr("taxId")
                    },
                    function(data) {
                        $(elt).html(data);
                    },
                    "html"
                );
                });
                $("#show_moredetail").click(function(){
                   $("#moreDetail").show();
                });
            });
        </script>

        <style type="text/css">
            #mainContent {
                margin-right: 50px;
                margin-left: 100px;
            }
            #mainAnnounce{
                width: 100%;
            }
            #leftPanel{
                display: none;
            }
            #slogan{
                /* margin-left: -50px;
                margin-right: -50px;
                margin-top: 50px; */
                text-align: left;
                font-style: italic;
                font-weight: bold;
                font-size: 12px;
                padding-bottom:5px;
            }
        </style>
        
<!-- combrex.bu.edu -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-428391-5']);
            _gaq.push(['_setDomainName', 'none']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>


    </head>
    <body>
        <div id="head">
            <div id="links">
                <b class=""><a href="http://genomics20.bu.edu:8080/combrex/">Home</a></b>
            </div>
            <div id="users">
                <!--
                <span class="rightLink"><a href="https://spreadsheets.google.com/viewform?formkey=dEZpbERUS1ZlRFVWNXAxTXNPNzNQa1E6MA">Report Bug</a></span>
                -->
                <!--<span class="rightLink"><a href="https://docs.google.com/document/pub?id=1JK5F99Mv8oIuJMmk8IuUjD_iqRohlG_4qCM4I_lFUFk" target="_blank">FAQ</a></span>-->
                <!--<span class="rightLink"><a href="/help/navigate/navigate.htm" target="_blank">Help</a></span>-->
                <!-- <span class="rightLink"><a href="javascript:printPage();" onclick="printPage();return false;">Print</a></span> -->
                <!-- <span class="rightLink"><a href="contact.jsp">Contact Us</a></span>-->
                <div class="rightLink" id="shareBTN"><a href="#shareBTN">Share/Save this page</a></div>
                <div class="rightLink"><div id="loginWidget" style="display:inline;"></div></div>
                <div id="share_submenu">
                    <div id="share_submenu_content">
                        <p style="font-weight: bold">Share this page on...</p>
                        <p>
                            <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://combrex.bu.edu" show_faces="false" width="350" action="recommend" font=""></fb:like>
                        </p>
                        <p>
                            <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://combrex.bu.edu" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                        </p>
                        <hr />
                        <p><span style="font-weight: bold">Grab the link</span><br />
                            Here's a link to this page. Just copy and paste.
                            <input type="text" style="width:350px;" value="http://combrex.bu.edu" /><br />
                            <input type="button" onclick="add2Favorite('http://combrex.bu.edu')" value="Add to bookmark" />
                            <br />Or press Ctrl+D (Windows) or Command/Cmd + D (Mac) to bookmark this page
                        </p>
                    </div>
                </div>
            </div>
            <div style="left: 0pt; opacity: 1;" class="gbh"></div>
            <div style="right: 0pt; opacity: 1;" class="gbh"></div>
        </div>

        <div id="searchMain">
            <div id="dialog" title="dialog">
                <p>&nbsp;</p>
            </div>
            <div id="searchField">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td title="Back to Home" style=" text-decoration: none;">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td height="60px" style="padding-left:5px;">
                                                <a style="text-decoration: none;" href="index.jsp">
                                                    <img src="logo_tmp.png" width="180" height="45" style="border: 1px solid #666666" alt="COMBREX"/>
                                                </a>
                                            </td>
                                            <td style="vertical-align: bottom; padding-bottom: 5px;"><div style="font-size: 10px;margin-left: 10px;">version: 20160605.22
                                                </div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td style="text-align: right; vertical-align: bottom;">
                                <!-- <div id="slogan">...a collaborative project to improve genome annotation in which computational biologists and biochemists join forces to predict the function of genes in bacteria and archaea and test the predictions experimentally...</div> -->
                                <div style="font-size: 12px; color: silver; display:none;"   id="javascript_check">
                                    <!--Supported Browsers: Internet Explorer 7+, Firefox 2.0+<br />-->
                                    <span>To take advantage of the fully features, you'll need to enable the JavaScript on your browser.</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="functionalBar" style="border-top: 1px solid gray;">
            <ul id="functionalLink2">
    <li class="headlink"><a href="index.jsp">Home</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink">Search</li>
-->
    <li class="vbar">|</li>
    <li class="headlink" id="tab_howto"><a href="#" title="" id="fun_howto">What You Can Do...</a>
	<div id="fun_sub_howto">
            <ul>
                <!--<li><a href="instructions_for_bid_submittal.html" title="Submit a bid to functionally validate a prediction" target="_blank">Submit a grant proposal</a></li>-->
                <li><a href="guide_to_submit_predictions.html" target="_blank">Submit Prediction</a></li>
                <li><a href="guide_to_submit_annotation.html" target="_blank">Submit Annotation</a></li>
                <li><a href="nominate_gene_for_high_priority_list.html" target="_blank">Nominate a gene for High Priority List</a></li>
            </ul>
        </div>
    </li>
    <!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="genome.jsp" title="Lists of the genes within the genomes of E. coli MG1655 and H. pylori 26695">Genome</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="statstic.jsp">Statistics</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="download.jsp" title="Download the Gold Standard Dataset and the Green Gene Set">Download</a></li>
    -->
    <li class="vbar">|</li>
    <li class="headlink"><a href="help_center.jsp">Help Center</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="acknowledgments.jsp">Acknowledgments</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="http://www.combrex.org/" target="_blank">About</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="https://spreadsheets0.google.com/viewform?hl=en&formkey=dEI5VG1xNGYydVhPZGlnM0NvM3hmWWc6MQ#gid=0" target="_blank">Bug Reporting/Feature Request</a></li>
-->
    <li class="vbar">|</li>
    <li class="headlink"><a href="contact.jsp">Contact Us</a></li>
    <li class="vbar">|</li>
<!--
    <li class="headlink"><a href="announcement.pdf">Grant Announcement</a></li>
    <li class="vbar">|</li>
-->
    <li class="headlink"><a href="whatIsNew.jsp">News</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="publications.jsp">Publications</a></li>

</ul>

        </div>
        <div id="main">
            <div id="leftPanel">
                <div id="leftPanelContent">
                    <div style=" font-size: larger; font-weight: bold">Announcements</div>
                    <div>
                        <p>
                            <span style="font-weight:bold">Last Update:</span><br />
                            2013.08.26 16:15
                            <!--<a href="http://docs.google.com/View?id=dfjxffm9_1483f67c82dn">Release Note</a>-->
                        </p>
                    </div>
                </div>
            </div>
            <div id="mainContent">
                <div id="slogan" style="display:none"><!--<span style="font-weight:normal; font-size:medium">COMBREX </span> - -->"a collaborative project to improve genome annotation in which computational biologists and biochemists join forces to predict the function of genes in bacteria and archaea and test the predictions experimentally."</div>
                <div id="mainAnnounce">





                    <!-- Temp Announcement
                    <div class="announceBlock" style="background-color: #ffff00;">
                        <div class="announceHead">
                            <a name="Announcement"><br />Notice to COMBREX Users</a>
                        </div>
                        <div class="announceContent"><br />

                            Dear COMBREX users,<br/><br/>

                            Due to the scheduled server maintenance, access to the COMBREX database will not be available on Nov 12 2014. <br /><br />
between Monday, Oct 27th, 2014 and Wednesday, Oct 29th, 2014 EST.<br/><br/>
-->
<!--                            Access to COMBREX might also be unstable during the COMBREX update scheduled to occur on Friday (March 18th) morning and be completed by 12:00 pm EST.<br/><br/>-->
<!--
                            Please contact us at: <a href="mailto:help-desk@combrex.bu.edu">help-desk@combrex.bu.edu</a> if you have any questions.<br /><br />

                        </div>
                    </div>

                     End temp announcement -->

                    <div class="announceBlock">
                        <div class="announceHead">
                        </div>
                        <div class="announceContent">
                            <p>COMBREX-DB is an online repository of information related to (i) experimentally determined protein function, (ii) predicted protein function, (iii) relationships among proteins of unknown function and various types of experimental data, including molecular function, protein structure, and associated phenotypes.</p>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                    <h3><a href="publications.jsp">Link to Recent Publications</a></h3>
                        </div>
                    </div>
                    <div class="announceBlock" style="background-color: #d9ffcb;">
                        <div class="announceHead">
                            <a name="searchFeature">Search the COMBREX database </a> <span style="font-weight:normal; font-size: medium">(all fields optional; <a href="help_center.jsp?tip">Help</a>)</span>
                        </div>
                        <div class="announceContent">
                            <form action="DAI?command=SciBay&fun=search" method="post">
                                <table cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td nowrap="nowrap" align="left" valign="middle" style="width:300px; padding: 2px 0px;">
                                                <input style="width: 280px;" maxlength="200" name="keyword" id="keyword" type="text" />
                                                <!--<input type="checkbox" name="species" value="511145,85962"/><span class="dialog_tooltips" title="i.e., E. coli K12 MG1655 and H. pylori 26695">Restrict search to COMBREX focus organisms</span> (<a href="FAQ.html#faq_EColiHPyloriFocusOrganisms" target="_blank">info</a>)-->
                                            </td>
                                            <td>Enter keyword, gene symbol, or identifier <span style="font-size: smaller">(examples: <a href="javascript:searchExample('GTPAse');"id="example1" title="Gene name" >by keyword</a>, <a href="javascript:searchExample('mnmE');" title="Gene symbol">by gene symbol</a>, or <a href="javascript:searchExample('948222');" title="NCBI Entrez Gene ID">by identifier</a>)</span></td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" align="left" valign="middle" style="width:300px; padding: 2px 0px;">
                                                <input style="width:280px;" maxlength="200" name="speciesText" id="speciesText" type="text" />
                                            </td>
                                            <td>Enter species name</td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 2px 0px;">
                                                <select style="width:280px;" name="geneStatus" id="geneStatus">
                                                    <option value="all" selected="true">[All]</option>
                                                    <option value="0">Gold (experimental evidence, curated)</option>
                                                    <option value="1">Green (experimental evidence, uncurated)</option>
                                                    <option value="2">Blue (function predicted)</option>
                                                    <option value="3">Black (not validated and no prediction)</option>
                                                </select>
                                            </td>
                                            <td>Gene functional status (select)</td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 2px 0px;">
                                                <select style="width:280px;" name="phenotype" id="phenotype" selectedValue="-1">
                                                    <option value="-1">[All]</option>
                                                </select>
                                            </td>
                                            <td>Phenotype association (select) [<a href="DAI?command=SciBay&fun=phenotypes">View list of phenotypes</a>]</td>
                                            <!--
                                            <td nowrap="nowrap" width="25%" align="left">
                                                <a href="#" id="disp_advSearch" title="Limit your search results by specifying gene experimental validation status, protein cluster experimental validation status, and species name. You can also limit your search results to include only genes with high quality predictions of gene function">Advanced Search</a>
                                            </td>
                                            -->
                                        </tr>
                                    </tbody>
                                </table>
                                <br />
                                Check to restrict search to:
                                <table cellspacing="0" cellpadding="0" style="margin-left:20px;">
                                    <tbody>
                                        <!--<tr>
                                            <td>
                                                Protein Cluster Status:
                                                <select name="proteinClusterStatus" id="proteinClusterStatus">
                                                    <option value="all" selected="true">No preference</option>
                                                    <option value="1">Green (experimental evidence)</option>
                                                    <option value="2">Blue (function predicted)</option>
                                                    <option value="-1">Black (not validated and no prediction)</option>
                                                </select>
                                            </td>
                                        </tr>-->
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="isPathogens" value="1" /> Genes in Category A-C and Emerging/Re-Emerging pathogens
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="dialog_tooltips" title="When box is checked, the search is refined so that only manually curated protein clusters are included in the results. When box is unchecked, the search is expanded so that both curated and uncurated clusters are included in the results.">
                                                <input type="checkbox" name="curated" value="1"/> Genes in curated Protein Clusters 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="hasPrediction" value="1" /> Genes with predictions submitted to COMBREX <div class="letterBox">P</div> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="hasStructure" value="1" /> Genes with solved structures in PDB <div class="letterBox">S</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="hasHumanHomology" value="1" /> Genes with Pfam domains shared with human proteins <div class="letterBox">H</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="isPurified" value="1" /> Genes with products purified by PSI <div class="letterBox">U</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="isCloned" value="1" /> Genes cloned by PSI <div class="letterBox">C</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br />
                                <input name="Search" value="Search" type="submit" title="Acceptable Search Terms: NCBI gene ID, Uniprot accession number, RefSeq protein ID, Enzyme Commission (EC) number, NCBI Protein Cluster ID, gene symbol, gene name or name fragment, keywords "/>
                            </form>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                        </div>
                        <div class="announceContent">
                            <p id="show_moredetail"><a href="#details">Expanded Description of This Site</a></P>
                        </div>
                    </div>	
                    <div id="moreDetail" style="display:none" >
                    <a name="details"></a>
                    <div class="announceBlock">
                        <div class="announceHead">
                            What you can do on this site
                        </div>
                        <div class="announceContent">
                            <p>
                                <ul>
                                    <li>
                                        <b><font color="#8b0000">Join the COMBREX community.</font></b>
                                        COMBREX is a multifaceted project that aims to bring together the
                                        computational and experimental communities of biologists
                                        in the interest of improving our understanding of microbial gene function.<br/>
                                        <ul type="circle">
                                            <li><a href="help_center.jsp?intro">Learn more about the COMBREX project.</a></li>
                                            <li><a href="../combrex-user/register.jsp">Become a member.</a><br/><br/></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <b><font color="#8b0000">Apply for a grant.</font></b>
                                        <!--
                                        COMBREX offers small monetary grants for biochemists to experimentally verify
                                        predicted functions of bacterial and archaeal gene products.
                                        -->
                                        COMBREX previously offered small monetary grants for biochemists to experimentally verify predicted functions of bacterial and archaeal gene products.  We anticipate being able to offer such grants again in the near future.  In the meantime, thanks for your patience, and check back often.
                                        <ul type="circle">
                                            <!--<li><a href="notice_about_grant.jsp">Important notice about COMBREX grants</a></li>
                                            <li><a href="help_center.jsp?bid">Learn more about our grants.</a></li>
                                            <li><a href="instructions_for_bid_submittal.html#TheGrantApplicationForm">Download grant applications.</a></li>-->
                                            <li><a href="investigator.jsp">View the teams we have already funded.</a><br/><br/></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <b><font color="#8b0000">Post gene function predictions.</font></b>
                                        COMBREX serves as a forum for computational biologists to post their
                                        most informative gene function predictions.  Biochemists can then
                                        browse these predictions, and may choose to test some of them experimentally.
                                        <ul type="circle">
                                            <li><a href="help_center.jsp?predictions">Learn more about submitting predictions.</a></li>
                                            <li><a href="guide_to_submit_predictions.html#howToSubmitPredictions_submittingPredictionsOfFunctionForMultipleGenes">Upload functional predictions.</a></li>
                                            <li><a href="Prediction_Description.jsp">View the teams that have already submitted functional predictions.</a><br/><br/></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <b><font color="#8b0000">Search our database.</font></b>
                                        Our evolving database currently includes experimentally determined and computationally predicted functions for more than three million microbial genes.
                                        <ul type="circle">
                                            <li><a href="help_center.jsp?navi">Learn more about navigating the database.</a></li>
                                            <li>Start searching – our search engine is in the green box above.</li>
                                        </ul>
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <!--
                    <div class="announceBlock">
                        <div class="announceHead">
                            Top 100 genes
                        </div>
                        <div class="announceContent">
                            A list of top 100 gene predictions in Combrex project. <a href="top100.jsp">list</a>
                        </div>
                    </div>
                    -->


                    <div class="announceBlock">
                        <div class="announceHead">
                            The COMBREX High Priority Gene List
                        </div>
                        <div class="announceContent">
                            A list of 100 microbial genes, with predicted functions, most in need of experimental validation. <a href="DAI?command=SciBay&fun=top100List">Show List</a>
                        </div>
                    </div>
                    <div class="announceBlock">
                        <div class="announceHead">
                            Successful COMBREX projects
                        </div>
                        <div class="announceContent">
                            Lists of genes and projects carried out using COMBREX funding. <a href="DAI?command=SciBay&fun=combrexExpResult">List by gene product.</a> <a href="investigator.jsp">List by investigator.</a>
                        </div>
                    </div>

                    <div class="announceBlock">
                        <div class="announceHead">
                            Prediction and validation status for two model organisms and for all genes in COMBREX
                        </div>
                        <div class="announceContent dialog_tooltips" title="Each bar represents the number of known genes in E. coli K12 MG1655 or H. pylori 26695 (the COMBREX focus organisms). Each bar is divided into four colored sections that categorize a gene's experimental validation status as follows: Gold - A gene that has been experimentally validated, and the DNA sequence coding for the exact protein has been determined; publication(s) reporting the sequence and the biochemistry are documented. Green - A gene that either (1) has been experimentally validated, but manual curation is incomplete or information required for gold status is lacking, or (2) has >98% sequence identity to a gold gene across the entire length of the gene. Blue - A gene that has a specific prediction of molecular function, but has not been experimentally validated, or has an undetermined validation status. Black - A gene that does not have a specific prediction of molecular function but it may have predictions of a 'general\" or \"non-specific\" nature. Numbers within each colored section of the bar indicate the number of genes within that category.">
                            <p>
                                Gold and green sections indicate the number of experimentally validated genes; blue sections indicate the number of unvalidated genes with predicted functions; black sections indicate the number of genes with no predicted functions.  See the FAQ for more information.
                            </p>
                            <blockquote>
                                <div class="progressThermometer" taxId="511145"></div>
                                <br/><br/>
                                <div class="progressThermometer" taxId="85962"></div>
                                <br/><br/>
                                <div class="progressThermometer" taxId="-1"></div>
                            </blockquote>
                        </div>
                        <!-- <div class="announceHead">
                            Prediction and validation status for all genes in COMBREX
                        </div>
                        <div class="announceContent">
                            <blockquote>
                                <div class="progressThermometer" taxId="-1"></div>
                            </blockquote>
                        </div>          -->
                    </div>
                    <div class="announceBlock" style="background-color: #ffff55;">
                        <div class="announceHead">
                        </div>
                        <div class="announceContent">
                                <br />The good news is that COMBREX has successfully funded many grants to biochemists in its initial phase.  The bad news is that our initial allocation of grant money has now been completely utilized.  Regrettably, we must suspend funding of further grants until our own funding is renewed.  We anticipate this is a temporary situation, as we are sending renewal applications to NIH and continue our strong interest in funding additional COMBREX grants in the near future. <br /><br />
In the meantime, we encourage interested laboratories to send us an <a href="mailto:it-help@combrex.bu.edu ">email</a> with a short (one or two paragraph) description of your project and this information will be highly helpful in our renewal application.  Your full application would be placed at the top of our application stack upon renewal.  We also continue to seek submission of predictions from computational prediction teams.<br /><br />
Many thanks for your continued support. <br /><br />
<div style="text-align:right">-- THE COMBREX DEVELOPMENT TEAM.</div>
                        <br />
                        </div>
                    </div>
                    </div>
                </div>
                <div id="aside" style="display: none;">
                    <div id="quickAnnounce">
                        <div id="quickAnnounceInner">
                            <div>
                                <div>Announce</div>
                                <div>
                                    <p>
                                        Last Update: 2010.03.10 09:50
                                        <a href="http://docs.google.com/View?id=dfjxffm9_1483f67c82dn">Release Note</a>
                                    </p>
                                    <p>Update History
                                        <ul>
                                            <li>2010.03.09 11:09</li>
                                            <li>2010.03.05 17:22</li>
                                            <li>2010.02.25 11:35</li>
                                            <li>2010.02.23 17:39</li>
                                            <li>2010.02.22 23.05</li>
                                            <li>2010.02.22 00:38</li>
                                            <li>2010.02.18 18:35 </li>
                                            <li>2010.02.18 16:35 </li>
                                            <li>First Release: 2010.02.03</li>

                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <ul>
                            <li>Number of Species: 2</li>
                            <li>Number of Genes: 3031</li>
                            <li>Number of Protein Cluster: 2703</li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
        <div id="foot" style=" text-align: center; margin-top: 30px;">
            <!--
            <div style=" font-size: smaller"><a href="contact.jsp">Contact Us</a> | <a href="http://www.bu.edu/bioinformatics/" target="_blank">Bioinformatics Program, Boston University</a></div>
-->
        </div>

    </body></html>
