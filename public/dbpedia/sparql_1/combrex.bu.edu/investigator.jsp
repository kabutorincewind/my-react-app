
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>COMBREX: Investigators Funded</title>
        <link rel="stylesheet" type="text/css" href="common.css" />
        <link type="text/css" rel="stylesheet" href="jquery/css/smoothness/jquery-ui-1.8.6.custom.css"></link>
        <script type="text/javascript" src="jquery/js/jquery.js"></script>
        <script type="text/javascript" src="jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="utility.js"></script>
        <script type="text/javascript" src="common.js"></script>
        <style type="text/css">
            #mainContent{
                /*overflow:inherit;*/
                margin-left: 10px;
                clear: left;
                margin-top: 20px;
                width: 95%;
                height: 100%;
            }
            .geneName{
                width: 350px;
            }
            .genePredFun{
                width: 350px;
                overflow: hidden;
                white-space: nowrap;
                padding: 5px 10px 5px 10px;
            }
	   #invest thead tr td{
		vertical-align: middle;
		text-align: center;
	   }
	  #invest tbody tr td{
		vertical-align: middle;
	   }
	  #invest tbody{
		text-align:center;
	   }
	  #invest .left{
		padding-left: 8px;
		text-align:left;
	  }
        </style>
        
<!-- combrex.bu.edu -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-428391-5']);
            _gaq.push(['_setDomainName', 'none']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>


    </head>
    <body>
        


<div id="head">
    <div id="links">
        <b class=""><a href="./">Home</a></b>
    </div>
    <div id="users">
        <div class="rightLink" id="shareBTN"><a href="#shareBTN">Share/Save this page</a></div>
        <div class="rightLink"><div id="loginWidget" style="display:inline;"></div></div>
        <div id="share_submenu">
            <div id="share_submenu_content">
                <p style="font-weight: bold">Share this page on...</p>
                <p>
                    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=investigator.jsp' show_faces="false" width="350" action="recommend" font=""></fb:like>
                </p>
                <p>
                    <a href="http://twitter.com/share" class="twitter-share-button" data-url='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=investigator.jsp' data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                </p>
                <hr />
                <p><span style="font-weight: bold">Grab the link</span><br />
                    Here's a link to this page. Just copy and paste.
                    <input type="text" style="width:350px;" value='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=investigator.jsp' /><br />
                    <input type="button" onclick="add2Favorite('http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=investigator.jsp','Combrex: Investigators Funded')" value="Add to bookmark" />
                    <br />Or press Ctrl+D (Windows) or Command/Cmd + D (Mac) to bookmark this page
                </p>
            </div>
        </div>
    </div>
    <div style="left: 0pt; opacity: 1;" class="gbh"></div>
    <div style="right: 0pt; opacity: 1;" class="gbh"></div>
</div>
<script type="text/javascript" src="jquery/plugins/autocomplete-mod/jquery.autocomplete.js" />
<script type="text/javascript">
</script>

<div id="searchMain" style=" ">&nbsp;
    <form action="DAI?command=SciBay&fun=search" method="post">
        <table cellpadding="0" cellspacing="0" id="basicSearch" style=" ">
            <tbody>
                <tr>
                    <td id="smallLego" title="Back to Home">
                        <a style="text-decoration: none;" href="index.jsp"><img src="logo_tmp.png" width="200" height="45" style="border: 1px solid #666666" alt="Combrex"/></a>
                    </td>
                    <td class="firstInp" nowrap="nowrap" align="left" valign="middle" style="width:300px;">
                        <input style="width:280px;" maxlength="200" name="keyword" id="keyword" type="text" value="" />
                    </td>
                    <td class="firstInp" >
                        Enter Keyword, gene symbol, or identifier <input name="Search" id="Search" value="Search" type="submit" />                            (<span id="moreOptions"><a href="#" id="disp_advSearch" title="Limit your search results by specifying gene experimental validation status, protein cluster experimental validation status, and species name. You can also limit your search results to include only genes with high quality predictions of gene function">more options</a>; </span>
                        <a href="help_center.jsp?tip" target="_bnalk">Help</a> )
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="advSearch" cellspacing="0" cellpadding="0" >
            <tbody>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px; padding: 2px 0px;">
                        <input style="width:280px;" maxlength="200" name="speciesText" id="speciesText" type="text" value=""  />
                    </td>
                    <td>Enter species name</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 2px 0px;">
                        <select style="width:280px;" name="geneStatus" id="geneStatus">
                            <option value="all" selected="true">[All]</option>
                            <option value="0" >Gold (experimental evidence, curated)</option>
                            <option value="1" >Green (experimental evidence, uncurated)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="3" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                    <td>Gene functional status (select)</td>
                </tr>
                <!--
                <tr>
                    <td>
                        Protein Cluster Status:
                        <select name="proteinClusterStatus" id="proteinClusterStatus">
                            <option value="all"  selected="true">No preference</option>
                            <option value="1" >Green (experimental evidence)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="-1" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                </tr>
                -->
                <tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 5px 0px;">
                        <select style="width:280px;" name="phenotype" id="phenotype" selectedValue="">
                            <option value="-1">[All]</option>
                        </select>
                    </td>
                    <td>Phenotype association (select)[<a href="DAI?command=SciBay&fun=phenotypes">View list of phenotypes</a>]</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Check to restrict search to:
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPathogens" value="1"  /> Genes in Category A-C and Emerging/Re-Emerging pathogens
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="curated" value="1"  /> Genes in curated Protein Clusters
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px">
                        <input type="checkbox" name="hasPrediction" value="1" /> Genes with predictions submitted to COMBREX <div class="letterBox">P</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasStructure" value="1"  /> Genes with solved structures in PDB <div class="letterBox">S</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasHumanHomology" value="1"  /> Genes with Pfam domains shared with human proteins <div class="letterBox">H</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPurified" value="1"  /> Genes with products purified by PSI <div class="letterBox">U</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isCloned" value="1"  /> Genes cloned by PSI <div class="letterBox">C</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height:50px">
                        <input name="Search2" id="Search2" value="Search" type="submit" title="Acceptable Search Terms: NCBI gene ID, Uniprot accession number, RefSeq protein ID, Enzyme Commission (EC) number, NCBI Protein Cluster ID, gene symbol, gene name or name fragment, keywords "/>
                        <a href="#" id="close_advSearch" >close options</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

</div>

        <div id="functionalBar">
            <ul id="functionalLink2">
    <li class="headlink"><a href="index.jsp">Home</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink">Search</li>
-->
    <li class="vbar">|</li>
    <li class="headlink" id="tab_howto"><a href="#" title="" id="fun_howto">What You Can Do...</a>
	<div id="fun_sub_howto">
            <ul>
                <!--<li><a href="instructions_for_bid_submittal.html" title="Submit a bid to functionally validate a prediction" target="_blank">Submit a grant proposal</a></li>-->
                <li><a href="guide_to_submit_predictions.html" target="_blank">Submit Prediction</a></li>
                <li><a href="guide_to_submit_annotation.html" target="_blank">Submit Annotation</a></li>
                <li><a href="nominate_gene_for_high_priority_list.html" target="_blank">Nominate a gene for High Priority List</a></li>
            </ul>
        </div>
    </li>
    <!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="genome.jsp" title="Lists of the genes within the genomes of E. coli MG1655 and H. pylori 26695">Genome</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="statstic.jsp">Statistics</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="download.jsp" title="Download the Gold Standard Dataset and the Green Gene Set">Download</a></li>
    -->
    <li class="vbar">|</li>
    <li class="headlink"><a href="help_center.jsp">Help Center</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="acknowledgments.jsp">Acknowledgments</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="http://www.combrex.org/" target="_blank">About</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="https://spreadsheets0.google.com/viewform?hl=en&formkey=dEI5VG1xNGYydVhPZGlnM0NvM3hmWWc6MQ#gid=0" target="_blank">Bug Reporting/Feature Request</a></li>
-->
    <li class="vbar">|</li>
    <li class="headlink"><a href="contact.jsp">Contact Us</a></li>
    <li class="vbar">|</li>
<!--
    <li class="headlink"><a href="announcement.pdf">Grant Announcement</a></li>
    <li class="vbar">|</li>
-->
    <li class="headlink"><a href="whatIsNew.jsp">News</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="publications.jsp">Publications</a></li>

</ul>

        </div>
        <div id="main">
            <div id="mainContent">
                <div id="headLine">
                    <h2>Investigators Funded</h2>
                </div>
                <div id="jumpDownList" style="display:none">
                    <p>
                        Jump to section on this page:
                        <span class="jupdDownLink"><a href="#grants">Investigators Funded</a></span>
                    </p>
                </div>
                <div id="sections">

                    <!-- Funding -->
                    <div class="section">
                        <p class="utilLink" style="display:none">
                            <a href="#" onclick="hideOrShowSection('sec_grants',this);return false;">Hide</a>
                            |
                            <a href="#">Top</a>
                        </p>
                        <div class="sectionHead" title="">
                            <a id="grants">Investigators Funded</a>
                        </div>
                        <div class="sectionContent" id="sec_grants">
                            <table id="invest" class="sectionTable" cellpadding="0" cellspacing="0" >
				<thead>
					<tr>
						<td>Project</td>
						<td>Investigator</td>
						<td>Institution</td>
						<td>Title</td>
						<td>Predictions Tested</td>
						<td>Predictions Validated</td>
						<td>Predictions Not Supported</td>
						<td>In Progress</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td class="left">Linda Columbus</td>
						<td class="left">University of Virginia</td>
						<td class="left">Functions of PSI-crystallized proteins from <span class="speciesName">Thermotoga maritima</span></td>
						<td>3</td>
						<td>3</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>2</td>
						<td class="left">Alexander Yakunin</td>
						<td class="left">University of Toronto</td>
						<td class="left">Maf phosphohydrolase proteins</td>
						<td>6</td>
						<td>2</td>
						<td>0</td>
						<td>4</td>
					</tr>
					<tr>
						<td>3</td>
						<td class="left">Dieter Söll</td>
						<td class="left">Yale University</td>
						<td class="left">2-selenouridine formation in <span class="speciesName">Methanococcus maripaludis</span></td>
						<td>2</td>
						<td>2</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>4</td>
						<td class="left">Valérie de Crécy-Lagard</td>
						<td class="left">University of Florida</td>
						<td class="left">Formation of the archaeosine base in tRNA</td>
						<td>4</td>
						<td>3</td>
						<td>1</td>
						<td>0</td>
					</tr>
					<tr>
						<td>5</td>
						<td class="left">Aaron Setterdahl</td>
						<td class="left">Indiana University Southeast</td>
						<td class="left">Glutathionylspermidine synthase</td>
						<td>1</td>
						<td>0</td>
						<td>0</td>
						<td>1</td>
					</tr>
					<tr>
						<td>6</td>
						<td class="left">Andrei L. Osterman</td>
						<td class="left">Sanford Burnham Medical Research Institute</td>
						<td class="left">Tagaturonate/fructuronate epimerase</td>
						<td>1</td>
						<td>1</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td rowspan="2">7</td>
						<td class="left">James Spain</td>
						<td class="left">Georgia Institute of Technology</td>
						<td rowspan="2" class="left">2-Nitropropane Dioxygenases</td>
						<td rowspan="2">11</td>
						<td rowspan="2">0</td>
						<td rowspan="2">0</td>
						<td rowspan="2">11</td>
					</tr>
					<tr>
						<td class="left">Giovanni Gadda</td>
						<td class="left">Georgia State University</td>
					</tr>
					<tr>
						<td>8</td>
						<td class="left">Ashok Bhagwat</td>
						<td class="left">Wayne State University</td>
						<td class="left">T•G specific glycosylases and endonucleases</td>
						<td>5</td>
						<td>0</td>
						<td>5</td>
						<td>0</td>
					</tr>
					<tr>
						<td>9</td>
						<td class="left">Steven Brenner</td>
						<td class="left">University of California Berkeley</td>
						<td class="left">NUDIX hydrolase</td>
						<td>1</td>
						<td>0</td>
						<td>1</td>
						<td>0</td>
					</tr>
					<tr>
						<td>10</td>
						<td class="left">Donald Ferguson</td>
						<td class="left">Miami University of Ohio</td>
						<td class="left">Trimethylamine (TMA):corrinoid methyltransferase</td>
						<td>1</td>
						<td>0</td>
						<td>0</td>
						<td>1</td>
					</tr>
					<tr>
						<td>11</td>
						<td class="left">Kenneth E. Rudd</td>
						<td class="left">University of Miami</td>
						<td class="left">Periplasmic proteins in <span class="speciesName">Escherichia coli</span></td>
						<td>29</td>
						<td>0</td>
						<td>0</td>
						<td>29</td>
					</tr>
					<tr>
						<td>12</td>
						<td class="left">Robert M. Blumenthal</td>
						<td class="left">University of Toledo</td>
						<td class="left">Transcriptional regulators in <span class="speciesName">Escherichia coli</span> and <span class="speciesName">Helicobacter pylori</span></td>
						<td>2</td>
						<td>0</td>
						<td>0</td>
						<td>2</td>
					</tr>
					<tr>
						<td rowspan="2">13</td>
						<td class="left" style="border-bottom-width: 0px;">J. Martin Bollinger</td>
						<td rowspan="2" class="left">Penn State University</td>
						<td rowspan="2" class="left">Ribonucleotide reductase</td>
						<td rowspan="2">2</td>
						<td rowspan="2">0</td>
						<td rowspan="2">0</td>
						<td rowspan="2">2</td>
					</tr>
					<tr>
						<td class="left" style="border-top-width: 0px;">Carsten Krebs</td>
					</tr>
					<tr>
						<td>14</td>
						<td class="left">Woo-Suk Chang</td>
						<td class="left">University of Texas Arlington</td>
						<td class="left">Isocitrate lyase</td>
						<td>3</td>
						<td>1</td>
						<td>0</td>
						<td>2</td>
					</tr>
					<tr>
						<td rowspan="2">15</td>
						<td class="left" style="border-bottom-width: 0px;">Richard D. Morgan</td>
						<td rowspan="2" class="left">New England Biolabs	</td>
						<td rowspan="2" class="left">Restriction modification systems in bacteria</td>
						<td rowspan="2">55</td>
						<td rowspan="2">32</td>
						<td rowspan="2">17</td>
						<td rowspan="2">6</td>
					</tr>
					<tr>
						<td class="left" style="border-top-width: 0px;">Richard J. Roberts</td>
					</tr>
					<tr>
						<td rowspan="2">16</td>
						<td class="left">Manuel Ferrer</td>
						<td class="left">Institute of Catalysis CSIC, Madrid, Spain</td>
						</td>
						<td rowspan="2" class="left">High throughput characterization of hypothetical proteins in <span class="speciesName">Helicobacter pylori</span></td>
						<td rowspan="2">14</td>
						<td rowspan="2">6</td>
						<td rowspan="2">2</td>
						<td rowspan="2">6</td>
					</tr>
					<tr>
						<td class="left">Martin Steffen</td>
						<td class="left">Boston University</td>
					</tr>
				</tbody>
			    </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="foot">
            <!--
            <div style=" font-size: smaller"><a href="contact.jsp">Contact Us</a> | <a href="http://www.bu.edu/bioinformatics/" target="_blank">Bioinformatics Program, Boston University</a></div>
-->
        </div>
    </body>
</html>

