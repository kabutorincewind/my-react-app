<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
<!-- combrex.bu.edu -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-428391-5']);
            _gaq.push(['_setDomainName', 'none']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>


    </head>
    <body>

        <span id="visantspan" style="visibility: hidden;">
            <!--
            <applet name="visant" id="visant" archive="VisAnt.jar" code="cagt.bu.visant.VisAntApplet.class" height="1" width="1" align="right">
                <param name=DAIurl value="http://combrex.bu.edu/VisANTProxy?" />
            -->
                <!-- for local

                <param name=DAIurl value="http://localhost:8084/SciBay_Dev/VisANTProxy?" />
                -->
                <!--
                <param name="KGML" value="http://www.genome.jp/kegg/KGML/KGML_v0.6.1/" />
                <param name="GI" value="NCBI Genebank=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=Protein&amp;dopt=GenPept&amp;list_uids="/>
                <param name="SGD ID" value="SGD Link=http://db.yeastgenome.org/cgi-bin/SGD/locus.pl?locus=" />
                <param name="SP" value="Swiss-Prot NiceProt View=http://us.expasy.org/cgi-bin/niceprot.pl?" />
                <param name="loc" value="Entrez Gene=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=gene&amp;cmd=Retrieve&amp;dopt=Graphics&amp;list_uids=" />
                <param name="mim" value="OMIM=http://www.ncbi.nlm.nih.gov/entrez/dispomim.cgi?id=" />
                <param name="ref_s" value="RefSeq=http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?val=" />
                <param name="gcard" value="GeneCard=http://bioinfo.weizmann.ac.il/cards-bin/carddisp?" />
                <param name="gtest" value="GeneClinics/GeneTests=http://www.genetests.org/query?gene=" />
                <param name="O_N_hsa" value="HUGO=http://www.genenames.org/data/hgnc_data.php?match=" />
                <param name="wormbase" value="WormBase=http://www.wormbase.org/db/gene/gene?name=" />
                <param name="fbgn" value="Flybase Report=http://flybase.net/.bin/fbidq.html?" />
                <param name="rgd" value="Rat Genome Database=http://rgd.mcw.edu/tools/genes/genes_view.cgi?id=" />
                <param name="ecocyc" value="EcoCyc=http://biocyc.org/ECOLI/NEW-IMAGE?type=NIL&amp;object=" />
                <param name="ratmap" value="RatMap=http://ratmap.gen.gu.se/ShowSingleLocus.htm?accno=" />
                <param name="pubmed" value="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=pubmed&amp;dopt=Abstract&amp;list_uids=" />
                <param name="target_frame" value="main" />
                <param name="Link1" value="MenuName=HugeIndex Home Link=http://www.HugeIndex.org" />
                <param name="Link2" value="MenuName=NCBI Home Link=http://www.ncbi.nlm.nih.gov" />
                -->
            </applet>
        </span>

    </body>
</html>
