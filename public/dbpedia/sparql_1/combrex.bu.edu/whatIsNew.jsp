

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>News</title>
        <link rel="stylesheet" type="text/css" href="common.css" />
        <link type="text/css" rel="stylesheet" href="jquery/css/smoothness/jquery-ui-1.8.6.custom.css"></link>
        <script type="text/javascript" src="jquery/js/jquery.js"></script>
        <script type="text/javascript" src="jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="utility.js"></script>
        <script type="text/javascript" src="common.js"></script>
        <style type="text/css">

            #mainContent{
                margin-right: 100px;
                margin-left: 20px;
                /*float: right;*/
            }
            #mainAnnounce{
                width: 90%
            }

            .geneName{
                width: 350px;
            }
            .genePredFun{
                width: 350px;
                overflow: hidden;
                white-space: nowrap;
                padding: 5px 10px 5px 10px;
            }
        </style>
        
<!-- combrex.bu.edu -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-428391-5']);
            _gaq.push(['_setDomainName', 'none']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>


    </head>
    <body>
        


<div id="head">
    <div id="links">
        <b class=""><a href="./">Home</a></b>
    </div>
    <div id="users">
        <div class="rightLink" id="shareBTN"><a href="#shareBTN">Share/Save this page</a></div>
        <div class="rightLink"><div id="loginWidget" style="display:inline;"></div></div>
        <div id="share_submenu">
            <div id="share_submenu_content">
                <p style="font-weight: bold">Share this page on...</p>
                <p>
                    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=whatIsNew.jsp' show_faces="false" width="350" action="recommend" font=""></fb:like>
                </p>
                <p>
                    <a href="http://twitter.com/share" class="twitter-share-button" data-url='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=whatIsNew.jsp' data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                </p>
                <hr />
                <p><span style="font-weight: bold">Grab the link</span><br />
                    Here's a link to this page. Just copy and paste.
                    <input type="text" style="width:350px;" value='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=whatIsNew.jsp' /><br />
                    <input type="button" onclick="add2Favorite('http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=whatIsNew.jsp','Combrex: News')" value="Add to bookmark" />
                    <br />Or press Ctrl+D (Windows) or Command/Cmd + D (Mac) to bookmark this page
                </p>
            </div>
        </div>
    </div>
    <div style="left: 0pt; opacity: 1;" class="gbh"></div>
    <div style="right: 0pt; opacity: 1;" class="gbh"></div>
</div>
<script type="text/javascript" src="jquery/plugins/autocomplete-mod/jquery.autocomplete.js" />
<script type="text/javascript">
</script>

<div id="searchMain" style=" ">&nbsp;
    <form action="DAI?command=SciBay&fun=search" method="post">
        <table cellpadding="0" cellspacing="0" id="basicSearch" style=" ">
            <tbody>
                <tr>
                    <td id="smallLego" title="Back to Home">
                        <a style="text-decoration: none;" href="index.jsp"><img src="logo_tmp.png" width="200" height="45" style="border: 1px solid #666666" alt="Combrex"/></a>
                    </td>
                    <td class="firstInp" nowrap="nowrap" align="left" valign="middle" style="width:300px;">
                        <input style="width:280px;" maxlength="200" name="keyword" id="keyword" type="text" value="" />
                    </td>
                    <td class="firstInp" >
                        Enter Keyword, gene symbol, or identifier <input name="Search" id="Search" value="Search" type="submit" />                            (<span id="moreOptions"><a href="#" id="disp_advSearch" title="Limit your search results by specifying gene experimental validation status, protein cluster experimental validation status, and species name. You can also limit your search results to include only genes with high quality predictions of gene function">more options</a>; </span>
                        <a href="help_center.jsp?tip" target="_bnalk">Help</a> )
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="advSearch" cellspacing="0" cellpadding="0" >
            <tbody>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px; padding: 2px 0px;">
                        <input style="width:280px;" maxlength="200" name="speciesText" id="speciesText" type="text" value=""  />
                    </td>
                    <td>Enter species name</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 2px 0px;">
                        <select style="width:280px;" name="geneStatus" id="geneStatus">
                            <option value="all" selected="true">[All]</option>
                            <option value="0" >Gold (experimental evidence, curated)</option>
                            <option value="1" >Green (experimental evidence, uncurated)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="3" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                    <td>Gene functional status (select)</td>
                </tr>
                <!--
                <tr>
                    <td>
                        Protein Cluster Status:
                        <select name="proteinClusterStatus" id="proteinClusterStatus">
                            <option value="all"  selected="true">No preference</option>
                            <option value="1" >Green (experimental evidence)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="-1" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                </tr>
                -->
                <tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 5px 0px;">
                        <select style="width:280px;" name="phenotype" id="phenotype" selectedValue="">
                            <option value="-1">[All]</option>
                        </select>
                    </td>
                    <td>Phenotype association (select)[<a href="DAI?command=SciBay&fun=phenotypes">View list of phenotypes</a>]</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Check to restrict search to:
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPathogens" value="1"  /> Genes in Category A-C and Emerging/Re-Emerging pathogens
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="curated" value="1"  /> Genes in curated Protein Clusters
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px">
                        <input type="checkbox" name="hasPrediction" value="1" /> Genes with predictions submitted to COMBREX <div class="letterBox">P</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasStructure" value="1"  /> Genes with solved structures in PDB <div class="letterBox">S</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasHumanHomology" value="1"  /> Genes with Pfam domains shared with human proteins <div class="letterBox">H</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPurified" value="1"  /> Genes with products purified by PSI <div class="letterBox">U</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isCloned" value="1"  /> Genes cloned by PSI <div class="letterBox">C</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height:50px">
                        <input name="Search2" id="Search2" value="Search" type="submit" title="Acceptable Search Terms: NCBI gene ID, Uniprot accession number, RefSeq protein ID, Enzyme Commission (EC) number, NCBI Protein Cluster ID, gene symbol, gene name or name fragment, keywords "/>
                        <a href="#" id="close_advSearch" >close options</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

</div>

        <div id="functionalBar">
            <ul id="functionalLink2">
    <li class="headlink"><a href="index.jsp">Home</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink">Search</li>
-->
    <li class="vbar">|</li>
    <li class="headlink" id="tab_howto"><a href="#" title="" id="fun_howto">What You Can Do...</a>
	<div id="fun_sub_howto">
            <ul>
                <!--<li><a href="instructions_for_bid_submittal.html" title="Submit a bid to functionally validate a prediction" target="_blank">Submit a grant proposal</a></li>-->
                <li><a href="guide_to_submit_predictions.html" target="_blank">Submit Prediction</a></li>
                <li><a href="guide_to_submit_annotation.html" target="_blank">Submit Annotation</a></li>
                <li><a href="nominate_gene_for_high_priority_list.html" target="_blank">Nominate a gene for High Priority List</a></li>
            </ul>
        </div>
    </li>
    <!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="genome.jsp" title="Lists of the genes within the genomes of E. coli MG1655 and H. pylori 26695">Genome</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="statstic.jsp">Statistics</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="download.jsp" title="Download the Gold Standard Dataset and the Green Gene Set">Download</a></li>
    -->
    <li class="vbar">|</li>
    <li class="headlink"><a href="help_center.jsp">Help Center</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="acknowledgments.jsp">Acknowledgments</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="http://www.combrex.org/" target="_blank">About</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="https://spreadsheets0.google.com/viewform?hl=en&formkey=dEI5VG1xNGYydVhPZGlnM0NvM3hmWWc6MQ#gid=0" target="_blank">Bug Reporting/Feature Request</a></li>
-->
    <li class="vbar">|</li>
    <li class="headlink"><a href="contact.jsp">Contact Us</a></li>
    <li class="vbar">|</li>
<!--
    <li class="headlink"><a href="announcement.pdf">Grant Announcement</a></li>
    <li class="vbar">|</li>
-->
    <li class="headlink"><a href="whatIsNew.jsp">News</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="publications.jsp">Publications</a></li>

</ul>

        </div>
        <div id="main">

            <h2>New features and content added to COMBREX</h2>
            <p>
                Below are brief descriptions of some of the new features and content that
                have been added to COMBREX with the May 6th 2011 update.
            </p>

            <h3><u>Documented phenotype data associated with genes</u></h3>
            <p>
                COMBREX now contains a “Phenotype data” section within gene detail pages which contains
                information about documented phenotypes associated with a gene of interest. Specifically,
                phenotype name, a brief description of the phenotype, expression class
                (e.g. wild type, knockout, etc.), and a link to the reference which documents the
                association of the gene with the listed phenotype are listed in this section.
                Currently, this phenotype data consists of antibiotic resistance, antibiotic hypersensitivity,
                and gene essentiality. Please refer to the
                <a href="FAQ.html#faq_phenotypeDataInformation">FAQ</a> section on Phenotype data.
            </p>
            <p>
                Users can search for clusters containing genes associated with a specific phenotype using
                the options available in advanced search. Users can also view a list of all the phenotypes
                available in COMBREX by following the “View list of phenotypes” link within the advanced search options.
            </p>
            <h3><u>Identification of proteins that have been cloned and/or purified for experimentation</u></h3>
            <p>
                COMBREX now contains information about proteins that have been cloned and proteins that have been purified
                by a participant in the Protein Structure Initiative (PSI). The cloning and purification of these proteins
                are two steps in a series of experimental steps taken to determine their structures using NMR
                or X-ray crystallography.   <br/><br/>
                Clusters containing proteins that been cloned and/or purified and genes encoding proteins
                that have been cloned and/or purified are marked with the <div class="letterBox">C</div> 
                and <div class="letterBox">U</div>
                symbols, respectively. Please refer to the <a href="help_center.jsp?glossary">Glossary of terms and icons</a> document in the help center.
                In addition users can also search specifically for clusters containing genes encoding cloned and/or purified
                proteins using the newly available options in advanced search.
            </p>

            <h3><u>NCBI Supercluster information</u></h3>
            <p>
                COMBREX now provides information on NCBI Superclusters. NCBI superclusters are a
                clique of NCBI related clusters. More info on related clusters can be found in the
                <a href="FAQ.html#faq_WhatAreRelatedProteinClusters?">FAQ</a>.<br/><br/>

                Users can access supercluster detail pages through cluster detail pages. For clusters within
                superclusters, i.e. those that have related clusters, there are links to the corresponding
                supercluster detail pages under the “summary” section in the cluster detail pages.
            </p>
            <h3><u>Downloading content from COMBREX</u></h3>
            <p>
                Users can now download content from COMBREX in Excel format. Currently, downloadable
                content include search results, a list of genes comprising a cluster, a list of clusters
                comprising a supercluster, a list of annotations of molecular function within a supercluster,
                and a list of genes associated with a certain phenotype.
            </p>


        </div>
        <div id="foot">
            <!--
            <div style=" font-size: smaller"><a href="contact.jsp">Contact Us</a> | <a href="http://www.bu.edu/bioinformatics/" target="_blank">Bioinformatics Program, Boston University</a></div>
-->
        </div>

    </body>
</html>
