

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Publications</title>
        <link rel="stylesheet" type="text/css" href="common.css" />
        <link type="text/css" rel="stylesheet" href="jquery/css/smoothness/jquery-ui-1.8.6.custom.css"></link>
        <script type="text/javascript" src="jquery/js/jquery.js"></script>
        <script type="text/javascript" src="jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="utility.js"></script>
        <script type="text/javascript" src="common.js"></script>
        <style type="text/css">

            #mainContent{
                margin-right: 100px;
                margin-left: 20px;
                /*float: right;*/
            }
            #mainAnnounce{
                width: 90%
            }

            .geneName{
                width: 350px;
            }
            .genePredFun{
                width: 350px;
                overflow: hidden;
                white-space: nowrap;
                padding: 5px 10px 5px 10px;
            }
        </style>
        
<!-- combrex.bu.edu -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-428391-5']);
            _gaq.push(['_setDomainName', 'none']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>


    </head>
    <body>
        


<div id="head">
    <div id="links">
        <b class=""><a href="./">Home</a></b>
    </div>
    <div id="users">
        <div class="rightLink" id="shareBTN"><a href="#shareBTN">Share/Save this page</a></div>
        <div class="rightLink"><div id="loginWidget" style="display:inline;"></div></div>
        <div id="share_submenu">
            <div id="share_submenu_content">
                <p style="font-weight: bold">Share this page on...</p>
                <p>
                    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=publications.jsp' show_faces="false" width="350" action="recommend" font=""></fb:like>
                </p>
                <p>
                    <a href="http://twitter.com/share" class="twitter-share-button" data-url='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=publications.jsp' data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                </p>
                <hr />
                <p><span style="font-weight: bold">Grab the link</span><br />
                    Here's a link to this page. Just copy and paste.
                    <input type="text" style="width:350px;" value='http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=publications.jsp' /><br />
                    <input type="button" onclick="add2Favorite('http://combrex.bu.edu/DAI?command=SciBay&fun=scibayLink&jsppage=publications.jsp','Combrex: Publications')" value="Add to bookmark" />
                    <br />Or press Ctrl+D (Windows) or Command/Cmd + D (Mac) to bookmark this page
                </p>
            </div>
        </div>
    </div>
    <div style="left: 0pt; opacity: 1;" class="gbh"></div>
    <div style="right: 0pt; opacity: 1;" class="gbh"></div>
</div>
<script type="text/javascript" src="jquery/plugins/autocomplete-mod/jquery.autocomplete.js" />
<script type="text/javascript">
</script>

<div id="searchMain" style=" ">&nbsp;
    <form action="DAI?command=SciBay&fun=search" method="post">
        <table cellpadding="0" cellspacing="0" id="basicSearch" style=" ">
            <tbody>
                <tr>
                    <td id="smallLego" title="Back to Home">
                        <a style="text-decoration: none;" href="index.jsp"><img src="logo_tmp.png" width="200" height="45" style="border: 1px solid #666666" alt="Combrex"/></a>
                    </td>
                    <td class="firstInp" nowrap="nowrap" align="left" valign="middle" style="width:300px;">
                        <input style="width:280px;" maxlength="200" name="keyword" id="keyword" type="text" value="" />
                    </td>
                    <td class="firstInp" >
                        Enter Keyword, gene symbol, or identifier <input name="Search" id="Search" value="Search" type="submit" />                            (<span id="moreOptions"><a href="#" id="disp_advSearch" title="Limit your search results by specifying gene experimental validation status, protein cluster experimental validation status, and species name. You can also limit your search results to include only genes with high quality predictions of gene function">more options</a>; </span>
                        <a href="help_center.jsp?tip" target="_bnalk">Help</a> )
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="advSearch" cellspacing="0" cellpadding="0" >
            <tbody>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px; padding: 2px 0px;">
                        <input style="width:280px;" maxlength="200" name="speciesText" id="speciesText" type="text" value=""  />
                    </td>
                    <td>Enter species name</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 2px 0px;">
                        <select style="width:280px;" name="geneStatus" id="geneStatus">
                            <option value="all" selected="true">[All]</option>
                            <option value="0" >Gold (experimental evidence, curated)</option>
                            <option value="1" >Green (experimental evidence, uncurated)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="3" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                    <td>Gene functional status (select)</td>
                </tr>
                <!--
                <tr>
                    <td>
                        Protein Cluster Status:
                        <select name="proteinClusterStatus" id="proteinClusterStatus">
                            <option value="all"  selected="true">No preference</option>
                            <option value="1" >Green (experimental evidence)</option>
                            <option value="2" >Blue (function predicted)</option>
                            <option value="-1" >Black (not validated and no prediction)</option>
                        </select>
                    </td>
                </tr>
                -->
                <tr>
                <tr>
                    <td nowrap="nowrap" align="left" valign="middle" style="width:300px;padding: 5px 0px;">
                        <select style="width:280px;" name="phenotype" id="phenotype" selectedValue="">
                            <option value="-1">[All]</option>
                        </select>
                    </td>
                    <td>Phenotype association (select)[<a href="DAI?command=SciBay&fun=phenotypes">View list of phenotypes</a>]</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Check to restrict search to:
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPathogens" value="1"  /> Genes in Category A-C and Emerging/Re-Emerging pathogens
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="curated" value="1"  /> Genes in curated Protein Clusters
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px">
                        <input type="checkbox" name="hasPrediction" value="1" /> Genes with predictions submitted to COMBREX <div class="letterBox">P</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasStructure" value="1"  /> Genes with solved structures in PDB <div class="letterBox">S</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="hasHumanHomology" value="1"  /> Genes with Pfam domains shared with human proteins <div class="letterBox">H</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isPurified" value="1"  /> Genes with products purified by PSI <div class="letterBox">U</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 20px;">
                        <input type="checkbox" name="isCloned" value="1"  /> Genes cloned by PSI <div class="letterBox">C</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height:50px">
                        <input name="Search2" id="Search2" value="Search" type="submit" title="Acceptable Search Terms: NCBI gene ID, Uniprot accession number, RefSeq protein ID, Enzyme Commission (EC) number, NCBI Protein Cluster ID, gene symbol, gene name or name fragment, keywords "/>
                        <a href="#" id="close_advSearch" >close options</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

</div>

        <div id="functionalBar">
            <ul id="functionalLink2">
    <li class="headlink"><a href="index.jsp">Home</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink">Search</li>
-->
    <li class="vbar">|</li>
    <li class="headlink" id="tab_howto"><a href="#" title="" id="fun_howto">What You Can Do...</a>
	<div id="fun_sub_howto">
            <ul>
                <!--<li><a href="instructions_for_bid_submittal.html" title="Submit a bid to functionally validate a prediction" target="_blank">Submit a grant proposal</a></li>-->
                <li><a href="guide_to_submit_predictions.html" target="_blank">Submit Prediction</a></li>
                <li><a href="guide_to_submit_annotation.html" target="_blank">Submit Annotation</a></li>
                <li><a href="nominate_gene_for_high_priority_list.html" target="_blank">Nominate a gene for High Priority List</a></li>
            </ul>
        </div>
    </li>
    <!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="genome.jsp" title="Lists of the genes within the genomes of E. coli MG1655 and H. pylori 26695">Genome</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="statstic.jsp">Statistics</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="download.jsp" title="Download the Gold Standard Dataset and the Green Gene Set">Download</a></li>
    -->
    <li class="vbar">|</li>
    <li class="headlink"><a href="help_center.jsp">Help Center</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="acknowledgments.jsp">Acknowledgments</a></li>
<!--
    <li class="vbar">|</li>
    <li class="headlink"><a href="http://www.combrex.org/" target="_blank">About</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="https://spreadsheets0.google.com/viewform?hl=en&formkey=dEI5VG1xNGYydVhPZGlnM0NvM3hmWWc6MQ#gid=0" target="_blank">Bug Reporting/Feature Request</a></li>
-->
    <li class="vbar">|</li>
    <li class="headlink"><a href="contact.jsp">Contact Us</a></li>
    <li class="vbar">|</li>
<!--
    <li class="headlink"><a href="announcement.pdf">Grant Announcement</a></li>
    <li class="vbar">|</li>
-->
    <li class="headlink"><a href="whatIsNew.jsp">News</a></li>
    <li class="vbar">|</li>
    <li class="headlink"><a href="publications.jsp">Publications</a></li>

</ul>

        </div>
        <div id="main">
            <h2>Recently Published</h2>
              <ul>
                <li>Blaxter, Mark, Antoine Danchin, Babis Savakis, Kaoru Fukami-Kobayashi, Ken Kurokawa, Sumio Sugano, Richard J Roberts, Steven L Salzberg, and Chung-I Wu. “<a href="http://science.sciencemag.org/content/early/2016/05/10/science.aaf7672" target="_blank">Reminder to Deposit DNA Sequences.</a>” Science (New York, N.Y.) 352, no. 6287 (May 13, 2016): 780. doi:10.1126/science.aaf7672.</li>
                <li>Chang, Yi-Chien, Zhenjun Hu, John Rachlin, Brian P. Anton, Simon Kasif, Richard J. Roberts, and Martin Steffen. "<a href="http://nar.oxfordjournals.org/content/44/D1/D330.full" target="_blank">COMBREX-DB: An Experiment Centered Database of Protein Function: Knowledge, Predictions and Knowledge Gaps.</a>” Nucleic Acids Research 44, no. D1 (January 4, 2016): D330–35. doi:10.1093/nar/gkv1324.</li>
                <li>Fomenkov, Alexey, Tamas Vincze, Margarita Y Grabovich, Galina Dubinina, Maria Orlova, Elena Belousova, and Richard J Roberts. “<a href="http://genomea.asm.org/content/3/6/e01436-15.full" target="_blank">Complete Genome Sequence of the Freshwater Colorless Sulfur Bacterium Beggiatoa Leptomitiformis Neotype Strain D-402T.</a>” Genome Announcements 3, no. 6 (2015). doi:10.1128/genomeA.01436-15.</li>
                <li>Mariita, Richard M, Srijak Bhatnagar, Kurt Hanselmann, Mohammad J Hossain, Jonas Korlach, Matthew Boitano, Richard J Roberts, et al. “<a href="http://genomea.asm.org/content/3/6/e01504-15.full" target="_blank">Complete Genome Sequence of Curtobacterium Sp. Strain MR_MD2014, Isolated from Topsoil in Woods Hole, Massachusetts.</a>” Genome Announcements 3, no. 6 (2015). doi:10.1128/genomeA.01504-15.</li>
               <li>Mariita, Richard M, Srijak Bhatnagar, Kurt Hanselmann, Mohammad J Hossain, Jonas Korlach, Matthew Boitano, Richard J Roberts, et al. “<a href="http://genomea.asm.org/content/3/6/e01506-15.full" target="_blank">Complete Genome Sequence of Streptomyces Sp. Strain CCM_MD2014, Isolated from Topsoil in Woods Hole, Massachusetts.</a>” Genome Announcements 3, no. 6 (2015). doi:10.1128/genomeA.01506-15.</li>
              <li>Fomenkov, Alexey, Tamas Vincze, Margarita Grabovich, Brian P Anton, Galina Dubinina, Maria Orlova, Elena Belousova, and Richard J Roberts. “<a href="http://genomea.asm.org/content/4/1/e01521-15.full" target="_blank">Complete Genome Sequence of a Strain of Azospirillum Thiophilum Isolated from a Sulfide Spring.</a>” Genome Announcements 4, no. 1 (2016). doi:10.1128/genomeA.01521-15.</li>
              <li>Yao, Kuan, Tim Muruvanda, Richard J Roberts, Justin Payne, Marc W Allard, and Maria Hoffmann. “<a href="http://genomea.asm.org/content/4/1/e01599-15.full" target="_blank">Complete Genome and Methylome Sequences of Two Salmonella Enterica Spp.</a>” Genome Announcements 4, no. 1 (2016). doi:10.1128/genomeA.01599-15.</li>
              <li>Agre, Peter, Carolyn Bertozzi, Mina Bissell, Kevin P Campbell, Richard D Cummings, Umesh R Desai, Mary Estes, et al. “<a href="https://www.jci.org/articles/view/85905" target="_blank">Training the next Generation of Biomedical Investigators in Glycosciences.</a>” The Journal of Clinical Investigation 126, no. 2 (February 2016): 405–8. doi:10.1172/JCI85905.</li>
              <li>Blow, Matthew J, Tyson A Clark, Chris G Daum, Adam M Deutschbauer, Alexey Fomenkov, Roxanne Fries, Jeff Froula, et al. “<a href="http://journals.plos.org/plosgenetics/article?id=10.1371%2Fjournal.pgen.1005854" target="_blank">The Epigenomic Landscape of Prokaryotes.</a>” PLoS Genetics 12, no. 2 (February 2016): e1005854. doi:10.1371/journal.pgen.1005854.</li>
             <li>Yao, Kuan, Tim Muruvanda, Richard J Roberts, Justin Payne, Marc W Allard, and Maria Hoffmann. “<a href="http://genomea.asm.org/content/4/2/e00133-16.full" target="_blank">Complete Genome and Methylome Sequences of Salmonella Enterica Subsp. Enterica Serovar Panama (ATCC 7378) and Salmonella Enterica Subsp. Enterica Serovar Sloterdijk (ATCC 15791).</a>” Genome Announcements 4, no. 2 (2016). doi:10.1128/genomeA.00133-16.</li>
            <li>Callahan, Scott J, Yvette A Luyten, Yogesh K Gupta, Geoffrey G Wilson, Richard J Roberts, Richard D Morgan, and Aneel K Aggarwal. “<a href="http://journals.plos.org/plosbiology/article?id=10.1371%2Fjournal.pbio.1002442" target="_blank">Structure of Type IIL Restriction-Modification Enzyme MmeI in Complex with DNA Has Implications for Engineering New Specificities.</a>” PLoS Biology 14, no. 4 (April 2016): e1002442. doi:10.1371/journal.pbio.1002442.</li>
            <li>Vanderweyde, Tara, Daniel J Apicco, Katherine Youmans-Kidder, Peter E A Ash, Casey Cook, Edroaldo Lummertz da Rocha, Karen Jansen-West, et al. “<a href="http://www.sciencedirect.com/science/article/pii/S2211124716304727" target="_blank">Interaction of Tau with the RNA-Binding Protein TIA1 Regulates Tau Pathophysiology and Toxicity.</a>” Cell Reports 15, no. 7 (May 17, 2016): 1455–66. doi:10.1016/j.celrep.2016.04.045.</li>
            <li>Herrera, Victorial, Martin Steffen, Ann Moran, Glaiza Tan, Pasion Khristine, Keith Rivera, Darryl Pappin, and Nelson Ruiz-Opazo. “Confirmation of Translatability and Functionality Certifies the Dual endothelin1/VEGFsp Receptor (DEspR) Protein Victoria LM Herrera.” BMC Molecular Biology, (Accepted).</li>
              </ul>
            <h2>Publications</h2>
            <p>
              <ul>
               <li>Anton, Brian P, Yi-Chien Chang, Peter Brown, Han-Pil Choi, Lina L Faller, Jyotsna Guleria, Zhenjun Hu, et al. “<a href="http://journals.plos.org/plosbiology/article?id=10.1371%2Fjournal.pbio.1001638" target="_blank">The COMBREX Project: Design, Methodology, and Initial Results.</a>” PLoS Biology 11, no. 8 (August 2013): e1001638. doi:10.1371/journal.pbio.1001638.</li>
              <li>Wood, Derrick E, Henry Lin, Ami Levy-Moonshine, Rajiswari Swaminathan, Yi-Chien Chang, Brian P Anton, Lais Osmani, Martin Steffen, Simon Kasif, and Steven L Salzberg. “<a href="https://biologydirect.biomedcentral.com/articles/10.1186/1745-6150-7-37" target="_blank">Thousands of Missed Genes Found in Bacterial Genomes and Their Analysis with COMBREX.</a>” Biology Direct 7, no. 1 (January 2012): 37. doi:10.1186/1745-6150-7-37.</li>
              <li>Liu, Bo, Lina L Faller, Niels Klitgord, Varun Mazumdar, Mohammad Ghodsi, Daniel D Sommer, Theodore R Gibbons, et al. “<a href="http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0037919" target="_blank">Deep Sequencing of the Oral Microbiome Reveals Signatures of Periodontal Disease.</a>” PloS One 7, no. 6 (January 2012): e37919. doi:10.1371/journal.pone.0037919.</li>
               <li>Roberts, Richard J, Yi-Chien Chang, Zhenjun Hu, John N Rachlin, Brian P Anton, Revonda M Pokrzywa, Han-Pil Choi, et al. “<a href="http://nar.oxfordjournals.org/content/39/suppl_1/D11.full" target="_blank">COMBREX: A Project to Accelerate the Functional Annotation of Prokaryotic Genomes.</a>” Nucleic Acids Research 39, no. Database issue (January 2011): D11–14. doi:10.1093/nar/gkq1168.</li>
             </ul>
            </p>
        </div>
        <div id="foot">
            <!--
            <div style=" font-size: smaller"><a href="contact.jsp">Contact Us</a> | <a href="http://www.bu.edu/bioinformatics/" target="_blank">Bioinformatics Program, Boston University</a></div>
-->
        </div>

    </body>
</html>
