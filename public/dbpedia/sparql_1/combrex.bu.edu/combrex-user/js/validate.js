"use strict";
var VALIDATE_DELIMITER = ":";
var VALIDATE_PREFIX = "validate";
var VALIDATE_REQUIRED = "required";
var VALIDATE_OPTIONAL = "optional";
var VALIDATE_REQUIREDIFEMPTY = "requiredIfEmpty";
var VALIDATE_REQUIREDIFNOTEMPTY = "requiredIfNotEmpty";
var VALIDATE_MATCH = "match";

//VALIDATE_NUMERIC = "numeric";
//VALIDATE_ALPHA = "alpha";
//VALIDATE_ALPHA_NUMERIC = "alpha-numeric";
//VALIDATE_EMAIL = "email";

var RC_ERROR = -1;
var RC_REQUIRED = 0;
var RC_OPTIONAL = 1;
var RC_REQUIREDIFEMPTY = 2;
var RC_REQUIREDIFNOTEMPTY = 3;

var NAME_INDEX = 0;
var VALIDATE_INDEX = 1;
var REQUIRED_INDEX = 2;
var CONTENT_INDEX = 3;
var SPECIAL_INDEX = 4;

/* Should be validated? */
function canValidate(idArray) {
	var val = idArray[VALIDATE_INDEX];
	if(val===null) { return false; }
	if(val.match(VALIDATE_PREFIX)) {
		return true;
	}
	else {
		return false;
	}
}

function isEnabled(formField) {
	return (formField.disabled==true) ? false : true; 
}

/* Get required code */
function requiredCode(idArray) {
	var required = idArray[REQUIRED_INDEX];
	if(required===null) { return RC_ERROR; }
	if(required===VALIDATE_REQUIRED) {
		return RC_REQUIRED;
	}
	else {
		if(required===VALIDATE_OPTIONAL) {
			return RC_OPTIONAL;
		}
		else {
			if(required.match("^" + VALIDATE_REQUIREDIFEMPTY)) {
				return RC_REQUIREDIFEMPTY;
			}
			else {
				if(required.match("^" + VALIDATE_REQUIREDIFNOTEMPTY)) {
					return RC_REQUIREDIFNOTEMPTY;
				}
				else {
				    return RC_ERROR;
				}
			}
		}
	}
}

// Check for an empty or null value
function isEmpty(formField) {
	// TODO handle non textbox objects
	var value = formField.value;
	return (value===null || value.length===0) ? true : false;
}

/* Check that the two values match */
function matches(value1,value2) {
	return (value1==value2) ? true : false;
}

/* Extract the term between the parens */
function extractTerm(value) {
	var startIndex = value.lastIndexOf("(");
	var endIndex = value.lastIndexOf(")");
	return value.substring(startIndex+1,endIndex);
}

function prepareAlertMessage(form,errorMap) {
	var message = "The following errors occurred:\n";
	for(i=0; i < form.elements.length; i++) {
		name = form.elements[i].name;
		error = errorMap.get(name);
		if(error!=null) {
			message = message.concat(error + ": " + name + "\n");
		}
	}
	return message;
}

function validate(form) {
	// Initialize the array and map which will keep track of errors and successes
	var indexMap = {
	    set : function(foo,bar) {this[foo] = bar;},
	    get : function(foo) {return this[foo];}
	};
	var validMap = {
	    set : function(foo,bar) {this[foo] = bar;},
	    get : function(foo) {return this[foo];}
	};
	var errorMap = {
        set : function(foo,bar) {this[foo] = bar;},
        get : function(foo) {return this[foo];}
    };
	var emptyMap = {
	    set : function(foo,bar) {this[foo] = bar;},
	    get : function(foo) {return this[foo];}
	};
	var isValid = true;
	// Loop through the form elements
	for(var i=0; i < form.elements.length; i++) {
		// If this form field is not enabled, skip it
		if(!isEnabled(form.elements[i])) { continue; }
		// Split the id into parts by the delim 
		var idArray = form.elements[i].id.split(VALIDATE_DELIMITER);
		// If array is length <= 1 (no id or no special validation instructions) - ignore
		if(idArray.length<=1) { continue; }
		// Check if we should validate
		if(canValidate(idArray)) {
			indexMap.set(idArray[0],i);
			emptyMap.set(idArray[0],isEmpty(form.elements[i]));
			switch(requiredCode(idArray)) {
				case RC_REQUIRED:
					// Required field - can not be empty
					if(emptyMap.get(idArray[0])) {
						// Record failure
						errorMap.set(idArray[0],"Required field is empty");
						isValid = false;
					}
					else {
						// TEMP fix for password matching
						if(idArray[0]=="passwordConfirmation") {
							// Added "var" in front of next two lines - JNR 7/28/11
							var pass = form.elements[indexMap.get("password")].value;
							var cpass = form.elements[i].value;
							if(matches(pass,cpass)) {
								// Record success
							    validMap.set(idArray[0],true);
							}
							else {
								errorMap.set(idArray[0],"Passwords do not match");
								isValid = false;
							}
						}
						else {
						    // Record success
						    validMap.set(idArray[0],true);
						}
					}
					break;
				case RC_OPTIONAL:
					/* For now do nothing */
					break;
				case RC_REQUIREDIFEMPTY:
					/* requiredIfEmpty field - this field is required ONLY IF the referenced field is empty */
					if(emptyMap.get(idArray[0])) {
						/* It's empty! Better make sure that this is ok... */
						
						// Added "var" in front of next two lines - JNR 9/6/11
					    var value = idArray[REQUIRED_INDEX];
					    var dependsOn = extractTerm(value);
					    if(emptyMap.get(dependsOn)) {
                            /* Dependency is empty, so this field is required - but it's empty so we declare an error!!! */
					    	errorMap.set(idArray[0],"Required field is empty");
					    	isValid = false;
					    }
					    else {
					    	validMap.set(idArray[0],true);
					    }
					}
					else {
						// Not empty so any requirements are satisfied
						validMap.set(idArray[0],true);
					}
					break;
				case RC_REQUIREDIFNOTEMPTY:
					/* requiredIfNotEmpty field - this field is required ONLY IF the referenced field is NOT empty */
					if(emptyMap.get(idArray[0])) {
						// It's empty! Better make sure that this is ok...
					    value = idArray[REQUIRED_INDEX];
					    dependsOn = extractTerm(value);
					    if(!emptyMap.get(dependsOn)) {
					    	errorMap.set(idArray[0],"Required field is empty");
					    	isValid = false;
					    }
					    else {
					    	validMap.set(idArray[0],true);
					    }
					}
					else {
						// Not empty so any requirements are satisfied
						validMap.set(idArray[0],true);
					}
					break;
				default:
					// Programmer error - not a user error.
					//console.error("programmer error!");
			}
		}
	}
	if(!isValid) {
	    alert(prepareAlertMessage(form,errorMap));
	}
	return isValid;
}

