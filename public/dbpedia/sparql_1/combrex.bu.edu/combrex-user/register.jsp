


<html>
<head>
<title>Combrex : Registration</title>
<link rel="stylesheet" type="text/css" href="/combrex-user/css/scibay.css" />
<script type="text/javascript" src="/combrex-user/js/validate.js"></script>
<script type="text/javascript" src="/combrex-user/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/combrex-user/js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/combrex-user/js/jquery.layout.min-1.2.0.js"></script>
<script>
    $(document).ready(function () {
        $('body').layout({ resizeable: false });
    });
</script>
<script type="text/javascript">
  function validateRegistration() {
    // Validate
	isValid = validate(document.forms["registration"]);
    	
    // If form was valid then submit.
    if(isValid) {
        document.forms["registration"].submit();
    }
  };

  function userExists() {
    // Check if user exists
    $.get("/combrex-user/actions/checkIfUsernameExistsAction.jsp", { username: document.forms["registration"].username.value},
      function(data){
		if($.trim(data)=="true") {
        	document.forms["registration"].register.disabled = true;
        	document.forms["registration"].username.style.backgroundColor = "#FF9999";
        	document.getElementById("usernameError").innerHTML = "Already in use!";
        }
        else {
        	document.forms["registration"].register.disabled = false;
        	document.forms["registration"].username.style.backgroundColor = "white";
        	document.getElementById("usernameError").innerHTML = "";
        }
      }
    );
    
    // Take appropriate action
  };
</script> 
</head>
<body>
  <div class="ui-layout-north">











<div class="header">
  <div class="header-login" style="">
  <a href="/combrex-user/login.jsp">Log In</a>
  </div>
  <div class="header-inner">
    <a class="logo" href="http://combrex.bu.edu/"><img border="0" src="images/combrex_logo.png"/></a></h2>
    <table>
      <tr>
        <td onclick="location='http://combrex.bu.edu/'">Home</td>
        <td align="center" onclick="location='profile.jsp'">Profile</td>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
		        <td onclick="location='http://combrex.bu.edu/contact.jsp'">Contact</td>
		
      </tr>
    </table>
  </div>
</div>
<br><br><br></div>
  <div class="ui-layout-center" style="overflow:auto">
  <div>
    <form id="registration" action="/combrex-user/actions/registerAction.jsp" method="POST">
    <div class="bubble" style="float: left">
    <table class="blue" width="470px" cellpadding="0">
      <tr>
        <td class="tl"></td>
        <td class="title"><h3>Registration</h3></td>
        <td class="tr"></td>
      </tr>
      <tr>
        <td background="/combrex-user/css/images/new-l.png">
          <img src="/combrex-user/css/images/new-l.png" width="14" height="14">
        </td>
        <td>
          <table class="bubble-content">
            <tr><td class="sectionTop" colspan="2">Enter your email address and password</td></tr>
            <tr><td>
              <table class="bubble-content"> 
                <tr><td width="100px">Email<sup>*</sup></td><td><input type="text" id="username:validate:required" name="username" size="30" onblur="userExists()"></input>&nbsp;<span id="usernameError" ></span></td></tr>
                <tr><td>Password<sup>*</sup></td><td><input type="password" id="password:validate:required" name="password" size="30"></input></td></tr>
                <tr><td>Confirm&nbsp;Password<sup>*</sup></td><td><input type="password" id="passwordConfirmation:validate:required:match(password)" name="passwordConfirmation" size="30"></input></td></tr>
              </table>
            </td></tr>
            <tr><td class="section" colspan="2">Please enter your contact information</td></tr>
            <tr><td>
              <table class="bubble-content">
                <tr><td>Title</td><td>First Name<sup>*</sup></td><td>Last Name<sup>*</sup></td></tr>
                <tr>
                  <td>
                    <select id="title" name="title">
                      <option value="Dr.">Dr.</option>
                      <option value="Mr.">Mr.</option>
                      <option value="Ms.">Ms.</option>
                      <option value="Prof.">Prof.</option>
                    </select>
                  </td>
                  <td><input type="text" id="firstName:validate:required" name="firstName" size="20"></input></td>
                  <td><input type="text" id="lastName:validate:required" name="lastName" size="30"></input></td>
                </tr>
              </table>
            </td></tr>
            <tr><td>Address<sup>*</sup></td></tr>
            <tr><td><input type="text" id="address1:validate:required" name="address1" size="50"></input></td></tr>
            <tr><td><input type="text" id="address2:validate:optional:alpha_numeric" name="address2" size="50"></input></td></tr>
            <tr><td>
              <table class="bubble-content">
                <tr><td>City<sup>*</sup></td><td>State<sup>*</sup></td><td>ZIP/Postal code<sup>*</sup></td></tr>
                <tr>
                  <td><input type="text" id="city:validate:required" name="city" size="30"></input></td>
                  <td><input type="text" id="state:validate:required" name="state" size="10"></td>
                  <td><input type="text" id="postalCode:validate:required" name="postalCode" size="10"></input></td>
                </tr>
              </table>
            </td></tr>
            <tr><td>Country<sup>*</sup></td></tr>
            <tr><td><select name="country"><script language="javascript" src="/combrex-user/js/countries.js"></script></select></td></tr>
            <tr><td>Phone<sup>*</sup></td></tr>
            <tr><td><input type="text" id="phone:validate:required" name="phone" size="30"></input></td></tr>
            <tr><td class="section" colspan="2">Please enter your affiliation information</td></tr>
            <tr><td>Company / University</td></tr>
            <tr><td><input type="text" id="affiliation:validate:optional" name="affiliation" size="50"></input></td></tr>
            <tr><td>Department / Laboratory</td></tr>
            <tr><td><input type="text" id="department:validate:optional" name="department" size="50"></input></td></tr>
            <tr><td><input type="checkbox" id="pi" name="pi"></input>&nbsp;I am a Principal Investigator for my group</td></tr>
            <tr><td class="section" colspan="2">In lieu of an affiliation, please provide a reference</td></tr>
            <tr><td>Name</td></tr>
            <tr><td><input type="text" id="refName:validate:requiredIfEmpty(affiliation)" name="refName" size="50"></input></td></tr>
            <tr><td>Affiliation</td></tr>
            <tr><td><input type="text" id="refAffiliation:validate:requiredIfNotEmpty(refName)" name="refAffiliation" size="50"></input></td></tr>
            <tr><td>Phone</td></tr>
            <tr><td><input type="text" id="refPhone:validate:requiredIfNotEmpty(refName)" name="refPhone" size="50"></input></td></tr>
            <tr><td>Email</td></tr>
            <tr><td><input type="text" id="refEmail:validate:requiredIfNotEmpty(refName)" name="refEmail" size="50"></input></td></tr>
            
            <tr><td class="section" colspan="2">Select your desired roles</td></tr>
            <tr><td><input type="checkbox" name="bids"></input>&nbsp;Submit bids.</td></tr>
            <tr><td><input type="checkbox" name="predictions"></input>&nbsp;Submit predictions.</td></tr>
            <tr><td><input type="checkbox" name="comments"></input>&nbsp;Submit comments.</td></tr>
            <tr><td><input type="checkbox" name="gold"></input>&nbsp;Submit GOLD annotations.</td></tr>
            <tr><td><input type="checkbox" name="nominations"></input>&nbsp;Nominate genes as high priority.</td></tr>
            <tr><td><input type="checkbox" name="notifications"></input>&nbsp;Configure and receive alerts (future).</td></tr>
            
            <tr>
              <td class="registrationButtons">
                <button type="button" name="register" onclick="validateRegistration()">Register</button>
                <button type="reset" name="reset">Reset</button>
              </td>
            </tr>
          </table>
        </td>
        <td background="/combrex-user/css/images/new-r.png">
          <img src="/combrex-user/css/images/new-r.png" width="14" height="14">
        </td>
      </tr>
      <tr>
        <td class="bl"></td>
        <td class="bottom"><img src="/combrex-user/css/images/new-b.png"></td>
        <td class="br"></td>
      </tr>
    </table>
  </div>
  <div class="bubble" style="margin-left: 450px; min-width: 250px; max-width: 350px">
    <table class="blue" cellpadding="0">
      <tr>
        <td class="tl"></td>
        <td class="title"><h3>Instructions</h3></td>
        <td class="tr"></td>
      </tr>
      <tr>
        <td background="/combrex-user/css/images/new-l.png">
          <img src="/combrex-user/css/images/new-l.png" width="14" height="14">
        </td>
        <td>
          <table class="bubble-content">
            <tr><td>
            <p style="color:green"><b>IMPORTANT!</b> Registration is <b><i>not</i></b> required in order to browse the Combrex gene prediction database. It is only required to submit bids for experimental validation, or to submit new predictions, comments, or annotations.</p>    
            <p>1.   Most fields are required (*).  A reference is only required in the event that you cannot provide us with an affiliation or if you are not the PI for your group.   If an affiliation cannot be provided you <b>must</b> provide a reference contact because we need to know in which laboratory the work will be conducted if you don't have your own laboratory.</p>
            <p>2.  In the address fields, please provide the address of your company or university to which you are affiliated, or that of your reference contact if no affiliation is provided.</p>    
			</td></tr>
          </table>
        </td>
        <td background="/combrex-user/css/images/new-r.png">
          <img src="/combrex-user/css/images/new-r.png" width="14" height="14">
        </td>
      </tr>
      <tr>
        <td class="bl"></td>
        <td class="bottom"><img src="/combrex-user/css/images/new-b.png"></td>
        <td class="br"></td>
      </tr>
    </table>
  </div>
  </div>
  </div>
  <div class="ui-layout-south">

<div class="footer">
<a href="http://www.bu.edu" target="_blank"><img src="/combrex-user/images/bu.png"/></a>

</div></div>
</body>
</html>