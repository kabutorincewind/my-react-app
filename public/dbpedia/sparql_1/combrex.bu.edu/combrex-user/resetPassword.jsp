















<html>
<head>
<title>Combrex : Reset Password</title>
<link rel="stylesheet" type="text/css" href="/combrex-user/css/scibay.css" />
<script type="text/javascript" src="/combrex-user/js/validate.js"></script>
<script type="text/javascript" src="/combrex-user/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/combrex-user/js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/combrex-user/js/jquery.layout.min-1.2.0.js"></script>
<script>
    $(document).ready(function () {
        $('body').layout({ resizeable: false });
    });
</script>
<script type="text/javascript">
  function validateResetPassword() {
    // Validate
	isValid = validate(document.forms["resetPassword"]);
    	
    // If form was valid then submit.
    if(isValid) {
        document.forms["resetPassword"].submit();
    }
  };
</script>
</head>
<body>
  <div class="ui-layout-center"> 
  











<div class="header">
  <div class="header-login" style="">
  <a href="/combrex-user/login.jsp">Log In</a>
  </div>
  <div class="header-inner">
    <a class="logo" href="http://combrex.bu.edu/"><img border="0" src="images/combrex_logo.png"/></a></h2>
    <table>
      <tr>
        <td onclick="location='http://combrex.bu.edu/'">Home</td>
        <td align="center" onclick="location='profile.jsp'">Profile</td>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
		        <td onclick="location='http://combrex.bu.edu/contact.jsp'">Contact</td>
		
      </tr>
    </table>
  </div>
</div>
<br><br><br>
  
  
  
  <form name="resetPassword" action="/combrex-user/actions/sendResetEmailAction.jsp" method="POST">
  <div class="bubble">
    <table class="blue" width="400px" cellpadding="0">
      <tr>
        <td class="tl"></td>
        <td class="title"><h3>Reset Password</h3></td>
        <td class="tr"></td>
      </tr>
      <tr>
        <td background="/combrex-user/css/images/new-l.png">
          <img src="/combrex-user/css/images/new-l.png" width="14" height="14">
        </td>
        <td>
          <table style="bubble-content" width="100%">
            
            
            <tr><td colspan="2">Please enter the email address you use to log in. An email will be sent to you containing a link. Click on that link to securely reset your Combrex password.</td></tr>
            <tr><td colspan="2" class="sectionTop"></td></tr>
            <tr><td colspan="2"></td></tr>
            <tr><td>Email:</td><td><input type="text" size="40" id="username:validate:required" name="username"></input>&nbsp;<button type="button" onclick="validateResetPassword()">Ok</button></td></tr>
          </table>
        </td>
        <td background="/combrex-user/css/images/new-r.png">
          <img src="/combrex-user/css/images/new-r.png" width="14" height="14">
        </td>
      </tr>
      <tr>
        <td class="bl"></td>
        <td class="bottom"><img src="/combrex-user/css/images/new-b.png"></td>
        <td class="br"></td>
      </tr>
    </table>
  </div>
  </form>
  
  
  </div>
  <div class="ui-layout-south">

<div class="footer">
<a href="http://www.bu.edu" target="_blank"><img src="/combrex-user/images/bu.png"/></a>

</div></div>
</body>
</html>