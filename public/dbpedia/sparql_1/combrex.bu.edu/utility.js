// JavaScript Document
var http_request;
var requestType;
var xmlPrescription;
//adapt to jQuery
function addLoadEvent(func)
{
    $(document).ready(func);
    /*
    var oldonload = window.onload;
    if (typeof window.onload != 'function')
    {
        window.onload = func;
    }
    else
    {
        window.onload = function ()
        {
            oldonload();
            func();
        }
    }
    */
}

function getFormateDate()
{
    var formatString = this.getFullYear() + "/";
    if (this.getMonth()<9) formatString = formatString + "0";
    formatString = formatString + (Number(this.getMonth()) +1 )+ "/";
    if (this.getDate()<10) formatString = formatString + "0";
    formatString = formatString + this.getDate();
    return 	formatString;
}
Date.prototype.getFormateDate = getFormateDate;

function genRand(minValue,maxValue)
{
    var seed = Math.random();
    return Math.round(Number(minValue + Number(seed*(maxValue - minValue))));
}

function removeZero()
{
    var self = this;
    while (self.substring(0,1) == "0")
    {
        self = self.substring(1,self.length);
    }
    return self;
}
String.prototype.removeZero = removeZero;

function nextElementNode(inpNOde)
{
    var nextNode = inpNOde;
    do
    {
        nextNode = nextNode.nextSibling;
    }
    while(nextNode!= null && nextNode.nodeType!='1')
    return nextNode;
}


/*
function nextElementNode(inpNode, step)
{
	var nextNode = inpNode.nextSibling;
	for(var i=1;(i<=step && nextNode!= null);i++)
	{
		alert ("if" + nextNode.tagName);
		do
		{
			nextNode = nextNode.nextSibling;
			alert("do:" + i + "," + nextNode)
		}
		while(nextNode!= null && nextNode.nodeType!='1')
	}
	return nextNode;
}
*/

function getElementChildNodeByIndex(inpNode, idx)
{
    var outNode=inpNode.childNodes[0];
    if (outNode.nodeType!='1')
    {
        outNode = nextElementNode(outNode);
    }
    for(i=1;(idx != 0 && i<=idx && outNode!= null);i++)
    {
        outNode = nextElementNode(outNode);
    }
    return outNode;
}

function getXMLTagValue(xml,tagName)
{
    var nodes = xml.getElementsByTagName(tagName)[0]
    if (nodes == undefined) return false;
    if (nodes.childNodes.length == 0 ) return "";
    return nodes.childNodes[0].data;
}

function setXMLTagValue(xml,tagName)
{
    //alert('tagName:' + tagName);
    //alert(xml.getElementsByTagName(tagName)[0]);
    var nodes = xml.getElementsByTagName(tagName)[0]
    if (nodes == undefined) return false;
    if (nodes.childNodes.length == 0 ) return "";
    nodes.childNodes[0].data = tagName;
    return true;
}


function setDivTagValue(tagId,txt)
{
    var target = document.getElementById(tagId);
    var txtNode = document.createTextNode(txt);
    if (!target.firstChild)
    {
        target.appendChild(txtNode);
        return;
    }
    for (var i=0;i < target.childNodes.length;i++)
    {
        if (target.childNodes[i].nodeType == 3)
        {
            target.childNodes[i].nodeValue = txt;
            return;
        }
    }
    target.appendChild(txtNode);
}

function removeAllChildNode(node)
{
    while (node.firstChild)
    {
        //The list is LIVE so it will re-index each call
        node.removeChild(node.firstChild);
    }
}

function addEventHandler(element, type, func) { //unfortunate hack to deal with Internet Explorer's horrible DOM event model <iehack>
    if(element.addEventListener) {
        element.addEventListener(type,func,false);
    }
    else if (element.attachEvent) {
        element.attachEvent('on'+type,func);
    }
}
function getWidth(){
    var x = 0;
    if (typeof( window.innerWidth ) == 'number'){
        x = window.innerWidth;
    }
    else if (document.documentElement && document.documentElement.clientHeight){
        x = document.documentElement.clientWidth;
    }else if (document.body){
        x = document.body.clientWidth;
    }
    return x;
}

function getHeight(){
    var y = 0;
    if (typeof( window.innerHeight ) == 'number'){
        y = window.innerHeight;
    }else if (document.documentElement && document.documentElement.clientHeight){
        y = document.documentElement.clientHeight;
    }else if (document.body){
        y = document.body.clientHeight;
    }
    return y;
}

