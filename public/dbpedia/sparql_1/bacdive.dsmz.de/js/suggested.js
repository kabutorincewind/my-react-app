/**
 * @author		Adam Podstawka
 * @version		1.14
 * @date		2012-06-21
 * @lastchange	2016-09-06
 * @copyright	Copyright (c) 2012 - 2016 DSMZ, Adam Podstawka, Anna Vetcininova
 * @package	bacdive.js.suggested
 * @lastchangeby Anna Vetcininova
 */


// Set Suggestresponse as global
var suggestresponse="";

function handleJSONResponse(json)
{
    if(json !="")
    {
        var response = eval("(" + json + ")");
    } else {
        response ="";
    }
    return response;
}

function append2Suggest(ssr2use, text2use, hits2use, color2use)
{
	
	suggest ='<div onmouseover="javascript:suggestOver(this);" onmouseout="javascript:suggestOut(this,'+color2use+');" onclick="javascript:setSearch('+ssr2use+');" class="suggest_link'+color2use+'">';
	suggest+='<div class="suggest_link_resulttitle">';
	suggest+=text2use.substring(0,50);
	if(text2use.length > 50) suggest+="[...]";
	suggest+='</div>';
	suggest+='<div class="suggest_link_resulthits">(';
	suggest+=hits2use;
	suggest+=')</div>';
	suggest+='</div>';
	
	$("#suggested").append(suggest);
}

function cleansuggest(){ $('#suggested').empty(); }

//Called when the AJAX response is returned.
function handleSuggest(serverresponse) {
    // element leeren
	cleansuggest();
   
    suggestresponse = handleJSONResponse(serverresponse);
    
   
    var color=1;
    if(suggestresponse.suggest != undefined)
    {
    	
        for(var ssr=0; ssr<suggestresponse.suggest.resultentry.length; ssr++)
        {
        	append2Suggest(ssr, suggestresponse.suggest.resultentry[ssr].resulttitle, suggestresponse.suggest.resultentry[ssr].resulthits, color);

            // toggle color...
            if(color == 2) {color=1;} else {color=2;}
        }
        // show after loop
        if(ssr !="") $('#suggested').show(1, function(){ $(this).css("display", "block"); $(this).css("visibility","visible")});
    }
}

//Called from keyup on the search textbox.
//Starts the AJAX request.
function suggestSearch() {
    // check if first letters are spaces
	if (/^\s/g.test($('#searchValue').val()) != true)
    {
		// Escape value
        var str = escape($('#searchValue').val());
        
        // if searchValue smaller then one letter, clean suggest and hide
        if(str.length <1)
        {        	
            cleansuggest();
            $('#suggested').hide();
        } else {
	        var anyfield = "";
	        if($('#searchAnyField').attr("checked")) anyfield = "&anyfield=true";
	        
	        // Ajax Query 
	        $.ajax({
	        	  url: 'suggested.php?search=' + str + anyfield,
	        	  cache: false
	        	}).done(function( response ) {
	        	   handleSuggest(response);
	        	});
        }
	}
}

//Mouse over function
function suggestOver(div_value) { div_value.className = 'suggest_link_over'; }
//Mouse out function
function suggestOut(div_value, rowid) { div_value.className = 'suggest_link'+rowid; }
//Click function
function setSearch(id_suggest)
{    
    /* if the user clicks on a suggest, he will redirect to detail view of the strains or resultlist, regarding the amout of resulthits */
	window.location.href = 'index.php?search='+suggestresponse.suggest.resultentry[id_suggest].term;
    
    // Clean Suggest
    cleansuggest();
    // Hide Suggest
    $("#suggested").hide();
}