/**
 * @author		Adam Podstawka
 * @version	    1.39
 * @date		2012-06-21
 * @lastchange	2017-11-03
 * @copyright	Copyright (c) 2012-2017, DSMZ, Adam Podstawka, Anna Vetcininova
 * @package	    bacdive.js.helper
 * @lastchangeby Anna Vetcininova
 */

//require_once "helperfunctions.php";
var VirtualID=1;
var VirtualIDsused=new Array();

var browserName=navigator.appName;
var browserVer=parseInt(navigator.appVersion);
var version="";
var msie4=(browserName=="Microsoft Internet Explorer"&&browserVer>=4);
if((browserName=="Netscape"&&browserVer>=3)||msie4||browserName=="Konqueror"||browserName=="Opera")
{
    version="n3";
}else{
    version="n2";
}

function blurLink(theObject)
{
    if(msie4)
    {
        theObject.blur();
    }
}

function decryptCharcode(n,start,end,offset)
{
    n=n+offset;
    if(offset>0&&n>end)
    {
        n=start+(n-end-1);
    }else if(offset<0&&n<start){
            n=end-(start-n-1);
    }
    return String.fromCharCode(n);
}

function decryptString(enc,offset)
{
    var dec="";
    var len=enc.length;
    for(var i=0;i<len;i++)
    {
        var n=enc.charCodeAt(i);
        if(n>=0x2B&&n<=0x3A)
        {
            dec+=decryptCharcode(n,0x2B,0x3A,offset);
        }else if(n>=0x40&&n<=0x5A)
        {
            dec+=decryptCharcode(n,0x40,0x5A,offset);
        }else if(n>=0x61&&n<=0x7A)
        {
            dec+=decryptCharcode(n,0x61,0x7A,offset);
        }else{
            dec+=enc.charAt(i);
        }
    }
    return dec;
}

function linkTo_UnCryptMailto(s)
{
    location.href=decryptString(s,-1);
}


function toggleTextmining() 
{
	// verstecke Textmining inhalte
    if($('#excludetextmining:checked').length != 0) 
    {
    	$('.notmanual_annotated').each(function() 
    	{
    		$('.notmanual_annotated').removeClass('notmanual_annotated').addClass('notmanual_annotated_hidden');
    	}); // ende $('.ref_'+this).each(function()
        	
       	$('.notmanual_annotated_hidden').hide();
    } else { // zeige Textmining inhalte
    	$('.notmanual_annotated_hidden').each(function() 
    	{ 
        	$('.notmanual_annotated_hidden').removeClass('notmanual_annotated_hidden').addClass('notmanual_annotated');
        }); // $('.ref_'+this+'_hidden').each(function()
        	
        $('.notmanual_annotated').show();
    }
}

	
function toggleSections(){
	if($('.expandall').attr("alt") == "open"){
		$('.id_1').toggle(false);
		$('.id_2').toggle(false);
		$('.id_3').toggle(false);
		$('.id_4').toggle(false);
		$('.id_5').toggle(false);
		$('.id_6').toggle(false);
		$('.id_7').toggle(false);
		$('.expandall').attr("alt",   "closed");
	}
	else{
		$('.id_1').toggle(true);
		$('.id_2').toggle(true);
		$('.id_3').toggle(true);
		$('.id_4').toggle(true);
		$('.id_5').toggle(true);
		$('.id_6').toggle(true);
		$('.id_7').toggle(true);
		$('.expandall').attr("alt",   "open");
	}

}

function toggleSectionsAdvsearch(){
	if($('.expandall').attr("alt") == "open"){
		$('.1_id').toggle(false);
		$('.2_id').toggle(false);
		$('.3_id').toggle(false);
		$('.4_id').toggle(false);
		$('.5_id').toggle(false);
		$('.6_id').toggle(false);
		$('.7_id').toggle(false);
		$('.8_id').toggle(false);
		$('.9_id').toggle(false);
		$('.10_id').toggle(false);
		$('.expandall').attr("alt",   "closed");
	}
	else{
		$('.1_id').toggle(true);
		$('.2_id').toggle(true);
		$('.3_id').toggle(true);
		$('.4_id').toggle(true);
		$('.5_id').toggle(true);
		$('.6_id').toggle(true);
		$('.7_id').toggle(true);
		$('.8_id').toggle(true);
		$('.9_id').toggle(true);
		$('.10_id').toggle(true);
		$('.expandall').attr("alt",   "open");
	}

}



// close suggest window on clicking outside it
function closeSuggest() { if($('#suggested')) $('#suggested').hide(); }
//function eventListener(elementid, action) { if($(elementid) != undefined) { $(elementid).attr("onclick",action); } }

function ModalWindow(msgtoshow)
{
	  $.modal("<div style='color:#fff;margin:auto;'>"+msgtoshow+"<br /></div>", /* <a href='#' class='simplemodal-close'>close</a>*/
			  {
		  		overlayClose:true,
		  		minHeight:100,
		  		minWidth:100,
		  		opacity:80,
		  		overlayCss: { backgroundColor:"#fff" },
		  		containerCss: {
		  			borderColor:"#ccc",
		  			backgroundColor:"#999",
		  		}
		  	   });	
}

function DuplicateField(FieldID, sourcetable, ValueID, generatedID, AllFieldIDs)
{

	var newfieldcontent  ='<input id="field'+FieldID+'_virtualid'+generatedID+'sourcetable" value="'+sourcetable+'" type="hidden" />';
        newfieldcontent +='<input id="field'+FieldID+'_virtualid'+generatedID+'scn" value="'+$("#field"+FieldID+"_"+ValueID+"scn").attr("value")+'" type="hidden" />';
        newfieldcontent +='<input id="field'+FieldID+'_virtualid'+generatedID+'fieldid" value="'+FieldID+'" type="hidden" />';
	    newfieldcontent +='<input id="field'+FieldID+'_virtualid'+generatedID+'valuehex" value="" type="hidden" />';
	    newfieldcontent +='<input id="field'+FieldID+'_virtualid'+generatedID+'reference" value="" type="hidden" />';
	    newfieldcontent +='<input id="field'+FieldID+'_virtualid'+generatedID+'valueid" value="virtualid'+generatedID+'" type="hidden" />';
	    newfieldcontent +='<input id="field'+FieldID+'_virtualid'+generatedID+'fieldtype" value="'+$("#field"+FieldID+"_"+ValueID+"fieldtype").attr("value")+'" type="hidden" />';
	    newfieldcontent +='<input id="field'+FieldID+'_virtualid'+generatedID+'multiple" value="true" type="hidden" />';
	    
	
	if($("#field"+FieldID+"_"+ValueID+"fieldtype").attr("value") == "enumeration")
	{
		fieldtype="select";
	} else if($("#field"+FieldID+"_"+ValueID+"fieldtype").attr("value") == "checkbox")
	{
		fieldtype="checkbox";
	} else {
		fieldtype="textarea";
	}

	newfieldcontent +='<span title="Click here to edit this field" class="editable_'+fieldtype+'" id="field'+FieldID+'_virtualid'+generatedID+'">edit?</span>';

	// build Empty Fields
	var newfielddata = '<tr class="'+sourcetable+'_'+FieldID+'">';
        newfielddata+= '<td class="resultdetail_emptyfield"></td>';
        newfielddata+= '<td class="resultdetail_fieldtitle"></td>';
        newfielddata+= '<td class="resultdetail_fieldreference"></td>';
        newfielddata+= '<td class="resultdetail_fieldvalue"></td>';
        newfielddata+= '<td class="resultdetail_fieldspecs"></td>';
        newfielddata+= '</tr>';

	
	// generate DOM of HTML
    generatedNewField = $(newfielddata);
    
    // Edit DOM of HTML
    $(generatedNewField).children(".resultdetail_emptyfield").append($('.'+sourcetable+'_'+FieldID).children('.resultdetail_emptyfield').html());
    $(generatedNewField).children(".resultdetail_fieldspecs").append($('.'+sourcetable+'_'+FieldID).children('.resultdetail_fieldspecs').html());
    //$(generatedNewField).children('.resultdetail_emptyfield').replaceWith('<span onclick="DuplicateFields("'+AllFieldIDs+'","'+sourcetable+'", "virtualid'+generatedID+'");"><img src="images/addfields.gif" alt="add new fields" title="add new fields"></span>');
    $(generatedNewField).children('.resultdetail_fieldtitle').append($('.'+sourcetable+'_'+FieldID).children('.resultdetail_fieldtitle').html());
    $(generatedNewField).children('.resultdetail_fieldvalue').append($(newfieldcontent));
	$(generatedNewField).css('background-color','#ccffff');

	// append it to the parent of the field
	$(generatedNewField[0]).appendTo($('.'+sourcetable+'_'+FieldID+'').parent());

	
	// do editable duplicated field
	doEditable($(generatedNewField[0]).children('.resultdetail_fieldvalue'));

	return true;
}

function DuplicateFields(FieldIDs, sourcetable, ValueID)
{
	generatedID=generateVirtualIDRandom();
	if(FieldIDs != "")
	{
		ArrayFields = FieldIDs.split(",");
		for (var i=0; i<ArrayFields.length; i++)
		{
			DuplicateField(ArrayFields[i], sourcetable, ValueID, generatedID, FieldIDs);
		}
	}
}

function generateVirtualIDRandom()
{
	// Generate new VirtualID without collisions
	isinarray=true;
	var generatedVirtualID="";
	while(isinarray==true)
	{
		generatedVirtualID=Math.floor(Math.random() * (1+10000-200))+200;
		if($.inArray(generatedVirtualID, VirtualIDsused) <= -1)
		{
			// aus schleife raus
			isinarray = false;
			VirtualIDsused.push(generatedVirtualID);
		} 

	}
	return generatedVirtualID;
}


/**
 * Popup 
 * 
 * */
function popup(url, title, width, height)
{
	newpopup = window.open(url, title, "width="+width+",height="+height+"resizeable=no");
	newpopup.focus();
	return false;
}




/**
 * For the advanced search: the params in the URL for the GET method should only display terms, that will be chosen by user
 * 
 * */
function check(fieldID){
	// name for inputfield
	var name_inputfield_input="searchparams["+fieldID+"][searchterm]";
	$("tr[id="+fieldID+"] td.inputfield input").attr("name",name_inputfield_input);
	
	var name_inputfield_select="searchparams["+fieldID+"][searchterm]";
	$("tr[id="+fieldID+"] td.inputfield select").attr("name",name_inputfield_select);
	
	
	// name for selectfield
	var name_selectfield_input="searchparams["+fieldID+"][contenttype]";
	$("tr[id="+fieldID+"] td.selectfield input").attr("name",name_selectfield_input);
	
	var name_selectfield_select="searchparams["+fieldID+"][typecontent]";
	$("tr[id="+fieldID+"] td.selectfield select").attr("name",name_selectfield_select);
	
}



















