/**
 * @author		Adam Podstawka
 * @version	    3.07
 * @date		2012-11-13
 * @lastchange	2017-11-16
 * @lastchangeby Anna Vetcininova
 * @copyright	Copyright (c) 2012 - 2017 DSMZ, Adam Podstawka, Anna Vetcininova
 * @package	    bacdive.js.dlcart
 *
 */
var dlcart = [];
var dlcartsaved = true;
var dlcartselect = false;
var dlcartselectall = false;
var dlcartexporttextmining = true;

/**
 * Put item into Cart Array
 * 
 * @param itemid
 */





function Item2Cart(itemid)
{

	
	console.log(itemid);
	if($('input[name=dlcart]#'+itemid).prop("checked")){
		console.log(itemid);
		if(dlcart.indexOf(itemid) == -1)
		{
			dlcart.push(itemid);
			dlcartsaved=false;
		}
	}
	else{
		dlcart.splice(dlcart.indexOf(itemid),1);
	}

	console.log(dlcart);
	console.log(dlcart.length);
		


	// check cart length to show/hide add2cart popuplayer
	if(dlcart.length>0)
	{
		// Count up/down the content of dlcart and put it into css popup
		$("#addtocartcount").empty();
		
		$("#addtocartcount").append(dlcart.length+" Item");
		if(dlcart.length>1){
			$("#addtocartcount").append("s");
		}
		
		$("#addtocartpopoup").show(1, function(){ $(this).css("display", "block"); $(this).css("visibility","visible")});
	} else {
		$("#addtocartpopoup").hide();
		dlcartsaved=true;
	}
	
}


/**
 * Printout/alertout the content of dlcart (the id's)
 * only for debugneeds...
 */
function inhaltdlcart()
{
	if(dlcart.length>0)
	{
		$.each(dlcart, function(key, val) {
				ModalWindow("entry "+key+" in array:"+val);
				});
	} else {
		ModalWindow("The Cart is empty");
	}
}

/**
 * Checks if Cart is Empty
 * 
 * @returns {Boolean}
 */
function checkdlcartempty()
{
	if (dlcart.length <= 0)
		return true;
	else
		return false;
}

/**
 * Saves the Cart through ajax into session variable
 * 
 */
function save2cart()
{
	
	// Ajax Query 
    $.ajax({
    	  type: "POST",
    	  dataType: "text",
    	  url: 'dlcart.php',
    	  cache: false,
    	  //processData: false,
    	  data: "data="+dlcart
    	}).done(function( msg ) {
    	  ModalWindow(msg);
    	  dlcartsaved=true;
    	});

}

/**
 * Adds ID to Cart through ajax
 * 
 */
function add2cart(itemid)
{
    $('#removefromcart').show(1, function(){ $(this).css("display", "inline"); $(this).css("visibility","visible")});
    $('#addtocart').hide();

	dlcartarray=new Array("dlcart_"+itemid);
    // Ajax Query 
    $.ajax({
    	  type: "POST",
    	  dataType: "text",
    	  url: 'dlcart.php',
    	  cache: false,
    	  //processData: false,
    	  data: "data="+dlcartarray
    	}).done(function( msg ) {
    	  ModalWindow(msg);
    	});
}

/**
 * Removes ID from the Cart through ajax 
 * 
 */
function rmvfromcart(itemid)
{
    // show add2cart
	$('#addtocart').show(1, function(){ $(this).css("display", "inline"); $(this).css("visibility","visible")});
    $('#removefromcart').hide();

	dlcartarray=new Array("dlcart_"+itemid);
    // Ajax Query 
    $.ajax({
    	  type: "POST",
    	  dataType: "text",
    	  url: 'dlcart.php',
    	  cache: false,
    	  data: "dataremove="+dlcartarray
    	}).done(function( msg ) {
    		ModalWindow(msg);
    	});
}


/**
 * PopUp Window with "Download" / "Export"
 * 
 * @returns {Boolean}
 */
function downloadExport() 
{
	(dlcartexporttextmining == true) ? exportscript="dlexport.php?exporttextmining=true" : exportscript="dlexport.php?exporttextmining=false" 
	var dlPopUpWithTimeout = window.open(exportscript, "Export2CSV", "width=300,height=200,scrollbars=yes");
	setTimeout(function() { dlPopUpWithTimeout.close(); } ,40000);
	//ab.setTimeout("self.close()", 3000);
	return false;
}


/**
 * PopUp Window with "Download" / "Export for custom export"
 * 
 * @returns {Boolean}
 */
function downloadCustomExport() 
{
//	var exporttextmining = document.getElementById("exportWOTextmining");
////    alert(exporttextmining.value);
//	(exporttextmining.checked == true) ? exportscript_custom="dlexport_custom.php?exporttextmining=false" : exportscript_custom="dlexport_custom.php?exporttextmining=true" 
	
	exportscript_custom="dlexport_custom.php";
	var dlPopUpWithTimeout = window.open(exportscript_custom, "Export2CSV", "width=300,height=200,scrollbars=yes");
	setTimeout(function() { dlPopUpWithTimeout.close(); } ,800);
	//ab.setTimeout("self.close()", 3000);
	
}

/**
 * (De)Selects all Checkboxes on Searchpage
 * 
 */
function selectall()
{
	
	if(dlcartselectall != true)
	{
		$('input[name=dlcart]').each(function() 
		{ 
			$(this).prop('checked', true);
			dlcartselectall = true;
			Item2Cart($(this).val());
		});
		
	} else {
		$('input[name=dlcart]').each(function() 
		{ 
			$(this).prop('checked', false);
			dlcartselectall = false;
			Item2Cart($(this).val());
		});
		
	}
}

function setExportTextmining(field)
{
	if($('#custom_textmining').attr("value") == "true"){
		$('#custom_textmining').attr("value",   "false");
	}
	else{
		$('#custom_textmining').attr("value",   "true");
	}
	(field.checked == true) ? dlcartexporttextmining=false : dlcartexporttextmining=true;
	
}



/**
 * Check/uncheck all checkboxes of the choosing section in custom export
 */
function checkSection(section){
	if($('input.'+section.id+'[type="checkbox"]').prop("checked")){
		$('input.'+section.id+'[type="checkbox"]').prop('checked', false);
		
	}
	else{
		$('input.'+section.id+'[type="checkbox"]').prop('checked', true);
	}
}















