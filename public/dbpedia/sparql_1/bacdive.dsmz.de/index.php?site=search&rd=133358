
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang='de' lang='de'>
<head>
<title>BacDive - The Bacterial Diversity Metadatabase : Kribbella deserti Sun et al. 2017</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-site-verification" content="mIOYHrpUl_1G65CeMEFGWkETjn4L82yOiWWvhY3Zo04" />
<meta name="expires" content="0" />
<meta http-equiv="expires" content="0" />
<meta name="pragma" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="cache-control" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="description" content="test" />

<meta name="abstract" content="BacDive - The Bacterial Diversity Metadatabase" />
<meta name="Content-Language" content="en" />
<meta name="Revisit-After" content="1 days" />
<meta name="author" content="Adam Podstawka" />
<meta name="Copyright" content="&copy; Adam Podstawka" />
<meta name="Publisher" content="Leibnitz-Institut DSMZ" />
<meta name="Page-Type" content="research" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="favicon.ico" />




<link rel="stylesheet" href="styles/style.css" type="text/css" />
<link rel="stylesheet" href="styles/bar.css" type="text/css" />
<link rel="stylesheet" href="styles/style.search.css" type="text/css" />
<link rel="stylesheet" href="styles/suggest.css" type="text/css" />

<link rel="stylesheet" href="styles/style.chooseref.css" type="text/css" />
<link rel="stylesheet" href="styles/style.kitapi.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="styles/bootstrap_pagination.css"/>



<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/helper.js"></script>
<script type="text/javascript" src="js/suggested.js"></script>
<script type="text/javascript" src="js/dlcart.js"></script> 

<script type="text/javascript" src="js/jquery.tablesorter.js"></script> 
<script type="text/javascript" src="js/angular_v1.5.8.min.js"></script>

<script type="text/javascript" src="js/dirPagination.js"></script>
<script type="text/javascript" src="js/smart-table.min.js"></script>
<script type="text/javascript" src="apisearch/apitableCtrl.js"></script>


<script type="text/javascript" src="js/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/slick.css"/>
<link rel="stylesheet" type="text/css" href="styles/slick-theme.css"/>













<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(["setCookieDomain", "*.bacdive.dsmz.de"]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://piwik.dsmz.de/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->



</head>

<body id="pagetop">
    <div id="topbar">
		
<div id="topbarcontent" >
    <div id="logo"><a href="./"><img src="images/bacdivelogo_high.png" alt="BacDive - The Bacterial Diversity Metadatabase" title="BacDive - The Bacterial Diversity Metadatabase" class="noBorder"/></a></div>
    <div id='announce'><a href="?site=news#survey" target='blank'>User satisfaction survey</a><img class="icon" src="images/good-idea_12.png" alt="icon for survey" /></div>
    
    <div  id="navicontainer">
        <ul class="links">
            
            <li><a href="help" onclick="return popup(this.href,'help', 1024, 768);" alt='help' title="help">help</a></li>
            <li><a href="?site=contact" alt="contact" title="contact">about</a></li>
            <li><a href="?site=tutorials" alt="tutorials" title="tutorials">tutorials</a></li>
            <li><a href="?site=news" alt="news" title="news">news</a></li>
            <li><a href="mailto:contact@bacdive.de?subject=Feedback for BacDive - You're missing something or something is wrong? Tell us!" alt="link for writing an email for feedback" title="feedback">contact</a></li>
        </ul>
    </div>
    
    <div id="denbi"><a href="https://www.denbi.de/">
        <div> member of </div>
        <img src="images/deNBI_Logo_rgb.png" /> 
    </a></div>
</div>

<div id="navbar">
   <div id = "navigation">
           <ul class="links">
                
                
                <li><a class="navlink" href="https://bacdive.dsmz.de/api/" alt="link to webservices" title="webservices" target="_blank">web services</a></li>
                <li><a class="navlink" href="?site=dlselect" alt="download selection" title="download selection">download selection </a></li>
                
                <li><a class="navlink" href="?site=advsearch"alt="advanced search function" title="advanced search">advanced search</a></li>
                <li><a class="navlink" href="?site=strains" alt="list of strains" title="browse through taxonomy">TAXplorer</a></li>
                <li><a class="navlink" href="?site=apisearch" alt="apikit search function" title="search through api">API &reg; test finder</a></li>
           </ul>
    </div>
    <div id="searchtop">
        <div>
            <form method="GET" action="index.php?site=search">
                <input class="font10" id="searchValue" type="text" name="search"  onkeyup="suggestSearch();" value="" />
                <input class="font10" id="submit" type="submit" name="submit" value="Search" /> 
                <a href="#" class="tooltip">
                    <span>search over all mainfields in database</span>
                    <input class="font10" id="searchAnyField" type="checkbox" name="anyfield" value="checked"   />
                </a>
                <span id="suggested"></span>
            </form>
        </div>
    </div>
</div>

    </div>
    <div id="middle_content">
        <div class="inhalt_start">
			


	from MySQLdb.constants.ER import YES

<div id="resultdetail">
	<div id="infoboxbar">
<div id="infobox">
	<table class="paddingleft">
		<tr>
			<td rowspan="2" class="width90 infobox_key blue">Strain identifier</td>
			<td class="infobox_key">BacDive ID:</td><td class="width72">133358</td>
			
			<td class="infobox_key">Species:</td><td class="cell">Kribbella deserti</td>			
			<td class="infobox_key width150">Strain Designation:</td><td class="cell">SL15-1</td>		</tr>
		<tr>
			<td class="infobox_key">Type strain:</td><td class="width72"><img src="images/haken.gif" alt="Yes" title="Yes" width="13" height="13" /></td>			<td class="infobox_key widthcell">Culture col. no.:</td><td class="cell widthmin" colspan="4">CGMCC 1.15906, KCTC 39825</td>		</tr>
	</table>
</div>
</div>
	<div id="resultdetailtoolbar">

<table id="toolbar">
	<tr>
		<td id="browsetable" ><div>
<div id="browsestrains">	
<table id="browsestrain_table">
	<tr>
		<td class="arrowinstrains"><a class="none_textdec" href="?rd=133357" target="_self">&laquo;</a></td>
		<td class="middle">Browse strain by BacDive ID</td>
		<td class="arrowinstrains"><a class="none_textdec" href="?rd=133359" target="_self">&raquo;</a></td>
	</tr>
</table>
</div></div></td>
		<td id="straindetailExpandall" class="expandall blue" alt="open" onclick="toggleSections();" colspan="3">
			<table class="expand_table"><tr>
				<td><a href="#">expand / minimize all</a></td>
				<td><a href="#"><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="blue arrow down"/></a></td>
			</tr></table>	
		</td>
		<td id="exclude">
			<table><tr>
				<td><input class="font10" type="checkbox" name="excludetextmining" id="excludetextmining" onclick="toggleTextmining();" /></td>
				<td>Exclude text mining derived information</td>
			</tr></table>	
		</td>
 		
		<td id="cite">
			<div>
				<div class=" doinumber hidden">
					<div>For referencing data from this strain:</div>
					<div><a href="https://doi.org/10.13145/bacdive133358.20171208.2.1">doi:10.13145/bacdive133358.20171208.2.1</a></div>
					<div>Please consider citation of Bac<i>Dive</i>: </div>
					<div><a href="https://doi.org/10.1093/nar/gkv983">doi: 10.1093/nar/gkv983</a></div>
				</div>
				<div><input onclick="$('.doinumber').toggle();" class="font10" type="submit" value="cite this strain"/></div>				
			</div>
		</td>
		<td id="pdf"><a href="?site=pdf_view&amp;id=133358" alt="pdf" title="pdf"><input class="font10" type="submit" value="PDF archive"/></a></td>
		<td id="download"><input class="font10" type="submit" value="download" onclick="add2cart(133358);"/></td>
	</tr>
</table>

</div>
	<div id="box">
		<div>
<div id="sectionlinks">
	<table id="heading">
		<tr onclick="$('.sections').toggle();">
			<th colspan="2">Section</th>
			<td><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="expand / minimize"/></td>			
		</tr>
	</table>
	<table class="sections">
					<tr><td colspan="3"><a href="#section_1">Name and taxonomic classification</a></td></tr>
							<tr><td colspan="3"><a href="#section_2">Morphology and physiology</a></td></tr>
							<tr><td colspan="3"><a href="#section_3">Culture and growth conditions</a></td></tr>
							<tr><td colspan="3"><a href="#section_4">Isolation, sampling and environmental information</a></td></tr>
									<tr><td colspan="3"><a href="#section_6">Molecular biology</a></td></tr>
							<tr><td colspan="3"><a href="#section_7">Strain availability</a></td></tr>
			</table>
</div>
</div>
		<div>

<div id="externallinks">
	<table id="heading">
		<tr onclick="$('#externallinks .links').toggle();">
			<th colspan="2">External links</th>
			<td><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="expand / minimize"/></td>			
		</tr>
	</table>
	<table class="links hidden">
		<tr><td>Search for species <i><b>Kribbella deserti</b></i> in external resources:</td></tr>
		<tr><td colspan="3"><a href="https://www.arb-silva.de/search/show/ssu/name/Kribbella deserti">SILVA</a></td></tr>
		<tr><td colspan="3"><a href="http://www.brenda-enzymes.org/search_result.php?a=20&W[1]=Kribbella deserti&T[1]=2&l=10&RNV=1&RN=&T[0]=2&Search=Search" target="_blank">BRENDA</a></a></td></tr>
		<tr><td colspan="3"><a href="http://www.pangaea.de/search?ie=UTF-8&count=10&q=parameter%3A%22Kribbella deserti%22" target="_blank">PANGAEA</a></a></td></tr>
		<tr><td colspan="3"><a href="http://www.straininfo.net/strains/search?strainNumber=&exactStrain=true&taxon=Kribbella deserti" target="_blank">StrainInfo</a></td></tr>
		<tr><td colspan="3"><a href="http://www.gbif.org/species/search?q=Kribbella deserti" target="_blank">GBIF</a></td></tr>
	</table>
</div>
</div> 
	</div>
	<div id="content">
		<div>
												<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="1" onclick="$('.id_1').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Information on the name and the taxonomic classification.</span>Name and taxonomic classification</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_1"></div>
		        	  		<table class="section_content"><tr><td>
			        	  						        	  				
										<table class="id_1 section expandsection">
																																																	<tr>
														<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
														<td class="bold_valigntop width180_valigntop">Genus</td>
																													<td class="valigntop"><i>Kribbella</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
														<td class="bold_valigntop width180_valigntop">Species</td>
																													<td class="valigntop"><i><i>Kribbella deserti</i></i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
														<td class="bold_valigntop width180_valigntop">Strain Designation</td>
																													<td class="valigntop"><i>SL15-1</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
														<td class="bold_valigntop width180_valigntop">Type strain</td>
																													<td class="valigntop"><i><img src="images/haken.gif" alt="Yes" title="Yes" width="13" height="13" /></i></td>
																									
													</tr>
													
												
										
											
											
																					</table>		
											
										<table class="id_1 section expandsection">
																							<th></th>
												<th></th>
												<th class="bold_valigntop paddingright"><a href="https://www.dsmz.de/bacterial-diversity/prokaryotic-nomenclature-up-to-date.html" target="_blank">Prokaryotic Nomenclature Up-to-date (PNU)</a></th>
												<th class="bold_valigntop paddingright">Taxonomical status</th>
												<th class="bold_valigntop paddingright">Literature reference</th>
																																																						<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																												
																	<td class="bold_valigntop width180_valigntop">Domain</td>
																																		
																	<td class="valigntop">Bacteria</td>
																															</tr>
																												
																																																																						<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop">Phylum</td>
																																																	<td class="valigntop"><i>Actinobacteria</i></td>
																															</tr>
																												
																																																																					
																													<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																												
																	<td class="bold_valigntop width180_valigntop">Class</td>
																	
															<td class="valigntop paddingright"><i>Actinobacteria</i></td>
																																																																					
																													<td></td>
															<td class="valigntop paddingright">Int. J. Syst. Bacteriol. 47:483*</td></tr>
																																																																					
																													<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop"> Family</td>
																	
															<td class="valigntop paddingright"><i>Nocardioidaceae</i></td>
																																																																					
																																											<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																											
																<td class="bold_valigntop width180_valigntop">Genus</td>
																														<td class="valigntop paddingright"><i>Kribbella</i></td>
																																																							
																																											<td class="valigntop paddingright">gen. nov. (VP) </td>
																																																							
																																											<td class="valigntop paddingright">Int. J. Syst. Bacteriol. 49:750*</td></tr>
																																																							
																																											<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																															<td class="bold_valigntop width180_valigntop"> Species</td>
																														<td class="valigntop paddingright"><i><i>Kribbella deserti</i></i></td>
																																																							
																																											<td class="valigntop paddingright">sp. nov. (VP)</td>
																																																							
																																											<td class="valigntop paddingright">Int. J. Syst. Evol. Microbiol. 67:695*</td></tr>
																																																								<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop">Full Scientific Name</td>
																																		
																	<td class="valigntop"><i>Kribbella deserti</i> Sun et al. 2017</td>
																															</tr>
																												
																																									
												
																					</table>		
									
									
											        	  	</td></tr></table>
		        	  	</ul>
		        	</div>	
		     													<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="2" onclick="$('.id_2').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Information on morphological and physiological properties</span>Morphology and physiology</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_2"></div>
		        	  		<table class="section_content"><tr><td>
			        	  																				
										<table class="id_2 section expandsection">
																																																																																				
											
												 													 																													<tr>
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="bold_valigntop width180_valigntop">Gram stain</td>
																															<td>positive</td>
																													</tr>
														
																											 																													<tr>
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="bold_valigntop width180_valigntop">Motility</td>
																															<td>no</td>
																													</tr>
														
																																																								
																																							
												
												
																																			
										</table>
																		
										<table class="id_2 section expandsection">
																																																																								
																								 																																																										<tr>
																															<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
																<td class="bold_valigntop width180_valigntop">Colony color</td>
																																	<td>pasty</td>
																															</tr>
																												
																																																																								<tr>
																															<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
																<td class="bold_valigntop width180_valigntop">Colony shape</td>
																																	<td>irregular</td>
																															</tr>
																												
																																																																								<tr>
																															<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
																<td class="bold_valigntop width180_valigntop">Cultivation medium used</td>
																																	<td ><a href="#LB">LB</a></td>	
																															</tr>
														
																												
																																																								
													
																																							
												
												
												
																																			
										</table>
																		
										<table class="id_2 section expandsection">
												
																																																<tr><td><table id="enzyme_table">
																																						
														
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																								
																																											
																																																																																					
																																																																																					
																																																																								
																																											
																																																																																					
																																																																								
																																											
																																																																																					
																																																																								
																																											
																																																																								
																																											
																																																																																				
												
													<thead>
													<th class="width90"></th>	
													<th class="bold_valigntop width180_valigntop">Enzymes</th>
													<th class="bold_valigntop italic border">Enzyme<img class="sort-arrow" src="images/sort_pfeil.png" alt="arrow to sort" title="to sort"/></th>	
													<th class="bold_valigntop italic border">Enzyme activity<img class="sort-arrow" src="images/sort_pfeil.png" alt="arrow to sort" title="to sort"/></th>	
													<th class="bold_valigntop italic border">EC number<img class="sort-arrow" src="images/sort_pfeil.png" alt="arrow to sort" title="to sort"/></th>								
													<th class="noborder"></th>
													</thead>
													
													<tbody>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">acid phosphatase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.1.3.2" target="blank">3.1.3.2</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">alkaline phosphatase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.1.3.1" target="blank">3.1.3.1</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">alpha-chymotrypsin</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.4.21.1" target="blank">3.4.21.1</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">alpha-fucosidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.2.1.51" target="blank">3.2.1.51</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">alpha-galactosidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.2.1.22" target="blank">3.2.1.22</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">alpha-glucosidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.2.1.20" target="blank">3.2.1.20</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">alpha-mannosidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.2.1.24" target="blank">3.2.1.24</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">arginine dihydrolase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">-</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.5.3.6" target="blank">3.5.3.6</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">beta-galactosidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.2.1.23" target="blank">3.2.1.23</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">beta-glucosidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.2.1.21" target="blank">3.2.1.21</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_rows_enz hidden">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">beta-glucuronidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">-</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.2.1.31" target="blank">3.2.1.31</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_rows_enz hidden">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">catalase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=1.11.1.6" target="blank">1.11.1.6</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_rows_enz hidden">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">cystine arylamidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">-</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.4.11.3" target="blank">3.4.11.3</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_rows_enz hidden">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">cytochrome oxidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">-</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=1.9.3.1" target="blank">1.9.3.1</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_rows_enz hidden">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">esterase (C 4)</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_rows_enz hidden">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">esterase Lipase (C 8)</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_rows_enz hidden">
																													<td class="firstcell"><a href="#ref25234">[Ref.: #25234]</a></td>
															<td class="width180_valigntop"></td>
																																																<td class="width180_valigntop border">leucine arylamidase</td>
																	
																																																																<td class="width80 textalign_center valigntop border">+</td>
																																																																<td class="valigntop border"><a href="http://brenda-enzymes.org/enzyme.php?ecno=3.4.11.1" target="blank">3.4.11.1</a></td>
																															
																													</tr>
														
													
														
																																												
																													<tr class="hidden_row