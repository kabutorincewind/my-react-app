
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang='de' lang='de'>
<head>
<title>BacDive - The Bacterial Diversity Metadatabase : Acetobacter pasteurianus (Hansen 1879) Beijerinck and Folpmers 1916</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-site-verification" content="mIOYHrpUl_1G65CeMEFGWkETjn4L82yOiWWvhY3Zo04" />
<meta name="expires" content="0" />
<meta http-equiv="expires" content="0" />
<meta name="pragma" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="cache-control" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="description" content="test" />

<meta name="abstract" content="BacDive - The Bacterial Diversity Metadatabase" />
<meta name="Content-Language" content="en" />
<meta name="Revisit-After" content="1 days" />
<meta name="author" content="Adam Podstawka" />
<meta name="Copyright" content="&copy; Adam Podstawka" />
<meta name="Publisher" content="Leibnitz-Institut DSMZ" />
<meta name="Page-Type" content="research" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="favicon.ico" />




<link rel="stylesheet" href="styles/style.css" type="text/css" />
<link rel="stylesheet" href="styles/bar.css" type="text/css" />
<link rel="stylesheet" href="styles/style.search.css" type="text/css" />
<link rel="stylesheet" href="styles/suggest.css" type="text/css" />

<link rel="stylesheet" href="styles/style.chooseref.css" type="text/css" />
<link rel="stylesheet" href="styles/style.kitapi.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="styles/bootstrap_pagination.css"/>



<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/helper.js"></script>
<script type="text/javascript" src="js/suggested.js"></script>
<script type="text/javascript" src="js/dlcart.js"></script> 

<script type="text/javascript" src="js/jquery.tablesorter.js"></script> 
<script type="text/javascript" src="js/angular_v1.5.8.min.js"></script>

<script type="text/javascript" src="js/dirPagination.js"></script>
<script type="text/javascript" src="js/smart-table.min.js"></script>
<script type="text/javascript" src="apisearch/apitableCtrl.js"></script>


<script type="text/javascript" src="js/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/slick.css"/>
<link rel="stylesheet" type="text/css" href="styles/slick-theme.css"/>













<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(["setCookieDomain", "*.bacdive.dsmz.de"]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://piwik.dsmz.de/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->



</head>

<body id="pagetop">
    <div id="topbar">
		
<div id="topbarcontent" >
    <div id="logo"><a href="./"><img src="images/bacdivelogo_high.png" alt="BacDive - The Bacterial Diversity Metadatabase" title="BacDive - The Bacterial Diversity Metadatabase" class="noBorder"/></a></div>
    <div id='announce'><a href="?site=news#survey" target='blank'>User satisfaction survey</a><img class="icon" src="images/good-idea_12.png" alt="icon for survey" /></div>
    
    <div  id="navicontainer">
        <ul class="links">
            
            <li><a href="help" onclick="return popup(this.href,'help', 1024, 768);" alt='help' title="help">help</a></li>
            <li><a href="?site=contact" alt="contact" title="contact">about</a></li>
            <li><a href="?site=tutorials" alt="tutorials" title="tutorials">tutorials</a></li>
            <li><a href="?site=news" alt="news" title="news">news</a></li>
            <li><a href="mailto:contact@bacdive.de?subject=Feedback for BacDive - You're missing something or something is wrong? Tell us!" alt="link for writing an email for feedback" title="feedback">contact</a></li>
        </ul>
    </div>
    
    <div id="denbi"><a href="https://www.denbi.de/">
        <div> member of </div>
        <img src="images/deNBI_Logo_rgb.png" /> 
    </a></div>
</div>

<div id="navbar">
   <div id = "navigation">
           <ul class="links">
                
                
                <li><a class="navlink" href="https://bacdive.dsmz.de/api/" alt="link to webservices" title="webservices" target="_blank">web services</a></li>
                <li><a class="navlink" href="?site=dlselect" alt="download selection" title="download selection">download selection </a></li>
                
                <li><a class="navlink" href="?site=advsearch"alt="advanced search function" title="advanced search">advanced search</a></li>
                <li><a class="navlink" href="?site=strains" alt="list of strains" title="browse through taxonomy">TAXplorer</a></li>
                <li><a class="navlink" href="?site=apisearch" alt="apikit search function" title="search through api">API &reg; test finder</a></li>
           </ul>
    </div>
    <div id="searchtop">
        <div>
            <form method="GET" action="index.php?site=search">
                <input class="font10" id="searchValue" type="text" name="search"  onkeyup="suggestSearch();" value="" />
                <input class="font10" id="submit" type="submit" name="submit" value="Search" /> 
                <a href="#" class="tooltip">
                    <span>search over all mainfields in database</span>
                    <input class="font10" id="searchAnyField" type="checkbox" name="anyfield" value="checked"   />
                </a>
                <span id="suggested"></span>
            </form>
        </div>
    </div>
</div>

    </div>
    <div id="middle_content">
        <div class="inhalt_start">
			


	from MySQLdb.constants.ER import YES

<div id="resultdetail">
	<div id="infoboxbar">
<div id="infobox">
	<table class="paddingleft">
		<tr>
			<td rowspan="2" class="width90 infobox_key blue">Strain identifier</td>
			<td class="infobox_key">BacDive ID:</td><td class="width72">23459</td>
			
			<td class="infobox_key">Species:</td><td class="cell">Acetobacter pasteurianus</td>			
			<td class="infobox_key width150">Strain Designation:</td><td class="cell">AB0220</td>		</tr>
		<tr>
			<td class="infobox_key">Type strain:</td><td class="width72"><img src="images/remove.png" alt="No" title="No" width="13" height="13" /></td>			<td class="infobox_key widthcell">Culture col. no.:</td><td class="cell widthmin" colspan="4"><a href="https://www.dsmz.de/catalogues/details/culture/DSM-25273.html" target="_blank">DSM 25273</a></td>		</tr>
	</table>
</div>
</div>
	<div id="resultdetailtoolbar">

<table id="toolbar">
	<tr>
		<td id="browsetable" ><div>
<div id="browsestrains">	
<table id="browsestrain_table">
	<tr>
		<td class="arrowinstrains"><a class="none_textdec" href="?rd=23458" target="_self">&laquo;</a></td>
		<td class="middle">Browse strain by BacDive ID</td>
		<td class="arrowinstrains"><a class="none_textdec" href="?rd=23488" target="_self">&raquo;</a></td>
	</tr>
</table>
</div></div></td>
		<td id="straindetailExpandall" class="expandall blue" alt="open" onclick="toggleSections();" colspan="3">
			<table class="expand_table"><tr>
				<td><a href="#">expand / minimize all</a></td>
				<td><a href="#"><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="blue arrow down"/></a></td>
			</tr></table>	
		</td>
		<td id="exclude">
			<table><tr>
				<td><input class="font10" type="checkbox" name="excludetextmining" id="excludetextmining" onclick="toggleTextmining();" /></td>
				<td>Exclude text mining derived information</td>
			</tr></table>	
		</td>
 		
		<td id="cite">
			<div>
				<div class=" doinumber hidden">
					<div>For referencing data from this strain:</div>
					<div><a href="https://doi.org/10.13145/bacdive23459.20171208.2.1">doi:10.13145/bacdive23459.20171208.2.1</a></div>
					<div>Please consider citation of Bac<i>Dive</i>: </div>
					<div><a href="https://doi.org/10.1093/nar/gkv983">doi: 10.1093/nar/gkv983</a></div>
				</div>
				<div><input onclick="$('.doinumber').toggle();" class="font10" type="submit" value="cite this strain"/></div>				
			</div>
		</td>
		<td id="pdf"><a href="?site=pdf_view&amp;id=23459" alt="pdf" title="pdf"><input class="font10" type="submit" value="PDF archive"/></a></td>
		<td id="download"><input class="font10" type="submit" value="download" onclick="add2cart(23459);"/></td>
	</tr>
</table>

</div>
	<div id="box">
		<div>
<div id="sectionlinks">
	<table id="heading">
		<tr onclick="$('.sections').toggle();">
			<th colspan="2">Section</th>
			<td><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="expand / minimize"/></td>			
		</tr>
	</table>
	<table class="sections">
					<tr><td colspan="3"><a href="#section_1">Name and taxonomic classification</a></td></tr>
							<tr><td colspan="3"><a href="#section_2">Morphology and physiology</a></td></tr>
							<tr><td colspan="3"><a href="#section_3">Culture and growth conditions</a></td></tr>
							<tr><td colspan="3"><a href="#section_4">Isolation, sampling and environmental information</a></td></tr>
											<tr><td colspan="3"><a href="#section_7">Strain availability</a></td></tr>
			</table>
</div>
</div>
		<div>

<div id="externallinks">
	<table id="heading">
		<tr onclick="$('#externallinks .links').toggle();">
			<th colspan="2">External links</th>
			<td><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="expand / minimize"/></td>			
		</tr>
	</table>
	<table class="links hidden">
		<tr><td>Search for species <i><b>Acetobacter pasteurianus</b></i> in external resources:</td></tr>
		<tr><td colspan="3"><a href="https://www.arb-silva.de/search/show/ssu/name/Acetobacter pasteurianus">SILVA</a></td></tr>
		<tr><td colspan="3"><a href="http://www.brenda-enzymes.org/search_result.php?a=20&W[1]=Acetobacter pasteurianus&T[1]=2&l=10&RNV=1&RN=&T[0]=2&Search=Search" target="_blank">BRENDA</a></a></td></tr>
		<tr><td colspan="3"><a href="http://www.pangaea.de/search?ie=UTF-8&count=10&q=parameter%3A%22Acetobacter pasteurianus%22" target="_blank">PANGAEA</a></a></td></tr>
		<tr><td colspan="3"><a href="http://www.straininfo.net/strains/search?strainNumber=&exactStrain=true&taxon=Acetobacter pasteurianus" target="_blank">StrainInfo</a></td></tr>
		<tr><td colspan="3"><a href="http://www.gbif.org/species/search?q=Acetobacter pasteurianus" target="_blank">GBIF</a></td></tr>
	</table>
</div>
</div> 
	</div>
	<div id="content">
		<div>
												<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="1" onclick="$('.id_1').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Information on the name and the taxonomic classification.</span>Name and taxonomic classification</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_1"></div>
		        	  		<table class="section_content"><tr><td>
			        	  						        	  				
										<table class="id_1 section expandsection">
																																																	<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Domain</td>
																
															<td class="valigntop">Bacteria</td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Phylum</td>
																													<td class="valigntop"><i>Proteobacteria</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Class</td>
																													<td class="valigntop"><i>Alphaproteobacteria</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Order</td>
																													<td class="valigntop"><i>Rhizobiales</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Family</td>
																													<td class="valigntop"><i>Acetobacteraceae</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Genus</td>
																													<td class="valigntop"><i>Acetobacter</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Species</td>
																													<td class="valigntop"><i><i>Acetobacter pasteurianus</i></i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Full Scientific Name</td>
																
															<td class="valigntop"><i>Acetobacter pasteurianus</i> (Hansen 1879) Beijerinck and Folpmers 1916</td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Strain Designation</td>
																													<td class="valigntop"><i>AB0220</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Type strain</td>
																													<td class="valigntop"><i><img src="images/remove.png" alt="No" title="No" width="13" height="13" /></i></td>
																									
													</tr>
													
												
										
											
											
																					</table>		
											
										<table class="id_1 section expandsection">
																							<th></th>
												<th></th>
												<th class="bold_valigntop paddingright"><a href="https://www.dsmz.de/bacterial-diversity/prokaryotic-nomenclature-up-to-date.html" target="_blank">Prokaryotic Nomenclature Up-to-date (PNU)</a></th>
												<th class="bold_valigntop paddingright">Taxonomical status</th>
												<th class="bold_valigntop paddingright">Literature reference</th>
																																																						<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																												
																	<td class="bold_valigntop width180_valigntop">Domain</td>
																																		
																	<td class="valigntop">Bacteria</td>
																															</tr>
																												
																																																																						<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop">Phylum</td>
																																																	<td class="valigntop"><i>Proteobacteria</i></td>
																															</tr>
																												
																																																																					
																													<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																												
																	<td class="bold_valigntop width180_valigntop">Class</td>
																	
															<td class="valigntop paddingright"><i>Alphaproteobacteria</i></td>
																																																																					
																													<td></td>
															<td class="valigntop paddingright">Int. J. Syst. Evol. Microbiol. 56:1</td></tr>
																																																																					
																													<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop"> Family</td>
																	
															<td class="valigntop paddingright"><i>Acetobacteraceae</i></td>
																																																																					
																													<td></td>
															<td class="valigntop paddingright">Int. J. Syst. Bacteriol. 30:23*</td></tr>
																																																																					
																																											<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																											
																<td class="bold_valigntop width180_valigntop">Genus</td>
																														<td class="valigntop paddingright"><i>Acetobacter</i></td>
																																																							
																																											<td class="valigntop paddingright">genus (AL) </td>
																																																							
																																											<td class="valigntop paddingright">Int. J. Syst. Bacteriol. 30:239 (AL)</td></tr>
																																																							
																																											<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																															<td class="bold_valigntop width180_valigntop"> Species</td>
																														<td class="valigntop paddingright"><i><i>Acetobacter pasteurianus</i></i></td>
																																																							
																																											<td class="valigntop paddingright">species (AL)</td>
																																																							
																																											<td class="valigntop paddingright">Int. J. Syst. Bacteriol. 30:239 (AL)</td></tr>
																																																								<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop">Full Scientific Name</td>
																																		
																	<td class="valigntop"><i>Acetobacter pasteurianus</i> (Hansen 1879) Beijerinck and Folpmers 1916</td>
																															</tr>
																												
																																									
												
																					</table>		
											
										<table class="id_1 section expandsection">
																																																		<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
															<td class="bold_valigntop width180_valigntop">Synonym</td>
															<td class="valigntop paddingright"><i>Acetobacter pasteurianus subsp. pasteurianus</i></td>												
														</tr>
														
																																							<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
															<td class="bold_valigntop width180_valigntop">Synonym</td>
															<td class="valigntop paddingright"><i>Acetobacter peroxydans</i></td>												
														</tr>
														
																																							<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
															<td class="bold_valigntop width180_valigntop">Synonym</td>
															<td class="valigntop paddingright"><i>Acetobacter pasteurianus subsp. ascendens</i></td>												
														</tr>
														
																																							<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
															<td class="bold_valigntop width180_valigntop">Synonym</td>
															<td class="valigntop paddingright"><i>Acetobacter pasteurianus subsp. paradoxus</i></td>												
														</tr>
														
																							
																					</table>		
									
									
											        	  	</td></tr></table>
		        	  	</ul>
		        	</div>	
		     													<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="2" onclick="$('.id_2').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Information on morphological and physiological properties</span>Morphology and physiology</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_2"></div>
		        	  		<table class="section_content"><tr><td>
			        	  																				
										<table class="id_2 section expandsection">
																																																																								
																								 																																																										<tr>
																															<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
																<td class="bold_valigntop width180_valigntop">Incubation period</td>
																																	<td>3-7 days</td>
																															</tr>
																												
																																																								
													
																																							
												
												
												
																																			
										</table>
									
					
									
											        	  	</td></tr></table>
		        	  	</ul>
		        	</div>	
		     													<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="3" onclick="$('.id_3').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Information on culture and growth conditions</span>Culture and growth conditions</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_3"></div>
		        	  		<table class="section_content"><tr><td>
			        	  					
										
										<table class="id_3 section expandsection">
																																																																																				
											
													
																																										<tr>
																																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
															<td class="bold_valigntop width180_valigntop">Culture medium</td>
															<td class="valigntop"><table class="bordercollapse">
																																	<tr><td>GLUCONOBACTER OXYDANS MEDIUM (DSMZ Medium 105), 28°C</td><td id="GLUCONOBACTER OXYDANS MEDIUM (DSMZ Medium 105), 28°C" class="anchor"></td></tr>
																															</table></td>
																												</tr>
														
																																																								<tr>
																																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
															<td class="bold_valigntop width180_valigntop">Culture medium growth</td>
																															<td class="valigntop"><img src="images/haken.png" alt="Yes" title="Yes" width="13" height="13" /></td>
																																											</tr>
														
																																																								<tr>
																																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
															<td class="bold_valigntop width180_valigntop">Culture medium link</td>
															<td><a href="https://www.dsmz.de/microorganisms/medium/pdf/DSMZ_Medium105.pdf" target="_blank">medium recipe provided by DSMZ</a></td></tr>
																												</tr>
														
																																																								<tr>
																																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
															<td class="bold_valigntop width180_valigntop">Culture medium composition</td>
																															<td> <a onclick="$('.medium_comp_rows_0').toggle();"> expand / minimize </a></td>
																														
															</tr>
															
															<tr class="medium_comp_rows_0 hidden">
																<td class="firstcell"></td>
																<td class="bold_valigntop width180_valigntop"></td>
																<td>
																																	<br> Glucose                                    100.0      g<br> Yeast extract                               10.0      g<br> CaCO<SUB>3</SUB>                                       20.0      g<br> Agar                                        15.0      g<br> Distilled water                           1000.0     ml<p>Adjust pH to 6.8.
																																</td>
															
																												</tr>
														
																																																									
																																						
											
										
																					</table>
										
										<table class="id_3 section expandsection">
																																																																								
														
																																																																																	
																																									
																																								
														
																																																																																	
																																									
																																							
											
											
																									<tr><td><table id="temp_table">
														<thead>
														<th class="width90"></th>	
														<th class="bold_valigntop width180_valigntop">Temperatures</th>
														<th class="bold_valigntop border italic" colspan="2">Kind of temperature<img class="sort-arrow" src="images/sort_pfeil.png" alt="arrow to sort" title="to sort"/></th>														<th class="bold_valigntop border italic">Temperature<img class="sort-arrow" src="images/sort_pfeil.png" alt="arrow to sort" title="to sort"/></th>	
														<th class="noborder"></th>
														</thead>
														<tbody>
														
															
																																																<tr>
																																	<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
																	<td></td>
																	
																		
																																																									<td class="border_rightfree textalign_right"></td>
																																																							
																																					<td class="border_leftfree">optimum</td>
																																				
																			
																			<td class="border_leftfree textalign_center">28 &nbsp;&#778;C </td>
																																				
																																			
																	
																																	</tr>
																														
																																																<tr>
																																	<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
																	<td></td>
																	
																		
																																																									<td class="border_rightfree textalign_right"></td>
																																																							
																																					<td class="border_leftfree">growth</td>
																																				
																			
																			<td class="border_leftfree textalign_center">28 &nbsp;&#778;C </td>
																																				
																																			
																	
																																	</tr>
																													
														</tbody>
													</table></td></tr>
																								
																										<tr><td>&nbsp;</td></tr>													<tr><td><table id="temp_range">
													    
																																																																																																																																																																															<tr>
																																	<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
																	<td class="bold_valigntop width180_valigntop">Temperature range</td>
																																			<td class="valigntop">mesophilic</td>
																																	</tr>
																																																													
																																																																																																																																															
													
													</table></td></tr>
																								
																					</table>
									
									
											        	  	</td></tr></table>
		        	  	</ul>
		        	</div>	
		     													<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="4" onclick="$('.id_4').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Information on isolation source, the sampling and environmental conditions</span>Isolation, sampling and environmental information</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_4"></div>
		        	  		<table class="section_content"><tr><td>
			        	  									
																	
										<table class="id_4 section expandsection">
																																																																			
												
																																																		<tr>
																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Sample type/isolated from</td>
																																													<td class="valigntop">viegar</td>
																																										</tr>
																																																				<tr>
																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Geographic location (country and/or sea, region)</td>
																																													<td class="valigntop">Reggio Emilia</td>
																																										</tr>
																																																				<tr>
																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Country</td>
																																													<td class="valigntop">Italy</td>
																																										</tr>
																																																				<tr>
																											<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
														<td class="bold_valigntop width180_valigntop">Continent</td>
																																													<td class="valigntop">Europe</td>
																																										</tr>
																																																				
																																				
										</table>
									
									
											        	  	</td></tr></table>
		        	  	</ul>
		        	</div>	
		     																											<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="7" onclick="$('.id_7').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Availability in culture collections</span>Strain availability</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_7"></div>
		        	  		<table class="section_content"><tr><td>
			        	  																			
										<table class="id_7 section expandsection">
																																																<tr>
																									<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
													<td class="bold_valigntop width180_valigntop">Culture collection no.</td>
																											<td class="valigntop"><a href="https://www.dsmz.de/catalogues/details/culture/DSM-25273.html" target="_blank">DSM 25273</a></td>
																									</tr>
												
											
										</table>
																	
										<table class="id_7 section expandsection">
																																																<tr>
																									<td class="firstcell"><a href="#ref20224">[Ref.: #20224]</a></td>
													<td class="bold_valigntop width180_valigntop">Strain history</td>
																											<td class="valigntop"><- M. Gullo, Univ. of Modena and Reggio Emilia, Dept. of Agricult. and Food Science, Italy; AB0220</td>
																									</tr>
												
											
										</table>
									
			        	  					        	  	</td></tr></table>
		        	  	</ul>
		        	</div>	
		     				
		</div>
	</div>	
	<div id="reference">
<div id="references">
	<ul>
		<li class="resultdetail_sectiontitle">References</li>
					<li class="resultdetail_reference">
				<table class="resultdetail_reference">
					<tr>
						<td class="resultdetail_reference_refid"><a id="ref20215" name="ref20215"></a>#20215</td>
						<td class="resultdetail_reference_refdata">
																																		
																																													Leibniz Institute DSMZ 
																																													
																																													D.Gleim, M.Kracht, N.Weiss et. al.: 
																																																								<a href="http://www.dsmz.de/bacterial-diversity/prokaryotic-nomenclature-up-to-date.html" target="_blank">Prokaryotic Nomenclature Up-to-date - compilation of all names of Bacteria and Archaea, validly published according to the Bacteriological Code since 1. Jan. 1980, and validly published nomenclatural changes since</a>
										. 
																																													
																																													
																																													
																																													
																																			 
										 
																																													
																																																																																																																													
																																													
																										
													</td>
					</tr>
				</table>
			</li>
					<li class="resultdetail_reference">
				<table class="resultdetail_reference">
					<tr>
						<td class="resultdetail_reference_refid"><a id="ref20224" name="ref20224"></a>#20224</td>
						<td class="resultdetail_reference_refdata">
															Leibniz Institut DSMZ-Deutsche Sammlung von Mikroorganismen und Zellkulturen GmbH
								; Curators of the DSMZ; 
																	<a href="http://www.dsmz.de/catalogues/details/culture/DSM-25273.html" target="_blank">DSM 25273</a>
																													</td>
					</tr>
				</table>
			</li>
		
		<li class="resultdetail_referencetextmining">* These References are textmined</li>
	</ul>
</div>

</div>
</div> 

        </div>
    </div>
	

<script type="text/javascript">
/* <![CDATA[ */
/*
window.onbeforeunload = function (e)
{
	var message = "Attention: You have some not commited items in the exportcart. Are you sure you want to exit this page without commiting?",
	e = e || window.event;
	// For IE and Firefox
	if(checkdlcartempty() != true)
	{
		if (e) { e.returnValue = message; }
		// For Safari
		return message;
	}
};
*/

$(window).on('beforeunload',function() {
	var message = "Attention: You have some not commited items in the exportcart. Are you sure you want to exit this page without commiting?";
	if(checkdlcartempty() != true && dlcartsaved != true)
	{
		ModalWindow(message);
		return message;
	}
});

// fill placeholder property in inputfield, if exists


if($('#searchValue') != undefined)
{
	$('#searchValue').prop("placeholder", "species name, culture col. no., sequence no.");
	$('#searchValue').prop("autocomplete", "off");
}


// for Download Selection Cart
if (($("[name='dlcart']").length != 0) && ($("[name='dlcart']").length != undefined))
{
	for (var i=0; i< $("[name='dlcart']").length;i++)
	{
		$("[name='dlcart']")[i].onclick=function(){Item2Cart(this.id);};
		//$("[name='dlcart']")[i].onchange=function()\{alert(this.id);};
		//eventListener(document.getElementsByName("dlcart")[i].id, alert(i));	
	}
}

//on outside click - close suggest, on inside click - open suggest
if($('#pagetop') != undefined) $('#pagetop').click(function() {closeSuggest(); });
if($('#searchValue') != undefined) $('#searchValue').click(function() {suggestSearch();})


$(document).ready(function(){
	// Slick Slide
	if ($('.propagateme').length > 0){					
		  $('.propagateme').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 3000,
		  accessibility:true,
		  dots:true,
		  fade:true,
		  infinite:true,
		  pauseOnHover:true,
		  pauseOnDotsHover:true,
		  variableWidth:false,
		  centerMode:true,
		  });
	}	
	
	//to sort the table				
	$("#enzyme_table").tablesorter(); 
    $(".halophily_table").tablesorter(); 
    $("#met_antibiotica_nogroup").tablesorter();
    $("#met_antibiotica_group").tablesorter();
    $("#met_production").tablesorter(); 
    $("#met_test").tablesorter(); 
    $("#met_util").tablesorter(); 
    $("#temp_table").tablesorter(); 
    $("#ph_table").tablesorter(); 
    $("#sequence_table").tablesorter(); 	  
    $(".fa_table").tablesorter(); 
															
});








/* ]]> */
</script>


</body>
</html>