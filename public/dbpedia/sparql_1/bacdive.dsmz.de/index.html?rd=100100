
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang='de' lang='de'>
<head>
<title>BacDive - The Bacterial Diversity Metadatabase : Deinococcus sp.</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-site-verification" content="mIOYHrpUl_1G65CeMEFGWkETjn4L82yOiWWvhY3Zo04" />
<meta name="expires" content="0" />
<meta http-equiv="expires" content="0" />
<meta name="pragma" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="cache-control" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="description" content="test" />

<meta name="abstract" content="BacDive - The Bacterial Diversity Metadatabase" />
<meta name="Content-Language" content="en" />
<meta name="Revisit-After" content="1 days" />
<meta name="author" content="Adam Podstawka" />
<meta name="Copyright" content="&copy; Adam Podstawka" />
<meta name="Publisher" content="Leibnitz-Institut DSMZ" />
<meta name="Page-Type" content="research" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="favicon.ico" />




<link rel="stylesheet" href="styles/style.css" type="text/css" />
<link rel="stylesheet" href="styles/bar.css" type="text/css" />
<link rel="stylesheet" href="styles/style.search.css" type="text/css" />
<link rel="stylesheet" href="styles/suggest.css" type="text/css" />

<link rel="stylesheet" href="styles/style.chooseref.css" type="text/css" />
<link rel="stylesheet" href="styles/style.kitapi.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="styles/bootstrap_pagination.css"/>



<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/helper.js"></script>
<script type="text/javascript" src="js/suggested.js"></script>
<script type="text/javascript" src="js/dlcart.js"></script> 

<script type="text/javascript" src="js/jquery.tablesorter.js"></script> 
<script type="text/javascript" src="js/angular_v1.5.8.min.js"></script>

<script type="text/javascript" src="js/dirPagination.js"></script>
<script type="text/javascript" src="js/smart-table.min.js"></script>
<script type="text/javascript" src="apisearch/apitableCtrl.js"></script>


<script type="text/javascript" src="js/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/slick.css"/>
<link rel="stylesheet" type="text/css" href="styles/slick-theme.css"/>













<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(["setCookieDomain", "*.bacdive.dsmz.de"]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://piwik.dsmz.de/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->



</head>

<body id="pagetop">
    <div id="topbar">
		
<div id="topbarcontent" >
    <div id="logo"><a href="./"><img src="images/bacdivelogo_high.png" alt="BacDive - The Bacterial Diversity Metadatabase" title="BacDive - The Bacterial Diversity Metadatabase" class="noBorder"/></a></div>
    <div id='announce'><a href="?site=news#survey" target='blank'>User satisfaction survey</a><img class="icon" src="images/good-idea_12.png" alt="icon for survey" /></div>
    
    <div  id="navicontainer">
        <ul class="links">
            
            <li><a href="help" onclick="return popup(this.href,'help', 1024, 768);" alt='help' title="help">help</a></li>
            <li><a href="?site=contact" alt="contact" title="contact">about</a></li>
            <li><a href="?site=tutorials" alt="tutorials" title="tutorials">tutorials</a></li>
            <li><a href="?site=news" alt="news" title="news">news</a></li>
            <li><a href="mailto:contact@bacdive.de?subject=Feedback for BacDive - You're missing something or something is wrong? Tell us!" alt="link for writing an email for feedback" title="feedback">contact</a></li>
        </ul>
    </div>
    
    <div id="denbi"><a href="https://www.denbi.de/">
        <div> member of </div>
        <img src="images/deNBI_Logo_rgb.png" /> 
    </a></div>
</div>

<div id="navbar">
   <div id = "navigation">
           <ul class="links">
                
                
                <li><a class="navlink" href="https://bacdive.dsmz.de/api/" alt="link to webservices" title="webservices" target="_blank">web services</a></li>
                <li><a class="navlink" href="?site=dlselect" alt="download selection" title="download selection">download selection </a></li>
                
                <li><a class="navlink" href="?site=advsearch"alt="advanced search function" title="advanced search">advanced search</a></li>
                <li><a class="navlink" href="?site=strains" alt="list of strains" title="browse through taxonomy">TAXplorer</a></li>
                <li><a class="navlink" href="?site=apisearch" alt="apikit search function" title="search through api">API &reg; test finder</a></li>
           </ul>
    </div>
    <div id="searchtop">
        <div>
            <form method="GET" action="index.php?site=search">
                <input class="font10" id="searchValue" type="text" name="search"  onkeyup="suggestSearch();" value="" />
                <input class="font10" id="submit" type="submit" name="submit" value="Search" /> 
                <a href="#" class="tooltip">
                    <span>search over all mainfields in database</span>
                    <input class="font10" id="searchAnyField" type="checkbox" name="anyfield" value="checked"   />
                </a>
                <span id="suggested"></span>
            </form>
        </div>
    </div>
</div>

    </div>
    <div id="middle_content">
        <div class="inhalt_start">
			


	from MySQLdb.constants.ER import YES

<div id="resultdetail">
	<div id="infoboxbar">
<div id="infobox">
	<table class="paddingleft">
		<tr>
			<td rowspan="2" class="width90 infobox_key blue">Strain identifier</td>
			<td class="infobox_key">BacDive ID:</td><td class="width72">100100</td>
			
			<td class="infobox_key">Species:</td><td class="cell">Deinococcus sp.</td>			
			<td class="infobox_key width150">Strain Designation:</td><td class="cell">ST027575(HKI)</td>		</tr>
		<tr>
								</tr>
	</table>
</div>
</div>
	<div id="resultdetailtoolbar">

<table id="toolbar">
	<tr>
		<td id="browsetable" ><div>
<div id="browsestrains">	
<table id="browsestrain_table">
	<tr>
		<td class="arrowinstrains"><a class="none_textdec" href="?rd=100099" target="_self">&laquo;</a></td>
		<td class="middle">Browse strain by BacDive ID</td>
		<td class="arrowinstrains"><a class="none_textdec" href="?rd=100101" target="_self">&raquo;</a></td>
	</tr>
</table>
</div></div></td>
		<td id="straindetailExpandall" class="expandall blue" alt="open" onclick="toggleSections();" colspan="3">
			<table class="expand_table"><tr>
				<td><a href="#">expand / minimize all</a></td>
				<td><a href="#"><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="blue arrow down"/></a></td>
			</tr></table>	
		</td>
		<td id="exclude">
			<table><tr>
				<td><input class="font10" type="checkbox" name="excludetextmining" id="excludetextmining" onclick="toggleTextmining();" /></td>
				<td>Exclude text mining derived information</td>
			</tr></table>	
		</td>
 		
		<td id="cite">
			<div>
				<div class=" doinumber hidden">
					<div>For referencing data from this strain:</div>
					<div><a href="https://doi.org/10.13145/bacdive100100.20171208.2.1">doi:10.13145/bacdive100100.20171208.2.1</a></div>
					<div>Please consider citation of Bac<i>Dive</i>: </div>
					<div><a href="https://doi.org/10.1093/nar/gkv983">doi: 10.1093/nar/gkv983</a></div>
				</div>
				<div><input onclick="$('.doinumber').toggle();" class="font10" type="submit" value="cite this strain"/></div>				
			</div>
		</td>
		<td id="pdf"><a href="?site=pdf_view&amp;id=100100" alt="pdf" title="pdf"><input class="font10" type="submit" value="PDF archive"/></a></td>
		<td id="download"><input class="font10" type="submit" value="download" onclick="add2cart(100100);"/></td>
	</tr>
</table>

</div>
	<div id="box">
		<div>
<div id="sectionlinks">
	<table id="heading">
		<tr onclick="$('.sections').toggle();">
			<th colspan="2">Section</th>
			<td><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="expand / minimize"/></td>			
		</tr>
	</table>
	<table class="sections">
					<tr><td colspan="3"><a href="#section_1">Name and taxonomic classification</a></td></tr>
															</table>
</div>
</div>
		<div>

<div id="externallinks">
	<table id="heading">
		<tr onclick="$('#externallinks .links').toggle();">
			<th colspan="2">External links</th>
			<td><img class="arrow" src="images/pfeile_blau.png" alt="blue arrow down" title="expand / minimize"/></td>			
		</tr>
	</table>
	<table class="links hidden">
		<tr><td>Search for species <i><b>Deinococcus sp.</b></i> in external resources:</td></tr>
		<tr><td colspan="3"><a href="https://www.arb-silva.de/search/show/ssu/name/Deinococcus sp.">SILVA</a></td></tr>
		<tr><td colspan="3"><a href="http://www.brenda-enzymes.org/search_result.php?a=20&W[1]=Deinococcus sp.&T[1]=2&l=10&RNV=1&RN=&T[0]=2&Search=Search" target="_blank">BRENDA</a></a></td></tr>
		<tr><td colspan="3"><a href="http://www.pangaea.de/search?ie=UTF-8&count=10&q=parameter%3A%22Deinococcus sp.%22" target="_blank">PANGAEA</a></a></td></tr>
		<tr><td colspan="3"><a href="http://www.straininfo.net/strains/search?strainNumber=&exactStrain=true&taxon=Deinococcus sp." target="_blank">StrainInfo</a></td></tr>
		<tr><td colspan="3"><a href="http://www.gbif.org/species/search?q=Deinococcus sp." target="_blank">GBIF</a></td></tr>
	</table>
</div>
</div> 
	</div>
	<div id="content">
		<div>
												<div class="section">				
		    			<ul class="resultdetail">
		    				<li id="1" onclick="$('.id_1').toggle();" class="resultdetail_sectiontitle"><a class="tooltip"><span class="tooltip">Information on the name and the taxonomic classification.</span>Name and taxonomic classification</a><img class="arrow" src="images/pfeile.png" alt="arrow_down" /></li>
		        	  		<div class="anchor" id="section_1"></div>
		        	  		<table class="section_content"><tr><td>
			        	  						        	  				
										<table class="id_1 section expandsection">
																																																	<tr>
														<td class="firstcell"><a href="#ref20216">[Ref.: #20216]</a></td>
														<td class="bold_valigntop width180_valigntop">Domain</td>
																
															<td class="valigntop">Bacteria</td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20216">[Ref.: #20216]</a></td>
														<td class="bold_valigntop width180_valigntop">Genus</td>
																													<td class="valigntop"><i>Deinococcus</i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20216">[Ref.: #20216]</a></td>
														<td class="bold_valigntop width180_valigntop">Species</td>
																													<td class="valigntop"><i><i>Deinococcus</i> sp.<i></i></i></td>
																									
													</tr>
																										<tr>
														<td class="firstcell"><a href="#ref20216">[Ref.: #20216]</a></td>
														<td class="bold_valigntop width180_valigntop">Strain Designation</td>
																													<td class="valigntop"><i>ST027575(HKI)</i></td>
																									
													</tr>
													
												
										
											
											
																					</table>		
											
										<table class="id_1 section expandsection">
																							<th></th>
												<th></th>
												<th class="bold_valigntop paddingright"><a href="https://www.dsmz.de/bacterial-diversity/prokaryotic-nomenclature-up-to-date.html" target="_blank">Prokaryotic Nomenclature Up-to-date (PNU)</a></th>
												<th class="bold_valigntop paddingright">Taxonomical status</th>
												<th class="bold_valigntop paddingright">Literature reference</th>
																																																						<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																												
																	<td class="bold_valigntop width180_valigntop">Domain</td>
																																		
																	<td class="valigntop">Bacteria</td>
																															</tr>
																												
																																																																						<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop">Phylum</td>
																																																	<td class="valigntop"><i>Deinococcus-Thermus</i></td>
																															</tr>
																												
																																																																					
																													<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																												
																	<td class="bold_valigntop width180_valigntop">Class</td>
																	
															<td class="valigntop paddingright"><i>Deinococci</i></td>
																																																																					
																													<td></td>
															<td class="valigntop paddingright">Int. J. Syst. Evol. Microbiol. 52:685</td></tr>
																																																																					
																													<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop"> Family</td>
																	
															<td class="valigntop paddingright"><i>Deinococcaceae</i></td>
																																																																					
																																											<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																											
																<td class="bold_valigntop width180_valigntop">Genus</td>
																														<td class="valigntop paddingright"><i>Deinococcus</i></td>
																																																							
																																											<td class="valigntop paddingright">gen. nov. (VP) </td>
																																																							
																																											<td class="valigntop paddingright">Int. J. Syst. Bacteriol. 31:354*</td></tr>
																																																							
																																											<tr>
															<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																															<td class="bold_valigntop width180_valigntop"> Species</td>
																														<td class="valigntop paddingright"><i><i>Deinococcus</i> sp.<i></i></i></td>
																																																								<tr>
																<td class="firstcell"><a href="#ref20215">[Ref.: #20215]</a></td>
																																	<td class="bold_valigntop width180_valigntop">Full Scientific Name</td>
																																		
																	<td class="valigntop"><i>Deinococcus</i> sp.<i></i></td>
																															</tr>
																												
																																									
												
																					</table>		
									
									
											        	  	</td></tr></table>
		        	  	</ul>
		        	</div>	
		     																																														
		</div>
	</div>	
	<div id="reference">
<div id="references">
	<ul>
		<li class="resultdetail_sectiontitle">References</li>
					<li class="resultdetail_reference">
				<table class="resultdetail_reference">
					<tr>
						<td class="resultdetail_reference_refid"><a id="ref20215" name="ref20215"></a>#20215</td>
						<td class="resultdetail_reference_refdata">
																																		
																																													Leibniz Institute DSMZ 
																																													
																																													D.Gleim, M.Kracht, N.Weiss et. al.: 
																																																								<a href="http://www.dsmz.de/bacterial-diversity/prokaryotic-nomenclature-up-to-date.html" target="_blank">Prokaryotic Nomenclature Up-to-date - compilation of all names of Bacteria and Archaea, validly published according to the Bacteriological Code since 1. Jan. 1980, and validly published nomenclatural changes since</a>
										. 
																																													
																																													
																																													
																																													
																																			 
										 
																																													
																																																																																																																													
																																													
																										
													</td>
					</tr>
				</table>
			</li>
					<li class="resultdetail_reference">
				<table class="resultdetail_reference">
					<tr>
						<td class="resultdetail_reference_refid"><a id="ref20216" name="ref20216"></a>#20216</td>
						<td class="resultdetail_reference_refdata">
																																		
																																													Leibniz-Institut für Naturstoff-Forschung und Infektionsbiologie e. V. Hans-Knöll-Institut (HKI)
																																													
																																													Curators of the HKI: 
																																																								<a href="http://www.leibniz-hki.de/de/" target="_blank">Collection Description Leibniz-Institut für Naturstoff-Forschung und Infektionsbiologie e. V. Hans-Knöll-Institut (HKI)</a>
										. 
																																													
																																													
																																													
																																													
																																			 
										 
																																													
																																																																																																																													
																																													
																										
													</td>
					</tr>
				</table>
			</li>
		
		<li class="resultdetail_referencetextmining">* These References are textmined</li>
	</ul>
</div>

</div>
</div> 

        </div>
    </div>
	

<script type="text/javascript">
/* <![CDATA[ */
/*
window.onbeforeunload = function (e)
{
	var message = "Attention: You have some not commited items in the exportcart. Are you sure you want to exit this page without commiting?",
	e = e || window.event;
	// For IE and Firefox
	if(checkdlcartempty() != true)
	{
		if (e) { e.returnValue = message; }
		// For Safari
		return message;
	}
};
*/

$(window).on('beforeunload',function() {
	var message = "Attention: You have some not commited items in the exportcart. Are you sure you want to exit this page without commiting?";
	if(checkdlcartempty() != true && dlcartsaved != true)
	{
		ModalWindow(message);
		return message;
	}
});

// fill placeholder property in inputfield, if exists


if($('#searchValue') != undefined)
{
	$('#searchValue').prop("placeholder", "species name, culture col. no., sequence no.");
	$('#searchValue').prop("autocomplete", "off");
}


// for Download Selection Cart
if (($("[name='dlcart']").length != 0) && ($("[name='dlcart']").length != undefined))
{
	for (var i=0; i< $("[name='dlcart']").length;i++)
	{
		$("[name='dlcart']")[i].onclick=function(){Item2Cart(this.id);};
		//$("[name='dlcart']")[i].onchange=function()\{alert(this.id);};
		//eventListener(document.getElementsByName("dlcart")[i].id, alert(i));	
	}
}

//on outside click - close suggest, on inside click - open suggest
if($('#pagetop') != undefined) $('#pagetop').click(function() {closeSuggest(); });
if($('#searchValue') != undefined) $('#searchValue').click(function() {suggestSearch();})


$(document).ready(function(){
	// Slick Slide
	if ($('.propagateme').length > 0){					
		  $('.propagateme').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 3000,
		  accessibility:true,
		  dots:true,
		  fade:true,
		  infinite:true,
		  pauseOnHover:true,
		  pauseOnDotsHover:true,
		  variableWidth:false,
		  centerMode:true,
		  });
	}	
	
	//to sort the table				
	$("#enzyme_table").tablesorter(); 
    $(".halophily_table").tablesorter(); 
    $("#met_antibiotica_nogroup").tablesorter();
    $("#met_antibiotica_group").tablesorter();
    $("#met_production").tablesorter(); 
    $("#met_test").tablesorter(); 
    $("#met_util").tablesorter(); 
    $("#temp_table").tablesorter(); 
    $("#ph_table").tablesorter(); 
    $("#sequence_table").tablesorter(); 	  
    $(".fa_table").tablesorter(); 
															
});








/* ]]> */
</script>


</body>
</html>