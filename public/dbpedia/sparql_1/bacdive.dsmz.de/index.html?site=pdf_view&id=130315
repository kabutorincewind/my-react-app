
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang='de' lang='de'>
<head>
<title>BacDive - The Bacterial Diversity Metadatabase </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="google-site-verification" content="mIOYHrpUl_1G65CeMEFGWkETjn4L82yOiWWvhY3Zo04" />
<meta name="expires" content="0" />
<meta http-equiv="expires" content="0" />
<meta name="pragma" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="cache-control" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="description" content="test" />

<meta name="abstract" content="BacDive - The Bacterial Diversity Metadatabase" />
<meta name="Content-Language" content="en" />
<meta name="Revisit-After" content="1 days" />
<meta name="author" content="Adam Podstawka" />
<meta name="Copyright" content="&copy; Adam Podstawka" />
<meta name="Publisher" content="Leibnitz-Institut DSMZ" />
<meta name="Page-Type" content="research" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="favicon.ico" />




<link rel="stylesheet" href="styles/style.css" type="text/css" />
<link rel="stylesheet" href="styles/bar.css" type="text/css" />
<link rel="stylesheet" href="styles/style.pdf.css" type="text/css" />
<link rel="stylesheet" href="styles/suggest.css" type="text/css" />

<link rel="stylesheet" href="styles/style.chooseref.css" type="text/css" />
<link rel="stylesheet" href="styles/style.kitapi.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="styles/bootstrap_pagination.css"/>



<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/helper.js"></script>
<script type="text/javascript" src="js/suggested.js"></script>
<script type="text/javascript" src="js/dlcart.js"></script> 

<script type="text/javascript" src="js/jquery.tablesorter.js"></script> 
<script type="text/javascript" src="js/angular_v1.5.8.min.js"></script>

<script type="text/javascript" src="js/dirPagination.js"></script>
<script type="text/javascript" src="js/smart-table.min.js"></script>
<script type="text/javascript" src="apisearch/apitableCtrl.js"></script>















<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(["setCookieDomain", "*.bacdive.dsmz.de"]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://piwik.dsmz.de/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->



</head>

<body id="pagetop">
    <div id="topbar">
		
<div id="topbarcontent" >
    <div id="logo"><a href="./"><img src="images/bacdivelogo_high.png" alt="BacDive - The Bacterial Diversity Metadatabase" title="BacDive - The Bacterial Diversity Metadatabase" class="noBorder"/></a></div>
    <div id='announce'><a href="?site=news#survey" target='blank'>User satisfaction survey</a><img class="icon" src="images/good-idea_12.png" alt="icon for survey" /></div>
    
    <div  id="navicontainer">
        <ul class="links">
            
            <li><a href="help" onclick="return popup(this.href,'help', 1024, 768);" alt='help' title="help">help</a></li>
            <li><a href="?site=contact" alt="contact" title="contact">about</a></li>
            <li><a href="?site=tutorials" alt="tutorials" title="tutorials">tutorials</a></li>
            <li><a href="?site=news" alt="news" title="news">news</a></li>
            <li><a href="mailto:contact@bacdive.de?subject=Feedback for BacDive - You're missing something or something is wrong? Tell us!" alt="link for writing an email for feedback" title="feedback">contact</a></li>
        </ul>
    </div>
    
    <div id="denbi"><a href="https://www.denbi.de/">
        <div> member of </div>
        <img src="images/deNBI_Logo_rgb.png" /> 
    </a></div>
</div>

<div id="navbar">
   <div id = "navigation">
           <ul class="links">
                
                
                <li><a class="navlink" href="https://bacdive.dsmz.de/api/" alt="link to webservices" title="webservices" target="_blank">web services</a></li>
                <li><a class="navlink" href="?site=dlselect" alt="download selection" title="download selection">download selection </a></li>
                
                <li><a class="navlink" href="?site=advsearch"alt="advanced search function" title="advanced search">advanced search</a></li>
                <li><a class="navlink" href="?site=strains" alt="list of strains" title="browse through taxonomy">TAXplorer</a></li>
                <li><a class="navlink" href="?site=apisearch" alt="apikit search function" title="search through api">API &reg; test finder</a></li>
           </ul>
    </div>
    <div id="searchtop">
        <div>
            <form method="GET" action="index.php?site=search">
                <input class="font10" id="searchValue" type="text" name="search"  onkeyup="suggestSearch();" value="" />
                <input class="font10" id="submit" type="submit" name="submit" value="Search" /> 
                <a href="#" class="tooltip">
                    <span>search over all mainfields in database</span>
                    <input class="font10" id="searchAnyField" type="checkbox" name="anyfield" value="checked"   />
                </a>
                <span id="suggested"></span>
            </form>
        </div>
    </div>
</div>

    </div>
    <div id="middle_content">
        <div class="inhalt_start">
			
<div id="pdf_view">
    <div id="infoboxbar">
<div id="infobox">
	<table class="paddingleft">
		<tr>
			<td rowspan="2" class="width90 infobox_key blue">Strain identifier</td>
			<td class="infobox_key">BacDive ID:</td><td class="width72">130315</td>
			
			<td class="infobox_key">Species:</td><td class="cell">Xenorhabdus indica</td>			
			<td class="infobox_key width150">Strain Designation:</td><td class="cell">157-C, SF157-C</td>		</tr>
		<tr>
			<td class="infobox_key">Type strain:</td><td class="width72"><img src="images/remove.png" alt="No" title="No" width="13" height="13" /></td>			<td class="infobox_key widthcell">Culture col. no.:</td><td class="cell widthmin" colspan="4"><a href="https://www.dsmz.de/catalogues/details/culture/DSM-26379.html" target="_blank">DSM 26379</a></td>		</tr>
	</table>
</div>
</div>
    
    <div id="pdf_content">
         <form  method="get" action="index.php?site=pdf_view">
             <div id="version">
                 <input class="hidden" name="site" value="pdf_view"/>
                 <input class="hidden" name="id" value="130315"/>
                 <div class="header font8"><a href="index.php?search=130315">&laquo;&nbsp;back to strain detail view</a></div>
                 <div id="citate" class="pdfarchive_header">For citation purpose refer to the digital object identifier (doi) of the <b>current version</b>.</div>
                 <div class="pdfarchive_header">PDF archive</div>
                                                             <div class="paddingleft">version&nbsp;2.1&nbsp;(<i>current version</i>):</div>
                        <div class="paddingleft"><input type="submit" name="doi"  value="doi:10.13145/bacdive130315.20171208.2.1" /></div> 
                                                                                 <div class="paddingleft">version&nbsp;2.0:<input type="submit" name="doi"  value="doi:10.13145/bacdive130315.20170829.2" /></div>               
                                                                                 <div class="paddingleft">version&nbsp;1.0:<input type="submit" name="doi"  value="doi:10.13145/bacdive130315.20170425.1" /></div>               
                                     
             </div>   
         </form>
                 
        <div id="pdffile">    
                            <div class="header">version&nbsp;2.1&nbsp;(<i>current version</i>)</div>
                <iframe src="pdf.php?id=130315&amp;doi=10.13145/bacdive130315.20171208.2.1" type="application/pdf"></iframe>   
                     </div>    
    </div>
    
</div>
        </div>
    </div>
	

<script type="text/javascript">
/* <![CDATA[ */
/*
window.onbeforeunload = function (e)
{
	var message = "Attention: You have some not commited items in the exportcart. Are you sure you want to exit this page without commiting?",
	e = e || window.event;
	// For IE and Firefox
	if(checkdlcartempty() != true)
	{
		if (e) { e.returnValue = message; }
		// For Safari
		return message;
	}
};
*/

$(window).on('beforeunload',function() {
	var message = "Attention: You have some not commited items in the exportcart. Are you sure you want to exit this page without commiting?";
	if(checkdlcartempty() != true && dlcartsaved != true)
	{
		ModalWindow(message);
		return message;
	}
});

// fill placeholder property in inputfield, if exists


if($('#searchValue') != undefined)
{
	$('#searchValue').prop("placeholder", "species name, culture col. no., sequence no.");
	$('#searchValue').prop("autocomplete", "off");
}


// for Download Selection Cart
if (($("[name='dlcart']").length != 0) && ($("[name='dlcart']").length != undefined))
{
	for (var i=0; i< $("[name='dlcart']").length;i++)
	{
		$("[name='dlcart']")[i].onclick=function(){Item2Cart(this.id);};
		//$("[name='dlcart']")[i].onchange=function()\{alert(this.id);};
		//eventListener(document.getElementsByName("dlcart")[i].id, alert(i));	
	}
}

//on outside click - close suggest, on inside click - open suggest
if($('#pagetop') != undefined) $('#pagetop').click(function() {closeSuggest(); });
if($('#searchValue') != undefined) $('#searchValue').click(function() {suggestSearch();})


$(document).ready(function(){
	// Slick Slide
	if ($('.propagateme').length > 0){					
		  $('.propagateme').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 3000,
		  accessibility:true,
		  dots:true,
		  fade:true,
		  infinite:true,
		  pauseOnHover:true,
		  pauseOnDotsHover:true,
		  variableWidth:false,
		  centerMode:true,
		  });
	}	
	
	//to sort the table				
	$("#enzyme_table").tablesorter(); 
    $(".halophily_table").tablesorter(); 
    $("#met_antibiotica_nogroup").tablesorter();
    $("#met_antibiotica_group").tablesorter();
    $("#met_production").tablesorter(); 
    $("#met_test").tablesorter(); 
    $("#met_util").tablesorter(); 
    $("#temp_table").tablesorter(); 
    $("#ph_table").tablesorter(); 
    $("#sequence_table").tablesorter(); 	  
    $(".fa_table").tablesorter(); 
															
});








/* ]]> */
</script>


</body>
</html>