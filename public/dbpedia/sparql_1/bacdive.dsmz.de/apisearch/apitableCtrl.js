/**
 * @author		Anna Vetcininova
 * @version	    1.18
 * @date		2017-06-15
 * @lastchange	2017-10-13
 * @lastchangeby Anna Vetcininova
 * @copyright	Copyright (c) 2017 DSMZ, Anna Vetcininova
 * @package	    bacdive.apisearch
 *
 */

var app = angular.module('apitableApp', [ 'angularUtils.directives.dirPagination','smart-table']);



app.controller('apitableCtrl', ['$scope','$http','$cacheFactory', function (scope, http, cacheFactory) {	 

	//cache Objekt 
	scope.cache = cacheFactory('cacheID'); 
	
	//displayed apis
	scope.apis = {
			  kit_api_20A         : "API 20A",
			  kit_api_20E         : "API 20E",
			  kit_api_20NE        : "API 20NE",
			  kit_api_20STR       : "API 20STR",
			  kit_api_50CHac      : "API 50CH acid",
			  kit_api_50CHas      : "API 50CH assim",
			  kit_api_CAM		  : "API CAMPY",
			  /*kit_api_ID32E     : "ID32E",*/
			  kit_api_ID32STA     : "API ID32STA",
			  kit_api_LIST        : "API LIST",
			  kit_api_NH          : "API NH",
			  kit_api_STA         : "API STA",
			  kit_api_coryne      : "API Coryne",
			  kit_api_rID32A      : "API rID32A",
			  kit_api_rID32STR    : "API rID32STR",
			  kit_api_zym         : "API zym"
	};	
	//order
	scope.reverse = 'ASC';
	
	//error message
	scope.errorMessage = '';
	
	
	//pagination 
    scope.pageSizeChanged = function() {
        scope.currentPage = 1;       
        getData(scope.currentPage,scope.sortCol);
    };
    scope.pageChangeHandler = function(num) {
    	getData(num,scope.sortCol);
    };
    
    //sort function
    scope.sortBy = function(propertyName) {
    	scope.currentPage = 1;
    	if(scope.sortCol == propertyName){
    		if(scope.reverse=='ASC')	scope.reverse='DESC';
    		else 					 	scope.reverse='ASC';
    	}  
    	scope.sortCol=propertyName;
    	getData(scope.currentPage,propertyName);		
    };

    
    
    //search filter 
    //regex expressions for the validation of user inputs
    var regexpApi = /^[+-]+$/;
    var regexpNo = /^[0-9\s]+$/;
    var regexpChar = /^[a-zA-Z\s]+$/;
    var regexpCharNo = /^[a-zA-Z0-9\s]+$/;
    
    scope.searchTextChanged = function(column,searchterm) {	
    	console.log(column)
    	console.log(searchterm)    	
    	if(column == 'ID' || column == 'BacDive_ID'){
    		if(regexpNo.test(searchterm)){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    		else if(searchterm==''){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    	}
    	else if(column == 'species' || column == 'Type_strain'){
    		if(regexpChar.test(searchterm)){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    		else if(searchterm==''){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    	}
    	else if(column == 'Collection_numbers'){
    		if(regexpCharNo.test(searchterm)){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    		else if(searchterm==''){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    	}
    	else if(column != 'ID' && column != 'BacDive_ID' && column != 'species' && column != 'Collection_numbers'){
    		if(regexpApi.test(searchterm)){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    		else if(searchterm==''){
    			apikit=scope.selectedApi;		
    	    	scope.currentPage = 1;
    	    	getData(scope.currentPage, scope.sortCol);
    		}
    	}
    		
    };
	   
    
	
    //reset filter
    scope.resetFilter = function(){
    	scope.input={};							//delete the inputs of last apitable
		scope.errorMessage = '';
		scope.currentPage = 1;
	    scope.totalItems = 0;
	    getData(scope.currentPage);	  
    }
    
    
    
	//build table after choose apitest
	scope.buildTable = function(){   		
		scope.cache.destroy();					//destroy cache when apitable changed
   	 	scope.cache = cacheFactory('cacheID');  //new cache object for actual apitable get generated 	 	
		scope.input={};							//delete the inputs of last apitable
		scope.errorMessage = '';
		scope.currentPage = 1;
	    scope.totalItems = 0;
	    scope.pageSize = 10;
	    getData(scope.currentPage);	  
	};
	
	//show data from session if exists
	if(window.sessionStorage.length!=0){
		scope.selectedApi=window.sessionStorage.getItem("apitable");
		scope.pageSize=window.sessionStorage.getItem("size");
		scope.currentPage=window.sessionStorage.getItem("page");
		scope.sortCol=window.sessionStorage.getItem("sort");
		scope.reverse=window.sessionStorage.getItem("reverse");
		scope.input=JSON.parse(window.sessionStorage.getItem("filter"));
		scope.totalItems=window.sessionStorage.getItem("totalItems");
        scope.startItem=window.sessionStorage.getItem("startItem");
        scope.endItem=window.sessionStorage.getItem("endItem");
        
        scope.testresults=JSON.parse(window.sessionStorage.getItem("data"));
        scope.header=JSON.parse(window.sessionStorage.getItem("header"));	
        scope.headerInput=JSON.parse(window.sessionStorage.getItem("headerInput"));
	}

	//show results on the page
	function getData(currentPage,orderColumn) {
	//for the post method
	//  scope.url='apisearch/apisearch.php';
	//  data = {"apitable":scope.selectedApi,	"page":currentPage,	"size":scope.pageSize,	"sort":orderColumn,	"reverse":scope.reverse, "filter":scope.input};	
	//  http.post(scope.url, data)

		scope.startItem = (currentPage - 1) * scope.pageSize + 1;
		if (scope.endItem > scope.totalCount) 	{scope.endItem = scope.totalCount;}
        else									{scope.endItem = currentPage * scope.pageSize; }		
		
		
		//code input from JSON into string
		json2url_params=encodeURIComponent(JSON.stringify(scope.input));
		console.log("here");
		console.log(scope.input);
		
		//the url is the key for the cache --> cache is stored in pairs of(key,value) --> each user interaction get stored in cache
		url='/apisearch/apisearch.php?apitable='+scope.selectedApi +'&page=' + currentPage + '&size=' + scope.pageSize + '&sort=' + orderColumn+'&reverse='+scope.reverse+'&filter='+json2url_params;	
	    
		//cached data of the key stored as url
		cached_data=scope.cache.get(url);    
//	    console.log(cached_data);
		
	    // check if data is already cached
	    if(!cached_data){
	    	http.get(url , {params:{"filter": scope.input}})
	    	.success(function(data) {
		            scope.totalItems = data.totalCount;
		            scope.testresults=data.data;  
		            scope.header=data.header;
		            scope.headerInput=data.headerInput;

		    		if(scope.totalItems == 0)	{scope.errorMessage="Nothing found."}
		    		else 						{scope.errorMessage=""}

		            //cache data
		            scope.cache.put(url, data); 		
		            
		            //store data in a session
		            window.sessionStorage.setItem("apitable",scope.selectedApi);
		            window.sessionStorage.setItem("size",scope.pageSize);
		            window.sessionStorage.setItem("page",currentPage);
		            window.sessionStorage.setItem("sort",orderColumn);
		            window.sessionStorage.setItem("reverse",scope.reverse);
		            window.sessionStorage.setItem("filter",JSON.stringify(scope.input));		            
		            window.sessionStorage.setItem("totalItems",scope.totalItems);
		            window.sessionStorage.setItem("startItem",scope.startItem);
		            window.sessionStorage.setItem("endItem",scope.endItem);		
		            window.sessionStorage.setItem("endItem",scope.endItem);		            
		            window.sessionStorage.setItem("data",JSON.stringify(data.data));	
		            window.sessionStorage.setItem("header",JSON.stringify(data.header));
		            window.sessionStorage.setItem("headerInput",JSON.stringify(data.headerInput));
		     });		     	
	    	
	    }
	    else{ //use cached data
	        scope.totalItems = cached_data.totalCount;

	        if(scope.totalItems == 0)	{scope.errorMessage="Nothing found."}
    		else 						{scope.errorMessage=""}
	        scope.testresults=cached_data.data;  
	        scope.header=cached_data.header;
	        scope.headerInput=cached_data.headerInput;
	    }
		
	    // cache will be destroyed if the size is 15
	    if(scope.cache.info().size == 15){
	    	 scope.cache.destroy();
	    	 scope.cache = cacheFactory('cacheID'); 
        };  	
    }; 
    
   
  
}]);


//to move the cursor in inputfields forward, when click on return or arrows keys
app.directive('navigatable', function(){
	return function(scope, element, attr) {
		element.on('keypress.mynavigation', 'input[type="search"]', handleNavigation);
        
		function handleNavigation(e){
 			var arrow = { left: 37, up: 38, right: 39, down: 40 };

			// select all on focus
			element.find('input').keydown(function (e) {

				// shortcut for key other than arrow keys
				if ($.inArray(e.which, [arrow.left, arrow.up, arrow.right, arrow.down]) < 0) { return; }

				var input = e.target;
				var td = $(e.target).closest('th');
				var moveTo = null;

				switch (e.which) {

					case arrow.left: {
						if (input.selectionStart == 0) {
							moveTo = td.prev('th:has(input,textarea)');
						}
						break;
					}
		            case arrow.right: {
		                if (input.selectionEnd == input.value.length) {
		                    moveTo = td.next('th:has(input,textarea)');
		                }
		                break;
		            }

		            case arrow.up:
		            case arrow.down: {
		
		                var tr = td.closest('tr');
		                var pos = td[0].cellIndex;
		
		                var moveToRow = null;
		                if (e.which == arrow.down) {
		                    moveToRow = tr.next('tr');
		                }
		                else if (e.which == arrow.up) {
		                    moveToRow = tr.prev('tr');
		                }
		                if (moveToRow.length) {
		                    moveTo = $(moveToRow[0].cells[pos]);
		                }
		                break;
		            }
				}
		        if (moveTo && moveTo.length) {
		
		            e.preventDefault();
		
		            moveTo.find('input,textarea').each(function (i, input) {
		                input.focus();
		                input.select();
		            });
		        }
			});
    
    
			var key = e.keyCode ? e.keyCode : e.which;
			if(key === 13){
				var focusedElement = $(e.target);
				var nextElement = focusedElement.parent().next();
				if (nextElement.find('input').length>0){
					nextElement.find('input').focus();
				}else{
					nextElement = nextElement.parent().next().find('input').first();
					nextElement.focus();
				}
			}
		}
    };
})





	