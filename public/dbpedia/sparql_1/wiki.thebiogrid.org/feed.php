<?xml version="1.0" encoding="utf-8"?>
<!-- generator="FeedCreator 1.7.2-ppt DokuWiki" -->
<?xml-stylesheet href="http://wiki.thebiogrid.org/lib/exe/css.php?s=feed" type="text/css"?>
<rdf:RDF
    xmlns="http://purl.org/rss/1.0/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    xmlns:dc="http://purl.org/dc/elements/1.1/">
    <channel rdf:about="http://wiki.thebiogrid.org/feed.php">
        <title>BioGRID Help and Support Resources</title>
        <description></description>
        <link>http://wiki.thebiogrid.org/</link>
        <image rdf:resource="http://wiki.thebiogrid.org/lib/tpl/biogrid3/images/favicon.ico" />
       <dc:date>2018-01-17T21:26:24-05:00</dc:date>
        <items>
            <rdf:Seq>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/curation_guide:biochemical_experimental_systems?rev=1516147804&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/statistics?rev=1514782994&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/build_3.4.156?rev=1514782925&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/sibley2017?rev=1513126879&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/build_3.4.155?rev=1512105583&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/orcs:statistics?rev=1511058315&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/build_3.4.154?rev=1509511778&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/build_3.4.153?rev=1506834637&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/orcs:filter_syntax?rev=1506222929&amp;do=diff"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=genome_quebec.jpg&amp;ns=&amp;rev=1441088551&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=genome_canada.jpg&amp;ns=&amp;rev=1441088091&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=genomequebec.jpg&amp;ns=&amp;rev=1441087796&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=genomecanada.jpg&amp;ns=&amp;rev=1441087796&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=nih2.jpg&amp;ns=&amp;rev=1433263123&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=nih-logo.jpg&amp;ns=&amp;rev=1433262921&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=fontawesome.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=cytoscapepanzoom.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=cytoscapejs.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=cytoscapectxmenu.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
                <rdf:li rdf:resource="http://wiki.thebiogrid.org/doku.php/?image=arborjs.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media"/>
            </rdf:Seq>
        </items>
    </channel>
    <image rdf:about="http://wiki.thebiogrid.org/lib/tpl/biogrid3/images/favicon.ico">
        <title>BioGRID Help and Support Resources</title>
        <link>http://wiki.thebiogrid.org/</link>
        <url>http://wiki.thebiogrid.org/lib/tpl/biogrid3/images/favicon.ico</url>
    </image>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/curation_guide:biochemical_experimental_systems?rev=1516147804&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2018-01-16T19:10:04-05:00</dc:date>
        <dc:creator>jenn</dc:creator>
        <title>Physical Experimental Systems - [Physical Interactions] </title>
        <link>http://wiki.thebiogrid.org/doku.php/curation_guide:biochemical_experimental_systems?rev=1516147804&amp;do=diff</link>
        <description>Physical Experimental Systems

For each interaction entered in the tool, an experimental system must be selected.  The different types of physical experimental systems are described in the table below.

Physical Interactions
  Type   Description</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/statistics?rev=1514782994&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2018-01-01T00:03:14-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>BioGRID Database Statistics</title>
        <link>http://wiki.thebiogrid.org/doku.php/statistics?rev=1514782994&amp;do=diff</link>
        <description>BioGRID Database Statistics

This page shows the latest snapshot of statistics for the BioGRID database. All previous snapshots are archived below separated by year. 

	*  Raw Interactions -  Each unique combination of interactors A and B, experimental system and publication is counted as a single interaction. Reciprocal interactions (A → B and B → A) are counted twice.</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/build_3.4.156?rev=1514782925&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2018-01-01T00:02:05-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>Build Statistics (3.4.156) - January 2018 - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/build_3.4.156?rev=1514782925&amp;do=diff</link>
        <description>Build Statistics (3.4.156) - January 2018

This page shows the statistics for the BioGRID database as was established via the above referenced data and release version. If you are interested in accessing the data from this exact build, please visit our</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/sibley2017?rev=1513126879&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2017-12-12T20:01:19-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>Sibley (2017)</title>
        <link>http://wiki.thebiogrid.org/doku.php/sibley2017?rev=1513126879&amp;do=diff</link>
        <description>Sibley (2017)

Title: A conserved ankyrin repeat-containing protein regulates conoid stability, motility and cell invasion in Toxoplasma gondii.

Authors: Shaojun Long, Bryan Anthony, Lisa L. Drewry &amp; L.David Sibley

Downloads: Microsoft Excel Format (XLS)</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/build_3.4.155?rev=1512105583&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2017-12-01T00:19:43-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>Build Statistics (3.4.155) - December 2017 - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/build_3.4.155?rev=1512105583&amp;do=diff</link>
        <description>Build Statistics (3.4.155) - December 2017

This page shows the statistics for the BioGRID database as was established via the above referenced data and release version. If you are interested in accessing the data from this exact build, please visit our</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/orcs:statistics?rev=1511058315&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2017-11-18T21:25:15-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>BioGRID ORCS Database Statistics - [CRISPR Screen Statistics] </title>
        <link>http://wiki.thebiogrid.org/doku.php/orcs:statistics?rev=1511058315&amp;do=diff</link>
        <description>BioGRID ORCS Database Statistics

This page shows the latest snapshot of statistics for the BioGRID ORCS database. All previous snapshots are archived below separated by year. 

Current Build Statistics (0.2.8Beta) - October 2017

CRISPR Screen Statistics</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/build_3.4.154?rev=1509511778&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2017-11-01T00:49:38-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>Build Statistics (3.4.154) - November 2017</title>
        <link>http://wiki.thebiogrid.org/doku.php/build_3.4.154?rev=1509511778&amp;do=diff</link>
        <description>Build Statistics (3.4.154) - November 2017

This page shows the statistics for the BioGRID database as was established via the above referenced data and release version. If you are interested in accessing the data from this exact build, please visit our</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/build_3.4.153?rev=1506834637&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2017-10-01T01:10:37-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>Build Statistics (3.4.153) - October 2017 - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/build_3.4.153?rev=1506834637&amp;do=diff</link>
        <description>Build Statistics (3.4.153) - October 2017

This page shows the statistics for the BioGRID database as was established via the above referenced data and release version. If you are interested in accessing the data from this exact build, please visit our</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/orcs:filter_syntax?rev=1506222929&amp;do=diff">
        <dc:format>text/html</dc:format>
        <dc:date>2017-09-23T23:15:29-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>Result Advanced Filter Search Syntax - [Examples] </title>
        <link>http://wiki.thebiogrid.org/doku.php/orcs:filter_syntax?rev=1506222929&amp;do=diff</link>
        <description>Result Advanced Filter Search Syntax

When utilizing BioGRID ORCS advanced filters, you have the option of applying simple boolean options to perform more advanced searches. First each individual field acts as an AND search. So, for instance, if you search for</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=genome_quebec.jpg&amp;ns=&amp;rev=1441088551&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-09-01T02:22:31-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>genome_quebec.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=genome_quebec.jpg&amp;ns=&amp;rev=1441088551&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/genome_quebec.jpg?w=500&amp;h=318&amp;t=1502211168&amp;amp;tok=f2dfc3&quot; alt=&quot;genome_quebec.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=genome_canada.jpg&amp;ns=&amp;rev=1441088091&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-09-01T02:14:51-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>genome_canada.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=genome_canada.jpg&amp;ns=&amp;rev=1441088091&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/genome_canada.jpg?w=500&amp;h=318&amp;t=1502211168&amp;amp;tok=42b521&quot; alt=&quot;genome_canada.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=genomequebec.jpg&amp;ns=&amp;rev=1441087796&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-09-01T02:09:56-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>genomequebec.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=genomequebec.jpg&amp;ns=&amp;rev=1441087796&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/genomequebec.jpg?w=125&amp;h=59&amp;t=1502211168&amp;amp;tok=f480ca&quot; alt=&quot;genomequebec.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=genomecanada.jpg&amp;ns=&amp;rev=1441087796&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-09-01T02:09:56-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>genomecanada.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=genomecanada.jpg&amp;ns=&amp;rev=1441087796&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/genomecanada.jpg?w=125&amp;h=59&amp;t=1502211168&amp;amp;tok=8e7d4b&quot; alt=&quot;genomecanada.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=nih2.jpg&amp;ns=&amp;rev=1433263123&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-06-02T12:38:43-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>nih2.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=nih2.jpg&amp;ns=&amp;rev=1433263123&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/nih2.jpg?w=125&amp;h=60&amp;t=1502211168&amp;amp;tok=528b03&quot; alt=&quot;nih2.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=nih-logo.jpg&amp;ns=&amp;rev=1433262921&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-06-02T12:35:21-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>nih-logo.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=nih-logo.jpg&amp;ns=&amp;rev=1433262921&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/nih-logo.jpg?w=250&amp;h=250&amp;t=1502211168&amp;amp;tok=fd9834&quot; alt=&quot;nih-logo.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=fontawesome.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-04-09T23:34:48-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>fontawesome.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=fontawesome.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/fontawesome.jpg?w=125&amp;h=60&amp;t=1502211168&amp;amp;tok=03c4a1&quot; alt=&quot;fontawesome.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=cytoscapepanzoom.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-04-09T23:34:48-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>cytoscapepanzoom.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=cytoscapepanzoom.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/cytoscapepanzoom.jpg?w=125&amp;h=60&amp;t=1502211168&amp;amp;tok=3d0bec&quot; alt=&quot;cytoscapepanzoom.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=cytoscapejs.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-04-09T23:34:48-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>cytoscapejs.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=cytoscapejs.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/cytoscapejs.jpg?w=125&amp;h=60&amp;t=1502211168&amp;amp;tok=17555a&quot; alt=&quot;cytoscapejs.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=cytoscapectxmenu.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-04-09T23:34:48-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>cytoscapectxmenu.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=cytoscapectxmenu.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/cytoscapectxmenu.jpg?w=125&amp;h=60&amp;t=1502211168&amp;amp;tok=c6367b&quot; alt=&quot;cytoscapectxmenu.jpg&quot; /&gt;</description>
    </item>
    <item rdf:about="http://wiki.thebiogrid.org/doku.php/?image=arborjs.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media">
        <dc:format>text/html</dc:format>
        <dc:date>2015-04-09T23:34:48-05:00</dc:date>
        <dc:creator>biogridadmin</dc:creator>
        <title>arborjs.jpg - created</title>
        <link>http://wiki.thebiogrid.org/doku.php/?image=arborjs.jpg&amp;ns=&amp;rev=1428636888&amp;tab_details=history&amp;mediado=diff&amp;do=media</link>
        <description>&lt;img src=&quot;http://wiki.thebiogrid.org/lib/exe/fetch.php/arborjs.jpg?w=125&amp;h=60&amp;t=1502211168&amp;amp;tok=d7759d&quot; alt=&quot;arborjs.jpg&quot; /&gt;</description>
    </item>
</rdf:RDF>
