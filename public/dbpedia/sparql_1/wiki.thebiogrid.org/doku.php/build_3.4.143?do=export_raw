====== Build Statistics (3.4.143) - December 2016 ======
This page shows the statistics for the BioGRID database as was established via the above referenced data and release version. If you are interested in accessing the data from this exact build, please visit our **[[http://thebiogrid.org/download.php|download page]]** and find the correct version under "Release Archive". It is highly recommended that all new projects use the latest BioGRID release when possible.

  * **Raw Interactions** -  Each unique combination of interactors A and B, experimental system and publication is counted as a single interaction. Reciprocal interactions (A → B and B → A) are counted twice.
  * **Non-Redundant Interactions** - Each unique combination of interactors A and B are counted as a single interaction, regardless of directionality,  experimental system and publication.

===== Physical and Genetic Interaction Statistics =====
^  Organism  ^  Experiment Type  ^  Raw Interactions  ^  Non-Redundant Interactions  ^  Unique Genes  ^  Unique Publications  ^
|  **//Anopheles gambiae (PEST)//**  |  PHYSICAL  |  2  |  1  |  2  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  1  |  2  |  2  |
|  **//Apis mellifera//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Arabidopsis thaliana (Columbia)//**  |  PHYSICAL  |  42,286  |  35,667  |  9,527  |  2,226  |
|  :::  |  GENETIC  |  341  |  274  |  291  |  146  |
|  :::  |  COMBINED  |  42,627  |  35,858  |  9,582  |  2,305  |
|  **//Bacillus subtilis (168)//**  |  PHYSICAL  |  2  |  2  |  3  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  3  |  2  |
|  **//Bos taurus//**  |  PHYSICAL  |  421  |  382  |  408  |  168  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  421  |  382  |  408  |  168  |
|  **//Caenorhabditis elegans//**  |  PHYSICAL  |  6,341  |  5,797  |  3,277  |  190  |
|  :::  |  GENETIC  |  2,332  |  2,265  |  1,127  |  33  |
|  :::  |  COMBINED  |  8,673  |  8,039  |  3,950  |  207  |
|  **//Candida albicans (SC5314)//**  |  PHYSICAL  |  148  |  115  |  129  |  39  |
|  :::  |  GENETIC  |  276  |  269  |  272  |  9  |
|  :::  |  COMBINED  |  424  |  382  |  379  |  47  |
|  **//Canis familiaris//**  |  PHYSICAL  |  43  |  32  |  49  |  22  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  43  |  32  |  49  |  22  |
|  **//Cavia porcellus//**  |  PHYSICAL  |  7  |  5  |  9  |  5  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  7  |  5  |  9  |  5  |
|  **//Chlamydomonas reinhardtii//**  |  PHYSICAL  |  15  |  14  |  16  |  3  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  15  |  14  |  16  |  3  |
|  **//Chlorocebus sabaeus//**  |  PHYSICAL  |  4  |  3  |  4  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  4  |  3  |  4  |  1  |
|  **//Cricetulus griseus//**  |  PHYSICAL  |  20  |  20  |  25  |  6  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  20  |  20  |  25  |  6  |
|  **//Danio rerio//**  |  PHYSICAL  |  218  |  192  |  187  |  35  |
|  :::  |  GENETIC  |  70  |  64  |  65  |  30  |
|  :::  |  COMBINED  |  288  |  253  |  245  |  64  |
|  **//Dictyostelium discoideum (AX4)//**  |  PHYSICAL  |  24  |  17  |  20  |  7  |
|  :::  |  GENETIC  |  6  |  6  |  8  |  2  |
|  :::  |  COMBINED  |  30  |  20  |  24  |  8  |
|  **//Drosophila melanogaster//**  |  PHYSICAL  |  38,642  |  37,538  |  8,239  |  456  |
|  :::  |  GENETIC  |  9,979  |  2,826  |  1,042  |  1,482  |
|  :::  |  COMBINED  |  48,621  |  40,214  |  8,372  |  1,880  |
|  **//Emericella nidulans (FGSC A4)//**  |  PHYSICAL  |  65  |  57  |  57  |  3  |
|  :::  |  GENETIC  |  6  |  5  |  7  |  3  |
|  :::  |  COMBINED  |  71  |  62  |  64  |  6  |
|  **//Equus caballus//**  |  PHYSICAL  |  2  |  2  |  4  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  4  |  2  |
|  **//Escherichia coli (K12/MG1655)//**  |  PHYSICAL  |  103  |  100  |  104  |  15  |
|  :::  |  GENETIC  |  26  |  25  |  36  |  12  |
|  :::  |  COMBINED  |  129  |  124  |  138  |  27  |
|  **//Escherichia coli (K12/W3110)//**  |  PHYSICAL  |  6  |  3  |  4  |  2  |
|  :::  |  GENETIC  |  171,219  |  169,594  |  3,973  |  4  |
|  :::  |  COMBINED  |  171,225  |  169,597  |  3,974  |  6  |
|  **//Gallus gallus//**  |  PHYSICAL  |  407  |  344  |  335  |  73  |
|  :::  |  GENETIC  |  9  |  6  |  10  |  7  |
|  :::  |  COMBINED  |  416  |  349  |  342  |  79  |
|  **//Glycine max//**  |  PHYSICAL  |  45  |  39  |  43  |  7  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  45  |  39  |  43  |  7  |
|  **//Hepatitus C Virus//**  |  PHYSICAL  |  174  |  117  |  119  |  29  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  174  |  117  |  119  |  29  |
|  **//Homo sapiens//**  |  PHYSICAL  |  367,527  |  278,133  |  20,924  |  25,736  |
|  :::  |  GENETIC  |  1,671  |  1,631  |  1,581  |  285  |
|  :::  |  COMBINED  |  369,198  |  279,502  |  21,259  |  25,839  |
|  **//Human Herpesvirus 1//**  |  PHYSICAL  |  228  |  176  |  163  |  42  |
|  :::  |  GENETIC  |  6  |  6  |  7  |  1  |
|  :::  |  COMBINED  |  234  |  181  |  168  |  42  |
|  **//Human Herpesvirus 2//**  |  PHYSICAL  |  4  |  3  |  5  |  3  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  4  |  3  |  5  |  3  |
|  **//Human Herpesvirus 3//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Human Herpesvirus 4//**  |  PHYSICAL  |  295  |  217  |  223  |  33  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  295  |  217  |  223  |  33  |
|  **//Human Herpesvirus 5//**  |  PHYSICAL  |  86  |  64  |  74  |  17  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  86  |  64  |  74  |  17  |
|  **//Human Herpesvirus 6A//**  |  PHYSICAL  |  6  |  4  |  6  |  4  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  6  |  4  |  6  |  4  |
|  **//Human Herpesvirus 6B//**  |  PHYSICAL  |  2  |  2  |  4  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  4  |  2  |
|  **//Human Herpesvirus 8//**  |  PHYSICAL  |  205  |  145  |  142  |  29  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  205  |  145  |  142  |  29  |
|  **//Human Immunodeficiency Virus 1//**  |  PHYSICAL  |  1,930  |  1,292  |  1,112  |  316  |
|  :::  |  GENETIC  |  1  |  1  |  2  |  1  |
|  :::  |  COMBINED  |  1,931  |  1,292  |  1,112  |  317  |
|  **//Human Immunodeficiency Virus 2//**  |  PHYSICAL  |  19  |  12  |  16  |  10  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  19  |  12  |  16  |  10  |
|  **//Human papillomavirus (16)//**  |  PHYSICAL  |  21  |  10  |  11  |  6  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  21  |  10  |  11  |  6  |
|  **//Macaca mulatta//**  |  PHYSICAL  |  22  |  13  |  15  |  13  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  22  |  13  |  15  |  13  |
|  **//Meleagris gallopavo//**  |  PHYSICAL  |  2  |  2  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  2  |  1  |
|  **//Mus musculus//**  |  PHYSICAL  |  38,476  |  33,302  |  11,801  |  3,572  |
|  :::  |  GENETIC  |  311  |  269  |  277  |  178  |
|  :::  |  COMBINED  |  38,787  |  33,502  |  11,847  |  3,695  |
|  **//Mycobacterium tuberculosis (H37Rv)//**  |  PHYSICAL  |  11  |  9  |  11  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  11  |  9  |  11  |  2  |
|  **//Neurospora crassa (OR74A)//**  |  PHYSICAL  |  11  |  10  |  12  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  11  |  10  |  12  |  2  |
|  **//Nicotiana tomentosiformis//**  |  PHYSICAL  |  3  |  2  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  3  |  2  |  2  |  1  |
|  **//Oryctolagus cuniculus//**  |  PHYSICAL  |  219  |  190  |  200  |  44  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  219  |  190  |  200  |  44  |
|  **//Oryza sativa (Japonica)//**  |  PHYSICAL  |  106  |  90  |  72  |  18  |
|  :::  |  GENETIC  |  2  |  2  |  3  |  1  |
|  :::  |  COMBINED  |  108  |  92  |  72  |  18  |
|  **//Pan troglodytes//**  |  PHYSICAL  |  5  |  4  |  8  |  4  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  5  |  4  |  8  |  4  |
|  **//Pediculus humanus//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Plasmodium falciparum (3D7)//**  |  PHYSICAL  |  2,545  |  2,508  |  1,227  |  5  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2,545  |  2,508  |  1,227  |  5  |
|  **//Rattus norvegicus//**  |  PHYSICAL  |  5,546  |  4,642  |  3,388  |  994  |
|  :::  |  GENETIC  |  19  |  19  |  31  |  12  |
|  :::  |  COMBINED  |  5,565  |  4,652  |  3,398  |  997  |
|  **//Ricinus communis//**  |  PHYSICAL  |  5  |  2  |  3  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  5  |  2  |  3  |  2  |
|  **//Saccharomyces cerevisiae (S288c)//**  |  PHYSICAL  |  134,722  |  87,847  |  6,354  |  8,182  |
|  :::  |  GENETIC  |  539,338  |  416,586  |  5,944  |  7,981  |
|  :::  |  COMBINED  |  674,060  |  493,279  |  6,656  |  13,663  |
|  **//Schizosaccharomyces pombe (972h)//**  |  PHYSICAL  |  12,661  |  9,346  |  2,951  |  1,248  |
|  :::  |  GENETIC  |  57,844  |  49,004  |  3,208  |  1,458  |
|  :::  |  COMBINED  |  70,505  |  57,547  |  4,205  |  2,148  |
|  **//Selaginella moellendorffii//**  |  PHYSICAL  |  8  |  6  |  5  |  1  |
|  :::  |  GENETIC  |  2  |  2  |  3  |  1  |
|  :::  |  COMBINED  |  10  |  8  |  6  |  1  |
|  **//Simian Immunodeficiency Virus//**  |  PHYSICAL  |  30  |  15  |  18  |  13  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  30  |  15  |  18  |  13  |
|  **//Solanum lycopersicum//**  |  PHYSICAL  |  137  |  107  |  44  |  8  |
|  :::  |  GENETIC  |  4  |  4  |  4  |  1  |
|  :::  |  COMBINED  |  141  |  109  |  45  |  8  |
|  **//Solanum tuberosum//**  |  PHYSICAL  |  2  |  2  |  3  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  3  |  2  |
|  **//Strongylocentrotus purpuratus//**  |  PHYSICAL  |  17  |  16  |  17  |  3  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  17  |  16  |  17  |  3  |
|  **//Sus scrofa//**  |  PHYSICAL  |  76  |  72  |  89  |  26  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  76  |  72  |  89  |  26  |
|  **//Tobacco Mosaic Virus//**  |  PHYSICAL  |  3  |  2  |  3  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  3  |  2  |  3  |  1  |
|  **//Ustilago maydis (521)//**  |  PHYSICAL  |  3  |  3  |  3  |  3  |
|  :::  |  GENETIC  |  1  |  1  |  2  |  1  |
|  :::  |  COMBINED  |  4  |  4  |  4  |  4  |
|  **//Vaccinia Virus//**  |  PHYSICAL  |  3  |  2  |  3  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  3  |  2  |  3  |  1  |
|  **//Vitis vinifera//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Xenopus laevis//**  |  PHYSICAL  |  1,334  |  1,184  |  1,087  |  206  |
|  :::  |  GENETIC  |  7  |  7  |  10  |  3  |
|  :::  |  COMBINED  |  1,341  |  1,191  |  1,097  |  209  |
|  **//Zea mays//**  |  PHYSICAL  |  18  |  13  |  21  |  9  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  18  |  13  |  21  |  9  |
|  **//All Organisms//**  |  PHYSICAL  |  627,517  |  474,818  |  58,638  |  39,809  |
|  :::  |  GENETIC  |  782,940  |  642,355  |  17,300  |  11,479  |
|  :::  |  COMBINED  |  1,410,457  |  1,104,640  |  65,333  |  47,836  |

===== Chemical Interaction Statistics =====
^  Organism  ^  Raw Interactions  ^  Non-Redundant Interactions  ^  Unique Genes  ^  Unique Publications  ^  Unique Chemicals  ^
|  **//Bacillus subtilis (168)//**  |  164  |  85  |  49  |  15  |  79  |
|  **//Caenorhabditis elegans//**  |  8  |  4  |  4  |  2  |  1  |
|  **//Candida albicans (SC5314)//**  |  108  |  25  |  7  |  50  |  24  |
|  **//Escherichia coli (K12)//**  |  5  |  2  |  2  |  3  |  2  |
|  **//Escherichia coli (K12/MG1655)//**  |  1,586  |  832  |  287  |  137  |  583  |
|  **//Homo sapiens//**  |  25,497  |  10,646  |  2,136  |  8,808  |  4,450  |
|  **//Human Herpesvirus 1//**  |  65  |  24  |  3  |  24  |  21  |
|  **//Human Herpesvirus 4//**  |  10  |  3  |  3  |  6  |  3  |
|  **//Human Immunodeficiency Virus 1//**  |  105  |  53  |  2  |  48  |  53  |
|  **//Leishmania major (Friedlin)//**  |  8  |  4  |  1  |  2  |  4  |
|  **//Mus musculus//**  |  2  |  1  |  1  |  2  |  1  |
|  **//Mycobacterium tuberculosis (CDC1551)//**  |  3  |  3  |  3  |  1  |  3  |
|  **//Mycobacterium tuberculosis (H37Rv)//**  |  1  |  1  |  1  |  1  |  1  |
|  **//Mycoplasma pneumoniae (M129)//**  |  2  |  1  |  1  |  2  |  1  |
|  **//Plasmodium falciparum (3D7)//**  |  4  |  1  |  1  |  4  |  1  |
|  **//Rattus norvegicus//**  |  2  |  2  |  1  |  2  |  2  |
|  **//Saccharomyces cerevisiae (S288c)//**  |  7  |  7  |  2  |  2  |  7  |
|  **//Streptococcus pneumoniae (ATCCBAA255)//**  |  82  |  72  |  8  |  11  |  29  |
|  **//Streptococcus pneumoniae (ATCCBAA334)//**  |  61  |  26  |  10  |  18  |  19  |
|  **//Synechococcus elongatus (PCC6301)//**  |  4  |  2  |  1  |  2  |  2  |
|  **//Vaccinia Virus//**  |  21  |  11  |  2  |  3  |  11  |
|  **//All Organisms//**  |  27,745  |  11,805  |  2,525  |  9,083  |  5,031  |

===== Post Translational Modification (PTM) Statistics =====
^  Organism  ^  PTM  ^  Sites  ^  Un-assigned Sites  ^  Unique Proteins  ^  Unique Genes  ^  Unique Publications  ^
|  **//Arabidopsis thaliana (Columbia)//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Bos taurus//**  |  SUMOYLATION  |  0  |  2  |  0  |  2  |  2  |
|  :::  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  3  |  0  |  3  |  3  |
|  **//Caenorhabditis elegans//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Cricetulus griseus//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Danio rerio//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  4  |  0  |  4  |  3  |
|  :::  |  COMBINED  |  0  |  5  |  0  |  5  |  4  |
|  **//Drosophila melanogaster//**  |  NEDDYLATION  |  0  |  2  |  0  |  2  |  1  |
|  :::  |  SUMOYLATION  |  0  |  2  |  0  |  2  |  2  |
|  :::  |  UBIQUITINATION  |  0  |  7  |  0  |  7  |  4  |
|  :::  |  COMBINED  |  0  |  11  |  0  |  9  |  6  |
|  **//Hepatitus C Virus//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  2  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  2  |
|  **//Homo sapiens//**  |  FAT10YLATION  |  0  |  659  |  0  |  659  |  5  |
|  :::  |  ISG15YLATION  |  0  |  7  |  0  |  7  |  6  |
|  :::  |  NEDDYLATION  |  0  |  903  |  0  |  903  |  75  |
|  :::  |  SUMOYLATION  |  0  |  2,315  |  0  |  2,315  |  325  |
|  :::  |  UBIQUITINATION  |  0  |  9,993  |  0  |  9,993  |  3,049  |
|  :::  |  COMBINED  |  0  |  13,877  |  0  |  10,253  |  3,332  |
|  **//Human Herpesvirus 1//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  3  |  0  |  3  |  4  |
|  :::  |  COMBINED  |  0  |  4  |  0  |  3  |  5  |
|  **//Human Herpesvirus 4//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  2  |  0  |  1  |  1  |
|  **//Human Herpesvirus 6A//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Human Immunodeficiency Virus 1//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  6  |  0  |  6  |  18  |
|  :::  |  COMBINED  |  0  |  7  |  0  |  6  |  18  |
|  **//Macaca mulatta//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Mus musculus//**  |  NEDDYLATION  |  0  |  5  |  0  |  5  |  3  |
|  :::  |  SUMOYLATION  |  0  |  19  |  0  |  19  |  18  |
|  :::  |  UBIQUITINATION  |  0  |  200  |  0  |  200  |  225  |
|  :::  |  UFMYLATION  |  0  |  10  |  0  |  10  |  1  |
|  :::  |  COMBINED  |  0  |  234  |  0  |  226  |  243  |
|  **//Rattus norvegicus//**  |  NEDDYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  SUMOYLATION  |  0  |  5  |  0  |  5  |  5  |
|  :::  |  UBIQUITINATION  |  0  |  34  |  0  |  34  |  33  |
|  :::  |  COMBINED  |  0  |  40  |  0  |  38  |  37  |
|  **//Saccharomyces cerevisiae (S288c)//**  |  NEDDYLATION  |  0  |  4  |  0  |  4  |  9  |
|  :::  |  PHOSPHORYLATION  |  19,981  |  0  |  3,165  |  3,165  |  528  |
|  :::  |  SUMOYLATION  |  0  |  818  |  0  |  818  |  66  |
|  :::  |  UBIQUITINATION  |  0  |  3,561  |  0  |  3,561  |  212  |
|  :::  |  COMBINED  |  19,981  |  4,383  |  3,165  |  4,443  |  798  |
|  **//Schizosaccharomyces pombe (972h)//**  |  UBIQUITINATION  |  0  |  4  |  0  |  4  |  3  |
|  :::  |  COMBINED  |  0  |  4  |  0  |  4  |  3  |
|  **//Xenopus laevis//**  |  UBIQUITINATION  |  0  |  2  |  0  |  2  |  2  |
|  :::  |  COMBINED  |  0  |  2  |  0  |  2  |  2  |
|  **//All Organisms//**  |  FAT10YLATION  |  0  |  659  |  0  |  659  |  5  |
|  :::  |  ISG15YLATION  |  0  |  7  |  0  |  7  |  6  |
|  :::  |  NEDDYLATION  |  0  |  915  |  0  |  915  |  82  |
|  :::  |  PHOSPHORYLATION  |  19,981  |  0  |  3,165  |  3,165  |  528  |
|  :::  |  SUMOYLATION  |  0  |  3,167  |  0  |  3,167  |  405  |
|  :::  |  UBIQUITINATION  |  0  |  13,820  |  0  |  13,820  |  3,444  |
|  :::  |  UFMYLATION  |  0  |  10  |  0  |  10  |  1  |
|  :::  |  COMBINED  |  19,981  |  18,578  |  3,165  |  14,999  |  4,317  |


[[statistics|View the Current Database Statistics]]