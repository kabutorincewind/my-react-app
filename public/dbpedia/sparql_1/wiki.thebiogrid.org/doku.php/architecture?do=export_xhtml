<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <title>architecture</title>
<meta name="generator" content="DokuWiki"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="architecture"/>
<link rel="search" type="application/opensearchdescription+xml" href="/lib/exe/opensearch.php" title="BioGRID Help and Support Resources"/>
<link rel="start" href="/"/>
<link rel="contents" href="/doku.php/architecture?do=index" title="Sitemap"/>
<link rel="alternate" type="application/rss+xml" title="Recent Changes" href="/feed.php"/>
<link rel="alternate" type="application/rss+xml" title="Current namespace" href="/feed.php?mode=list&amp;ns="/>
<link rel="alternate" type="text/html" title="Plain HTML" href="/doku.php/architecture?do=export_xhtml"/>
<link rel="alternate" type="text/plain" title="Wiki Markup" href="/doku.php/architecture?do=export_raw"/>
<link rel="canonical" href="http://wiki.thebiogrid.org/doku.php/architecture"/>
<link rel="stylesheet" type="text/css" href="/lib/exe/css.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"/>
<!--[if gte IE 9]><!-->
<script type="text/javascript">/*<![CDATA[*/var NS='';var JSINFO = {"id":"architecture","namespace":""};
/*!]]>*/</script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f"></script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/js.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"></script>
<!--<![endif]-->
</head>
<body>
<div class="dokuwiki export">

<h1 class="sectionedit1" id="biogrid_architecture_and_data_model">BioGRID Architecture and Data Model</h1>
<div class="level1">

<p>
The BioGRID database architecture consists of three distinct components: (i) the <strong><a href="/doku.php/core" class="wikilink1" title="core">Core</a></strong> (ii) the <strong><a href="/doku.php/web" class="wikilink1" title="web">Web</a></strong> (iii) the <strong><a href="/doku.php/interaction_management_system" class="wikilink1" title="interaction_management_system">Interaction Management System (IMS)</a></strong>. This BioGRID data model was developed to be both extensible and modular. Each of the three components has a specific role in driving the BioGRID system and can easily be modified to meet rapidly changing needs in data management without entailing major changes to the applications it supports. In large databases, the integrity of the data model is often compromised (for example, by duplicating data within the database to speed up queries) to improve performance. This compromise frequently leads to serious problems with data integrity. Alternatively, a completely normalized database that strictly follows theoretical design principles, while maintaining data integrity, can often suffer greatly in performance, especially in cases where the database grows to a substantial size. The BioGRID data model solves this problem by utilizing a suite of tables specifically engineered to reduce query time while maintaining a structured normalized form that does not compromise fundamental design principles.
</p>

<p>
All of the BioGRID databases use <strong><a href="http://www.mysql.com/" class="urlextern" target="_blank" title="http://www.mysql.com/" rel="nofollow noopener">MySQL 5.1</a></strong>. We chose MySQL as the database because it is freely available, platform independant, and easy to install and maintain without sacrificing performance or features.
</p>

<p>
<a href="/lib/exe/detail.php/biogrid-data-model.jpg?id=architecture" class="media" title="biogrid-data-model.jpg"><img src="/lib/exe/fetch.php/biogrid-data-model.jpg" class="mediacenter" alt="" /></a>
</p>

</div>
</div>
</body>
</html>
