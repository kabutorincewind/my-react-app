<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
 lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    Terms and Conditions   	| BioGRID
  </title>

  <meta name="generator" content="DokuWiki"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="terms_and_conditions"/>
<link rel="search" type="application/opensearchdescription+xml" href="/lib/exe/opensearch.php" title="BioGRID Help and Support Resources"/>
<link rel="start" href="/"/>
<link rel="contents" href="/doku.php/terms_and_conditions?do=index" title="Sitemap"/>
<link rel="alternate" type="application/rss+xml" title="Recent Changes" href="/feed.php"/>
<link rel="alternate" type="application/rss+xml" title="Current namespace" href="/feed.php?mode=list&amp;ns="/>
<link rel="alternate" type="text/html" title="Plain HTML" href="/doku.php/terms_and_conditions?do=export_xhtml"/>
<link rel="alternate" type="text/plain" title="Wiki Markup" href="/doku.php/terms_and_conditions?do=export_raw"/>
<link rel="canonical" href="http://wiki.thebiogrid.org/doku.php/terms_and_conditions"/>
<link rel="stylesheet" type="text/css" href="/lib/exe/css.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"/>
<!--[if gte IE 9]><!-->
<script type="text/javascript">/*<![CDATA[*/var NS='';var JSINFO = {"id":"terms_and_conditions","namespace":""};
/*!]]>*/</script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f"></script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/js.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"></script>
<!--<![endif]-->

  <link rel="shortcut icon" href="/lib/tpl/biogrid3/images/favicon.ico" />

  </head>

<body>
<div id="wrap" class="inside">
    <div id="content">
    
    <div id="header" class="inside">
    
        <div id="navbar">
            
            <div id="logo">
                <a href="https://thebiogrid.org" title="BioGRID Search for Protein Interactions, Chemical Interactions, and Genetic Interactions">
                    <span class="unbold">Bio</span>GRID<sup>3.4</sup>
                </a>
            </div>
            
            <div id="links">
                <ul>
                	<li><a href="https://thebiogrid.org" title='Search the BioGRID'>home</a></li>
                    <li><a href="https://wiki.thebiogrid.org">help wiki</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/tools">tools</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/contribute">contribute</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/statistics">stats</a></li>
                    <li><a href="https://downloads.thebiogrid.org/BioGRID">downloads</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/partners">partners</a></li>
                    <li><a class="rightbar" href="https://wiki.thebiogrid.org/doku.php/aboutus" title='About the BIOGRID'>about us</a></li>
                    <li><a href="http://twitter.com/#!/biogrid" title='Follow @biogrid on Twitter'><img style='margin-top: -5px;' src='https://thebiogrid.org/images/twitter-button-small.png' alt='Follow Us on Twitter' /></a></li>
                    <!--<li><a href="{SITE_URL}">signup</a></li>
                    <li><a href="{SITE_URL}">login</a></li>-->
                </ul>
            </div>
            
        </div>
        
        <div id="insidetext">
            <h1>Terms and Conditions</h1> 
        </div>
    
	</div>
    
<div class="dokuwiki">
  
  <div class="stylehead">

    
	<br />

        <div class="breadcrumbs">
      <span class="bchead">You are here: </span><span class="home"><bdi><a href="/doku.php/start" class="wikilink1" title="start">Help and Support Resources</a></bdi></span> » <bdi><span class="curid"><a href="/doku.php/terms_and_conditions" class="wikilink1" title="terms_and_conditions">Terms and Conditions</a></span></bdi>    </div>
    
  </div>
  
  
  <div class="page">
    <!-- wikipage start -->
    <!-- TOC START -->
<div id="dw__toc">
<h3 class="toggle">Table of Contents</h3>
<div>

<ul class="toc">
<li class="level1"><div class="li"><a href="#terms_and_conditions">Terms and Conditions</a></div>
<ul class="toc">
<li class="level2"><div class="li"><a href="#acceptance_of_terms">1. ACCEPTANCE OF TERMS</a></div></li>
<li class="level2"><div class="li"><a href="#no_unlawful_or_prohibited_use">2. NO UNLAWFUL OR PROHIBITED USE</a></div></li>
<li class="level2"><div class="li"><a href="#description_of_services">3. DESCRIPTION OF SERVICES</a></div></li>
<li class="level2"><div class="li"><a href="#registration_obligations">4. REGISTRATION OBLIGATIONS</a></div></li>
<li class="level2"><div class="li"><a href="#content">5. CONTENT</a></div></li>
<li class="level2"><div class="li"><a href="#licensing_and_other_terms_applying_to_content_posted_on_the_biogrid_sites">6. LICENSING AND OTHER TERMS APPLYING TO CONTENT POSTED ON THE BioGRID SITES:</a></div></li>
<li class="level2"><div class="li"><a href="#no_resale_of_service">7. NO RESALE OF SERVICE</a></div></li>
<li class="level2"><div class="li"><a href="#general_practices_regarding_use_and_storage">8. GENERAL PRACTICES REGARDING USE AND STORAGE</a></div></li>
<li class="level2"><div class="li"><a href="#termination">9. TERMINATION</a></div></li>
<li class="level2"><div class="li"><a href="#links">10. LINKS</a></div></li>
<li class="level2"><div class="li"><a href="#indemnitydisclaimerlimitations_of_liability">11. INDEMNITY; DISCLAIMER; LIMITATIONS OF LIABILITY</a></div></li>
<li class="level2"><div class="li"><a href="#copyrights">12. COPYRIGHTS</a></div></li>
<li class="level2"><div class="li"><a href="#general_information">13. GENERAL INFORMATION</a></div></li>
<li class="level2"><div class="li"><a href="#violations">14. VIOLATIONS</a></div></li>
</ul></li>
</ul>
</div>
</div>
<!-- TOC END -->

<h1 class="sectionedit1" id="terms_and_conditions">Terms and Conditions</h1>
<div class="level1">

</div>

<h2 class="sectionedit2" id="acceptance_of_terms">1. ACCEPTANCE OF TERMS</h2>
<div class="level2">

<p>
BioGRID, provides the information and services on BioGRID sites to you, the user, conditioned upon your acceptance, without modification, of the terms and conditions of use (“Terms”) contained herein. Your use of BioGRID sites constitutes agreement with such Terms.
</p>

<p>
Before using BioGRID Sites, please carefully read this agreement relating to your use of BioGRID Sites. By using BioGRID Sites, you agree to be bound by these terms and conditions. If you do not agree to these terms and conditions, please do not use BioGRID sites.
</p>

<p>
BioGRID reserves the right, at BioGRID&#039;s sole discretion, to change, modify, add or remove portions of these Terms periodically. Such modifications shall be effective immediately upon posting of the modified agreement to the website unless provided otherwise (e.g., when implementing major, substantive changes, BioGRID intends to provide users with up to fourteen days of advance notice). Your continued use of the BioGRID sites following the posting of changes to these Terms will mean that you accept those changes.
</p>

<p>
Use of BioGRID sites constitutes full acceptance of and agreement to the Terms; if a user does not accept BioGRID&#039;s Terms, he or she is not granted rights to use BioGRID Sites as defined herein, and should refrain from accessing BioGRID Sites.
</p>

<p>
BioGRID reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, any or all of BioGRID sites (or any part thereof). BioGRID shall not be liable to any user or other third party for any such modification, suspension or discontinuance except as expressly provided herein.
</p>

</div>

<h2 class="sectionedit3" id="no_unlawful_or_prohibited_use">2. NO UNLAWFUL OR PROHIBITED USE</h2>
<div class="level2">

<p>
By using BioGRID sites, you warrant to BioGRID that you will not use BioGRID Sites, or any of the content obtained from BioGRID Sites, for any purpose that is unlawful or prohibited by these Terms. If you violate any of these Terms, your permission to use the BioGRID Sites automatically terminates.
</p>

</div>

<h2 class="sectionedit4" id="description_of_services">3. DESCRIPTION OF SERVICES</h2>
<div class="level2">

<p>
BioGRID Sites are owned and operated by BioGRID for the purpose of software development, discussion, implementation and innovation (the “Purpose”). BioGRID Sites provide news, tools, products and education for the biological interaction community.
</p>

</div>

<h2 class="sectionedit5" id="registration_obligations">4. REGISTRATION OBLIGATIONS</h2>
<div class="level2">

<p>
When requested, each BioGRID Site user must: (1) personally provide true, accurate, current and complete information on the BioGRID Site&#039;s registration form (collectively, the “Registration Data”) and (2) maintain and promptly update the Registration Data as necessary to keep it true, accurate, current and complete. If, after investigation, BioGRID has reasonable grounds to suspect that any user&#039;s information is untrue, inaccurate, not current or incomplete, BioGRID may suspend or terminate that user&#039;s account and prohibit any and all current or future use of the BioGRID Sites (or any portion thereof) by that user other than as expressly provided herein.
</p>

<p>
Each user will receive passwords and account designations upon completing certain BioGRID Site registration processes and is wholly responsible for maintaining the confidentiality thereof and wholly liable for all activities occurring thereunder. BioGRID cannot and will not be liable for any loss or damage arising from a user&#039;s failure to comply with this Section 4, including any loss or damage arising from any user&#039;s failure to: (1) immediately notify BioGRID of any unauthorized use of his or her password or account or any other breach of security; and (2) ensure that he or she exits from his or her account at the end of each session.
</p>

<p>
BioGRID handles user Registration Data in accordance with the BioGRID Sites&#039; Privacy Policy .
</p>

</div>

<h2 class="sectionedit6" id="content">5. CONTENT</h2>
<div class="level2">

<p>
All information, data, text, software, music, sound, photographs, graphics, video, messages, or any other materials whatsoever (collectively, “Content”), whether publicly posted or privately transmitted, is the sole responsibility of the person from whom such Content originated. This means that the user, and not BioGRID, is entirely responsible for all Content that he or she uploads, posts, emails or otherwise transmits via the BioGRID Sites. No user shall transmit Content or otherwise conduct or participate in any activities on BioGRID Sites that, in the judgment of BioGRID, is likely to be prohibited by law in any applicable jurisdiction, including laws governing the encryption of software, the export of technology, the transmission of obscenity, or the permissible uses of intellectual property.
</p>

<p>
BioGRID reserves the right to refuse or delete any Content of which it becomes aware and reasonably deems not to fulfill the Purpose. In addition, BioGRID shall have the right (but not the obligation) in its sole discretion to refuse or delete any Content that it reasonably considers to violate the Terms or be otherwise illegal. BioGRID, in its sole and absolute discretion, may preserve Content and may also disclose Content if required to do so by law or judicial or governmental mandate or as reasonably determined useful by BioGRID to protect the rights, property, or personal safety of BioGRID Sites&#039; users and the public. BioGRID does not control the Content posted to the BioGRID Sites and, as such, does not guarantee the accuracy, integrity or quality of such Content. Under no circumstances will BioGRID be liable in any way for any Content, including, but not limited to, liability for any errors or omissions in any Content or for any loss or damage of any kind incurred as a result of the use of any Content posted, emailed or otherwise transmitted via BioGRID Sites.
</p>

<p>
Each user, by using BioGRID Sites, may be exposed to Content that is offensive, indecent or objectionable. Each user must evaluate, and bear all risks associated with the use of any Content, including any reliance on the accuracy, completeness, or usefulness of such Content.
</p>

</div>

<h2 class="sectionedit7" id="licensing_and_other_terms_applying_to_content_posted_on_the_biogrid_sites">6. LICENSING AND OTHER TERMS APPLYING TO CONTENT POSTED ON THE BioGRID SITES:</h2>
<div class="level2">

<p>
Use, reproduction, modification, and other intellectual property rights to data stored on the BioGRID Sites will be subject to licensing arrangements that may be approved by BioGRID as applicable to such Content.
</p>

<p>
With respect to text or data entered into and stored by publicly-accessible site features such as forums, comments and bug trackers (“BioGRID Public Content”), the submitting user retains ownership of such BioGRID Public Content; with respect to publicly-available statistical content which is generated by the site to monitor and display content activity, such content is owned by BioGRID. In each such case, the submitting user grants BioGRID the royalty-free, perpetual, irrevocable, non-exclusive, transferable license to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, perform, and display such Content (in whole or part) worldwide and/or to incorporate it in other works in any form, media, or technology now known or later developed, all subject to the terms of any applicable license.
</p>

<p>
Content located on any BioGRID-hosted subdomain which is subject to the sole editorial control of the owner or licensee of such subdomain, shall be subject to the appropriate license applicable to such Content, or to such other licensing arrangements as may be approved by BioGRID as applicable to such Content.
</p>

</div>

<h2 class="sectionedit8" id="no_resale_of_service">7. NO RESALE OF SERVICE</h2>
<div class="level2">

<p>
You agree not to sell, resell, or offer for any commercial purposes, any portion of the BioGRID Sites, use of the BioGRID Sites, or access to the BioGRID Sites.
</p>

</div>

<h2 class="sectionedit9" id="general_practices_regarding_use_and_storage">8. GENERAL PRACTICES REGARDING USE AND STORAGE</h2>
<div class="level2">

<p>
BioGRID may establish general practices and limits concerning use of the BioGRID Sites. While BioGRID will use reasonable efforts to back up site data and make such data available in the event of loss or deletion, BioGRID has no responsibility or liability for the deletion or failure to store any messages and other communications or other Content maintained or transmitted by any BioGRID Site. BioGRID reserves the right to mark as “inactive” and archive accounts and/or Content that are inactive for an extended period of time. BioGRID reserves the right to change these general practusers and the public as described in Section 1 above.
</p>

</div>

<h2 class="sectionedit10" id="termination">9. TERMINATION</h2>
<div class="level2">

<p>
BioGRID may terminate a user&#039;s account in BioGRID&#039;s absolute discretion and for any reason. BioGRID is especially likely to terminate for reasons that include, but are not limited to, the following: (1) violation of these Terms; (2) abuse of site resources or attempt to gain unauthorized entry to the site or site resources; (3) use of a BioGRID Site in a manner inconsistent with the Purpose; (4) a user&#039;s request for such termination; or (4) as required by law, regulation, court or governing agency order.
</p>

<p>
BioGRID&#039;s termination of any user&#039;s access to any or all BioGRID Sites may be effected without notice and, on such termination, BioGRID may immediately deactivate or delete user&#039;s account and/or bar any further access to such files. BioGRID shall not be liable to any user or other third party for any termination of that user&#039;s access or account hereunder. In addition, a user&#039;s request for termination will result in deactivation but not necessarily deletion of the account. BioGRID reserves the right to delete, or not delete, a user&#039;s account at BioGRID&#039;s sole discretion, as well as to delete, or not delete, content at BioGRID&#039;s sole discretion.
</p>

</div>

<h2 class="sectionedit11" id="links">10. LINKS</h2>
<div class="level2">

<p>
BioGRID, any BioGRID Site or a third party may provide links to other websites. BioGRID exercises no control whatsoever over such other websites and web-based resources and is not responsible or liable for the availability thereof or the Content, advertising, products or other materials thereon. BioGRID shall not be responsible or liable, directly or indirectly, for any damage or loss incurred or suffered by any user in connection therewith. Your access and use of linked websites, including information, material, products and services therein, is solely at your own risk.
</p>

<p>
BioGRID Sites&#039; Privacy Statement is applicable only when you are on a BioGRID Site. Once you choose to link to another website, you should read that website&#039;s privacy statement before disclosing any personal information.
</p>

</div>

<h2 class="sectionedit12" id="indemnitydisclaimerlimitations_of_liability">11. INDEMNITY; DISCLAIMER; LIMITATIONS OF LIABILITY</h2>
<div class="level2">

<p>
Each user shall indemnify, defend and hold harmless BioGRID, and its parent corporation and affiliates and their respective officers, employees and agents, and each of BioGRID&#039;s website partners, from any and all claims, demands, damages, costs, and liabilities including reasonable attorneys&#039; fees, made by any third party due to or arising out of that user&#039;s acts or omissions, including claims arising out of that user&#039;s use of the BioGRID Sites; his or her submission, posting or transmission of Content or his or her violation of the Terms.
</p>

<p>
EACH USER&#039;S USE OF THE BioGRID SITES IS AT HIS OR HER SOLE RISK. THE BioGRID SITES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS AND THE BioGRID ASSUMES NO RESPONSIBILITY FOR THE TIMELINESS, DELETION, MIS-DELIVERY OR FAILURE TO STORE ANY USER COMMUNICATIONS OR PERSONALIZATION SETTINGS. EACH USER WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO HIS OR HER COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL. THE BioGRID EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. SPECIFICALLY, THE BioGRID MAKES NO WARRANTY THAT (i) THE BioGRID SITES OR ANY SERVICE THEREON WILL MEET YOUR REQUIREMENTS, (ii) ANY USER ACCESS WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, (iii) THE QUALITY OF ANY CONTENT, PRODUCTS, SERVICES, INFORMATION OR OTHER MATERIAL OBTAINED BY ANY USER WILL MEET HIS OR HER EXPECTATIONS, AND (iv) ANY ERRORS IN THE SOFTWARE WILL BE CORRECTED. EXCLUDING ONLY DAMAGES ARISING OUT OF THE BioGRID&#039;S GROSS NEGLIGENCE OR WILLFUL MISCONDUCT, THE BioGRID SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF THE BioGRID HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM ANY USER&#039;S USE OR INABILITY TO USE ANY BioGRID SITES OR SERVICES THEREON; THE COST OF PROCUREMENT OF SUBSTITUTE SERVICES; UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE BioGRID SITES; OR ANY OTHER MATTER RELATING TO THE BioGRID SITES. IN NO EVENT SHALL THE BioGRID&#039;S TOTAL CUMULATIVE LIABILITY TO ANY USER OR OTHER PARTY UNDER THESE TERMS OF SERVICE OR OTHERWISE EXCEED $1,000.00. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU. NOTHING HEREIN SHALL BE DEEMED TO CREATE AN AGENCY, PARTNERSHIP, JOINT VENTURE, EMPLOYEE-EMPLOYER OR FRANCHISOR-FRANCHISEE RELATIONSHIP OF ANY KIND BETWEEN THE BioGRID AND ANY USER OR OTHER PERSON OR ENTITY NOR DO THESE TERMS OF SERVICE EXTEND RIGHTS TO ANY THIRD PARTY. AS NOTED ABOVE, THE BioGRID DOES NOT AND CANNOT CONTROL THE ACTIONS OF BioGRID SITE USERS, VISITORS OR LINKED THIRD PARTIES. WE RESERVE THE RIGHT TO REPORT ANY MALFEASANCE THAT COMES TO OUR ATTENTION TO THE APPROPRIATE AUTHORITIES. WE DO NOT GUARANTEE CONTINUOUS UNINTERRUPTED OR SECURE ACCESS TO BioGRID SITES. OPERATION OF BioGRID SITES MAY BE SUBJECT TO INTERFERENCE FROM NUMEROUS FACTORS OUTSIDE OUR CONTROL. FURTHER, SCHEDULED AND PREVENTIVE MAINTENANCE AS WELL AS REQUIRED AND EMERGENCY MAINTENANCE WORK MAY TEMPORARILY INTERRUPT SERVICES OR ACCESS TO THE WEBSITE. THE DISCLAIMERS OF WARRANTY AND LIMITATIONS OF LIABILITY APPLY, WITHOUT LIMITATION, TO ANY DAMAGES OR INJURY CAUSED BY THE FAILURE OF PERFORMANCE, ERROR, OMISSION, INTERRUPTION, DELETION, DEFECT, DELAY IN OPERATION OR TRANSMISSION, COMPUTER VIRUS, COMMUNICATION LINE FAILURE, THEFT OR DESTRUCTION OR UNAUTHORIZED ACCESS TO, ALTERATION OF OR USE OF ANY ASSET, WHETHER ARISING OUT OF BREACH OF CONTRACT, TORTIOUS BEHAVIOUR, NEGLIGENCE OR ANY OTHER COURSE OF ACTION BY THE BioGRID.
</p>

</div>

<h2 class="sectionedit13" id="copyrights">12. COPYRIGHTS</h2>
<div class="level2">

<p>
BioGRID respects the intellectual property rights of others, and requires that the people who use the BioGRID Sites do the same. It is our policy to respond promptly to claims of intellectual property misuse.
</p>

<p>
If you believe that your work has been copied and is accessible on this site in a way that constitutes copyright infringement, you may notify us by providing our copyright agent with the following information in writing:
</p>

<p>
(1) the electronic or physical signature of the owner of the copyright or the person authorized to act on the owner&#039;s behalf;
</p>

<p>
(2) identification of the copyrighted work that you claim has been infringed;
</p>

<p>
(3) identification of the material that is claimed to be infringing and information reasonably sufficient to permit BioGRID to locate the material, including the full <abbr title="Uniform Resource Locator">URL</abbr>.
</p>

<p>
(4) your name, address, telephone number, and email address.
</p>

<p>
(5) a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;
</p>

<p>
(6) a statement, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or are authorized to act on the copyright owner&#039;s behalf.
</p>

<p>
If BioGRID receives such a claim, BioGRID reserves the right to refuse or delete Content as described under Section 5 hereto, or to terminate a user&#039;s account in accordance with Section 9.
</p>

<p>
Our designated agent to receive notification of claimed infringement is:
</p>

<p>
Mr. Terry Donaghue
Director Technology Transfer and Industrial Liason
600 University Avenue
Toronto, ON, Canada
<a href="mailto:&#x64;&#x6f;&#x6e;&#x61;&#x67;&#x68;&#x75;&#x65;&#x40;&#x6d;&#x73;&#x68;&#x72;&#x69;&#x2e;&#x6f;&#x6e;&#x2e;&#x63;&#x61;" class="mail" title="&#x64;&#x6f;&#x6e;&#x61;&#x67;&#x68;&#x75;&#x65;&#x40;&#x6d;&#x73;&#x68;&#x72;&#x69;&#x2e;&#x6f;&#x6e;&#x2e;&#x63;&#x61;">&#x64;&#x6f;&#x6e;&#x61;&#x67;&#x68;&#x75;&#x65;&#x40;&#x6d;&#x73;&#x68;&#x72;&#x69;&#x2e;&#x6f;&#x6e;&#x2e;&#x63;&#x61;</a>
TEL: 416-586-8225
FAX: 416-586-3110
</p>

<p>
After receiving a claim of infringement, BioGRID will process and investigate notices of alleged infringement and will take appropriate actions under applicable intellectual property laws. Upon receipt of notices complying or substantially complying with the intellectual property laws, BioGRID will act expeditiously to remove or disable access to any material claimed to be infringing or claimed to be the subject of infringing activity, and will act expeditiously to remove or disable access to any reference or link to material or activity that is claimed to be infringing. BioGRID will take reasonable steps to expeditiously notify the subscriber that it has removed or disabled access to such material.
</p>

<p>
Upon receipt of a proper counter notification, BioGRID will promptly provide the person who provided the initial notification of claimed infringement with a copy of the counter notification and inform that person that it will replace the removed material or cease disabling access to it in ten (10) to fourteen (14) business days. Additionally, BioGRID will replace the removed material and cease disabling access to it ten (10) to fourteen (14) business days following receipt of the counter notice, unless BioGRID&#039;s designated agent first receives notice from the person who submitted the initial notification that such person has filed an action seeking a court order to restrain the subscriber from engaging in infringing activity relating to the material on the BioGRID system or network.
</p>

<p>
You may provide us with a counter notification by providing our copyright agent the following information in writing:
</p>

<p>
(1) your physical or electronic signature;
</p>

<p>
(2) identification of the material that has been removed or to which access has been disabled, and the location at which the material appeared before it was removed or access to it was disabled, including the full <abbr title="Uniform Resource Locator">URL</abbr>;
</p>

<p>
(3) a statement under penalty of perjury that you have a good faith belief that the material was removed or disabled as a result of mistake or misidentification of the material to be removed or disabled;
</p>

<p>
(4) your name, address, and telephone number, and a statement that you consent to the jurisdiction of Federal District Court for the judicial district in which your address is located, or if your address is outside of the United States, for any judicial district in which BioGRID may be found and that you will accept service of process from the person who provided the initial notification of infringement.
</p>

</div>

<h2 class="sectionedit14" id="general_information">13. GENERAL INFORMATION</h2>
<div class="level2">

<p>
The Terms constitute the entire agreement between each user and BioGRID and govern each user&#039;s use of BioGRID Sites, superseding any prior agreements. Each user may be subject to additional terms and conditions that may apply when that user uses affiliate services, third party content or third party software. The Terms and the relationship between each user and BioGRID shall be governed by the laws of the Province of Ontario without regard to its conflict of law provisions and each party shall submit to the personal and exclusive jurisdiction of the courts located within the Province of Ontario . If any provision of the Terms is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties&#039; intentions as reflected in the provision, and the other provisions of the Terms remain in full force and effect.
</p>

</div>

<h2 class="sectionedit15" id="violations">14. VIOLATIONS</h2>
<div class="level2">

<p>
Please report any violations of the Terms (except for claims of intellectual property infringement) to the BioGRID Site Director at <a href="mailto:&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;" class="mail" title="&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;">&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;</a>.
</p>

</div>

    <!-- wikipage stop -->
  </div>

  <div class="clearer">&nbsp;</div>

  
  <div class="stylefoot">

    <div class="meta">
      <div class="user">
              </div>
      <div class="doc">
        <bdi>terms_and_conditions.txt</bdi> · Last modified: 2017/08/08 12:52 (external edit)      </div>
    </div>

   
    <div class="bar" id="bar__bottom">
      <div class="bar-left" id="bar__bottomleft">
      	      </div>
      <div class="bar-right" id="bar__bottomright">
                                        <a class="nolink" href="#dokuwiki__top"><input type="button" class="button" value="Back to top" onclick="window.scrollTo(0, 0)" title="Back to top" /></a>        <form class="button btn_login" method="get" action="/doku.php/terms_and_conditions"><div class="no"><input type="hidden" name="do" value="login" /><input type="hidden" name="sectok" value="" /><button type="submit" title="Log In">Log In</button></div></form>      </div>
      <div class="clearer"></div>
    </div>

  </div>

</div>

<div class="no"><img src="/lib/exe/indexer.php?id=terms_and_conditions&amp;1516242462" width="2" height="1" alt="" /></div>
<div id="footer">
		Copyright &copy; 2018 <a target='_blank' href='http://www.tyerslab.com' title='Mike Tyers Lab'>TyersLab.com</a>, All Rights Reserved.
</div>

<div id="footer-links">
	<a href='{WIKI_URL}/doku.php/terms_and_conditions' title="BioGRID Terms and Conditions">Terms and Conditions</a> | 
    <a href='{WIKI_URL}/doku.php/privacy_policy' title="BioGRID Privacy Policy">Privacy Policy</a> | 
    <a target='_blank' href='https://osprey.thebiogrid.org' title="Osprey Network Visualization System">Osprey Network Visualization System</a> |
	<a target="_blank" href="https://yeastkinome.org" title="Yeast Kinome">Yeast Kinome</a> |
    <a target='_blank' href='http://www.tyerslab.com' title='Mike Tyers Lab Webpage'>TyersLab.com</a> |
    <a target='_blank' href='http://www.yeastgenome.org' title='Saccharomyces Genome Database'>SGD</a>
</div>

</div>
</div>

 <script type="text/javascript">
 var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
 document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
 </script>
 <script type="text/javascript">
 try {
 var pageTracker = _gat._getTracker("UA-239330-2");
 pageTracker._setDomainName(".thebiogrid.org");
 pageTracker._trackPageview();
 } catch(err) {}</script>
 


</body>
</html>
