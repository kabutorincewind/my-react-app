<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <title>tools</title>
<meta name="generator" content="DokuWiki"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="tools"/>
<link rel="search" type="application/opensearchdescription+xml" href="/lib/exe/opensearch.php" title="BioGRID Help and Support Resources"/>
<link rel="start" href="/"/>
<link rel="contents" href="/doku.php/tools?do=index" title="Sitemap"/>
<link rel="alternate" type="application/rss+xml" title="Recent Changes" href="/feed.php"/>
<link rel="alternate" type="application/rss+xml" title="Current namespace" href="/feed.php?mode=list&amp;ns="/>
<link rel="alternate" type="text/html" title="Plain HTML" href="/doku.php/tools?do=export_xhtml"/>
<link rel="alternate" type="text/plain" title="Wiki Markup" href="/doku.php/tools?do=export_raw"/>
<link rel="canonical" href="http://wiki.thebiogrid.org/doku.php/tools"/>
<link rel="stylesheet" type="text/css" href="/lib/exe/css.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"/>
<!--[if gte IE 9]><!-->
<script type="text/javascript">/*<![CDATA[*/var NS='';var JSINFO = {"id":"tools","namespace":""};
/*!]]>*/</script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f"></script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/js.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"></script>
<!--<![endif]-->
</head>
<body>
<div class="dokuwiki export">
<!-- TOC START -->
<div id="dw__toc">
<h3 class="toggle">Table of Contents</h3>
<div>

<ul class="toc">
<li class="level1"><div class="li"><a href="#online_tools_and_resources">Online Tools and Resources</a></div>
<ul class="toc">
<li class="level2"><div class="li"><a href="#partner_resources">Partner Resources</a></div></li>
<li class="level2"><div class="li"><a href="#network_visualization">Network Visualization</a></div></li>
<li class="level2"><div class="li"><a href="#data_management">Data Management</a></div></li>
<li class="level2"><div class="li"><a href="#plugins">Plugins</a></div></li>
<li class="level2"><div class="li"><a href="#web_services">Web Services</a></div></li>
</ul></li>
</ul>
</div>
</div>
<!-- TOC END -->

<h1 class="sectionedit1" id="online_tools_and_resources">Online Tools and Resources</h1>
<div class="level1">

<p>
Listed on this page are tools, software, and resources either written by the BioGRID team or a third party that can help you make use of BioGRID interaction data. All tools and resources are released <strong>WITHOUT ANY WARRANTY</strong> and are free to both academic and commercial entities for research purposes only. If you know of a useful software tool that should be in this listing, let us know at <strong><a href="mailto:&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;" class="mail" title="&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;">&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;</a></strong>.
</p>

</div>
<!-- EDIT1 SECTION "Online Tools and Resources" [1-463] -->
<h2 class="sectionedit2" id="partner_resources">Partner Resources</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> <strong><a href="https://phosphogrid.org" class="urlextern" target="_blank" title="https://phosphogrid.org" rel="nofollow noopener">PhosphoGRID</a></strong> - PhosphoGRID records the positions of more than 5000 specific phosphorylated residues on 1495 gene products in Saccharomyces cerevisiae. Each site is documented with a hierarchy of experimental evidence codes.</div>
</li>
<li class="level1"><div class="li"> <strong><a href="https://yeastkinome.org" class="urlextern" target="_blank" title="https://yeastkinome.org" rel="nofollow noopener">Yeast Kinome</a></strong> - Saccharomyces cerevisiae Kinase and Phosphatase Interactome (KPI) Resource.</div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://www.pathwaycommons.org/pc/home.do" class="urlextern" target="_blank" title="http://www.pathwaycommons.org/pc/home.do" rel="nofollow noopener">Pathway Commons</a></strong> - Pathway Commons is a collection of publicly available pathways from multiple organisms. It provides researchers with convenient access to a comprehensive collection of pathways from multiple sources represented in a common language.</div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://wodaklab.org/iRefWeb" class="urlextern" target="_blank" title="http://wodaklab.org/iRefWeb" rel="nofollow noopener">iRefWeb</a></strong> - The iRefWeb interface groups interaction records from the different databases into a single non-redundant view. </div>
</li>
</ul>

</div>
<!-- EDIT2 SECTION "Partner Resources" [464-1347] -->
<h2 class="sectionedit3" id="network_visualization">Network Visualization</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> <strong><a href="https://osprey.thebiogrid.org" class="urlextern" target="_blank" title="https://osprey.thebiogrid.org" rel="nofollow noopener">Osprey: Network Visualization System</a></strong> - Osprey is a software platform for the visualization of complex interaction networks. Osprey builds data-rich graphical representations from Gene Ontology (GO) annotated interaction data maintained by the BioGRID.</div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://www.cytoscape.org/" class="urlextern" target="_blank" title="http://www.cytoscape.org/" rel="nofollow noopener">Cytoscape</a></strong> - Cytoscape is an open source bioinformatics software platform for visualizing molecular interaction networks and integrating these interactions with gene expression profiles.</div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://genemania.org" class="urlextern" target="_blank" title="http://genemania.org" rel="nofollow noopener">GeneMANIA</a></strong> - GeneMANIA finds genes that are related to a set of input genes, using a very large set of functional association data.</div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://www.esyn.org" class="urlextern" target="_blank" title="http://www.esyn.org" rel="nofollow noopener">esyN</a></strong> - esyN displays online interaction networks integrated with major databases so you can retrieve data automatically as you construct the network.</div>
</li>
</ul>

</div>
<!-- EDIT3 SECTION "Network Visualization" [1348-2245] -->
<h2 class="sectionedit4" id="data_management">Data Management</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> <strong><a href="http://prohitsms.com/" class="urlextern" target="_blank" title="http://prohitsms.com/" rel="nofollow noopener">ProHits</a></strong> - Prohits is a laboratory information management system for processing large scale mass spectrometry experiments. Prohits is closely integrated with the BioGRID providing real-time comparisons between experimental results and published interactions.</div>
</li>
</ul>

</div>
<!-- EDIT4 SECTION "Data Management" [2246-2566] -->
<h2 class="sectionedit5" id="plugins">Plugins</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> <strong><a href="/doku.php/biogrid_tab_file_loader_plugin" class="wikilink1" title="biogrid_tab_file_loader_plugin">BioGRID Tab File Loader Plugin for Cytoscape</a></strong> - This plugin was developed to make it simpler to import data from BioGRID .tab files, and to allow multiple redundant interactions (evidences), represented by a single line in a .tab file, to be imported into Cytoscape as a single edge with multiple evidence codes and publication annotation.</div>
</li>
<li class="level1"><div class="li"> <strong><a href="/doku.php/biogridplugin2" class="wikilink1" title="biogridplugin2">BiogridPlugin2 for Cytoscape</a></strong> - This plugin supercedes the BioGRID Tab File Loader allowing import of data from our new tab2 file format, ability to filter data during import based on gene lists and various interaction attributes, the capacity to expand network nodes and saving of networks to tab2 files.</div>
</li>
</ul>

</div>
<!-- EDIT5 SECTION "Plugins" [2567-3301] -->
<h2 class="sectionedit6" id="web_services">Web Services</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> <strong><a href="/doku.php/biogridrest" class="wikilink1" title="biogridrest">BioGRID REST Service</a></strong> - BioGRID interaction data is now available programatically over HTTP through a REST-style web service. </div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://code.google.com/p/psicquic/" class="urlextern" target="_blank" title="http://code.google.com/p/psicquic/" rel="nofollow noopener">PSICQUIC</a></strong> - PSICQUIC is a web service that attempts to standardize access to molecular interaction databases programatically.</div>
</li>
</ul>

</div>
<!-- EDIT6 SECTION "Web Services" [3302-] --></div>
</body>
</html>
