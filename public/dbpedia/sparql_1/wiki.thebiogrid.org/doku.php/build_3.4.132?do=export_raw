====== Build Statistics (3.4.132) - January 2016 ======
This page shows the statistics for the BioGRID database as was established via the above referenced data and release version. If you are interested in accessing the data from this exact build, please visit our **[[http://thebiogrid.org/download.php|download page]]** and find the correct version under "Release Archive". It is highly recommended that all new projects use the latest BioGRID release when possible.

  * **Raw Interactions** -  Each unique combination of interactors A and B, experimental system and publication is counted as a single interaction. Reciprocal interactions (A → B and B → A) are counted twice.
  * **Non-Redundant Interactions** - Each unique combination of interactors A and B are counted as a single interaction, regardless of directionality,  experimental system and publication.

===== Physical and Genetic Interaction Statistics =====
^  Organism  ^  Experiment Type  ^  Raw Interactions  ^  Non-Redundant Interactions  ^  Unique Genes  ^  Unique Publications  ^
|  **//Anopheles gambiae (PEST)//**  |  PHYSICAL  |  2  |  1  |  2  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  1  |  2  |  2  |
|  **//Apis mellifera//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Arabidopsis thaliana (Columbia)//**  |  PHYSICAL  |  40,548  |  34,629  |  9,280  |  1,987  |
|  :::  |  GENETIC  |  246  |  185  |  182  |  97  |
|  :::  |  COMBINED  |  40,794  |  34,769  |  9,328  |  2,054  |
|  **//Bacillus subtilis (168)//**  |  PHYSICAL  |  2  |  2  |  3  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  3  |  2  |
|  **//Bos taurus//**  |  PHYSICAL  |  414  |  375  |  402  |  162  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  414  |  375  |  402  |  162  |
|  **//Caenorhabditis elegans//**  |  PHYSICAL  |  6,320  |  5,785  |  3,271  |  186  |
|  :::  |  GENETIC  |  2,330  |  2,263  |  1,123  |  31  |
|  :::  |  COMBINED  |  8,650  |  8,025  |  3,945  |  201  |
|  **//Candida albicans (SC5314)//**  |  PHYSICAL  |  147  |  114  |  128  |  38  |
|  :::  |  GENETIC  |  268  |  263  |  266  |  7  |
|  :::  |  COMBINED  |  415  |  376  |  373  |  44  |
|  **//Canis familiaris//**  |  PHYSICAL  |  38  |  28  |  45  |  21  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  38  |  28  |  45  |  21  |
|  **//Cavia porcellus//**  |  PHYSICAL  |  6  |  4  |  7  |  4  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  6  |  4  |  7  |  4  |
|  **//Chlamydomonas reinhardtii//**  |  PHYSICAL  |  15  |  14  |  16  |  3  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  15  |  14  |  16  |  3  |
|  **//Chlorocebus sabaeus//**  |  PHYSICAL  |  4  |  3  |  4  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  4  |  3  |  4  |  1  |
|  **//Cricetulus griseus//**  |  PHYSICAL  |  6  |  6  |  11  |  5  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  6  |  6  |  11  |  5  |
|  **//Danio rerio//**  |  PHYSICAL  |  214  |  188  |  181  |  33  |
|  :::  |  GENETIC  |  70  |  64  |  65  |  30  |
|  :::  |  COMBINED  |  284  |  249  |  239  |  62  |
|  **//Dictyostelium discoideum (AX4)//**  |  PHYSICAL  |  24  |  17  |  20  |  7  |
|  :::  |  GENETIC  |  6  |  6  |  8  |  2  |
|  :::  |  COMBINED  |  30  |  20  |  24  |  8  |
|  **//Drosophila melanogaster//**  |  PHYSICAL  |  38,567  |  37,494  |  8,217  |  440  |
|  :::  |  GENETIC  |  9,976  |  2,823  |  1,039  |  1,481  |
|  :::  |  COMBINED  |  48,543  |  40,167  |  8,349  |  1,864  |
|  **//Emericella nidulans (FGSC A4)//**  |  PHYSICAL  |  65  |  57  |  57  |  3  |
|  :::  |  GENETIC  |  6  |  5  |  7  |  3  |
|  :::  |  COMBINED  |  71  |  62  |  64  |  6  |
|  **//Equus caballus//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Escherichia coli (K12/MG1655)//**  |  PHYSICAL  |  102  |  99  |  104  |  14  |
|  :::  |  GENETIC  |  26  |  25  |  36  |  12  |
|  :::  |  COMBINED  |  128  |  123  |  138  |  26  |
|  **//Escherichia coli (K12/W3110)//**  |  PHYSICAL  |  6  |  3  |  4  |  2  |
|  :::  |  GENETIC  |  134,467  |  133,779  |  3,923  |  2  |
|  :::  |  COMBINED  |  134,473  |  133,782  |  3,924  |  4  |
|  **//Gallus gallus//**  |  PHYSICAL  |  401  |  339  |  329  |  69  |
|  :::  |  GENETIC  |  9  |  6  |  10  |  7  |
|  :::  |  COMBINED  |  410  |  344  |  336  |  75  |
|  **//Glycine max//**  |  PHYSICAL  |  23  |  20  |  22  |  4  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  23  |  20  |  22  |  4  |
|  **//Hepatitus C Virus//**  |  PHYSICAL  |  164  |  110  |  112  |  24  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  164  |  110  |  112  |  24  |
|  **//Homo sapiens//**  |  PHYSICAL  |  316,717  |  237,548  |  20,085  |  24,425  |
|  :::  |  GENETIC  |  1,531  |  1,494  |  1,462  |  280  |
|  :::  |  COMBINED  |  318,248  |  238,789  |  20,413  |  24,524  |
|  **//Human Herpesvirus 1//**  |  PHYSICAL  |  222  |  175  |  162  |  39  |
|  :::  |  GENETIC  |  6  |  6  |  7  |  1  |
|  :::  |  COMBINED  |  228  |  180  |  167  |  39  |
|  **//Human Herpesvirus 2//**  |  PHYSICAL  |  4  |  3  |  5  |  3  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  4  |  3  |  5  |  3  |
|  **//Human Herpesvirus 3//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Human Herpesvirus 4//**  |  PHYSICAL  |  295  |  217  |  223  |  33  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  295  |  217  |  223  |  33  |
|  **//Human Herpesvirus 5//**  |  PHYSICAL  |  79  |  61  |  71  |  15  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  79  |  61  |  71  |  15  |
|  **//Human Herpesvirus 6A//**  |  PHYSICAL  |  6  |  4  |  6  |  4  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  6  |  4  |  6  |  4  |
|  **//Human Herpesvirus 6B//**  |  PHYSICAL  |  2  |  2  |  4  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  4  |  2  |
|  **//Human Herpesvirus 8//**  |  PHYSICAL  |  191  |  142  |  140  |  25  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  191  |  142  |  140  |  25  |
|  **//Human Immunodeficiency Virus 1//**  |  PHYSICAL  |  1,800  |  1,189  |  1,027  |  305  |
|  :::  |  GENETIC  |  1  |  1  |  2  |  1  |
|  :::  |  COMBINED  |  1,801  |  1,189  |  1,027  |  306  |
|  **//Human Immunodeficiency Virus 2//**  |  PHYSICAL  |  19  |  12  |  16  |  10  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  19  |  12  |  16  |  10  |
|  **//Human papillomavirus (16)//**  |  PHYSICAL  |  5  |  4  |  5  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  5  |  4  |  5  |  1  |
|  **//Macaca mulatta//**  |  PHYSICAL  |  21  |  12  |  14  |  12  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  21  |  12  |  14  |  12  |
|  **//Meleagris gallopavo//**  |  PHYSICAL  |  2  |  2  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  2  |  1  |
|  **//Mus musculus//**  |  PHYSICAL  |  23,115  |  18,617  |  8,650  |  3,412  |
|  :::  |  GENETIC  |  308  |  266  |  274  |  175  |
|  :::  |  COMBINED  |  23,423  |  18,814  |  8,706  |  3,534  |
|  **//Mycobacterium tuberculosis (H37Rv)//**  |  PHYSICAL  |  10  |  8  |  9  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  10  |  8  |  9  |  1  |
|  **//Neurospora crassa (OR74A)//**  |  PHYSICAL  |  11  |  10  |  12  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  11  |  10  |  12  |  2  |
|  **//Nicotiana tomentosiformis//**  |  PHYSICAL  |  3  |  2  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  3  |  2  |  2  |  1  |
|  **//Oryctolagus cuniculus//**  |  PHYSICAL  |  205  |  179  |  189  |  41  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  205  |  179  |  189  |  41  |
|  **//Oryza sativa (Japonica)//**  |  PHYSICAL  |  30  |  21  |  26  |  10  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  30  |  21  |  26  |  10  |
|  **//Pan troglodytes//**  |  PHYSICAL  |  5  |  4  |  8  |  4  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  5  |  4  |  8  |  4  |
|  **//Pediculus humanus//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Plasmodium falciparum (3D7)//**  |  PHYSICAL  |  2,545  |  2,508  |  1,227  |  5  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2,545  |  2,508  |  1,227  |  5  |
|  **//Rattus norvegicus//**  |  PHYSICAL  |  5,415  |  4,548  |  3,342  |  953  |
|  :::  |  GENETIC  |  19  |  19  |  31  |  12  |
|  :::  |  COMBINED  |  5,434  |  4,558  |  3,352  |  956  |
|  **//Ricinus communis//**  |  PHYSICAL  |  5  |  2  |  3  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  5  |  2  |  3  |  2  |
|  **//Saccharomyces cerevisiae (S288c)//**  |  PHYSICAL  |  127,802  |  82,713  |  6,278  |  7,882  |
|  :::  |  GENETIC  |  210,796  |  152,053  |  5,705  |  7,704  |
|  :::  |  COMBINED  |  338,598  |  227,893  |  6,514  |  13,189  |
|  **//Schizosaccharomyces pombe (972h)//**  |  PHYSICAL  |  10,074  |  7,268  |  2,392  |  1,206  |
|  :::  |  GENETIC  |  57,726  |  48,918  |  3,200  |  1,426  |
|  :::  |  COMBINED  |  67,800  |  55,435  |  3,977  |  2,088  |
|  **//Simian Immunodeficiency Virus//**  |  PHYSICAL  |  27  |  14  |  17  |  11  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  27  |  14  |  17  |  11  |
|  **//Solanum lycopersicum//**  |  PHYSICAL  |  92  |  79  |  30  |  5  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  92  |  79  |  30  |  5  |
|  **//Solanum tuberosum//**  |  PHYSICAL  |  2  |  2  |  3  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  2  |  2  |  3  |  2  |
|  **//Strongylocentrotus purpuratus//**  |  PHYSICAL  |  16  |  15  |  16  |  2  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  16  |  15  |  16  |  2  |
|  **//Sus scrofa//**  |  PHYSICAL  |  76  |  72  |  89  |  26  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  76  |  72  |  89  |  26  |
|  **//Tobacco Mosaic Virus//**  |  PHYSICAL  |  3  |  2  |  3  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  3  |  2  |  3  |  1  |
|  **//Ustilago maydis (521)//**  |  PHYSICAL  |  3  |  3  |  3  |  3  |
|  :::  |  GENETIC  |  1  |  1  |  2  |  1  |
|  :::  |  COMBINED  |  4  |  4  |  4  |  4  |
|  **//Vaccinia Virus//**  |  PHYSICAL  |  3  |  2  |  3  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  3  |  2  |  3  |  1  |
|  **//Vitis vinifera//**  |  PHYSICAL  |  1  |  1  |  2  |  1  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  1  |  1  |  2  |  1  |
|  **//Xenopus laevis//**  |  PHYSICAL  |  687  |  537  |  476  |  199  |
|  :::  |  GENETIC  |  7  |  7  |  10  |  3  |
|  :::  |  COMBINED  |  694  |  544  |  486  |  202  |
|  **//Zea mays//**  |  PHYSICAL  |  9  |  8  |  13  |  5  |
|  :::  |  GENETIC  |  0  |  0  |  0  |  0  |
|  :::  |  COMBINED  |  9  |  8  |  13  |  5  |
|  **//All Organisms//**  |  PHYSICAL  |  563,181  |  423,938  |  56,378  |  37,865  |
|  :::  |  GENETIC  |  417,286  |  341,688  |  16,765  |  11,110  |
|  :::  |  COMBINED  |  980,467  |  757,476  |  63,284  |  45,684  |

===== Chemical Interaction Statistics =====
^  Organism  ^  Raw Interactions  ^  Non-Redundant Interactions  ^  Unique Genes  ^  Unique Publications  ^  Unique Chemicals  ^
|  **//Bacillus subtilis (168)//**  |  166  |  86  |  50  |  15  |  80  |
|  **//Caenorhabditis elegans//**  |  8  |  4  |  4  |  2  |  1  |
|  **//Candida albicans (SC5314)//**  |  108  |  25  |  7  |  50  |  24  |
|  **//Escherichia coli (K12)//**  |  5  |  2  |  2  |  3  |  2  |
|  **//Escherichia coli (K12/MG1655)//**  |  1,568  |  824  |  287  |  136  |  581  |
|  **//Homo sapiens//**  |  25,269  |  10,542  |  2,129  |  8,715  |  4,417  |
|  **//Human Herpesvirus 1//**  |  65  |  24  |  3  |  24  |  21  |
|  **//Human Herpesvirus 4//**  |  10  |  3  |  3  |  6  |  3  |
|  **//Human Immunodeficiency Virus 1//**  |  105  |  53  |  2  |  48  |  53  |
|  **//Leishmania major (Friedlin)//**  |  8  |  4  |  1  |  2  |  4  |
|  **//Mus musculus//**  |  2  |  1  |  1  |  2  |  1  |
|  **//Mycobacterium tuberculosis (CDC1551)//**  |  3  |  3  |  3  |  1  |  3  |
|  **//Mycobacterium tuberculosis (H37Rv)//**  |  1  |  1  |  1  |  1  |  1  |
|  **//Mycoplasma pneumoniae (M129)//**  |  2  |  1  |  1  |  2  |  1  |
|  **//Plasmodium falciparum (3D7)//**  |  4  |  1  |  1  |  4  |  1  |
|  **//Rattus norvegicus//**  |  2  |  2  |  1  |  2  |  2  |
|  **//Saccharomyces cerevisiae (S288c)//**  |  7  |  7  |  2  |  2  |  7  |
|  **//Streptococcus pneumoniae (ATCCBAA255)//**  |  82  |  72  |  8  |  11  |  29  |
|  **//Streptococcus pneumoniae (ATCCBAA334)//**  |  61  |  26  |  10  |  18  |  19  |
|  **//Synechococcus elongatus (PCC6301)//**  |  4  |  2  |  1  |  2  |  2  |
|  **//Vaccinia Virus//**  |  21  |  11  |  2  |  3  |  11  |
|  **//All Organisms//**  |  27,501  |  11,694  |  2,519  |  8,989  |  4,999  |

===== Post Translational Modification (PTM) Statistics =====
^  Organism  ^  PTM  ^  Sites  ^  Un-assigned Sites  ^  Unique Proteins  ^  Unique Genes  ^  Unique Publications  ^
|  **//Arabidopsis thaliana (Columbia)//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Bos taurus//**  |  SUMOYLATION  |  0  |  2  |  0  |  2  |  2  |
|  :::  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  3  |  0  |  3  |  3  |
|  **//Caenorhabditis elegans//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Cricetulus griseus//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Danio rerio//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  4  |  0  |  4  |  3  |
|  :::  |  COMBINED  |  0  |  5  |  0  |  5  |  4  |
|  **//Drosophila melanogaster//**  |  NEDDYLATION  |  0  |  2  |  0  |  2  |  1  |
|  :::  |  SUMOYLATION  |  0  |  2  |  0  |  2  |  2  |
|  :::  |  UBIQUITINATION  |  0  |  7  |  0  |  7  |  4  |
|  :::  |  COMBINED  |  0  |  11  |  0  |  9  |  6  |
|  **//Hepatitus C Virus//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  2  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  2  |
|  **//Homo sapiens//**  |  FAT10YLATION  |  0  |  659  |  0  |  659  |  5  |
|  :::  |  ISG15YLATION  |  0  |  7  |  0  |  7  |  6  |
|  :::  |  NEDDYLATION  |  0  |  903  |  0  |  903  |  75  |
|  :::  |  SUMOYLATION  |  0  |  2,315  |  0  |  2,315  |  325  |
|  :::  |  UBIQUITINATION  |  0  |  9,993  |  0  |  9,993  |  3,049  |
|  :::  |  COMBINED  |  0  |  13,877  |  0  |  10,253  |  3,332  |
|  **//Human Herpesvirus 1//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  3  |  0  |  3  |  4  |
|  :::  |  COMBINED  |  0  |  4  |  0  |  3  |  5  |
|  **//Human Herpesvirus 4//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  2  |  0  |  1  |  1  |
|  **//Human Herpesvirus 6A//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Human Immunodeficiency Virus 1//**  |  SUMOYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  UBIQUITINATION  |  0  |  6  |  0  |  6  |  18  |
|  :::  |  COMBINED  |  0  |  7  |  0  |  6  |  18  |
|  **//Macaca mulatta//**  |  UBIQUITINATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  COMBINED  |  0  |  1  |  0  |  1  |  1  |
|  **//Mus musculus//**  |  NEDDYLATION  |  0  |  5  |  0  |  5  |  3  |
|  :::  |  SUMOYLATION  |  0  |  19  |  0  |  19  |  18  |
|  :::  |  UBIQUITINATION  |  0  |  200  |  0  |  200  |  225  |
|  :::  |  UFMYLATION  |  0  |  10  |  0  |  10  |  1  |
|  :::  |  COMBINED  |  0  |  234  |  0  |  226  |  243  |
|  **//Rattus norvegicus//**  |  NEDDYLATION  |  0  |  1  |  0  |  1  |  1  |
|  :::  |  SUMOYLATION  |  0  |  5  |  0  |  5  |  5  |
|  :::  |  UBIQUITINATION  |  0  |  34  |  0  |  34  |  33  |
|  :::  |  COMBINED  |  0  |  40  |  0  |  38  |  37  |
|  **//Saccharomyces cerevisiae (S288c)//**  |  NEDDYLATION  |  0  |  4  |  0  |  4  |  9  |
|  :::  |  PHOSPHORYLATION  |  19,981  |  0  |  3,165  |  3,165  |  528  |
|  :::  |  SUMOYLATION  |  0  |  818  |  0  |  818  |  66  |
|  :::  |  UBIQUITINATION  |  0  |  3,561  |  0  |  3,561  |  212  |
|  :::  |  COMBINED  |  19,981  |  4,383  |  3,165  |  4,443  |  798  |
|  **//Schizosaccharomyces pombe (972h)//**  |  UBIQUITINATION  |  0  |  4  |  0  |  4  |  3  |
|  :::  |  COMBINED  |  0  |  4  |  0  |  4  |  3  |
|  **//Xenopus laevis//**  |  UBIQUITINATION  |  0  |  2  |  0  |  2  |  2  |
|  :::  |  COMBINED  |  0  |  2  |  0  |  2  |  2  |
|  **//All Organisms//**  |  FAT10YLATION  |  0  |  659  |  0  |  659  |  5  |
|  :::  |  ISG15YLATION  |  0  |  7  |  0  |  7  |  6  |
|  :::  |  NEDDYLATION  |  0  |  915  |  0  |  915  |  82  |
|  :::  |  PHOSPHORYLATION  |  19,981  |  0  |  3,165  |  3,165  |  528  |
|  :::  |  SUMOYLATION  |  0  |  3,167  |  0  |  3,167  |  405  |
|  :::  |  UBIQUITINATION  |  0  |  13,820  |  0  |  13,820  |  3,444  |
|  :::  |  UFMYLATION  |  0  |  10  |  0  |  10  |  1  |
|  :::  |  COMBINED  |  19,981  |  18,578  |  3,165  |  14,999  |  4,317  |


[[statistics|View the Current Database Statistics]]