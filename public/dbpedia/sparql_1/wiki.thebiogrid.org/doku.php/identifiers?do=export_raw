====== Supported Identifiers for Searches ======

While we do support a wide variety of identifiers, our goal of limiting redundancy in our search results means that several large sequence databases such as Genbank and Ensembl do not have all their ID's available in the BioGRID. With each annotation update, our coverage increases by hundreds of thousands of new identifiers so please be patient if your ID is not currently supported. If you have a suggestion for a new identifier type, please let us know at **[[biogridadmin@gmail.com]]**.

**These are listed as follows in the BIOGRID-IDENTIFIERS-* files available at on our [[http://thebiogrid.org/download.php|Downloads]] page:**
  * ANIMALQTLDB
  * APHIDBASE
  * BEEBASE
  * BGD
  * BIOGRID
  * CGD
  * CGNC
  * DICTYBASE
  * ECOGENE
  * ENSEMBL
  * ENSEMBL_GENE
  * ENSEMBL_PROTEIN
  * ENSEMBL_RNA
  * ENTREZ_GENE
  * ENTREZ_GENE_ETG
  * FLYBASE
  * GRID_LEGACY
  * HGNC
  * HPRD
  * IMGT/GENE-DB
  * MAIZEGDB
  * MGI
  * MIM
  * MIRBASE
  * OFFICIAL_SYMBOL
  * ORDERED_LOCUS
  * PBR
  * REFSEQ
  * REFSEQ-LEGACY
  * REFSEQ-PROTEIN-ACCESSION
  * REFSEQ-PROTEIN-ACCESSION-VERSIONED
  * REFSEQ-PROTEIN-GI
  * REFSEQ-RNA-ACCESSION
  * REFSEQ-RNA-GI
  * RGD
  * SGD
  * SWISS-PROT
  * SYNONYM
  * SYSTEMATIC_NAME
  * TAIR
  * TREMBL
  * UNIPROT
  * UNIPROT-ACCESSION
  * UNIPROT-ISOFORM
  * UNIPROTKB
  * VECTORBASE
  * VEGA
  * WORMBASE
  * WORMBASE-OLD
  * XENBASE
  * ZFIN