<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
 lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    BioGRID Network Viewer   	| BioGRID
  </title>

  <meta name="generator" content="DokuWiki"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="network_viewer,start"/>
<link rel="search" type="application/opensearchdescription+xml" href="/lib/exe/opensearch.php" title="BioGRID Help and Support Resources"/>
<link rel="start" href="/"/>
<link rel="contents" href="/doku.php/network_viewer:start?do=index" title="Sitemap"/>
<link rel="alternate" type="application/rss+xml" title="Recent Changes" href="/feed.php"/>
<link rel="alternate" type="application/rss+xml" title="Current namespace" href="/feed.php?mode=list&amp;ns=network_viewer"/>
<link rel="alternate" type="text/html" title="Plain HTML" href="/doku.php/network_viewer:start?do=export_xhtml"/>
<link rel="alternate" type="text/plain" title="Wiki Markup" href="/doku.php/network_viewer:start?do=export_raw"/>
<link rel="canonical" href="http://wiki.thebiogrid.org/doku.php/network_viewer:start"/>
<link rel="stylesheet" type="text/css" href="/lib/exe/css.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"/>
<!--[if gte IE 9]><!-->
<script type="text/javascript">/*<![CDATA[*/var NS='network_viewer';var JSINFO = {"id":"network_viewer:start","namespace":"network_viewer"};
/*!]]>*/</script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f"></script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/js.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"></script>
<!--<![endif]-->

  <link rel="shortcut icon" href="/lib/tpl/biogrid3/images/favicon.ico" />

  </head>

<body>
<div id="wrap" class="inside">
    <div id="content">
    
    <div id="header" class="inside">
    
        <div id="navbar">
            
            <div id="logo">
                <a href="https://thebiogrid.org" title="BioGRID Search for Protein Interactions, Chemical Interactions, and Genetic Interactions">
                    <span class="unbold">Bio</span>GRID<sup>3.4</sup>
                </a>
            </div>
            
            <div id="links">
                <ul>
                	<li><a href="https://thebiogrid.org" title='Search the BioGRID'>home</a></li>
                    <li><a href="https://wiki.thebiogrid.org">help wiki</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/tools">tools</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/contribute">contribute</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/statistics">stats</a></li>
                    <li><a href="https://downloads.thebiogrid.org/BioGRID">downloads</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/partners">partners</a></li>
                    <li><a class="rightbar" href="https://wiki.thebiogrid.org/doku.php/aboutus" title='About the BIOGRID'>about us</a></li>
                    <li><a href="http://twitter.com/#!/biogrid" title='Follow @biogrid on Twitter'><img style='margin-top: -5px;' src='https://thebiogrid.org/images/twitter-button-small.png' alt='Follow Us on Twitter' /></a></li>
                    <!--<li><a href="{SITE_URL}">signup</a></li>
                    <li><a href="{SITE_URL}">login</a></li>-->
                </ul>
            </div>
            
        </div>
        
        <div id="insidetext">
            <h1>BioGRID Network Viewer</h1> 
        </div>
    
	</div>
    
<div class="dokuwiki">
  
  <div class="stylehead">

    
	<br />

        <div class="breadcrumbs">
      <span class="bchead">You are here: </span><span class="home"><bdi><a href="/doku.php/start" class="wikilink1" title="start">Help and Support Resources</a></bdi></span> » <bdi><span class="curid"><a href="/doku.php/network_viewer:start" class="wikilink1" title="network_viewer:start">BioGRID Network Viewer</a></span></bdi>    </div>
    
  </div>
  
  
  <div class="page">
    <!-- wikipage start -->
    
<h1 class="sectionedit1" id="biogrid_network_viewer">BioGRID Network Viewer</h1>
<div class="level1">

<p>
The BioGRID Network viewer is a graphical tool for visualising complex networks. You can access it from any of our result pages by simply choosing the “network” view. The following Network Viewer specific resources are also available…
</p>
<ul>
<li class="level1"><div class="li"> <a href="/doku.php/network_viewer:frequently_asked_questions" class="wikilink1" title="network_viewer:frequently_asked_questions">Frequently Asked Questions</a></div>
</li>
</ul>

</div>

<h2 class="sectionedit2" id="tools_and_technologies">Tools and Technologies</h2>
<div class="level2">

<p>
The BioGRID Network Viewer uses the following tools and technologies…
</p>
<ul>
<li class="level1"><div class="li"> <strong><a href="http://js.cytoscape.org/" class="urlextern" target="_blank" title="http://js.cytoscape.org/" rel="nofollow noopener">Cytoscape.js</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://arborjs.org/" class="urlextern" target="_blank" title="http://arborjs.org/" rel="nofollow noopener">Arbor.js</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://fortawesome.github.io/Font-Awesome/" class="urlextern" target="_blank" title="http://fortawesome.github.io/Font-Awesome/" rel="nofollow noopener">Font-Awesome</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong><a href="http://alertifyjs.com/" class="urlextern" target="_blank" title="http://alertifyjs.com/" rel="nofollow noopener">Alertify.js</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong><a href="https://github.com/cytoscape/cytoscape.js-panzoom" class="urlextern" target="_blank" title="https://github.com/cytoscape/cytoscape.js-panzoom" rel="nofollow noopener">Cytoscape.js Pan/Zoom Widget</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong><a href="https://github.com/cytoscape/cytoscape.js-cxtmenu" class="urlextern" target="_blank" title="https://github.com/cytoscape/cytoscape.js-cxtmenu" rel="nofollow noopener">Cytoscape.js Context Menu</a></strong></div>
</li>
</ul>

</div>

    <!-- wikipage stop -->
  </div>

  <div class="clearer">&nbsp;</div>

  
  <div class="stylefoot">

    <div class="meta">
      <div class="user">
              </div>
      <div class="doc">
        <bdi>network_viewer/start.txt</bdi> · Last modified: 2017/08/08 12:52 (external edit)      </div>
    </div>

   
    <div class="bar" id="bar__bottom">
      <div class="bar-left" id="bar__bottomleft">
      	      </div>
      <div class="bar-right" id="bar__bottomright">
                                        <a class="nolink" href="#dokuwiki__top"><input type="button" class="button" value="Back to top" onclick="window.scrollTo(0, 0)" title="Back to top" /></a>        <form class="button btn_login" method="get" action="/doku.php/network_viewer:start"><div class="no"><input type="hidden" name="do" value="login" /><input type="hidden" name="sectok" value="" /><button type="submit" title="Log In">Log In</button></div></form>      </div>
      <div class="clearer"></div>
    </div>

  </div>

</div>

<div class="no"><img src="/lib/exe/indexer.php?id=network_viewer%3Astart&amp;1516242461" width="2" height="1" alt="" /></div>
<div id="footer">
		Copyright &copy; 2018 <a target='_blank' href='http://www.tyerslab.com' title='Mike Tyers Lab'>TyersLab.com</a>, All Rights Reserved.
</div>

<div id="footer-links">
	<a href='{WIKI_URL}/doku.php/terms_and_conditions' title="BioGRID Terms and Conditions">Terms and Conditions</a> | 
    <a href='{WIKI_URL}/doku.php/privacy_policy' title="BioGRID Privacy Policy">Privacy Policy</a> | 
    <a target='_blank' href='https://osprey.thebiogrid.org' title="Osprey Network Visualization System">Osprey Network Visualization System</a> |
	<a target="_blank" href="https://yeastkinome.org" title="Yeast Kinome">Yeast Kinome</a> |
    <a target='_blank' href='http://www.tyerslab.com' title='Mike Tyers Lab Webpage'>TyersLab.com</a> |
    <a target='_blank' href='http://www.yeastgenome.org' title='Saccharomyces Genome Database'>SGD</a>
</div>

</div>
</div>

 <script type="text/javascript">
 var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
 document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
 </script>
 <script type="text/javascript">
 try {
 var pageTracker = _gat._getTracker("UA-239330-2");
 pageTracker._setDomainName(".thebiogrid.org");
 pageTracker._trackPageview();
 } catch(err) {}</script>
 


</body>
</html>
