<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
 lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    Editing Submitted Interactions   	| BioGRID
  </title>

  <meta name="generator" content="DokuWiki"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="curation_guide,editing_submitted_interactions"/>
<link rel="search" type="application/opensearchdescription+xml" href="/lib/exe/opensearch.php" title="BioGRID Help and Support Resources"/>
<link rel="start" href="/"/>
<link rel="contents" href="/doku.php/curation_guide:editing_submitted_interactions?do=index" title="Sitemap"/>
<link rel="alternate" type="application/rss+xml" title="Recent Changes" href="/feed.php"/>
<link rel="alternate" type="application/rss+xml" title="Current namespace" href="/feed.php?mode=list&amp;ns=curation_guide"/>
<link rel="alternate" type="text/html" title="Plain HTML" href="/doku.php/curation_guide:editing_submitted_interactions?do=export_xhtml"/>
<link rel="alternate" type="text/plain" title="Wiki Markup" href="/doku.php/curation_guide:editing_submitted_interactions?do=export_raw"/>
<link rel="canonical" href="http://wiki.thebiogrid.org/doku.php/curation_guide:editing_submitted_interactions"/>
<link rel="stylesheet" type="text/css" href="/lib/exe/css.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"/>
<!--[if gte IE 9]><!-->
<script type="text/javascript">/*<![CDATA[*/var NS='curation_guide';var JSINFO = {"id":"curation_guide:editing_submitted_interactions","namespace":"curation_guide"};
/*!]]>*/</script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f"></script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/js.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"></script>
<!--<![endif]-->

  <link rel="shortcut icon" href="/lib/tpl/biogrid3/images/favicon.ico" />

  </head>

<body>
<div id="wrap" class="inside">
    <div id="content">
    
    <div id="header" class="inside">
    
        <div id="navbar">
            
            <div id="logo">
                <a href="https://thebiogrid.org" title="BioGRID Search for Protein Interactions, Chemical Interactions, and Genetic Interactions">
                    <span class="unbold">Bio</span>GRID<sup>3.4</sup>
                </a>
            </div>
            
            <div id="links">
                <ul>
                	<li><a href="https://thebiogrid.org" title='Search the BioGRID'>home</a></li>
                    <li><a href="https://wiki.thebiogrid.org">help wiki</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/tools">tools</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/contribute">contribute</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/statistics">stats</a></li>
                    <li><a href="https://downloads.thebiogrid.org/BioGRID">downloads</a></li>
                    <li><a href="https://wiki.thebiogrid.org/doku.php/partners">partners</a></li>
                    <li><a class="rightbar" href="https://wiki.thebiogrid.org/doku.php/aboutus" title='About the BIOGRID'>about us</a></li>
                    <li><a href="http://twitter.com/#!/biogrid" title='Follow @biogrid on Twitter'><img style='margin-top: -5px;' src='https://thebiogrid.org/images/twitter-button-small.png' alt='Follow Us on Twitter' /></a></li>
                    <!--<li><a href="{SITE_URL}">signup</a></li>
                    <li><a href="{SITE_URL}">login</a></li>-->
                </ul>
            </div>
            
        </div>
        
        <div id="insidetext">
            <h1>Editing Submitted Interactions</h1> 
        </div>
    
	</div>
    
<div class="dokuwiki">
  
  <div class="stylehead">

    
	<br />

        <div class="breadcrumbs">
      <span class="bchead">You are here: </span><span class="home"><bdi><a href="/doku.php/start" class="wikilink1" title="start">Help and Support Resources</a></bdi></span> » <bdi><a href="/doku.php/curation_guide" class="wikilink1" title="curation_guide">BioGRID Curation Guide</a></bdi> » <bdi><span class="curid"><a href="/doku.php/curation_guide:editing_submitted_interactions" class="wikilink1" title="curation_guide:editing_submitted_interactions">Editing Submitted Interactions</a></span></bdi>    </div>
    
  </div>
  
  
  <div class="page">
    <!-- wikipage start -->
    
<h1 class="sectionedit1" id="editing_submitted_interactions">Editing Submitted Interactions</h1>
<div class="level1">

<p>
Submitted interactions can be edited by selecting the interaction in the left hand column of the IMS and clicking on the “Modify Annotation on Checked” button at the bottom of the entry form.
</p>

<p>
This will pull up a menu that will allow the curator to select one of the following:
</p>
<ul>
<li class="level1"><div class="li"> Throughput</div>
</li>
<li class="level1"><div class="li"> Qualification</div>
</li>
<li class="level1"><div class="li"> Phenotype</div>
</li>
<li class="level1"><div class="li"> Interaction Tag</div>
</li>
</ul>

<p>
in order to modify it for the selected interaction. After selecting a feature to modify the curator can remove erroneous information, overwrite erroneous information, or append missing information.
</p>

<p>
The curator can double check that modifications have gone through by clicking on the appropriate icon in the “Other” column in the Activated Interactions table.
</p>

<p>
Currently other features of an interaction, such as directionality or experimental system, cannot be edited but must be changed by submitting a new interaction with the appropriate information to the system.
</p>

<p>
<a href="/doku.php/curation_guide" class="wikilink1" title="curation_guide">Back to BioGRID Curation Guide</a>
</p>

</div>

    <!-- wikipage stop -->
  </div>

  <div class="clearer">&nbsp;</div>

  
  <div class="stylefoot">

    <div class="meta">
      <div class="user">
              </div>
      <div class="doc">
        <bdi>curation_guide/editing_submitted_interactions.txt</bdi> · Last modified: 2017/08/08 12:52 (external edit)      </div>
    </div>

   
    <div class="bar" id="bar__bottom">
      <div class="bar-left" id="bar__bottomleft">
      	      </div>
      <div class="bar-right" id="bar__bottomright">
                                        <a class="nolink" href="#dokuwiki__top"><input type="button" class="button" value="Back to top" onclick="window.scrollTo(0, 0)" title="Back to top" /></a>        <form class="button btn_login" method="get" action="/doku.php/curation_guide:editing_submitted_interactions"><div class="no"><input type="hidden" name="do" value="login" /><input type="hidden" name="sectok" value="" /><button type="submit" title="Log In">Log In</button></div></form>      </div>
      <div class="clearer"></div>
    </div>

  </div>

</div>

<div class="no"><img src="/lib/exe/indexer.php?id=curation_guide%3Aediting_submitted_interactions&amp;1516242707" width="2" height="1" alt="" /></div>
<div id="footer">
		Copyright &copy; 2018 <a target='_blank' href='http://www.tyerslab.com' title='Mike Tyers Lab'>TyersLab.com</a>, All Rights Reserved.
</div>

<div id="footer-links">
	<a href='{WIKI_URL}/doku.php/terms_and_conditions' title="BioGRID Terms and Conditions">Terms and Conditions</a> | 
    <a href='{WIKI_URL}/doku.php/privacy_policy' title="BioGRID Privacy Policy">Privacy Policy</a> | 
    <a target='_blank' href='https://osprey.thebiogrid.org' title="Osprey Network Visualization System">Osprey Network Visualization System</a> |
	<a target="_blank" href="https://yeastkinome.org" title="Yeast Kinome">Yeast Kinome</a> |
    <a target='_blank' href='http://www.tyerslab.com' title='Mike Tyers Lab Webpage'>TyersLab.com</a> |
    <a target='_blank' href='http://www.yeastgenome.org' title='Saccharomyces Genome Database'>SGD</a>
</div>

</div>
</div>

 <script type="text/javascript">
 var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
 document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
 </script>
 <script type="text/javascript">
 try {
 var pageTracker = _gat._getTracker("UA-239330-2");
 pageTracker._setDomainName(".thebiogrid.org");
 pageTracker._trackPageview();
 } catch(err) {}</script>
 


</body>
</html>
