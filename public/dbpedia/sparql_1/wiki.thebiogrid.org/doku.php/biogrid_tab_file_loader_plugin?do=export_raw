====== BioGRID TAB File Loader Plugin for Cytoscape ======

**This is an older version of our Plugin that is supported for legacy uses only. We recommend that you use our new
[[biogridplugin2|BioGRID Plugin 2.0]] for any new projects.**

===== Version 0.2 =====
Update to accommodate new evidence codes 'Positive Genetic' and 'Negative Genetic'.

===== Version 0.1 =====

This plugin can be downloaded from [[http://www.thebiogrid.org/downloads.php]] or [[http://www.cytoscape.org/]]. It has been developed to make it simpler to import data from BioGRID .tab files, and to allow multiple redundant interactions (evidences), represented by a single line in a .tab file, to be imported into Cytoscape as a single edge with multiple evidence code and publication annotation.

The BioGRID Cytoscape plugin has been tested on Cytoscape 2.6.0 in a Windows, Mac and Linux environments. Please report any bugs or feature requests to biogridadmin@gmail.com. To install the BioGRID Cytoscape plugin, drop BiogridPlugin.jar into your Cytoscape plugins folder (e.g. /Cytoscape_2.6.0/plugins) and restart Cytoscape.
Click the Tools menu option "Import BioGRID tab file" and use the file chooser to open a .tab file you have downloaded from www.thebiogrid.org/downloads.php.

A network (and corresponding view) will be created with the same name as the file you imported, and will have the attributes as described below.

==== Node attributes ====

**ID:** the value of the INTERACTOR_A or INTERACTOR_B columns of the .tab file.\\ 
**biogrid.OFFICIAL_SYMBOL:** the value of the OFFICIAL_SYMBOL_A or OFFICIAL_SYMBOL_B columns of the .tab file.\\ 
**biogrid.ALIASES:** a list of values corresponding to gene/protein aliases found as pipe separated values in the ALIASES_FOR_A or ALIASES_FOR_B columns of the .tab file.\\ 

==== Edge attributes ====
Please note that each row in a .tab file represents a unique combination of interactors, experimental system and publication, whereas edges in the Cytoscape network represent a unique pair of interactors only. Therefore edges in the Cytoscape network can have multiple experimental systems and pubmed ids.\\ 

**ID:** the IDs of the connected nodes separated by "(pp)".\\ 
**biogrid.EXPERIMENTAL_SYSTEM:** a list of values corresponding to the EXPERIMENTAL_SYSTEM column of the .tab file (see www.wiki.thebiogrid.org/doku.php/#evidence_codes).\\ 
**biogrid.PUBMED_ID:** a list of values corresponding to the PUBMED_ID column of the .tab file.\\ 
**biogrid.SOURCE:** a list of values corresponding to the SOURCE column of the .tab file.\\ 
**biogrid.INTERACTION_TYPE:** set to the value 'Physical' if the EXPERIMENTAL_SYSTEM list has only physical experimental systems, 'Genetic' if the list has only genetic experimental systems, and 'PhysicalANDGenetic' if the list has both physical and genetic experimental systems. Experimental systems are defined as physical or genetic according to www.wiki.thebiogrid.org/doku.php/#evidence_codes.\\ 
**biogrid.NUMBER_OF_EXPERIMENTAL_SYSTEMS:** the number of values in the EXPERIMENTAL_SYSTEM list.\\ 
**biogrid.NUMBER_OF_PMIDS:** the number of values in the PUBMED_ID and SOURCE lists.\\ 
**biogrid.TOTAL_RAW:** the number of rows in the .tab file corresponding to this edge.\\ 

==== Visual Style ====
A default visual style has been applied to the network as follows:\\ 
Nodes are light blue circles and have biogrid.OFFICIAL_SYMBOL as labels.\\ 
Edge colors are green for edges with biogrid.INTERACTION_TYPE=Genetic, orange for edges with biogrid.INTERACTION_TYPE=Physical, and blue for edges with biogrid.INTERACTION_TYPE=PhysicalANDGenetic.\\ 
Edge thicknesses are the default thickness of 1 for edges with biogrid.NUMBER_OF_EXPERIMENTAL_SYSTEMS=1, and 3 for edges with biogrid.NUMBER_OF_EXPERIMENTAL_SYSTEMS>1.\\ 
