======Online Tools and Resources======
Listed on this page are tools, software, and resources either written by the BioGRID team or a third party that can help you make use of BioGRID interaction data. All tools and resources are released **WITHOUT ANY WARRANTY** and are free to both academic and commercial entities for research purposes only. If you know of a useful software tool that should be in this listing, let us know at **[[biogridadmin@gmail.com]]**.
===== Partner Resources =====
  * **[[https://phosphogrid.org|PhosphoGRID]]** - PhosphoGRID records the positions of more than 5000 specific phosphorylated residues on 1495 gene products in Saccharomyces cerevisiae. Each site is documented with a hierarchy of experimental evidence codes.
  * **[[https://yeastkinome.org|Yeast Kinome]]** - Saccharomyces cerevisiae Kinase and Phosphatase Interactome (KPI) Resource.
  * **[[http://www.pathwaycommons.org/pc/home.do|Pathway Commons]]** - Pathway Commons is a collection of publicly available pathways from multiple organisms. It provides researchers with convenient access to a comprehensive collection of pathways from multiple sources represented in a common language.
  * **[[http://wodaklab.org/iRefWeb|iRefWeb]]** - The iRefWeb interface groups interaction records from the different databases into a single non-redundant view. 

===== Network Visualization =====
  * **[[https://osprey.thebiogrid.org|Osprey: Network Visualization System]]** - Osprey is a software platform for the visualization of complex interaction networks. Osprey builds data-rich graphical representations from Gene Ontology (GO) annotated interaction data maintained by the BioGRID.
  * **[[http://www.cytoscape.org/|Cytoscape]]** - Cytoscape is an open source bioinformatics software platform for visualizing molecular interaction networks and integrating these interactions with gene expression profiles.
  * **[[http://genemania.org|GeneMANIA]]** - GeneMANIA finds genes that are related to a set of input genes, using a very large set of functional association data.
  * **[[http://www.esyn.org|esyN]]** - esyN displays online interaction networks integrated with major databases so you can retrieve data automatically as you construct the network.
===== Data Management =====
  * **[[http://prohitsms.com/|ProHits]]** - Prohits is a laboratory information management system for processing large scale mass spectrometry experiments. Prohits is closely integrated with the BioGRID providing real-time comparisons between experimental results and published interactions.

===== Plugins =====
  * **[[biogrid_tab_file_loader_plugin|BioGRID Tab File Loader Plugin for Cytoscape]]** - This plugin was developed to make it simpler to import data from BioGRID .tab files, and to allow multiple redundant interactions (evidences), represented by a single line in a .tab file, to be imported into Cytoscape as a single edge with multiple evidence codes and publication annotation.
  * **[[biogridplugin2|BiogridPlugin2 for Cytoscape]]** - This plugin supercedes the BioGRID Tab File Loader allowing import of data from our new tab2 file format, ability to filter data during import based on gene lists and various interaction attributes, the capacity to expand network nodes and saving of networks to tab2 files.

===== Web Services =====
  * **[[biogridrest|BioGRID REST Service]]** - BioGRID interaction data is now available programatically over HTTP through a REST-style web service. 
  * **[[http://code.google.com/p/psicquic/|PSICQUIC]]** - PSICQUIC is a web service that attempts to standardize access to molecular interaction databases programatically.