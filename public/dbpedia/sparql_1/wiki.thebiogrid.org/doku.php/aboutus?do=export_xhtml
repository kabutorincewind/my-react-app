<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <title>aboutus</title>
<meta name="generator" content="DokuWiki"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="aboutus"/>
<link rel="search" type="application/opensearchdescription+xml" href="/lib/exe/opensearch.php" title="BioGRID Help and Support Resources"/>
<link rel="start" href="/"/>
<link rel="contents" href="/doku.php/aboutus?do=index" title="Sitemap"/>
<link rel="alternate" type="application/rss+xml" title="Recent Changes" href="/feed.php"/>
<link rel="alternate" type="application/rss+xml" title="Current namespace" href="/feed.php?mode=list&amp;ns="/>
<link rel="alternate" type="text/html" title="Plain HTML" href="/doku.php/aboutus?do=export_xhtml"/>
<link rel="alternate" type="text/plain" title="Wiki Markup" href="/doku.php/aboutus?do=export_raw"/>
<link rel="canonical" href="http://wiki.thebiogrid.org/doku.php/aboutus"/>
<link rel="stylesheet" type="text/css" href="/lib/exe/css.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"/>
<!--[if gte IE 9]><!-->
<script type="text/javascript">/*<![CDATA[*/var NS='';var JSINFO = {"id":"aboutus","namespace":""};
/*!]]>*/</script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f"></script>
<script type="text/javascript" charset="utf-8" src="/lib/exe/js.php?t=biogrid3&amp;tseed=f30835c13617d2f792379b3e0590c114"></script>
<!--<![endif]-->
</head>
<body>
<div class="dokuwiki export">
<!-- TOC START -->
<div id="dw__toc">
<h3 class="toggle">Table of Contents</h3>
<div>

<ul class="toc">
<li class="level1"><div class="li"><a href="#about_the_biogrid">About the BioGRID</a></div>
<ul class="toc">
<li class="level2"><div class="li"><a href="#biogrid_team">BioGRID Team</a></div></li>
<li class="level2"><div class="li"><a href="#biogrid_funding">BioGRID Funding</a></div></li>
<li class="level2"><div class="li"><a href="#biogrid_publications">BioGRID Publications</a></div></li>
<li class="level2"><div class="li"><a href="#biogrid_affiliations">BioGRID Affiliations</a></div></li>
</ul></li>
</ul>
</div>
</div>
<!-- TOC END -->

<h1 class="sectionedit1" id="about_the_biogrid">About the BioGRID</h1>
<div class="level1">

<p>
The Biological General Repository for Interaction Datasets (BioGRID) is a public database that archives and disseminates genetic and protein interaction data from model organisms and humans (<strong><a href="https://thebiogrid.org" class="urlextern" target="_blank" title="https://thebiogrid.org" rel="nofollow noopener">thebiogrid.org</a></strong>). BioGRID currently holds over <strong><a href="/doku.php/statistics" class="wikilink1" title="statistics">1,400,000 interactions</a></strong> curated from both high-throughput datasets and individual focused studies, as derived from over 57,000 publications in the primary literature. Complete coverage of the entire literature is maintained for budding yeast (S. cerevisiae), fission yeast (S. pombe) and thale cress (A. thaliana), and efforts to expand curation across multiple metazoan species are underway. Current curation drives are focused on particular areas of biology to enable insights into conserved networks and pathways that are relevant to human health. The BioGRID 3.2 web interface contains new search and display features that enable rapid queries across multiple data types and sources. BioGRID provides interaction data to several model organism databases, resources such as <strong><a href="http://www.ncbi.nlm.nih.gov/gene" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/gene" rel="nofollow noopener">Entrez-Gene</a></strong>, <strong><a href="http://www.yeastgenome.org/" class="urlextern" target="_blank" title="http://www.yeastgenome.org/" rel="nofollow noopener">SGD</a></strong>, <strong><a href="http://www.arabidopsis.org/" class="urlextern" target="_blank" title="http://www.arabidopsis.org/" rel="nofollow noopener">TAIR</a></strong>, <strong><a href="http://flybase.org/" class="urlextern" target="_blank" title="http://flybase.org/" rel="nofollow noopener">FlyBase</a></strong> and other <strong><a href="https://wiki.thebiogrid.org/doku.php/partners" class="urlextern" target="_blank" title="https://wiki.thebiogrid.org/doku.php/partners" rel="nofollow noopener">interaction meta-databases</a></strong>. The entire BioGRID 3.2 data collection may be <strong><a href="http://thebiogrid.org/download.php" class="urlextern" target="_blank" title="http://thebiogrid.org/download.php" rel="nofollow noopener">downloaded in multiple file formats</a></strong>, including <strong><a href="http://www.imexconsortium.org" class="urlextern" target="_blank" title="http://www.imexconsortium.org" rel="nofollow noopener">IMEx</a></strong> compatible PSI MI XML. For developers, BioGRID interactions are also available via a <strong><a href="/doku.php/biogridrest" class="wikilink1" title="biogridrest">REST based Web Service</a></strong> and <strong><a href="/doku.php/biogridplugin2" class="wikilink1" title="biogridplugin2">Cytoscape plugin</a></strong>. All BioGRID documentation is available online in the <strong><a href="https://wiki.thebiogrid.org" class="urlextern" target="_blank" title="https://wiki.thebiogrid.org" rel="nofollow noopener">BioGRID Wiki</a></strong>.
</p>

<p>
<strong> Contact Information </strong>
</p>
<ul>
<li class="level1"><div class="li"> <strong>EMAIL</strong>: <strong><a href="mailto:&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;" class="mail" title="&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;">&#x62;&#x69;&#x6f;&#x67;&#x72;&#x69;&#x64;&#x61;&#x64;&#x6d;&#x69;&#x6e;&#x40;&#x67;&#x6d;&#x61;&#x69;&#x6c;&#x2e;&#x63;&#x6f;&#x6d;</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong>TWITTER</strong>: <strong><a href="http://twitter.com/#!/biogrid" class="urlextern" target="_blank" title="http://twitter.com/#!/biogrid" rel="nofollow noopener">@biogrid</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong>YOUTUBE</strong>: <strong><a href="https://www.youtube.com/channel/UCIdwfNwL9gi4oLBUqDB189g" class="urlextern" target="_blank" title="https://www.youtube.com/channel/UCIdwfNwL9gi4oLBUqDB189g" rel="nofollow noopener">BioGRID</a></strong></div>
</li>
<li class="level1"><div class="li"> <strong>GITHUB</strong>: <strong><a href="https://github.com/BioGRID" class="urlextern" target="_blank" title="https://github.com/BioGRID" rel="nofollow noopener">BioGRID</a></strong></div>
</li>
</ul>

</div>
<!-- EDIT1 SECTION "About the BioGRID" [1-2102] -->
<h2 class="sectionedit2" id="biogrid_team">BioGRID Team</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> <strong>Lorrie Boucher</strong> [curator, Toronto]<a href="http://www.princeton.edu/main/" class="media" target="_blank" title="http://www.princeton.edu/main/" rel="nofollow noopener"><img src="/lib/exe/fetch.php/princeton_university.gif.png" class="mediaright" alt="" /></a></div>
</li>
<li class="level1"><div class="li"> <strong>Bobby-Joe Breitkreutz</strong> [software engineer, Toronto]</div>
</li>
<li class="level1"><div class="li"> <strong>Christie Chang</strong> [curator, Princeton]</div>
</li>
<li class="level1"><div class="li"> <strong>Andrew Chatr-Aryamontri</strong> [curator, Montreal]<a href="http://www.iric.ca/" class="media" target="_blank" title="http://www.iric.ca/" rel="nofollow noopener"><img src="/lib/exe/fetch.php/universityofmontreal.jpg" class="mediaright" alt="" /></a></div>
</li>
<li class="level1"><div class="li"> <strong>Kara Dolinski</strong> [co-principal investigator, Princeton]</div>
</li>
<li class="level1"><div class="li"> <strong>Nadine Kolas</strong> [curator, Toronto]<a href="http://www.ed.ac.uk/home" class="media" target="_blank" title="http://www.ed.ac.uk/home" rel="nofollow noopener"><img src="/lib/exe/fetch.php/edinburgh.jpg" class="mediaright" alt="" /></a></div>
</li>
<li class="level1"><div class="li"> <strong>Lara O’Donnell</strong> [curator, Toronto]</div>
</li>
<li class="level1"><div class="li"> <strong>Sara Oster</strong> [curator, Toronto]</div>
</li>
<li class="level1"><div class="li"> <strong>Rose Oughtred</strong> [curator, Princeton]</div>
</li>
<li class="level1"><div class="li"> <strong>Jennifer Rust</strong> [curator, Princeton]<a href="http://www.lunenfeld.ca/" class="media" target="_blank" title="http://www.lunenfeld.ca/" rel="nofollow noopener"><img src="/lib/exe/fetch.php/slri_logo.gif" class="mediaright" alt="" /></a></div>
</li>
<li class="level1"><div class="li"> <strong>Adnane Sellam</strong> [curator, Laval]</div>
</li>
<li class="level1"><div class="li"> <strong>Chris Stark</strong> [software engineer, Toronto]</div>
</li>
<li class="level1"><div class="li"> <strong>Chandra Theesfeld</strong> [curator, Princeton]</div>
</li>
<li class="level1"><div class="li"> <strong>Mike Tyers</strong> [principal investigator, Montreal]</div>
</li>
</ul>

</div>
<!-- EDIT2 SECTION "BioGRID Team" [2103-3009] -->
<h3 class="sectionedit3" id="alumni">Alumni</h3>
<div class="level3">
<ul>
<li class="level1"><div class="li"> <strong>Ashton Breitkreutz</strong> [curator, Toronto]</div>
</li>
<li class="level1"><div class="li"> <strong>Daici Chen</strong> [curator, Montreal] </div>
</li>
<li class="level1"><div class="li"> <strong>Sven Heinicke</strong> [software engineer, Princeton]</div>
</li>
<li class="level1"><div class="li"> <strong>Jodi Hirschman</strong> [curator, Princeton]</div>
</li>
<li class="level1"><div class="li"> <strong>Michael Livstone</strong> [curator, Princeton]</div>
</li>
<li class="level1"><div class="li"> <strong>Julie Nixon</strong> [curator, Edinburgh]</div>
</li>
<li class="level1"><div class="li"> <strong>Lindsay Ramage</strong> [curator, Edinburgh]</div>
</li>
<li class="level1"><div class="li"> <strong>Teresa Reguly</strong> [curator, Toronto]</div>
</li>
<li class="level1"><div class="li"> <strong>Jean Tang</strong> [curator, Toronto]</div>
</li>
<li class="level1"><div class="li"> <strong>Andrew Winter</strong> [curator, Edinburgh]</div>
</li>
</ul>

</div>
<!-- EDIT3 SECTION "Alumni" [3010-3461] -->
<h2 class="sectionedit4" id="biogrid_funding">BioGRID Funding</h2>
<div class="level2">

<p>
<a href="http://www.nih.gov/" class="media" target="_blank" title="http://www.nih.gov/" rel="nofollow noopener"><img src="/lib/exe/fetch.php/nih-logo.jpg" class="media" alt="" /></a><a href="http://www.cihr-irsc.gc.ca/" class="media" target="_blank" title="http://www.cihr-irsc.gc.ca/" rel="nofollow noopener"><img src="/lib/exe/fetch.php/cihr_logo_big_e.png?w=225&amp;tok=753aaf" class="media" alt="" width="225" /></a><a href="http://www.genomecanada.ca/en/" class="media" target="_blank" title="http://www.genomecanada.ca/en/" rel="nofollow noopener"><img src="/lib/exe/fetch.php/genome_canada.jpg?w=225&amp;tok=3cf647" class="media" alt="" width="225" /></a><a href="http://www.genomequebec.com/" class="media" target="_blank" title="http://www.genomequebec.com/" rel="nofollow noopener"><img src="/lib/exe/fetch.php/genome_quebec.jpg?w=225&amp;tok=f3c63e" class="media" alt="" width="225" /></a>
</p>

<p>
NIH - <strong>#R01 RR024031</strong>
</p>

</div>
<!-- EDIT4 SECTION "BioGRID Funding" [3462-3742] -->
<h2 class="sectionedit5" id="biogrid_publications">BioGRID Publications</h2>
<div class="level2">

<p>
<a href="/lib/exe/detail.php/news.png?id=aboutus" class="media" title="news.png"><img src="/lib/exe/fetch.php/news.png" class="mediaright" alt="" /></a>
</p>
<ul>
<li class="level1"><div class="li"> Chatr-Aryamontri A, Oughtred R, Boucher L, Rust J, Chang C, Kolas NK, O&#039;Donnell L, Oster S, Theesfeld C, Sellam A, Stark C, Breitkreutz BJ, Dolinski K, Tyers M. <strong>The BioGRID interaction database: 2017 update</strong>. Nucleic Acids Res. 2016 Dec 14;2017(1) [ <a href="https://www.ncbi.nlm.nih.gov/pubmed/27980099" class="urlextern" target="_blank" title="https://www.ncbi.nlm.nih.gov/pubmed/27980099" rel="nofollow noopener">Pubmed</a>, <a href="http://nar.oxfordjournals.org/content/45/D1/D369" class="urlextern" target="_blank" title="http://nar.oxfordjournals.org/content/45/D1/D369" rel="nofollow noopener">NAR</a> ]</div>
</li>
<li class="level1"><div class="li"> Oughtred R, Chatr-Aryamontri A, Breitkreutz BJ, Chang CS, Rust JM, Theesfeld CL, Heinicke S, Breitkreutz A, Chen D, Hirschman J, Kolas N, Livstone MS, Nixon J, O&#039;Donnell L, Ramage L, Winter A, Reguly T, Sellam A, Stark C, Boucher L, Dolinski K, Tyers M. <strong>Use of the BioGRID Database for Analysis of Yeast Protein and Genetic Interactions</strong>. Cold Spring Harb Protoc. 2016 Jan 4;2016(1):pdb.prot088880 [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/26729909" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/26729909" rel="nofollow noopener">Pubmed</a> ] </div>
</li>
<li class="level1"><div class="li"> Oughtred R, Chatr-Aryamontri A, Breitkreutz BJ, Chang CS, Rust JM, Theesfeld CL, Heinicke S, Breitkreutz A, Chen D, Hirschman J, Kolas N, Livstone MS, Nixon J, O&#039;Donnell L, Ramage L, Winter A, Reguly T, Sellam A, Stark C, Boucher L, Dolinski K, Tyers M. <strong>BioGRID: A Resource for Studying Biological Interactions in Yeast</strong>. Cold Spring Harb Protoc. 2016 Jan 4;2016(1):pdb.top080754 [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/26729913" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/26729913" rel="nofollow noopener">Pubmed</a> ] </div>
</li>
<li class="level1"><div class="li"> Wildenhain J, Spitzer M, Dolma S, Jarvik N, White R, Roy M, Griffiths E, Bellows DS, Wright GD, Tyers M. <strong>Prediction of Synergism from Chemical-Genetic Interactions by Machine Learning.</strong> Cell Systems. Dec. 23, 2015, 1(6):383-95. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/27136353" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/27136353" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Chatr-Aryamontri A, Breitkreutz BJ, Oughtred R, Boucher L, Heinicke S, Chen D, Stark C, Breitkreutz A, Kolas N, O&#039;Donnell L, Reguly T, Nixon J, Ramage L, Winter A, Sellam A, Chang C, Hirschman J, Theesfeld C, Rust J, Livstone MS, Dolinski K, Tyers M. <strong>The BioGRID interaction database: 2015 update.</strong> Nucleic Acids Research. Nov. 2014, [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/25428363" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/25428363" rel="nofollow noopener">Pubmed</a> ] </div>
</li>
<li class="level1"><div class="li"> Sadowski I, Breitkreutz BJ, Stark C, Su TC, Dahabieh M, Raithatha S, Bernhard W, Oughtred R, Dolinski K, Barreto K, Tyers M. <strong>The PhosphoGRID Saccharomyces cerevisiae Protein Phosphorylation Site Database: Version 2.0 Update.</strong> Database (Oxford) May 2013; [ <a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3653121/" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3653121/" rel="nofollow noopener">Pubmed</a> ] </div>
</li>
<li class="level1"><div class="li"> Chatr-Aryamontri A, Breitkreutz BJ, Heinicke S, Boucher L, Winter A, Stark C, Nixon J, Ramage L, Kolas N, O&#039;Donnell L, Reguly T, Breitkreutz A, Sellam A, Chen D, Chang C, Rust JM, Livstone MS, Oughtred R, Dolinski K, Tyers M. <strong>The BioGRID Interaction Database: 2013 update.</strong> Nucleic Acids Res. 2012 Nov 30. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/23203989" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/23203989" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Winter AG, Wildenhain J, Tyers M. <strong>BioGRID REST Service, BiogridPlugin2 and BioGRID WebGraph: new tools for access to interaction data at BioGRID.</strong> Bioinformatics, 2011 Apr 1. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/21300700" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/21300700" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Stark C, Breitkreutz BJ, Chatr-Aryamontri A, Boucher L, Oughtred R, Livstone MS, Nixon J, Van Auken K, Wang X, Shi X, Reguly T, Rust JM, Winter A, Dolinski K, Tyers M. <strong>The BioGRID Interaction Database: 2011 update.</strong> Nucleic Acids Res. 2010 Nov 11. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/21071413" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/21071413" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Breitkreutz A, Choi H, Sharom JR, Boucher L, Neduva V, Larsen B, Lin ZY, Breitkreutz BJ, Stark C, Liu G, Ahn J, Dewar-Darch D, Reguly T, Tang X, Almeida R, Qin ZS, Pawson T, Gingras AC, Nesvizhskii AI, Tyers M.  <strong>A global protein kinase and phosphatase interaction network in yeast.</strong> Science. 2010 May 21;328(5981):1043-6. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/20489023" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/20489023" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Stark C, Ting-Cheng Su, Breitkreutz A, Lourenco P, Dahabieh M, Breitkreutz BJ, Tyers M, Sadowski I. <strong>PhosphoGRID: a database of experimentally verified in vivo protein phosphorylation sites from the budding yeast Saccharomyces cerevisiae.</strong> Database. 2010 Jan; Vol. 2010 [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/20428315" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/20428315" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Salwinski L, Licata L, Winter A, Thorneycroft D, Khadake J, Ceol A, Aryamontri AC, Oughtred R, Livstone M, Boucher L, Botstein D, Dolinski K, Berardini T, Huala E, Tyers M, Eisenberg D, Cesareni G, Hermjakob H. <strong>Recurated protein interaction datasets.</strong> Nat Methods. 2009 Jan;6(1):39-46. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/19935838" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/19935838" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Breitkreutz BJ, Stark C, Reguly T, Boucher L, Breitkreutz A, Livstone M, Oughtred R, Lackner DH, Bähler J, Wood V, Dolinski K, Tyers M. <strong>The BioGRID Interaction Database: 2008 update.</strong> Nucleic Acids Res. 2008 Jan;36(Database issue):D637-40. Epub 2007 Nov 13. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/18000002" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/18000002" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Reguly T, Breitkreutz A, Boucher L, Breitkreutz BJ, Hon GC, Myers CL, Parsons A, Friesen H, Oughtred R, Tong A, Stark C, Ho Y, Botstein D, Andrews B, Boone C, Troyanskya OG, Ideker T, Dolinski K, Batada NN, Tyers M. <strong>Comprehensive curation and analysis of global interaction networks in Saccharomyces cerevisiae.</strong> J Biol. 2006;5(4):11. Epub 2006 Jun 8. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/16762047" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/16762047" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Stark C, Breitkreutz BJ, Reguly T, Boucher L, Breitkreutz A, Tyers M. <strong>BioGRID: a general repository for interaction datasets.</strong> Nucleic Acids Res. 2006 Jan 1;34(Database issue):D535-9. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/16381927" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/16381927" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Breitkreutz BJ, Stark C, Tyers M. <strong>The GRID: the General Repository for Interaction Datasets.</strong> Genome Biol. 2003;4(3):R23. Epub 2003 Feb 27. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/12620108" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/12620108" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
<li class="level1"><div class="li"> Breitkreutz BJ, Stark C, Tyers M. <strong>Osprey: a network visualization system.</strong> Genome Biol. 2003;4(3):R23. Epub 2003 Feb 27. [ <a href="http://www.ncbi.nlm.nih.gov/pubmed/12620107" class="urlextern" target="_blank" title="http://www.ncbi.nlm.nih.gov/pubmed/12620107" rel="nofollow noopener">Pubmed</a> ]</div>
</li>
</ul>

</div>
<!-- EDIT5 SECTION "BioGRID Publications" [3743-9491] -->
<h2 class="sectionedit6" id="biogrid_affiliations">BioGRID Affiliations</h2>
<div class="level2">

<p>
<a href="/lib/exe/detail.php/globe2.png?id=aboutus" class="media" title="globe2.png"><img src="/lib/exe/fetch.php/globe2.png" class="mediaright" alt="" /></a>
</p>
<div class="table sectionedit7"><table class="inline">
	<tr class="row0">
		<td class="col0 leftalign"> <a href="https://www.systemsbiology.org/people/faculty/aitchison-lab/" class="urlextern" target="_blank" title="https://www.systemsbiology.org/people/faculty/aitchison-lab/" rel="nofollow noopener">John Aitchison Lab</a>    </td><td class="col1 leftalign"> <a href="http://sites.utoronto.ca/andrewslab/people.shtml" class="urlextern" target="_blank" title="http://sites.utoronto.ca/andrewslab/people.shtml" rel="nofollow noopener">Brenda Andrews Lab</a>     </td><td class="col2 leftalign"> <a href="http://baderlab.org/Home" class="urlextern" target="_blank" title="http://baderlab.org/Home" rel="nofollow noopener">Gary Bader Lab</a>        </td><td class="col3 leftalign"> <a href="http://www.bahlerlab.info/" class="urlextern" target="_blank" title="http://www.bahlerlab.info/" rel="nofollow noopener">Jürg Bähler Lab</a>    </td>
	</tr>
	<tr class="row1">
		<td class="col0 leftalign"> <a href="https://www.jax.org/research-and-faculty/research-labs/the-blake-lab" class="urlextern" target="_blank" title="https://www.jax.org/research-and-faculty/research-labs/the-blake-lab" rel="nofollow noopener">Judy Blake Lab</a>    </td><td class="col1 leftalign"> <a href="http://www.bs.jhmi.edu/MBG/boekelab/" class="urlextern" target="_blank" title="http://www.bs.jhmi.edu/MBG/boekelab/" rel="nofollow noopener">Jef Boeke Lab</a>     </td><td class="col2 leftalign"> <a href="http://sites.utoronto.ca/boonelab/" class="urlextern" target="_blank" title="http://sites.utoronto.ca/boonelab/" rel="nofollow noopener">Charlie Boone Lab</a>        </td><td class="col3 leftalign"> <a href="http://www.princeton.edu/genomics/botstein/" class="urlextern" target="_blank" title="http://www.princeton.edu/genomics/botstein/" rel="nofollow noopener">David Botstein Lab</a>    </td>
	</tr>
	<tr class="row2">
		<td class="col0 leftalign"> <a href="http://mint.bio.uniroma2.it/mint/Welcome.do" class="urlextern" target="_blank" title="http://mint.bio.uniroma2.it/mint/Welcome.do" rel="nofollow noopener">Gianni Cesarini Lab</a>    </td><td class="col1 leftalign"> <a href="https://cherrylab.stanford.edu/" class="urlextern" target="_blank" title="https://cherrylab.stanford.edu/" rel="nofollow noopener">Mike Cherry Lab</a>     </td><td class="col2 leftalign"> <a href="http://proteome.wayne.edu/index.html" class="urlextern" target="_blank" title="http://proteome.wayne.edu/index.html" rel="nofollow noopener">Russ Finley Lab</a>      </td><td class="col3 leftalign"> <a href="http://www.embl.de/research/units/scb/gavin/members/?s_personId=4074" class="urlextern" target="_blank" title="http://www.embl.de/research/units/scb/gavin/members/?s_personId=4074" rel="nofollow noopener">Anne-Claude Gavin Lab</a>    </td>
	</tr>
	<tr class="row3">
		<td class="col0 leftalign"> <a href="http://virus.chem.ucla.edu/" class="urlextern" target="_blank" title="http://virus.chem.ucla.edu/" rel="nofollow noopener">Bill Gelbart Lab</a>    </td><td class="col1 leftalign"> <a href="http://gingraslab.lunenfeld.ca" class="urlextern" target="_blank" title="http://gingraslab.lunenfeld.ca" rel="nofollow noopener">Anne-Claude Gingras Lab</a>     </td><td class="col2 leftalign"> <a href="http://www.ebi.ac.uk/~hhe/" class="urlextern" target="_blank" title="http://www.ebi.ac.uk/~hhe/" rel="nofollow noopener">Henning Hermjakob Lab</a>      </td><td class="col3 leftalign"> <a href="http://www.arabidopsis.org/about/staff.jsp" class="urlextern" target="_blank" title="http://www.arabidopsis.org/about/staff.jsp" rel="nofollow noopener">Eva Huala Lab</a>    </td>
	</tr>
	<tr class="row4">
		<td class="col0 leftalign"> <a href="http://chianti.ucsd.edu/idekerlab/" class="urlextern" target="_blank" title="http://chianti.ucsd.edu/idekerlab/" rel="nofollow noopener">Trey Ideker Lab</a>    </td><td class="col1 leftalign"> <a href="https://harper.hms.harvard.edu/" class="urlextern" target="_blank" title="https://harper.hms.harvard.edu/" rel="nofollow noopener">Wade Harper Lab</a>    </td><td class="col2 leftalign"> <a href="http://www.morrislab.ca/" class="urlextern" target="_blank" title="http://www.morrislab.ca/" rel="nofollow noopener">Quaid Morris Lab</a>      </td><td class="col3 leftalign"> <a href="http://pawsonlab.mshri.on.ca/" class="urlextern" target="_blank" title="http://pawsonlab.mshri.on.ca/" rel="nofollow noopener">Tony Pawson Lab</a>    </td>
	</tr>
	<tr class="row5">
		<td class="col0 leftalign"> <a href="http://www.bc.biol.ethz.ch/research/peter/people/matthias-peter.html" class="urlextern" target="_blank" title="http://www.bc.biol.ethz.ch/research/peter/people/matthias-peter.html" rel="nofollow noopener">Matthias Peter Lab</a>    </td><td class="col1 leftalign"> <a href="https://labs.oicr.on.ca/francis-lab" class="urlextern" target="_blank" title="https://labs.oicr.on.ca/francis-lab" rel="nofollow noopener">Francis Ouellette Lab</a>     </td><td class="col2 leftalign"> <a href="https://dpb.carnegiescience.edu/labs/rhee-lab" class="urlextern" target="_blank" title="https://dpb.carnegiescience.edu/labs/rhee-lab" rel="nofollow noopener">Sue Rhee Lab</a>      </td><td class="col3 leftalign"> <a href="http://biochem.ubc.ca/person/ivan-sadowski/" class="urlextern" target="_blank" title="http://biochem.ubc.ca/person/ivan-sadowski/" rel="nofollow noopener">Ivan Sadowski Lab</a>    </td>
	</tr>
	<tr class="row6">
		<td class="col0 leftalign"> <a href="http://www.sanderlab.org/#/" class="urlextern" target="_blank" title="http://www.sanderlab.org/#/" rel="nofollow noopener">Chris Sander Lab</a>    </td><td class="col1 leftalign"> <a href="http://fafner.stanford.edu/~sherlock/" class="urlextern" target="_blank" title="http://fafner.stanford.edu/~sherlock/" rel="nofollow noopener">Gavin Sherlock Lab</a>     </td><td class="col2 leftalign"> <a href="http://snyderlab.stanford.edu/" class="urlextern" target="_blank" title="http://snyderlab.stanford.edu/" rel="nofollow noopener">Mike Snyder Lab</a>      </td><td class="col3 leftalign"> <a href="https://labs.oicr.on.ca/stein-lab" class="urlextern" target="_blank" title="https://labs.oicr.on.ca/stein-lab" rel="nofollow noopener">Lincoln Stein Lab</a>    </td>
	</tr>
	<tr class="row7">
		<td class="col0 leftalign"> <a href="http://wormlab.caltech.edu/" class="urlextern" target="_blank" title="http://wormlab.caltech.edu/" rel="nofollow noopener">Paul Sternberg Lab</a>    </td><td class="col1 leftalign"> <a href="http://www.cnio.es/ing/grupos/plantillas/curriculum.asp?pag=1002" class="urlextern" target="_blank" title="http://www.cnio.es/ing/grupos/plantillas/curriculum.asp?pag=1002" rel="nofollow noopener">Alfonso Valencia Lab</a>     </td><td class="col2 leftalign"> <a href="http://ion.uoregon.edu/content/westerfield-laboratory" class="urlextern" target="_blank" title="http://ion.uoregon.edu/content/westerfield-laboratory" rel="nofollow noopener">Monte Westerfield Lab</a>      </td><td class="col3 leftalign"> <a href="http://wodaklab.org/ws/" class="urlextern" target="_blank" title="http://wodaklab.org/ws/" rel="nofollow noopener">Shoshana Wodak Lab</a>    </td>
	</tr>
	<tr class="row8">
		<td class="col0 leftalign"> <a href="http://research.lunenfeld.ca/woodgett/" class="urlextern" target="_blank" title="http://research.lunenfeld.ca/woodgett/" rel="nofollow noopener">Jim Woodgett Lab</a>    </td><td class="col1 leftalign"><a href="http://yaffelab.mit.edu/" class="urlextern" target="_blank" title="http://yaffelab.mit.edu/" rel="nofollow noopener">Mike Yaffe Lab</a>  </td><td class="col2 leftalign"> <a href="http://function.princeton.edu/" class="urlextern" target="_blank" title="http://function.princeton.edu/" rel="nofollow noopener">Olga Troyanskaya Lab</a>    </td><td class="col3 leftalign">     </td>
	</tr>
</table></div>
<!-- EDIT7 TABLE [9541-11904] -->
</div>
<!-- EDIT6 SECTION "BioGRID Affiliations" [9492-] --></div>
</body>
</html>
