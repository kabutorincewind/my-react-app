<?xml version="1.0"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
  <ShortName>BioGRID Help and Support Resources</ShortName>
  <Image width="16" height="16" type="image/x-icon">http://wiki.thebiogrid.org/lib/tpl/biogrid3/images/favicon.ico</Image>
  <Url type="text/html" template="http://wiki.thebiogrid.org/doku.php?do=search&amp;id={searchTerms}" />
  <Url type="application/x-suggestions+json" template="http://wiki.thebiogrid.org/lib/exe/ajax.php?call=suggestions&amp;q={searchTerms}" />
</OpenSearchDescription>
