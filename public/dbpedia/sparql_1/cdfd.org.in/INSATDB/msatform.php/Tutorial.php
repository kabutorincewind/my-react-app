<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Copyright 2005 Macromedia, Inc. All rights reserved. -->
<title>Insect Microsatellite Database</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="mm_lodging1.css">
<script language="JavaScript" type="text/JavaScript">
<!--
function dropdown(mySel){
	var myWin, myVal;
	myVal = mySel.options[mySel.selectedIndex].value;
	if(myVal){
   		if(mySel.form.target)myWin = parent[mySel.form.target];
   		else myWin = window;
   		if (! myWin) return true;
   		myWin.location = myVal;
   		}
	return false;
	}
function download() {
    	// perform something processing in here
	}
//-->
function expand(s)
{
  var td = s;
  var d = td.getElementsByTagName("div").item(0);

  td.className = "menuHover";
  d.className = "menuHover";
}

function collapse(s)
{
  var td = s;
  var d = td.getElementsByTagName("div").item(0);

  td.className = "menuNormal";
  d.className = "menuNormal";
}
</script>

<!--
.style2 {font-size: 24px}
.style12 {
	color: #FFFFFF;
	border-top-color: #FFFFFF;
	border-right-color: #FFFFFF;
	border-bottom-color: #FFFFFF;
	border-left-color: #FFFFFF;
	}
.style16 {font-size: xx-large}
.style17 {color: #000000; font-size: 17px;}
.style20 {color: #FFFFFF; border-top-color: #FFFFFF; border-right-color: #FFFFFF; border-bottom-color: #FFFFFF; border-left-color: #FFFFFF; font-weight: bold; }
.style21 {
	color: #FFFFFF;
	font-family: Georgia, "Times New Roman", Times, serif;
	}
-->

<style type="text/css">
.style3 {font-family: Georgia, "Times New Roman", Times, serif}
.style4 {font-size: xx-large}
.style5 {
        font-size: 16px;
        color: #00000;
        font-family: Georgia, "Times New Roman", Times, serif;
        }
.style7 {color: #FFFFFF}
.style8 {font-size: 12px}
.style9 {color: #000099}
.style10 {
        font-size: 16px;
        font-weight: bold;
        }
.style11 {font-size: 14px}
.style12 {font-size: 14px; font-weight: bold; }
.style13 {color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold; }
.style16 {font-size: 14px; font-weight: bold; font-style: normal; }
.style17 {font-style: italic}
.style18 {color: #FF0000}
.style23 {font-size: 12px; font-style: italic; }
.style27 {
	font-size: 16px;
	font-weight: bold;
	}
.style28 {font-size: 16px}
.style29 {color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16; font-weight: bold; }
.style30 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	}
.style32 {color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
.style33 {color: #FFFFFF}
.style34 {
	font-size: 24px;
	font-style: italic;
	}
.style35 {font-weight: bold}
.style36 {font-family: Georgia, "Times New Roman", Times, serif}
.style37 {
        color: #FFFFFF;
        font-family: Georgia, "Times New Roman", Times, serif;
        }
</style>
</head>

<script language="JavaScript" type="text/javascript">
var count=0;
var arr = new Array();
</script>



<body bgcolor="#006666" >
<table id="main" width="790" height="50" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
	<BR><td><img src ="images/title.jpg" width=790 height=98 ></td>
  </tr>
</table>

<div align="center">
<p align=center><table width="790" height="35" border="0" align="center" cellpadding="0" cellspacing="0" class="navbar"></p>
    <tr>	

	  <td class="menuNormal" width="160"></td>	
      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center"><a href="home.php" target="_parent" class="menuNormal style7">Home</a></p>
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center" class="style7">About</p>
        <div class="menuNormal" width="155">
          <table class="menu" width="170" border="0" cellspacing="0" cellpadding="0">
            <tr><td class="menuNormal" height="18">
              <a HREF="insect.php" class="menuitem">Insects</a>
            </td></tr>
            <tr><td class="menuNormal" height="18">
              <a HREF="about.php" class="menuitem">Microsatellites</a>
            </td></tr>
            <tr><td class="menuNormal" height="18">
              <a HREF="about.php#extraction" class="menuitem">Extraction method</a>
            </td></tr>
	    </td></tr>
                 <tr><td class="menuNormal" height="18">
              <a HREF="acknowledgement.php" class="menuitem">Acknowledgements</a>
            </td></tr>
                        <tr><td class="menuNormal" height="18">
              <a HREF="team.php" class="menuitem">Team</a>
            </td></tr>
          </table>
        </div>
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
	<p align="center"><a href="analysis.php" target="_parent" class="menuNormal style7">Analysis</a></p>
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
	onmouseout="collapse(this);">
	<p align="center" class="style7">Database</p>
	<div class="menuNormal" width="200">
	 <table class="menu" width="200">
	   <tr><td class="menuNormal" height="18">
		<a href="download.php" class="menuitem">Batch download</a>
		</td></tr>
	    <tr><td class="menuNormal" height="18">
		<a href="msatform.php" class="menuitem">Query based access</a>
		</td></tr>
	    </table>
	  </div>
	</td> 

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center" class="style7">Tutorial</p>
        <div class="menuNormal" width="200">
          <table class="menu" width="200">
            <tr><td class="menuNormal" height="18">
              <a HREF="Tutorial.php" class="menuitem">Extract Microsatellites</a>
            </td></tr>
		<tr><td class="menuNormal" height="18">
              <a HREF="Glossary.php" class="menuitem">Glossary</a>
            </td></tr>

          </table>
        </div> 
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center" class="style7"><a href="links.php" target="_parent" class="style7">Links</a></p>      </td>


      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center"><a href="http://www.cdfd.org.in/lmg/home.php" target="_blank" class="style7">Lab page</a></p>
      </td>
      <td class="menuNormal" width="90" onmouseover="expand(this);"
      onmouseout="collapse(this);">
        <p align="center"><a href="contactus.php" target="
_blank" class="style7">Contact Us</a></p>
      </td>


    </tr>
  </table>
</div>

<table width="785" border="0" align="center" bordercolor="#ECE9D8" bgcolor="#ECE9D8">
	<tr>
   	  	<td width="785" height="600" bordercolor="#ECE9D8" bgcolor="#ECE9D8">
   	    	<p align="left" class="style29"> <span class="style27">Insect :</span></p>
	    	<form action="msatresults.php" method="POST" name="form1"  onsubmit="return validate_form(form1)">
      		<p align="left">
         	<em>
         		<label>
          			<input name="insect" type="radio" value="bombyx">
          				Bombyx mori					</label>
         		<label>
          			<input name="insect" type="radio" value="anopheles">
          				Anopheles gambiae					</label>
         		<label>
          			<input name="insect" type="radio" value="drosophila">
          				Drosophila melanogaster					</label>
         		<label>
        			<input name="insect" type="radio" value="apis">
        				Apis mellifera					</label>
         		<label>
          			<input name="insect" type="radio" value="tribolium">
         			Tribolium castaneum					</label>
		</p>		
		</em>
      		<hr>
      			<p align="left" class="style29"> <span class="style27">Location :</span> </p>
			<p align="left">
        			<label><input type="checkbox" name="location1" value="intron"> Intron </label>
         			<label><input type="checkbox" name="location2" value="exon"> Exon </label>
       				<label><input type="checkbox" name="location3" value="ieboundary"> I-E boundary </label>
        			<label><input type="checkbox" name="location4" value="upstream"> Upstream(1000 bp) </label>
       				<label><input type="checkbox" name="location5" value="intergenic"> Intergenic </label>
       				<label><input type="checkbox" name="location6" value="repeat_elements"> Repeat elements </label>
       				<label><input type="checkbox" name="location7" value="all" > All </label>
			</p>	
		<hr/>
<table width="785" height="30" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
        	<td width="369" height="80"><span class="style9"></span>
        		<span class="style26"><div align="left" class="style27">Microsatellite characteristics :&nbsp;</div></span>
        		<p align = "left"><span> 1.&nbsp;
	       			<label><input type="radio" name="repeat" value="repeat_type" CHECKED></label>
   	        		<label><a href="glossary.html#repeattype">Repeat type</a></label>
	        		<span class="style9">
   	        			<label>
       	                			<select name="select_repeat_type">
       	                  				<option value = "all">All</option>
       	                  				<option value = "mono">mono</option> 
       	                  				<option value = "di">di</option> 
       	                  				<option value = "tri">tri</option>
       	                  				<option value = "tetra">tetra</option>	
       	                  				<option value = "penta">penta</option>
       	                  				<option value = "hexa">hexa</option> 
	                			</select>
                       			</label>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>OR</b>
				</span>
			</p>
      		</td>
	      	<td width="100" height="15">
			<div align="left">
				<p>&nbsp</p>
	    			<p><span class="style9"><input type="radio" name="repeat" value="repeat_unit" ></span>
		        		<a href="glossary.html#motif"> Repeat motif</a>
				</p>
		      	</div>
		</td>
	      	<td width="273" height="190" class="style9">	
       	        <label>	
			<br><br><br><br>
                	<br />
              	</label>
       	        <div align="left">
            		<input name="repeat_unit_enter" type="text" value="" size="6" maxlength="6" onfocus=checkButton(form1)>
              		<input type="button" name="repeat_unit_submit" value="Enter motif and Click" onClick=appText(form1)>
   	        </div>
	      	</label>
	  	<script language="JavaScript" type="text/javascript">
	 		function checkButton(form1){
				if(form1.repeat[0].checked==true){	
					form1.repeat[1].checked=true;
					}			
			  	}	
		 	function checkbpButton(){
				if(form1.size[1].checked==true){	
					form1.size[0].checked=true;
					}			
				 } 
			 function checkrnButton(){
				if(form1.size[0].checked==true){	
					form1.size[1].checked=true;
					}			
			  	}
			 function appText(form1){
				if (form1.repeat_unit_enter.value =="xyz")
   					{
        				alert("please enter the repeat motif"); 
        				form1.repeat_unit_enter.focus();
      					return false;
    					}		
				if (form1.repeat_unit_enter.value !=""){

					var string1=form1.repeat_unit_enter.value
					if(string1.match(/[bdefhijklmnopqrsuvwxyzBDEFHIJKLMNOPQRSUVWXYZ0123456789]/)){
   						alert("please enter proper motif"); 
        					form1.repeat_unit_enter.focus();
        					return false;
						}	
    					else{
						if(count <= 4){
							arr[count]=document.form1.repeat_unit_enter.value;
                                                        document.form1.repeat_unit_enter.value="";
                                                        document.form1.repeat_unit_show.value=arr;
                                                        count++;
							}
						else	{
							alert("you can't enter more than five repeat motifs");
							document.form1.repeat_unit_enter.value="";
							}
						}
					}	 
			       }	
		</script> 	
         		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         	 	<label></label>
                        <div align="left">
                                <input name="repeat_unit_show" type="text" value="" size="35">
				
			</div>
		</td>
         	</tr>
      		</table>
         	<p align="left">2.<a href="glossary.html#gc"> GC% </a>&nbsp;&nbsp; 
         		<label>
    				<input name="gc1" type="text" value="" size="3" maxlength="3">
			</label>

  			&nbsp;&nbsp;
        		&ge;&nbsp;
                  	<label>
                  		<input name="gc2" type="text" value="" size="3" maxlength="3">
                  	</label>

  			&nbsp;&nbsp;&nbsp;&nbsp;
        		&le;&nbsp;
                  	<label>
                  		<input name="gc3" type="text" value="" size="3" maxlength="3">
                  	</label>
			<p> &nbsp; </p>
			<p align="left">

       		  	3.&nbsp;&nbsp;&nbsp;
		  	Size        				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <label>
		        <input name="size" type="radio" value="size_bp" CHECKED><a href="glossary.html#bp">
					bp </a></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <label>
				  <input name="size_bp1" type="text" value="" size="4" maxlength="4" onfocus=checkbpButton(form1)>
			  </label>
			  &ge;
			  <label>
				  <input name="size_bp2" type="text" value="" size="4" maxlength="4" onfocus=checkbpButton(form1)>
			  </label>
			  &le;
			  <label>
				  <input name="size_bp3" type="text" value="" size="4" maxlength="4" onfocus=checkbpButton(form1)>
			  </label>
		      <br>
					<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="style22">
						<input name="size" type="radio" value="size_repeat">
       					</span><a href="glossary.html#copy">Copy no</a> =
       					<label> 
       						<input name="size_repeat1" type="text" value="" size="4" maxlength="4" onfocus=checkrnButton(form1)> 
       					</label>
						&ge;
	   					<label>
       						<input name="size_repeat2" type="text" value="" size="4" maxlength="4" onfocus=checkrnButton(form1)> 
       					</label>
						&le;
	   					<label>
       						<input name="size_repeat3" type="text" value="" size="4" maxlength="4" onfocus=checkrnButton(form1)>
	   					</label>
			  </p>
			  <p >
	     				  <label>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          				  <a href="glossary.html#repeatkind">Repeat kind</a>
	       				  <select name="repeat_kind">
							<option value = "All">All</option>
		    				<option value = "Perfect">Perfect</option> 
            				<option value = "Imperfect">Imperfect</option> 
          				</select>
		    </label>
			<hr/>
         			<div align="center">
		   			<label>
            				<input type="submit" name="submit_data" value="GO..." >
           				</label> 
						&nbsp;&nbsp;&nbsp;
						<label>
		   					<input type="reset" name="Submit3" value="Reset">
					</label>
        			</div>
			<p align="center"> Loading of results will take time. Please be patient.</p>
   		  </table>
		<br><br>
<p>


<script language="JavaScript" type="text/javascript">	
function chkBox(form1){		
document.form1.repeat_unit_show.value="";	
}
	function validate_form(form1){
			var insect_choice = false;
			for (counter = 0;  counter < form1.insect.length; counter++)
			{
				if (form1.insect[counter].checked)
					insect_choice = true; 
			}
			if (!insect_choice)
			{	 
				alert("Please select an insect.")
				return (false);
			}
			if((document.form1.location1.checked==false && document.form1.location2.checked==false && document.form1.location3.checked == false && document.form1.location4.checked == false && document.form1.location5.checked == false && document.form1.location6.checked == false) && document.form1.location7.checked == false){
				alert("Select a location"); form1.location7.focus(); return false;	
				}

			if((document.form1.location1.checked==true || document.form1.location2.checked==true || document.form1.location3.checked==true || document.form1.location4.checked==true || document.form1.location5.checked==true || document.form1.location6.checked==true) && document.form1.location7.checked==true){
				alert("By default all locations are selected, you can't select \"ALL\" with other locations");				form1.location7.focus();
				return false;
			}
			if(form1.gc1.value != "" && form1.gc2.value != "" && form1.gc3.value == ""){
			        alert("you can't fill both \"=\" and \">\" fields"); 
        			form1.gc1.focus();
        			return false;
			}

			if(form1.gc1.value != "" && form1.gc2.value == "" && form1.gc3.value != ""){
			        alert("you can't fill both \"=\" and \"<\" fields"); 
        			form1.gc1.focus();
        			return false;
			}

			if(form1.gc1.value != "" && form1.gc2.value != "" && form1.gc3.value != ""){
			        alert("please enter the GC % properly,you can't fill all the fields"); 
        			form1.gc1.focus();
        			return false;
			}

			if(form1.gc1.value != "" && form1.gc2.value == "" && form1.gc3.value == ""){
			var gca=form1.gc1.value;
				if(gca.match(/[^0-9]/)){
        			alert("please enter numerical value"); 
        			form1.gc1.focus();
        			return false;
				}else if(gca.match(/[0-9]/) && (gca < 0 || gca >100)){
					alert("please enter a GC % ranging between 0 and 100");
					form1.gc1.focus();
        			return false;
				}
			}

			if(form1.gc1.value == "" && form1.gc2.value != "" && form1.gc3.value == ""){
			var gcb=form1.gc2.value;
				if(gcb.match(/[^0-9]/)){
        			alert("please enter numerical value"); 
        			form1.gc2.focus();
        			return false;
				}else if(gcb.match(/[0-9]/) && (gcb < 0 || gcb >100)){
					alert("please enter a GC % ranging between 0 and 100");
					form1.gc2.focus();
        			return false;
				}
			}

			if(form1.gc1.value == "" && form1.gc2.value == "" && form1.gc3.value != ""){
			var gcc=form1.gc3.value;
				if(gcc.match(/[^0-9]/)){
        			alert("please enter numerical value"); 
        			form1.gc3.focus();
        			return false;
				}else if(gcc.match(/[0-9]/) && (gcc < 0 || gcc >100)){
					alert("please enter a GC % ranging between 0 and 100");
					form1.gc3.focus();
        			return false;
				}
			}

			if(form1.gc1.value == "" && form1.gc2.value != "" && form1.gc3.value != ""){
			var gcb=form1.gc2.value;
			var gcc=form1.gc3.value;
				if(gcb.match(/[^0-9]/) || gcc.match(/[^0-9]/)){
        			alert("please enter numerical value");
        			return false;
				}else if((gcc.match(/[0-9]/) && (gcc > 0 || gcc <100)) || (gcb.match(/[0-9]/) && (gcb > 0 || gcb <100))){
					if(gcb > gcc){
						alert("\">\" field should be less than \"<\" field");
						form1.gc2.focus();
        				return false;
					}
				}else if((gcc.match(/[0-9]/) && (gcc < 0 || gcc >100)) || (gcb.match(/[0-9]/) && (gcb < 0 || gcb >100))){
					alert("please enter a GC % ranging between 0 and 100");
        			return false;
				}
			}

			if(form1.size_bp1.value != "" && form1.size_bp2.value != "" && form1.size_bp3.value == ""){
			        alert("you can't fill both \"=\" and \">\" fields"); 
        			form1.size_bp1.focus();
        			return false;
			}

			if(form1.size_bp1.value != "" && form1.size_bp2.value == "" && form1.size_bp3.value != ""){
			        alert("you can't fill both \"=\" and \"<\" fields"); 
        			form1.size_bp1.focus();
        			return false;
			}

			if(form1.size_bp1.value != "" && form1.size_bp2.value != "" && form1.size_bp3.value != ""){
			        alert("please enter the base pair number properly,you can't fill all the fields"); 
        			form1.size_bp1.focus();
        			return false;
			}

			if(form1.size_bp1.value != "" && form1.size_bp2.value == "" && form1.size_bp3.value == ""){
			var size_bpa=form1.size_bp1.value;
				if(size_bpa.match(/[^0-9]/) || (size_bpa < 0)){
        			alert("please enter numerical value properly"); 
        			form1.size_bp1.focus();
        			return false;
				}
			}

			if(form1.size_bp1.value == "" && form1.size_bp2.value != "" && form1.size_bp3.value == ""){
			var size_bpb=form1.size_bp2.value;
				if(size_bpb.match(/[^0-9]/) || (size_bpb < 0)){
        			alert("please enter numerical value properly"); 
        			form1.size_bp2.focus();
        			return false;
				}
			}

			if(form1.size_bp1.value == "" && form1.size_bp2.value == "" && form1.size_bp3.value != ""){
			var size_bpc=form1.size_bp3.value;
				if(size_bpc.match(/[^0-9]/) || (size_bpc < 0)){
        			alert("please enter numerical value properly"); 
        			form1.size_bp3.focus();
        			return false;
				}
			}

			if(form1.size_bp1.value == "" && form1.size_bp2.value != "" && form1.size_bp3.value != ""){
			var size_bpb=form1.size_bp2.value;
			var size_bpc=form1.size_bp3.value;
				if(size_bpb.match(/[^0-9]/) || size_bpc.match(/[^0-9]/) || (size_bpb < 0) || (size_bpc < 0)){
        			alert("please enter numerical value properly");
        			return false;
				}else if((size_bpc.match(/[0-9]/) && (size_bpc > 0)) || (size_bpb.match(/[0-9]/) && (size_bpb > 0))){
					if(size_bpb > size_bpc){
						alert("\">\" field should be less than \"<\" field");
						form1.size_bp2.focus();
        				return false;
					}
				}
			}


			if(form1.size_repeat1.value != "" && form1.size_repeat2.value != "" && form1.size_repeat3.value == ""){
			        alert("you can't fill both \"=\" and \">\" fields"); 
        			form1.size_repeat1.focus();
        			return false;
			}

			if(form1.size_repeat1.value != "" && form1.size_repeat2.value == "" && form1.size_repeat3.value != ""){
			        alert("you can't fill both \"=\" and \"<\" fields"); 
        			form1.size_repeat1.focus();
        			return false;
			}

			if(form1.size_repeat1.value != "" && form1.size_repeat2.value != "" && form1.size_repeat3.value != ""){
			        alert("please enter the base pair number properly,you can't fill all the fields"); 
        			form1.size_repeat1.focus();
        			return false;
			}

			if(form1.size_repeat1.value != "" && form1.size_repeat2.value == "" && form1.size_repeat3.value == ""){
			var size_repeata=form1.size_repeat1.value;
				if(size_repeata.match(/[^0-9]/) || (size_repeata < 0)){
        			alert("please enter numerical value properly"); 
        			form1.size_repeat1.focus();
        			return false;
				}
			}

			if(form1.size_repeat1.value == "" && form1.size_repeat2.value != "" && form1.size_repeat3.value == ""){
			var size_repeatb=form1.size_repeat2.value;
				if(size_repeatb.match(/[^0-9]/) || (size_repeatb < 0)){
        			alert("please enter numerical value properly"); 
        			form1.size_repeat2.focus();
        			return false;
				}
			}

			if(form1.size_repeat1.value == "" && form1.size_repeat2.value == "" && form1.size_repeat3.value != ""){
			var size_repeatc=form1.size_repeat3.value;
				if(size_repeatc.match(/[^0-9]/) || (size_repeatc < 0)){
        			alert("please enter numerical value properly"); 
        			form1.size_repeat3.focus();
        			return false;
				}
			}

			if(form1.size_repeat1.value == "" && form1.size_repeat2.value != "" && form1.size_repeat3.value != ""){
			var size_repeatb=form1.size_repeat2.value;
			var size_repeatc=form1.size_repeat3.value;
				if(size_repeatb.match(/[^0-9]/) || size_repeatc.match(/[^0-9]/) || (size_repeatb < 0) || (size_repeatc < 0)){
        			alert("please enter numerical value properly");
        			return false;
				}else if((size_repeatc.match(/[0-9]/) && (size_repeatc > 0)) || (size_repeatb.match(/[0-9]/) && (size_repeatb > 0))){
					if(size_repeatb > size_repeatc){
						alert("\">\" field should be less than \"<\" field");
						form1.size_repeat2.focus();
        				return false;
					}
				}
			}
	}
	</script>
  
  
</p>

</body>
</html>

