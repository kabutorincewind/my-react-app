<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Copyright 2005 Macromedia, Inc. All rights reserved. -->
<title>Insect Microsatellite Database</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="mm_lodging1.css">
<script language="JavaScript" type="text/JavaScript">
<!--
function dropdown(mySel){
	var myWin, myVal;
	myVal = mySel.options[mySel.selectedIndex].value;
	if(myVal){
   		if(mySel.form.target)myWin = parent[mySel.form.target];
   		else myWin = window;
   		if (! myWin) return true;
   		myWin.location = myVal;
   		}
	return false;
	}
function download() {
    	// perform something processing in here
	}
//-->
function expand(s)
{
  var td = s;
  var d = td.getElementsByTagName("div").item(0);

  td.className = "menuHover";
  d.className = "menuHover";
}

function collapse(s)
{
  var td = s;
  var d = td.getElementsByTagName("div").item(0);

  td.className = "menuNormal";
  d.className = "menuNormal";
}
</script>

<!--
.style2 {font-size: 24px}
.style12 {
	color: #FFFFFF;
	border-top-color: #FFFFFF;
	border-right-color: #FFFFFF;
	border-bottom-color: #FFFFFF;
	border-left-color: #FFFFFF;
	}
.style16 {font-size: xx-large}
.style17 {color: #000000; font-size: 17px;}
.style20 {color: #FFFFFF; border-top-color: #FFFFFF; border-right-color: #FFFFFF; border-bottom-color: #FFFFFF; border-left-color: #FFFFFF; font-weight: bold; }
.style21 {
	color: #FFFFFF;
	font-family: Georgia, "Times New Roman", Times, serif;
	}
-->
<style type="text/css">
.style3 {font-family: Georgia, "Times New Roman", Times, serif}
.style4 {font-size: xx-large}
.style7 {color: #FFFFFF}
.style9 {
	font-size: 18px;
	font-weight: bold;
	}
.style10 {font-size: 14px}
</style>
</head>
<body bgcolor="#006666">
<table id="main" width="790" height="50" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr><BR><td><img src ="images/title.jpg" width=790 height=98 ></td>
  </tr>
</table>
<div align="center">
<p align=center><table width="790" height="35" border="0" align="center" cellpadding="0" cellspacing="0" class="navbar"></p>
    <tr>	

	  <td class="menuNormal" width="160"></td>	
      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center"><a href="home.php" target="_parent" class="menuNormal style7">Home</a></p>
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center" class="style7">About</p>
        <div class="menuNormal" width="155">
          <table class="menu" width="170" border="0" cellspacing="0" cellpadding="0">
            <tr><td class="menuNormal" height="18">
              <a HREF="insect.php" class="menuitem">Insects</a>
            </td></tr>
            <tr><td class="menuNormal" height="18">
              <a HREF="about.php" class="menuitem">Microsatellites</a>
            </td></tr>
            <tr><td class="menuNormal" height="18">
              <a HREF="about.php#extraction" class="menuitem">Extraction method</a>
            </td></tr>
	    </td></tr>
                 <tr><td class="menuNormal" height="18">
              <a HREF="acknowledgement.php" class="menuitem">Acknowledgements</a>
            </td></tr>
                        <tr><td class="menuNormal" height="18">
              <a HREF="team.php" class="menuitem">Team</a>
            </td></tr>
          </table>
        </div>
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
	<p align="center"><a href="analysis.php" target="_parent" class="menuNormal style7">Analysis</a></p>
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
	onmouseout="collapse(this);">
	<p align="center" class="style7">Database</p>
	<div class="menuNormal" width="200">
	 <table class="menu" width="200">
	   <tr><td class="menuNormal" height="18">
		<a href="download.php" class="menuitem">Batch download</a>
		</td></tr>
	    <tr><td class="menuNormal" height="18">
		<a href="msatform.php" class="menuitem">Query based access</a>
		</td></tr>
	    </table>
	  </div>
	</td> 

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center" class="style7">Tutorial</p>
        <div class="menuNormal" width="200">
          <table class="menu" width="200">
            <tr><td class="menuNormal" height="18">
              <a HREF="Tutorial.php" class="menuitem">Extract Microsatellites</a>
            </td></tr>
		<tr><td class="menuNormal" height="18">
              <a HREF="Glossary.php" class="menuitem">Glossary</a>
            </td></tr>

          </table>
        </div> 
      </td>

      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center" class="style7"><a href="links.php" target="_parent" class="style7">Links</a></p>      </td>


      <td class="menuNormal" width="90" onmouseover="expand(this);" 
      onmouseout="collapse(this);">
        <p align="center"><a href="http://www.cdfd.org.in/lmg/home.php" target="_blank" class="style7">Lab page</a></p>
      </td>
      <td class="menuNormal" width="90" onmouseover="expand(this);"
      onmouseout="collapse(this);">
        <p align="center"><a href="contactus.php" target="
_blank" class="style7">Contact Us</a></p>
      </td>


    </tr>
  </table>
</div>

<table width="790" border="0" align="center" cellpadding="0" cellspacing="0">  
<tr bgcolor="#FFFFFF"> 
<td width="650" class="bodyText">
<p>
<BR><BR>
	
<ul><h2> Please select data file of one insect and click Go to download the data</h2>
	<ul><form action="../cgi-bin/redirect.pl" METHOD=POST onSubmit="return dropdown(this.gourl)">
               	<SELECT NAME="gourl">
               	<OPTION VALUE="">Choose a Destination...
		<OPTION VALUE="./DATA/Ano_repeats.csv.tar.gz" target = "_blank">Anopheles gambiae Microsatellites
		<OPTION VALUE="./DATA/Ano_compound.csv.tar.gz" target = "_blank">Anopheles gambiae Comound repeats
		<OPTION VALUE="./DATA/Ano_families.csv.tar.gz" target = "_blank">Anopheles gambiae Microsatellite families
                <OPTION VALUE="./DATA/Ano_sequences.csv.tar.gz" target = "_blank">Anopheles gambiae Microsatellite sequences
		<option value="">..............

		<OPTION VALUE="./DATA/Apis_repeats.csv.tar.gz" target = "_blank">Apis mellifera Microsatellites
		<OPTION VALUE="./DATA/Apis_compound.csv.tar.gz" target = "_blank">Apis mellifera Compound repeats
		<OPTION VALUE="./DATA/Apis_families.csv.tar.gz" target = "_blank">Apis melliefera Microsatellite families
		<OPTION VALUE="./DATA/Apis_sequences.csv.tar.gz" target = "_blank">Apis mellifera Microsatellite sequences

		<option value="">..............

		<OPTION VALUE="./DATA/Bombyx_repeats.csv.tar.gz" target = "_blank">Bombyx mori Microsatellites
          	<OPTION VALUE="./DATA/Bombyx_compound.csv.tar.gz" target = "_blank">Bombyx mori Compound repeats
          	<OPTION VALUE="./DATA/Bombyx_families.csv.tar.gz" target = "_blank">Bombyx mori Microsatellite families
                <OPTION VALUE="./DATA/Bombyx_sequences.csv.tar.gz" target = "_blank">Bombyx mori Microsatellite sequences
		<option value="">..............

                <OPTION VALUE="./DATA/Dmel_repeats.csv.tar.gz" target = "_blank">Drosophila melanogaster  Microsatellites
                <OPTION VALUE="./DATA/Dmel_compound.csv.tar.gz" target = "_blank">Drosophila melanogaster  Compound repeats
                <OPTION VALUE="./DATA/Dmel_families.csv.tar.gz" target = "_blank">Drosophila melanogaster  Microsatellite families
                <OPTION VALUE="./DATA/Dmel_sequences.csv.tar.gz" target = "_blank">Drosophila melanogaster  Microsatellite sequences

		<option value="">..............

		<OPTION VALUE="./DATA/Tcas_repeats.csv.tar.gz" target = "_blank">Tribolium castaneum Microsatellites
                <OPTION VALUE="./DATA/Tcas_compound.csv.tar.gz" target = "_blank">Tribolium castaneum Compound repeats
                <OPTION VALUE="./DATA/Tcas_families.csv.tar.gz" target = "_blank">Tribolium castaneum Microsatellite families
                <OPTION VALUE="./DATA/Tcas_sequences.csv.tar.gz" target = "_blank">Tribolium castaneum Microsatellite sequences
		<option value="">..............
                </SELECT>
                <INPUT TYPE=SUBMIT VALUE="Go">
        	</FORM>	
		</ul> 
	</ul>
</p>
</body>
</html>

