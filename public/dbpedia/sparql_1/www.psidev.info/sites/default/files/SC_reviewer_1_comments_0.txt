
SC reviewer 1 comments:
-----------------------

4. Relationship to other specifications
=> no relationship to MIAPE? Explicitly? implicitly? not necessary?
=> no relationships with TrAML?

4.1
=> Please add NEWT as possible CV as it is used in examples in 6.2.23 to examplify the unit {UNIT_ID}(-{SUB_ID})-species[1-n] ;
same for BTO, CL, DOID etc used under 6.2.nn

6.2.13 {UNIT_ID}-uri
=> if multiplicity is 0 ... *, please specify 6.2.13 {UNIT_ID}-uri[1-n]
otherwise | as seperator for multiple uris is not appropriate and can be missleading


5.1. Handling updates to the controlled vocabulary
the form http://www.psidev.info/index.php?q=node/440  points to a url that does not exist. 
=> Please provide a more stable one
  
in 5.4
=>In text " ...for an experiment �EXP_1�, the replicates must have the UNIT_IDs �EXP_1-rep[1-n]� "  must have MUST be capitalized here.

=> "Biological replicates are not explicitly supported in the same way in mzTab."  therefore anything is possible? what are the constraints? no constraints = difficult to limit imagination of people...

in 5.5
example :
COM The following example shows how two different quantitative experiments
COM can be reported in one mzTab file. Not all labels are shown
�
MTD EXP_1-quantification_method [MS,MS:1001837,iTraq,]
=> in MS CV, MS:1001837 is defined as "iTRAQ quantitation analysis"  ; please correct the example accordingly

later in same example:
MTD EXP_2-quantification_method [MS,MS:100999,SILAC,]   ; 
=> replace text in brackets by [MS,MS:1001835,SILAC quantitation analysis,]

in second example of 5.5:
COM Example showing how emPAI values are reported in an additional column using
COM MS CV parameter �emPAI value� (MS:1001905)
�
PRH accession � opt_cv_MS:1001905
PRT P12345 � 0.658

=> the column title is  opt_cv_MS:1001905, therefore not readable by a non bioinformatician. Where can one have a full text name to help targeted users reading the information? As stated in 5.10.3, it is allowed to use anything, as soon as it is different from another column. So why specifying this possibility that is not a human readable one?



5.8  
=> in text "  ... �-� must be provided as the value for each of "  : please capitalize MUST

=> in the text " The reliability MUST be an integer" : change to  "When a reliability value is provided, this value MUST be an integer"


in 5.9
in text : "All (identified) variable modifications as well as fixed modifications MUST be reported for every identification."
=> this sounds nice but if mzTab is meant to cover a simplified but straight to the point representation of peptide or protein or small molecule identifications, this point is overkilling. Take the simple example of phosphopeptides: it is not important to show the position of an oxidized methionine, or of a iTraQ label when an author wants to report a list of phosphorylation positions. I'm sure that this will not be followed. Therefore change MUST by something less stringent

later in text: "Furthermore, mass deltas MUST NOT be reported if the given delta can be expressed through a known and unambiguous
chemical formula." 
=> as this is commonly used by tools such as Sequest and ProteinProspector, I'm not convinced at all that you can claim this to be followed, particularly because these mods are defined by a mass delta value without requiring a "name" for it.


about 5.10.1
the approach means that one peptide (or one protein) is represented twice (two lines) if is is found both by Andromeda and Mascot. This is not a simple fimal list. Authors prefer to have one line with both scores in one line (this comment is based on mztab_merged_example.txt
=> An appropriate example is probably missing, as one can encode [MS,MS:1001171,Mascot score,50]|[MS,MS:1001155,Sequest:xcorr,2] according to 6.3.9


about 5.10.4:
about the text "This field MUST NOT be used to reference an external MS data file. MS data files should be referenced using the method described in Section 5.2".
=> what about referencing mzML? or a specific spectrum in mzML? this is not specified in 5.2


in 6.
 "Every line in an mzTab file must start"
=> capitalize MUST

in text (under Params) "Any field that is not available should be left empty"
=> should'nt that be MUST be left empty ??
=> how are space and comma characters constrained?

About the numbers: is a scientific number forbidden (such as 1.4E10) ?

6.2
in section Unit ID: the term unit is given in small caps: under 5.4 and 6.1 it is always written UNIT. 
=> please be coherent

about
{UNIT_ID}-{SUB_ID}-custom
and
{UNIT_ID}-custom
=> what is the difference as "-" characters are allowed in the naming of these terms

in 6.3
The protein section must always come
=> capitalize MUST


There MUST NOT be any empty cells.
=> how do we need to fill these? NA?

The columns in the protein section MUST be in the order they are presented in this document
=> it is not said whether the columns described under 6.3. are all mandatory or not. It is overkilling to add all of these columns if not necessary from the user point of view to explain and transfer its result (for instance go-terms, number of peptides, taxid, etc.)




6.3.4 taxid and 6.3.5 species
=> what if a custom or synthesized protein?  NA?

6.3.7 database_version
=> today, UniprotKB version names are using underscore and not hyphen in their names. Please change 2011-11 by 2011_11

6.3.11, 12, 13 
=> what about number of PSM? is this considered identical to 6.3.11?
  
  
6.3.15 modifications
=> sounds like reinventing the wheel and is not compatible with current outputs frm some tools (forcing a double between 0 and 1 when a given software provides an open scale value is not a good idea. Why not looking at PEFF and at GPMDB for this?

6.3.16 uri
=> not sur what this is: uri representing teh entry in the searched database or something else?

6.3.17 go_terms
=> why using a string list and not a string with | separators

6.3.19 protein_abundance_sub[1-n] (Optional)
=> what is expected here? concentrations, number of molecules? there are no units...

6.4.
The peptide section must always
=>capitalize MUST

=> are they all required? this might potentially generate huge and non necessary redundancy (for instance if one database and one database version was used, which is the case for all non merge results)


6.4.1 sequence
=> how to encode sequence ambiguity (I/L), others, and results from sequence tags experiments?

6.4.5 database
=> how to describe UniProtKB/Swiss-Prot human complete proteome subset + crap database ?

6.4.8 search_engine_score
=> I want to report score ane evalues for one peptide identified by Mascot and xcorr and peptide prophet probabilities . How do ai show this?

6.4.11 retention_time
=> why constraining to seconds? It might be simpler and actually more often used as minutes of relative retention time or even retention index depending on the application

6.4.12 charge
=> why a Double?

6.4.14 uri
=> I thought it can be thepointer to a mzIdentML file position... SO how to do this?


6.5
The small molecule section must always
=> capitalize MUST

6.5.3 chemical_formula
Elements should be capitalized properly to avoid confusion
=> I would change should to MUST ...

The chemical formula reported should refer to the neutral form.
Charge state is reported by the charge field. This permits the comparison of
positive and negative mode results  
=> No only chemical formula of neutral and charge state is not sufficient. what about adducts? and how to exemplify for glycine: C2H5NO2: the protonated form is C2H3NO2 with charge 1 and the deprotd ionated (negative form) is C2H4NO2 with charge -1. But sodiates C2H4NO2Na charge 1. And if I take the example of a doubly charged species, it gets even worse... How do you want to compare the chemical formulae?
Please be more precise with what is expected or remove the comment
   
6.5.6 description
if it is allowed to provide a list of identifiers, then it should also be needed to have multiple descriptions (same is true for inchi...)
=> is it allowed? if not, please add a constrain under 6.5.1; if yes, please make sure there are no ambiguity left...

6.5.9 retention time 
=> same as 6.4.11

6.5.16 spectra_ref
The reference must be in the
format
ms_file[1-n]:{SPEC_REF} where SPEC_REF must follow the format defined in mzIdentML
=> but according to the specs, it is possible to refer to something else than a mzIdentML file, which can have another way to index or point to a spectrum. Please allow more options

   
7. Conclusions
"These artefacts are currently undergoing the PSI document process standardization process"
=> remove "standardization process"

   
   
Other question: why not using PSI-MS CV for terms? no relationships to mzIdentML terms? just independant terms?  
