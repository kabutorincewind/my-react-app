comments from invited reviewers:
-------------------------------
invited reviewer 1:
"I think, after almost everything now is clarified, the mzTab-Format is good to be launched.
Especially the introduction and differentiation of peptides and PSMs makes mzTab much more appealing for usage (at least by me). Despite some minor comments below, I have no further suggestions.
It may be noted, that I have almost no knowledge about the small molecule part, though.
" 
+ specific comments => see R3_mzTab_comments_invited_reviewer_1_anon.docx

invited reviewer 2:
no answer


public comments from original commenters:
-----------------------------------------
SC reviewer 1:
no further comments yet

public commenter 1:
no further comments

public commenter 2:
"find a very small number of comments attached."
=> R3_mzTab_comments_public_commenter_2_anon.docx


comments from additional commenters:
-----------------------------------
public commenter 3:
no further comments (now author) => considered

public commenter 4:
no further comments
