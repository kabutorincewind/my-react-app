1. wording in the pdf could be confusing that there are 12 'mandatory fields' when 9 are actually 'optional'
 
"In BED, data lines are defined as tab-separated plain text with 12 mandatory fields (columns). Of those, only the first three fields are required, and the other 9 are optional."
 
Other than that, I don't have any comments. I thought it was generally well written, sufficiently descriptive and the examples were good.
