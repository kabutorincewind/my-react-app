The submission comprises the following resources:

mzQuantML1.0.0-rc.doc (Release candidate version 1.0.0 of the mzQuantML schema documentation)
mzQuantML_1_0_0-rc.xsd (Release candidate version 1.0.0 of the mzQuantML XSD)

Please note, the MIAPEQuant document is currently under active development. Mappings from mzQuantML to MIAPEQuant will be provided when that document is finalised.


The mzQuantML specifications should be read in conjunction with the following resources:

http://code.google.com/p/mzquantml/source/browse/#svn/trunk/examples/version1.0-rc1/ (Example files)
http://psidev.cvs.sourceforge.net/viewvc/*checkout*/psidev/psi/psi-ms/mzML/controlledVocabulary/psi-ms.obo (The PSI-MS controlled vocabulary)
http://code.google.com/p/mzquantml/source/browse/trunk/cv/mzQuantML-mapping_1.0.0-rc.xml (The mapping file governing CV usage in the schema

Any open issues with the specifications will be discussed here:
http://code.google.com/p/mzquantml/issues/list 
 
