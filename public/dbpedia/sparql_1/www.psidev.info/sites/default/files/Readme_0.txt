Readme.txt
********************

The attached files comprise the final submission of the GelML Version 1.0 specifications:

- GelML_version1.doc		
Notes: The main spec document

- GelML-v1-refManual.html.txt
Notes: The GelML reference manual. This should be read in conjuction with the main spec document. The file should be saved onto a local drive and the file extension changed to ".html".

- MIAPE_GE_1_2_EncodeGelML.doc			
Notes: A document demonstrating how MIAPE GE can be encoded in GelML

- GelML-V1.mdzip						
Notes: The GelML UML model, requires MagicDraw 11.5 or above

- FuGE-v1-profile.mdzip					
Notes: The FuGE UML model profile. The GelML UML model must be supplied with a local reference to this file in MagicDraw.

- Profiles.zip												
Notes: A zip archive containing the AndroMDA profiles required for XSD generation. This should be unzipped and MagicDraw will ask for the location of the andromda-profile-3.2-SNAPSHOT.xml.zip file

- XMLInstances.zip
Notes: A zip archive containing a directory of sample instance files and the XML Schema for GelML and FuGE.


