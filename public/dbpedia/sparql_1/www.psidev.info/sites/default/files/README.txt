The version 1.1 release of mzIdentML comprises the following resources:

mzIdentML1.1.0.xsd (Version 1.1.0 of the mzIdentML XSD)
mzIdentML1.1.0.doc (specification document)
mzIdentML_MCP_1.1.0.doc (Mapping between the schema and the MCP guidelines)
mzIdentML_MIAPE_1.1.0.doc (Mapping between the schema and the MIAPE MSI guidelines)


The mzIdentML v1,1 specifications should be read in conjunction with the following resources:

http://code.google.com/p/psi-pi/source/browse/#svn/trunk/examples (Directory of example files - also in associated zip file)
http://psidev.cvs.sourceforge.net/viewvc/*checkout*/psidev/psi/psi-ms/mzML/controlledVocabulary/psi-ms.obo (The PSI-MS controlled vocabulary)
http://code.google.com/p/psi-pi/source/browse/trunk/cv/mzIdentML-mapping_1.1.0.xml(The mapping file governing CV usage in the schema)

Any open issues with the specifications will be discussed here:
http://code.google.com/p/psi-pi/issues/list  
