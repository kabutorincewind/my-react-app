
	
	(function($) {

		_tip = $.Class({
			_init: function(ele, opts){
				this.wrap = $('<div />')
					.appendTo('body')
					.css({
						position: 'absolute',
						width : opts.width + 'px',
						'z-index': 99
					});
				this.arrow = $('<div />')
					.appendTo(this.wrap)
					.css('text-align', opts.dir)
				
				this.main = $('<div />')
					.appendTo(this.wrap)
					.addClass(opts.css);
					//.height(opts.height - this.arrow.height());
				this.closewrap = null;
				if( opts.close ){
					this.closewrap = $('<div />')
						.appendTo(this.main)
						.css({
							'text-align' : 'right'
						});
					$('<a />')
						.attr('href', 'javascript:;')
						.appendTo(this.closewrap)
						.text('X')
						.css({
							'color':'#666',
							'text-decoration' : 'none',
							'padding' : '0 0px'
						});
					this.bindClose();
				}
				this.main.append(opts.content);
				
				var t = this;
				var resetPos = function(){
					var offset = ele.offset();
					var left = opts.dir == 'left' ? offset.left : offset.left - opts.width + ele.width();
					t.wrap.css({
						top: '20px',
						left: left + 'px'
					});
				}
				
				resetPos();
				$(window)
					.load(resetPos)		// just in case user is changing size of page while loading
					.resize(resetPos);
			},
			
			hide: function(){
				this.wrap.fadeOut();
			},
			
			close: function(){
				this.wrap.hide();
			},
			
			show: function(){
				$(".tip").parent().hide();
				this.wrap.fadeIn();
			},
			
			remove: function(){
				this.wrap.remove();
			},
			
			bindClose: function(){
				var t = this;
				this.closewrap.find('a').click(function(){t.hide()});
			},
			
			setWidth: function(width){
				this.wrap.css('width', width + 'px');
			},
			
			//setHeight: function(height){
			//	this.main.css('height', (height - this.arrow.height()) + 'px');
			//},
			
			setContent: function(content){
				if( this.closewrap ){
					this.main.empty().append(this.closewrap).append(content);
					this.bindClose();
				} else this.main.html(content);
			},
			
			isVisible: function(){
				return this.wrap.is(':visible');
			}
			
		});
		
		$.tip = function(ele, options){
			options = options || {};
			options.img    = options.img     || '../../images/tip.png';
			options.css    = options.css     || 'tip';
			options.width  = options.width   || 200;
			//options.height = options.height  || 120;
			options.dir    = options.dir     || 'left';
			options.close  = typeof(options.close) == 'undefined' ? true : options.close;
			options.content= options.content || 'Hello, world!';
			return new _tip(ele, options);
		}
		
	})(jQuery);