var dolite_pageSize = 7;
var chrLen = [
	249250621, // chr1
	243199373,
	198022430,
    191154276,
    180915260,
    171115067,
    159138663,
    146364022,
    141213431,
    135534747,
    135006516,
    133851895,
    115169878,
    107349540,
    102531392,
    90354753,
    81195210,
    78077248,
    59128983,
    63025520,
    48129895,
    51304566,
	155270560,
	59373566 // chrY
];
var chrColor = [
	'#335DAF', 
	'#0FF24E',
	'#7BC74D',
    '#D599D5',
    '#C7E4E8',
    '#8A6FB4',
    '#FFAF39',
    '#CB75B4',
    '#F63234',
    '#BBE09D',
    '#C63135',
    '#000001',
    '#FCD3D9',
    '#C767E8',
    '#7B384D',
    '#F63034',
    '#BCF00D',
    '#D8d800',
    '#F06434',
    '#FFD74E',
    '#000045',
    '#33956F',
	'#7B234D',
	'#00F5B4'
];

var outLinks = {
	pubmed : 'http://www.ncbi.nlm.nih.gov/pubmed/',
	omim: 'http://www.ncbi.nlm.nih.gov/omim/',
	gad: 'http://geneticassociationdb.nih.gov/cgi-bin/view.cgi?table=allview&id=',
	pharm: 'http://www.pharmgkb.org/do/serve?objId=',
	entrezgene: 'http://www.ncbi.nlm.nih.gov/gene?term=',
	dbgap: 'http://www.ncbi.nlm.nih.gov/projects/gap/cgi-bin/study.cgi?study_id=phs'
}
