(function(Raphael, $){

	$.rldplot = function(wrapid, width, height, data, chr, rsid){
	
		
		var theSnp = {
			chr_m1 : data[0].rs_m2 == rsid ? data[0].chr_m2 : data[0].chr_m1,
			rs_m1  : rsid, 
			rsquare: 1.0
		}
		
		var paper = Raphael(wrapid, width, height);
		paper.customAttributes.data = function(d){return {data:d};}
		var margin = {
			left : 35,
			top : 20,
			bottom : 40,
			right: 35
		};
		var yPxLen = height - margin.top - margin.bottom;
		var xPxLen = width - margin.left - margin.right;
		
		var yMin = .5;
		var yMax = 1.0;
		var xMin = 250000000;
		var xMax = -1;
		var rulerNum = 5;
		for( var i=0, len=data.length; i<len; i++ ){
			var pos = data[i].rs_m1 == rsid ? data[i].chr_m2 : data[i].chr_m1;
			xMin = xMin > pos ? pos : xMin;
			xMax = xMax < pos ? pos : xMax;
		}
		xMin = xMin > theSnp.chr_m1 ? theSnp.chr_m1 : xMin;
		xMax = xMax < theSnp.chr_m1 ? theSnp.chr_m1 : xMax;
		//if( xMax - theSnp.chr_m1 > theSnp.chr_m1 - xMin )
		//	xMin = 2*theSnp.chr_m1 - xMax;
		//else
		//	xMax = 2*theSnp.chr_m1 - xMin;
		xMin -= parseInt((xMax-xMin)/20/rulerNum);
		xMax += parseInt((xMax-xMin)/20/rulerNum);
			
		var xPerPx = (xMax - xMin) / xPxLen;
		var yPerPx = (yMax - yMin) / yPxLen;
		var r2coord = function(r){
			return yPxLen - (r-yMin)/yPerPx + margin.top;
		};
		var bp2coord = function(bp){
			return (bp-xMin)/xPerPx + margin.left;
		};
		
		// draw y anxis
		paper.text(28, 8, 'R-squared');
		paper.path('M' + margin.left + ' ' + margin.top +
				   'L' + margin.left + ' ' + (height - margin.bottom));
		for( var i=yMin; i<=yMax; i+=(yMax-yMin)/rulerNum ){
			i = Math.ceil(i*10) / 10;
			paper.path('M' + (margin.left-4) + ' ' + r2coord(i) +
					   'L' + (margin.left) + ' ' + r2coord(i));
			paper.text(14, r2coord(i), i);
		}
		
		// draw x anxis
		paper.path('M' + margin.left + ' ' + r2coord(yMin) +
				   'L' + (bp2coord(xMax)) + ' ' + r2coord(yMin));
		for( var i=xMin; i<=xMax; i+=(xMax-xMin)/rulerNum ){
			paper.path('M' + bp2coord(i) + ' ' + (r2coord(yMin)) +
					   'L' + bp2coord(i) + ' ' + (r2coord(yMin) + 4 ));
			paper.text( bp2coord(i), r2coord(yMin)+14, $.formatInt(parseInt(i)) );
		}
		paper.text(bp2coord((xMax+xMin)/2), r2coord(yMin) + 26, 'Chromosome '+chr+' Position');
		
		// draw dots...
		for( var i=0, len=data.length; i<len; i++ ){
			var pos = data[i].rs_m1 == rsid ? data[i].chr_m2 : data[i].chr_m1;
			var dot = paper.circle(bp2coord(pos), r2coord(data[i].rsquare), 2).attr({
				data: data[i],
				fill: 'rgba(255,0,0,'+((data[i].rsquare-.5)*2)+')',
				stroke: 'none',
				cursor: 'pointer'
			}).mouseover(function(){this.attr({r:4})}).mouseout(function(){this.attr({r:2})});
			$(dot.node).floatip('<b>rsId: </b>' + (data[i].rs_m1 == rsid ? data[i].rs_m2 : data[i].rs_m1) + '<br />' +
								'<b>Chromosome: </b>' + chr + '<br />' +
								'<b>Position: </b>' + pos + '<br />' +
								'<b>R-squared: </b>' + data[i].rsquare + '<br />' +
								'<b>Dprime: </b>' + data[i].dprime + '<br />' +
								'<b>Lod: </b>' + data[i].lod + '<br />');
		}
		// draw the snp
		var thedot = paper.circle(bp2coord(theSnp.chr_m1), r2coord(theSnp.rsquare), 4).attr({
			fill: '#f0f',
			stroke: '#ff0',
			cursor: 'pointer'
		});
		$(thedot.node).floatip('<b>The Queried SNP</b> <br />' +
							   '<b>rsId: </b>' + rsid + '<br />' +
							   '<b>Chromosome: </b>' + chr + '<br />' +
							   '<b>Position: </b>' + theSnp.chr_m1);
	
	};

})(Raphael, jQuery);