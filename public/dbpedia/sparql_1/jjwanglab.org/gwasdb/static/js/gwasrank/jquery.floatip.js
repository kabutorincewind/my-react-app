(function($) {
 
    $.floatip = function(src, content, cls) {
	if(!content) return;
        var $tdiv = $('<div />');
        $tdiv.css({
            'position':'absolute',
            'display':'none',
            'z-index':'9999'
        }).html(content).appendTo('body');
 
		cls = cls || 'floatip';
        $tdiv.addClass(cls);
 
        var move = function(e){
			var y = e.pageY + 23;
			if(y+$tdiv.outerHeight()>$(window).height()){
				y = y - $tdiv.outerHeight() - 35;
			}
			var x = e.pageX;
			if( x+$tdiv.outerWidth()>$(window).width() ){
				x = x - $tdiv.outerWidth();
			}
            $tdiv.css({
                'top' : y + 'px',
                'left' : x + 'px'
            });
        };
		var timeout = null;
        $(src).mouseenter(function(e){
            $tdiv.show();
			timeout = setTimeout(function(){
				$tdiv.fadeOut();
			}, 5000);
            $(document).bind('mousemove', move);
        });
 
        $(src).mouseleave(function(){
			if(timeout) clearTimeout(timeout);
            $tdiv.hide();
            $(document).unbind('mousemove', move);
        });
 
    }
 
    // content: what is the tip
    // cls: the style class of the tip div
    $.fn.floatip = function(content, cls) {
 
        this.each(function() {
            new $.floatip(this, content, cls);
        });
        return this;    
    };
})(jQuery);