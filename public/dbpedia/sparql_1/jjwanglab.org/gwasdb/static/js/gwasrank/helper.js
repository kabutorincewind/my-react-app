/**
 *
 * Some helper functions under the jQuery namespace.
 * It makes things easier to handle in Gviewer.
 *
 * @Author: pwwang
 * @Date  : 21/01/2011
 * 
 */

(function($){

	/**
	 *
	 * Extend a function icss of $.fn to 
	 * get an integer css property value of an element.
	 *
	 * @Param : k - the key of css property
	 * @Return: v - the integer value of the css property
	 *              or boolean false if invalid.
	 *
	 **/
	 
	$.extend($.fn, {
		icss : function(k){
			var v = parseInt($(this).css(k));
			if( isNaN(v) ) return false;
			return v;
		}
	});
	
	/**
	 *
	 * To dump an object, simply with no recursion. 
	 *
	 * @Param : obj - the object to dump
	 * @Return: ret - the dumped string result
	 *
	 **/
	
	$.dump = function(obj){
		var ret = '';
		for( var k in obj ){
			ret += k + "\t: " + obj[k] + "\n";
		}
		return ret;
	};
	
	/**
	 *
	 * To display a message, use window.console.log 
	 * if it is availab or use window.alert.
	 *
	 * @Param : msg - message to show
	 * @Return: 
	 *
	 **/
	
	$.log = function(msg){
	    if( typeof msg == 'object' ) msg = $.dump(msg);
		if( window.console && window.console.log ){
			window.console.log(msg)	;
		} else {
			window.alert(msg);
		}
	};
	
	/**
	 *
	 * To tell whether an object is a jQuery object.
	 *
	 * @Param : obj - the object to tell
	 * @Return: boolean
	 *
	 **/
	
	$.isJQuery = function(obj){
		return ( obj instanceof $ );
	};
	
	/**
	 *
	 * Extend js array 
	 *
	 **/
	
	$.array = $.Class({
		
		// constructor
		// array : array to be extended
		_init: function(arr){
			this.array = arr || [];	
		},
		
		// add an element to array
		add: function(el){
			this.array.push(el);	
		},
		
		// insert an element
		insertAt: function(el, i){
			this.array.splice(i,0,el);
		},
		
		// move an element
		move: function(at, to){
			this.insertAt( this.array[at], to > at ? to + 1 : to );
			this.removeAt( to < at ? at + 1 : at );
		},
		
		// find an element by value
		find: function(el){
			for( var i=0; i<this.array.length; i++ ){
				if( this.array[i] === el )	{
					return i;	
				}
			}
			return -1;
		},
		
		// each
		each: function(func){
			for( var i=0; i<this.array.length; i++ ){
				func(i, this.array[i]);	
			}
		},
		
		// find an element by subscript
		get: function(i){
			return this.array[i];
		},
		
		// remove an element from array by value
		remove: function(el){
			var i = this.find(el);
			if( i>-1 ){
				this.removeAt(i);
			}
		},
		
		// remove an element from array by key
		removeAt: function(i){
			this.array.splice(i, 1);
			
		},
		
		// get the count of array
		count: function(){
			return this.array.length;
		},
		
		// clear the array
		clear: function(){
            this.array = [];
		}
	});
	
	/**
	 *
	 * Event listener 
	 *
	 **/
	
	$.listener = $.Class({
		
		// constructor
		// if handler is jQuery, use original jQuery event listener
		// else convert the canvas element to jQuery
		// obj : object to be convert to jQuery as event handler.
		_init: function(obj){
			if( $.isJQuery( obj ) ){
				this.handler = obj;	
			} else {
				this.handler = $(obj.node);	
			}
		},
		
		// add a listener to handler		
		// and specify the THIS of fn to tiz.
		add: function(et, fn, tiz){
			tiz = tiz || this;
			this.handler.bind(et, (function(f){
				return function(){
					fn.apply(tiz, arguments);	
				}	
			})(fn));	
			return this;
		},
		
		// remove listener
		remove: function(et, fn){
			this.handler.unbind(et, fn);	
			return this;
		},
		
		// add a listener to handler
		// and remove it once it is fired.
		one: function(et, fn, tiz){
			tiz = tiz || this;
			this.handler.one(et, (function(f){
				return function(){
					fn.apply(tiz, arguments);	
				}	
			})(fn));
			return this;
		},
		
		// add a lister to a existed handler or to-be-existed handler
		live: function(et, fn, tiz){
			tiz = tiz || this;
			this.handler.live(et, (function(f){
				return function(){
					fn.apply(tiz, arguments);	
				}	
			})(fn));
			return this;
		}

	});
	
	// use setTimeout to delay a function
	Function.prototype.delay = function(t){
		setTimeout(this, t);
	}
	
	$.formatInt = function(num){
		var str = parseInt(num);
		str = num + '';
		if( str.length <= 3 ) return str;
		var ret = str.substr(0, str.length % 3) ;
		for( var i=str.length % 3; i<str.length; i+=3 ){
			ret += (ret=='' ? '' : ',') + str.substr(i, 3);
		}
		return ret;
	};
	

})(jQuery);