
(function($){
	
	var _tablePager = $.Class({
		_init: function(table, ps, infowrap, data){
			this.tableObject = $(table);
			this.totalRows = 0;
			this.from = 0;
			this.to = 0;
			this.totalPages = 0;
			this.pageSize = ps;
			this.currentPage = 1;
			//pagerInfo = 'view:{from} ~ {to}, total:{totalRows}, page:{currentPage}/{totalPages}';
			this.pagerInfo = 'Total:{totalRows} Page:{currentPage}/{totalPages} ';
			this.currentData	= null;
			this.infoWrap = infowrap;
			this.addData(data);
			this.setPageSize(this.pageSize);
		},
		
		setPagerInfo : function(info) {
			this.pagerInfo = info || this.pagerInfo;
		},
		
		setPageSize : function(size) {
			this.pageSize = size;
			this.totalRows = $('tbody tr', this.tableObject).length;
			this.totalPages = Math.ceil(this.totalRows / this.pageSize);
			this.moveCurrentPage();
		},
		
		setCurrentPage : function(pageNo) {
			if(pageNo > this.totalPages){
				this.currentPage = this.totalPages;
			} else if(pageNo < 1){
				this.currentPage = 1;
			} else {
				this.currentPage = pageNo;
			}
		},
		
		addData : function(data){
			var data2append = '';
			for( var i=0; i<data.length; i++ ){
				data2append += '<tr class="' +(i%2 ? 'even' : 'odd')+ '">';
				for( var j=0; j<data[i].length; j++ ){
					data2append += '<td>' + data[i][j] + '</td>';
				}
				data2append += "</tr>\n";
			}
			this.tableObject.find('tbody').append(data2append);
		},
		
		getPagerInfo : function (){
			var clonePagerInfo = this.pagerInfo;
			clonePagerInfo = clonePagerInfo.replace('{from}',this.from+1)
			clonePagerInfo = clonePagerInfo.replace('{to}',this.to+1);
			clonePagerInfo = clonePagerInfo.replace('{totalRows}',this.totalRows);
			clonePagerInfo = clonePagerInfo.replace('{currentPage}',this.currentPage);
			clonePagerInfo = clonePagerInfo.replace('{totalPages}',this.totalPages);
			return clonePagerInfo;
		},
		
		moveRangePage : function (start,end){
			if(this.currentData != null){
				this.currentData.hide();
			} else {
				this.tableObject.find('tbody tr').css('display','none');
			}
			this.from = start;
			this.to = end;
			this.setCurrentPage(this.currentPage);
			this.currentData = this.tableObject.find('tbody tr:eq(' + (start) + ')');
			this.currentData = this.currentData.nextAll().andSelf().not(':gt(' + (end-(this.pageSize*(this.currentPage-1))) + ')');
			this.currentData.show();	
		},
		
		moveSetPage : function (pagerNo){
			this.setCurrentPage(pagerNo);
			this.moveCurrentPage(this.from, this.to);
		},
		
		//public moveCurrentPage
		moveCurrentPage : function (){
			this.setCurrentPage(this.currentPage);
			var from = (this.currentPage-1) * this.pageSize;
			var to = ((this.currentPage) * this.pageSize - 1);
			
			this.moveRangePage(from,to);
			this.updateInfo();
		},
		//public moveFirstPage
		moveFirstPage : function() {
			this.setCurrentPage(1);
			this.moveCurrentPage();
		},
		//public moveLastPage
		moveLastPage : function() {
			this.setCurrentPage(this.totalPages);
			this.moveCurrentPage();
		},
		//public moveNextPage
		moveNextPage : function() {
			this.currentPage++;
			this.moveCurrentPage();
		},
		//public movePrevPage
		movePrevPage : function() {
			this.currentPage--;
			this.moveCurrentPage();
		},
		
		highlight : function(index, clz){
			clz = clz || 'dolite_tablepager_highlight';
			this.tableObject.find('tbody tr.'+clz).removeClass(clz);
			var p = Math.ceil((index+1)/this.pageSize);
			this.setCurrentPage(p);
			this.moveCurrentPage();
			var tr = this.tableObject.find('tbody tr:eq('+index+')').addClass(clz);
			setTimeout(function(){
				tr.removeClass(clz);
			}, 100);
			setTimeout(function(){
				tr.addClass(clz);
			}, 300);
			setTimeout(function(){
				tr.removeClass(clz);
			}, 100);
		},
		
		updateInfo : function(){
			this.infoWrap.text(this.getPagerInfo());
		},
		
		addInfoWrap: function(infowrap){
			this.infoWrap = this.infoWrap.add(infowrap);
		}
		
	});

	$.fn.tablepager = function(opts) {
		opts = opts || {};
		opts.oper = opts.oper || 'both';  // up, both
		opts.data = opts.data || [];
		opts.pageSize = opts.pageSize || 30;
		opts.pageInfo = opts.pageInfo || '';
			
		//this.each(function() {
			var infowrap = $('<div style="text-align:right"></div>');
			var info = $('<span class="tablepager_info" />')
				.appendTo(infowrap)
				
			var ts = new _tablePager(this, opts.pageSize, info, opts.data);
			ts.setPagerInfo(opts.pageInfo);
			
			var first = $('<a href="javascript:;">First</a>')
				.appendTo(infowrap)
				.click(function(){
					ts.moveFirstPage();
				});
			infowrap.append('&nbsp;');
			var prev = $('<a href="javascript:;">Prev</a>')
				.appendTo(infowrap)
				.click(function(){
					ts.movePrevPage();
				});
			infowrap.append('&nbsp;');
			var next = $('<a href="javascript:;">Next</a>')
				.appendTo(infowrap)
				.click(function(){
					ts.moveNextPage();
				});
			infowrap.append('&nbsp;');
			var prev = $('<a href="javascript:;">Last</a>')
				.appendTo(infowrap)
				.click(function(){
					ts.moveLastPage();
				});
			
			if( /^(?:up|both)$/.test(opts.oper) ){
				infowrap.insertBefore($(this));
			}
			if( /^(?:down|both)$/.test(opts.oper) ){
				var clone_infowrap = infowrap.clone(true);
				ts.addInfoWrap(clone_infowrap.find('.tablepager_info'));
				clone_infowrap.insertAfter($(this));
			}
		//});
		return ts;
	};

})(jQuery);